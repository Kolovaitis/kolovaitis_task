package by.kolovaitis.getdatafromcontentprovider2

import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import androidx.core.view.children
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        getApps.setOnClickListener {
            try {
                val uri =
                    Uri.parse("content://by.kolovaitis.provider/apps")
                val cursor = contentResolver.query(uri, null, null, null, null)
                cursor?.moveToFirst()
                linearLayout.removeAllViews()
                for (i in 0..(cursor?.count!! - 1)) {
                    val textView = TextView(this)
                    textView.setText(cursor.getString(cursor.getColumnIndex("package")))
                    linearLayout.addView(textView)
                    cursor?.moveToNext()
                }
            } catch (e: Exception) {
                Toast.makeText(this, "Access denied", Toast.LENGTH_LONG).show()
            }

        }
        getLast.setOnClickListener {
            try {
                val uri =
                    Uri.parse("content://by.kolovaitis.provider/appslast")
                val cursor = contentResolver.query(uri, null, null, null, null)
                cursor?.moveToFirst()
                linearLayout.removeAllViews()
                val textView = TextView(this)
                textView.setText(cursor?.getString(cursor.getColumnIndex("package")))
                linearLayout.addView(textView)

            } catch (e: Exception) {
                Toast.makeText(this, "Access denied", Toast.LENGTH_LONG).show()
            }
        }
    }
}

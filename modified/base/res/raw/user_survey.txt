Thank you for using Epson printer.
For the purpose that Epson provides products and services to meet your needs in the future, Epson uses the web access analysis service ""Google Analytics"" by Google Inc. and ask for your cooperation in survey of the usage conditions of this application. 
Google Analytics uses cookies to collect your information. How Google collect and use the information, please refer to the Google Privacy Policy site from the following.
http://www.google.com/intl/en/policies/privacy/
Epson collects your device information and the usage conditions of this application through Google Analytics.
NOTE: NO PERSONAL INFORMATION OR INFORMATION THAT IDENTIFIES, OR CAN BE USED IN CONNECTION WITH OTHER INFORMATION TO IDENTIFY, YOU AS AN INDIVISUAL WILL BE COLLECTED DURING THIS PROCESS.  EPSON ONLY COLLECTS DE-IDENTIFIED INFORMATION.  
Epson Privacy Statement:
http://global.epson.com/privacy_policy.html
　　Please note that Epson is not responsible for any applicable charges for Internet connectively through your device.

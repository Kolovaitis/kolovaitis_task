﻿PRIVACY STATEMENT
Effective Date January 22, 2018

Introduction. This privacy statement ("Privacy Statement") is designed to provide information about the privacy and data usage practices of Seiko Epson Corporation and its subsidiaries and affiliates (collectively referred to herein as "Epson", "we", "our" or "us") for the Epson iPrint and any other software products or software features that refer to, post a link to, or otherwise include this Privacy Statement (collectively the "Service"). This Privacy Statement does not apply to any other websites, applications or services.

Please review this Privacy Statement carefully. If you have questions or concerns regarding this Privacy Statement, contact Epson’s privacy coordinator by sending email to Privacy.epsonmobileprintapps@exc.epson.co.jp,　or sending a letter to 80 Harashinden, Hirooka, Shiojiri, Nagano, Japan, Seiko Epson Corporation Printing Solutions Division, Epson Mobile Application (for Printer) Personal Information Protection Manager. 

In addition, please review any applicable terms and conditions, which govern your use of the Service.

Information we collect through the Application. We will only use Google Analytics provided by Google Inc. to collect the following information if you opt-in to the usage survey of the Application (“Usage Survey”).
See the URL (https://www.google.com/intl/en/policies/privacy/partners/) for details on how data is used by Google Inc.

Information we collect does not include identification information on the individual. Information is collected through the Application to improve the service.

Google Analytics uses first party cookies to record information on the usage status of users.

Epson does not collect personally identifiable information. Epson uses Google Analytics to help us measure usage trends including the following to view and analyze the application usage by you through the Usage Survey.

User information
 * Version of the application
 * Location and language
 * Frequency of application usage and usage time
 * Most frequently used OS / device
Number of users
 * Trends in the number of new users who installed the application
Behavior information
 * View status of each screen and total screen impressions per session
 * The order and flow in which the screens were displayed
 * Number of technical errors such as crash of application
 * Time taken for each application element to read
 * Function usage of application
 * Connection status to device
 * Print settings and number of printed pages

Opt-out. You can prevent this data from being collected by opting out of the Usage Survey in the application's settings menu.

How we use information. Based on the above information, Epson may use information from Usage Survey that we receive to understand user's behavior and continually improves user-friendly applications monitor metrics, diagnose or fix technology problems, develop and test new features. The aggregated information may be shared within the Epson affiliated companies. For a list of Epson affiliated companies, you can check the link below.
https://global.epson.com/company/global_network/

How we store information. The Usage Survey data is stored and processed by Google, and Epson uses and analyzes this information collected through the Usage Survey. Such information may be stored and processed in Japan.

Children’s Privacy. Epson does not knowingly collect or solicit any information from anyone under the age of 13 or knowingly allow such persons to register for the Application or Usage Survey. The Application or Usage Survey and its contents are not directed at children under the age of 13. In the event that we learn that we have collected personal information from a child under age 13 without parental consent, we will delete that information as quickly as possible.

Access to function. In order that you use the Application, the Application accesses following functions of your device. 
	Camera: Camera function is accessed for printing the captured images.* 
	Wi-Fi: Wi-Fi function is accessed for connecting the devices.*

The Application accesses these functions, but Epson does not store or collect any access logs or contents that are printed through your device.

Access to data. In order that you use the Application, the Application accesses the following data from your device. 
	Image files: The specified image files are accessed for printing them.* 
	Document files: The specified document files are accessed for printing them.*

	*Depending on OS version or devices, some functions may not be available. 

The Application accesses these files, but Epson does not store or collect any access logs or contents that are projected through your device.

Complaints, inquiries, and requests for disclosure. You should direct any complaints, inquiries, and requests pertaining to this Privacy Statement to the Privacy Coordinator. You can terminate your use of the Service by following the instructions set forth in the applicable terms of service, or by uninstalling and deleting the software or application.

Contacting Us. If you have any questions about this Privacy Statement, our practices, or your dealings with the Services, or you have reason to believe Epson may have failed to adhere to this Privacy Statement, you can contact us by email or you can write to:

Privacy Coordinator. 
       80 Harashinden, Hirooka, Shiojiri, Nagano, Japan
       Seiko Epson Corporation
       Printing Solutions Division
       Epson Mobile Application (for Printer) Personal Information Protection Manager
       Privacy.epsonmobileprintapps@exc.epson.co.jp
       


.class public Lorg/apache/commons/lang/text/StrTokenizer;
.super Ljava/lang/Object;
.source "StrTokenizer.java"

# interfaces
.implements Ljava/util/ListIterator;
.implements Ljava/lang/Cloneable;


# static fields
.field private static final CSV_TOKENIZER_PROTOTYPE:Lorg/apache/commons/lang/text/StrTokenizer;

.field private static final TSV_TOKENIZER_PROTOTYPE:Lorg/apache/commons/lang/text/StrTokenizer;


# instance fields
.field private chars:[C

.field private delimMatcher:Lorg/apache/commons/lang/text/StrMatcher;

.field private emptyAsNull:Z

.field private ignoreEmptyTokens:Z

.field private ignoredMatcher:Lorg/apache/commons/lang/text/StrMatcher;

.field private quoteMatcher:Lorg/apache/commons/lang/text/StrMatcher;

.field private tokenPos:I

.field private tokens:[Ljava/lang/String;

.field private trimmerMatcher:Lorg/apache/commons/lang/text/StrMatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 93
    new-instance v0, Lorg/apache/commons/lang/text/StrTokenizer;

    invoke-direct {v0}, Lorg/apache/commons/lang/text/StrTokenizer;-><init>()V

    sput-object v0, Lorg/apache/commons/lang/text/StrTokenizer;->CSV_TOKENIZER_PROTOTYPE:Lorg/apache/commons/lang/text/StrTokenizer;

    .line 94
    sget-object v0, Lorg/apache/commons/lang/text/StrTokenizer;->CSV_TOKENIZER_PROTOTYPE:Lorg/apache/commons/lang/text/StrTokenizer;

    invoke-static {}, Lorg/apache/commons/lang/text/StrMatcher;->commaMatcher()Lorg/apache/commons/lang/text/StrMatcher;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/commons/lang/text/StrTokenizer;->setDelimiterMatcher(Lorg/apache/commons/lang/text/StrMatcher;)Lorg/apache/commons/lang/text/StrTokenizer;

    .line 95
    sget-object v0, Lorg/apache/commons/lang/text/StrTokenizer;->CSV_TOKENIZER_PROTOTYPE:Lorg/apache/commons/lang/text/StrTokenizer;

    invoke-static {}, Lorg/apache/commons/lang/text/StrMatcher;->doubleQuoteMatcher()Lorg/apache/commons/lang/text/StrMatcher;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/commons/lang/text/StrTokenizer;->setQuoteMatcher(Lorg/apache/commons/lang/text/StrMatcher;)Lorg/apache/commons/lang/text/StrTokenizer;

    .line 96
    sget-object v0, Lorg/apache/commons/lang/text/StrTokenizer;->CSV_TOKENIZER_PROTOTYPE:Lorg/apache/commons/lang/text/StrTokenizer;

    invoke-static {}, Lorg/apache/commons/lang/text/StrMatcher;->noneMatcher()Lorg/apache/commons/lang/text/StrMatcher;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/commons/lang/text/StrTokenizer;->setIgnoredMatcher(Lorg/apache/commons/lang/text/StrMatcher;)Lorg/apache/commons/lang/text/StrTokenizer;

    .line 97
    sget-object v0, Lorg/apache/commons/lang/text/StrTokenizer;->CSV_TOKENIZER_PROTOTYPE:Lorg/apache/commons/lang/text/StrTokenizer;

    invoke-static {}, Lorg/apache/commons/lang/text/StrMatcher;->trimMatcher()Lorg/apache/commons/lang/text/StrMatcher;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/commons/lang/text/StrTokenizer;->setTrimmerMatcher(Lorg/apache/commons/lang/text/StrMatcher;)Lorg/apache/commons/lang/text/StrTokenizer;

    .line 98
    sget-object v0, Lorg/apache/commons/lang/text/StrTokenizer;->CSV_TOKENIZER_PROTOTYPE:Lorg/apache/commons/lang/text/StrTokenizer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/apache/commons/lang/text/StrTokenizer;->setEmptyTokenAsNull(Z)Lorg/apache/commons/lang/text/StrTokenizer;

    .line 99
    sget-object v0, Lorg/apache/commons/lang/text/StrTokenizer;->CSV_TOKENIZER_PROTOTYPE:Lorg/apache/commons/lang/text/StrTokenizer;

    invoke-virtual {v0, v1}, Lorg/apache/commons/lang/text/StrTokenizer;->setIgnoreEmptyTokens(Z)Lorg/apache/commons/lang/text/StrTokenizer;

    .line 101
    new-instance v0, Lorg/apache/commons/lang/text/StrTokenizer;

    invoke-direct {v0}, Lorg/apache/commons/lang/text/StrTokenizer;-><init>()V

    sput-object v0, Lorg/apache/commons/lang/text/StrTokenizer;->TSV_TOKENIZER_PROTOTYPE:Lorg/apache/commons/lang/text/StrTokenizer;

    .line 102
    sget-object v0, Lorg/apache/commons/lang/text/StrTokenizer;->TSV_TOKENIZER_PROTOTYPE:Lorg/apache/commons/lang/text/StrTokenizer;

    invoke-static {}, Lorg/apache/commons/lang/text/StrMatcher;->tabMatcher()Lorg/apache/commons/lang/text/StrMatcher;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/apache/commons/lang/text/StrTokenizer;->setDelimiterMatcher(Lorg/apache/commons/lang/text/StrMatcher;)Lorg/apache/commons/lang/text/StrTokenizer;

    .line 103
    sget-object v0, Lorg/apache/commons/lang/text/StrTokenizer;->TSV_TOKENIZER_PROTOTYPE:Lorg/apache/commons/lang/text/StrTokenizer;

    invoke-static {}, Lorg/apache/commons/lang/text/StrMatcher;->doubleQuoteMatcher()Lorg/apache/commons/lang/text/StrMatcher;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/apache/commons/lang/text/StrTokenizer;->setQuoteMatcher(Lorg/apache/commons/lang/text/StrMatcher;)Lorg/apache/commons/lang/text/StrTokenizer;

    .line 104
    sget-object v0, Lorg/apache/commons/lang/text/StrTokenizer;->TSV_TOKENIZER_PROTOTYPE:Lorg/apache/commons/lang/text/StrTokenizer;

    invoke-static {}, Lorg/apache/commons/lang/text/StrMatcher;->noneMatcher()Lorg/apache/commons/lang/text/StrMatcher;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/apache/commons/lang/text/StrTokenizer;->setIgnoredMatcher(Lorg/apache/commons/lang/text/StrMatcher;)Lorg/apache/commons/lang/text/StrTokenizer;

    .line 105
    sget-object v0, Lorg/apache/commons/lang/text/StrTokenizer;->TSV_TOKENIZER_PROTOTYPE:Lorg/apache/commons/lang/text/StrTokenizer;

    invoke-static {}, Lorg/apache/commons/lang/text/StrMatcher;->trimMatcher()Lorg/apache/commons/lang/text/StrMatcher;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/apache/commons/lang/text/StrTokenizer;->setTrimmerMatcher(Lorg/apache/commons/lang/text/StrMatcher;)Lorg/apache/commons/lang/text/StrTokenizer;

    .line 106
    sget-object v0, Lorg/apache/commons/lang/text/StrTokenizer;->TSV_TOKENIZER_PROTOTYPE:Lorg/apache/commons/lang/text/StrTokenizer;

    invoke-virtual {v0, v1}, Lorg/apache/commons/lang/text/StrTokenizer;->setEmptyTokenAsNull(Z)Lorg/apache/commons/lang/text/StrTokenizer;

    .line 107
    sget-object v0, Lorg/apache/commons/lang/text/StrTokenizer;->TSV_TOKENIZER_PROTOTYPE:Lorg/apache/commons/lang/text/StrTokenizer;

    invoke-virtual {v0, v1}, Lorg/apache/commons/lang/text/StrTokenizer;->setIgnoreEmptyTokens(Z)Lorg/apache/commons/lang/text/StrTokenizer;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 241
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 118
    invoke-static {}, Lorg/apache/commons/lang/text/StrMatcher;->splitMatcher()Lorg/apache/commons/lang/text/StrMatcher;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/commons/lang/text/StrTokenizer;->delimMatcher:Lorg/apache/commons/lang/text/StrMatcher;

    .line 120
    invoke-static {}, Lorg/apache/commons/lang/text/StrMatcher;->noneMatcher()Lorg/apache/commons/lang/text/StrMatcher;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/commons/lang/text/StrTokenizer;->quoteMatcher:Lorg/apache/commons/lang/text/StrMatcher;

    .line 122
    invoke-static {}, Lorg/apache/commons/lang/text/StrMatcher;->noneMatcher()Lorg/apache/commons/lang/text/StrMatcher;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/commons/lang/text/StrTokenizer;->ignoredMatcher:Lorg/apache/commons/lang/text/StrMatcher;

    .line 124
    invoke-static {}, Lorg/apache/commons/lang/text/StrMatcher;->noneMatcher()Lorg/apache/commons/lang/text/StrMatcher;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/commons/lang/text/StrTokenizer;->trimmerMatcher:Lorg/apache/commons/lang/text/StrMatcher;

    const/4 v0, 0x0

    .line 127
    iput-boolean v0, p0, Lorg/apache/commons/lang/text/StrTokenizer;->emptyAsNull:Z

    const/4 v0, 0x1

    .line 129
    iput-boolean v0, p0, Lorg/apache/commons/lang/text/StrTokenizer;->ignoreEmptyTokens:Z

    const/4 v0, 0x0

    .line 242
    iput-object v0, p0, Lorg/apache/commons/lang/text/StrTokenizer;->chars:[C

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 252
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 118
    invoke-static {}, Lorg/apache/commons/lang/text/StrMatcher;->splitMatcher()Lorg/apache/commons/lang/text/StrMatcher;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/commons/lang/text/StrTokenizer;->delimMatcher:Lorg/apache/commons/lang/text/StrMatcher;

    .line 120
    invoke-static {}, Lorg/apache/commons/lang/text/StrMatcher;->noneMatcher()Lorg/apache/commons/lang/text/StrMatcher;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/commons/lang/text/StrTokenizer;->quoteMatcher:Lorg/apache/commons/lang/text/StrMatcher;

    .line 122
    invoke-static {}, Lorg/apache/commons/lang/text/StrMatcher;->noneMatcher()Lorg/apache/commons/lang/text/StrMatcher;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/commons/lang/text/StrTokenizer;->ignoredMatcher:Lorg/apache/commons/lang/text/StrMatcher;

    .line 124
    invoke-static {}, Lorg/apache/commons/lang/text/StrMatcher;->noneMatcher()Lorg/apache/commons/lang/text/StrMatcher;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/commons/lang/text/StrTokenizer;->trimmerMatcher:Lorg/apache/commons/lang/text/StrMatcher;

    const/4 v0, 0x0

    .line 127
    iput-boolean v0, p0, Lorg/apache/commons/lang/text/StrTokenizer;->emptyAsNull:Z

    const/4 v0, 0x1

    .line 129
    iput-boolean v0, p0, Lorg/apache/commons/lang/text/StrTokenizer;->ignoreEmptyTokens:Z

    if-eqz p1, :cond_0

    .line 254
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object p1

    iput-object p1, p0, Lorg/apache/commons/lang/text/StrTokenizer;->chars:[C

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 256
    iput-object p1, p0, Lorg/apache/commons/lang/text/StrTokenizer;->chars:[C

    :goto_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;C)V
    .locals 0

    .line 267
    invoke-direct {p0, p1}, Lorg/apache/commons/lang/text/StrTokenizer;-><init>(Ljava/lang/String;)V

    .line 268
    invoke-virtual {p0, p2}, Lorg/apache/commons/lang/text/StrTokenizer;->setDelimiterChar(C)Lorg/apache/commons/lang/text/StrTokenizer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;CC)V
    .locals 0

    .line 302
    invoke-direct {p0, p1, p2}, Lorg/apache/commons/lang/text/StrTokenizer;-><init>(Ljava/lang/String;C)V

    .line 303
    invoke-virtual {p0, p3}, Lorg/apache/commons/lang/text/StrTokenizer;->setQuoteChar(C)Lorg/apache/commons/lang/text/StrTokenizer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 278
    invoke-direct {p0, p1}, Lorg/apache/commons/lang/text/StrTokenizer;-><init>(Ljava/lang/String;)V

    .line 279
    invoke-virtual {p0, p2}, Lorg/apache/commons/lang/text/StrTokenizer;->setDelimiterString(Ljava/lang/String;)Lorg/apache/commons/lang/text/StrTokenizer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/apache/commons/lang/text/StrMatcher;)V
    .locals 0

    .line 289
    invoke-direct {p0, p1}, Lorg/apache/commons/lang/text/StrTokenizer;-><init>(Ljava/lang/String;)V

    .line 290
    invoke-virtual {p0, p2}, Lorg/apache/commons/lang/text/StrTokenizer;->setDelimiterMatcher(Lorg/apache/commons/lang/text/StrMatcher;)Lorg/apache/commons/lang/text/StrTokenizer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/apache/commons/lang/text/StrMatcher;Lorg/apache/commons/lang/text/StrMatcher;)V
    .locals 0

    .line 315
    invoke-direct {p0, p1, p2}, Lorg/apache/commons/lang/text/StrTokenizer;-><init>(Ljava/lang/String;Lorg/apache/commons/lang/text/StrMatcher;)V

    .line 316
    invoke-virtual {p0, p3}, Lorg/apache/commons/lang/text/StrTokenizer;->setQuoteMatcher(Lorg/apache/commons/lang/text/StrMatcher;)Lorg/apache/commons/lang/text/StrTokenizer;

    return-void
.end method

.method public constructor <init>([C)V
    .locals 1

    .line 329
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 118
    invoke-static {}, Lorg/apache/commons/lang/text/StrMatcher;->splitMatcher()Lorg/apache/commons/lang/text/StrMatcher;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/commons/lang/text/StrTokenizer;->delimMatcher:Lorg/apache/commons/lang/text/StrMatcher;

    .line 120
    invoke-static {}, Lorg/apache/commons/lang/text/StrMatcher;->noneMatcher()Lorg/apache/commons/lang/text/StrMatcher;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/commons/lang/text/StrTokenizer;->quoteMatcher:Lorg/apache/commons/lang/text/StrMatcher;

    .line 122
    invoke-static {}, Lorg/apache/commons/lang/text/StrMatcher;->noneMatcher()Lorg/apache/commons/lang/text/StrMatcher;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/commons/lang/text/StrTokenizer;->ignoredMatcher:Lorg/apache/commons/lang/text/StrMatcher;

    .line 124
    invoke-static {}, Lorg/apache/commons/lang/text/StrMatcher;->noneMatcher()Lorg/apache/commons/lang/text/StrMatcher;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/commons/lang/text/StrTokenizer;->trimmerMatcher:Lorg/apache/commons/lang/text/StrMatcher;

    const/4 v0, 0x0

    .line 127
    iput-boolean v0, p0, Lorg/apache/commons/lang/text/StrTokenizer;->emptyAsNull:Z

    const/4 v0, 0x1

    .line 129
    iput-boolean v0, p0, Lorg/apache/commons/lang/text/StrTokenizer;->ignoreEmptyTokens:Z

    .line 330
    iput-object p1, p0, Lorg/apache/commons/lang/text/StrTokenizer;->chars:[C

    return-void
.end method

.method public constructor <init>([CC)V
    .locals 0

    .line 343
    invoke-direct {p0, p1}, Lorg/apache/commons/lang/text/StrTokenizer;-><init>([C)V

    .line 344
    invoke-virtual {p0, p2}, Lorg/apache/commons/lang/text/StrTokenizer;->setDelimiterChar(C)Lorg/apache/commons/lang/text/StrTokenizer;

    return-void
.end method

.method public constructor <init>([CCC)V
    .locals 0

    .line 387
    invoke-direct {p0, p1, p2}, Lorg/apache/commons/lang/text/StrTokenizer;-><init>([CC)V

    .line 388
    invoke-virtual {p0, p3}, Lorg/apache/commons/lang/text/StrTokenizer;->setQuoteChar(C)Lorg/apache/commons/lang/text/StrTokenizer;

    return-void
.end method

.method public constructor <init>([CLjava/lang/String;)V
    .locals 0

    .line 357
    invoke-direct {p0, p1}, Lorg/apache/commons/lang/text/StrTokenizer;-><init>([C)V

    .line 358
    invoke-virtual {p0, p2}, Lorg/apache/commons/lang/text/StrTokenizer;->setDelimiterString(Ljava/lang/String;)Lorg/apache/commons/lang/text/StrTokenizer;

    return-void
.end method

.method public constructor <init>([CLorg/apache/commons/lang/text/StrMatcher;)V
    .locals 0

    .line 371
    invoke-direct {p0, p1}, Lorg/apache/commons/lang/text/StrTokenizer;-><init>([C)V

    .line 372
    invoke-virtual {p0, p2}, Lorg/apache/commons/lang/text/StrTokenizer;->setDelimiterMatcher(Lorg/apache/commons/lang/text/StrMatcher;)Lorg/apache/commons/lang/text/StrTokenizer;

    return-void
.end method

.method public constructor <init>([CLorg/apache/commons/lang/text/StrMatcher;Lorg/apache/commons/lang/text/StrMatcher;)V
    .locals 0

    .line 403
    invoke-direct {p0, p1, p2}, Lorg/apache/commons/lang/text/StrTokenizer;-><init>([CLorg/apache/commons/lang/text/StrMatcher;)V

    .line 404
    invoke-virtual {p0, p3}, Lorg/apache/commons/lang/text/StrTokenizer;->setQuoteMatcher(Lorg/apache/commons/lang/text/StrMatcher;)Lorg/apache/commons/lang/text/StrTokenizer;

    return-void
.end method

.method private addToken(Ljava/util/List;Ljava/lang/String;)V
    .locals 1

    if-eqz p2, :cond_0

    .line 675
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 676
    :cond_0
    invoke-virtual {p0}, Lorg/apache/commons/lang/text/StrTokenizer;->isIgnoreEmptyTokens()Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    .line 679
    :cond_1
    invoke-virtual {p0}, Lorg/apache/commons/lang/text/StrTokenizer;->isEmptyTokenAsNull()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 p2, 0x0

    .line 683
    :cond_2
    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private checkTokenized()V
    .locals 3

    .line 615
    iget-object v0, p0, Lorg/apache/commons/lang/text/StrTokenizer;->tokens:[Ljava/lang/String;

    if-nez v0, :cond_1

    .line 616
    iget-object v0, p0, Lorg/apache/commons/lang/text/StrTokenizer;->chars:[C

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 618
    invoke-virtual {p0, v0, v1, v1}, Lorg/apache/commons/lang/text/StrTokenizer;->tokenize([CII)Ljava/util/List;

    move-result-object v0

    .line 619
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/commons/lang/text/StrTokenizer;->tokens:[Ljava/lang/String;

    goto :goto_0

    .line 621
    :cond_0
    array-length v2, v0

    invoke-virtual {p0, v0, v1, v2}, Lorg/apache/commons/lang/text/StrTokenizer;->tokenize([CII)Ljava/util/List;

    move-result-object v0

    .line 622
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/commons/lang/text/StrTokenizer;->tokens:[Ljava/lang/String;

    :cond_1
    :goto_0
    return-void
.end method

.method private static getCSVClone()Lorg/apache/commons/lang/text/StrTokenizer;
    .locals 1

    .line 139
    sget-object v0, Lorg/apache/commons/lang/text/StrTokenizer;->CSV_TOKENIZER_PROTOTYPE:Lorg/apache/commons/lang/text/StrTokenizer;

    invoke-virtual {v0}, Lorg/apache/commons/lang/text/StrTokenizer;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/lang/text/StrTokenizer;

    return-object v0
.end method

.method public static getCSVInstance()Lorg/apache/commons/lang/text/StrTokenizer;
    .locals 1

    .line 152
    invoke-static {}, Lorg/apache/commons/lang/text/StrTokenizer;->getCSVClone()Lorg/apache/commons/lang/text/StrTokenizer;

    move-result-object v0

    return-object v0
.end method

.method public static getCSVInstance(Ljava/lang/String;)Lorg/apache/commons/lang/text/StrTokenizer;
    .locals 1

    .line 165
    invoke-static {}, Lorg/apache/commons/lang/text/StrTokenizer;->getCSVClone()Lorg/apache/commons/lang/text/StrTokenizer;

    move-result-object v0

    .line 166
    invoke-virtual {v0, p0}, Lorg/apache/commons/lang/text/StrTokenizer;->reset(Ljava/lang/String;)Lorg/apache/commons/lang/text/StrTokenizer;

    return-object v0
.end method

.method public static getCSVInstance([C)Lorg/apache/commons/lang/text/StrTokenizer;
    .locals 1

    .line 180
    invoke-static {}, Lorg/apache/commons/lang/text/StrTokenizer;->getCSVClone()Lorg/apache/commons/lang/text/StrTokenizer;

    move-result-object v0

    .line 181
    invoke-virtual {v0, p0}, Lorg/apache/commons/lang/text/StrTokenizer;->reset([C)Lorg/apache/commons/lang/text/StrTokenizer;

    return-object v0
.end method

.method private static getTSVClone()Lorg/apache/commons/lang/text/StrTokenizer;
    .locals 1

    .line 191
    sget-object v0, Lorg/apache/commons/lang/text/StrTokenizer;->TSV_TOKENIZER_PROTOTYPE:Lorg/apache/commons/lang/text/StrTokenizer;

    invoke-virtual {v0}, Lorg/apache/commons/lang/text/StrTokenizer;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/lang/text/StrTokenizer;

    return-object v0
.end method

.method public static getTSVInstance()Lorg/apache/commons/lang/text/StrTokenizer;
    .locals 1

    .line 204
    invoke-static {}, Lorg/apache/commons/lang/text/StrTokenizer;->getTSVClone()Lorg/apache/commons/lang/text/StrTokenizer;

    move-result-object v0

    return-object v0
.end method

.method public static getTSVInstance(Ljava/lang/String;)Lorg/apache/commons/lang/text/StrTokenizer;
    .locals 1

    .line 215
    invoke-static {}, Lorg/apache/commons/lang/text/StrTokenizer;->getTSVClone()Lorg/apache/commons/lang/text/StrTokenizer;

    move-result-object v0

    .line 216
    invoke-virtual {v0, p0}, Lorg/apache/commons/lang/text/StrTokenizer;->reset(Ljava/lang/String;)Lorg/apache/commons/lang/text/StrTokenizer;

    return-object v0
.end method

.method public static getTSVInstance([C)Lorg/apache/commons/lang/text/StrTokenizer;
    .locals 1

    .line 228
    invoke-static {}, Lorg/apache/commons/lang/text/StrTokenizer;->getTSVClone()Lorg/apache/commons/lang/text/StrTokenizer;

    move-result-object v0

    .line 229
    invoke-virtual {v0, p0}, Lorg/apache/commons/lang/text/StrTokenizer;->reset([C)Lorg/apache/commons/lang/text/StrTokenizer;

    return-object v0
.end method

.method private isQuote([CIIII)Z
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p5, :cond_2

    add-int v2, p2, v1

    if-ge v2, p3, :cond_1

    .line 848
    aget-char v2, p1, v2

    add-int v3, p4, v1

    aget-char v3, p1, v3

    if-eq v2, v3, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    return v0

    :cond_2
    const/4 p1, 0x1

    return p1
.end method

.method private readNextToken([CIILorg/apache/commons/lang/text/StrBuilder;Ljava/util/List;)I
    .locals 8

    :goto_0
    if-ge p2, p3, :cond_1

    .line 701
    invoke-virtual {p0}, Lorg/apache/commons/lang/text/StrTokenizer;->getIgnoredMatcher()Lorg/apache/commons/lang/text/StrMatcher;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p2, p3}, Lorg/apache/commons/lang/text/StrMatcher;->isMatch([CIII)I

    move-result v0

    invoke-virtual {p0}, Lorg/apache/commons/lang/text/StrTokenizer;->getTrimmerMatcher()Lorg/apache/commons/lang/text/StrMatcher;

    move-result-object v1

    invoke-virtual {v1, p1, p2, p2, p3}, Lorg/apache/commons/lang/text/StrMatcher;->isMatch([CIII)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    if-eqz v0, :cond_1

    .line 704
    invoke-virtual {p0}, Lorg/apache/commons/lang/text/StrTokenizer;->getDelimiterMatcher()Lorg/apache/commons/lang/text/StrMatcher;

    move-result-object v1

    invoke-virtual {v1, p1, p2, p2, p3}, Lorg/apache/commons/lang/text/StrMatcher;->isMatch([CIII)I

    move-result v1

    if-gtz v1, :cond_1

    invoke-virtual {p0}, Lorg/apache/commons/lang/text/StrTokenizer;->getQuoteMatcher()Lorg/apache/commons/lang/text/StrMatcher;

    move-result-object v1

    invoke-virtual {v1, p1, p2, p2, p3}, Lorg/apache/commons/lang/text/StrMatcher;->isMatch([CIII)I

    move-result v1

    if-lez v1, :cond_0

    goto :goto_1

    :cond_0
    add-int/2addr p2, v0

    goto :goto_0

    :cond_1
    :goto_1
    if-lt p2, p3, :cond_2

    const-string p1, ""

    .line 714
    invoke-direct {p0, p5, p1}, Lorg/apache/commons/lang/text/StrTokenizer;->addToken(Ljava/util/List;Ljava/lang/String;)V

    const/4 p1, -0x1

    return p1

    .line 719
    :cond_2
    invoke-virtual {p0}, Lorg/apache/commons/lang/text/StrTokenizer;->getDelimiterMatcher()Lorg/apache/commons/lang/text/StrMatcher;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p2, p3}, Lorg/apache/commons/lang/text/StrMatcher;->isMatch([CIII)I

    move-result v0

    if-lez v0, :cond_3

    const-string p1, ""

    .line 721
    invoke-direct {p0, p5, p1}, Lorg/apache/commons/lang/text/StrTokenizer;->addToken(Ljava/util/List;Ljava/lang/String;)V

    add-int/2addr p2, v0

    return p2

    .line 726
    :cond_3
    invoke-virtual {p0}, Lorg/apache/commons/lang/text/StrTokenizer;->getQuoteMatcher()Lorg/apache/commons/lang/text/StrMatcher;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p2, p3}, Lorg/apache/commons/lang/text/StrMatcher;->isMatch([CIII)I

    move-result v7

    if-lez v7, :cond_4

    add-int v2, p2, v7

    move-object v0, p0

    move-object v1, p1

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p2

    .line 728
    invoke-direct/range {v0 .. v7}, Lorg/apache/commons/lang/text/StrTokenizer;->readWithQuotes([CIILorg/apache/commons/lang/text/StrBuilder;Ljava/util/List;II)I

    move-result p1

    return p1

    :cond_4
    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 730
    invoke-direct/range {v0 .. v7}, Lorg/apache/commons/lang/text/StrTokenizer;->readWithQuotes([CIILorg/apache/commons/lang/text/StrBuilder;Ljava/util/List;II)I

    move-result p1

    return p1
.end method

.method private readWithQuotes([CIILorg/apache/commons/lang/text/StrBuilder;Ljava/util/List;II)I
    .locals 18

    move-object/from16 v6, p0

    move-object/from16 v7, p1

    move/from16 v8, p2

    move/from16 v9, p3

    move-object/from16 v10, p4

    move-object/from16 v11, p5

    move/from16 v12, p7

    .line 752
    invoke-virtual/range {p4 .. p4}, Lorg/apache/commons/lang/text/StrBuilder;->clear()Lorg/apache/commons/lang/text/StrBuilder;

    const/4 v14, 0x0

    if-lez v12, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    move/from16 v16, v0

    move v15, v8

    const/4 v5, 0x0

    :goto_1
    if-ge v15, v9, :cond_8

    if-eqz v16, :cond_3

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move v2, v15

    move/from16 v3, p3

    move/from16 v4, p6

    move v13, v5

    move/from16 v5, p7

    .line 768
    invoke-direct/range {v0 .. v5}, Lorg/apache/commons/lang/text/StrTokenizer;->isQuote([CIIII)Z

    move-result v0

    if-eqz v0, :cond_2

    add-int v17, v15, v12

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v17

    move/from16 v3, p3

    move/from16 v4, p6

    move/from16 v5, p7

    .line 769
    invoke-direct/range {v0 .. v5}, Lorg/apache/commons/lang/text/StrTokenizer;->isQuote([CIIII)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 771
    invoke-virtual {v10, v7, v15, v12}, Lorg/apache/commons/lang/text/StrBuilder;->append([CII)Lorg/apache/commons/lang/text/StrBuilder;

    mul-int/lit8 v0, v12, 0x2

    add-int/2addr v15, v0

    .line 773
    invoke-virtual/range {p4 .. p4}, Lorg/apache/commons/lang/text/StrBuilder;->size()I

    move-result v5

    goto :goto_1

    :cond_1
    move v5, v13

    move/from16 v15, v17

    const/16 v16, 0x0

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v15, 0x1

    .line 784
    aget-char v1, v7, v15

    invoke-virtual {v10, v1}, Lorg/apache/commons/lang/text/StrBuilder;->append(C)Lorg/apache/commons/lang/text/StrBuilder;

    .line 785
    invoke-virtual/range {p4 .. p4}, Lorg/apache/commons/lang/text/StrBuilder;->size()I

    move-result v5

    move v15, v0

    goto :goto_1

    :cond_3
    move v13, v5

    .line 791
    invoke-virtual/range {p0 .. p0}, Lorg/apache/commons/lang/text/StrTokenizer;->getDelimiterMatcher()Lorg/apache/commons/lang/text/StrMatcher;

    move-result-object v0

    invoke-virtual {v0, v7, v15, v8, v9}, Lorg/apache/commons/lang/text/StrMatcher;->isMatch([CIII)I

    move-result v0

    if-lez v0, :cond_4

    .line 794
    invoke-virtual {v10, v14, v13}, Lorg/apache/commons/lang/text/StrBuilder;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v6, v11, v1}, Lorg/apache/commons/lang/text/StrTokenizer;->addToken(Ljava/util/List;Ljava/lang/String;)V

    add-int/2addr v15, v0

    return v15

    :cond_4
    if-lez v12, :cond_5

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move v2, v15

    move/from16 v3, p3

    move/from16 v4, p6

    move/from16 v5, p7

    .line 800
    invoke-direct/range {v0 .. v5}, Lorg/apache/commons/lang/text/StrTokenizer;->isQuote([CIIII)Z

    move-result v0

    if-eqz v0, :cond_5

    add-int/2addr v15, v12

    move v5, v13

    const/16 v16, 0x1

    goto :goto_1

    .line 808
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lorg/apache/commons/lang/text/StrTokenizer;->getIgnoredMatcher()Lorg/apache/commons/lang/text/StrMatcher;

    move-result-object v0

    invoke-virtual {v0, v7, v15, v8, v9}, Lorg/apache/commons/lang/text/StrMatcher;->isMatch([CIII)I

    move-result v0

    if-lez v0, :cond_6

    add-int/2addr v15, v0

    move v5, v13

    goto/16 :goto_1

    .line 817
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lorg/apache/commons/lang/text/StrTokenizer;->getTrimmerMatcher()Lorg/apache/commons/lang/text/StrMatcher;

    move-result-object v0

    invoke-virtual {v0, v7, v15, v8, v9}, Lorg/apache/commons/lang/text/StrMatcher;->isMatch([CIII)I

    move-result v0

    if-lez v0, :cond_7

    .line 819
    invoke-virtual {v10, v7, v15, v0}, Lorg/apache/commons/lang/text/StrBuilder;->append([CII)Lorg/apache/commons/lang/text/StrBuilder;

    add-int/2addr v15, v0

    move v5, v13

    goto/16 :goto_1

    :cond_7
    add-int/lit8 v0, v15, 0x1

    .line 825
    aget-char v1, v7, v15

    invoke-virtual {v10, v1}, Lorg/apache/commons/lang/text/StrBuilder;->append(C)Lorg/apache/commons/lang/text/StrBuilder;

    .line 826
    invoke-virtual/range {p4 .. p4}, Lorg/apache/commons/lang/text/StrBuilder;->size()I

    move-result v5

    move v15, v0

    goto/16 :goto_1

    :cond_8
    move v13, v5

    .line 831
    invoke-virtual {v10, v14, v13}, Lorg/apache/commons/lang/text/StrBuilder;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v6, v11, v0}, Lorg/apache/commons/lang/text/StrTokenizer;->addToken(Ljava/util/List;Ljava/lang/String;)V

    const/4 v0, -0x1

    return v0
.end method


# virtual methods
.method public add(Ljava/lang/Object;)V
    .locals 1

    .line 606
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "add() is unsupported"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public clone()Ljava/lang/Object;
    .locals 1

    .line 1091
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/commons/lang/text/StrTokenizer;->cloneReset()Ljava/lang/Object;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 v0, 0x0

    return-object v0
.end method

.method cloneReset()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .line 1106
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/lang/text/StrTokenizer;

    .line 1107
    iget-object v1, v0, Lorg/apache/commons/lang/text/StrTokenizer;->chars:[C

    if-eqz v1, :cond_0

    .line 1108
    invoke-virtual {v1}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [C

    iput-object v1, v0, Lorg/apache/commons/lang/text/StrTokenizer;->chars:[C

    .line 1110
    :cond_0
    invoke-virtual {v0}, Lorg/apache/commons/lang/text/StrTokenizer;->reset()Lorg/apache/commons/lang/text/StrTokenizer;

    return-object v0
.end method

.method public getContent()Ljava/lang/String;
    .locals 2

    .line 1075
    iget-object v0, p0, Lorg/apache/commons/lang/text/StrTokenizer;->chars:[C

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 1078
    :cond_0
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    return-object v1
.end method

.method public getDelimiterMatcher()Lorg/apache/commons/lang/text/StrMatcher;
    .locals 1

    .line 863
    iget-object v0, p0, Lorg/apache/commons/lang/text/StrTokenizer;->delimMatcher:Lorg/apache/commons/lang/text/StrMatcher;

    return-object v0
.end method

.method public getIgnoredMatcher()Lorg/apache/commons/lang/text/StrMatcher;
    .locals 1

    .line 959
    iget-object v0, p0, Lorg/apache/commons/lang/text/StrTokenizer;->ignoredMatcher:Lorg/apache/commons/lang/text/StrMatcher;

    return-object v0
.end method

.method public getQuoteMatcher()Lorg/apache/commons/lang/text/StrMatcher;
    .locals 1

    .line 915
    iget-object v0, p0, Lorg/apache/commons/lang/text/StrTokenizer;->quoteMatcher:Lorg/apache/commons/lang/text/StrMatcher;

    return-object v0
.end method

.method public getTokenArray()[Ljava/lang/String;
    .locals 1

    .line 451
    invoke-direct {p0}, Lorg/apache/commons/lang/text/StrTokenizer;->checkTokenized()V

    .line 452
    iget-object v0, p0, Lorg/apache/commons/lang/text/StrTokenizer;->tokens:[Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method public getTokenList()Ljava/util/List;
    .locals 4

    .line 461
    invoke-direct {p0}, Lorg/apache/commons/lang/text/StrTokenizer;->checkTokenized()V

    .line 462
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lorg/apache/commons/lang/text/StrTokenizer;->tokens:[Ljava/lang/String;

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v1, 0x0

    .line 463
    :goto_0
    iget-object v2, p0, Lorg/apache/commons/lang/text/StrTokenizer;->tokens:[Ljava/lang/String;

    array-length v3, v2

    if-ge v1, v3, :cond_0

    .line 464
    aget-object v2, v2, v1

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public getTrimmerMatcher()Lorg/apache/commons/lang/text/StrMatcher;
    .locals 1

    .line 1003
    iget-object v0, p0, Lorg/apache/commons/lang/text/StrTokenizer;->trimmerMatcher:Lorg/apache/commons/lang/text/StrMatcher;

    return-object v0
.end method

.method public hasNext()Z
    .locals 2

    .line 525
    invoke-direct {p0}, Lorg/apache/commons/lang/text/StrTokenizer;->checkTokenized()V

    .line 526
    iget v0, p0, Lorg/apache/commons/lang/text/StrTokenizer;->tokenPos:I

    iget-object v1, p0, Lorg/apache/commons/lang/text/StrTokenizer;->tokens:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasPrevious()Z
    .locals 1

    .line 557
    invoke-direct {p0}, Lorg/apache/commons/lang/text/StrTokenizer;->checkTokenized()V

    .line 558
    iget v0, p0, Lorg/apache/commons/lang/text/StrTokenizer;->tokenPos:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isEmptyTokenAsNull()Z
    .locals 1

    .line 1030
    iget-boolean v0, p0, Lorg/apache/commons/lang/text/StrTokenizer;->emptyAsNull:Z

    return v0
.end method

.method public isIgnoreEmptyTokens()Z
    .locals 1

    .line 1053
    iget-boolean v0, p0, Lorg/apache/commons/lang/text/StrTokenizer;->ignoreEmptyTokens:Z

    return v0
.end method

.method public next()Ljava/lang/Object;
    .locals 3

    .line 536
    invoke-virtual {p0}, Lorg/apache/commons/lang/text/StrTokenizer;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 537
    iget-object v0, p0, Lorg/apache/commons/lang/text/StrTokenizer;->tokens:[Ljava/lang/String;

    iget v1, p0, Lorg/apache/commons/lang/text/StrTokenizer;->tokenPos:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/commons/lang/text/StrTokenizer;->tokenPos:I

    aget-object v0, v0, v1

    return-object v0

    .line 539
    :cond_0
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0
.end method

.method public nextIndex()I
    .locals 1

    .line 548
    iget v0, p0, Lorg/apache/commons/lang/text/StrTokenizer;->tokenPos:I

    return v0
.end method

.method public nextToken()Ljava/lang/String;
    .locals 3

    .line 427
    invoke-virtual {p0}, Lorg/apache/commons/lang/text/StrTokenizer;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 428
    iget-object v0, p0, Lorg/apache/commons/lang/text/StrTokenizer;->tokens:[Ljava/lang/String;

    iget v1, p0, Lorg/apache/commons/lang/text/StrTokenizer;->tokenPos:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/commons/lang/text/StrTokenizer;->tokenPos:I

    aget-object v0, v0, v1

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public previous()Ljava/lang/Object;
    .locals 2

    .line 567
    invoke-virtual {p0}, Lorg/apache/commons/lang/text/StrTokenizer;->hasPrevious()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 568
    iget-object v0, p0, Lorg/apache/commons/lang/text/StrTokenizer;->tokens:[Ljava/lang/String;

    iget v1, p0, Lorg/apache/commons/lang/text/StrTokenizer;->tokenPos:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/commons/lang/text/StrTokenizer;->tokenPos:I

    aget-object v0, v0, v1

    return-object v0

    .line 570
    :cond_0
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0
.end method

.method public previousIndex()I
    .locals 1

    .line 579
    iget v0, p0, Lorg/apache/commons/lang/text/StrTokenizer;->tokenPos:I

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public previousToken()Ljava/lang/String;
    .locals 2

    .line 439
    invoke-virtual {p0}, Lorg/apache/commons/lang/text/StrTokenizer;->hasPrevious()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 440
    iget-object v0, p0, Lorg/apache/commons/lang/text/StrTokenizer;->tokens:[Ljava/lang/String;

    iget v1, p0, Lorg/apache/commons/lang/text/StrTokenizer;->tokenPos:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/commons/lang/text/StrTokenizer;->tokenPos:I

    aget-object v0, v0, v1

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public remove()V
    .locals 2

    .line 588
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "remove() is unsupported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public reset()Lorg/apache/commons/lang/text/StrTokenizer;
    .locals 1

    const/4 v0, 0x0

    .line 477
    iput v0, p0, Lorg/apache/commons/lang/text/StrTokenizer;->tokenPos:I

    const/4 v0, 0x0

    .line 478
    iput-object v0, p0, Lorg/apache/commons/lang/text/StrTokenizer;->tokens:[Ljava/lang/String;

    return-object p0
.end method

.method public reset(Ljava/lang/String;)Lorg/apache/commons/lang/text/StrTokenizer;
    .locals 0

    .line 491
    invoke-virtual {p0}, Lorg/apache/commons/lang/text/StrTokenizer;->reset()Lorg/apache/commons/lang/text/StrTokenizer;

    if-eqz p1, :cond_0

    .line 493
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object p1

    iput-object p1, p0, Lorg/apache/commons/lang/text/StrTokenizer;->chars:[C

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 495
    iput-object p1, p0, Lorg/apache/commons/lang/text/StrTokenizer;->chars:[C

    :goto_0
    return-object p0
.end method

.method public reset([C)Lorg/apache/commons/lang/text/StrTokenizer;
    .locals 0

    .line 512
    invoke-virtual {p0}, Lorg/apache/commons/lang/text/StrTokenizer;->reset()Lorg/apache/commons/lang/text/StrTokenizer;

    .line 513
    iput-object p1, p0, Lorg/apache/commons/lang/text/StrTokenizer;->chars:[C

    return-object p0
.end method

.method public set(Ljava/lang/Object;)V
    .locals 1

    .line 597
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "set() is unsupported"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setDelimiterChar(C)Lorg/apache/commons/lang/text/StrTokenizer;
    .locals 0

    .line 890
    invoke-static {p1}, Lorg/apache/commons/lang/text/StrMatcher;->charMatcher(C)Lorg/apache/commons/lang/text/StrMatcher;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/apache/commons/lang/text/StrTokenizer;->setDelimiterMatcher(Lorg/apache/commons/lang/text/StrMatcher;)Lorg/apache/commons/lang/text/StrTokenizer;

    move-result-object p1

    return-object p1
.end method

.method public setDelimiterMatcher(Lorg/apache/commons/lang/text/StrMatcher;)Lorg/apache/commons/lang/text/StrTokenizer;
    .locals 0

    if-nez p1, :cond_0

    .line 876
    invoke-static {}, Lorg/apache/commons/lang/text/StrMatcher;->noneMatcher()Lorg/apache/commons/lang/text/StrMatcher;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/commons/lang/text/StrTokenizer;->delimMatcher:Lorg/apache/commons/lang/text/StrMatcher;

    goto :goto_0

    .line 878
    :cond_0
    iput-object p1, p0, Lorg/apache/commons/lang/text/StrTokenizer;->delimMatcher:Lorg/apache/commons/lang/text/StrMatcher;

    :goto_0
    return-object p0
.end method

.method public setDelimiterString(Ljava/lang/String;)Lorg/apache/commons/lang/text/StrTokenizer;
    .locals 0

    .line 900
    invoke-static {p1}, Lorg/apache/commons/lang/text/StrMatcher;->stringMatcher(Ljava/lang/String;)Lorg/apache/commons/lang/text/StrMatcher;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/apache/commons/lang/text/StrTokenizer;->setDelimiterMatcher(Lorg/apache/commons/lang/text/StrMatcher;)Lorg/apache/commons/lang/text/StrTokenizer;

    move-result-object p1

    return-object p1
.end method

.method public setEmptyTokenAsNull(Z)Lorg/apache/commons/lang/text/StrTokenizer;
    .locals 0

    .line 1041
    iput-boolean p1, p0, Lorg/apache/commons/lang/text/StrTokenizer;->emptyAsNull:Z

    return-object p0
.end method

.method public setIgnoreEmptyTokens(Z)Lorg/apache/commons/lang/text/StrTokenizer;
    .locals 0

    .line 1064
    iput-boolean p1, p0, Lorg/apache/commons/lang/text/StrTokenizer;->ignoreEmptyTokens:Z

    return-object p0
.end method

.method public setIgnoredChar(C)Lorg/apache/commons/lang/text/StrTokenizer;
    .locals 0

    .line 988
    invoke-static {p1}, Lorg/apache/commons/lang/text/StrMatcher;->charMatcher(C)Lorg/apache/commons/lang/text/StrMatcher;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/apache/commons/lang/text/StrTokenizer;->setIgnoredMatcher(Lorg/apache/commons/lang/text/StrMatcher;)Lorg/apache/commons/lang/text/StrTokenizer;

    move-result-object p1

    return-object p1
.end method

.method public setIgnoredMatcher(Lorg/apache/commons/lang/text/StrMatcher;)Lorg/apache/commons/lang/text/StrTokenizer;
    .locals 0

    if-eqz p1, :cond_0

    .line 973
    iput-object p1, p0, Lorg/apache/commons/lang/text/StrTokenizer;->ignoredMatcher:Lorg/apache/commons/lang/text/StrMatcher;

    :cond_0
    return-object p0
.end method

.method public setQuoteChar(C)Lorg/apache/commons/lang/text/StrTokenizer;
    .locals 0

    .line 944
    invoke-static {p1}, Lorg/apache/commons/lang/text/StrMatcher;->charMatcher(C)Lorg/apache/commons/lang/text/StrMatcher;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/apache/commons/lang/text/StrTokenizer;->setQuoteMatcher(Lorg/apache/commons/lang/text/StrMatcher;)Lorg/apache/commons/lang/text/StrTokenizer;

    move-result-object p1

    return-object p1
.end method

.method public setQuoteMatcher(Lorg/apache/commons/lang/text/StrMatcher;)Lorg/apache/commons/lang/text/StrTokenizer;
    .locals 0

    if-eqz p1, :cond_0

    .line 929
    iput-object p1, p0, Lorg/apache/commons/lang/text/StrTokenizer;->quoteMatcher:Lorg/apache/commons/lang/text/StrMatcher;

    :cond_0
    return-object p0
.end method

.method public setTrimmerMatcher(Lorg/apache/commons/lang/text/StrMatcher;)Lorg/apache/commons/lang/text/StrTokenizer;
    .locals 0

    if-eqz p1, :cond_0

    .line 1017
    iput-object p1, p0, Lorg/apache/commons/lang/text/StrTokenizer;->trimmerMatcher:Lorg/apache/commons/lang/text/StrMatcher;

    :cond_0
    return-object p0
.end method

.method public size()I
    .locals 1

    .line 415
    invoke-direct {p0}, Lorg/apache/commons/lang/text/StrTokenizer;->checkTokenized()V

    .line 416
    iget-object v0, p0, Lorg/apache/commons/lang/text/StrTokenizer;->tokens:[Ljava/lang/String;

    array-length v0, v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1121
    iget-object v0, p0, Lorg/apache/commons/lang/text/StrTokenizer;->tokens:[Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "StrTokenizer[not tokenized yet]"

    return-object v0

    .line 1124
    :cond_0
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "StrTokenizer"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {p0}, Lorg/apache/commons/lang/text/StrTokenizer;->getTokenList()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected tokenize([CII)Ljava/util/List;
    .locals 8

    if-eqz p1, :cond_3

    if-nez p3, :cond_0

    goto :goto_1

    .line 651
    :cond_0
    new-instance v6, Lorg/apache/commons/lang/text/StrBuilder;

    invoke-direct {v6}, Lorg/apache/commons/lang/text/StrBuilder;-><init>()V

    .line 652
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    move v2, p2

    :cond_1
    :goto_0
    if-ltz v2, :cond_2

    if-ge v2, p3, :cond_2

    move-object v0, p0

    move-object v1, p1

    move v3, p3

    move-object v4, v6

    move-object v5, v7

    .line 658
    invoke-direct/range {v0 .. v5}, Lorg/apache/commons/lang/text/StrTokenizer;->readNextToken([CIILorg/apache/commons/lang/text/StrBuilder;Ljava/util/List;)I

    move-result v2

    if-lt v2, p3, :cond_1

    const-string p2, ""

    .line 662
    invoke-direct {p0, v7, p2}, Lorg/apache/commons/lang/text/StrTokenizer;->addToken(Ljava/util/List;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    return-object v7

    .line 649
    :cond_3
    :goto_1
    sget-object p1, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    return-object p1
.end method

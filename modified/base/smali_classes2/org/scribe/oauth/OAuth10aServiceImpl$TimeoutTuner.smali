.class Lorg/scribe/oauth/OAuth10aServiceImpl$TimeoutTuner;
.super Lorg/scribe/model/RequestTuner;
.source "OAuth10aServiceImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/scribe/oauth/OAuth10aServiceImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TimeoutTuner"
.end annotation


# instance fields
.field private final duration:I

.field private final unit:Ljava/util/concurrent/TimeUnit;


# direct methods
.method public constructor <init>(ILjava/util/concurrent/TimeUnit;)V
    .locals 0

    .line 185
    invoke-direct {p0}, Lorg/scribe/model/RequestTuner;-><init>()V

    .line 186
    iput p1, p0, Lorg/scribe/oauth/OAuth10aServiceImpl$TimeoutTuner;->duration:I

    .line 187
    iput-object p2, p0, Lorg/scribe/oauth/OAuth10aServiceImpl$TimeoutTuner;->unit:Ljava/util/concurrent/TimeUnit;

    return-void
.end method


# virtual methods
.method public tune(Lorg/scribe/model/Request;)V
    .locals 2

    .line 193
    iget v0, p0, Lorg/scribe/oauth/OAuth10aServiceImpl$TimeoutTuner;->duration:I

    iget-object v1, p0, Lorg/scribe/oauth/OAuth10aServiceImpl$TimeoutTuner;->unit:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v0, v1}, Lorg/scribe/model/Request;->setReadTimeout(ILjava/util/concurrent/TimeUnit;)V

    return-void
.end method

.class public Lorg/scribe/builder/api/EvernoteApi$Sandbox;
.super Lorg/scribe/builder/api/EvernoteApi;
.source "EvernoteApi.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/scribe/builder/api/EvernoteApi;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Sandbox"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 37
    invoke-direct {p0}, Lorg/scribe/builder/api/EvernoteApi;-><init>()V

    return-void
.end method


# virtual methods
.method protected serviceUrl()Ljava/lang/String;
    .locals 1

    const-string v0, "https://sandbox.evernote.com"

    return-object v0
.end method

.class public Lorg/scribe/builder/api/FreelancerApi$Sandbox;
.super Lorg/scribe/builder/api/FreelancerApi;
.source "FreelancerApi.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/scribe/builder/api/FreelancerApi;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Sandbox"
.end annotation


# static fields
.field private static final SANDBOX_AUTHORIZATION_URL:Ljava/lang/String; = "http://www.sandbox.freelancer.com/users/api-token/auth.php"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 39
    invoke-direct {p0}, Lorg/scribe/builder/api/FreelancerApi;-><init>()V

    return-void
.end method


# virtual methods
.method public getAccessTokenEndpoint()Ljava/lang/String;
    .locals 1

    const-string v0, "http://api.sandbox.freelancer.com/RequestAccessToken/requestAccessToken.xml?"

    return-object v0
.end method

.method public getAuthorizationUrl(Lorg/scribe/model/Token;)Ljava/lang/String;
    .locals 3

    const-string v0, "http://www.sandbox.freelancer.com/users/api-token/auth.php?oauth_token=%s"

    const/4 v1, 0x1

    .line 58
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Lorg/scribe/model/Token;->getToken()Ljava/lang/String;

    move-result-object p1

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getRequestTokenEndpoint()Ljava/lang/String;
    .locals 1

    const-string v0, "http://api.sandbox.freelancer.com/RequestRequestToken/requestRequestToken.xml"

    return-object v0
.end method

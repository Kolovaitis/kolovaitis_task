.class public Lorg/scribe/builder/api/MeetupApi;
.super Lorg/scribe/builder/api/DefaultApi10a;
.source "MeetupApi.java"


# static fields
.field private static final AUTHORIZE_URL:Ljava/lang/String; = "http://www.meetup.com/authenticate?oauth_token=%s"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Lorg/scribe/builder/api/DefaultApi10a;-><init>()V

    return-void
.end method


# virtual methods
.method public getAccessTokenEndpoint()Ljava/lang/String;
    .locals 1

    const-string v0, "http://api.meetup.com/oauth/access/"

    return-object v0
.end method

.method public getAuthorizationUrl(Lorg/scribe/model/Token;)Ljava/lang/String;
    .locals 3

    const-string v0, "http://www.meetup.com/authenticate?oauth_token=%s"

    const/4 v1, 0x1

    .line 28
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Lorg/scribe/model/Token;->getToken()Ljava/lang/String;

    move-result-object p1

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getRequestTokenEndpoint()Ljava/lang/String;
    .locals 1

    const-string v0, "http://api.meetup.com/oauth/request/"

    return-object v0
.end method

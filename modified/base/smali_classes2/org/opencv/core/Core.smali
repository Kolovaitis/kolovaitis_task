.class public Lorg/opencv/core/Core;
.super Ljava/lang/Object;
.source "Core.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/opencv/core/Core$MinMaxLocResult;
    }
.end annotation


# static fields
.field public static final BORDER_CONSTANT:I = 0x0

.field public static final BORDER_DEFAULT:I = 0x4

.field public static final BORDER_ISOLATED:I = 0x10

.field public static final BORDER_REFLECT:I = 0x2

.field public static final BORDER_REFLECT101:I = 0x4

.field public static final BORDER_REFLECT_101:I = 0x4

.field public static final BORDER_REPLICATE:I = 0x1

.field public static final BORDER_TRANSPARENT:I = 0x5

.field public static final BORDER_WRAP:I = 0x3

.field public static final BadAlign:I = -0x15

.field public static final BadAlphaChannel:I = -0x12

.field public static final BadCOI:I = -0x18

.field public static final BadCallBack:I = -0x16

.field public static final BadDataPtr:I = -0xc

.field public static final BadDepth:I = -0x11

.field public static final BadImageSize:I = -0xa

.field public static final BadModelOrChSeq:I = -0xe

.field public static final BadNumChannel1U:I = -0x10

.field public static final BadNumChannels:I = -0xf

.field public static final BadOffset:I = -0xb

.field public static final BadOrder:I = -0x13

.field public static final BadOrigin:I = -0x14

.field public static final BadROISize:I = -0x19

.field public static final BadStep:I = -0xd

.field public static final BadTileSize:I = -0x17

.field public static final CMP_EQ:I = 0x0

.field public static final CMP_GE:I = 0x2

.field public static final CMP_GT:I = 0x1

.field public static final CMP_LE:I = 0x4

.field public static final CMP_LT:I = 0x3

.field public static final CMP_NE:I = 0x5

.field public static final COVAR_COLS:I = 0x10

.field public static final COVAR_NORMAL:I = 0x1

.field public static final COVAR_ROWS:I = 0x8

.field public static final COVAR_SCALE:I = 0x4

.field public static final COVAR_SCRAMBLED:I = 0x0

.field public static final COVAR_USE_AVG:I = 0x2

.field private static final CV_16S:I = 0x3

.field private static final CV_16U:I = 0x2

.field private static final CV_32F:I = 0x5

.field private static final CV_32S:I = 0x4

.field private static final CV_64F:I = 0x6

.field private static final CV_8S:I = 0x1

.field private static final CV_8U:I = 0x0

.field private static final CV_USRTYPE1:I = 0x7

.field public static final DCT_INVERSE:I = 0x1

.field public static final DCT_ROWS:I = 0x4

.field public static final DECOMP_CHOLESKY:I = 0x3

.field public static final DECOMP_EIG:I = 0x2

.field public static final DECOMP_LU:I = 0x0

.field public static final DECOMP_NORMAL:I = 0x10

.field public static final DECOMP_QR:I = 0x4

.field public static final DECOMP_SVD:I = 0x1

.field public static final DFT_COMPLEX_OUTPUT:I = 0x10

.field public static final DFT_INVERSE:I = 0x1

.field public static final DFT_REAL_OUTPUT:I = 0x20

.field public static final DFT_ROWS:I = 0x4

.field public static final DFT_SCALE:I = 0x2

.field public static final FILLED:I = -0x1

.field public static final FONT_HERSHEY_COMPLEX:I = 0x3

.field public static final FONT_HERSHEY_COMPLEX_SMALL:I = 0x5

.field public static final FONT_HERSHEY_DUPLEX:I = 0x2

.field public static final FONT_HERSHEY_PLAIN:I = 0x1

.field public static final FONT_HERSHEY_SCRIPT_COMPLEX:I = 0x7

.field public static final FONT_HERSHEY_SCRIPT_SIMPLEX:I = 0x6

.field public static final FONT_HERSHEY_SIMPLEX:I = 0x0

.field public static final FONT_HERSHEY_TRIPLEX:I = 0x4

.field public static final FONT_ITALIC:I = 0x10

.field public static final GEMM_1_T:I = 0x1

.field public static final GEMM_2_T:I = 0x2

.field public static final GEMM_3_T:I = 0x4

.field public static final GpuApiCallError:I = -0xd9

.field public static final GpuNotSupported:I = -0xd8

.field public static final HeaderIsNull:I = -0x9

.field public static final KMEANS_PP_CENTERS:I = 0x2

.field public static final KMEANS_RANDOM_CENTERS:I = 0x0

.field public static final KMEANS_USE_INITIAL_LABELS:I = 0x1

.field public static final LINE_4:I = 0x4

.field public static final LINE_8:I = 0x8

.field public static final LINE_AA:I = 0x10

.field public static final MaskIsTiled:I = -0x1a

.field public static final NATIVE_LIBRARY_NAME:Ljava/lang/String;

.field public static final NORM_HAMMING:I = 0x6

.field public static final NORM_HAMMING2:I = 0x7

.field public static final NORM_INF:I = 0x1

.field public static final NORM_L1:I = 0x2

.field public static final NORM_L2:I = 0x4

.field public static final NORM_L2SQR:I = 0x5

.field public static final NORM_MINMAX:I = 0x20

.field public static final NORM_RELATIVE:I = 0x8

.field public static final NORM_TYPE_MASK:I = 0x7

.field public static final OpenCLApiCallError:I = -0xdc

.field public static final OpenCLDoubleNotSupported:I = -0xdd

.field public static final OpenCLInitError:I = -0xde

.field public static final OpenCLNoAMDBlasFft:I = -0xdf

.field public static final OpenGlApiCallError:I = -0xdb

.field public static final OpenGlNotSupported:I = -0xda

.field public static final REDUCE_AVG:I = 0x1

.field public static final REDUCE_MAX:I = 0x2

.field public static final REDUCE_MIN:I = 0x3

.field public static final REDUCE_SUM:I = 0x0

.field public static final SORT_ASCENDING:I = 0x0

.field public static final SORT_DESCENDING:I = 0x10

.field public static final SORT_EVERY_COLUMN:I = 0x1

.field public static final SORT_EVERY_ROW:I = 0x0

.field public static final SVD_FULL_UV:I = 0x4

.field public static final SVD_MODIFY_A:I = 0x1

.field public static final SVD_NO_UV:I = 0x2

.field public static final StsAssert:I = -0xd7

.field public static final StsAutoTrace:I = -0x8

.field public static final StsBackTrace:I = -0x1

.field public static final StsBadArg:I = -0x5

.field public static final StsBadFlag:I = -0xce

.field public static final StsBadFunc:I = -0x6

.field public static final StsBadMask:I = -0xd0

.field public static final StsBadMemBlock:I = -0xd6

.field public static final StsBadPoint:I = -0xcf

.field public static final StsBadSize:I = -0xc9

.field public static final StsDivByZero:I = -0xca

.field public static final StsError:I = -0x2

.field public static final StsFilterOffsetErr:I = -0x1f

.field public static final StsFilterStructContentErr:I = -0x1d

.field public static final StsInplaceNotSupported:I = -0xcb

.field public static final StsInternal:I = -0x3

.field public static final StsKernelStructContentErr:I = -0x1e

.field public static final StsNoConv:I = -0x7

.field public static final StsNoMem:I = -0x4

.field public static final StsNotImplemented:I = -0xd5

.field public static final StsNullPtr:I = -0x1b

.field public static final StsObjectNotFound:I = -0xcc

.field public static final StsOk:I = 0x0

.field public static final StsOutOfRange:I = -0xd3

.field public static final StsParseError:I = -0xd4

.field public static final StsUnmatchedFormats:I = -0xcd

.field public static final StsUnmatchedSizes:I = -0xd1

.field public static final StsUnsupportedFormat:I = -0xd2

.field public static final StsVecLengthErr:I = -0x1c

.field public static final VERSION:Ljava/lang/String;

.field public static final VERSION_MAJOR:I

.field public static final VERSION_MINOR:I

.field public static final VERSION_REVISION:I

.field public static final VERSION_STATUS:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 27
    invoke-static {}, Lorg/opencv/core/Core;->getVersion()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/opencv/core/Core;->VERSION:Ljava/lang/String;

    .line 28
    invoke-static {}, Lorg/opencv/core/Core;->getNativeLibraryName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/opencv/core/Core;->NATIVE_LIBRARY_NAME:Ljava/lang/String;

    .line 29
    invoke-static {}, Lorg/opencv/core/Core;->getVersionMajor()I

    move-result v0

    sput v0, Lorg/opencv/core/Core;->VERSION_MAJOR:I

    .line 30
    invoke-static {}, Lorg/opencv/core/Core;->getVersionMinor()I

    move-result v0

    sput v0, Lorg/opencv/core/Core;->VERSION_MINOR:I

    .line 31
    invoke-static {}, Lorg/opencv/core/Core;->getVersionRevision()I

    move-result v0

    sput v0, Lorg/opencv/core/Core;->VERSION_REVISION:I

    .line 32
    invoke-static {}, Lorg/opencv/core/Core;->getVersionStatus()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/opencv/core/Core;->VERSION_STATUS:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static LUT(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 6

    .line 661
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->LUT_0(JJJ)V

    return-void
.end method

.method private static native LUT_0(JJJ)V
.end method

.method public static Mahalanobis(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)D
    .locals 6

    .line 318
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->Mahalanobis_0(JJJ)D

    move-result-wide p0

    return-wide p0
.end method

.method private static native Mahalanobis_0(JJJ)D
.end method

.method public static PCABackProject(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 8

    .line 675
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p3, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v7}, Lorg/opencv/core/Core;->PCABackProject_0(JJJJ)V

    return-void
.end method

.method private static native PCABackProject_0(JJJJ)V
.end method

.method public static PCACompute(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 6

    .line 712
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->PCACompute_2(JJJ)V

    return-void
.end method

.method public static PCACompute(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;D)V
    .locals 8

    .line 689
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v6, p3

    invoke-static/range {v0 .. v7}, Lorg/opencv/core/Core;->PCACompute_0(JJJD)V

    return-void
.end method

.method public static PCACompute(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V
    .locals 7

    .line 703
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    move v6, p3

    invoke-static/range {v0 .. v6}, Lorg/opencv/core/Core;->PCACompute_1(JJJI)V

    return-void
.end method

.method private static native PCACompute_0(JJJD)V
.end method

.method private static native PCACompute_1(JJJI)V
.end method

.method private static native PCACompute_2(JJJ)V
.end method

.method public static PCAProject(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 8

    .line 726
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p3, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v7}, Lorg/opencv/core/Core;->PCAProject_0(JJJJ)V

    return-void
.end method

.method private static native PCAProject_0(JJJJ)V
.end method

.method public static PSNR(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)D
    .locals 2

    .line 332
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1}, Lorg/opencv/core/Core;->PSNR_0(JJ)D

    move-result-wide p0

    return-wide p0
.end method

.method private static native PSNR_0(JJ)D
.end method

.method public static SVBackSubst(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 10

    .line 740
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p3, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v8, p4, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v9}, Lorg/opencv/core/Core;->SVBackSubst_0(JJJJJ)V

    return-void
.end method

.method private static native SVBackSubst_0(JJJJJ)V
.end method

.method public static SVDecomp(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 8

    .line 763
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p3, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v7}, Lorg/opencv/core/Core;->SVDecomp_1(JJJJ)V

    return-void
.end method

.method public static SVDecomp(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V
    .locals 9

    .line 754
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p3, Lorg/opencv/core/Mat;->nativeObj:J

    move v8, p4

    invoke-static/range {v0 .. v8}, Lorg/opencv/core/Core;->SVDecomp_0(JJJJI)V

    return-void
.end method

.method private static native SVDecomp_0(JJJJI)V
.end method

.method private static native SVDecomp_1(JJJJ)V
.end method

.method public static absdiff(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 6

    .line 777
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->absdiff_0(JJJ)V

    return-void
.end method

.method public static absdiff(Lorg/opencv/core/Mat;Lorg/opencv/core/Scalar;Lorg/opencv/core/Mat;)V
    .locals 12

    .line 791
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-object p0, p1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v2, 0x0

    aget-wide v2, p0, v2

    iget-object p0, p1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v4, 0x1

    aget-wide v4, p0, v4

    iget-object p0, p1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v6, 0x2

    aget-wide v6, p0, v6

    iget-object p0, p1, Lorg/opencv/core/Scalar;->val:[D

    const/4 p1, 0x3

    aget-wide v8, p0, p1

    iget-wide v10, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v11}, Lorg/opencv/core/Core;->absdiff_1(JDDDDJ)V

    return-void
.end method

.method private static native absdiff_0(JJJ)V
.end method

.method private static native absdiff_1(JDDDDJ)V
.end method

.method public static add(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 6

    .line 823
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->add_2(JJJ)V

    return-void
.end method

.method public static add(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 8

    .line 814
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p3, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v7}, Lorg/opencv/core/Core;->add_1(JJJJ)V

    return-void
.end method

.method public static add(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V
    .locals 9

    .line 805
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p3, Lorg/opencv/core/Mat;->nativeObj:J

    move v8, p4

    invoke-static/range {v0 .. v8}, Lorg/opencv/core/Core;->add_0(JJJJI)V

    return-void
.end method

.method public static add(Lorg/opencv/core/Mat;Lorg/opencv/core/Scalar;Lorg/opencv/core/Mat;)V
    .locals 12

    .line 855
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-object p0, p1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v2, 0x0

    aget-wide v2, p0, v2

    iget-object p0, p1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v4, 0x1

    aget-wide v4, p0, v4

    iget-object p0, p1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v6, 0x2

    aget-wide v6, p0, v6

    iget-object p0, p1, Lorg/opencv/core/Scalar;->val:[D

    const/4 p1, 0x3

    aget-wide v8, p0, p1

    iget-wide v10, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v11}, Lorg/opencv/core/Core;->add_5(JDDDDJ)V

    return-void
.end method

.method public static add(Lorg/opencv/core/Mat;Lorg/opencv/core/Scalar;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 16

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    .line 846
    iget-wide v1, v1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-object v3, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v4, 0x0

    aget-wide v4, v3, v4

    iget-object v3, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v6, 0x1

    aget-wide v6, v3, v6

    iget-object v3, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v8, 0x2

    aget-wide v8, v3, v8

    iget-object v0, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v3, 0x3

    aget-wide v10, v0, v3

    move-object/from16 v0, p2

    iget-wide v12, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p3

    iget-wide v14, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v0, v1

    move-wide v2, v4

    move-wide v4, v6

    move-wide v6, v8

    move-wide v8, v10

    move-wide v10, v12

    move-wide v12, v14

    invoke-static/range {v0 .. v13}, Lorg/opencv/core/Core;->add_4(JDDDDJJ)V

    return-void
.end method

.method public static add(Lorg/opencv/core/Mat;Lorg/opencv/core/Scalar;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V
    .locals 16

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    .line 837
    iget-wide v1, v1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-object v3, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v4, 0x0

    aget-wide v4, v3, v4

    iget-object v3, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v6, 0x1

    aget-wide v6, v3, v6

    iget-object v3, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v8, 0x2

    aget-wide v8, v3, v8

    iget-object v0, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v3, 0x3

    aget-wide v10, v0, v3

    move-object/from16 v0, p2

    iget-wide v12, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p3

    iget-wide v14, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v0, v1

    move-wide v2, v4

    move-wide v4, v6

    move-wide v6, v8

    move-wide v8, v10

    move-wide v10, v12

    move-wide v12, v14

    move/from16 v14, p4

    invoke-static/range {v0 .. v14}, Lorg/opencv/core/Core;->add_3(JDDDDJJI)V

    return-void
.end method

.method public static addWeighted(Lorg/opencv/core/Mat;DLorg/opencv/core/Mat;DDLorg/opencv/core/Mat;)V
    .locals 12

    move-object v0, p0

    .line 878
    iget-wide v0, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object v2, p3

    iget-wide v4, v2, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v2, p8

    iget-wide v10, v2, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v2, p1

    move-wide/from16 v6, p4

    move-wide/from16 v8, p6

    invoke-static/range {v0 .. v11}, Lorg/opencv/core/Core;->addWeighted_1(JDJDDJ)V

    return-void
.end method

.method public static addWeighted(Lorg/opencv/core/Mat;DLorg/opencv/core/Mat;DDLorg/opencv/core/Mat;I)V
    .locals 13

    move-object v0, p0

    .line 869
    iget-wide v0, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v2, p3

    iget-wide v4, v2, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v2, p8

    iget-wide v10, v2, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v2, p1

    move-wide/from16 v6, p4

    move-wide/from16 v8, p6

    move/from16 v12, p9

    invoke-static/range {v0 .. v12}, Lorg/opencv/core/Core;->addWeighted_0(JDJDDJI)V

    return-void
.end method

.method private static native addWeighted_0(JDJDDJI)V
.end method

.method private static native addWeighted_1(JDJDDJ)V
.end method

.method private static native add_0(JJJJI)V
.end method

.method private static native add_1(JJJJ)V
.end method

.method private static native add_2(JJJ)V
.end method

.method private static native add_3(JDDDDJJI)V
.end method

.method private static native add_4(JDDDDJJ)V
.end method

.method private static native add_5(JDDDDJ)V
.end method

.method public static batchDistance(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;ILorg/opencv/core/Mat;)V
    .locals 9

    .line 910
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v7, p4, Lorg/opencv/core/Mat;->nativeObj:J

    move v6, p3

    invoke-static/range {v0 .. v8}, Lorg/opencv/core/Core;->batchDistance_2(JJJIJ)V

    return-void
.end method

.method public static batchDistance(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;ILorg/opencv/core/Mat;II)V
    .locals 11

    move-object v0, p0

    .line 901
    iget-wide v0, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object v2, p1

    iget-wide v2, v2, Lorg/opencv/core/Mat;->nativeObj:J

    move-object v4, p2

    iget-wide v4, v4, Lorg/opencv/core/Mat;->nativeObj:J

    move-object v6, p4

    iget-wide v7, v6, Lorg/opencv/core/Mat;->nativeObj:J

    move v6, p3

    move/from16 v9, p5

    move/from16 v10, p6

    invoke-static/range {v0 .. v10}, Lorg/opencv/core/Core;->batchDistance_1(JJJIJII)V

    return-void
.end method

.method public static batchDistance(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;ILorg/opencv/core/Mat;IILorg/opencv/core/Mat;IZ)V
    .locals 15

    move-object v0, p0

    .line 892
    iget-wide v0, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v2, p1

    iget-wide v2, v2, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v4, p2

    iget-wide v4, v4, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v6, p4

    iget-wide v7, v6, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v6, p7

    iget-wide v11, v6, Lorg/opencv/core/Mat;->nativeObj:J

    move/from16 v6, p3

    move/from16 v9, p5

    move/from16 v10, p6

    move/from16 v13, p8

    move/from16 v14, p9

    invoke-static/range {v0 .. v14}, Lorg/opencv/core/Core;->batchDistance_0(JJJIJIIJIZ)V

    return-void
.end method

.method private static native batchDistance_0(JJJIJIIJIZ)V
.end method

.method private static native batchDistance_1(JJJIJII)V
.end method

.method private static native batchDistance_2(JJJIJ)V
.end method

.method public static bitwise_and(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 6

    .line 933
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->bitwise_and_1(JJJ)V

    return-void
.end method

.method public static bitwise_and(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 8

    .line 924
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p3, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v7}, Lorg/opencv/core/Core;->bitwise_and_0(JJJJ)V

    return-void
.end method

.method private static native bitwise_and_0(JJJJ)V
.end method

.method private static native bitwise_and_1(JJJ)V
.end method

.method public static bitwise_not(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 2

    .line 956
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1}, Lorg/opencv/core/Core;->bitwise_not_1(JJ)V

    return-void
.end method

.method public static bitwise_not(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 6

    .line 947
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->bitwise_not_0(JJJ)V

    return-void
.end method

.method private static native bitwise_not_0(JJJ)V
.end method

.method private static native bitwise_not_1(JJ)V
.end method

.method public static bitwise_or(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 6

    .line 979
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->bitwise_or_1(JJJ)V

    return-void
.end method

.method public static bitwise_or(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 8

    .line 970
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p3, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v7}, Lorg/opencv/core/Core;->bitwise_or_0(JJJJ)V

    return-void
.end method

.method private static native bitwise_or_0(JJJJ)V
.end method

.method private static native bitwise_or_1(JJJ)V
.end method

.method public static bitwise_xor(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 6

    .line 1002
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->bitwise_xor_1(JJJ)V

    return-void
.end method

.method public static bitwise_xor(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 8

    .line 993
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p3, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v7}, Lorg/opencv/core/Core;->bitwise_xor_0(JJJJ)V

    return-void
.end method

.method private static native bitwise_xor_0(JJJJ)V
.end method

.method private static native bitwise_xor_1(JJJ)V
.end method

.method public static borderInterpolate(III)I
    .locals 0

    .line 535
    invoke-static {p0, p1, p2}, Lorg/opencv/core/Core;->borderInterpolate_0(III)I

    move-result p0

    return p0
.end method

.method private static native borderInterpolate_0(III)I
.end method

.method public static calcCovarMatrix(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V
    .locals 7

    .line 1025
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    move v6, p3

    invoke-static/range {v0 .. v6}, Lorg/opencv/core/Core;->calcCovarMatrix_1(JJJI)V

    return-void
.end method

.method public static calcCovarMatrix(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;II)V
    .locals 8

    .line 1016
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    move v6, p3

    move v7, p4

    invoke-static/range {v0 .. v7}, Lorg/opencv/core/Core;->calcCovarMatrix_0(JJJII)V

    return-void
.end method

.method private static native calcCovarMatrix_0(JJJII)V
.end method

.method private static native calcCovarMatrix_1(JJJI)V
.end method

.method public static cartToPolar(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 8

    .line 1048
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p3, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v7}, Lorg/opencv/core/Core;->cartToPolar_1(JJJJ)V

    return-void
.end method

.method public static cartToPolar(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Z)V
    .locals 9

    .line 1039
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p3, Lorg/opencv/core/Mat;->nativeObj:J

    move v8, p4

    invoke-static/range {v0 .. v8}, Lorg/opencv/core/Core;->cartToPolar_0(JJJJZ)V

    return-void
.end method

.method private static native cartToPolar_0(JJJJZ)V
.end method

.method private static native cartToPolar_1(JJJJ)V
.end method

.method public static checkRange(Lorg/opencv/core/Mat;)Z
    .locals 2

    .line 258
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1}, Lorg/opencv/core/Core;->checkRange_1(J)Z

    move-result p0

    return p0
.end method

.method public static checkRange(Lorg/opencv/core/Mat;ZDD)Z
    .locals 7

    .line 249
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    move v2, p1

    move-wide v3, p2

    move-wide v5, p4

    invoke-static/range {v0 .. v6}, Lorg/opencv/core/Core;->checkRange_0(JZDD)Z

    move-result p0

    return p0
.end method

.method private static native checkRange_0(JZDD)Z
.end method

.method private static native checkRange_1(J)Z
.end method

.method public static compare(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V
    .locals 7

    .line 1062
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    move v6, p3

    invoke-static/range {v0 .. v6}, Lorg/opencv/core/Core;->compare_0(JJJI)V

    return-void
.end method

.method public static compare(Lorg/opencv/core/Mat;Lorg/opencv/core/Scalar;Lorg/opencv/core/Mat;I)V
    .locals 14

    move-object v0, p1

    move-object v1, p0

    .line 1076
    iget-wide v1, v1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-object v3, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v4, 0x0

    aget-wide v4, v3, v4

    iget-object v3, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v6, 0x1

    aget-wide v6, v3, v6

    iget-object v3, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v8, 0x2

    aget-wide v8, v3, v8

    iget-object v0, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v3, 0x3

    aget-wide v10, v0, v3

    move-object/from16 v0, p2

    iget-wide v12, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v0, v1

    move-wide v2, v4

    move-wide v4, v6

    move-wide v6, v8

    move-wide v8, v10

    move-wide v10, v12

    move/from16 v12, p3

    invoke-static/range {v0 .. v12}, Lorg/opencv/core/Core;->compare_1(JDDDDJI)V

    return-void
.end method

.method private static native compare_0(JJJI)V
.end method

.method private static native compare_1(JDDDDJI)V
.end method

.method public static completeSymm(Lorg/opencv/core/Mat;)V
    .locals 2

    .line 1099
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1}, Lorg/opencv/core/Core;->completeSymm_1(J)V

    return-void
.end method

.method public static completeSymm(Lorg/opencv/core/Mat;Z)V
    .locals 2

    .line 1090
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p1}, Lorg/opencv/core/Core;->completeSymm_0(JZ)V

    return-void
.end method

.method private static native completeSymm_0(JZ)V
.end method

.method private static native completeSymm_1(J)V
.end method

.method public static convertScaleAbs(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 2

    .line 1122
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1}, Lorg/opencv/core/Core;->convertScaleAbs_1(JJ)V

    return-void
.end method

.method public static convertScaleAbs(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;DD)V
    .locals 8

    .line 1113
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v4, p2

    move-wide v6, p4

    invoke-static/range {v0 .. v7}, Lorg/opencv/core/Core;->convertScaleAbs_0(JJDD)V

    return-void
.end method

.method private static native convertScaleAbs_0(JJDD)V
.end method

.method private static native convertScaleAbs_1(JJ)V
.end method

.method public static copyMakeBorder(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;IIIII)V
    .locals 9

    .line 1145
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    move v4, p2

    move v5, p3

    move v6, p4

    move v7, p5

    move v8, p6

    invoke-static/range {v0 .. v8}, Lorg/opencv/core/Core;->copyMakeBorder_1(JJIIIII)V

    return-void
.end method

.method public static copyMakeBorder(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;IIIIILorg/opencv/core/Scalar;)V
    .locals 18

    move-object/from16 v0, p7

    move/from16 v5, p2

    move/from16 v6, p3

    move/from16 v7, p4

    move/from16 v8, p5

    move/from16 v9, p6

    move-object/from16 v1, p0

    .line 1136
    iget-wide v1, v1, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v3, p1

    iget-wide v3, v3, Lorg/opencv/core/Mat;->nativeObj:J

    iget-object v10, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v11, 0x0

    aget-wide v11, v10, v11

    move-wide v10, v11

    iget-object v12, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v13, 0x1

    aget-wide v13, v12, v13

    move-wide v12, v13

    iget-object v14, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v15, 0x2

    aget-wide v15, v14, v15

    move-wide v14, v15

    iget-object v0, v0, Lorg/opencv/core/Scalar;->val:[D

    const/16 v16, 0x3

    aget-wide v16, v0, v16

    invoke-static/range {v1 .. v17}, Lorg/opencv/core/Core;->copyMakeBorder_0(JJIIIIIDDDD)V

    return-void
.end method

.method private static native copyMakeBorder_0(JJIIIIIDDDD)V
.end method

.method private static native copyMakeBorder_1(JJIIIII)V
.end method

.method public static countNonZero(Lorg/opencv/core/Mat;)I
    .locals 2

    .line 549
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1}, Lorg/opencv/core/Core;->countNonZero_0(J)I

    move-result p0

    return p0
.end method

.method private static native countNonZero_0(J)I
.end method

.method public static cubeRoot(F)F
    .locals 0

    .line 507
    invoke-static {p0}, Lorg/opencv/core/Core;->cubeRoot_0(F)F

    move-result p0

    return p0
.end method

.method private static native cubeRoot_0(F)F
.end method

.method public static dct(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 2

    .line 1168
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1}, Lorg/opencv/core/Core;->dct_1(JJ)V

    return-void
.end method

.method public static dct(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V
    .locals 2

    .line 1159
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1, p2}, Lorg/opencv/core/Core;->dct_0(JJI)V

    return-void
.end method

.method private static native dct_0(JJI)V
.end method

.method private static native dct_1(JJ)V
.end method

.method public static determinant(Lorg/opencv/core/Mat;)D
    .locals 2

    .line 346
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1}, Lorg/opencv/core/Core;->determinant_0(J)D

    move-result-wide v0

    return-wide v0
.end method

.method private static native determinant_0(J)D
.end method

.method public static dft(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 2

    .line 1191
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1}, Lorg/opencv/core/Core;->dft_1(JJ)V

    return-void
.end method

.method public static dft(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;II)V
    .locals 6

    .line 1182
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->dft_0(JJII)V

    return-void
.end method

.method private static native dft_0(JJII)V
.end method

.method private static native dft_1(JJ)V
.end method

.method public static divide(DLorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 6

    .line 1278
    iget-wide v2, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p3, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v0, p0

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->divide_7(DJJ)V

    return-void
.end method

.method public static divide(DLorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V
    .locals 7

    .line 1269
    iget-wide v2, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p3, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v0, p0

    move v6, p4

    invoke-static/range {v0 .. v6}, Lorg/opencv/core/Core;->divide_6(DJJI)V

    return-void
.end method

.method public static divide(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 6

    .line 1223
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->divide_2(JJJ)V

    return-void
.end method

.method public static divide(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;D)V
    .locals 8

    .line 1214
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v6, p3

    invoke-static/range {v0 .. v7}, Lorg/opencv/core/Core;->divide_1(JJJD)V

    return-void
.end method

.method public static divide(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;DI)V
    .locals 9

    .line 1205
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v6, p3

    move v8, p5

    invoke-static/range {v0 .. v8}, Lorg/opencv/core/Core;->divide_0(JJJDI)V

    return-void
.end method

.method public static divide(Lorg/opencv/core/Mat;Lorg/opencv/core/Scalar;Lorg/opencv/core/Mat;)V
    .locals 12

    .line 1255
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-object p0, p1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v2, 0x0

    aget-wide v2, p0, v2

    iget-object p0, p1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v4, 0x1

    aget-wide v4, p0, v4

    iget-object p0, p1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v6, 0x2

    aget-wide v6, p0, v6

    iget-object p0, p1, Lorg/opencv/core/Scalar;->val:[D

    const/4 p1, 0x3

    aget-wide v8, p0, p1

    iget-wide v10, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v11}, Lorg/opencv/core/Core;->divide_5(JDDDDJ)V

    return-void
.end method

.method public static divide(Lorg/opencv/core/Mat;Lorg/opencv/core/Scalar;Lorg/opencv/core/Mat;D)V
    .locals 14

    move-object v0, p1

    move-object v1, p0

    .line 1246
    iget-wide v1, v1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-object v3, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v4, 0x0

    aget-wide v4, v3, v4

    iget-object v3, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v6, 0x1

    aget-wide v6, v3, v6

    iget-object v3, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v8, 0x2

    aget-wide v8, v3, v8

    iget-object v0, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v3, 0x3

    aget-wide v10, v0, v3

    move-object/from16 v0, p2

    iget-wide v12, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v0, v1

    move-wide v2, v4

    move-wide v4, v6

    move-wide v6, v8

    move-wide v8, v10

    move-wide v10, v12

    move-wide/from16 v12, p3

    invoke-static/range {v0 .. v13}, Lorg/opencv/core/Core;->divide_4(JDDDDJD)V

    return-void
.end method

.method public static divide(Lorg/opencv/core/Mat;Lorg/opencv/core/Scalar;Lorg/opencv/core/Mat;DI)V
    .locals 15

    move-object/from16 v0, p1

    move-object v1, p0

    .line 1237
    iget-wide v1, v1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-object v3, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v4, 0x0

    aget-wide v4, v3, v4

    iget-object v3, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v6, 0x1

    aget-wide v6, v3, v6

    iget-object v3, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v8, 0x2

    aget-wide v8, v3, v8

    iget-object v0, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v3, 0x3

    aget-wide v10, v0, v3

    move-object/from16 v0, p2

    iget-wide v12, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v0, v1

    move-wide v2, v4

    move-wide v4, v6

    move-wide v6, v8

    move-wide v8, v10

    move-wide v10, v12

    move-wide/from16 v12, p3

    move/from16 v14, p5

    invoke-static/range {v0 .. v14}, Lorg/opencv/core/Core;->divide_3(JDDDDJDI)V

    return-void
.end method

.method private static native divide_0(JJJDI)V
.end method

.method private static native divide_1(JJJD)V
.end method

.method private static native divide_2(JJJ)V
.end method

.method private static native divide_3(JDDDDJDI)V
.end method

.method private static native divide_4(JDDDDJD)V
.end method

.method private static native divide_5(JDDDDJ)V
.end method

.method private static native divide_6(DJJI)V
.end method

.method private static native divide_7(DJJ)V
.end method

.method public static eigen(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)Z
    .locals 2

    .line 281
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1}, Lorg/opencv/core/Core;->eigen_1(JJ)Z

    move-result p0

    return p0
.end method

.method public static eigen(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)Z
    .locals 6

    .line 272
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->eigen_0(JJJ)Z

    move-result p0

    return p0
.end method

.method private static native eigen_0(JJJ)Z
.end method

.method private static native eigen_1(JJ)Z
.end method

.method public static exp(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 2

    .line 1292
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1}, Lorg/opencv/core/Core;->exp_0(JJ)V

    return-void
.end method

.method private static native exp_0(JJ)V
.end method

.method public static extractChannel(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V
    .locals 2

    .line 1306
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1, p2}, Lorg/opencv/core/Core;->extractChannel_0(JJI)V

    return-void
.end method

.method private static native extractChannel_0(JJI)V
.end method

.method public static fastAtan2(FF)F
    .locals 0

    .line 521
    invoke-static {p0, p1}, Lorg/opencv/core/Core;->fastAtan2_0(FF)F

    move-result p0

    return p0
.end method

.method private static native fastAtan2_0(FF)F
.end method

.method public static findNonZero(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 2

    .line 1320
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1}, Lorg/opencv/core/Core;->findNonZero_0(JJ)V

    return-void
.end method

.method private static native findNonZero_0(JJ)V
.end method

.method public static flip(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V
    .locals 2

    .line 1334
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1, p2}, Lorg/opencv/core/Core;->flip_0(JJI)V

    return-void
.end method

.method private static native flip_0(JJI)V
.end method

.method public static gemm(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;DLorg/opencv/core/Mat;DLorg/opencv/core/Mat;)V
    .locals 12

    move-object v0, p0

    .line 1357
    iget-wide v0, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object v2, p1

    iget-wide v2, v2, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v4, p4

    iget-wide v6, v4, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v4, p7

    iget-wide v10, v4, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v4, p2

    move-wide/from16 v8, p5

    invoke-static/range {v0 .. v11}, Lorg/opencv/core/Core;->gemm_1(JJDJDJ)V

    return-void
.end method

.method public static gemm(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;DLorg/opencv/core/Mat;DLorg/opencv/core/Mat;I)V
    .locals 13

    move-object v0, p0

    .line 1348
    iget-wide v0, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object v2, p1

    iget-wide v2, v2, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v4, p4

    iget-wide v6, v4, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v4, p7

    iget-wide v10, v4, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v4, p2

    move-wide/from16 v8, p5

    move/from16 v12, p8

    invoke-static/range {v0 .. v12}, Lorg/opencv/core/Core;->gemm_0(JJDJDJI)V

    return-void
.end method

.method private static native gemm_0(JJDJDJI)V
.end method

.method private static native gemm_1(JJDJDJ)V
.end method

.method public static getBuildInformation()Ljava/lang/String;
    .locals 1

    .line 235
    invoke-static {}, Lorg/opencv/core/Core;->getBuildInformation_0()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static native getBuildInformation_0()Ljava/lang/String;
.end method

.method public static getCPUTickCount()J
    .locals 2

    .line 633
    invoke-static {}, Lorg/opencv/core/Core;->getCPUTickCount_0()J

    move-result-wide v0

    return-wide v0
.end method

.method private static native getCPUTickCount_0()J
.end method

.method private static getNativeLibraryName()Ljava/lang/String;
    .locals 1

    const-string v0, "opencv_java310"

    return-object v0
.end method

.method public static getNumThreads()I
    .locals 1

    .line 563
    invoke-static {}, Lorg/opencv/core/Core;->getNumThreads_0()I

    move-result v0

    return v0
.end method

.method private static native getNumThreads_0()I
.end method

.method public static getNumberOfCPUs()I
    .locals 1

    .line 577
    invoke-static {}, Lorg/opencv/core/Core;->getNumberOfCPUs_0()I

    move-result v0

    return v0
.end method

.method private static native getNumberOfCPUs_0()I
.end method

.method public static getOptimalDFTSize(I)I
    .locals 0

    .line 591
    invoke-static {p0}, Lorg/opencv/core/Core;->getOptimalDFTSize_0(I)I

    move-result p0

    return p0
.end method

.method private static native getOptimalDFTSize_0(I)I
.end method

.method public static getThreadNum()I
    .locals 1

    .line 605
    invoke-static {}, Lorg/opencv/core/Core;->getThreadNum_0()I

    move-result v0

    return v0
.end method

.method private static native getThreadNum_0()I
.end method

.method public static getTickCount()J
    .locals 2

    .line 647
    invoke-static {}, Lorg/opencv/core/Core;->getTickCount_0()J

    move-result-wide v0

    return-wide v0
.end method

.method private static native getTickCount_0()J
.end method

.method public static getTickFrequency()D
    .locals 2

    .line 360
    invoke-static {}, Lorg/opencv/core/Core;->getTickFrequency_0()D

    move-result-wide v0

    return-wide v0
.end method

.method private static native getTickFrequency_0()D
.end method

.method private static getVersion()Ljava/lang/String;
    .locals 1

    const-string v0, "3.1.0"

    return-object v0
.end method

.method private static getVersionMajor()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method private static getVersionMinor()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method private static getVersionRevision()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method private static getVersionStatus()Ljava/lang/String;
    .locals 1

    const-string v0, ""

    return-object v0
.end method

.method public static hconcat(Ljava/util/List;Lorg/opencv/core/Mat;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/opencv/core/Mat;",
            ">;",
            "Lorg/opencv/core/Mat;",
            ")V"
        }
    .end annotation

    .line 1370
    invoke-static {p0}, Lorg/opencv/utils/Converters;->vector_Mat_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;

    move-result-object p0

    .line 1371
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1}, Lorg/opencv/core/Core;->hconcat_0(JJ)V

    return-void
.end method

.method private static native hconcat_0(JJ)V
.end method

.method public static idct(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 2

    .line 1394
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1}, Lorg/opencv/core/Core;->idct_1(JJ)V

    return-void
.end method

.method public static idct(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V
    .locals 2

    .line 1385
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1, p2}, Lorg/opencv/core/Core;->idct_0(JJI)V

    return-void
.end method

.method private static native idct_0(JJI)V
.end method

.method private static native idct_1(JJ)V
.end method

.method public static idft(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 2

    .line 1417
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1}, Lorg/opencv/core/Core;->idft_1(JJ)V

    return-void
.end method

.method public static idft(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;II)V
    .locals 6

    .line 1408
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->idft_0(JJII)V

    return-void
.end method

.method private static native idft_0(JJII)V
.end method

.method private static native idft_1(JJ)V
.end method

.method public static inRange(Lorg/opencv/core/Mat;Lorg/opencv/core/Scalar;Lorg/opencv/core/Scalar;Lorg/opencv/core/Mat;)V
    .locals 22

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p0

    .line 1431
    iget-wide v2, v2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-object v4, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v12, 0x0

    aget-wide v5, v4, v12

    move-wide v4, v5

    iget-object v6, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v14, 0x1

    aget-wide v7, v6, v14

    move-wide v6, v7

    iget-object v8, v0, Lorg/opencv/core/Scalar;->val:[D

    const/16 v16, 0x2

    aget-wide v9, v8, v16

    move-wide v8, v9

    iget-object v0, v0, Lorg/opencv/core/Scalar;->val:[D

    const/16 v18, 0x3

    aget-wide v10, v0, v18

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    aget-wide v12, v0, v12

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    aget-wide v14, v0, v14

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    aget-wide v16, v0, v16

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    aget-wide v18, v0, v18

    move-object/from16 v0, p3

    iget-wide v0, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide/from16 v20, v0

    invoke-static/range {v2 .. v21}, Lorg/opencv/core/Core;->inRange_0(JDDDDDDDDJ)V

    return-void
.end method

.method private static native inRange_0(JDDDDDDDDJ)V
.end method

.method public static insertChannel(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V
    .locals 2

    .line 1445
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1, p2}, Lorg/opencv/core/Core;->insertChannel_0(JJI)V

    return-void
.end method

.method private static native insertChannel_0(JJI)V
.end method

.method public static invert(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)D
    .locals 2

    .line 383
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1}, Lorg/opencv/core/Core;->invert_1(JJ)D

    move-result-wide p0

    return-wide p0
.end method

.method public static invert(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)D
    .locals 2

    .line 374
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1, p2}, Lorg/opencv/core/Core;->invert_0(JJI)D

    move-result-wide p0

    return-wide p0
.end method

.method private static native invert_0(JJI)D
.end method

.method private static native invert_1(JJ)D
.end method

.method public static kmeans(Lorg/opencv/core/Mat;ILorg/opencv/core/Mat;Lorg/opencv/core/TermCriteria;II)D
    .locals 11

    move-object v0, p3

    move-object v1, p0

    .line 406
    iget-wide v1, v1, Lorg/opencv/core/Mat;->nativeObj:J

    move-object v3, p2

    iget-wide v3, v3, Lorg/opencv/core/Mat;->nativeObj:J

    iget v5, v0, Lorg/opencv/core/TermCriteria;->type:I

    iget v6, v0, Lorg/opencv/core/TermCriteria;->maxCount:I

    iget-wide v7, v0, Lorg/opencv/core/TermCriteria;->epsilon:D

    move-wide v0, v1

    move v2, p1

    move v9, p4

    move/from16 v10, p5

    invoke-static/range {v0 .. v10}, Lorg/opencv/core/Core;->kmeans_1(JIJIIDII)D

    move-result-wide v0

    return-wide v0
.end method

.method public static kmeans(Lorg/opencv/core/Mat;ILorg/opencv/core/Mat;Lorg/opencv/core/TermCriteria;IILorg/opencv/core/Mat;)D
    .locals 13

    move-object/from16 v0, p3

    move-object v1, p0

    .line 397
    iget-wide v1, v1, Lorg/opencv/core/Mat;->nativeObj:J

    move-object v3, p2

    iget-wide v3, v3, Lorg/opencv/core/Mat;->nativeObj:J

    iget v5, v0, Lorg/opencv/core/TermCriteria;->type:I

    iget v6, v0, Lorg/opencv/core/TermCriteria;->maxCount:I

    iget-wide v7, v0, Lorg/opencv/core/TermCriteria;->epsilon:D

    move-object/from16 v0, p6

    iget-wide v11, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v0, v1

    move v2, p1

    move/from16 v9, p4

    move/from16 v10, p5

    invoke-static/range {v0 .. v12}, Lorg/opencv/core/Core;->kmeans_0(JIJIIDIIJ)D

    move-result-wide v0

    return-wide v0
.end method

.method private static native kmeans_0(JIJIIDIIJ)D
.end method

.method private static native kmeans_1(JIJIIDII)D
.end method

.method public static log(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 2

    .line 1459
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1}, Lorg/opencv/core/Core;->log_0(JJ)V

    return-void
.end method

.method private static native log_0(JJ)V
.end method

.method public static magnitude(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 6

    .line 1473
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->magnitude_0(JJJ)V

    return-void
.end method

.method private static native magnitude_0(JJJ)V
.end method

.method public static max(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 6

    .line 1487
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->max_0(JJJ)V

    return-void
.end method

.method public static max(Lorg/opencv/core/Mat;Lorg/opencv/core/Scalar;Lorg/opencv/core/Mat;)V
    .locals 12

    .line 1501
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-object p0, p1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v2, 0x0

    aget-wide v2, p0, v2

    iget-object p0, p1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v4, 0x1

    aget-wide v4, p0, v4

    iget-object p0, p1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v6, 0x2

    aget-wide v6, p0, v6

    iget-object p0, p1, Lorg/opencv/core/Scalar;->val:[D

    const/4 p1, 0x3

    aget-wide v8, p0, p1

    iget-wide v10, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v11}, Lorg/opencv/core/Core;->max_1(JDDDDJ)V

    return-void
.end method

.method private static native max_0(JJJ)V
.end method

.method private static native max_1(JDDDDJ)V
.end method

.method public static mean(Lorg/opencv/core/Mat;)Lorg/opencv/core/Scalar;
    .locals 3

    .line 193
    new-instance v0, Lorg/opencv/core/Scalar;

    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2}, Lorg/opencv/core/Core;->mean_1(J)[D

    move-result-object p0

    invoke-direct {v0, p0}, Lorg/opencv/core/Scalar;-><init>([D)V

    return-object v0
.end method

.method public static mean(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)Lorg/opencv/core/Scalar;
    .locals 3

    .line 184
    new-instance v0, Lorg/opencv/core/Scalar;

    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2, p0, p1}, Lorg/opencv/core/Core;->mean_0(JJ)[D

    move-result-object p0

    invoke-direct {v0, p0}, Lorg/opencv/core/Scalar;-><init>([D)V

    return-object v0
.end method

.method public static meanStdDev(Lorg/opencv/core/Mat;Lorg/opencv/core/MatOfDouble;Lorg/opencv/core/MatOfDouble;)V
    .locals 6

    .line 1526
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->meanStdDev_1(JJJ)V

    return-void
.end method

.method public static meanStdDev(Lorg/opencv/core/Mat;Lorg/opencv/core/MatOfDouble;Lorg/opencv/core/MatOfDouble;Lorg/opencv/core/Mat;)V
    .locals 8

    .line 1516
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p3, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v7}, Lorg/opencv/core/Core;->meanStdDev_0(JJJJ)V

    return-void
.end method

.method private static native meanStdDev_0(JJJJ)V
.end method

.method private static native meanStdDev_1(JJJ)V
.end method

.method private static native mean_0(JJ)[D
.end method

.method private static native mean_1(J)[D
.end method

.method public static merge(Ljava/util/List;Lorg/opencv/core/Mat;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/opencv/core/Mat;",
            ">;",
            "Lorg/opencv/core/Mat;",
            ")V"
        }
    .end annotation

    .line 1539
    invoke-static {p0}, Lorg/opencv/utils/Converters;->vector_Mat_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;

    move-result-object p0

    .line 1540
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1}, Lorg/opencv/core/Core;->merge_0(JJ)V

    return-void
.end method

.method private static native merge_0(JJ)V
.end method

.method public static min(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 6

    .line 1554
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->min_0(JJJ)V

    return-void
.end method

.method public static min(Lorg/opencv/core/Mat;Lorg/opencv/core/Scalar;Lorg/opencv/core/Mat;)V
    .locals 12

    .line 1568
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-object p0, p1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v2, 0x0

    aget-wide v2, p0, v2

    iget-object p0, p1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v4, 0x1

    aget-wide v4, p0, v4

    iget-object p0, p1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v6, 0x2

    aget-wide v6, p0, v6

    iget-object p0, p1, Lorg/opencv/core/Scalar;->val:[D

    const/4 p1, 0x3

    aget-wide v8, p0, p1

    iget-wide v10, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v11}, Lorg/opencv/core/Core;->min_1(JDDDDJ)V

    return-void
.end method

.method public static minMaxLoc(Lorg/opencv/core/Mat;)Lorg/opencv/core/Core$MinMaxLocResult;
    .locals 1

    const/4 v0, 0x0

    .line 2198
    invoke-static {p0, v0}, Lorg/opencv/core/Core;->minMaxLoc(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)Lorg/opencv/core/Core$MinMaxLocResult;

    move-result-object p0

    return-object p0
.end method

.method public static minMaxLoc(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)Lorg/opencv/core/Core$MinMaxLocResult;
    .locals 3

    .line 2181
    new-instance v0, Lorg/opencv/core/Core$MinMaxLocResult;

    invoke-direct {v0}, Lorg/opencv/core/Core$MinMaxLocResult;-><init>()V

    if-eqz p1, :cond_0

    .line 2184
    iget-wide v1, p1, Lorg/opencv/core/Mat;->nativeObj:J

    goto :goto_0

    :cond_0
    const-wide/16 v1, 0x0

    .line 2186
    :goto_0
    iget-wide p0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {p0, p1, v1, v2}, Lorg/opencv/core/Core;->n_minMaxLocManual(JJ)[D

    move-result-object p0

    const/4 p1, 0x0

    .line 2187
    aget-wide v1, p0, p1

    iput-wide v1, v0, Lorg/opencv/core/Core$MinMaxLocResult;->minVal:D

    const/4 p1, 0x1

    .line 2188
    aget-wide v1, p0, p1

    iput-wide v1, v0, Lorg/opencv/core/Core$MinMaxLocResult;->maxVal:D

    .line 2189
    iget-object p1, v0, Lorg/opencv/core/Core$MinMaxLocResult;->minLoc:Lorg/opencv/core/Point;

    const/4 v1, 0x2

    aget-wide v1, p0, v1

    iput-wide v1, p1, Lorg/opencv/core/Point;->x:D

    .line 2190
    iget-object p1, v0, Lorg/opencv/core/Core$MinMaxLocResult;->minLoc:Lorg/opencv/core/Point;

    const/4 v1, 0x3

    aget-wide v1, p0, v1

    iput-wide v1, p1, Lorg/opencv/core/Point;->y:D

    .line 2191
    iget-object p1, v0, Lorg/opencv/core/Core$MinMaxLocResult;->maxLoc:Lorg/opencv/core/Point;

    const/4 v1, 0x4

    aget-wide v1, p0, v1

    iput-wide v1, p1, Lorg/opencv/core/Point;->x:D

    .line 2192
    iget-object p1, v0, Lorg/opencv/core/Core$MinMaxLocResult;->maxLoc:Lorg/opencv/core/Point;

    const/4 v1, 0x5

    aget-wide v1, p0, v1

    iput-wide v1, p1, Lorg/opencv/core/Point;->y:D

    return-object v0
.end method

.method private static native min_0(JJJ)V
.end method

.method private static native min_1(JDDDDJ)V
.end method

.method public static mixChannels(Ljava/util/List;Ljava/util/List;Lorg/opencv/core/MatOfInt;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/opencv/core/Mat;",
            ">;",
            "Ljava/util/List<",
            "Lorg/opencv/core/Mat;",
            ">;",
            "Lorg/opencv/core/MatOfInt;",
            ")V"
        }
    .end annotation

    .line 1581
    invoke-static {p0}, Lorg/opencv/utils/Converters;->vector_Mat_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;

    move-result-object p0

    .line 1582
    invoke-static {p1}, Lorg/opencv/utils/Converters;->vector_Mat_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;

    move-result-object p1

    .line 1584
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->mixChannels_0(JJJ)V

    return-void
.end method

.method private static native mixChannels_0(JJJ)V
.end method

.method public static mulSpectrums(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V
    .locals 7

    .line 1607
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    move v6, p3

    invoke-static/range {v0 .. v6}, Lorg/opencv/core/Core;->mulSpectrums_1(JJJI)V

    return-void
.end method

.method public static mulSpectrums(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;IZ)V
    .locals 8

    .line 1598
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    move v6, p3

    move v7, p4

    invoke-static/range {v0 .. v7}, Lorg/opencv/core/Core;->mulSpectrums_0(JJJIZ)V

    return-void
.end method

.method private static native mulSpectrums_0(JJJIZ)V
.end method

.method private static native mulSpectrums_1(JJJI)V
.end method

.method public static mulTransposed(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Z)V
    .locals 2

    .line 1639
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1, p2}, Lorg/opencv/core/Core;->mulTransposed_2(JJZ)V

    return-void
.end method

.method public static mulTransposed(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;ZLorg/opencv/core/Mat;D)V
    .locals 9

    .line 1630
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v5, p3, Lorg/opencv/core/Mat;->nativeObj:J

    move v4, p2

    move-wide v7, p4

    invoke-static/range {v0 .. v8}, Lorg/opencv/core/Core;->mulTransposed_1(JJZJD)V

    return-void
.end method

.method public static mulTransposed(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;ZLorg/opencv/core/Mat;DI)V
    .locals 10

    move-object v0, p0

    .line 1621
    iget-wide v0, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object v2, p1

    iget-wide v2, v2, Lorg/opencv/core/Mat;->nativeObj:J

    move-object v4, p3

    iget-wide v5, v4, Lorg/opencv/core/Mat;->nativeObj:J

    move v4, p2

    move-wide v7, p4

    move/from16 v9, p6

    invoke-static/range {v0 .. v9}, Lorg/opencv/core/Core;->mulTransposed_0(JJZJDI)V

    return-void
.end method

.method private static native mulTransposed_0(JJZJDI)V
.end method

.method private static native mulTransposed_1(JJZJD)V
.end method

.method private static native mulTransposed_2(JJZ)V
.end method

.method public static multiply(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 6

    .line 1671
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->multiply_2(JJJ)V

    return-void
.end method

.method public static multiply(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;D)V
    .locals 8

    .line 1662
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v6, p3

    invoke-static/range {v0 .. v7}, Lorg/opencv/core/Core;->multiply_1(JJJD)V

    return-void
.end method

.method public static multiply(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;DI)V
    .locals 9

    .line 1653
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v6, p3

    move v8, p5

    invoke-static/range {v0 .. v8}, Lorg/opencv/core/Core;->multiply_0(JJJDI)V

    return-void
.end method

.method public static multiply(Lorg/opencv/core/Mat;Lorg/opencv/core/Scalar;Lorg/opencv/core/Mat;)V
    .locals 12

    .line 1703
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-object p0, p1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v2, 0x0

    aget-wide v2, p0, v2

    iget-object p0, p1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v4, 0x1

    aget-wide v4, p0, v4

    iget-object p0, p1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v6, 0x2

    aget-wide v6, p0, v6

    iget-object p0, p1, Lorg/opencv/core/Scalar;->val:[D

    const/4 p1, 0x3

    aget-wide v8, p0, p1

    iget-wide v10, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v11}, Lorg/opencv/core/Core;->multiply_5(JDDDDJ)V

    return-void
.end method

.method public static multiply(Lorg/opencv/core/Mat;Lorg/opencv/core/Scalar;Lorg/opencv/core/Mat;D)V
    .locals 14

    move-object v0, p1

    move-object v1, p0

    .line 1694
    iget-wide v1, v1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-object v3, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v4, 0x0

    aget-wide v4, v3, v4

    iget-object v3, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v6, 0x1

    aget-wide v6, v3, v6

    iget-object v3, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v8, 0x2

    aget-wide v8, v3, v8

    iget-object v0, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v3, 0x3

    aget-wide v10, v0, v3

    move-object/from16 v0, p2

    iget-wide v12, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v0, v1

    move-wide v2, v4

    move-wide v4, v6

    move-wide v6, v8

    move-wide v8, v10

    move-wide v10, v12

    move-wide/from16 v12, p3

    invoke-static/range {v0 .. v13}, Lorg/opencv/core/Core;->multiply_4(JDDDDJD)V

    return-void
.end method

.method public static multiply(Lorg/opencv/core/Mat;Lorg/opencv/core/Scalar;Lorg/opencv/core/Mat;DI)V
    .locals 15

    move-object/from16 v0, p1

    move-object v1, p0

    .line 1685
    iget-wide v1, v1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-object v3, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v4, 0x0

    aget-wide v4, v3, v4

    iget-object v3, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v6, 0x1

    aget-wide v6, v3, v6

    iget-object v3, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v8, 0x2

    aget-wide v8, v3, v8

    iget-object v0, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v3, 0x3

    aget-wide v10, v0, v3

    move-object/from16 v0, p2

    iget-wide v12, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v0, v1

    move-wide v2, v4

    move-wide v4, v6

    move-wide v6, v8

    move-wide v8, v10

    move-wide v10, v12

    move-wide/from16 v12, p3

    move/from16 v14, p5

    invoke-static/range {v0 .. v14}, Lorg/opencv/core/Core;->multiply_3(JDDDDJDI)V

    return-void
.end method

.method private static native multiply_0(JJJDI)V
.end method

.method private static native multiply_1(JJJD)V
.end method

.method private static native multiply_2(JJJ)V
.end method

.method private static native multiply_3(JDDDDJDI)V
.end method

.method private static native multiply_4(JDDDDJD)V
.end method

.method private static native multiply_5(JDDDDJ)V
.end method

.method private static native n_minMaxLocManual(JJ)[D
.end method

.method public static norm(Lorg/opencv/core/Mat;)D
    .locals 2

    .line 470
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1}, Lorg/opencv/core/Core;->norm_5(J)D

    move-result-wide v0

    return-wide v0
.end method

.method public static norm(Lorg/opencv/core/Mat;I)D
    .locals 2

    .line 461
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p1}, Lorg/opencv/core/Core;->norm_4(JI)D

    move-result-wide p0

    return-wide p0
.end method

.method public static norm(Lorg/opencv/core/Mat;ILorg/opencv/core/Mat;)D
    .locals 4

    .line 452
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p1, v2, v3}, Lorg/opencv/core/Core;->norm_3(JIJ)D

    move-result-wide p0

    return-wide p0
.end method

.method public static norm(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)D
    .locals 2

    .line 438
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1}, Lorg/opencv/core/Core;->norm_2(JJ)D

    move-result-wide p0

    return-wide p0
.end method

.method public static norm(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)D
    .locals 2

    .line 429
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1, p2}, Lorg/opencv/core/Core;->norm_1(JJI)D

    move-result-wide p0

    return-wide p0
.end method

.method public static norm(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;ILorg/opencv/core/Mat;)D
    .locals 7

    .line 420
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v5, p3, Lorg/opencv/core/Mat;->nativeObj:J

    move v4, p2

    invoke-static/range {v0 .. v6}, Lorg/opencv/core/Core;->norm_0(JJIJ)D

    move-result-wide p0

    return-wide p0
.end method

.method private static native norm_0(JJIJ)D
.end method

.method private static native norm_1(JJI)D
.end method

.method private static native norm_2(JJ)D
.end method

.method private static native norm_3(JIJ)D
.end method

.method private static native norm_4(JI)D
.end method

.method private static native norm_5(J)D
.end method

.method public static normalize(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 2

    .line 1744
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1}, Lorg/opencv/core/Core;->normalize_3(JJ)V

    return-void
.end method

.method public static normalize(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;DDI)V
    .locals 9

    .line 1735
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v4, p2

    move-wide v6, p4

    move v8, p6

    invoke-static/range {v0 .. v8}, Lorg/opencv/core/Core;->normalize_2(JJDDI)V

    return-void
.end method

.method public static normalize(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;DDII)V
    .locals 10

    move-object v0, p0

    .line 1726
    iget-wide v0, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object v2, p1

    iget-wide v2, v2, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v4, p2

    move-wide v6, p4

    move/from16 v8, p6

    move/from16 v9, p7

    invoke-static/range {v0 .. v9}, Lorg/opencv/core/Core;->normalize_1(JJDDII)V

    return-void
.end method

.method public static normalize(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;DDIILorg/opencv/core/Mat;)V
    .locals 12

    move-object v0, p0

    .line 1717
    iget-wide v0, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object v2, p1

    iget-wide v2, v2, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v4, p8

    iget-wide v10, v4, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v4, p2

    move-wide/from16 v6, p4

    move/from16 v8, p6

    move/from16 v9, p7

    invoke-static/range {v0 .. v11}, Lorg/opencv/core/Core;->normalize_0(JJDDIIJ)V

    return-void
.end method

.method private static native normalize_0(JJDDIIJ)V
.end method

.method private static native normalize_1(JJDDII)V
.end method

.method private static native normalize_2(JJDDI)V
.end method

.method private static native normalize_3(JJ)V
.end method

.method public static patchNaNs(Lorg/opencv/core/Mat;)V
    .locals 2

    .line 1767
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1}, Lorg/opencv/core/Core;->patchNaNs_1(J)V

    return-void
.end method

.method public static patchNaNs(Lorg/opencv/core/Mat;D)V
    .locals 2

    .line 1758
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p1, p2}, Lorg/opencv/core/Core;->patchNaNs_0(JD)V

    return-void
.end method

.method private static native patchNaNs_0(JD)V
.end method

.method private static native patchNaNs_1(J)V
.end method

.method public static perspectiveTransform(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 6

    .line 1781
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->perspectiveTransform_0(JJJ)V

    return-void
.end method

.method private static native perspectiveTransform_0(JJJ)V
.end method

.method public static phase(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 6

    .line 1804
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->phase_1(JJJ)V

    return-void
.end method

.method public static phase(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Z)V
    .locals 7

    .line 1795
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    move v6, p3

    invoke-static/range {v0 .. v6}, Lorg/opencv/core/Core;->phase_0(JJJZ)V

    return-void
.end method

.method private static native phase_0(JJJZ)V
.end method

.method private static native phase_1(JJJ)V
.end method

.method public static polarToCart(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 8

    .line 1827
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p3, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v7}, Lorg/opencv/core/Core;->polarToCart_1(JJJJ)V

    return-void
.end method

.method public static polarToCart(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Z)V
    .locals 9

    .line 1818
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p3, Lorg/opencv/core/Mat;->nativeObj:J

    move v8, p4

    invoke-static/range {v0 .. v8}, Lorg/opencv/core/Core;->polarToCart_0(JJJJZ)V

    return-void
.end method

.method private static native polarToCart_0(JJJJZ)V
.end method

.method private static native polarToCart_1(JJJJ)V
.end method

.method public static pow(Lorg/opencv/core/Mat;DLorg/opencv/core/Mat;)V
    .locals 6

    .line 1841
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p3, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v2, p1

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->pow_0(JDJ)V

    return-void
.end method

.method private static native pow_0(JDJ)V
.end method

.method public static randShuffle(Lorg/opencv/core/Mat;)V
    .locals 2

    .line 1864
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1}, Lorg/opencv/core/Core;->randShuffle_1(J)V

    return-void
.end method

.method public static randShuffle(Lorg/opencv/core/Mat;D)V
    .locals 2

    .line 1855
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p1, p2}, Lorg/opencv/core/Core;->randShuffle_0(JD)V

    return-void
.end method

.method private static native randShuffle_0(JD)V
.end method

.method private static native randShuffle_1(J)V
.end method

.method public static randn(Lorg/opencv/core/Mat;DD)V
    .locals 6

    .line 1878
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v2, p1

    move-wide v4, p3

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->randn_0(JDD)V

    return-void
.end method

.method private static native randn_0(JDD)V
.end method

.method public static randu(Lorg/opencv/core/Mat;DD)V
    .locals 6

    .line 1892
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v2, p1

    move-wide v4, p3

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->randu_0(JDD)V

    return-void
.end method

.method private static native randu_0(JDD)V
.end method

.method public static reduce(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;II)V
    .locals 6

    .line 1915
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->reduce_1(JJII)V

    return-void
.end method

.method public static reduce(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;III)V
    .locals 7

    .line 1906
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    move v4, p2

    move v5, p3

    move v6, p4

    invoke-static/range {v0 .. v6}, Lorg/opencv/core/Core;->reduce_0(JJIII)V

    return-void
.end method

.method private static native reduce_0(JJIII)V
.end method

.method private static native reduce_1(JJII)V
.end method

.method public static repeat(Lorg/opencv/core/Mat;IILorg/opencv/core/Mat;)V
    .locals 6

    .line 1929
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p3, Lorg/opencv/core/Mat;->nativeObj:J

    move v2, p1

    move v3, p2

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->repeat_0(JIIJ)V

    return-void
.end method

.method private static native repeat_0(JIIJ)V
.end method

.method public static scaleAdd(Lorg/opencv/core/Mat;DLorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 8

    .line 1943
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p3, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p4, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v2, p1

    invoke-static/range {v0 .. v7}, Lorg/opencv/core/Core;->scaleAdd_0(JDJJ)V

    return-void
.end method

.method private static native scaleAdd_0(JDJJ)V
.end method

.method public static setErrorVerbosity(Z)V
    .locals 0

    .line 1957
    invoke-static {p0}, Lorg/opencv/core/Core;->setErrorVerbosity_0(Z)V

    return-void
.end method

.method private static native setErrorVerbosity_0(Z)V
.end method

.method public static setIdentity(Lorg/opencv/core/Mat;)V
    .locals 2

    .line 1980
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1}, Lorg/opencv/core/Core;->setIdentity_1(J)V

    return-void
.end method

.method public static setIdentity(Lorg/opencv/core/Mat;Lorg/opencv/core/Scalar;)V
    .locals 10

    .line 1971
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-object p0, p1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v2, 0x0

    aget-wide v2, p0, v2

    iget-object p0, p1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v4, 0x1

    aget-wide v4, p0, v4

    iget-object p0, p1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v6, 0x2

    aget-wide v6, p0, v6

    iget-object p0, p1, Lorg/opencv/core/Scalar;->val:[D

    const/4 p1, 0x3

    aget-wide v8, p0, p1

    invoke-static/range {v0 .. v9}, Lorg/opencv/core/Core;->setIdentity_0(JDDDD)V

    return-void
.end method

.method private static native setIdentity_0(JDDDD)V
.end method

.method private static native setIdentity_1(J)V
.end method

.method public static setNumThreads(I)V
    .locals 0

    .line 1994
    invoke-static {p0}, Lorg/opencv/core/Core;->setNumThreads_0(I)V

    return-void
.end method

.method private static native setNumThreads_0(I)V
.end method

.method public static solve(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)Z
    .locals 6

    .line 304
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->solve_1(JJJ)Z

    move-result p0

    return p0
.end method

.method public static solve(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)Z
    .locals 7

    .line 295
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    move v6, p3

    invoke-static/range {v0 .. v6}, Lorg/opencv/core/Core;->solve_0(JJJI)Z

    move-result p0

    return p0
.end method

.method public static solveCubic(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)I
    .locals 2

    .line 619
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1}, Lorg/opencv/core/Core;->solveCubic_0(JJ)I

    move-result p0

    return p0
.end method

.method private static native solveCubic_0(JJ)I
.end method

.method public static solvePoly(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)D
    .locals 2

    .line 493
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1}, Lorg/opencv/core/Core;->solvePoly_1(JJ)D

    move-result-wide p0

    return-wide p0
.end method

.method public static solvePoly(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)D
    .locals 2

    .line 484
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1, p2}, Lorg/opencv/core/Core;->solvePoly_0(JJI)D

    move-result-wide p0

    return-wide p0
.end method

.method private static native solvePoly_0(JJI)D
.end method

.method private static native solvePoly_1(JJ)D
.end method

.method private static native solve_0(JJJI)Z
.end method

.method private static native solve_1(JJJ)Z
.end method

.method public static sort(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V
    .locals 2

    .line 2008
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1, p2}, Lorg/opencv/core/Core;->sort_0(JJI)V

    return-void
.end method

.method public static sortIdx(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V
    .locals 2

    .line 2022
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1, p2}, Lorg/opencv/core/Core;->sortIdx_0(JJI)V

    return-void
.end method

.method private static native sortIdx_0(JJI)V
.end method

.method private static native sort_0(JJI)V
.end method

.method public static split(Lorg/opencv/core/Mat;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List<",
            "Lorg/opencv/core/Mat;",
            ">;)V"
        }
    .end annotation

    .line 2035
    new-instance v0, Lorg/opencv/core/Mat;

    invoke-direct {v0}, Lorg/opencv/core/Mat;-><init>()V

    .line 2036
    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v3, v0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2, v3, v4}, Lorg/opencv/core/Core;->split_0(JJ)V

    .line 2037
    invoke-static {v0, p1}, Lorg/opencv/utils/Converters;->Mat_to_vector_Mat(Lorg/opencv/core/Mat;Ljava/util/List;)V

    .line 2038
    invoke-virtual {v0}, Lorg/opencv/core/Mat;->release()V

    return-void
.end method

.method private static native split_0(JJ)V
.end method

.method public static sqrt(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 2

    .line 2051
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1}, Lorg/opencv/core/Core;->sqrt_0(JJ)V

    return-void
.end method

.method private static native sqrt_0(JJ)V
.end method

.method public static subtract(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 6

    .line 2083
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->subtract_2(JJJ)V

    return-void
.end method

.method public static subtract(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 8

    .line 2074
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p3, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v7}, Lorg/opencv/core/Core;->subtract_1(JJJJ)V

    return-void
.end method

.method public static subtract(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V
    .locals 9

    .line 2065
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p3, Lorg/opencv/core/Mat;->nativeObj:J

    move v8, p4

    invoke-static/range {v0 .. v8}, Lorg/opencv/core/Core;->subtract_0(JJJJI)V

    return-void
.end method

.method public static subtract(Lorg/opencv/core/Mat;Lorg/opencv/core/Scalar;Lorg/opencv/core/Mat;)V
    .locals 12

    .line 2115
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-object p0, p1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v2, 0x0

    aget-wide v2, p0, v2

    iget-object p0, p1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v4, 0x1

    aget-wide v4, p0, v4

    iget-object p0, p1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v6, 0x2

    aget-wide v6, p0, v6

    iget-object p0, p1, Lorg/opencv/core/Scalar;->val:[D

    const/4 p1, 0x3

    aget-wide v8, p0, p1

    iget-wide v10, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v11}, Lorg/opencv/core/Core;->subtract_5(JDDDDJ)V

    return-void
.end method

.method public static subtract(Lorg/opencv/core/Mat;Lorg/opencv/core/Scalar;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 16

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    .line 2106
    iget-wide v1, v1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-object v3, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v4, 0x0

    aget-wide v4, v3, v4

    iget-object v3, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v6, 0x1

    aget-wide v6, v3, v6

    iget-object v3, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v8, 0x2

    aget-wide v8, v3, v8

    iget-object v0, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v3, 0x3

    aget-wide v10, v0, v3

    move-object/from16 v0, p2

    iget-wide v12, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p3

    iget-wide v14, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v0, v1

    move-wide v2, v4

    move-wide v4, v6

    move-wide v6, v8

    move-wide v8, v10

    move-wide v10, v12

    move-wide v12, v14

    invoke-static/range {v0 .. v13}, Lorg/opencv/core/Core;->subtract_4(JDDDDJJ)V

    return-void
.end method

.method public static subtract(Lorg/opencv/core/Mat;Lorg/opencv/core/Scalar;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V
    .locals 16

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    .line 2097
    iget-wide v1, v1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-object v3, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v4, 0x0

    aget-wide v4, v3, v4

    iget-object v3, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v6, 0x1

    aget-wide v6, v3, v6

    iget-object v3, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v8, 0x2

    aget-wide v8, v3, v8

    iget-object v0, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v3, 0x3

    aget-wide v10, v0, v3

    move-object/from16 v0, p2

    iget-wide v12, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p3

    iget-wide v14, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v0, v1

    move-wide v2, v4

    move-wide v4, v6

    move-wide v6, v8

    move-wide v8, v10

    move-wide v10, v12

    move-wide v12, v14

    move/from16 v14, p4

    invoke-static/range {v0 .. v14}, Lorg/opencv/core/Core;->subtract_3(JDDDDJJI)V

    return-void
.end method

.method private static native subtract_0(JJJJI)V
.end method

.method private static native subtract_1(JJJJ)V
.end method

.method private static native subtract_2(JJJ)V
.end method

.method private static native subtract_3(JDDDDJJI)V
.end method

.method private static native subtract_4(JDDDDJJ)V
.end method

.method private static native subtract_5(JDDDDJ)V
.end method

.method public static sumElems(Lorg/opencv/core/Mat;)Lorg/opencv/core/Scalar;
    .locals 3

    .line 207
    new-instance v0, Lorg/opencv/core/Scalar;

    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2}, Lorg/opencv/core/Core;->sumElems_0(J)[D

    move-result-object p0

    invoke-direct {v0, p0}, Lorg/opencv/core/Scalar;-><init>([D)V

    return-object v0
.end method

.method private static native sumElems_0(J)[D
.end method

.method public static trace(Lorg/opencv/core/Mat;)Lorg/opencv/core/Scalar;
    .locals 3

    .line 221
    new-instance v0, Lorg/opencv/core/Scalar;

    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2}, Lorg/opencv/core/Core;->trace_0(J)[D

    move-result-object p0

    invoke-direct {v0, p0}, Lorg/opencv/core/Scalar;-><init>([D)V

    return-object v0
.end method

.method private static native trace_0(J)[D
.end method

.method public static transform(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 6

    .line 2129
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->transform_0(JJJ)V

    return-void
.end method

.method private static native transform_0(JJJ)V
.end method

.method public static transpose(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 2

    .line 2143
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1}, Lorg/opencv/core/Core;->transpose_0(JJ)V

    return-void
.end method

.method private static native transpose_0(JJ)V
.end method

.method public static vconcat(Ljava/util/List;Lorg/opencv/core/Mat;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/opencv/core/Mat;",
            ">;",
            "Lorg/opencv/core/Mat;",
            ")V"
        }
    .end annotation

    .line 2156
    invoke-static {p0}, Lorg/opencv/utils/Converters;->vector_Mat_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;

    move-result-object p0

    .line 2157
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1}, Lorg/opencv/core/Core;->vconcat_0(JJ)V

    return-void
.end method

.method private static native vconcat_0(JJ)V
.end method

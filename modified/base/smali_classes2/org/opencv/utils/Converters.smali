.class public Lorg/opencv/utils/Converters;
.super Ljava/lang/Object;
.source "Converters.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static Mat_to_vector_DMatch(Lorg/opencv/core/Mat;Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List<",
            "Lorg/opencv/core/DMatch;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_2

    .line 660
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->rows()I

    move-result v0

    .line 661
    sget v1, Lorg/opencv/core/CvType;->CV_64FC4:I

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->type()I

    move-result v2

    if-ne v1, v2, :cond_1

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->cols()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 665
    invoke-interface {p1}, Ljava/util/List;->clear()V

    mul-int/lit8 v1, v0, 0x4

    .line 666
    new-array v1, v1, [D

    const/4 v2, 0x0

    .line 667
    invoke-virtual {p0, v2, v2, v1}, Lorg/opencv/core/Mat;->get(II[D)I

    :goto_0
    if-ge v2, v0, :cond_0

    .line 669
    new-instance p0, Lorg/opencv/core/DMatch;

    mul-int/lit8 v3, v2, 0x4

    aget-wide v4, v1, v3

    double-to-int v4, v4

    add-int/lit8 v5, v3, 0x1

    aget-wide v5, v1, v5

    double-to-int v5, v5

    add-int/lit8 v6, v3, 0x2

    aget-wide v6, v1, v6

    double-to-int v6, v6

    add-int/lit8 v3, v3, 0x3

    aget-wide v7, v1, v3

    double-to-float v3, v7

    invoke-direct {p0, v4, v5, v6, v3}, Lorg/opencv/core/DMatch;-><init>(IIIF)V

    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void

    .line 662
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CvType.CV_64FC4 != m.type() ||  m.cols()!=1\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 659
    :cond_2
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Output List can\'t be null"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static Mat_to_vector_KeyPoint(Lorg/opencv/core/Mat;Ljava/util/List;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List<",
            "Lorg/opencv/core/KeyPoint;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_2

    .line 464
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->rows()I

    move-result v0

    const/4 v1, 0x7

    .line 465
    invoke-static {v1}, Lorg/opencv/core/CvType;->CV_64FC(I)I

    move-result v1

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->type()I

    move-result v2

    if-ne v1, v2, :cond_1

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->cols()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 469
    invoke-interface {p1}, Ljava/util/List;->clear()V

    mul-int/lit8 v1, v0, 0x7

    .line 470
    new-array v1, v1, [D

    const/4 v2, 0x0

    .line 471
    invoke-virtual {p0, v2, v2, v1}, Lorg/opencv/core/Mat;->get(II[D)I

    :goto_0
    if-ge v2, v0, :cond_0

    .line 473
    new-instance p0, Lorg/opencv/core/KeyPoint;

    mul-int/lit8 v3, v2, 0x7

    aget-wide v4, v1, v3

    double-to-float v4, v4

    add-int/lit8 v5, v3, 0x1

    aget-wide v5, v1, v5

    double-to-float v5, v5

    add-int/lit8 v6, v3, 0x2

    aget-wide v6, v1, v6

    double-to-float v6, v6

    add-int/lit8 v7, v3, 0x3

    aget-wide v7, v1, v7

    double-to-float v7, v7

    add-int/lit8 v8, v3, 0x4

    aget-wide v8, v1, v8

    double-to-float v8, v8

    add-int/lit8 v9, v3, 0x5

    aget-wide v9, v1, v9

    double-to-int v9, v9

    add-int/lit8 v3, v3, 0x6

    aget-wide v10, v1, v3

    double-to-int v10, v10

    move-object v3, p0

    invoke-direct/range {v3 .. v10}, Lorg/opencv/core/KeyPoint;-><init>(FFFFFII)V

    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void

    .line 466
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CvType.CV_64FC(7) != m.type() ||  m.cols()!=1\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 463
    :cond_2
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Output List can\'t be null"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static Mat_to_vector_Mat(Lorg/opencv/core/Mat;Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List<",
            "Lorg/opencv/core/Mat;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_2

    .line 256
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->rows()I

    move-result v0

    .line 257
    sget v1, Lorg/opencv/core/CvType;->CV_32SC2:I

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->type()I

    move-result v2

    if-ne v1, v2, :cond_1

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->cols()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 261
    invoke-interface {p1}, Ljava/util/List;->clear()V

    mul-int/lit8 v1, v0, 0x2

    .line 262
    new-array v1, v1, [I

    const/4 v3, 0x0

    .line 263
    invoke-virtual {p0, v3, v3, v1}, Lorg/opencv/core/Mat;->get(II[I)I

    :goto_0
    if-ge v3, v0, :cond_0

    mul-int/lit8 p0, v3, 0x2

    .line 265
    aget v4, v1, p0

    int-to-long v4, v4

    const/16 v6, 0x20

    shl-long/2addr v4, v6

    add-int/2addr p0, v2

    aget p0, v1, p0

    int-to-long v6, p0

    const-wide v8, 0xffffffffL

    and-long/2addr v6, v8

    or-long/2addr v4, v6

    .line 266
    new-instance p0, Lorg/opencv/core/Mat;

    invoke-direct {p0, v4, v5}, Lorg/opencv/core/Mat;-><init>(J)V

    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    return-void

    .line 258
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CvType.CV_32SC2 != m.type() ||  m.cols()!=1\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 255
    :cond_2
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "mats == null"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static Mat_to_vector_Point(Lorg/opencv/core/Mat;Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List<",
            "Lorg/opencv/core/Point;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_5

    .line 160
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->rows()I

    move-result v0

    .line 161
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->type()I

    move-result v1

    .line 162
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->cols()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_4

    .line 165
    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 166
    sget v2, Lorg/opencv/core/CvType;->CV_32SC2:I

    const/4 v4, 0x0

    if-ne v1, v2, :cond_0

    mul-int/lit8 v1, v0, 0x2

    .line 167
    new-array v1, v1, [I

    .line 168
    invoke-virtual {p0, v4, v4, v1}, Lorg/opencv/core/Mat;->get(II[I)I

    :goto_0
    if-ge v4, v0, :cond_2

    .line 170
    new-instance p0, Lorg/opencv/core/Point;

    mul-int/lit8 v2, v4, 0x2

    aget v5, v1, v2

    int-to-double v5, v5

    add-int/2addr v2, v3

    aget v2, v1, v2

    int-to-double v7, v2

    invoke-direct {p0, v5, v6, v7, v8}, Lorg/opencv/core/Point;-><init>(DD)V

    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 172
    :cond_0
    sget v2, Lorg/opencv/core/CvType;->CV_32FC2:I

    if-ne v1, v2, :cond_1

    mul-int/lit8 v1, v0, 0x2

    .line 173
    new-array v1, v1, [F

    .line 174
    invoke-virtual {p0, v4, v4, v1}, Lorg/opencv/core/Mat;->get(II[F)I

    :goto_1
    if-ge v4, v0, :cond_2

    .line 176
    new-instance p0, Lorg/opencv/core/Point;

    mul-int/lit8 v2, v4, 0x2

    aget v5, v1, v2

    float-to-double v5, v5

    add-int/2addr v2, v3

    aget v2, v1, v2

    float-to-double v7, v2

    invoke-direct {p0, v5, v6, v7, v8}, Lorg/opencv/core/Point;-><init>(DD)V

    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 178
    :cond_1
    sget v2, Lorg/opencv/core/CvType;->CV_64FC2:I

    if-ne v1, v2, :cond_3

    mul-int/lit8 v1, v0, 0x2

    .line 179
    new-array v1, v1, [D

    .line 180
    invoke-virtual {p0, v4, v4, v1}, Lorg/opencv/core/Mat;->get(II[D)I

    :goto_2
    if-ge v4, v0, :cond_2

    .line 182
    new-instance p0, Lorg/opencv/core/Point;

    mul-int/lit8 v2, v4, 0x2

    aget-wide v5, v1, v2

    add-int/2addr v2, v3

    aget-wide v7, v1, v2

    invoke-direct {p0, v5, v6, v7, v8}, Lorg/opencv/core/Point;-><init>(DD)V

    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_2
    return-void

    .line 185
    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Input Mat should be of CV_32SC2, CV_32FC2 or CV_64FC2 type\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 163
    :cond_4
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Input Mat should have one column\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 159
    :cond_5
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Output List can\'t be null"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static Mat_to_vector_Point2d(Lorg/opencv/core/Mat;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List<",
            "Lorg/opencv/core/Point;",
            ">;)V"
        }
    .end annotation

    .line 154
    invoke-static {p0, p1}, Lorg/opencv/utils/Converters;->Mat_to_vector_Point(Lorg/opencv/core/Mat;Ljava/util/List;)V

    return-void
.end method

.method public static Mat_to_vector_Point2f(Lorg/opencv/core/Mat;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List<",
            "Lorg/opencv/core/Point;",
            ">;)V"
        }
    .end annotation

    .line 150
    invoke-static {p0, p1}, Lorg/opencv/utils/Converters;->Mat_to_vector_Point(Lorg/opencv/core/Mat;Ljava/util/List;)V

    return-void
.end method

.method public static Mat_to_vector_Point3(Lorg/opencv/core/Mat;Ljava/util/List;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List<",
            "Lorg/opencv/core/Point3;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_5

    .line 205
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->rows()I

    move-result v0

    .line 206
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->type()I

    move-result v1

    .line 207
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->cols()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_4

    .line 210
    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 211
    sget v2, Lorg/opencv/core/CvType;->CV_32SC3:I

    const/4 v3, 0x0

    if-ne v1, v2, :cond_0

    mul-int/lit8 v1, v0, 0x3

    .line 212
    new-array v1, v1, [I

    .line 213
    invoke-virtual {p0, v3, v3, v1}, Lorg/opencv/core/Mat;->get(II[I)I

    :goto_0
    if-ge v3, v0, :cond_2

    .line 215
    new-instance p0, Lorg/opencv/core/Point3;

    mul-int/lit8 v2, v3, 0x3

    aget v4, v1, v2

    int-to-double v5, v4

    add-int/lit8 v4, v2, 0x1

    aget v4, v1, v4

    int-to-double v7, v4

    add-int/lit8 v2, v2, 0x2

    aget v2, v1, v2

    int-to-double v9, v2

    move-object v4, p0

    invoke-direct/range {v4 .. v10}, Lorg/opencv/core/Point3;-><init>(DDD)V

    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 217
    :cond_0
    sget v2, Lorg/opencv/core/CvType;->CV_32FC3:I

    if-ne v1, v2, :cond_1

    mul-int/lit8 v1, v0, 0x3

    .line 218
    new-array v1, v1, [F

    .line 219
    invoke-virtual {p0, v3, v3, v1}, Lorg/opencv/core/Mat;->get(II[F)I

    :goto_1
    if-ge v3, v0, :cond_2

    .line 221
    new-instance p0, Lorg/opencv/core/Point3;

    mul-int/lit8 v2, v3, 0x3

    aget v4, v1, v2

    float-to-double v5, v4

    add-int/lit8 v4, v2, 0x1

    aget v4, v1, v4

    float-to-double v7, v4

    add-int/lit8 v2, v2, 0x2

    aget v2, v1, v2

    float-to-double v9, v2

    move-object v4, p0

    invoke-direct/range {v4 .. v10}, Lorg/opencv/core/Point3;-><init>(DDD)V

    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 223
    :cond_1
    sget v2, Lorg/opencv/core/CvType;->CV_64FC3:I

    if-ne v1, v2, :cond_3

    mul-int/lit8 v1, v0, 0x3

    .line 224
    new-array v1, v1, [D

    .line 225
    invoke-virtual {p0, v3, v3, v1}, Lorg/opencv/core/Mat;->get(II[D)I

    :goto_2
    if-ge v3, v0, :cond_2

    .line 227
    new-instance p0, Lorg/opencv/core/Point3;

    mul-int/lit8 v2, v3, 0x3

    aget-wide v5, v1, v2

    add-int/lit8 v4, v2, 0x1

    aget-wide v7, v1, v4

    add-int/lit8 v2, v2, 0x2

    aget-wide v9, v1, v2

    move-object v4, p0

    invoke-direct/range {v4 .. v10}, Lorg/opencv/core/Point3;-><init>(DDD)V

    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_2
    return-void

    .line 230
    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Input Mat should be of CV_32SC3, CV_32FC3 or CV_64FC3 type\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 208
    :cond_4
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Input Mat should have one column\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 204
    :cond_5
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Output List can\'t be null"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static Mat_to_vector_Point3d(Lorg/opencv/core/Mat;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List<",
            "Lorg/opencv/core/Point3;",
            ">;)V"
        }
    .end annotation

    .line 199
    invoke-static {p0, p1}, Lorg/opencv/utils/Converters;->Mat_to_vector_Point3(Lorg/opencv/core/Mat;Ljava/util/List;)V

    return-void
.end method

.method public static Mat_to_vector_Point3f(Lorg/opencv/core/Mat;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List<",
            "Lorg/opencv/core/Point3;",
            ">;)V"
        }
    .end annotation

    .line 195
    invoke-static {p0, p1}, Lorg/opencv/utils/Converters;->Mat_to_vector_Point3(Lorg/opencv/core/Mat;Ljava/util/List;)V

    return-void
.end method

.method public static Mat_to_vector_Point3i(Lorg/opencv/core/Mat;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List<",
            "Lorg/opencv/core/Point3;",
            ">;)V"
        }
    .end annotation

    .line 191
    invoke-static {p0, p1}, Lorg/opencv/utils/Converters;->Mat_to_vector_Point3(Lorg/opencv/core/Mat;Ljava/util/List;)V

    return-void
.end method

.method public static Mat_to_vector_Rect(Lorg/opencv/core/Mat;Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List<",
            "Lorg/opencv/core/Rect;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_2

    .line 425
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->rows()I

    move-result v0

    .line 426
    sget v1, Lorg/opencv/core/CvType;->CV_32SC4:I

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->type()I

    move-result v2

    if-ne v1, v2, :cond_1

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->cols()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 430
    invoke-interface {p1}, Ljava/util/List;->clear()V

    mul-int/lit8 v1, v0, 0x4

    .line 431
    new-array v1, v1, [I

    const/4 v2, 0x0

    .line 432
    invoke-virtual {p0, v2, v2, v1}, Lorg/opencv/core/Mat;->get(II[I)I

    :goto_0
    if-ge v2, v0, :cond_0

    .line 434
    new-instance p0, Lorg/opencv/core/Rect;

    mul-int/lit8 v3, v2, 0x4

    aget v4, v1, v3

    add-int/lit8 v5, v3, 0x1

    aget v5, v1, v5

    add-int/lit8 v6, v3, 0x2

    aget v6, v1, v6

    add-int/lit8 v3, v3, 0x3

    aget v3, v1, v3

    invoke-direct {p0, v4, v5, v6, v3}, Lorg/opencv/core/Rect;-><init>(IIII)V

    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void

    .line 427
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CvType.CV_32SC4 != m.type() ||  m.rows()!=1\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 424
    :cond_2
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "rs == null"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static Mat_to_vector_char(Lorg/opencv/core/Mat;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List<",
            "Ljava/lang/Byte;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_2

    .line 389
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->rows()I

    move-result v0

    .line 390
    sget v1, Lorg/opencv/core/CvType;->CV_8SC1:I

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->type()I

    move-result v2

    if-ne v1, v2, :cond_1

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->cols()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 394
    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 395
    new-array v1, v0, [B

    const/4 v2, 0x0

    .line 396
    invoke-virtual {p0, v2, v2, v1}, Lorg/opencv/core/Mat;->get(II[B)I

    :goto_0
    if-ge v2, v0, :cond_0

    .line 398
    aget-byte p0, v1, v2

    invoke-static {p0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object p0

    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void

    .line 391
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CvType.CV_8SC1 != m.type() ||  m.cols()!=1\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 388
    :cond_2
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Output List can\'t be null"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static Mat_to_vector_double(Lorg/opencv/core/Mat;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List<",
            "Ljava/lang/Double;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_2

    .line 624
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->rows()I

    move-result v0

    .line 625
    sget v1, Lorg/opencv/core/CvType;->CV_64FC1:I

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->type()I

    move-result v2

    if-ne v1, v2, :cond_1

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->cols()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 629
    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 630
    new-array v1, v0, [D

    const/4 v2, 0x0

    .line 631
    invoke-virtual {p0, v2, v2, v1}, Lorg/opencv/core/Mat;->get(II[D)I

    :goto_0
    if-ge v2, v0, :cond_0

    .line 633
    aget-wide v3, v1, v2

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p0

    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void

    .line 626
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CvType.CV_64FC1 != m.type() ||  m.cols()!=1\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 623
    :cond_2
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "ds == null"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static Mat_to_vector_float(Lorg/opencv/core/Mat;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List<",
            "Ljava/lang/Float;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_2

    .line 290
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->rows()I

    move-result v0

    .line 291
    sget v1, Lorg/opencv/core/CvType;->CV_32FC1:I

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->type()I

    move-result v2

    if-ne v1, v2, :cond_1

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->cols()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 295
    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 296
    new-array v1, v0, [F

    const/4 v2, 0x0

    .line 297
    invoke-virtual {p0, v2, v2, v1}, Lorg/opencv/core/Mat;->get(II[F)I

    :goto_0
    if-ge v2, v0, :cond_0

    .line 299
    aget p0, v1, v2

    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p0

    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void

    .line 292
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CvType.CV_32FC1 != m.type() ||  m.cols()!=1\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 289
    :cond_2
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "fs == null"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static Mat_to_vector_int(Lorg/opencv/core/Mat;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_2

    .line 373
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->rows()I

    move-result v0

    .line 374
    sget v1, Lorg/opencv/core/CvType;->CV_32SC1:I

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->type()I

    move-result v2

    if-ne v1, v2, :cond_1

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->cols()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 378
    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 379
    new-array v1, v0, [I

    const/4 v2, 0x0

    .line 380
    invoke-virtual {p0, v2, v2, v1}, Lorg/opencv/core/Mat;->get(II[I)I

    :goto_0
    if-ge v2, v0, :cond_0

    .line 382
    aget p0, v1, v2

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void

    .line 375
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CvType.CV_32SC1 != m.type() ||  m.cols()!=1\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 372
    :cond_2
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "is == null"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static Mat_to_vector_uchar(Lorg/opencv/core/Mat;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List<",
            "Ljava/lang/Byte;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_2

    .line 323
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->rows()I

    move-result v0

    .line 324
    sget v1, Lorg/opencv/core/CvType;->CV_8UC1:I

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->type()I

    move-result v2

    if-ne v1, v2, :cond_1

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->cols()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 328
    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 329
    new-array v1, v0, [B

    const/4 v2, 0x0

    .line 330
    invoke-virtual {p0, v2, v2, v1}, Lorg/opencv/core/Mat;->get(II[B)I

    :goto_0
    if-ge v2, v0, :cond_0

    .line 332
    aget-byte p0, v1, v2

    invoke-static {p0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object p0

    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void

    .line 325
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CvType.CV_8UC1 != m.type() ||  m.cols()!=1\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 322
    :cond_2
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Output List can\'t be null"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static Mat_to_vector_vector_DMatch(Lorg/opencv/core/Mat;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List<",
            "Lorg/opencv/core/MatOfDMatch;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_2

    if-eqz p0, :cond_1

    .line 694
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->rows()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 695
    invoke-static {p0, v0}, Lorg/opencv/utils/Converters;->Mat_to_vector_Mat(Lorg/opencv/core/Mat;Ljava/util/List;)V

    .line 696
    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 697
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/opencv/core/Mat;

    .line 698
    new-instance v2, Lorg/opencv/core/MatOfDMatch;

    invoke-direct {v2, v1}, Lorg/opencv/core/MatOfDMatch;-><init>(Lorg/opencv/core/Mat;)V

    .line 699
    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 700
    invoke-virtual {v1}, Lorg/opencv/core/Mat;->release()V

    goto :goto_0

    .line 702
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void

    .line 692
    :cond_1
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Input Mat can\'t be null"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 689
    :cond_2
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Output List can\'t be null"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static Mat_to_vector_vector_KeyPoint(Lorg/opencv/core/Mat;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List<",
            "Lorg/opencv/core/MatOfKeyPoint;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_2

    if-eqz p0, :cond_1

    .line 594
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->rows()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 595
    invoke-static {p0, v0}, Lorg/opencv/utils/Converters;->Mat_to_vector_Mat(Lorg/opencv/core/Mat;Ljava/util/List;)V

    .line 596
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/opencv/core/Mat;

    .line 597
    new-instance v2, Lorg/opencv/core/MatOfKeyPoint;

    invoke-direct {v2, v1}, Lorg/opencv/core/MatOfKeyPoint;-><init>(Lorg/opencv/core/Mat;)V

    .line 598
    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 599
    invoke-virtual {v1}, Lorg/opencv/core/Mat;->release()V

    goto :goto_0

    .line 601
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void

    .line 592
    :cond_1
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Input Mat can\'t be null"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 589
    :cond_2
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Output List can\'t be null"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static Mat_to_vector_vector_Point(Lorg/opencv/core/Mat;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List<",
            "Lorg/opencv/core/MatOfPoint;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_2

    if-eqz p0, :cond_1

    .line 499
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->rows()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 500
    invoke-static {p0, v0}, Lorg/opencv/utils/Converters;->Mat_to_vector_Mat(Lorg/opencv/core/Mat;Ljava/util/List;)V

    .line 501
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/opencv/core/Mat;

    .line 502
    new-instance v2, Lorg/opencv/core/MatOfPoint;

    invoke-direct {v2, v1}, Lorg/opencv/core/MatOfPoint;-><init>(Lorg/opencv/core/Mat;)V

    .line 503
    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 504
    invoke-virtual {v1}, Lorg/opencv/core/Mat;->release()V

    goto :goto_0

    .line 506
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void

    .line 497
    :cond_1
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Input Mat can\'t be null"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 494
    :cond_2
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Output List can\'t be null"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static Mat_to_vector_vector_Point2f(Lorg/opencv/core/Mat;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List<",
            "Lorg/opencv/core/MatOfPoint2f;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_2

    if-eqz p0, :cond_1

    .line 517
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->rows()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 518
    invoke-static {p0, v0}, Lorg/opencv/utils/Converters;->Mat_to_vector_Mat(Lorg/opencv/core/Mat;Ljava/util/List;)V

    .line 519
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/opencv/core/Mat;

    .line 520
    new-instance v2, Lorg/opencv/core/MatOfPoint2f;

    invoke-direct {v2, v1}, Lorg/opencv/core/MatOfPoint2f;-><init>(Lorg/opencv/core/Mat;)V

    .line 521
    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 522
    invoke-virtual {v1}, Lorg/opencv/core/Mat;->release()V

    goto :goto_0

    .line 524
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void

    .line 515
    :cond_1
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Input Mat can\'t be null"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 512
    :cond_2
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Output List can\'t be null"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static Mat_to_vector_vector_Point3f(Lorg/opencv/core/Mat;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List<",
            "Lorg/opencv/core/MatOfPoint3f;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_2

    if-eqz p0, :cond_1

    .line 549
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->rows()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 550
    invoke-static {p0, v0}, Lorg/opencv/utils/Converters;->Mat_to_vector_Mat(Lorg/opencv/core/Mat;Ljava/util/List;)V

    .line 551
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/opencv/core/Mat;

    .line 552
    new-instance v2, Lorg/opencv/core/MatOfPoint3f;

    invoke-direct {v2, v1}, Lorg/opencv/core/MatOfPoint3f;-><init>(Lorg/opencv/core/Mat;)V

    .line 553
    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 554
    invoke-virtual {v1}, Lorg/opencv/core/Mat;->release()V

    goto :goto_0

    .line 556
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void

    .line 547
    :cond_1
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Input Mat can\'t be null"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 544
    :cond_2
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Output List can\'t be null"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static Mat_to_vector_vector_char(Lorg/opencv/core/Mat;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List<",
            "Ljava/util/List<",
            "Ljava/lang/Byte;",
            ">;>;)V"
        }
    .end annotation

    if-eqz p1, :cond_2

    if-eqz p0, :cond_1

    .line 726
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->rows()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 727
    invoke-static {p0, v0}, Lorg/opencv/utils/Converters;->Mat_to_vector_Mat(Lorg/opencv/core/Mat;Ljava/util/List;)V

    .line 728
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/opencv/core/Mat;

    .line 729
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 730
    invoke-static {v1, v2}, Lorg/opencv/utils/Converters;->Mat_to_vector_char(Lorg/opencv/core/Mat;Ljava/util/List;)V

    .line 731
    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 732
    invoke-virtual {v1}, Lorg/opencv/core/Mat;->release()V

    goto :goto_0

    .line 734
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void

    .line 724
    :cond_1
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Input Mat can\'t be null"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 721
    :cond_2
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Output List can\'t be null"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static vector_DMatch_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/opencv/core/DMatch;",
            ">;)",
            "Lorg/opencv/core/Mat;"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    .line 639
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-lez v1, :cond_2

    .line 641
    new-instance v2, Lorg/opencv/core/Mat;

    sget v3, Lorg/opencv/core/CvType;->CV_64FC4:I

    const/4 v4, 0x1

    invoke-direct {v2, v1, v4, v3}, Lorg/opencv/core/Mat;-><init>(III)V

    mul-int/lit8 v3, v1, 0x4

    .line 642
    new-array v3, v3, [D

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v1, :cond_1

    .line 644
    invoke-interface {p0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/opencv/core/DMatch;

    mul-int/lit8 v6, v4, 0x4

    .line 645
    iget v7, v5, Lorg/opencv/core/DMatch;->queryIdx:I

    int-to-double v7, v7

    aput-wide v7, v3, v6

    add-int/lit8 v7, v6, 0x1

    .line 646
    iget v8, v5, Lorg/opencv/core/DMatch;->trainIdx:I

    int-to-double v8, v8

    aput-wide v8, v3, v7

    add-int/lit8 v7, v6, 0x2

    .line 647
    iget v8, v5, Lorg/opencv/core/DMatch;->imgIdx:I

    int-to-double v8, v8

    aput-wide v8, v3, v7

    add-int/lit8 v6, v6, 0x3

    .line 648
    iget v5, v5, Lorg/opencv/core/DMatch;->distance:F

    float-to-double v7, v5

    aput-wide v7, v3, v6

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 650
    :cond_1
    invoke-virtual {v2, v0, v0, v3}, Lorg/opencv/core/Mat;->put(II[D)I

    goto :goto_2

    .line 652
    :cond_2
    new-instance v2, Lorg/opencv/core/Mat;

    invoke-direct {v2}, Lorg/opencv/core/Mat;-><init>()V

    :goto_2
    return-object v2
.end method

.method public static vector_KeyPoint_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/opencv/core/KeyPoint;",
            ">;)",
            "Lorg/opencv/core/Mat;"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    .line 440
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-lez v1, :cond_2

    .line 442
    new-instance v2, Lorg/opencv/core/Mat;

    const/4 v3, 0x7

    invoke-static {v3}, Lorg/opencv/core/CvType;->CV_64FC(I)I

    move-result v3

    const/4 v4, 0x1

    invoke-direct {v2, v1, v4, v3}, Lorg/opencv/core/Mat;-><init>(III)V

    mul-int/lit8 v3, v1, 0x7

    .line 443
    new-array v3, v3, [D

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v1, :cond_1

    .line 445
    invoke-interface {p0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/opencv/core/KeyPoint;

    mul-int/lit8 v6, v4, 0x7

    .line 446
    iget-object v7, v5, Lorg/opencv/core/KeyPoint;->pt:Lorg/opencv/core/Point;

    iget-wide v7, v7, Lorg/opencv/core/Point;->x:D

    aput-wide v7, v3, v6

    add-int/lit8 v7, v6, 0x1

    .line 447
    iget-object v8, v5, Lorg/opencv/core/KeyPoint;->pt:Lorg/opencv/core/Point;

    iget-wide v8, v8, Lorg/opencv/core/Point;->y:D

    aput-wide v8, v3, v7

    add-int/lit8 v7, v6, 0x2

    .line 448
    iget v8, v5, Lorg/opencv/core/KeyPoint;->size:F

    float-to-double v8, v8

    aput-wide v8, v3, v7

    add-int/lit8 v7, v6, 0x3

    .line 449
    iget v8, v5, Lorg/opencv/core/KeyPoint;->angle:F

    float-to-double v8, v8

    aput-wide v8, v3, v7

    add-int/lit8 v7, v6, 0x4

    .line 450
    iget v8, v5, Lorg/opencv/core/KeyPoint;->response:F

    float-to-double v8, v8

    aput-wide v8, v3, v7

    add-int/lit8 v7, v6, 0x5

    .line 451
    iget v8, v5, Lorg/opencv/core/KeyPoint;->octave:I

    int-to-double v8, v8

    aput-wide v8, v3, v7

    add-int/lit8 v6, v6, 0x6

    .line 452
    iget v5, v5, Lorg/opencv/core/KeyPoint;->class_id:I

    int-to-double v7, v5

    aput-wide v7, v3, v6

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 454
    :cond_1
    invoke-virtual {v2, v0, v0, v3}, Lorg/opencv/core/Mat;->put(II[D)I

    goto :goto_2

    .line 456
    :cond_2
    new-instance v2, Lorg/opencv/core/Mat;

    invoke-direct {v2}, Lorg/opencv/core/Mat;-><init>()V

    :goto_2
    return-object v2
.end method

.method public static vector_Mat_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/opencv/core/Mat;",
            ">;)",
            "Lorg/opencv/core/Mat;"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    .line 237
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-lez v1, :cond_2

    .line 239
    new-instance v2, Lorg/opencv/core/Mat;

    sget v3, Lorg/opencv/core/CvType;->CV_32SC2:I

    const/4 v4, 0x1

    invoke-direct {v2, v1, v4, v3}, Lorg/opencv/core/Mat;-><init>(III)V

    mul-int/lit8 v3, v1, 0x2

    .line 240
    new-array v3, v3, [I

    const/4 v5, 0x0

    :goto_1
    if-ge v5, v1, :cond_1

    .line 242
    invoke-interface {p0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/opencv/core/Mat;

    iget-wide v6, v6, Lorg/opencv/core/Mat;->nativeObj:J

    mul-int/lit8 v8, v5, 0x2

    const/16 v9, 0x20

    shr-long v9, v6, v9

    long-to-int v10, v9

    .line 243
    aput v10, v3, v8

    add-int/2addr v8, v4

    const-wide/16 v9, -0x1

    and-long/2addr v6, v9

    long-to-int v7, v6

    .line 244
    aput v7, v3, v8

    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 246
    :cond_1
    invoke-virtual {v2, v0, v0, v3}, Lorg/opencv/core/Mat;->put(II[I)I

    goto :goto_2

    .line 248
    :cond_2
    new-instance v2, Lorg/opencv/core/Mat;

    invoke-direct {v2}, Lorg/opencv/core/Mat;-><init>()V

    :goto_2
    return-object v2
.end method

.method public static vector_Point2d_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/opencv/core/Point;",
            ">;)",
            "Lorg/opencv/core/Mat;"
        }
    .end annotation

    const/4 v0, 0x6

    .line 31
    invoke-static {p0, v0}, Lorg/opencv/utils/Converters;->vector_Point_to_Mat(Ljava/util/List;I)Lorg/opencv/core/Mat;

    move-result-object p0

    return-object p0
.end method

.method public static vector_Point2f_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/opencv/core/Point;",
            ">;)",
            "Lorg/opencv/core/Mat;"
        }
    .end annotation

    const/4 v0, 0x5

    .line 27
    invoke-static {p0, v0}, Lorg/opencv/utils/Converters;->vector_Point_to_Mat(Ljava/util/List;I)Lorg/opencv/core/Mat;

    move-result-object p0

    return-object p0
.end method

.method public static vector_Point3_to_Mat(Ljava/util/List;I)Lorg/opencv/core/Mat;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/opencv/core/Point3;",
            ">;I)",
            "Lorg/opencv/core/Mat;"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    .line 98
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-lez v1, :cond_4

    const/4 v2, 0x1

    packed-switch p1, :pswitch_data_0

    .line 141
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "\'typeDepth\' can be CV_32S, CV_32F or CV_64F"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 128
    :pswitch_0
    new-instance p1, Lorg/opencv/core/Mat;

    sget v3, Lorg/opencv/core/CvType;->CV_64FC3:I

    invoke-direct {p1, v1, v2, v3}, Lorg/opencv/core/Mat;-><init>(III)V

    mul-int/lit8 v2, v1, 0x3

    .line 129
    new-array v2, v2, [D

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v1, :cond_1

    .line 131
    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/opencv/core/Point3;

    mul-int/lit8 v5, v3, 0x3

    .line 132
    iget-wide v6, v4, Lorg/opencv/core/Point3;->x:D

    aput-wide v6, v2, v5

    add-int/lit8 v6, v5, 0x1

    .line 133
    iget-wide v7, v4, Lorg/opencv/core/Point3;->y:D

    aput-wide v7, v2, v6

    add-int/lit8 v5, v5, 0x2

    .line 134
    iget-wide v6, v4, Lorg/opencv/core/Point3;->z:D

    aput-wide v6, v2, v5

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 136
    :cond_1
    invoke-virtual {p1, v0, v0, v2}, Lorg/opencv/core/Mat;->put(II[D)I

    goto :goto_4

    .line 115
    :pswitch_1
    new-instance p1, Lorg/opencv/core/Mat;

    sget v3, Lorg/opencv/core/CvType;->CV_32FC3:I

    invoke-direct {p1, v1, v2, v3}, Lorg/opencv/core/Mat;-><init>(III)V

    mul-int/lit8 v2, v1, 0x3

    .line 116
    new-array v2, v2, [F

    const/4 v3, 0x0

    :goto_2
    if-ge v3, v1, :cond_2

    .line 118
    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/opencv/core/Point3;

    mul-int/lit8 v5, v3, 0x3

    .line 119
    iget-wide v6, v4, Lorg/opencv/core/Point3;->x:D

    double-to-float v6, v6

    aput v6, v2, v5

    add-int/lit8 v6, v5, 0x1

    .line 120
    iget-wide v7, v4, Lorg/opencv/core/Point3;->y:D

    double-to-float v7, v7

    aput v7, v2, v6

    add-int/lit8 v5, v5, 0x2

    .line 121
    iget-wide v6, v4, Lorg/opencv/core/Point3;->z:D

    double-to-float v4, v6

    aput v4, v2, v5

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 123
    :cond_2
    invoke-virtual {p1, v0, v0, v2}, Lorg/opencv/core/Mat;->put(II[F)I

    goto :goto_4

    .line 102
    :pswitch_2
    new-instance p1, Lorg/opencv/core/Mat;

    sget v3, Lorg/opencv/core/CvType;->CV_32SC3:I

    invoke-direct {p1, v1, v2, v3}, Lorg/opencv/core/Mat;-><init>(III)V

    mul-int/lit8 v2, v1, 0x3

    .line 103
    new-array v2, v2, [I

    const/4 v3, 0x0

    :goto_3
    if-ge v3, v1, :cond_3

    .line 105
    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/opencv/core/Point3;

    mul-int/lit8 v5, v3, 0x3

    .line 106
    iget-wide v6, v4, Lorg/opencv/core/Point3;->x:D

    double-to-int v6, v6

    aput v6, v2, v5

    add-int/lit8 v6, v5, 0x1

    .line 107
    iget-wide v7, v4, Lorg/opencv/core/Point3;->y:D

    double-to-int v7, v7

    aput v7, v2, v6

    add-int/lit8 v5, v5, 0x2

    .line 108
    iget-wide v6, v4, Lorg/opencv/core/Point3;->z:D

    double-to-int v4, v6

    aput v4, v2, v5

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 110
    :cond_3
    invoke-virtual {p1, v0, v0, v2}, Lorg/opencv/core/Mat;->put(II[I)I

    goto :goto_4

    .line 144
    :cond_4
    new-instance p1, Lorg/opencv/core/Mat;

    invoke-direct {p1}, Lorg/opencv/core/Mat;-><init>()V

    :goto_4
    return-object p1

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static vector_Point3d_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/opencv/core/Point3;",
            ">;)",
            "Lorg/opencv/core/Mat;"
        }
    .end annotation

    const/4 v0, 0x6

    .line 93
    invoke-static {p0, v0}, Lorg/opencv/utils/Converters;->vector_Point3_to_Mat(Ljava/util/List;I)Lorg/opencv/core/Mat;

    move-result-object p0

    return-object p0
.end method

.method public static vector_Point3f_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/opencv/core/Point3;",
            ">;)",
            "Lorg/opencv/core/Mat;"
        }
    .end annotation

    const/4 v0, 0x5

    .line 89
    invoke-static {p0, v0}, Lorg/opencv/utils/Converters;->vector_Point3_to_Mat(Ljava/util/List;I)Lorg/opencv/core/Mat;

    move-result-object p0

    return-object p0
.end method

.method public static vector_Point3i_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/opencv/core/Point3;",
            ">;)",
            "Lorg/opencv/core/Mat;"
        }
    .end annotation

    const/4 v0, 0x4

    .line 85
    invoke-static {p0, v0}, Lorg/opencv/utils/Converters;->vector_Point3_to_Mat(Ljava/util/List;I)Lorg/opencv/core/Mat;

    move-result-object p0

    return-object p0
.end method

.method public static vector_Point_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/opencv/core/Point;",
            ">;)",
            "Lorg/opencv/core/Mat;"
        }
    .end annotation

    const/4 v0, 0x4

    .line 23
    invoke-static {p0, v0}, Lorg/opencv/utils/Converters;->vector_Point_to_Mat(Ljava/util/List;I)Lorg/opencv/core/Mat;

    move-result-object p0

    return-object p0
.end method

.method public static vector_Point_to_Mat(Ljava/util/List;I)Lorg/opencv/core/Mat;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/opencv/core/Point;",
            ">;I)",
            "Lorg/opencv/core/Mat;"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    .line 36
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-lez v1, :cond_4

    const/4 v2, 0x1

    packed-switch p1, :pswitch_data_0

    .line 76
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "\'typeDepth\' can be CV_32S, CV_32F or CV_64F"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 64
    :pswitch_0
    new-instance p1, Lorg/opencv/core/Mat;

    sget v3, Lorg/opencv/core/CvType;->CV_64FC2:I

    invoke-direct {p1, v1, v2, v3}, Lorg/opencv/core/Mat;-><init>(III)V

    mul-int/lit8 v3, v1, 0x2

    .line 65
    new-array v3, v3, [D

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v1, :cond_1

    .line 67
    invoke-interface {p0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/opencv/core/Point;

    mul-int/lit8 v6, v4, 0x2

    .line 68
    iget-wide v7, v5, Lorg/opencv/core/Point;->x:D

    aput-wide v7, v3, v6

    add-int/2addr v6, v2

    .line 69
    iget-wide v7, v5, Lorg/opencv/core/Point;->y:D

    aput-wide v7, v3, v6

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 71
    :cond_1
    invoke-virtual {p1, v0, v0, v3}, Lorg/opencv/core/Mat;->put(II[D)I

    goto :goto_4

    .line 52
    :pswitch_1
    new-instance p1, Lorg/opencv/core/Mat;

    sget v3, Lorg/opencv/core/CvType;->CV_32FC2:I

    invoke-direct {p1, v1, v2, v3}, Lorg/opencv/core/Mat;-><init>(III)V

    mul-int/lit8 v3, v1, 0x2

    .line 53
    new-array v3, v3, [F

    const/4 v4, 0x0

    :goto_2
    if-ge v4, v1, :cond_2

    .line 55
    invoke-interface {p0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/opencv/core/Point;

    mul-int/lit8 v6, v4, 0x2

    .line 56
    iget-wide v7, v5, Lorg/opencv/core/Point;->x:D

    double-to-float v7, v7

    aput v7, v3, v6

    add-int/2addr v6, v2

    .line 57
    iget-wide v7, v5, Lorg/opencv/core/Point;->y:D

    double-to-float v5, v7

    aput v5, v3, v6

    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 59
    :cond_2
    invoke-virtual {p1, v0, v0, v3}, Lorg/opencv/core/Mat;->put(II[F)I

    goto :goto_4

    .line 40
    :pswitch_2
    new-instance p1, Lorg/opencv/core/Mat;

    sget v3, Lorg/opencv/core/CvType;->CV_32SC2:I

    invoke-direct {p1, v1, v2, v3}, Lorg/opencv/core/Mat;-><init>(III)V

    mul-int/lit8 v3, v1, 0x2

    .line 41
    new-array v3, v3, [I

    const/4 v4, 0x0

    :goto_3
    if-ge v4, v1, :cond_3

    .line 43
    invoke-interface {p0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/opencv/core/Point;

    mul-int/lit8 v6, v4, 0x2

    .line 44
    iget-wide v7, v5, Lorg/opencv/core/Point;->x:D

    double-to-int v7, v7

    aput v7, v3, v6

    add-int/2addr v6, v2

    .line 45
    iget-wide v7, v5, Lorg/opencv/core/Point;->y:D

    double-to-int v5, v7

    aput v5, v3, v6

    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 47
    :cond_3
    invoke-virtual {p1, v0, v0, v3}, Lorg/opencv/core/Mat;->put(II[I)I

    goto :goto_4

    .line 79
    :cond_4
    new-instance p1, Lorg/opencv/core/Mat;

    invoke-direct {p1}, Lorg/opencv/core/Mat;-><init>()V

    :goto_4
    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static vector_Rect_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/opencv/core/Rect;",
            ">;)",
            "Lorg/opencv/core/Mat;"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    .line 404
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-lez v1, :cond_2

    .line 406
    new-instance v2, Lorg/opencv/core/Mat;

    sget v3, Lorg/opencv/core/CvType;->CV_32SC4:I

    const/4 v4, 0x1

    invoke-direct {v2, v1, v4, v3}, Lorg/opencv/core/Mat;-><init>(III)V

    mul-int/lit8 v3, v1, 0x4

    .line 407
    new-array v3, v3, [I

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v1, :cond_1

    .line 409
    invoke-interface {p0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/opencv/core/Rect;

    mul-int/lit8 v6, v4, 0x4

    .line 410
    iget v7, v5, Lorg/opencv/core/Rect;->x:I

    aput v7, v3, v6

    add-int/lit8 v7, v6, 0x1

    .line 411
    iget v8, v5, Lorg/opencv/core/Rect;->y:I

    aput v8, v3, v7

    add-int/lit8 v7, v6, 0x2

    .line 412
    iget v8, v5, Lorg/opencv/core/Rect;->width:I

    aput v8, v3, v7

    add-int/lit8 v6, v6, 0x3

    .line 413
    iget v5, v5, Lorg/opencv/core/Rect;->height:I

    aput v5, v3, v6

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 415
    :cond_1
    invoke-virtual {v2, v0, v0, v3}, Lorg/opencv/core/Mat;->put(II[I)I

    goto :goto_2

    .line 417
    :cond_2
    new-instance v2, Lorg/opencv/core/Mat;

    invoke-direct {v2}, Lorg/opencv/core/Mat;-><init>()V

    :goto_2
    return-object v2
.end method

.method public static vector_char_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Byte;",
            ">;)",
            "Lorg/opencv/core/Mat;"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    .line 338
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-lez v1, :cond_2

    .line 340
    new-instance v2, Lorg/opencv/core/Mat;

    sget v3, Lorg/opencv/core/CvType;->CV_8SC1:I

    const/4 v4, 0x1

    invoke-direct {v2, v1, v4, v3}, Lorg/opencv/core/Mat;-><init>(III)V

    .line 341
    new-array v3, v1, [B

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v1, :cond_1

    .line 343
    invoke-interface {p0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Byte;

    invoke-virtual {v5}, Ljava/lang/Byte;->byteValue()B

    move-result v5

    .line 344
    aput-byte v5, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 346
    :cond_1
    invoke-virtual {v2, v0, v0, v3}, Lorg/opencv/core/Mat;->put(II[B)I

    goto :goto_2

    .line 348
    :cond_2
    new-instance v2, Lorg/opencv/core/Mat;

    invoke-direct {v2}, Lorg/opencv/core/Mat;-><init>()V

    :goto_2
    return-object v2
.end method

.method public static vector_double_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Double;",
            ">;)",
            "Lorg/opencv/core/Mat;"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    .line 606
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-lez v1, :cond_2

    .line 608
    new-instance v2, Lorg/opencv/core/Mat;

    sget v3, Lorg/opencv/core/CvType;->CV_64FC1:I

    const/4 v4, 0x1

    invoke-direct {v2, v1, v4, v3}, Lorg/opencv/core/Mat;-><init>(III)V

    .line 609
    new-array v3, v1, [D

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v1, :cond_1

    .line 611
    invoke-interface {p0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Double;

    invoke-virtual {v5}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v5

    .line 612
    aput-wide v5, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 614
    :cond_1
    invoke-virtual {v2, v0, v0, v3}, Lorg/opencv/core/Mat;->put(II[D)I

    goto :goto_2

    .line 616
    :cond_2
    new-instance v2, Lorg/opencv/core/Mat;

    invoke-direct {v2}, Lorg/opencv/core/Mat;-><init>()V

    :goto_2
    return-object v2
.end method

.method public static vector_float_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Float;",
            ">;)",
            "Lorg/opencv/core/Mat;"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    .line 272
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-lez v1, :cond_2

    .line 274
    new-instance v2, Lorg/opencv/core/Mat;

    sget v3, Lorg/opencv/core/CvType;->CV_32FC1:I

    const/4 v4, 0x1

    invoke-direct {v2, v1, v4, v3}, Lorg/opencv/core/Mat;-><init>(III)V

    .line 275
    new-array v3, v1, [F

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v1, :cond_1

    .line 277
    invoke-interface {p0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    .line 278
    aput v5, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 280
    :cond_1
    invoke-virtual {v2, v0, v0, v3}, Lorg/opencv/core/Mat;->put(II[F)I

    goto :goto_2

    .line 282
    :cond_2
    new-instance v2, Lorg/opencv/core/Mat;

    invoke-direct {v2}, Lorg/opencv/core/Mat;-><init>()V

    :goto_2
    return-object v2
.end method

.method public static vector_int_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lorg/opencv/core/Mat;"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    .line 355
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-lez v1, :cond_2

    .line 357
    new-instance v2, Lorg/opencv/core/Mat;

    sget v3, Lorg/opencv/core/CvType;->CV_32SC1:I

    const/4 v4, 0x1

    invoke-direct {v2, v1, v4, v3}, Lorg/opencv/core/Mat;-><init>(III)V

    .line 358
    new-array v3, v1, [I

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v1, :cond_1

    .line 360
    invoke-interface {p0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 361
    aput v5, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 363
    :cond_1
    invoke-virtual {v2, v0, v0, v3}, Lorg/opencv/core/Mat;->put(II[I)I

    goto :goto_2

    .line 365
    :cond_2
    new-instance v2, Lorg/opencv/core/Mat;

    invoke-direct {v2}, Lorg/opencv/core/Mat;-><init>()V

    :goto_2
    return-object v2
.end method

.method public static vector_uchar_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Byte;",
            ">;)",
            "Lorg/opencv/core/Mat;"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    .line 305
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-lez v1, :cond_2

    .line 307
    new-instance v2, Lorg/opencv/core/Mat;

    sget v3, Lorg/opencv/core/CvType;->CV_8UC1:I

    const/4 v4, 0x1

    invoke-direct {v2, v1, v4, v3}, Lorg/opencv/core/Mat;-><init>(III)V

    .line 308
    new-array v3, v1, [B

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v1, :cond_1

    .line 310
    invoke-interface {p0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Byte;

    invoke-virtual {v5}, Ljava/lang/Byte;->byteValue()B

    move-result v5

    .line 311
    aput-byte v5, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 313
    :cond_1
    invoke-virtual {v2, v0, v0, v3}, Lorg/opencv/core/Mat;->put(II[B)I

    goto :goto_2

    .line 315
    :cond_2
    new-instance v2, Lorg/opencv/core/Mat;

    invoke-direct {v2}, Lorg/opencv/core/Mat;-><init>()V

    :goto_2
    return-object v2
.end method

.method public static vector_vector_DMatch_to_Mat(Ljava/util/List;Ljava/util/List;)Lorg/opencv/core/Mat;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/opencv/core/MatOfDMatch;",
            ">;",
            "Ljava/util/List<",
            "Lorg/opencv/core/Mat;",
            ">;)",
            "Lorg/opencv/core/Mat;"
        }
    .end annotation

    if-eqz p0, :cond_0

    .line 676
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-lez v0, :cond_2

    .line 678
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/opencv/core/MatOfDMatch;

    .line 679
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 680
    :cond_1
    invoke-static {p1}, Lorg/opencv/utils/Converters;->vector_Mat_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;

    move-result-object p0

    goto :goto_2

    .line 682
    :cond_2
    new-instance p0, Lorg/opencv/core/Mat;

    invoke-direct {p0}, Lorg/opencv/core/Mat;-><init>()V

    :goto_2
    return-object p0
.end method

.method public static vector_vector_KeyPoint_to_Mat(Ljava/util/List;Ljava/util/List;)Lorg/opencv/core/Mat;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/opencv/core/MatOfKeyPoint;",
            ">;",
            "Ljava/util/List<",
            "Lorg/opencv/core/Mat;",
            ">;)",
            "Lorg/opencv/core/Mat;"
        }
    .end annotation

    if-eqz p0, :cond_0

    .line 576
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-lez v0, :cond_2

    .line 578
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/opencv/core/MatOfKeyPoint;

    .line 579
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 580
    :cond_1
    invoke-static {p1}, Lorg/opencv/utils/Converters;->vector_Mat_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;

    move-result-object p0

    goto :goto_2

    .line 582
    :cond_2
    new-instance p0, Lorg/opencv/core/Mat;

    invoke-direct {p0}, Lorg/opencv/core/Mat;-><init>()V

    :goto_2
    return-object p0
.end method

.method public static vector_vector_Point2f_to_Mat(Ljava/util/List;Ljava/util/List;)Lorg/opencv/core/Mat;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/opencv/core/MatOfPoint2f;",
            ">;",
            "Ljava/util/List<",
            "Lorg/opencv/core/Mat;",
            ">;)",
            "Lorg/opencv/core/Mat;"
        }
    .end annotation

    if-eqz p0, :cond_0

    .line 530
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-lez v0, :cond_2

    .line 532
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/opencv/core/MatOfPoint2f;

    .line 533
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 534
    :cond_1
    invoke-static {p1}, Lorg/opencv/utils/Converters;->vector_Mat_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;

    move-result-object p0

    goto :goto_2

    .line 536
    :cond_2
    new-instance p0, Lorg/opencv/core/Mat;

    invoke-direct {p0}, Lorg/opencv/core/Mat;-><init>()V

    :goto_2
    return-object p0
.end method

.method public static vector_vector_Point3f_to_Mat(Ljava/util/List;Ljava/util/List;)Lorg/opencv/core/Mat;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/opencv/core/MatOfPoint3f;",
            ">;",
            "Ljava/util/List<",
            "Lorg/opencv/core/Mat;",
            ">;)",
            "Lorg/opencv/core/Mat;"
        }
    .end annotation

    if-eqz p0, :cond_0

    .line 562
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-lez v0, :cond_2

    .line 564
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/opencv/core/MatOfPoint3f;

    .line 565
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 566
    :cond_1
    invoke-static {p1}, Lorg/opencv/utils/Converters;->vector_Mat_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;

    move-result-object p0

    goto :goto_2

    .line 568
    :cond_2
    new-instance p0, Lorg/opencv/core/Mat;

    invoke-direct {p0}, Lorg/opencv/core/Mat;-><init>()V

    :goto_2
    return-object p0
.end method

.method public static vector_vector_Point_to_Mat(Ljava/util/List;Ljava/util/List;)Lorg/opencv/core/Mat;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/opencv/core/MatOfPoint;",
            ">;",
            "Ljava/util/List<",
            "Lorg/opencv/core/Mat;",
            ">;)",
            "Lorg/opencv/core/Mat;"
        }
    .end annotation

    if-eqz p0, :cond_0

    .line 481
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-lez v0, :cond_2

    .line 483
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/opencv/core/MatOfPoint;

    .line 484
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 485
    :cond_1
    invoke-static {p1}, Lorg/opencv/utils/Converters;->vector_Mat_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;

    move-result-object p0

    goto :goto_2

    .line 487
    :cond_2
    new-instance p0, Lorg/opencv/core/Mat;

    invoke-direct {p0}, Lorg/opencv/core/Mat;-><init>()V

    :goto_2
    return-object p0
.end method

.method public static vector_vector_char_to_Mat(Ljava/util/List;Ljava/util/List;)Lorg/opencv/core/Mat;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/opencv/core/MatOfByte;",
            ">;",
            "Ljava/util/List<",
            "Lorg/opencv/core/Mat;",
            ">;)",
            "Lorg/opencv/core/Mat;"
        }
    .end annotation

    if-eqz p0, :cond_0

    .line 708
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-lez v0, :cond_2

    .line 710
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/opencv/core/MatOfByte;

    .line 711
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 712
    :cond_1
    invoke-static {p1}, Lorg/opencv/utils/Converters;->vector_Mat_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;

    move-result-object p0

    goto :goto_2

    .line 714
    :cond_2
    new-instance p0, Lorg/opencv/core/Mat;

    invoke-direct {p0}, Lorg/opencv/core/Mat;-><init>()V

    :goto_2
    return-object p0
.end method

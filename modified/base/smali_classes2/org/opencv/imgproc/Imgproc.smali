.class public Lorg/opencv/imgproc/Imgproc;
.super Ljava/lang/Object;
.source "Imgproc.java"


# static fields
.field public static final ADAPTIVE_THRESH_GAUSSIAN_C:I = 0x1

.field public static final ADAPTIVE_THRESH_MEAN_C:I = 0x0

.field public static final CC_STAT_AREA:I = 0x4

.field public static final CC_STAT_HEIGHT:I = 0x3

.field public static final CC_STAT_LEFT:I = 0x0

.field public static final CC_STAT_MAX:I = 0x5

.field public static final CC_STAT_TOP:I = 0x1

.field public static final CC_STAT_WIDTH:I = 0x2

.field public static final CHAIN_APPROX_NONE:I = 0x1

.field public static final CHAIN_APPROX_SIMPLE:I = 0x2

.field public static final CHAIN_APPROX_TC89_KCOS:I = 0x4

.field public static final CHAIN_APPROX_TC89_L1:I = 0x3

.field public static final COLORMAP_AUTUMN:I = 0x0

.field public static final COLORMAP_BONE:I = 0x1

.field public static final COLORMAP_COOL:I = 0x8

.field public static final COLORMAP_HOT:I = 0xb

.field public static final COLORMAP_HSV:I = 0x9

.field public static final COLORMAP_JET:I = 0x2

.field public static final COLORMAP_OCEAN:I = 0x5

.field public static final COLORMAP_PARULA:I = 0xc

.field public static final COLORMAP_PINK:I = 0xa

.field public static final COLORMAP_RAINBOW:I = 0x4

.field public static final COLORMAP_SPRING:I = 0x7

.field public static final COLORMAP_SUMMER:I = 0x6

.field public static final COLORMAP_WINTER:I = 0x3

.field public static final COLOR_BGR2BGR555:I = 0x16

.field public static final COLOR_BGR2BGR565:I = 0xc

.field public static final COLOR_BGR2BGRA:I = 0x0

.field public static final COLOR_BGR2GRAY:I = 0x6

.field public static final COLOR_BGR2HLS:I = 0x34

.field public static final COLOR_BGR2HLS_FULL:I = 0x44

.field public static final COLOR_BGR2HSV:I = 0x28

.field public static final COLOR_BGR2HSV_FULL:I = 0x42

.field public static final COLOR_BGR2Lab:I = 0x2c

.field public static final COLOR_BGR2Luv:I = 0x32

.field public static final COLOR_BGR2RGB:I = 0x4

.field public static final COLOR_BGR2RGBA:I = 0x2

.field public static final COLOR_BGR2XYZ:I = 0x20

.field public static final COLOR_BGR2YCrCb:I = 0x24

.field public static final COLOR_BGR2YUV:I = 0x52

.field public static final COLOR_BGR2YUV_I420:I = 0x80

.field public static final COLOR_BGR2YUV_IYUV:I = 0x80

.field public static final COLOR_BGR2YUV_YV12:I = 0x84

.field public static final COLOR_BGR5552BGR:I = 0x18

.field public static final COLOR_BGR5552BGRA:I = 0x1c

.field public static final COLOR_BGR5552GRAY:I = 0x1f

.field public static final COLOR_BGR5552RGB:I = 0x19

.field public static final COLOR_BGR5552RGBA:I = 0x1d

.field public static final COLOR_BGR5652BGR:I = 0xe

.field public static final COLOR_BGR5652BGRA:I = 0x12

.field public static final COLOR_BGR5652GRAY:I = 0x15

.field public static final COLOR_BGR5652RGB:I = 0xf

.field public static final COLOR_BGR5652RGBA:I = 0x13

.field public static final COLOR_BGRA2BGR:I = 0x1

.field public static final COLOR_BGRA2BGR555:I = 0x1a

.field public static final COLOR_BGRA2BGR565:I = 0x10

.field public static final COLOR_BGRA2GRAY:I = 0xa

.field public static final COLOR_BGRA2RGB:I = 0x3

.field public static final COLOR_BGRA2RGBA:I = 0x5

.field public static final COLOR_BGRA2YUV_I420:I = 0x82

.field public static final COLOR_BGRA2YUV_IYUV:I = 0x82

.field public static final COLOR_BGRA2YUV_YV12:I = 0x86

.field public static final COLOR_BayerBG2BGR:I = 0x2e

.field public static final COLOR_BayerBG2BGR_EA:I = 0x87

.field public static final COLOR_BayerBG2BGR_VNG:I = 0x3e

.field public static final COLOR_BayerBG2GRAY:I = 0x56

.field public static final COLOR_BayerBG2RGB:I = 0x30

.field public static final COLOR_BayerBG2RGB_EA:I = 0x89

.field public static final COLOR_BayerBG2RGB_VNG:I = 0x40

.field public static final COLOR_BayerGB2BGR:I = 0x2f

.field public static final COLOR_BayerGB2BGR_EA:I = 0x88

.field public static final COLOR_BayerGB2BGR_VNG:I = 0x3f

.field public static final COLOR_BayerGB2GRAY:I = 0x57

.field public static final COLOR_BayerGB2RGB:I = 0x31

.field public static final COLOR_BayerGB2RGB_EA:I = 0x8a

.field public static final COLOR_BayerGB2RGB_VNG:I = 0x41

.field public static final COLOR_BayerGR2BGR:I = 0x31

.field public static final COLOR_BayerGR2BGR_EA:I = 0x8a

.field public static final COLOR_BayerGR2BGR_VNG:I = 0x41

.field public static final COLOR_BayerGR2GRAY:I = 0x59

.field public static final COLOR_BayerGR2RGB:I = 0x2f

.field public static final COLOR_BayerGR2RGB_EA:I = 0x88

.field public static final COLOR_BayerGR2RGB_VNG:I = 0x3f

.field public static final COLOR_BayerRG2BGR:I = 0x30

.field public static final COLOR_BayerRG2BGR_EA:I = 0x89

.field public static final COLOR_BayerRG2BGR_VNG:I = 0x40

.field public static final COLOR_BayerRG2GRAY:I = 0x58

.field public static final COLOR_BayerRG2RGB:I = 0x2e

.field public static final COLOR_BayerRG2RGB_EA:I = 0x87

.field public static final COLOR_BayerRG2RGB_VNG:I = 0x3e

.field public static final COLOR_COLORCVT_MAX:I = 0x8b

.field public static final COLOR_GRAY2BGR:I = 0x8

.field public static final COLOR_GRAY2BGR555:I = 0x1e

.field public static final COLOR_GRAY2BGR565:I = 0x14

.field public static final COLOR_GRAY2BGRA:I = 0x9

.field public static final COLOR_GRAY2RGB:I = 0x8

.field public static final COLOR_GRAY2RGBA:I = 0x9

.field public static final COLOR_HLS2BGR:I = 0x3c

.field public static final COLOR_HLS2BGR_FULL:I = 0x48

.field public static final COLOR_HLS2RGB:I = 0x3d

.field public static final COLOR_HLS2RGB_FULL:I = 0x49

.field public static final COLOR_HSV2BGR:I = 0x36

.field public static final COLOR_HSV2BGR_FULL:I = 0x46

.field public static final COLOR_HSV2RGB:I = 0x37

.field public static final COLOR_HSV2RGB_FULL:I = 0x47

.field public static final COLOR_LBGR2Lab:I = 0x4a

.field public static final COLOR_LBGR2Luv:I = 0x4c

.field public static final COLOR_LRGB2Lab:I = 0x4b

.field public static final COLOR_LRGB2Luv:I = 0x4d

.field public static final COLOR_Lab2BGR:I = 0x38

.field public static final COLOR_Lab2LBGR:I = 0x4e

.field public static final COLOR_Lab2LRGB:I = 0x4f

.field public static final COLOR_Lab2RGB:I = 0x39

.field public static final COLOR_Luv2BGR:I = 0x3a

.field public static final COLOR_Luv2LBGR:I = 0x50

.field public static final COLOR_Luv2LRGB:I = 0x51

.field public static final COLOR_Luv2RGB:I = 0x3b

.field public static final COLOR_RGB2BGR:I = 0x4

.field public static final COLOR_RGB2BGR555:I = 0x17

.field public static final COLOR_RGB2BGR565:I = 0xd

.field public static final COLOR_RGB2BGRA:I = 0x2

.field public static final COLOR_RGB2GRAY:I = 0x7

.field public static final COLOR_RGB2HLS:I = 0x35

.field public static final COLOR_RGB2HLS_FULL:I = 0x45

.field public static final COLOR_RGB2HSV:I = 0x29

.field public static final COLOR_RGB2HSV_FULL:I = 0x43

.field public static final COLOR_RGB2Lab:I = 0x2d

.field public static final COLOR_RGB2Luv:I = 0x33

.field public static final COLOR_RGB2RGBA:I = 0x0

.field public static final COLOR_RGB2XYZ:I = 0x21

.field public static final COLOR_RGB2YCrCb:I = 0x25

.field public static final COLOR_RGB2YUV:I = 0x53

.field public static final COLOR_RGB2YUV_I420:I = 0x7f

.field public static final COLOR_RGB2YUV_IYUV:I = 0x7f

.field public static final COLOR_RGB2YUV_YV12:I = 0x83

.field public static final COLOR_RGBA2BGR:I = 0x3

.field public static final COLOR_RGBA2BGR555:I = 0x1b

.field public static final COLOR_RGBA2BGR565:I = 0x11

.field public static final COLOR_RGBA2BGRA:I = 0x5

.field public static final COLOR_RGBA2GRAY:I = 0xb

.field public static final COLOR_RGBA2RGB:I = 0x1

.field public static final COLOR_RGBA2YUV_I420:I = 0x81

.field public static final COLOR_RGBA2YUV_IYUV:I = 0x81

.field public static final COLOR_RGBA2YUV_YV12:I = 0x85

.field public static final COLOR_RGBA2mRGBA:I = 0x7d

.field public static final COLOR_XYZ2BGR:I = 0x22

.field public static final COLOR_XYZ2RGB:I = 0x23

.field public static final COLOR_YCrCb2BGR:I = 0x26

.field public static final COLOR_YCrCb2RGB:I = 0x27

.field public static final COLOR_YUV2BGR:I = 0x54

.field public static final COLOR_YUV2BGRA_I420:I = 0x69

.field public static final COLOR_YUV2BGRA_IYUV:I = 0x69

.field public static final COLOR_YUV2BGRA_NV12:I = 0x5f

.field public static final COLOR_YUV2BGRA_NV21:I = 0x61

.field public static final COLOR_YUV2BGRA_UYNV:I = 0x70

.field public static final COLOR_YUV2BGRA_UYVY:I = 0x70

.field public static final COLOR_YUV2BGRA_Y422:I = 0x70

.field public static final COLOR_YUV2BGRA_YUNV:I = 0x78

.field public static final COLOR_YUV2BGRA_YUY2:I = 0x78

.field public static final COLOR_YUV2BGRA_YUYV:I = 0x78

.field public static final COLOR_YUV2BGRA_YV12:I = 0x67

.field public static final COLOR_YUV2BGRA_YVYU:I = 0x7a

.field public static final COLOR_YUV2BGR_I420:I = 0x65

.field public static final COLOR_YUV2BGR_IYUV:I = 0x65

.field public static final COLOR_YUV2BGR_NV12:I = 0x5b

.field public static final COLOR_YUV2BGR_NV21:I = 0x5d

.field public static final COLOR_YUV2BGR_UYNV:I = 0x6c

.field public static final COLOR_YUV2BGR_UYVY:I = 0x6c

.field public static final COLOR_YUV2BGR_Y422:I = 0x6c

.field public static final COLOR_YUV2BGR_YUNV:I = 0x74

.field public static final COLOR_YUV2BGR_YUY2:I = 0x74

.field public static final COLOR_YUV2BGR_YUYV:I = 0x74

.field public static final COLOR_YUV2BGR_YV12:I = 0x63

.field public static final COLOR_YUV2BGR_YVYU:I = 0x76

.field public static final COLOR_YUV2GRAY_420:I = 0x6a

.field public static final COLOR_YUV2GRAY_I420:I = 0x6a

.field public static final COLOR_YUV2GRAY_IYUV:I = 0x6a

.field public static final COLOR_YUV2GRAY_NV12:I = 0x6a

.field public static final COLOR_YUV2GRAY_NV21:I = 0x6a

.field public static final COLOR_YUV2GRAY_UYNV:I = 0x7b

.field public static final COLOR_YUV2GRAY_UYVY:I = 0x7b

.field public static final COLOR_YUV2GRAY_Y422:I = 0x7b

.field public static final COLOR_YUV2GRAY_YUNV:I = 0x7c

.field public static final COLOR_YUV2GRAY_YUY2:I = 0x7c

.field public static final COLOR_YUV2GRAY_YUYV:I = 0x7c

.field public static final COLOR_YUV2GRAY_YV12:I = 0x6a

.field public static final COLOR_YUV2GRAY_YVYU:I = 0x7c

.field public static final COLOR_YUV2RGB:I = 0x55

.field public static final COLOR_YUV2RGBA_I420:I = 0x68

.field public static final COLOR_YUV2RGBA_IYUV:I = 0x68

.field public static final COLOR_YUV2RGBA_NV12:I = 0x5e

.field public static final COLOR_YUV2RGBA_NV21:I = 0x60

.field public static final COLOR_YUV2RGBA_UYNV:I = 0x6f

.field public static final COLOR_YUV2RGBA_UYVY:I = 0x6f

.field public static final COLOR_YUV2RGBA_Y422:I = 0x6f

.field public static final COLOR_YUV2RGBA_YUNV:I = 0x77

.field public static final COLOR_YUV2RGBA_YUY2:I = 0x77

.field public static final COLOR_YUV2RGBA_YUYV:I = 0x77

.field public static final COLOR_YUV2RGBA_YV12:I = 0x66

.field public static final COLOR_YUV2RGBA_YVYU:I = 0x79

.field public static final COLOR_YUV2RGB_I420:I = 0x64

.field public static final COLOR_YUV2RGB_IYUV:I = 0x64

.field public static final COLOR_YUV2RGB_NV12:I = 0x5a

.field public static final COLOR_YUV2RGB_NV21:I = 0x5c

.field public static final COLOR_YUV2RGB_UYNV:I = 0x6b

.field public static final COLOR_YUV2RGB_UYVY:I = 0x6b

.field public static final COLOR_YUV2RGB_Y422:I = 0x6b

.field public static final COLOR_YUV2RGB_YUNV:I = 0x73

.field public static final COLOR_YUV2RGB_YUY2:I = 0x73

.field public static final COLOR_YUV2RGB_YUYV:I = 0x73

.field public static final COLOR_YUV2RGB_YV12:I = 0x62

.field public static final COLOR_YUV2RGB_YVYU:I = 0x75

.field public static final COLOR_YUV420p2BGR:I = 0x63

.field public static final COLOR_YUV420p2BGRA:I = 0x67

.field public static final COLOR_YUV420p2GRAY:I = 0x6a

.field public static final COLOR_YUV420p2RGB:I = 0x62

.field public static final COLOR_YUV420p2RGBA:I = 0x66

.field public static final COLOR_YUV420sp2BGR:I = 0x5d

.field public static final COLOR_YUV420sp2BGRA:I = 0x61

.field public static final COLOR_YUV420sp2GRAY:I = 0x6a

.field public static final COLOR_YUV420sp2RGB:I = 0x5c

.field public static final COLOR_YUV420sp2RGBA:I = 0x60

.field public static final COLOR_mRGBA2RGBA:I = 0x7e

.field public static final CV_BILATERAL:I = 0x4

.field public static final CV_BLUR:I = 0x1

.field public static final CV_BLUR_NO_SCALE:I = 0x0

.field public static final CV_CANNY_L2_GRADIENT:I = -0x80000000

.field private static final CV_CHAIN_APPROX_NONE:I = 0x1

.field private static final CV_CHAIN_APPROX_SIMPLE:I = 0x2

.field private static final CV_CHAIN_APPROX_TC89_KCOS:I = 0x4

.field private static final CV_CHAIN_APPROX_TC89_L1:I = 0x3

.field public static final CV_CHAIN_CODE:I = 0x0

.field public static final CV_CLOCKWISE:I = 0x1

.field public static final CV_COMP_BHATTACHARYYA:I = 0x3

.field public static final CV_COMP_CHISQR:I = 0x1

.field public static final CV_COMP_CHISQR_ALT:I = 0x4

.field public static final CV_COMP_CORREL:I = 0x0

.field public static final CV_COMP_HELLINGER:I = 0x3

.field public static final CV_COMP_INTERSECT:I = 0x2

.field public static final CV_COMP_KL_DIV:I = 0x5

.field public static final CV_CONTOURS_MATCH_I1:I = 0x1

.field public static final CV_CONTOURS_MATCH_I2:I = 0x2

.field public static final CV_CONTOURS_MATCH_I3:I = 0x3

.field public static final CV_COUNTER_CLOCKWISE:I = 0x2

.field public static final CV_DIST_C:I = 0x3

.field public static final CV_DIST_FAIR:I = 0x5

.field public static final CV_DIST_HUBER:I = 0x7

.field public static final CV_DIST_L1:I = 0x1

.field public static final CV_DIST_L12:I = 0x4

.field public static final CV_DIST_L2:I = 0x2

.field public static final CV_DIST_LABEL_CCOMP:I = 0x0

.field public static final CV_DIST_LABEL_PIXEL:I = 0x1

.field public static final CV_DIST_MASK_3:I = 0x3

.field public static final CV_DIST_MASK_5:I = 0x5

.field public static final CV_DIST_MASK_PRECISE:I = 0x0

.field public static final CV_DIST_USER:I = -0x1

.field public static final CV_DIST_WELSCH:I = 0x6

.field public static final CV_GAUSSIAN:I = 0x2

.field public static final CV_GAUSSIAN_5x5:I = 0x7

.field public static final CV_HOUGH_GRADIENT:I = 0x3

.field public static final CV_HOUGH_MULTI_SCALE:I = 0x2

.field public static final CV_HOUGH_PROBABILISTIC:I = 0x1

.field public static final CV_HOUGH_STANDARD:I = 0x0

.field private static final CV_INTER_AREA:I = 0x3

.field private static final CV_INTER_CUBIC:I = 0x2

.field private static final CV_INTER_LANCZOS4:I = 0x4

.field private static final CV_INTER_LINEAR:I = 0x1

.field private static final CV_INTER_NN:I = 0x0

.field public static final CV_LINK_RUNS:I = 0x5

.field public static final CV_MAX_SOBEL_KSIZE:I = 0x7

.field public static final CV_MEDIAN:I = 0x3

.field private static final CV_MOP_BLACKHAT:I = 0x6

.field private static final CV_MOP_CLOSE:I = 0x3

.field private static final CV_MOP_DILATE:I = 0x1

.field private static final CV_MOP_ERODE:I = 0x0

.field private static final CV_MOP_GRADIENT:I = 0x4

.field private static final CV_MOP_OPEN:I = 0x2

.field private static final CV_MOP_TOPHAT:I = 0x5

.field public static final CV_POLY_APPROX_DP:I = 0x0

.field private static final CV_RETR_CCOMP:I = 0x2

.field private static final CV_RETR_EXTERNAL:I = 0x0

.field private static final CV_RETR_FLOODFILL:I = 0x4

.field private static final CV_RETR_LIST:I = 0x1

.field private static final CV_RETR_TREE:I = 0x3

.field public static final CV_RGBA2mRGBA:I = 0x7d

.field public static final CV_SCHARR:I = -0x1

.field public static final CV_SHAPE_CROSS:I = 0x1

.field public static final CV_SHAPE_CUSTOM:I = 0x64

.field public static final CV_SHAPE_ELLIPSE:I = 0x2

.field public static final CV_SHAPE_RECT:I = 0x0

.field private static final CV_THRESH_BINARY:I = 0x0

.field private static final CV_THRESH_BINARY_INV:I = 0x1

.field private static final CV_THRESH_MASK:I = 0x7

.field private static final CV_THRESH_OTSU:I = 0x8

.field private static final CV_THRESH_TOZERO:I = 0x3

.field private static final CV_THRESH_TOZERO_INV:I = 0x4

.field private static final CV_THRESH_TRIANGLE:I = 0x10

.field private static final CV_THRESH_TRUNC:I = 0x2

.field public static final CV_WARP_FILL_OUTLIERS:I = 0x8

.field public static final CV_WARP_INVERSE_MAP:I = 0x10

.field public static final CV_mRGBA2RGBA:I = 0x7e

.field public static final DIST_C:I = 0x3

.field public static final DIST_FAIR:I = 0x5

.field public static final DIST_HUBER:I = 0x7

.field public static final DIST_L1:I = 0x1

.field public static final DIST_L12:I = 0x4

.field public static final DIST_L2:I = 0x2

.field public static final DIST_LABEL_CCOMP:I = 0x0

.field public static final DIST_LABEL_PIXEL:I = 0x1

.field public static final DIST_MASK_3:I = 0x3

.field public static final DIST_MASK_5:I = 0x5

.field public static final DIST_MASK_PRECISE:I = 0x0

.field public static final DIST_USER:I = -0x1

.field public static final DIST_WELSCH:I = 0x6

.field public static final FLOODFILL_FIXED_RANGE:I = 0x10000

.field public static final FLOODFILL_MASK_ONLY:I = 0x20000

.field public static final GC_BGD:I = 0x0

.field public static final GC_EVAL:I = 0x2

.field public static final GC_FGD:I = 0x1

.field public static final GC_INIT_WITH_MASK:I = 0x1

.field public static final GC_INIT_WITH_RECT:I = 0x0

.field public static final GC_PR_BGD:I = 0x2

.field public static final GC_PR_FGD:I = 0x3

.field public static final HISTCMP_BHATTACHARYYA:I = 0x3

.field public static final HISTCMP_CHISQR:I = 0x1

.field public static final HISTCMP_CHISQR_ALT:I = 0x4

.field public static final HISTCMP_CORREL:I = 0x0

.field public static final HISTCMP_HELLINGER:I = 0x3

.field public static final HISTCMP_INTERSECT:I = 0x2

.field public static final HISTCMP_KL_DIV:I = 0x5

.field public static final HOUGH_GRADIENT:I = 0x3

.field public static final HOUGH_MULTI_SCALE:I = 0x2

.field public static final HOUGH_PROBABILISTIC:I = 0x1

.field public static final HOUGH_STANDARD:I = 0x0

.field public static final INTERSECT_FULL:I = 0x2

.field public static final INTERSECT_NONE:I = 0x0

.field public static final INTERSECT_PARTIAL:I = 0x1

.field public static final INTER_AREA:I = 0x3

.field public static final INTER_BITS:I = 0x5

.field public static final INTER_BITS2:I = 0xa

.field public static final INTER_CUBIC:I = 0x2

.field public static final INTER_LANCZOS4:I = 0x4

.field public static final INTER_LINEAR:I = 0x1

.field public static final INTER_MAX:I = 0x7

.field public static final INTER_NEAREST:I = 0x0

.field public static final INTER_TAB_SIZE:I = 0x20

.field public static final INTER_TAB_SIZE2:I = 0x400

.field private static final IPL_BORDER_CONSTANT:I = 0x0

.field private static final IPL_BORDER_REFLECT:I = 0x2

.field private static final IPL_BORDER_REFLECT_101:I = 0x4

.field private static final IPL_BORDER_REPLICATE:I = 0x1

.field private static final IPL_BORDER_TRANSPARENT:I = 0x5

.field private static final IPL_BORDER_WRAP:I = 0x3

.field public static final LINE_4:I = 0x4

.field public static final LINE_8:I = 0x8

.field public static final LINE_AA:I = 0x10

.field public static final LSD_REFINE_ADV:I = 0x2

.field public static final LSD_REFINE_NONE:I = 0x0

.field public static final LSD_REFINE_STD:I = 0x1

.field public static final MARKER_CROSS:I = 0x0

.field public static final MARKER_DIAMOND:I = 0x3

.field public static final MARKER_SQUARE:I = 0x4

.field public static final MARKER_STAR:I = 0x2

.field public static final MARKER_TILTED_CROSS:I = 0x1

.field public static final MARKER_TRIANGLE_DOWN:I = 0x6

.field public static final MARKER_TRIANGLE_UP:I = 0x5

.field public static final MORPH_BLACKHAT:I = 0x6

.field public static final MORPH_CLOSE:I = 0x3

.field public static final MORPH_CROSS:I = 0x1

.field public static final MORPH_DILATE:I = 0x1

.field public static final MORPH_ELLIPSE:I = 0x2

.field public static final MORPH_ERODE:I = 0x0

.field public static final MORPH_GRADIENT:I = 0x4

.field public static final MORPH_HITMISS:I = 0x7

.field public static final MORPH_OPEN:I = 0x2

.field public static final MORPH_RECT:I = 0x0

.field public static final MORPH_TOPHAT:I = 0x5

.field public static final PROJ_SPHERICAL_EQRECT:I = 0x1

.field public static final PROJ_SPHERICAL_ORTHO:I = 0x0

.field public static final RETR_CCOMP:I = 0x2

.field public static final RETR_EXTERNAL:I = 0x0

.field public static final RETR_FLOODFILL:I = 0x4

.field public static final RETR_LIST:I = 0x1

.field public static final RETR_TREE:I = 0x3

.field public static final THRESH_BINARY:I = 0x0

.field public static final THRESH_BINARY_INV:I = 0x1

.field public static final THRESH_MASK:I = 0x7

.field public static final THRESH_OTSU:I = 0x8

.field public static final THRESH_TOZERO:I = 0x3

.field public static final THRESH_TOZERO_INV:I = 0x4

.field public static final THRESH_TRIANGLE:I = 0x10

.field public static final THRESH_TRUNC:I = 0x2

.field public static final TM_CCOEFF:I = 0x4

.field public static final TM_CCOEFF_NORMED:I = 0x5

.field public static final TM_CCORR:I = 0x2

.field public static final TM_CCORR_NORMED:I = 0x3

.field public static final TM_SQDIFF:I = 0x0

.field public static final TM_SQDIFF_NORMED:I = 0x1

.field public static final WARP_FILL_OUTLIERS:I = 0x8

.field public static final WARP_INVERSE_MAP:I = 0x10


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static Canny(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;DD)V
    .locals 8

    .line 984
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v4, p2

    move-wide v6, p4

    invoke-static/range {v0 .. v7}, Lorg/opencv/imgproc/Imgproc;->Canny_1(JJDD)V

    return-void
.end method

.method public static Canny(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;DDIZ)V
    .locals 10

    move-object v0, p0

    .line 975
    iget-wide v0, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object v2, p1

    iget-wide v2, v2, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v4, p2

    move-wide v6, p4

    move/from16 v8, p6

    move/from16 v9, p7

    invoke-static/range {v0 .. v9}, Lorg/opencv/imgproc/Imgproc;->Canny_0(JJDDIZ)V

    return-void
.end method

.method private static native Canny_0(JJDDIZ)V
.end method

.method private static native Canny_1(JJDD)V
.end method

.method public static GaussianBlur(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Size;D)V
    .locals 10

    .line 1016
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Size;->width:D

    iget-wide v6, p2, Lorg/opencv/core/Size;->height:D

    move-wide v8, p3

    invoke-static/range {v0 .. v9}, Lorg/opencv/imgproc/Imgproc;->GaussianBlur_2(JJDDD)V

    return-void
.end method

.method public static GaussianBlur(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Size;DD)V
    .locals 12

    move-object v0, p2

    move-object v1, p0

    .line 1007
    iget-wide v1, v1, Lorg/opencv/core/Mat;->nativeObj:J

    move-object v3, p1

    iget-wide v3, v3, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v5, v0, Lorg/opencv/core/Size;->width:D

    iget-wide v7, v0, Lorg/opencv/core/Size;->height:D

    move-wide v0, v1

    move-wide v2, v3

    move-wide v4, v5

    move-wide v6, v7

    move-wide v8, p3

    move-wide/from16 v10, p5

    invoke-static/range {v0 .. v11}, Lorg/opencv/imgproc/Imgproc;->GaussianBlur_1(JJDDDD)V

    return-void
.end method

.method public static GaussianBlur(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Size;DDI)V
    .locals 13

    move-object v0, p2

    move-object v1, p0

    .line 998
    iget-wide v1, v1, Lorg/opencv/core/Mat;->nativeObj:J

    move-object v3, p1

    iget-wide v3, v3, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v5, v0, Lorg/opencv/core/Size;->width:D

    iget-wide v7, v0, Lorg/opencv/core/Size;->height:D

    move-wide v0, v1

    move-wide v2, v3

    move-wide v4, v5

    move-wide v6, v7

    move-wide/from16 v8, p3

    move-wide/from16 v10, p5

    move/from16 v12, p7

    invoke-static/range {v0 .. v12}, Lorg/opencv/imgproc/Imgproc;->GaussianBlur_0(JJDDDDI)V

    return-void
.end method

.method private static native GaussianBlur_0(JJDDDDI)V
.end method

.method private static native GaussianBlur_1(JJDDDD)V
.end method

.method private static native GaussianBlur_2(JJDDD)V
.end method

.method public static HoughCircles(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;IDD)V
    .locals 9

    .line 1039
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    move v4, p2

    move-wide v5, p3

    move-wide v7, p5

    invoke-static/range {v0 .. v8}, Lorg/opencv/imgproc/Imgproc;->HoughCircles_1(JJIDD)V

    return-void
.end method

.method public static HoughCircles(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;IDDDDII)V
    .locals 15

    move-object v0, p0

    .line 1030
    iget-wide v0, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v2, p1

    iget-wide v2, v2, Lorg/opencv/core/Mat;->nativeObj:J

    move/from16 v4, p2

    move-wide/from16 v5, p3

    move-wide/from16 v7, p5

    move-wide/from16 v9, p7

    move-wide/from16 v11, p9

    move/from16 v13, p11

    move/from16 v14, p12

    invoke-static/range {v0 .. v14}, Lorg/opencv/imgproc/Imgproc;->HoughCircles_0(JJIDDDDII)V

    return-void
.end method

.method private static native HoughCircles_0(JJIDDDDII)V
.end method

.method private static native HoughCircles_1(JJIDD)V
.end method

.method public static HoughLines(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;DDI)V
    .locals 9

    .line 1062
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v4, p2

    move-wide v6, p4

    move v8, p6

    invoke-static/range {v0 .. v8}, Lorg/opencv/imgproc/Imgproc;->HoughLines_1(JJDDI)V

    return-void
.end method

.method public static HoughLines(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;DDIDDDD)V
    .locals 17

    move-wide/from16 v4, p2

    move-wide/from16 v6, p4

    move/from16 v8, p6

    move-wide/from16 v9, p7

    move-wide/from16 v11, p9

    move-wide/from16 v13, p11

    move-wide/from16 v15, p13

    move-object/from16 v0, p0

    .line 1053
    iget-wide v0, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v2, p1

    iget-wide v2, v2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v16}, Lorg/opencv/imgproc/Imgproc;->HoughLines_0(JJDDIDDDD)V

    return-void
.end method

.method public static HoughLinesP(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;DDI)V
    .locals 9

    .line 1085
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v4, p2

    move-wide v6, p4

    move v8, p6

    invoke-static/range {v0 .. v8}, Lorg/opencv/imgproc/Imgproc;->HoughLinesP_1(JJDDI)V

    return-void
.end method

.method public static HoughLinesP(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;DDIDD)V
    .locals 13

    move-object v0, p0

    .line 1076
    iget-wide v0, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object v2, p1

    iget-wide v2, v2, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v4, p2

    move-wide/from16 v6, p4

    move/from16 v8, p6

    move-wide/from16 v9, p7

    move-wide/from16 v11, p9

    invoke-static/range {v0 .. v12}, Lorg/opencv/imgproc/Imgproc;->HoughLinesP_0(JJDDIDD)V

    return-void
.end method

.method private static native HoughLinesP_0(JJDDIDD)V
.end method

.method private static native HoughLinesP_1(JJDDI)V
.end method

.method private static native HoughLines_0(JJDDIDDDD)V
.end method

.method private static native HoughLines_1(JJDDI)V
.end method

.method public static HuMoments(Lorg/opencv/imgproc/Moments;Lorg/opencv/core/Mat;)V
    .locals 25

    move-object/from16 v0, p0

    .line 1099
    iget-wide v1, v0, Lorg/opencv/imgproc/Moments;->m00:D

    iget-wide v3, v0, Lorg/opencv/imgproc/Moments;->m10:D

    iget-wide v5, v0, Lorg/opencv/imgproc/Moments;->m01:D

    iget-wide v7, v0, Lorg/opencv/imgproc/Moments;->m20:D

    iget-wide v9, v0, Lorg/opencv/imgproc/Moments;->m11:D

    iget-wide v11, v0, Lorg/opencv/imgproc/Moments;->m02:D

    iget-wide v13, v0, Lorg/opencv/imgproc/Moments;->m30:D

    move-wide/from16 v23, v1

    iget-wide v1, v0, Lorg/opencv/imgproc/Moments;->m21:D

    move-wide v15, v1

    iget-wide v1, v0, Lorg/opencv/imgproc/Moments;->m12:D

    move-wide/from16 v17, v1

    iget-wide v0, v0, Lorg/opencv/imgproc/Moments;->m03:D

    move-wide/from16 v19, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide/from16 v21, v0

    move-wide/from16 v1, v23

    invoke-static/range {v1 .. v22}, Lorg/opencv/imgproc/Imgproc;->HuMoments_0(DDDDDDDDDDJ)V

    return-void
.end method

.method private static native HuMoments_0(DDDDDDDDDDJ)V
.end method

.method public static Laplacian(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V
    .locals 2

    .line 1131
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1, p2}, Lorg/opencv/imgproc/Imgproc;->Laplacian_2(JJI)V

    return-void
.end method

.method public static Laplacian(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;IIDD)V
    .locals 10

    move-object v0, p0

    .line 1122
    iget-wide v0, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object v2, p1

    iget-wide v2, v2, Lorg/opencv/core/Mat;->nativeObj:J

    move v4, p2

    move v5, p3

    move-wide v6, p4

    move-wide/from16 v8, p6

    invoke-static/range {v0 .. v9}, Lorg/opencv/imgproc/Imgproc;->Laplacian_1(JJIIDD)V

    return-void
.end method

.method public static Laplacian(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;IIDDI)V
    .locals 11

    move-object v0, p0

    .line 1113
    iget-wide v0, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object v2, p1

    iget-wide v2, v2, Lorg/opencv/core/Mat;->nativeObj:J

    move v4, p2

    move v5, p3

    move-wide v6, p4

    move-wide/from16 v8, p6

    move/from16 v10, p8

    invoke-static/range {v0 .. v10}, Lorg/opencv/imgproc/Imgproc;->Laplacian_0(JJIIDDI)V

    return-void
.end method

.method private static native Laplacian_0(JJIIDDI)V
.end method

.method private static native Laplacian_1(JJIIDD)V
.end method

.method private static native Laplacian_2(JJI)V
.end method

.method public static Scharr(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;III)V
    .locals 7

    .line 1163
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    move v4, p2

    move v5, p3

    move v6, p4

    invoke-static/range {v0 .. v6}, Lorg/opencv/imgproc/Imgproc;->Scharr_2(JJIII)V

    return-void
.end method

.method public static Scharr(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;IIIDD)V
    .locals 11

    move-object v0, p0

    .line 1154
    iget-wide v0, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object v2, p1

    iget-wide v2, v2, Lorg/opencv/core/Mat;->nativeObj:J

    move v4, p2

    move v5, p3

    move v6, p4

    move-wide/from16 v7, p5

    move-wide/from16 v9, p7

    invoke-static/range {v0 .. v10}, Lorg/opencv/imgproc/Imgproc;->Scharr_1(JJIIIDD)V

    return-void
.end method

.method public static Scharr(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;IIIDDI)V
    .locals 12

    move-object v0, p0

    .line 1145
    iget-wide v0, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object v2, p1

    iget-wide v2, v2, Lorg/opencv/core/Mat;->nativeObj:J

    move v4, p2

    move v5, p3

    move/from16 v6, p4

    move-wide/from16 v7, p5

    move-wide/from16 v9, p7

    move/from16 v11, p9

    invoke-static/range {v0 .. v11}, Lorg/opencv/imgproc/Imgproc;->Scharr_0(JJIIIDDI)V

    return-void
.end method

.method private static native Scharr_0(JJIIIDDI)V
.end method

.method private static native Scharr_1(JJIIIDD)V
.end method

.method private static native Scharr_2(JJIII)V
.end method

.method public static Sobel(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;III)V
    .locals 7

    .line 1195
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    move v4, p2

    move v5, p3

    move v6, p4

    invoke-static/range {v0 .. v6}, Lorg/opencv/imgproc/Imgproc;->Sobel_2(JJIII)V

    return-void
.end method

.method public static Sobel(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;IIIIDD)V
    .locals 12

    move-object v0, p0

    .line 1186
    iget-wide v0, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object v2, p1

    iget-wide v2, v2, Lorg/opencv/core/Mat;->nativeObj:J

    move v4, p2

    move v5, p3

    move/from16 v6, p4

    move/from16 v7, p5

    move-wide/from16 v8, p6

    move-wide/from16 v10, p8

    invoke-static/range {v0 .. v11}, Lorg/opencv/imgproc/Imgproc;->Sobel_1(JJIIIIDD)V

    return-void
.end method

.method public static Sobel(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;IIIIDDI)V
    .locals 13

    move-object v0, p0

    .line 1177
    iget-wide v0, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object v2, p1

    iget-wide v2, v2, Lorg/opencv/core/Mat;->nativeObj:J

    move v4, p2

    move/from16 v5, p3

    move/from16 v6, p4

    move/from16 v7, p5

    move-wide/from16 v8, p6

    move-wide/from16 v10, p8

    move/from16 v12, p10

    invoke-static/range {v0 .. v12}, Lorg/opencv/imgproc/Imgproc;->Sobel_0(JJIIIIDDI)V

    return-void
.end method

.method private static native Sobel_0(JJIIIIDDI)V
.end method

.method private static native Sobel_1(JJIIIIDD)V
.end method

.method private static native Sobel_2(JJIII)V
.end method

.method public static accumulate(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 2

    .line 1218
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1}, Lorg/opencv/imgproc/Imgproc;->accumulate_1(JJ)V

    return-void
.end method

.method public static accumulate(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 6

    .line 1209
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/imgproc/Imgproc;->accumulate_0(JJJ)V

    return-void
.end method

.method public static accumulateProduct(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 6

    .line 1241
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/imgproc/Imgproc;->accumulateProduct_1(JJJ)V

    return-void
.end method

.method public static accumulateProduct(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 8

    .line 1232
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p3, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v7}, Lorg/opencv/imgproc/Imgproc;->accumulateProduct_0(JJJJ)V

    return-void
.end method

.method private static native accumulateProduct_0(JJJJ)V
.end method

.method private static native accumulateProduct_1(JJJ)V
.end method

.method public static accumulateSquare(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 2

    .line 1264
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1}, Lorg/opencv/imgproc/Imgproc;->accumulateSquare_1(JJ)V

    return-void
.end method

.method public static accumulateSquare(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 6

    .line 1255
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/imgproc/Imgproc;->accumulateSquare_0(JJJ)V

    return-void
.end method

.method private static native accumulateSquare_0(JJJ)V
.end method

.method private static native accumulateSquare_1(JJ)V
.end method

.method public static accumulateWeighted(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;D)V
    .locals 6

    .line 1287
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v4, p2

    invoke-static/range {v0 .. v5}, Lorg/opencv/imgproc/Imgproc;->accumulateWeighted_1(JJD)V

    return-void
.end method

.method public static accumulateWeighted(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;DLorg/opencv/core/Mat;)V
    .locals 8

    .line 1278
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p4, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v4, p2

    invoke-static/range {v0 .. v7}, Lorg/opencv/imgproc/Imgproc;->accumulateWeighted_0(JJDJ)V

    return-void
.end method

.method private static native accumulateWeighted_0(JJDJ)V
.end method

.method private static native accumulateWeighted_1(JJD)V
.end method

.method private static native accumulate_0(JJJ)V
.end method

.method private static native accumulate_1(JJ)V
.end method

.method public static adaptiveThreshold(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;DIIID)V
    .locals 11

    move-object v0, p0

    .line 1301
    iget-wide v0, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object v2, p1

    iget-wide v2, v2, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v4, p2

    move v6, p4

    move/from16 v7, p5

    move/from16 v8, p6

    move-wide/from16 v9, p7

    invoke-static/range {v0 .. v10}, Lorg/opencv/imgproc/Imgproc;->adaptiveThreshold_0(JJDIIID)V

    return-void
.end method

.method private static native adaptiveThreshold_0(JJDIIID)V
.end method

.method public static applyColorMap(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V
    .locals 2

    .line 1315
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1, p2}, Lorg/opencv/imgproc/Imgproc;->applyColorMap_0(JJI)V

    return-void
.end method

.method private static native applyColorMap_0(JJI)V
.end method

.method public static approxPolyDP(Lorg/opencv/core/MatOfPoint2f;Lorg/opencv/core/MatOfPoint2f;DZ)V
    .locals 7

    .line 1330
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v4, p2

    move v6, p4

    invoke-static/range {v0 .. v6}, Lorg/opencv/imgproc/Imgproc;->approxPolyDP_0(JJDZ)V

    return-void
.end method

.method private static native approxPolyDP_0(JJDZ)V
.end method

.method public static arcLength(Lorg/opencv/core/MatOfPoint2f;Z)D
    .locals 2

    .line 739
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p1}, Lorg/opencv/imgproc/Imgproc;->arcLength_0(JZ)D

    move-result-wide p0

    return-wide p0
.end method

.method private static native arcLength_0(JZ)D
.end method

.method public static arrowedLine(Lorg/opencv/core/Mat;Lorg/opencv/core/Point;Lorg/opencv/core/Point;Lorg/opencv/core/Scalar;)V
    .locals 22

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p0

    move-object/from16 v3, p3

    .line 1353
    iget-wide v4, v2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, v0, Lorg/opencv/core/Point;->x:D

    iget-wide v8, v0, Lorg/opencv/core/Point;->y:D

    iget-wide v10, v1, Lorg/opencv/core/Point;->x:D

    iget-wide v12, v1, Lorg/opencv/core/Point;->y:D

    iget-object v0, v3, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x0

    aget-wide v14, v0, v1

    iget-object v0, v3, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x1

    aget-wide v16, v0, v1

    iget-object v0, v3, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x2

    aget-wide v18, v0, v1

    iget-object v0, v3, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x3

    aget-wide v20, v0, v1

    invoke-static/range {v4 .. v21}, Lorg/opencv/imgproc/Imgproc;->arrowedLine_1(JDDDDDDDD)V

    return-void
.end method

.method public static arrowedLine(Lorg/opencv/core/Mat;Lorg/opencv/core/Point;Lorg/opencv/core/Point;Lorg/opencv/core/Scalar;IIID)V
    .locals 26

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move/from16 v21, p4

    move/from16 v22, p5

    move/from16 v23, p6

    move-wide/from16 v24, p7

    move-object/from16 v3, p0

    .line 1344
    iget-wide v3, v3, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v5, v0, Lorg/opencv/core/Point;->x:D

    iget-wide v7, v0, Lorg/opencv/core/Point;->y:D

    iget-wide v9, v1, Lorg/opencv/core/Point;->x:D

    iget-wide v11, v1, Lorg/opencv/core/Point;->y:D

    iget-object v0, v2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x0

    aget-wide v13, v0, v1

    iget-object v0, v2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x1

    aget-wide v15, v0, v1

    iget-object v0, v2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x2

    aget-wide v17, v0, v1

    iget-object v0, v2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x3

    aget-wide v19, v0, v1

    invoke-static/range {v3 .. v25}, Lorg/opencv/imgproc/Imgproc;->arrowedLine_0(JDDDDDDDDIIID)V

    return-void
.end method

.method private static native arrowedLine_0(JDDDDDDDDIIID)V
.end method

.method private static native arrowedLine_1(JDDDDDDDD)V
.end method

.method public static bilateralFilter(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;IDD)V
    .locals 9

    .line 1376
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    move v4, p2

    move-wide v5, p3

    move-wide v7, p5

    invoke-static/range {v0 .. v8}, Lorg/opencv/imgproc/Imgproc;->bilateralFilter_1(JJIDD)V

    return-void
.end method

.method public static bilateralFilter(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;IDDI)V
    .locals 10

    move-object v0, p0

    .line 1367
    iget-wide v0, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object v2, p1

    iget-wide v2, v2, Lorg/opencv/core/Mat;->nativeObj:J

    move v4, p2

    move-wide v5, p3

    move-wide v7, p5

    move/from16 v9, p7

    invoke-static/range {v0 .. v9}, Lorg/opencv/imgproc/Imgproc;->bilateralFilter_0(JJIDDI)V

    return-void
.end method

.method private static native bilateralFilter_0(JJIDDI)V
.end method

.method private static native bilateralFilter_1(JJIDD)V
.end method

.method public static blur(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Size;)V
    .locals 8

    .line 1408
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Size;->width:D

    iget-wide v6, p2, Lorg/opencv/core/Size;->height:D

    invoke-static/range {v0 .. v7}, Lorg/opencv/imgproc/Imgproc;->blur_2(JJDD)V

    return-void
.end method

.method public static blur(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Size;Lorg/opencv/core/Point;)V
    .locals 12

    .line 1399
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Size;->width:D

    iget-wide v6, p2, Lorg/opencv/core/Size;->height:D

    iget-wide v8, p3, Lorg/opencv/core/Point;->x:D

    iget-wide v10, p3, Lorg/opencv/core/Point;->y:D

    invoke-static/range {v0 .. v11}, Lorg/opencv/imgproc/Imgproc;->blur_1(JJDDDD)V

    return-void
.end method

.method public static blur(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Size;Lorg/opencv/core/Point;I)V
    .locals 14

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    move-object v2, p0

    .line 1390
    iget-wide v2, v2, Lorg/opencv/core/Mat;->nativeObj:J

    move-object v4, p1

    iget-wide v4, v4, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, v0, Lorg/opencv/core/Size;->width:D

    iget-wide v8, v0, Lorg/opencv/core/Size;->height:D

    iget-wide v10, v1, Lorg/opencv/core/Point;->x:D

    iget-wide v12, v1, Lorg/opencv/core/Point;->y:D

    move-wide v0, v2

    move-wide v2, v4

    move-wide v4, v6

    move-wide v6, v8

    move-wide v8, v10

    move-wide v10, v12

    move/from16 v12, p4

    invoke-static/range {v0 .. v12}, Lorg/opencv/imgproc/Imgproc;->blur_0(JJDDDDI)V

    return-void
.end method

.method private static native blur_0(JJDDDDI)V
.end method

.method private static native blur_1(JJDDDD)V
.end method

.method private static native blur_2(JJDD)V
.end method

.method public static boundingRect(Lorg/opencv/core/MatOfPoint;)Lorg/opencv/core/Rect;
    .locals 3

    .line 667
    new-instance v0, Lorg/opencv/core/Rect;

    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2}, Lorg/opencv/imgproc/Imgproc;->boundingRect_0(J)[D

    move-result-object p0

    invoke-direct {v0, p0}, Lorg/opencv/core/Rect;-><init>([D)V

    return-object v0
.end method

.method private static native boundingRect_0(J)[D
.end method

.method public static boxFilter(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;ILorg/opencv/core/Size;)V
    .locals 9

    .line 1440
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v5, p3, Lorg/opencv/core/Size;->width:D

    iget-wide v7, p3, Lorg/opencv/core/Size;->height:D

    move v4, p2

    invoke-static/range {v0 .. v8}, Lorg/opencv/imgproc/Imgproc;->boxFilter_2(JJIDD)V

    return-void
.end method

.method public static boxFilter(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;ILorg/opencv/core/Size;Lorg/opencv/core/Point;Z)V
    .locals 14

    move-object/from16 v0, p3

    move-object/from16 v1, p4

    move-object v2, p0

    .line 1431
    iget-wide v2, v2, Lorg/opencv/core/Mat;->nativeObj:J

    move-object v4, p1

    iget-wide v4, v4, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, v0, Lorg/opencv/core/Size;->width:D

    iget-wide v8, v0, Lorg/opencv/core/Size;->height:D

    iget-wide v10, v1, Lorg/opencv/core/Point;->x:D

    iget-wide v12, v1, Lorg/opencv/core/Point;->y:D

    move-wide v0, v2

    move-wide v2, v4

    move/from16 v4, p2

    move-wide v5, v6

    move-wide v7, v8

    move-wide v9, v10

    move-wide v11, v12

    move/from16 v13, p5

    invoke-static/range {v0 .. v13}, Lorg/opencv/imgproc/Imgproc;->boxFilter_1(JJIDDDDZ)V

    return-void
.end method

.method public static boxFilter(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;ILorg/opencv/core/Size;Lorg/opencv/core/Point;ZI)V
    .locals 15

    move-object/from16 v0, p3

    move-object/from16 v1, p4

    move-object v2, p0

    .line 1422
    iget-wide v2, v2, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v4, p1

    iget-wide v4, v4, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, v0, Lorg/opencv/core/Size;->width:D

    iget-wide v8, v0, Lorg/opencv/core/Size;->height:D

    iget-wide v10, v1, Lorg/opencv/core/Point;->x:D

    iget-wide v12, v1, Lorg/opencv/core/Point;->y:D

    move-wide v0, v2

    move-wide v2, v4

    move/from16 v4, p2

    move-wide v5, v6

    move-wide v7, v8

    move-wide v9, v10

    move-wide v11, v12

    move/from16 v13, p5

    move/from16 v14, p6

    invoke-static/range {v0 .. v14}, Lorg/opencv/imgproc/Imgproc;->boxFilter_0(JJIDDDDZI)V

    return-void
.end method

.method private static native boxFilter_0(JJIDDDDZI)V
.end method

.method private static native boxFilter_1(JJIDDDDZ)V
.end method

.method private static native boxFilter_2(JJIDD)V
.end method

.method public static boxPoints(Lorg/opencv/core/RotatedRect;Lorg/opencv/core/Mat;)V
    .locals 13

    .line 1454
    iget-object v0, p0, Lorg/opencv/core/RotatedRect;->center:Lorg/opencv/core/Point;

    iget-wide v1, v0, Lorg/opencv/core/Point;->x:D

    iget-object v0, p0, Lorg/opencv/core/RotatedRect;->center:Lorg/opencv/core/Point;

    iget-wide v3, v0, Lorg/opencv/core/Point;->y:D

    iget-object v0, p0, Lorg/opencv/core/RotatedRect;->size:Lorg/opencv/core/Size;

    iget-wide v5, v0, Lorg/opencv/core/Size;->width:D

    iget-object v0, p0, Lorg/opencv/core/RotatedRect;->size:Lorg/opencv/core/Size;

    iget-wide v7, v0, Lorg/opencv/core/Size;->height:D

    iget-wide v9, p0, Lorg/opencv/core/RotatedRect;->angle:D

    iget-wide v11, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v1 .. v12}, Lorg/opencv/imgproc/Imgproc;->boxPoints_0(DDDDDJ)V

    return-void
.end method

.method private static native boxPoints_0(DDDDDJ)V
.end method

.method public static calcBackProject(Ljava/util/List;Lorg/opencv/core/MatOfInt;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/MatOfFloat;D)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/opencv/core/Mat;",
            ">;",
            "Lorg/opencv/core/MatOfInt;",
            "Lorg/opencv/core/Mat;",
            "Lorg/opencv/core/Mat;",
            "Lorg/opencv/core/MatOfFloat;",
            "D)V"
        }
    .end annotation

    .line 1467
    invoke-static {p0}, Lorg/opencv/utils/Converters;->vector_Mat_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;

    move-result-object v0

    .line 1470
    iget-wide v1, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object v0, p1

    iget-wide v3, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object v0, p2

    iget-wide v5, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p3

    iget-wide v7, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p4

    iget-wide v9, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide/from16 v11, p5

    invoke-static/range {v1 .. v12}, Lorg/opencv/imgproc/Imgproc;->calcBackProject_0(JJJJJD)V

    return-void
.end method

.method private static native calcBackProject_0(JJJJJD)V
.end method

.method public static calcHist(Ljava/util/List;Lorg/opencv/core/MatOfInt;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/MatOfInt;Lorg/opencv/core/MatOfFloat;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/opencv/core/Mat;",
            ">;",
            "Lorg/opencv/core/MatOfInt;",
            "Lorg/opencv/core/Mat;",
            "Lorg/opencv/core/Mat;",
            "Lorg/opencv/core/MatOfInt;",
            "Lorg/opencv/core/MatOfFloat;",
            ")V"
        }
    .end annotation

    .line 1495
    invoke-static {p0}, Lorg/opencv/utils/Converters;->vector_Mat_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;

    move-result-object v0

    .line 1499
    iget-wide v1, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object v0, p1

    iget-wide v3, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object v0, p2

    iget-wide v5, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p3

    iget-wide v7, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p4

    iget-wide v9, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p5

    iget-wide v11, v0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v1 .. v12}, Lorg/opencv/imgproc/Imgproc;->calcHist_1(JJJJJJ)V

    return-void
.end method

.method public static calcHist(Ljava/util/List;Lorg/opencv/core/MatOfInt;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/MatOfInt;Lorg/opencv/core/MatOfFloat;Z)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/opencv/core/Mat;",
            ">;",
            "Lorg/opencv/core/MatOfInt;",
            "Lorg/opencv/core/Mat;",
            "Lorg/opencv/core/Mat;",
            "Lorg/opencv/core/MatOfInt;",
            "Lorg/opencv/core/MatOfFloat;",
            "Z)V"
        }
    .end annotation

    .line 1483
    invoke-static {p0}, Lorg/opencv/utils/Converters;->vector_Mat_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;

    move-result-object v0

    .line 1487
    iget-wide v1, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object v0, p1

    iget-wide v3, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p2

    iget-wide v5, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p3

    iget-wide v7, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p4

    iget-wide v9, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p5

    iget-wide v11, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move/from16 v13, p6

    invoke-static/range {v1 .. v13}, Lorg/opencv/imgproc/Imgproc;->calcHist_0(JJJJJJZ)V

    return-void
.end method

.method private static native calcHist_0(JJJJJJZ)V
.end method

.method private static native calcHist_1(JJJJJJ)V
.end method

.method public static circle(Lorg/opencv/core/Mat;Lorg/opencv/core/Point;ILorg/opencv/core/Scalar;)V
    .locals 17

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    move-object/from16 v2, p3

    .line 1531
    iget-wide v3, v1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v5, v0, Lorg/opencv/core/Point;->x:D

    iget-wide v7, v0, Lorg/opencv/core/Point;->y:D

    iget-object v0, v2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x0

    aget-wide v9, v0, v1

    iget-object v0, v2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x1

    aget-wide v11, v0, v1

    iget-object v0, v2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x2

    aget-wide v13, v0, v1

    iget-object v0, v2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x3

    aget-wide v15, v0, v1

    move-wide v0, v3

    move-wide v2, v5

    move-wide v4, v7

    move/from16 v6, p2

    move-wide v7, v9

    move-wide v9, v11

    move-wide v11, v13

    move-wide v13, v15

    invoke-static/range {v0 .. v14}, Lorg/opencv/imgproc/Imgproc;->circle_2(JDDIDDDD)V

    return-void
.end method

.method public static circle(Lorg/opencv/core/Mat;Lorg/opencv/core/Point;ILorg/opencv/core/Scalar;I)V
    .locals 16

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    move-object/from16 v2, p0

    .line 1522
    iget-wide v2, v2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, v0, Lorg/opencv/core/Point;->x:D

    iget-wide v6, v0, Lorg/opencv/core/Point;->y:D

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v8, 0x0

    aget-wide v8, v0, v8

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v10, 0x1

    aget-wide v10, v0, v10

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v12, 0x2

    aget-wide v12, v0, v12

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x3

    aget-wide v14, v0, v1

    move-wide v0, v2

    move-wide v2, v4

    move-wide v4, v6

    move/from16 v6, p2

    move-wide v7, v8

    move-wide v9, v10

    move-wide v11, v12

    move-wide v13, v14

    move/from16 v15, p4

    invoke-static/range {v0 .. v15}, Lorg/opencv/imgproc/Imgproc;->circle_1(JDDIDDDDI)V

    return-void
.end method

.method public static circle(Lorg/opencv/core/Mat;Lorg/opencv/core/Point;ILorg/opencv/core/Scalar;III)V
    .locals 20

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    move/from16 v8, p2

    move/from16 v17, p4

    move/from16 v18, p5

    move/from16 v19, p6

    move-object/from16 v2, p0

    .line 1513
    iget-wide v2, v2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, v0, Lorg/opencv/core/Point;->x:D

    iget-wide v6, v0, Lorg/opencv/core/Point;->y:D

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v9, 0x0

    aget-wide v9, v0, v9

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v11, 0x1

    aget-wide v11, v0, v11

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v13, 0x2

    aget-wide v13, v0, v13

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x3

    aget-wide v15, v0, v1

    invoke-static/range {v2 .. v19}, Lorg/opencv/imgproc/Imgproc;->circle_0(JDDIDDDDIII)V

    return-void
.end method

.method private static native circle_0(JDDIDDDDIII)V
.end method

.method private static native circle_1(JDDIDDDDI)V
.end method

.method private static native circle_2(JDDIDDDD)V
.end method

.method public static clipLine(Lorg/opencv/core/Rect;Lorg/opencv/core/Point;Lorg/opencv/core/Point;)Z
    .locals 19

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    const/4 v3, 0x2

    .line 708
    new-array v15, v3, [D

    .line 709
    new-array v3, v3, [D

    .line 710
    iget v4, v0, Lorg/opencv/core/Rect;->x:I

    iget v5, v0, Lorg/opencv/core/Rect;->y:I

    iget v6, v0, Lorg/opencv/core/Rect;->width:I

    iget v7, v0, Lorg/opencv/core/Rect;->height:I

    iget-wide v8, v1, Lorg/opencv/core/Point;->x:D

    iget-wide v10, v1, Lorg/opencv/core/Point;->y:D

    iget-wide v13, v2, Lorg/opencv/core/Point;->x:D

    iget-wide v0, v2, Lorg/opencv/core/Point;->y:D

    move-object v12, v15

    move-object/from16 v18, v15

    move-wide v15, v0

    move-object/from16 v17, v3

    invoke-static/range {v4 .. v17}, Lorg/opencv/imgproc/Imgproc;->clipLine_0(IIIIDD[DDD[D)Z

    move-result v0

    const/4 v1, 0x1

    const/4 v4, 0x0

    move-object/from16 v5, p1

    if-eqz v5, :cond_0

    .line 711
    aget-wide v6, v18, v4

    iput-wide v6, v5, Lorg/opencv/core/Point;->x:D

    aget-wide v6, v18, v1

    iput-wide v6, v5, Lorg/opencv/core/Point;->y:D

    :cond_0
    if-eqz v2, :cond_1

    .line 712
    aget-wide v4, v3, v4

    iput-wide v4, v2, Lorg/opencv/core/Point;->x:D

    aget-wide v4, v3, v1

    iput-wide v4, v2, Lorg/opencv/core/Point;->y:D

    :cond_1
    return v0
.end method

.method private static native clipLine_0(IIIIDD[DDD[D)Z
.end method

.method public static compareHist(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)D
    .locals 2

    .line 753
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1, p2}, Lorg/opencv/imgproc/Imgproc;->compareHist_0(JJI)D

    move-result-wide p0

    return-wide p0
.end method

.method private static native compareHist_0(JJI)D
.end method

.method public static connectedComponents(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)I
    .locals 2

    .line 901
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1}, Lorg/opencv/imgproc/Imgproc;->connectedComponents_1(JJ)I

    move-result p0

    return p0
.end method

.method public static connectedComponents(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;II)I
    .locals 6

    .line 892
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lorg/opencv/imgproc/Imgproc;->connectedComponents_0(JJII)I

    move-result p0

    return p0
.end method

.method public static connectedComponentsWithStats(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)I
    .locals 8

    .line 924
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p3, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v7}, Lorg/opencv/imgproc/Imgproc;->connectedComponentsWithStats_1(JJJJ)I

    move-result p0

    return p0
.end method

.method public static connectedComponentsWithStats(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;II)I
    .locals 10

    .line 915
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p3, Lorg/opencv/core/Mat;->nativeObj:J

    move v8, p4

    move v9, p5

    invoke-static/range {v0 .. v9}, Lorg/opencv/imgproc/Imgproc;->connectedComponentsWithStats_0(JJJJII)I

    move-result p0

    return p0
.end method

.method private static native connectedComponentsWithStats_0(JJJJII)I
.end method

.method private static native connectedComponentsWithStats_1(JJJJ)I
.end method

.method private static native connectedComponents_0(JJII)I
.end method

.method private static native connectedComponents_1(JJ)I
.end method

.method public static contourArea(Lorg/opencv/core/Mat;)D
    .locals 2

    .line 776
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1}, Lorg/opencv/imgproc/Imgproc;->contourArea_1(J)D

    move-result-wide v0

    return-wide v0
.end method

.method public static contourArea(Lorg/opencv/core/Mat;Z)D
    .locals 2

    .line 767
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p1}, Lorg/opencv/imgproc/Imgproc;->contourArea_0(JZ)D

    move-result-wide p0

    return-wide p0
.end method

.method private static native contourArea_0(JZ)D
.end method

.method private static native contourArea_1(J)D
.end method

.method public static convertMaps(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V
    .locals 9

    .line 1554
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p3, Lorg/opencv/core/Mat;->nativeObj:J

    move v8, p4

    invoke-static/range {v0 .. v8}, Lorg/opencv/imgproc/Imgproc;->convertMaps_1(JJJJI)V

    return-void
.end method

.method public static convertMaps(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;IZ)V
    .locals 10

    .line 1545
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p3, Lorg/opencv/core/Mat;->nativeObj:J

    move v8, p4

    move v9, p5

    invoke-static/range {v0 .. v9}, Lorg/opencv/imgproc/Imgproc;->convertMaps_0(JJJJIZ)V

    return-void
.end method

.method private static native convertMaps_0(JJJJIZ)V
.end method

.method private static native convertMaps_1(JJJJI)V
.end method

.method public static convexHull(Lorg/opencv/core/MatOfPoint;Lorg/opencv/core/MatOfInt;)V
    .locals 2

    .line 1579
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1}, Lorg/opencv/imgproc/Imgproc;->convexHull_1(JJ)V

    return-void
.end method

.method public static convexHull(Lorg/opencv/core/MatOfPoint;Lorg/opencv/core/MatOfInt;Z)V
    .locals 2

    .line 1569
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1, p2}, Lorg/opencv/imgproc/Imgproc;->convexHull_0(JJZ)V

    return-void
.end method

.method private static native convexHull_0(JJZ)V
.end method

.method private static native convexHull_1(JJ)V
.end method

.method public static convexityDefects(Lorg/opencv/core/MatOfPoint;Lorg/opencv/core/MatOfInt;Lorg/opencv/core/MatOfInt4;)V
    .locals 6

    .line 1595
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/imgproc/Imgproc;->convexityDefects_0(JJJ)V

    return-void
.end method

.method private static native convexityDefects_0(JJJ)V
.end method

.method public static cornerEigenValsAndVecs(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;II)V
    .locals 6

    .line 1618
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lorg/opencv/imgproc/Imgproc;->cornerEigenValsAndVecs_1(JJII)V

    return-void
.end method

.method public static cornerEigenValsAndVecs(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;III)V
    .locals 7

    .line 1609
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    move v4, p2

    move v5, p3

    move v6, p4

    invoke-static/range {v0 .. v6}, Lorg/opencv/imgproc/Imgproc;->cornerEigenValsAndVecs_0(JJIII)V

    return-void
.end method

.method private static native cornerEigenValsAndVecs_0(JJIII)V
.end method

.method private static native cornerEigenValsAndVecs_1(JJII)V
.end method

.method public static cornerHarris(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;IID)V
    .locals 8

    .line 1641
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    move v4, p2

    move v5, p3

    move-wide v6, p4

    invoke-static/range {v0 .. v7}, Lorg/opencv/imgproc/Imgproc;->cornerHarris_1(JJIID)V

    return-void
.end method

.method public static cornerHarris(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;IIDI)V
    .locals 9

    .line 1632
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    move v4, p2

    move v5, p3

    move-wide v6, p4

    move v8, p6

    invoke-static/range {v0 .. v8}, Lorg/opencv/imgproc/Imgproc;->cornerHarris_0(JJIIDI)V

    return-void
.end method

.method private static native cornerHarris_0(JJIIDI)V
.end method

.method private static native cornerHarris_1(JJIID)V
.end method

.method public static cornerMinEigenVal(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V
    .locals 2

    .line 1673
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1, p2}, Lorg/opencv/imgproc/Imgproc;->cornerMinEigenVal_2(JJI)V

    return-void
.end method

.method public static cornerMinEigenVal(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;II)V
    .locals 6

    .line 1664
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lorg/opencv/imgproc/Imgproc;->cornerMinEigenVal_1(JJII)V

    return-void
.end method

.method public static cornerMinEigenVal(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;III)V
    .locals 7

    .line 1655
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    move v4, p2

    move v5, p3

    move v6, p4

    invoke-static/range {v0 .. v6}, Lorg/opencv/imgproc/Imgproc;->cornerMinEigenVal_0(JJIII)V

    return-void
.end method

.method private static native cornerMinEigenVal_0(JJIII)V
.end method

.method private static native cornerMinEigenVal_1(JJII)V
.end method

.method private static native cornerMinEigenVal_2(JJI)V
.end method

.method public static cornerSubPix(Lorg/opencv/core/Mat;Lorg/opencv/core/MatOfPoint2f;Lorg/opencv/core/Size;Lorg/opencv/core/Size;Lorg/opencv/core/TermCriteria;)V
    .locals 18

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    move-object/from16 v2, p0

    move-object/from16 v3, p4

    .line 1687
    iget-wide v4, v2, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v2, p1

    iget-wide v6, v2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v8, v0, Lorg/opencv/core/Size;->width:D

    iget-wide v10, v0, Lorg/opencv/core/Size;->height:D

    iget-wide v12, v1, Lorg/opencv/core/Size;->width:D

    iget-wide v14, v1, Lorg/opencv/core/Size;->height:D

    iget v2, v3, Lorg/opencv/core/TermCriteria;->type:I

    iget v0, v3, Lorg/opencv/core/TermCriteria;->maxCount:I

    move-wide/from16 p0, v14

    iget-wide v14, v3, Lorg/opencv/core/TermCriteria;->epsilon:D

    move/from16 v16, v0

    move-wide v0, v4

    move/from16 v17, v2

    move-wide v2, v6

    move-wide v4, v8

    move-wide v6, v10

    move-wide v8, v12

    move-wide/from16 v10, p0

    move/from16 v12, v17

    move/from16 v13, v16

    invoke-static/range {v0 .. v15}, Lorg/opencv/imgproc/Imgproc;->cornerSubPix_0(JJDDDDIID)V

    return-void
.end method

.method private static native cornerSubPix_0(JJDDDDIID)V
.end method

.method public static createCLAHE()Lorg/opencv/imgproc/CLAHE;
    .locals 3

    .line 630
    new-instance v0, Lorg/opencv/imgproc/CLAHE;

    invoke-static {}, Lorg/opencv/imgproc/Imgproc;->createCLAHE_1()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lorg/opencv/imgproc/CLAHE;-><init>(J)V

    return-object v0
.end method

.method public static createCLAHE(DLorg/opencv/core/Size;)Lorg/opencv/imgproc/CLAHE;
    .locals 7

    .line 621
    new-instance v0, Lorg/opencv/imgproc/CLAHE;

    iget-wide v3, p2, Lorg/opencv/core/Size;->width:D

    iget-wide v5, p2, Lorg/opencv/core/Size;->height:D

    move-wide v1, p0

    invoke-static/range {v1 .. v6}, Lorg/opencv/imgproc/Imgproc;->createCLAHE_0(DDD)J

    move-result-wide p0

    invoke-direct {v0, p0, p1}, Lorg/opencv/imgproc/CLAHE;-><init>(J)V

    return-object v0
.end method

.method private static native createCLAHE_0(DDD)J
.end method

.method private static native createCLAHE_1()J
.end method

.method public static createHanningWindow(Lorg/opencv/core/Mat;Lorg/opencv/core/Size;I)V
    .locals 7

    .line 1701
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Size;->width:D

    iget-wide v4, p1, Lorg/opencv/core/Size;->height:D

    move v6, p2

    invoke-static/range {v0 .. v6}, Lorg/opencv/imgproc/Imgproc;->createHanningWindow_0(JDDI)V

    return-void
.end method

.method private static native createHanningWindow_0(JDDI)V
.end method

.method public static createLineSegmentDetector()Lorg/opencv/imgproc/LineSegmentDetector;
    .locals 3

    .line 653
    new-instance v0, Lorg/opencv/imgproc/LineSegmentDetector;

    invoke-static {}, Lorg/opencv/imgproc/Imgproc;->createLineSegmentDetector_1()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lorg/opencv/imgproc/LineSegmentDetector;-><init>(J)V

    return-object v0
.end method

.method public static createLineSegmentDetector(IDDDDDDI)Lorg/opencv/imgproc/LineSegmentDetector;
    .locals 1

    .line 644
    new-instance v0, Lorg/opencv/imgproc/LineSegmentDetector;

    invoke-static/range {p0 .. p13}, Lorg/opencv/imgproc/Imgproc;->createLineSegmentDetector_0(IDDDDDDI)J

    move-result-wide p0

    invoke-direct {v0, p0, p1}, Lorg/opencv/imgproc/LineSegmentDetector;-><init>(J)V

    return-object v0
.end method

.method private static native createLineSegmentDetector_0(IDDDDDDI)J
.end method

.method private static native createLineSegmentDetector_1()J
.end method

.method public static cvtColor(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V
    .locals 2

    .line 1724
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1, p2}, Lorg/opencv/imgproc/Imgproc;->cvtColor_1(JJI)V

    return-void
.end method

.method public static cvtColor(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;II)V
    .locals 6

    .line 1715
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lorg/opencv/imgproc/Imgproc;->cvtColor_0(JJII)V

    return-void
.end method

.method private static native cvtColor_0(JJII)V
.end method

.method private static native cvtColor_1(JJI)V
.end method

.method public static demosaicing(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V
    .locals 2

    .line 1747
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1, p2}, Lorg/opencv/imgproc/Imgproc;->demosaicing_1(JJI)V

    return-void
.end method

.method public static demosaicing(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;II)V
    .locals 6

    .line 1738
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lorg/opencv/imgproc/Imgproc;->demosaicing_0(JJII)V

    return-void
.end method

.method private static native demosaicing_0(JJII)V
.end method

.method private static native demosaicing_1(JJI)V
.end method

.method public static dilate(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 6

    .line 1779
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/imgproc/Imgproc;->dilate_2(JJJ)V

    return-void
.end method

.method public static dilate(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Point;I)V
    .locals 11

    .line 1770
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p3, Lorg/opencv/core/Point;->x:D

    iget-wide v8, p3, Lorg/opencv/core/Point;->y:D

    move v10, p4

    invoke-static/range {v0 .. v10}, Lorg/opencv/imgproc/Imgproc;->dilate_1(JJJDDI)V

    return-void
.end method

.method public static dilate(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Point;IILorg/opencv/core/Scalar;)V
    .locals 22

    move-object/from16 v0, p3

    move-object/from16 v1, p6

    move/from16 v12, p4

    move/from16 v13, p5

    move-object/from16 v2, p0

    .line 1761
    iget-wide v2, v2, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v4, p1

    iget-wide v4, v4, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v6, p2

    iget-wide v6, v6, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v8, v0, Lorg/opencv/core/Point;->x:D

    iget-wide v10, v0, Lorg/opencv/core/Point;->y:D

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v14, 0x0

    aget-wide v14, v0, v14

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/16 v16, 0x1

    aget-wide v16, v0, v16

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/16 v18, 0x2

    aget-wide v18, v0, v18

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x3

    aget-wide v20, v0, v1

    invoke-static/range {v2 .. v21}, Lorg/opencv/imgproc/Imgproc;->dilate_0(JJJDDIIDDDD)V

    return-void
.end method

.method private static native dilate_0(JJJDDIIDDDD)V
.end method

.method private static native dilate_1(JJJDDI)V
.end method

.method private static native dilate_2(JJJ)V
.end method

.method public static distanceTransform(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;II)V
    .locals 6

    .line 1825
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lorg/opencv/imgproc/Imgproc;->distanceTransform_1(JJII)V

    return-void
.end method

.method public static distanceTransform(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;III)V
    .locals 7

    .line 1816
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    move v4, p2

    move v5, p3

    move v6, p4

    invoke-static/range {v0 .. v6}, Lorg/opencv/imgproc/Imgproc;->distanceTransform_0(JJIII)V

    return-void
.end method

.method public static distanceTransformWithLabels(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;II)V
    .locals 8

    .line 1802
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    move v6, p3

    move v7, p4

    invoke-static/range {v0 .. v7}, Lorg/opencv/imgproc/Imgproc;->distanceTransformWithLabels_1(JJJII)V

    return-void
.end method

.method public static distanceTransformWithLabels(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;III)V
    .locals 9

    .line 1793
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    move v6, p3

    move v7, p4

    move v8, p5

    invoke-static/range {v0 .. v8}, Lorg/opencv/imgproc/Imgproc;->distanceTransformWithLabels_0(JJJIII)V

    return-void
.end method

.method private static native distanceTransformWithLabels_0(JJJIII)V
.end method

.method private static native distanceTransformWithLabels_1(JJJII)V
.end method

.method private static native distanceTransform_0(JJIII)V
.end method

.method private static native distanceTransform_1(JJII)V
.end method

.method public static drawContours(Lorg/opencv/core/Mat;Ljava/util/List;ILorg/opencv/core/Scalar;)V
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List<",
            "Lorg/opencv/core/MatOfPoint;",
            ">;I",
            "Lorg/opencv/core/Scalar;",
            ")V"
        }
    .end annotation

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    .line 1858
    new-instance v2, Ljava/util/ArrayList;

    const/4 v3, 0x0

    if-eqz v0, :cond_0

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v4

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    :goto_0
    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 1859
    invoke-static {v0, v2}, Lorg/opencv/utils/Converters;->vector_vector_Point_to_Mat(Ljava/util/List;Ljava/util/List;)Lorg/opencv/core/Mat;

    move-result-object v0

    move-object/from16 v2, p0

    .line 1860
    iget-wide v4, v2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, v0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    aget-wide v9, v0, v3

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v2, 0x1

    aget-wide v11, v0, v2

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v2, 0x2

    aget-wide v13, v0, v2

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x3

    aget-wide v15, v0, v1

    move/from16 v8, p2

    invoke-static/range {v4 .. v16}, Lorg/opencv/imgproc/Imgproc;->drawContours_2(JJIDDDD)V

    return-void
.end method

.method public static drawContours(Lorg/opencv/core/Mat;Ljava/util/List;ILorg/opencv/core/Scalar;I)V
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List<",
            "Lorg/opencv/core/MatOfPoint;",
            ">;I",
            "Lorg/opencv/core/Scalar;",
            "I)V"
        }
    .end annotation

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    .line 1848
    new-instance v2, Ljava/util/ArrayList;

    const/4 v3, 0x0

    if-eqz v0, :cond_0

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v4

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    :goto_0
    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 1849
    invoke-static {v0, v2}, Lorg/opencv/utils/Converters;->vector_vector_Point_to_Mat(Ljava/util/List;Ljava/util/List;)Lorg/opencv/core/Mat;

    move-result-object v0

    move-object/from16 v2, p0

    .line 1850
    iget-wide v4, v2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, v0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    aget-wide v9, v0, v3

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v2, 0x1

    aget-wide v11, v0, v2

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v2, 0x2

    aget-wide v13, v0, v2

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x3

    aget-wide v15, v0, v1

    move/from16 v8, p2

    move/from16 v17, p4

    invoke-static/range {v4 .. v17}, Lorg/opencv/imgproc/Imgproc;->drawContours_1(JJIDDDDI)V

    return-void
.end method

.method public static drawContours(Lorg/opencv/core/Mat;Ljava/util/List;ILorg/opencv/core/Scalar;IILorg/opencv/core/Mat;ILorg/opencv/core/Point;)V
    .locals 27
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List<",
            "Lorg/opencv/core/MatOfPoint;",
            ">;I",
            "Lorg/opencv/core/Scalar;",
            "II",
            "Lorg/opencv/core/Mat;",
            "I",
            "Lorg/opencv/core/Point;",
            ")V"
        }
    .end annotation

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    move-object/from16 v2, p8

    .line 1838
    new-instance v3, Ljava/util/ArrayList;

    const/4 v4, 0x0

    if-eqz v0, :cond_0

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v5

    goto :goto_0

    :cond_0
    const/4 v5, 0x0

    :goto_0
    invoke-direct {v3, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 1839
    invoke-static {v0, v3}, Lorg/opencv/utils/Converters;->vector_vector_Point_to_Mat(Ljava/util/List;Ljava/util/List;)Lorg/opencv/core/Mat;

    move-result-object v0

    move-object/from16 v3, p0

    .line 1840
    iget-wide v5, v3, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v7, v0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    aget-wide v10, v0, v4

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v3, 0x1

    aget-wide v12, v0, v3

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v3, 0x2

    aget-wide v14, v0, v3

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x3

    aget-wide v16, v0, v1

    move-object/from16 v0, p6

    iget-wide v0, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide/from16 v20, v0

    iget-wide v0, v2, Lorg/opencv/core/Point;->x:D

    move-wide/from16 v23, v0

    iget-wide v0, v2, Lorg/opencv/core/Point;->y:D

    move-wide/from16 v25, v0

    move/from16 v9, p2

    move/from16 v18, p4

    move/from16 v19, p5

    move/from16 v22, p7

    invoke-static/range {v5 .. v26}, Lorg/opencv/imgproc/Imgproc;->drawContours_0(JJIDDDDIIJIDD)V

    return-void
.end method

.method private static native drawContours_0(JJIDDDDIIJIDD)V
.end method

.method private static native drawContours_1(JJIDDDDI)V
.end method

.method private static native drawContours_2(JJIDDDD)V
.end method

.method public static drawMarker(Lorg/opencv/core/Mat;Lorg/opencv/core/Point;Lorg/opencv/core/Scalar;)V
    .locals 17

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    .line 1883
    iget-wide v3, v1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v5, v0, Lorg/opencv/core/Point;->x:D

    iget-wide v7, v0, Lorg/opencv/core/Point;->y:D

    iget-object v0, v2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x0

    aget-wide v9, v0, v1

    iget-object v0, v2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x1

    aget-wide v11, v0, v1

    iget-object v0, v2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x2

    aget-wide v13, v0, v1

    iget-object v0, v2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x3

    aget-wide v15, v0, v1

    move-wide v0, v3

    move-wide v2, v5

    move-wide v4, v7

    move-wide v6, v9

    move-wide v8, v11

    move-wide v10, v13

    move-wide v12, v15

    invoke-static/range {v0 .. v13}, Lorg/opencv/imgproc/Imgproc;->drawMarker_1(JDDDDDD)V

    return-void
.end method

.method public static drawMarker(Lorg/opencv/core/Mat;Lorg/opencv/core/Point;Lorg/opencv/core/Scalar;IIII)V
    .locals 20

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move/from16 v16, p3

    move/from16 v17, p4

    move/from16 v18, p5

    move/from16 v19, p6

    move-object/from16 v2, p0

    .line 1874
    iget-wide v2, v2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, v0, Lorg/opencv/core/Point;->x:D

    iget-wide v6, v0, Lorg/opencv/core/Point;->y:D

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v8, 0x0

    aget-wide v8, v0, v8

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v10, 0x1

    aget-wide v10, v0, v10

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v12, 0x2

    aget-wide v12, v0, v12

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x3

    aget-wide v14, v0, v1

    invoke-static/range {v2 .. v19}, Lorg/opencv/imgproc/Imgproc;->drawMarker_0(JDDDDDDIIII)V

    return-void
.end method

.method private static native drawMarker_0(JDDDDDDIIII)V
.end method

.method private static native drawMarker_1(JDDDDDD)V
.end method

.method public static ellipse(Lorg/opencv/core/Mat;Lorg/opencv/core/Point;Lorg/opencv/core/Size;DDDLorg/opencv/core/Scalar;)V
    .locals 27

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p9

    move-wide/from16 v13, p3

    move-wide/from16 v15, p5

    move-wide/from16 v17, p7

    move-object/from16 v3, p0

    .line 1915
    iget-wide v3, v3, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v5, v0, Lorg/opencv/core/Point;->x:D

    iget-wide v7, v0, Lorg/opencv/core/Point;->y:D

    iget-wide v9, v1, Lorg/opencv/core/Size;->width:D

    iget-wide v11, v1, Lorg/opencv/core/Size;->height:D

    iget-object v0, v2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x0

    aget-wide v19, v0, v1

    iget-object v0, v2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x1

    aget-wide v21, v0, v1

    iget-object v0, v2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x2

    aget-wide v23, v0, v1

    iget-object v0, v2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x3

    aget-wide v25, v0, v1

    invoke-static/range {v3 .. v26}, Lorg/opencv/imgproc/Imgproc;->ellipse_2(JDDDDDDDDDDD)V

    return-void
.end method

.method public static ellipse(Lorg/opencv/core/Mat;Lorg/opencv/core/Point;Lorg/opencv/core/Size;DDDLorg/opencv/core/Scalar;I)V
    .locals 28

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p9

    move-wide/from16 v13, p3

    move-wide/from16 v15, p5

    move-wide/from16 v17, p7

    move/from16 v27, p10

    move-object/from16 v3, p0

    .line 1906
    iget-wide v3, v3, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v5, v0, Lorg/opencv/core/Point;->x:D

    iget-wide v7, v0, Lorg/opencv/core/Point;->y:D

    iget-wide v9, v1, Lorg/opencv/core/Size;->width:D

    iget-wide v11, v1, Lorg/opencv/core/Size;->height:D

    iget-object v0, v2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x0

    aget-wide v19, v0, v1

    iget-object v0, v2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x1

    aget-wide v21, v0, v1

    iget-object v0, v2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x2

    aget-wide v23, v0, v1

    iget-object v0, v2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x3

    aget-wide v25, v0, v1

    invoke-static/range {v3 .. v27}, Lorg/opencv/imgproc/Imgproc;->ellipse_1(JDDDDDDDDDDDI)V

    return-void
.end method

.method public static ellipse(Lorg/opencv/core/Mat;Lorg/opencv/core/Point;Lorg/opencv/core/Size;DDDLorg/opencv/core/Scalar;III)V
    .locals 30

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p9

    move-wide/from16 v13, p3

    move-wide/from16 v15, p5

    move-wide/from16 v17, p7

    move/from16 v27, p10

    move/from16 v28, p11

    move/from16 v29, p12

    move-object/from16 v3, p0

    .line 1897
    iget-wide v3, v3, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v5, v0, Lorg/opencv/core/Point;->x:D

    iget-wide v7, v0, Lorg/opencv/core/Point;->y:D

    iget-wide v9, v1, Lorg/opencv/core/Size;->width:D

    iget-wide v11, v1, Lorg/opencv/core/Size;->height:D

    iget-object v0, v2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x0

    aget-wide v19, v0, v1

    iget-object v0, v2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x1

    aget-wide v21, v0, v1

    iget-object v0, v2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x2

    aget-wide v23, v0, v1

    iget-object v0, v2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x3

    aget-wide v25, v0, v1

    invoke-static/range {v3 .. v29}, Lorg/opencv/imgproc/Imgproc;->ellipse_0(JDDDDDDDDDDDIII)V

    return-void
.end method

.method public static ellipse(Lorg/opencv/core/Mat;Lorg/opencv/core/RotatedRect;Lorg/opencv/core/Scalar;)V
    .locals 23

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    .line 1947
    iget-wide v3, v1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-object v1, v0, Lorg/opencv/core/RotatedRect;->center:Lorg/opencv/core/Point;

    iget-wide v5, v1, Lorg/opencv/core/Point;->x:D

    iget-object v1, v0, Lorg/opencv/core/RotatedRect;->center:Lorg/opencv/core/Point;

    iget-wide v7, v1, Lorg/opencv/core/Point;->y:D

    iget-object v1, v0, Lorg/opencv/core/RotatedRect;->size:Lorg/opencv/core/Size;

    iget-wide v9, v1, Lorg/opencv/core/Size;->width:D

    iget-object v1, v0, Lorg/opencv/core/RotatedRect;->size:Lorg/opencv/core/Size;

    iget-wide v11, v1, Lorg/opencv/core/Size;->height:D

    iget-wide v13, v0, Lorg/opencv/core/RotatedRect;->angle:D

    iget-object v0, v2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x0

    aget-wide v15, v0, v1

    iget-object v0, v2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x1

    aget-wide v17, v0, v1

    iget-object v0, v2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x2

    aget-wide v19, v0, v1

    iget-object v0, v2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x3

    aget-wide v21, v0, v1

    invoke-static/range {v3 .. v22}, Lorg/opencv/imgproc/Imgproc;->ellipse_5(JDDDDDDDDD)V

    return-void
.end method

.method public static ellipse(Lorg/opencv/core/Mat;Lorg/opencv/core/RotatedRect;Lorg/opencv/core/Scalar;I)V
    .locals 23

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move/from16 v22, p3

    move-object/from16 v2, p0

    .line 1938
    iget-wide v2, v2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-object v4, v0, Lorg/opencv/core/RotatedRect;->center:Lorg/opencv/core/Point;

    iget-wide v4, v4, Lorg/opencv/core/Point;->x:D

    iget-object v6, v0, Lorg/opencv/core/RotatedRect;->center:Lorg/opencv/core/Point;

    iget-wide v6, v6, Lorg/opencv/core/Point;->y:D

    iget-object v8, v0, Lorg/opencv/core/RotatedRect;->size:Lorg/opencv/core/Size;

    iget-wide v8, v8, Lorg/opencv/core/Size;->width:D

    iget-object v10, v0, Lorg/opencv/core/RotatedRect;->size:Lorg/opencv/core/Size;

    iget-wide v10, v10, Lorg/opencv/core/Size;->height:D

    iget-wide v12, v0, Lorg/opencv/core/RotatedRect;->angle:D

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v14, 0x0

    aget-wide v14, v0, v14

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/16 v16, 0x1

    aget-wide v16, v0, v16

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/16 v18, 0x2

    aget-wide v18, v0, v18

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x3

    aget-wide v20, v0, v1

    invoke-static/range {v2 .. v22}, Lorg/opencv/imgproc/Imgproc;->ellipse_4(JDDDDDDDDDI)V

    return-void
.end method

.method public static ellipse(Lorg/opencv/core/Mat;Lorg/opencv/core/RotatedRect;Lorg/opencv/core/Scalar;II)V
    .locals 24

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move/from16 v22, p3

    move/from16 v23, p4

    move-object/from16 v2, p0

    .line 1929
    iget-wide v2, v2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-object v4, v0, Lorg/opencv/core/RotatedRect;->center:Lorg/opencv/core/Point;

    iget-wide v4, v4, Lorg/opencv/core/Point;->x:D

    iget-object v6, v0, Lorg/opencv/core/RotatedRect;->center:Lorg/opencv/core/Point;

    iget-wide v6, v6, Lorg/opencv/core/Point;->y:D

    iget-object v8, v0, Lorg/opencv/core/RotatedRect;->size:Lorg/opencv/core/Size;

    iget-wide v8, v8, Lorg/opencv/core/Size;->width:D

    iget-object v10, v0, Lorg/opencv/core/RotatedRect;->size:Lorg/opencv/core/Size;

    iget-wide v10, v10, Lorg/opencv/core/Size;->height:D

    iget-wide v12, v0, Lorg/opencv/core/RotatedRect;->angle:D

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v14, 0x0

    aget-wide v14, v0, v14

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/16 v16, 0x1

    aget-wide v16, v0, v16

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/16 v18, 0x2

    aget-wide v18, v0, v18

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x3

    aget-wide v20, v0, v1

    invoke-static/range {v2 .. v23}, Lorg/opencv/imgproc/Imgproc;->ellipse_3(JDDDDDDDDDII)V

    return-void
.end method

.method public static ellipse2Poly(Lorg/opencv/core/Point;Lorg/opencv/core/Size;IIIILorg/opencv/core/MatOfPoint;)V
    .locals 14

    move-object v0, p0

    move-object v1, p1

    .line 1961
    iget-wide v2, v0, Lorg/opencv/core/Point;->x:D

    iget-wide v4, v0, Lorg/opencv/core/Point;->y:D

    iget-wide v6, v1, Lorg/opencv/core/Size;->width:D

    iget-wide v8, v1, Lorg/opencv/core/Size;->height:D

    move-object/from16 v0, p6

    iget-wide v12, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v0, v2

    move-wide v2, v4

    move-wide v4, v6

    move-wide v6, v8

    move/from16 v8, p2

    move/from16 v9, p3

    move/from16 v10, p4

    move/from16 v11, p5

    invoke-static/range {v0 .. v13}, Lorg/opencv/imgproc/Imgproc;->ellipse2Poly_0(DDDDIIIIJ)V

    return-void
.end method

.method private static native ellipse2Poly_0(DDDDIIIIJ)V
.end method

.method private static native ellipse_0(JDDDDDDDDDDDIII)V
.end method

.method private static native ellipse_1(JDDDDDDDDDDDI)V
.end method

.method private static native ellipse_2(JDDDDDDDDDDD)V
.end method

.method private static native ellipse_3(JDDDDDDDDDII)V
.end method

.method private static native ellipse_4(JDDDDDDDDDI)V
.end method

.method private static native ellipse_5(JDDDDDDDDD)V
.end method

.method public static equalizeHist(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 2

    .line 1975
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1}, Lorg/opencv/imgproc/Imgproc;->equalizeHist_0(JJ)V

    return-void
.end method

.method private static native equalizeHist_0(JJ)V
.end method

.method public static erode(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 6

    .line 2007
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/imgproc/Imgproc;->erode_2(JJJ)V

    return-void
.end method

.method public static erode(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Point;I)V
    .locals 11

    .line 1998
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p3, Lorg/opencv/core/Point;->x:D

    iget-wide v8, p3, Lorg/opencv/core/Point;->y:D

    move v10, p4

    invoke-static/range {v0 .. v10}, Lorg/opencv/imgproc/Imgproc;->erode_1(JJJDDI)V

    return-void
.end method

.method public static erode(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Point;IILorg/opencv/core/Scalar;)V
    .locals 22

    move-object/from16 v0, p3

    move-object/from16 v1, p6

    move/from16 v12, p4

    move/from16 v13, p5

    move-object/from16 v2, p0

    .line 1989
    iget-wide v2, v2, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v4, p1

    iget-wide v4, v4, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v6, p2

    iget-wide v6, v6, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v8, v0, Lorg/opencv/core/Point;->x:D

    iget-wide v10, v0, Lorg/opencv/core/Point;->y:D

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v14, 0x0

    aget-wide v14, v0, v14

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/16 v16, 0x1

    aget-wide v16, v0, v16

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/16 v18, 0x2

    aget-wide v18, v0, v18

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x3

    aget-wide v20, v0, v1

    invoke-static/range {v2 .. v21}, Lorg/opencv/imgproc/Imgproc;->erode_0(JJJDDIIDDDD)V

    return-void
.end method

.method private static native erode_0(JJJDDIIDDDD)V
.end method

.method private static native erode_1(JJJDDI)V
.end method

.method private static native erode_2(JJJ)V
.end method

.method public static fillConvexPoly(Lorg/opencv/core/Mat;Lorg/opencv/core/MatOfPoint;Lorg/opencv/core/Scalar;)V
    .locals 12

    .line 2030
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-object p0, p2, Lorg/opencv/core/Scalar;->val:[D

    const/4 p1, 0x0

    aget-wide v4, p0, p1

    iget-object p0, p2, Lorg/opencv/core/Scalar;->val:[D

    const/4 p1, 0x1

    aget-wide v6, p0, p1

    iget-object p0, p2, Lorg/opencv/core/Scalar;->val:[D

    const/4 p1, 0x2

    aget-wide v8, p0, p1

    iget-object p0, p2, Lorg/opencv/core/Scalar;->val:[D

    const/4 p1, 0x3

    aget-wide v10, p0, p1

    invoke-static/range {v0 .. v11}, Lorg/opencv/imgproc/Imgproc;->fillConvexPoly_1(JJDDDD)V

    return-void
.end method

.method public static fillConvexPoly(Lorg/opencv/core/Mat;Lorg/opencv/core/MatOfPoint;Lorg/opencv/core/Scalar;II)V
    .locals 14

    move-object/from16 v0, p2

    move-object v1, p0

    .line 2021
    iget-wide v1, v1, Lorg/opencv/core/Mat;->nativeObj:J

    move-object v3, p1

    iget-wide v3, v3, Lorg/opencv/core/Mat;->nativeObj:J

    iget-object v5, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v6, 0x0

    aget-wide v6, v5, v6

    iget-object v5, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v8, 0x1

    aget-wide v8, v5, v8

    iget-object v5, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v10, 0x2

    aget-wide v10, v5, v10

    iget-object v0, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v5, 0x3

    aget-wide v12, v0, v5

    move-wide v0, v1

    move-wide v2, v3

    move-wide v4, v6

    move-wide v6, v8

    move-wide v8, v10

    move-wide v10, v12

    move/from16 v12, p3

    move/from16 v13, p4

    invoke-static/range {v0 .. v13}, Lorg/opencv/imgproc/Imgproc;->fillConvexPoly_0(JJDDDDII)V

    return-void
.end method

.method private static native fillConvexPoly_0(JJDDDDII)V
.end method

.method private static native fillConvexPoly_1(JJDDDD)V
.end method

.method public static fillPoly(Lorg/opencv/core/Mat;Ljava/util/List;Lorg/opencv/core/Scalar;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List<",
            "Lorg/opencv/core/MatOfPoint;",
            ">;",
            "Lorg/opencv/core/Scalar;",
            ")V"
        }
    .end annotation

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    .line 2053
    new-instance v2, Ljava/util/ArrayList;

    const/4 v3, 0x0

    if-eqz v0, :cond_0

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v4

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    :goto_0
    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 2054
    invoke-static {v0, v2}, Lorg/opencv/utils/Converters;->vector_vector_Point_to_Mat(Ljava/util/List;Ljava/util/List;)Lorg/opencv/core/Mat;

    move-result-object v0

    move-object/from16 v2, p0

    .line 2055
    iget-wide v4, v2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, v0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    aget-wide v8, v0, v3

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v2, 0x1

    aget-wide v10, v0, v2

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v2, 0x2

    aget-wide v12, v0, v2

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x3

    aget-wide v14, v0, v1

    invoke-static/range {v4 .. v15}, Lorg/opencv/imgproc/Imgproc;->fillPoly_1(JJDDDD)V

    return-void
.end method

.method public static fillPoly(Lorg/opencv/core/Mat;Ljava/util/List;Lorg/opencv/core/Scalar;IILorg/opencv/core/Point;)V
    .locals 23
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List<",
            "Lorg/opencv/core/MatOfPoint;",
            ">;",
            "Lorg/opencv/core/Scalar;",
            "II",
            "Lorg/opencv/core/Point;",
            ")V"
        }
    .end annotation

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p5

    .line 2043
    new-instance v3, Ljava/util/ArrayList;

    const/4 v4, 0x0

    if-eqz v0, :cond_0

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v5

    goto :goto_0

    :cond_0
    const/4 v5, 0x0

    :goto_0
    invoke-direct {v3, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 2044
    invoke-static {v0, v3}, Lorg/opencv/utils/Converters;->vector_vector_Point_to_Mat(Ljava/util/List;Ljava/util/List;)Lorg/opencv/core/Mat;

    move-result-object v0

    move-object/from16 v3, p0

    .line 2045
    iget-wide v5, v3, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v7, v0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    aget-wide v9, v0, v4

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v3, 0x1

    aget-wide v11, v0, v3

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v3, 0x2

    aget-wide v13, v0, v3

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x3

    aget-wide v15, v0, v1

    iget-wide v0, v2, Lorg/opencv/core/Point;->x:D

    move-wide/from16 v19, v0

    iget-wide v0, v2, Lorg/opencv/core/Point;->y:D

    move-wide/from16 v21, v0

    move/from16 v17, p3

    move/from16 v18, p4

    invoke-static/range {v5 .. v22}, Lorg/opencv/imgproc/Imgproc;->fillPoly_0(JJDDDDIIDD)V

    return-void
.end method

.method private static native fillPoly_0(JJDDDDIIDD)V
.end method

.method private static native fillPoly_1(JJDDDD)V
.end method

.method public static filter2D(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;ILorg/opencv/core/Mat;)V
    .locals 7

    .line 2087
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v5, p3, Lorg/opencv/core/Mat;->nativeObj:J

    move v4, p2

    invoke-static/range {v0 .. v6}, Lorg/opencv/imgproc/Imgproc;->filter2D_2(JJIJ)V

    return-void
.end method

.method public static filter2D(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;ILorg/opencv/core/Mat;Lorg/opencv/core/Point;D)V
    .locals 13

    move-object/from16 v0, p4

    move-object v1, p0

    .line 2078
    iget-wide v1, v1, Lorg/opencv/core/Mat;->nativeObj:J

    move-object v3, p1

    iget-wide v3, v3, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v5, p3

    iget-wide v5, v5, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v7, v0, Lorg/opencv/core/Point;->x:D

    iget-wide v9, v0, Lorg/opencv/core/Point;->y:D

    move-wide v0, v1

    move-wide v2, v3

    move v4, p2

    move-wide/from16 v11, p5

    invoke-static/range {v0 .. v12}, Lorg/opencv/imgproc/Imgproc;->filter2D_1(JJIJDDD)V

    return-void
.end method

.method public static filter2D(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;ILorg/opencv/core/Mat;Lorg/opencv/core/Point;DI)V
    .locals 14

    move-object/from16 v0, p4

    move-object v1, p0

    .line 2069
    iget-wide v1, v1, Lorg/opencv/core/Mat;->nativeObj:J

    move-object v3, p1

    iget-wide v3, v3, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v5, p3

    iget-wide v5, v5, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v7, v0, Lorg/opencv/core/Point;->x:D

    iget-wide v9, v0, Lorg/opencv/core/Point;->y:D

    move-wide v0, v1

    move-wide v2, v3

    move/from16 v4, p2

    move-wide/from16 v11, p5

    move/from16 v13, p7

    invoke-static/range {v0 .. v13}, Lorg/opencv/imgproc/Imgproc;->filter2D_0(JJIJDDDI)V

    return-void
.end method

.method private static native filter2D_0(JJIJDDDI)V
.end method

.method private static native filter2D_1(JJIJDDD)V
.end method

.method private static native filter2D_2(JJIJ)V
.end method

.method public static findContours(Lorg/opencv/core/Mat;Ljava/util/List;Lorg/opencv/core/Mat;II)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List<",
            "Lorg/opencv/core/MatOfPoint;",
            ">;",
            "Lorg/opencv/core/Mat;",
            "II)V"
        }
    .end annotation

    .line 2110
    new-instance v0, Lorg/opencv/core/Mat;

    invoke-direct {v0}, Lorg/opencv/core/Mat;-><init>()V

    .line 2111
    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v3, v0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v5, p2, Lorg/opencv/core/Mat;->nativeObj:J

    move v7, p3

    move v8, p4

    invoke-static/range {v1 .. v8}, Lorg/opencv/imgproc/Imgproc;->findContours_1(JJJII)V

    .line 2112
    invoke-static {v0, p1}, Lorg/opencv/utils/Converters;->Mat_to_vector_vector_Point(Lorg/opencv/core/Mat;Ljava/util/List;)V

    .line 2113
    invoke-virtual {v0}, Lorg/opencv/core/Mat;->release()V

    return-void
.end method

.method public static findContours(Lorg/opencv/core/Mat;Ljava/util/List;Lorg/opencv/core/Mat;IILorg/opencv/core/Point;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List<",
            "Lorg/opencv/core/MatOfPoint;",
            ">;",
            "Lorg/opencv/core/Mat;",
            "II",
            "Lorg/opencv/core/Point;",
            ")V"
        }
    .end annotation

    move-object/from16 v0, p5

    .line 2100
    new-instance v1, Lorg/opencv/core/Mat;

    invoke-direct {v1}, Lorg/opencv/core/Mat;-><init>()V

    move-object v2, p0

    .line 2101
    iget-wide v2, v2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, v1, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v6, p2

    iget-wide v6, v6, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v10, v0, Lorg/opencv/core/Point;->x:D

    iget-wide v12, v0, Lorg/opencv/core/Point;->y:D

    move/from16 v8, p3

    move/from16 v9, p4

    invoke-static/range {v2 .. v13}, Lorg/opencv/imgproc/Imgproc;->findContours_0(JJJIIDD)V

    move-object v0, p1

    .line 2102
    invoke-static {v1, p1}, Lorg/opencv/utils/Converters;->Mat_to_vector_vector_Point(Lorg/opencv/core/Mat;Ljava/util/List;)V

    .line 2103
    invoke-virtual {v1}, Lorg/opencv/core/Mat;->release()V

    return-void
.end method

.method private static native findContours_0(JJJIIDD)V
.end method

.method private static native findContours_1(JJJII)V
.end method

.method public static fitEllipse(Lorg/opencv/core/MatOfPoint2f;)Lorg/opencv/core/RotatedRect;
    .locals 3

    .line 681
    new-instance v0, Lorg/opencv/core/RotatedRect;

    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2}, Lorg/opencv/imgproc/Imgproc;->fitEllipse_0(J)[D

    move-result-object p0

    invoke-direct {v0, p0}, Lorg/opencv/core/RotatedRect;-><init>([D)V

    return-object v0
.end method

.method private static native fitEllipse_0(J)[D
.end method

.method public static fitLine(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;IDDD)V
    .locals 11

    move-object v0, p0

    .line 2126
    iget-wide v0, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object v2, p1

    iget-wide v2, v2, Lorg/opencv/core/Mat;->nativeObj:J

    move v4, p2

    move-wide v5, p3

    move-wide/from16 v7, p5

    move-wide/from16 v9, p7

    invoke-static/range {v0 .. v10}, Lorg/opencv/imgproc/Imgproc;->fitLine_0(JJIDDD)V

    return-void
.end method

.method private static native fitLine_0(JJIDDD)V
.end method

.method public static floodFill(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Point;Lorg/opencv/core/Scalar;)I
    .locals 19

    move-object/from16 v0, p2

    move-object/from16 v1, p0

    move-object/from16 v2, p3

    .line 947
    iget-wide v3, v1, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v1, p1

    iget-wide v5, v1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v7, v0, Lorg/opencv/core/Point;->x:D

    iget-wide v9, v0, Lorg/opencv/core/Point;->y:D

    iget-object v0, v2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x0

    aget-wide v11, v0, v1

    iget-object v0, v2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x1

    aget-wide v13, v0, v1

    iget-object v0, v2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x2

    aget-wide v15, v0, v1

    iget-object v0, v2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x3

    aget-wide v17, v0, v1

    move-wide v0, v3

    move-wide v2, v5

    move-wide v4, v7

    move-wide v6, v9

    move-wide v8, v11

    move-wide v10, v13

    move-wide v12, v15

    move-wide/from16 v14, v17

    invoke-static/range {v0 .. v15}, Lorg/opencv/imgproc/Imgproc;->floodFill_1(JJDDDDDD)I

    move-result v0

    return v0
.end method

.method public static floodFill(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Point;Lorg/opencv/core/Scalar;Lorg/opencv/core/Rect;Lorg/opencv/core/Scalar;Lorg/opencv/core/Scalar;I)I
    .locals 42

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    move-object/from16 v3, p5

    move-object/from16 v4, p6

    move/from16 v38, p7

    const/4 v5, 0x4

    .line 937
    new-array v15, v5, [D

    move-object/from16 v21, v15

    move-object/from16 v5, p0

    .line 938
    iget-wide v5, v5, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v7, p1

    iget-wide v7, v7, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v9, v0, Lorg/opencv/core/Point;->x:D

    iget-wide v11, v0, Lorg/opencv/core/Point;->y:D

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/16 v39, 0x0

    aget-wide v13, v0, v39

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/16 v40, 0x1

    aget-wide v16, v0, v40

    move-object v0, v15

    move-wide/from16 v15, v16

    move-object/from16 p0, v0

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/16 v41, 0x2

    aget-wide v17, v0, v41

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x3

    aget-wide v19, v0, v1

    iget-object v0, v3, Lorg/opencv/core/Scalar;->val:[D

    aget-wide v22, v0, v39

    iget-object v0, v3, Lorg/opencv/core/Scalar;->val:[D

    aget-wide v24, v0, v40

    iget-object v0, v3, Lorg/opencv/core/Scalar;->val:[D

    aget-wide v26, v0, v41

    iget-object v0, v3, Lorg/opencv/core/Scalar;->val:[D

    aget-wide v28, v0, v1

    iget-object v0, v4, Lorg/opencv/core/Scalar;->val:[D

    aget-wide v30, v0, v39

    iget-object v0, v4, Lorg/opencv/core/Scalar;->val:[D

    aget-wide v32, v0, v40

    iget-object v0, v4, Lorg/opencv/core/Scalar;->val:[D

    aget-wide v34, v0, v41

    iget-object v0, v4, Lorg/opencv/core/Scalar;->val:[D

    aget-wide v36, v0, v1

    invoke-static/range {v5 .. v38}, Lorg/opencv/imgproc/Imgproc;->floodFill_0(JJDDDDDD[DDDDDDDDDI)I

    move-result v0

    if-eqz v2, :cond_0

    .line 939
    aget-wide v3, p0, v39

    double-to-int v3, v3

    iput v3, v2, Lorg/opencv/core/Rect;->x:I

    aget-wide v3, p0, v40

    double-to-int v3, v3

    iput v3, v2, Lorg/opencv/core/Rect;->y:I

    aget-wide v3, p0, v41

    double-to-int v3, v3

    iput v3, v2, Lorg/opencv/core/Rect;->width:I

    aget-wide v3, p0, v1

    double-to-int v1, v3

    iput v1, v2, Lorg/opencv/core/Rect;->height:I

    :cond_0
    return v0
.end method

.method private static native floodFill_0(JJDDDDDD[DDDDDDDDDI)I
.end method

.method private static native floodFill_1(JJDDDDDD)I
.end method

.method public static getAffineTransform(Lorg/opencv/core/MatOfPoint2f;Lorg/opencv/core/MatOfPoint2f;)Lorg/opencv/core/Mat;
    .locals 3

    .line 441
    new-instance v0, Lorg/opencv/core/Mat;

    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2, p0, p1}, Lorg/opencv/imgproc/Imgproc;->getAffineTransform_0(JJ)J

    move-result-wide p0

    invoke-direct {v0, p0, p1}, Lorg/opencv/core/Mat;-><init>(J)V

    return-object v0
.end method

.method private static native getAffineTransform_0(JJ)J
.end method

.method public static getDefaultNewCameraMatrix(Lorg/opencv/core/Mat;)Lorg/opencv/core/Mat;
    .locals 3

    .line 464
    new-instance v0, Lorg/opencv/core/Mat;

    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2}, Lorg/opencv/imgproc/Imgproc;->getDefaultNewCameraMatrix_1(J)J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lorg/opencv/core/Mat;-><init>(J)V

    return-object v0
.end method

.method public static getDefaultNewCameraMatrix(Lorg/opencv/core/Mat;Lorg/opencv/core/Size;Z)Lorg/opencv/core/Mat;
    .locals 8

    .line 455
    new-instance v0, Lorg/opencv/core/Mat;

    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v3, p1, Lorg/opencv/core/Size;->width:D

    iget-wide v5, p1, Lorg/opencv/core/Size;->height:D

    move v7, p2

    invoke-static/range {v1 .. v7}, Lorg/opencv/imgproc/Imgproc;->getDefaultNewCameraMatrix_0(JDDZ)J

    move-result-wide p0

    invoke-direct {v0, p0, p1}, Lorg/opencv/core/Mat;-><init>(J)V

    return-object v0
.end method

.method private static native getDefaultNewCameraMatrix_0(JDDZ)J
.end method

.method private static native getDefaultNewCameraMatrix_1(J)J
.end method

.method public static getDerivKernels(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;III)V
    .locals 7

    .line 2149
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    move v4, p2

    move v5, p3

    move v6, p4

    invoke-static/range {v0 .. v6}, Lorg/opencv/imgproc/Imgproc;->getDerivKernels_1(JJIII)V

    return-void
.end method

.method public static getDerivKernels(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;IIIZI)V
    .locals 9

    .line 2140
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    move v4, p2

    move v5, p3

    move v6, p4

    move v7, p5

    move v8, p6

    invoke-static/range {v0 .. v8}, Lorg/opencv/imgproc/Imgproc;->getDerivKernels_0(JJIIIZI)V

    return-void
.end method

.method private static native getDerivKernels_0(JJIIIZI)V
.end method

.method private static native getDerivKernels_1(JJIII)V
.end method

.method public static getGaborKernel(Lorg/opencv/core/Size;DDDD)Lorg/opencv/core/Mat;
    .locals 14

    move-object v0, p0

    .line 487
    new-instance v1, Lorg/opencv/core/Mat;

    iget-wide v2, v0, Lorg/opencv/core/Size;->width:D

    iget-wide v4, v0, Lorg/opencv/core/Size;->height:D

    move-wide v6, p1

    move-wide/from16 v8, p3

    move-wide/from16 v10, p5

    move-wide/from16 v12, p7

    invoke-static/range {v2 .. v13}, Lorg/opencv/imgproc/Imgproc;->getGaborKernel_1(DDDDDD)J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lorg/opencv/core/Mat;-><init>(J)V

    return-object v1
.end method

.method public static getGaborKernel(Lorg/opencv/core/Size;DDDDDI)Lorg/opencv/core/Mat;
    .locals 17

    move-object/from16 v0, p0

    .line 478
    new-instance v1, Lorg/opencv/core/Mat;

    iget-wide v2, v0, Lorg/opencv/core/Size;->width:D

    iget-wide v4, v0, Lorg/opencv/core/Size;->height:D

    move-wide/from16 v6, p1

    move-wide/from16 v8, p3

    move-wide/from16 v10, p5

    move-wide/from16 v12, p7

    move-wide/from16 v14, p9

    move/from16 v16, p11

    invoke-static/range {v2 .. v16}, Lorg/opencv/imgproc/Imgproc;->getGaborKernel_0(DDDDDDDI)J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lorg/opencv/core/Mat;-><init>(J)V

    return-object v1
.end method

.method private static native getGaborKernel_0(DDDDDDDI)J
.end method

.method private static native getGaborKernel_1(DDDDDD)J
.end method

.method public static getGaussianKernel(ID)Lorg/opencv/core/Mat;
    .locals 1

    .line 510
    new-instance v0, Lorg/opencv/core/Mat;

    invoke-static {p0, p1, p2}, Lorg/opencv/imgproc/Imgproc;->getGaussianKernel_1(ID)J

    move-result-wide p0

    invoke-direct {v0, p0, p1}, Lorg/opencv/core/Mat;-><init>(J)V

    return-object v0
.end method

.method public static getGaussianKernel(IDI)Lorg/opencv/core/Mat;
    .locals 1

    .line 501
    new-instance v0, Lorg/opencv/core/Mat;

    invoke-static {p0, p1, p2, p3}, Lorg/opencv/imgproc/Imgproc;->getGaussianKernel_0(IDI)J

    move-result-wide p0

    invoke-direct {v0, p0, p1}, Lorg/opencv/core/Mat;-><init>(J)V

    return-object v0
.end method

.method private static native getGaussianKernel_0(IDI)J
.end method

.method private static native getGaussianKernel_1(ID)J
.end method

.method public static getPerspectiveTransform(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)Lorg/opencv/core/Mat;
    .locals 3

    .line 524
    new-instance v0, Lorg/opencv/core/Mat;

    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2, p0, p1}, Lorg/opencv/imgproc/Imgproc;->getPerspectiveTransform_0(JJ)J

    move-result-wide p0

    invoke-direct {v0, p0, p1}, Lorg/opencv/core/Mat;-><init>(J)V

    return-object v0
.end method

.method private static native getPerspectiveTransform_0(JJ)J
.end method

.method public static getRectSubPix(Lorg/opencv/core/Mat;Lorg/opencv/core/Size;Lorg/opencv/core/Point;Lorg/opencv/core/Mat;)V
    .locals 12

    .line 2172
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Size;->width:D

    iget-wide v4, p1, Lorg/opencv/core/Size;->height:D

    iget-wide v6, p2, Lorg/opencv/core/Point;->x:D

    iget-wide v8, p2, Lorg/opencv/core/Point;->y:D

    iget-wide v10, p3, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v11}, Lorg/opencv/imgproc/Imgproc;->getRectSubPix_1(JDDDDJ)V

    return-void
.end method

.method public static getRectSubPix(Lorg/opencv/core/Mat;Lorg/opencv/core/Size;Lorg/opencv/core/Point;Lorg/opencv/core/Mat;I)V
    .locals 14

    move-object v0, p1

    move-object/from16 v1, p2

    move-object v2, p0

    .line 2163
    iget-wide v2, v2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, v0, Lorg/opencv/core/Size;->width:D

    iget-wide v6, v0, Lorg/opencv/core/Size;->height:D

    iget-wide v8, v1, Lorg/opencv/core/Point;->x:D

    iget-wide v10, v1, Lorg/opencv/core/Point;->y:D

    move-object/from16 v0, p3

    iget-wide v12, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v0, v2

    move-wide v2, v4

    move-wide v4, v6

    move-wide v6, v8

    move-wide v8, v10

    move-wide v10, v12

    move/from16 v12, p4

    invoke-static/range {v0 .. v12}, Lorg/opencv/imgproc/Imgproc;->getRectSubPix_0(JDDDDJI)V

    return-void
.end method

.method private static native getRectSubPix_0(JDDDDJI)V
.end method

.method private static native getRectSubPix_1(JDDDDJ)V
.end method

.method public static getRotationMatrix2D(Lorg/opencv/core/Point;DD)Lorg/opencv/core/Mat;
    .locals 9

    .line 538
    new-instance v0, Lorg/opencv/core/Mat;

    iget-wide v1, p0, Lorg/opencv/core/Point;->x:D

    iget-wide v3, p0, Lorg/opencv/core/Point;->y:D

    move-wide v5, p1

    move-wide v7, p3

    invoke-static/range {v1 .. v8}, Lorg/opencv/imgproc/Imgproc;->getRotationMatrix2D_0(DDDD)J

    move-result-wide p0

    invoke-direct {v0, p0, p1}, Lorg/opencv/core/Mat;-><init>(J)V

    return-object v0
.end method

.method private static native getRotationMatrix2D_0(DDDD)J
.end method

.method public static getStructuringElement(ILorg/opencv/core/Size;)Lorg/opencv/core/Mat;
    .locals 5

    .line 561
    new-instance v0, Lorg/opencv/core/Mat;

    iget-wide v1, p1, Lorg/opencv/core/Size;->width:D

    iget-wide v3, p1, Lorg/opencv/core/Size;->height:D

    invoke-static {p0, v1, v2, v3, v4}, Lorg/opencv/imgproc/Imgproc;->getStructuringElement_1(IDD)J

    move-result-wide p0

    invoke-direct {v0, p0, p1}, Lorg/opencv/core/Mat;-><init>(J)V

    return-object v0
.end method

.method public static getStructuringElement(ILorg/opencv/core/Size;Lorg/opencv/core/Point;)Lorg/opencv/core/Mat;
    .locals 10

    .line 552
    new-instance v0, Lorg/opencv/core/Mat;

    iget-wide v2, p1, Lorg/opencv/core/Size;->width:D

    iget-wide v4, p1, Lorg/opencv/core/Size;->height:D

    iget-wide v6, p2, Lorg/opencv/core/Point;->x:D

    iget-wide v8, p2, Lorg/opencv/core/Point;->y:D

    move v1, p0

    invoke-static/range {v1 .. v9}, Lorg/opencv/imgproc/Imgproc;->getStructuringElement_0(IDDDD)J

    move-result-wide p0

    invoke-direct {v0, p0, p1}, Lorg/opencv/core/Mat;-><init>(J)V

    return-object v0
.end method

.method private static native getStructuringElement_0(IDDDD)J
.end method

.method private static native getStructuringElement_1(IDD)J
.end method

.method public static getTextSize(Ljava/lang/String;IDI[I)Lorg/opencv/core/Size;
    .locals 2

    if-eqz p5, :cond_1

    .line 2947
    array-length v0, p5

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 2948
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "\'baseLine\' must be \'int[1]\' or \'null\'."

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 2949
    :cond_1
    :goto_0
    new-instance v0, Lorg/opencv/core/Size;

    invoke-static/range {p0 .. p5}, Lorg/opencv/imgproc/Imgproc;->n_getTextSize(Ljava/lang/String;IDI[I)[D

    move-result-object p0

    invoke-direct {v0, p0}, Lorg/opencv/core/Size;-><init>([D)V

    return-object v0
.end method

.method public static goodFeaturesToTrack(Lorg/opencv/core/Mat;Lorg/opencv/core/MatOfPoint;IDD)V
    .locals 9

    .line 2195
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    move v4, p2

    move-wide v5, p3

    move-wide v7, p5

    invoke-static/range {v0 .. v8}, Lorg/opencv/imgproc/Imgproc;->goodFeaturesToTrack_1(JJIDD)V

    return-void
.end method

.method public static goodFeaturesToTrack(Lorg/opencv/core/Mat;Lorg/opencv/core/MatOfPoint;IDDLorg/opencv/core/Mat;IZD)V
    .locals 15

    move-object v0, p0

    .line 2186
    iget-wide v0, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v2, p1

    iget-wide v2, v2, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v4, p7

    iget-wide v9, v4, Lorg/opencv/core/Mat;->nativeObj:J

    move/from16 v4, p2

    move-wide/from16 v5, p3

    move-wide/from16 v7, p5

    move/from16 v11, p8

    move/from16 v12, p9

    move-wide/from16 v13, p10

    invoke-static/range {v0 .. v14}, Lorg/opencv/imgproc/Imgproc;->goodFeaturesToTrack_0(JJIDDJIZD)V

    return-void
.end method

.method private static native goodFeaturesToTrack_0(JJIDDJIZD)V
.end method

.method private static native goodFeaturesToTrack_1(JJIDD)V
.end method

.method public static grabCut(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Rect;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V
    .locals 13

    move-object v0, p2

    move-object v1, p0

    .line 2218
    iget-wide v1, v1, Lorg/opencv/core/Mat;->nativeObj:J

    move-object v3, p1

    iget-wide v3, v3, Lorg/opencv/core/Mat;->nativeObj:J

    iget v5, v0, Lorg/opencv/core/Rect;->x:I

    iget v6, v0, Lorg/opencv/core/Rect;->y:I

    iget v7, v0, Lorg/opencv/core/Rect;->width:I

    iget v8, v0, Lorg/opencv/core/Rect;->height:I

    move-object/from16 v0, p3

    iget-wide v9, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p4

    iget-wide v11, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v0, v1

    move-wide v2, v3

    move v4, v5

    move v5, v6

    move v6, v7

    move v7, v8

    move-wide v8, v9

    move-wide v10, v11

    move/from16 v12, p5

    invoke-static/range {v0 .. v12}, Lorg/opencv/imgproc/Imgproc;->grabCut_1(JJIIIIJJI)V

    return-void
.end method

.method public static grabCut(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Rect;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;II)V
    .locals 14

    move-object/from16 v0, p2

    move-object v1, p0

    .line 2209
    iget-wide v1, v1, Lorg/opencv/core/Mat;->nativeObj:J

    move-object v3, p1

    iget-wide v3, v3, Lorg/opencv/core/Mat;->nativeObj:J

    iget v5, v0, Lorg/opencv/core/Rect;->x:I

    iget v6, v0, Lorg/opencv/core/Rect;->y:I

    iget v7, v0, Lorg/opencv/core/Rect;->width:I

    iget v8, v0, Lorg/opencv/core/Rect;->height:I

    move-object/from16 v0, p3

    iget-wide v9, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p4

    iget-wide v11, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v0, v1

    move-wide v2, v3

    move v4, v5

    move v5, v6

    move v6, v7

    move v7, v8

    move-wide v8, v9

    move-wide v10, v11

    move/from16 v12, p5

    move/from16 v13, p6

    invoke-static/range {v0 .. v13}, Lorg/opencv/imgproc/Imgproc;->grabCut_0(JJIIIIJJII)V

    return-void
.end method

.method private static native grabCut_0(JJIIIIJJII)V
.end method

.method private static native grabCut_1(JJIIIIJJI)V
.end method

.method public static initUndistortRectifyMap(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Size;ILorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 20

    move-object/from16 v0, p4

    move/from16 v13, p5

    move-object/from16 v1, p0

    .line 2232
    iget-wide v1, v1, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v3, p1

    iget-wide v3, v3, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v5, p2

    iget-wide v5, v5, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v7, p3

    iget-wide v7, v7, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v9, v0, Lorg/opencv/core/Size;->width:D

    iget-wide v11, v0, Lorg/opencv/core/Size;->height:D

    move-object/from16 v0, p6

    iget-wide v14, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p7

    move-wide/from16 v18, v1

    iget-wide v0, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide/from16 v16, v0

    move-wide/from16 v1, v18

    invoke-static/range {v1 .. v17}, Lorg/opencv/imgproc/Imgproc;->initUndistortRectifyMap_0(JJJJDDIJJ)V

    return-void
.end method

.method private static native initUndistortRectifyMap_0(JJJJDDIJJ)V
.end method

.method public static initWideAngleProjMap(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Size;IILorg/opencv/core/Mat;Lorg/opencv/core/Mat;)F
    .locals 14

    move-object/from16 v0, p2

    move-object v1, p0

    .line 855
    iget-wide v1, v1, Lorg/opencv/core/Mat;->nativeObj:J

    move-object v3, p1

    iget-wide v3, v3, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v5, v0, Lorg/opencv/core/Size;->width:D

    iget-wide v7, v0, Lorg/opencv/core/Size;->height:D

    move-object/from16 v0, p5

    iget-wide v10, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p6

    iget-wide v12, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v0, v1

    move-wide v2, v3

    move-wide v4, v5

    move-wide v6, v7

    move/from16 v8, p3

    move/from16 v9, p4

    invoke-static/range {v0 .. v13}, Lorg/opencv/imgproc/Imgproc;->initWideAngleProjMap_1(JJDDIIJJ)F

    move-result v0

    return v0
.end method

.method public static initWideAngleProjMap(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Size;IILorg/opencv/core/Mat;Lorg/opencv/core/Mat;ID)F
    .locals 18

    move-object/from16 v0, p2

    move/from16 v9, p3

    move/from16 v10, p4

    move/from16 v15, p7

    move-wide/from16 v16, p8

    move-object/from16 v1, p0

    .line 846
    iget-wide v1, v1, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v3, p1

    iget-wide v3, v3, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v5, v0, Lorg/opencv/core/Size;->width:D

    iget-wide v7, v0, Lorg/opencv/core/Size;->height:D

    move-object/from16 v0, p5

    iget-wide v11, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p6

    iget-wide v13, v0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v1 .. v17}, Lorg/opencv/imgproc/Imgproc;->initWideAngleProjMap_0(JJDDIIJJID)F

    move-result v0

    return v0
.end method

.method private static native initWideAngleProjMap_0(JJDDIIJJID)F
.end method

.method private static native initWideAngleProjMap_1(JJDDIIJJ)F
.end method

.method public static integral(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 2

    .line 2301
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1}, Lorg/opencv/imgproc/Imgproc;->integral_1(JJ)V

    return-void
.end method

.method public static integral(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V
    .locals 2

    .line 2292
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1, p2}, Lorg/opencv/imgproc/Imgproc;->integral_0(JJI)V

    return-void
.end method

.method public static integral2(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 6

    .line 2278
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/imgproc/Imgproc;->integral2_1(JJJ)V

    return-void
.end method

.method public static integral2(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;II)V
    .locals 8

    .line 2269
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    move v6, p3

    move v7, p4

    invoke-static/range {v0 .. v7}, Lorg/opencv/imgproc/Imgproc;->integral2_0(JJJII)V

    return-void
.end method

.method private static native integral2_0(JJJII)V
.end method

.method private static native integral2_1(JJJ)V
.end method

.method public static integral3(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 8

    .line 2255
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p3, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v7}, Lorg/opencv/imgproc/Imgproc;->integral3_1(JJJJ)V

    return-void
.end method

.method public static integral3(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;II)V
    .locals 10

    .line 2246
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p3, Lorg/opencv/core/Mat;->nativeObj:J

    move v8, p4

    move v9, p5

    invoke-static/range {v0 .. v9}, Lorg/opencv/imgproc/Imgproc;->integral3_0(JJJJII)V

    return-void
.end method

.method private static native integral3_0(JJJJII)V
.end method

.method private static native integral3_1(JJJJ)V
.end method

.method private static native integral_0(JJI)V
.end method

.method private static native integral_1(JJ)V
.end method

.method public static intersectConvexConvex(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)F
    .locals 6

    .line 878
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/imgproc/Imgproc;->intersectConvexConvex_1(JJJ)F

    move-result p0

    return p0
.end method

.method public static intersectConvexConvex(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Z)F
    .locals 7

    .line 869
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    move v6, p3

    invoke-static/range {v0 .. v6}, Lorg/opencv/imgproc/Imgproc;->intersectConvexConvex_0(JJJZ)F

    move-result p0

    return p0
.end method

.method private static native intersectConvexConvex_0(JJJZ)F
.end method

.method private static native intersectConvexConvex_1(JJJ)F
.end method

.method public static invertAffineTransform(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 2

    .line 2315
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1}, Lorg/opencv/imgproc/Imgproc;->invertAffineTransform_0(JJ)V

    return-void
.end method

.method private static native invertAffineTransform_0(JJ)V
.end method

.method public static isContourConvex(Lorg/opencv/core/MatOfPoint;)Z
    .locals 2

    .line 725
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1}, Lorg/opencv/imgproc/Imgproc;->isContourConvex_0(J)Z

    move-result p0

    return p0
.end method

.method private static native isContourConvex_0(J)Z
.end method

.method public static line(Lorg/opencv/core/Mat;Lorg/opencv/core/Point;Lorg/opencv/core/Point;Lorg/opencv/core/Scalar;)V
    .locals 22

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p0

    move-object/from16 v3, p3

    .line 2347
    iget-wide v4, v2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, v0, Lorg/opencv/core/Point;->x:D

    iget-wide v8, v0, Lorg/opencv/core/Point;->y:D

    iget-wide v10, v1, Lorg/opencv/core/Point;->x:D

    iget-wide v12, v1, Lorg/opencv/core/Point;->y:D

    iget-object v0, v3, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x0

    aget-wide v14, v0, v1

    iget-object v0, v3, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x1

    aget-wide v16, v0, v1

    iget-object v0, v3, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x2

    aget-wide v18, v0, v1

    iget-object v0, v3, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x3

    aget-wide v20, v0, v1

    invoke-static/range {v4 .. v21}, Lorg/opencv/imgproc/Imgproc;->line_2(JDDDDDDDD)V

    return-void
.end method

.method public static line(Lorg/opencv/core/Mat;Lorg/opencv/core/Point;Lorg/opencv/core/Point;Lorg/opencv/core/Scalar;I)V
    .locals 22

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move/from16 v21, p4

    move-object/from16 v3, p0

    .line 2338
    iget-wide v3, v3, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v5, v0, Lorg/opencv/core/Point;->x:D

    iget-wide v7, v0, Lorg/opencv/core/Point;->y:D

    iget-wide v9, v1, Lorg/opencv/core/Point;->x:D

    iget-wide v11, v1, Lorg/opencv/core/Point;->y:D

    iget-object v0, v2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x0

    aget-wide v13, v0, v1

    iget-object v0, v2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x1

    aget-wide v15, v0, v1

    iget-object v0, v2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x2

    aget-wide v17, v0, v1

    iget-object v0, v2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x3

    aget-wide v19, v0, v1

    invoke-static/range {v3 .. v21}, Lorg/opencv/imgproc/Imgproc;->line_1(JDDDDDDDDI)V

    return-void
.end method

.method public static line(Lorg/opencv/core/Mat;Lorg/opencv/core/Point;Lorg/opencv/core/Point;Lorg/opencv/core/Scalar;III)V
    .locals 24

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move/from16 v21, p4

    move/from16 v22, p5

    move/from16 v23, p6

    move-object/from16 v3, p0

    .line 2329
    iget-wide v3, v3, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v5, v0, Lorg/opencv/core/Point;->x:D

    iget-wide v7, v0, Lorg/opencv/core/Point;->y:D

    iget-wide v9, v1, Lorg/opencv/core/Point;->x:D

    iget-wide v11, v1, Lorg/opencv/core/Point;->y:D

    iget-object v0, v2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x0

    aget-wide v13, v0, v1

    iget-object v0, v2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x1

    aget-wide v15, v0, v1

    iget-object v0, v2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x2

    aget-wide v17, v0, v1

    iget-object v0, v2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x3

    aget-wide v19, v0, v1

    invoke-static/range {v3 .. v23}, Lorg/opencv/imgproc/Imgproc;->line_0(JDDDDDDDDIII)V

    return-void
.end method

.method private static native line_0(JDDDDDDDDIII)V
.end method

.method private static native line_1(JDDDDDDDDI)V
.end method

.method private static native line_2(JDDDDDDDD)V
.end method

.method public static linearPolar(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Point;DI)V
    .locals 11

    move-object v0, p2

    move-object v1, p0

    .line 2361
    iget-wide v1, v1, Lorg/opencv/core/Mat;->nativeObj:J

    move-object v3, p1

    iget-wide v3, v3, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v5, v0, Lorg/opencv/core/Point;->x:D

    iget-wide v7, v0, Lorg/opencv/core/Point;->y:D

    move-wide v0, v1

    move-wide v2, v3

    move-wide v4, v5

    move-wide v6, v7

    move-wide v8, p3

    move/from16 v10, p5

    invoke-static/range {v0 .. v10}, Lorg/opencv/imgproc/Imgproc;->linearPolar_0(JJDDDI)V

    return-void
.end method

.method private static native linearPolar_0(JJDDDI)V
.end method

.method public static logPolar(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Point;DI)V
    .locals 11

    move-object v0, p2

    move-object v1, p0

    .line 2375
    iget-wide v1, v1, Lorg/opencv/core/Mat;->nativeObj:J

    move-object v3, p1

    iget-wide v3, v3, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v5, v0, Lorg/opencv/core/Point;->x:D

    iget-wide v7, v0, Lorg/opencv/core/Point;->y:D

    move-wide v0, v1

    move-wide v2, v3

    move-wide v4, v5

    move-wide v6, v7

    move-wide v8, p3

    move/from16 v10, p5

    invoke-static/range {v0 .. v10}, Lorg/opencv/imgproc/Imgproc;->logPolar_0(JJDDDI)V

    return-void
.end method

.method private static native logPolar_0(JJDDDI)V
.end method

.method public static matchShapes(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;ID)D
    .locals 7

    .line 790
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    move v4, p2

    move-wide v5, p3

    invoke-static/range {v0 .. v6}, Lorg/opencv/imgproc/Imgproc;->matchShapes_0(JJID)D

    move-result-wide p0

    return-wide p0
.end method

.method private static native matchShapes_0(JJID)D
.end method

.method public static matchTemplate(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V
    .locals 7

    .line 2398
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    move v6, p3

    invoke-static/range {v0 .. v6}, Lorg/opencv/imgproc/Imgproc;->matchTemplate_1(JJJI)V

    return-void
.end method

.method public static matchTemplate(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;ILorg/opencv/core/Mat;)V
    .locals 9

    .line 2389
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v7, p4, Lorg/opencv/core/Mat;->nativeObj:J

    move v6, p3

    invoke-static/range {v0 .. v8}, Lorg/opencv/imgproc/Imgproc;->matchTemplate_0(JJJIJ)V

    return-void
.end method

.method private static native matchTemplate_0(JJJIJ)V
.end method

.method private static native matchTemplate_1(JJJI)V
.end method

.method public static medianBlur(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V
    .locals 2

    .line 2412
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1, p2}, Lorg/opencv/imgproc/Imgproc;->medianBlur_0(JJI)V

    return-void
.end method

.method private static native medianBlur_0(JJI)V
.end method

.method public static minAreaRect(Lorg/opencv/core/MatOfPoint2f;)Lorg/opencv/core/RotatedRect;
    .locals 3

    .line 695
    new-instance v0, Lorg/opencv/core/RotatedRect;

    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2}, Lorg/opencv/imgproc/Imgproc;->minAreaRect_0(J)[D

    move-result-object p0

    invoke-direct {v0, p0}, Lorg/opencv/core/RotatedRect;-><init>([D)V

    return-object v0
.end method

.method private static native minAreaRect_0(J)[D
.end method

.method public static minEnclosingCircle(Lorg/opencv/core/MatOfPoint2f;Lorg/opencv/core/Point;[F)V
    .locals 5

    const/4 v0, 0x2

    .line 2426
    new-array v0, v0, [D

    const/4 v1, 0x1

    .line 2427
    new-array v2, v1, [D

    .line 2428
    iget-wide v3, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v3, v4, v0, v2}, Lorg/opencv/imgproc/Imgproc;->minEnclosingCircle_0(J[D[D)V

    const/4 p0, 0x0

    if-eqz p1, :cond_0

    .line 2429
    aget-wide v3, v0, p0

    iput-wide v3, p1, Lorg/opencv/core/Point;->x:D

    aget-wide v3, v0, v1

    iput-wide v3, p1, Lorg/opencv/core/Point;->y:D

    :cond_0
    if-eqz p2, :cond_1

    .line 2430
    aget-wide v0, v2, p0

    double-to-float p1, v0

    aput p1, p2, p0

    :cond_1
    return-void
.end method

.method private static native minEnclosingCircle_0(J[D[D)V
.end method

.method public static minEnclosingTriangle(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)D
    .locals 2

    .line 804
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1}, Lorg/opencv/imgproc/Imgproc;->minEnclosingTriangle_0(JJ)D

    move-result-wide p0

    return-wide p0
.end method

.method private static native minEnclosingTriangle_0(JJ)D
.end method

.method public static moments(Lorg/opencv/core/Mat;)Lorg/opencv/imgproc/Moments;
    .locals 3

    .line 584
    new-instance v0, Lorg/opencv/imgproc/Moments;

    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2}, Lorg/opencv/imgproc/Imgproc;->moments_1(J)[D

    move-result-object p0

    invoke-direct {v0, p0}, Lorg/opencv/imgproc/Moments;-><init>([D)V

    return-object v0
.end method

.method public static moments(Lorg/opencv/core/Mat;Z)Lorg/opencv/imgproc/Moments;
    .locals 3

    .line 575
    new-instance v0, Lorg/opencv/imgproc/Moments;

    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2, p1}, Lorg/opencv/imgproc/Imgproc;->moments_0(JZ)[D

    move-result-object p0

    invoke-direct {v0, p0}, Lorg/opencv/imgproc/Moments;-><init>([D)V

    return-object v0
.end method

.method private static native moments_0(JZ)[D
.end method

.method private static native moments_1(J)[D
.end method

.method public static morphologyEx(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;ILorg/opencv/core/Mat;)V
    .locals 7

    .line 2461
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v5, p3, Lorg/opencv/core/Mat;->nativeObj:J

    move v4, p2

    invoke-static/range {v0 .. v6}, Lorg/opencv/imgproc/Imgproc;->morphologyEx_2(JJIJ)V

    return-void
.end method

.method public static morphologyEx(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;ILorg/opencv/core/Mat;Lorg/opencv/core/Point;I)V
    .locals 12

    move-object/from16 v0, p4

    move-object v1, p0

    .line 2452
    iget-wide v1, v1, Lorg/opencv/core/Mat;->nativeObj:J

    move-object v3, p1

    iget-wide v3, v3, Lorg/opencv/core/Mat;->nativeObj:J

    move-object v5, p3

    iget-wide v5, v5, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v7, v0, Lorg/opencv/core/Point;->x:D

    iget-wide v9, v0, Lorg/opencv/core/Point;->y:D

    move-wide v0, v1

    move-wide v2, v3

    move v4, p2

    move/from16 v11, p5

    invoke-static/range {v0 .. v11}, Lorg/opencv/imgproc/Imgproc;->morphologyEx_1(JJIJDDI)V

    return-void
.end method

.method public static morphologyEx(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;ILorg/opencv/core/Mat;Lorg/opencv/core/Point;IILorg/opencv/core/Scalar;)V
    .locals 23

    move-object/from16 v0, p4

    move-object/from16 v1, p7

    move/from16 v6, p2

    move/from16 v13, p5

    move/from16 v14, p6

    move-object/from16 v2, p0

    .line 2443
    iget-wide v2, v2, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v4, p1

    iget-wide v4, v4, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v7, p3

    iget-wide v7, v7, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v9, v0, Lorg/opencv/core/Point;->x:D

    iget-wide v11, v0, Lorg/opencv/core/Point;->y:D

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v15, 0x0

    aget-wide v15, v0, v15

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/16 v17, 0x1

    aget-wide v17, v0, v17

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/16 v19, 0x2

    aget-wide v19, v0, v19

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x3

    aget-wide v21, v0, v1

    invoke-static/range {v2 .. v22}, Lorg/opencv/imgproc/Imgproc;->morphologyEx_0(JJIJDDIIDDDD)V

    return-void
.end method

.method private static native morphologyEx_0(JJIJDDIIDDDD)V
.end method

.method private static native morphologyEx_1(JJIJDDI)V
.end method

.method private static native morphologyEx_2(JJIJ)V
.end method

.method private static native n_getTextSize(Ljava/lang/String;IDI[I)[D
.end method

.method public static phaseCorrelate(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)Lorg/opencv/core/Point;
    .locals 3

    .line 607
    new-instance v0, Lorg/opencv/core/Point;

    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2, p0, p1}, Lorg/opencv/imgproc/Imgproc;->phaseCorrelate_1(JJ)[D

    move-result-object p0

    invoke-direct {v0, p0}, Lorg/opencv/core/Point;-><init>([D)V

    return-object v0
.end method

.method public static phaseCorrelate(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;[D)Lorg/opencv/core/Point;
    .locals 9

    const/4 v0, 0x1

    .line 597
    new-array v0, v0, [D

    .line 598
    new-instance v8, Lorg/opencv/core/Point;

    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v3, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v5, p2, Lorg/opencv/core/Mat;->nativeObj:J

    move-object v7, v0

    invoke-static/range {v1 .. v7}, Lorg/opencv/imgproc/Imgproc;->phaseCorrelate_0(JJJ[D)[D

    move-result-object p0

    invoke-direct {v8, p0}, Lorg/opencv/core/Point;-><init>([D)V

    if-eqz p3, :cond_0

    const/4 p0, 0x0

    .line 599
    aget-wide p1, v0, p0

    aput-wide p1, p3, p0

    :cond_0
    return-object v8
.end method

.method private static native phaseCorrelate_0(JJJ[D)[D
.end method

.method private static native phaseCorrelate_1(JJ)[D
.end method

.method public static pointPolygonTest(Lorg/opencv/core/MatOfPoint2f;Lorg/opencv/core/Point;Z)D
    .locals 7

    .line 818
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Point;->x:D

    iget-wide v4, p1, Lorg/opencv/core/Point;->y:D

    move v6, p2

    invoke-static/range {v0 .. v6}, Lorg/opencv/imgproc/Imgproc;->pointPolygonTest_0(JDDZ)D

    move-result-wide p0

    return-wide p0
.end method

.method private static native pointPolygonTest_0(JDDZ)D
.end method

.method public static polylines(Lorg/opencv/core/Mat;Ljava/util/List;ZLorg/opencv/core/Scalar;)V
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List<",
            "Lorg/opencv/core/MatOfPoint;",
            ">;Z",
            "Lorg/opencv/core/Scalar;",
            ")V"
        }
    .end annotation

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    .line 2494
    new-instance v2, Ljava/util/ArrayList;

    const/4 v3, 0x0

    if-eqz v0, :cond_0

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v4

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    :goto_0
    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 2495
    invoke-static {v0, v2}, Lorg/opencv/utils/Converters;->vector_vector_Point_to_Mat(Ljava/util/List;Ljava/util/List;)Lorg/opencv/core/Mat;

    move-result-object v0

    move-object/from16 v2, p0

    .line 2496
    iget-wide v4, v2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, v0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    aget-wide v9, v0, v3

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v2, 0x1

    aget-wide v11, v0, v2

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v2, 0x2

    aget-wide v13, v0, v2

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x3

    aget-wide v15, v0, v1

    move/from16 v8, p2

    invoke-static/range {v4 .. v16}, Lorg/opencv/imgproc/Imgproc;->polylines_2(JJZDDDD)V

    return-void
.end method

.method public static polylines(Lorg/opencv/core/Mat;Ljava/util/List;ZLorg/opencv/core/Scalar;I)V
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List<",
            "Lorg/opencv/core/MatOfPoint;",
            ">;Z",
            "Lorg/opencv/core/Scalar;",
            "I)V"
        }
    .end annotation

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    .line 2484
    new-instance v2, Ljava/util/ArrayList;

    const/4 v3, 0x0

    if-eqz v0, :cond_0

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v4

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    :goto_0
    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 2485
    invoke-static {v0, v2}, Lorg/opencv/utils/Converters;->vector_vector_Point_to_Mat(Ljava/util/List;Ljava/util/List;)Lorg/opencv/core/Mat;

    move-result-object v0

    move-object/from16 v2, p0

    .line 2486
    iget-wide v4, v2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, v0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    aget-wide v9, v0, v3

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v2, 0x1

    aget-wide v11, v0, v2

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v2, 0x2

    aget-wide v13, v0, v2

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x3

    aget-wide v15, v0, v1

    move/from16 v8, p2

    move/from16 v17, p4

    invoke-static/range {v4 .. v17}, Lorg/opencv/imgproc/Imgproc;->polylines_1(JJZDDDDI)V

    return-void
.end method

.method public static polylines(Lorg/opencv/core/Mat;Ljava/util/List;ZLorg/opencv/core/Scalar;III)V
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List<",
            "Lorg/opencv/core/MatOfPoint;",
            ">;Z",
            "Lorg/opencv/core/Scalar;",
            "III)V"
        }
    .end annotation

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    .line 2474
    new-instance v2, Ljava/util/ArrayList;

    const/4 v3, 0x0

    if-eqz v0, :cond_0

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v4

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    :goto_0
    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 2475
    invoke-static {v0, v2}, Lorg/opencv/utils/Converters;->vector_vector_Point_to_Mat(Ljava/util/List;Ljava/util/List;)Lorg/opencv/core/Mat;

    move-result-object v0

    move-object/from16 v2, p0

    .line 2476
    iget-wide v4, v2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, v0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    aget-wide v9, v0, v3

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v2, 0x1

    aget-wide v11, v0, v2

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v2, 0x2

    aget-wide v13, v0, v2

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x3

    aget-wide v15, v0, v1

    move/from16 v8, p2

    move/from16 v17, p4

    move/from16 v18, p5

    move/from16 v19, p6

    invoke-static/range {v4 .. v19}, Lorg/opencv/imgproc/Imgproc;->polylines_0(JJZDDDDIII)V

    return-void
.end method

.method private static native polylines_0(JJZDDDDIII)V
.end method

.method private static native polylines_1(JJZDDDDI)V
.end method

.method private static native polylines_2(JJZDDDD)V
.end method

.method public static preCornerDetect(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V
    .locals 2

    .line 2519
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1, p2}, Lorg/opencv/imgproc/Imgproc;->preCornerDetect_1(JJI)V

    return-void
.end method

.method public static preCornerDetect(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;II)V
    .locals 6

    .line 2510
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lorg/opencv/imgproc/Imgproc;->preCornerDetect_0(JJII)V

    return-void
.end method

.method private static native preCornerDetect_0(JJII)V
.end method

.method private static native preCornerDetect_1(JJI)V
.end method

.method public static putText(Lorg/opencv/core/Mat;Ljava/lang/String;Lorg/opencv/core/Point;IDLorg/opencv/core/Scalar;)V
    .locals 20

    move-object/from16 v0, p2

    move-object/from16 v1, p6

    move-object/from16 v4, p1

    move/from16 v9, p3

    move-wide/from16 v10, p4

    move-object/from16 v2, p0

    .line 2551
    iget-wide v2, v2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v5, v0, Lorg/opencv/core/Point;->x:D

    iget-wide v7, v0, Lorg/opencv/core/Point;->y:D

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v12, 0x0

    aget-wide v12, v0, v12

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v14, 0x1

    aget-wide v14, v0, v14

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/16 v16, 0x2

    aget-wide v16, v0, v16

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x3

    aget-wide v18, v0, v1

    invoke-static/range {v2 .. v19}, Lorg/opencv/imgproc/Imgproc;->putText_2(JLjava/lang/String;DDIDDDDD)V

    return-void
.end method

.method public static putText(Lorg/opencv/core/Mat;Ljava/lang/String;Lorg/opencv/core/Point;IDLorg/opencv/core/Scalar;I)V
    .locals 21

    move-object/from16 v0, p2

    move-object/from16 v1, p6

    move-object/from16 v4, p1

    move/from16 v9, p3

    move-wide/from16 v10, p4

    move/from16 v20, p7

    move-object/from16 v2, p0

    .line 2542
    iget-wide v2, v2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v5, v0, Lorg/opencv/core/Point;->x:D

    iget-wide v7, v0, Lorg/opencv/core/Point;->y:D

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v12, 0x0

    aget-wide v12, v0, v12

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v14, 0x1

    aget-wide v14, v0, v14

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/16 v16, 0x2

    aget-wide v16, v0, v16

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x3

    aget-wide v18, v0, v1

    invoke-static/range {v2 .. v20}, Lorg/opencv/imgproc/Imgproc;->putText_1(JLjava/lang/String;DDIDDDDDI)V

    return-void
.end method

.method public static putText(Lorg/opencv/core/Mat;Ljava/lang/String;Lorg/opencv/core/Point;IDLorg/opencv/core/Scalar;IIZ)V
    .locals 23

    move-object/from16 v0, p2

    move-object/from16 v1, p6

    move-object/from16 v4, p1

    move/from16 v9, p3

    move-wide/from16 v10, p4

    move/from16 v20, p7

    move/from16 v21, p8

    move/from16 v22, p9

    move-object/from16 v2, p0

    .line 2533
    iget-wide v2, v2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v5, v0, Lorg/opencv/core/Point;->x:D

    iget-wide v7, v0, Lorg/opencv/core/Point;->y:D

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v12, 0x0

    aget-wide v12, v0, v12

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v14, 0x1

    aget-wide v14, v0, v14

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/16 v16, 0x2

    aget-wide v16, v0, v16

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x3

    aget-wide v18, v0, v1

    invoke-static/range {v2 .. v22}, Lorg/opencv/imgproc/Imgproc;->putText_0(JLjava/lang/String;DDIDDDDDIIZ)V

    return-void
.end method

.method private static native putText_0(JLjava/lang/String;DDIDDDDDIIZ)V
.end method

.method private static native putText_1(JLjava/lang/String;DDIDDDDDI)V
.end method

.method private static native putText_2(JLjava/lang/String;DDIDDDDD)V
.end method

.method public static pyrDown(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 2

    .line 2583
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1}, Lorg/opencv/imgproc/Imgproc;->pyrDown_2(JJ)V

    return-void
.end method

.method public static pyrDown(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Size;)V
    .locals 8

    .line 2574
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Size;->width:D

    iget-wide v6, p2, Lorg/opencv/core/Size;->height:D

    invoke-static/range {v0 .. v7}, Lorg/opencv/imgproc/Imgproc;->pyrDown_1(JJDD)V

    return-void
.end method

.method public static pyrDown(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Size;I)V
    .locals 9

    .line 2565
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Size;->width:D

    iget-wide v6, p2, Lorg/opencv/core/Size;->height:D

    move v8, p3

    invoke-static/range {v0 .. v8}, Lorg/opencv/imgproc/Imgproc;->pyrDown_0(JJDDI)V

    return-void
.end method

.method private static native pyrDown_0(JJDDI)V
.end method

.method private static native pyrDown_1(JJDD)V
.end method

.method private static native pyrDown_2(JJ)V
.end method

.method public static pyrMeanShiftFiltering(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;DD)V
    .locals 8

    .line 2606
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v4, p2

    move-wide v6, p4

    invoke-static/range {v0 .. v7}, Lorg/opencv/imgproc/Imgproc;->pyrMeanShiftFiltering_1(JJDD)V

    return-void
.end method

.method public static pyrMeanShiftFiltering(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;DDILorg/opencv/core/TermCriteria;)V
    .locals 13

    move-object v0, p0

    move-object/from16 v1, p7

    .line 2597
    iget-wide v2, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object v0, p1

    iget-wide v4, v0, Lorg/opencv/core/Mat;->nativeObj:J

    iget v9, v1, Lorg/opencv/core/TermCriteria;->type:I

    iget v10, v1, Lorg/opencv/core/TermCriteria;->maxCount:I

    iget-wide v11, v1, Lorg/opencv/core/TermCriteria;->epsilon:D

    move-wide v0, v2

    move-wide v2, v4

    move-wide v4, p2

    move-wide/from16 v6, p4

    move/from16 v8, p6

    invoke-static/range {v0 .. v12}, Lorg/opencv/imgproc/Imgproc;->pyrMeanShiftFiltering_0(JJDDIIID)V

    return-void
.end method

.method private static native pyrMeanShiftFiltering_0(JJDDIIID)V
.end method

.method private static native pyrMeanShiftFiltering_1(JJDD)V
.end method

.method public static pyrUp(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 2

    .line 2638
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1}, Lorg/opencv/imgproc/Imgproc;->pyrUp_2(JJ)V

    return-void
.end method

.method public static pyrUp(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Size;)V
    .locals 8

    .line 2629
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Size;->width:D

    iget-wide v6, p2, Lorg/opencv/core/Size;->height:D

    invoke-static/range {v0 .. v7}, Lorg/opencv/imgproc/Imgproc;->pyrUp_1(JJDD)V

    return-void
.end method

.method public static pyrUp(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Size;I)V
    .locals 9

    .line 2620
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Size;->width:D

    iget-wide v6, p2, Lorg/opencv/core/Size;->height:D

    move v8, p3

    invoke-static/range {v0 .. v8}, Lorg/opencv/imgproc/Imgproc;->pyrUp_0(JJDDI)V

    return-void
.end method

.method private static native pyrUp_0(JJDDI)V
.end method

.method private static native pyrUp_1(JJDD)V
.end method

.method private static native pyrUp_2(JJ)V
.end method

.method public static rectangle(Lorg/opencv/core/Mat;Lorg/opencv/core/Point;Lorg/opencv/core/Point;Lorg/opencv/core/Scalar;)V
    .locals 22

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p0

    move-object/from16 v3, p3

    .line 2670
    iget-wide v4, v2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, v0, Lorg/opencv/core/Point;->x:D

    iget-wide v8, v0, Lorg/opencv/core/Point;->y:D

    iget-wide v10, v1, Lorg/opencv/core/Point;->x:D

    iget-wide v12, v1, Lorg/opencv/core/Point;->y:D

    iget-object v0, v3, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x0

    aget-wide v14, v0, v1

    iget-object v0, v3, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x1

    aget-wide v16, v0, v1

    iget-object v0, v3, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x2

    aget-wide v18, v0, v1

    iget-object v0, v3, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x3

    aget-wide v20, v0, v1

    invoke-static/range {v4 .. v21}, Lorg/opencv/imgproc/Imgproc;->rectangle_2(JDDDDDDDD)V

    return-void
.end method

.method public static rectangle(Lorg/opencv/core/Mat;Lorg/opencv/core/Point;Lorg/opencv/core/Point;Lorg/opencv/core/Scalar;I)V
    .locals 22

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move/from16 v21, p4

    move-object/from16 v3, p0

    .line 2661
    iget-wide v3, v3, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v5, v0, Lorg/opencv/core/Point;->x:D

    iget-wide v7, v0, Lorg/opencv/core/Point;->y:D

    iget-wide v9, v1, Lorg/opencv/core/Point;->x:D

    iget-wide v11, v1, Lorg/opencv/core/Point;->y:D

    iget-object v0, v2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x0

    aget-wide v13, v0, v1

    iget-object v0, v2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x1

    aget-wide v15, v0, v1

    iget-object v0, v2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x2

    aget-wide v17, v0, v1

    iget-object v0, v2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x3

    aget-wide v19, v0, v1

    invoke-static/range {v3 .. v21}, Lorg/opencv/imgproc/Imgproc;->rectangle_1(JDDDDDDDDI)V

    return-void
.end method

.method public static rectangle(Lorg/opencv/core/Mat;Lorg/opencv/core/Point;Lorg/opencv/core/Point;Lorg/opencv/core/Scalar;III)V
    .locals 24

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move/from16 v21, p4

    move/from16 v22, p5

    move/from16 v23, p6

    move-object/from16 v3, p0

    .line 2652
    iget-wide v3, v3, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v5, v0, Lorg/opencv/core/Point;->x:D

    iget-wide v7, v0, Lorg/opencv/core/Point;->y:D

    iget-wide v9, v1, Lorg/opencv/core/Point;->x:D

    iget-wide v11, v1, Lorg/opencv/core/Point;->y:D

    iget-object v0, v2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x0

    aget-wide v13, v0, v1

    iget-object v0, v2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x1

    aget-wide v15, v0, v1

    iget-object v0, v2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x2

    aget-wide v17, v0, v1

    iget-object v0, v2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x3

    aget-wide v19, v0, v1

    invoke-static/range {v3 .. v23}, Lorg/opencv/imgproc/Imgproc;->rectangle_0(JDDDDDDDDIII)V

    return-void
.end method

.method private static native rectangle_0(JDDDDDDDDIII)V
.end method

.method private static native rectangle_1(JDDDDDDDDI)V
.end method

.method private static native rectangle_2(JDDDDDDDD)V
.end method

.method public static remap(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V
    .locals 9

    .line 2693
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p3, Lorg/opencv/core/Mat;->nativeObj:J

    move v8, p4

    invoke-static/range {v0 .. v8}, Lorg/opencv/imgproc/Imgproc;->remap_1(JJJJI)V

    return-void
.end method

.method public static remap(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;IILorg/opencv/core/Scalar;)V
    .locals 19

    move-object/from16 v0, p6

    move/from16 v9, p4

    move/from16 v10, p5

    move-object/from16 v1, p0

    .line 2684
    iget-wide v1, v1, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v3, p1

    iget-wide v3, v3, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v5, p2

    iget-wide v5, v5, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v7, p3

    iget-wide v7, v7, Lorg/opencv/core/Mat;->nativeObj:J

    iget-object v11, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v12, 0x0

    aget-wide v12, v11, v12

    move-wide v11, v12

    iget-object v13, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v14, 0x1

    aget-wide v14, v13, v14

    move-wide v13, v14

    iget-object v15, v0, Lorg/opencv/core/Scalar;->val:[D

    const/16 v16, 0x2

    aget-wide v16, v15, v16

    move-wide/from16 v15, v16

    iget-object v0, v0, Lorg/opencv/core/Scalar;->val:[D

    const/16 v17, 0x3

    aget-wide v17, v0, v17

    invoke-static/range {v1 .. v18}, Lorg/opencv/imgproc/Imgproc;->remap_0(JJJJIIDDDD)V

    return-void
.end method

.method private static native remap_0(JJJJIIDDDD)V
.end method

.method private static native remap_1(JJJJI)V
.end method

.method public static resize(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Size;)V
    .locals 8

    .line 2716
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Size;->width:D

    iget-wide v6, p2, Lorg/opencv/core/Size;->height:D

    invoke-static/range {v0 .. v7}, Lorg/opencv/imgproc/Imgproc;->resize_1(JJDD)V

    return-void
.end method

.method public static resize(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Size;DDI)V
    .locals 13

    move-object v0, p2

    move-object v1, p0

    .line 2707
    iget-wide v1, v1, Lorg/opencv/core/Mat;->nativeObj:J

    move-object v3, p1

    iget-wide v3, v3, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v5, v0, Lorg/opencv/core/Size;->width:D

    iget-wide v7, v0, Lorg/opencv/core/Size;->height:D

    move-wide v0, v1

    move-wide v2, v3

    move-wide v4, v5

    move-wide v6, v7

    move-wide/from16 v8, p3

    move-wide/from16 v10, p5

    move/from16 v12, p7

    invoke-static/range {v0 .. v12}, Lorg/opencv/imgproc/Imgproc;->resize_0(JJDDDDI)V

    return-void
.end method

.method private static native resize_0(JJDDDDI)V
.end method

.method private static native resize_1(JJDD)V
.end method

.method public static rotatedRectangleIntersection(Lorg/opencv/core/RotatedRect;Lorg/opencv/core/RotatedRect;Lorg/opencv/core/Mat;)I
    .locals 27

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 961
    iget-object v2, v0, Lorg/opencv/core/RotatedRect;->center:Lorg/opencv/core/Point;

    iget-wide v3, v2, Lorg/opencv/core/Point;->x:D

    iget-object v2, v0, Lorg/opencv/core/RotatedRect;->center:Lorg/opencv/core/Point;

    iget-wide v5, v2, Lorg/opencv/core/Point;->y:D

    iget-object v2, v0, Lorg/opencv/core/RotatedRect;->size:Lorg/opencv/core/Size;

    iget-wide v7, v2, Lorg/opencv/core/Size;->width:D

    iget-object v2, v0, Lorg/opencv/core/RotatedRect;->size:Lorg/opencv/core/Size;

    iget-wide v9, v2, Lorg/opencv/core/Size;->height:D

    iget-wide v11, v0, Lorg/opencv/core/RotatedRect;->angle:D

    iget-object v0, v1, Lorg/opencv/core/RotatedRect;->center:Lorg/opencv/core/Point;

    iget-wide v13, v0, Lorg/opencv/core/Point;->x:D

    iget-object v0, v1, Lorg/opencv/core/RotatedRect;->center:Lorg/opencv/core/Point;

    move-wide/from16 v25, v3

    iget-wide v2, v0, Lorg/opencv/core/Point;->y:D

    move-wide v15, v2

    iget-object v0, v1, Lorg/opencv/core/RotatedRect;->size:Lorg/opencv/core/Size;

    iget-wide v2, v0, Lorg/opencv/core/Size;->width:D

    move-wide/from16 v17, v2

    iget-object v0, v1, Lorg/opencv/core/RotatedRect;->size:Lorg/opencv/core/Size;

    iget-wide v2, v0, Lorg/opencv/core/Size;->height:D

    move-wide/from16 v19, v2

    iget-wide v0, v1, Lorg/opencv/core/RotatedRect;->angle:D

    move-wide/from16 v21, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide/from16 v23, v0

    move-wide/from16 v3, v25

    invoke-static/range {v3 .. v24}, Lorg/opencv/imgproc/Imgproc;->rotatedRectangleIntersection_0(DDDDDDDDDDJ)I

    move-result v0

    return v0
.end method

.method private static native rotatedRectangleIntersection_0(DDDDDDDDDDJ)I
.end method

.method public static sepFilter2D(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;ILorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 9

    .line 2748
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v5, p3, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v7, p4, Lorg/opencv/core/Mat;->nativeObj:J

    move v4, p2

    invoke-static/range {v0 .. v8}, Lorg/opencv/imgproc/Imgproc;->sepFilter2D_2(JJIJJ)V

    return-void
.end method

.method public static sepFilter2D(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;ILorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Point;D)V
    .locals 15

    move-object/from16 v0, p5

    move-object v1, p0

    .line 2739
    iget-wide v1, v1, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v3, p1

    iget-wide v3, v3, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v5, p3

    iget-wide v5, v5, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v7, p4

    iget-wide v7, v7, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v9, v0, Lorg/opencv/core/Point;->x:D

    iget-wide v11, v0, Lorg/opencv/core/Point;->y:D

    move-wide v0, v1

    move-wide v2, v3

    move/from16 v4, p2

    move-wide/from16 v13, p6

    invoke-static/range {v0 .. v14}, Lorg/opencv/imgproc/Imgproc;->sepFilter2D_1(JJIJJDDD)V

    return-void
.end method

.method public static sepFilter2D(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;ILorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Point;DI)V
    .locals 16

    move-object/from16 v0, p5

    move-object/from16 v1, p0

    .line 2730
    iget-wide v1, v1, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v3, p1

    iget-wide v3, v3, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v5, p3

    iget-wide v5, v5, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v7, p4

    iget-wide v7, v7, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v9, v0, Lorg/opencv/core/Point;->x:D

    iget-wide v11, v0, Lorg/opencv/core/Point;->y:D

    move-wide v0, v1

    move-wide v2, v3

    move/from16 v4, p2

    move-wide/from16 v13, p6

    move/from16 v15, p8

    invoke-static/range {v0 .. v15}, Lorg/opencv/imgproc/Imgproc;->sepFilter2D_0(JJIJJDDDI)V

    return-void
.end method

.method private static native sepFilter2D_0(JJIJJDDDI)V
.end method

.method private static native sepFilter2D_1(JJIJJDDD)V
.end method

.method private static native sepFilter2D_2(JJIJJ)V
.end method

.method public static spatialGradient(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 6

    .line 2780
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/imgproc/Imgproc;->spatialGradient_2(JJJ)V

    return-void
.end method

.method public static spatialGradient(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V
    .locals 7

    .line 2771
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    move v6, p3

    invoke-static/range {v0 .. v6}, Lorg/opencv/imgproc/Imgproc;->spatialGradient_1(JJJI)V

    return-void
.end method

.method public static spatialGradient(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;II)V
    .locals 8

    .line 2762
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    move v6, p3

    move v7, p4

    invoke-static/range {v0 .. v7}, Lorg/opencv/imgproc/Imgproc;->spatialGradient_0(JJJII)V

    return-void
.end method

.method private static native spatialGradient_0(JJJII)V
.end method

.method private static native spatialGradient_1(JJJI)V
.end method

.method private static native spatialGradient_2(JJJ)V
.end method

.method public static sqrBoxFilter(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;ILorg/opencv/core/Size;)V
    .locals 9

    .line 2812
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v5, p3, Lorg/opencv/core/Size;->width:D

    iget-wide v7, p3, Lorg/opencv/core/Size;->height:D

    move v4, p2

    invoke-static/range {v0 .. v8}, Lorg/opencv/imgproc/Imgproc;->sqrBoxFilter_2(JJIDD)V

    return-void
.end method

.method public static sqrBoxFilter(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;ILorg/opencv/core/Size;Lorg/opencv/core/Point;Z)V
    .locals 14

    move-object/from16 v0, p3

    move-object/from16 v1, p4

    move-object v2, p0

    .line 2803
    iget-wide v2, v2, Lorg/opencv/core/Mat;->nativeObj:J

    move-object v4, p1

    iget-wide v4, v4, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, v0, Lorg/opencv/core/Size;->width:D

    iget-wide v8, v0, Lorg/opencv/core/Size;->height:D

    iget-wide v10, v1, Lorg/opencv/core/Point;->x:D

    iget-wide v12, v1, Lorg/opencv/core/Point;->y:D

    move-wide v0, v2

    move-wide v2, v4

    move/from16 v4, p2

    move-wide v5, v6

    move-wide v7, v8

    move-wide v9, v10

    move-wide v11, v12

    move/from16 v13, p5

    invoke-static/range {v0 .. v13}, Lorg/opencv/imgproc/Imgproc;->sqrBoxFilter_1(JJIDDDDZ)V

    return-void
.end method

.method public static sqrBoxFilter(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;ILorg/opencv/core/Size;Lorg/opencv/core/Point;ZI)V
    .locals 15

    move-object/from16 v0, p3

    move-object/from16 v1, p4

    move-object v2, p0

    .line 2794
    iget-wide v2, v2, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v4, p1

    iget-wide v4, v4, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, v0, Lorg/opencv/core/Size;->width:D

    iget-wide v8, v0, Lorg/opencv/core/Size;->height:D

    iget-wide v10, v1, Lorg/opencv/core/Point;->x:D

    iget-wide v12, v1, Lorg/opencv/core/Point;->y:D

    move-wide v0, v2

    move-wide v2, v4

    move/from16 v4, p2

    move-wide v5, v6

    move-wide v7, v8

    move-wide v9, v10

    move-wide v11, v12

    move/from16 v13, p5

    move/from16 v14, p6

    invoke-static/range {v0 .. v14}, Lorg/opencv/imgproc/Imgproc;->sqrBoxFilter_0(JJIDDDDZI)V

    return-void
.end method

.method private static native sqrBoxFilter_0(JJIDDDDZI)V
.end method

.method private static native sqrBoxFilter_1(JJIDDDDZ)V
.end method

.method private static native sqrBoxFilter_2(JJIDD)V
.end method

.method public static threshold(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;DDI)D
    .locals 9

    .line 832
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v4, p2

    move-wide v6, p4

    move v8, p6

    invoke-static/range {v0 .. v8}, Lorg/opencv/imgproc/Imgproc;->threshold_0(JJDDI)D

    move-result-wide p0

    return-wide p0
.end method

.method private static native threshold_0(JJDDI)D
.end method

.method public static undistort(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 8

    .line 2835
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p3, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v7}, Lorg/opencv/imgproc/Imgproc;->undistort_1(JJJJ)V

    return-void
.end method

.method public static undistort(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 10

    .line 2826
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p3, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v8, p4, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v9}, Lorg/opencv/imgproc/Imgproc;->undistort_0(JJJJJ)V

    return-void
.end method

.method public static undistortPoints(Lorg/opencv/core/MatOfPoint2f;Lorg/opencv/core/MatOfPoint2f;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 8

    .line 2860
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p3, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v7}, Lorg/opencv/imgproc/Imgproc;->undistortPoints_1(JJJJ)V

    return-void
.end method

.method public static undistortPoints(Lorg/opencv/core/MatOfPoint2f;Lorg/opencv/core/MatOfPoint2f;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 12

    move-object v0, p0

    .line 2850
    iget-wide v0, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object v2, p1

    iget-wide v2, v2, Lorg/opencv/core/Mat;->nativeObj:J

    move-object v4, p2

    iget-wide v4, v4, Lorg/opencv/core/Mat;->nativeObj:J

    move-object v6, p3

    iget-wide v6, v6, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v8, p4

    iget-wide v8, v8, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v10, p5

    iget-wide v10, v10, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v11}, Lorg/opencv/imgproc/Imgproc;->undistortPoints_0(JJJJJJ)V

    return-void
.end method

.method private static native undistortPoints_0(JJJJJJ)V
.end method

.method private static native undistortPoints_1(JJJJ)V
.end method

.method private static native undistort_0(JJJJJ)V
.end method

.method private static native undistort_1(JJJJ)V
.end method

.method public static warpAffine(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Size;)V
    .locals 10

    .line 2892
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p3, Lorg/opencv/core/Size;->width:D

    iget-wide v8, p3, Lorg/opencv/core/Size;->height:D

    invoke-static/range {v0 .. v9}, Lorg/opencv/imgproc/Imgproc;->warpAffine_2(JJJDD)V

    return-void
.end method

.method public static warpAffine(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Size;I)V
    .locals 11

    .line 2883
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p3, Lorg/opencv/core/Size;->width:D

    iget-wide v8, p3, Lorg/opencv/core/Size;->height:D

    move v10, p4

    invoke-static/range {v0 .. v10}, Lorg/opencv/imgproc/Imgproc;->warpAffine_1(JJJDDI)V

    return-void
.end method

.method public static warpAffine(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Size;IILorg/opencv/core/Scalar;)V
    .locals 22

    move-object/from16 v0, p3

    move-object/from16 v1, p6

    move/from16 v12, p4

    move/from16 v13, p5

    move-object/from16 v2, p0

    .line 2874
    iget-wide v2, v2, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v4, p1

    iget-wide v4, v4, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v6, p2

    iget-wide v6, v6, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v8, v0, Lorg/opencv/core/Size;->width:D

    iget-wide v10, v0, Lorg/opencv/core/Size;->height:D

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v14, 0x0

    aget-wide v14, v0, v14

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/16 v16, 0x1

    aget-wide v16, v0, v16

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/16 v18, 0x2

    aget-wide v18, v0, v18

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x3

    aget-wide v20, v0, v1

    invoke-static/range {v2 .. v21}, Lorg/opencv/imgproc/Imgproc;->warpAffine_0(JJJDDIIDDDD)V

    return-void
.end method

.method private static native warpAffine_0(JJJDDIIDDDD)V
.end method

.method private static native warpAffine_1(JJJDDI)V
.end method

.method private static native warpAffine_2(JJJDD)V
.end method

.method public static warpPerspective(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Size;)V
    .locals 10

    .line 2924
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p3, Lorg/opencv/core/Size;->width:D

    iget-wide v8, p3, Lorg/opencv/core/Size;->height:D

    invoke-static/range {v0 .. v9}, Lorg/opencv/imgproc/Imgproc;->warpPerspective_2(JJJDD)V

    return-void
.end method

.method public static warpPerspective(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Size;I)V
    .locals 11

    .line 2915
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p3, Lorg/opencv/core/Size;->width:D

    iget-wide v8, p3, Lorg/opencv/core/Size;->height:D

    move v10, p4

    invoke-static/range {v0 .. v10}, Lorg/opencv/imgproc/Imgproc;->warpPerspective_1(JJJDDI)V

    return-void
.end method

.method public static warpPerspective(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Size;IILorg/opencv/core/Scalar;)V
    .locals 22

    move-object/from16 v0, p3

    move-object/from16 v1, p6

    move/from16 v12, p4

    move/from16 v13, p5

    move-object/from16 v2, p0

    .line 2906
    iget-wide v2, v2, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v4, p1

    iget-wide v4, v4, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v6, p2

    iget-wide v6, v6, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v8, v0, Lorg/opencv/core/Size;->width:D

    iget-wide v10, v0, Lorg/opencv/core/Size;->height:D

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v14, 0x0

    aget-wide v14, v0, v14

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/16 v16, 0x1

    aget-wide v16, v0, v16

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/16 v18, 0x2

    aget-wide v18, v0, v18

    iget-object v0, v1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x3

    aget-wide v20, v0, v1

    invoke-static/range {v2 .. v21}, Lorg/opencv/imgproc/Imgproc;->warpPerspective_0(JJJDDIIDDDD)V

    return-void
.end method

.method private static native warpPerspective_0(JJJDDIIDDDD)V
.end method

.method private static native warpPerspective_1(JJJDDI)V
.end method

.method private static native warpPerspective_2(JJJDD)V
.end method

.method public static watershed(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 2

    .line 2938
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide p0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p0, p1}, Lorg/opencv/imgproc/Imgproc;->watershed_0(JJ)V

    return-void
.end method

.method private static native watershed_0(JJ)V
.end method

.class public final Lnet/vrallev/android/task/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/vrallev/android/task/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final abc_background_cache_hint_selector_material_dark:I = 0x7f050000

.field public static final abc_background_cache_hint_selector_material_light:I = 0x7f050001

.field public static final abc_input_method_navigation_guard:I = 0x7f050007

.field public static final abc_primary_text_disable_only_material_dark:I = 0x7f050008

.field public static final abc_primary_text_disable_only_material_light:I = 0x7f050009

.field public static final abc_primary_text_material_dark:I = 0x7f05000a

.field public static final abc_primary_text_material_light:I = 0x7f05000b

.field public static final abc_search_url_text:I = 0x7f05000c

.field public static final abc_search_url_text_normal:I = 0x7f05000d

.field public static final abc_search_url_text_pressed:I = 0x7f05000e

.field public static final abc_search_url_text_selected:I = 0x7f05000f

.field public static final abc_secondary_text_material_dark:I = 0x7f050010

.field public static final abc_secondary_text_material_light:I = 0x7f050011

.field public static final accent_material_dark:I = 0x7f050018

.field public static final accent_material_light:I = 0x7f050019

.field public static final background_floating_material_dark:I = 0x7f050020

.field public static final background_floating_material_light:I = 0x7f050021

.field public static final background_material_dark:I = 0x7f050026

.field public static final background_material_light:I = 0x7f050027

.field public static final bright_foreground_disabled_material_dark:I = 0x7f050031

.field public static final bright_foreground_disabled_material_light:I = 0x7f050032

.field public static final bright_foreground_inverse_material_dark:I = 0x7f050033

.field public static final bright_foreground_inverse_material_light:I = 0x7f050034

.field public static final bright_foreground_material_dark:I = 0x7f050035

.field public static final bright_foreground_material_light:I = 0x7f050036

.field public static final button_material_dark:I = 0x7f050037

.field public static final button_material_light:I = 0x7f050038

.field public static final dim_foreground_disabled_material_dark:I = 0x7f050046

.field public static final dim_foreground_disabled_material_light:I = 0x7f050047

.field public static final dim_foreground_material_dark:I = 0x7f050048

.field public static final dim_foreground_material_light:I = 0x7f050049

.field public static final highlighted_text_material_dark:I = 0x7f050070

.field public static final highlighted_text_material_light:I = 0x7f050071

.field public static final material_blue_grey_800:I = 0x7f05007d

.field public static final material_blue_grey_900:I = 0x7f05007e

.field public static final material_blue_grey_950:I = 0x7f05007f

.field public static final material_deep_teal_200:I = 0x7f050080

.field public static final material_deep_teal_500:I = 0x7f050081

.field public static final primary_dark_material_dark:I = 0x7f05008d

.field public static final primary_dark_material_light:I = 0x7f05008e

.field public static final primary_material_dark:I = 0x7f05008f

.field public static final primary_material_light:I = 0x7f050090

.field public static final primary_text_default_material_dark:I = 0x7f050091

.field public static final primary_text_default_material_light:I = 0x7f050092

.field public static final primary_text_disabled_material_dark:I = 0x7f050093

.field public static final primary_text_disabled_material_light:I = 0x7f050094

.field public static final ripple_material_dark:I = 0x7f050096

.field public static final ripple_material_light:I = 0x7f050097

.field public static final secondary_text_default_material_dark:I = 0x7f050098

.field public static final secondary_text_default_material_light:I = 0x7f050099

.field public static final secondary_text_disabled_material_dark:I = 0x7f05009a

.field public static final secondary_text_disabled_material_light:I = 0x7f05009b

.field public static final switch_thumb_disabled_material_dark:I = 0x7f05009d

.field public static final switch_thumb_disabled_material_light:I = 0x7f05009e

.field public static final switch_thumb_material_dark:I = 0x7f05009f

.field public static final switch_thumb_material_light:I = 0x7f0500a0

.field public static final switch_thumb_normal_material_dark:I = 0x7f0500a1

.field public static final switch_thumb_normal_material_light:I = 0x7f0500a2


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 231
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

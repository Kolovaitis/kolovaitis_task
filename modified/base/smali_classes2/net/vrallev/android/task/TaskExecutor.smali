.class public final Lnet/vrallev/android/task/TaskExecutor;
.super Ljava/lang/Object;
.source "TaskExecutor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lnet/vrallev/android/task/TaskExecutor$PostResult;,
        Lnet/vrallev/android/task/TaskExecutor$Builder;,
        Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "TaskExecutor"

.field private static final TASK_COUNTER:Ljava/util/concurrent/atomic/AtomicInteger;

.field private static instance:Lnet/vrallev/android/task/TaskExecutor;


# instance fields
.field private mApplication:Landroid/app/Application;

.field private final mCacheFactory:Lnet/vrallev/android/task/TaskCacheFragmentInterface$Factory;

.field private mExecutorService:Ljava/util/concurrent/ExecutorService;

.field private final mPostResult:Lnet/vrallev/android/task/TaskExecutor$PostResult;

.field private mTargetMethodFinder:Lnet/vrallev/android/task/TargetMethodFinder;

.field private mTasks:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lnet/vrallev/android/task/Task<",
            "*>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 31
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lnet/vrallev/android/task/TaskExecutor;->TASK_COUNTER:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method private constructor <init>(Ljava/util/concurrent/ExecutorService;Lnet/vrallev/android/task/TaskExecutor$PostResult;Lnet/vrallev/android/task/TaskCacheFragmentInterface$Factory;)V
    .locals 0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p1, p0, Lnet/vrallev/android/task/TaskExecutor;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    .line 60
    iput-object p2, p0, Lnet/vrallev/android/task/TaskExecutor;->mPostResult:Lnet/vrallev/android/task/TaskExecutor$PostResult;

    .line 61
    iput-object p3, p0, Lnet/vrallev/android/task/TaskExecutor;->mCacheFactory:Lnet/vrallev/android/task/TaskCacheFragmentInterface$Factory;

    .line 63
    new-instance p1, Landroid/util/SparseArray;

    invoke-direct {p1}, Landroid/util/SparseArray;-><init>()V

    iput-object p1, p0, Lnet/vrallev/android/task/TaskExecutor;->mTasks:Landroid/util/SparseArray;

    .line 64
    new-instance p1, Lnet/vrallev/android/task/TargetMethodFinder;

    const-class p2, Lnet/vrallev/android/task/TaskResult;

    invoke-direct {p1, p2}, Lnet/vrallev/android/task/TargetMethodFinder;-><init>(Ljava/lang/Class;)V

    iput-object p1, p0, Lnet/vrallev/android/task/TaskExecutor;->mTargetMethodFinder:Lnet/vrallev/android/task/TargetMethodFinder;

    return-void
.end method

.method synthetic constructor <init>(Ljava/util/concurrent/ExecutorService;Lnet/vrallev/android/task/TaskExecutor$PostResult;Lnet/vrallev/android/task/TaskCacheFragmentInterface$Factory;Lnet/vrallev/android/task/TaskExecutor$1;)V
    .locals 0

    .line 27
    invoke-direct {p0, p1, p2, p3}, Lnet/vrallev/android/task/TaskExecutor;-><init>(Ljava/util/concurrent/ExecutorService;Lnet/vrallev/android/task/TaskExecutor$PostResult;Lnet/vrallev/android/task/TaskCacheFragmentInterface$Factory;)V

    return-void
.end method

.method static synthetic access$100(Lnet/vrallev/android/task/TaskExecutor;Lnet/vrallev/android/task/Task;)V
    .locals 0

    .line 27
    invoke-direct {p0, p1}, Lnet/vrallev/android/task/TaskExecutor;->cleanUpTask(Lnet/vrallev/android/task/Task;)V

    return-void
.end method

.method static synthetic access$200(Lnet/vrallev/android/task/TaskExecutor;)Landroid/app/Application;
    .locals 0

    .line 27
    iget-object p0, p0, Lnet/vrallev/android/task/TaskExecutor;->mApplication:Landroid/app/Application;

    return-object p0
.end method

.method static synthetic access$300(Lnet/vrallev/android/task/TaskExecutor;)Lnet/vrallev/android/task/TargetMethodFinder;
    .locals 0

    .line 27
    iget-object p0, p0, Lnet/vrallev/android/task/TaskExecutor;->mTargetMethodFinder:Lnet/vrallev/android/task/TargetMethodFinder;

    return-object p0
.end method

.method static synthetic access$400(Lnet/vrallev/android/task/TaskExecutor;)Lnet/vrallev/android/task/TaskExecutor$PostResult;
    .locals 0

    .line 27
    iget-object p0, p0, Lnet/vrallev/android/task/TaskExecutor;->mPostResult:Lnet/vrallev/android/task/TaskExecutor$PostResult;

    return-object p0
.end method

.method static synthetic access$600(Lnet/vrallev/android/task/TaskExecutor;)Lnet/vrallev/android/task/TaskCacheFragmentInterface$Factory;
    .locals 0

    .line 27
    iget-object p0, p0, Lnet/vrallev/android/task/TaskExecutor;->mCacheFactory:Lnet/vrallev/android/task/TaskCacheFragmentInterface$Factory;

    return-object p0
.end method

.method private cleanUpTask(Lnet/vrallev/android/task/Task;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lnet/vrallev/android/task/Task<",
            "*>;)V"
        }
    .end annotation

    .line 175
    invoke-virtual {p1}, Lnet/vrallev/android/task/Task;->setFinished()V

    .line 176
    invoke-direct {p0, p1}, Lnet/vrallev/android/task/TaskExecutor;->removeTask(Lnet/vrallev/android/task/Task;)V

    return-void
.end method

.method private declared-synchronized executeInner(Lnet/vrallev/android/task/Task;Landroid/app/Activity;Lnet/vrallev/android/task/TaskCacheFragmentInterface;Ljava/lang/String;Ljava/lang/String;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lnet/vrallev/android/task/Task<",
            "*>;",
            "Landroid/app/Activity;",
            "Lnet/vrallev/android/task/TaskCacheFragmentInterface;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")I"
        }
    .end annotation

    monitor-enter p0

    .line 85
    :try_start_0
    invoke-virtual {p0}, Lnet/vrallev/android/task/TaskExecutor;->isShutdown()Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 p1, -0x1

    .line 86
    monitor-exit p0

    return p1

    .line 89
    :cond_0
    :try_start_1
    iget-object v0, p0, Lnet/vrallev/android/task/TaskExecutor;->mApplication:Landroid/app/Application;

    if-nez v0, :cond_1

    .line 90
    invoke-virtual {p2}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object p2

    iput-object p2, p0, Lnet/vrallev/android/task/TaskExecutor;->mApplication:Landroid/app/Application;

    .line 93
    :cond_1
    sget-object p2, Lnet/vrallev/android/task/TaskExecutor;->TASK_COUNTER:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {p2}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result p2

    .line 95
    invoke-virtual {p1, p2}, Lnet/vrallev/android/task/Task;->setKey(I)V

    .line 96
    invoke-virtual {p1, p0}, Lnet/vrallev/android/task/Task;->setTaskExecutor(Lnet/vrallev/android/task/TaskExecutor;)V

    .line 97
    invoke-virtual {p1, p3}, Lnet/vrallev/android/task/Task;->setCacheFragment(Lnet/vrallev/android/task/TaskCacheFragmentInterface;)V

    .line 98
    invoke-virtual {p1, p4}, Lnet/vrallev/android/task/Task;->setAnnotationId(Ljava/lang/String;)V

    .line 99
    invoke-virtual {p1, p5}, Lnet/vrallev/android/task/Task;->setFragmentId(Ljava/lang/String;)V

    .line 101
    iget-object p4, p0, Lnet/vrallev/android/task/TaskExecutor;->mTasks:Landroid/util/SparseArray;

    invoke-virtual {p4, p2, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 103
    new-instance p4, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;

    const/4 p5, 0x0

    invoke-direct {p4, p0, p1, p3, p5}, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;-><init>(Lnet/vrallev/android/task/TaskExecutor;Lnet/vrallev/android/task/Task;Lnet/vrallev/android/task/TaskCacheFragmentInterface;Lnet/vrallev/android/task/TaskExecutor$1;)V

    .line 104
    iget-object p1, p0, Lnet/vrallev/android/task/TaskExecutor;->mApplication:Landroid/app/Application;

    invoke-virtual {p1, p4}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 105
    iget-object p1, p0, Lnet/vrallev/android/task/TaskExecutor;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    invoke-interface {p1, p4}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 107
    monitor-exit p0

    return p2

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public static getInstance()Lnet/vrallev/android/task/TaskExecutor;
    .locals 2

    .line 36
    sget-object v0, Lnet/vrallev/android/task/TaskExecutor;->instance:Lnet/vrallev/android/task/TaskExecutor;

    if-nez v0, :cond_1

    .line 37
    const-class v0, Lnet/vrallev/android/task/TaskExecutor;

    monitor-enter v0

    .line 38
    :try_start_0
    sget-object v1, Lnet/vrallev/android/task/TaskExecutor;->instance:Lnet/vrallev/android/task/TaskExecutor;

    if-nez v1, :cond_0

    .line 39
    new-instance v1, Lnet/vrallev/android/task/TaskExecutor$Builder;

    invoke-direct {v1}, Lnet/vrallev/android/task/TaskExecutor$Builder;-><init>()V

    invoke-virtual {v1}, Lnet/vrallev/android/task/TaskExecutor$Builder;->build()Lnet/vrallev/android/task/TaskExecutor;

    move-result-object v1

    invoke-virtual {v1}, Lnet/vrallev/android/task/TaskExecutor;->asSingleton()Lnet/vrallev/android/task/TaskExecutor;

    .line 43
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 46
    :cond_1
    :goto_0
    sget-object v0, Lnet/vrallev/android/task/TaskExecutor;->instance:Lnet/vrallev/android/task/TaskExecutor;

    return-object v0
.end method

.method private declared-synchronized removeTask(Lnet/vrallev/android/task/Task;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lnet/vrallev/android/task/Task<",
            "*>;)V"
        }
    .end annotation

    monitor-enter p0

    .line 140
    :try_start_0
    iget-object v0, p0, Lnet/vrallev/android/task/TaskExecutor;->mTasks:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->indexOfValue(Ljava/lang/Object;)I

    move-result p1

    if-ltz p1, :cond_0

    .line 142
    iget-object v0, p0, Lnet/vrallev/android/task/TaskExecutor;->mTasks:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->removeAt(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 144
    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method


# virtual methods
.method public asSingleton()Lnet/vrallev/android/task/TaskExecutor;
    .locals 2

    .line 147
    const-class v0, Lnet/vrallev/android/task/TaskExecutor;

    monitor-enter v0

    .line 148
    :try_start_0
    sput-object p0, Lnet/vrallev/android/task/TaskExecutor;->instance:Lnet/vrallev/android/task/TaskExecutor;

    .line 149
    monitor-exit v0

    return-object p0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public declared-synchronized execute(Lnet/vrallev/android/task/Task;Landroid/app/Activity;)I
    .locals 1
    .param p1    # Lnet/vrallev/android/task/Task;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/app/Activity;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lnet/vrallev/android/task/Task<",
            "*>;",
            "Landroid/app/Activity;",
            ")I"
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x0

    .line 77
    :try_start_0
    invoke-virtual {p0, p1, p2, v0}, Lnet/vrallev/android/task/TaskExecutor;->execute(Lnet/vrallev/android/task/Task;Landroid/app/Activity;Ljava/lang/String;)I

    move-result p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized execute(Lnet/vrallev/android/task/Task;Landroid/app/Activity;Ljava/lang/String;)I
    .locals 7
    .param p1    # Lnet/vrallev/android/task/Task;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/app/Activity;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lnet/vrallev/android/task/Task<",
            "*>;",
            "Landroid/app/Activity;",
            "Ljava/lang/String;",
            ")I"
        }
    .end annotation

    monitor-enter p0

    .line 81
    :try_start_0
    iget-object v0, p0, Lnet/vrallev/android/task/TaskExecutor;->mCacheFactory:Lnet/vrallev/android/task/TaskCacheFragmentInterface$Factory;

    invoke-interface {v0, p2}, Lnet/vrallev/android/task/TaskCacheFragmentInterface$Factory;->create(Landroid/app/Activity;)Lnet/vrallev/android/task/TaskCacheFragmentInterface;

    move-result-object v4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, p3

    invoke-direct/range {v1 .. v6}, Lnet/vrallev/android/task/TaskExecutor;->executeInner(Lnet/vrallev/android/task/Task;Landroid/app/Activity;Lnet/vrallev/android/task/TaskCacheFragmentInterface;Ljava/lang/String;Ljava/lang/String;)I

    move-result p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized execute(Lnet/vrallev/android/task/Task;Landroid/support/v4/app/Fragment;)I
    .locals 1
    .param p1    # Lnet/vrallev/android/task/Task;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/support/v4/app/Fragment;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lnet/vrallev/android/task/Task<",
            "*>;",
            "Landroid/support/v4/app/Fragment;",
            ")I"
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x0

    .line 68
    :try_start_0
    invoke-virtual {p0, p1, p2, v0}, Lnet/vrallev/android/task/TaskExecutor;->execute(Lnet/vrallev/android/task/Task;Landroid/support/v4/app/Fragment;Ljava/lang/String;)I

    move-result p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized execute(Lnet/vrallev/android/task/Task;Landroid/support/v4/app/Fragment;Ljava/lang/String;)I
    .locals 6
    .param p1    # Lnet/vrallev/android/task/Task;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/support/v4/app/Fragment;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lnet/vrallev/android/task/Task<",
            "*>;",
            "Landroid/support/v4/app/Fragment;",
            "Ljava/lang/String;",
            ")I"
        }
    .end annotation

    monitor-enter p0

    .line 72
    :try_start_0
    invoke-virtual {p2}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    .line 73
    iget-object v0, p0, Lnet/vrallev/android/task/TaskExecutor;->mCacheFactory:Lnet/vrallev/android/task/TaskCacheFragmentInterface$Factory;

    invoke-interface {v0, v2}, Lnet/vrallev/android/task/TaskCacheFragmentInterface$Factory;->create(Landroid/app/Activity;)Lnet/vrallev/android/task/TaskCacheFragmentInterface;

    move-result-object v3

    invoke-static {p2}, Lnet/vrallev/android/task/FragmentIdHelper;->getFragmentId(Landroid/support/v4/app/Fragment;)Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lnet/vrallev/android/task/TaskExecutor;->executeInner(Lnet/vrallev/android/task/Task;Landroid/app/Activity;Lnet/vrallev/android/task/TaskCacheFragmentInterface;Ljava/lang/String;Ljava/lang/String;)I

    move-result p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized getAllTasks()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lnet/vrallev/android/task/Task<",
            "*>;>;"
        }
    .end annotation

    monitor-enter p0

    .line 120
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    .line 121
    :goto_0
    iget-object v2, p0, Lnet/vrallev/android/task/TaskExecutor;->mTasks:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 122
    iget-object v2, p0, Lnet/vrallev/android/task/TaskExecutor;->mTasks:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 124
    :cond_0
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getAllTasks(Ljava/lang/Class;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lnet/vrallev/android/task/Task<",
            "*>;>(",
            "Ljava/lang/Class<",
            "TT;>;)",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    monitor-enter p0

    .line 129
    :try_start_0
    invoke-virtual {p0}, Lnet/vrallev/android/task/TaskExecutor;->getAllTasks()Ljava/util/List;

    move-result-object v0

    .line 130
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 131
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 132
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lnet/vrallev/android/task/Task;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 133
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 136
    :cond_1
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized getTask(I)Lnet/vrallev/android/task/Task;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lnet/vrallev/android/task/Task<",
            "*>;"
        }
    .end annotation

    monitor-enter p0

    .line 112
    :try_start_0
    iget-object v0, p0, Lnet/vrallev/android/task/TaskExecutor;->mTasks:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-gez v0, :cond_0

    const/4 p1, 0x0

    .line 113
    monitor-exit p0

    return-object p1

    .line 115
    :cond_0
    :try_start_1
    iget-object v0, p0, Lnet/vrallev/android/task/TaskExecutor;->mTasks:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lnet/vrallev/android/task/Task;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized isShutdown()Z
    .locals 1

    monitor-enter p0

    .line 165
    :try_start_0
    iget-object v0, p0, Lnet/vrallev/android/task/TaskExecutor;->mExecutorService:Ljava/util/concurrent/ExecutorService;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method postResultNow(Landroid/util/Pair;Ljava/lang/Object;Lnet/vrallev/android/task/Task;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair<",
            "Ljava/lang/reflect/Method;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/Object;",
            "Lnet/vrallev/android/task/Task<",
            "*>;)V"
        }
    .end annotation

    .line 169
    invoke-direct {p0, p3}, Lnet/vrallev/android/task/TaskExecutor;->cleanUpTask(Lnet/vrallev/android/task/Task;)V

    .line 171
    iget-object v0, p0, Lnet/vrallev/android/task/TaskExecutor;->mTargetMethodFinder:Lnet/vrallev/android/task/TargetMethodFinder;

    invoke-virtual {v0, p1, p2, p3}, Lnet/vrallev/android/task/TargetMethodFinder;->invoke(Landroid/util/Pair;Ljava/lang/Object;Lnet/vrallev/android/task/Task;)V

    return-void
.end method

.method public declared-synchronized shutdown()V
    .locals 3

    monitor-enter p0

    .line 154
    :try_start_0
    iget-object v0, p0, Lnet/vrallev/android/task/TaskExecutor;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    const/4 v0, 0x0

    .line 155
    iput-object v0, p0, Lnet/vrallev/android/task/TaskExecutor;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    .line 157
    const-class v1, Lnet/vrallev/android/task/TaskExecutor;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 158
    :try_start_1
    sget-object v2, Lnet/vrallev/android/task/TaskExecutor;->instance:Lnet/vrallev/android/task/TaskExecutor;

    if-ne p0, v2, :cond_0

    .line 159
    sput-object v0, Lnet/vrallev/android/task/TaskExecutor;->instance:Lnet/vrallev/android/task/TaskExecutor;

    .line 161
    :cond_0
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 162
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    .line 161
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

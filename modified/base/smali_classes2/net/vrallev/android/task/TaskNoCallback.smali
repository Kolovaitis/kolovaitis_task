.class public abstract Lnet/vrallev/android/task/TaskNoCallback;
.super Lnet/vrallev/android/task/Task;
.source "TaskNoCallback.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lnet/vrallev/android/task/Task<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Lnet/vrallev/android/task/Task;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic execute()Ljava/lang/Object;
    .locals 1

    .line 6
    invoke-virtual {p0}, Lnet/vrallev/android/task/TaskNoCallback;->execute()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected final execute()Ljava/lang/Void;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lnet/vrallev/android/task/TaskNoCallback;->executeTask()V

    const/4 v0, 0x0

    return-object v0
.end method

.method protected abstract executeTask()V
.end method

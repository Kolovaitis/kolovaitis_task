.class final Lnet/vrallev/android/task/MethodHolder;
.super Ljava/lang/Object;
.source "MethodHolder.java"


# static fields
.field private static final NULL_METHOD_HOLDER:Lnet/vrallev/android/task/MethodHolder;

.field private static final POOL:Landroid/support/v4/util/Pools$SynchronizedPool;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/Pools$SynchronizedPool<",
            "Lnet/vrallev/android/task/MethodHolder;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mMethod:Ljava/lang/reflect/Method;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 12
    new-instance v0, Landroid/support/v4/util/Pools$SynchronizedPool;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, Landroid/support/v4/util/Pools$SynchronizedPool;-><init>(I)V

    sput-object v0, Lnet/vrallev/android/task/MethodHolder;->POOL:Landroid/support/v4/util/Pools$SynchronizedPool;

    .line 14
    new-instance v0, Lnet/vrallev/android/task/MethodHolder;

    invoke-direct {v0}, Lnet/vrallev/android/task/MethodHolder;-><init>()V

    sput-object v0, Lnet/vrallev/android/task/MethodHolder;->NULL_METHOD_HOLDER:Lnet/vrallev/android/task/MethodHolder;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private init(Ljava/lang/reflect/Method;)V
    .locals 0

    .line 36
    iput-object p1, p0, Lnet/vrallev/android/task/MethodHolder;->mMethod:Ljava/lang/reflect/Method;

    return-void
.end method

.method public static obtain(Ljava/lang/reflect/Method;)Lnet/vrallev/android/task/MethodHolder;
    .locals 1

    if-nez p0, :cond_0

    .line 18
    sget-object p0, Lnet/vrallev/android/task/MethodHolder;->NULL_METHOD_HOLDER:Lnet/vrallev/android/task/MethodHolder;

    return-object p0

    .line 21
    :cond_0
    sget-object v0, Lnet/vrallev/android/task/MethodHolder;->POOL:Landroid/support/v4/util/Pools$SynchronizedPool;

    invoke-virtual {v0}, Landroid/support/v4/util/Pools$SynchronizedPool;->acquire()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/vrallev/android/task/MethodHolder;

    if-nez v0, :cond_1

    .line 23
    new-instance v0, Lnet/vrallev/android/task/MethodHolder;

    invoke-direct {v0}, Lnet/vrallev/android/task/MethodHolder;-><init>()V

    .line 26
    :cond_1
    invoke-direct {v0, p0}, Lnet/vrallev/android/task/MethodHolder;->init(Ljava/lang/reflect/Method;)V

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_5

    .line 50
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_2

    .line 52
    :cond_1
    check-cast p1, Lnet/vrallev/android/task/MethodHolder;

    .line 54
    iget-object v2, p0, Lnet/vrallev/android/task/MethodHolder;->mMethod:Ljava/lang/reflect/Method;

    if-eqz v2, :cond_2

    iget-object p1, p1, Lnet/vrallev/android/task/MethodHolder;->mMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v2, p1}, Ljava/lang/reflect/Method;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_4

    goto :goto_0

    :cond_2
    iget-object p1, p1, Lnet/vrallev/android/task/MethodHolder;->mMethod:Ljava/lang/reflect/Method;

    if-nez p1, :cond_3

    goto :goto_1

    :cond_3
    :goto_0
    const/4 v0, 0x0

    :cond_4
    :goto_1
    return v0

    :cond_5
    :goto_2
    return v1
.end method

.method public getMethod()Ljava/lang/reflect/Method;
    .locals 1

    .line 40
    iget-object v0, p0, Lnet/vrallev/android/task/MethodHolder;->mMethod:Ljava/lang/reflect/Method;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .line 60
    iget-object v0, p0, Lnet/vrallev/android/task/MethodHolder;->mMethod:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/reflect/Method;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public recycle()V
    .locals 1

    .line 44
    sget-object v0, Lnet/vrallev/android/task/MethodHolder;->POOL:Landroid/support/v4/util/Pools$SynchronizedPool;

    invoke-virtual {v0, p0}, Landroid/support/v4/util/Pools$SynchronizedPool;->release(Ljava/lang/Object;)Z

    return-void
.end method

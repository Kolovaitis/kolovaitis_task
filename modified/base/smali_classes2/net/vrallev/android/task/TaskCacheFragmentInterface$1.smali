.class final Lnet/vrallev/android/task/TaskCacheFragmentInterface$1;
.super Ljava/lang/Object;
.source "TaskCacheFragmentInterface.java"

# interfaces
.implements Lnet/vrallev/android/task/TaskCacheFragmentInterface$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/vrallev/android/task/TaskCacheFragmentInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Landroid/app/Activity;)Lnet/vrallev/android/task/TaskCacheFragmentInterface;
    .locals 1

    .line 39
    instance-of v0, p1, Landroid/support/v4/app/FragmentActivity;

    if-eqz v0, :cond_0

    .line 40
    check-cast p1, Landroid/support/v4/app/FragmentActivity;

    invoke-static {p1}, Lnet/vrallev/android/task/TaskCacheFragmentSupport;->getFrom(Landroid/support/v4/app/FragmentActivity;)Lnet/vrallev/android/task/TaskCacheFragmentSupport;

    move-result-object p1

    return-object p1

    .line 42
    :cond_0
    invoke-static {p1}, Lnet/vrallev/android/task/TaskCacheFragment;->getFrom(Landroid/app/Activity;)Lnet/vrallev/android/task/TaskCacheFragment;

    move-result-object p1

    return-object p1
.end method

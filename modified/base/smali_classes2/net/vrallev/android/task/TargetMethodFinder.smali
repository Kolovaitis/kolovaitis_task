.class Lnet/vrallev/android/task/TargetMethodFinder;
.super Ljava/lang/Object;
.source "TargetMethodFinder.java"


# static fields
.field private static final CACHE_METHOD:Landroid/support/v4/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LruCache<",
            "Lnet/vrallev/android/task/MethodHolderKey;",
            "Lnet/vrallev/android/task/MethodHolder;",
            ">;"
        }
    .end annotation
.end field

.field private static final CACHE_RETURN_TYPE:Landroid/support/v4/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LruCache<",
            "Ljava/lang/Class<",
            "*>;",
            "Ljava/lang/Class<",
            "*>;>;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "TargetMethodFinder"


# instance fields
.field private final mAnnotation:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "+",
            "Lnet/vrallev/android/task/TaskResult;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 26
    new-instance v0, Lnet/vrallev/android/task/TargetMethodFinder$1;

    const/16 v1, 0xf

    invoke-direct {v0, v1}, Lnet/vrallev/android/task/TargetMethodFinder$1;-><init>(I)V

    sput-object v0, Lnet/vrallev/android/task/TargetMethodFinder;->CACHE_RETURN_TYPE:Landroid/support/v4/util/LruCache;

    .line 33
    new-instance v0, Lnet/vrallev/android/task/TargetMethodFinder$2;

    const/16 v1, 0x19

    invoke-direct {v0, v1}, Lnet/vrallev/android/task/TargetMethodFinder$2;-><init>(I)V

    sput-object v0, Lnet/vrallev/android/task/TargetMethodFinder;->CACHE_METHOD:Landroid/support/v4/util/LruCache;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "+",
            "Lnet/vrallev/android/task/TaskResult;",
            ">;)V"
        }
    .end annotation

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Lnet/vrallev/android/task/TargetMethodFinder;->mAnnotation:Ljava/lang/Class;

    return-void
.end method

.method static synthetic access$000(Ljava/lang/Class;)Ljava/lang/Class;
    .locals 0

    .line 22
    invoke-static {p0}, Lnet/vrallev/android/task/TargetMethodFinder;->findReturnType(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$100(Lnet/vrallev/android/task/MethodHolderKey;)Ljava/lang/reflect/Method;
    .locals 0

    .line 22
    invoke-static {p0}, Lnet/vrallev/android/task/TargetMethodFinder;->findMethodInClass(Lnet/vrallev/android/task/MethodHolderKey;)Ljava/lang/reflect/Method;

    move-result-object p0

    return-object p0
.end method

.method private static findMethodInActivity(Landroid/app/Activity;Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/Class;Lnet/vrallev/android/task/Task;)Landroid/util/Pair;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/lang/Class<",
            "*>;",
            "Ljava/lang/Class<",
            "*>;",
            "Ljava/lang/Class<",
            "+",
            "Lnet/vrallev/android/task/TaskResult;",
            ">;",
            "Lnet/vrallev/android/task/Task<",
            "*>;)",
            "Landroid/util/Pair<",
            "Ljava/lang/reflect/Method;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 146
    const-class v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-class v0, Landroid/app/Activity;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 150
    :cond_0
    invoke-static {p1, p2, p3, p4}, Lnet/vrallev/android/task/TargetMethodFinder;->findMethodInClass(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/Class;Lnet/vrallev/android/task/Task;)Ljava/lang/reflect/Method;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 152
    new-instance p1, Landroid/util/Pair;

    invoke-direct {p1, v0, p0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object p1

    .line 155
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object p1

    invoke-static {p0, p1, p2, p3, p4}, Lnet/vrallev/android/task/TargetMethodFinder;->findMethodInActivity(Landroid/app/Activity;Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/Class;Lnet/vrallev/android/task/Task;)Landroid/util/Pair;

    move-result-object p0

    return-object p0

    :cond_2
    :goto_0
    const/4 p0, 0x0

    return-object p0
.end method

.method private static findMethodInActivityAndFragments(Landroid/support/v4/app/FragmentActivity;Ljava/lang/Class;Ljava/lang/Class;Lnet/vrallev/android/task/Task;Z)Landroid/util/Pair;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/app/FragmentActivity;",
            "Ljava/lang/Class<",
            "*>;",
            "Ljava/lang/Class<",
            "+",
            "Lnet/vrallev/android/task/TaskResult;",
            ">;",
            "Lnet/vrallev/android/task/Task<",
            "*>;Z)",
            "Landroid/util/Pair<",
            "Ljava/lang/reflect/Method;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 135
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {p0, v0, p1, p2, p3}, Lnet/vrallev/android/task/TargetMethodFinder;->findMethodInActivity(Landroid/app/Activity;Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/Class;Lnet/vrallev/android/task/Task;)Landroid/util/Pair;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 140
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object p0

    invoke-static {p0, p1, p2, p3, p4}, Lnet/vrallev/android/task/TargetMethodFinder;->findMethodInFragmentManager(Landroid/support/v4/app/FragmentManager;Ljava/lang/Class;Ljava/lang/Class;Lnet/vrallev/android/task/Task;Z)Landroid/util/Pair;

    move-result-object p0

    return-object p0
.end method

.method private static findMethodInClass(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/Class;Lnet/vrallev/android/task/Task;)Ljava/lang/reflect/Method;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "*>;",
            "Ljava/lang/Class<",
            "*>;",
            "Ljava/lang/Class<",
            "+",
            "Lnet/vrallev/android/task/TaskResult;",
            ">;",
            "Lnet/vrallev/android/task/Task<",
            "*>;)",
            "Ljava/lang/reflect/Method;"
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 213
    :cond_0
    invoke-static {p0, p1, p2, p3}, Lnet/vrallev/android/task/MethodHolderKey;->obtain(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/Class;Lnet/vrallev/android/task/Task;)Lnet/vrallev/android/task/MethodHolderKey;

    move-result-object p0

    .line 214
    sget-object p1, Lnet/vrallev/android/task/TargetMethodFinder;->CACHE_METHOD:Landroid/support/v4/util/LruCache;

    invoke-virtual {p1, p0}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lnet/vrallev/android/task/MethodHolder;

    invoke-virtual {p0}, Lnet/vrallev/android/task/MethodHolder;->getMethod()Ljava/lang/reflect/Method;

    move-result-object p0

    return-object p0
.end method

.method private static findMethodInClass(Lnet/vrallev/android/task/MethodHolderKey;)Ljava/lang/reflect/Method;
    .locals 16

    .line 218
    invoke-virtual/range {p0 .. p0}, Lnet/vrallev/android/task/MethodHolderKey;->getTarget()Ljava/lang/Class;

    move-result-object v0

    .line 219
    invoke-virtual/range {p0 .. p0}, Lnet/vrallev/android/task/MethodHolderKey;->getAnnotation()Ljava/lang/Class;

    move-result-object v1

    .line 220
    invoke-virtual/range {p0 .. p0}, Lnet/vrallev/android/task/MethodHolderKey;->getResultType()Ljava/lang/Class;

    move-result-object v2

    .line 221
    invoke-virtual/range {p0 .. p0}, Lnet/vrallev/android/task/MethodHolderKey;->getTaskClass()Ljava/lang/Class;

    move-result-object v3

    .line 223
    invoke-virtual/range {p0 .. p0}, Lnet/vrallev/android/task/MethodHolderKey;->getAnnotationId()Ljava/lang/String;

    move-result-object v4

    .line 224
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    const/4 v6, 0x1

    xor-int/2addr v5, v6

    const/4 v7, 0x0

    .line 228
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Class;->getDeclaredMethods()[Ljava/lang/reflect/Method;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_0

    return-object v7

    .line 240
    :cond_0
    array-length v8, v0

    const/4 v9, 0x0

    move-object v10, v7

    const/4 v7, 0x0

    :goto_0
    if-ge v7, v8, :cond_a

    aget-object v11, v0, v7

    .line 241
    invoke-virtual {v11, v1}, Ljava/lang/reflect/Method;->isAnnotationPresent(Ljava/lang/Class;)Z

    move-result v12

    if-nez v12, :cond_1

    goto/16 :goto_2

    :cond_1
    if-eqz v5, :cond_2

    .line 244
    invoke-virtual {v11, v1}, Ljava/lang/reflect/Method;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v12

    check-cast v12, Lnet/vrallev/android/task/TaskResult;

    invoke-interface {v12}, Lnet/vrallev/android/task/TaskResult;->id()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v4, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_2

    goto :goto_2

    :cond_2
    if-nez v5, :cond_3

    .line 247
    invoke-virtual {v11, v1}, Ljava/lang/reflect/Method;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v12

    check-cast v12, Lnet/vrallev/android/task/TaskResult;

    invoke-interface {v12}, Lnet/vrallev/android/task/TaskResult;->id()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_3

    goto :goto_2

    .line 251
    :cond_3
    invoke-virtual {v11}, Ljava/lang/reflect/Method;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v12

    .line 252
    array-length v13, v12

    if-eqz v13, :cond_9

    array-length v13, v12

    const/4 v14, 0x2

    if-le v13, v14, :cond_4

    goto :goto_2

    .line 255
    :cond_4
    aget-object v13, v12, v9

    invoke-virtual {v13, v2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v13

    if-eqz v13, :cond_6

    if-nez v10, :cond_5

    move-object v10, v11

    goto :goto_1

    :cond_5
    const-string v13, "TargetMethodFinder"

    .line 259
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Found another method, which is ignored "

    invoke-virtual {v15, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v15, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v13, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    :cond_6
    :goto_1
    aget-object v6, v12, v9

    invoke-virtual {v6, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_7

    const/4 v6, 0x1

    goto :goto_2

    .line 267
    :cond_7
    array-length v6, v12

    if-ne v6, v14, :cond_8

    const/4 v6, 0x1

    aget-object v12, v12, v6

    invoke-virtual {v12, v3}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v12

    if-nez v12, :cond_8

    goto :goto_2

    :cond_8
    return-object v11

    :cond_9
    :goto_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_a
    return-object v10

    :catch_0
    move-exception v0

    move-object v1, v0

    const-string v0, "TargetMethodFinder"

    .line 230
    invoke-virtual {v1}, Ljava/lang/Error;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    return-object v7
.end method

.method private static findMethodInFragment(Landroid/support/v4/app/Fragment;Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/Class;Lnet/vrallev/android/task/Task;Z)Landroid/util/Pair;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/app/Fragment;",
            "Ljava/lang/Class<",
            "*>;",
            "Ljava/lang/Class<",
            "*>;",
            "Ljava/lang/Class<",
            "+",
            "Lnet/vrallev/android/task/TaskResult;",
            ">;",
            "Lnet/vrallev/android/task/Task<",
            "*>;Z)",
            "Landroid/util/Pair<",
            "Ljava/lang/reflect/Method;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 161
    const-class v0, Landroid/support/v4/app/Fragment;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const-class v0, Landroid/support/v4/app/DialogFragment;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 165
    :cond_0
    invoke-virtual {p4}, Lnet/vrallev/android/task/Task;->getFragmentId()Ljava/lang/String;

    move-result-object v0

    .line 166
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 168
    invoke-static {p0}, Lnet/vrallev/android/task/FragmentIdHelper;->getFragmentId(Landroid/support/v4/app/Fragment;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p5}, Lnet/vrallev/android/task/FragmentIdHelper;->equals(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 169
    :cond_1
    invoke-static {p1, p2, p3, p4}, Lnet/vrallev/android/task/TargetMethodFinder;->findMethodInClass(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/Class;Lnet/vrallev/android/task/Task;)Ljava/lang/reflect/Method;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 171
    new-instance p1, Landroid/util/Pair;

    invoke-direct {p1, v0, p0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object p1

    .line 174
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v2

    move-object v1, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    invoke-static/range {v1 .. v6}, Lnet/vrallev/android/task/TargetMethodFinder;->findMethodInFragment(Landroid/support/v4/app/Fragment;Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/Class;Lnet/vrallev/android/task/Task;Z)Landroid/util/Pair;

    move-result-object p1

    if-eqz p1, :cond_3

    return-object p1

    .line 180
    :cond_3
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object p0

    invoke-static {p0, p2, p3, p4, p5}, Lnet/vrallev/android/task/TargetMethodFinder;->findMethodInFragmentManager(Landroid/support/v4/app/FragmentManager;Ljava/lang/Class;Ljava/lang/Class;Lnet/vrallev/android/task/Task;Z)Landroid/util/Pair;

    move-result-object p0

    return-object p0

    :cond_4
    :goto_0
    const/4 p0, 0x0

    return-object p0
.end method

.method private static findMethodInFragmentManager(Landroid/support/v4/app/FragmentManager;Ljava/lang/Class;Ljava/lang/Class;Lnet/vrallev/android/task/Task;Z)Landroid/util/Pair;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/app/FragmentManager;",
            "Ljava/lang/Class<",
            "*>;",
            "Ljava/lang/Class<",
            "+",
            "Lnet/vrallev/android/task/TaskResult;",
            ">;",
            "Lnet/vrallev/android/task/Task<",
            "*>;Z)",
            "Landroid/util/Pair<",
            "Ljava/lang/reflect/Method;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    .line 190
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentManager;->getFragments()Ljava/util/List;

    move-result-object p0

    if-nez p0, :cond_1

    return-object v0

    .line 195
    :cond_1
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_2
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Landroid/support/v4/app/Fragment;

    if-nez v2, :cond_3

    goto :goto_0

    .line 199
    :cond_3
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    move v7, p4

    invoke-static/range {v2 .. v7}, Lnet/vrallev/android/task/TargetMethodFinder;->findMethodInFragment(Landroid/support/v4/app/Fragment;Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/Class;Lnet/vrallev/android/task/Task;Z)Landroid/util/Pair;

    move-result-object v1

    if-eqz v1, :cond_2

    return-object v1

    :cond_4
    return-object v0
.end method

.method private static findReturnType(Ljava/lang/Class;)Ljava/lang/Class;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "*>;)",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 278
    const-class v0, Ljava/lang/Object;

    invoke-virtual {p0, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const-class v0, Lnet/vrallev/android/task/Task;

    invoke-virtual {p0, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_2

    .line 282
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Class;->getDeclaredMethods()[Ljava/lang/reflect/Method;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_4

    aget-object v3, v0, v2

    .line 283
    invoke-virtual {v3}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "execute"

    .line 284
    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    goto :goto_1

    .line 288
    :cond_1
    invoke-virtual {v3}, Ljava/lang/reflect/Method;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 289
    array-length v4, v4

    if-eqz v4, :cond_2

    goto :goto_1

    .line 293
    :cond_2
    invoke-virtual {v3}, Ljava/lang/reflect/Method;->getReturnType()Ljava/lang/Class;

    move-result-object v3

    .line 294
    const-class v4, Ljava/lang/Object;

    invoke-virtual {v4, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    return-object v3

    :cond_3
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 299
    :cond_4
    invoke-virtual {p0}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object p0

    invoke-static {p0}, Lnet/vrallev/android/task/TargetMethodFinder;->findReturnType(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object p0

    return-object p0

    :cond_5
    :goto_2
    const/4 p0, 0x0

    return-object p0
.end method


# virtual methods
.method public getMethod(Lnet/vrallev/android/task/TaskCacheFragmentInterface;Ljava/lang/Class;Lnet/vrallev/android/task/Task;)Landroid/util/Pair;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lnet/vrallev/android/task/TaskCacheFragmentInterface;",
            "Ljava/lang/Class<",
            "*>;",
            "Lnet/vrallev/android/task/Task<",
            "*>;)",
            "Landroid/util/Pair<",
            "Ljava/lang/reflect/Method;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 70
    invoke-interface {p1}, Lnet/vrallev/android/task/TaskCacheFragmentInterface;->getParentActivity()Landroid/app/Activity;

    move-result-object p1

    const/4 v0, 0x0

    if-nez p1, :cond_0

    const-string p1, "TargetMethodFinder"

    const-string p2, "Activity is null, can\'t find target"

    .line 72
    invoke-static {p1, p2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-object v0

    .line 77
    :cond_0
    instance-of v1, p1, Landroid/support/v4/app/FragmentActivity;

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v1, :cond_1

    .line 78
    check-cast p1, Landroid/support/v4/app/FragmentActivity;

    iget-object v1, p0, Lnet/vrallev/android/task/TargetMethodFinder;->mAnnotation:Ljava/lang/Class;

    invoke-static {p1, p2, v1, p3, v3}, Lnet/vrallev/android/task/TargetMethodFinder;->findMethodInActivityAndFragments(Landroid/support/v4/app/FragmentActivity;Ljava/lang/Class;Ljava/lang/Class;Lnet/vrallev/android/task/Task;Z)Landroid/util/Pair;

    move-result-object v1

    if-nez v1, :cond_2

    .line 80
    iget-object v1, p0, Lnet/vrallev/android/task/TargetMethodFinder;->mAnnotation:Ljava/lang/Class;

    invoke-static {p1, p2, v1, p3, v2}, Lnet/vrallev/android/task/TargetMethodFinder;->findMethodInActivityAndFragments(Landroid/support/v4/app/FragmentActivity;Ljava/lang/Class;Ljava/lang/Class;Lnet/vrallev/android/task/Task;Z)Landroid/util/Pair;

    move-result-object v1

    goto :goto_0

    .line 84
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    iget-object v4, p0, Lnet/vrallev/android/task/TargetMethodFinder;->mAnnotation:Ljava/lang/Class;

    invoke-static {p1, v1, p2, v4, p3}, Lnet/vrallev/android/task/TargetMethodFinder;->findMethodInActivity(Landroid/app/Activity;Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/Class;Lnet/vrallev/android/task/Task;)Landroid/util/Pair;

    move-result-object v1

    :cond_2
    :goto_0
    if-nez v1, :cond_3

    .line 90
    :try_start_0
    invoke-virtual {p3}, Lnet/vrallev/android/task/Task;->getResult()Ljava/lang/Object;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const-string p1, "TargetMethodFinder"

    const-string v4, "Didn\'t find method, result type %s, result %s, annotationId %s, fragmentId %s"

    const/4 v5, 0x4

    .line 95
    new-array v5, v5, [Ljava/lang/Object;

    aput-object p2, v5, v2

    aput-object v0, v5, v3

    const/4 p2, 0x2

    invoke-virtual {p3}, Lnet/vrallev/android/task/Task;->getAnnotationId()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, p2

    const/4 p2, 0x3

    invoke-virtual {p3}, Lnet/vrallev/android/task/Task;->getFragmentId()Ljava/lang/String;

    move-result-object p3

    aput-object p3, v5, p2

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    return-object v1
.end method

.method public getResultType(Ljava/lang/Object;Lnet/vrallev/android/task/Task;)Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lnet/vrallev/android/task/Task<",
            "*>;)",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 55
    invoke-virtual {p2}, Lnet/vrallev/android/task/Task;->getResultClass()Ljava/lang/Class;

    move-result-object v0

    if-nez v0, :cond_0

    .line 57
    sget-object v0, Lnet/vrallev/android/task/TargetMethodFinder;->CACHE_RETURN_TYPE:Landroid/support/v4/util/LruCache;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p2

    invoke-virtual {v0, p2}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    move-object v0, p2

    check-cast v0, Ljava/lang/Class;

    :cond_0
    if-nez v0, :cond_1

    if-eqz p1, :cond_1

    .line 60
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    :cond_1
    if-nez v0, :cond_2

    const-string p1, "TargetMethodFinder"

    const-string p2, "Couldn\'t find result type"

    .line 63
    invoke-static {p1, p2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-object v0
.end method

.method public invoke(Landroid/util/Pair;Ljava/lang/Object;Lnet/vrallev/android/task/Task;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair<",
            "Ljava/lang/reflect/Method;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/Object;",
            "Lnet/vrallev/android/task/Task<",
            "*>;)V"
        }
    .end annotation

    .line 102
    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/reflect/Method;

    iget-object p1, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-virtual {p0, v0, p1, p2, p3}, Lnet/vrallev/android/task/TargetMethodFinder;->invoke(Ljava/lang/reflect/Method;Ljava/lang/Object;Ljava/lang/Object;Lnet/vrallev/android/task/Task;)V

    return-void
.end method

.method public invoke(Ljava/lang/reflect/Method;Ljava/lang/Object;Ljava/lang/Object;Lnet/vrallev/android/task/Task;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Method;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Lnet/vrallev/android/task/Task<",
            "*>;)V"
        }
    .end annotation

    .line 109
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/reflect/Method;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v0

    array-length v0, v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    .line 110
    new-array v0, v3, [Ljava/lang/Object;

    aput-object p3, v0, v1

    aput-object p4, v0, v2

    invoke-virtual {p1, p2, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 112
    :cond_0
    new-array p4, v2, [Ljava/lang/Object;

    aput-object p3, p4, v1

    invoke-virtual {p1, p2, p4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const-string p2, "TargetMethodFinder"

    .line 119
    invoke-virtual {p1}, Ljava/lang/reflect/InvocationTargetException;->getMessage()Ljava/lang/String;

    move-result-object p3

    invoke-static {p2, p3, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_1
    move-exception p1

    const-string p2, "TargetMethodFinder"

    .line 116
    invoke-virtual {p1}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object p3

    invoke-static {p2, p3, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method

.class final Lnet/vrallev/android/task/MethodHolderKey;
.super Ljava/lang/Object;
.source "MethodHolderKey.java"


# static fields
.field private static final POOL:Landroid/support/v4/util/Pools$SynchronizedPool;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/Pools$SynchronizedPool<",
            "Lnet/vrallev/android/task/MethodHolderKey;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAnnotation:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "+",
            "Lnet/vrallev/android/task/TaskResult;",
            ">;"
        }
    .end annotation
.end field

.field private mAnnotationId:Ljava/lang/String;

.field private mResultType:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end field

.field private mTarget:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end field

.field private mTaskClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "+",
            "Lnet/vrallev/android/task/Task;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 10
    new-instance v0, Landroid/support/v4/util/Pools$SynchronizedPool;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, Landroid/support/v4/util/Pools$SynchronizedPool;-><init>(I)V

    sput-object v0, Lnet/vrallev/android/task/MethodHolderKey;->POOL:Landroid/support/v4/util/Pools$SynchronizedPool;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private init(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "*>;",
            "Ljava/lang/Class<",
            "*>;",
            "Ljava/lang/Class<",
            "+",
            "Lnet/vrallev/android/task/TaskResult;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/Class<",
            "+",
            "Lnet/vrallev/android/task/Task;",
            ">;)V"
        }
    .end annotation

    .line 34
    iput-object p1, p0, Lnet/vrallev/android/task/MethodHolderKey;->mTarget:Ljava/lang/Class;

    .line 35
    iput-object p2, p0, Lnet/vrallev/android/task/MethodHolderKey;->mResultType:Ljava/lang/Class;

    .line 36
    iput-object p3, p0, Lnet/vrallev/android/task/MethodHolderKey;->mAnnotation:Ljava/lang/Class;

    .line 37
    iput-object p4, p0, Lnet/vrallev/android/task/MethodHolderKey;->mAnnotationId:Ljava/lang/String;

    .line 38
    iput-object p5, p0, Lnet/vrallev/android/task/MethodHolderKey;->mTaskClass:Ljava/lang/Class;

    return-void
.end method

.method public static obtain(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/Class;Lnet/vrallev/android/task/Task;)Lnet/vrallev/android/task/MethodHolderKey;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "*>;",
            "Ljava/lang/Class<",
            "*>;",
            "Ljava/lang/Class<",
            "+",
            "Lnet/vrallev/android/task/TaskResult;",
            ">;",
            "Lnet/vrallev/android/task/Task<",
            "*>;)",
            "Lnet/vrallev/android/task/MethodHolderKey;"
        }
    .end annotation

    .line 13
    sget-object v0, Lnet/vrallev/android/task/MethodHolderKey;->POOL:Landroid/support/v4/util/Pools$SynchronizedPool;

    invoke-virtual {v0}, Landroid/support/v4/util/Pools$SynchronizedPool;->acquire()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/vrallev/android/task/MethodHolderKey;

    if-nez v0, :cond_0

    .line 15
    new-instance v0, Lnet/vrallev/android/task/MethodHolderKey;

    invoke-direct {v0}, Lnet/vrallev/android/task/MethodHolderKey;-><init>()V

    .line 18
    :cond_0
    invoke-virtual {p3}, Lnet/vrallev/android/task/Task;->getAnnotationId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    move-object v1, v0

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v1 .. v6}, Lnet/vrallev/android/task/MethodHolderKey;->init(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Class;)V

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_d

    .line 68
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_6

    .line 70
    :cond_1
    check-cast p1, Lnet/vrallev/android/task/MethodHolderKey;

    .line 72
    iget-object v2, p0, Lnet/vrallev/android/task/MethodHolderKey;->mTarget:Ljava/lang/Class;

    if-eqz v2, :cond_2

    iget-object v3, p1, Lnet/vrallev/android/task/MethodHolderKey;->mTarget:Ljava/lang/Class;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    goto :goto_0

    :cond_2
    iget-object v2, p1, Lnet/vrallev/android/task/MethodHolderKey;->mTarget:Ljava/lang/Class;

    if-eqz v2, :cond_3

    :goto_0
    return v1

    .line 73
    :cond_3
    iget-object v2, p0, Lnet/vrallev/android/task/MethodHolderKey;->mResultType:Ljava/lang/Class;

    if-eqz v2, :cond_4

    iget-object v3, p1, Lnet/vrallev/android/task/MethodHolderKey;->mResultType:Ljava/lang/Class;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    goto :goto_1

    :cond_4
    iget-object v2, p1, Lnet/vrallev/android/task/MethodHolderKey;->mResultType:Ljava/lang/Class;

    if-eqz v2, :cond_5

    :goto_1
    return v1

    .line 75
    :cond_5
    iget-object v2, p0, Lnet/vrallev/android/task/MethodHolderKey;->mAnnotation:Ljava/lang/Class;

    if-eqz v2, :cond_6

    iget-object v3, p1, Lnet/vrallev/android/task/MethodHolderKey;->mAnnotation:Ljava/lang/Class;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    goto :goto_2

    :cond_6
    iget-object v2, p1, Lnet/vrallev/android/task/MethodHolderKey;->mAnnotation:Ljava/lang/Class;

    if-eqz v2, :cond_7

    :goto_2
    return v1

    .line 78
    :cond_7
    iget-object v2, p0, Lnet/vrallev/android/task/MethodHolderKey;->mAnnotationId:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v3, p1, Lnet/vrallev/android/task/MethodHolderKey;->mAnnotationId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    goto :goto_3

    :cond_8
    iget-object v2, p1, Lnet/vrallev/android/task/MethodHolderKey;->mAnnotationId:Ljava/lang/String;

    if-eqz v2, :cond_9

    :goto_3
    return v1

    .line 80
    :cond_9
    iget-object v2, p0, Lnet/vrallev/android/task/MethodHolderKey;->mTaskClass:Ljava/lang/Class;

    if-eqz v2, :cond_a

    iget-object p1, p1, Lnet/vrallev/android/task/MethodHolderKey;->mTaskClass:Ljava/lang/Class;

    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_c

    goto :goto_4

    :cond_a
    iget-object p1, p1, Lnet/vrallev/android/task/MethodHolderKey;->mTaskClass:Ljava/lang/Class;

    if-nez p1, :cond_b

    goto :goto_5

    :cond_b
    :goto_4
    const/4 v0, 0x0

    :cond_c
    :goto_5
    return v0

    :cond_d
    :goto_6
    return v1
.end method

.method public getAnnotation()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "+",
            "Lnet/vrallev/android/task/TaskResult;",
            ">;"
        }
    .end annotation

    .line 54
    iget-object v0, p0, Lnet/vrallev/android/task/MethodHolderKey;->mAnnotation:Ljava/lang/Class;

    return-object v0
.end method

.method public getAnnotationId()Ljava/lang/String;
    .locals 1

    .line 58
    iget-object v0, p0, Lnet/vrallev/android/task/MethodHolderKey;->mAnnotationId:Ljava/lang/String;

    return-object v0
.end method

.method public getResultType()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 50
    iget-object v0, p0, Lnet/vrallev/android/task/MethodHolderKey;->mResultType:Ljava/lang/Class;

    return-object v0
.end method

.method public getTarget()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 46
    iget-object v0, p0, Lnet/vrallev/android/task/MethodHolderKey;->mTarget:Ljava/lang/Class;

    return-object v0
.end method

.method public getTaskClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "+",
            "Lnet/vrallev/android/task/Task;",
            ">;"
        }
    .end annotation

    .line 62
    iget-object v0, p0, Lnet/vrallev/android/task/MethodHolderKey;->mTaskClass:Ljava/lang/Class;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .line 86
    iget-object v0, p0, Lnet/vrallev/android/task/MethodHolderKey;->mTarget:Ljava/lang/Class;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    .line 87
    iget-object v2, p0, Lnet/vrallev/android/task/MethodHolderKey;->mResultType:Ljava/lang/Class;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 88
    iget-object v2, p0, Lnet/vrallev/android/task/MethodHolderKey;->mAnnotation:Ljava/lang/Class;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 89
    iget-object v2, p0, Lnet/vrallev/android/task/MethodHolderKey;->mAnnotationId:Ljava/lang/String;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 90
    iget-object v2, p0, Lnet/vrallev/android/task/MethodHolderKey;->mTaskClass:Ljava/lang/Class;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_4
    add-int/2addr v0, v1

    return v0
.end method

.method public recycle()V
    .locals 1

    .line 42
    sget-object v0, Lnet/vrallev/android/task/MethodHolderKey;->POOL:Landroid/support/v4/util/Pools$SynchronizedPool;

    invoke-virtual {v0, p0}, Landroid/support/v4/util/Pools$SynchronizedPool;->release(Ljava/lang/Object;)Z

    return-void
.end method

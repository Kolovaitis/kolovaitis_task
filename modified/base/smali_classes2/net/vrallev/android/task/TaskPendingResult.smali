.class public Lnet/vrallev/android/task/TaskPendingResult;
.super Ljava/lang/Object;
.source "TaskPendingResult.java"


# instance fields
.field private final mResult:Ljava/lang/Object;

.field private final mResultType:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end field

.field private final mTask:Lnet/vrallev/android/task/Task;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lnet/vrallev/android/task/Task<",
            "*>;"
        }
    .end annotation
.end field

.field private final mTaskExecutor:Lnet/vrallev/android/task/TaskExecutor;


# direct methods
.method public constructor <init>(Ljava/lang/Class;Ljava/lang/Object;Lnet/vrallev/android/task/Task;Lnet/vrallev/android/task/TaskExecutor;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "*>;",
            "Ljava/lang/Object;",
            "Lnet/vrallev/android/task/Task<",
            "*>;",
            "Lnet/vrallev/android/task/TaskExecutor;",
            ")V"
        }
    .end annotation

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lnet/vrallev/android/task/TaskPendingResult;->mResultType:Ljava/lang/Class;

    .line 15
    iput-object p2, p0, Lnet/vrallev/android/task/TaskPendingResult;->mResult:Ljava/lang/Object;

    .line 16
    iput-object p3, p0, Lnet/vrallev/android/task/TaskPendingResult;->mTask:Lnet/vrallev/android/task/Task;

    .line 17
    iput-object p4, p0, Lnet/vrallev/android/task/TaskPendingResult;->mTaskExecutor:Lnet/vrallev/android/task/TaskExecutor;

    return-void
.end method


# virtual methods
.method public getResult()Ljava/lang/Object;
    .locals 1

    .line 25
    iget-object v0, p0, Lnet/vrallev/android/task/TaskPendingResult;->mResult:Ljava/lang/Object;

    return-object v0
.end method

.method public getResultType()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 21
    iget-object v0, p0, Lnet/vrallev/android/task/TaskPendingResult;->mResultType:Ljava/lang/Class;

    return-object v0
.end method

.method public getTask()Lnet/vrallev/android/task/Task;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lnet/vrallev/android/task/Task<",
            "*>;"
        }
    .end annotation

    .line 29
    iget-object v0, p0, Lnet/vrallev/android/task/TaskPendingResult;->mTask:Lnet/vrallev/android/task/Task;

    return-object v0
.end method

.method public getTaskExecutor()Lnet/vrallev/android/task/TaskExecutor;
    .locals 1

    .line 33
    iget-object v0, p0, Lnet/vrallev/android/task/TaskPendingResult;->mTaskExecutor:Lnet/vrallev/android/task/TaskExecutor;

    return-object v0
.end method

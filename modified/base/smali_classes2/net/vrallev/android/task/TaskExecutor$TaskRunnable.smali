.class final Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;
.super Ljava/lang/Object;
.source "TaskExecutor.java"

# interfaces
.implements Ljava/lang/Runnable;
.implements Landroid/app/Application$ActivityLifecycleCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/vrallev/android/task/TaskExecutor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "TaskRunnable"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Runnable;",
        "Landroid/app/Application$ActivityLifecycleCallbacks;"
    }
.end annotation


# instance fields
.field private final mTask:Lnet/vrallev/android/task/Task;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lnet/vrallev/android/task/Task<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final mWeakReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lnet/vrallev/android/task/TaskCacheFragmentInterface;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lnet/vrallev/android/task/TaskExecutor;


# direct methods
.method private constructor <init>(Lnet/vrallev/android/task/TaskExecutor;Lnet/vrallev/android/task/Task;Lnet/vrallev/android/task/TaskCacheFragmentInterface;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lnet/vrallev/android/task/Task<",
            "TT;>;",
            "Lnet/vrallev/android/task/TaskCacheFragmentInterface;",
            ")V"
        }
    .end annotation

    .line 184
    iput-object p1, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->this$0:Lnet/vrallev/android/task/TaskExecutor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 185
    iput-object p2, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->mTask:Lnet/vrallev/android/task/Task;

    .line 186
    new-instance p1, Ljava/lang/ref/WeakReference;

    invoke-direct {p1, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->mWeakReference:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method synthetic constructor <init>(Lnet/vrallev/android/task/TaskExecutor;Lnet/vrallev/android/task/Task;Lnet/vrallev/android/task/TaskCacheFragmentInterface;Lnet/vrallev/android/task/TaskExecutor$1;)V
    .locals 0

    .line 179
    invoke-direct {p0, p1, p2, p3}, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;-><init>(Lnet/vrallev/android/task/TaskExecutor;Lnet/vrallev/android/task/Task;Lnet/vrallev/android/task/TaskCacheFragmentInterface;)V

    return-void
.end method

.method static synthetic access$500(Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;)Lnet/vrallev/android/task/Task;
    .locals 0

    .line 179
    iget-object p0, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->mTask:Lnet/vrallev/android/task/Task;

    return-object p0
.end method

.method private postResult(Ljava/lang/Object;Lnet/vrallev/android/task/TaskCacheFragmentInterface;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lnet/vrallev/android/task/TaskCacheFragmentInterface;",
            ")V"
        }
    .end annotation

    .line 207
    iget-object v0, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->this$0:Lnet/vrallev/android/task/TaskExecutor;

    invoke-virtual {v0}, Lnet/vrallev/android/task/TaskExecutor;->isShutdown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 208
    iget-object p1, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->this$0:Lnet/vrallev/android/task/TaskExecutor;

    iget-object p2, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->mTask:Lnet/vrallev/android/task/Task;

    invoke-static {p1, p2}, Lnet/vrallev/android/task/TaskExecutor;->access$100(Lnet/vrallev/android/task/TaskExecutor;Lnet/vrallev/android/task/Task;)V

    .line 209
    iget-object p1, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->this$0:Lnet/vrallev/android/task/TaskExecutor;

    invoke-static {p1}, Lnet/vrallev/android/task/TaskExecutor;->access$200(Lnet/vrallev/android/task/TaskExecutor;)Landroid/app/Application;

    move-result-object p1

    invoke-virtual {p1, p0}, Landroid/app/Application;->unregisterActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    return-void

    .line 213
    :cond_0
    iget-object v0, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->this$0:Lnet/vrallev/android/task/TaskExecutor;

    invoke-static {v0}, Lnet/vrallev/android/task/TaskExecutor;->access$300(Lnet/vrallev/android/task/TaskExecutor;)Lnet/vrallev/android/task/TargetMethodFinder;

    move-result-object v0

    iget-object v1, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->this$0:Lnet/vrallev/android/task/TaskExecutor;

    invoke-static {v1}, Lnet/vrallev/android/task/TaskExecutor;->access$300(Lnet/vrallev/android/task/TaskExecutor;)Lnet/vrallev/android/task/TargetMethodFinder;

    move-result-object v1

    iget-object v2, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->mTask:Lnet/vrallev/android/task/Task;

    invoke-virtual {v1, p1, v2}, Lnet/vrallev/android/task/TargetMethodFinder;->getResultType(Ljava/lang/Object;Lnet/vrallev/android/task/Task;)Ljava/lang/Class;

    move-result-object v1

    iget-object v2, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->mTask:Lnet/vrallev/android/task/Task;

    invoke-virtual {v0, p2, v1, v2}, Lnet/vrallev/android/task/TargetMethodFinder;->getMethod(Lnet/vrallev/android/task/TaskCacheFragmentInterface;Ljava/lang/Class;Lnet/vrallev/android/task/Task;)Landroid/util/Pair;

    move-result-object v0

    if-nez v0, :cond_1

    .line 215
    iget-object p1, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->this$0:Lnet/vrallev/android/task/TaskExecutor;

    iget-object p2, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->mTask:Lnet/vrallev/android/task/Task;

    invoke-static {p1, p2}, Lnet/vrallev/android/task/TaskExecutor;->access$100(Lnet/vrallev/android/task/TaskExecutor;Lnet/vrallev/android/task/Task;)V

    .line 216
    iget-object p1, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->this$0:Lnet/vrallev/android/task/TaskExecutor;

    invoke-static {p1}, Lnet/vrallev/android/task/TaskExecutor;->access$200(Lnet/vrallev/android/task/TaskExecutor;)Landroid/app/Application;

    move-result-object p1

    invoke-virtual {p1, p0}, Landroid/app/Application;->unregisterActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    return-void

    .line 220
    :cond_1
    iget-object v1, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->this$0:Lnet/vrallev/android/task/TaskExecutor;

    invoke-static {v1}, Lnet/vrallev/android/task/TaskExecutor;->access$400(Lnet/vrallev/android/task/TaskExecutor;)Lnet/vrallev/android/task/TaskExecutor$PostResult;

    move-result-object v1

    sget-object v2, Lnet/vrallev/android/task/TaskExecutor$PostResult;->IMMEDIATELY:Lnet/vrallev/android/task/TaskExecutor$PostResult;

    invoke-virtual {v1, v2}, Lnet/vrallev/android/task/TaskExecutor$PostResult;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 221
    iget-object p2, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->this$0:Lnet/vrallev/android/task/TaskExecutor;

    invoke-static {p2}, Lnet/vrallev/android/task/TaskExecutor;->access$200(Lnet/vrallev/android/task/TaskExecutor;)Landroid/app/Application;

    move-result-object p2

    invoke-virtual {p2, p0}, Landroid/app/Application;->unregisterActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 222
    iget-object p2, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->this$0:Lnet/vrallev/android/task/TaskExecutor;

    iget-object v1, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->mTask:Lnet/vrallev/android/task/Task;

    invoke-virtual {p2, v0, p1, v1}, Lnet/vrallev/android/task/TaskExecutor;->postResultNow(Landroid/util/Pair;Ljava/lang/Object;Lnet/vrallev/android/task/Task;)V

    return-void

    .line 226
    :cond_2
    invoke-interface {p2}, Lnet/vrallev/android/task/TaskCacheFragmentInterface;->canSaveInstanceState()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 227
    iget-object v1, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->this$0:Lnet/vrallev/android/task/TaskExecutor;

    invoke-static {v1}, Lnet/vrallev/android/task/TaskExecutor;->access$200(Lnet/vrallev/android/task/TaskExecutor;)Landroid/app/Application;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/app/Application;->unregisterActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 229
    iget-object v1, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->this$0:Lnet/vrallev/android/task/TaskExecutor;

    invoke-static {v1}, Lnet/vrallev/android/task/TaskExecutor;->access$400(Lnet/vrallev/android/task/TaskExecutor;)Lnet/vrallev/android/task/TaskExecutor$PostResult;

    move-result-object v1

    sget-object v2, Lnet/vrallev/android/task/TaskExecutor$PostResult;->ON_ANY_THREAD:Lnet/vrallev/android/task/TaskExecutor$PostResult;

    invoke-virtual {v1, v2}, Lnet/vrallev/android/task/TaskExecutor$PostResult;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 230
    iget-object p2, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->this$0:Lnet/vrallev/android/task/TaskExecutor;

    iget-object v1, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->mTask:Lnet/vrallev/android/task/Task;

    invoke-virtual {p2, v0, p1, v1}, Lnet/vrallev/android/task/TaskExecutor;->postResultNow(Landroid/util/Pair;Ljava/lang/Object;Lnet/vrallev/android/task/Task;)V

    goto :goto_0

    .line 233
    :cond_3
    invoke-interface {p2}, Lnet/vrallev/android/task/TaskCacheFragmentInterface;->getParentActivity()Landroid/app/Activity;

    move-result-object p2

    new-instance v1, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable$1;

    invoke-direct {v1, p0, v0, p1}, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable$1;-><init>(Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;Landroid/util/Pair;Ljava/lang/Object;)V

    invoke-virtual {p2, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 242
    :cond_4
    iget-object v0, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->this$0:Lnet/vrallev/android/task/TaskExecutor;

    invoke-static {v0}, Lnet/vrallev/android/task/TaskExecutor;->access$300(Lnet/vrallev/android/task/TaskExecutor;)Lnet/vrallev/android/task/TargetMethodFinder;

    move-result-object v0

    iget-object v1, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->mTask:Lnet/vrallev/android/task/Task;

    invoke-virtual {v0, p1, v1}, Lnet/vrallev/android/task/TargetMethodFinder;->getResultType(Ljava/lang/Object;Lnet/vrallev/android/task/Task;)Ljava/lang/Class;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 244
    new-instance v1, Lnet/vrallev/android/task/TaskPendingResult;

    iget-object v2, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->mTask:Lnet/vrallev/android/task/Task;

    iget-object v3, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->this$0:Lnet/vrallev/android/task/TaskExecutor;

    invoke-direct {v1, v0, p1, v2, v3}, Lnet/vrallev/android/task/TaskPendingResult;-><init>(Ljava/lang/Class;Ljava/lang/Object;Lnet/vrallev/android/task/Task;Lnet/vrallev/android/task/TaskExecutor;)V

    invoke-interface {p2, v1}, Lnet/vrallev/android/task/TaskCacheFragmentInterface;->putPendingResult(Lnet/vrallev/android/task/TaskPendingResult;)V

    :cond_5
    :goto_0
    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 2

    if-eqz p2, :cond_3

    .line 251
    iget-object v0, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->mTask:Lnet/vrallev/android/task/Task;

    invoke-virtual {v0}, Lnet/vrallev/android/task/Task;->isExecuting()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    .line 255
    :cond_0
    iget-object v0, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->mTask:Lnet/vrallev/android/task/Task;

    invoke-virtual {v0}, Lnet/vrallev/android/task/Task;->getKey()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result p2

    if-ne p2, v1, :cond_1

    .line 257
    iget-object p1, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->this$0:Lnet/vrallev/android/task/TaskExecutor;

    invoke-static {p1}, Lnet/vrallev/android/task/TaskExecutor;->access$200(Lnet/vrallev/android/task/TaskExecutor;)Landroid/app/Application;

    move-result-object p1

    invoke-virtual {p1, p0}, Landroid/app/Application;->unregisterActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    return-void

    .line 261
    :cond_1
    iget-object v0, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->mTask:Lnet/vrallev/android/task/Task;

    invoke-virtual {v0}, Lnet/vrallev/android/task/Task;->getKey()I

    move-result v0

    if-eq p2, v0, :cond_2

    return-void

    .line 265
    :cond_2
    iget-object p2, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->this$0:Lnet/vrallev/android/task/TaskExecutor;

    invoke-static {p2}, Lnet/vrallev/android/task/TaskExecutor;->access$200(Lnet/vrallev/android/task/TaskExecutor;)Landroid/app/Application;

    move-result-object p2

    invoke-virtual {p2, p0}, Landroid/app/Application;->unregisterActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 267
    iget-object p2, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->this$0:Lnet/vrallev/android/task/TaskExecutor;

    invoke-static {p2}, Lnet/vrallev/android/task/TaskExecutor;->access$600(Lnet/vrallev/android/task/TaskExecutor;)Lnet/vrallev/android/task/TaskCacheFragmentInterface$Factory;

    move-result-object p2

    invoke-interface {p2, p1}, Lnet/vrallev/android/task/TaskCacheFragmentInterface$Factory;->create(Landroid/app/Activity;)Lnet/vrallev/android/task/TaskCacheFragmentInterface;

    move-result-object p1

    .line 269
    :try_start_0
    iget-object p2, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->mTask:Lnet/vrallev/android/task/Task;

    invoke-virtual {p2}, Lnet/vrallev/android/task/Task;->getResult()Ljava/lang/Object;

    move-result-object p2

    invoke-direct {p0, p2, p1}, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->postResult(Ljava/lang/Object;Lnet/vrallev/android/task/TaskCacheFragmentInterface;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const-string p2, "TaskExecutor"

    const-string v0, "getResult failed"

    .line 271
    invoke-static {p2, v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    return-void

    :cond_3
    :goto_1
    return-void
.end method

.method public onActivityDestroyed(Landroid/app/Activity;)V
    .locals 0

    return-void
.end method

.method public onActivityPaused(Landroid/app/Activity;)V
    .locals 0

    return-void
.end method

.method public onActivityResumed(Landroid/app/Activity;)V
    .locals 0

    return-void
.end method

.method public onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 1

    .line 307
    iget-object v0, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->mWeakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/vrallev/android/task/TaskCacheFragmentInterface;

    if-eqz v0, :cond_1

    .line 308
    invoke-interface {v0}, Lnet/vrallev/android/task/TaskCacheFragmentInterface;->getParentActivity()Landroid/app/Activity;

    move-result-object v0

    if-eq v0, p1, :cond_0

    goto :goto_0

    .line 312
    :cond_0
    iget-object p1, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->mTask:Lnet/vrallev/android/task/Task;

    invoke-virtual {p1}, Lnet/vrallev/android/task/Task;->getKey()I

    move-result p1

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->mTask:Lnet/vrallev/android/task/Task;

    invoke-virtual {v0}, Lnet/vrallev/android/task/Task;->getKey()I

    move-result v0

    invoke-virtual {p2, p1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void

    :cond_1
    :goto_0
    return-void
.end method

.method public onActivityStarted(Landroid/app/Activity;)V
    .locals 1

    .line 277
    iget-object v0, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->mTask:Lnet/vrallev/android/task/Task;

    invoke-virtual {v0}, Lnet/vrallev/android/task/Task;->isExecuting()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 281
    :cond_0
    iget-object v0, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->this$0:Lnet/vrallev/android/task/TaskExecutor;

    invoke-static {v0}, Lnet/vrallev/android/task/TaskExecutor;->access$600(Lnet/vrallev/android/task/TaskExecutor;)Lnet/vrallev/android/task/TaskCacheFragmentInterface$Factory;

    move-result-object v0

    invoke-interface {v0, p1}, Lnet/vrallev/android/task/TaskCacheFragmentInterface$Factory;->create(Landroid/app/Activity;)Lnet/vrallev/android/task/TaskCacheFragmentInterface;

    move-result-object p1

    const-string v0, "PENDING_RESULT_KEY"

    .line 282
    invoke-interface {p1, v0}, Lnet/vrallev/android/task/TaskCacheFragmentInterface;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    if-eqz p1, :cond_1

    .line 283
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_1

    .line 284
    iget-object p1, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->this$0:Lnet/vrallev/android/task/TaskExecutor;

    invoke-static {p1}, Lnet/vrallev/android/task/TaskExecutor;->access$200(Lnet/vrallev/android/task/TaskExecutor;)Landroid/app/Application;

    move-result-object p1

    invoke-virtual {p1, p0}, Landroid/app/Application;->unregisterActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    :cond_1
    return-void
.end method

.method public onActivityStopped(Landroid/app/Activity;)V
    .locals 0

    .line 300
    invoke-virtual {p1}, Landroid/app/Activity;->isFinishing()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 301
    iget-object p1, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->this$0:Lnet/vrallev/android/task/TaskExecutor;

    invoke-static {p1}, Lnet/vrallev/android/task/TaskExecutor;->access$200(Lnet/vrallev/android/task/TaskExecutor;)Landroid/app/Application;

    move-result-object p1

    invoke-virtual {p1, p0}, Landroid/app/Application;->unregisterActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    :cond_0
    return-void
.end method

.method public run()V
    .locals 3

    .line 191
    iget-object v0, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->mTask:Lnet/vrallev/android/task/Task;

    invoke-virtual {v0}, Lnet/vrallev/android/task/Task;->executeInner()Ljava/lang/Object;

    move-result-object v0

    .line 193
    iget-object v1, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->mTask:Lnet/vrallev/android/task/Task;

    instance-of v2, v1, Lnet/vrallev/android/task/TaskNoCallback;

    if-eqz v2, :cond_0

    .line 194
    iget-object v0, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->this$0:Lnet/vrallev/android/task/TaskExecutor;

    invoke-static {v0, v1}, Lnet/vrallev/android/task/TaskExecutor;->access$100(Lnet/vrallev/android/task/TaskExecutor;Lnet/vrallev/android/task/Task;)V

    .line 195
    iget-object v0, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->this$0:Lnet/vrallev/android/task/TaskExecutor;

    invoke-static {v0}, Lnet/vrallev/android/task/TaskExecutor;->access$200(Lnet/vrallev/android/task/TaskExecutor;)Landroid/app/Application;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/app/Application;->unregisterActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    return-void

    .line 199
    :cond_0
    iget-object v1, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->mWeakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnet/vrallev/android/task/TaskCacheFragmentInterface;

    if-eqz v1, :cond_1

    .line 201
    invoke-direct {p0, v0, v1}, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->postResult(Ljava/lang/Object;Lnet/vrallev/android/task/TaskCacheFragmentInterface;)V

    :cond_1
    return-void
.end method

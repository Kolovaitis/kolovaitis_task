.class public abstract Lnet/vrallev/android/task/Task;
.super Ljava/lang/Object;
.source "Task.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<RESU",
        "LT:Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private mAnnotationId:Ljava/lang/String;

.field private mCacheFragment:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lnet/vrallev/android/task/TaskCacheFragmentInterface;",
            ">;"
        }
    .end annotation
.end field

.field private volatile mCancelled:Z

.field private final mCountDownLatch:Ljava/util/concurrent/CountDownLatch;

.field private volatile mFinished:Z

.field private mFragmentId:Ljava/lang/String;

.field private mKey:I

.field private mResult:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TRESU",
            "LT;"
        }
    .end annotation
.end field

.field private mTaskExecutor:Lnet/vrallev/android/task/TaskExecutor;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    .line 26
    iput v0, p0, Lnet/vrallev/android/task/Task;->mKey:I

    .line 35
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lnet/vrallev/android/task/Task;->mCountDownLatch:Ljava/util/concurrent/CountDownLatch;

    return-void
.end method

.method private findFragment(Landroid/support/v4/app/FragmentManager;)Landroid/support/v4/app/Fragment;
    .locals 4

    .line 127
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentManager;->getFragments()Ljava/util/List;

    move-result-object p1

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    .line 132
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/Fragment;

    if-nez v1, :cond_2

    goto :goto_0

    .line 136
    :cond_2
    invoke-static {v1}, Lnet/vrallev/android/task/FragmentIdHelper;->getFragmentId(Landroid/support/v4/app/Fragment;)Ljava/lang/String;

    move-result-object v2

    .line 137
    iget-object v3, p0, Lnet/vrallev/android/task/Task;->mFragmentId:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    return-object v1

    .line 141
    :cond_3
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 142
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-direct {p0, v1}, Lnet/vrallev/android/task/Task;->findFragment(Landroid/support/v4/app/FragmentManager;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_1

    return-object v1

    :cond_4
    return-object v0
.end method


# virtual methods
.method public final cancel()V
    .locals 1

    const/4 v0, 0x1

    .line 81
    iput-boolean v0, p0, Lnet/vrallev/android/task/Task;->mCancelled:Z

    return-void
.end method

.method protected abstract execute()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TRESU",
            "LT;"
        }
    .end annotation
.end method

.method final executeInner()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TRESU",
            "LT;"
        }
    .end annotation

    .line 67
    invoke-virtual {p0}, Lnet/vrallev/android/task/Task;->execute()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lnet/vrallev/android/task/Task;->mResult:Ljava/lang/Object;

    .line 68
    iget-object v0, p0, Lnet/vrallev/android/task/Task;->mCountDownLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 69
    iget-object v0, p0, Lnet/vrallev/android/task/Task;->mResult:Ljava/lang/Object;

    return-object v0
.end method

.method protected final findFragment(I)Landroid/app/Fragment;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 184
    invoke-virtual {p0}, Lnet/vrallev/android/task/Task;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 186
    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method protected final findFragment(Ljava/lang/String;)Landroid/app/Fragment;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 174
    invoke-virtual {p0}, Lnet/vrallev/android/task/Task;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 176
    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method protected final findFragmentSupport(I)Landroid/support/v4/app/Fragment;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 164
    invoke-virtual {p0}, Lnet/vrallev/android/task/Task;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 165
    instance-of v1, v0, Landroid/support/v4/app/FragmentActivity;

    if-eqz v1, :cond_0

    .line 166
    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method protected final findFragmentSupport(Ljava/lang/String;)Landroid/support/v4/app/Fragment;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 154
    invoke-virtual {p0}, Lnet/vrallev/android/task/Task;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 155
    instance-of v1, v0, Landroid/support/v4/app/FragmentActivity;

    if-eqz v1, :cond_0

    .line 156
    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method protected final getActivity()Landroid/app/Activity;
    .locals 1

    .line 106
    iget-object v0, p0, Lnet/vrallev/android/task/Task;->mCacheFragment:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/vrallev/android/task/TaskCacheFragmentInterface;

    if-eqz v0, :cond_0

    .line 108
    invoke-interface {v0}, Lnet/vrallev/android/task/TaskCacheFragmentInterface;->getParentActivity()Landroid/app/Activity;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method final getAnnotationId()Ljava/lang/String;
    .locals 1

    .line 55
    iget-object v0, p0, Lnet/vrallev/android/task/Task;->mAnnotationId:Ljava/lang/String;

    return-object v0
.end method

.method protected final getFragment()Landroid/support/v4/app/Fragment;
    .locals 3

    .line 115
    iget-object v0, p0, Lnet/vrallev/android/task/Task;->mFragmentId:Ljava/lang/String;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 118
    :cond_0
    invoke-virtual {p0}, Lnet/vrallev/android/task/Task;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 119
    instance-of v2, v0, Landroid/support/v4/app/FragmentActivity;

    if-nez v2, :cond_1

    return-object v1

    .line 123
    :cond_1
    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-direct {p0, v0}, Lnet/vrallev/android/task/Task;->findFragment(Landroid/support/v4/app/FragmentManager;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    return-object v0
.end method

.method final getFragmentId()Ljava/lang/String;
    .locals 1

    .line 63
    iget-object v0, p0, Lnet/vrallev/android/task/Task;->mFragmentId:Ljava/lang/String;

    return-object v0
.end method

.method public final getKey()I
    .locals 1

    .line 77
    iget v0, p0, Lnet/vrallev/android/task/Task;->mKey:I

    return v0
.end method

.method public getResult()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TRESU",
            "LT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .line 89
    iget-object v0, p0, Lnet/vrallev/android/task/Task;->mCountDownLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V

    .line 90
    iget-object v0, p0, Lnet/vrallev/android/task/Task;->mResult:Ljava/lang/Object;

    return-object v0
.end method

.method protected getResultClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "TRESU",
            "LT;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public final isCancelled()Z
    .locals 1

    .line 85
    iget-boolean v0, p0, Lnet/vrallev/android/task/Task;->mCancelled:Z

    if-nez v0, :cond_1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public final isExecuting()Z
    .locals 5

    .line 94
    iget-object v0, p0, Lnet/vrallev/android/task/Task;->mCountDownLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->getCount()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final isFinished()Z
    .locals 1

    .line 98
    iget-boolean v0, p0, Lnet/vrallev/android/task/Task;->mFinished:Z

    return v0
.end method

.method final setAnnotationId(Ljava/lang/String;)V
    .locals 0

    .line 51
    iput-object p1, p0, Lnet/vrallev/android/task/Task;->mAnnotationId:Ljava/lang/String;

    return-void
.end method

.method final setCacheFragment(Lnet/vrallev/android/task/TaskCacheFragmentInterface;)V
    .locals 1

    .line 47
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lnet/vrallev/android/task/Task;->mCacheFragment:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method final setFinished()V
    .locals 1

    const/4 v0, 0x1

    .line 73
    iput-boolean v0, p0, Lnet/vrallev/android/task/Task;->mFinished:Z

    return-void
.end method

.method final setFragmentId(Ljava/lang/String;)V
    .locals 0

    .line 59
    iput-object p1, p0, Lnet/vrallev/android/task/Task;->mFragmentId:Ljava/lang/String;

    return-void
.end method

.method final setKey(I)V
    .locals 0

    .line 39
    iput p1, p0, Lnet/vrallev/android/task/Task;->mKey:I

    return-void
.end method

.method final setTaskExecutor(Lnet/vrallev/android/task/TaskExecutor;)V
    .locals 0

    .line 43
    iput-object p1, p0, Lnet/vrallev/android/task/Task;->mTaskExecutor:Lnet/vrallev/android/task/TaskExecutor;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .line 194
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%s{mKey=%d, executing=%b, finished=%b, cancelled=%b"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    iget v3, p0, Lnet/vrallev/android/task/Task;->mKey:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {p0}, Lnet/vrallev/android/task/Task;->isExecuting()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x2

    aput-object v3, v2, v4

    invoke-virtual {p0}, Lnet/vrallev/android/task/Task;->isFinished()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x3

    aput-object v3, v2, v4

    invoke-virtual {p0}, Lnet/vrallev/android/task/Task;->isCancelled()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x4

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

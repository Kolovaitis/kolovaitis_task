.class public final Lnet/vrallev/android/task/TaskCacheFragmentSupport;
.super Landroid/support/v4/app/Fragment;
.source "TaskCacheFragmentSupport.java"

# interfaces
.implements Lnet/vrallev/android/task/TaskCacheFragmentInterface;


# static fields
.field private static final TAG:Ljava/lang/String; = "TaskCacheFragmentSupport"


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private final mCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mCanSaveInstanceState:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 58
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    const/4 v0, 0x1

    .line 59
    invoke-virtual {p0, v0}, Lnet/vrallev/android/task/TaskCacheFragmentSupport;->setRetainInstance(Z)V

    .line 61
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lnet/vrallev/android/task/TaskCacheFragmentSupport;->mCache:Ljava/util/Map;

    return-void
.end method

.method static getFrom(Landroid/support/v4/app/FragmentActivity;)Lnet/vrallev/android/task/TaskCacheFragmentSupport;
    .locals 4

    .line 26
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "TaskCacheFragmentSupport"

    .line 28
    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 29
    instance-of v2, v1, Lnet/vrallev/android/task/TaskCacheFragmentSupport;

    if-eqz v2, :cond_0

    .line 30
    check-cast v1, Lnet/vrallev/android/task/TaskCacheFragmentSupport;

    return-object v1

    .line 33
    :cond_0
    invoke-static {p0}, Lnet/vrallev/android/task/TaskCacheFragmentInterface$Helper;->getTempCacheFragment(Landroid/app/Activity;)Lnet/vrallev/android/task/TaskCacheFragmentInterface;

    move-result-object v1

    .line 34
    instance-of v2, v1, Lnet/vrallev/android/task/TaskCacheFragmentSupport;

    if-eqz v2, :cond_1

    .line 35
    check-cast v1, Lnet/vrallev/android/task/TaskCacheFragmentSupport;

    return-object v1

    .line 38
    :cond_1
    new-instance v1, Lnet/vrallev/android/task/TaskCacheFragmentSupport;

    invoke-direct {v1}, Lnet/vrallev/android/task/TaskCacheFragmentSupport;-><init>()V

    .line 39
    iput-object p0, v1, Lnet/vrallev/android/task/TaskCacheFragmentSupport;->mActivity:Landroid/app/Activity;

    .line 40
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    const-string v3, "TaskCacheFragmentSupport"

    invoke-virtual {v2, v1, v3}, Landroid/support/v4/app/FragmentTransaction;->add(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 45
    :try_start_0
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->executePendingTransactions()Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 48
    :catch_0
    invoke-static {p0, v1}, Lnet/vrallev/android/task/TaskCacheFragmentInterface$Helper;->putTempCacheFragment(Landroid/app/Activity;Lnet/vrallev/android/task/TaskCacheFragmentInterface;)V

    :goto_0
    return-object v1
.end method


# virtual methods
.method public canSaveInstanceState()Z
    .locals 1

    .line 116
    iget-boolean v0, p0, Lnet/vrallev/android/task/TaskCacheFragmentSupport;->mCanSaveInstanceState:Z

    return v0
.end method

.method public declared-synchronized get(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    monitor-enter p0

    .line 122
    :try_start_0
    iget-object v0, p0, Lnet/vrallev/android/task/TaskCacheFragmentSupport;->mCache:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public getParentActivity()Landroid/app/Activity;
    .locals 1

    .line 150
    iget-object v0, p0, Lnet/vrallev/android/task/TaskCacheFragmentSupport;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .line 72
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    const/4 p1, 0x1

    .line 73
    iput-boolean p1, p0, Lnet/vrallev/android/task/TaskCacheFragmentSupport;->mCanSaveInstanceState:Z

    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .line 66
    iput-object p1, p0, Lnet/vrallev/android/task/TaskCacheFragmentSupport;->mActivity:Landroid/app/Activity;

    .line 67
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    return-void
.end method

.method public onDetach()V
    .locals 1

    .line 102
    iget-object v0, p0, Lnet/vrallev/android/task/TaskCacheFragmentSupport;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 103
    iput-object v0, p0, Lnet/vrallev/android/task/TaskCacheFragmentSupport;->mActivity:Landroid/app/Activity;

    .line 105
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    return-void
.end method

.method public onResume()V
    .locals 1

    .line 90
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    const/4 v0, 0x1

    .line 91
    iput-boolean v0, p0, Lnet/vrallev/android/task/TaskCacheFragmentSupport;->mCanSaveInstanceState:Z

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    const/4 v0, 0x0

    .line 110
    iput-boolean v0, p0, Lnet/vrallev/android/task/TaskCacheFragmentSupport;->mCanSaveInstanceState:Z

    .line 111
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method public onStart()V
    .locals 2

    .line 78
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    const/4 v0, 0x1

    .line 80
    iput-boolean v0, p0, Lnet/vrallev/android/task/TaskCacheFragmentSupport;->mCanSaveInstanceState:Z

    const-string v0, "PENDING_RESULT_KEY"

    .line 82
    invoke-virtual {p0, v0}, Lnet/vrallev/android/task/TaskCacheFragmentSupport;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_0

    .line 83
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 84
    invoke-static {v0, p0}, Lnet/vrallev/android/task/TaskCacheFragmentInterface$Helper;->postPendingResults(Ljava/util/List;Lnet/vrallev/android/task/TaskCacheFragmentInterface;)V

    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 1

    const/4 v0, 0x0

    .line 96
    iput-boolean v0, p0, Lnet/vrallev/android/task/TaskCacheFragmentSupport;->mCanSaveInstanceState:Z

    .line 97
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStop()V

    return-void
.end method

.method public declared-synchronized put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ")TT;"
        }
    .end annotation

    monitor-enter p0

    .line 128
    :try_start_0
    iget-object v0, p0, Lnet/vrallev/android/task/TaskCacheFragmentSupport;->mCache:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized putPendingResult(Lnet/vrallev/android/task/TaskPendingResult;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    const-string v0, "PENDING_RESULT_KEY"

    .line 139
    invoke-virtual {p0, v0}, Lnet/vrallev/android/task/TaskCacheFragmentSupport;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-nez v0, :cond_0

    .line 141
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    const-string v1, "PENDING_RESULT_KEY"

    .line 142
    invoke-virtual {p0, v1, v0}, Lnet/vrallev/android/task/TaskCacheFragmentSupport;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    :cond_0
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 146
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized remove(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    monitor-enter p0

    .line 134
    :try_start_0
    iget-object v0, p0, Lnet/vrallev/android/task/TaskCacheFragmentSupport;->mCache:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.class Lcom/microsoft/services/msa/AuthorizationRequest;
.super Ljava/lang/Object;
.source "AuthorizationRequest.java"

# interfaces
.implements Lcom/microsoft/services/msa/ObservableOAuthRequest;
.implements Lcom/microsoft/services/msa/OAuthRequestObserver;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/services/msa/AuthorizationRequest$UriComparator;,
        Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog;
    }
.end annotation


# static fields
.field private static final AMPERSAND:Ljava/lang/String; = "&"

.field private static final EQUALS:Ljava/lang/String; = "="


# instance fields
.field private final activity:Landroid/app/Activity;

.field private final client:Lorg/apache/http/client/HttpClient;

.field private final clientId:Ljava/lang/String;

.field private final loginHint:Ljava/lang/String;

.field private final mOAuthConfig:Lcom/microsoft/services/msa/OAuthConfig;

.field private final observable:Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;

.field private final scope:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lorg/apache/http/client/HttpClient;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/services/msa/OAuthConfig;)V
    .locals 1

    .line 305
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_3

    if-eqz p2, :cond_2

    .line 308
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 309
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 311
    iput-object p1, p0, Lcom/microsoft/services/msa/AuthorizationRequest;->activity:Landroid/app/Activity;

    .line 312
    iput-object p2, p0, Lcom/microsoft/services/msa/AuthorizationRequest;->client:Lorg/apache/http/client/HttpClient;

    .line 313
    iput-object p3, p0, Lcom/microsoft/services/msa/AuthorizationRequest;->clientId:Ljava/lang/String;

    .line 314
    iput-object p6, p0, Lcom/microsoft/services/msa/AuthorizationRequest;->mOAuthConfig:Lcom/microsoft/services/msa/OAuthConfig;

    .line 315
    new-instance p1, Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;

    invoke-direct {p1}, Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;-><init>()V

    iput-object p1, p0, Lcom/microsoft/services/msa/AuthorizationRequest;->observable:Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;

    .line 316
    iput-object p4, p0, Lcom/microsoft/services/msa/AuthorizationRequest;->scope:Ljava/lang/String;

    .line 317
    iput-object p5, p0, Lcom/microsoft/services/msa/AuthorizationRequest;->loginHint:Ljava/lang/String;

    return-void

    .line 309
    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1

    .line 308
    :cond_1
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1

    .line 307
    :cond_2
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1

    .line 306
    :cond_3
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1
.end method

.method static synthetic access$000(Lcom/microsoft/services/msa/AuthorizationRequest;)Lcom/microsoft/services/msa/OAuthConfig;
    .locals 0

    .line 62
    iget-object p0, p0, Lcom/microsoft/services/msa/AuthorizationRequest;->mOAuthConfig:Lcom/microsoft/services/msa/OAuthConfig;

    return-object p0
.end method

.method static synthetic access$100(Lcom/microsoft/services/msa/AuthorizationRequest;Landroid/net/Uri;)V
    .locals 0

    .line 62
    invoke-direct {p0, p1}, Lcom/microsoft/services/msa/AuthorizationRequest;->onEndUri(Landroid/net/Uri;)V

    return-void
.end method

.method static synthetic access$200(Lcom/microsoft/services/msa/AuthorizationRequest;)Landroid/app/Activity;
    .locals 0

    .line 62
    iget-object p0, p0, Lcom/microsoft/services/msa/AuthorizationRequest;->activity:Landroid/app/Activity;

    return-object p0
.end method

.method static synthetic access$300(Lcom/microsoft/services/msa/AuthorizationRequest;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 62
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/services/msa/AuthorizationRequest;->onError(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private getDisplayParameter()Ljava/lang/String;
    .locals 2

    .line 373
    iget-object v0, p0, Lcom/microsoft/services/msa/AuthorizationRequest;->activity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/microsoft/services/msa/ScreenSize;->determineScreenSize(Landroid/app/Activity;)Lcom/microsoft/services/msa/ScreenSize;

    move-result-object v0

    .line 374
    invoke-virtual {v0}, Lcom/microsoft/services/msa/ScreenSize;->getDeviceType()Lcom/microsoft/services/msa/DeviceType;

    move-result-object v0

    .line 376
    invoke-virtual {v0}, Lcom/microsoft/services/msa/DeviceType;->getDisplayParameter()Lcom/microsoft/services/msa/OAuth$DisplayType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/services/msa/OAuth$DisplayType;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getFragmentParametersMap(Landroid/net/Uri;)Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 278
    invoke-virtual {p0}, Landroid/net/Uri;->getFragment()Ljava/lang/String;

    move-result-object p0

    const-string v0, "&"

    .line 279
    invoke-static {p0, v0}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p0

    .line 280
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 282
    array-length v1, p0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_0

    aget-object v4, p0, v3

    const-string v5, "="

    .line 283
    invoke-virtual {v4, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    .line 284
    invoke-virtual {v4, v2, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    add-int/lit8 v5, v5, 0x1

    .line 285
    invoke-virtual {v4, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 286
    invoke-interface {v0, v6, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private onAccessTokenResponse(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 395
    :try_start_0
    invoke-static {p1}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->createFromFragment(Ljava/util/Map;)Lcom/microsoft/services/msa/OAuthSuccessfulResponse;

    move-result-object p1
    :try_end_0
    .catch Lcom/microsoft/services/msa/LiveAuthException; {:try_start_0 .. :try_end_0} :catch_0

    .line 401
    invoke-virtual {p0, p1}, Lcom/microsoft/services/msa/AuthorizationRequest;->onResponse(Lcom/microsoft/services/msa/OAuthResponse;)V

    return-void

    :catch_0
    move-exception p1

    .line 397
    invoke-virtual {p0, p1}, Lcom/microsoft/services/msa/AuthorizationRequest;->onException(Lcom/microsoft/services/msa/LiveAuthException;)V

    return-void

    .line 391
    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1
.end method

.method private onAuthorizationResponse(Ljava/lang/String;)V
    .locals 4

    .line 415
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 420
    new-instance v0, Lcom/microsoft/services/msa/AccessTokenRequest;

    iget-object v1, p0, Lcom/microsoft/services/msa/AuthorizationRequest;->client:Lorg/apache/http/client/HttpClient;

    iget-object v2, p0, Lcom/microsoft/services/msa/AuthorizationRequest;->clientId:Ljava/lang/String;

    iget-object v3, p0, Lcom/microsoft/services/msa/AuthorizationRequest;->mOAuthConfig:Lcom/microsoft/services/msa/OAuthConfig;

    invoke-direct {v0, v1, v2, p1, v3}, Lcom/microsoft/services/msa/AccessTokenRequest;-><init>(Lorg/apache/http/client/HttpClient;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/services/msa/OAuthConfig;)V

    .line 425
    new-instance p1, Lcom/microsoft/services/msa/TokenRequestAsync;

    invoke-direct {p1, v0}, Lcom/microsoft/services/msa/TokenRequestAsync;-><init>(Lcom/microsoft/services/msa/TokenRequest;)V

    .line 428
    invoke-virtual {p1, p0}, Lcom/microsoft/services/msa/TokenRequestAsync;->addObserver(Lcom/microsoft/services/msa/OAuthRequestObserver;)V

    .line 429
    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {p1, v0, v1}, Lcom/microsoft/services/msa/TokenRequestAsync;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void

    .line 415
    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1
.end method

.method private onEndUri(Landroid/net/Uri;)V
    .locals 6

    .line 448
    invoke-virtual {p1}, Landroid/net/Uri;->getFragment()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 449
    :goto_0
    invoke-virtual {p1}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    :goto_1
    if-nez v0, :cond_2

    if-nez v3, :cond_2

    const/4 v4, 0x1

    goto :goto_2

    :cond_2
    const/4 v4, 0x0

    .line 451
    :goto_2
    invoke-virtual {p1}, Landroid/net/Uri;->isHierarchical()Z

    move-result v5

    if-eqz v4, :cond_3

    .line 455
    invoke-direct {p0}, Lcom/microsoft/services/msa/AuthorizationRequest;->onInvalidUri()V

    return-void

    :cond_3
    if-eqz v0, :cond_6

    .line 460
    invoke-static {p1}, Lcom/microsoft/services/msa/AuthorizationRequest;->getFragmentParametersMap(Landroid/net/Uri;)Ljava/util/Map;

    move-result-object v0

    const-string v4, "access_token"

    .line 463
    invoke-interface {v0, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    const-string v4, "token_type"

    invoke-interface {v0, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v4, 0x1

    goto :goto_3

    :cond_4
    const/4 v4, 0x0

    :goto_3
    if-eqz v4, :cond_5

    .line 467
    invoke-direct {p0, v0}, Lcom/microsoft/services/msa/AuthorizationRequest;->onAccessTokenResponse(Ljava/util/Map;)V

    return-void

    :cond_5
    const-string v4, "error"

    .line 471
    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    if-eqz v4, :cond_6

    const-string p1, "error_description"

    .line 473
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    const-string v1, "error_uri"

    .line 474
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 475
    invoke-direct {p0, v4, p1, v0}, Lcom/microsoft/services/msa/AuthorizationRequest;->onError(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_6
    if-eqz v3, :cond_8

    if-eqz v5, :cond_8

    const-string v0, "code"

    .line 481
    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 483
    invoke-direct {p0, v0}, Lcom/microsoft/services/msa/AuthorizationRequest;->onAuthorizationResponse(Ljava/lang/String;)V

    return-void

    :cond_7
    const-string v0, "error"

    .line 487
    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_8

    const-string v1, "error_description"

    .line 489
    invoke-virtual {p1, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "error_uri"

    .line 490
    invoke-virtual {p1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 491
    invoke-direct {p0, v0, v1, p1}, Lcom/microsoft/services/msa/AuthorizationRequest;->onError(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_8
    if-eqz v3, :cond_a

    if-nez v5, :cond_a

    .line 497
    invoke-virtual {p1}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object p1

    const-string v0, "&|="

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    .line 498
    :goto_4
    array-length v0, p1

    if-ge v2, v0, :cond_a

    .line 499
    aget-object v0, p1, v2

    const-string v3, "code"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    add-int/2addr v2, v1

    .line 500
    aget-object p1, p1, v2

    invoke-direct {p0, p1}, Lcom/microsoft/services/msa/AuthorizationRequest;->onAuthorizationResponse(Ljava/lang/String;)V

    return-void

    :cond_9
    const/4 v2, 0x2

    goto :goto_4

    .line 509
    :cond_a
    invoke-direct {p0}, Lcom/microsoft/services/msa/AuthorizationRequest;->onInvalidUri()V

    return-void
.end method

.method private onError(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 523
    new-instance v0, Lcom/microsoft/services/msa/LiveAuthException;

    invoke-direct {v0, p1, p2, p3}, Lcom/microsoft/services/msa/LiveAuthException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 526
    invoke-virtual {p0, v0}, Lcom/microsoft/services/msa/AuthorizationRequest;->onException(Lcom/microsoft/services/msa/LiveAuthException;)V

    return-void
.end method

.method private onInvalidUri()V
    .locals 2

    .line 537
    new-instance v0, Lcom/microsoft/services/msa/LiveAuthException;

    const-string v1, "An error occured while communicating with the server during the operation. Please try again later."

    invoke-direct {v0, v1}, Lcom/microsoft/services/msa/LiveAuthException;-><init>(Ljava/lang/String;)V

    .line 538
    invoke-virtual {p0, v0}, Lcom/microsoft/services/msa/AuthorizationRequest;->onException(Lcom/microsoft/services/msa/LiveAuthException;)V

    return-void
.end method


# virtual methods
.method public addObserver(Lcom/microsoft/services/msa/OAuthRequestObserver;)V
    .locals 1

    .line 322
    iget-object v0, p0, Lcom/microsoft/services/msa/AuthorizationRequest;->observable:Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;

    invoke-virtual {v0, p1}, Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;->addObserver(Lcom/microsoft/services/msa/OAuthRequestObserver;)V

    return-void
.end method

.method public execute()V
    .locals 5

    .line 331
    invoke-direct {p0}, Lcom/microsoft/services/msa/AuthorizationRequest;->getDisplayParameter()Ljava/lang/String;

    move-result-object v0

    .line 332
    sget-object v1, Lcom/microsoft/services/msa/OAuth$ResponseType;->CODE:Lcom/microsoft/services/msa/OAuth$ResponseType;

    invoke-virtual {v1}, Lcom/microsoft/services/msa/OAuth$ResponseType;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 333
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    .line 334
    iget-object v2, p0, Lcom/microsoft/services/msa/AuthorizationRequest;->mOAuthConfig:Lcom/microsoft/services/msa/OAuthConfig;

    invoke-interface {v2}, Lcom/microsoft/services/msa/OAuthConfig;->getAuthorizeUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "client_id"

    iget-object v4, p0, Lcom/microsoft/services/msa/AuthorizationRequest;->clientId:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "scope"

    iget-object v4, p0, Lcom/microsoft/services/msa/AuthorizationRequest;->scope:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "display"

    invoke-virtual {v2, v3, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "response_type"

    invoke-virtual {v0, v2, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "redirect_uri"

    iget-object v2, p0, Lcom/microsoft/services/msa/AuthorizationRequest;->mOAuthConfig:Lcom/microsoft/services/msa/OAuthConfig;

    invoke-interface {v2}, Lcom/microsoft/services/msa/OAuthConfig;->getDesktopUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 342
    iget-object v1, p0, Lcom/microsoft/services/msa/AuthorizationRequest;->loginHint:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v2, "login_hint"

    .line 343
    invoke-virtual {v0, v2, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string v1, "username"

    .line 344
    iget-object v2, p0, Lcom/microsoft/services/msa/AuthorizationRequest;->loginHint:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 347
    :cond_0
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 349
    new-instance v1, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog;

    invoke-direct {v1, p0, v0}, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog;-><init>(Lcom/microsoft/services/msa/AuthorizationRequest;Landroid/net/Uri;)V

    .line 350
    invoke-virtual {v1}, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog;->show()V

    return-void
.end method

.method public onException(Lcom/microsoft/services/msa/LiveAuthException;)V
    .locals 1

    .line 355
    iget-object v0, p0, Lcom/microsoft/services/msa/AuthorizationRequest;->observable:Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;

    invoke-virtual {v0, p1}, Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;->notifyObservers(Lcom/microsoft/services/msa/LiveAuthException;)V

    return-void
.end method

.method public onResponse(Lcom/microsoft/services/msa/OAuthResponse;)V
    .locals 1

    .line 360
    iget-object v0, p0, Lcom/microsoft/services/msa/AuthorizationRequest;->observable:Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;

    invoke-virtual {v0, p1}, Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;->notifyObservers(Lcom/microsoft/services/msa/OAuthResponse;)V

    return-void
.end method

.method public removeObserver(Lcom/microsoft/services/msa/OAuthRequestObserver;)Z
    .locals 1

    .line 365
    iget-object v0, p0, Lcom/microsoft/services/msa/AuthorizationRequest;->observable:Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;

    invoke-virtual {v0, p1}, Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;->removeObserver(Lcom/microsoft/services/msa/OAuthRequestObserver;)Z

    move-result p1

    return p1
.end method

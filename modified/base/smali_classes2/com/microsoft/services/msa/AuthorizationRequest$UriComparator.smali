.class final enum Lcom/microsoft/services/msa/AuthorizationRequest$UriComparator;
.super Ljava/lang/Enum;
.source "AuthorizationRequest.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/services/msa/AuthorizationRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "UriComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/microsoft/services/msa/AuthorizationRequest$UriComparator;",
        ">;",
        "Ljava/util/Comparator<",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/services/msa/AuthorizationRequest$UriComparator;

.field public static final enum INSTANCE:Lcom/microsoft/services/msa/AuthorizationRequest$UriComparator;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 245
    new-instance v0, Lcom/microsoft/services/msa/AuthorizationRequest$UriComparator;

    const-string v1, "INSTANCE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/microsoft/services/msa/AuthorizationRequest$UriComparator;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/services/msa/AuthorizationRequest$UriComparator;->INSTANCE:Lcom/microsoft/services/msa/AuthorizationRequest$UriComparator;

    const/4 v0, 0x1

    .line 244
    new-array v0, v0, [Lcom/microsoft/services/msa/AuthorizationRequest$UriComparator;

    sget-object v1, Lcom/microsoft/services/msa/AuthorizationRequest$UriComparator;->INSTANCE:Lcom/microsoft/services/msa/AuthorizationRequest$UriComparator;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/services/msa/AuthorizationRequest$UriComparator;->$VALUES:[Lcom/microsoft/services/msa/AuthorizationRequest$UriComparator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 244
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/services/msa/AuthorizationRequest$UriComparator;
    .locals 1

    .line 244
    const-class v0, Lcom/microsoft/services/msa/AuthorizationRequest$UriComparator;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/microsoft/services/msa/AuthorizationRequest$UriComparator;

    return-object p0
.end method

.method public static values()[Lcom/microsoft/services/msa/AuthorizationRequest$UriComparator;
    .locals 1

    .line 244
    sget-object v0, Lcom/microsoft/services/msa/AuthorizationRequest$UriComparator;->$VALUES:[Lcom/microsoft/services/msa/AuthorizationRequest$UriComparator;

    invoke-virtual {v0}, [Lcom/microsoft/services/msa/AuthorizationRequest$UriComparator;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/services/msa/AuthorizationRequest$UriComparator;

    return-object v0
.end method


# virtual methods
.method public compare(Landroid/net/Uri;Landroid/net/Uri;)I
    .locals 5

    const/4 v0, 0x3

    .line 249
    new-array v1, v0, [Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x1

    aput-object v2, v1, v4

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object p1

    const/4 v2, 0x2

    aput-object p1, v1, v2

    .line 250
    new-array p1, v0, [Ljava/lang/String;

    invoke-virtual {p2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    aput-object v0, p1, v3

    invoke-virtual {p2}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    aput-object v0, p1, v4

    invoke-virtual {p2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object p2

    aput-object p2, p1, v2

    .line 252
    array-length p2, v1

    array-length v0, p1

    if-ne p2, v0, :cond_3

    const/4 p2, 0x0

    .line 253
    :goto_0
    array-length v0, v1

    if-ge p2, v0, :cond_2

    .line 254
    aget-object v0, v1, p2

    if-nez v0, :cond_0

    aget-object v0, p1, p2

    if-nez v0, :cond_0

    return v3

    .line 258
    :cond_0
    aget-object v0, v1, p2

    aget-object v2, p1, p2

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_1

    return v0

    :cond_1
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_2
    return v3

    .line 252
    :cond_3
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    .line 244
    check-cast p1, Landroid/net/Uri;

    check-cast p2, Landroid/net/Uri;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/services/msa/AuthorizationRequest$UriComparator;->compare(Landroid/net/Uri;Landroid/net/Uri;)I

    move-result p1

    return p1
.end method

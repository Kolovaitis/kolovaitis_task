.class Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;
.super Ljava/lang/Object;
.source "DefaultObservableOAuthRequest.java"

# interfaces
.implements Lcom/microsoft/services/msa/ObservableOAuthRequest;


# instance fields
.field private final observers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/microsoft/services/msa/OAuthRequestObserver;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;->observers:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public addObserver(Lcom/microsoft/services/msa/OAuthRequestObserver;)V
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;->observers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public notifyObservers(Lcom/microsoft/services/msa/LiveAuthException;)V
    .locals 2

    .line 51
    iget-object v0, p0, Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;->observers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/services/msa/OAuthRequestObserver;

    .line 52
    invoke-interface {v1, p1}, Lcom/microsoft/services/msa/OAuthRequestObserver;->onException(Lcom/microsoft/services/msa/LiveAuthException;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public notifyObservers(Lcom/microsoft/services/msa/OAuthResponse;)V
    .locals 2

    .line 62
    iget-object v0, p0, Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;->observers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/services/msa/OAuthRequestObserver;

    .line 63
    invoke-interface {v1, p1}, Lcom/microsoft/services/msa/OAuthRequestObserver;->onResponse(Lcom/microsoft/services/msa/OAuthResponse;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public removeObserver(Lcom/microsoft/services/msa/OAuthRequestObserver;)Z
    .locals 1

    .line 69
    iget-object v0, p0, Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;->observers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.class public Lcom/microsoft/services/msa/MicrosoftOAuthConfig;
.super Ljava/lang/Object;
.source "MicrosoftOAuthConfig.java"

# interfaces
.implements Lcom/microsoft/services/msa/OAuthConfig;


# static fields
.field public static final HTTPS_LOGIN_LIVE_COM:Ljava/lang/String; = "https://login.live.com/"

.field private static sInstance:Lcom/microsoft/services/msa/MicrosoftOAuthConfig;


# instance fields
.field private mOAuthAuthorizeUri:Landroid/net/Uri;

.field private mOAuthDesktopUri:Landroid/net/Uri;

.field private mOAuthLogoutUri:Landroid/net/Uri;

.field private mOAuthTokenUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "https://login.live.com/oauth20_authorize.srf"

    .line 44
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/services/msa/MicrosoftOAuthConfig;->mOAuthAuthorizeUri:Landroid/net/Uri;

    const-string v0, "https://login.live.com/oauth20_desktop.srf"

    .line 45
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/services/msa/MicrosoftOAuthConfig;->mOAuthDesktopUri:Landroid/net/Uri;

    const-string v0, "https://login.live.com/oauth20_logout.srf"

    .line 46
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/services/msa/MicrosoftOAuthConfig;->mOAuthLogoutUri:Landroid/net/Uri;

    const-string v0, "https://login.live.com/oauth20_token.srf"

    .line 47
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/services/msa/MicrosoftOAuthConfig;->mOAuthTokenUri:Landroid/net/Uri;

    return-void
.end method

.method public static getInstance()Lcom/microsoft/services/msa/MicrosoftOAuthConfig;
    .locals 1

    .line 55
    sget-object v0, Lcom/microsoft/services/msa/MicrosoftOAuthConfig;->sInstance:Lcom/microsoft/services/msa/MicrosoftOAuthConfig;

    if-nez v0, :cond_0

    .line 56
    new-instance v0, Lcom/microsoft/services/msa/MicrosoftOAuthConfig;

    invoke-direct {v0}, Lcom/microsoft/services/msa/MicrosoftOAuthConfig;-><init>()V

    sput-object v0, Lcom/microsoft/services/msa/MicrosoftOAuthConfig;->sInstance:Lcom/microsoft/services/msa/MicrosoftOAuthConfig;

    .line 59
    :cond_0
    sget-object v0, Lcom/microsoft/services/msa/MicrosoftOAuthConfig;->sInstance:Lcom/microsoft/services/msa/MicrosoftOAuthConfig;

    return-object v0
.end method


# virtual methods
.method public getAuthorizeUri()Landroid/net/Uri;
    .locals 1

    .line 64
    iget-object v0, p0, Lcom/microsoft/services/msa/MicrosoftOAuthConfig;->mOAuthAuthorizeUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getDesktopUri()Landroid/net/Uri;
    .locals 1

    .line 69
    iget-object v0, p0, Lcom/microsoft/services/msa/MicrosoftOAuthConfig;->mOAuthDesktopUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getLogoutUri()Landroid/net/Uri;
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/microsoft/services/msa/MicrosoftOAuthConfig;->mOAuthLogoutUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getTokenUri()Landroid/net/Uri;
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/microsoft/services/msa/MicrosoftOAuthConfig;->mOAuthTokenUri:Landroid/net/Uri;

    return-object v0
.end method

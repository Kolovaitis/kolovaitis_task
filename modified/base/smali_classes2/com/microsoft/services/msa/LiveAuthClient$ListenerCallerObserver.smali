.class Lcom/microsoft/services/msa/LiveAuthClient$ListenerCallerObserver;
.super Lcom/microsoft/services/msa/LiveAuthClient$AuthListenerCaller;
.source "LiveAuthClient.java"

# interfaces
.implements Lcom/microsoft/services/msa/OAuthRequestObserver;
.implements Lcom/microsoft/services/msa/OAuthResponseVisitor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/services/msa/LiveAuthClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ListenerCallerObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/services/msa/LiveAuthClient;


# direct methods
.method public constructor <init>(Lcom/microsoft/services/msa/LiveAuthClient;Lcom/microsoft/services/msa/LiveAuthListener;Ljava/lang/Object;)V
    .locals 0

    .line 115
    iput-object p1, p0, Lcom/microsoft/services/msa/LiveAuthClient$ListenerCallerObserver;->this$0:Lcom/microsoft/services/msa/LiveAuthClient;

    .line 116
    invoke-direct {p0, p2, p3}, Lcom/microsoft/services/msa/LiveAuthClient$AuthListenerCaller;-><init>(Lcom/microsoft/services/msa/LiveAuthListener;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public onException(Lcom/microsoft/services/msa/LiveAuthException;)V
    .locals 3

    .line 121
    new-instance v0, Lcom/microsoft/services/msa/LiveAuthClient$AuthErrorRunnable;

    iget-object v1, p0, Lcom/microsoft/services/msa/LiveAuthClient$ListenerCallerObserver;->listener:Lcom/microsoft/services/msa/LiveAuthListener;

    iget-object v2, p0, Lcom/microsoft/services/msa/LiveAuthClient$ListenerCallerObserver;->userState:Ljava/lang/Object;

    invoke-direct {v0, v1, v2, p1}, Lcom/microsoft/services/msa/LiveAuthClient$AuthErrorRunnable;-><init>(Lcom/microsoft/services/msa/LiveAuthListener;Ljava/lang/Object;Lcom/microsoft/services/msa/LiveAuthException;)V

    invoke-virtual {v0}, Lcom/microsoft/services/msa/LiveAuthClient$AuthErrorRunnable;->run()V

    return-void
.end method

.method public onResponse(Lcom/microsoft/services/msa/OAuthResponse;)V
    .locals 0

    .line 126
    invoke-interface {p1, p0}, Lcom/microsoft/services/msa/OAuthResponse;->accept(Lcom/microsoft/services/msa/OAuthResponseVisitor;)V

    return-void
.end method

.method public visit(Lcom/microsoft/services/msa/OAuthErrorResponse;)V
    .locals 3

    .line 131
    invoke-virtual {p1}, Lcom/microsoft/services/msa/OAuthErrorResponse;->getError()Lcom/microsoft/services/msa/OAuth$ErrorType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/services/msa/OAuth$ErrorType;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 132
    invoke-virtual {p1}, Lcom/microsoft/services/msa/OAuthErrorResponse;->getErrorDescription()Ljava/lang/String;

    move-result-object v1

    .line 133
    invoke-virtual {p1}, Lcom/microsoft/services/msa/OAuthErrorResponse;->getErrorUri()Ljava/lang/String;

    move-result-object p1

    .line 134
    new-instance v2, Lcom/microsoft/services/msa/LiveAuthException;

    invoke-direct {v2, v0, v1, p1}, Lcom/microsoft/services/msa/LiveAuthException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    new-instance p1, Lcom/microsoft/services/msa/LiveAuthClient$AuthErrorRunnable;

    iget-object v0, p0, Lcom/microsoft/services/msa/LiveAuthClient$ListenerCallerObserver;->listener:Lcom/microsoft/services/msa/LiveAuthListener;

    iget-object v1, p0, Lcom/microsoft/services/msa/LiveAuthClient$ListenerCallerObserver;->userState:Ljava/lang/Object;

    invoke-direct {p1, v0, v1, v2}, Lcom/microsoft/services/msa/LiveAuthClient$AuthErrorRunnable;-><init>(Lcom/microsoft/services/msa/LiveAuthListener;Ljava/lang/Object;Lcom/microsoft/services/msa/LiveAuthException;)V

    invoke-virtual {p1}, Lcom/microsoft/services/msa/LiveAuthClient$AuthErrorRunnable;->run()V

    return-void
.end method

.method public visit(Lcom/microsoft/services/msa/OAuthSuccessfulResponse;)V
    .locals 4

    .line 143
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveAuthClient$ListenerCallerObserver;->this$0:Lcom/microsoft/services/msa/LiveAuthClient;

    invoke-static {v0}, Lcom/microsoft/services/msa/LiveAuthClient;->access$000(Lcom/microsoft/services/msa/LiveAuthClient;)Lcom/microsoft/services/msa/LiveConnectSession;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/microsoft/services/msa/LiveConnectSession;->loadFromOAuthResponse(Lcom/microsoft/services/msa/OAuthSuccessfulResponse;)V

    .line 145
    new-instance p1, Lcom/microsoft/services/msa/LiveAuthClient$AuthCompleteRunnable;

    iget-object v0, p0, Lcom/microsoft/services/msa/LiveAuthClient$ListenerCallerObserver;->listener:Lcom/microsoft/services/msa/LiveAuthListener;

    iget-object v1, p0, Lcom/microsoft/services/msa/LiveAuthClient$ListenerCallerObserver;->userState:Ljava/lang/Object;

    sget-object v2, Lcom/microsoft/services/msa/LiveStatus;->CONNECTED:Lcom/microsoft/services/msa/LiveStatus;

    iget-object v3, p0, Lcom/microsoft/services/msa/LiveAuthClient$ListenerCallerObserver;->this$0:Lcom/microsoft/services/msa/LiveAuthClient;

    invoke-static {v3}, Lcom/microsoft/services/msa/LiveAuthClient;->access$000(Lcom/microsoft/services/msa/LiveAuthClient;)Lcom/microsoft/services/msa/LiveConnectSession;

    move-result-object v3

    invoke-direct {p1, v0, v1, v2, v3}, Lcom/microsoft/services/msa/LiveAuthClient$AuthCompleteRunnable;-><init>(Lcom/microsoft/services/msa/LiveAuthListener;Ljava/lang/Object;Lcom/microsoft/services/msa/LiveStatus;Lcom/microsoft/services/msa/LiveConnectSession;)V

    invoke-virtual {p1}, Lcom/microsoft/services/msa/LiveAuthClient$AuthCompleteRunnable;->run()V

    return-void
.end method

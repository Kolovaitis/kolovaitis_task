.class Lcom/microsoft/services/msa/TokenRequestAsync;
.super Landroid/os/AsyncTask;
.source "TokenRequestAsync.java"

# interfaces
.implements Lcom/microsoft/services/msa/ObservableOAuthRequest;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;",
        "Lcom/microsoft/services/msa/ObservableOAuthRequest;"
    }
.end annotation


# instance fields
.field private exception:Lcom/microsoft/services/msa/LiveAuthException;

.field private final observerable:Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;

.field private final request:Lcom/microsoft/services/msa/TokenRequest;

.field private response:Lcom/microsoft/services/msa/OAuthResponse;


# direct methods
.method public constructor <init>(Lcom/microsoft/services/msa/TokenRequest;)V
    .locals 1

    .line 48
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    if-eqz p1, :cond_0

    .line 51
    new-instance v0, Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;

    invoke-direct {v0}, Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;-><init>()V

    iput-object v0, p0, Lcom/microsoft/services/msa/TokenRequestAsync;->observerable:Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;

    .line 52
    iput-object p1, p0, Lcom/microsoft/services/msa/TokenRequestAsync;->request:Lcom/microsoft/services/msa/TokenRequest;

    return-void

    .line 49
    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1
.end method


# virtual methods
.method public addObserver(Lcom/microsoft/services/msa/OAuthRequestObserver;)V
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/microsoft/services/msa/TokenRequestAsync;->observerable:Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;

    invoke-virtual {v0, p1}, Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;->addObserver(Lcom/microsoft/services/msa/OAuthRequestObserver;)V

    return-void
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 31
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/microsoft/services/msa/TokenRequestAsync;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 0

    .line 68
    :try_start_0
    iget-object p1, p0, Lcom/microsoft/services/msa/TokenRequestAsync;->request:Lcom/microsoft/services/msa/TokenRequest;

    invoke-virtual {p1}, Lcom/microsoft/services/msa/TokenRequest;->execute()Lcom/microsoft/services/msa/OAuthResponse;

    move-result-object p1

    iput-object p1, p0, Lcom/microsoft/services/msa/TokenRequestAsync;->response:Lcom/microsoft/services/msa/OAuthResponse;
    :try_end_0
    .catch Lcom/microsoft/services/msa/LiveAuthException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 70
    iput-object p1, p0, Lcom/microsoft/services/msa/TokenRequestAsync;->exception:Lcom/microsoft/services/msa/LiveAuthException;

    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 31
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/microsoft/services/msa/TokenRequestAsync;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 1

    .line 78
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 80
    iget-object p1, p0, Lcom/microsoft/services/msa/TokenRequestAsync;->response:Lcom/microsoft/services/msa/OAuthResponse;

    if-eqz p1, :cond_0

    .line 81
    iget-object v0, p0, Lcom/microsoft/services/msa/TokenRequestAsync;->observerable:Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;

    invoke-virtual {v0, p1}, Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;->notifyObservers(Lcom/microsoft/services/msa/OAuthResponse;)V

    goto :goto_0

    .line 82
    :cond_0
    iget-object p1, p0, Lcom/microsoft/services/msa/TokenRequestAsync;->exception:Lcom/microsoft/services/msa/LiveAuthException;

    if-eqz p1, :cond_1

    .line 83
    iget-object v0, p0, Lcom/microsoft/services/msa/TokenRequestAsync;->observerable:Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;

    invoke-virtual {v0, p1}, Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;->notifyObservers(Lcom/microsoft/services/msa/LiveAuthException;)V

    goto :goto_0

    .line 85
    :cond_1
    new-instance p1, Lcom/microsoft/services/msa/LiveAuthException;

    const-string v0, "An error occured on the client during the operation."

    invoke-direct {p1, v0}, Lcom/microsoft/services/msa/LiveAuthException;-><init>(Ljava/lang/String;)V

    .line 86
    iget-object v0, p0, Lcom/microsoft/services/msa/TokenRequestAsync;->observerable:Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;

    invoke-virtual {v0, p1}, Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;->notifyObservers(Lcom/microsoft/services/msa/LiveAuthException;)V

    :goto_0
    return-void
.end method

.method public removeObserver(Lcom/microsoft/services/msa/OAuthRequestObserver;)Z
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/microsoft/services/msa/TokenRequestAsync;->observerable:Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;

    invoke-virtual {v0, p1}, Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;->removeObserver(Lcom/microsoft/services/msa/OAuthRequestObserver;)Z

    move-result p1

    return p1
.end method

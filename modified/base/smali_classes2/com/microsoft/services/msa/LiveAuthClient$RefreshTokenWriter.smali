.class Lcom/microsoft/services/msa/LiveAuthClient$RefreshTokenWriter;
.super Ljava/lang/Object;
.source "LiveAuthClient.java"

# interfaces
.implements Lcom/microsoft/services/msa/OAuthRequestObserver;
.implements Lcom/microsoft/services/msa/OAuthResponseVisitor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/services/msa/LiveAuthClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RefreshTokenWriter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/services/msa/LiveAuthClient;


# direct methods
.method private constructor <init>(Lcom/microsoft/services/msa/LiveAuthClient;)V
    .locals 0

    .line 150
    iput-object p1, p0, Lcom/microsoft/services/msa/LiveAuthClient$RefreshTokenWriter;->this$0:Lcom/microsoft/services/msa/LiveAuthClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/services/msa/LiveAuthClient;Lcom/microsoft/services/msa/LiveAuthClient$1;)V
    .locals 0

    .line 150
    invoke-direct {p0, p1}, Lcom/microsoft/services/msa/LiveAuthClient$RefreshTokenWriter;-><init>(Lcom/microsoft/services/msa/LiveAuthClient;)V

    return-void
.end method

.method private saveRefreshTokenToPreferences(Ljava/lang/String;)Z
    .locals 3

    .line 176
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 178
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveAuthClient$RefreshTokenWriter;->this$0:Lcom/microsoft/services/msa/LiveAuthClient;

    invoke-static {v0}, Lcom/microsoft/services/msa/LiveAuthClient;->access$200(Lcom/microsoft/services/msa/LiveAuthClient;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.microsoft.live"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 181
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "refresh_token"

    .line 182
    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 184
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result p1

    return p1

    .line 176
    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1
.end method


# virtual methods
.method public onException(Lcom/microsoft/services/msa/LiveAuthException;)V
    .locals 0

    return-void
.end method

.method public onResponse(Lcom/microsoft/services/msa/OAuthResponse;)V
    .locals 0

    .line 157
    invoke-interface {p1, p0}, Lcom/microsoft/services/msa/OAuthResponse;->accept(Lcom/microsoft/services/msa/OAuthResponseVisitor;)V

    return-void
.end method

.method public visit(Lcom/microsoft/services/msa/OAuthErrorResponse;)V
    .locals 1

    .line 162
    invoke-virtual {p1}, Lcom/microsoft/services/msa/OAuthErrorResponse;->getError()Lcom/microsoft/services/msa/OAuth$ErrorType;

    move-result-object p1

    sget-object v0, Lcom/microsoft/services/msa/OAuth$ErrorType;->INVALID_GRANT:Lcom/microsoft/services/msa/OAuth$ErrorType;

    if-ne p1, v0, :cond_0

    .line 163
    iget-object p1, p0, Lcom/microsoft/services/msa/LiveAuthClient$RefreshTokenWriter;->this$0:Lcom/microsoft/services/msa/LiveAuthClient;

    invoke-static {p1}, Lcom/microsoft/services/msa/LiveAuthClient;->access$100(Lcom/microsoft/services/msa/LiveAuthClient;)Z

    :cond_0
    return-void
.end method

.method public visit(Lcom/microsoft/services/msa/OAuthSuccessfulResponse;)V
    .locals 1

    .line 169
    invoke-virtual {p1}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->getRefreshToken()Ljava/lang/String;

    move-result-object p1

    .line 170
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 171
    invoke-direct {p0, p1}, Lcom/microsoft/services/msa/LiveAuthClient$RefreshTokenWriter;->saveRefreshTokenToPreferences(Ljava/lang/String;)Z

    :cond_0
    return-void
.end method

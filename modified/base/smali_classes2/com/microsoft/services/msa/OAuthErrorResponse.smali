.class Lcom/microsoft/services/msa/OAuthErrorResponse;
.super Ljava/lang/Object;
.source "OAuthErrorResponse.java"

# interfaces
.implements Lcom/microsoft/services/msa/OAuthResponse;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/services/msa/OAuthErrorResponse$Builder;
    }
.end annotation


# instance fields
.field private final error:Lcom/microsoft/services/msa/OAuth$ErrorType;

.field private final errorDescription:Ljava/lang/String;

.field private final errorUri:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/microsoft/services/msa/OAuthErrorResponse$Builder;)V
    .locals 1

    .line 150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 151
    invoke-static {p1}, Lcom/microsoft/services/msa/OAuthErrorResponse$Builder;->access$100(Lcom/microsoft/services/msa/OAuthErrorResponse$Builder;)Lcom/microsoft/services/msa/OAuth$ErrorType;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/services/msa/OAuthErrorResponse;->error:Lcom/microsoft/services/msa/OAuth$ErrorType;

    .line 152
    invoke-static {p1}, Lcom/microsoft/services/msa/OAuthErrorResponse$Builder;->access$200(Lcom/microsoft/services/msa/OAuthErrorResponse$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/services/msa/OAuthErrorResponse;->errorDescription:Ljava/lang/String;

    .line 153
    invoke-static {p1}, Lcom/microsoft/services/msa/OAuthErrorResponse$Builder;->access$300(Lcom/microsoft/services/msa/OAuthErrorResponse$Builder;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/microsoft/services/msa/OAuthErrorResponse;->errorUri:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/services/msa/OAuthErrorResponse$Builder;Lcom/microsoft/services/msa/OAuthErrorResponse$1;)V
    .locals 0

    .line 34
    invoke-direct {p0, p1}, Lcom/microsoft/services/msa/OAuthErrorResponse;-><init>(Lcom/microsoft/services/msa/OAuthErrorResponse$Builder;)V

    return-void
.end method

.method public static createFromJson(Lorg/json/JSONObject;)Lcom/microsoft/services/msa/OAuthErrorResponse;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/services/msa/LiveAuthException;
        }
    .end annotation

    :try_start_0
    const-string v0, "error"

    .line 81
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_4

    .line 88
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/services/msa/OAuth$ErrorType;->valueOf(Ljava/lang/String;)Lcom/microsoft/services/msa/OAuth$ErrorType;

    move-result-object v0
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_2

    .line 95
    new-instance v1, Lcom/microsoft/services/msa/OAuthErrorResponse$Builder;

    invoke-direct {v1, v0}, Lcom/microsoft/services/msa/OAuthErrorResponse$Builder;-><init>(Lcom/microsoft/services/msa/OAuth$ErrorType;)V

    const-string v0, "error_description"

    .line 96
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_2
    const-string v0, "error_description"

    .line 99
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    .line 103
    invoke-virtual {v1, v0}, Lcom/microsoft/services/msa/OAuthErrorResponse$Builder;->errorDescription(Ljava/lang/String;)Lcom/microsoft/services/msa/OAuthErrorResponse$Builder;

    goto :goto_0

    :catch_0
    move-exception p0

    .line 101
    new-instance v0, Lcom/microsoft/services/msa/LiveAuthException;

    const-string v1, "An error occured on the client during the operation."

    invoke-direct {v0, v1, p0}, Lcom/microsoft/services/msa/LiveAuthException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    :cond_0
    :goto_0
    const-string v0, "error_uri"

    .line 106
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :try_start_3
    const-string v0, "error_uri"

    .line 109
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1

    .line 113
    invoke-virtual {v1, p0}, Lcom/microsoft/services/msa/OAuthErrorResponse$Builder;->errorUri(Ljava/lang/String;)Lcom/microsoft/services/msa/OAuthErrorResponse$Builder;

    goto :goto_1

    :catch_1
    move-exception p0

    .line 111
    new-instance v0, Lcom/microsoft/services/msa/LiveAuthException;

    const-string v1, "An error occured on the client during the operation."

    invoke-direct {v0, v1, p0}, Lcom/microsoft/services/msa/LiveAuthException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 116
    :cond_1
    :goto_1
    invoke-virtual {v1}, Lcom/microsoft/services/msa/OAuthErrorResponse$Builder;->build()Lcom/microsoft/services/msa/OAuthErrorResponse;

    move-result-object p0

    return-object p0

    :catch_2
    move-exception p0

    .line 92
    new-instance v0, Lcom/microsoft/services/msa/LiveAuthException;

    const-string v1, "An error occured while communicating with the server during the operation. Please try again later."

    invoke-direct {v0, v1, p0}, Lcom/microsoft/services/msa/LiveAuthException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    :catch_3
    move-exception p0

    .line 90
    new-instance v0, Lcom/microsoft/services/msa/LiveAuthException;

    const-string v1, "An error occured while communicating with the server during the operation. Please try again later."

    invoke-direct {v0, v1, p0}, Lcom/microsoft/services/msa/LiveAuthException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    :catch_4
    move-exception p0

    .line 83
    new-instance v0, Lcom/microsoft/services/msa/LiveAuthException;

    const-string v1, "An error occured while communicating with the server during the operation. Please try again later."

    invoke-direct {v0, v1, p0}, Lcom/microsoft/services/msa/LiveAuthException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public static validOAuthErrorResponse(Lorg/json/JSONObject;)Z
    .locals 1

    const-string v0, "error"

    .line 124
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method


# virtual methods
.method public accept(Lcom/microsoft/services/msa/OAuthResponseVisitor;)V
    .locals 0

    .line 158
    invoke-interface {p1, p0}, Lcom/microsoft/services/msa/OAuthResponseVisitor;->visit(Lcom/microsoft/services/msa/OAuthErrorResponse;)V

    return-void
.end method

.method public getError()Lcom/microsoft/services/msa/OAuth$ErrorType;
    .locals 1

    .line 166
    iget-object v0, p0, Lcom/microsoft/services/msa/OAuthErrorResponse;->error:Lcom/microsoft/services/msa/OAuth$ErrorType;

    return-object v0
.end method

.method public getErrorDescription()Ljava/lang/String;
    .locals 1

    .line 174
    iget-object v0, p0, Lcom/microsoft/services/msa/OAuthErrorResponse;->errorDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getErrorUri()Ljava/lang/String;
    .locals 1

    .line 182
    iget-object v0, p0, Lcom/microsoft/services/msa/OAuthErrorResponse;->errorUri:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    const-string v0, "OAuthErrorResponse [error=%s, errorDescription=%s, errorUri=%s]"

    const/4 v1, 0x3

    .line 187
    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/microsoft/services/msa/OAuthErrorResponse;->error:Lcom/microsoft/services/msa/OAuth$ErrorType;

    invoke-virtual {v2}, Lcom/microsoft/services/msa/OAuth$ErrorType;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/microsoft/services/msa/OAuthErrorResponse;->errorDescription:Ljava/lang/String;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/microsoft/services/msa/OAuthErrorResponse;->errorUri:Ljava/lang/String;

    const/4 v3, 0x2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

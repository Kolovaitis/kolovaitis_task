.class abstract Lcom/microsoft/services/msa/TokenRequest;
.super Ljava/lang/Object;
.source "TokenRequest.java"


# static fields
.field private static final CONTENT_TYPE:Ljava/lang/String; = "application/x-www-form-urlencoded;charset=UTF-8"


# instance fields
.field protected final client:Lorg/apache/http/client/HttpClient;

.field protected final clientId:Ljava/lang/String;

.field protected final mOAuthConfig:Lcom/microsoft/services/msa/OAuthConfig;


# direct methods
.method public constructor <init>(Lorg/apache/http/client/HttpClient;Ljava/lang/String;Lcom/microsoft/services/msa/OAuthConfig;)V
    .locals 1

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_2

    if-eqz p2, :cond_1

    .line 69
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 71
    iput-object p1, p0, Lcom/microsoft/services/msa/TokenRequest;->client:Lorg/apache/http/client/HttpClient;

    .line 72
    iput-object p2, p0, Lcom/microsoft/services/msa/TokenRequest;->clientId:Ljava/lang/String;

    .line 73
    iput-object p3, p0, Lcom/microsoft/services/msa/TokenRequest;->mOAuthConfig:Lcom/microsoft/services/msa/OAuthConfig;

    return-void

    .line 69
    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1

    .line 68
    :cond_1
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1

    .line 67
    :cond_2
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1
.end method


# virtual methods
.method protected abstract constructBody(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/apache/http/NameValuePair;",
            ">;)V"
        }
    .end annotation
.end method

.method public execute()Lcom/microsoft/services/msa/OAuthResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/services/msa/LiveAuthException;
        }
    .end annotation

    .line 84
    iget-object v0, p0, Lcom/microsoft/services/msa/TokenRequest;->mOAuthConfig:Lcom/microsoft/services/msa/OAuthConfig;

    invoke-interface {v0}, Lcom/microsoft/services/msa/OAuthConfig;->getTokenUri()Landroid/net/Uri;

    move-result-object v0

    .line 86
    new-instance v1, Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 88
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 89
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "client_id"

    iget-object v4, p0, Lcom/microsoft/services/msa/TokenRequest;->clientId:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 92
    invoke-virtual {p0, v0}, Lcom/microsoft/services/msa/TokenRequest;->constructBody(Ljava/util/List;)V

    .line 95
    :try_start_0
    new-instance v2, Lorg/apache/http/client/entity/UrlEncodedFormEntity;

    const-string v3, "UTF-8"

    invoke-direct {v2, v0, v3}, Lorg/apache/http/client/entity/UrlEncodedFormEntity;-><init>(Ljava/util/List;Ljava/lang/String;)V

    const-string v0, "application/x-www-form-urlencoded;charset=UTF-8"

    .line 96
    invoke-virtual {v2, v0}, Lorg/apache/http/client/entity/UrlEncodedFormEntity;->setContentType(Ljava/lang/String;)V

    .line 97
    invoke-virtual {v1, v2}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_4

    .line 104
    :try_start_1
    iget-object v0, p0, Lcom/microsoft/services/msa/TokenRequest;->client:Lorg/apache/http/client/HttpClient;

    invoke-interface {v0, v1}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0
    :try_end_1
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 111
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 114
    :try_start_2
    invoke-static {v0}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 121
    :try_start_3
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0

    .line 126
    invoke-static {v1}, Lcom/microsoft/services/msa/OAuthErrorResponse;->validOAuthErrorResponse(Lorg/json/JSONObject;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 127
    invoke-static {v1}, Lcom/microsoft/services/msa/OAuthErrorResponse;->createFromJson(Lorg/json/JSONObject;)Lcom/microsoft/services/msa/OAuthErrorResponse;

    move-result-object v0

    return-object v0

    .line 128
    :cond_0
    invoke-static {v1}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->validOAuthSuccessfulResponse(Lorg/json/JSONObject;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 129
    invoke-static {v1}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->createFromJson(Lorg/json/JSONObject;)Lcom/microsoft/services/msa/OAuthSuccessfulResponse;

    move-result-object v0

    return-object v0

    .line 131
    :cond_1
    new-instance v0, Lcom/microsoft/services/msa/LiveAuthException;

    const-string v1, "An error occured while communicating with the server during the operation. Please try again later."

    invoke-direct {v0, v1}, Lcom/microsoft/services/msa/LiveAuthException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_0
    move-exception v0

    .line 123
    new-instance v1, Lcom/microsoft/services/msa/LiveAuthException;

    const-string v2, "An error occured while communicating with the server during the operation. Please try again later."

    invoke-direct {v1, v2, v0}, Lcom/microsoft/services/msa/LiveAuthException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    .line 116
    new-instance v1, Lcom/microsoft/services/msa/LiveAuthException;

    const-string v2, "An error occured while communicating with the server during the operation. Please try again later."

    invoke-direct {v1, v2, v0}, Lcom/microsoft/services/msa/LiveAuthException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_2
    move-exception v0

    .line 108
    new-instance v1, Lcom/microsoft/services/msa/LiveAuthException;

    const-string v2, "An error occured while communicating with the server during the operation. Please try again later."

    invoke-direct {v1, v2, v0}, Lcom/microsoft/services/msa/LiveAuthException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_3
    move-exception v0

    .line 106
    new-instance v1, Lcom/microsoft/services/msa/LiveAuthException;

    const-string v2, "An error occured while communicating with the server during the operation. Please try again later."

    invoke-direct {v1, v2, v0}, Lcom/microsoft/services/msa/LiveAuthException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_4
    move-exception v0

    .line 99
    new-instance v1, Lcom/microsoft/services/msa/LiveAuthException;

    const-string v2, "An error occured on the client during the operation."

    invoke-direct {v1, v2, v0}, Lcom/microsoft/services/msa/LiveAuthException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

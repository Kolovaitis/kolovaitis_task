.class public Lcom/microsoft/services/msa/LiveConnectSession;
.super Ljava/lang/Object;
.source "LiveConnectSession.java"


# instance fields
.field private accessToken:Ljava/lang/String;

.field private authenticationToken:Ljava/lang/String;

.field private final changeSupport:Ljava/beans/PropertyChangeSupport;

.field private final creator:Lcom/microsoft/services/msa/LiveAuthClient;

.field private expiresIn:Ljava/util/Date;

.field private refreshToken:Ljava/lang/String;

.field private scopes:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private tokenType:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/microsoft/services/msa/LiveAuthClient;)V
    .locals 0

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_0

    .line 66
    iput-object p1, p0, Lcom/microsoft/services/msa/LiveConnectSession;->creator:Lcom/microsoft/services/msa/LiveAuthClient;

    .line 67
    new-instance p1, Ljava/beans/PropertyChangeSupport;

    invoke-direct {p1, p0}, Ljava/beans/PropertyChangeSupport;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/microsoft/services/msa/LiveConnectSession;->changeSupport:Ljava/beans/PropertyChangeSupport;

    return-void

    .line 64
    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1
.end method


# virtual methods
.method public addPropertyChangeListener(Ljava/beans/PropertyChangeListener;)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveConnectSession;->changeSupport:Ljava/beans/PropertyChangeSupport;

    invoke-virtual {v0, p1}, Ljava/beans/PropertyChangeSupport;->addPropertyChangeListener(Ljava/beans/PropertyChangeListener;)V

    return-void
.end method

.method public addPropertyChangeListener(Ljava/lang/String;Ljava/beans/PropertyChangeListener;)V
    .locals 1

    if-nez p2, :cond_0

    return-void

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveConnectSession;->changeSupport:Ljava/beans/PropertyChangeSupport;

    invoke-virtual {v0, p1, p2}, Ljava/beans/PropertyChangeSupport;->addPropertyChangeListener(Ljava/lang/String;Ljava/beans/PropertyChangeListener;)V

    return-void
.end method

.method contains(Ljava/lang/Iterable;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    const/4 v0, 0x1

    if-nez p1, :cond_0

    return v0

    .line 212
    :cond_0
    iget-object v1, p0, Lcom/microsoft/services/msa/LiveConnectSession;->scopes:Ljava/util/Set;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 216
    :cond_1
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 217
    iget-object v3, p0, Lcom/microsoft/services/msa/LiveConnectSession;->scopes:Ljava/util/Set;

    invoke-interface {v3, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    return v2

    :cond_3
    return v0
.end method

.method public getAccessToken()Ljava/lang/String;
    .locals 1

    .line 103
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveConnectSession;->accessToken:Ljava/lang/String;

    return-object v0
.end method

.method public getAuthenticationToken()Ljava/lang/String;
    .locals 1

    .line 111
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveConnectSession;->authenticationToken:Ljava/lang/String;

    return-object v0
.end method

.method public getExpiresIn()Ljava/util/Date;
    .locals 3

    .line 119
    new-instance v0, Ljava/util/Date;

    iget-object v1, p0, Lcom/microsoft/services/msa/LiveConnectSession;->expiresIn:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    return-object v0
.end method

.method public getPropertyChangeListeners()[Ljava/beans/PropertyChangeListener;
    .locals 1

    .line 126
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveConnectSession;->changeSupport:Ljava/beans/PropertyChangeSupport;

    invoke-virtual {v0}, Ljava/beans/PropertyChangeSupport;->getPropertyChangeListeners()[Ljava/beans/PropertyChangeListener;

    move-result-object v0

    return-object v0
.end method

.method public getPropertyChangeListeners(Ljava/lang/String;)[Ljava/beans/PropertyChangeListener;
    .locals 1

    .line 134
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveConnectSession;->changeSupport:Ljava/beans/PropertyChangeSupport;

    invoke-virtual {v0, p1}, Ljava/beans/PropertyChangeSupport;->getPropertyChangeListeners(Ljava/lang/String;)[Ljava/beans/PropertyChangeListener;

    move-result-object p1

    return-object p1
.end method

.method public getRefreshToken()Ljava/lang/String;
    .locals 1

    .line 141
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveConnectSession;->refreshToken:Ljava/lang/String;

    return-object v0
.end method

.method public getScopes()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 149
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveConnectSession;->scopes:Ljava/util/Set;

    return-object v0
.end method

.method public getTokenType()Ljava/lang/String;
    .locals 1

    .line 156
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveConnectSession;->tokenType:Ljava/lang/String;

    return-object v0
.end method

.method public isExpired()Z
    .locals 2

    .line 163
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveConnectSession;->expiresIn:Ljava/util/Date;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    return v0

    .line 167
    :cond_0
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 169
    iget-object v1, p0, Lcom/microsoft/services/msa/LiveConnectSession;->expiresIn:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v0

    return v0
.end method

.method loadFromOAuthResponse(Lcom/microsoft/services/msa/OAuthSuccessfulResponse;)V
    .locals 3

    .line 232
    invoke-virtual {p1}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->getAccessToken()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/services/msa/LiveConnectSession;->accessToken:Ljava/lang/String;

    .line 233
    invoke-virtual {p1}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->getTokenType()Lcom/microsoft/services/msa/OAuth$TokenType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/services/msa/OAuth$TokenType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/services/msa/LiveConnectSession;->tokenType:Ljava/lang/String;

    .line 235
    invoke-virtual {p1}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->hasAuthenticationToken()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 236
    invoke-virtual {p1}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->getAuthenticationToken()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/services/msa/LiveConnectSession;->authenticationToken:Ljava/lang/String;

    .line 239
    :cond_0
    invoke-virtual {p1}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->hasExpiresIn()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 240
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const/16 v1, 0xd

    .line 241
    invoke-virtual {p1}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->getExpiresIn()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 242
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/services/msa/LiveConnectSession;->setExpiresIn(Ljava/util/Date;)V

    .line 245
    :cond_1
    invoke-virtual {p1}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->hasRefreshToken()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 246
    invoke-virtual {p1}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->getRefreshToken()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/services/msa/LiveConnectSession;->refreshToken:Ljava/lang/String;

    .line 249
    :cond_2
    invoke-virtual {p1}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->hasScope()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 250
    invoke-virtual {p1}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->getScope()Ljava/lang/String;

    move-result-object p1

    const-string v0, " "

    .line 251
    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/microsoft/services/msa/LiveConnectSession;->setScopes(Ljava/lang/Iterable;)V

    :cond_3
    return-void
.end method

.method refresh()Z
    .locals 2

    .line 261
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveConnectSession;->creator:Lcom/microsoft/services/msa/LiveAuthClient;

    invoke-virtual {p0}, Lcom/microsoft/services/msa/LiveConnectSession;->getScopes()Ljava/lang/Iterable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/services/msa/LiveAuthClient;->tryRefresh(Ljava/lang/Iterable;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public removePropertyChangeListener(Ljava/beans/PropertyChangeListener;)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    .line 181
    :cond_0
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveConnectSession;->changeSupport:Ljava/beans/PropertyChangeSupport;

    invoke-virtual {v0, p1}, Ljava/beans/PropertyChangeSupport;->removePropertyChangeListener(Ljava/beans/PropertyChangeListener;)V

    return-void
.end method

.method public removePropertyChangeListener(Ljava/lang/String;Ljava/beans/PropertyChangeListener;)V
    .locals 1

    if-nez p2, :cond_0

    return-void

    .line 195
    :cond_0
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveConnectSession;->changeSupport:Ljava/beans/PropertyChangeSupport;

    invoke-virtual {v0, p1, p2}, Ljava/beans/PropertyChangeSupport;->removePropertyChangeListener(Ljava/lang/String;Ljava/beans/PropertyChangeListener;)V

    return-void
.end method

.method setAccessToken(Ljava/lang/String;)V
    .locals 3

    .line 265
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveConnectSession;->accessToken:Ljava/lang/String;

    .line 266
    iput-object p1, p0, Lcom/microsoft/services/msa/LiveConnectSession;->accessToken:Ljava/lang/String;

    .line 268
    iget-object p1, p0, Lcom/microsoft/services/msa/LiveConnectSession;->changeSupport:Ljava/beans/PropertyChangeSupport;

    const-string v1, "accessToken"

    iget-object v2, p0, Lcom/microsoft/services/msa/LiveConnectSession;->accessToken:Ljava/lang/String;

    invoke-virtual {p1, v1, v0, v2}, Ljava/beans/PropertyChangeSupport;->firePropertyChange(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method setAuthenticationToken(Ljava/lang/String;)V
    .locals 3

    .line 272
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveConnectSession;->authenticationToken:Ljava/lang/String;

    .line 273
    iput-object p1, p0, Lcom/microsoft/services/msa/LiveConnectSession;->authenticationToken:Ljava/lang/String;

    .line 275
    iget-object p1, p0, Lcom/microsoft/services/msa/LiveConnectSession;->changeSupport:Ljava/beans/PropertyChangeSupport;

    const-string v1, "authenticationToken"

    iget-object v2, p0, Lcom/microsoft/services/msa/LiveConnectSession;->authenticationToken:Ljava/lang/String;

    invoke-virtual {p1, v1, v0, v2}, Ljava/beans/PropertyChangeSupport;->firePropertyChange(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method setExpiresIn(Ljava/util/Date;)V
    .locals 4

    .line 281
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveConnectSession;->expiresIn:Ljava/util/Date;

    .line 282
    new-instance v1, Ljava/util/Date;

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    iput-object v1, p0, Lcom/microsoft/services/msa/LiveConnectSession;->expiresIn:Ljava/util/Date;

    .line 284
    iget-object p1, p0, Lcom/microsoft/services/msa/LiveConnectSession;->changeSupport:Ljava/beans/PropertyChangeSupport;

    const-string v1, "expiresIn"

    iget-object v2, p0, Lcom/microsoft/services/msa/LiveConnectSession;->expiresIn:Ljava/util/Date;

    invoke-virtual {p1, v1, v0, v2}, Ljava/beans/PropertyChangeSupport;->firePropertyChange(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method setRefreshToken(Ljava/lang/String;)V
    .locals 3

    .line 288
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveConnectSession;->refreshToken:Ljava/lang/String;

    .line 289
    iput-object p1, p0, Lcom/microsoft/services/msa/LiveConnectSession;->refreshToken:Ljava/lang/String;

    .line 291
    iget-object p1, p0, Lcom/microsoft/services/msa/LiveConnectSession;->changeSupport:Ljava/beans/PropertyChangeSupport;

    const-string v1, "refreshToken"

    iget-object v2, p0, Lcom/microsoft/services/msa/LiveConnectSession;->refreshToken:Ljava/lang/String;

    invoke-virtual {p1, v1, v0, v2}, Ljava/beans/PropertyChangeSupport;->firePropertyChange(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method setScopes(Ljava/lang/Iterable;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 295
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveConnectSession;->scopes:Ljava/util/Set;

    .line 298
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/microsoft/services/msa/LiveConnectSession;->scopes:Ljava/util/Set;

    if-eqz p1, :cond_0

    .line 300
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 301
    iget-object v2, p0, Lcom/microsoft/services/msa/LiveConnectSession;->scopes:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 305
    :cond_0
    iget-object p1, p0, Lcom/microsoft/services/msa/LiveConnectSession;->scopes:Ljava/util/Set;

    invoke-static {p1}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object p1

    iput-object p1, p0, Lcom/microsoft/services/msa/LiveConnectSession;->scopes:Ljava/util/Set;

    .line 307
    iget-object p1, p0, Lcom/microsoft/services/msa/LiveConnectSession;->changeSupport:Ljava/beans/PropertyChangeSupport;

    const-string v1, "scopes"

    iget-object v2, p0, Lcom/microsoft/services/msa/LiveConnectSession;->scopes:Ljava/util/Set;

    invoke-virtual {p1, v1, v0, v2}, Ljava/beans/PropertyChangeSupport;->firePropertyChange(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method setTokenType(Ljava/lang/String;)V
    .locals 3

    .line 311
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveConnectSession;->tokenType:Ljava/lang/String;

    .line 312
    iput-object p1, p0, Lcom/microsoft/services/msa/LiveConnectSession;->tokenType:Ljava/lang/String;

    .line 314
    iget-object p1, p0, Lcom/microsoft/services/msa/LiveConnectSession;->changeSupport:Ljava/beans/PropertyChangeSupport;

    const-string v1, "tokenType"

    iget-object v2, p0, Lcom/microsoft/services/msa/LiveConnectSession;->tokenType:Ljava/lang/String;

    invoke-virtual {p1, v1, v0, v2}, Ljava/beans/PropertyChangeSupport;->firePropertyChange(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    const-string v0, "LiveConnectSession [accessToken=%s, authenticationToken=%s, expiresIn=%s, refreshToken=%s, scopes=%s, tokenType=%s]"

    const/4 v1, 0x6

    .line 200
    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/microsoft/services/msa/LiveConnectSession;->accessToken:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/microsoft/services/msa/LiveConnectSession;->authenticationToken:Ljava/lang/String;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/microsoft/services/msa/LiveConnectSession;->expiresIn:Ljava/util/Date;

    const/4 v3, 0x2

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/microsoft/services/msa/LiveConnectSession;->refreshToken:Ljava/lang/String;

    const/4 v3, 0x3

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/microsoft/services/msa/LiveConnectSession;->scopes:Ljava/util/Set;

    const/4 v3, 0x4

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/microsoft/services/msa/LiveConnectSession;->tokenType:Ljava/lang/String;

    const/4 v3, 0x5

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method willExpireInSecs(I)Z
    .locals 2

    .line 318
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const/16 v1, 0xd

    .line 319
    invoke-virtual {v0, v1, p1}, Ljava/util/Calendar;->add(II)V

    .line 321
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object p1

    .line 325
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveConnectSession;->expiresIn:Ljava/util/Date;

    invoke-virtual {p1, v0}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result p1

    return p1
.end method

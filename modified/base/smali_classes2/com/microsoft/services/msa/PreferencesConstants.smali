.class final Lcom/microsoft/services/msa/PreferencesConstants;
.super Ljava/lang/Object;
.source "PreferencesConstants.java"


# static fields
.field public static final COOKIES_KEY:Ljava/lang/String; = "cookies"

.field public static final COOKIE_DELIMITER:Ljava/lang/String; = ","

.field public static final FILE_NAME:Ljava/lang/String; = "com.microsoft.live"

.field public static final REFRESH_TOKEN_KEY:Ljava/lang/String; = "refresh_token"


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

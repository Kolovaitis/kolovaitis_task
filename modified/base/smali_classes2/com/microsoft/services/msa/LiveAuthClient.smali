.class public Lcom/microsoft/services/msa/LiveAuthClient;
.super Ljava/lang/Object;
.source "LiveAuthClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/services/msa/LiveAuthClient$SessionRefresher;,
        Lcom/microsoft/services/msa/LiveAuthClient$RefreshTokenWriter;,
        Lcom/microsoft/services/msa/LiveAuthClient$ListenerCallerObserver;,
        Lcom/microsoft/services/msa/LiveAuthClient$AuthListenerCaller;,
        Lcom/microsoft/services/msa/LiveAuthClient$AuthErrorRunnable;,
        Lcom/microsoft/services/msa/LiveAuthClient$AuthCompleteRunnable;
    }
.end annotation


# static fields
.field private static final NULL_LISTENER:Lcom/microsoft/services/msa/LiveAuthListener;

.field private static final TAG:Ljava/lang/String; = "LiveAuthClient"


# instance fields
.field private final applicationContext:Landroid/content/Context;

.field private baseScopes:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final clientId:Ljava/lang/String;

.field private hasPendingLoginRequest:Z

.field private httpClient:Lorg/apache/http/client/HttpClient;

.field private final mOAuthConfig:Lcom/microsoft/services/msa/OAuthConfig;

.field private final session:Lcom/microsoft/services/msa/LiveConnectSession;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 225
    new-instance v0, Lcom/microsoft/services/msa/LiveAuthClient$1;

    invoke-direct {v0}, Lcom/microsoft/services/msa/LiveAuthClient$1;-><init>()V

    sput-object v0, Lcom/microsoft/services/msa/LiveAuthClient;->NULL_LISTENER:Lcom/microsoft/services/msa/LiveAuthListener;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 312
    invoke-direct {p0, p1, p2, v0}, Lcom/microsoft/services/msa/LiveAuthClient;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Iterable;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Iterable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/Iterable<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x0

    .line 318
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/microsoft/services/msa/LiveAuthClient;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Iterable;Lcom/microsoft/services/msa/OAuthConfig;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Iterable;Lcom/microsoft/services/msa/OAuthConfig;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/Iterable<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/microsoft/services/msa/OAuthConfig;",
            ")V"
        }
    .end annotation

    .line 270
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 253
    new-instance v0, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v0}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    iput-object v0, p0, Lcom/microsoft/services/msa/LiveAuthClient;->httpClient:Lorg/apache/http/client/HttpClient;

    const/4 v0, 0x0

    .line 254
    iput-boolean v0, p0, Lcom/microsoft/services/msa/LiveAuthClient;->hasPendingLoginRequest:Z

    .line 255
    new-instance v1, Lcom/microsoft/services/msa/LiveConnectSession;

    invoke-direct {v1, p0}, Lcom/microsoft/services/msa/LiveConnectSession;-><init>(Lcom/microsoft/services/msa/LiveAuthClient;)V

    iput-object v1, p0, Lcom/microsoft/services/msa/LiveAuthClient;->session:Lcom/microsoft/services/msa/LiveConnectSession;

    const-string v1, "context"

    .line 271
    invoke-static {p1, v1}, Lcom/microsoft/services/msa/LiveConnectUtils;->assertNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "clientId"

    .line 272
    invoke-static {p2, v1}, Lcom/microsoft/services/msa/LiveConnectUtils;->assertNotNullOrEmpty(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/microsoft/services/msa/LiveAuthClient;->applicationContext:Landroid/content/Context;

    .line 275
    iput-object p2, p0, Lcom/microsoft/services/msa/LiveAuthClient;->clientId:Ljava/lang/String;

    if-nez p4, :cond_0

    .line 279
    invoke-static {}, Lcom/microsoft/services/msa/MicrosoftOAuthConfig;->getInstance()Lcom/microsoft/services/msa/MicrosoftOAuthConfig;

    move-result-object p1

    iput-object p1, p0, Lcom/microsoft/services/msa/LiveAuthClient;->mOAuthConfig:Lcom/microsoft/services/msa/OAuthConfig;

    goto :goto_0

    .line 281
    :cond_0
    iput-object p4, p0, Lcom/microsoft/services/msa/LiveAuthClient;->mOAuthConfig:Lcom/microsoft/services/msa/OAuthConfig;

    :goto_0
    if-nez p3, :cond_1

    .line 287
    new-array p1, v0, [Ljava/lang/String;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p3

    .line 290
    :cond_1
    new-instance p1, Ljava/util/HashSet;

    invoke-direct {p1}, Ljava/util/HashSet;-><init>()V

    iput-object p1, p0, Lcom/microsoft/services/msa/LiveAuthClient;->baseScopes:Ljava/util/Set;

    .line 291
    invoke-interface {p3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    .line 292
    iget-object p3, p0, Lcom/microsoft/services/msa/LiveAuthClient;->baseScopes:Ljava/util/Set;

    invoke-interface {p3, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 295
    :cond_2
    iget-object p1, p0, Lcom/microsoft/services/msa/LiveAuthClient;->baseScopes:Ljava/util/Set;

    invoke-static {p1}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object p1

    iput-object p1, p0, Lcom/microsoft/services/msa/LiveAuthClient;->baseScopes:Ljava/util/Set;

    .line 297
    invoke-direct {p0}, Lcom/microsoft/services/msa/LiveAuthClient;->getRefreshTokenFromPreferences()Ljava/lang/String;

    move-result-object v4

    .line 298
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_3

    const-string p1, " "

    .line 299
    iget-object p2, p0, Lcom/microsoft/services/msa/LiveAuthClient;->baseScopes:Ljava/util/Set;

    invoke-static {p1, p2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v5

    .line 300
    new-instance p1, Lcom/microsoft/services/msa/RefreshAccessTokenRequest;

    iget-object v2, p0, Lcom/microsoft/services/msa/LiveAuthClient;->httpClient:Lorg/apache/http/client/HttpClient;

    iget-object v3, p0, Lcom/microsoft/services/msa/LiveAuthClient;->clientId:Ljava/lang/String;

    iget-object v6, p0, Lcom/microsoft/services/msa/LiveAuthClient;->mOAuthConfig:Lcom/microsoft/services/msa/OAuthConfig;

    move-object v1, p1

    invoke-direct/range {v1 .. v6}, Lcom/microsoft/services/msa/RefreshAccessTokenRequest;-><init>(Lorg/apache/http/client/HttpClient;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/services/msa/OAuthConfig;)V

    .line 305
    new-instance p2, Lcom/microsoft/services/msa/TokenRequestAsync;

    invoke-direct {p2, p1}, Lcom/microsoft/services/msa/TokenRequestAsync;-><init>(Lcom/microsoft/services/msa/TokenRequest;)V

    .line 306
    new-instance p1, Lcom/microsoft/services/msa/LiveAuthClient$RefreshTokenWriter;

    const/4 p3, 0x0

    invoke-direct {p1, p0, p3}, Lcom/microsoft/services/msa/LiveAuthClient$RefreshTokenWriter;-><init>(Lcom/microsoft/services/msa/LiveAuthClient;Lcom/microsoft/services/msa/LiveAuthClient$1;)V

    invoke-virtual {p2, p1}, Lcom/microsoft/services/msa/TokenRequestAsync;->addObserver(Lcom/microsoft/services/msa/OAuthRequestObserver;)V

    .line 307
    new-array p1, v0, [Ljava/lang/Void;

    invoke-virtual {p2, p1}, Lcom/microsoft/services/msa/TokenRequestAsync;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_3
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/services/msa/LiveAuthClient;)Lcom/microsoft/services/msa/LiveConnectSession;
    .locals 0

    .line 52
    iget-object p0, p0, Lcom/microsoft/services/msa/LiveAuthClient;->session:Lcom/microsoft/services/msa/LiveConnectSession;

    return-object p0
.end method

.method static synthetic access$100(Lcom/microsoft/services/msa/LiveAuthClient;)Z
    .locals 0

    .line 52
    invoke-direct {p0}, Lcom/microsoft/services/msa/LiveAuthClient;->clearRefreshTokenFromPreferences()Z

    move-result p0

    return p0
.end method

.method static synthetic access$200(Lcom/microsoft/services/msa/LiveAuthClient;)Landroid/content/Context;
    .locals 0

    .line 52
    iget-object p0, p0, Lcom/microsoft/services/msa/LiveAuthClient;->applicationContext:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic access$402(Lcom/microsoft/services/msa/LiveAuthClient;Z)Z
    .locals 0

    .line 52
    iput-boolean p1, p0, Lcom/microsoft/services/msa/LiveAuthClient;->hasPendingLoginRequest:Z

    return p1
.end method

.method private clearRefreshTokenFromPreferences()Z
    .locals 2

    .line 606
    invoke-direct {p0}, Lcom/microsoft/services/msa/LiveAuthClient;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 607
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "refresh_token"

    .line 608
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 610
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    return v0
.end method

.method private getCookieKeysFromPreferences()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 619
    invoke-direct {p0}, Lcom/microsoft/services/msa/LiveAuthClient;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "cookies"

    const-string v2, ""

    .line 620
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ","

    .line 622
    invoke-static {v0, v1}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private getRefreshTokenFromPreferences()Ljava/lang/String;
    .locals 3

    .line 632
    invoke-direct {p0}, Lcom/microsoft/services/msa/LiveAuthClient;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "refresh_token"

    const/4 v2, 0x0

    .line 633
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getSharedPreferences()Landroid/content/SharedPreferences;
    .locals 3

    .line 614
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveAuthClient;->applicationContext:Landroid/content/Context;

    const-string v1, "com.microsoft.live"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getClientId()Ljava/lang/String;
    .locals 1

    .line 323
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveAuthClient;->clientId:Ljava/lang/String;

    return-object v0
.end method

.method getHttpClient()Lorg/apache/http/client/HttpClient;
    .locals 1

    .line 548
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveAuthClient;->httpClient:Lorg/apache/http/client/HttpClient;

    return-object v0
.end method

.method public getSession()Lcom/microsoft/services/msa/LiveConnectSession;
    .locals 1

    .line 553
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveAuthClient;->session:Lcom/microsoft/services/msa/LiveConnectSession;

    return-object v0
.end method

.method public login(Landroid/app/Activity;Lcom/microsoft/services/msa/LiveAuthListener;)V
    .locals 1

    const/4 v0, 0x0

    .line 327
    invoke-virtual {p0, p1, v0, v0, p2}, Lcom/microsoft/services/msa/LiveAuthClient;->login(Landroid/app/Activity;Ljava/lang/Iterable;Ljava/lang/Object;Lcom/microsoft/services/msa/LiveAuthListener;)V

    return-void
.end method

.method public login(Landroid/app/Activity;Ljava/lang/Iterable;Lcom/microsoft/services/msa/LiveAuthListener;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/lang/Iterable<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/microsoft/services/msa/LiveAuthListener;",
            ")V"
        }
    .end annotation

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p3

    .line 331
    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/services/msa/LiveAuthClient;->login(Landroid/app/Activity;Ljava/lang/Iterable;Ljava/lang/Object;Ljava/lang/String;Lcom/microsoft/services/msa/LiveAuthListener;)V

    return-void
.end method

.method public login(Landroid/app/Activity;Ljava/lang/Iterable;Ljava/lang/Object;Lcom/microsoft/services/msa/LiveAuthListener;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/lang/Iterable<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Object;",
            "Lcom/microsoft/services/msa/LiveAuthListener;",
            ")V"
        }
    .end annotation

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    .line 335
    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/services/msa/LiveAuthClient;->login(Landroid/app/Activity;Ljava/lang/Iterable;Ljava/lang/Object;Ljava/lang/String;Lcom/microsoft/services/msa/LiveAuthListener;)V

    return-void
.end method

.method public login(Landroid/app/Activity;Ljava/lang/Iterable;Ljava/lang/Object;Ljava/lang/String;Lcom/microsoft/services/msa/LiveAuthListener;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/lang/Iterable<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Object;",
            "Ljava/lang/String;",
            "Lcom/microsoft/services/msa/LiveAuthListener;",
            ")V"
        }
    .end annotation

    const-string v0, "activity"

    .line 364
    invoke-static {p1, v0}, Lcom/microsoft/services/msa/LiveConnectUtils;->assertNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-nez p5, :cond_0

    .line 367
    sget-object p5, Lcom/microsoft/services/msa/LiveAuthClient;->NULL_LISTENER:Lcom/microsoft/services/msa/LiveAuthListener;

    .line 370
    :cond_0
    iget-boolean v0, p0, Lcom/microsoft/services/msa/LiveAuthClient;->hasPendingLoginRequest:Z

    if-nez v0, :cond_3

    if-nez p2, :cond_1

    .line 377
    iget-object p2, p0, Lcom/microsoft/services/msa/LiveAuthClient;->baseScopes:Ljava/util/Set;

    if-nez p2, :cond_1

    const/4 p2, 0x0

    .line 378
    new-array p2, p2, [Ljava/lang/String;

    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    .line 385
    :cond_1
    invoke-virtual {p0, p2, p3, p5}, Lcom/microsoft/services/msa/LiveAuthClient;->loginSilent(Ljava/lang/Iterable;Ljava/lang/Object;Lcom/microsoft/services/msa/LiveAuthListener;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string p1, "LiveAuthClient"

    const-string p2, "Interactive login not required."

    .line 386
    invoke-static {p1, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_2
    const-string v0, " "

    .line 391
    invoke-static {v0, p2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v5

    .line 393
    new-instance p2, Lcom/microsoft/services/msa/AuthorizationRequest;

    iget-object v3, p0, Lcom/microsoft/services/msa/LiveAuthClient;->httpClient:Lorg/apache/http/client/HttpClient;

    iget-object v4, p0, Lcom/microsoft/services/msa/LiveAuthClient;->clientId:Ljava/lang/String;

    iget-object v7, p0, Lcom/microsoft/services/msa/LiveAuthClient;->mOAuthConfig:Lcom/microsoft/services/msa/OAuthConfig;

    move-object v1, p2

    move-object v2, p1

    move-object v6, p4

    invoke-direct/range {v1 .. v7}, Lcom/microsoft/services/msa/AuthorizationRequest;-><init>(Landroid/app/Activity;Lorg/apache/http/client/HttpClient;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/services/msa/OAuthConfig;)V

    .line 400
    new-instance p1, Lcom/microsoft/services/msa/LiveAuthClient$ListenerCallerObserver;

    invoke-direct {p1, p0, p5, p3}, Lcom/microsoft/services/msa/LiveAuthClient$ListenerCallerObserver;-><init>(Lcom/microsoft/services/msa/LiveAuthClient;Lcom/microsoft/services/msa/LiveAuthListener;Ljava/lang/Object;)V

    invoke-virtual {p2, p1}, Lcom/microsoft/services/msa/AuthorizationRequest;->addObserver(Lcom/microsoft/services/msa/OAuthRequestObserver;)V

    .line 401
    new-instance p1, Lcom/microsoft/services/msa/LiveAuthClient$RefreshTokenWriter;

    const/4 p3, 0x0

    invoke-direct {p1, p0, p3}, Lcom/microsoft/services/msa/LiveAuthClient$RefreshTokenWriter;-><init>(Lcom/microsoft/services/msa/LiveAuthClient;Lcom/microsoft/services/msa/LiveAuthClient$1;)V

    invoke-virtual {p2, p1}, Lcom/microsoft/services/msa/AuthorizationRequest;->addObserver(Lcom/microsoft/services/msa/OAuthRequestObserver;)V

    .line 402
    new-instance p1, Lcom/microsoft/services/msa/LiveAuthClient$2;

    invoke-direct {p1, p0}, Lcom/microsoft/services/msa/LiveAuthClient$2;-><init>(Lcom/microsoft/services/msa/LiveAuthClient;)V

    invoke-virtual {p2, p1}, Lcom/microsoft/services/msa/AuthorizationRequest;->addObserver(Lcom/microsoft/services/msa/OAuthRequestObserver;)V

    const/4 p1, 0x1

    .line 414
    iput-boolean p1, p0, Lcom/microsoft/services/msa/LiveAuthClient;->hasPendingLoginRequest:Z

    .line 416
    invoke-virtual {p2}, Lcom/microsoft/services/msa/AuthorizationRequest;->execute()V

    return-void

    .line 371
    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Another login operation is already in progress."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public loginSilent(Lcom/microsoft/services/msa/LiveAuthListener;)Ljava/lang/Boolean;
    .locals 1

    const/4 v0, 0x0

    .line 420
    invoke-virtual {p0, v0, v0, p1}, Lcom/microsoft/services/msa/LiveAuthClient;->loginSilent(Ljava/lang/Iterable;Ljava/lang/Object;Lcom/microsoft/services/msa/LiveAuthListener;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public loginSilent(Ljava/lang/Iterable;Lcom/microsoft/services/msa/LiveAuthListener;)Ljava/lang/Boolean;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/microsoft/services/msa/LiveAuthListener;",
            ")",
            "Ljava/lang/Boolean;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 424
    invoke-virtual {p0, p1, v0, p2}, Lcom/microsoft/services/msa/LiveAuthClient;->loginSilent(Ljava/lang/Iterable;Ljava/lang/Object;Lcom/microsoft/services/msa/LiveAuthListener;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public loginSilent(Ljava/lang/Iterable;Ljava/lang/Object;Lcom/microsoft/services/msa/LiveAuthListener;)Ljava/lang/Boolean;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Object;",
            "Lcom/microsoft/services/msa/LiveAuthListener;",
            ")",
            "Ljava/lang/Boolean;"
        }
    .end annotation

    .line 450
    iget-boolean v0, p0, Lcom/microsoft/services/msa/LiveAuthClient;->hasPendingLoginRequest:Z

    if-nez v0, :cond_5

    const/4 v0, 0x0

    if-nez p1, :cond_1

    .line 456
    iget-object p1, p0, Lcom/microsoft/services/msa/LiveAuthClient;->baseScopes:Ljava/util/Set;

    if-nez p1, :cond_0

    .line 457
    new-array p1, v0, [Ljava/lang/String;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    move-object v6, p1

    goto :goto_0

    :cond_0
    move-object v6, p1

    goto :goto_0

    :cond_1
    move-object v6, p1

    .line 465
    :goto_0
    iget-object p1, p0, Lcom/microsoft/services/msa/LiveAuthClient;->session:Lcom/microsoft/services/msa/LiveConnectSession;

    invoke-virtual {p1}, Lcom/microsoft/services/msa/LiveConnectSession;->getRefreshToken()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 466
    iget-object p1, p0, Lcom/microsoft/services/msa/LiveAuthClient;->session:Lcom/microsoft/services/msa/LiveConnectSession;

    invoke-direct {p0}, Lcom/microsoft/services/msa/LiveAuthClient;->getRefreshTokenFromPreferences()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/microsoft/services/msa/LiveConnectSession;->setRefreshToken(Ljava/lang/String;)V

    .line 470
    :cond_2
    iget-object p1, p0, Lcom/microsoft/services/msa/LiveAuthClient;->session:Lcom/microsoft/services/msa/LiveConnectSession;

    invoke-virtual {p1}, Lcom/microsoft/services/msa/LiveConnectSession;->isExpired()Z

    move-result p1

    const/4 v7, 0x1

    if-nez p1, :cond_4

    iget-object p1, p0, Lcom/microsoft/services/msa/LiveAuthClient;->session:Lcom/microsoft/services/msa/LiveConnectSession;

    invoke-virtual {p1, v6}, Lcom/microsoft/services/msa/LiveConnectSession;->contains(Ljava/lang/Iterable;)Z

    move-result p1

    if-nez p1, :cond_3

    goto :goto_1

    :cond_3
    const/4 v3, 0x0

    goto :goto_2

    :cond_4
    :goto_1
    const/4 v3, 0x1

    .line 471
    :goto_2
    iget-object p1, p0, Lcom/microsoft/services/msa/LiveAuthClient;->session:Lcom/microsoft/services/msa/LiveConnectSession;

    invoke-virtual {p1}, Lcom/microsoft/services/msa/LiveConnectSession;->getRefreshToken()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    .line 473
    new-instance v8, Lcom/microsoft/services/msa/LiveAuthClient$3;

    move-object v1, v8

    move-object v2, p0

    move-object v4, p3

    move-object v5, p2

    invoke-direct/range {v1 .. v6}, Lcom/microsoft/services/msa/LiveAuthClient$3;-><init>(Lcom/microsoft/services/msa/LiveAuthClient;ZLcom/microsoft/services/msa/LiveAuthListener;Ljava/lang/Object;Ljava/lang/Iterable;)V

    new-array p2, v0, [Ljava/lang/Void;

    invoke-virtual {v8, p2}, Lcom/microsoft/services/msa/LiveAuthClient$3;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    xor-int/2addr p1, v7

    .line 491
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    .line 451
    :cond_5
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Another login operation is already in progress."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public loginSilent(Ljava/lang/Object;Lcom/microsoft/services/msa/LiveAuthListener;)Ljava/lang/Boolean;
    .locals 1

    const/4 v0, 0x0

    .line 428
    invoke-virtual {p0, v0, p1, p2}, Lcom/microsoft/services/msa/LiveAuthClient;->loginSilent(Ljava/lang/Iterable;Ljava/lang/Object;Lcom/microsoft/services/msa/LiveAuthListener;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public logout(Lcom/microsoft/services/msa/LiveAuthListener;)V
    .locals 1

    const/4 v0, 0x0

    .line 505
    invoke-virtual {p0, v0, p1}, Lcom/microsoft/services/msa/LiveAuthClient;->logout(Ljava/lang/Object;Lcom/microsoft/services/msa/LiveAuthListener;)V

    return-void
.end method

.method public logout(Ljava/lang/Object;Lcom/microsoft/services/msa/LiveAuthListener;)V
    .locals 5

    if-nez p2, :cond_0

    .line 521
    sget-object p2, Lcom/microsoft/services/msa/LiveAuthClient;->NULL_LISTENER:Lcom/microsoft/services/msa/LiveAuthListener;

    .line 524
    :cond_0
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveAuthClient;->session:Lcom/microsoft/services/msa/LiveConnectSession;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/services/msa/LiveConnectSession;->setAccessToken(Ljava/lang/String;)V

    .line 525
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveAuthClient;->session:Lcom/microsoft/services/msa/LiveConnectSession;

    invoke-virtual {v0, v1}, Lcom/microsoft/services/msa/LiveConnectSession;->setAuthenticationToken(Ljava/lang/String;)V

    .line 526
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveAuthClient;->session:Lcom/microsoft/services/msa/LiveConnectSession;

    invoke-virtual {v0, v1}, Lcom/microsoft/services/msa/LiveConnectSession;->setRefreshToken(Ljava/lang/String;)V

    .line 527
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveAuthClient;->session:Lcom/microsoft/services/msa/LiveConnectSession;

    invoke-virtual {v0, v1}, Lcom/microsoft/services/msa/LiveConnectSession;->setScopes(Ljava/lang/Iterable;)V

    .line 528
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveAuthClient;->session:Lcom/microsoft/services/msa/LiveConnectSession;

    invoke-virtual {v0, v1}, Lcom/microsoft/services/msa/LiveConnectSession;->setTokenType(Ljava/lang/String;)V

    .line 530
    invoke-direct {p0}, Lcom/microsoft/services/msa/LiveAuthClient;->clearRefreshTokenFromPreferences()Z

    .line 532
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveAuthClient;->applicationContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    move-result-object v0

    .line 534
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v2

    .line 537
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x15

    if-lt v3, v4, :cond_1

    .line 538
    invoke-virtual {v2, v1}, Landroid/webkit/CookieManager;->removeAllCookies(Landroid/webkit/ValueCallback;)V

    goto :goto_0

    .line 540
    :cond_1
    invoke-virtual {v2}, Landroid/webkit/CookieManager;->removeAllCookie()V

    .line 542
    :goto_0
    invoke-virtual {v0}, Landroid/webkit/CookieSyncManager;->sync()V

    .line 543
    sget-object v0, Lcom/microsoft/services/msa/LiveStatus;->UNKNOWN:Lcom/microsoft/services/msa/LiveStatus;

    invoke-interface {p2, v0, v1, p1}, Lcom/microsoft/services/msa/LiveAuthListener;->onAuthComplete(Lcom/microsoft/services/msa/LiveStatus;Lcom/microsoft/services/msa/LiveConnectSession;Ljava/lang/Object;)V

    return-void
.end method

.method setHttpClient(Lorg/apache/http/client/HttpClient;)V
    .locals 0

    if-eqz p1, :cond_0

    .line 596
    iput-object p1, p0, Lcom/microsoft/services/msa/LiveAuthClient;->httpClient:Lorg/apache/http/client/HttpClient;

    return-void

    .line 595
    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1
.end method

.method tryRefresh(Ljava/lang/Iterable;)Ljava/lang/Boolean;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/Boolean;"
        }
    .end annotation

    const-string v0, " "

    .line 563
    invoke-static {v0, p1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v5

    .line 564
    iget-object p1, p0, Lcom/microsoft/services/msa/LiveAuthClient;->session:Lcom/microsoft/services/msa/LiveConnectSession;

    invoke-virtual {p1}, Lcom/microsoft/services/msa/LiveConnectSession;->getRefreshToken()Ljava/lang/String;

    move-result-object v4

    .line 566
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    const-string p1, "LiveAuthClient"

    const-string v1, "No refresh token available, sorry!"

    .line 567
    invoke-static {p1, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 568
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    :cond_0
    const-string p1, "LiveAuthClient"

    const-string v1, "Refresh token found, attempting to refresh access and refresh tokens."

    .line 571
    invoke-static {p1, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 572
    new-instance p1, Lcom/microsoft/services/msa/RefreshAccessTokenRequest;

    iget-object v2, p0, Lcom/microsoft/services/msa/LiveAuthClient;->httpClient:Lorg/apache/http/client/HttpClient;

    iget-object v3, p0, Lcom/microsoft/services/msa/LiveAuthClient;->clientId:Ljava/lang/String;

    iget-object v6, p0, Lcom/microsoft/services/msa/LiveAuthClient;->mOAuthConfig:Lcom/microsoft/services/msa/OAuthConfig;

    move-object v1, p1

    invoke-direct/range {v1 .. v6}, Lcom/microsoft/services/msa/RefreshAccessTokenRequest;-><init>(Lorg/apache/http/client/HttpClient;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/services/msa/OAuthConfig;)V

    .line 577
    :try_start_0
    invoke-virtual {p1}, Lcom/microsoft/services/msa/RefreshAccessTokenRequest;->execute()Lcom/microsoft/services/msa/OAuthResponse;

    move-result-object p1
    :try_end_0
    .catch Lcom/microsoft/services/msa/LiveAuthException; {:try_start_0 .. :try_end_0} :catch_0

    .line 582
    new-instance v0, Lcom/microsoft/services/msa/LiveAuthClient$SessionRefresher;

    iget-object v1, p0, Lcom/microsoft/services/msa/LiveAuthClient;->session:Lcom/microsoft/services/msa/LiveConnectSession;

    invoke-direct {v0, v1}, Lcom/microsoft/services/msa/LiveAuthClient$SessionRefresher;-><init>(Lcom/microsoft/services/msa/LiveConnectSession;)V

    .line 583
    invoke-interface {p1, v0}, Lcom/microsoft/services/msa/OAuthResponse;->accept(Lcom/microsoft/services/msa/OAuthResponseVisitor;)V

    .line 584
    new-instance v1, Lcom/microsoft/services/msa/LiveAuthClient$RefreshTokenWriter;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/microsoft/services/msa/LiveAuthClient$RefreshTokenWriter;-><init>(Lcom/microsoft/services/msa/LiveAuthClient;Lcom/microsoft/services/msa/LiveAuthClient$1;)V

    invoke-interface {p1, v1}, Lcom/microsoft/services/msa/OAuthResponse;->accept(Lcom/microsoft/services/msa/OAuthResponseVisitor;)V

    .line 586
    invoke-virtual {v0}, Lcom/microsoft/services/msa/LiveAuthClient$SessionRefresher;->visitedSuccessfulResponse()Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    .line 579
    :catch_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.class public final enum Lcom/microsoft/services/msa/OAuth$GrantType;
.super Ljava/lang/Enum;
.source "OAuth.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/services/msa/OAuth;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "GrantType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/microsoft/services/msa/OAuth$GrantType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/services/msa/OAuth$GrantType;

.field public static final enum AUTHORIZATION_CODE:Lcom/microsoft/services/msa/OAuth$GrantType;

.field public static final enum CLIENT_CREDENTIALS:Lcom/microsoft/services/msa/OAuth$GrantType;

.field public static final enum PASSWORD:Lcom/microsoft/services/msa/OAuth$GrantType;

.field public static final enum REFRESH_TOKEN:Lcom/microsoft/services/msa/OAuth$GrantType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 92
    new-instance v0, Lcom/microsoft/services/msa/OAuth$GrantType;

    const-string v1, "AUTHORIZATION_CODE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/microsoft/services/msa/OAuth$GrantType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/services/msa/OAuth$GrantType;->AUTHORIZATION_CODE:Lcom/microsoft/services/msa/OAuth$GrantType;

    .line 93
    new-instance v0, Lcom/microsoft/services/msa/OAuth$GrantType;

    const-string v1, "CLIENT_CREDENTIALS"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/microsoft/services/msa/OAuth$GrantType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/services/msa/OAuth$GrantType;->CLIENT_CREDENTIALS:Lcom/microsoft/services/msa/OAuth$GrantType;

    .line 94
    new-instance v0, Lcom/microsoft/services/msa/OAuth$GrantType;

    const-string v1, "PASSWORD"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/microsoft/services/msa/OAuth$GrantType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/services/msa/OAuth$GrantType;->PASSWORD:Lcom/microsoft/services/msa/OAuth$GrantType;

    .line 95
    new-instance v0, Lcom/microsoft/services/msa/OAuth$GrantType;

    const-string v1, "REFRESH_TOKEN"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lcom/microsoft/services/msa/OAuth$GrantType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/services/msa/OAuth$GrantType;->REFRESH_TOKEN:Lcom/microsoft/services/msa/OAuth$GrantType;

    const/4 v0, 0x4

    .line 91
    new-array v0, v0, [Lcom/microsoft/services/msa/OAuth$GrantType;

    sget-object v1, Lcom/microsoft/services/msa/OAuth$GrantType;->AUTHORIZATION_CODE:Lcom/microsoft/services/msa/OAuth$GrantType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/services/msa/OAuth$GrantType;->CLIENT_CREDENTIALS:Lcom/microsoft/services/msa/OAuth$GrantType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/services/msa/OAuth$GrantType;->PASSWORD:Lcom/microsoft/services/msa/OAuth$GrantType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/services/msa/OAuth$GrantType;->REFRESH_TOKEN:Lcom/microsoft/services/msa/OAuth$GrantType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/microsoft/services/msa/OAuth$GrantType;->$VALUES:[Lcom/microsoft/services/msa/OAuth$GrantType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 91
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/services/msa/OAuth$GrantType;
    .locals 1

    .line 91
    const-class v0, Lcom/microsoft/services/msa/OAuth$GrantType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/microsoft/services/msa/OAuth$GrantType;

    return-object p0
.end method

.method public static values()[Lcom/microsoft/services/msa/OAuth$GrantType;
    .locals 1

    .line 91
    sget-object v0, Lcom/microsoft/services/msa/OAuth$GrantType;->$VALUES:[Lcom/microsoft/services/msa/OAuth$GrantType;

    invoke-virtual {v0}, [Lcom/microsoft/services/msa/OAuth$GrantType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/services/msa/OAuth$GrantType;

    return-object v0
.end method

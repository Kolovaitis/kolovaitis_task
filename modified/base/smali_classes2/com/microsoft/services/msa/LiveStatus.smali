.class public final enum Lcom/microsoft/services/msa/LiveStatus;
.super Ljava/lang/Enum;
.source "LiveStatus.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/microsoft/services/msa/LiveStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/services/msa/LiveStatus;

.field public static final enum CONNECTED:Lcom/microsoft/services/msa/LiveStatus;

.field public static final enum NOT_CONNECTED:Lcom/microsoft/services/msa/LiveStatus;

.field public static final enum UNKNOWN:Lcom/microsoft/services/msa/LiveStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 30
    new-instance v0, Lcom/microsoft/services/msa/LiveStatus;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/microsoft/services/msa/LiveStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/services/msa/LiveStatus;->UNKNOWN:Lcom/microsoft/services/msa/LiveStatus;

    .line 33
    new-instance v0, Lcom/microsoft/services/msa/LiveStatus;

    const-string v1, "CONNECTED"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/microsoft/services/msa/LiveStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/services/msa/LiveStatus;->CONNECTED:Lcom/microsoft/services/msa/LiveStatus;

    .line 36
    new-instance v0, Lcom/microsoft/services/msa/LiveStatus;

    const-string v1, "NOT_CONNECTED"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/microsoft/services/msa/LiveStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/services/msa/LiveStatus;->NOT_CONNECTED:Lcom/microsoft/services/msa/LiveStatus;

    const/4 v0, 0x3

    .line 28
    new-array v0, v0, [Lcom/microsoft/services/msa/LiveStatus;

    sget-object v1, Lcom/microsoft/services/msa/LiveStatus;->UNKNOWN:Lcom/microsoft/services/msa/LiveStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/services/msa/LiveStatus;->CONNECTED:Lcom/microsoft/services/msa/LiveStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/services/msa/LiveStatus;->NOT_CONNECTED:Lcom/microsoft/services/msa/LiveStatus;

    aput-object v1, v0, v4

    sput-object v0, Lcom/microsoft/services/msa/LiveStatus;->$VALUES:[Lcom/microsoft/services/msa/LiveStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 28
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/services/msa/LiveStatus;
    .locals 1

    .line 28
    const-class v0, Lcom/microsoft/services/msa/LiveStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/microsoft/services/msa/LiveStatus;

    return-object p0
.end method

.method public static values()[Lcom/microsoft/services/msa/LiveStatus;
    .locals 1

    .line 28
    sget-object v0, Lcom/microsoft/services/msa/LiveStatus;->$VALUES:[Lcom/microsoft/services/msa/LiveStatus;

    invoke-virtual {v0}, [Lcom/microsoft/services/msa/LiveStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/services/msa/LiveStatus;

    return-object v0
.end method

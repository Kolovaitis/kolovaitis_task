.class public final enum Lcom/microsoft/services/msa/OAuth$TokenType;
.super Ljava/lang/Enum;
.source "OAuth.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/services/msa/OAuth;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TokenType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/microsoft/services/msa/OAuth$TokenType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/services/msa/OAuth$TokenType;

.field public static final enum BEARER:Lcom/microsoft/services/msa/OAuth$TokenType;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 104
    new-instance v0, Lcom/microsoft/services/msa/OAuth$TokenType;

    const-string v1, "BEARER"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/microsoft/services/msa/OAuth$TokenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/services/msa/OAuth$TokenType;->BEARER:Lcom/microsoft/services/msa/OAuth$TokenType;

    const/4 v0, 0x1

    .line 103
    new-array v0, v0, [Lcom/microsoft/services/msa/OAuth$TokenType;

    sget-object v1, Lcom/microsoft/services/msa/OAuth$TokenType;->BEARER:Lcom/microsoft/services/msa/OAuth$TokenType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/services/msa/OAuth$TokenType;->$VALUES:[Lcom/microsoft/services/msa/OAuth$TokenType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 103
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/services/msa/OAuth$TokenType;
    .locals 1

    .line 103
    const-class v0, Lcom/microsoft/services/msa/OAuth$TokenType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/microsoft/services/msa/OAuth$TokenType;

    return-object p0
.end method

.method public static values()[Lcom/microsoft/services/msa/OAuth$TokenType;
    .locals 1

    .line 103
    sget-object v0, Lcom/microsoft/services/msa/OAuth$TokenType;->$VALUES:[Lcom/microsoft/services/msa/OAuth$TokenType;

    invoke-virtual {v0}, [Lcom/microsoft/services/msa/OAuth$TokenType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/services/msa/OAuth$TokenType;

    return-object v0
.end method

.class Lcom/microsoft/services/msa/AccessTokenRequest;
.super Lcom/microsoft/services/msa/TokenRequest;
.source "AccessTokenRequest.java"


# instance fields
.field private final code:Ljava/lang/String;

.field private final grantType:Lcom/microsoft/services/msa/OAuth$GrantType;


# direct methods
.method public constructor <init>(Lorg/apache/http/client/HttpClient;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/services/msa/OAuthConfig;)V
    .locals 0

    .line 62
    invoke-direct {p0, p1, p2, p4}, Lcom/microsoft/services/msa/TokenRequest;-><init>(Lorg/apache/http/client/HttpClient;Ljava/lang/String;Lcom/microsoft/services/msa/OAuthConfig;)V

    .line 64
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 67
    iput-object p3, p0, Lcom/microsoft/services/msa/AccessTokenRequest;->code:Ljava/lang/String;

    .line 68
    sget-object p1, Lcom/microsoft/services/msa/OAuth$GrantType;->AUTHORIZATION_CODE:Lcom/microsoft/services/msa/OAuth$GrantType;

    iput-object p1, p0, Lcom/microsoft/services/msa/AccessTokenRequest;->grantType:Lcom/microsoft/services/msa/OAuth$GrantType;

    return-void

    .line 65
    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1
.end method


# virtual methods
.method protected constructBody(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/apache/http/NameValuePair;",
            ">;)V"
        }
    .end annotation

    .line 78
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "code"

    iget-object v2, p0, Lcom/microsoft/services/msa/AccessTokenRequest;->code:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 79
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "redirect_uri"

    iget-object v2, p0, Lcom/microsoft/services/msa/AccessTokenRequest;->mOAuthConfig:Lcom/microsoft/services/msa/OAuthConfig;

    invoke-interface {v2}, Lcom/microsoft/services/msa/OAuthConfig;->getDesktopUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 80
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "grant_type"

    iget-object v2, p0, Lcom/microsoft/services/msa/AccessTokenRequest;->grantType:Lcom/microsoft/services/msa/OAuth$GrantType;

    invoke-virtual {v2}, Lcom/microsoft/services/msa/OAuth$GrantType;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.class public final enum Lcom/microsoft/services/msa/OAuth$ResponseType;
.super Ljava/lang/Enum;
.source "OAuth.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/services/msa/OAuth;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ResponseType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/microsoft/services/msa/OAuth$ResponseType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/services/msa/OAuth$ResponseType;

.field public static final enum CODE:Lcom/microsoft/services/msa/OAuth$ResponseType;

.field public static final enum TOKEN:Lcom/microsoft/services/msa/OAuth$ResponseType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 99
    new-instance v0, Lcom/microsoft/services/msa/OAuth$ResponseType;

    const-string v1, "CODE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/microsoft/services/msa/OAuth$ResponseType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/services/msa/OAuth$ResponseType;->CODE:Lcom/microsoft/services/msa/OAuth$ResponseType;

    .line 100
    new-instance v0, Lcom/microsoft/services/msa/OAuth$ResponseType;

    const-string v1, "TOKEN"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/microsoft/services/msa/OAuth$ResponseType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/services/msa/OAuth$ResponseType;->TOKEN:Lcom/microsoft/services/msa/OAuth$ResponseType;

    const/4 v0, 0x2

    .line 98
    new-array v0, v0, [Lcom/microsoft/services/msa/OAuth$ResponseType;

    sget-object v1, Lcom/microsoft/services/msa/OAuth$ResponseType;->CODE:Lcom/microsoft/services/msa/OAuth$ResponseType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/services/msa/OAuth$ResponseType;->TOKEN:Lcom/microsoft/services/msa/OAuth$ResponseType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/microsoft/services/msa/OAuth$ResponseType;->$VALUES:[Lcom/microsoft/services/msa/OAuth$ResponseType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 98
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/services/msa/OAuth$ResponseType;
    .locals 1

    .line 98
    const-class v0, Lcom/microsoft/services/msa/OAuth$ResponseType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/microsoft/services/msa/OAuth$ResponseType;

    return-object p0
.end method

.method public static values()[Lcom/microsoft/services/msa/OAuth$ResponseType;
    .locals 1

    .line 98
    sget-object v0, Lcom/microsoft/services/msa/OAuth$ResponseType;->$VALUES:[Lcom/microsoft/services/msa/OAuth$ResponseType;

    invoke-virtual {v0}, [Lcom/microsoft/services/msa/OAuth$ResponseType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/services/msa/OAuth$ResponseType;

    return-object v0
.end method

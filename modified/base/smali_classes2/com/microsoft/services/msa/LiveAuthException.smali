.class public Lcom/microsoft/services/msa/LiveAuthException;
.super Ljava/lang/RuntimeException;
.source "LiveAuthException.java"


# static fields
.field private static final serialVersionUID:J = 0x2ebff25dba7416c8L


# instance fields
.field private final error:Ljava/lang/String;

.field private final errorUri:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 37
    invoke-direct {p0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    const-string p1, ""

    .line 38
    iput-object p1, p0, Lcom/microsoft/services/msa/LiveAuthException;->error:Ljava/lang/String;

    const-string p1, ""

    .line 39
    iput-object p1, p0, Lcom/microsoft/services/msa/LiveAuthException;->errorUri:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 49
    invoke-direct {p0, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    if-eqz p1, :cond_0

    .line 53
    iput-object p1, p0, Lcom/microsoft/services/msa/LiveAuthException;->error:Ljava/lang/String;

    .line 54
    iput-object p3, p0, Lcom/microsoft/services/msa/LiveAuthException;->errorUri:Ljava/lang/String;

    return-void

    .line 51
    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    .line 58
    invoke-direct {p0, p2, p4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    if-eqz p1, :cond_0

    .line 62
    iput-object p1, p0, Lcom/microsoft/services/msa/LiveAuthException;->error:Ljava/lang/String;

    .line 63
    iput-object p3, p0, Lcom/microsoft/services/msa/LiveAuthException;->errorUri:Ljava/lang/String;

    return-void

    .line 60
    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    .line 43
    invoke-direct {p0, p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    const-string p1, ""

    .line 44
    iput-object p1, p0, Lcom/microsoft/services/msa/LiveAuthException;->error:Ljava/lang/String;

    const-string p1, ""

    .line 45
    iput-object p1, p0, Lcom/microsoft/services/msa/LiveAuthException;->errorUri:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getError()Ljava/lang/String;
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveAuthException;->error:Ljava/lang/String;

    return-object v0
.end method

.method public getErrorUri()Ljava/lang/String;
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveAuthException;->errorUri:Ljava/lang/String;

    return-object v0
.end method

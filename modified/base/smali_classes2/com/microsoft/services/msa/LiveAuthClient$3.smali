.class Lcom/microsoft/services/msa/LiveAuthClient$3;
.super Landroid/os/AsyncTask;
.source "LiveAuthClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/services/msa/LiveAuthClient;->loginSilent(Ljava/lang/Iterable;Ljava/lang/Object;Lcom/microsoft/services/msa/LiveAuthListener;)Ljava/lang/Boolean;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/services/msa/LiveAuthClient;

.field final synthetic val$activeScopes:Ljava/lang/Iterable;

.field final synthetic val$listener:Lcom/microsoft/services/msa/LiveAuthListener;

.field final synthetic val$needNewAccessToken:Z

.field final synthetic val$userState:Ljava/lang/Object;


# direct methods
.method constructor <init>(Lcom/microsoft/services/msa/LiveAuthClient;ZLcom/microsoft/services/msa/LiveAuthListener;Ljava/lang/Object;Ljava/lang/Iterable;)V
    .locals 0

    .line 473
    iput-object p1, p0, Lcom/microsoft/services/msa/LiveAuthClient$3;->this$0:Lcom/microsoft/services/msa/LiveAuthClient;

    iput-boolean p2, p0, Lcom/microsoft/services/msa/LiveAuthClient$3;->val$needNewAccessToken:Z

    iput-object p3, p0, Lcom/microsoft/services/msa/LiveAuthClient$3;->val$listener:Lcom/microsoft/services/msa/LiveAuthListener;

    iput-object p4, p0, Lcom/microsoft/services/msa/LiveAuthClient$3;->val$userState:Ljava/lang/Object;

    iput-object p5, p0, Lcom/microsoft/services/msa/LiveAuthClient$3;->val$activeScopes:Ljava/lang/Iterable;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 473
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/microsoft/services/msa/LiveAuthClient$3;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 3

    .line 476
    iget-boolean p1, p0, Lcom/microsoft/services/msa/LiveAuthClient$3;->val$needNewAccessToken:Z

    if-nez p1, :cond_0

    const-string p1, "LiveAuthClient"

    const-string v0, "Access token still valid, so using it."

    .line 477
    invoke-static {p1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 478
    iget-object p1, p0, Lcom/microsoft/services/msa/LiveAuthClient$3;->val$listener:Lcom/microsoft/services/msa/LiveAuthListener;

    sget-object v0, Lcom/microsoft/services/msa/LiveStatus;->CONNECTED:Lcom/microsoft/services/msa/LiveStatus;

    iget-object v1, p0, Lcom/microsoft/services/msa/LiveAuthClient$3;->this$0:Lcom/microsoft/services/msa/LiveAuthClient;

    invoke-static {v1}, Lcom/microsoft/services/msa/LiveAuthClient;->access$000(Lcom/microsoft/services/msa/LiveAuthClient;)Lcom/microsoft/services/msa/LiveConnectSession;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/services/msa/LiveAuthClient$3;->val$userState:Ljava/lang/Object;

    invoke-interface {p1, v0, v1, v2}, Lcom/microsoft/services/msa/LiveAuthListener;->onAuthComplete(Lcom/microsoft/services/msa/LiveStatus;Lcom/microsoft/services/msa/LiveConnectSession;Ljava/lang/Object;)V

    goto :goto_0

    .line 479
    :cond_0
    iget-object p1, p0, Lcom/microsoft/services/msa/LiveAuthClient$3;->this$0:Lcom/microsoft/services/msa/LiveAuthClient;

    iget-object v0, p0, Lcom/microsoft/services/msa/LiveAuthClient$3;->val$activeScopes:Ljava/lang/Iterable;

    invoke-virtual {p1, v0}, Lcom/microsoft/services/msa/LiveAuthClient;->tryRefresh(Ljava/lang/Iterable;)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_1

    const-string p1, "LiveAuthClient"

    const-string v0, "Used refresh token to refresh access and refresh tokens."

    .line 480
    invoke-static {p1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 481
    iget-object p1, p0, Lcom/microsoft/services/msa/LiveAuthClient$3;->val$listener:Lcom/microsoft/services/msa/LiveAuthListener;

    sget-object v0, Lcom/microsoft/services/msa/LiveStatus;->CONNECTED:Lcom/microsoft/services/msa/LiveStatus;

    iget-object v1, p0, Lcom/microsoft/services/msa/LiveAuthClient$3;->this$0:Lcom/microsoft/services/msa/LiveAuthClient;

    invoke-static {v1}, Lcom/microsoft/services/msa/LiveAuthClient;->access$000(Lcom/microsoft/services/msa/LiveAuthClient;)Lcom/microsoft/services/msa/LiveConnectSession;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/services/msa/LiveAuthClient$3;->val$userState:Ljava/lang/Object;

    invoke-interface {p1, v0, v1, v2}, Lcom/microsoft/services/msa/LiveAuthListener;->onAuthComplete(Lcom/microsoft/services/msa/LiveStatus;Lcom/microsoft/services/msa/LiveConnectSession;Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    const-string p1, "LiveAuthClient"

    const-string v0, "All tokens expired, you need to call login() to initiate interactive logon"

    .line 483
    invoke-static {p1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 484
    iget-object p1, p0, Lcom/microsoft/services/msa/LiveAuthClient$3;->val$listener:Lcom/microsoft/services/msa/LiveAuthListener;

    sget-object v0, Lcom/microsoft/services/msa/LiveStatus;->NOT_CONNECTED:Lcom/microsoft/services/msa/LiveStatus;

    iget-object v1, p0, Lcom/microsoft/services/msa/LiveAuthClient$3;->this$0:Lcom/microsoft/services/msa/LiveAuthClient;

    invoke-virtual {v1}, Lcom/microsoft/services/msa/LiveAuthClient;->getSession()Lcom/microsoft/services/msa/LiveConnectSession;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/services/msa/LiveAuthClient$3;->val$userState:Ljava/lang/Object;

    invoke-interface {p1, v0, v1, v2}, Lcom/microsoft/services/msa/LiveAuthListener;->onAuthComplete(Lcom/microsoft/services/msa/LiveStatus;Lcom/microsoft/services/msa/LiveConnectSession;Ljava/lang/Object;)V

    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

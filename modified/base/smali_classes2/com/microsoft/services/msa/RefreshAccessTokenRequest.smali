.class Lcom/microsoft/services/msa/RefreshAccessTokenRequest;
.super Lcom/microsoft/services/msa/TokenRequest;
.source "RefreshAccessTokenRequest.java"


# instance fields
.field private final grantType:Lcom/microsoft/services/msa/OAuth$GrantType;

.field private final refreshToken:Ljava/lang/String;

.field private final scope:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lorg/apache/http/client/HttpClient;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/services/msa/OAuthConfig;)V
    .locals 0

    .line 54
    invoke-direct {p0, p1, p2, p5}, Lcom/microsoft/services/msa/TokenRequest;-><init>(Lorg/apache/http/client/HttpClient;Ljava/lang/String;Lcom/microsoft/services/msa/OAuthConfig;)V

    .line 42
    sget-object p1, Lcom/microsoft/services/msa/OAuth$GrantType;->REFRESH_TOKEN:Lcom/microsoft/services/msa/OAuth$GrantType;

    iput-object p1, p0, Lcom/microsoft/services/msa/RefreshAccessTokenRequest;->grantType:Lcom/microsoft/services/msa/OAuth$GrantType;

    if-eqz p3, :cond_3

    .line 57
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_2

    if-eqz p4, :cond_1

    .line 59
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 61
    iput-object p3, p0, Lcom/microsoft/services/msa/RefreshAccessTokenRequest;->refreshToken:Ljava/lang/String;

    .line 62
    iput-object p4, p0, Lcom/microsoft/services/msa/RefreshAccessTokenRequest;->scope:Ljava/lang/String;

    return-void

    .line 59
    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1

    .line 58
    :cond_1
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1

    .line 57
    :cond_2
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1

    .line 56
    :cond_3
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1
.end method


# virtual methods
.method protected constructBody(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/apache/http/NameValuePair;",
            ">;)V"
        }
    .end annotation

    .line 67
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "refresh_token"

    iget-object v2, p0, Lcom/microsoft/services/msa/RefreshAccessTokenRequest;->refreshToken:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 68
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "scope"

    iget-object v2, p0, Lcom/microsoft/services/msa/RefreshAccessTokenRequest;->scope:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 69
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "grant_type"

    iget-object v2, p0, Lcom/microsoft/services/msa/RefreshAccessTokenRequest;->grantType:Lcom/microsoft/services/msa/OAuth$GrantType;

    invoke-virtual {v2}, Lcom/microsoft/services/msa/OAuth$GrantType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

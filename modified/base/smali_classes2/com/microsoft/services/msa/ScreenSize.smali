.class abstract enum Lcom/microsoft/services/msa/ScreenSize;
.super Ljava/lang/Enum;
.source "ScreenSize.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/microsoft/services/msa/ScreenSize;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/services/msa/ScreenSize;

.field public static final enum LARGE:Lcom/microsoft/services/msa/ScreenSize;

.field public static final enum NORMAL:Lcom/microsoft/services/msa/ScreenSize;

.field private static final SCREENLAYOUT_SIZE_XLARGE:I = 0x4

.field public static final enum SMALL:Lcom/microsoft/services/msa/ScreenSize;

.field public static final enum XLARGE:Lcom/microsoft/services/msa/ScreenSize;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 37
    new-instance v0, Lcom/microsoft/services/msa/ScreenSize$1;

    const-string v1, "SMALL"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/microsoft/services/msa/ScreenSize$1;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/services/msa/ScreenSize;->SMALL:Lcom/microsoft/services/msa/ScreenSize;

    .line 43
    new-instance v0, Lcom/microsoft/services/msa/ScreenSize$2;

    const-string v1, "NORMAL"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/microsoft/services/msa/ScreenSize$2;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/services/msa/ScreenSize;->NORMAL:Lcom/microsoft/services/msa/ScreenSize;

    .line 50
    new-instance v0, Lcom/microsoft/services/msa/ScreenSize$3;

    const-string v1, "LARGE"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/microsoft/services/msa/ScreenSize$3;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/services/msa/ScreenSize;->LARGE:Lcom/microsoft/services/msa/ScreenSize;

    .line 56
    new-instance v0, Lcom/microsoft/services/msa/ScreenSize$4;

    const-string v1, "XLARGE"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lcom/microsoft/services/msa/ScreenSize$4;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/services/msa/ScreenSize;->XLARGE:Lcom/microsoft/services/msa/ScreenSize;

    const/4 v0, 0x4

    .line 36
    new-array v0, v0, [Lcom/microsoft/services/msa/ScreenSize;

    sget-object v1, Lcom/microsoft/services/msa/ScreenSize;->SMALL:Lcom/microsoft/services/msa/ScreenSize;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/services/msa/ScreenSize;->NORMAL:Lcom/microsoft/services/msa/ScreenSize;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/services/msa/ScreenSize;->LARGE:Lcom/microsoft/services/msa/ScreenSize;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/services/msa/ScreenSize;->XLARGE:Lcom/microsoft/services/msa/ScreenSize;

    aput-object v1, v0, v5

    sput-object v0, Lcom/microsoft/services/msa/ScreenSize;->$VALUES:[Lcom/microsoft/services/msa/ScreenSize;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 36
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/microsoft/services/msa/ScreenSize$1;)V
    .locals 0

    .line 36
    invoke-direct {p0, p1, p2}, Lcom/microsoft/services/msa/ScreenSize;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static determineScreenSize(Landroid/app/Activity;)Lcom/microsoft/services/msa/ScreenSize;
    .locals 1

    .line 72
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p0

    iget p0, p0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 p0, p0, 0xf

    packed-switch p0, :pswitch_data_0

    const-string p0, "Live SDK ScreenSize"

    const-string v0, "Unable to determine ScreenSize. A Normal ScreenSize will be returned."

    .line 85
    invoke-static {p0, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    sget-object p0, Lcom/microsoft/services/msa/ScreenSize;->NORMAL:Lcom/microsoft/services/msa/ScreenSize;

    return-object p0

    .line 82
    :pswitch_0
    sget-object p0, Lcom/microsoft/services/msa/ScreenSize;->XLARGE:Lcom/microsoft/services/msa/ScreenSize;

    return-object p0

    .line 80
    :pswitch_1
    sget-object p0, Lcom/microsoft/services/msa/ScreenSize;->LARGE:Lcom/microsoft/services/msa/ScreenSize;

    return-object p0

    .line 78
    :pswitch_2
    sget-object p0, Lcom/microsoft/services/msa/ScreenSize;->NORMAL:Lcom/microsoft/services/msa/ScreenSize;

    return-object p0

    .line 76
    :pswitch_3
    sget-object p0, Lcom/microsoft/services/msa/ScreenSize;->SMALL:Lcom/microsoft/services/msa/ScreenSize;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/services/msa/ScreenSize;
    .locals 1

    .line 36
    const-class v0, Lcom/microsoft/services/msa/ScreenSize;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/microsoft/services/msa/ScreenSize;

    return-object p0
.end method

.method public static values()[Lcom/microsoft/services/msa/ScreenSize;
    .locals 1

    .line 36
    sget-object v0, Lcom/microsoft/services/msa/ScreenSize;->$VALUES:[Lcom/microsoft/services/msa/ScreenSize;

    invoke-virtual {v0}, [Lcom/microsoft/services/msa/ScreenSize;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/services/msa/ScreenSize;

    return-object v0
.end method


# virtual methods
.method public abstract getDeviceType()Lcom/microsoft/services/msa/DeviceType;
.end method

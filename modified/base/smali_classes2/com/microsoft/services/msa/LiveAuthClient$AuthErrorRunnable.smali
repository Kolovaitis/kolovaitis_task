.class Lcom/microsoft/services/msa/LiveAuthClient$AuthErrorRunnable;
.super Lcom/microsoft/services/msa/LiveAuthClient$AuthListenerCaller;
.source "LiveAuthClient.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/services/msa/LiveAuthClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AuthErrorRunnable"
.end annotation


# instance fields
.field private final exception:Lcom/microsoft/services/msa/LiveAuthException;


# direct methods
.method public constructor <init>(Lcom/microsoft/services/msa/LiveAuthListener;Ljava/lang/Object;Lcom/microsoft/services/msa/LiveAuthException;)V
    .locals 0

    .line 83
    invoke-direct {p0, p1, p2}, Lcom/microsoft/services/msa/LiveAuthClient$AuthListenerCaller;-><init>(Lcom/microsoft/services/msa/LiveAuthListener;Ljava/lang/Object;)V

    .line 84
    iput-object p3, p0, Lcom/microsoft/services/msa/LiveAuthClient$AuthErrorRunnable;->exception:Lcom/microsoft/services/msa/LiveAuthException;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 89
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveAuthClient$AuthErrorRunnable;->listener:Lcom/microsoft/services/msa/LiveAuthListener;

    iget-object v1, p0, Lcom/microsoft/services/msa/LiveAuthClient$AuthErrorRunnable;->exception:Lcom/microsoft/services/msa/LiveAuthException;

    iget-object v2, p0, Lcom/microsoft/services/msa/LiveAuthClient$AuthErrorRunnable;->userState:Ljava/lang/Object;

    invoke-interface {v0, v1, v2}, Lcom/microsoft/services/msa/LiveAuthListener;->onAuthError(Lcom/microsoft/services/msa/LiveAuthException;Ljava/lang/Object;)V

    return-void
.end method

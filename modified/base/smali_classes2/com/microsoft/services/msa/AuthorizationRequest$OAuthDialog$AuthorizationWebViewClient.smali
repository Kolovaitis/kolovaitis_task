.class Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog$AuthorizationWebViewClient;
.super Landroid/webkit/WebViewClient;
.source "AuthorizationRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AuthorizationWebViewClient"
.end annotation


# instance fields
.field private final cookieKeys:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final cookieManager:Landroid/webkit/CookieManager;

.field final synthetic this$1:Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog;


# direct methods
.method public constructor <init>(Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog;)V
    .locals 0

    .line 81
    iput-object p1, p0, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog$AuthorizationWebViewClient;->this$1:Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    .line 83
    invoke-virtual {p1}, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    .line 84
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object p1

    iput-object p1, p0, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog$AuthorizationWebViewClient;->cookieManager:Landroid/webkit/CookieManager;

    .line 85
    new-instance p1, Ljava/util/HashSet;

    invoke-direct {p1}, Ljava/util/HashSet;-><init>()V

    iput-object p1, p0, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog$AuthorizationWebViewClient;->cookieKeys:Ljava/util/Set;

    return-void
.end method

.method private dismissDialog()V
    .locals 1

    .line 120
    iget-object v0, p0, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog$AuthorizationWebViewClient;->this$1:Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog;

    invoke-virtual {v0}, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog$AuthorizationWebViewClient;->this$1:Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog;

    iget-object v0, v0, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog;->this$0:Lcom/microsoft/services/msa/AuthorizationRequest;

    invoke-static {v0}, Lcom/microsoft/services/msa/AuthorizationRequest;->access$200(Lcom/microsoft/services/msa/AuthorizationRequest;)Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog$AuthorizationWebViewClient;->this$1:Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog;

    iget-object v0, v0, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog;->this$0:Lcom/microsoft/services/msa/AuthorizationRequest;

    invoke-static {v0}, Lcom/microsoft/services/msa/AuthorizationRequest;->access$200(Lcom/microsoft/services/msa/AuthorizationRequest;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog$AuthorizationWebViewClient;->this$1:Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog;

    invoke-virtual {v0}, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog;->dismiss()V

    :cond_0
    return-void
.end method

.method private saveCookiesInMemory(Ljava/lang/String;)V
    .locals 5

    .line 150
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const-string v0, "; "

    .line 154
    invoke-static {p1, v0}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    .line 155
    array-length v0, p1

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    aget-object v3, p1, v2

    const-string v4, "="

    .line 156
    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    .line 157
    invoke-virtual {v3, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 158
    iget-object v4, p0, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog$AuthorizationWebViewClient;->cookieKeys:Ljava/util/Set;

    invoke-interface {v4, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private saveCookiesToPreferences()V
    .locals 3

    .line 163
    iget-object v0, p0, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog$AuthorizationWebViewClient;->this$1:Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog;

    invoke-virtual {v0}, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.microsoft.live"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "cookies"

    const-string v2, ""

    .line 171
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ","

    .line 172
    invoke-static {v1, v2}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 174
    iget-object v2, p0, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog$AuthorizationWebViewClient;->cookieKeys:Ljava/util/Set;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 176
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, ","

    .line 177
    iget-object v2, p0, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog$AuthorizationWebViewClient;->cookieKeys:Ljava/util/Set;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "cookies"

    .line 178
    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 179
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 184
    iget-object v0, p0, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog$AuthorizationWebViewClient;->cookieKeys:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2

    .line 100
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    .line 103
    iget-object v0, p0, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog$AuthorizationWebViewClient;->this$1:Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog;

    iget-object v0, v0, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog;->this$0:Lcom/microsoft/services/msa/AuthorizationRequest;

    invoke-static {v0}, Lcom/microsoft/services/msa/AuthorizationRequest;->access$000(Lcom/microsoft/services/msa/AuthorizationRequest;)Lcom/microsoft/services/msa/OAuthConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/microsoft/services/msa/OAuthConfig;->getLogoutUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog$AuthorizationWebViewClient;->cookieManager:Landroid/webkit/CookieManager;

    invoke-virtual {v0, p2}, Landroid/webkit/CookieManager;->getCookie(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p0, p2}, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog$AuthorizationWebViewClient;->saveCookiesInMemory(Ljava/lang/String;)V

    .line 107
    :cond_0
    iget-object p2, p0, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog$AuthorizationWebViewClient;->this$1:Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog;

    iget-object p2, p2, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog;->this$0:Lcom/microsoft/services/msa/AuthorizationRequest;

    invoke-static {p2}, Lcom/microsoft/services/msa/AuthorizationRequest;->access$000(Lcom/microsoft/services/msa/AuthorizationRequest;)Lcom/microsoft/services/msa/OAuthConfig;

    move-result-object p2

    invoke-interface {p2}, Lcom/microsoft/services/msa/OAuthConfig;->getDesktopUri()Landroid/net/Uri;

    move-result-object p2

    .line 108
    sget-object v0, Lcom/microsoft/services/msa/AuthorizationRequest$UriComparator;->INSTANCE:Lcom/microsoft/services/msa/AuthorizationRequest$UriComparator;

    invoke-virtual {v0, p1, p2}, Lcom/microsoft/services/msa/AuthorizationRequest$UriComparator;->compare(Landroid/net/Uri;Landroid/net/Uri;)I

    move-result p2

    if-nez p2, :cond_1

    const/4 p2, 0x1

    goto :goto_0

    :cond_1
    const/4 p2, 0x0

    :goto_0
    if-nez p2, :cond_2

    return-void

    .line 113
    :cond_2
    invoke-direct {p0}, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog$AuthorizationWebViewClient;->saveCookiesToPreferences()V

    .line 115
    iget-object p2, p0, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog$AuthorizationWebViewClient;->this$1:Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog;

    iget-object p2, p2, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog;->this$0:Lcom/microsoft/services/msa/AuthorizationRequest;

    invoke-static {p2, p1}, Lcom/microsoft/services/msa/AuthorizationRequest;->access$100(Lcom/microsoft/services/msa/AuthorizationRequest;Landroid/net/Uri;)V

    .line 116
    invoke-direct {p0}, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog$AuthorizationWebViewClient;->dismissDialog()V

    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    const/16 p1, -0xa

    if-ne p2, p1, :cond_0

    return-void

    .line 144
    :cond_0
    iget-object p1, p0, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog$AuthorizationWebViewClient;->this$1:Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog;

    iget-object p1, p1, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog;->this$0:Lcom/microsoft/services/msa/AuthorizationRequest;

    const-string p2, ""

    invoke-static {p1, p2, p3, p4}, Lcom/microsoft/services/msa/AuthorizationRequest;->access$300(Lcom/microsoft/services/msa/AuthorizationRequest;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    invoke-direct {p0}, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog$AuthorizationWebViewClient;->dismissDialog()V

    return-void
.end method

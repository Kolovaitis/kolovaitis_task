.class Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog;
.super Landroid/app/Dialog;
.source "AuthorizationRequest.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/services/msa/AuthorizationRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OAuthDialog"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog$AuthorizationWebViewClient;
    }
.end annotation


# instance fields
.field private final requestUri:Landroid/net/Uri;

.field final synthetic this$0:Lcom/microsoft/services/msa/AuthorizationRequest;

.field private webView:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>(Lcom/microsoft/services/msa/AuthorizationRequest;Landroid/net/Uri;)V
    .locals 2

    .line 197
    iput-object p1, p0, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog;->this$0:Lcom/microsoft/services/msa/AuthorizationRequest;

    .line 198
    invoke-static {p1}, Lcom/microsoft/services/msa/AuthorizationRequest;->access$200(Lcom/microsoft/services/msa/AuthorizationRequest;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x1030010

    invoke-direct {p0, v0, v1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 199
    invoke-static {p1}, Lcom/microsoft/services/msa/AuthorizationRequest;->access$200(Lcom/microsoft/services/msa/AuthorizationRequest;)Landroid/app/Activity;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog;->setOwnerActivity(Landroid/app/Activity;)V

    if-eqz p2, :cond_0

    .line 202
    iput-object p2, p0, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog;->requestUri:Landroid/net/Uri;

    return-void

    .line 201
    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .line 208
    new-instance p1, Lcom/microsoft/services/msa/LiveAuthException;

    const-string v0, "The user cancelled the login operation."

    invoke-direct {p1, v0}, Lcom/microsoft/services/msa/LiveAuthException;-><init>(Ljava/lang/String;)V

    .line 209
    iget-object v0, p0, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog;->this$0:Lcom/microsoft/services/msa/AuthorizationRequest;

    invoke-virtual {v0, p1}, Lcom/microsoft/services/msa/AuthorizationRequest;->onException(Lcom/microsoft/services/msa/LiveAuthException;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetJavaScriptEnabled"
        }
    .end annotation

    .line 215
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 217
    invoke-virtual {p0, p0}, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 219
    new-instance p1, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p1, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 221
    iget-object v0, p0, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog;->webView:Landroid/webkit/WebView;

    const/4 v1, 0x0

    const/4 v2, -0x1

    if-nez v0, :cond_0

    .line 222
    new-instance v0, Landroid/webkit/WebView;

    invoke-virtual {p0}, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog;->webView:Landroid/webkit/WebView;

    .line 223
    iget-object v0, p0, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog;->webView:Landroid/webkit/WebView;

    new-instance v3, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog$AuthorizationWebViewClient;

    invoke-direct {v3, p0}, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog$AuthorizationWebViewClient;-><init>(Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog;)V

    invoke-virtual {v0, v3}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 224
    iget-object v0, p0, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v3, 0x1

    .line 225
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 226
    iget-object v0, p0, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog;->webView:Landroid/webkit/WebView;

    iget-object v3, p0, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog;->requestUri:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 227
    iget-object v0, p0, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog;->webView:Landroid/webkit/WebView;

    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v3, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v3}, Landroid/webkit/WebView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 229
    iget-object v0, p0, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 231
    :cond_0
    iget-object v0, p0, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog;->webView:Landroid/webkit/WebView;

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 232
    invoke-virtual {p1, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 233
    invoke-virtual {p1}, Landroid/widget/LinearLayout;->forceLayout()V

    .line 235
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, p1, v0}, Lcom/microsoft/services/msa/AuthorizationRequest$OAuthDialog;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

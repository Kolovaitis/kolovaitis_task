.class public interface abstract Lcom/microsoft/services/msa/OAuthConfig;
.super Ljava/lang/Object;
.source "OAuthConfig.java"


# virtual methods
.method public abstract getAuthorizeUri()Landroid/net/Uri;
.end method

.method public abstract getDesktopUri()Landroid/net/Uri;
.end method

.method public abstract getLogoutUri()Landroid/net/Uri;
.end method

.method public abstract getTokenUri()Landroid/net/Uri;
.end method

.class public abstract enum Lcom/microsoft/services/msa/OverwriteOption;
.super Ljava/lang/Enum;
.source "OverwriteOption.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/microsoft/services/msa/OverwriteOption;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/services/msa/OverwriteOption;

.field public static final enum DoNotOverwrite:Lcom/microsoft/services/msa/OverwriteOption;

.field public static final enum Overwrite:Lcom/microsoft/services/msa/OverwriteOption;

.field public static final enum Rename:Lcom/microsoft/services/msa/OverwriteOption;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 31
    new-instance v0, Lcom/microsoft/services/msa/OverwriteOption$1;

    const-string v1, "Overwrite"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/microsoft/services/msa/OverwriteOption$1;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/services/msa/OverwriteOption;->Overwrite:Lcom/microsoft/services/msa/OverwriteOption;

    .line 39
    new-instance v0, Lcom/microsoft/services/msa/OverwriteOption$2;

    const-string v1, "DoNotOverwrite"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/microsoft/services/msa/OverwriteOption$2;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/services/msa/OverwriteOption;->DoNotOverwrite:Lcom/microsoft/services/msa/OverwriteOption;

    .line 47
    new-instance v0, Lcom/microsoft/services/msa/OverwriteOption$3;

    const-string v1, "Rename"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/microsoft/services/msa/OverwriteOption$3;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/services/msa/OverwriteOption;->Rename:Lcom/microsoft/services/msa/OverwriteOption;

    const/4 v0, 0x3

    .line 28
    new-array v0, v0, [Lcom/microsoft/services/msa/OverwriteOption;

    sget-object v1, Lcom/microsoft/services/msa/OverwriteOption;->Overwrite:Lcom/microsoft/services/msa/OverwriteOption;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/services/msa/OverwriteOption;->DoNotOverwrite:Lcom/microsoft/services/msa/OverwriteOption;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/services/msa/OverwriteOption;->Rename:Lcom/microsoft/services/msa/OverwriteOption;

    aput-object v1, v0, v4

    sput-object v0, Lcom/microsoft/services/msa/OverwriteOption;->$VALUES:[Lcom/microsoft/services/msa/OverwriteOption;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 28
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/microsoft/services/msa/OverwriteOption$1;)V
    .locals 0

    .line 28
    invoke-direct {p0, p1, p2}, Lcom/microsoft/services/msa/OverwriteOption;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/services/msa/OverwriteOption;
    .locals 1

    .line 28
    const-class v0, Lcom/microsoft/services/msa/OverwriteOption;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/microsoft/services/msa/OverwriteOption;

    return-object p0
.end method

.method public static values()[Lcom/microsoft/services/msa/OverwriteOption;
    .locals 1

    .line 28
    sget-object v0, Lcom/microsoft/services/msa/OverwriteOption;->$VALUES:[Lcom/microsoft/services/msa/OverwriteOption;

    invoke-virtual {v0}, [Lcom/microsoft/services/msa/OverwriteOption;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/services/msa/OverwriteOption;

    return-object v0
.end method


# virtual methods
.method appendQueryParameterOnTo(Lcom/microsoft/services/msa/UriBuilder;)V
    .locals 2

    const-string v0, "overwrite"

    .line 59
    invoke-virtual {p0}, Lcom/microsoft/services/msa/OverwriteOption;->overwriteQueryParamValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/microsoft/services/msa/UriBuilder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/services/msa/UriBuilder;

    return-void
.end method

.method protected abstract overwriteQueryParamValue()Ljava/lang/String;
.end method

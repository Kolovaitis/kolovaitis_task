.class final Lcom/microsoft/services/msa/QueryParameters;
.super Ljava/lang/Object;
.source "QueryParameters.java"


# static fields
.field public static final CALLBACK:Ljava/lang/String; = "callback"

.field public static final METHOD:Ljava/lang/String; = "method"

.field public static final OVERWRITE:Ljava/lang/String; = "overwrite"

.field public static final PRETTY:Ljava/lang/String; = "pretty"

.field public static final RETURN_SSL_RESOURCES:Ljava/lang/String; = "return_ssl_resources"

.field public static final SUPPRESS_REDIRECTS:Ljava/lang/String; = "suppress_redirects"

.field public static final SUPPRESS_RESPONSE_CODES:Ljava/lang/String; = "suppress_response_codes"


# direct methods
.method private constructor <init>()V
    .locals 2

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Non-instantiable class"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

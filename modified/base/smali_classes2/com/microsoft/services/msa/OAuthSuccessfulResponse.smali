.class Lcom/microsoft/services/msa/OAuthSuccessfulResponse;
.super Ljava/lang/Object;
.source "OAuthSuccessfulResponse.java"

# interfaces
.implements Lcom/microsoft/services/msa/OAuthResponse;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;
    }
.end annotation


# static fields
.field private static final UNINITIALIZED:I = -0x1


# instance fields
.field private final accessToken:Ljava/lang/String;

.field private final authenticationToken:Ljava/lang/String;

.field private final expiresIn:I

.field private final refreshToken:Ljava/lang/String;

.field private final scope:Ljava/lang/String;

.field private final tokenType:Lcom/microsoft/services/msa/OAuth$TokenType;


# direct methods
.method private constructor <init>(Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;)V
    .locals 1

    .line 253
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 254
    invoke-static {p1}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->access$100(Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->accessToken:Ljava/lang/String;

    .line 255
    invoke-static {p1}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->access$200(Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->authenticationToken:Ljava/lang/String;

    .line 256
    invoke-static {p1}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->access$300(Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;)Lcom/microsoft/services/msa/OAuth$TokenType;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->tokenType:Lcom/microsoft/services/msa/OAuth$TokenType;

    .line 257
    invoke-static {p1}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->access$400(Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->refreshToken:Ljava/lang/String;

    .line 258
    invoke-static {p1}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->access$500(Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;)I

    move-result v0

    iput v0, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->expiresIn:I

    .line 259
    invoke-static {p1}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->access$600(Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->scope:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;Lcom/microsoft/services/msa/OAuthSuccessfulResponse$1;)V
    .locals 0

    .line 36
    invoke-direct {p0, p1}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;-><init>(Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;)V

    return-void
.end method

.method public static createFromFragment(Ljava/util/Map;)Lcom/microsoft/services/msa/OAuthSuccessfulResponse;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/services/msa/OAuthSuccessfulResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/services/msa/LiveAuthException;
        }
    .end annotation

    const-string v0, "access_token"

    .line 93
    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "token_type"

    .line 94
    invoke-interface {p0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    if-eqz v0, :cond_4

    if-eqz v1, :cond_3

    .line 102
    :try_start_0
    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/services/msa/OAuth$TokenType;->valueOf(Ljava/lang/String;)Lcom/microsoft/services/msa/OAuth$TokenType;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    .line 107
    new-instance v2, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;

    invoke-direct {v2, v0, v1}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;-><init>(Ljava/lang/String;Lcom/microsoft/services/msa/OAuth$TokenType;)V

    const-string v0, "authentication_token"

    .line 110
    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 112
    invoke-virtual {v2, v0}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->authenticationToken(Ljava/lang/String;)Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;

    :cond_0
    const-string v0, "expires_in"

    .line 115
    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 119
    :try_start_1
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    .line 124
    invoke-virtual {v2, v0}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->expiresIn(I)Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;

    goto :goto_0

    :catch_0
    move-exception p0

    .line 121
    new-instance v0, Lcom/microsoft/services/msa/LiveAuthException;

    const-string v1, "An error occured while communicating with the server during the operation. Please try again later."

    invoke-direct {v0, v1, p0}, Lcom/microsoft/services/msa/LiveAuthException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    :cond_1
    :goto_0
    const-string v0, "scope"

    .line 127
    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    if-eqz p0, :cond_2

    .line 129
    invoke-virtual {v2, p0}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->scope(Ljava/lang/String;)Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;

    .line 132
    :cond_2
    invoke-virtual {v2}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->build()Lcom/microsoft/services/msa/OAuthSuccessfulResponse;

    move-result-object p0

    return-object p0

    :catch_1
    move-exception p0

    .line 104
    new-instance v0, Lcom/microsoft/services/msa/LiveAuthException;

    const-string v1, "An error occured while communicating with the server during the operation. Please try again later."

    invoke-direct {v0, v1, p0}, Lcom/microsoft/services/msa/LiveAuthException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 98
    :cond_3
    new-instance p0, Ljava/lang/AssertionError;

    invoke-direct {p0}, Ljava/lang/AssertionError;-><init>()V

    throw p0

    .line 97
    :cond_4
    new-instance p0, Ljava/lang/AssertionError;

    invoke-direct {p0}, Ljava/lang/AssertionError;-><init>()V

    throw p0
.end method

.method public static createFromJson(Lorg/json/JSONObject;)Lcom/microsoft/services/msa/OAuthSuccessfulResponse;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/services/msa/LiveAuthException;
        }
    .end annotation

    .line 145
    invoke-static {p0}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->validOAuthSuccessfulResponse(Lorg/json/JSONObject;)Z

    move-result v0

    if-eqz v0, :cond_4

    :try_start_0
    const-string v0, "access_token"

    .line 149
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_7

    :try_start_1
    const-string v1, "token_type"

    .line 156
    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_6

    .line 163
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/services/msa/OAuth$TokenType;->valueOf(Ljava/lang/String;)Lcom/microsoft/services/msa/OAuth$TokenType;

    move-result-object v1
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_4

    .line 170
    new-instance v2, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;

    invoke-direct {v2, v0, v1}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;-><init>(Ljava/lang/String;Lcom/microsoft/services/msa/OAuth$TokenType;)V

    const-string v0, "authentication_token"

    .line 172
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_3
    const-string v0, "authentication_token"

    .line 175
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0

    .line 179
    invoke-virtual {v2, v0}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->authenticationToken(Ljava/lang/String;)Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;

    goto :goto_0

    :catch_0
    move-exception p0

    .line 177
    new-instance v0, Lcom/microsoft/services/msa/LiveAuthException;

    const-string v1, "An error occured on the client during the operation."

    invoke-direct {v0, v1, p0}, Lcom/microsoft/services/msa/LiveAuthException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    :cond_0
    :goto_0
    const-string v0, "refresh_token"

    .line 182
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :try_start_4
    const-string v0, "refresh_token"

    .line 185
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_1

    .line 189
    invoke-virtual {v2, v0}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->refreshToken(Ljava/lang/String;)Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;

    goto :goto_1

    :catch_1
    move-exception p0

    .line 187
    new-instance v0, Lcom/microsoft/services/msa/LiveAuthException;

    const-string v1, "An error occured on the client during the operation."

    invoke-direct {v0, v1, p0}, Lcom/microsoft/services/msa/LiveAuthException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    :cond_1
    :goto_1
    const-string v0, "expires_in"

    .line 192
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :try_start_5
    const-string v0, "expires_in"

    .line 195
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_2

    .line 199
    invoke-virtual {v2, v0}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->expiresIn(I)Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;

    goto :goto_2

    :catch_2
    move-exception p0

    .line 197
    new-instance v0, Lcom/microsoft/services/msa/LiveAuthException;

    const-string v1, "An error occured on the client during the operation."

    invoke-direct {v0, v1, p0}, Lcom/microsoft/services/msa/LiveAuthException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    :cond_2
    :goto_2
    const-string v0, "scope"

    .line 202
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    :try_start_6
    const-string v0, "scope"

    .line 205
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0
    :try_end_6
    .catch Lorg/json/JSONException; {:try_start_6 .. :try_end_6} :catch_3

    .line 209
    invoke-virtual {v2, p0}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->scope(Ljava/lang/String;)Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;

    goto :goto_3

    :catch_3
    move-exception p0

    .line 207
    new-instance v0, Lcom/microsoft/services/msa/LiveAuthException;

    const-string v1, "An error occured on the client during the operation."

    invoke-direct {v0, v1, p0}, Lcom/microsoft/services/msa/LiveAuthException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 212
    :cond_3
    :goto_3
    invoke-virtual {v2}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->build()Lcom/microsoft/services/msa/OAuthSuccessfulResponse;

    move-result-object p0

    return-object p0

    :catch_4
    move-exception p0

    .line 167
    new-instance v0, Lcom/microsoft/services/msa/LiveAuthException;

    const-string v1, "An error occured while communicating with the server during the operation. Please try again later."

    invoke-direct {v0, v1, p0}, Lcom/microsoft/services/msa/LiveAuthException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    :catch_5
    move-exception p0

    .line 165
    new-instance v0, Lcom/microsoft/services/msa/LiveAuthException;

    const-string v1, "An error occured while communicating with the server during the operation. Please try again later."

    invoke-direct {v0, v1, p0}, Lcom/microsoft/services/msa/LiveAuthException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    :catch_6
    move-exception p0

    .line 158
    new-instance v0, Lcom/microsoft/services/msa/LiveAuthException;

    const-string v1, "An error occured while communicating with the server during the operation. Please try again later."

    invoke-direct {v0, v1, p0}, Lcom/microsoft/services/msa/LiveAuthException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    :catch_7
    move-exception p0

    .line 151
    new-instance v0, Lcom/microsoft/services/msa/LiveAuthException;

    const-string v1, "An error occured while communicating with the server during the operation. Please try again later."

    invoke-direct {v0, v1, p0}, Lcom/microsoft/services/msa/LiveAuthException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 145
    :cond_4
    new-instance p0, Ljava/lang/AssertionError;

    invoke-direct {p0}, Ljava/lang/AssertionError;-><init>()V

    throw p0
.end method

.method public static validOAuthSuccessfulResponse(Lorg/json/JSONObject;)Z
    .locals 1

    const-string v0, "access_token"

    .line 221
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "token_type"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method


# virtual methods
.method public accept(Lcom/microsoft/services/msa/OAuthResponseVisitor;)V
    .locals 0

    .line 264
    invoke-interface {p1, p0}, Lcom/microsoft/services/msa/OAuthResponseVisitor;->visit(Lcom/microsoft/services/msa/OAuthSuccessfulResponse;)V

    return-void
.end method

.method public getAccessToken()Ljava/lang/String;
    .locals 1

    .line 268
    iget-object v0, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->accessToken:Ljava/lang/String;

    return-object v0
.end method

.method public getAuthenticationToken()Ljava/lang/String;
    .locals 1

    .line 272
    iget-object v0, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->authenticationToken:Ljava/lang/String;

    return-object v0
.end method

.method public getExpiresIn()I
    .locals 1

    .line 276
    iget v0, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->expiresIn:I

    return v0
.end method

.method public getRefreshToken()Ljava/lang/String;
    .locals 1

    .line 280
    iget-object v0, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->refreshToken:Ljava/lang/String;

    return-object v0
.end method

.method public getScope()Ljava/lang/String;
    .locals 1

    .line 284
    iget-object v0, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->scope:Ljava/lang/String;

    return-object v0
.end method

.method public getTokenType()Lcom/microsoft/services/msa/OAuth$TokenType;
    .locals 1

    .line 288
    iget-object v0, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->tokenType:Lcom/microsoft/services/msa/OAuth$TokenType;

    return-object v0
.end method

.method public hasAuthenticationToken()Z
    .locals 1

    .line 292
    iget-object v0, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->authenticationToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasExpiresIn()Z
    .locals 2

    .line 296
    iget v0, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->expiresIn:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasRefreshToken()Z
    .locals 1

    .line 300
    iget-object v0, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->refreshToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasScope()Z
    .locals 1

    .line 304
    iget-object v0, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->scope:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    const-string v0, "OAuthSuccessfulResponse [accessToken=%s, authenticationToken=%s, tokenType=%s, refreshToken=%s, expiresIn=%s, scope=%s]"

    const/4 v1, 0x6

    .line 309
    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->accessToken:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->authenticationToken:Ljava/lang/String;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->tokenType:Lcom/microsoft/services/msa/OAuth$TokenType;

    const/4 v3, 0x2

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->refreshToken:Ljava/lang/String;

    const/4 v3, 0x3

    aput-object v2, v1, v3

    iget v2, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->expiresIn:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x4

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->scope:Ljava/lang/String;

    const/4 v3, 0x5

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

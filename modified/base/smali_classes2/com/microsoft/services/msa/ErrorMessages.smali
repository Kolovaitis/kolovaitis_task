.class final Lcom/microsoft/services/msa/ErrorMessages;
.super Ljava/lang/Object;
.source "ErrorMessages.java"


# static fields
.field public static final ABSOLUTE_PARAMETER:Ljava/lang/String; = "Input parameter \'%1$s\' is invalid. \'%1$s\' cannot be absolute."

.field public static final CLIENT_ERROR:Ljava/lang/String; = "An error occured on the client during the operation."

.field public static final EMPTY_PARAMETER:Ljava/lang/String; = "Input parameter \'%1$s\' is invalid. \'%1$s\' cannot be empty."

.field public static final INVALID_URI:Ljava/lang/String; = "Input parameter \'%1$s\' is invalid. \'%1$s\' must be a valid URI."

.field public static final LOGGED_OUT:Ljava/lang/String; = "The user has is logged out."

.field public static final LOGIN_IN_PROGRESS:Ljava/lang/String; = "Another login operation is already in progress."

.field public static final MISSING_UPLOAD_LOCATION:Ljava/lang/String; = "The provided path does not contain an upload_location."

.field public static final NON_INSTANTIABLE_CLASS:Ljava/lang/String; = "Non-instantiable class"

.field public static final NULL_PARAMETER:Ljava/lang/String; = "Input parameter \'%1$s\' is invalid. \'%1$s\' cannot be null."

.field public static final SERVER_ERROR:Ljava/lang/String; = "An error occured while communicating with the server during the operation. Please try again later."

.field public static final SIGNIN_CANCEL:Ljava/lang/String; = "The user cancelled the login operation."


# direct methods
.method private constructor <init>()V
    .locals 2

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Non-instantiable class"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

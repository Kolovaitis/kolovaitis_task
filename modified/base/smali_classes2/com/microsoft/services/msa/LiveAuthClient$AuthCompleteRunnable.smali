.class Lcom/microsoft/services/msa/LiveAuthClient$AuthCompleteRunnable;
.super Lcom/microsoft/services/msa/LiveAuthClient$AuthListenerCaller;
.source "LiveAuthClient.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/services/msa/LiveAuthClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AuthCompleteRunnable"
.end annotation


# instance fields
.field private final session:Lcom/microsoft/services/msa/LiveConnectSession;

.field private final status:Lcom/microsoft/services/msa/LiveStatus;


# direct methods
.method public constructor <init>(Lcom/microsoft/services/msa/LiveAuthListener;Ljava/lang/Object;Lcom/microsoft/services/msa/LiveStatus;Lcom/microsoft/services/msa/LiveConnectSession;)V
    .locals 0

    .line 65
    invoke-direct {p0, p1, p2}, Lcom/microsoft/services/msa/LiveAuthClient$AuthListenerCaller;-><init>(Lcom/microsoft/services/msa/LiveAuthListener;Ljava/lang/Object;)V

    .line 66
    iput-object p3, p0, Lcom/microsoft/services/msa/LiveAuthClient$AuthCompleteRunnable;->status:Lcom/microsoft/services/msa/LiveStatus;

    .line 67
    iput-object p4, p0, Lcom/microsoft/services/msa/LiveAuthClient$AuthCompleteRunnable;->session:Lcom/microsoft/services/msa/LiveConnectSession;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 72
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveAuthClient$AuthCompleteRunnable;->listener:Lcom/microsoft/services/msa/LiveAuthListener;

    iget-object v1, p0, Lcom/microsoft/services/msa/LiveAuthClient$AuthCompleteRunnable;->status:Lcom/microsoft/services/msa/LiveStatus;

    iget-object v2, p0, Lcom/microsoft/services/msa/LiveAuthClient$AuthCompleteRunnable;->session:Lcom/microsoft/services/msa/LiveConnectSession;

    iget-object v3, p0, Lcom/microsoft/services/msa/LiveAuthClient$AuthCompleteRunnable;->userState:Ljava/lang/Object;

    invoke-interface {v0, v1, v2, v3}, Lcom/microsoft/services/msa/LiveAuthListener;->onAuthComplete(Lcom/microsoft/services/msa/LiveStatus;Lcom/microsoft/services/msa/LiveConnectSession;Ljava/lang/Object;)V

    return-void
.end method

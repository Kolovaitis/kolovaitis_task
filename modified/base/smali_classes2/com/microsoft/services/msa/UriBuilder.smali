.class Lcom/microsoft/services/msa/UriBuilder;
.super Ljava/lang/Object;
.source "UriBuilder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/services/msa/UriBuilder$QueryParameter;
    }
.end annotation


# static fields
.field private static final AMPERSAND:Ljava/lang/String; = "&"

.field private static final EQUAL:Ljava/lang/String; = "="

.field private static final FORWARD_SLASH:C = '/'


# instance fields
.field private host:Ljava/lang/String;

.field private path:Ljava/lang/StringBuilder;

.field private final queryParameters:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList<",
            "Lcom/microsoft/services/msa/UriBuilder$QueryParameter;",
            ">;"
        }
    .end annotation
.end field

.field private scheme:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/services/msa/UriBuilder;->queryParameters:Ljava/util/LinkedList;

    return-void
.end method

.method public static newInstance(Landroid/net/Uri;)Lcom/microsoft/services/msa/UriBuilder;
    .locals 2

    .line 101
    new-instance v0, Lcom/microsoft/services/msa/UriBuilder;

    invoke-direct {v0}, Lcom/microsoft/services/msa/UriBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/services/msa/UriBuilder;->scheme(Ljava/lang/String;)Lcom/microsoft/services/msa/UriBuilder;

    move-result-object v0

    invoke-virtual {p0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/services/msa/UriBuilder;->host(Ljava/lang/String;)Lcom/microsoft/services/msa/UriBuilder;

    move-result-object v0

    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/services/msa/UriBuilder;->path(Ljava/lang/String;)Lcom/microsoft/services/msa/UriBuilder;

    move-result-object v0

    invoke-virtual {p0}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/microsoft/services/msa/UriBuilder;->query(Ljava/lang/String;)Lcom/microsoft/services/msa/UriBuilder;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/services/msa/UriBuilder;
    .locals 2

    if-eqz p1, :cond_1

    if-eqz p2, :cond_0

    .line 125
    iget-object v0, p0, Lcom/microsoft/services/msa/UriBuilder;->queryParameters:Ljava/util/LinkedList;

    new-instance v1, Lcom/microsoft/services/msa/UriBuilder$QueryParameter;

    invoke-direct {v1, p1, p2}, Lcom/microsoft/services/msa/UriBuilder$QueryParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    return-object p0

    .line 123
    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1

    .line 122
    :cond_1
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1
.end method

.method public appendQueryString(Ljava/lang/String;)Lcom/microsoft/services/msa/UriBuilder;
    .locals 8

    if-nez p1, :cond_0

    return-object p0

    :cond_0
    const-string v0, "&"

    .line 144
    invoke-static {p1, v0}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    .line 145
    array-length v0, p1

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_3

    aget-object v3, p1, v2

    const-string v4, "="

    .line 146
    invoke-static {v3, v4}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 147
    array-length v5, v4

    const/4 v6, 0x2

    const/4 v7, 0x1

    if-ne v5, v6, :cond_1

    .line 148
    aget-object v3, v4, v1

    .line 149
    aget-object v4, v4, v7

    .line 151
    iget-object v5, p0, Lcom/microsoft/services/msa/UriBuilder;->queryParameters:Ljava/util/LinkedList;

    new-instance v6, Lcom/microsoft/services/msa/UriBuilder$QueryParameter;

    invoke-direct {v6, v3, v4}, Lcom/microsoft/services/msa/UriBuilder$QueryParameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 152
    :cond_1
    array-length v5, v4

    if-ne v5, v7, :cond_2

    .line 153
    aget-object v3, v4, v1

    .line 155
    iget-object v4, p0, Lcom/microsoft/services/msa/UriBuilder;->queryParameters:Ljava/util/LinkedList;

    new-instance v5, Lcom/microsoft/services/msa/UriBuilder$QueryParameter;

    invoke-direct {v5, v3}, Lcom/microsoft/services/msa/UriBuilder$QueryParameter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    const-string v4, "live.auth.UriBuilder"

    .line 157
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Invalid query parameter: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    return-object p0
.end method

.method public appendToPath(Ljava/lang/String;)Lcom/microsoft/services/msa/UriBuilder;
    .locals 6

    if-eqz p1, :cond_8

    .line 173
    iget-object v0, p0, Lcom/microsoft/services/msa/UriBuilder;->path:Ljava/lang/StringBuilder;

    if-nez v0, :cond_0

    .line 174
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/microsoft/services/msa/UriBuilder;->path:Ljava/lang/StringBuilder;

    goto :goto_2

    .line 176
    :cond_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/16 v1, 0x2f

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/microsoft/services/msa/UriBuilder;->path:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    sub-int/2addr v4, v3

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v0

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    .line 178
    :goto_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    goto :goto_1

    .line 179
    :cond_3
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-ne v5, v1, :cond_4

    const/4 v2, 0x1

    :cond_4
    :goto_1
    if-eqz v0, :cond_5

    if-eqz v2, :cond_5

    .line 183
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v3, :cond_7

    .line 184
    iget-object v0, p0, Lcom/microsoft/services/msa/UriBuilder;->path:Ljava/lang/StringBuilder;

    invoke-virtual {p1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_5
    if-nez v0, :cond_6

    if-nez v2, :cond_6

    if-nez v4, :cond_7

    .line 189
    iget-object v0, p0, Lcom/microsoft/services/msa/UriBuilder;->path:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 192
    :cond_6
    iget-object v0, p0, Lcom/microsoft/services/msa/UriBuilder;->path:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_7
    :goto_2
    return-object p0

    .line 171
    :cond_8
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1
.end method

.method public build()Landroid/net/Uri;
    .locals 3

    .line 205
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    iget-object v1, p0, Lcom/microsoft/services/msa/UriBuilder;->scheme:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/services/msa/UriBuilder;->host:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/services/msa/UriBuilder;->path:Ljava/lang/StringBuilder;

    if-nez v1, :cond_0

    const-string v1, ""

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "&"

    iget-object v2, p0, Lcom/microsoft/services/msa/UriBuilder;->queryParameters:Ljava/util/LinkedList;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->encodedQuery(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public host(Ljava/lang/String;)Lcom/microsoft/services/msa/UriBuilder;
    .locals 0

    if-eqz p1, :cond_0

    .line 219
    iput-object p1, p0, Lcom/microsoft/services/msa/UriBuilder;->host:Ljava/lang/String;

    return-object p0

    .line 218
    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1
.end method

.method public path(Ljava/lang/String;)Lcom/microsoft/services/msa/UriBuilder;
    .locals 1

    if-eqz p1, :cond_0

    .line 232
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/microsoft/services/msa/UriBuilder;->path:Ljava/lang/StringBuilder;

    return-object p0

    .line 231
    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1
.end method

.method public query(Ljava/lang/String;)Lcom/microsoft/services/msa/UriBuilder;
    .locals 1

    .line 245
    iget-object v0, p0, Lcom/microsoft/services/msa/UriBuilder;->queryParameters:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 247
    invoke-virtual {p0, p1}, Lcom/microsoft/services/msa/UriBuilder;->appendQueryString(Ljava/lang/String;)Lcom/microsoft/services/msa/UriBuilder;

    move-result-object p1

    return-object p1
.end method

.method public removeQueryParametersWithKey(Ljava/lang/String;)Lcom/microsoft/services/msa/UriBuilder;
    .locals 2

    .line 262
    iget-object v0, p0, Lcom/microsoft/services/msa/UriBuilder;->queryParameters:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 264
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 265
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/services/msa/UriBuilder$QueryParameter;

    .line 266
    invoke-virtual {v1}, Lcom/microsoft/services/msa/UriBuilder$QueryParameter;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 267
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    return-object p0
.end method

.method public scheme(Ljava/lang/String;)Lcom/microsoft/services/msa/UriBuilder;
    .locals 0

    if-eqz p1, :cond_0

    .line 281
    iput-object p1, p0, Lcom/microsoft/services/msa/UriBuilder;->scheme:Ljava/lang/String;

    return-object p0

    .line 280
    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 291
    invoke-virtual {p0}, Lcom/microsoft/services/msa/UriBuilder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

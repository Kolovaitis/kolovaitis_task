.class Lcom/microsoft/services/msa/LiveAuthClient$SessionRefresher;
.super Ljava/lang/Object;
.source "LiveAuthClient.java"

# interfaces
.implements Lcom/microsoft/services/msa/OAuthResponseVisitor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/services/msa/LiveAuthClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SessionRefresher"
.end annotation


# instance fields
.field private final session:Lcom/microsoft/services/msa/LiveConnectSession;

.field private visitedSuccessfulResponse:Z


# direct methods
.method public constructor <init>(Lcom/microsoft/services/msa/LiveConnectSession;)V
    .locals 0

    .line 197
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_0

    .line 200
    iput-object p1, p0, Lcom/microsoft/services/msa/LiveAuthClient$SessionRefresher;->session:Lcom/microsoft/services/msa/LiveConnectSession;

    const/4 p1, 0x0

    .line 201
    iput-boolean p1, p0, Lcom/microsoft/services/msa/LiveAuthClient$SessionRefresher;->visitedSuccessfulResponse:Z

    return-void

    .line 198
    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1
.end method


# virtual methods
.method public visit(Lcom/microsoft/services/msa/OAuthErrorResponse;)V
    .locals 0

    const/4 p1, 0x0

    .line 206
    iput-boolean p1, p0, Lcom/microsoft/services/msa/LiveAuthClient$SessionRefresher;->visitedSuccessfulResponse:Z

    return-void
.end method

.method public visit(Lcom/microsoft/services/msa/OAuthSuccessfulResponse;)V
    .locals 1

    .line 211
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveAuthClient$SessionRefresher;->session:Lcom/microsoft/services/msa/LiveConnectSession;

    invoke-virtual {v0, p1}, Lcom/microsoft/services/msa/LiveConnectSession;->loadFromOAuthResponse(Lcom/microsoft/services/msa/OAuthSuccessfulResponse;)V

    const/4 p1, 0x1

    .line 212
    iput-boolean p1, p0, Lcom/microsoft/services/msa/LiveAuthClient$SessionRefresher;->visitedSuccessfulResponse:Z

    return-void
.end method

.method public visitedSuccessfulResponse()Z
    .locals 1

    .line 216
    iget-boolean v0, p0, Lcom/microsoft/services/msa/LiveAuthClient$SessionRefresher;->visitedSuccessfulResponse:Z

    return v0
.end method

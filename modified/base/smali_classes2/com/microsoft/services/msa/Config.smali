.class final enum Lcom/microsoft/services/msa/Config;
.super Ljava/lang/Enum;
.source "Config.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/microsoft/services/msa/Config;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/services/msa/Config;

.field static final synthetic $assertionsDisabled:Z

.field public static final enum INSTANCE:Lcom/microsoft/services/msa/Config;


# instance fields
.field private apiUri:Landroid/net/Uri;

.field private apiVersion:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 32
    new-instance v0, Lcom/microsoft/services/msa/Config;

    const-string v1, "INSTANCE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/microsoft/services/msa/Config;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/services/msa/Config;->INSTANCE:Lcom/microsoft/services/msa/Config;

    const/4 v0, 0x1

    .line 31
    new-array v0, v0, [Lcom/microsoft/services/msa/Config;

    sget-object v1, Lcom/microsoft/services/msa/Config;->INSTANCE:Lcom/microsoft/services/msa/Config;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/services/msa/Config;->$VALUES:[Lcom/microsoft/services/msa/Config;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    const-string p1, "https://apis.live.net/v5.0"

    .line 39
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    iput-object p1, p0, Lcom/microsoft/services/msa/Config;->apiUri:Landroid/net/Uri;

    const-string p1, "5.0"

    .line 40
    iput-object p1, p0, Lcom/microsoft/services/msa/Config;->apiVersion:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/services/msa/Config;
    .locals 1

    .line 31
    const-class v0, Lcom/microsoft/services/msa/Config;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/microsoft/services/msa/Config;

    return-object p0
.end method

.method public static values()[Lcom/microsoft/services/msa/Config;
    .locals 1

    .line 31
    sget-object v0, Lcom/microsoft/services/msa/Config;->$VALUES:[Lcom/microsoft/services/msa/Config;

    invoke-virtual {v0}, [Lcom/microsoft/services/msa/Config;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/services/msa/Config;

    return-object v0
.end method


# virtual methods
.method public getApiUri()Landroid/net/Uri;
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/microsoft/services/msa/Config;->apiUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getApiVersion()Ljava/lang/String;
    .locals 1

    .line 48
    iget-object v0, p0, Lcom/microsoft/services/msa/Config;->apiVersion:Ljava/lang/String;

    return-object v0
.end method

.method public setApiUri(Landroid/net/Uri;)V
    .locals 0

    .line 53
    iput-object p1, p0, Lcom/microsoft/services/msa/Config;->apiUri:Landroid/net/Uri;

    return-void
.end method

.method public setApiVersion(Ljava/lang/String;)V
    .locals 1

    .line 57
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 58
    iput-object p1, p0, Lcom/microsoft/services/msa/Config;->apiVersion:Ljava/lang/String;

    return-void

    .line 57
    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1
.end method

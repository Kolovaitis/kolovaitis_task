.class public Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;
.super Ljava/lang/Object;
.source "OAuthSuccessfulResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/services/msa/OAuthSuccessfulResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private final accessToken:Ljava/lang/String;

.field private authenticationToken:Ljava/lang/String;

.field private expiresIn:I

.field private refreshToken:Ljava/lang/String;

.field private scope:Ljava/lang/String;

.field private final tokenType:Lcom/microsoft/services/msa/OAuth$TokenType;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/microsoft/services/msa/OAuth$TokenType;)V
    .locals 1

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    .line 45
    iput v0, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->expiresIn:I

    if-eqz p1, :cond_2

    .line 52
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz p2, :cond_0

    .line 55
    iput-object p1, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->accessToken:Ljava/lang/String;

    .line 56
    iput-object p2, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->tokenType:Lcom/microsoft/services/msa/OAuth$TokenType;

    return-void

    .line 53
    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1

    .line 52
    :cond_1
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1

    .line 51
    :cond_2
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1
.end method

.method static synthetic access$100(Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;)Ljava/lang/String;
    .locals 0

    .line 42
    iget-object p0, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->accessToken:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$200(Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;)Ljava/lang/String;
    .locals 0

    .line 42
    iget-object p0, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->authenticationToken:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$300(Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;)Lcom/microsoft/services/msa/OAuth$TokenType;
    .locals 0

    .line 42
    iget-object p0, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->tokenType:Lcom/microsoft/services/msa/OAuth$TokenType;

    return-object p0
.end method

.method static synthetic access$400(Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;)Ljava/lang/String;
    .locals 0

    .line 42
    iget-object p0, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->refreshToken:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$500(Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;)I
    .locals 0

    .line 42
    iget p0, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->expiresIn:I

    return p0
.end method

.method static synthetic access$600(Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;)Ljava/lang/String;
    .locals 0

    .line 42
    iget-object p0, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->scope:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public authenticationToken(Ljava/lang/String;)Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;
    .locals 0

    .line 60
    iput-object p1, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->authenticationToken:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/microsoft/services/msa/OAuthSuccessfulResponse;
    .locals 2

    .line 69
    new-instance v0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;-><init>(Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;Lcom/microsoft/services/msa/OAuthSuccessfulResponse$1;)V

    return-object v0
.end method

.method public expiresIn(I)Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;
    .locals 0

    .line 73
    iput p1, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->expiresIn:I

    return-object p0
.end method

.method public refreshToken(Ljava/lang/String;)Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;
    .locals 0

    .line 78
    iput-object p1, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->refreshToken:Ljava/lang/String;

    return-object p0
.end method

.method public scope(Ljava/lang/String;)Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;
    .locals 0

    .line 83
    iput-object p1, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->scope:Ljava/lang/String;

    return-object p0
.end method

.class Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$InternalHandler;
.super Landroid/os/Handler;
.source "AsyncTaskCompat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "InternalHandler"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 707
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$1;)V
    .locals 0

    .line 707
    invoke-direct {p0}, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$InternalHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2

    .line 711
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$AsyncTaskResult;

    .line 712
    iget p1, p1, Landroid/os/Message;->what:I

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 718
    :pswitch_0
    iget-object p1, v0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$AsyncTaskResult;->mTask:Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;

    iget-object v0, v0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$AsyncTaskResult;->mData:[Ljava/lang/Object;

    invoke-virtual {p1, v0}, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->onProgressUpdate([Ljava/lang/Object;)V

    goto :goto_0

    .line 715
    :pswitch_1
    iget-object p1, v0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$AsyncTaskResult;->mTask:Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;

    iget-object v0, v0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$AsyncTaskResult;->mData:[Ljava/lang/Object;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-static {p1, v0}, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->access$600(Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;Ljava/lang/Object;)V

    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

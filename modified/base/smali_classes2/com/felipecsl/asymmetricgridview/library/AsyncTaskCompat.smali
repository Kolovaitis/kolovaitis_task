.class public abstract Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;
.super Ljava/lang/Object;
.source "AsyncTaskCompat.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$AsyncTaskResult;,
        Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$WorkerRunnable;,
        Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$InternalHandler;,
        Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;,
        Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$SerialExecutor;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Params:",
        "Ljava/lang/Object;",
        "Progress:",
        "Ljava/lang/Object;",
        "Result:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final CORE_POOL_SIZE:I

.field private static final CPU_COUNT:I

.field private static final KEEP_ALIVE:I = 0x1

.field private static final LOG_TAG:Ljava/lang/String; = "AsyncTaskCompat"

.field private static final MAXIMUM_POOL_SIZE:I

.field private static final MESSAGE_POST_PROGRESS:I = 0x2

.field private static final MESSAGE_POST_RESULT:I = 0x1

.field public static final SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

.field public static final THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

.field private static volatile sDefaultExecutor:Ljava/util/concurrent/Executor;

.field private static final sHandler:Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$InternalHandler;

.field private static final sPoolWorkQueue:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private static final sThreadFactory:Ljava/util/concurrent/ThreadFactory;


# instance fields
.field private final mCancelled:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final mFuture:Ljava/util/concurrent/FutureTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/FutureTask<",
            "TResult;>;"
        }
    .end annotation
.end field

.field private volatile mStatus:Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;

.field private final mTaskInvoked:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final mWorker:Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$WorkerRunnable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$WorkerRunnable<",
            "TParams;TResult;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .line 186
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v0

    sput v0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->CPU_COUNT:I

    .line 187
    sget v0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->CPU_COUNT:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->CORE_POOL_SIZE:I

    mul-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x1

    .line 188
    sput v0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->MAXIMUM_POOL_SIZE:I

    .line 191
    new-instance v0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$1;

    invoke-direct {v0}, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$1;-><init>()V

    sput-object v0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->sThreadFactory:Ljava/util/concurrent/ThreadFactory;

    .line 199
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>(I)V

    sput-object v0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->sPoolWorkQueue:Ljava/util/concurrent/BlockingQueue;

    .line 205
    new-instance v0, Ljava/util/concurrent/ThreadPoolExecutor;

    sget v3, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->CORE_POOL_SIZE:I

    sget v4, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->MAXIMUM_POOL_SIZE:I

    sget-object v7, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v8, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->sPoolWorkQueue:Ljava/util/concurrent/BlockingQueue;

    sget-object v9, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->sThreadFactory:Ljava/util/concurrent/ThreadFactory;

    const-wide/16 v5, 0x1

    move-object v2, v0

    invoke-direct/range {v2 .. v9}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    sput-object v0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    .line 213
    new-instance v0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$SerialExecutor;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$SerialExecutor;-><init>(Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$1;)V

    sput-object v0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    .line 218
    new-instance v0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$InternalHandler;

    invoke-direct {v0, v1}, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$InternalHandler;-><init>(Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$1;)V

    sput-object v0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->sHandler:Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$InternalHandler;

    .line 220
    sget-object v0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    sput-object v0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->sDefaultExecutor:Ljava/util/concurrent/Executor;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 288
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 224
    sget-object v0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;->PENDING:Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;

    iput-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->mStatus:Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;

    .line 226
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->mCancelled:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 227
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->mTaskInvoked:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 289
    new-instance v0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$2;

    invoke-direct {v0, p0}, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$2;-><init>(Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;)V

    iput-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->mWorker:Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$WorkerRunnable;

    .line 299
    new-instance v0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$3;

    iget-object v1, p0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->mWorker:Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$WorkerRunnable;

    invoke-direct {v0, p0, v1}, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$3;-><init>(Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;Ljava/util/concurrent/Callable;)V

    iput-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->mFuture:Ljava/util/concurrent/FutureTask;

    return-void
.end method

.method static synthetic access$300(Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 0

    .line 183
    iget-object p0, p0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->mTaskInvoked:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object p0
.end method

.method static synthetic access$400(Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 183
    invoke-direct {p0, p1}, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->postResult(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$500(Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;Ljava/lang/Object;)V
    .locals 0

    .line 183
    invoke-direct {p0, p1}, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->postResultIfNotInvoked(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$600(Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;Ljava/lang/Object;)V
    .locals 0

    .line 183
    invoke-direct {p0, p1}, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->finish(Ljava/lang/Object;)V

    return-void
.end method

.method public static execute(Ljava/lang/Runnable;)V
    .locals 1

    .line 674
    sget-object v0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->sDefaultExecutor:Ljava/util/concurrent/Executor;

    invoke-interface {v0, p0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private finish(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TResult;)V"
        }
    .end annotation

    .line 699
    invoke-virtual {p0}, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 700
    invoke-virtual {p0, p1}, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->onCancelled(Ljava/lang/Object;)V

    goto :goto_0

    .line 702
    :cond_0
    invoke-virtual {p0, p1}, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->onPostExecute(Ljava/lang/Object;)V

    .line 704
    :goto_0
    sget-object p1, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;->FINISHED:Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;

    iput-object p1, p0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->mStatus:Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;

    return-void
.end method

.method public static init()V
    .locals 1

    .line 277
    sget-object v0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->sHandler:Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$InternalHandler;

    invoke-virtual {v0}, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$InternalHandler;->getLooper()Landroid/os/Looper;

    return-void
.end method

.method private postResult(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TResult;)TResult;"
        }
    .end annotation

    .line 325
    sget-object v0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->sHandler:Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$InternalHandler;

    new-instance v1, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$AsyncTaskResult;

    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-direct {v1, p0, v3}, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$AsyncTaskResult;-><init>(Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;[Ljava/lang/Object;)V

    invoke-virtual {v0, v2, v1}, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$InternalHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 327
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-object p1
.end method

.method private postResultIfNotInvoked(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TResult;)V"
        }
    .end annotation

    .line 317
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->mTaskInvoked:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    .line 319
    invoke-direct {p0, p1}, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->postResult(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public static setDefaultExecutor(Ljava/util/concurrent/Executor;)V
    .locals 0

    .line 282
    sput-object p0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->sDefaultExecutor:Ljava/util/concurrent/Executor;

    return-void
.end method


# virtual methods
.method public final cancel(Z)Z
    .locals 2

    .line 474
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->mCancelled:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 475
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->mFuture:Ljava/util/concurrent/FutureTask;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/FutureTask;->cancel(Z)Z

    move-result p1

    return p1
.end method

.method protected varargs abstract doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TParams;)TResult;"
        }
    .end annotation
.end method

.method public final varargs execute([Ljava/lang/Object;)Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TParams;)",
            "Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat<",
            "TParams;TProgress;TResult;>;"
        }
    .end annotation

    .line 544
    sget-object v0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->sDefaultExecutor:Ljava/util/concurrent/Executor;

    invoke-virtual {p0, v0, p1}, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;

    move-result-object p1

    return-object p1
.end method

.method public final varargs executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Executor;",
            "[TParams;)",
            "Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat<",
            "TParams;TProgress;TResult;>;"
        }
    .end annotation

    .line 643
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->mStatus:Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;

    sget-object v1, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;->PENDING:Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;

    if-eq v0, v1, :cond_0

    .line 644
    sget-object v0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$4;->$SwitchMap$com$felipecsl$asymmetricgridview$library$AsyncTaskCompat$Status:[I

    iget-object v1, p0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->mStatus:Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;

    invoke-virtual {v1}, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 649
    :pswitch_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Cannot execute task: the task has already been executed (a task can be executed only once)"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 646
    :pswitch_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Cannot execute task: the task is already running."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 655
    :cond_0
    :goto_0
    sget-object v0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;->RUNNING:Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;

    iput-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->mStatus:Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;

    .line 657
    invoke-virtual {p0}, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->onPreExecute()V

    .line 659
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->mWorker:Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$WorkerRunnable;

    iput-object p2, v0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$WorkerRunnable;->mParams:[Ljava/lang/Object;

    .line 660
    iget-object p2, p0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->mFuture:Ljava/util/concurrent/FutureTask;

    invoke-interface {p1, p2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final varargs executeParallely([Ljava/lang/Object;)Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TParams;)",
            "Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat<",
            "TParams;TProgress;TResult;>;"
        }
    .end annotation

    .line 603
    sget-object v0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    invoke-virtual {p0, v0, p1}, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;

    move-result-object p1

    return-object p1
.end method

.method public final varargs executeSerially([Ljava/lang/Object;)Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TParams;)",
            "Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat<",
            "TParams;TProgress;TResult;>;"
        }
    .end annotation

    .line 568
    sget-object v0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    invoke-virtual {p0, v0, p1}, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;

    move-result-object p1

    return-object p1
.end method

.method public final get()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/util/concurrent/ExecutionException;
        }
    .end annotation

    .line 490
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->mFuture:Ljava/util/concurrent/FutureTask;

    invoke-virtual {v0}, Ljava/util/concurrent/FutureTask;->get()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/concurrent/TimeUnit;",
            ")TResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/util/concurrent/ExecutionException;,
            Ljava/util/concurrent/TimeoutException;
        }
    .end annotation

    .line 510
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->mFuture:Ljava/util/concurrent/FutureTask;

    invoke-virtual {v0, p1, p2, p3}, Ljava/util/concurrent/FutureTask;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final getStatus()Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;
    .locals 1

    .line 337
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->mStatus:Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;

    return-object v0
.end method

.method public final isCancelled()Z
    .locals 1

    .line 441
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->mCancelled:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method protected onCancelled()V
    .locals 0

    return-void
.end method

.method protected onCancelled(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TResult;)V"
        }
    .end annotation

    .line 412
    invoke-virtual {p0}, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->onCancelled()V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TResult;)V"
        }
    .end annotation

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TProgress;)V"
        }
    .end annotation

    return-void
.end method

.method protected final varargs publishProgress([Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TProgress;)V"
        }
    .end annotation

    .line 692
    invoke-virtual {p0}, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 693
    sget-object v0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;->sHandler:Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$InternalHandler;

    const/4 v1, 0x2

    new-instance v2, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$AsyncTaskResult;

    invoke-direct {v2, p0, p1}, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$AsyncTaskResult;-><init>(Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1, v2}, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$InternalHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    :cond_0
    return-void
.end method

.class public final enum Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;
.super Ljava/lang/Enum;
.source "AsyncTaskCompat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Status"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;

.field public static final enum FINISHED:Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;

.field public static final enum PENDING:Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;

.field public static final enum RUNNING:Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 264
    new-instance v0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;

    const-string v1, "PENDING"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;->PENDING:Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;

    .line 268
    new-instance v0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;

    const-string v1, "RUNNING"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;->RUNNING:Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;

    .line 272
    new-instance v0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;

    const-string v1, "FINISHED"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;->FINISHED:Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;

    const/4 v0, 0x3

    .line 260
    new-array v0, v0, [Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;

    sget-object v1, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;->PENDING:Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;

    aput-object v1, v0, v2

    sget-object v1, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;->RUNNING:Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;

    aput-object v1, v0, v3

    sget-object v1, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;->FINISHED:Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;

    aput-object v1, v0, v4

    sput-object v0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;->$VALUES:[Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 260
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;
    .locals 1

    .line 260
    const-class v0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;

    return-object p0
.end method

.method public static values()[Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;
    .locals 1

    .line 260
    sget-object v0, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;->$VALUES:[Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;

    invoke-virtual {v0}, [Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat$Status;

    return-object v0
.end method

.class Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$1;
.super Ljava/lang/Object;
.source "AsymmetricGridView.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;


# direct methods
.method constructor <init>(Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;)V
    .locals 0

    .line 37
    iput-object p1, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$1;->this$0:Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$1;->this$0:Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;

    invoke-virtual {v0}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 40
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$1;->this$0:Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;

    invoke-virtual {v0}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->determineColumns()I

    .line 41
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$1;->this$0:Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;

    iget-object v0, v0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->gridAdapter:Lcom/felipecsl/asymmetricgridview/library/AsymmetricGridViewAdapterContract;

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$1;->this$0:Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;

    iget-object v0, v0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->gridAdapter:Lcom/felipecsl/asymmetricgridview/library/AsymmetricGridViewAdapterContract;

    invoke-interface {v0}, Lcom/felipecsl/asymmetricgridview/library/AsymmetricGridViewAdapterContract;->recalculateItemsPerRow()V

    :cond_0
    return-void
.end method

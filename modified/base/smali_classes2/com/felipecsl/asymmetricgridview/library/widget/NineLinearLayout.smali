.class public Lcom/felipecsl/asymmetricgridview/library/widget/NineLinearLayout;
.super Landroid/widget/LinearLayout;
.source "NineLinearLayout.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# instance fields
.field private final mProxy:Lcom/felipecsl/asymmetricgridview/library/widget/AnimatorProxy;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 14
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 15
    sget-boolean p1, Lcom/felipecsl/asymmetricgridview/library/widget/AnimatorProxy;->NEEDS_PROXY:Z

    if-eqz p1, :cond_0

    invoke-static {p0}, Lcom/felipecsl/asymmetricgridview/library/widget/AnimatorProxy;->wrap(Landroid/view/View;)Lcom/felipecsl/asymmetricgridview/library/widget/AnimatorProxy;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-object p1, p0, Lcom/felipecsl/asymmetricgridview/library/widget/NineLinearLayout;->mProxy:Lcom/felipecsl/asymmetricgridview/library/widget/AnimatorProxy;

    return-void
.end method


# virtual methods
.method public getAlpha()F
    .locals 1

    .line 31
    sget-boolean v0, Lcom/felipecsl/asymmetricgridview/library/widget/AnimatorProxy;->NEEDS_PROXY:Z

    if-eqz v0, :cond_0

    .line 32
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/NineLinearLayout;->mProxy:Lcom/felipecsl/asymmetricgridview/library/widget/AnimatorProxy;

    invoke-virtual {v0}, Lcom/felipecsl/asymmetricgridview/library/widget/AnimatorProxy;->getAlpha()F

    move-result v0

    return v0

    .line 34
    :cond_0
    invoke-super {p0}, Landroid/widget/LinearLayout;->getAlpha()F

    move-result v0

    return v0
.end method

.method public getTranslationX()F
    .locals 1

    .line 47
    sget-boolean v0, Lcom/felipecsl/asymmetricgridview/library/widget/AnimatorProxy;->NEEDS_PROXY:Z

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/NineLinearLayout;->mProxy:Lcom/felipecsl/asymmetricgridview/library/widget/AnimatorProxy;

    invoke-virtual {v0}, Lcom/felipecsl/asymmetricgridview/library/widget/AnimatorProxy;->getTranslationX()F

    move-result v0

    return v0

    .line 50
    :cond_0
    invoke-super {p0}, Landroid/widget/LinearLayout;->getTranslationX()F

    move-result v0

    return v0
.end method

.method public setAlpha(F)V
    .locals 1

    .line 39
    sget-boolean v0, Lcom/felipecsl/asymmetricgridview/library/widget/AnimatorProxy;->NEEDS_PROXY:Z

    if-eqz v0, :cond_0

    .line 40
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/NineLinearLayout;->mProxy:Lcom/felipecsl/asymmetricgridview/library/widget/AnimatorProxy;

    invoke-virtual {v0, p1}, Lcom/felipecsl/asymmetricgridview/library/widget/AnimatorProxy;->setAlpha(F)V

    goto :goto_0

    .line 42
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setAlpha(F)V

    :goto_0
    return-void
.end method

.method public setTranslationX(F)V
    .locals 1

    .line 55
    sget-boolean v0, Lcom/felipecsl/asymmetricgridview/library/widget/AnimatorProxy;->NEEDS_PROXY:Z

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/NineLinearLayout;->mProxy:Lcom/felipecsl/asymmetricgridview/library/widget/AnimatorProxy;

    invoke-virtual {v0, p1}, Lcom/felipecsl/asymmetricgridview/library/widget/AnimatorProxy;->setTranslationX(F)V

    goto :goto_0

    .line 58
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setTranslationX(F)V

    :goto_0
    return-void
.end method

.method public setVisibility(I)V
    .locals 2

    .line 20
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/NineLinearLayout;->mProxy:Lcom/felipecsl/asymmetricgridview/library/widget/AnimatorProxy;

    if-eqz v0, :cond_1

    const/16 v1, 0x8

    if-ne p1, v1, :cond_0

    .line 22
    invoke-virtual {p0}, Lcom/felipecsl/asymmetricgridview/library/widget/NineLinearLayout;->clearAnimation()V

    goto :goto_0

    :cond_0
    if-nez p1, :cond_1

    .line 24
    invoke-virtual {p0, v0}, Lcom/felipecsl/asymmetricgridview/library/widget/NineLinearLayout;->setAnimation(Landroid/view/animation/Animation;)V

    .line 27
    :cond_1
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    return-void
.end method

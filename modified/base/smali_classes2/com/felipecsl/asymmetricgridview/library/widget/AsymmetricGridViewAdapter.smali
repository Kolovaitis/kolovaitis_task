.class public abstract Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;
.super Landroid/widget/BaseAdapter;
.source "AsymmetricGridViewAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Lcom/felipecsl/asymmetricgridview/library/AsymmetricGridViewAdapterContract;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter$ProcessRowsTask;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/felipecsl/asymmetricgridview/library/model/AsymmetricItem;",
        ">",
        "Landroid/widget/BaseAdapter;",
        "Landroid/view/View$OnClickListener;",
        "Landroid/view/View$OnLongClickListener;",
        "Lcom/felipecsl/asymmetricgridview/library/AsymmetricGridViewAdapterContract;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "AsymmetricGridViewAdapter"


# instance fields
.field private asyncTask:Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter$ProcessRowsTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter<",
            "TT;>.ProcessRowsTask;"
        }
    .end annotation
.end field

.field protected final context:Landroid/content/Context;

.field protected final items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation
.end field

.field private itemsPerRow:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/felipecsl/asymmetricgridview/library/widget/RowInfo<",
            "TT;>;>;"
        }
    .end annotation
.end field

.field private final linearLayoutPool:Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool<",
            "Lcom/felipecsl/asymmetricgridview/library/widget/IcsLinearLayout;",
            ">;"
        }
    .end annotation
.end field

.field protected final listView:Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;

.field private final viewPool:Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;",
            "Ljava/util/List<",
            "TT;>;)V"
        }
    .end annotation

    .line 43
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 36
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->itemsPerRow:Ljava/util/Map;

    .line 38
    new-instance v0, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool;

    invoke-direct {v0}, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool;-><init>()V

    iput-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->viewPool:Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool;

    .line 45
    new-instance v0, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool;

    new-instance v1, Lcom/felipecsl/asymmetricgridview/library/widget/LinearLayoutPoolObjectFactory;

    invoke-direct {v1, p1}, Lcom/felipecsl/asymmetricgridview/library/widget/LinearLayoutPoolObjectFactory;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1}, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool;-><init>(Lcom/felipecsl/asymmetricgridview/library/widget/PoolObjectFactory;)V

    iput-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->linearLayoutPool:Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool;

    .line 46
    iput-object p3, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->items:Ljava/util/List;

    .line 47
    iput-object p1, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->context:Landroid/content/Context;

    .line 48
    iput-object p2, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->listView:Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;

    return-void
.end method

.method static synthetic access$000(Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;)Ljava/util/Map;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->itemsPerRow:Ljava/util/Map;

    return-object p0
.end method

.method static synthetic access$100(Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;Ljava/util/List;)Lcom/felipecsl/asymmetricgridview/library/widget/RowInfo;
    .locals 0

    .line 25
    invoke-direct {p0, p1}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->calculateItemsForRow(Ljava/util/List;)Lcom/felipecsl/asymmetricgridview/library/widget/RowInfo;

    move-result-object p0

    return-object p0
.end method

.method private calculateItemsForRow(Ljava/util/List;)Lcom/felipecsl/asymmetricgridview/library/widget/RowInfo;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "TT;>;)",
            "Lcom/felipecsl/asymmetricgridview/library/widget/RowInfo<",
            "TT;>;"
        }
    .end annotation

    .line 317
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->listView:Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;

    invoke-virtual {v0}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->getNumColumns()I

    move-result v0

    int-to-float v0, v0

    invoke-direct {p0, p1, v0}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->calculateItemsForRow(Ljava/util/List;F)Lcom/felipecsl/asymmetricgridview/library/widget/RowInfo;

    move-result-object p1

    return-object p1
.end method

.method private calculateItemsForRow(Ljava/util/List;F)Lcom/felipecsl/asymmetricgridview/library/widget/RowInfo;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "TT;>;F)",
            "Lcom/felipecsl/asymmetricgridview/library/widget/RowInfo<",
            "TT;>;"
        }
    .end annotation

    .line 321
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x1

    const/4 v2, 0x0

    move v3, p2

    const/4 v4, 0x0

    const/4 v5, 0x1

    :goto_0
    const/4 v6, 0x0

    cmpl-float v6, v3, v6

    if-lez v6, :cond_4

    .line 326
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    if-ge v4, v6, :cond_4

    add-int/lit8 v6, v4, 0x1

    .line 327
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/felipecsl/asymmetricgridview/library/model/AsymmetricItem;

    .line 328
    invoke-interface {v4}, Lcom/felipecsl/asymmetricgridview/library/model/AsymmetricItem;->getRowSpan()I

    move-result v7

    invoke-interface {v4}, Lcom/felipecsl/asymmetricgridview/library/model/AsymmetricItem;->getColumnSpan()I

    move-result v8

    mul-int v7, v7, v8

    int-to-float v7, v7

    .line 330
    iget-object v8, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->listView:Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;

    invoke-virtual {v8}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->isDebugging()Z

    move-result v8

    if-eqz v8, :cond_0

    const-string v8, "AsymmetricGridViewAdapter"

    const-string v9, "item %s in row with height %s consumes %s area"

    const/4 v10, 0x3

    .line 331
    new-array v10, v10, [Ljava/lang/Object;

    aput-object v4, v10, v2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v10, v1

    const/4 v11, 0x2

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    :cond_0
    invoke-interface {v4}, Lcom/felipecsl/asymmetricgridview/library/model/AsymmetricItem;->getRowSpan()I

    move-result v8

    if-ge v5, v8, :cond_1

    .line 335
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 336
    invoke-interface {v4}, Lcom/felipecsl/asymmetricgridview/library/model/AsymmetricItem;->getRowSpan()I

    move-result v3

    .line 338
    invoke-interface {v4}, Lcom/felipecsl/asymmetricgridview/library/model/AsymmetricItem;->getRowSpan()I

    move-result v4

    int-to-float v4, v4

    mul-float v4, v4, p2

    move v5, v3

    move v3, v4

    const/4 v4, 0x0

    goto :goto_0

    :cond_1
    cmpl-float v8, v3, v7

    if-ltz v8, :cond_2

    sub-float/2addr v3, v7

    .line 341
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v4, v6

    goto :goto_0

    .line 342
    :cond_2
    iget-object v4, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->listView:Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;

    invoke-virtual {v4}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->isAllowReordering()Z

    move-result v4

    if-nez v4, :cond_3

    goto :goto_1

    :cond_3
    move v4, v6

    goto :goto_0

    .line 347
    :cond_4
    :goto_1
    new-instance p1, Lcom/felipecsl/asymmetricgridview/library/widget/RowInfo;

    invoke-direct {p1, v5, v0, v3}, Lcom/felipecsl/asymmetricgridview/library/widget/RowInfo;-><init>(ILjava/util/List;F)V

    return-object p1
.end method

.method private findOrInitializeChildLayout(Landroid/widget/LinearLayout;I)Lcom/felipecsl/asymmetricgridview/library/widget/IcsLinearLayout;
    .locals 3

    .line 210
    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/felipecsl/asymmetricgridview/library/widget/IcsLinearLayout;

    if-nez p2, :cond_1

    .line 213
    iget-object p2, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->linearLayoutPool:Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool;

    invoke-virtual {p2}, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool;->get()Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/felipecsl/asymmetricgridview/library/widget/IcsLinearLayout;

    const/4 v0, 0x1

    .line 214
    invoke-virtual {p2, v0}, Lcom/felipecsl/asymmetricgridview/library/widget/IcsLinearLayout;->setOrientation(I)V

    .line 216
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->listView:Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;

    invoke-virtual {v0}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->isDebugging()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "#837BF2"

    .line 217
    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/felipecsl/asymmetricgridview/library/widget/IcsLinearLayout;->setBackgroundColor(I)V

    :cond_0
    const/4 v0, 0x2

    .line 219
    invoke-virtual {p2, v0}, Lcom/felipecsl/asymmetricgridview/library/widget/IcsLinearLayout;->setShowDividers(I)V

    .line 220
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/felipecsl/asymmetricgridview/library/R$drawable;->item_divider_vertical:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/felipecsl/asymmetricgridview/library/widget/IcsLinearLayout;->setDividerDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 222
    new-instance v0, Landroid/widget/AbsListView$LayoutParams;

    const/4 v1, -0x2

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {p2, v0}, Lcom/felipecsl/asymmetricgridview/library/widget/IcsLinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 224
    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    :cond_1
    return-object p2
.end method

.method private findOrInitializeLayout(Landroid/view/View;)Lcom/felipecsl/asymmetricgridview/library/widget/IcsLinearLayout;
    .locals 6

    if-eqz p1, :cond_1

    .line 182
    instance-of v0, p1, Lcom/felipecsl/asymmetricgridview/library/widget/IcsLinearLayout;

    if-nez v0, :cond_0

    goto :goto_0

    .line 193
    :cond_0
    check-cast p1, Lcom/felipecsl/asymmetricgridview/library/widget/IcsLinearLayout;

    goto :goto_1

    .line 183
    :cond_1
    :goto_0
    new-instance p1, Lcom/felipecsl/asymmetricgridview/library/widget/IcsLinearLayout;

    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->context:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-direct {p1, v0, v1}, Lcom/felipecsl/asymmetricgridview/library/widget/IcsLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 184
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->listView:Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;

    invoke-virtual {v0}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->isDebugging()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "#83F27B"

    .line 185
    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/felipecsl/asymmetricgridview/library/widget/IcsLinearLayout;->setBackgroundColor(I)V

    :cond_2
    const/4 v0, 0x2

    .line 187
    invoke-virtual {p1, v0}, Lcom/felipecsl/asymmetricgridview/library/widget/IcsLinearLayout;->setShowDividers(I)V

    .line 188
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/felipecsl/asymmetricgridview/library/R$drawable;->item_divider_horizontal:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/felipecsl/asymmetricgridview/library/widget/IcsLinearLayout;->setDividerDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 190
    new-instance v0, Landroid/widget/AbsListView$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {p1, v0}, Lcom/felipecsl/asymmetricgridview/library/widget/IcsLinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :goto_1
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 196
    :goto_2
    invoke-virtual {p1}, Lcom/felipecsl/asymmetricgridview/library/widget/IcsLinearLayout;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_4

    .line 197
    invoke-virtual {p1, v1}, Lcom/felipecsl/asymmetricgridview/library/widget/IcsLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/felipecsl/asymmetricgridview/library/widget/IcsLinearLayout;

    .line 198
    iget-object v3, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->linearLayoutPool:Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool;

    invoke-virtual {v3, v2}, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool;->put(Landroid/view/View;)V

    const/4 v3, 0x0

    .line 199
    :goto_3
    invoke-virtual {v2}, Lcom/felipecsl/asymmetricgridview/library/widget/IcsLinearLayout;->getChildCount()I

    move-result v4

    if-ge v3, v4, :cond_3

    .line 200
    iget-object v4, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->viewPool:Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool;

    invoke-virtual {v2, v3}, Lcom/felipecsl/asymmetricgridview/library/widget/IcsLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool;->put(Landroid/view/View;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 201
    :cond_3
    invoke-virtual {v2}, Lcom/felipecsl/asymmetricgridview/library/widget/IcsLinearLayout;->removeAllViews()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 203
    :cond_4
    invoke-virtual {p1}, Lcom/felipecsl/asymmetricgridview/library/widget/IcsLinearLayout;->removeAllViews()V

    return-object p1
.end method


# virtual methods
.method public appendItems(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "TT;>;)V"
        }
    .end annotation

    .line 241
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->items:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 244
    invoke-virtual {p0}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->getRowCount()I

    move-result v0

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    if-ltz v0, :cond_0

    .line 246
    iget-object v2, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->itemsPerRow:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/felipecsl/asymmetricgridview/library/widget/RowInfo;

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    const/4 v3, 0x0

    if-eqz v2, :cond_4

    .line 249
    invoke-virtual {v2}, Lcom/felipecsl/asymmetricgridview/library/widget/RowInfo;->getSpaceLeft()F

    move-result v4

    .line 251
    iget-object v5, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->listView:Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;

    invoke-virtual {v5}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->isDebugging()Z

    move-result v5

    if-eqz v5, :cond_1

    const-string v5, "AsymmetricGridViewAdapter"

    .line 252
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Space left in last row: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const/4 v5, 0x0

    cmpl-float v4, v4, v5

    if-lez v4, :cond_4

    .line 257
    invoke-virtual {v2}, Lcom/felipecsl/asymmetricgridview/library/widget/RowInfo;->getItems()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/felipecsl/asymmetricgridview/library/model/AsymmetricItem;

    .line 258
    invoke-interface {p1, v3, v4}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_1

    .line 260
    :cond_2
    invoke-direct {p0, p1}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->calculateItemsForRow(Ljava/util/List;)Lcom/felipecsl/asymmetricgridview/library/widget/RowInfo;

    move-result-object v2

    .line 261
    invoke-virtual {v2}, Lcom/felipecsl/asymmetricgridview/library/widget/RowInfo;->getItems()Ljava/util/List;

    move-result-object v4

    .line 263
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_4

    .line 264
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/felipecsl/asymmetricgridview/library/model/AsymmetricItem;

    invoke-interface {p1, v5}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_2

    .line 266
    :cond_3
    iget-object v4, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->itemsPerRow:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v4, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 267
    invoke-virtual {p0}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->notifyDataSetChanged()V

    .line 272
    :cond_4
    new-instance v0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter$ProcessRowsTask;

    invoke-direct {v0, p0}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter$ProcessRowsTask;-><init>(Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;)V

    iput-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->asyncTask:Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter$ProcessRowsTask;

    .line 273
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->asyncTask:Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter$ProcessRowsTask;

    new-array v1, v1, [Ljava/util/List;

    aput-object p1, v1, v3

    invoke-virtual {v0, v1}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter$ProcessRowsTask;->executeSerially([Ljava/lang/Object;)Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;

    return-void
.end method

.method public abstract getActualView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end method

.method public getCount()I
    .locals 1

    .line 293
    invoke-virtual {p0}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->getRowCount()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/felipecsl/asymmetricgridview/library/model/AsymmetricItem;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .line 58
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->items:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/felipecsl/asymmetricgridview/library/model/AsymmetricItem;

    return-object p1
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 0

    .line 25
    invoke-virtual {p0, p1}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->getItem(I)Lcom/felipecsl/asymmetricgridview/library/model/AsymmetricItem;

    move-result-object p1

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getRowCount()I
    .locals 1

    .line 297
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->itemsPerRow:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method

.method protected getRowHeight(I)I
    .locals 2

    .line 66
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->listView:Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;

    invoke-virtual {v0}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->getColumnWidth()I

    move-result v0

    mul-int v0, v0, p1

    add-int/lit8 p1, p1, -0x1

    .line 69
    iget-object v1, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->listView:Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;

    invoke-virtual {v1}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->getDividerHeight()I

    move-result v1

    mul-int p1, p1, v1

    add-int/2addr v0, p1

    return v0
.end method

.method protected getRowHeight(Lcom/felipecsl/asymmetricgridview/library/model/AsymmetricItem;)I
    .locals 0

    .line 54
    invoke-interface {p1}, Lcom/felipecsl/asymmetricgridview/library/model/AsymmetricItem;->getRowSpan()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->getRowHeight(I)I

    move-result p1

    return p1
.end method

.method protected getRowWidth(I)I
    .locals 2

    .line 77
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->listView:Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;

    invoke-virtual {v0}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->getColumnWidth()I

    move-result v0

    mul-int v0, v0, p1

    add-int/lit8 p1, p1, -0x1

    .line 80
    iget-object v1, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->listView:Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;

    invoke-virtual {v1}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->getRequestedHorizontalSpacing()I

    move-result v1

    mul-int p1, p1, v1

    add-int/2addr v0, p1

    iget-object p1, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->context:Landroid/content/Context;

    invoke-static {p1}, Lcom/felipecsl/asymmetricgridview/library/Utils;->getScreenWidth(Landroid/content/Context;)I

    move-result p1

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result p1

    return p1
.end method

.method protected getRowWidth(Lcom/felipecsl/asymmetricgridview/library/model/AsymmetricItem;)I
    .locals 0

    .line 73
    invoke-interface {p1}, Lcom/felipecsl/asymmetricgridview/library/model/AsymmetricItem;->getColumnSpan()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->getRowWidth(I)I

    move-result p1

    return p1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10

    .line 85
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->listView:Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;

    invoke-virtual {v0}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->isDebugging()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "AsymmetricGridViewAdapter"

    .line 86
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getView("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    :cond_0
    invoke-direct {p0, p2}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->findOrInitializeLayout(Landroid/view/View;)Lcom/felipecsl/asymmetricgridview/library/widget/IcsLinearLayout;

    move-result-object p2

    .line 90
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->itemsPerRow:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/felipecsl/asymmetricgridview/library/widget/RowInfo;

    .line 91
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 92
    invoke-virtual {v0}, Lcom/felipecsl/asymmetricgridview/library/widget/RowInfo;->getItems()Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 102
    invoke-virtual {v0}, Lcom/felipecsl/asymmetricgridview/library/widget/RowInfo;->getRowHeight()I

    move-result v2

    const/4 v3, 0x0

    move v5, v2

    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 104
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_3

    iget-object v6, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->listView:Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;

    invoke-virtual {v6}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->getNumColumns()I

    move-result v6

    if-ge v2, v6, :cond_3

    .line 105
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/felipecsl/asymmetricgridview/library/model/AsymmetricItem;

    if-nez v5, :cond_1

    add-int/lit8 v2, v2, 0x1

    .line 111
    invoke-virtual {v0}, Lcom/felipecsl/asymmetricgridview/library/widget/RowInfo;->getRowHeight()I

    move-result v5

    const/4 v4, 0x0

    goto :goto_0

    .line 116
    :cond_1
    invoke-interface {v6}, Lcom/felipecsl/asymmetricgridview/library/model/AsymmetricItem;->getRowSpan()I

    move-result v7

    if-lt v5, v7, :cond_2

    .line 117
    invoke-interface {v1, v6}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 119
    iget-object v4, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->items:Ljava/util/List;

    invoke-interface {v4, v6}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v4

    .line 120
    invoke-direct {p0, p2, v2}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->findOrInitializeChildLayout(Landroid/widget/LinearLayout;I)Lcom/felipecsl/asymmetricgridview/library/widget/IcsLinearLayout;

    move-result-object v7

    .line 121
    iget-object v8, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->viewPool:Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool;

    invoke-virtual {v8}, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool;->get()Landroid/view/View;

    move-result-object v8

    .line 122
    invoke-virtual {p0, v4, v8, p3}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->getActualView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 123
    invoke-virtual {v4, v6}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 124
    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 125
    invoke-virtual {v4, p0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 127
    invoke-interface {v6}, Lcom/felipecsl/asymmetricgridview/library/model/AsymmetricItem;->getRowSpan()I

    move-result v8

    sub-int/2addr v5, v8

    .line 130
    new-instance v8, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {p0, v6}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->getRowWidth(Lcom/felipecsl/asymmetricgridview/library/model/AsymmetricItem;)I

    move-result v9

    invoke-virtual {p0, v6}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->getRowHeight(Lcom/felipecsl/asymmetricgridview/library/model/AsymmetricItem;)I

    move-result v6

    invoke-direct {v8, v9, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v8}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 133
    invoke-virtual {v7, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    const/4 v4, 0x0

    goto :goto_0

    .line 134
    :cond_2
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    if-ge v4, v6, :cond_3

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 142
    :cond_3
    iget-object p3, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->listView:Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;

    invoke-virtual {p3}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->isDebugging()Z

    move-result p3

    if-eqz p3, :cond_4

    rem-int/lit8 p1, p1, 0x14

    if-nez p1, :cond_4

    const-string p1, "AsymmetricGridViewAdapter"

    .line 143
    iget-object p3, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->linearLayoutPool:Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool;

    const-string v0, "LinearLayout"

    invoke-virtual {p3, v0}, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool;->getStats(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    invoke-static {p1, p3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string p1, "AsymmetricGridViewAdapter"

    .line 144
    iget-object p3, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->viewPool:Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool;

    const-string v0, "Views"

    invoke-virtual {p3, v0}, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool;->getStats(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    invoke-static {p1, p3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    return-object p2
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .line 279
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/felipecsl/asymmetricgridview/library/model/AsymmetricItem;

    .line 280
    iget-object v1, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->listView:Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;

    iget-object v2, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->items:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    invoke-virtual {v1, v0, p1}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->fireOnItemClick(ILandroid/view/View;)V

    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 3

    .line 286
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/felipecsl/asymmetricgridview/library/model/AsymmetricItem;

    .line 287
    iget-object v1, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->listView:Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;

    iget-object v2, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->items:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    invoke-virtual {v1, v0, p1}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->fireOnItemLongClick(ILandroid/view/View;)Z

    move-result p1

    return p1
.end method

.method public recalculateItemsPerRow()V
    .locals 4

    .line 302
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->asyncTask:Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter$ProcessRowsTask;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 303
    invoke-virtual {v0, v1}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter$ProcessRowsTask;->cancel(Z)Z

    .line 305
    :cond_0
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->linearLayoutPool:Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool;

    invoke-virtual {v0}, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool;->clear()V

    .line 306
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->viewPool:Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool;

    invoke-virtual {v0}, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool;->clear()V

    .line 307
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->itemsPerRow:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 309
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 310
    iget-object v2, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->items:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 312
    new-instance v2, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter$ProcessRowsTask;

    invoke-direct {v2, p0}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter$ProcessRowsTask;-><init>(Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;)V

    iput-object v2, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->asyncTask:Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter$ProcessRowsTask;

    .line 313
    iget-object v2, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->asyncTask:Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter$ProcessRowsTask;

    new-array v1, v1, [Ljava/util/List;

    const/4 v3, 0x0

    aput-object v0, v1, v3

    invoke-virtual {v2, v1}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter$ProcessRowsTask;->executeSerially([Ljava/lang/Object;)Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;

    return-void
.end method

.method public restoreState(Landroid/os/Parcelable;)V
    .locals 5

    .line 162
    check-cast p1, Landroid/os/Bundle;

    if-eqz p1, :cond_1

    .line 165
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    const-string v0, "totalItems"

    .line 167
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 168
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    .line 171
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "item_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/felipecsl/asymmetricgridview/library/model/AsymmetricItem;

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 174
    :cond_0
    invoke-virtual {p0, v1}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->setItems(Ljava/util/List;)V

    :cond_1
    return-void
.end method

.method public saveState()Landroid/os/Parcelable;
    .locals 4

    .line 151
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "totalItems"

    .line 152
    iget-object v2, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->items:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const/4 v1, 0x0

    .line 154
    :goto_0
    iget-object v2, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->items:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 155
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "item_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->items:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/Parcelable;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public setItems(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "TT;>;)V"
        }
    .end annotation

    .line 231
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->linearLayoutPool:Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool;

    invoke-virtual {v0}, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool;->clear()V

    .line 232
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->viewPool:Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool;

    invoke-virtual {v0}, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool;->clear()V

    .line 233
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 234
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->items:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 235
    invoke-virtual {p0}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->recalculateItemsPerRow()V

    .line 236
    invoke-virtual {p0}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->notifyDataSetChanged()V

    return-void
.end method

.class Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter$ProcessRowsTask;
.super Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;
.source "AsymmetricGridViewAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ProcessRowsTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat<",
        "Ljava/util/List<",
        "TT;>;",
        "Ljava/lang/Void;",
        "Ljava/util/List<",
        "Lcom/felipecsl/asymmetricgridview/library/widget/RowInfo<",
        "TT;>;>;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;


# direct methods
.method constructor <init>(Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;)V
    .locals 0

    .line 350
    iput-object p1, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter$ProcessRowsTask;->this$0:Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;

    invoke-direct {p0}, Lcom/felipecsl/asymmetricgridview/library/AsyncTaskCompat;-><init>()V

    return-void
.end method

.method private calculateItemsPerRow(ILjava/util/List;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "TT;>;)",
            "Ljava/util/List<",
            "Lcom/felipecsl/asymmetricgridview/library/widget/RowInfo<",
            "TT;>;>;"
        }
    .end annotation

    .line 372
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 374
    :goto_0
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 375
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter$ProcessRowsTask;->this$0:Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;

    invoke-static {v0, p2}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->access$100(Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;Ljava/util/List;)Lcom/felipecsl/asymmetricgridview/library/widget/RowInfo;

    move-result-object v0

    .line 377
    invoke-virtual {v0}, Lcom/felipecsl/asymmetricgridview/library/widget/RowInfo;->getItems()Ljava/util/List;

    move-result-object v1

    .line 378
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_2

    .line 384
    :cond_0
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/felipecsl/asymmetricgridview/library/model/AsymmetricItem;

    .line 385
    invoke-interface {p2, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 387
    :cond_1
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    :goto_2
    return-object p1
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 350
    check-cast p1, [Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter$ProcessRowsTask;->doInBackground([Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method protected final varargs doInBackground([Ljava/util/List;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/util/List<",
            "TT;>;)",
            "Ljava/util/List<",
            "Lcom/felipecsl/asymmetricgridview/library/widget/RowInfo<",
            "TT;>;>;"
        }
    .end annotation

    .annotation runtime Ljava/lang/SafeVarargs;
    .end annotation

    const/4 v0, 0x0

    .line 355
    aget-object p1, p1, v0

    invoke-direct {p0, v0, p1}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter$ProcessRowsTask;->calculateItemsPerRow(ILjava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 350
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter$ProcessRowsTask;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/felipecsl/asymmetricgridview/library/widget/RowInfo<",
            "TT;>;>;)V"
        }
    .end annotation

    .line 360
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/felipecsl/asymmetricgridview/library/widget/RowInfo;

    .line 361
    iget-object v1, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter$ProcessRowsTask;->this$0:Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;

    invoke-static {v1}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->access$000(Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;)Ljava/util/Map;

    move-result-object v1

    iget-object v2, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter$ProcessRowsTask;->this$0:Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;

    invoke-virtual {v2}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->getRowCount()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 363
    :cond_0
    iget-object p1, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter$ProcessRowsTask;->this$0:Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;

    iget-object p1, p1, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->listView:Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;

    invoke-virtual {p1}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->isDebugging()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 364
    iget-object p1, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter$ProcessRowsTask;->this$0:Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;

    invoke-static {p1}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->access$000(Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;)Ljava/util/Map;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    const-string v1, "AsymmetricGridViewAdapter"

    .line 365
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "row: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v3, ", items: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/felipecsl/asymmetricgridview/library/widget/RowInfo;

    invoke-virtual {v0}, Lcom/felipecsl/asymmetricgridview/library/widget/RowInfo;->getItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 368
    :cond_1
    iget-object p1, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter$ProcessRowsTask;->this$0:Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;

    invoke-virtual {p1}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridViewAdapter;->notifyDataSetChanged()V

    return-void
.end method

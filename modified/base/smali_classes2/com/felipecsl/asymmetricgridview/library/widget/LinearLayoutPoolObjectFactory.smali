.class public Lcom/felipecsl/asymmetricgridview/library/widget/LinearLayoutPoolObjectFactory;
.super Ljava/lang/Object;
.source "LinearLayoutPoolObjectFactory.java"

# interfaces
.implements Lcom/felipecsl/asymmetricgridview/library/widget/PoolObjectFactory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/felipecsl/asymmetricgridview/library/widget/PoolObjectFactory<",
        "Lcom/felipecsl/asymmetricgridview/library/widget/IcsLinearLayout;",
        ">;"
    }
.end annotation


# instance fields
.field private final context:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object p1, p0, Lcom/felipecsl/asymmetricgridview/library/widget/LinearLayoutPoolObjectFactory;->context:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public bridge synthetic createObject()Landroid/view/View;
    .locals 1

    .line 5
    invoke-virtual {p0}, Lcom/felipecsl/asymmetricgridview/library/widget/LinearLayoutPoolObjectFactory;->createObject()Lcom/felipecsl/asymmetricgridview/library/widget/IcsLinearLayout;

    move-result-object v0

    return-object v0
.end method

.method public createObject()Lcom/felipecsl/asymmetricgridview/library/widget/IcsLinearLayout;
    .locals 3

    .line 15
    new-instance v0, Lcom/felipecsl/asymmetricgridview/library/widget/IcsLinearLayout;

    iget-object v1, p0, Lcom/felipecsl/asymmetricgridview/library/widget/LinearLayoutPoolObjectFactory;->context:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/felipecsl/asymmetricgridview/library/widget/IcsLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

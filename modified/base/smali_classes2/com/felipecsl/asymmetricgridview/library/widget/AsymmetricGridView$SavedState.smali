.class public Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$SavedState;
.super Landroid/view/View$BaseSavedState;
.source "AsymmetricGridView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SavedState"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$SavedState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field adapterState:Landroid/os/Parcelable;

.field allowReordering:Z

.field debugging:Z

.field defaultPadding:I

.field loader:Ljava/lang/ClassLoader;

.field numColumns:I

.field requestedColumnCount:I

.field requestedColumnWidth:I

.field requestedHorizontalSpacing:I

.field requestedVerticalSpacing:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 248
    new-instance v0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$SavedState$1;

    invoke-direct {v0}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$SavedState$1;-><init>()V

    sput-object v0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$SavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .line 220
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    .line 222
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$SavedState;->numColumns:I

    .line 223
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$SavedState;->requestedColumnWidth:I

    .line 224
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$SavedState;->requestedColumnCount:I

    .line 225
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$SavedState;->requestedVerticalSpacing:I

    .line 226
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$SavedState;->requestedHorizontalSpacing:I

    .line 227
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$SavedState;->defaultPadding:I

    .line 228
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$SavedState;->debugging:Z

    .line 229
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v2, :cond_1

    const/4 v1, 0x1

    :cond_1
    iput-boolean v1, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$SavedState;->allowReordering:Z

    .line 230
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$SavedState;->loader:Ljava/lang/ClassLoader;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p1

    iput-object p1, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$SavedState;->adapterState:Landroid/os/Parcelable;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcelable;)V
    .locals 0

    .line 216
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    return-void
.end method


# virtual methods
.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 235
    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 237
    iget v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$SavedState;->numColumns:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 238
    iget v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$SavedState;->requestedColumnWidth:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 239
    iget v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$SavedState;->requestedColumnCount:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 240
    iget v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$SavedState;->requestedVerticalSpacing:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 241
    iget v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$SavedState;->requestedHorizontalSpacing:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 242
    iget v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$SavedState;->defaultPadding:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 243
    iget-boolean v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$SavedState;->debugging:Z

    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 244
    iget-boolean v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$SavedState;->allowReordering:Z

    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 245
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$SavedState;->adapterState:Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.class Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool$PoolStats;
.super Ljava/lang/Object;
.source "ViewPool.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "PoolStats"
.end annotation


# instance fields
.field created:I

.field hits:I

.field misses:I

.field size:I


# direct methods
.method constructor <init>()V
    .locals 1

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 12
    iput v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool$PoolStats;->size:I

    .line 13
    iput v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool$PoolStats;->hits:I

    .line 14
    iput v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool$PoolStats;->misses:I

    .line 15
    iput v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool$PoolStats;->created:I

    return-void
.end method


# virtual methods
.method getStats(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    const-string v0, "%s: size %d, hits %d, misses %d, created %d"

    const/4 v1, 0x5

    .line 18
    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    iget p1, p0, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool$PoolStats;->size:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v2, 0x1

    aput-object p1, v1, v2

    iget p1, p0, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool$PoolStats;->hits:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v2, 0x2

    aput-object p1, v1, v2

    iget p1, p0, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool$PoolStats;->misses:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v2, 0x3

    aput-object p1, v1, v2

    iget p1, p0, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool$PoolStats;->created:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v2, 0x4

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

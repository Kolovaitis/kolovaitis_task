.class public Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;
.super Landroid/widget/ListView;
.source "AsymmetricGridView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$SavedState;
    }
.end annotation


# static fields
.field private static final DEFAULT_COLUMN_COUNT:I = 0x2

.field private static final TAG:Ljava/lang/String; = "AsymmetricGridView"


# instance fields
.field protected allowReordering:Z

.field protected debugging:Z

.field protected gridAdapter:Lcom/felipecsl/asymmetricgridview/library/AsymmetricGridViewAdapterContract;

.field protected numColumns:I

.field protected onItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field protected onItemLongClickListener:Landroid/widget/AdapterView$OnItemLongClickListener;

.field protected requestedColumnCount:I

.field protected requestedColumnWidth:I

.field protected requestedHorizontalSpacing:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p2, 0x2

    .line 20
    iput p2, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->numColumns:I

    const/high16 p2, 0x40a00000    # 5.0f

    .line 33
    invoke-static {p1, p2}, Lcom/felipecsl/asymmetricgridview/library/Utils;->dpToPx(Landroid/content/Context;F)I

    move-result p1

    iput p1, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->requestedHorizontalSpacing:I

    .line 35
    invoke-virtual {p0}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 37
    new-instance p2, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$1;

    invoke-direct {p2, p0}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$1;-><init>(Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;)V

    invoke-virtual {p1, p2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public determineColumns()I
    .locals 3

    .line 111
    invoke-virtual {p0}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->getAvailableSpace()I

    move-result v0

    .line 113
    iget v1, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->requestedColumnWidth:I

    if-lez v1, :cond_0

    .line 114
    iget v2, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->requestedHorizontalSpacing:I

    add-int/2addr v0, v2

    add-int/2addr v1, v2

    div-int/2addr v0, v1

    goto :goto_0

    .line 116
    :cond_0
    iget v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->requestedColumnCount:I

    if-lez v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x2

    :goto_0
    if-gtz v0, :cond_2

    const/4 v0, 0x1

    .line 126
    :cond_2
    iput v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->numColumns:I

    return v0
.end method

.method protected fireOnItemClick(ILandroid/view/View;)V
    .locals 6

    .line 53
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->onItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    if-eqz v0, :cond_0

    .line 54
    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result v1

    int-to-long v4, v1

    move-object v1, p0

    move-object v2, p2

    move v3, p1

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemClickListener;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    :cond_0
    return-void
.end method

.method protected fireOnItemLongClick(ILandroid/view/View;)Z
    .locals 6

    .line 63
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->onItemLongClickListener:Landroid/widget/AdapterView$OnItemLongClickListener;

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result v1

    int-to-long v4, v1

    move-object v1, p0

    move-object v2, p2

    move v3, p1

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemLongClickListener;->onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public getAvailableSpace()I
    .locals 2

    .line 180
    invoke-virtual {p0}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public getColumnWidth()I
    .locals 4

    .line 176
    invoke-virtual {p0}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->getAvailableSpace()I

    move-result v0

    iget v1, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->numColumns:I

    add-int/lit8 v2, v1, -0x1

    iget v3, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->requestedHorizontalSpacing:I

    mul-int v2, v2, v3

    sub-int/2addr v0, v2

    div-int/2addr v0, v1

    return v0
.end method

.method public getNumColumns()I
    .locals 1

    .line 172
    iget v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->numColumns:I

    return v0
.end method

.method public getRequestedHorizontalSpacing()I
    .locals 1

    .line 96
    iget v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->requestedHorizontalSpacing:I

    return v0
.end method

.method public isAllowReordering()Z
    .locals 1

    .line 184
    iget-boolean v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->allowReordering:Z

    return v0
.end method

.method public isDebugging()Z
    .locals 1

    .line 195
    iget-boolean v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->debugging:Z

    return v0
.end method

.method protected onMeasure(II)V
    .locals 0

    .line 105
    invoke-super {p0, p1, p2}, Landroid/widget/ListView;->onMeasure(II)V

    .line 106
    invoke-virtual {p0}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->determineColumns()I

    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .line 150
    instance-of v0, p1, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$SavedState;

    if-nez v0, :cond_0

    .line 151
    invoke-super {p0, p1}, Landroid/widget/ListView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    return-void

    .line 155
    :cond_0
    check-cast p1, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$SavedState;

    .line 156
    invoke-virtual {p1}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/ListView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 158
    iget-boolean v0, p1, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$SavedState;->allowReordering:Z

    iput-boolean v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->allowReordering:Z

    .line 159
    iget-boolean v0, p1, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$SavedState;->debugging:Z

    iput-boolean v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->debugging:Z

    .line 160
    iget v0, p1, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$SavedState;->numColumns:I

    iput v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->numColumns:I

    .line 161
    iget v0, p1, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$SavedState;->requestedColumnCount:I

    iput v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->requestedColumnCount:I

    .line 162
    iget v0, p1, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$SavedState;->requestedColumnWidth:I

    iput v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->requestedColumnWidth:I

    .line 163
    iget v0, p1, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$SavedState;->requestedHorizontalSpacing:I

    iput v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->requestedHorizontalSpacing:I

    .line 165
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->gridAdapter:Lcom/felipecsl/asymmetricgridview/library/AsymmetricGridViewAdapterContract;

    if-eqz v0, :cond_1

    .line 166
    iget-object p1, p1, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$SavedState;->adapterState:Landroid/os/Parcelable;

    invoke-interface {v0, p1}, Lcom/felipecsl/asymmetricgridview/library/AsymmetricGridViewAdapterContract;->restoreState(Landroid/os/Parcelable;)V

    :cond_1
    const/16 p1, 0x14

    const/4 v0, 0x0

    .line 168
    invoke-virtual {p0, p1, v0}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->setSelectionFromTop(II)V

    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .line 133
    invoke-super {p0}, Landroid/widget/ListView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 134
    new-instance v1, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$SavedState;

    invoke-direct {v1, v0}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 135
    iget-boolean v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->allowReordering:Z

    iput-boolean v0, v1, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$SavedState;->allowReordering:Z

    .line 136
    iget-boolean v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->debugging:Z

    iput-boolean v0, v1, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$SavedState;->debugging:Z

    .line 137
    iget v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->numColumns:I

    iput v0, v1, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$SavedState;->numColumns:I

    .line 138
    iget v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->requestedColumnCount:I

    iput v0, v1, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$SavedState;->requestedColumnCount:I

    .line 139
    iget v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->requestedColumnWidth:I

    iput v0, v1, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$SavedState;->requestedColumnWidth:I

    .line 140
    iget v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->requestedHorizontalSpacing:I

    iput v0, v1, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$SavedState;->requestedHorizontalSpacing:I

    .line 142
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->gridAdapter:Lcom/felipecsl/asymmetricgridview/library/AsymmetricGridViewAdapterContract;

    if-eqz v0, :cond_0

    .line 143
    invoke-interface {v0}, Lcom/felipecsl/asymmetricgridview/library/AsymmetricGridViewAdapterContract;->saveState()Landroid/os/Parcelable;

    move-result-object v0

    iput-object v0, v1, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView$SavedState;->adapterState:Landroid/os/Parcelable;

    :cond_0
    return-object v1
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0

    .line 16
    check-cast p1, Landroid/widget/ListAdapter;

    invoke-virtual {p0, p1}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 1

    .line 71
    instance-of v0, p1, Landroid/widget/WrapperListAdapter;

    if-eqz v0, :cond_1

    .line 72
    check-cast p1, Landroid/widget/WrapperListAdapter;

    .line 73
    invoke-interface {p1}, Landroid/widget/WrapperListAdapter;->getWrappedAdapter()Landroid/widget/ListAdapter;

    move-result-object p1

    .line 75
    instance-of v0, p1, Lcom/felipecsl/asymmetricgridview/library/AsymmetricGridViewAdapterContract;

    if-eqz v0, :cond_0

    goto :goto_0

    .line 76
    :cond_0
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Wrapped adapter must implement AsymmetricGridViewAdapterContract"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 78
    :cond_1
    instance-of v0, p1, Lcom/felipecsl/asymmetricgridview/library/AsymmetricGridViewAdapterContract;

    if-eqz v0, :cond_2

    .line 82
    :goto_0
    move-object v0, p1

    check-cast v0, Lcom/felipecsl/asymmetricgridview/library/AsymmetricGridViewAdapterContract;

    iput-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->gridAdapter:Lcom/felipecsl/asymmetricgridview/library/AsymmetricGridViewAdapterContract;

    .line 83
    invoke-super {p0, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 84
    iget-object p1, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->gridAdapter:Lcom/felipecsl/asymmetricgridview/library/AsymmetricGridViewAdapterContract;

    invoke-interface {p1}, Lcom/felipecsl/asymmetricgridview/library/AsymmetricGridViewAdapterContract;->recalculateItemsPerRow()V

    return-void

    .line 79
    :cond_2
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Adapter must implement AsymmetricGridViewAdapterContract"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setAllowReordering(Z)V
    .locals 0

    .line 188
    iput-boolean p1, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->allowReordering:Z

    .line 189
    iget-object p1, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->gridAdapter:Lcom/felipecsl/asymmetricgridview/library/AsymmetricGridViewAdapterContract;

    if-eqz p1, :cond_0

    .line 190
    invoke-interface {p1}, Lcom/felipecsl/asymmetricgridview/library/AsymmetricGridViewAdapterContract;->recalculateItemsPerRow()V

    :cond_0
    return-void
.end method

.method public setDebugging(Z)V
    .locals 0

    .line 199
    iput-boolean p1, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->debugging:Z

    return-void
.end method

.method public setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 0

    .line 49
    iput-object p1, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->onItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    return-void
.end method

.method public setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V
    .locals 0

    .line 59
    iput-object p1, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->onItemLongClickListener:Landroid/widget/AdapterView$OnItemLongClickListener;

    return-void
.end method

.method public setRequestedColumnCount(I)V
    .locals 0

    .line 92
    iput p1, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->requestedColumnCount:I

    return-void
.end method

.method public setRequestedColumnWidth(I)V
    .locals 0

    .line 88
    iput p1, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->requestedColumnWidth:I

    return-void
.end method

.method public setRequestedHorizontalSpacing(I)V
    .locals 0

    .line 100
    iput p1, p0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->requestedHorizontalSpacing:I

    return-void
.end method

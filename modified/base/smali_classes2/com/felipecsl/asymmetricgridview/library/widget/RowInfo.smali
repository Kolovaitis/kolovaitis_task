.class Lcom/felipecsl/asymmetricgridview/library/widget/RowInfo;
.super Ljava/lang/Object;
.source "RowInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/felipecsl/asymmetricgridview/library/model/AsymmetricItem;",
        ">",
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/felipecsl/asymmetricgridview/library/widget/RowInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final rowHeight:I

.field private final spaceLeft:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 68
    new-instance v0, Lcom/felipecsl/asymmetricgridview/library/widget/RowInfo$1;

    invoke-direct {v0}, Lcom/felipecsl/asymmetricgridview/library/widget/RowInfo$1;-><init>()V

    sput-object v0, Lcom/felipecsl/asymmetricgridview/library/widget/RowInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ILjava/util/List;F)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "TT;>;F)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput p1, p0, Lcom/felipecsl/asymmetricgridview/library/widget/RowInfo;->rowHeight:I

    .line 22
    iput-object p2, p0, Lcom/felipecsl/asymmetricgridview/library/widget/RowInfo;->items:Ljava/util/List;

    .line 23
    iput p3, p0, Lcom/felipecsl/asymmetricgridview/library/widget/RowInfo;->spaceLeft:F

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 5

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/RowInfo;->rowHeight:I

    .line 29
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/RowInfo;->spaceLeft:F

    .line 30
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 32
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/felipecsl/asymmetricgridview/library/widget/RowInfo;->items:Ljava/util/List;

    .line 33
    const-class v1, Lcom/felipecsl/asymmetricgridview/library/model/AsymmetricItem;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    .line 36
    iget-object v3, p0, Lcom/felipecsl/asymmetricgridview/library/widget/RowInfo;->items:Ljava/util/List;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/felipecsl/asymmetricgridview/library/model/AsymmetricItem;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    .line 41
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/RowInfo;->items:Ljava/util/List;

    return-object v0
.end method

.method public getRowHeight()I
    .locals 1

    .line 45
    iget v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/RowInfo;->rowHeight:I

    return v0
.end method

.method public getSpaceLeft()F
    .locals 1

    .line 49
    iget v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/RowInfo;->spaceLeft:F

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .line 59
    iget p2, p0, Lcom/felipecsl/asymmetricgridview/library/widget/RowInfo;->rowHeight:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 60
    iget p2, p0, Lcom/felipecsl/asymmetricgridview/library/widget/RowInfo;->spaceLeft:F

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeFloat(F)V

    .line 61
    iget-object p2, p0, Lcom/felipecsl/asymmetricgridview/library/widget/RowInfo;->items:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    const/4 p2, 0x0

    const/4 v0, 0x0

    .line 63
    :goto_0
    iget-object v1, p0, Lcom/felipecsl/asymmetricgridview/library/widget/RowInfo;->items:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 64
    iget-object v1, p0, Lcom/felipecsl/asymmetricgridview/library/widget/RowInfo;->items:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Parcelable;

    invoke-virtual {p1, v1, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

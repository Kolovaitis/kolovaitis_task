.class Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool;
.super Ljava/lang/Object;
.source "ViewPool.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool$PoolStats;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Landroid/view/View;",
        ">",
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# instance fields
.field factory:Lcom/felipecsl/asymmetricgridview/library/widget/PoolObjectFactory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/felipecsl/asymmetricgridview/library/widget/PoolObjectFactory<",
            "TT;>;"
        }
    .end annotation
.end field

.field stack:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack<",
            "TT;>;"
        }
    .end annotation
.end field

.field stats:Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool$PoolStats;


# direct methods
.method constructor <init>()V
    .locals 1

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool;->stack:Ljava/util/Stack;

    const/4 v0, 0x0

    .line 24
    iput-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool;->factory:Lcom/felipecsl/asymmetricgridview/library/widget/PoolObjectFactory;

    .line 28
    new-instance v0, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool$PoolStats;

    invoke-direct {v0}, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool$PoolStats;-><init>()V

    iput-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool;->stats:Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool$PoolStats;

    return-void
.end method

.method constructor <init>(Lcom/felipecsl/asymmetricgridview/library/widget/PoolObjectFactory;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/felipecsl/asymmetricgridview/library/widget/PoolObjectFactory<",
            "TT;>;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool;->stack:Ljava/util/Stack;

    const/4 v0, 0x0

    .line 24
    iput-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool;->factory:Lcom/felipecsl/asymmetricgridview/library/widget/PoolObjectFactory;

    .line 32
    iput-object p1, p0, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool;->factory:Lcom/felipecsl/asymmetricgridview/library/widget/PoolObjectFactory;

    return-void
.end method


# virtual methods
.method clear()V
    .locals 1

    .line 58
    new-instance v0, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool$PoolStats;

    invoke-direct {v0}, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool$PoolStats;-><init>()V

    iput-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool;->stats:Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool$PoolStats;

    .line 59
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool;->stack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->clear()V

    return-void
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method get()Landroid/view/View;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .line 36
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool;->stack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 37
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool;->stats:Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool$PoolStats;

    iget v1, v0, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool$PoolStats;->hits:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool$PoolStats;->hits:I

    .line 38
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool;->stats:Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool$PoolStats;

    iget v1, v0, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool$PoolStats;->size:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool$PoolStats;->size:I

    .line 39
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool;->stack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0

    .line 42
    :cond_0
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool;->stats:Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool$PoolStats;

    iget v1, v0, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool$PoolStats;->misses:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool$PoolStats;->misses:I

    .line 44
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool;->factory:Lcom/felipecsl/asymmetricgridview/library/widget/PoolObjectFactory;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/felipecsl/asymmetricgridview/library/widget/PoolObjectFactory;->createObject()Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_2

    .line 47
    iget-object v1, p0, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool;->stats:Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool$PoolStats;

    iget v2, v1, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool$PoolStats;->created:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool$PoolStats;->created:I

    :cond_2
    return-object v0
.end method

.method getStats(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool;->stats:Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool$PoolStats;

    invoke-virtual {v0, p1}, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool$PoolStats;->getStats(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method put(Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .line 53
    iget-object v0, p0, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool;->stack:Ljava/util/Stack;

    invoke-virtual {v0, p1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    iget-object p1, p0, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool;->stats:Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool$PoolStats;

    iget v0, p1, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool$PoolStats;->size:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Lcom/felipecsl/asymmetricgridview/library/widget/ViewPool$PoolStats;->size:I

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    return-void
.end method

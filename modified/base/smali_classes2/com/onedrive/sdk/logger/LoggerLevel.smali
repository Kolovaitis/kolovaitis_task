.class public final enum Lcom/onedrive/sdk/logger/LoggerLevel;
.super Ljava/lang/Enum;
.source "LoggerLevel.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/onedrive/sdk/logger/LoggerLevel;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/onedrive/sdk/logger/LoggerLevel;

.field public static final enum Debug:Lcom/onedrive/sdk/logger/LoggerLevel;

.field public static final enum Error:Lcom/onedrive/sdk/logger/LoggerLevel;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 32
    new-instance v0, Lcom/onedrive/sdk/logger/LoggerLevel;

    const-string v1, "Error"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/onedrive/sdk/logger/LoggerLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/logger/LoggerLevel;->Error:Lcom/onedrive/sdk/logger/LoggerLevel;

    .line 37
    new-instance v0, Lcom/onedrive/sdk/logger/LoggerLevel;

    const-string v1, "Debug"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/onedrive/sdk/logger/LoggerLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/logger/LoggerLevel;->Debug:Lcom/onedrive/sdk/logger/LoggerLevel;

    const/4 v0, 0x2

    .line 28
    new-array v0, v0, [Lcom/onedrive/sdk/logger/LoggerLevel;

    sget-object v1, Lcom/onedrive/sdk/logger/LoggerLevel;->Error:Lcom/onedrive/sdk/logger/LoggerLevel;

    aput-object v1, v0, v2

    sget-object v1, Lcom/onedrive/sdk/logger/LoggerLevel;->Debug:Lcom/onedrive/sdk/logger/LoggerLevel;

    aput-object v1, v0, v3

    sput-object v0, Lcom/onedrive/sdk/logger/LoggerLevel;->$VALUES:[Lcom/onedrive/sdk/logger/LoggerLevel;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 28
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/onedrive/sdk/logger/LoggerLevel;
    .locals 1

    .line 28
    const-class v0, Lcom/onedrive/sdk/logger/LoggerLevel;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/onedrive/sdk/logger/LoggerLevel;

    return-object p0
.end method

.method public static values()[Lcom/onedrive/sdk/logger/LoggerLevel;
    .locals 1

    .line 28
    sget-object v0, Lcom/onedrive/sdk/logger/LoggerLevel;->$VALUES:[Lcom/onedrive/sdk/logger/LoggerLevel;

    invoke-virtual {v0}, [Lcom/onedrive/sdk/logger/LoggerLevel;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/onedrive/sdk/logger/LoggerLevel;

    return-object v0
.end method

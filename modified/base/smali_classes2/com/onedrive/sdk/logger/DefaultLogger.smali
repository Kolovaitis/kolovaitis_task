.class public Lcom/onedrive/sdk/logger/DefaultLogger;
.super Ljava/lang/Object;
.source "DefaultLogger.java"

# interfaces
.implements Lcom/onedrive/sdk/logger/ILogger;


# instance fields
.field private mLevel:Lcom/onedrive/sdk/logger/LoggerLevel;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    sget-object v0, Lcom/onedrive/sdk/logger/LoggerLevel;->Error:Lcom/onedrive/sdk/logger/LoggerLevel;

    iput-object v0, p0, Lcom/onedrive/sdk/logger/DefaultLogger;->mLevel:Lcom/onedrive/sdk/logger/LoggerLevel;

    return-void
.end method

.method private getTag()Ljava/lang/String;
    .locals 4

    .line 61
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 63
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v1

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-virtual {v1}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v1

    const-string v3, "."

    .line 64
    invoke-virtual {v1, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "["

    .line 65
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 66
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v1

    aget-object v1, v1, v2

    invoke-virtual {v1}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "] - "

    .line 67
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v1

    aget-object v1, v1, v2

    invoke-virtual {v1}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 69
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "DefaultLogger"

    .line 71
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return-object v0
.end method


# virtual methods
.method public getLoggingLevel()Lcom/onedrive/sdk/logger/LoggerLevel;
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/onedrive/sdk/logger/DefaultLogger;->mLevel:Lcom/onedrive/sdk/logger/LoggerLevel;

    return-object v0
.end method

.method public logDebug(Ljava/lang/String;)V
    .locals 2

    .line 82
    sget-object v0, Lcom/onedrive/sdk/logger/DefaultLogger$1;->$SwitchMap$com$onedrive$sdk$logger$LoggerLevel:[I

    iget-object v1, p0, Lcom/onedrive/sdk/logger/DefaultLogger;->mLevel:Lcom/onedrive/sdk/logger/LoggerLevel;

    invoke-virtual {v1}, Lcom/onedrive/sdk/logger/LoggerLevel;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 84
    :cond_0
    invoke-direct {p0}, Lcom/onedrive/sdk/logger/DefaultLogger;->getTag()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method public logError(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .line 97
    sget-object v0, Lcom/onedrive/sdk/logger/DefaultLogger$1;->$SwitchMap$com$onedrive$sdk$logger$LoggerLevel:[I

    iget-object v1, p0, Lcom/onedrive/sdk/logger/DefaultLogger;->mLevel:Lcom/onedrive/sdk/logger/LoggerLevel;

    invoke-virtual {v1}, Lcom/onedrive/sdk/logger/LoggerLevel;->ordinal()I

    move-result v1

    aget v0, v0, v1

    .line 101
    invoke-direct {p0}, Lcom/onedrive/sdk/logger/DefaultLogger;->getTag()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    return-void
.end method

.method public setLoggingLevel(Lcom/onedrive/sdk/logger/LoggerLevel;)V
    .locals 3

    .line 42
    invoke-direct {p0}, Lcom/onedrive/sdk/logger/DefaultLogger;->getTag()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Setting logging level to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    iput-object p1, p0, Lcom/onedrive/sdk/logger/DefaultLogger;->mLevel:Lcom/onedrive/sdk/logger/LoggerLevel;

    return-void
.end method

.class public Lcom/onedrive/sdk/http/OneDriveServiceException;
.super Lcom/onedrive/sdk/core/ClientException;
.source "OneDriveServiceException.java"


# static fields
.field public static final INDENT_SPACES:I = 0x3

.field public static final INTERNAL_SERVER_ERROR:I = 0x1f4

.field protected static final MAX_BREVITY_LENGTH:I = 0x32

.field protected static final MAX_BYTE_COUNT_BEFORE_TRUNCATION:I = 0x8

.field protected static final NEW_LINE:C = '\n'

.field protected static final TRUNCATION_MARKER:Ljava/lang/String; = "[...]"

.field protected static final X_THROWSITE:Ljava/lang/String; = "x-throwsite"


# instance fields
.field private final mError:Lcom/onedrive/sdk/http/OneDriveErrorResponse;

.field private final mMethod:Ljava/lang/String;

.field private final mRequestBody:Ljava/lang/String;

.field private final mRequestHeaders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mResponseCode:I

.field private final mResponseHeaders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mResponseMessage:Ljava/lang/String;

.field private final mUrl:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;ILjava/lang/String;Ljava/util/List;Lcom/onedrive/sdk/http/OneDriveErrorResponse;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/onedrive/sdk/http/OneDriveErrorResponse;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x0

    .line 138
    invoke-direct {p0, p6, v0, v0}, Lcom/onedrive/sdk/core/ClientException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lcom/onedrive/sdk/core/OneDriveErrorCodes;)V

    .line 139
    iput-object p1, p0, Lcom/onedrive/sdk/http/OneDriveServiceException;->mMethod:Ljava/lang/String;

    .line 140
    iput-object p2, p0, Lcom/onedrive/sdk/http/OneDriveServiceException;->mUrl:Ljava/lang/String;

    .line 141
    iput-object p3, p0, Lcom/onedrive/sdk/http/OneDriveServiceException;->mRequestHeaders:Ljava/util/List;

    .line 142
    iput-object p4, p0, Lcom/onedrive/sdk/http/OneDriveServiceException;->mRequestBody:Ljava/lang/String;

    .line 143
    iput p5, p0, Lcom/onedrive/sdk/http/OneDriveServiceException;->mResponseCode:I

    .line 144
    iput-object p6, p0, Lcom/onedrive/sdk/http/OneDriveServiceException;->mResponseMessage:Ljava/lang/String;

    .line 145
    iput-object p7, p0, Lcom/onedrive/sdk/http/OneDriveServiceException;->mResponseHeaders:Ljava/util/List;

    .line 146
    iput-object p8, p0, Lcom/onedrive/sdk/http/OneDriveServiceException;->mError:Lcom/onedrive/sdk/http/OneDriveErrorResponse;

    return-void
.end method

.method public static createFromConnection(Lcom/onedrive/sdk/http/IHttpRequest;Ljava/lang/Object;Lcom/onedrive/sdk/serializer/ISerializer;Lcom/onedrive/sdk/http/IConnection;)Lcom/onedrive/sdk/http/OneDriveServiceException;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/onedrive/sdk/http/IHttpRequest;",
            "TT;",
            "Lcom/onedrive/sdk/serializer/ISerializer;",
            "Lcom/onedrive/sdk/http/IConnection;",
            ")",
            "Lcom/onedrive/sdk/http/OneDriveServiceException;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 259
    invoke-interface {p3}, Lcom/onedrive/sdk/http/IConnection;->getRequestMethod()Ljava/lang/String;

    move-result-object v1

    .line 260
    invoke-interface {p0}, Lcom/onedrive/sdk/http/IHttpRequest;->getRequestUrl()Ljava/net/URL;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v2

    .line 261
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    .line 262
    invoke-interface {p0}, Lcom/onedrive/sdk/http/IHttpRequest;->getHeaders()Ljava/util/List;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/onedrive/sdk/options/HeaderOption;

    .line 263
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/onedrive/sdk/options/HeaderOption;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/onedrive/sdk/options/HeaderOption;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 266
    :cond_0
    instance-of p0, p1, [B

    const/4 v0, 0x0

    if-eqz p0, :cond_3

    .line 267
    check-cast p1, [B

    .line 268
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "byte["

    .line 269
    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    array-length v4, p1

    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, "]"

    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " {"

    .line 271
    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v4, 0x0

    :goto_1
    const/16 v5, 0x8

    if-ge v4, v5, :cond_1

    .line 272
    array-length v6, p1

    if-ge v4, v6, :cond_1

    .line 273
    aget-byte v5, p1, v4

    invoke-virtual {p0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v5, ", "

    invoke-virtual {p0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 275
    :cond_1
    array-length p1, p1

    if-le p1, v5, :cond_2

    const-string p1, "[...]"

    .line 276
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "}"

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 278
    :cond_2
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    move-object v4, p0

    goto :goto_2

    :cond_3
    if-eqz p1, :cond_4

    .line 280
    invoke-interface {p2, p1}, Lcom/onedrive/sdk/serializer/ISerializer;->serializeObject(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    move-object v4, p0

    goto :goto_2

    :cond_4
    move-object v4, v0

    .line 285
    :goto_2
    invoke-interface {p3}, Lcom/onedrive/sdk/http/IConnection;->getResponseCode()I

    move-result v5

    .line 286
    new-instance v7, Ljava/util/LinkedList;

    invoke-direct {v7}, Ljava/util/LinkedList;-><init>()V

    .line 287
    invoke-interface {p3}, Lcom/onedrive/sdk/http/IConnection;->getHeaders()Ljava/util/Map;

    move-result-object p0

    .line 288
    invoke-interface {p0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    if-nez v6, :cond_5

    const-string v8, ""

    goto :goto_4

    .line 293
    :cond_5
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, " : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 295
    :goto_4
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 298
    :cond_6
    invoke-interface {p3}, Lcom/onedrive/sdk/http/IConnection;->getResponseMessage()Ljava/lang/String;

    move-result-object v6

    .line 299
    invoke-interface {p3}, Lcom/onedrive/sdk/http/IConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object p1

    invoke-static {p1}, Lcom/onedrive/sdk/http/DefaultHttpProvider;->streamToString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object p1

    const-string p3, "Content-Type"

    .line 303
    invoke-interface {p0, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    if-eqz p0, :cond_7

    const-string p3, "application/json"

    .line 304
    invoke-virtual {p0, p3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 306
    :try_start_0
    const-class p0, Lcom/onedrive/sdk/http/OneDriveErrorResponse;

    invoke-interface {p2, p1, p0}, Lcom/onedrive/sdk/serializer/ISerializer;->deserializeObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/onedrive/sdk/http/OneDriveErrorResponse;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v10, v0

    move-object v0, p0

    move-object p0, v10

    goto :goto_5

    :catch_0
    move-exception p0

    goto :goto_5

    :cond_7
    move-object p0, v0

    :goto_5
    if-nez v0, :cond_9

    .line 313
    new-instance p2, Lcom/onedrive/sdk/http/OneDriveErrorResponse;

    invoke-direct {p2}, Lcom/onedrive/sdk/http/OneDriveErrorResponse;-><init>()V

    .line 314
    new-instance p3, Lcom/onedrive/sdk/http/OneDriveError;

    invoke-direct {p3}, Lcom/onedrive/sdk/http/OneDriveError;-><init>()V

    iput-object p3, p2, Lcom/onedrive/sdk/http/OneDriveErrorResponse;->error:Lcom/onedrive/sdk/http/OneDriveError;

    .line 315
    iget-object p3, p2, Lcom/onedrive/sdk/http/OneDriveErrorResponse;->error:Lcom/onedrive/sdk/http/OneDriveError;

    const-string v0, "Unable to parse error response message"

    iput-object v0, p3, Lcom/onedrive/sdk/http/OneDriveError;->code:Ljava/lang/String;

    .line 316
    iget-object p3, p2, Lcom/onedrive/sdk/http/OneDriveErrorResponse;->error:Lcom/onedrive/sdk/http/OneDriveError;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Raw error: "

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p3, Lcom/onedrive/sdk/http/OneDriveError;->message:Ljava/lang/String;

    if-eqz p0, :cond_8

    .line 318
    iget-object p1, p2, Lcom/onedrive/sdk/http/OneDriveErrorResponse;->error:Lcom/onedrive/sdk/http/OneDriveError;

    new-instance p3, Lcom/onedrive/sdk/http/OneDriveInnerError;

    invoke-direct {p3}, Lcom/onedrive/sdk/http/OneDriveInnerError;-><init>()V

    iput-object p3, p1, Lcom/onedrive/sdk/http/OneDriveError;->innererror:Lcom/onedrive/sdk/http/OneDriveInnerError;

    .line 319
    iget-object p1, p2, Lcom/onedrive/sdk/http/OneDriveErrorResponse;->error:Lcom/onedrive/sdk/http/OneDriveError;

    iget-object p1, p1, Lcom/onedrive/sdk/http/OneDriveError;->innererror:Lcom/onedrive/sdk/http/OneDriveInnerError;

    invoke-virtual {p0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p0

    iput-object p0, p1, Lcom/onedrive/sdk/http/OneDriveInnerError;->code:Ljava/lang/String;

    :cond_8
    move-object v8, p2

    goto :goto_6

    :cond_9
    move-object v8, v0

    :goto_6
    const/16 p0, 0x1f4

    if-ne v5, p0, :cond_a

    .line 324
    new-instance p0, Lcom/onedrive/sdk/http/OneDriveFatalServiceException;

    move-object v0, p0

    invoke-direct/range {v0 .. v8}, Lcom/onedrive/sdk/http/OneDriveFatalServiceException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;ILjava/lang/String;Ljava/util/List;Lcom/onedrive/sdk/http/OneDriveErrorResponse;)V

    return-object p0

    .line 334
    :cond_a
    new-instance p0, Lcom/onedrive/sdk/http/OneDriveServiceException;

    move-object v0, p0

    invoke-direct/range {v0 .. v8}, Lcom/onedrive/sdk/http/OneDriveServiceException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;ILjava/lang/String;Ljava/util/List;Lcom/onedrive/sdk/http/OneDriveErrorResponse;)V

    return-object p0
.end method


# virtual methods
.method public getMessage()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    .line 151
    invoke-virtual {p0, v0}, Lcom/onedrive/sdk/http/OneDriveServiceException;->getMessage(Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMessage(Z)Ljava/lang/String;
    .locals 7

    .line 168
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 169
    iget-object v1, p0, Lcom/onedrive/sdk/http/OneDriveServiceException;->mError:Lcom/onedrive/sdk/http/OneDriveErrorResponse;

    const/16 v2, 0xa

    if-eqz v1, :cond_0

    iget-object v1, v1, Lcom/onedrive/sdk/http/OneDriveErrorResponse;->error:Lcom/onedrive/sdk/http/OneDriveError;

    if-eqz v1, :cond_0

    const-string v1, "Error code: "

    .line 170
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/onedrive/sdk/http/OneDriveServiceException;->mError:Lcom/onedrive/sdk/http/OneDriveErrorResponse;

    iget-object v1, v1, Lcom/onedrive/sdk/http/OneDriveErrorResponse;->error:Lcom/onedrive/sdk/http/OneDriveError;

    iget-object v1, v1, Lcom/onedrive/sdk/http/OneDriveError;->code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, "Error message: "

    .line 171
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/onedrive/sdk/http/OneDriveServiceException;->mError:Lcom/onedrive/sdk/http/OneDriveErrorResponse;

    iget-object v1, v1, Lcom/onedrive/sdk/http/OneDriveErrorResponse;->error:Lcom/onedrive/sdk/http/OneDriveError;

    iget-object v1, v1, Lcom/onedrive/sdk/http/OneDriveError;->message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 172
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 175
    :cond_0
    iget-object v1, p0, Lcom/onedrive/sdk/http/OneDriveServiceException;->mMethod:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/onedrive/sdk/http/OneDriveServiceException;->mUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 176
    iget-object v1, p0, Lcom/onedrive/sdk/http/OneDriveServiceException;->mRequestHeaders:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const/4 v4, 0x0

    const/16 v5, 0x32

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    if-eqz p1, :cond_1

    .line 178
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 180
    :cond_1
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v6

    invoke-virtual {v3, v4, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 181
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 182
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-ne v3, v5, :cond_2

    const-string v3, "[...]"

    .line 183
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 186
    :cond_2
    :goto_1
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 188
    :cond_3
    iget-object v1, p0, Lcom/onedrive/sdk/http/OneDriveServiceException;->mRequestBody:Ljava/lang/String;

    if-eqz v1, :cond_5

    if-eqz p1, :cond_4

    .line 190
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 192
    :cond_4
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-static {v5, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 193
    iget-object v3, p0, Lcom/onedrive/sdk/http/OneDriveServiceException;->mRequestBody:Ljava/lang/String;

    invoke-virtual {v3, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 194
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 195
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-ne v1, v5, :cond_5

    const-string v1, "[...]"

    .line 196
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 200
    :cond_5
    :goto_2
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 203
    iget v1, p0, Lcom/onedrive/sdk/http/OneDriveServiceException;->mResponseCode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/onedrive/sdk/http/OneDriveServiceException;->mResponseMessage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 204
    iget-object v1, p0, Lcom/onedrive/sdk/http/OneDriveServiceException;->mResponseHeaders:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    if-eqz p1, :cond_7

    .line 206
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 208
    :cond_7
    sget-object v4, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "x-throwsite"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 209
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_3

    :cond_8
    if-eqz p1, :cond_9

    .line 213
    iget-object p1, p0, Lcom/onedrive/sdk/http/OneDriveServiceException;->mError:Lcom/onedrive/sdk/http/OneDriveErrorResponse;

    if-eqz p1, :cond_9

    iget-object p1, p1, Lcom/onedrive/sdk/http/OneDriveErrorResponse;->rawObject:Lcom/google/gson/JsonObject;

    if-eqz p1, :cond_9

    .line 215
    :try_start_0
    new-instance p1, Lorg/json/JSONObject;

    iget-object v1, p0, Lcom/onedrive/sdk/http/OneDriveServiceException;->mError:Lcom/onedrive/sdk/http/OneDriveErrorResponse;

    iget-object v1, v1, Lcom/onedrive/sdk/http/OneDriveErrorResponse;->rawObject:Lcom/google/gson/JsonObject;

    invoke-virtual {v1}, Lcom/google/gson/JsonObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p1, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x3

    .line 216
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_4

    :catch_0
    const-string p1, "[Warning: Unable to parse error message body]"

    .line 218
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 219
    iget-object p1, p0, Lcom/onedrive/sdk/http/OneDriveServiceException;->mError:Lcom/onedrive/sdk/http/OneDriveErrorResponse;

    iget-object p1, p1, Lcom/onedrive/sdk/http/OneDriveErrorResponse;->rawObject:Lcom/google/gson/JsonObject;

    invoke-virtual {p1}, Lcom/google/gson/JsonObject;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_4

    :cond_9
    const-string p1, "[...]"

    .line 222
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string p1, "[Some information was truncated for brevity, enable debug logging for more details]"

    .line 223
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 225
    :goto_4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getResponseHeaders()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 159
    iget-object v0, p0, Lcom/onedrive/sdk/http/OneDriveServiceException;->mResponseHeaders:Ljava/util/List;

    return-object v0
.end method

.method public getServiceError()Lcom/onedrive/sdk/http/OneDriveError;
    .locals 1

    .line 233
    iget-object v0, p0, Lcom/onedrive/sdk/http/OneDriveServiceException;->mError:Lcom/onedrive/sdk/http/OneDriveErrorResponse;

    iget-object v0, v0, Lcom/onedrive/sdk/http/OneDriveErrorResponse;->error:Lcom/onedrive/sdk/http/OneDriveError;

    return-object v0
.end method

.method public isError(Lcom/onedrive/sdk/core/OneDriveErrorCodes;)Z
    .locals 1

    .line 238
    invoke-virtual {p0}, Lcom/onedrive/sdk/http/OneDriveServiceException;->getServiceError()Lcom/onedrive/sdk/http/OneDriveError;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 239
    invoke-virtual {p0}, Lcom/onedrive/sdk/http/OneDriveServiceException;->getServiceError()Lcom/onedrive/sdk/http/OneDriveError;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/onedrive/sdk/http/OneDriveError;->isError(Lcom/onedrive/sdk/core/OneDriveErrorCodes;)Z

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.class public interface abstract Lcom/onedrive/sdk/http/IRequestBuilder;
.super Ljava/lang/Object;
.source "IRequestBuilder.java"


# virtual methods
.method public abstract getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;
.end method

.method public abstract getRequestUrl()Ljava/lang/String;
.end method

.method public abstract getRequestUrlWithAdditionalSegment(Ljava/lang/String;)Ljava/lang/String;
.end method

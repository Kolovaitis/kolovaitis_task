.class public Lcom/onedrive/sdk/http/OneDriveErrorResponse;
.super Ljava/lang/Object;
.source "OneDriveErrorResponse.java"

# interfaces
.implements Lcom/onedrive/sdk/serializer/IJsonBackedObject;


# instance fields
.field public error:Lcom/onedrive/sdk/http/OneDriveError;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "error"
    .end annotation
.end field

.field public rawObject:Lcom/google/gson/JsonObject;
    .annotation runtime Lcom/google/gson/annotations/Expose;
        deserialize = false
        serialize = false
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setRawObject(Lcom/onedrive/sdk/serializer/ISerializer;Lcom/google/gson/JsonObject;)V
    .locals 0

    .line 47
    iput-object p2, p0, Lcom/onedrive/sdk/http/OneDriveErrorResponse;->rawObject:Lcom/google/gson/JsonObject;

    return-void
.end method

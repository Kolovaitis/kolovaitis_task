.class public abstract Lcom/onedrive/sdk/http/BaseStreamRequest;
.super Ljava/lang/Object;
.source "BaseStreamRequest.java"

# interfaces
.implements Lcom/onedrive/sdk/http/IHttpStreamRequest;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/onedrive/sdk/http/IHttpStreamRequest;"
    }
.end annotation


# instance fields
.field private final mBaseRequest:Lcom/onedrive/sdk/http/BaseRequest;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;Ljava/lang/Class;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/onedrive/sdk/extensions/IOneDriveClient;",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;",
            "Ljava/lang/Class<",
            "TT;>;)V"
        }
    .end annotation

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    new-instance v6, Lcom/onedrive/sdk/http/BaseStreamRequest$1;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/onedrive/sdk/http/BaseStreamRequest$1;-><init>(Lcom/onedrive/sdk/http/BaseStreamRequest;Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;Ljava/lang/Class;)V

    iput-object v6, p0, Lcom/onedrive/sdk/http/BaseStreamRequest;->mBaseRequest:Lcom/onedrive/sdk/http/BaseRequest;

    return-void
.end method


# virtual methods
.method public addHeader(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 126
    iget-object v0, p0, Lcom/onedrive/sdk/http/BaseStreamRequest;->mBaseRequest:Lcom/onedrive/sdk/http/BaseRequest;

    invoke-virtual {v0, p1, p2}, Lcom/onedrive/sdk/http/BaseRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public getHeaders()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/HeaderOption;",
            ">;"
        }
    .end annotation

    .line 135
    iget-object v0, p0, Lcom/onedrive/sdk/http/BaseStreamRequest;->mBaseRequest:Lcom/onedrive/sdk/http/BaseRequest;

    invoke-virtual {v0}, Lcom/onedrive/sdk/http/BaseRequest;->getHeaders()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getHttpMethod()Lcom/onedrive/sdk/http/HttpMethod;
    .locals 1

    .line 116
    iget-object v0, p0, Lcom/onedrive/sdk/http/BaseStreamRequest;->mBaseRequest:Lcom/onedrive/sdk/http/BaseRequest;

    invoke-virtual {v0}, Lcom/onedrive/sdk/http/BaseRequest;->getHttpMethod()Lcom/onedrive/sdk/http/HttpMethod;

    move-result-object v0

    return-object v0
.end method

.method public getOptions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;"
        }
    .end annotation

    .line 144
    iget-object v0, p0, Lcom/onedrive/sdk/http/BaseStreamRequest;->mBaseRequest:Lcom/onedrive/sdk/http/BaseRequest;

    invoke-virtual {v0}, Lcom/onedrive/sdk/http/BaseRequest;->getOptions()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getRequestUrl()Ljava/net/URL;
    .locals 1

    .line 107
    iget-object v0, p0, Lcom/onedrive/sdk/http/BaseStreamRequest;->mBaseRequest:Lcom/onedrive/sdk/http/BaseRequest;

    invoke-virtual {v0}, Lcom/onedrive/sdk/http/BaseRequest;->getRequestUrl()Ljava/net/URL;

    move-result-object v0

    return-object v0
.end method

.method protected send()Ljava/io/InputStream;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    .line 75
    iget-object v0, p0, Lcom/onedrive/sdk/http/BaseStreamRequest;->mBaseRequest:Lcom/onedrive/sdk/http/BaseRequest;

    sget-object v1, Lcom/onedrive/sdk/http/HttpMethod;->GET:Lcom/onedrive/sdk/http/HttpMethod;

    invoke-virtual {v0, v1}, Lcom/onedrive/sdk/http/BaseRequest;->setHttpMethod(Lcom/onedrive/sdk/http/HttpMethod;)V

    .line 76
    iget-object v0, p0, Lcom/onedrive/sdk/http/BaseStreamRequest;->mBaseRequest:Lcom/onedrive/sdk/http/BaseRequest;

    invoke-virtual {v0}, Lcom/onedrive/sdk/http/BaseRequest;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v0

    invoke-interface {v0}, Lcom/onedrive/sdk/extensions/IOneDriveClient;->getHttpProvider()Lcom/onedrive/sdk/http/IHttpProvider;

    move-result-object v0

    const-class v1, Ljava/io/InputStream;

    const/4 v2, 0x0

    invoke-interface {v0, p0, v1, v2}, Lcom/onedrive/sdk/http/IHttpProvider;->send(Lcom/onedrive/sdk/http/IHttpRequest;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    return-object v0
.end method

.method protected send([B)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)TT;"
        }
    .end annotation

    .line 97
    iget-object v0, p0, Lcom/onedrive/sdk/http/BaseStreamRequest;->mBaseRequest:Lcom/onedrive/sdk/http/BaseRequest;

    sget-object v1, Lcom/onedrive/sdk/http/HttpMethod;->PUT:Lcom/onedrive/sdk/http/HttpMethod;

    invoke-virtual {v0, v1}, Lcom/onedrive/sdk/http/BaseRequest;->setHttpMethod(Lcom/onedrive/sdk/http/HttpMethod;)V

    .line 98
    iget-object v0, p0, Lcom/onedrive/sdk/http/BaseStreamRequest;->mBaseRequest:Lcom/onedrive/sdk/http/BaseRequest;

    invoke-virtual {v0}, Lcom/onedrive/sdk/http/BaseRequest;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v0

    invoke-interface {v0}, Lcom/onedrive/sdk/extensions/IOneDriveClient;->getHttpProvider()Lcom/onedrive/sdk/http/IHttpProvider;

    move-result-object v0

    iget-object v1, p0, Lcom/onedrive/sdk/http/BaseStreamRequest;->mBaseRequest:Lcom/onedrive/sdk/http/BaseRequest;

    invoke-virtual {v1}, Lcom/onedrive/sdk/http/BaseRequest;->getResponseType()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, p0, v1, p1}, Lcom/onedrive/sdk/http/IHttpProvider;->send(Lcom/onedrive/sdk/http/IHttpRequest;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method protected send(Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Ljava/io/InputStream;",
            ">;)V"
        }
    .end annotation

    .line 65
    iget-object v0, p0, Lcom/onedrive/sdk/http/BaseStreamRequest;->mBaseRequest:Lcom/onedrive/sdk/http/BaseRequest;

    sget-object v1, Lcom/onedrive/sdk/http/HttpMethod;->GET:Lcom/onedrive/sdk/http/HttpMethod;

    invoke-virtual {v0, v1}, Lcom/onedrive/sdk/http/BaseRequest;->setHttpMethod(Lcom/onedrive/sdk/http/HttpMethod;)V

    .line 66
    iget-object v0, p0, Lcom/onedrive/sdk/http/BaseStreamRequest;->mBaseRequest:Lcom/onedrive/sdk/http/BaseRequest;

    invoke-virtual {v0}, Lcom/onedrive/sdk/http/BaseRequest;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v0

    invoke-interface {v0}, Lcom/onedrive/sdk/extensions/IOneDriveClient;->getHttpProvider()Lcom/onedrive/sdk/http/IHttpProvider;

    move-result-object v0

    const-class v1, Ljava/io/InputStream;

    const/4 v2, 0x0

    invoke-interface {v0, p0, p1, v1, v2}, Lcom/onedrive/sdk/http/IHttpProvider;->send(Lcom/onedrive/sdk/http/IHttpRequest;Lcom/onedrive/sdk/concurrency/ICallback;Ljava/lang/Class;Ljava/lang/Object;)V

    return-void
.end method

.method protected send([BLcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "TT;>;)V"
        }
    .end annotation

    .line 86
    iget-object v0, p0, Lcom/onedrive/sdk/http/BaseStreamRequest;->mBaseRequest:Lcom/onedrive/sdk/http/BaseRequest;

    sget-object v1, Lcom/onedrive/sdk/http/HttpMethod;->PUT:Lcom/onedrive/sdk/http/HttpMethod;

    invoke-virtual {v0, v1}, Lcom/onedrive/sdk/http/BaseRequest;->setHttpMethod(Lcom/onedrive/sdk/http/HttpMethod;)V

    .line 87
    iget-object v0, p0, Lcom/onedrive/sdk/http/BaseStreamRequest;->mBaseRequest:Lcom/onedrive/sdk/http/BaseRequest;

    invoke-virtual {v0}, Lcom/onedrive/sdk/http/BaseRequest;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v0

    invoke-interface {v0}, Lcom/onedrive/sdk/extensions/IOneDriveClient;->getHttpProvider()Lcom/onedrive/sdk/http/IHttpProvider;

    move-result-object v0

    iget-object v1, p0, Lcom/onedrive/sdk/http/BaseStreamRequest;->mBaseRequest:Lcom/onedrive/sdk/http/BaseRequest;

    invoke-virtual {v1}, Lcom/onedrive/sdk/http/BaseRequest;->getResponseType()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, p0, p2, v1, p1}, Lcom/onedrive/sdk/http/IHttpProvider;->send(Lcom/onedrive/sdk/http/IHttpRequest;Lcom/onedrive/sdk/concurrency/ICallback;Ljava/lang/Class;Ljava/lang/Object;)V

    return-void
.end method

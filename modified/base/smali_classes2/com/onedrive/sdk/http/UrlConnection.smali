.class public Lcom/onedrive/sdk/http/UrlConnection;
.super Ljava/lang/Object;
.source "UrlConnection.java"

# interfaces
.implements Lcom/onedrive/sdk/http/IConnection;


# instance fields
.field private final mConnection:Ljava/net/HttpURLConnection;

.field private mHeaders:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/onedrive/sdk/http/IHttpRequest;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    invoke-interface {p1}, Lcom/onedrive/sdk/http/IHttpRequest;->getRequestUrl()Ljava/net/URL;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    iput-object v0, p0, Lcom/onedrive/sdk/http/UrlConnection;->mConnection:Ljava/net/HttpURLConnection;

    .line 58
    invoke-interface {p1}, Lcom/onedrive/sdk/http/IHttpRequest;->getHeaders()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/onedrive/sdk/options/HeaderOption;

    .line 59
    iget-object v2, p0, Lcom/onedrive/sdk/http/UrlConnection;->mConnection:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Lcom/onedrive/sdk/options/HeaderOption;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/onedrive/sdk/options/HeaderOption;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Ljava/net/HttpURLConnection;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 63
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/onedrive/sdk/http/UrlConnection;->mConnection:Ljava/net/HttpURLConnection;

    invoke-interface {p1}, Lcom/onedrive/sdk/http/IHttpRequest;->getHttpMethod()Lcom/onedrive/sdk/http/HttpMethod;

    move-result-object v1

    invoke-virtual {v1}, Lcom/onedrive/sdk/http/HttpMethod;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/ProtocolException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 66
    :catch_0
    iget-object v0, p0, Lcom/onedrive/sdk/http/UrlConnection;->mConnection:Ljava/net/HttpURLConnection;

    sget-object v1, Lcom/onedrive/sdk/http/HttpMethod;->POST:Lcom/onedrive/sdk/http/HttpMethod;

    invoke-virtual {v1}, Lcom/onedrive/sdk/http/HttpMethod;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 67
    iget-object v0, p0, Lcom/onedrive/sdk/http/UrlConnection;->mConnection:Ljava/net/HttpURLConnection;

    const-string v1, "X-HTTP-Method-Override"

    invoke-interface {p1}, Lcom/onedrive/sdk/http/IHttpRequest;->getHttpMethod()Lcom/onedrive/sdk/http/HttpMethod;

    move-result-object v2

    invoke-virtual {v2}, Lcom/onedrive/sdk/http/HttpMethod;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/net/HttpURLConnection;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    iget-object v0, p0, Lcom/onedrive/sdk/http/UrlConnection;->mConnection:Ljava/net/HttpURLConnection;

    const-string v1, "X-HTTP-Method"

    invoke-interface {p1}, Lcom/onedrive/sdk/http/IHttpRequest;->getHttpMethod()Lcom/onedrive/sdk/http/HttpMethod;

    move-result-object p1

    invoke-virtual {p1}, Lcom/onedrive/sdk/http/HttpMethod;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Ljava/net/HttpURLConnection;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    return-void
.end method

.method private static getResponseHeaders(Ljava/net/HttpURLConnection;)Ljava/util/HashMap;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/net/HttpURLConnection;",
            ")",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 137
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const/4 v1, 0x0

    .line 140
    :goto_0
    invoke-virtual {p0, v1}, Ljava/net/HttpURLConnection;->getHeaderFieldKey(I)Ljava/lang/String;

    move-result-object v2

    .line 141
    invoke-virtual {p0, v1}, Ljava/net/HttpURLConnection;->getHeaderField(I)Ljava/lang/String;

    move-result-object v3

    if-nez v2, :cond_0

    if-nez v3, :cond_0

    return-object v0

    .line 145
    :cond_0
    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public addRequestHeader(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/onedrive/sdk/http/UrlConnection;->mConnection:Ljava/net/HttpURLConnection;

    invoke-virtual {v0, p1, p2}, Ljava/net/HttpURLConnection;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public close()V
    .locals 1

    .line 110
    iget-object v0, p0, Lcom/onedrive/sdk/http/UrlConnection;->mConnection:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    return-void
.end method

.method public getHeaders()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 115
    iget-object v0, p0, Lcom/onedrive/sdk/http/UrlConnection;->mHeaders:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/onedrive/sdk/http/UrlConnection;->mConnection:Ljava/net/HttpURLConnection;

    invoke-static {v0}, Lcom/onedrive/sdk/http/UrlConnection;->getResponseHeaders(Ljava/net/HttpURLConnection;)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/onedrive/sdk/http/UrlConnection;->mHeaders:Ljava/util/HashMap;

    .line 118
    :cond_0
    iget-object v0, p0, Lcom/onedrive/sdk/http/UrlConnection;->mHeaders:Ljava/util/HashMap;

    return-object v0
.end method

.method public getInputStream()Ljava/io/InputStream;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 91
    iget-object v0, p0, Lcom/onedrive/sdk/http/UrlConnection;->mConnection:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v0

    const/16 v1, 0x190

    if-lt v0, v1, :cond_0

    .line 92
    iget-object v0, p0, Lcom/onedrive/sdk/http/UrlConnection;->mConnection:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getErrorStream()Ljava/io/InputStream;

    move-result-object v0

    return-object v0

    .line 94
    :cond_0
    iget-object v0, p0, Lcom/onedrive/sdk/http/UrlConnection;->mConnection:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public getOutputStream()Ljava/io/OutputStream;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 84
    iget-object v0, p0, Lcom/onedrive/sdk/http/UrlConnection;->mConnection:Ljava/net/HttpURLConnection;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 85
    iget-object v0, p0, Lcom/onedrive/sdk/http/UrlConnection;->mConnection:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    return-object v0
.end method

.method public getRequestMethod()Ljava/lang/String;
    .locals 1

    .line 123
    iget-object v0, p0, Lcom/onedrive/sdk/http/UrlConnection;->mConnection:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getRequestMethod()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getResponseCode()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 100
    iget-object v0, p0, Lcom/onedrive/sdk/http/UrlConnection;->mConnection:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v0

    return v0
.end method

.method public getResponseMessage()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 105
    iget-object v0, p0, Lcom/onedrive/sdk/http/UrlConnection;->mConnection:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseMessage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setContentLength(I)V
    .locals 1

    .line 128
    iget-object v0, p0, Lcom/onedrive/sdk/http/UrlConnection;->mConnection:Ljava/net/HttpURLConnection;

    invoke-virtual {v0, p1}, Ljava/net/HttpURLConnection;->setFixedLengthStreamingMode(I)V

    return-void
.end method

.method public setFollowRedirects(Z)V
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/onedrive/sdk/http/UrlConnection;->mConnection:Ljava/net/HttpURLConnection;

    invoke-virtual {v0, p1}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    return-void
.end method

.class public interface abstract Lcom/onedrive/sdk/http/IHttpProvider;
.super Ljava/lang/Object;
.source "IHttpProvider.java"


# virtual methods
.method public abstract getSerializer()Lcom/onedrive/sdk/serializer/ISerializer;
.end method

.method public abstract send(Lcom/onedrive/sdk/http/IHttpRequest;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Result:",
            "Ljava/lang/Object;",
            "BodyType:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/onedrive/sdk/http/IHttpRequest;",
            "Ljava/lang/Class<",
            "TResult;>;TBodyType;)TResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation
.end method

.method public abstract send(Lcom/onedrive/sdk/http/IHttpRequest;Ljava/lang/Class;Ljava/lang/Object;Lcom/onedrive/sdk/http/IStatefulResponseHandler;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Result:",
            "Ljava/lang/Object;",
            "BodyType:",
            "Ljava/lang/Object;",
            "DeserializeType:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/onedrive/sdk/http/IHttpRequest;",
            "Ljava/lang/Class<",
            "TResult;>;TBodyType;",
            "Lcom/onedrive/sdk/http/IStatefulResponseHandler<",
            "TResult;TDeserializeType;>;)TResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation
.end method

.method public abstract send(Lcom/onedrive/sdk/http/IHttpRequest;Lcom/onedrive/sdk/concurrency/ICallback;Ljava/lang/Class;Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Result:",
            "Ljava/lang/Object;",
            "BodyType:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/onedrive/sdk/http/IHttpRequest;",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "TResult;>;",
            "Ljava/lang/Class<",
            "TResult;>;TBodyType;)V"
        }
    .end annotation
.end method

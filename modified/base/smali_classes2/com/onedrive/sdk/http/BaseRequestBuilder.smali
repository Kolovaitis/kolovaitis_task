.class public abstract Lcom/onedrive/sdk/http/BaseRequestBuilder;
.super Ljava/lang/Object;
.source "BaseRequestBuilder.java"

# interfaces
.implements Lcom/onedrive/sdk/http/IRequestBuilder;


# instance fields
.field private final mClient:Lcom/onedrive/sdk/extensions/IOneDriveClient;

.field private final mOptions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;"
        }
    .end annotation
.end field

.field private final mRequestUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/onedrive/sdk/extensions/IOneDriveClient;",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;)V"
        }
    .end annotation

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/onedrive/sdk/http/BaseRequestBuilder;->mOptions:Ljava/util/List;

    .line 59
    iput-object p1, p0, Lcom/onedrive/sdk/http/BaseRequestBuilder;->mRequestUrl:Ljava/lang/String;

    .line 60
    iput-object p2, p0, Lcom/onedrive/sdk/http/BaseRequestBuilder;->mClient:Lcom/onedrive/sdk/extensions/IOneDriveClient;

    if-eqz p3, :cond_0

    .line 63
    iget-object p1, p0, Lcom/onedrive/sdk/http/BaseRequestBuilder;->mOptions:Ljava/util/List;

    invoke-interface {p1, p3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_0
    return-void
.end method


# virtual methods
.method public getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/onedrive/sdk/http/BaseRequestBuilder;->mClient:Lcom/onedrive/sdk/extensions/IOneDriveClient;

    return-object v0
.end method

.method public getOptions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;"
        }
    .end annotation

    .line 88
    iget-object v0, p0, Lcom/onedrive/sdk/http/BaseRequestBuilder;->mOptions:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getRequestUrl()Ljava/lang/String;
    .locals 1

    .line 80
    iget-object v0, p0, Lcom/onedrive/sdk/http/BaseRequestBuilder;->mRequestUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getRequestUrlWithAdditionalSegment(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 97
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/onedrive/sdk/http/BaseRequestBuilder;->mRequestUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.class public abstract Lcom/onedrive/sdk/http/BaseCollectionPage;
.super Ljava/lang/Object;
.source "BaseCollectionPage.java"

# interfaces
.implements Lcom/onedrive/sdk/http/IBaseCollectionPage;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2::",
        "Lcom/onedrive/sdk/http/IRequestBuilder;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/onedrive/sdk/http/IBaseCollectionPage<",
        "TT1;TT2;>;"
    }
.end annotation


# instance fields
.field private final mPageContents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "TT1;>;"
        }
    .end annotation
.end field

.field private transient mRawObject:Lcom/google/gson/JsonObject;

.field private final mRequestBuilder:Lcom/onedrive/sdk/http/IRequestBuilder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT2;"
        }
    .end annotation
.end field

.field private transient mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/onedrive/sdk/http/IRequestBuilder;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "TT1;>;TT2;)V"
        }
    .end annotation

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    invoke-static {p1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/onedrive/sdk/http/BaseCollectionPage;->mPageContents:Ljava/util/List;

    .line 68
    iput-object p2, p0, Lcom/onedrive/sdk/http/BaseCollectionPage;->mRequestBuilder:Lcom/onedrive/sdk/http/IRequestBuilder;

    return-void
.end method


# virtual methods
.method public getCurrentPage()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "TT1;>;"
        }
    .end annotation

    .line 84
    iget-object v0, p0, Lcom/onedrive/sdk/http/BaseCollectionPage;->mPageContents:Ljava/util/List;

    return-object v0
.end method

.method public getNextPage()Lcom/onedrive/sdk/http/IRequestBuilder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT2;"
        }
    .end annotation

    .line 76
    iget-object v0, p0, Lcom/onedrive/sdk/http/BaseCollectionPage;->mRequestBuilder:Lcom/onedrive/sdk/http/IRequestBuilder;

    return-object v0
.end method

.method public getRawObject()Lcom/google/gson/JsonObject;
    .locals 1

    .line 92
    iget-object v0, p0, Lcom/onedrive/sdk/http/BaseCollectionPage;->mRawObject:Lcom/google/gson/JsonObject;

    return-object v0
.end method

.method protected getSerializer()Lcom/onedrive/sdk/serializer/ISerializer;
    .locals 1

    .line 100
    iget-object v0, p0, Lcom/onedrive/sdk/http/BaseCollectionPage;->mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;

    return-object v0
.end method

.method public setRawObject(Lcom/onedrive/sdk/serializer/ISerializer;Lcom/google/gson/JsonObject;)V
    .locals 0

    .line 110
    iput-object p1, p0, Lcom/onedrive/sdk/http/BaseCollectionPage;->mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;

    .line 111
    iput-object p2, p0, Lcom/onedrive/sdk/http/BaseCollectionPage;->mRawObject:Lcom/google/gson/JsonObject;

    return-void
.end method

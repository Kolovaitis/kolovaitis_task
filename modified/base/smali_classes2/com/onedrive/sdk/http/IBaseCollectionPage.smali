.class public interface abstract Lcom/onedrive/sdk/http/IBaseCollectionPage;
.super Ljava/lang/Object;
.source "IBaseCollectionPage.java"

# interfaces
.implements Lcom/onedrive/sdk/serializer/IJsonBackedObject;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2::",
        "Lcom/onedrive/sdk/http/IRequestBuilder;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/onedrive/sdk/serializer/IJsonBackedObject;"
    }
.end annotation


# virtual methods
.method public abstract getCurrentPage()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "TT1;>;"
        }
    .end annotation
.end method

.method public abstract getNextPage()Lcom/onedrive/sdk/http/IRequestBuilder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT2;"
        }
    .end annotation
.end method

.method public abstract getRawObject()Lcom/google/gson/JsonObject;
.end method

.class public final enum Lcom/onedrive/sdk/http/HttpMethod;
.super Ljava/lang/Enum;
.source "HttpMethod.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/onedrive/sdk/http/HttpMethod;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/onedrive/sdk/http/HttpMethod;

.field public static final enum DELETE:Lcom/onedrive/sdk/http/HttpMethod;

.field public static final enum GET:Lcom/onedrive/sdk/http/HttpMethod;

.field public static final enum PATCH:Lcom/onedrive/sdk/http/HttpMethod;

.field public static final enum POST:Lcom/onedrive/sdk/http/HttpMethod;

.field public static final enum PUT:Lcom/onedrive/sdk/http/HttpMethod;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 33
    new-instance v0, Lcom/onedrive/sdk/http/HttpMethod;

    const-string v1, "GET"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/onedrive/sdk/http/HttpMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/http/HttpMethod;->GET:Lcom/onedrive/sdk/http/HttpMethod;

    .line 38
    new-instance v0, Lcom/onedrive/sdk/http/HttpMethod;

    const-string v1, "POST"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/onedrive/sdk/http/HttpMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/http/HttpMethod;->POST:Lcom/onedrive/sdk/http/HttpMethod;

    .line 43
    new-instance v0, Lcom/onedrive/sdk/http/HttpMethod;

    const-string v1, "PATCH"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/onedrive/sdk/http/HttpMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/http/HttpMethod;->PATCH:Lcom/onedrive/sdk/http/HttpMethod;

    .line 48
    new-instance v0, Lcom/onedrive/sdk/http/HttpMethod;

    const-string v1, "DELETE"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lcom/onedrive/sdk/http/HttpMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/http/HttpMethod;->DELETE:Lcom/onedrive/sdk/http/HttpMethod;

    .line 53
    new-instance v0, Lcom/onedrive/sdk/http/HttpMethod;

    const-string v1, "PUT"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Lcom/onedrive/sdk/http/HttpMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/http/HttpMethod;->PUT:Lcom/onedrive/sdk/http/HttpMethod;

    const/4 v0, 0x5

    .line 28
    new-array v0, v0, [Lcom/onedrive/sdk/http/HttpMethod;

    sget-object v1, Lcom/onedrive/sdk/http/HttpMethod;->GET:Lcom/onedrive/sdk/http/HttpMethod;

    aput-object v1, v0, v2

    sget-object v1, Lcom/onedrive/sdk/http/HttpMethod;->POST:Lcom/onedrive/sdk/http/HttpMethod;

    aput-object v1, v0, v3

    sget-object v1, Lcom/onedrive/sdk/http/HttpMethod;->PATCH:Lcom/onedrive/sdk/http/HttpMethod;

    aput-object v1, v0, v4

    sget-object v1, Lcom/onedrive/sdk/http/HttpMethod;->DELETE:Lcom/onedrive/sdk/http/HttpMethod;

    aput-object v1, v0, v5

    sget-object v1, Lcom/onedrive/sdk/http/HttpMethod;->PUT:Lcom/onedrive/sdk/http/HttpMethod;

    aput-object v1, v0, v6

    sput-object v0, Lcom/onedrive/sdk/http/HttpMethod;->$VALUES:[Lcom/onedrive/sdk/http/HttpMethod;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 28
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/onedrive/sdk/http/HttpMethod;
    .locals 1

    .line 28
    const-class v0, Lcom/onedrive/sdk/http/HttpMethod;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/onedrive/sdk/http/HttpMethod;

    return-object p0
.end method

.method public static values()[Lcom/onedrive/sdk/http/HttpMethod;
    .locals 1

    .line 28
    sget-object v0, Lcom/onedrive/sdk/http/HttpMethod;->$VALUES:[Lcom/onedrive/sdk/http/HttpMethod;

    invoke-virtual {v0}, [Lcom/onedrive/sdk/http/HttpMethod;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/onedrive/sdk/http/HttpMethod;

    return-object v0
.end method

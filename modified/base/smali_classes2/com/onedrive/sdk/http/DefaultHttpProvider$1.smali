.class Lcom/onedrive/sdk/http/DefaultHttpProvider$1;
.super Ljava/lang/Object;
.source "DefaultHttpProvider.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/onedrive/sdk/http/DefaultHttpProvider;->send(Lcom/onedrive/sdk/http/IHttpRequest;Lcom/onedrive/sdk/concurrency/ICallback;Ljava/lang/Class;Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/onedrive/sdk/http/DefaultHttpProvider;

.field final synthetic val$callback:Lcom/onedrive/sdk/concurrency/ICallback;

.field final synthetic val$progressCallback:Lcom/onedrive/sdk/concurrency/IProgressCallback;

.field final synthetic val$request:Lcom/onedrive/sdk/http/IHttpRequest;

.field final synthetic val$resultClass:Ljava/lang/Class;

.field final synthetic val$serializable:Ljava/lang/Object;


# direct methods
.method constructor <init>(Lcom/onedrive/sdk/http/DefaultHttpProvider;Lcom/onedrive/sdk/http/IHttpRequest;Ljava/lang/Class;Ljava/lang/Object;Lcom/onedrive/sdk/concurrency/IProgressCallback;Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 0

    .line 133
    iput-object p1, p0, Lcom/onedrive/sdk/http/DefaultHttpProvider$1;->this$0:Lcom/onedrive/sdk/http/DefaultHttpProvider;

    iput-object p2, p0, Lcom/onedrive/sdk/http/DefaultHttpProvider$1;->val$request:Lcom/onedrive/sdk/http/IHttpRequest;

    iput-object p3, p0, Lcom/onedrive/sdk/http/DefaultHttpProvider$1;->val$resultClass:Ljava/lang/Class;

    iput-object p4, p0, Lcom/onedrive/sdk/http/DefaultHttpProvider$1;->val$serializable:Ljava/lang/Object;

    iput-object p5, p0, Lcom/onedrive/sdk/http/DefaultHttpProvider$1;->val$progressCallback:Lcom/onedrive/sdk/concurrency/IProgressCallback;

    iput-object p6, p0, Lcom/onedrive/sdk/http/DefaultHttpProvider$1;->val$callback:Lcom/onedrive/sdk/concurrency/ICallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .line 137
    :try_start_0
    iget-object v0, p0, Lcom/onedrive/sdk/http/DefaultHttpProvider$1;->this$0:Lcom/onedrive/sdk/http/DefaultHttpProvider;

    invoke-static {v0}, Lcom/onedrive/sdk/http/DefaultHttpProvider;->access$100(Lcom/onedrive/sdk/http/DefaultHttpProvider;)Lcom/onedrive/sdk/concurrency/IExecutors;

    move-result-object v0

    iget-object v1, p0, Lcom/onedrive/sdk/http/DefaultHttpProvider$1;->this$0:Lcom/onedrive/sdk/http/DefaultHttpProvider;

    iget-object v2, p0, Lcom/onedrive/sdk/http/DefaultHttpProvider$1;->val$request:Lcom/onedrive/sdk/http/IHttpRequest;

    iget-object v3, p0, Lcom/onedrive/sdk/http/DefaultHttpProvider$1;->val$resultClass:Ljava/lang/Class;

    iget-object v4, p0, Lcom/onedrive/sdk/http/DefaultHttpProvider$1;->val$serializable:Ljava/lang/Object;

    iget-object v5, p0, Lcom/onedrive/sdk/http/DefaultHttpProvider$1;->val$progressCallback:Lcom/onedrive/sdk/concurrency/IProgressCallback;

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/onedrive/sdk/http/DefaultHttpProvider;->access$000(Lcom/onedrive/sdk/http/DefaultHttpProvider;Lcom/onedrive/sdk/http/IHttpRequest;Ljava/lang/Class;Ljava/lang/Object;Lcom/onedrive/sdk/concurrency/IProgressCallback;Lcom/onedrive/sdk/http/IStatefulResponseHandler;)Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lcom/onedrive/sdk/http/DefaultHttpProvider$1;->val$callback:Lcom/onedrive/sdk/concurrency/ICallback;

    invoke-interface {v0, v1, v2}, Lcom/onedrive/sdk/concurrency/IExecutors;->performOnForeground(Ljava/lang/Object;Lcom/onedrive/sdk/concurrency/ICallback;)V
    :try_end_0
    .catch Lcom/onedrive/sdk/core/ClientException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 144
    iget-object v1, p0, Lcom/onedrive/sdk/http/DefaultHttpProvider$1;->this$0:Lcom/onedrive/sdk/http/DefaultHttpProvider;

    invoke-static {v1}, Lcom/onedrive/sdk/http/DefaultHttpProvider;->access$100(Lcom/onedrive/sdk/http/DefaultHttpProvider;)Lcom/onedrive/sdk/concurrency/IExecutors;

    move-result-object v1

    iget-object v2, p0, Lcom/onedrive/sdk/http/DefaultHttpProvider$1;->val$callback:Lcom/onedrive/sdk/concurrency/ICallback;

    invoke-interface {v1, v0, v2}, Lcom/onedrive/sdk/concurrency/IExecutors;->performOnForeground(Lcom/onedrive/sdk/core/ClientException;Lcom/onedrive/sdk/concurrency/ICallback;)V

    :goto_0
    return-void
.end method

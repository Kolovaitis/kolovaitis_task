.class public abstract Lcom/onedrive/sdk/http/BaseRequest;
.super Ljava/lang/Object;
.source "BaseRequest.java"

# interfaces
.implements Lcom/onedrive/sdk/http/IHttpRequest;


# static fields
.field private static final REQUEST_STATS_HEADER_NAME:Ljava/lang/String; = "X-RequestStats"

.field public static final REQUEST_STATS_HEADER_VALUE_FORMAT_STRING:Ljava/lang/String; = "SDK-Version=Android-v%s"


# instance fields
.field private final mClient:Lcom/onedrive/sdk/extensions/IOneDriveClient;

.field private final mHeadersOptions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/HeaderOption;",
            ">;"
        }
    .end annotation
.end field

.field private mMethod:Lcom/onedrive/sdk/http/HttpMethod;

.field private final mQueryOptions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/QueryOption;",
            ">;"
        }
    .end annotation
.end field

.field private final mRequestUrl:Ljava/lang/String;

.field private final mResponseClass:Ljava/lang/Class;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;Ljava/lang/Class;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/onedrive/sdk/extensions/IOneDriveClient;",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;",
            "Ljava/lang/Class;",
            ")V"
        }
    .end annotation

    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    iput-object p1, p0, Lcom/onedrive/sdk/http/BaseRequest;->mRequestUrl:Ljava/lang/String;

    .line 100
    iput-object p2, p0, Lcom/onedrive/sdk/http/BaseRequest;->mClient:Lcom/onedrive/sdk/extensions/IOneDriveClient;

    .line 101
    iput-object p4, p0, Lcom/onedrive/sdk/http/BaseRequest;->mResponseClass:Ljava/lang/Class;

    .line 103
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/onedrive/sdk/http/BaseRequest;->mHeadersOptions:Ljava/util/List;

    .line 104
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/onedrive/sdk/http/BaseRequest;->mQueryOptions:Ljava/util/List;

    if-eqz p3, :cond_2

    .line 107
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/onedrive/sdk/options/Option;

    .line 108
    instance-of p3, p2, Lcom/onedrive/sdk/options/HeaderOption;

    if-eqz p3, :cond_1

    .line 109
    iget-object p3, p0, Lcom/onedrive/sdk/http/BaseRequest;->mHeadersOptions:Ljava/util/List;

    move-object p4, p2

    check-cast p4, Lcom/onedrive/sdk/options/HeaderOption;

    invoke-interface {p3, p4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 111
    :cond_1
    instance-of p3, p2, Lcom/onedrive/sdk/options/QueryOption;

    if-eqz p3, :cond_0

    .line 112
    iget-object p3, p0, Lcom/onedrive/sdk/http/BaseRequest;->mQueryOptions:Ljava/util/List;

    check-cast p2, Lcom/onedrive/sdk/options/QueryOption;

    invoke-interface {p3, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 116
    :cond_2
    new-instance p1, Lcom/onedrive/sdk/options/HeaderOption;

    const-string p2, "X-RequestStats"

    const-string p3, "SDK-Version=Android-v%s"

    const/4 p4, 0x1

    new-array p4, p4, [Ljava/lang/Object;

    const/4 v0, 0x0

    const-string v1, "1.3.1"

    aput-object v1, p4, v0

    invoke-static {p3, p4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    invoke-direct {p1, p2, p3}, Lcom/onedrive/sdk/options/HeaderOption;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    iget-object p2, p0, Lcom/onedrive/sdk/http/BaseRequest;->mHeadersOptions:Ljava/util/List;

    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method public addHeader(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 177
    iget-object v0, p0, Lcom/onedrive/sdk/http/BaseRequest;->mHeadersOptions:Ljava/util/List;

    new-instance v1, Lcom/onedrive/sdk/options/HeaderOption;

    invoke-direct {v1, p1, p2}, Lcom/onedrive/sdk/options/HeaderOption;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;
    .locals 1

    .line 248
    iget-object v0, p0, Lcom/onedrive/sdk/http/BaseRequest;->mClient:Lcom/onedrive/sdk/extensions/IOneDriveClient;

    return-object v0
.end method

.method public getHeaders()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/HeaderOption;",
            ">;"
        }
    .end annotation

    .line 167
    iget-object v0, p0, Lcom/onedrive/sdk/http/BaseRequest;->mHeadersOptions:Ljava/util/List;

    return-object v0
.end method

.method public getHttpMethod()Lcom/onedrive/sdk/http/HttpMethod;
    .locals 1

    .line 157
    iget-object v0, p0, Lcom/onedrive/sdk/http/BaseRequest;->mMethod:Lcom/onedrive/sdk/http/HttpMethod;

    return-object v0
.end method

.method public getOptions()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;"
        }
    .end annotation

    .line 227
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 228
    iget-object v1, p0, Lcom/onedrive/sdk/http/BaseRequest;->mHeadersOptions:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->addAll(Ljava/util/Collection;)Z

    .line 229
    iget-object v1, p0, Lcom/onedrive/sdk/http/BaseRequest;->mQueryOptions:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->addAll(Ljava/util/Collection;)Z

    .line 230
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getQueryOptions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/QueryOption;",
            ">;"
        }
    .end annotation

    .line 218
    iget-object v0, p0, Lcom/onedrive/sdk/http/BaseRequest;->mQueryOptions:Ljava/util/List;

    return-object v0
.end method

.method public getRequestUrl()Ljava/net/URL;
    .locals 5

    .line 128
    iget-object v0, p0, Lcom/onedrive/sdk/http/BaseRequest;->mRequestUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 129
    new-instance v1, Landroid/net/Uri$Builder;

    invoke-direct {v1}, Landroid/net/Uri$Builder;-><init>()V

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/Uri;->getEncodedAuthority()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->encodedAuthority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/Uri;->getEncodedQuery()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->encodedQuery(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    .line 134
    invoke-virtual {v0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 135
    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0

    .line 138
    :cond_0
    iget-object v0, p0, Lcom/onedrive/sdk/http/BaseRequest;->mQueryOptions:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/onedrive/sdk/options/QueryOption;

    .line 139
    invoke-virtual {v2}, Lcom/onedrive/sdk/options/QueryOption;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lcom/onedrive/sdk/options/QueryOption;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_1

    .line 142
    :cond_1
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 144
    :try_start_0
    new-instance v1, Ljava/net/URL;

    invoke-direct {v1, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :catch_0
    move-exception v1

    .line 146
    new-instance v2, Lcom/onedrive/sdk/core/ClientException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid URL: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->InvalidRequest:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    invoke-direct {v2, v0, v1, v3}, Lcom/onedrive/sdk/core/ClientException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lcom/onedrive/sdk/core/OneDriveErrorCodes;)V

    throw v2
.end method

.method public getResponseType()Ljava/lang/Class;
    .locals 1

    .line 257
    iget-object v0, p0, Lcom/onedrive/sdk/http/BaseRequest;->mResponseClass:Ljava/lang/Class;

    return-object v0
.end method

.method protected send(Lcom/onedrive/sdk/http/HttpMethod;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/onedrive/sdk/http/HttpMethod;",
            "TT2;)TT1;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    .line 208
    iput-object p1, p0, Lcom/onedrive/sdk/http/BaseRequest;->mMethod:Lcom/onedrive/sdk/http/HttpMethod;

    .line 209
    iget-object p1, p0, Lcom/onedrive/sdk/http/BaseRequest;->mClient:Lcom/onedrive/sdk/extensions/IOneDriveClient;

    invoke-interface {p1}, Lcom/onedrive/sdk/extensions/IOneDriveClient;->getHttpProvider()Lcom/onedrive/sdk/http/IHttpProvider;

    move-result-object p1

    iget-object v0, p0, Lcom/onedrive/sdk/http/BaseRequest;->mResponseClass:Ljava/lang/Class;

    invoke-interface {p1, p0, v0, p2}, Lcom/onedrive/sdk/http/IHttpProvider;->send(Lcom/onedrive/sdk/http/IHttpRequest;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method protected send(Lcom/onedrive/sdk/http/HttpMethod;Lcom/onedrive/sdk/concurrency/ICallback;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/onedrive/sdk/http/HttpMethod;",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "TT1;>;TT2;)V"
        }
    .end annotation

    .line 192
    iput-object p1, p0, Lcom/onedrive/sdk/http/BaseRequest;->mMethod:Lcom/onedrive/sdk/http/HttpMethod;

    .line 193
    iget-object p1, p0, Lcom/onedrive/sdk/http/BaseRequest;->mClient:Lcom/onedrive/sdk/extensions/IOneDriveClient;

    invoke-interface {p1}, Lcom/onedrive/sdk/extensions/IOneDriveClient;->getHttpProvider()Lcom/onedrive/sdk/http/IHttpProvider;

    move-result-object p1

    iget-object v0, p0, Lcom/onedrive/sdk/http/BaseRequest;->mResponseClass:Ljava/lang/Class;

    invoke-interface {p1, p0, p2, v0, p3}, Lcom/onedrive/sdk/http/IHttpProvider;->send(Lcom/onedrive/sdk/http/IHttpRequest;Lcom/onedrive/sdk/concurrency/ICallback;Ljava/lang/Class;Ljava/lang/Object;)V

    return-void
.end method

.method public setHttpMethod(Lcom/onedrive/sdk/http/HttpMethod;)V
    .locals 0

    .line 239
    iput-object p1, p0, Lcom/onedrive/sdk/http/BaseRequest;->mMethod:Lcom/onedrive/sdk/http/HttpMethod;

    return-void
.end method

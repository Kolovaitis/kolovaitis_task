.class public Lcom/onedrive/sdk/http/DefaultHttpProvider;
.super Ljava/lang/Object;
.source "DefaultHttpProvider.java"

# interfaces
.implements Lcom/onedrive/sdk/http/IHttpProvider;


# static fields
.field static final CONTENT_TYPE_HEADER_NAME:Ljava/lang/String; = "Content-Type"

.field static final JSON_CONTENT_TYPE:Ljava/lang/String; = "application/json"


# instance fields
.field private mConnectionFactory:Lcom/onedrive/sdk/http/IConnectionFactory;

.field private final mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

.field private final mLogger:Lcom/onedrive/sdk/logger/ILogger;

.field private final mRequestInterceptor:Lcom/onedrive/sdk/http/IRequestInterceptor;

.field private final mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;


# direct methods
.method public constructor <init>(Lcom/onedrive/sdk/serializer/ISerializer;Lcom/onedrive/sdk/http/IRequestInterceptor;Lcom/onedrive/sdk/concurrency/IExecutors;Lcom/onedrive/sdk/logger/ILogger;)V
    .locals 0

    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    iput-object p1, p0, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;

    .line 96
    iput-object p2, p0, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mRequestInterceptor:Lcom/onedrive/sdk/http/IRequestInterceptor;

    .line 97
    iput-object p3, p0, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

    .line 98
    iput-object p4, p0, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    .line 99
    new-instance p1, Lcom/onedrive/sdk/http/DefaultConnectionFactory;

    invoke-direct {p1}, Lcom/onedrive/sdk/http/DefaultConnectionFactory;-><init>()V

    iput-object p1, p0, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mConnectionFactory:Lcom/onedrive/sdk/http/IConnectionFactory;

    return-void
.end method

.method static synthetic access$000(Lcom/onedrive/sdk/http/DefaultHttpProvider;Lcom/onedrive/sdk/http/IHttpRequest;Ljava/lang/Class;Ljava/lang/Object;Lcom/onedrive/sdk/concurrency/IProgressCallback;Lcom/onedrive/sdk/http/IStatefulResponseHandler;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    .line 47
    invoke-direct/range {p0 .. p5}, Lcom/onedrive/sdk/http/DefaultHttpProvider;->sendRequestInternal(Lcom/onedrive/sdk/http/IHttpRequest;Ljava/lang/Class;Ljava/lang/Object;Lcom/onedrive/sdk/concurrency/IProgressCallback;Lcom/onedrive/sdk/http/IStatefulResponseHandler;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$100(Lcom/onedrive/sdk/http/DefaultHttpProvider;)Lcom/onedrive/sdk/concurrency/IExecutors;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

    return-object p0
.end method

.method private handleBinaryStream(Ljava/io/InputStream;)Ljava/io/InputStream;
    .locals 0

    return-object p1
.end method

.method private handleErrorResponse(Lcom/onedrive/sdk/http/IHttpRequest;Ljava/lang/Object;Lcom/onedrive/sdk/http/IConnection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Body:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/onedrive/sdk/http/IHttpRequest;",
            "TBody;",
            "Lcom/onedrive/sdk/http/IConnection;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 343
    iget-object v0, p0, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;

    invoke-static {p1, p2, v0, p3}, Lcom/onedrive/sdk/http/OneDriveServiceException;->createFromConnection(Lcom/onedrive/sdk/http/IHttpRequest;Ljava/lang/Object;Lcom/onedrive/sdk/serializer/ISerializer;Lcom/onedrive/sdk/http/IConnection;)Lcom/onedrive/sdk/http/OneDriveServiceException;

    move-result-object p1

    throw p1
.end method

.method private handleJsonResponse(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Result:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/io/InputStream;",
            "Ljava/lang/Class<",
            "TResult;>;)TResult;"
        }
    .end annotation

    if-nez p2, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 369
    :cond_0
    invoke-static {p1}, Lcom/onedrive/sdk/http/DefaultHttpProvider;->streamToString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object p1

    .line 370
    invoke-virtual {p0}, Lcom/onedrive/sdk/http/DefaultHttpProvider;->getSerializer()Lcom/onedrive/sdk/serializer/ISerializer;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/onedrive/sdk/serializer/ISerializer;->deserializeObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method private sendRequestInternal(Lcom/onedrive/sdk/http/IHttpRequest;Ljava/lang/Class;Ljava/lang/Object;Lcom/onedrive/sdk/concurrency/IProgressCallback;Lcom/onedrive/sdk/http/IStatefulResponseHandler;)Ljava/lang/Object;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Result:",
            "Ljava/lang/Object;",
            "Body:",
            "Ljava/lang/Object;",
            "DeserializeType:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/onedrive/sdk/http/IHttpRequest;",
            "Ljava/lang/Class<",
            "TResult;>;TBody;",
            "Lcom/onedrive/sdk/concurrency/IProgressCallback<",
            "TResult;>;",
            "Lcom/onedrive/sdk/http/IStatefulResponseHandler<",
            "TResult;TDeserializeType;>;)TResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v0, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 211
    :try_start_0
    iget-object v8, v1, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mRequestInterceptor:Lcom/onedrive/sdk/http/IRequestInterceptor;

    if-eqz v8, :cond_0

    .line 212
    iget-object v8, v1, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mRequestInterceptor:Lcom/onedrive/sdk/http/IRequestInterceptor;

    invoke-interface {v8, v0}, Lcom/onedrive/sdk/http/IRequestInterceptor;->intercept(Lcom/onedrive/sdk/http/IHttpRequest;)V

    .line 218
    :cond_0
    invoke-interface/range {p1 .. p1}, Lcom/onedrive/sdk/http/IHttpRequest;->getRequestUrl()Ljava/net/URL;

    move-result-object v8

    .line 219
    iget-object v9, v1, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Starting to send request, URL "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v9, v8}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 220
    iget-object v8, v1, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mConnectionFactory:Lcom/onedrive/sdk/http/IConnectionFactory;

    invoke-interface {v8, v0}, Lcom/onedrive/sdk/http/IConnectionFactory;->createFromRequest(Lcom/onedrive/sdk/http/IHttpRequest;)Lcom/onedrive/sdk/http/IConnection;

    move-result-object v8
    :try_end_0
    .catch Lcom/onedrive/sdk/http/OneDriveServiceException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 223
    :try_start_1
    iget-object v10, v1, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Request Method "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface/range {p1 .. p1}, Lcom/onedrive/sdk/http/IHttpRequest;->getHttpMethod()Lcom/onedrive/sdk/http/HttpMethod;

    move-result-object v12

    invoke-virtual {v12}, Lcom/onedrive/sdk/http/HttpMethod;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v10, v11}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    if-nez v3, :cond_1

    const/4 v10, 0x0

    goto :goto_0

    .line 228
    :cond_1
    instance-of v10, v3, [B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_6

    if-eqz v10, :cond_2

    .line 229
    :try_start_2
    iget-object v10, v1, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v11, "Sending byte[] as request body"

    invoke-interface {v10, v11}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 230
    move-object v10, v3

    check-cast v10, [B

    check-cast v10, [B

    const-string v11, "Content-Type"

    const-string v12, "application/octet-stream"

    .line 231
    invoke-interface {v8, v11, v12}, Lcom/onedrive/sdk/http/IConnection;->addRequestHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    array-length v11, v10

    invoke-interface {v8, v11}, Lcom/onedrive/sdk/http/IConnection;->setContentLength(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v9, 0x0

    goto/16 :goto_7

    .line 234
    :cond_2
    :try_start_3
    iget-object v10, v1, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Sending "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p3 .. p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v12, " as request body"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v10, v11}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 235
    iget-object v10, v1, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;

    invoke-interface {v10, v3}, Lcom/onedrive/sdk/serializer/ISerializer;->serializeObject(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 236
    invoke-virtual {v10}, Ljava/lang/String;->getBytes()[B

    move-result-object v10

    const-string v11, "Content-Type"

    const-string v12, "application/json"

    .line 237
    invoke-interface {v8, v11, v12}, Lcom/onedrive/sdk/http/IConnection;->addRequestHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    array-length v11, v10

    invoke-interface {v8, v11}, Lcom/onedrive/sdk/http/IConnection;->setContentLength(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_6

    :goto_0
    if-eqz v10, :cond_5

    .line 243
    :try_start_4
    invoke-interface {v8}, Lcom/onedrive/sdk/http/IConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v11
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 246
    :try_start_5
    new-instance v12, Ljava/io/BufferedOutputStream;

    invoke-direct {v12, v11}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    const/4 v13, 0x0

    :cond_3
    const/16 v14, 0x1000

    .line 250
    array-length v15, v10

    sub-int/2addr v15, v13

    invoke-static {v14, v15}, Ljava/lang/Math;->min(II)I

    move-result v14

    .line 251
    invoke-virtual {v12, v10, v13, v14}, Ljava/io/BufferedOutputStream;->write([BII)V

    add-int/2addr v13, v14

    if-eqz v4, :cond_4

    .line 254
    iget-object v15, v1, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

    array-length v9, v10

    invoke-interface {v15, v13, v9, v4}, Lcom/onedrive/sdk/concurrency/IExecutors;->performOnForeground(IILcom/onedrive/sdk/concurrency/IProgressCallback;)V

    :cond_4
    if-gtz v14, :cond_3

    .line 258
    invoke-virtual {v12}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-object v9, v11

    goto :goto_1

    :catchall_1
    move-exception v0

    move-object v9, v11

    goto/16 :goto_5

    :cond_5
    const/4 v9, 0x0

    :goto_1
    if-eqz v5, :cond_6

    .line 262
    :try_start_6
    invoke-interface {v5, v8}, Lcom/onedrive/sdk/http/IStatefulResponseHandler;->configConnection(Lcom/onedrive/sdk/http/IConnection;)V

    .line 265
    :cond_6
    iget-object v4, v1, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v10, "Response code %d, %s"

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    invoke-interface {v8}, Lcom/onedrive/sdk/http/IConnection;->getResponseCode()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v11, v7

    invoke-interface {v8}, Lcom/onedrive/sdk/http/IConnection;->getResponseMessage()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v11, v6

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v4, v10}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    if-eqz v5, :cond_8

    .line 270
    iget-object v2, v1, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v3, "StatefulResponse is handling the HTTP response."

    invoke-interface {v2, v3}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 271
    invoke-virtual/range {p0 .. p0}, Lcom/onedrive/sdk/http/DefaultHttpProvider;->getSerializer()Lcom/onedrive/sdk/serializer/ISerializer;

    move-result-object v2

    iget-object v3, v1, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    invoke-interface {v5, v0, v8, v2, v3}, Lcom/onedrive/sdk/http/IStatefulResponseHandler;->generateResult(Lcom/onedrive/sdk/http/IHttpRequest;Lcom/onedrive/sdk/http/IConnection;Lcom/onedrive/sdk/serializer/ISerializer;Lcom/onedrive/sdk/logger/ILogger;)Ljava/lang/Object;

    move-result-object v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_5

    if-eqz v9, :cond_7

    .line 311
    :try_start_7
    invoke-virtual {v9}, Ljava/io/OutputStream;->close()V
    :try_end_7
    .catch Lcom/onedrive/sdk/http/OneDriveServiceException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0

    :cond_7
    return-object v0

    .line 275
    :cond_8
    :try_start_8
    invoke-interface {v8}, Lcom/onedrive/sdk/http/IConnection;->getResponseCode()I

    move-result v4

    const/16 v5, 0x190

    if-lt v4, v5, :cond_9

    .line 276
    iget-object v4, v1, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v5, "Handling error response"

    invoke-interface {v4, v5}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 277
    invoke-interface {v8}, Lcom/onedrive/sdk/http/IConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v4
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_5

    .line 278
    :try_start_9
    invoke-direct {v1, v0, v3, v8}, Lcom/onedrive/sdk/http/DefaultHttpProvider;->handleErrorResponse(Lcom/onedrive/sdk/http/IHttpRequest;Ljava/lang/Object;Lcom/onedrive/sdk/http/IConnection;)V

    goto :goto_2

    :cond_9
    const/4 v4, 0x0

    .line 281
    :goto_2
    invoke-interface {v8}, Lcom/onedrive/sdk/http/IConnection;->getResponseCode()I

    move-result v0

    const/16 v3, 0xcc

    if-eq v0, v3, :cond_11

    invoke-interface {v8}, Lcom/onedrive/sdk/http/IConnection;->getResponseCode()I

    move-result v0

    const/16 v3, 0x130

    if-ne v0, v3, :cond_a

    goto/16 :goto_3

    .line 287
    :cond_a
    invoke-interface {v8}, Lcom/onedrive/sdk/http/IConnection;->getResponseCode()I

    move-result v0

    const/16 v3, 0xca

    if-ne v0, v3, :cond_d

    .line 288
    iget-object v0, v1, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v3, "Handling accepted response"

    invoke-interface {v0, v3}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 289
    const-class v0, Lcom/onedrive/sdk/concurrency/AsyncMonitorLocation;

    if-ne v2, v0, :cond_d

    .line 291
    new-instance v0, Lcom/onedrive/sdk/concurrency/AsyncMonitorLocation;

    invoke-interface {v8}, Lcom/onedrive/sdk/http/IConnection;->getHeaders()Ljava/util/Map;

    move-result-object v2

    const-string v3, "Location"

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {v0, v2}, Lcom/onedrive/sdk/concurrency/AsyncMonitorLocation;-><init>(Ljava/lang/String;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    if-eqz v9, :cond_b

    .line 311
    :try_start_a
    invoke-virtual {v9}, Ljava/io/OutputStream;->close()V

    :cond_b
    if-eqz v4, :cond_c

    .line 314
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 315
    invoke-interface {v8}, Lcom/onedrive/sdk/http/IConnection;->close()V
    :try_end_a
    .catch Lcom/onedrive/sdk/http/OneDriveServiceException; {:try_start_a .. :try_end_a} :catch_1
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_0

    :cond_c
    return-object v0

    .line 295
    :cond_d
    :try_start_b
    new-instance v3, Ljava/io/BufferedInputStream;

    invoke-interface {v8}, Lcom/onedrive/sdk/http/IConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_4

    .line 297
    :try_start_c
    invoke-interface {v8}, Lcom/onedrive/sdk/http/IConnection;->getHeaders()Ljava/util/Map;

    move-result-object v0

    const-string v4, "Content-Type"

    .line 299
    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v4, "application/json"

    .line 300
    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 301
    iget-object v0, v1, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v4, "Response json"

    invoke-interface {v0, v4}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 302
    invoke-direct {v1, v3, v2}, Lcom/onedrive/sdk/http/DefaultHttpProvider;->handleJsonResponse(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    if-eqz v9, :cond_e

    .line 311
    :try_start_d
    invoke-virtual {v9}, Ljava/io/OutputStream;->close()V

    .line 314
    :cond_e
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    .line 315
    invoke-interface {v8}, Lcom/onedrive/sdk/http/IConnection;->close()V
    :try_end_d
    .catch Lcom/onedrive/sdk/http/OneDriveServiceException; {:try_start_d .. :try_end_d} :catch_1
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_0

    return-object v0

    .line 304
    :cond_f
    :try_start_e
    iget-object v0, v1, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v2, "Response binary"

    invoke-interface {v0, v2}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_3

    .line 307
    :try_start_f
    invoke-direct {v1, v3}, Lcom/onedrive/sdk/http/DefaultHttpProvider;->handleBinaryStream(Ljava/io/InputStream;)Ljava/io/InputStream;

    move-result-object v0
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_2

    if-eqz v9, :cond_10

    .line 311
    :try_start_10
    invoke-virtual {v9}, Ljava/io/OutputStream;->close()V
    :try_end_10
    .catch Lcom/onedrive/sdk/http/OneDriveServiceException; {:try_start_10 .. :try_end_10} :catch_1
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_0

    :cond_10
    return-object v0

    :catchall_2
    move-exception v0

    move-object v2, v3

    const/4 v3, 0x1

    goto :goto_7

    :catchall_3
    move-exception v0

    move-object v2, v3

    goto :goto_6

    .line 283
    :cond_11
    :goto_3
    :try_start_11
    iget-object v0, v1, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v2, "Handling response with no body"

    invoke-interface {v0, v2}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_4

    if-eqz v9, :cond_12

    .line 311
    :try_start_12
    invoke-virtual {v9}, Ljava/io/OutputStream;->close()V

    :cond_12
    if-eqz v4, :cond_13

    .line 314
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 315
    invoke-interface {v8}, Lcom/onedrive/sdk/http/IConnection;->close()V

    const/4 v2, 0x0

    goto :goto_4

    :cond_13
    const/4 v2, 0x0

    :goto_4
    return-object v2

    :catchall_4
    move-exception v0

    move-object v2, v4

    goto :goto_6

    :catchall_5
    move-exception v0

    :goto_5
    const/4 v2, 0x0

    goto :goto_6

    :catchall_6
    move-exception v0

    const/4 v2, 0x0

    move-object v9, v2

    :goto_6
    const/4 v3, 0x0

    :goto_7
    if-eqz v9, :cond_14

    .line 311
    invoke-virtual {v9}, Ljava/io/OutputStream;->close()V

    :cond_14
    if-nez v3, :cond_15

    if-eqz v2, :cond_15

    .line 314
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 315
    invoke-interface {v8}, Lcom/onedrive/sdk/http/IConnection;->close()V

    :cond_15
    throw v0
    :try_end_12
    .catch Lcom/onedrive/sdk/http/OneDriveServiceException; {:try_start_12 .. :try_end_12} :catch_1
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_0

    :catch_0
    move-exception v0

    .line 323
    new-instance v2, Lcom/onedrive/sdk/core/ClientException;

    sget-object v3, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->GeneralException:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v4, "Error during http request"

    invoke-direct {v2, v4, v0, v3}, Lcom/onedrive/sdk/core/ClientException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lcom/onedrive/sdk/core/OneDriveErrorCodes;)V

    .line 326
    iget-object v0, v1, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v3, "Error during http request"

    invoke-interface {v0, v3, v2}, Lcom/onedrive/sdk/logger/ILogger;->logError(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 327
    throw v2

    :catch_1
    move-exception v0

    .line 319
    iget-object v2, v1, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    invoke-interface {v2}, Lcom/onedrive/sdk/logger/ILogger;->getLoggingLevel()Lcom/onedrive/sdk/logger/LoggerLevel;

    move-result-object v2

    sget-object v3, Lcom/onedrive/sdk/logger/LoggerLevel;->Debug:Lcom/onedrive/sdk/logger/LoggerLevel;

    if-ne v2, v3, :cond_16

    goto :goto_8

    :cond_16
    const/4 v6, 0x0

    .line 320
    :goto_8
    iget-object v2, v1, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "OneDrive Service exception "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v6}, Lcom/onedrive/sdk/http/OneDriveServiceException;->getMessage(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Lcom/onedrive/sdk/logger/ILogger;->logError(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 321
    throw v0
.end method

.method public static streamToString(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 2

    .line 391
    new-instance v0, Ljava/util/Scanner;

    const-string v1, "UTF-8"

    invoke-direct {v0, p0, v1}, Ljava/util/Scanner;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    const-string p0, "\\A"

    invoke-virtual {v0, p0}, Ljava/util/Scanner;->useDelimiter(Ljava/lang/String;)Ljava/util/Scanner;

    move-result-object p0

    .line 392
    invoke-virtual {p0}, Ljava/util/Scanner;->next()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public getSerializer()Lcom/onedrive/sdk/serializer/ISerializer;
    .locals 1

    .line 109
    iget-object v0, p0, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;

    return-object v0
.end method

.method public send(Lcom/onedrive/sdk/http/IHttpRequest;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Result:",
            "Ljava/lang/Object;",
            "Body:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/onedrive/sdk/http/IHttpRequest;",
            "Ljava/lang/Class<",
            "TResult;>;TBody;)TResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    const/4 v0, 0x0

    .line 165
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/onedrive/sdk/http/DefaultHttpProvider;->send(Lcom/onedrive/sdk/http/IHttpRequest;Ljava/lang/Class;Ljava/lang/Object;Lcom/onedrive/sdk/http/IStatefulResponseHandler;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public send(Lcom/onedrive/sdk/http/IHttpRequest;Ljava/lang/Class;Ljava/lang/Object;Lcom/onedrive/sdk/http/IStatefulResponseHandler;)Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Result:",
            "Ljava/lang/Object;",
            "Body:",
            "Ljava/lang/Object;",
            "DeserializeType:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/onedrive/sdk/http/IHttpRequest;",
            "Ljava/lang/Class<",
            "TResult;>;TBody;",
            "Lcom/onedrive/sdk/http/IStatefulResponseHandler<",
            "TResult;TDeserializeType;>;)TResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    .line 184
    invoke-direct/range {v0 .. v5}, Lcom/onedrive/sdk/http/DefaultHttpProvider;->sendRequestInternal(Lcom/onedrive/sdk/http/IHttpRequest;Ljava/lang/Class;Ljava/lang/Object;Lcom/onedrive/sdk/concurrency/IProgressCallback;Lcom/onedrive/sdk/http/IStatefulResponseHandler;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public send(Lcom/onedrive/sdk/http/IHttpRequest;Lcom/onedrive/sdk/concurrency/ICallback;Ljava/lang/Class;Ljava/lang/Object;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Result:",
            "Ljava/lang/Object;",
            "Body:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/onedrive/sdk/http/IHttpRequest;",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "TResult;>;",
            "Ljava/lang/Class<",
            "TResult;>;TBody;)V"
        }
    .end annotation

    .line 127
    instance-of v0, p2, Lcom/onedrive/sdk/concurrency/IProgressCallback;

    if-eqz v0, :cond_0

    .line 128
    move-object v0, p2

    check-cast v0, Lcom/onedrive/sdk/concurrency/IProgressCallback;

    move-object v6, v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    move-object v6, v0

    .line 133
    :goto_0
    iget-object v0, p0, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

    new-instance v8, Lcom/onedrive/sdk/http/DefaultHttpProvider$1;

    move-object v1, v8

    move-object v2, p0

    move-object v3, p1

    move-object v4, p3

    move-object v5, p4

    move-object v7, p2

    invoke-direct/range {v1 .. v7}, Lcom/onedrive/sdk/http/DefaultHttpProvider$1;-><init>(Lcom/onedrive/sdk/http/DefaultHttpProvider;Lcom/onedrive/sdk/http/IHttpRequest;Ljava/lang/Class;Ljava/lang/Object;Lcom/onedrive/sdk/concurrency/IProgressCallback;Lcom/onedrive/sdk/concurrency/ICallback;)V

    invoke-interface {v0, v8}, Lcom/onedrive/sdk/concurrency/IExecutors;->performOnBackground(Ljava/lang/Runnable;)V

    return-void
.end method

.method setConnectionFactory(Lcom/onedrive/sdk/http/IConnectionFactory;)V
    .locals 0

    .line 379
    iput-object p1, p0, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mConnectionFactory:Lcom/onedrive/sdk/http/IConnectionFactory;

    return-void
.end method

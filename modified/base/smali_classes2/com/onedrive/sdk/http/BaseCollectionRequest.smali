.class public abstract Lcom/onedrive/sdk/http/BaseCollectionRequest;
.super Ljava/lang/Object;
.source "BaseCollectionRequest.java"

# interfaces
.implements Lcom/onedrive/sdk/http/IHttpRequest;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/onedrive/sdk/http/IHttpRequest;"
    }
.end annotation


# instance fields
.field private final mBaseRequest:Lcom/onedrive/sdk/http/BaseRequest;

.field private final mCollectionPageClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "TT2;>;"
        }
    .end annotation
.end field

.field private final mResponseClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "TT1;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;Ljava/lang/Class;Ljava/lang/Class;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/onedrive/sdk/extensions/IOneDriveClient;",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;",
            "Ljava/lang/Class<",
            "TT1;>;",
            "Ljava/lang/Class<",
            "TT2;>;)V"
        }
    .end annotation

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput-object p4, p0, Lcom/onedrive/sdk/http/BaseCollectionRequest;->mResponseClass:Ljava/lang/Class;

    .line 70
    iput-object p5, p0, Lcom/onedrive/sdk/http/BaseCollectionRequest;->mCollectionPageClass:Ljava/lang/Class;

    .line 71
    new-instance p4, Lcom/onedrive/sdk/http/BaseCollectionRequest$1;

    iget-object v5, p0, Lcom/onedrive/sdk/http/BaseCollectionRequest;->mResponseClass:Ljava/lang/Class;

    move-object v0, p4

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/onedrive/sdk/http/BaseCollectionRequest$1;-><init>(Lcom/onedrive/sdk/http/BaseCollectionRequest;Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;Ljava/lang/Class;)V

    iput-object p4, p0, Lcom/onedrive/sdk/http/BaseCollectionRequest;->mBaseRequest:Lcom/onedrive/sdk/http/BaseRequest;

    .line 72
    iget-object p1, p0, Lcom/onedrive/sdk/http/BaseCollectionRequest;->mBaseRequest:Lcom/onedrive/sdk/http/BaseRequest;

    sget-object p2, Lcom/onedrive/sdk/http/HttpMethod;->GET:Lcom/onedrive/sdk/http/HttpMethod;

    invoke-virtual {p1, p2}, Lcom/onedrive/sdk/http/BaseRequest;->setHttpMethod(Lcom/onedrive/sdk/http/HttpMethod;)V

    return-void
.end method


# virtual methods
.method public addHeader(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 119
    iget-object v0, p0, Lcom/onedrive/sdk/http/BaseCollectionRequest;->mBaseRequest:Lcom/onedrive/sdk/http/BaseRequest;

    invoke-virtual {v0, p1, p2}, Lcom/onedrive/sdk/http/BaseRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public addQueryOption(Lcom/onedrive/sdk/options/QueryOption;)V
    .locals 1

    .line 135
    iget-object v0, p0, Lcom/onedrive/sdk/http/BaseCollectionRequest;->mBaseRequest:Lcom/onedrive/sdk/http/BaseRequest;

    invoke-virtual {v0}, Lcom/onedrive/sdk/http/BaseRequest;->getQueryOptions()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method protected getBaseRequest()Lcom/onedrive/sdk/http/BaseRequest;
    .locals 1

    .line 144
    iget-object v0, p0, Lcom/onedrive/sdk/http/BaseCollectionRequest;->mBaseRequest:Lcom/onedrive/sdk/http/BaseRequest;

    return-object v0
.end method

.method public getCollectionPageClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "TT2;>;"
        }
    .end annotation

    .line 152
    iget-object v0, p0, Lcom/onedrive/sdk/http/BaseCollectionRequest;->mCollectionPageClass:Ljava/lang/Class;

    return-object v0
.end method

.method public getHeaders()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/HeaderOption;",
            ">;"
        }
    .end annotation

    .line 109
    iget-object v0, p0, Lcom/onedrive/sdk/http/BaseCollectionRequest;->mBaseRequest:Lcom/onedrive/sdk/http/BaseRequest;

    invoke-virtual {v0}, Lcom/onedrive/sdk/http/BaseRequest;->getHeaders()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getHttpMethod()Lcom/onedrive/sdk/http/HttpMethod;
    .locals 1

    .line 100
    iget-object v0, p0, Lcom/onedrive/sdk/http/BaseCollectionRequest;->mBaseRequest:Lcom/onedrive/sdk/http/BaseRequest;

    invoke-virtual {v0}, Lcom/onedrive/sdk/http/BaseRequest;->getHttpMethod()Lcom/onedrive/sdk/http/HttpMethod;

    move-result-object v0

    return-object v0
.end method

.method public getOptions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;"
        }
    .end annotation

    .line 127
    iget-object v0, p0, Lcom/onedrive/sdk/http/BaseCollectionRequest;->mBaseRequest:Lcom/onedrive/sdk/http/BaseRequest;

    invoke-virtual {v0}, Lcom/onedrive/sdk/http/BaseRequest;->getOptions()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getRequestUrl()Ljava/net/URL;
    .locals 1

    .line 91
    iget-object v0, p0, Lcom/onedrive/sdk/http/BaseCollectionRequest;->mBaseRequest:Lcom/onedrive/sdk/http/BaseRequest;

    invoke-virtual {v0}, Lcom/onedrive/sdk/http/BaseRequest;->getRequestUrl()Ljava/net/URL;

    move-result-object v0

    return-object v0
.end method

.method protected send()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT1;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    .line 82
    iget-object v0, p0, Lcom/onedrive/sdk/http/BaseCollectionRequest;->mBaseRequest:Lcom/onedrive/sdk/http/BaseRequest;

    invoke-virtual {v0}, Lcom/onedrive/sdk/http/BaseRequest;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v0

    invoke-interface {v0}, Lcom/onedrive/sdk/extensions/IOneDriveClient;->getHttpProvider()Lcom/onedrive/sdk/http/IHttpProvider;

    move-result-object v0

    iget-object v1, p0, Lcom/onedrive/sdk/http/BaseCollectionRequest;->mResponseClass:Ljava/lang/Class;

    const/4 v2, 0x0

    invoke-interface {v0, p0, v1, v2}, Lcom/onedrive/sdk/http/IHttpProvider;->send(Lcom/onedrive/sdk/http/IHttpRequest;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

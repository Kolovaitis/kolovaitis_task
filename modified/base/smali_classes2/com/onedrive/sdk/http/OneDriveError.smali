.class public Lcom/onedrive/sdk/http/OneDriveError;
.super Ljava/lang/Object;
.source "OneDriveError.java"


# instance fields
.field public code:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "code"
    .end annotation
.end field

.field public innererror:Lcom/onedrive/sdk/http/OneDriveInnerError;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "innererror"
    .end annotation
.end field

.field public message:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "message"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public isError(Lcom/onedrive/sdk/core/OneDriveErrorCodes;)Z
    .locals 4

    .line 46
    iget-object v0, p0, Lcom/onedrive/sdk/http/OneDriveError;->code:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/onedrive/sdk/http/OneDriveError;->innererror:Lcom/onedrive/sdk/http/OneDriveInnerError;

    :goto_0
    if-eqz v0, :cond_2

    .line 51
    iget-object v2, v0, Lcom/onedrive/sdk/http/OneDriveInnerError;->code:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    return v1

    .line 54
    :cond_1
    iget-object v0, v0, Lcom/onedrive/sdk/http/OneDriveInnerError;->innererror:Lcom/onedrive/sdk/http/OneDriveInnerError;

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    return p1
.end method

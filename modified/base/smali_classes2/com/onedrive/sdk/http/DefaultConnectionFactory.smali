.class public Lcom/onedrive/sdk/http/DefaultConnectionFactory;
.super Ljava/lang/Object;
.source "DefaultConnectionFactory.java"

# interfaces
.implements Lcom/onedrive/sdk/http/IConnectionFactory;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromRequest(Lcom/onedrive/sdk/http/IHttpRequest;)Lcom/onedrive/sdk/http/IConnection;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 40
    new-instance v0, Lcom/onedrive/sdk/http/UrlConnection;

    invoke-direct {v0, p1}, Lcom/onedrive/sdk/http/UrlConnection;-><init>(Lcom/onedrive/sdk/http/IHttpRequest;)V

    return-object v0
.end method

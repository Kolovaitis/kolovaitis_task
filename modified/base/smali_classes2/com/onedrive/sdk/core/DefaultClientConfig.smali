.class public abstract Lcom/onedrive/sdk/core/DefaultClientConfig;
.super Ljava/lang/Object;
.source "DefaultClientConfig.java"

# interfaces
.implements Lcom/onedrive/sdk/core/IClientConfig;


# instance fields
.field private mAuthenticator:Lcom/onedrive/sdk/authentication/IAuthenticator;

.field private mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

.field private mHttpProvider:Lcom/onedrive/sdk/http/DefaultHttpProvider;

.field private mLogger:Lcom/onedrive/sdk/logger/ILogger;

.field private mRequestInterceptor:Lcom/onedrive/sdk/http/IRequestInterceptor;

.field private mSerializer:Lcom/onedrive/sdk/serializer/DefaultSerializer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createWithAuthenticator(Lcom/onedrive/sdk/authentication/IAuthenticator;)Lcom/onedrive/sdk/core/IClientConfig;
    .locals 2

    .line 81
    new-instance v0, Lcom/onedrive/sdk/core/DefaultClientConfig$1;

    invoke-direct {v0}, Lcom/onedrive/sdk/core/DefaultClientConfig$1;-><init>()V

    .line 82
    iput-object p0, v0, Lcom/onedrive/sdk/core/DefaultClientConfig;->mAuthenticator:Lcom/onedrive/sdk/authentication/IAuthenticator;

    .line 83
    invoke-virtual {v0}, Lcom/onedrive/sdk/core/DefaultClientConfig;->getLogger()Lcom/onedrive/sdk/logger/ILogger;

    move-result-object p0

    const-string v1, "Using provided authenticator"

    invoke-interface {p0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    return-object v0
.end method

.method public static createWithAuthenticators(Lcom/onedrive/sdk/authentication/MSAAuthenticator;Lcom/onedrive/sdk/authentication/ADALAuthenticator;)Lcom/onedrive/sdk/core/IClientConfig;
    .locals 2

    .line 96
    new-instance v0, Lcom/onedrive/sdk/core/DefaultClientConfig$2;

    invoke-direct {v0}, Lcom/onedrive/sdk/core/DefaultClientConfig$2;-><init>()V

    .line 97
    new-instance v1, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;

    invoke-direct {v1, p0, p1}, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;-><init>(Lcom/onedrive/sdk/authentication/MSAAuthenticator;Lcom/onedrive/sdk/authentication/ADALAuthenticator;)V

    iput-object v1, v0, Lcom/onedrive/sdk/core/DefaultClientConfig;->mAuthenticator:Lcom/onedrive/sdk/authentication/IAuthenticator;

    .line 98
    invoke-virtual {v0}, Lcom/onedrive/sdk/core/DefaultClientConfig;->getLogger()Lcom/onedrive/sdk/logger/ILogger;

    move-result-object p0

    const-string p1, "Created DisambiguationAuthenticator"

    invoke-interface {p0, p1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    return-object v0
.end method

.method private getRequestInterceptor()Lcom/onedrive/sdk/http/IRequestInterceptor;
    .locals 3

    .line 170
    iget-object v0, p0, Lcom/onedrive/sdk/core/DefaultClientConfig;->mRequestInterceptor:Lcom/onedrive/sdk/http/IRequestInterceptor;

    if-nez v0, :cond_0

    .line 171
    new-instance v0, Lcom/onedrive/sdk/authentication/AuthorizationInterceptor;

    invoke-virtual {p0}, Lcom/onedrive/sdk/core/DefaultClientConfig;->getAuthenticator()Lcom/onedrive/sdk/authentication/IAuthenticator;

    move-result-object v1

    invoke-virtual {p0}, Lcom/onedrive/sdk/core/DefaultClientConfig;->getLogger()Lcom/onedrive/sdk/logger/ILogger;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/onedrive/sdk/authentication/AuthorizationInterceptor;-><init>(Lcom/onedrive/sdk/authentication/IAuthenticator;Lcom/onedrive/sdk/logger/ILogger;)V

    iput-object v0, p0, Lcom/onedrive/sdk/core/DefaultClientConfig;->mRequestInterceptor:Lcom/onedrive/sdk/http/IRequestInterceptor;

    .line 173
    :cond_0
    iget-object v0, p0, Lcom/onedrive/sdk/core/DefaultClientConfig;->mRequestInterceptor:Lcom/onedrive/sdk/http/IRequestInterceptor;

    return-object v0
.end method


# virtual methods
.method public getAuthenticator()Lcom/onedrive/sdk/authentication/IAuthenticator;
    .locals 1

    .line 108
    iget-object v0, p0, Lcom/onedrive/sdk/core/DefaultClientConfig;->mAuthenticator:Lcom/onedrive/sdk/authentication/IAuthenticator;

    return-object v0
.end method

.method public getExecutors()Lcom/onedrive/sdk/concurrency/IExecutors;
    .locals 2

    .line 146
    iget-object v0, p0, Lcom/onedrive/sdk/core/DefaultClientConfig;->mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

    if-nez v0, :cond_0

    .line 147
    new-instance v0, Lcom/onedrive/sdk/concurrency/DefaultExecutors;

    invoke-virtual {p0}, Lcom/onedrive/sdk/core/DefaultClientConfig;->getLogger()Lcom/onedrive/sdk/logger/ILogger;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/onedrive/sdk/concurrency/DefaultExecutors;-><init>(Lcom/onedrive/sdk/logger/ILogger;)V

    iput-object v0, p0, Lcom/onedrive/sdk/core/DefaultClientConfig;->mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

    .line 148
    iget-object v0, p0, Lcom/onedrive/sdk/core/DefaultClientConfig;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Created DefaultExecutors"

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 150
    :cond_0
    iget-object v0, p0, Lcom/onedrive/sdk/core/DefaultClientConfig;->mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

    return-object v0
.end method

.method public getHttpProvider()Lcom/onedrive/sdk/http/IHttpProvider;
    .locals 5

    .line 117
    iget-object v0, p0, Lcom/onedrive/sdk/core/DefaultClientConfig;->mHttpProvider:Lcom/onedrive/sdk/http/DefaultHttpProvider;

    if-nez v0, :cond_0

    .line 118
    new-instance v0, Lcom/onedrive/sdk/http/DefaultHttpProvider;

    invoke-virtual {p0}, Lcom/onedrive/sdk/core/DefaultClientConfig;->getSerializer()Lcom/onedrive/sdk/serializer/ISerializer;

    move-result-object v1

    invoke-direct {p0}, Lcom/onedrive/sdk/core/DefaultClientConfig;->getRequestInterceptor()Lcom/onedrive/sdk/http/IRequestInterceptor;

    move-result-object v2

    invoke-virtual {p0}, Lcom/onedrive/sdk/core/DefaultClientConfig;->getExecutors()Lcom/onedrive/sdk/concurrency/IExecutors;

    move-result-object v3

    invoke-virtual {p0}, Lcom/onedrive/sdk/core/DefaultClientConfig;->getLogger()Lcom/onedrive/sdk/logger/ILogger;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/onedrive/sdk/http/DefaultHttpProvider;-><init>(Lcom/onedrive/sdk/serializer/ISerializer;Lcom/onedrive/sdk/http/IRequestInterceptor;Lcom/onedrive/sdk/concurrency/IExecutors;Lcom/onedrive/sdk/logger/ILogger;)V

    iput-object v0, p0, Lcom/onedrive/sdk/core/DefaultClientConfig;->mHttpProvider:Lcom/onedrive/sdk/http/DefaultHttpProvider;

    .line 122
    iget-object v0, p0, Lcom/onedrive/sdk/core/DefaultClientConfig;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Created DefaultHttpProvider"

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 124
    :cond_0
    iget-object v0, p0, Lcom/onedrive/sdk/core/DefaultClientConfig;->mHttpProvider:Lcom/onedrive/sdk/http/DefaultHttpProvider;

    return-object v0
.end method

.method public getLogger()Lcom/onedrive/sdk/logger/ILogger;
    .locals 2

    .line 158
    iget-object v0, p0, Lcom/onedrive/sdk/core/DefaultClientConfig;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    if-nez v0, :cond_0

    .line 159
    new-instance v0, Lcom/onedrive/sdk/logger/DefaultLogger;

    invoke-direct {v0}, Lcom/onedrive/sdk/logger/DefaultLogger;-><init>()V

    iput-object v0, p0, Lcom/onedrive/sdk/core/DefaultClientConfig;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    .line 160
    iget-object v0, p0, Lcom/onedrive/sdk/core/DefaultClientConfig;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Created DefaultLogger"

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 162
    :cond_0
    iget-object v0, p0, Lcom/onedrive/sdk/core/DefaultClientConfig;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    return-object v0
.end method

.method public getSerializer()Lcom/onedrive/sdk/serializer/ISerializer;
    .locals 2

    .line 133
    iget-object v0, p0, Lcom/onedrive/sdk/core/DefaultClientConfig;->mSerializer:Lcom/onedrive/sdk/serializer/DefaultSerializer;

    if-nez v0, :cond_0

    .line 134
    new-instance v0, Lcom/onedrive/sdk/serializer/DefaultSerializer;

    invoke-virtual {p0}, Lcom/onedrive/sdk/core/DefaultClientConfig;->getLogger()Lcom/onedrive/sdk/logger/ILogger;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/onedrive/sdk/serializer/DefaultSerializer;-><init>(Lcom/onedrive/sdk/logger/ILogger;)V

    iput-object v0, p0, Lcom/onedrive/sdk/core/DefaultClientConfig;->mSerializer:Lcom/onedrive/sdk/serializer/DefaultSerializer;

    .line 135
    iget-object v0, p0, Lcom/onedrive/sdk/core/DefaultClientConfig;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Created DefaultSerializer"

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 137
    :cond_0
    iget-object v0, p0, Lcom/onedrive/sdk/core/DefaultClientConfig;->mSerializer:Lcom/onedrive/sdk/serializer/DefaultSerializer;

    return-object v0
.end method

.class public final enum Lcom/onedrive/sdk/core/OneDriveErrorCodes;
.super Ljava/lang/Enum;
.source "OneDriveErrorCodes.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/onedrive/sdk/core/OneDriveErrorCodes;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum AccessDenied:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum AccessRestricted:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum ActivityLimitReached:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum AsyncTaskFailed:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum AsyncTaskNotCompleted:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum AuthenicationPermissionsDenied:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum AuthenticationCancelled:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum AuthenticationFailure:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum CannotSnapshotTree:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum ChildItemCountExceeded:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum EntityTagDoesNotMatch:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum FragmentLengthMismatch:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum FragmentOutOfOrder:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum FragmentOverlap:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum GeneralException:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum InvalidAcceptType:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum InvalidParameterFormat:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum InvalidPath:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum InvalidQueryOption:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum InvalidRange:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum InvalidRequest:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum InvalidStartIndex:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum ItemNotFound:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum LockMismatch:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum LockNotFoundOrAlreadyExpired:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum LockOwnerMismatch:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum MalformedEntityTag:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum MalwareDetected:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum MaxDocumentCountExceeded:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum MaxFileSizeExceeded:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum MaxFolderCountExceeded:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum MaxFragmentLengthExceeded:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum MaxItemCountExceeded:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum MaxQueryLengthExceeded:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum MaxStreamSizeExceeded:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum NameAlreadyExists:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum NotAllowed:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum NotSupported:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum ParameterIsTooLong:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum ParameterIsTooSmall:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum PathIsTooLong:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum PathTooDeep:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum PropertyNotUpdateable:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum QuotaLimitReached:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum ResourceModified:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum ResyncApplyDifferences:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum ResyncRequired:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum ResyncUploadDifferences:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum ServiceNotAvailable:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum ServiceReadOnly:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum ThrottledRequest:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum TooManyRedirects:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum TooManyResultsRequested:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum TooManyTermsInQuery:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum TotalAffectedItemCountExceeded:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum TruncationNotAllowed:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum Unauthenticated:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum UploadSessionFailed:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum UploadSessionIncomplete:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum UploadSessionNotFound:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum VirusSuspicious:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

.field public static final enum ZeroOrFewerResultsRequested:Lcom/onedrive/sdk/core/OneDriveErrorCodes;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 31
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "AccessDenied"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->AccessDenied:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 32
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "ActivityLimitReached"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->ActivityLimitReached:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 33
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "AsyncTaskFailed"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->AsyncTaskFailed:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 34
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "AsyncTaskNotCompleted"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->AsyncTaskNotCompleted:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 35
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "AuthenticationCancelled"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->AuthenticationCancelled:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 36
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "AuthenticationFailure"

    const/4 v7, 0x5

    invoke-direct {v0, v1, v7}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->AuthenticationFailure:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 37
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "AuthenicationPermissionsDenied"

    const/4 v8, 0x6

    invoke-direct {v0, v1, v8}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->AuthenicationPermissionsDenied:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 38
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "GeneralException"

    const/4 v9, 0x7

    invoke-direct {v0, v1, v9}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->GeneralException:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 39
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "InvalidRange"

    const/16 v10, 0x8

    invoke-direct {v0, v1, v10}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->InvalidRange:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 40
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "InvalidRequest"

    const/16 v11, 0x9

    invoke-direct {v0, v1, v11}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->InvalidRequest:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 41
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "ItemNotFound"

    const/16 v12, 0xa

    invoke-direct {v0, v1, v12}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->ItemNotFound:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 42
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "MalwareDetected"

    const/16 v13, 0xb

    invoke-direct {v0, v1, v13}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->MalwareDetected:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 43
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "NameAlreadyExists"

    const/16 v14, 0xc

    invoke-direct {v0, v1, v14}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->NameAlreadyExists:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 44
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "NotAllowed"

    const/16 v15, 0xd

    invoke-direct {v0, v1, v15}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->NotAllowed:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 45
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "NotSupported"

    const/16 v15, 0xe

    invoke-direct {v0, v1, v15}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->NotSupported:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 46
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "QuotaLimitReached"

    const/16 v15, 0xf

    invoke-direct {v0, v1, v15}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->QuotaLimitReached:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 47
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "ResourceModified"

    const/16 v15, 0x10

    invoke-direct {v0, v1, v15}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->ResourceModified:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 48
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "ResyncRequired"

    const/16 v15, 0x11

    invoke-direct {v0, v1, v15}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->ResyncRequired:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 49
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "ServiceNotAvailable"

    const/16 v15, 0x12

    invoke-direct {v0, v1, v15}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->ServiceNotAvailable:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 50
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "TooManyRedirects"

    const/16 v15, 0x13

    invoke-direct {v0, v1, v15}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->TooManyRedirects:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 51
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "Unauthenticated"

    const/16 v15, 0x14

    invoke-direct {v0, v1, v15}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->Unauthenticated:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 54
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "AccessRestricted"

    const/16 v15, 0x15

    invoke-direct {v0, v1, v15}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->AccessRestricted:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 55
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "CannotSnapshotTree"

    const/16 v15, 0x16

    invoke-direct {v0, v1, v15}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->CannotSnapshotTree:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 56
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "ChildItemCountExceeded"

    const/16 v15, 0x17

    invoke-direct {v0, v1, v15}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->ChildItemCountExceeded:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 57
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "EntityTagDoesNotMatch"

    const/16 v15, 0x18

    invoke-direct {v0, v1, v15}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->EntityTagDoesNotMatch:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 58
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "FragmentLengthMismatch"

    const/16 v15, 0x19

    invoke-direct {v0, v1, v15}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->FragmentLengthMismatch:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 59
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "FragmentOutOfOrder"

    const/16 v15, 0x1a

    invoke-direct {v0, v1, v15}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->FragmentOutOfOrder:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 60
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "FragmentOverlap"

    const/16 v15, 0x1b

    invoke-direct {v0, v1, v15}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->FragmentOverlap:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 61
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "InvalidAcceptType"

    const/16 v15, 0x1c

    invoke-direct {v0, v1, v15}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->InvalidAcceptType:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 62
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "InvalidParameterFormat"

    const/16 v15, 0x1d

    invoke-direct {v0, v1, v15}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->InvalidParameterFormat:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 63
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "InvalidPath"

    const/16 v15, 0x1e

    invoke-direct {v0, v1, v15}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->InvalidPath:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 64
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "InvalidQueryOption"

    const/16 v15, 0x1f

    invoke-direct {v0, v1, v15}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->InvalidQueryOption:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 65
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "InvalidStartIndex"

    const/16 v15, 0x20

    invoke-direct {v0, v1, v15}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->InvalidStartIndex:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 66
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "LockMismatch"

    const/16 v15, 0x21

    invoke-direct {v0, v1, v15}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->LockMismatch:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 67
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "LockNotFoundOrAlreadyExpired"

    const/16 v15, 0x22

    invoke-direct {v0, v1, v15}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->LockNotFoundOrAlreadyExpired:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 68
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "LockOwnerMismatch"

    const/16 v15, 0x23

    invoke-direct {v0, v1, v15}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->LockOwnerMismatch:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 69
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "MalformedEntityTag"

    const/16 v15, 0x24

    invoke-direct {v0, v1, v15}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->MalformedEntityTag:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 70
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "MaxDocumentCountExceeded"

    const/16 v15, 0x25

    invoke-direct {v0, v1, v15}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->MaxDocumentCountExceeded:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 71
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "MaxFileSizeExceeded"

    const/16 v15, 0x26

    invoke-direct {v0, v1, v15}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->MaxFileSizeExceeded:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 72
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "MaxFolderCountExceeded"

    const/16 v15, 0x27

    invoke-direct {v0, v1, v15}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->MaxFolderCountExceeded:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 73
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "MaxFragmentLengthExceeded"

    const/16 v15, 0x28

    invoke-direct {v0, v1, v15}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->MaxFragmentLengthExceeded:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 74
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "MaxItemCountExceeded"

    const/16 v15, 0x29

    invoke-direct {v0, v1, v15}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->MaxItemCountExceeded:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 75
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "MaxQueryLengthExceeded"

    const/16 v15, 0x2a

    invoke-direct {v0, v1, v15}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->MaxQueryLengthExceeded:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 76
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "MaxStreamSizeExceeded"

    const/16 v15, 0x2b

    invoke-direct {v0, v1, v15}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->MaxStreamSizeExceeded:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 77
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "ParameterIsTooLong"

    const/16 v15, 0x2c

    invoke-direct {v0, v1, v15}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->ParameterIsTooLong:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 78
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "ParameterIsTooSmall"

    const/16 v15, 0x2d

    invoke-direct {v0, v1, v15}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->ParameterIsTooSmall:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 79
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "PathIsTooLong"

    const/16 v15, 0x2e

    invoke-direct {v0, v1, v15}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->PathIsTooLong:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 80
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "PathTooDeep"

    const/16 v15, 0x2f

    invoke-direct {v0, v1, v15}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->PathTooDeep:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 81
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "PropertyNotUpdateable"

    const/16 v15, 0x30

    invoke-direct {v0, v1, v15}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->PropertyNotUpdateable:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 82
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "ResyncApplyDifferences"

    const/16 v15, 0x31

    invoke-direct {v0, v1, v15}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->ResyncApplyDifferences:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 83
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "ResyncUploadDifferences"

    const/16 v15, 0x32

    invoke-direct {v0, v1, v15}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->ResyncUploadDifferences:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 84
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "ServiceReadOnly"

    const/16 v15, 0x33

    invoke-direct {v0, v1, v15}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->ServiceReadOnly:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 85
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "ThrottledRequest"

    const/16 v15, 0x34

    invoke-direct {v0, v1, v15}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->ThrottledRequest:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 86
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "TooManyResultsRequested"

    const/16 v15, 0x35

    invoke-direct {v0, v1, v15}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->TooManyResultsRequested:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 87
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "TooManyTermsInQuery"

    const/16 v15, 0x36

    invoke-direct {v0, v1, v15}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->TooManyTermsInQuery:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 88
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "TotalAffectedItemCountExceeded"

    const/16 v15, 0x37

    invoke-direct {v0, v1, v15}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->TotalAffectedItemCountExceeded:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 89
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "TruncationNotAllowed"

    const/16 v15, 0x38

    invoke-direct {v0, v1, v15}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->TruncationNotAllowed:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 90
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "UploadSessionFailed"

    const/16 v15, 0x39

    invoke-direct {v0, v1, v15}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->UploadSessionFailed:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 91
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "UploadSessionIncomplete"

    const/16 v15, 0x3a

    invoke-direct {v0, v1, v15}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->UploadSessionIncomplete:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 92
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "UploadSessionNotFound"

    const/16 v15, 0x3b

    invoke-direct {v0, v1, v15}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->UploadSessionNotFound:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 93
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "VirusSuspicious"

    const/16 v15, 0x3c

    invoke-direct {v0, v1, v15}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->VirusSuspicious:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 94
    new-instance v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "ZeroOrFewerResultsRequested"

    const/16 v15, 0x3d

    invoke-direct {v0, v1, v15}, Lcom/onedrive/sdk/core/OneDriveErrorCodes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->ZeroOrFewerResultsRequested:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const/16 v0, 0x3e

    .line 29
    new-array v0, v0, [Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->AccessDenied:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    aput-object v1, v0, v2

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->ActivityLimitReached:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    aput-object v1, v0, v3

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->AsyncTaskFailed:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    aput-object v1, v0, v4

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->AsyncTaskNotCompleted:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    aput-object v1, v0, v5

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->AuthenticationCancelled:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    aput-object v1, v0, v6

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->AuthenticationFailure:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    aput-object v1, v0, v7

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->AuthenicationPermissionsDenied:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    aput-object v1, v0, v8

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->GeneralException:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    aput-object v1, v0, v9

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->InvalidRange:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    aput-object v1, v0, v10

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->InvalidRequest:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    aput-object v1, v0, v11

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->ItemNotFound:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    aput-object v1, v0, v12

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->MalwareDetected:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    aput-object v1, v0, v13

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->NameAlreadyExists:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    aput-object v1, v0, v14

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->NotAllowed:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->NotSupported:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->QuotaLimitReached:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->ResourceModified:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->ResyncRequired:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->ServiceNotAvailable:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->TooManyRedirects:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->Unauthenticated:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->AccessRestricted:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->CannotSnapshotTree:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->ChildItemCountExceeded:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->EntityTagDoesNotMatch:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->FragmentLengthMismatch:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->FragmentOutOfOrder:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->FragmentOverlap:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->InvalidAcceptType:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->InvalidParameterFormat:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->InvalidPath:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->InvalidQueryOption:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->InvalidStartIndex:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->LockMismatch:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->LockNotFoundOrAlreadyExpired:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->LockOwnerMismatch:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const/16 v2, 0x23

    aput-object v1, v0, v2

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->MalformedEntityTag:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const/16 v2, 0x24

    aput-object v1, v0, v2

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->MaxDocumentCountExceeded:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const/16 v2, 0x25

    aput-object v1, v0, v2

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->MaxFileSizeExceeded:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const/16 v2, 0x26

    aput-object v1, v0, v2

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->MaxFolderCountExceeded:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const/16 v2, 0x27

    aput-object v1, v0, v2

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->MaxFragmentLengthExceeded:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const/16 v2, 0x28

    aput-object v1, v0, v2

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->MaxItemCountExceeded:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const/16 v2, 0x29

    aput-object v1, v0, v2

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->MaxQueryLengthExceeded:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const/16 v2, 0x2a

    aput-object v1, v0, v2

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->MaxStreamSizeExceeded:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const/16 v2, 0x2b

    aput-object v1, v0, v2

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->ParameterIsTooLong:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const/16 v2, 0x2c

    aput-object v1, v0, v2

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->ParameterIsTooSmall:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const/16 v2, 0x2d

    aput-object v1, v0, v2

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->PathIsTooLong:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const/16 v2, 0x2e

    aput-object v1, v0, v2

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->PathTooDeep:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const/16 v2, 0x2f

    aput-object v1, v0, v2

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->PropertyNotUpdateable:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const/16 v2, 0x30

    aput-object v1, v0, v2

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->ResyncApplyDifferences:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const/16 v2, 0x31

    aput-object v1, v0, v2

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->ResyncUploadDifferences:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const/16 v2, 0x32

    aput-object v1, v0, v2

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->ServiceReadOnly:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const/16 v2, 0x33

    aput-object v1, v0, v2

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->ThrottledRequest:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const/16 v2, 0x34

    aput-object v1, v0, v2

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->TooManyResultsRequested:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const/16 v2, 0x35

    aput-object v1, v0, v2

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->TooManyTermsInQuery:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const/16 v2, 0x36

    aput-object v1, v0, v2

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->TotalAffectedItemCountExceeded:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const/16 v2, 0x37

    aput-object v1, v0, v2

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->TruncationNotAllowed:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const/16 v2, 0x38

    aput-object v1, v0, v2

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->UploadSessionFailed:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const/16 v2, 0x39

    aput-object v1, v0, v2

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->UploadSessionIncomplete:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const/16 v2, 0x3a

    aput-object v1, v0, v2

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->UploadSessionNotFound:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const/16 v2, 0x3b

    aput-object v1, v0, v2

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->VirusSuspicious:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const/16 v2, 0x3c

    aput-object v1, v0, v2

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->ZeroOrFewerResultsRequested:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const/16 v2, 0x3d

    aput-object v1, v0, v2

    sput-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->$VALUES:[Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/onedrive/sdk/core/OneDriveErrorCodes;
    .locals 1

    .line 29
    const-class v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    return-object p0
.end method

.method public static values()[Lcom/onedrive/sdk/core/OneDriveErrorCodes;
    .locals 1

    .line 29
    sget-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->$VALUES:[Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    invoke-virtual {v0}, [Lcom/onedrive/sdk/core/OneDriveErrorCodes;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    return-object v0
.end method

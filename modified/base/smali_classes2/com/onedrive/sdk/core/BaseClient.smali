.class public Lcom/onedrive/sdk/core/BaseClient;
.super Ljava/lang/Object;
.source "BaseClient.java"

# interfaces
.implements Lcom/onedrive/sdk/core/IBaseClient;


# instance fields
.field private mAuthenticator:Lcom/onedrive/sdk/authentication/IAuthenticator;

.field private mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

.field private mHttpProvider:Lcom/onedrive/sdk/http/IHttpProvider;

.field private mLogger:Lcom/onedrive/sdk/logger/ILogger;

.field private mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAuthenticator()Lcom/onedrive/sdk/authentication/IAuthenticator;
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/onedrive/sdk/core/BaseClient;->mAuthenticator:Lcom/onedrive/sdk/authentication/IAuthenticator;

    return-object v0
.end method

.method public getExecutors()Lcom/onedrive/sdk/concurrency/IExecutors;
    .locals 1

    .line 85
    iget-object v0, p0, Lcom/onedrive/sdk/core/BaseClient;->mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

    return-object v0
.end method

.method public getHttpProvider()Lcom/onedrive/sdk/http/IHttpProvider;
    .locals 1

    .line 94
    iget-object v0, p0, Lcom/onedrive/sdk/core/BaseClient;->mHttpProvider:Lcom/onedrive/sdk/http/IHttpProvider;

    return-object v0
.end method

.method public getLogger()Lcom/onedrive/sdk/logger/ILogger;
    .locals 1

    .line 102
    iget-object v0, p0, Lcom/onedrive/sdk/core/BaseClient;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    return-object v0
.end method

.method public getSerializer()Lcom/onedrive/sdk/serializer/ISerializer;
    .locals 1

    .line 111
    iget-object v0, p0, Lcom/onedrive/sdk/core/BaseClient;->mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;

    return-object v0
.end method

.method public getServiceRoot()Ljava/lang/String;
    .locals 1

    .line 76
    invoke-virtual {p0}, Lcom/onedrive/sdk/core/BaseClient;->getAuthenticator()Lcom/onedrive/sdk/authentication/IAuthenticator;

    move-result-object v0

    invoke-interface {v0}, Lcom/onedrive/sdk/authentication/IAuthenticator;->getAccountInfo()Lcom/onedrive/sdk/authentication/IAccountInfo;

    move-result-object v0

    invoke-interface {v0}, Lcom/onedrive/sdk/authentication/IAccountInfo;->getServiceRoot()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected setAuthenticator(Lcom/onedrive/sdk/authentication/IAuthenticator;)V
    .locals 0

    .line 157
    iput-object p1, p0, Lcom/onedrive/sdk/core/BaseClient;->mAuthenticator:Lcom/onedrive/sdk/authentication/IAuthenticator;

    return-void
.end method

.method protected setExecutors(Lcom/onedrive/sdk/concurrency/IExecutors;)V
    .locals 0

    .line 149
    iput-object p1, p0, Lcom/onedrive/sdk/core/BaseClient;->mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

    return-void
.end method

.method protected setHttpProvider(Lcom/onedrive/sdk/http/IHttpProvider;)V
    .locals 0

    .line 165
    iput-object p1, p0, Lcom/onedrive/sdk/core/BaseClient;->mHttpProvider:Lcom/onedrive/sdk/http/IHttpProvider;

    return-void
.end method

.method protected setLogger(Lcom/onedrive/sdk/logger/ILogger;)V
    .locals 0

    .line 141
    iput-object p1, p0, Lcom/onedrive/sdk/core/BaseClient;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    return-void
.end method

.method public setSerializer(Lcom/onedrive/sdk/serializer/ISerializer;)V
    .locals 0

    .line 173
    iput-object p1, p0, Lcom/onedrive/sdk/core/BaseClient;->mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;

    return-void
.end method

.method public validate()V
    .locals 2

    .line 119
    iget-object v0, p0, Lcom/onedrive/sdk/core/BaseClient;->mAuthenticator:Lcom/onedrive/sdk/authentication/IAuthenticator;

    if-eqz v0, :cond_3

    .line 123
    iget-object v0, p0, Lcom/onedrive/sdk/core/BaseClient;->mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

    if-eqz v0, :cond_2

    .line 127
    iget-object v0, p0, Lcom/onedrive/sdk/core/BaseClient;->mHttpProvider:Lcom/onedrive/sdk/http/IHttpProvider;

    if-eqz v0, :cond_1

    .line 131
    iget-object v0, p0, Lcom/onedrive/sdk/core/BaseClient;->mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;

    if-eqz v0, :cond_0

    return-void

    .line 132
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Serializer"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 128
    :cond_1
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "HttpProvider"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 124
    :cond_2
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Executors"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 120
    :cond_3
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Authenticator"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

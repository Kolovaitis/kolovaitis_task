.class public Lcom/onedrive/sdk/core/ClientException;
.super Ljava/lang/RuntimeException;
.source "ClientException.java"


# instance fields
.field private final mErrorCode:Lcom/onedrive/sdk/core/OneDriveErrorCodes;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;Lcom/onedrive/sdk/core/OneDriveErrorCodes;)V
    .locals 0

    .line 42
    invoke-direct {p0, p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 43
    iput-object p3, p0, Lcom/onedrive/sdk/core/ClientException;->mErrorCode:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    return-void
.end method


# virtual methods
.method public isError(Lcom/onedrive/sdk/core/OneDriveErrorCodes;)Z
    .locals 1

    .line 52
    iget-object v0, p0, Lcom/onedrive/sdk/core/ClientException;->mErrorCode:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    if-ne v0, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

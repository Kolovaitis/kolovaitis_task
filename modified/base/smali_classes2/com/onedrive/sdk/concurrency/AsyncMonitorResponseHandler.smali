.class public Lcom/onedrive/sdk/concurrency/AsyncMonitorResponseHandler;
.super Ljava/lang/Object;
.source "AsyncMonitorResponseHandler.java"

# interfaces
.implements Lcom/onedrive/sdk/http/IStatefulResponseHandler;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/onedrive/sdk/http/IStatefulResponseHandler<",
        "Lcom/onedrive/sdk/extensions/AsyncOperationStatus;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public configConnection(Lcom/onedrive/sdk/http/IConnection;)V
    .locals 1

    const/4 v0, 0x0

    .line 49
    invoke-interface {p1, v0}, Lcom/onedrive/sdk/http/IConnection;->setFollowRedirects(Z)V

    return-void
.end method

.method public generateResult(Lcom/onedrive/sdk/http/IHttpRequest;Lcom/onedrive/sdk/http/IConnection;Lcom/onedrive/sdk/serializer/ISerializer;Lcom/onedrive/sdk/logger/ILogger;)Lcom/onedrive/sdk/extensions/AsyncOperationStatus;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 68
    invoke-interface {p2}, Lcom/onedrive/sdk/http/IConnection;->getResponseCode()I

    move-result p1

    const/16 v0, 0x12f

    if-ne p1, v0, :cond_0

    const-string p1, "Item copy job has completed."

    .line 69
    invoke-interface {p4, p1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 70
    invoke-interface {p2}, Lcom/onedrive/sdk/http/IConnection;->getHeaders()Ljava/util/Map;

    move-result-object p1

    const-string p2, "Location"

    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    invoke-static {p1}, Lcom/onedrive/sdk/extensions/AsyncOperationStatus;->createdCompleted(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/AsyncOperationStatus;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 p1, 0x0

    .line 76
    :try_start_0
    new-instance p4, Ljava/io/BufferedInputStream;

    invoke-interface {p2}, Lcom/onedrive/sdk/http/IConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {p4, v0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 77
    :try_start_1
    invoke-static {p4}, Lcom/onedrive/sdk/http/DefaultHttpProvider;->streamToString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object p1

    const-class v0, Lcom/onedrive/sdk/extensions/AsyncOperationStatus;

    invoke-interface {p3, p1, v0}, Lcom/onedrive/sdk/serializer/ISerializer;->deserializeObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/onedrive/sdk/extensions/AsyncOperationStatus;

    .line 79
    invoke-interface {p2}, Lcom/onedrive/sdk/http/IConnection;->getHeaders()Ljava/util/Map;

    move-result-object p2

    const-string p3, "Location"

    invoke-interface {p2, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    iput-object p2, p1, Lcom/onedrive/sdk/extensions/AsyncOperationStatus;->seeOther:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 83
    invoke-virtual {p4}, Ljava/io/InputStream;->close()V

    return-object p1

    :catchall_0
    move-exception p1

    goto :goto_0

    :catchall_1
    move-exception p2

    move-object p4, p1

    move-object p1, p2

    :goto_0
    if-eqz p4, :cond_1

    invoke-virtual {p4}, Ljava/io/InputStream;->close()V

    :cond_1
    throw p1
.end method

.method public bridge synthetic generateResult(Lcom/onedrive/sdk/http/IHttpRequest;Lcom/onedrive/sdk/http/IConnection;Lcom/onedrive/sdk/serializer/ISerializer;Lcom/onedrive/sdk/logger/ILogger;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 40
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/onedrive/sdk/concurrency/AsyncMonitorResponseHandler;->generateResult(Lcom/onedrive/sdk/http/IHttpRequest;Lcom/onedrive/sdk/http/IConnection;Lcom/onedrive/sdk/serializer/ISerializer;Lcom/onedrive/sdk/logger/ILogger;)Lcom/onedrive/sdk/extensions/AsyncOperationStatus;

    move-result-object p1

    return-object p1
.end method

.class Lcom/onedrive/sdk/concurrency/SynchronousExecutor$1;
.super Landroid/os/AsyncTask;
.source "SynchronousExecutor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/onedrive/sdk/concurrency/SynchronousExecutor;->execute(Ljava/lang/Runnable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/onedrive/sdk/concurrency/SynchronousExecutor;

.field final synthetic val$runnable:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Lcom/onedrive/sdk/concurrency/SynchronousExecutor;Ljava/lang/Runnable;)V
    .locals 0

    .line 46
    iput-object p1, p0, Lcom/onedrive/sdk/concurrency/SynchronousExecutor$1;->this$0:Lcom/onedrive/sdk/concurrency/SynchronousExecutor;

    iput-object p2, p0, Lcom/onedrive/sdk/concurrency/SynchronousExecutor$1;->val$runnable:Ljava/lang/Runnable;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 46
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/onedrive/sdk/concurrency/SynchronousExecutor$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 46
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/onedrive/sdk/concurrency/SynchronousExecutor$1;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 0

    .line 53
    iget-object p1, p0, Lcom/onedrive/sdk/concurrency/SynchronousExecutor$1;->this$0:Lcom/onedrive/sdk/concurrency/SynchronousExecutor;

    invoke-static {p1}, Lcom/onedrive/sdk/concurrency/SynchronousExecutor;->access$000(Lcom/onedrive/sdk/concurrency/SynchronousExecutor;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 54
    iget-object p1, p0, Lcom/onedrive/sdk/concurrency/SynchronousExecutor$1;->val$runnable:Ljava/lang/Runnable;

    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 55
    iget-object p1, p0, Lcom/onedrive/sdk/concurrency/SynchronousExecutor$1;->this$0:Lcom/onedrive/sdk/concurrency/SynchronousExecutor;

    invoke-static {p1}, Lcom/onedrive/sdk/concurrency/SynchronousExecutor;->access$000(Lcom/onedrive/sdk/concurrency/SynchronousExecutor;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    return-void
.end method

.class public Lcom/onedrive/sdk/concurrency/AsyncMonitor;
.super Ljava/lang/Object;
.source "AsyncMonitor.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final mClient:Lcom/onedrive/sdk/extensions/IOneDriveClient;

.field private final mHandler:Lcom/onedrive/sdk/concurrency/AsyncMonitorResponseHandler;

.field private final mMonitorLocation:Lcom/onedrive/sdk/concurrency/AsyncMonitorLocation;

.field private final mResultGetter:Lcom/onedrive/sdk/concurrency/ResultGetter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/onedrive/sdk/concurrency/ResultGetter<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/onedrive/sdk/extensions/IOneDriveClient;Lcom/onedrive/sdk/concurrency/AsyncMonitorLocation;Lcom/onedrive/sdk/concurrency/ResultGetter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/extensions/IOneDriveClient;",
            "Lcom/onedrive/sdk/concurrency/AsyncMonitorLocation;",
            "Lcom/onedrive/sdk/concurrency/ResultGetter<",
            "TT;>;)V"
        }
    .end annotation

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput-object p1, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->mClient:Lcom/onedrive/sdk/extensions/IOneDriveClient;

    .line 68
    iput-object p2, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->mMonitorLocation:Lcom/onedrive/sdk/concurrency/AsyncMonitorLocation;

    .line 69
    iput-object p3, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->mResultGetter:Lcom/onedrive/sdk/concurrency/ResultGetter;

    .line 70
    new-instance p1, Lcom/onedrive/sdk/concurrency/AsyncMonitorResponseHandler;

    invoke-direct {p1}, Lcom/onedrive/sdk/concurrency/AsyncMonitorResponseHandler;-><init>()V

    iput-object p1, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->mHandler:Lcom/onedrive/sdk/concurrency/AsyncMonitorResponseHandler;

    return-void
.end method

.method static synthetic access$000(Lcom/onedrive/sdk/concurrency/AsyncMonitor;)Lcom/onedrive/sdk/extensions/IOneDriveClient;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->mClient:Lcom/onedrive/sdk/extensions/IOneDriveClient;

    return-object p0
.end method

.method static synthetic access$100(Lcom/onedrive/sdk/concurrency/AsyncMonitor;Lcom/onedrive/sdk/extensions/AsyncOperationStatus;)Z
    .locals 0

    .line 36
    invoke-direct {p0, p1}, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->isCompleted(Lcom/onedrive/sdk/extensions/AsyncOperationStatus;)Z

    move-result p0

    return p0
.end method

.method static synthetic access$200(Lcom/onedrive/sdk/concurrency/AsyncMonitor;Lcom/onedrive/sdk/extensions/AsyncOperationStatus;)Z
    .locals 0

    .line 36
    invoke-direct {p0, p1}, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->isFailed(Lcom/onedrive/sdk/extensions/AsyncOperationStatus;)Z

    move-result p0

    return p0
.end method

.method private isCompleted(Lcom/onedrive/sdk/extensions/AsyncOperationStatus;)Z
    .locals 1

    .line 188
    iget-object p1, p1, Lcom/onedrive/sdk/extensions/AsyncOperationStatus;->status:Ljava/lang/String;

    const-string v0, "completed"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method private isFailed(Lcom/onedrive/sdk/extensions/AsyncOperationStatus;)Z
    .locals 1

    .line 197
    iget-object p1, p1, Lcom/onedrive/sdk/extensions/AsyncOperationStatus;->status:Ljava/lang/String;

    const-string v0, "failed"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method


# virtual methods
.method public getResult()Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    .line 131
    invoke-virtual {p0}, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->getStatus()Lcom/onedrive/sdk/extensions/AsyncOperationStatus;

    move-result-object v0

    .line 132
    iget-object v1, v0, Lcom/onedrive/sdk/extensions/AsyncOperationStatus;->seeOther:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 137
    iget-object v1, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->mResultGetter:Lcom/onedrive/sdk/concurrency/ResultGetter;

    iget-object v0, v0, Lcom/onedrive/sdk/extensions/AsyncOperationStatus;->seeOther:Ljava/lang/String;

    iget-object v2, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->mClient:Lcom/onedrive/sdk/extensions/IOneDriveClient;

    invoke-interface {v1, v0, v2}, Lcom/onedrive/sdk/concurrency/ResultGetter;->getResultFrom(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 133
    :cond_0
    new-instance v1, Lcom/onedrive/sdk/core/ClientException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Async operation \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, v0, Lcom/onedrive/sdk/extensions/AsyncOperationStatus;->operation:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\' has not completed!"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    sget-object v3, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->AsyncTaskNotCompleted:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    invoke-direct {v1, v0, v2, v3}, Lcom/onedrive/sdk/core/ClientException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lcom/onedrive/sdk/core/OneDriveErrorCodes;)V

    throw v1
.end method

.method public getResult(Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "TT;>;)V"
        }
    .end annotation

    .line 113
    iget-object v0, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->mClient:Lcom/onedrive/sdk/extensions/IOneDriveClient;

    invoke-interface {v0}, Lcom/onedrive/sdk/extensions/IOneDriveClient;->getExecutors()Lcom/onedrive/sdk/concurrency/IExecutors;

    move-result-object v0

    new-instance v1, Lcom/onedrive/sdk/concurrency/AsyncMonitor$3;

    invoke-direct {v1, p0, p1}, Lcom/onedrive/sdk/concurrency/AsyncMonitor$3;-><init>(Lcom/onedrive/sdk/concurrency/AsyncMonitor;Lcom/onedrive/sdk/concurrency/ICallback;)V

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/concurrency/IExecutors;->performOnBackground(Ljava/lang/Runnable;)V

    return-void
.end method

.method public getStatus()Lcom/onedrive/sdk/extensions/AsyncOperationStatus;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    .line 96
    new-instance v6, Lcom/onedrive/sdk/concurrency/AsyncMonitor$2;

    iget-object v0, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->mMonitorLocation:Lcom/onedrive/sdk/concurrency/AsyncMonitorLocation;

    invoke-virtual {v0}, Lcom/onedrive/sdk/concurrency/AsyncMonitorLocation;->getLocation()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->mClient:Lcom/onedrive/sdk/extensions/IOneDriveClient;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, v6

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/onedrive/sdk/concurrency/AsyncMonitor$2;-><init>(Lcom/onedrive/sdk/concurrency/AsyncMonitor;Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;Ljava/lang/Class;)V

    .line 100
    sget-object v0, Lcom/onedrive/sdk/http/HttpMethod;->GET:Lcom/onedrive/sdk/http/HttpMethod;

    invoke-virtual {v6, v0}, Lcom/onedrive/sdk/http/BaseRequest;->setHttpMethod(Lcom/onedrive/sdk/http/HttpMethod;)V

    .line 102
    iget-object v0, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->mClient:Lcom/onedrive/sdk/extensions/IOneDriveClient;

    invoke-interface {v0}, Lcom/onedrive/sdk/extensions/IOneDriveClient;->getHttpProvider()Lcom/onedrive/sdk/http/IHttpProvider;

    move-result-object v0

    const-class v1, Lcom/onedrive/sdk/extensions/AsyncOperationStatus;

    iget-object v2, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->mHandler:Lcom/onedrive/sdk/concurrency/AsyncMonitorResponseHandler;

    const/4 v3, 0x0

    invoke-interface {v0, v6, v1, v3, v2}, Lcom/onedrive/sdk/http/IHttpProvider;->send(Lcom/onedrive/sdk/http/IHttpRequest;Ljava/lang/Class;Ljava/lang/Object;Lcom/onedrive/sdk/http/IStatefulResponseHandler;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/onedrive/sdk/extensions/AsyncOperationStatus;

    return-object v0
.end method

.method public getStatus(Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/extensions/AsyncOperationStatus;",
            ">;)V"
        }
    .end annotation

    .line 78
    iget-object v0, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->mClient:Lcom/onedrive/sdk/extensions/IOneDriveClient;

    invoke-interface {v0}, Lcom/onedrive/sdk/extensions/IOneDriveClient;->getExecutors()Lcom/onedrive/sdk/concurrency/IExecutors;

    move-result-object v0

    new-instance v1, Lcom/onedrive/sdk/concurrency/AsyncMonitor$1;

    invoke-direct {v1, p0, p1}, Lcom/onedrive/sdk/concurrency/AsyncMonitor$1;-><init>(Lcom/onedrive/sdk/concurrency/AsyncMonitor;Lcom/onedrive/sdk/concurrency/ICallback;)V

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/concurrency/IExecutors;->performOnBackground(Ljava/lang/Runnable;)V

    return-void
.end method

.method public pollForResult(JLcom/onedrive/sdk/concurrency/IProgressCallback;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/onedrive/sdk/concurrency/IProgressCallback<",
            "TT;>;)V"
        }
    .end annotation

    .line 147
    iget-object v0, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->mClient:Lcom/onedrive/sdk/extensions/IOneDriveClient;

    invoke-interface {v0}, Lcom/onedrive/sdk/extensions/IOneDriveClient;->getLogger()Lcom/onedrive/sdk/logger/ILogger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Starting to poll for request "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->mMonitorLocation:Lcom/onedrive/sdk/concurrency/AsyncMonitorLocation;

    invoke-virtual {v2}, Lcom/onedrive/sdk/concurrency/AsyncMonitorLocation;->getLocation()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 148
    iget-object v0, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->mClient:Lcom/onedrive/sdk/extensions/IOneDriveClient;

    invoke-interface {v0}, Lcom/onedrive/sdk/extensions/IOneDriveClient;->getExecutors()Lcom/onedrive/sdk/concurrency/IExecutors;

    move-result-object v0

    new-instance v1, Lcom/onedrive/sdk/concurrency/AsyncMonitor$4;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/onedrive/sdk/concurrency/AsyncMonitor$4;-><init>(Lcom/onedrive/sdk/concurrency/AsyncMonitor;JLcom/onedrive/sdk/concurrency/IProgressCallback;)V

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/concurrency/IExecutors;->performOnBackground(Ljava/lang/Runnable;)V

    return-void
.end method

.class public Lcom/onedrive/sdk/concurrency/DefaultExecutors;
.super Ljava/lang/Object;
.source "DefaultExecutors.java"

# interfaces
.implements Lcom/onedrive/sdk/concurrency/IExecutors;


# instance fields
.field private final mBackgroundExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

.field private final mForegroundExecutor:Lcom/onedrive/sdk/concurrency/SynchronousExecutor;

.field private final mLogger:Lcom/onedrive/sdk/logger/ILogger;


# direct methods
.method public constructor <init>(Lcom/onedrive/sdk/logger/ILogger;)V
    .locals 0

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Lcom/onedrive/sdk/concurrency/DefaultExecutors;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    .line 57
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object p1

    check-cast p1, Ljava/util/concurrent/ThreadPoolExecutor;

    iput-object p1, p0, Lcom/onedrive/sdk/concurrency/DefaultExecutors;->mBackgroundExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 58
    new-instance p1, Lcom/onedrive/sdk/concurrency/SynchronousExecutor;

    invoke-direct {p1}, Lcom/onedrive/sdk/concurrency/SynchronousExecutor;-><init>()V

    iput-object p1, p0, Lcom/onedrive/sdk/concurrency/DefaultExecutors;->mForegroundExecutor:Lcom/onedrive/sdk/concurrency/SynchronousExecutor;

    return-void
.end method


# virtual methods
.method public performOnBackground(Ljava/lang/Runnable;)V
    .locals 3

    .line 67
    iget-object v0, p0, Lcom/onedrive/sdk/concurrency/DefaultExecutors;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Starting background task, current active count: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/onedrive/sdk/concurrency/DefaultExecutors;->mBackgroundExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v2}, Ljava/util/concurrent/ThreadPoolExecutor;->getActiveCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 69
    iget-object v0, p0, Lcom/onedrive/sdk/concurrency/DefaultExecutors;->mBackgroundExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public performOnForeground(IILcom/onedrive/sdk/concurrency/IProgressCallback;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Result:",
            "Ljava/lang/Object;",
            ">(II",
            "Lcom/onedrive/sdk/concurrency/IProgressCallback<",
            "TResult;>;)V"
        }
    .end annotation

    .line 103
    iget-object v0, p0, Lcom/onedrive/sdk/concurrency/DefaultExecutors;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Starting foreground task, current active count:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/onedrive/sdk/concurrency/DefaultExecutors;->mForegroundExecutor:Lcom/onedrive/sdk/concurrency/SynchronousExecutor;

    invoke-virtual {v2}, Lcom/onedrive/sdk/concurrency/SynchronousExecutor;->getActiveCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ", with progress  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ", max progress"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 109
    iget-object v0, p0, Lcom/onedrive/sdk/concurrency/DefaultExecutors;->mForegroundExecutor:Lcom/onedrive/sdk/concurrency/SynchronousExecutor;

    new-instance v1, Lcom/onedrive/sdk/concurrency/DefaultExecutors$2;

    invoke-direct {v1, p0, p3, p1, p2}, Lcom/onedrive/sdk/concurrency/DefaultExecutors$2;-><init>(Lcom/onedrive/sdk/concurrency/DefaultExecutors;Lcom/onedrive/sdk/concurrency/IProgressCallback;II)V

    invoke-virtual {v0, v1}, Lcom/onedrive/sdk/concurrency/SynchronousExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public performOnForeground(Lcom/onedrive/sdk/core/ClientException;Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Result:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/onedrive/sdk/core/ClientException;",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "TResult;>;)V"
        }
    .end annotation

    .line 125
    iget-object v0, p0, Lcom/onedrive/sdk/concurrency/DefaultExecutors;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Starting foreground task, current active count:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/onedrive/sdk/concurrency/DefaultExecutors;->mForegroundExecutor:Lcom/onedrive/sdk/concurrency/SynchronousExecutor;

    invoke-virtual {v2}, Lcom/onedrive/sdk/concurrency/SynchronousExecutor;->getActiveCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ", with exception "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 129
    iget-object v0, p0, Lcom/onedrive/sdk/concurrency/DefaultExecutors;->mForegroundExecutor:Lcom/onedrive/sdk/concurrency/SynchronousExecutor;

    new-instance v1, Lcom/onedrive/sdk/concurrency/DefaultExecutors$3;

    invoke-direct {v1, p0, p2, p1}, Lcom/onedrive/sdk/concurrency/DefaultExecutors$3;-><init>(Lcom/onedrive/sdk/concurrency/DefaultExecutors;Lcom/onedrive/sdk/concurrency/ICallback;Lcom/onedrive/sdk/core/ClientException;)V

    invoke-virtual {v0, v1}, Lcom/onedrive/sdk/concurrency/SynchronousExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public performOnForeground(Ljava/lang/Object;Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Result:",
            "Ljava/lang/Object;",
            ">(TResult;",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "TResult;>;)V"
        }
    .end annotation

    .line 81
    iget-object v0, p0, Lcom/onedrive/sdk/concurrency/DefaultExecutors;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Starting foreground task, current active count:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/onedrive/sdk/concurrency/DefaultExecutors;->mForegroundExecutor:Lcom/onedrive/sdk/concurrency/SynchronousExecutor;

    invoke-virtual {v2}, Lcom/onedrive/sdk/concurrency/SynchronousExecutor;->getActiveCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ", with result "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 85
    iget-object v0, p0, Lcom/onedrive/sdk/concurrency/DefaultExecutors;->mForegroundExecutor:Lcom/onedrive/sdk/concurrency/SynchronousExecutor;

    new-instance v1, Lcom/onedrive/sdk/concurrency/DefaultExecutors$1;

    invoke-direct {v1, p0, p2, p1}, Lcom/onedrive/sdk/concurrency/DefaultExecutors$1;-><init>(Lcom/onedrive/sdk/concurrency/DefaultExecutors;Lcom/onedrive/sdk/concurrency/ICallback;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lcom/onedrive/sdk/concurrency/SynchronousExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.class public Lcom/onedrive/sdk/concurrency/ChunkedUploadProvider;
.super Ljava/lang/Object;
.source "ChunkedUploadProvider.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<UploadType:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final DEFAULT_CHUNK_SIZE:I = 0x500000

.field private static final MAXIMUM_CHUNK_SIZE:I = 0x3c00000

.field private static final MAXIMUM_RETRY_TIMES:I = 0x3

.field private static final REQUIRED_CHUNK_SIZE_INCREMENT:I = 0x50000


# instance fields
.field private final mClient:Lcom/onedrive/sdk/extensions/IOneDriveClient;

.field private final mInputStream:Ljava/io/InputStream;

.field private mReadSoFar:I

.field private final mResponseHandler:Lcom/onedrive/sdk/concurrency/ChunkedUploadResponseHandler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/onedrive/sdk/concurrency/ChunkedUploadResponseHandler<",
            "TUploadType;>;"
        }
    .end annotation
.end field

.field private final mStreamSize:I

.field private final mUploadUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/onedrive/sdk/extensions/UploadSession;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/io/InputStream;ILjava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/extensions/UploadSession;",
            "Lcom/onedrive/sdk/extensions/IOneDriveClient;",
            "Ljava/io/InputStream;",
            "I",
            "Ljava/lang/Class<",
            "TUploadType;>;)V"
        }
    .end annotation

    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_3

    if-eqz p2, :cond_2

    if-eqz p3, :cond_1

    if-lez p4, :cond_0

    .line 123
    iput-object p2, p0, Lcom/onedrive/sdk/concurrency/ChunkedUploadProvider;->mClient:Lcom/onedrive/sdk/extensions/IOneDriveClient;

    const/4 p2, 0x0

    .line 124
    iput p2, p0, Lcom/onedrive/sdk/concurrency/ChunkedUploadProvider;->mReadSoFar:I

    .line 125
    iput-object p3, p0, Lcom/onedrive/sdk/concurrency/ChunkedUploadProvider;->mInputStream:Ljava/io/InputStream;

    .line 126
    iput p4, p0, Lcom/onedrive/sdk/concurrency/ChunkedUploadProvider;->mStreamSize:I

    .line 127
    iget-object p1, p1, Lcom/onedrive/sdk/extensions/UploadSession;->uploadUrl:Ljava/lang/String;

    iput-object p1, p0, Lcom/onedrive/sdk/concurrency/ChunkedUploadProvider;->mUploadUrl:Ljava/lang/String;

    .line 128
    new-instance p1, Lcom/onedrive/sdk/concurrency/ChunkedUploadResponseHandler;

    invoke-direct {p1, p5}, Lcom/onedrive/sdk/concurrency/ChunkedUploadResponseHandler;-><init>(Ljava/lang/Class;)V

    iput-object p1, p0, Lcom/onedrive/sdk/concurrency/ChunkedUploadProvider;->mResponseHandler:Lcom/onedrive/sdk/concurrency/ChunkedUploadResponseHandler;

    return-void

    .line 120
    :cond_0
    new-instance p1, Ljava/security/InvalidParameterException;

    const-string p2, "Stream size should larger than 0."

    invoke-direct {p1, p2}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 116
    :cond_1
    new-instance p1, Ljava/security/InvalidParameterException;

    const-string p2, "Input stream is null."

    invoke-direct {p1, p2}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 112
    :cond_2
    new-instance p1, Ljava/security/InvalidParameterException;

    const-string p2, "OneDrive client is null."

    invoke-direct {p1, p2}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 108
    :cond_3
    new-instance p1, Ljava/security/InvalidParameterException;

    const-string p2, "Upload session is null."

    invoke-direct {p1, p2}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public varargs upload(Ljava/util/List;Lcom/onedrive/sdk/concurrency/IProgressCallback;[I)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;",
            "Lcom/onedrive/sdk/concurrency/IProgressCallback<",
            "TUploadType;>;[I)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 146
    array-length v0, p3

    if-lez v0, :cond_0

    const/4 v0, 0x0

    .line 147
    aget v0, p3, v0

    goto :goto_0

    :cond_0
    const/high16 v0, 0x500000

    :goto_0
    const/4 v1, 0x3

    .line 152
    array-length v2, p3

    const/4 v3, 0x1

    if-le v2, v3, :cond_1

    .line 153
    aget v1, p3, v3

    :cond_1
    const/high16 p3, 0x50000

    .line 156
    rem-int p3, v0, p3

    if-nez p3, :cond_8

    const/high16 p3, 0x3c00000

    if-gt v0, p3, :cond_7

    .line 164
    new-array p3, v0, [B

    .line 166
    :goto_1
    iget v0, p0, Lcom/onedrive/sdk/concurrency/ChunkedUploadProvider;->mReadSoFar:I

    iget v2, p0, Lcom/onedrive/sdk/concurrency/ChunkedUploadProvider;->mStreamSize:I

    if-ge v0, v2, :cond_6

    .line 167
    iget-object v0, p0, Lcom/onedrive/sdk/concurrency/ChunkedUploadProvider;->mInputStream:Ljava/io/InputStream;

    invoke-virtual {v0, p3}, Ljava/io/InputStream;->read([B)I

    move-result v0

    const/4 v2, -0x1

    if-ne v0, v2, :cond_2

    goto :goto_3

    .line 173
    :cond_2
    new-instance v11, Lcom/onedrive/sdk/concurrency/ChunkedUploadRequest;

    iget-object v3, p0, Lcom/onedrive/sdk/concurrency/ChunkedUploadProvider;->mUploadUrl:Ljava/lang/String;

    iget-object v4, p0, Lcom/onedrive/sdk/concurrency/ChunkedUploadProvider;->mClient:Lcom/onedrive/sdk/extensions/IOneDriveClient;

    iget v9, p0, Lcom/onedrive/sdk/concurrency/ChunkedUploadProvider;->mReadSoFar:I

    iget v10, p0, Lcom/onedrive/sdk/concurrency/ChunkedUploadProvider;->mStreamSize:I

    move-object v2, v11

    move-object v5, p1

    move-object v6, p3

    move v7, v0

    move v8, v1

    invoke-direct/range {v2 .. v10}, Lcom/onedrive/sdk/concurrency/ChunkedUploadRequest;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;[BIIII)V

    .line 176
    iget-object v2, p0, Lcom/onedrive/sdk/concurrency/ChunkedUploadProvider;->mResponseHandler:Lcom/onedrive/sdk/concurrency/ChunkedUploadResponseHandler;

    invoke-virtual {v11, v2}, Lcom/onedrive/sdk/concurrency/ChunkedUploadRequest;->upload(Lcom/onedrive/sdk/concurrency/ChunkedUploadResponseHandler;)Lcom/onedrive/sdk/extensions/ChunkedUploadResult;

    move-result-object v2

    .line 178
    invoke-virtual {v2}, Lcom/onedrive/sdk/extensions/ChunkedUploadResult;->uploadCompleted()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 179
    iget p1, p0, Lcom/onedrive/sdk/concurrency/ChunkedUploadProvider;->mStreamSize:I

    int-to-long v0, p1

    int-to-long v3, p1

    invoke-interface {p2, v0, v1, v3, v4}, Lcom/onedrive/sdk/concurrency/IProgressCallback;->progress(JJ)V

    .line 180
    invoke-virtual {v2}, Lcom/onedrive/sdk/extensions/ChunkedUploadResult;->getItem()Ljava/lang/Object;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/onedrive/sdk/concurrency/IProgressCallback;->success(Ljava/lang/Object;)V

    goto :goto_3

    .line 182
    :cond_3
    invoke-virtual {v2}, Lcom/onedrive/sdk/extensions/ChunkedUploadResult;->chunkCompleted()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 183
    iget v2, p0, Lcom/onedrive/sdk/concurrency/ChunkedUploadProvider;->mReadSoFar:I

    int-to-long v2, v2

    iget v4, p0, Lcom/onedrive/sdk/concurrency/ChunkedUploadProvider;->mStreamSize:I

    int-to-long v4, v4

    invoke-interface {p2, v2, v3, v4, v5}, Lcom/onedrive/sdk/concurrency/IProgressCallback;->progress(JJ)V

    goto :goto_2

    .line 184
    :cond_4
    invoke-virtual {v2}, Lcom/onedrive/sdk/extensions/ChunkedUploadResult;->hasError()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 185
    invoke-virtual {v2}, Lcom/onedrive/sdk/extensions/ChunkedUploadResult;->getError()Lcom/onedrive/sdk/core/ClientException;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/onedrive/sdk/concurrency/IProgressCallback;->failure(Lcom/onedrive/sdk/core/ClientException;)V

    goto :goto_3

    .line 189
    :cond_5
    :goto_2
    iget v2, p0, Lcom/onedrive/sdk/concurrency/ChunkedUploadProvider;->mReadSoFar:I

    add-int/2addr v2, v0

    iput v2, p0, Lcom/onedrive/sdk/concurrency/ChunkedUploadProvider;->mReadSoFar:I

    goto :goto_1

    :cond_6
    :goto_3
    return-void

    .line 161
    :cond_7
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Please set chunk size smaller than 60 MiB"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 157
    :cond_8
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Chunk size must be a multiple of 320 KiB"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

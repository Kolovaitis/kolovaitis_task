.class public Lcom/onedrive/sdk/concurrency/ChunkedUploadRequest;
.super Ljava/lang/Object;
.source "ChunkedUploadRequest.java"


# static fields
.field private static final CONTENT_RANGE_FORMAT:Ljava/lang/String; = "bytes %1$d-%2$d/%3$d"

.field private static final CONTENT_RANGE_HEADER_NAME:Ljava/lang/String; = "Content-Range"

.field private static final RETRY_DELAY:I = 0x7d0


# instance fields
.field private final mBaseRequest:Lcom/onedrive/sdk/http/BaseRequest;

.field private final mData:[B

.field private final mMaxRetry:I

.field private mRetryCount:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;[BIIII)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/onedrive/sdk/extensions/IOneDriveClient;",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;[BIIII)V"
        }
    .end annotation

    move-object v6, p0

    move v7, p5

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    new-array v0, v7, [B

    iput-object v0, v6, Lcom/onedrive/sdk/concurrency/ChunkedUploadRequest;->mData:[B

    .line 95
    iget-object v0, v6, Lcom/onedrive/sdk/concurrency/ChunkedUploadRequest;->mData:[B

    const/4 v8, 0x0

    move-object v1, p4

    invoke-static {p4, v8, v0, v8, p5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 96
    iput v8, v6, Lcom/onedrive/sdk/concurrency/ChunkedUploadRequest;->mRetryCount:I

    move/from16 v0, p6

    .line 97
    iput v0, v6, Lcom/onedrive/sdk/concurrency/ChunkedUploadRequest;->mMaxRetry:I

    .line 98
    new-instance v9, Lcom/onedrive/sdk/concurrency/ChunkedUploadRequest$1;

    const-class v5, Lcom/onedrive/sdk/extensions/ChunkedUploadResult;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/onedrive/sdk/concurrency/ChunkedUploadRequest$1;-><init>(Lcom/onedrive/sdk/concurrency/ChunkedUploadRequest;Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;Ljava/lang/Class;)V

    iput-object v9, v6, Lcom/onedrive/sdk/concurrency/ChunkedUploadRequest;->mBaseRequest:Lcom/onedrive/sdk/http/BaseRequest;

    .line 99
    iget-object v0, v6, Lcom/onedrive/sdk/concurrency/ChunkedUploadRequest;->mBaseRequest:Lcom/onedrive/sdk/http/BaseRequest;

    sget-object v1, Lcom/onedrive/sdk/http/HttpMethod;->PUT:Lcom/onedrive/sdk/http/HttpMethod;

    invoke-virtual {v0, v1}, Lcom/onedrive/sdk/http/BaseRequest;->setHttpMethod(Lcom/onedrive/sdk/http/HttpMethod;)V

    .line 100
    iget-object v0, v6, Lcom/onedrive/sdk/concurrency/ChunkedUploadRequest;->mBaseRequest:Lcom/onedrive/sdk/http/BaseRequest;

    const-string v1, "Content-Range"

    const-string v2, "bytes %1$d-%2$d/%3$d"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static/range {p7 .. p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v8

    add-int v4, p7, v7

    const/4 v5, 0x1

    sub-int/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static/range {p8 .. p8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x2

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/onedrive/sdk/http/BaseRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public upload(Lcom/onedrive/sdk/concurrency/ChunkedUploadResponseHandler;)Lcom/onedrive/sdk/extensions/ChunkedUploadResult;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<UploadType:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/onedrive/sdk/concurrency/ChunkedUploadResponseHandler<",
            "TUploadType;>;)",
            "Lcom/onedrive/sdk/extensions/ChunkedUploadResult;"
        }
    .end annotation

    .line 116
    :goto_0
    iget v0, p0, Lcom/onedrive/sdk/concurrency/ChunkedUploadRequest;->mRetryCount:I

    iget v1, p0, Lcom/onedrive/sdk/concurrency/ChunkedUploadRequest;->mMaxRetry:I

    const/4 v2, 0x0

    if-ge v0, v1, :cond_1

    mul-int/lit16 v1, v0, 0x7d0

    mul-int v1, v1, v0

    int-to-long v0, v1

    .line 118
    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    .line 120
    iget-object v1, p0, Lcom/onedrive/sdk/concurrency/ChunkedUploadRequest;->mBaseRequest:Lcom/onedrive/sdk/http/BaseRequest;

    invoke-virtual {v1}, Lcom/onedrive/sdk/http/BaseRequest;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v1

    invoke-interface {v1}, Lcom/onedrive/sdk/extensions/IOneDriveClient;->getLogger()Lcom/onedrive/sdk/logger/ILogger;

    move-result-object v1

    const-string v3, "Exception while waiting upload file retry"

    invoke-interface {v1, v3, v0}, Lcom/onedrive/sdk/logger/ILogger;->logError(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 126
    :goto_1
    :try_start_1
    iget-object v0, p0, Lcom/onedrive/sdk/concurrency/ChunkedUploadRequest;->mBaseRequest:Lcom/onedrive/sdk/http/BaseRequest;

    invoke-virtual {v0}, Lcom/onedrive/sdk/http/BaseRequest;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v0

    invoke-interface {v0}, Lcom/onedrive/sdk/extensions/IOneDriveClient;->getHttpProvider()Lcom/onedrive/sdk/http/IHttpProvider;

    move-result-object v0

    iget-object v1, p0, Lcom/onedrive/sdk/concurrency/ChunkedUploadRequest;->mBaseRequest:Lcom/onedrive/sdk/http/BaseRequest;

    const-class v3, Lcom/onedrive/sdk/extensions/ChunkedUploadResult;

    iget-object v4, p0, Lcom/onedrive/sdk/concurrency/ChunkedUploadRequest;->mData:[B

    invoke-interface {v0, v1, v3, v4, p1}, Lcom/onedrive/sdk/http/IHttpProvider;->send(Lcom/onedrive/sdk/http/IHttpRequest;Ljava/lang/Class;Ljava/lang/Object;Lcom/onedrive/sdk/http/IStatefulResponseHandler;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/onedrive/sdk/extensions/ChunkedUploadResult;
    :try_end_1
    .catch Lcom/onedrive/sdk/core/ClientException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 131
    :catch_1
    iget-object v0, p0, Lcom/onedrive/sdk/concurrency/ChunkedUploadRequest;->mBaseRequest:Lcom/onedrive/sdk/http/BaseRequest;

    invoke-virtual {v0}, Lcom/onedrive/sdk/http/BaseRequest;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v0

    invoke-interface {v0}, Lcom/onedrive/sdk/extensions/IOneDriveClient;->getLogger()Lcom/onedrive/sdk/logger/ILogger;

    move-result-object v0

    const-string v1, "Request failed with, retry if necessary."

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    move-object v0, v2

    :goto_2
    if-eqz v0, :cond_0

    .line 134
    invoke-virtual {v0}, Lcom/onedrive/sdk/extensions/ChunkedUploadResult;->chunkCompleted()Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    .line 138
    :cond_0
    iget v0, p0, Lcom/onedrive/sdk/concurrency/ChunkedUploadRequest;->mRetryCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/onedrive/sdk/concurrency/ChunkedUploadRequest;->mRetryCount:I

    goto :goto_0

    .line 141
    :cond_1
    new-instance p1, Lcom/onedrive/sdk/extensions/ChunkedUploadResult;

    new-instance v0, Lcom/onedrive/sdk/core/ClientException;

    const-string v1, "Upload session failed to many times."

    sget-object v3, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->UploadSessionIncomplete:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    invoke-direct {v0, v1, v2, v3}, Lcom/onedrive/sdk/core/ClientException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lcom/onedrive/sdk/core/OneDriveErrorCodes;)V

    invoke-direct {p1, v0}, Lcom/onedrive/sdk/extensions/ChunkedUploadResult;-><init>(Lcom/onedrive/sdk/core/ClientException;)V

    return-object p1
.end method

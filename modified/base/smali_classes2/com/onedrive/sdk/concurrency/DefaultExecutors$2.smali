.class Lcom/onedrive/sdk/concurrency/DefaultExecutors$2;
.super Ljava/lang/Object;
.source "DefaultExecutors.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/onedrive/sdk/concurrency/DefaultExecutors;->performOnForeground(IILcom/onedrive/sdk/concurrency/IProgressCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/onedrive/sdk/concurrency/DefaultExecutors;

.field final synthetic val$callback:Lcom/onedrive/sdk/concurrency/IProgressCallback;

.field final synthetic val$progress:I

.field final synthetic val$progressMax:I


# direct methods
.method constructor <init>(Lcom/onedrive/sdk/concurrency/DefaultExecutors;Lcom/onedrive/sdk/concurrency/IProgressCallback;II)V
    .locals 0

    .line 109
    iput-object p1, p0, Lcom/onedrive/sdk/concurrency/DefaultExecutors$2;->this$0:Lcom/onedrive/sdk/concurrency/DefaultExecutors;

    iput-object p2, p0, Lcom/onedrive/sdk/concurrency/DefaultExecutors$2;->val$callback:Lcom/onedrive/sdk/concurrency/IProgressCallback;

    iput p3, p0, Lcom/onedrive/sdk/concurrency/DefaultExecutors$2;->val$progress:I

    iput p4, p0, Lcom/onedrive/sdk/concurrency/DefaultExecutors$2;->val$progressMax:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 112
    iget-object v0, p0, Lcom/onedrive/sdk/concurrency/DefaultExecutors$2;->val$callback:Lcom/onedrive/sdk/concurrency/IProgressCallback;

    iget v1, p0, Lcom/onedrive/sdk/concurrency/DefaultExecutors$2;->val$progress:I

    int-to-long v1, v1

    iget v3, p0, Lcom/onedrive/sdk/concurrency/DefaultExecutors$2;->val$progressMax:I

    int-to-long v3, v3

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/onedrive/sdk/concurrency/IProgressCallback;->progress(JJ)V

    return-void
.end method

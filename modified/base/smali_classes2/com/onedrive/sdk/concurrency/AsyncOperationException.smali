.class public Lcom/onedrive/sdk/concurrency/AsyncOperationException;
.super Lcom/onedrive/sdk/core/ClientException;
.source "AsyncOperationException.java"


# instance fields
.field private final mStatus:Lcom/onedrive/sdk/extensions/AsyncOperationStatus;


# direct methods
.method public constructor <init>(Lcom/onedrive/sdk/extensions/AsyncOperationStatus;)V
    .locals 3

    .line 44
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p1, Lcom/onedrive/sdk/extensions/AsyncOperationStatus;->status:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/onedrive/sdk/extensions/AsyncOperationStatus;->statusDescription:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->AsyncTaskFailed:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const/4 v2, 0x0

    invoke-direct {p0, v0, v2, v1}, Lcom/onedrive/sdk/core/ClientException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lcom/onedrive/sdk/core/OneDriveErrorCodes;)V

    .line 45
    iput-object p1, p0, Lcom/onedrive/sdk/concurrency/AsyncOperationException;->mStatus:Lcom/onedrive/sdk/extensions/AsyncOperationStatus;

    return-void
.end method


# virtual methods
.method public getStatus()Lcom/onedrive/sdk/extensions/AsyncOperationStatus;
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/onedrive/sdk/concurrency/AsyncOperationException;->mStatus:Lcom/onedrive/sdk/extensions/AsyncOperationStatus;

    return-object v0
.end method

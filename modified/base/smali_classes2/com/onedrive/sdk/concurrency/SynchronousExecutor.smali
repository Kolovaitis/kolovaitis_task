.class public Lcom/onedrive/sdk/concurrency/SynchronousExecutor;
.super Ljava/lang/Object;
.source "SynchronousExecutor.java"

# interfaces
.implements Ljava/util/concurrent/Executor;


# instance fields
.field private mActiveCount:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/onedrive/sdk/concurrency/SynchronousExecutor;->mActiveCount:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method static synthetic access$000(Lcom/onedrive/sdk/concurrency/SynchronousExecutor;)Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/onedrive/sdk/concurrency/SynchronousExecutor;->mActiveCount:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object p0
.end method


# virtual methods
.method public execute(Ljava/lang/Runnable;)V
    .locals 1
    .param p1    # Ljava/lang/Runnable;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 46
    new-instance v0, Lcom/onedrive/sdk/concurrency/SynchronousExecutor$1;

    invoke-direct {v0, p0, p1}, Lcom/onedrive/sdk/concurrency/SynchronousExecutor$1;-><init>(Lcom/onedrive/sdk/concurrency/SynchronousExecutor;Ljava/lang/Runnable;)V

    const/4 p1, 0x0

    .line 58
    new-array p1, p1, [Ljava/lang/Void;

    invoke-virtual {v0, p1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public getActiveCount()I
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/onedrive/sdk/concurrency/SynchronousExecutor;->mActiveCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    return v0
.end method

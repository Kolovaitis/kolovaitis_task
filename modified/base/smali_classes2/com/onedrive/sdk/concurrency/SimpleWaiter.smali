.class public Lcom/onedrive/sdk/concurrency/SimpleWaiter;
.super Ljava/lang/Object;
.source "SimpleWaiter.java"


# instance fields
.field private final mInternalLock:Ljava/lang/Object;

.field private mTriggerState:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/onedrive/sdk/concurrency/SimpleWaiter;->mInternalLock:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public signal()V
    .locals 2

    .line 60
    iget-object v0, p0, Lcom/onedrive/sdk/concurrency/SimpleWaiter;->mInternalLock:Ljava/lang/Object;

    monitor-enter v0

    const/4 v1, 0x1

    .line 61
    :try_start_0
    iput-boolean v1, p0, Lcom/onedrive/sdk/concurrency/SimpleWaiter;->mTriggerState:Z

    .line 62
    iget-object v1, p0, Lcom/onedrive/sdk/concurrency/SimpleWaiter;->mInternalLock:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 63
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public waitForSignal()V
    .locals 3

    .line 44
    iget-object v0, p0, Lcom/onedrive/sdk/concurrency/SimpleWaiter;->mInternalLock:Ljava/lang/Object;

    monitor-enter v0

    .line 45
    :try_start_0
    iget-boolean v1, p0, Lcom/onedrive/sdk/concurrency/SimpleWaiter;->mTriggerState:Z

    if-eqz v1, :cond_0

    .line 46
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    .line 49
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/onedrive/sdk/concurrency/SimpleWaiter;->mInternalLock:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 53
    :try_start_2
    monitor-exit v0

    return-void

    :catch_0
    move-exception v1

    .line 51
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    :catchall_0
    move-exception v1

    .line 53
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

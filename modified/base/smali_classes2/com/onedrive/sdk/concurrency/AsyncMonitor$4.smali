.class Lcom/onedrive/sdk/concurrency/AsyncMonitor$4;
.super Ljava/lang/Object;
.source "AsyncMonitor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/onedrive/sdk/concurrency/AsyncMonitor;->pollForResult(JLcom/onedrive/sdk/concurrency/IProgressCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/onedrive/sdk/concurrency/AsyncMonitor;

.field final synthetic val$callback:Lcom/onedrive/sdk/concurrency/IProgressCallback;

.field final synthetic val$millisBetweenPoll:J


# direct methods
.method constructor <init>(Lcom/onedrive/sdk/concurrency/AsyncMonitor;JLcom/onedrive/sdk/concurrency/IProgressCallback;)V
    .locals 0

    .line 148
    iput-object p1, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor$4;->this$0:Lcom/onedrive/sdk/concurrency/AsyncMonitor;

    iput-wide p2, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor$4;->val$millisBetweenPoll:J

    iput-object p4, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor$4;->val$callback:Lcom/onedrive/sdk/concurrency/IProgressCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    const/4 v0, 0x0

    :cond_0
    if-eqz v0, :cond_1

    .line 156
    :try_start_0
    iget-wide v0, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor$4;->val$millisBetweenPoll:J

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/onedrive/sdk/core/ClientException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 158
    :catch_0
    :try_start_1
    iget-object v0, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor$4;->this$0:Lcom/onedrive/sdk/concurrency/AsyncMonitor;

    invoke-static {v0}, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->access$000(Lcom/onedrive/sdk/concurrency/AsyncMonitor;)Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v0

    invoke-interface {v0}, Lcom/onedrive/sdk/extensions/IOneDriveClient;->getLogger()Lcom/onedrive/sdk/logger/ILogger;

    move-result-object v0

    const-string v1, "InterruptedException ignored"

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 161
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor$4;->this$0:Lcom/onedrive/sdk/concurrency/AsyncMonitor;

    invoke-virtual {v0}, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->getStatus()Lcom/onedrive/sdk/extensions/AsyncOperationStatus;

    move-result-object v0

    .line 162
    iget-object v1, v0, Lcom/onedrive/sdk/extensions/AsyncOperationStatus;->percentageComplete:Ljava/lang/Double;

    if-eqz v1, :cond_2

    .line 163
    iget-object v1, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor$4;->this$0:Lcom/onedrive/sdk/concurrency/AsyncMonitor;

    invoke-static {v1}, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->access$000(Lcom/onedrive/sdk/concurrency/AsyncMonitor;)Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v1

    invoke-interface {v1}, Lcom/onedrive/sdk/extensions/IOneDriveClient;->getExecutors()Lcom/onedrive/sdk/concurrency/IExecutors;

    move-result-object v1

    iget-object v2, v0, Lcom/onedrive/sdk/extensions/AsyncOperationStatus;->percentageComplete:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->intValue()I

    move-result v2

    const/16 v3, 0x64

    iget-object v4, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor$4;->val$callback:Lcom/onedrive/sdk/concurrency/IProgressCallback;

    invoke-interface {v1, v2, v3, v4}, Lcom/onedrive/sdk/concurrency/IExecutors;->performOnForeground(IILcom/onedrive/sdk/concurrency/IProgressCallback;)V

    .line 167
    :cond_2
    iget-object v1, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor$4;->this$0:Lcom/onedrive/sdk/concurrency/AsyncMonitor;

    invoke-static {v1, v0}, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->access$100(Lcom/onedrive/sdk/concurrency/AsyncMonitor;Lcom/onedrive/sdk/extensions/AsyncOperationStatus;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor$4;->this$0:Lcom/onedrive/sdk/concurrency/AsyncMonitor;

    invoke-static {v1, v0}, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->access$200(Lcom/onedrive/sdk/concurrency/AsyncMonitor;Lcom/onedrive/sdk/extensions/AsyncOperationStatus;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 168
    :cond_3
    iget-object v1, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor$4;->this$0:Lcom/onedrive/sdk/concurrency/AsyncMonitor;

    invoke-static {v1}, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->access$000(Lcom/onedrive/sdk/concurrency/AsyncMonitor;)Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v1

    invoke-interface {v1}, Lcom/onedrive/sdk/extensions/IOneDriveClient;->getLogger()Lcom/onedrive/sdk/logger/ILogger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Polling has completed, got final status: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, v0, Lcom/onedrive/sdk/extensions/AsyncOperationStatus;->status:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 169
    iget-object v1, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor$4;->this$0:Lcom/onedrive/sdk/concurrency/AsyncMonitor;

    invoke-static {v1, v0}, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->access$200(Lcom/onedrive/sdk/concurrency/AsyncMonitor;Lcom/onedrive/sdk/extensions/AsyncOperationStatus;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 170
    iget-object v1, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor$4;->this$0:Lcom/onedrive/sdk/concurrency/AsyncMonitor;

    invoke-static {v1}, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->access$000(Lcom/onedrive/sdk/concurrency/AsyncMonitor;)Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v1

    invoke-interface {v1}, Lcom/onedrive/sdk/extensions/IOneDriveClient;->getExecutors()Lcom/onedrive/sdk/concurrency/IExecutors;

    move-result-object v1

    new-instance v2, Lcom/onedrive/sdk/concurrency/AsyncOperationException;

    invoke-direct {v2, v0}, Lcom/onedrive/sdk/concurrency/AsyncOperationException;-><init>(Lcom/onedrive/sdk/extensions/AsyncOperationStatus;)V

    iget-object v0, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor$4;->val$callback:Lcom/onedrive/sdk/concurrency/IProgressCallback;

    invoke-interface {v1, v2, v0}, Lcom/onedrive/sdk/concurrency/IExecutors;->performOnForeground(Lcom/onedrive/sdk/core/ClientException;Lcom/onedrive/sdk/concurrency/ICallback;)V

    .line 174
    :cond_4
    iget-object v0, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor$4;->this$0:Lcom/onedrive/sdk/concurrency/AsyncMonitor;

    invoke-static {v0}, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->access$000(Lcom/onedrive/sdk/concurrency/AsyncMonitor;)Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v0

    invoke-interface {v0}, Lcom/onedrive/sdk/extensions/IOneDriveClient;->getExecutors()Lcom/onedrive/sdk/concurrency/IExecutors;

    move-result-object v0

    iget-object v1, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor$4;->this$0:Lcom/onedrive/sdk/concurrency/AsyncMonitor;

    invoke-virtual {v1}, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->getResult()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor$4;->val$callback:Lcom/onedrive/sdk/concurrency/IProgressCallback;

    invoke-interface {v0, v1, v2}, Lcom/onedrive/sdk/concurrency/IExecutors;->performOnForeground(Ljava/lang/Object;Lcom/onedrive/sdk/concurrency/ICallback;)V
    :try_end_1
    .catch Lcom/onedrive/sdk/core/ClientException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    .line 176
    iget-object v1, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor$4;->this$0:Lcom/onedrive/sdk/concurrency/AsyncMonitor;

    invoke-static {v1}, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->access$000(Lcom/onedrive/sdk/concurrency/AsyncMonitor;)Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v1

    invoke-interface {v1}, Lcom/onedrive/sdk/extensions/IOneDriveClient;->getExecutors()Lcom/onedrive/sdk/concurrency/IExecutors;

    move-result-object v1

    iget-object v2, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor$4;->val$callback:Lcom/onedrive/sdk/concurrency/IProgressCallback;

    invoke-interface {v1, v0, v2}, Lcom/onedrive/sdk/concurrency/IExecutors;->performOnForeground(Lcom/onedrive/sdk/core/ClientException;Lcom/onedrive/sdk/concurrency/ICallback;)V

    :goto_1
    return-void
.end method

.class public Lcom/onedrive/sdk/options/Option;
.super Ljava/lang/Object;
.source "Option.java"


# instance fields
.field private final mName:Ljava/lang/String;

.field private final mValue:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/onedrive/sdk/options/Option;->mName:Ljava/lang/String;

    .line 47
    iput-object p2, p0, Lcom/onedrive/sdk/options/Option;->mValue:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/onedrive/sdk/options/Option;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/onedrive/sdk/options/Option;->mValue:Ljava/lang/String;

    return-object v0
.end method

.class public Lcom/onedrive/sdk/extensions/UploadSession;
.super Lcom/onedrive/sdk/generated/BaseUploadSession;
.source "UploadSession.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<UploadType:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/onedrive/sdk/generated/BaseUploadSession;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 35
    invoke-direct {p0}, Lcom/onedrive/sdk/generated/BaseUploadSession;-><init>()V

    return-void
.end method


# virtual methods
.method public createUploadProvider(Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/io/InputStream;ILjava/lang/Class;)Lcom/onedrive/sdk/concurrency/ChunkedUploadProvider;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/extensions/IOneDriveClient;",
            "Ljava/io/InputStream;",
            "I",
            "Ljava/lang/Class<",
            "TUploadType;>;)",
            "Lcom/onedrive/sdk/concurrency/ChunkedUploadProvider;"
        }
    .end annotation

    .line 46
    new-instance v6, Lcom/onedrive/sdk/concurrency/ChunkedUploadProvider;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/onedrive/sdk/concurrency/ChunkedUploadProvider;-><init>(Lcom/onedrive/sdk/extensions/UploadSession;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/io/InputStream;ILjava/lang/Class;)V

    return-object v6
.end method

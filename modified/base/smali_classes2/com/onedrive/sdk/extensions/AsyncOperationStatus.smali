.class public Lcom/onedrive/sdk/extensions/AsyncOperationStatus;
.super Lcom/onedrive/sdk/generated/BaseAsyncOperationStatus;
.source "AsyncOperationStatus.java"


# instance fields
.field public seeOther:Ljava/lang/String;

.field public statusDescription:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 40
    invoke-direct {p0}, Lcom/onedrive/sdk/generated/BaseAsyncOperationStatus;-><init>()V

    return-void
.end method

.method public static createdCompleted(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/AsyncOperationStatus;
    .locals 3

    .line 58
    new-instance v0, Lcom/onedrive/sdk/extensions/AsyncOperationStatus;

    invoke-direct {v0}, Lcom/onedrive/sdk/extensions/AsyncOperationStatus;-><init>()V

    .line 59
    iput-object p0, v0, Lcom/onedrive/sdk/extensions/AsyncOperationStatus;->seeOther:Ljava/lang/String;

    const-wide/high16 v1, 0x4059000000000000L    # 100.0

    .line 60
    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p0

    iput-object p0, v0, Lcom/onedrive/sdk/extensions/AsyncOperationStatus;->percentageComplete:Ljava/lang/Double;

    const-string p0, "Completed"

    .line 61
    iput-object p0, v0, Lcom/onedrive/sdk/extensions/AsyncOperationStatus;->status:Ljava/lang/String;

    return-object v0
.end method

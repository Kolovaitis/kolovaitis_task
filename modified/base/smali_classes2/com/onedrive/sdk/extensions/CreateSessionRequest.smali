.class public Lcom/onedrive/sdk/extensions/CreateSessionRequest;
.super Lcom/onedrive/sdk/generated/BaseCreateSessionRequest;
.source "CreateSessionRequest.java"

# interfaces
.implements Lcom/onedrive/sdk/extensions/ICreateSessionRequest;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;Lcom/onedrive/sdk/extensions/ChunkedUploadSessionDescriptor;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/onedrive/sdk/extensions/IOneDriveClient;",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;",
            "Lcom/onedrive/sdk/extensions/ChunkedUploadSessionDescriptor;",
            ")V"
        }
    .end annotation

    .line 50
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/onedrive/sdk/generated/BaseCreateSessionRequest;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;Lcom/onedrive/sdk/extensions/ChunkedUploadSessionDescriptor;)V

    return-void
.end method

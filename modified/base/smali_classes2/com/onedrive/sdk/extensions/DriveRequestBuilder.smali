.class public Lcom/onedrive/sdk/extensions/DriveRequestBuilder;
.super Lcom/onedrive/sdk/generated/BaseDriveRequestBuilder;
.source "DriveRequestBuilder.java"

# interfaces
.implements Lcom/onedrive/sdk/extensions/IDriveRequestBuilder;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/onedrive/sdk/extensions/IOneDriveClient;",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;)V"
        }
    .end annotation

    .line 50
    invoke-direct {p0, p1, p2, p3}, Lcom/onedrive/sdk/generated/BaseDriveRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public getRoot()Lcom/onedrive/sdk/extensions/IItemRequestBuilder;
    .locals 4

    .line 59
    new-instance v0, Lcom/onedrive/sdk/extensions/ItemRequestBuilder;

    const-string v1, "root"

    invoke-virtual {p0, v1}, Lcom/onedrive/sdk/extensions/DriveRequestBuilder;->getRequestUrlWithAdditionalSegment(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/onedrive/sdk/extensions/DriveRequestBuilder;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/onedrive/sdk/extensions/ItemRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V

    return-object v0
.end method

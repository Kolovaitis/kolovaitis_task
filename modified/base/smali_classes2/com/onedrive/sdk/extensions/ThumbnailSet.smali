.class public Lcom/onedrive/sdk/extensions/ThumbnailSet;
.super Lcom/onedrive/sdk/generated/BaseThumbnailSet;
.source "ThumbnailSet.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 40
    invoke-direct {p0}, Lcom/onedrive/sdk/generated/BaseThumbnailSet;-><init>()V

    return-void
.end method


# virtual methods
.method public getCustomThumbnail(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/Thumbnail;
    .locals 2

    .line 48
    invoke-virtual {p0}, Lcom/onedrive/sdk/extensions/ThumbnailSet;->getRawObject()Lcom/google/gson/JsonObject;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/gson/JsonObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    invoke-virtual {p0}, Lcom/onedrive/sdk/extensions/ThumbnailSet;->getSerializer()Lcom/onedrive/sdk/serializer/ISerializer;

    move-result-object v0

    invoke-virtual {p0}, Lcom/onedrive/sdk/extensions/ThumbnailSet;->getRawObject()Lcom/google/gson/JsonObject;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/gson/JsonObject;->get(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/gson/JsonElement;->toString()Ljava/lang/String;

    move-result-object p1

    const-class v1, Lcom/onedrive/sdk/extensions/Thumbnail;

    invoke-interface {v0, p1, v1}, Lcom/onedrive/sdk/serializer/ISerializer;->deserializeObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/onedrive/sdk/extensions/Thumbnail;

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

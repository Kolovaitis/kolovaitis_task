.class Lcom/onedrive/sdk/extensions/OneDriveClient$Builder$1;
.super Ljava/lang/Object;
.source "OneDriveClient.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;->loginAndBuildClient(Landroid/app/Activity;Lcom/onedrive/sdk/concurrency/ICallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;

.field final synthetic val$activity:Landroid/app/Activity;

.field final synthetic val$callback:Lcom/onedrive/sdk/concurrency/ICallback;


# direct methods
.method constructor <init>(Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;Landroid/app/Activity;Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 0

    .line 142
    iput-object p1, p0, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder$1;->this$0:Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;

    iput-object p2, p0, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder$1;->val$activity:Landroid/app/Activity;

    iput-object p3, p0, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder$1;->val$callback:Lcom/onedrive/sdk/concurrency/ICallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 145
    iget-object v0, p0, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder$1;->this$0:Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;

    invoke-static {v0}, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;->access$400(Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;)Lcom/onedrive/sdk/extensions/OneDriveClient;

    move-result-object v0

    invoke-virtual {v0}, Lcom/onedrive/sdk/extensions/OneDriveClient;->getExecutors()Lcom/onedrive/sdk/concurrency/IExecutors;

    move-result-object v0

    .line 147
    :try_start_0
    iget-object v1, p0, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder$1;->this$0:Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;

    iget-object v2, p0, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder$1;->val$activity:Landroid/app/Activity;

    invoke-static {v1, v2}, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;->access$500(Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;Landroid/app/Activity;)Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v1

    iget-object v2, p0, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder$1;->val$callback:Lcom/onedrive/sdk/concurrency/ICallback;

    invoke-interface {v0, v1, v2}, Lcom/onedrive/sdk/concurrency/IExecutors;->performOnForeground(Ljava/lang/Object;Lcom/onedrive/sdk/concurrency/ICallback;)V
    :try_end_0
    .catch Lcom/onedrive/sdk/core/ClientException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 149
    iget-object v2, p0, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder$1;->val$callback:Lcom/onedrive/sdk/concurrency/ICallback;

    invoke-interface {v0, v1, v2}, Lcom/onedrive/sdk/concurrency/IExecutors;->performOnForeground(Lcom/onedrive/sdk/core/ClientException;Lcom/onedrive/sdk/concurrency/ICallback;)V

    :goto_0
    return-void
.end method

.class public Lcom/onedrive/sdk/extensions/ThumbnailSetRequestBuilder;
.super Lcom/onedrive/sdk/generated/BaseThumbnailSetRequestBuilder;
.source "ThumbnailSetRequestBuilder.java"

# interfaces
.implements Lcom/onedrive/sdk/extensions/IThumbnailSetRequestBuilder;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/onedrive/sdk/extensions/IOneDriveClient;",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;)V"
        }
    .end annotation

    .line 50
    invoke-direct {p0, p1, p2, p3}, Lcom/onedrive/sdk/generated/BaseThumbnailSetRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public getThumbnailSize(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IThumbnailRequestBuilder;
    .locals 3

    .line 55
    new-instance v0, Lcom/onedrive/sdk/extensions/ThumbnailRequestBuilder;

    invoke-virtual {p0, p1}, Lcom/onedrive/sdk/extensions/ThumbnailSetRequestBuilder;->getRequestUrlWithAdditionalSegment(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0}, Lcom/onedrive/sdk/extensions/ThumbnailSetRequestBuilder;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, v2}, Lcom/onedrive/sdk/extensions/ThumbnailRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V

    return-object v0
.end method

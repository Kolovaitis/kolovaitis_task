.class public Lcom/onedrive/sdk/extensions/CopyRequestBuilder;
.super Lcom/onedrive/sdk/generated/BaseCopyRequestBuilder;
.source "CopyRequestBuilder.java"

# interfaces
.implements Lcom/onedrive/sdk/extensions/ICopyRequestBuilder;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;Ljava/lang/String;Lcom/onedrive/sdk/extensions/ItemReference;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/onedrive/sdk/extensions/IOneDriveClient;",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/onedrive/sdk/extensions/ItemReference;",
            ")V"
        }
    .end annotation

    .line 50
    invoke-direct/range {p0 .. p5}, Lcom/onedrive/sdk/generated/BaseCopyRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;Ljava/lang/String;Lcom/onedrive/sdk/extensions/ItemReference;)V

    return-void
.end method

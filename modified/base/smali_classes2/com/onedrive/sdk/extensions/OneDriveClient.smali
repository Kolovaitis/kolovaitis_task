.class public Lcom/onedrive/sdk/extensions/OneDriveClient;
.super Lcom/onedrive/sdk/generated/BaseOneDriveClient;
.source "OneDriveClient.java"

# interfaces
.implements Lcom/onedrive/sdk/extensions/IOneDriveClient;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;
    }
.end annotation


# direct methods
.method protected constructor <init>()V
    .locals 0

    .line 49
    invoke-direct {p0}, Lcom/onedrive/sdk/generated/BaseOneDriveClient;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/onedrive/sdk/extensions/OneDriveClient;Lcom/onedrive/sdk/http/IHttpProvider;)V
    .locals 0

    .line 44
    invoke-virtual {p0, p1}, Lcom/onedrive/sdk/extensions/OneDriveClient;->setHttpProvider(Lcom/onedrive/sdk/http/IHttpProvider;)V

    return-void
.end method

.method static synthetic access$100(Lcom/onedrive/sdk/extensions/OneDriveClient;Lcom/onedrive/sdk/authentication/IAuthenticator;)V
    .locals 0

    .line 44
    invoke-virtual {p0, p1}, Lcom/onedrive/sdk/extensions/OneDriveClient;->setAuthenticator(Lcom/onedrive/sdk/authentication/IAuthenticator;)V

    return-void
.end method

.method static synthetic access$200(Lcom/onedrive/sdk/extensions/OneDriveClient;Lcom/onedrive/sdk/concurrency/IExecutors;)V
    .locals 0

    .line 44
    invoke-virtual {p0, p1}, Lcom/onedrive/sdk/extensions/OneDriveClient;->setExecutors(Lcom/onedrive/sdk/concurrency/IExecutors;)V

    return-void
.end method

.method static synthetic access$300(Lcom/onedrive/sdk/extensions/OneDriveClient;Lcom/onedrive/sdk/logger/ILogger;)V
    .locals 0

    .line 44
    invoke-virtual {p0, p1}, Lcom/onedrive/sdk/extensions/OneDriveClient;->setLogger(Lcom/onedrive/sdk/logger/ILogger;)V

    return-void
.end method


# virtual methods
.method public getDrive()Lcom/onedrive/sdk/extensions/IDriveRequestBuilder;
    .locals 3

    .line 58
    new-instance v0, Lcom/onedrive/sdk/extensions/DriveRequestBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/onedrive/sdk/extensions/OneDriveClient;->getServiceRoot()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "/drive"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, Lcom/onedrive/sdk/extensions/DriveRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V

    return-object v0
.end method

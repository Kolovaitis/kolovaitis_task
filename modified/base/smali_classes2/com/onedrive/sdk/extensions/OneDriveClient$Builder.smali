.class public Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;
.super Ljava/lang/Object;
.source "OneDriveClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/onedrive/sdk/extensions/OneDriveClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private final mClient:Lcom/onedrive/sdk/extensions/OneDriveClient;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    new-instance v0, Lcom/onedrive/sdk/extensions/OneDriveClient;

    invoke-direct {v0}, Lcom/onedrive/sdk/extensions/OneDriveClient;-><init>()V

    iput-object v0, p0, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;->mClient:Lcom/onedrive/sdk/extensions/OneDriveClient;

    return-void
.end method

.method static synthetic access$400(Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;)Lcom/onedrive/sdk/extensions/OneDriveClient;
    .locals 0

    .line 64
    iget-object p0, p0, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;->mClient:Lcom/onedrive/sdk/extensions/OneDriveClient;

    return-object p0
.end method

.method static synthetic access$500(Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;Landroid/app/Activity;)Lcom/onedrive/sdk/extensions/IOneDriveClient;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    .line 64
    invoke-direct {p0, p1}, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;->loginAndBuildClient(Landroid/app/Activity;)Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object p0

    return-object p0
.end method

.method private logger(Lcom/onedrive/sdk/logger/ILogger;)Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;
    .locals 1

    .line 117
    iget-object v0, p0, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;->mClient:Lcom/onedrive/sdk/extensions/OneDriveClient;

    invoke-static {v0, p1}, Lcom/onedrive/sdk/extensions/OneDriveClient;->access$300(Lcom/onedrive/sdk/extensions/OneDriveClient;Lcom/onedrive/sdk/logger/ILogger;)V

    return-object p0
.end method

.method private loginAndBuildClient(Landroid/app/Activity;)Lcom/onedrive/sdk/extensions/IOneDriveClient;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    .line 161
    iget-object v0, p0, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;->mClient:Lcom/onedrive/sdk/extensions/OneDriveClient;

    invoke-virtual {v0}, Lcom/onedrive/sdk/extensions/OneDriveClient;->validate()V

    .line 163
    iget-object v0, p0, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;->mClient:Lcom/onedrive/sdk/extensions/OneDriveClient;

    invoke-virtual {v0}, Lcom/onedrive/sdk/extensions/OneDriveClient;->getAuthenticator()Lcom/onedrive/sdk/authentication/IAuthenticator;

    move-result-object v0

    iget-object v1, p0, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;->mClient:Lcom/onedrive/sdk/extensions/OneDriveClient;

    invoke-virtual {v1}, Lcom/onedrive/sdk/extensions/OneDriveClient;->getExecutors()Lcom/onedrive/sdk/concurrency/IExecutors;

    move-result-object v1

    iget-object v2, p0, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;->mClient:Lcom/onedrive/sdk/extensions/OneDriveClient;

    invoke-virtual {v2}, Lcom/onedrive/sdk/extensions/OneDriveClient;->getHttpProvider()Lcom/onedrive/sdk/http/IHttpProvider;

    move-result-object v2

    iget-object v3, p0, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;->mClient:Lcom/onedrive/sdk/extensions/OneDriveClient;

    invoke-virtual {v3}, Lcom/onedrive/sdk/extensions/OneDriveClient;->getLogger()Lcom/onedrive/sdk/logger/ILogger;

    move-result-object v3

    invoke-interface {v0, v1, v2, p1, v3}, Lcom/onedrive/sdk/authentication/IAuthenticator;->init(Lcom/onedrive/sdk/concurrency/IExecutors;Lcom/onedrive/sdk/http/IHttpProvider;Landroid/app/Activity;Lcom/onedrive/sdk/logger/ILogger;)V

    const/4 p1, 0x0

    .line 168
    :try_start_0
    iget-object v0, p0, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;->mClient:Lcom/onedrive/sdk/extensions/OneDriveClient;

    invoke-virtual {v0}, Lcom/onedrive/sdk/extensions/OneDriveClient;->getAuthenticator()Lcom/onedrive/sdk/authentication/IAuthenticator;

    move-result-object v0

    invoke-interface {v0}, Lcom/onedrive/sdk/authentication/IAuthenticator;->loginSilent()Lcom/onedrive/sdk/authentication/IAccountInfo;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-object v0, p1

    :goto_0
    if-nez v0, :cond_1

    .line 172
    iget-object v0, p0, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;->mClient:Lcom/onedrive/sdk/extensions/OneDriveClient;

    invoke-virtual {v0}, Lcom/onedrive/sdk/extensions/OneDriveClient;->getAuthenticator()Lcom/onedrive/sdk/authentication/IAuthenticator;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/onedrive/sdk/authentication/IAuthenticator;->login(Ljava/lang/String;)Lcom/onedrive/sdk/authentication/IAccountInfo;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_1

    .line 174
    :cond_0
    new-instance p1, Lcom/onedrive/sdk/authentication/ClientAuthenticatorException;

    sget-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->AuthenticationFailure:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "Unable to authenticate silently or interactively"

    invoke-direct {p1, v1, v0}, Lcom/onedrive/sdk/authentication/ClientAuthenticatorException;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/core/OneDriveErrorCodes;)V

    throw p1

    .line 178
    :cond_1
    :goto_1
    iget-object p1, p0, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;->mClient:Lcom/onedrive/sdk/extensions/OneDriveClient;

    return-object p1
.end method


# virtual methods
.method public authenticator(Lcom/onedrive/sdk/authentication/IAuthenticator;)Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;
    .locals 1

    .line 97
    iget-object v0, p0, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;->mClient:Lcom/onedrive/sdk/extensions/OneDriveClient;

    invoke-static {v0, p1}, Lcom/onedrive/sdk/extensions/OneDriveClient;->access$100(Lcom/onedrive/sdk/extensions/OneDriveClient;Lcom/onedrive/sdk/authentication/IAuthenticator;)V

    return-object p0
.end method

.method public executors(Lcom/onedrive/sdk/concurrency/IExecutors;)Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;
    .locals 1

    .line 107
    iget-object v0, p0, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;->mClient:Lcom/onedrive/sdk/extensions/OneDriveClient;

    invoke-static {v0, p1}, Lcom/onedrive/sdk/extensions/OneDriveClient;->access$200(Lcom/onedrive/sdk/extensions/OneDriveClient;Lcom/onedrive/sdk/concurrency/IExecutors;)V

    return-object p0
.end method

.method public fromConfig(Lcom/onedrive/sdk/core/IClientConfig;)Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;
    .locals 2

    .line 127
    invoke-interface {p1}, Lcom/onedrive/sdk/core/IClientConfig;->getAuthenticator()Lcom/onedrive/sdk/authentication/IAuthenticator;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;->authenticator(Lcom/onedrive/sdk/authentication/IAuthenticator;)Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;

    move-result-object v0

    invoke-interface {p1}, Lcom/onedrive/sdk/core/IClientConfig;->getExecutors()Lcom/onedrive/sdk/concurrency/IExecutors;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;->executors(Lcom/onedrive/sdk/concurrency/IExecutors;)Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;

    move-result-object v0

    invoke-interface {p1}, Lcom/onedrive/sdk/core/IClientConfig;->getHttpProvider()Lcom/onedrive/sdk/http/IHttpProvider;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;->httpProvider(Lcom/onedrive/sdk/http/IHttpProvider;)Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;

    move-result-object v0

    invoke-interface {p1}, Lcom/onedrive/sdk/core/IClientConfig;->getLogger()Lcom/onedrive/sdk/logger/ILogger;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;->logger(Lcom/onedrive/sdk/logger/ILogger;)Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;

    move-result-object v0

    invoke-interface {p1}, Lcom/onedrive/sdk/core/IClientConfig;->getSerializer()Lcom/onedrive/sdk/serializer/ISerializer;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;->serializer(Lcom/onedrive/sdk/serializer/ISerializer;)Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;

    move-result-object p1

    return-object p1
.end method

.method public httpProvider(Lcom/onedrive/sdk/http/IHttpProvider;)Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;
    .locals 1

    .line 87
    iget-object v0, p0, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;->mClient:Lcom/onedrive/sdk/extensions/OneDriveClient;

    invoke-static {v0, p1}, Lcom/onedrive/sdk/extensions/OneDriveClient;->access$000(Lcom/onedrive/sdk/extensions/OneDriveClient;Lcom/onedrive/sdk/http/IHttpProvider;)V

    return-object p0
.end method

.method public loginAndBuildClient(Landroid/app/Activity;Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/extensions/IOneDriveClient;",
            ">;)V"
        }
    .end annotation

    .line 140
    iget-object v0, p0, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;->mClient:Lcom/onedrive/sdk/extensions/OneDriveClient;

    invoke-virtual {v0}, Lcom/onedrive/sdk/extensions/OneDriveClient;->validate()V

    .line 142
    iget-object v0, p0, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;->mClient:Lcom/onedrive/sdk/extensions/OneDriveClient;

    invoke-virtual {v0}, Lcom/onedrive/sdk/extensions/OneDriveClient;->getExecutors()Lcom/onedrive/sdk/concurrency/IExecutors;

    move-result-object v0

    new-instance v1, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder$1;-><init>(Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;Landroid/app/Activity;Lcom/onedrive/sdk/concurrency/ICallback;)V

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/concurrency/IExecutors;->performOnBackground(Ljava/lang/Runnable;)V

    return-void
.end method

.method public serializer(Lcom/onedrive/sdk/serializer/ISerializer;)Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;->mClient:Lcom/onedrive/sdk/extensions/OneDriveClient;

    invoke-virtual {v0, p1}, Lcom/onedrive/sdk/extensions/OneDriveClient;->setSerializer(Lcom/onedrive/sdk/serializer/ISerializer;)V

    return-object p0
.end method

.class synthetic Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator$5;
.super Ljava/lang/Object;
.source "DisambiguationAuthenticator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$onedrive$sdk$authentication$AccountType:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 214
    invoke-static {}, Lcom/onedrive/sdk/authentication/AccountType;->values()[Lcom/onedrive/sdk/authentication/AccountType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator$5;->$SwitchMap$com$onedrive$sdk$authentication$AccountType:[I

    :try_start_0
    sget-object v0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator$5;->$SwitchMap$com$onedrive$sdk$authentication$AccountType:[I

    sget-object v1, Lcom/onedrive/sdk/authentication/AccountType;->ActiveDirectory:Lcom/onedrive/sdk/authentication/AccountType;

    invoke-virtual {v1}, Lcom/onedrive/sdk/authentication/AccountType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :try_start_1
    sget-object v0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator$5;->$SwitchMap$com$onedrive$sdk$authentication$AccountType:[I

    sget-object v1, Lcom/onedrive/sdk/authentication/AccountType;->MicrosoftAccount:Lcom/onedrive/sdk/authentication/AccountType;

    invoke-virtual {v1}, Lcom/onedrive/sdk/authentication/AccountType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    return-void
.end method

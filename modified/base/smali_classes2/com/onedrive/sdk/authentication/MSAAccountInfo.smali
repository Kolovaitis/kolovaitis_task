.class public Lcom/onedrive/sdk/authentication/MSAAccountInfo;
.super Ljava/lang/Object;
.source "MSAAccountInfo.java"

# interfaces
.implements Lcom/onedrive/sdk/authentication/IAccountInfo;


# static fields
.field public static final ONE_DRIVE_PERSONAL_SERVICE_ROOT:Ljava/lang/String; = "https://api.onedrive.com/v1.0"


# instance fields
.field private final mAuthenticator:Lcom/onedrive/sdk/authentication/MSAAuthenticator;

.field private final mLogger:Lcom/onedrive/sdk/logger/ILogger;

.field private mSession:Lcom/microsoft/services/msa/LiveConnectSession;


# direct methods
.method public constructor <init>(Lcom/onedrive/sdk/authentication/MSAAuthenticator;Lcom/microsoft/services/msa/LiveConnectSession;Lcom/onedrive/sdk/logger/ILogger;)V
    .locals 0

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p1, p0, Lcom/onedrive/sdk/authentication/MSAAccountInfo;->mAuthenticator:Lcom/onedrive/sdk/authentication/MSAAuthenticator;

    .line 63
    iput-object p2, p0, Lcom/onedrive/sdk/authentication/MSAAccountInfo;->mSession:Lcom/microsoft/services/msa/LiveConnectSession;

    .line 64
    iput-object p3, p0, Lcom/onedrive/sdk/authentication/MSAAccountInfo;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    return-void
.end method


# virtual methods
.method public getAccessToken()Ljava/lang/String;
    .locals 1

    .line 82
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/MSAAccountInfo;->mSession:Lcom/microsoft/services/msa/LiveConnectSession;

    invoke-virtual {v0}, Lcom/microsoft/services/msa/LiveConnectSession;->getAccessToken()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAccountType()Lcom/onedrive/sdk/authentication/AccountType;
    .locals 1

    .line 73
    sget-object v0, Lcom/onedrive/sdk/authentication/AccountType;->MicrosoftAccount:Lcom/onedrive/sdk/authentication/AccountType;

    return-object v0
.end method

.method public getServiceRoot()Ljava/lang/String;
    .locals 1

    const-string v0, "https://api.onedrive.com/v1.0"

    return-object v0
.end method

.method public isExpired()Z
    .locals 1

    .line 101
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/MSAAccountInfo;->mSession:Lcom/microsoft/services/msa/LiveConnectSession;

    invoke-virtual {v0}, Lcom/microsoft/services/msa/LiveConnectSession;->isExpired()Z

    move-result v0

    return v0
.end method

.method public refresh()V
    .locals 2

    .line 109
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/MSAAccountInfo;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Refreshing access token..."

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 110
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/MSAAccountInfo;->mAuthenticator:Lcom/onedrive/sdk/authentication/MSAAuthenticator;

    invoke-virtual {v0}, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->loginSilent()Lcom/onedrive/sdk/authentication/IAccountInfo;

    move-result-object v0

    check-cast v0, Lcom/onedrive/sdk/authentication/MSAAccountInfo;

    .line 111
    iget-object v0, v0, Lcom/onedrive/sdk/authentication/MSAAccountInfo;->mSession:Lcom/microsoft/services/msa/LiveConnectSession;

    iput-object v0, p0, Lcom/onedrive/sdk/authentication/MSAAccountInfo;->mSession:Lcom/microsoft/services/msa/LiveConnectSession;

    return-void
.end method

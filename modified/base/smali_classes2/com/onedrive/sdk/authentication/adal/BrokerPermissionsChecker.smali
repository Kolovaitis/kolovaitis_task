.class public Lcom/onedrive/sdk/authentication/adal/BrokerPermissionsChecker;
.super Ljava/lang/Object;
.source "BrokerPermissionsChecker.java"


# instance fields
.field private final mAdalProjectUrl:Ljava/lang/String;

.field private final mBrokerRequirePermissions:[Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field private final mLogger:Lcom/onedrive/sdk/logger/ILogger;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/onedrive/sdk/logger/ILogger;)V
    .locals 3

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "https://github.com/AzureAD/azure-activedirectory-library-for-android"

    .line 42
    iput-object v0, p0, Lcom/onedrive/sdk/authentication/adal/BrokerPermissionsChecker;->mAdalProjectUrl:Ljava/lang/String;

    const-string v0, "android.permission.GET_ACCOUNTS"

    const-string v1, "android.permission.MANAGE_ACCOUNTS"

    const-string v2, "android.permission.USE_CREDENTIALS"

    .line 48
    filled-new-array {v0, v1, v2}, [Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/onedrive/sdk/authentication/adal/BrokerPermissionsChecker;->mBrokerRequirePermissions:[Ljava/lang/String;

    .line 70
    iput-object p1, p0, Lcom/onedrive/sdk/authentication/adal/BrokerPermissionsChecker;->mContext:Landroid/content/Context;

    .line 71
    iput-object p2, p0, Lcom/onedrive/sdk/authentication/adal/BrokerPermissionsChecker;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    return-void
.end method


# virtual methods
.method public check()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/authentication/ClientAuthenticatorException;
        }
    .end annotation

    .line 80
    sget-object v0, Lcom/microsoft/aad/adal/AuthenticationSettings;->INSTANCE:Lcom/microsoft/aad/adal/AuthenticationSettings;

    invoke-virtual {v0}, Lcom/microsoft/aad/adal/AuthenticationSettings;->getSkipBroker()Z

    move-result v0

    if-nez v0, :cond_2

    .line 81
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/adal/BrokerPermissionsChecker;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Checking permissions for use with the ADAL Broker."

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 82
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/adal/BrokerPermissionsChecker;->mBrokerRequirePermissions:[Ljava/lang/String;

    array-length v1, v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_1

    aget-object v4, v0, v3

    .line 83
    iget-object v5, p0, Lcom/onedrive/sdk/authentication/adal/BrokerPermissionsChecker;->mContext:Landroid/content/Context;

    invoke-static {v5, v4}, Landroid/support/v4/content/ContextCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v5

    const/4 v6, -0x1

    if-eq v5, v6, :cond_0

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x2

    .line 84
    new-array v0, v0, [Ljava/lang/Object;

    aput-object v4, v0, v2

    const/4 v1, 0x1

    const-string v2, "https://github.com/AzureAD/azure-activedirectory-library-for-android"

    aput-object v2, v0, v1

    const-string v1, "Required permissions to use the Broker are denied: %s, see %s for more details."

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 88
    iget-object v1, p0, Lcom/onedrive/sdk/authentication/adal/BrokerPermissionsChecker;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    invoke-interface {v1, v0}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 89
    new-instance v1, Lcom/onedrive/sdk/authentication/ClientAuthenticatorException;

    sget-object v2, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->AuthenicationPermissionsDenied:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    invoke-direct {v1, v0, v2}, Lcom/onedrive/sdk/authentication/ClientAuthenticatorException;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/core/OneDriveErrorCodes;)V

    throw v1

    .line 92
    :cond_1
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/adal/BrokerPermissionsChecker;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "All required permissions found."

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    :cond_2
    return-void
.end method

.class Lcom/onedrive/sdk/authentication/DisambiguationResponse;
.super Ljava/lang/Object;
.source "DisambiguationResponse.java"


# instance fields
.field private final mAccount:Ljava/lang/String;

.field private final mAccountType:Lcom/onedrive/sdk/authentication/AccountType;


# direct methods
.method public constructor <init>(Lcom/onedrive/sdk/authentication/AccountType;Ljava/lang/String;)V
    .locals 0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/onedrive/sdk/authentication/DisambiguationResponse;->mAccountType:Lcom/onedrive/sdk/authentication/AccountType;

    .line 47
    iput-object p2, p0, Lcom/onedrive/sdk/authentication/DisambiguationResponse;->mAccount:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getAccount()Ljava/lang/String;
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationResponse;->mAccount:Ljava/lang/String;

    return-object v0
.end method

.method public getAccountType()Lcom/onedrive/sdk/authentication/AccountType;
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationResponse;->mAccountType:Lcom/onedrive/sdk/authentication/AccountType;

    return-object v0
.end method

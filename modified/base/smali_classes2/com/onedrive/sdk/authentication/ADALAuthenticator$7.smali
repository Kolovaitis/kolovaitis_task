.class Lcom/onedrive/sdk/authentication/ADALAuthenticator$7;
.super Ljava/lang/Object;
.source "ADALAuthenticator.java"

# interfaces
.implements Lcom/microsoft/aad/adal/AuthenticationCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/onedrive/sdk/authentication/ADALAuthenticator;->getOneDriveServiceAuthResult(Lcom/onedrive/sdk/authentication/ServiceInfo;)Lcom/microsoft/aad/adal/AuthenticationResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/microsoft/aad/adal/AuthenticationCallback<",
        "Lcom/microsoft/aad/adal/AuthenticationResult;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/onedrive/sdk/authentication/ADALAuthenticator;

.field final synthetic val$authorityCallbackWaiter:Lcom/onedrive/sdk/concurrency/SimpleWaiter;

.field final synthetic val$error:Ljava/util/concurrent/atomic/AtomicReference;

.field final synthetic val$oneDriveServiceAuthToken:Ljava/util/concurrent/atomic/AtomicReference;


# direct methods
.method constructor <init>(Lcom/onedrive/sdk/authentication/ADALAuthenticator;Ljava/util/concurrent/atomic/AtomicReference;Lcom/onedrive/sdk/concurrency/SimpleWaiter;Ljava/util/concurrent/atomic/AtomicReference;)V
    .locals 0

    .line 636
    iput-object p1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator$7;->this$0:Lcom/onedrive/sdk/authentication/ADALAuthenticator;

    iput-object p2, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator$7;->val$oneDriveServiceAuthToken:Ljava/util/concurrent/atomic/AtomicReference;

    iput-object p3, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator$7;->val$authorityCallbackWaiter:Lcom/onedrive/sdk/concurrency/SimpleWaiter;

    iput-object p4, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator$7;->val$error:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Ljava/lang/Exception;)V
    .locals 5

    const-string v0, "Error while retrieving the service specific auth token"

    .line 647
    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->AuthenticationFailure:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 648
    instance-of v2, p1, Lcom/microsoft/aad/adal/AuthenticationCancelError;

    if-eqz v2, :cond_0

    .line 649
    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->AuthenticationCancelled:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 651
    :cond_0
    instance-of v2, p1, Lcom/microsoft/aad/adal/AuthenticationException;

    if-eqz v2, :cond_1

    const-string v2, "%s; Code %s"

    const/4 v3, 0x2

    .line 652
    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    move-object v4, p1

    check-cast v4, Lcom/microsoft/aad/adal/AuthenticationException;

    invoke-virtual {v4}, Lcom/microsoft/aad/adal/AuthenticationException;->getCode()Lcom/microsoft/aad/adal/ADALError;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/aad/adal/ADALError;->getDescription()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 657
    :cond_1
    iget-object v2, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator$7;->val$error:Ljava/util/concurrent/atomic/AtomicReference;

    new-instance v3, Lcom/onedrive/sdk/authentication/ClientAuthenticatorException;

    invoke-direct {v3, v0, p1, v1}, Lcom/onedrive/sdk/authentication/ClientAuthenticatorException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lcom/onedrive/sdk/core/OneDriveErrorCodes;)V

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 660
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator$7;->this$0:Lcom/onedrive/sdk/authentication/ADALAuthenticator;

    invoke-static {p1}, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->access$100(Lcom/onedrive/sdk/authentication/ADALAuthenticator;)Lcom/onedrive/sdk/logger/ILogger;

    move-result-object p1

    const-string v0, "Unable to refresh token into OneDrive service access token"

    iget-object v1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator$7;->val$error:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Throwable;

    invoke-interface {p1, v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logError(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 661
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator$7;->val$authorityCallbackWaiter:Lcom/onedrive/sdk/concurrency/SimpleWaiter;

    invoke-virtual {p1}, Lcom/onedrive/sdk/concurrency/SimpleWaiter;->signal()V

    return-void
.end method

.method public onSuccess(Lcom/microsoft/aad/adal/AuthenticationResult;)V
    .locals 2

    .line 639
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator$7;->this$0:Lcom/onedrive/sdk/authentication/ADALAuthenticator;

    invoke-static {v0}, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->access$100(Lcom/onedrive/sdk/authentication/ADALAuthenticator;)Lcom/onedrive/sdk/logger/ILogger;

    move-result-object v0

    const-string v1, "Successful refreshed the OneDrive service authentication token"

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 640
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator$7;->val$oneDriveServiceAuthToken:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 641
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator$7;->val$authorityCallbackWaiter:Lcom/onedrive/sdk/concurrency/SimpleWaiter;

    invoke-virtual {p1}, Lcom/onedrive/sdk/concurrency/SimpleWaiter;->signal()V

    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .line 636
    check-cast p1, Lcom/microsoft/aad/adal/AuthenticationResult;

    invoke-virtual {p0, p1}, Lcom/onedrive/sdk/authentication/ADALAuthenticator$7;->onSuccess(Lcom/microsoft/aad/adal/AuthenticationResult;)V

    return-void
.end method

.class public Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;
.super Ljava/lang/Object;
.source "DisambiguationAuthenticator.java"

# interfaces
.implements Lcom/onedrive/sdk/authentication/IAuthenticator;


# static fields
.field public static final ACCOUNT_TYPE_KEY:Ljava/lang/String; = "accountType"

.field private static final DISAMBIGUATION_AUTHENTICATOR_PREFS:Ljava/lang/String; = "DisambiguationAuthenticatorPrefs"

.field public static final VERSION_CODE_KEY:Ljava/lang/String; = "versionCode"


# instance fields
.field private final mADALAuthenticator:Lcom/onedrive/sdk/authentication/ADALAuthenticator;

.field private final mAccountInfo:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference<",
            "Lcom/onedrive/sdk/authentication/IAccountInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mActivity:Landroid/app/Activity;

.field private mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

.field private mInitialized:Z

.field private mLogger:Lcom/onedrive/sdk/logger/ILogger;

.field private final mMSAAuthenticator:Lcom/onedrive/sdk/authentication/MSAAuthenticator;


# direct methods
.method public constructor <init>(Lcom/onedrive/sdk/authentication/MSAAuthenticator;Lcom/onedrive/sdk/authentication/ADALAuthenticator;)V
    .locals 1

    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mAccountInfo:Ljava/util/concurrent/atomic/AtomicReference;

    .line 105
    iput-object p1, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mMSAAuthenticator:Lcom/onedrive/sdk/authentication/MSAAuthenticator;

    .line 106
    iput-object p2, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mADALAuthenticator:Lcom/onedrive/sdk/authentication/ADALAuthenticator;

    return-void
.end method

.method static synthetic access$000(Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;)Lcom/onedrive/sdk/concurrency/IExecutors;
    .locals 0

    .line 45
    iget-object p0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

    return-object p0
.end method

.method static synthetic access$100(Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;)Lcom/onedrive/sdk/logger/ILogger;
    .locals 0

    .line 45
    iget-object p0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    return-object p0
.end method

.method private getAccountTypeInPreferences()Lcom/onedrive/sdk/authentication/AccountType;
    .locals 3
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .line 389
    invoke-direct {p0}, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "accountType"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    return-object v2

    .line 393
    :cond_0
    invoke-static {v0}, Lcom/onedrive/sdk/authentication/AccountType;->valueOf(Ljava/lang/String;)Lcom/onedrive/sdk/authentication/AccountType;

    move-result-object v0

    return-object v0
.end method

.method private getSharedPreferences()Landroid/content/SharedPreferences;
    .locals 3

    .line 364
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mActivity:Landroid/app/Activity;

    const-string v1, "DisambiguationAuthenticatorPrefs"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method private setAccountTypeInPreferences(Lcom/onedrive/sdk/authentication/AccountType;)V
    .locals 2
    .param p1    # Lcom/onedrive/sdk/authentication/AccountType;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    if-nez p1, :cond_0

    return-void

    .line 376
    :cond_0
    invoke-direct {p0}, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "accountType"

    invoke-virtual {p1}, Lcom/onedrive/sdk/authentication/AccountType;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    const-string v0, "versionCode"

    const/16 v1, 0x283d

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method


# virtual methods
.method public getAccountInfo()Lcom/onedrive/sdk/authentication/IAccountInfo;
    .locals 1

    .line 356
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mAccountInfo:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/onedrive/sdk/authentication/IAccountInfo;

    return-object v0
.end method

.method public declared-synchronized init(Lcom/onedrive/sdk/concurrency/IExecutors;Lcom/onedrive/sdk/http/IHttpProvider;Landroid/app/Activity;Lcom/onedrive/sdk/logger/ILogger;)V
    .locals 2

    monitor-enter p0

    .line 121
    :try_start_0
    iget-boolean v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mInitialized:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 122
    monitor-exit p0

    return-void

    .line 125
    :cond_0
    :try_start_1
    iput-object p1, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

    .line 126
    iput-object p3, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mActivity:Landroid/app/Activity;

    .line 127
    iput-object p4, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    .line 128
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Initializing MSA and ADAL authenticators"

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 129
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mMSAAuthenticator:Lcom/onedrive/sdk/authentication/MSAAuthenticator;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->init(Lcom/onedrive/sdk/concurrency/IExecutors;Lcom/onedrive/sdk/http/IHttpProvider;Landroid/app/Activity;Lcom/onedrive/sdk/logger/ILogger;)V

    .line 130
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mADALAuthenticator:Lcom/onedrive/sdk/authentication/ADALAuthenticator;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->init(Lcom/onedrive/sdk/concurrency/IExecutors;Lcom/onedrive/sdk/http/IHttpProvider;Landroid/app/Activity;Lcom/onedrive/sdk/logger/ILogger;)V

    const/4 p1, 0x1

    .line 131
    iput-boolean p1, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mInitialized:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 132
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized login(Ljava/lang/String;)Lcom/onedrive/sdk/authentication/IAccountInfo;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    monitor-enter p0

    .line 171
    :try_start_0
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v0, "Starting login"

    invoke-interface {p1, v0}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 172
    new-instance p1, Lcom/onedrive/sdk/concurrency/SimpleWaiter;

    invoke-direct {p1}, Lcom/onedrive/sdk/concurrency/SimpleWaiter;-><init>()V

    .line 173
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 174
    new-instance v1, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 175
    new-instance v2, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator$2;

    invoke-direct {v2, p0, v0, p1, v1}, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator$2;-><init>(Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;Ljava/util/concurrent/atomic/AtomicReference;Lcom/onedrive/sdk/concurrency/SimpleWaiter;Ljava/util/concurrent/atomic/AtomicReference;)V

    .line 195
    invoke-direct {p0}, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->getAccountTypeInPreferences()Lcom/onedrive/sdk/authentication/AccountType;

    move-result-object v3

    const/4 v4, 0x0

    if-eqz v3, :cond_0

    .line 198
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v0, "Found saved account information %s type of account"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    goto :goto_0

    .line 200
    :cond_0
    iget-object v3, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v4, "Creating disambiguation ui, waiting for user to sign in"

    invoke-interface {v3, v4}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 201
    new-instance v3, Lcom/onedrive/sdk/authentication/DisambiguationRequest;

    iget-object v4, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mActivity:Landroid/app/Activity;

    iget-object v5, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    invoke-direct {v3, v4, v2, v5}, Lcom/onedrive/sdk/authentication/DisambiguationRequest;-><init>(Landroid/app/Activity;Lcom/onedrive/sdk/concurrency/ICallback;Lcom/onedrive/sdk/logger/ILogger;)V

    invoke-virtual {v3}, Lcom/onedrive/sdk/authentication/DisambiguationRequest;->execute()V

    .line 202
    invoke-virtual {p1}, Lcom/onedrive/sdk/concurrency/SimpleWaiter;->waitForSignal()V

    .line 205
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_1

    .line 208
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/onedrive/sdk/authentication/DisambiguationResponse;

    .line 209
    invoke-virtual {p1}, Lcom/onedrive/sdk/authentication/DisambiguationResponse;->getAccountType()Lcom/onedrive/sdk/authentication/AccountType;

    move-result-object v3

    .line 210
    invoke-virtual {p1}, Lcom/onedrive/sdk/authentication/DisambiguationResponse;->getAccount()Ljava/lang/String;

    move-result-object v4

    .line 214
    :goto_0
    sget-object p1, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator$5;->$SwitchMap$com$onedrive$sdk$authentication$AccountType:[I

    invoke-virtual {v3}, Lcom/onedrive/sdk/authentication/AccountType;->ordinal()I

    move-result v0

    aget p1, p1, v0

    packed-switch p1, :pswitch_data_0

    .line 222
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    goto :goto_2

    .line 219
    :pswitch_0
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mMSAAuthenticator:Lcom/onedrive/sdk/authentication/MSAAuthenticator;

    invoke-virtual {p1, v4}, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->login(Ljava/lang/String;)Lcom/onedrive/sdk/authentication/IAccountInfo;

    move-result-object p1

    goto :goto_1

    .line 216
    :pswitch_1
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mADALAuthenticator:Lcom/onedrive/sdk/authentication/ADALAuthenticator;

    invoke-virtual {p1, v4}, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->login(Ljava/lang/String;)Lcom/onedrive/sdk/authentication/IAccountInfo;

    move-result-object p1

    .line 229
    :goto_1
    invoke-direct {p0, v3}, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->setAccountTypeInPreferences(Lcom/onedrive/sdk/authentication/AccountType;)V

    .line 230
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mAccountInfo:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 231
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mAccountInfo:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/onedrive/sdk/authentication/IAccountInfo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    .line 222
    :goto_2
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unrecognized account type "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    .line 225
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Unrecognized account type"

    invoke-interface {v0, v1, p1}, Lcom/onedrive/sdk/logger/ILogger;->logError(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 226
    throw p1

    .line 206
    :cond_1
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/onedrive/sdk/core/ClientException;

    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public login(Ljava/lang/String;Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/authentication/IAccountInfo;",
            ">;)V"
        }
    .end annotation

    .line 141
    iget-boolean v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mInitialized:Z

    if-eqz v0, :cond_1

    if-eqz p2, :cond_0

    .line 149
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Starting login async"

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 151
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

    new-instance v1, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator$1;-><init>(Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;Ljava/lang/String;Lcom/onedrive/sdk/concurrency/ICallback;)V

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/concurrency/IExecutors;->performOnBackground(Ljava/lang/Runnable;)V

    return-void

    .line 146
    :cond_0
    new-instance p1, Ljava/security/InvalidParameterException;

    const-string p2, "loginCallback"

    invoke-direct {p1, p2}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 142
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "init must be called"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public declared-synchronized loginSilent()Lcom/onedrive/sdk/authentication/IAccountInfo;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    monitor-enter p0

    .line 268
    :try_start_0
    iget-boolean v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mInitialized:Z

    if-eqz v0, :cond_3

    .line 272
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Starting login silent"

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 274
    invoke-direct {p0}, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->getAccountTypeInPreferences()Lcom/onedrive/sdk/authentication/AccountType;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 276
    iget-object v1, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v2, "Expecting %s type of account"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 279
    :cond_0
    iget-object v1, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v2, "Checking MSA"

    invoke-interface {v1, v2}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 280
    iget-object v1, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mMSAAuthenticator:Lcom/onedrive/sdk/authentication/MSAAuthenticator;

    invoke-virtual {v1}, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->loginSilent()Lcom/onedrive/sdk/authentication/IAccountInfo;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 282
    iget-object v2, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v3, "Found account info in MSA"

    invoke-interface {v2, v3}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 283
    invoke-direct {p0, v0}, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->setAccountTypeInPreferences(Lcom/onedrive/sdk/authentication/AccountType;)V

    .line 284
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mAccountInfo:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 285
    monitor-exit p0

    return-object v1

    .line 288
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v2, "Checking ADAL"

    invoke-interface {v1, v2}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 289
    iget-object v1, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mADALAuthenticator:Lcom/onedrive/sdk/authentication/ADALAuthenticator;

    invoke-virtual {v1}, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->loginSilent()Lcom/onedrive/sdk/authentication/IAccountInfo;

    move-result-object v1

    .line 290
    iget-object v2, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mAccountInfo:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    if-eqz v1, :cond_2

    .line 292
    iget-object v1, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v2, "Found account info in ADAL"

    invoke-interface {v1, v2}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 293
    invoke-direct {p0, v0}, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->setAccountTypeInPreferences(Lcom/onedrive/sdk/authentication/AccountType;)V

    .line 295
    :cond_2
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mAccountInfo:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/onedrive/sdk/authentication/IAccountInfo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0

    .line 269
    :cond_3
    :try_start_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "init must be called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public loginSilent(Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/authentication/IAccountInfo;",
            ">;)V"
        }
    .end annotation

    .line 240
    iget-boolean v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mInitialized:Z

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    .line 248
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Starting login silent async"

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 249
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

    new-instance v1, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator$3;

    invoke-direct {v1, p0, p1}, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator$3;-><init>(Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;Lcom/onedrive/sdk/concurrency/ICallback;)V

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/concurrency/IExecutors;->performOnBackground(Ljava/lang/Runnable;)V

    return-void

    .line 245
    :cond_0
    new-instance p1, Ljava/security/InvalidParameterException;

    const-string v0, "loginCallback"

    invoke-direct {p1, v0}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 241
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "init must be called"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public declared-synchronized logout()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    monitor-enter p0

    .line 328
    :try_start_0
    iget-boolean v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mInitialized:Z

    if-eqz v0, :cond_2

    .line 332
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Starting logout"

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 333
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mMSAAuthenticator:Lcom/onedrive/sdk/authentication/MSAAuthenticator;

    invoke-virtual {v0}, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->getAccountInfo()Lcom/onedrive/sdk/authentication/IAccountInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 334
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Starting logout of MSA account"

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 335
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mMSAAuthenticator:Lcom/onedrive/sdk/authentication/MSAAuthenticator;

    invoke-virtual {v0}, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->logout()V

    .line 338
    :cond_0
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mADALAuthenticator:Lcom/onedrive/sdk/authentication/ADALAuthenticator;

    invoke-virtual {v0}, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->getAccountInfo()Lcom/onedrive/sdk/authentication/IAccountInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 339
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Starting logout of ADAL account"

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 340
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mADALAuthenticator:Lcom/onedrive/sdk/authentication/ADALAuthenticator;

    invoke-virtual {v0}, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->logout()V

    .line 343
    :cond_1
    invoke-direct {p0}, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "versionCode"

    const/16 v2, 0x283d

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 348
    monitor-exit p0

    return-void

    .line 329
    :cond_2
    :try_start_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "init must be called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public logout(Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .line 304
    iget-boolean v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mInitialized:Z

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    .line 312
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Starting logout async"

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 313
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

    new-instance v1, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator$4;

    invoke-direct {v1, p0, p1}, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator$4;-><init>(Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;Lcom/onedrive/sdk/concurrency/ICallback;)V

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/concurrency/IExecutors;->performOnBackground(Ljava/lang/Runnable;)V

    return-void

    .line 309
    :cond_0
    new-instance p1, Ljava/security/InvalidParameterException;

    const-string v0, "logoutCallback"

    invoke-direct {p1, v0}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 305
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "init must be called"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

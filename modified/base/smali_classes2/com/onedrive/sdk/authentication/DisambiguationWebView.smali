.class Lcom/onedrive/sdk/authentication/DisambiguationWebView;
.super Landroid/webkit/WebViewClient;
.source "DisambiguationWebView.java"


# instance fields
.field private final mCallback:Lcom/onedrive/sdk/concurrency/ICallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/authentication/DisambiguationResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final mDisambiguationDialog:Lcom/onedrive/sdk/authentication/DisambiguationDialog;


# direct methods
.method public constructor <init>(Lcom/onedrive/sdk/authentication/DisambiguationDialog;Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/authentication/DisambiguationDialog;",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/authentication/DisambiguationResponse;",
            ">;)V"
        }
    .end annotation

    .line 56
    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    .line 57
    iput-object p1, p0, Lcom/onedrive/sdk/authentication/DisambiguationWebView;->mDisambiguationDialog:Lcom/onedrive/sdk/authentication/DisambiguationDialog;

    .line 58
    iput-object p2, p0, Lcom/onedrive/sdk/authentication/DisambiguationWebView;->mCallback:Lcom/onedrive/sdk/concurrency/ICallback;

    return-void
.end method


# virtual methods
.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 3

    .line 63
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationWebView;->mDisambiguationDialog:Lcom/onedrive/sdk/authentication/DisambiguationDialog;

    invoke-virtual {v0}, Lcom/onedrive/sdk/authentication/DisambiguationDialog;->getLogger()Lcom/onedrive/sdk/logger/ILogger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPageStarted for url \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 64
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 65
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p2

    .line 66
    invoke-virtual {p2}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object p3

    const-string v0, "localhost:777"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p3

    if-eqz p3, :cond_0

    .line 67
    iget-object p3, p0, Lcom/onedrive/sdk/authentication/DisambiguationWebView;->mDisambiguationDialog:Lcom/onedrive/sdk/authentication/DisambiguationDialog;

    invoke-virtual {p3}, Lcom/onedrive/sdk/authentication/DisambiguationDialog;->getLogger()Lcom/onedrive/sdk/logger/ILogger;

    move-result-object p3

    const-string v0, "Found callback from disambiguation service"

    invoke-interface {p3, v0}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    const-string p3, "account_type"

    .line 68
    invoke-virtual {p2, p3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    invoke-static {p3}, Lcom/onedrive/sdk/authentication/AccountType;->fromRepresentation(Ljava/lang/String;)Lcom/onedrive/sdk/authentication/AccountType;

    move-result-object p3

    const-string v0, "user_email"

    .line 69
    invoke-virtual {p2, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 70
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationWebView;->mCallback:Lcom/onedrive/sdk/concurrency/ICallback;

    new-instance v1, Lcom/onedrive/sdk/authentication/DisambiguationResponse;

    invoke-direct {v1, p3, p2}, Lcom/onedrive/sdk/authentication/DisambiguationResponse;-><init>(Lcom/onedrive/sdk/authentication/AccountType;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/concurrency/ICallback;->success(Ljava/lang/Object;)V

    .line 71
    invoke-virtual {p1}, Landroid/webkit/WebView;->stopLoading()V

    .line 72
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/DisambiguationWebView;->mDisambiguationDialog:Lcom/onedrive/sdk/authentication/DisambiguationDialog;

    invoke-virtual {p1}, Lcom/onedrive/sdk/authentication/DisambiguationDialog;->dismiss()V

    :cond_0
    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 3

    .line 82
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 84
    sget-object p1, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    const-string v0, "Url %s, Error code: %d, Description %s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p4, v1, v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    const/4 p4, 0x1

    aput-object p2, v1, p4

    const/4 p2, 0x2

    aput-object p3, v1, p2

    invoke-static {p1, v0, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 89
    iget-object p2, p0, Lcom/onedrive/sdk/authentication/DisambiguationWebView;->mCallback:Lcom/onedrive/sdk/concurrency/ICallback;

    new-instance p3, Lcom/onedrive/sdk/authentication/ClientAuthenticatorException;

    sget-object p4, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->AuthenticationFailure:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    invoke-direct {p3, p1, p4}, Lcom/onedrive/sdk/authentication/ClientAuthenticatorException;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/core/OneDriveErrorCodes;)V

    invoke-interface {p2, p3}, Lcom/onedrive/sdk/concurrency/ICallback;->failure(Lcom/onedrive/sdk/core/ClientException;)V

    .line 90
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/DisambiguationWebView;->mDisambiguationDialog:Lcom/onedrive/sdk/authentication/DisambiguationDialog;

    invoke-virtual {p1}, Lcom/onedrive/sdk/authentication/DisambiguationDialog;->dismiss()V

    return-void
.end method

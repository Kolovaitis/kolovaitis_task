.class Lcom/onedrive/sdk/authentication/MSAAuthenticator$3;
.super Ljava/lang/Object;
.source "MSAAuthenticator.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/onedrive/sdk/authentication/MSAAuthenticator;->login(Ljava/lang/String;)Lcom/onedrive/sdk/authentication/IAccountInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/onedrive/sdk/authentication/MSAAuthenticator;

.field final synthetic val$emailAddressHint:Ljava/lang/String;

.field final synthetic val$listener:Lcom/microsoft/services/msa/LiveAuthListener;


# direct methods
.method constructor <init>(Lcom/onedrive/sdk/authentication/MSAAuthenticator;Ljava/lang/String;Lcom/microsoft/services/msa/LiveAuthListener;)V
    .locals 0

    .line 222
    iput-object p1, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator$3;->this$0:Lcom/onedrive/sdk/authentication/MSAAuthenticator;

    iput-object p2, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator$3;->val$emailAddressHint:Ljava/lang/String;

    iput-object p3, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator$3;->val$listener:Lcom/microsoft/services/msa/LiveAuthListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .line 225
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator$3;->this$0:Lcom/onedrive/sdk/authentication/MSAAuthenticator;

    invoke-static {v0}, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->access$300(Lcom/onedrive/sdk/authentication/MSAAuthenticator;)Lcom/microsoft/services/msa/LiveAuthClient;

    move-result-object v1

    iget-object v0, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator$3;->this$0:Lcom/onedrive/sdk/authentication/MSAAuthenticator;

    invoke-static {v0}, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->access$200(Lcom/onedrive/sdk/authentication/MSAAuthenticator;)Landroid/app/Activity;

    move-result-object v2

    iget-object v5, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator$3;->val$emailAddressHint:Ljava/lang/String;

    iget-object v6, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator$3;->val$listener:Lcom/microsoft/services/msa/LiveAuthListener;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual/range {v1 .. v6}, Lcom/microsoft/services/msa/LiveAuthClient;->login(Landroid/app/Activity;Ljava/lang/Iterable;Ljava/lang/Object;Ljava/lang/String;Lcom/microsoft/services/msa/LiveAuthListener;)V

    return-void
.end method

.class Lcom/onedrive/sdk/authentication/MicrosoftOAuthConfig;
.super Ljava/lang/Object;
.source "MicrosoftOAuthConfig.java"

# interfaces
.implements Lcom/microsoft/services/msa/OAuthConfig;


# static fields
.field public static final HTTPS_LOGIN_LIVE_COM:Ljava/lang/String; = "https://login.microsoftonline.com/common/oauth2/"


# instance fields
.field private final mOAuthAuthorizeUri:Landroid/net/Uri;

.field private final mOAuthDesktopUri:Landroid/net/Uri;

.field private final mOAuthLogoutUri:Landroid/net/Uri;

.field private final mOAuthTokenUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "https://login.microsoftonline.com/common/oauth2/authorize"

    .line 63
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/onedrive/sdk/authentication/MicrosoftOAuthConfig;->mOAuthAuthorizeUri:Landroid/net/Uri;

    const-string v0, "https://login.microsoftonline.com/common/oauth2/desktop"

    .line 64
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/onedrive/sdk/authentication/MicrosoftOAuthConfig;->mOAuthDesktopUri:Landroid/net/Uri;

    const-string v0, "https://login.microsoftonline.com/common/oauth2/logout"

    .line 65
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/onedrive/sdk/authentication/MicrosoftOAuthConfig;->mOAuthLogoutUri:Landroid/net/Uri;

    const-string v0, "https://login.microsoftonline.com/common/oauth2/token"

    .line 66
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/onedrive/sdk/authentication/MicrosoftOAuthConfig;->mOAuthTokenUri:Landroid/net/Uri;

    return-void
.end method


# virtual methods
.method public getAuthorizeUri()Landroid/net/Uri;
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/MicrosoftOAuthConfig;->mOAuthAuthorizeUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getDesktopUri()Landroid/net/Uri;
    .locals 1

    .line 76
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/MicrosoftOAuthConfig;->mOAuthDesktopUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getLogoutUri()Landroid/net/Uri;
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/MicrosoftOAuthConfig;->mOAuthLogoutUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getTokenUri()Landroid/net/Uri;
    .locals 1

    .line 86
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/MicrosoftOAuthConfig;->mOAuthTokenUri:Landroid/net/Uri;

    return-object v0
.end method

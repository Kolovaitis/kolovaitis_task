.class public abstract Lcom/onedrive/sdk/authentication/MSAAuthenticator;
.super Ljava/lang/Object;
.source "MSAAuthenticator.java"

# interfaces
.implements Lcom/onedrive/sdk/authentication/IAuthenticator;


# static fields
.field private static final DEFAULT_USER_ID:Ljava/lang/String; = "@@defaultUser"

.field private static final MSA_AUTHENTICATOR_PREFS:Ljava/lang/String; = "MSAAuthenticatorPrefs"

.field private static final SIGN_IN_CANCELLED_MESSAGE:Ljava/lang/String; = "The user cancelled the login operation."

.field private static final USER_ID_KEY:Ljava/lang/String; = "userId"

.field public static final VERSION_CODE_KEY:Ljava/lang/String; = "versionCode"


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mAuthClient:Lcom/microsoft/services/msa/LiveAuthClient;

.field private mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

.field private mInitialized:Z

.field private mLogger:Lcom/onedrive/sdk/logger/ILogger;

.field private final mUserId:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->mUserId:Ljava/util/concurrent/atomic/AtomicReference;

    return-void
.end method

.method static synthetic access$000(Lcom/onedrive/sdk/authentication/MSAAuthenticator;)Lcom/onedrive/sdk/concurrency/IExecutors;
    .locals 0

    .line 52
    iget-object p0, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

    return-object p0
.end method

.method static synthetic access$100(Lcom/onedrive/sdk/authentication/MSAAuthenticator;)Lcom/onedrive/sdk/logger/ILogger;
    .locals 0

    .line 52
    iget-object p0, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    return-object p0
.end method

.method static synthetic access$200(Lcom/onedrive/sdk/authentication/MSAAuthenticator;)Landroid/app/Activity;
    .locals 0

    .line 52
    iget-object p0, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->mActivity:Landroid/app/Activity;

    return-object p0
.end method

.method static synthetic access$300(Lcom/onedrive/sdk/authentication/MSAAuthenticator;)Lcom/microsoft/services/msa/LiveAuthClient;
    .locals 0

    .line 52
    iget-object p0, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->mAuthClient:Lcom/microsoft/services/msa/LiveAuthClient;

    return-object p0
.end method

.method private getSharedPreferences()Landroid/content/SharedPreferences;
    .locals 3

    .line 448
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->mActivity:Landroid/app/Activity;

    const-string v1, "MSAAuthenticatorPrefs"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAccountInfo()Lcom/onedrive/sdk/authentication/IAccountInfo;
    .locals 3

    .line 435
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->mAuthClient:Lcom/microsoft/services/msa/LiveAuthClient;

    invoke-virtual {v0}, Lcom/microsoft/services/msa/LiveAuthClient;->getSession()Lcom/microsoft/services/msa/LiveConnectSession;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 440
    :cond_0
    new-instance v1, Lcom/onedrive/sdk/authentication/MSAAccountInfo;

    iget-object v2, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    invoke-direct {v1, p0, v0, v2}, Lcom/onedrive/sdk/authentication/MSAAccountInfo;-><init>(Lcom/onedrive/sdk/authentication/MSAAuthenticator;Lcom/microsoft/services/msa/LiveConnectSession;Lcom/onedrive/sdk/logger/ILogger;)V

    return-object v1
.end method

.method public abstract getClientId()Ljava/lang/String;
.end method

.method public abstract getScopes()[Ljava/lang/String;
.end method

.method public declared-synchronized init(Lcom/onedrive/sdk/concurrency/IExecutors;Lcom/onedrive/sdk/http/IHttpProvider;Landroid/app/Activity;Lcom/onedrive/sdk/logger/ILogger;)V
    .locals 0

    monitor-enter p0

    .line 135
    :try_start_0
    iget-boolean p2, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->mInitialized:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p2, :cond_0

    .line 136
    monitor-exit p0

    return-void

    .line 139
    :cond_0
    :try_start_1
    iput-object p1, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

    .line 140
    iput-object p3, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->mActivity:Landroid/app/Activity;

    .line 141
    iput-object p4, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const/4 p1, 0x1

    .line 142
    iput-boolean p1, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->mInitialized:Z

    .line 143
    new-instance p1, Lcom/microsoft/services/msa/LiveAuthClient;

    invoke-virtual {p0}, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->getClientId()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0}, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->getScopes()[Ljava/lang/String;

    move-result-object p4

    invoke-static {p4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p4

    invoke-direct {p1, p3, p2, p4}, Lcom/microsoft/services/msa/LiveAuthClient;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Iterable;)V

    iput-object p1, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->mAuthClient:Lcom/microsoft/services/msa/LiveAuthClient;

    .line 145
    invoke-direct {p0}, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object p1

    .line 146
    iget-object p2, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->mUserId:Ljava/util/concurrent/atomic/AtomicReference;

    const-string p3, "userId"

    const/4 p4, 0x0

    invoke-interface {p1, p3, p4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 147
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized login(Ljava/lang/String;)Lcom/onedrive/sdk/authentication/IAccountInfo;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    monitor-enter p0

    .line 186
    :try_start_0
    iget-boolean v0, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->mInitialized:Z

    if-eqz v0, :cond_2

    .line 190
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Starting login"

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 192
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 193
    new-instance v1, Lcom/onedrive/sdk/concurrency/SimpleWaiter;

    invoke-direct {v1}, Lcom/onedrive/sdk/concurrency/SimpleWaiter;-><init>()V

    .line 195
    new-instance v2, Lcom/onedrive/sdk/authentication/MSAAuthenticator$2;

    invoke-direct {v2, p0, v1, v0}, Lcom/onedrive/sdk/authentication/MSAAuthenticator$2;-><init>(Lcom/onedrive/sdk/authentication/MSAAuthenticator;Lcom/onedrive/sdk/concurrency/SimpleWaiter;Ljava/util/concurrent/atomic/AtomicReference;)V

    .line 222
    iget-object v3, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->mActivity:Landroid/app/Activity;

    new-instance v4, Lcom/onedrive/sdk/authentication/MSAAuthenticator$3;

    invoke-direct {v4, p0, p1, v2}, Lcom/onedrive/sdk/authentication/MSAAuthenticator$3;-><init>(Lcom/onedrive/sdk/authentication/MSAAuthenticator;Ljava/lang/String;Lcom/microsoft/services/msa/LiveAuthListener;)V

    invoke-virtual {v3, v4}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 229
    iget-object v2, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v3, "Waiting for MSA callback"

    invoke-interface {v2, v3}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 230
    invoke-virtual {v1}, Lcom/onedrive/sdk/concurrency/SimpleWaiter;->waitForSignal()V

    .line 232
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/onedrive/sdk/core/ClientException;

    if-nez v0, :cond_1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const-string p1, "@@defaultUser"

    .line 244
    :goto_0
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->mUserId:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 246
    invoke-direct {p0}, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object p1

    .line 247
    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    const-string v0, "userId"

    iget-object v1, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->mUserId:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    const-string v0, "versionCode"

    const/16 v1, 0x283d

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 252
    invoke-virtual {p0}, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->getAccountInfo()Lcom/onedrive/sdk/authentication/IAccountInfo;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    .line 234
    :cond_1
    :try_start_1
    throw v0

    .line 187
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "init must be called"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public login(Ljava/lang/String;Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/authentication/IAccountInfo;",
            ">;)V"
        }
    .end annotation

    .line 156
    iget-boolean v0, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->mInitialized:Z

    if-eqz v0, :cond_1

    if-eqz p2, :cond_0

    .line 164
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Starting login async"

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 166
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

    new-instance v1, Lcom/onedrive/sdk/authentication/MSAAuthenticator$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/onedrive/sdk/authentication/MSAAuthenticator$1;-><init>(Lcom/onedrive/sdk/authentication/MSAAuthenticator;Ljava/lang/String;Lcom/onedrive/sdk/concurrency/ICallback;)V

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/concurrency/IExecutors;->performOnBackground(Ljava/lang/Runnable;)V

    return-void

    .line 161
    :cond_0
    new-instance p1, Ljava/security/InvalidParameterException;

    const-string p2, "loginCallback"

    invoke-direct {p1, p2}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 157
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "init must be called"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public declared-synchronized loginSilent()Lcom/onedrive/sdk/authentication/IAccountInfo;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    monitor-enter p0

    .line 290
    :try_start_0
    iget-boolean v0, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->mInitialized:Z

    if-eqz v0, :cond_3

    .line 294
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Starting login silent"

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 297
    invoke-direct {p0}, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "versionCode"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    const/16 v1, 0x2780

    const/4 v2, 0x0

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->mUserId:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 299
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "No login information found for silent authentication"

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 300
    monitor-exit p0

    return-object v2

    .line 303
    :cond_0
    :try_start_1
    new-instance v0, Lcom/onedrive/sdk/concurrency/SimpleWaiter;

    invoke-direct {v0}, Lcom/onedrive/sdk/concurrency/SimpleWaiter;-><init>()V

    .line 304
    new-instance v1, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 306
    iget-object v3, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->mAuthClient:Lcom/microsoft/services/msa/LiveAuthClient;

    new-instance v4, Lcom/onedrive/sdk/authentication/MSAAuthenticator$5;

    invoke-direct {v4, p0, v1, v0}, Lcom/onedrive/sdk/authentication/MSAAuthenticator$5;-><init>(Lcom/onedrive/sdk/authentication/MSAAuthenticator;Ljava/util/concurrent/atomic/AtomicReference;Lcom/onedrive/sdk/concurrency/SimpleWaiter;)V

    invoke-virtual {v3, v4}, Lcom/microsoft/services/msa/LiveAuthClient;->loginSilent(Lcom/microsoft/services/msa/LiveAuthListener;)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_1

    .line 336
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "MSA silent auth fast-failed"

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 337
    monitor-exit p0

    return-object v2

    .line 340
    :cond_1
    :try_start_2
    iget-object v2, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v3, "Waiting for MSA callback"

    invoke-interface {v2, v3}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 341
    invoke-virtual {v0}, Lcom/onedrive/sdk/concurrency/SimpleWaiter;->waitForSignal()V

    .line 342
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/onedrive/sdk/core/ClientException;

    if-nez v0, :cond_2

    .line 347
    invoke-virtual {p0}, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->getAccountInfo()Lcom/onedrive/sdk/authentication/IAccountInfo;

    move-result-object v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object v0

    .line 344
    :cond_2
    :try_start_3
    throw v0

    .line 291
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "init must be called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public loginSilent(Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/authentication/IAccountInfo;",
            ">;)V"
        }
    .end annotation

    .line 261
    iget-boolean v0, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->mInitialized:Z

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    .line 269
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Starting login silent async"

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 271
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

    new-instance v1, Lcom/onedrive/sdk/authentication/MSAAuthenticator$4;

    invoke-direct {v1, p0, p1}, Lcom/onedrive/sdk/authentication/MSAAuthenticator$4;-><init>(Lcom/onedrive/sdk/authentication/MSAAuthenticator;Lcom/onedrive/sdk/concurrency/ICallback;)V

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/concurrency/IExecutors;->performOnBackground(Ljava/lang/Runnable;)V

    return-void

    .line 266
    :cond_0
    new-instance p1, Ljava/security/InvalidParameterException;

    const-string v0, "loginCallback"

    invoke-direct {p1, v0}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 262
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "init must be called"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public declared-synchronized logout()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    monitor-enter p0

    .line 385
    :try_start_0
    iget-boolean v0, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->mInitialized:Z

    if-eqz v0, :cond_1

    .line 389
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Starting logout"

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 391
    new-instance v0, Lcom/onedrive/sdk/concurrency/SimpleWaiter;

    invoke-direct {v0}, Lcom/onedrive/sdk/concurrency/SimpleWaiter;-><init>()V

    .line 392
    new-instance v1, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 393
    iget-object v2, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->mAuthClient:Lcom/microsoft/services/msa/LiveAuthClient;

    new-instance v3, Lcom/onedrive/sdk/authentication/MSAAuthenticator$7;

    invoke-direct {v3, p0, v0, v1}, Lcom/onedrive/sdk/authentication/MSAAuthenticator$7;-><init>(Lcom/onedrive/sdk/authentication/MSAAuthenticator;Lcom/onedrive/sdk/concurrency/SimpleWaiter;Ljava/util/concurrent/atomic/AtomicReference;)V

    invoke-virtual {v2, v3}, Lcom/microsoft/services/msa/LiveAuthClient;->logout(Lcom/microsoft/services/msa/LiveAuthListener;)V

    .line 412
    iget-object v2, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v3, "Waiting for logout to complete"

    invoke-interface {v2, v3}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 413
    invoke-virtual {v0}, Lcom/onedrive/sdk/concurrency/SimpleWaiter;->waitForSignal()V

    .line 415
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v2, "Clearing all MSA Authenticator shared preferences"

    invoke-interface {v0, v2}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 416
    invoke-direct {p0}, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 417
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "versionCode"

    const/16 v3, 0x283d

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 421
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->mUserId:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 423
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/onedrive/sdk/core/ClientException;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 427
    monitor-exit p0

    return-void

    .line 425
    :cond_0
    :try_start_1
    throw v0

    .line 386
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "init must be called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public logout(Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .line 356
    iget-boolean v0, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->mInitialized:Z

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    .line 364
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Starting logout async"

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 366
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

    new-instance v1, Lcom/onedrive/sdk/authentication/MSAAuthenticator$6;

    invoke-direct {v1, p0, p1}, Lcom/onedrive/sdk/authentication/MSAAuthenticator$6;-><init>(Lcom/onedrive/sdk/authentication/MSAAuthenticator;Lcom/onedrive/sdk/concurrency/ICallback;)V

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/concurrency/IExecutors;->performOnBackground(Ljava/lang/Runnable;)V

    return-void

    .line 361
    :cond_0
    new-instance p1, Ljava/security/InvalidParameterException;

    const-string v0, "logoutCallback"

    invoke-direct {p1, v0}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 357
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "init must be called"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

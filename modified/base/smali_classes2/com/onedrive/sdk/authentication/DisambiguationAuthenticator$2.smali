.class Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator$2;
.super Ljava/lang/Object;
.source "DisambiguationAuthenticator.java"

# interfaces
.implements Lcom/onedrive/sdk/concurrency/ICallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->login(Ljava/lang/String;)Lcom/onedrive/sdk/authentication/IAccountInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/onedrive/sdk/concurrency/ICallback<",
        "Lcom/onedrive/sdk/authentication/DisambiguationResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;

.field final synthetic val$disambiguationWaiter:Lcom/onedrive/sdk/concurrency/SimpleWaiter;

.field final synthetic val$error:Ljava/util/concurrent/atomic/AtomicReference;

.field final synthetic val$response:Ljava/util/concurrent/atomic/AtomicReference;


# direct methods
.method constructor <init>(Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;Ljava/util/concurrent/atomic/AtomicReference;Lcom/onedrive/sdk/concurrency/SimpleWaiter;Ljava/util/concurrent/atomic/AtomicReference;)V
    .locals 0

    .line 175
    iput-object p1, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator$2;->this$0:Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;

    iput-object p2, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator$2;->val$response:Ljava/util/concurrent/atomic/AtomicReference;

    iput-object p3, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator$2;->val$disambiguationWaiter:Lcom/onedrive/sdk/concurrency/SimpleWaiter;

    iput-object p4, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator$2;->val$error:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public failure(Lcom/onedrive/sdk/core/ClientException;)V
    .locals 3

    .line 187
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator$2;->val$error:Ljava/util/concurrent/atomic/AtomicReference;

    new-instance v0, Lcom/onedrive/sdk/authentication/ClientAuthenticatorException;

    const-string v1, "Unable to disambiguate account type"

    sget-object v2, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->AuthenticationFailure:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    invoke-direct {v0, v1, v2}, Lcom/onedrive/sdk/authentication/ClientAuthenticatorException;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/core/OneDriveErrorCodes;)V

    invoke-virtual {p1, v0}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 190
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator$2;->this$0:Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;

    invoke-static {p1}, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->access$100(Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;)Lcom/onedrive/sdk/logger/ILogger;

    move-result-object p1

    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator$2;->val$error:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/onedrive/sdk/core/ClientException;

    invoke-virtual {v0}, Lcom/onedrive/sdk/core/ClientException;->getMessage()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator$2;->val$error:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Throwable;

    invoke-interface {p1, v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logError(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 191
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator$2;->val$disambiguationWaiter:Lcom/onedrive/sdk/concurrency/SimpleWaiter;

    invoke-virtual {p1}, Lcom/onedrive/sdk/concurrency/SimpleWaiter;->signal()V

    return-void
.end method

.method public success(Lcom/onedrive/sdk/authentication/DisambiguationResponse;)V
    .locals 5

    .line 178
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator$2;->this$0:Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;

    invoke-static {v0}, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->access$100(Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;)Lcom/onedrive/sdk/logger/ILogger;

    move-result-object v0

    const-string v1, "Successfully disambiguated \'%s\' as account type \'%s\'"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/onedrive/sdk/authentication/DisambiguationResponse;->getAccount()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-virtual {p1}, Lcom/onedrive/sdk/authentication/DisambiguationResponse;->getAccountType()Lcom/onedrive/sdk/authentication/AccountType;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 181
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator$2;->val$response:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 182
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator$2;->val$disambiguationWaiter:Lcom/onedrive/sdk/concurrency/SimpleWaiter;

    invoke-virtual {p1}, Lcom/onedrive/sdk/concurrency/SimpleWaiter;->signal()V

    return-void
.end method

.method public bridge synthetic success(Ljava/lang/Object;)V
    .locals 0

    .line 175
    check-cast p1, Lcom/onedrive/sdk/authentication/DisambiguationResponse;

    invoke-virtual {p0, p1}, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator$2;->success(Lcom/onedrive/sdk/authentication/DisambiguationResponse;)V

    return-void
.end method

.class Lcom/onedrive/sdk/authentication/MSAAuthenticator$5;
.super Ljava/lang/Object;
.source "MSAAuthenticator.java"

# interfaces
.implements Lcom/microsoft/services/msa/LiveAuthListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/onedrive/sdk/authentication/MSAAuthenticator;->loginSilent()Lcom/onedrive/sdk/authentication/IAccountInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/onedrive/sdk/authentication/MSAAuthenticator;

.field final synthetic val$error:Ljava/util/concurrent/atomic/AtomicReference;

.field final synthetic val$loginSilentWaiter:Lcom/onedrive/sdk/concurrency/SimpleWaiter;


# direct methods
.method constructor <init>(Lcom/onedrive/sdk/authentication/MSAAuthenticator;Ljava/util/concurrent/atomic/AtomicReference;Lcom/onedrive/sdk/concurrency/SimpleWaiter;)V
    .locals 0

    .line 306
    iput-object p1, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator$5;->this$0:Lcom/onedrive/sdk/authentication/MSAAuthenticator;

    iput-object p2, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator$5;->val$error:Ljava/util/concurrent/atomic/AtomicReference;

    iput-object p3, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator$5;->val$loginSilentWaiter:Lcom/onedrive/sdk/concurrency/SimpleWaiter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAuthComplete(Lcom/microsoft/services/msa/LiveStatus;Lcom/microsoft/services/msa/LiveConnectSession;Ljava/lang/Object;)V
    .locals 1

    .line 311
    sget-object p2, Lcom/microsoft/services/msa/LiveStatus;->NOT_CONNECTED:Lcom/microsoft/services/msa/LiveStatus;

    if-ne p1, p2, :cond_0

    .line 312
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator$5;->val$error:Ljava/util/concurrent/atomic/AtomicReference;

    new-instance p2, Lcom/onedrive/sdk/authentication/ClientAuthenticatorException;

    const-string p3, "Failed silent login, interactive login required"

    sget-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->AuthenticationFailure:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    invoke-direct {p2, p3, v0}, Lcom/onedrive/sdk/authentication/ClientAuthenticatorException;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/core/OneDriveErrorCodes;)V

    invoke-virtual {p1, p2}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 314
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator$5;->this$0:Lcom/onedrive/sdk/authentication/MSAAuthenticator;

    invoke-static {p1}, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->access$100(Lcom/onedrive/sdk/authentication/MSAAuthenticator;)Lcom/onedrive/sdk/logger/ILogger;

    move-result-object p1

    iget-object p2, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator$5;->val$error:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {p2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/onedrive/sdk/core/ClientException;

    invoke-virtual {p2}, Lcom/onedrive/sdk/core/ClientException;->getMessage()Ljava/lang/String;

    move-result-object p2

    iget-object p3, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator$5;->val$error:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/lang/Throwable;

    invoke-interface {p1, p2, p3}, Lcom/onedrive/sdk/logger/ILogger;->logError(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 316
    :cond_0
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator$5;->this$0:Lcom/onedrive/sdk/authentication/MSAAuthenticator;

    invoke-static {p1}, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->access$100(Lcom/onedrive/sdk/authentication/MSAAuthenticator;)Lcom/onedrive/sdk/logger/ILogger;

    move-result-object p1

    const-string p2, "Successful silent login"

    invoke-interface {p1, p2}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 318
    :goto_0
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator$5;->val$loginSilentWaiter:Lcom/onedrive/sdk/concurrency/SimpleWaiter;

    invoke-virtual {p1}, Lcom/onedrive/sdk/concurrency/SimpleWaiter;->signal()V

    return-void
.end method

.method public onAuthError(Lcom/microsoft/services/msa/LiveAuthException;Ljava/lang/Object;)V
    .locals 3

    .line 324
    sget-object p2, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->AuthenticationFailure:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 325
    invoke-virtual {p1}, Lcom/microsoft/services/msa/LiveAuthException;->getError()Ljava/lang/String;

    move-result-object v0

    const-string v1, "The user cancelled the login operation."

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 326
    sget-object p2, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->AuthenticationCancelled:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 329
    :cond_0
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator$5;->val$error:Ljava/util/concurrent/atomic/AtomicReference;

    new-instance v1, Lcom/onedrive/sdk/authentication/ClientAuthenticatorException;

    const-string v2, "Login silent authentication error"

    invoke-direct {v1, v2, p1, p2}, Lcom/onedrive/sdk/authentication/ClientAuthenticatorException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lcom/onedrive/sdk/core/OneDriveErrorCodes;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 330
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator$5;->this$0:Lcom/onedrive/sdk/authentication/MSAAuthenticator;

    invoke-static {p1}, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->access$100(Lcom/onedrive/sdk/authentication/MSAAuthenticator;)Lcom/onedrive/sdk/logger/ILogger;

    move-result-object p1

    iget-object p2, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator$5;->val$error:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {p2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/onedrive/sdk/core/ClientException;

    invoke-virtual {p2}, Lcom/onedrive/sdk/core/ClientException;->getMessage()Ljava/lang/String;

    move-result-object p2

    iget-object v0, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator$5;->val$error:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    invoke-interface {p1, p2, v0}, Lcom/onedrive/sdk/logger/ILogger;->logError(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 331
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator$5;->val$loginSilentWaiter:Lcom/onedrive/sdk/concurrency/SimpleWaiter;

    invoke-virtual {p1}, Lcom/onedrive/sdk/concurrency/SimpleWaiter;->signal()V

    return-void
.end method

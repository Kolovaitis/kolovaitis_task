.class public Lcom/onedrive/sdk/authentication/ADALAccountInfo;
.super Ljava/lang/Object;
.source "ADALAccountInfo.java"

# interfaces
.implements Lcom/onedrive/sdk/authentication/IAccountInfo;


# instance fields
.field private mAuthenticationResult:Lcom/microsoft/aad/adal/AuthenticationResult;

.field private final mAuthenticator:Lcom/onedrive/sdk/authentication/ADALAuthenticator;

.field private final mLogger:Lcom/onedrive/sdk/logger/ILogger;

.field private final mOneDriveServiceInfo:Lcom/onedrive/sdk/authentication/ServiceInfo;


# direct methods
.method public constructor <init>(Lcom/onedrive/sdk/authentication/ADALAuthenticator;Lcom/microsoft/aad/adal/AuthenticationResult;Lcom/onedrive/sdk/authentication/ServiceInfo;Lcom/onedrive/sdk/logger/ILogger;)V
    .locals 0

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-object p1, p0, Lcom/onedrive/sdk/authentication/ADALAccountInfo;->mAuthenticator:Lcom/onedrive/sdk/authentication/ADALAuthenticator;

    .line 65
    iput-object p2, p0, Lcom/onedrive/sdk/authentication/ADALAccountInfo;->mAuthenticationResult:Lcom/microsoft/aad/adal/AuthenticationResult;

    .line 66
    iput-object p3, p0, Lcom/onedrive/sdk/authentication/ADALAccountInfo;->mOneDriveServiceInfo:Lcom/onedrive/sdk/authentication/ServiceInfo;

    .line 67
    iput-object p4, p0, Lcom/onedrive/sdk/authentication/ADALAccountInfo;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    return-void
.end method


# virtual methods
.method public getAccessToken()Ljava/lang/String;
    .locals 1

    .line 85
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAccountInfo;->mAuthenticationResult:Lcom/microsoft/aad/adal/AuthenticationResult;

    invoke-virtual {v0}, Lcom/microsoft/aad/adal/AuthenticationResult;->getAccessToken()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAccountType()Lcom/onedrive/sdk/authentication/AccountType;
    .locals 1

    .line 76
    sget-object v0, Lcom/onedrive/sdk/authentication/AccountType;->ActiveDirectory:Lcom/onedrive/sdk/authentication/AccountType;

    return-object v0
.end method

.method public getServiceRoot()Ljava/lang/String;
    .locals 1

    .line 94
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAccountInfo;->mOneDriveServiceInfo:Lcom/onedrive/sdk/authentication/ServiceInfo;

    iget-object v0, v0, Lcom/onedrive/sdk/authentication/ServiceInfo;->serviceEndpointUri:Ljava/lang/String;

    return-object v0
.end method

.method public isExpired()Z
    .locals 1

    .line 104
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAccountInfo;->mAuthenticationResult:Lcom/microsoft/aad/adal/AuthenticationResult;

    invoke-virtual {v0}, Lcom/microsoft/aad/adal/AuthenticationResult;->isExpired()Z

    move-result v0

    return v0
.end method

.method public refresh()V
    .locals 2

    .line 112
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAccountInfo;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Refreshing access token..."

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 113
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAccountInfo;->mAuthenticator:Lcom/onedrive/sdk/authentication/ADALAuthenticator;

    invoke-virtual {v0}, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->loginSilent()Lcom/onedrive/sdk/authentication/IAccountInfo;

    move-result-object v0

    check-cast v0, Lcom/onedrive/sdk/authentication/ADALAccountInfo;

    .line 114
    iget-object v0, v0, Lcom/onedrive/sdk/authentication/ADALAccountInfo;->mAuthenticationResult:Lcom/microsoft/aad/adal/AuthenticationResult;

    iput-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAccountInfo;->mAuthenticationResult:Lcom/microsoft/aad/adal/AuthenticationResult;

    return-void
.end method

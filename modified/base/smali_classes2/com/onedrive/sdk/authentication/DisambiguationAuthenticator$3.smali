.class Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator$3;
.super Ljava/lang/Object;
.source "DisambiguationAuthenticator.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->loginSilent(Lcom/onedrive/sdk/concurrency/ICallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;

.field final synthetic val$loginCallback:Lcom/onedrive/sdk/concurrency/ICallback;


# direct methods
.method constructor <init>(Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 0

    .line 249
    iput-object p1, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator$3;->this$0:Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;

    iput-object p2, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator$3;->val$loginCallback:Lcom/onedrive/sdk/concurrency/ICallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 253
    :try_start_0
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator$3;->this$0:Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;

    invoke-static {v0}, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->access$000(Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;)Lcom/onedrive/sdk/concurrency/IExecutors;

    move-result-object v0

    iget-object v1, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator$3;->this$0:Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;

    invoke-virtual {v1}, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->loginSilent()Lcom/onedrive/sdk/authentication/IAccountInfo;

    move-result-object v1

    iget-object v2, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator$3;->val$loginCallback:Lcom/onedrive/sdk/concurrency/ICallback;

    invoke-interface {v0, v1, v2}, Lcom/onedrive/sdk/concurrency/IExecutors;->performOnForeground(Ljava/lang/Object;Lcom/onedrive/sdk/concurrency/ICallback;)V
    :try_end_0
    .catch Lcom/onedrive/sdk/core/ClientException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 255
    iget-object v1, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator$3;->this$0:Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;

    invoke-static {v1}, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->access$000(Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;)Lcom/onedrive/sdk/concurrency/IExecutors;

    move-result-object v1

    iget-object v2, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator$3;->val$loginCallback:Lcom/onedrive/sdk/concurrency/ICallback;

    invoke-interface {v1, v0, v2}, Lcom/onedrive/sdk/concurrency/IExecutors;->performOnForeground(Lcom/onedrive/sdk/core/ClientException;Lcom/onedrive/sdk/concurrency/ICallback;)V

    :goto_0
    return-void
.end method

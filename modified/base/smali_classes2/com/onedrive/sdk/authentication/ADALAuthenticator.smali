.class public abstract Lcom/onedrive/sdk/authentication/ADALAuthenticator;
.super Ljava/lang/Object;
.source "ADALAuthenticator.java"

# interfaces
.implements Lcom/onedrive/sdk/authentication/IAuthenticator;


# static fields
.field private static final ADAL_AUTHENTICATOR_PREFS:Ljava/lang/String; = "ADALAuthenticatorPrefs"

.field private static final DISCOVERY_SERVICE_URL:Ljava/lang/String; = "https://api.office.com/discovery/v2.0/me/Services"

.field private static final DISCOVER_SERVICE_RESOURCE_ID:Ljava/lang/String; = "https://api.office.com/discovery/"

.field private static final LOGIN_AUTHORITY:Ljava/lang/String; = "https://login.windows.net/common/oauth2/authorize"

.field private static final RESOURCE_URL_KEY:Ljava/lang/String; = "resourceUrl"

.field private static final SERVICE_INFO_KEY:Ljava/lang/String; = "serviceInfo"

.field private static final USER_ID_KEY:Ljava/lang/String; = "userId"

.field private static final VALIDATE_AUTHORITY:Z = true

.field private static final VERSION_CODE_KEY:Ljava/lang/String; = "versionCode"


# instance fields
.field private final mAccountInfo:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference<",
            "Lcom/onedrive/sdk/authentication/IAccountInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mActivity:Landroid/app/Activity;

.field private mAdalContext:Lcom/microsoft/aad/adal/AuthenticationContext;

.field private mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

.field private mHttpProvider:Lcom/onedrive/sdk/http/IHttpProvider;

.field private mInitialized:Z

.field private mLogger:Lcom/onedrive/sdk/logger/ILogger;

.field private final mOneDriveServiceInfo:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference<",
            "Lcom/onedrive/sdk/authentication/ServiceInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mResourceUrl:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mUserId:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mResourceUrl:Ljava/util/concurrent/atomic/AtomicReference;

    .line 118
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mUserId:Ljava/util/concurrent/atomic/AtomicReference;

    .line 123
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mOneDriveServiceInfo:Ljava/util/concurrent/atomic/AtomicReference;

    .line 128
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mAccountInfo:Ljava/util/concurrent/atomic/AtomicReference;

    return-void
.end method

.method static synthetic access$000(Lcom/onedrive/sdk/authentication/ADALAuthenticator;)Lcom/onedrive/sdk/concurrency/IExecutors;
    .locals 0

    .line 62
    iget-object p0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

    return-object p0
.end method

.method static synthetic access$100(Lcom/onedrive/sdk/authentication/ADALAuthenticator;)Lcom/onedrive/sdk/logger/ILogger;
    .locals 0

    .line 62
    iget-object p0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    return-object p0
.end method

.method private getDiscoveryServiceAuthResult(Ljava/lang/String;)Lcom/microsoft/aad/adal/AuthenticationResult;
    .locals 11

    .line 510
    new-instance v0, Lcom/onedrive/sdk/concurrency/SimpleWaiter;

    invoke-direct {v0}, Lcom/onedrive/sdk/concurrency/SimpleWaiter;-><init>()V

    .line 511
    new-instance v1, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 512
    new-instance v2, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v2}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 514
    new-instance v10, Lcom/onedrive/sdk/authentication/ADALAuthenticator$5;

    invoke-direct {v10, p0, v2, v0, v1}, Lcom/onedrive/sdk/authentication/ADALAuthenticator$5;-><init>(Lcom/onedrive/sdk/authentication/ADALAuthenticator;Ljava/util/concurrent/atomic/AtomicReference;Lcom/onedrive/sdk/concurrency/SimpleWaiter;Ljava/util/concurrent/atomic/AtomicReference;)V

    .line 553
    iget-object v3, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v4, "Starting interactive login for the discover service access token"

    invoke-interface {v3, v4}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 556
    iget-object v3, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mAdalContext:Lcom/microsoft/aad/adal/AuthenticationContext;

    const-string v4, "https://api.office.com/discovery/"

    invoke-virtual {p0}, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->getClientId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->getRedirectUrl()Ljava/lang/String;

    move-result-object v6

    sget-object v8, Lcom/microsoft/aad/adal/PromptBehavior;->Auto:Lcom/microsoft/aad/adal/PromptBehavior;

    const/4 v9, 0x0

    move-object v7, p1

    invoke-virtual/range {v3 .. v10}, Lcom/microsoft/aad/adal/AuthenticationContext;->acquireToken(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/aad/adal/PromptBehavior;Ljava/lang/String;Lcom/microsoft/aad/adal/AuthenticationCallback;)V

    .line 564
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v3, "Waiting for interactive login to complete"

    invoke-interface {p1, v3}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 565
    invoke-virtual {v0}, Lcom/onedrive/sdk/concurrency/SimpleWaiter;->waitForSignal()V

    .line 567
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_0

    .line 570
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/microsoft/aad/adal/AuthenticationResult;

    return-object p1

    .line 568
    :cond_0
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/onedrive/sdk/core/ClientException;

    throw p1
.end method

.method private getOneDriveApiService([Lcom/onedrive/sdk/authentication/ServiceInfo;)Lcom/onedrive/sdk/authentication/ServiceInfo;
    .locals 9

    .line 587
    array-length v0, p1

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    aget-object v3, p1, v2

    .line 588
    iget-object v4, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v5, "Service info resource id%s capabilities %s version %s"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, v3, Lcom/onedrive/sdk/authentication/ServiceInfo;->serviceResourceId:Ljava/lang/String;

    aput-object v7, v6, v1

    iget-object v7, v3, Lcom/onedrive/sdk/authentication/ServiceInfo;->capability:Ljava/lang/String;

    const/4 v8, 0x1

    aput-object v7, v6, v8

    const/4 v7, 0x2

    iget-object v8, v3, Lcom/onedrive/sdk/authentication/ServiceInfo;->serviceApiVersion:Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 592
    iget-object v4, v3, Lcom/onedrive/sdk/authentication/ServiceInfo;->capability:Ljava/lang/String;

    const-string v5, "MyFiles"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, v3, Lcom/onedrive/sdk/authentication/ServiceInfo;->serviceApiVersion:Ljava/lang/String;

    const-string v5, "v2.0"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 598
    :cond_1
    new-instance p1, Lcom/onedrive/sdk/authentication/ClientAuthenticatorException;

    sget-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->AuthenticationFailure:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    const-string v1, "Unable to file the files services from the directory provider"

    invoke-direct {p1, v1, v0}, Lcom/onedrive/sdk/authentication/ClientAuthenticatorException;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/core/OneDriveErrorCodes;)V

    throw p1
.end method

.method private getOneDriveServiceAuthResult(Lcom/onedrive/sdk/authentication/ServiceInfo;)Lcom/microsoft/aad/adal/AuthenticationResult;
    .locals 10

    .line 632
    new-instance v0, Lcom/onedrive/sdk/concurrency/SimpleWaiter;

    invoke-direct {v0}, Lcom/onedrive/sdk/concurrency/SimpleWaiter;-><init>()V

    .line 633
    new-instance v1, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 634
    new-instance v2, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v2}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 635
    new-instance v9, Lcom/onedrive/sdk/authentication/ADALAuthenticator$7;

    invoke-direct {v9, p0, v2, v0, v1}, Lcom/onedrive/sdk/authentication/ADALAuthenticator$7;-><init>(Lcom/onedrive/sdk/authentication/ADALAuthenticator;Ljava/util/concurrent/atomic/AtomicReference;Lcom/onedrive/sdk/concurrency/SimpleWaiter;Ljava/util/concurrent/atomic/AtomicReference;)V

    .line 664
    iget-object v3, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v4, "Starting OneDrive resource refresh token request"

    invoke-interface {v3, v4}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 665
    iget-object v3, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mAdalContext:Lcom/microsoft/aad/adal/AuthenticationContext;

    iget-object v4, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mActivity:Landroid/app/Activity;

    iget-object v5, p1, Lcom/onedrive/sdk/authentication/ServiceInfo;->serviceResourceId:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->getClientId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->getRedirectUrl()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lcom/microsoft/aad/adal/PromptBehavior;->Auto:Lcom/microsoft/aad/adal/PromptBehavior;

    invoke-virtual/range {v3 .. v9}, Lcom/microsoft/aad/adal/AuthenticationContext;->acquireToken(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/aad/adal/PromptBehavior;Lcom/microsoft/aad/adal/AuthenticationCallback;)V

    .line 672
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v3, "Waiting for token refresh"

    invoke-interface {p1, v3}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 673
    invoke-virtual {v0}, Lcom/onedrive/sdk/concurrency/SimpleWaiter;->waitForSignal()V

    .line 675
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_0

    .line 678
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/microsoft/aad/adal/AuthenticationResult;

    return-object p1

    .line 676
    :cond_0
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/onedrive/sdk/core/ClientException;

    throw p1
.end method

.method private getOneDriveServiceInfoFromDiscoveryService(Ljava/lang/String;)Lcom/onedrive/sdk/authentication/ServiceInfo;
    .locals 6

    .line 608
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 609
    new-instance v0, Lcom/onedrive/sdk/options/HeaderOption;

    const-string v1, "Authorization"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bearer "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/onedrive/sdk/options/HeaderOption;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 612
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v0, "Starting discovery service request"

    invoke-interface {p1, v0}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 613
    new-instance p1, Lcom/onedrive/sdk/authentication/ADALAuthenticator$6;

    const-string v2, "https://api.office.com/discovery/v2.0/me/Services"

    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object v0, p1

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/onedrive/sdk/authentication/ADALAuthenticator$6;-><init>(Lcom/onedrive/sdk/authentication/ADALAuthenticator;Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;Ljava/lang/Class;)V

    .line 617
    sget-object v0, Lcom/onedrive/sdk/http/HttpMethod;->GET:Lcom/onedrive/sdk/http/HttpMethod;

    invoke-virtual {p1, v0}, Lcom/onedrive/sdk/http/BaseRequest;->setHttpMethod(Lcom/onedrive/sdk/http/HttpMethod;)V

    .line 619
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mHttpProvider:Lcom/onedrive/sdk/http/IHttpProvider;

    const-class v1, Lcom/onedrive/sdk/authentication/DiscoveryServiceResponse;

    const/4 v2, 0x0

    invoke-interface {v0, p1, v1, v2}, Lcom/onedrive/sdk/http/IHttpProvider;->send(Lcom/onedrive/sdk/http/IHttpRequest;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/onedrive/sdk/authentication/DiscoveryServiceResponse;

    .line 623
    iget-object p1, p1, Lcom/onedrive/sdk/authentication/DiscoveryServiceResponse;->services:[Lcom/onedrive/sdk/authentication/ServiceInfo;

    invoke-direct {p0, p1}, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->getOneDriveApiService([Lcom/onedrive/sdk/authentication/ServiceInfo;)Lcom/onedrive/sdk/authentication/ServiceInfo;

    move-result-object p1

    return-object p1
.end method

.method private getSharedPreferences()Landroid/content/SharedPreferences;
    .locals 3

    .line 578
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mActivity:Landroid/app/Activity;

    const-string v1, "ADALAuthenticatorPrefs"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAccountInfo()Lcom/onedrive/sdk/authentication/IAccountInfo;
    .locals 1

    .line 178
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mAccountInfo:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/onedrive/sdk/authentication/IAccountInfo;

    return-object v0
.end method

.method protected abstract getClientId()Ljava/lang/String;
.end method

.method protected abstract getRedirectUrl()Ljava/lang/String;
.end method

.method public declared-synchronized init(Lcom/onedrive/sdk/concurrency/IExecutors;Lcom/onedrive/sdk/http/IHttpProvider;Landroid/app/Activity;Lcom/onedrive/sdk/logger/ILogger;)V
    .locals 1

    monitor-enter p0

    .line 193
    :try_start_0
    iget-boolean v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mInitialized:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 194
    monitor-exit p0

    return-void

    .line 197
    :cond_0
    :try_start_1
    iput-object p1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

    .line 198
    iput-object p2, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mHttpProvider:Lcom/onedrive/sdk/http/IHttpProvider;

    .line 199
    iput-object p3, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mActivity:Landroid/app/Activity;

    .line 200
    iput-object p4, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    .line 202
    new-instance p1, Lcom/onedrive/sdk/authentication/adal/BrokerPermissionsChecker;

    iget-object p2, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mActivity:Landroid/app/Activity;

    iget-object p4, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    invoke-direct {p1, p2, p4}, Lcom/onedrive/sdk/authentication/adal/BrokerPermissionsChecker;-><init>(Landroid/content/Context;Lcom/onedrive/sdk/logger/ILogger;)V

    .line 203
    invoke-virtual {p1}, Lcom/onedrive/sdk/authentication/adal/BrokerPermissionsChecker;->check()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 206
    :try_start_2
    new-instance p1, Lcom/microsoft/aad/adal/AuthenticationContext;

    const-string p2, "https://login.windows.net/common/oauth2/authorize"

    const/4 p4, 0x1

    invoke-direct {p1, p3, p2, p4}, Lcom/microsoft/aad/adal/AuthenticationContext;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    iput-object p1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mAdalContext:Lcom/microsoft/aad/adal/AuthenticationContext;
    :try_end_2
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 218
    :try_start_3
    invoke-direct {p0}, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object p1

    .line 219
    iget-object p2, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mUserId:Ljava/util/concurrent/atomic/AtomicReference;

    const-string p3, "userId"

    const/4 v0, 0x0

    invoke-interface {p1, p3, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 220
    iget-object p2, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mResourceUrl:Ljava/util/concurrent/atomic/AtomicReference;

    const-string p3, "resourceUrl"

    invoke-interface {p1, p3, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    const-string p2, "serviceInfo"

    .line 222
    invoke-interface {p1, p2, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz p1, :cond_1

    .line 226
    :try_start_4
    iget-object p2, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mHttpProvider:Lcom/onedrive/sdk/http/IHttpProvider;

    invoke-interface {p2}, Lcom/onedrive/sdk/http/IHttpProvider;->getSerializer()Lcom/onedrive/sdk/serializer/ISerializer;

    move-result-object p2

    const-class p3, Lcom/onedrive/sdk/authentication/ServiceInfo;

    invoke-interface {p2, p1, p3}, Lcom/onedrive/sdk/serializer/ISerializer;->deserializeObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/onedrive/sdk/authentication/ServiceInfo;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-object v0, p1

    goto :goto_0

    :catch_0
    move-exception p1

    .line 230
    :try_start_5
    iget-object p2, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string p3, "Unable to parse serviceInfo from saved preferences"

    invoke-interface {p2, p3, p1}, Lcom/onedrive/sdk/logger/ILogger;->logError(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 232
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mOneDriveServiceInfo:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {p1, v0}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 233
    iput-boolean p4, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mInitialized:Z

    .line 237
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mUserId:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_2

    iget-object p1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mResourceUrl:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_2

    iget-object p1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mOneDriveServiceInfo:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_4

    .line 238
    :cond_2
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string p2, "Found existing login information"

    invoke-interface {p1, p2}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 239
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mUserId:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mResourceUrl:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mOneDriveServiceInfo:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_4

    .line 240
    :cond_3
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string p2, "Existing login information was incompletely, flushing sign in state"

    invoke-interface {p1, p2}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 241
    invoke-virtual {p0}, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->logout()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 244
    :cond_4
    monitor-exit p0

    return-void

    :catch_1
    move-exception p1

    goto :goto_1

    :catch_2
    move-exception p1

    .line 210
    :goto_1
    :try_start_6
    new-instance p2, Lcom/onedrive/sdk/authentication/ClientAuthenticatorException;

    const-string p3, "Unable to access required cryptographic libraries for ADAL"

    sget-object p4, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->AuthenticationFailure:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    invoke-direct {p2, p3, p1, p4}, Lcom/onedrive/sdk/authentication/ClientAuthenticatorException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lcom/onedrive/sdk/core/OneDriveErrorCodes;)V

    .line 214
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string p3, "Problem creating the AuthenticationContext for ADAL"

    invoke-interface {p1, p3, p2}, Lcom/onedrive/sdk/logger/ILogger;->logError(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 215
    throw p2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized login(Ljava/lang/String;)Lcom/onedrive/sdk/authentication/IAccountInfo;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    monitor-enter p0

    .line 283
    :try_start_0
    iget-boolean v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mInitialized:Z

    if-eqz v0, :cond_1

    .line 287
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Starting login"

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 289
    invoke-direct {p0, p1}, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->getDiscoveryServiceAuthResult(Ljava/lang/String;)Lcom/microsoft/aad/adal/AuthenticationResult;

    move-result-object p1

    .line 292
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationResult;->getStatus()Lcom/microsoft/aad/adal/AuthenticationResult$AuthenticationStatus;

    move-result-object v0

    sget-object v1, Lcom/microsoft/aad/adal/AuthenticationResult$AuthenticationStatus;->Succeeded:Lcom/microsoft/aad/adal/AuthenticationResult$AuthenticationStatus;

    if-ne v0, v1, :cond_0

    .line 305
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationResult;->getAccessToken()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->getOneDriveServiceInfoFromDiscoveryService(Ljava/lang/String;)Lcom/onedrive/sdk/authentication/ServiceInfo;

    move-result-object v0

    .line 309
    invoke-direct {p0, v0}, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->getOneDriveServiceAuthResult(Lcom/onedrive/sdk/authentication/ServiceInfo;)Lcom/microsoft/aad/adal/AuthenticationResult;

    move-result-object v1

    .line 313
    iget-object v2, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mHttpProvider:Lcom/onedrive/sdk/http/IHttpProvider;

    invoke-interface {v2}, Lcom/onedrive/sdk/http/IHttpProvider;->getSerializer()Lcom/onedrive/sdk/serializer/ISerializer;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/onedrive/sdk/serializer/ISerializer;->serializeObject(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 316
    iget-object v3, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v4, "Successful login, saving information for silent re-auth"

    invoke-interface {v3, v4}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 317
    invoke-direct {p0}, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v3

    .line 318
    iget-object v4, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mResourceUrl:Ljava/util/concurrent/atomic/AtomicReference;

    iget-object v5, v0, Lcom/onedrive/sdk/authentication/ServiceInfo;->serviceEndpointUri:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 319
    iget-object v4, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mUserId:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationResult;->getUserInfo()Lcom/microsoft/aad/adal/UserInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/microsoft/aad/adal/UserInfo;->getUserId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v4, p1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 320
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mOneDriveServiceInfo:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {p1, v0}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 321
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    const-string v3, "resourceUrl"

    iget-object v4, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mResourceUrl:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-interface {p1, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    const-string v3, "userId"

    iget-object v4, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mUserId:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-interface {p1, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    const-string v3, "serviceInfo"

    invoke-interface {p1, v3, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    const-string v3, "versionCode"

    const/16 v4, 0x283d

    invoke-interface {p1, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 329
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v3, "Successfully retrieved login information"

    invoke-interface {p1, v3}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 330
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "   Resource Url: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mResourceUrl:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v3}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 331
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "   User ID: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mUserId:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v3}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 332
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "   Service Info: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v2}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 333
    new-instance p1, Lcom/onedrive/sdk/authentication/ADALAccountInfo;

    iget-object v2, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    invoke-direct {p1, p0, v1, v0, v2}, Lcom/onedrive/sdk/authentication/ADALAccountInfo;-><init>(Lcom/onedrive/sdk/authentication/ADALAuthenticator;Lcom/microsoft/aad/adal/AuthenticationResult;Lcom/onedrive/sdk/authentication/ServiceInfo;Lcom/onedrive/sdk/logger/ILogger;)V

    .line 337
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mAccountInfo:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 338
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mAccountInfo:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/onedrive/sdk/authentication/IAccountInfo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    .line 293
    :cond_0
    :try_start_1
    new-instance v0, Lcom/onedrive/sdk/authentication/ClientAuthenticatorException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to authenticate user with ADAL, Error Code: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationResult;->getErrorCode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " Error Message"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationResult;->getErrorDescription()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->AuthenticationFailure:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    invoke-direct {v0, p1, v1}, Lcom/onedrive/sdk/authentication/ClientAuthenticatorException;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/core/OneDriveErrorCodes;)V

    .line 300
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Unsuccessful login attempt"

    invoke-interface {p1, v1, v0}, Lcom/onedrive/sdk/logger/ILogger;->logError(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 301
    throw v0

    .line 284
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "init must be called"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public login(Ljava/lang/String;Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/authentication/IAccountInfo;",
            ">;)V"
        }
    .end annotation

    .line 253
    iget-boolean v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mInitialized:Z

    if-eqz v0, :cond_1

    if-eqz p2, :cond_0

    .line 261
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Starting login async"

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 263
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

    new-instance v1, Lcom/onedrive/sdk/authentication/ADALAuthenticator$1;

    invoke-direct {v1, p0, p2, p1}, Lcom/onedrive/sdk/authentication/ADALAuthenticator$1;-><init>(Lcom/onedrive/sdk/authentication/ADALAuthenticator;Lcom/onedrive/sdk/concurrency/ICallback;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/concurrency/IExecutors;->performOnBackground(Ljava/lang/Runnable;)V

    return-void

    .line 258
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "loginCallback"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 254
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "init must be called"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public declared-synchronized loginSilent()Lcom/onedrive/sdk/authentication/IAccountInfo;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    monitor-enter p0

    .line 376
    :try_start_0
    iget-boolean v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mInitialized:Z

    if-eqz v0, :cond_2

    .line 380
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Starting login silent"

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 382
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mResourceUrl:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 383
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "No login information found for silent authentication"

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x0

    .line 384
    monitor-exit p0

    return-object v0

    .line 387
    :cond_0
    :try_start_1
    new-instance v0, Lcom/onedrive/sdk/concurrency/SimpleWaiter;

    invoke-direct {v0}, Lcom/onedrive/sdk/concurrency/SimpleWaiter;-><init>()V

    .line 388
    new-instance v1, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 389
    new-instance v2, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v2}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 391
    new-instance v3, Lcom/onedrive/sdk/authentication/ADALAuthenticator$3;

    invoke-direct {v3, p0, v1, v0, v2}, Lcom/onedrive/sdk/authentication/ADALAuthenticator$3;-><init>(Lcom/onedrive/sdk/authentication/ADALAuthenticator;Ljava/util/concurrent/atomic/AtomicReference;Lcom/onedrive/sdk/concurrency/SimpleWaiter;Ljava/util/concurrent/atomic/AtomicReference;)V

    .line 425
    iget-object v4, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mAdalContext:Lcom/microsoft/aad/adal/AuthenticationContext;

    iget-object v5, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mOneDriveServiceInfo:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/onedrive/sdk/authentication/ServiceInfo;

    iget-object v5, v5, Lcom/onedrive/sdk/authentication/ServiceInfo;->serviceResourceId:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->getClientId()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mUserId:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v4, v5, v6, v7, v3}, Lcom/microsoft/aad/adal/AuthenticationContext;->acquireTokenSilent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/aad/adal/AuthenticationCallback;)Ljava/util/concurrent/Future;

    .line 430
    invoke-virtual {v0}, Lcom/onedrive/sdk/concurrency/SimpleWaiter;->waitForSignal()V

    .line 432
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 436
    new-instance v0, Lcom/onedrive/sdk/authentication/ADALAccountInfo;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/aad/adal/AuthenticationResult;

    iget-object v2, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mOneDriveServiceInfo:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/onedrive/sdk/authentication/ServiceInfo;

    iget-object v3, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/onedrive/sdk/authentication/ADALAccountInfo;-><init>(Lcom/onedrive/sdk/authentication/ADALAuthenticator;Lcom/microsoft/aad/adal/AuthenticationResult;Lcom/onedrive/sdk/authentication/ServiceInfo;Lcom/onedrive/sdk/logger/ILogger;)V

    .line 440
    iget-object v1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mAccountInfo:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 441
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mAccountInfo:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/onedrive/sdk/authentication/IAccountInfo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0

    .line 433
    :cond_1
    :try_start_2
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/onedrive/sdk/core/ClientException;

    throw v0

    .line 377
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "init must be called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public loginSilent(Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/authentication/IAccountInfo;",
            ">;)V"
        }
    .end annotation

    .line 347
    iget-boolean v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mInitialized:Z

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    .line 355
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Starting login silent async"

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 357
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

    new-instance v1, Lcom/onedrive/sdk/authentication/ADALAuthenticator$2;

    invoke-direct {v1, p0, p1}, Lcom/onedrive/sdk/authentication/ADALAuthenticator$2;-><init>(Lcom/onedrive/sdk/authentication/ADALAuthenticator;Lcom/onedrive/sdk/concurrency/ICallback;)V

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/concurrency/IExecutors;->performOnBackground(Ljava/lang/Runnable;)V

    return-void

    .line 352
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "loginCallback"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 348
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "init must be called"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public declared-synchronized logout()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    monitor-enter p0

    .line 480
    :try_start_0
    iget-boolean v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mInitialized:Z

    if-eqz v0, :cond_0

    .line 484
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Starting logout"

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 485
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Clearing ADAL cache"

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 486
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mAdalContext:Lcom/microsoft/aad/adal/AuthenticationContext;

    invoke-virtual {v0}, Lcom/microsoft/aad/adal/AuthenticationContext;->getCache()Lcom/microsoft/aad/adal/ITokenCacheStore;

    move-result-object v0

    invoke-interface {v0}, Lcom/microsoft/aad/adal/ITokenCacheStore;->removeAll()V

    .line 488
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Clearing all webview cookies"

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 489
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    .line 490
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v0

    .line 491
    invoke-virtual {v0}, Landroid/webkit/CookieManager;->removeAllCookie()V

    .line 492
    invoke-static {}, Landroid/webkit/CookieSyncManager;->getInstance()Landroid/webkit/CookieSyncManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/CookieSyncManager;->sync()V

    .line 494
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Clearing all ADAL Authenticator shared preferences"

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 495
    invoke-direct {p0}, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 496
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "versionCode"

    const/16 v2, 0x283d

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 500
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mUserId:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 501
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mResourceUrl:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 502
    monitor-exit p0

    return-void

    .line 481
    :cond_0
    :try_start_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "init must be called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public logout(Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .line 450
    iget-boolean v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mInitialized:Z

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    .line 458
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Starting logout async"

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 460
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

    new-instance v1, Lcom/onedrive/sdk/authentication/ADALAuthenticator$4;

    invoke-direct {v1, p0, p1}, Lcom/onedrive/sdk/authentication/ADALAuthenticator$4;-><init>(Lcom/onedrive/sdk/authentication/ADALAuthenticator;Lcom/onedrive/sdk/concurrency/ICallback;)V

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/concurrency/IExecutors;->performOnBackground(Ljava/lang/Runnable;)V

    return-void

    .line 455
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "logoutCallback"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 451
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "init must be called"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.class Lcom/onedrive/sdk/authentication/DisambiguationDialog;
.super Landroid/app/Dialog;
.source "DisambiguationDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# static fields
.field private static final DISAMBIGUATION_PAGE_URL:Ljava/lang/String; = "https://onedrive.live.com/picker/accountchooser?ru=https://localhost:777&load_login=false"


# instance fields
.field private final mRequest:Lcom/onedrive/sdk/authentication/DisambiguationRequest;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/onedrive/sdk/authentication/DisambiguationRequest;)V
    .locals 1

    const v0, 0x1030010

    .line 63
    invoke-direct {p0, p1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 64
    iput-object p2, p0, Lcom/onedrive/sdk/authentication/DisambiguationDialog;->mRequest:Lcom/onedrive/sdk/authentication/DisambiguationRequest;

    return-void
.end method


# virtual methods
.method public getLogger()Lcom/onedrive/sdk/logger/ILogger;
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationDialog;->mRequest:Lcom/onedrive/sdk/authentication/DisambiguationRequest;

    invoke-virtual {v0}, Lcom/onedrive/sdk/authentication/DisambiguationRequest;->getLogger()Lcom/onedrive/sdk/logger/ILogger;

    move-result-object v0

    return-object v0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 3

    .line 77
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/DisambiguationDialog;->mRequest:Lcom/onedrive/sdk/authentication/DisambiguationRequest;

    invoke-virtual {p1}, Lcom/onedrive/sdk/authentication/DisambiguationRequest;->getLogger()Lcom/onedrive/sdk/logger/ILogger;

    move-result-object p1

    const-string v0, "Disambiguation dialog canceled"

    invoke-interface {p1, v0}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 78
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/DisambiguationDialog;->mRequest:Lcom/onedrive/sdk/authentication/DisambiguationRequest;

    invoke-virtual {p1}, Lcom/onedrive/sdk/authentication/DisambiguationRequest;->getCallback()Lcom/onedrive/sdk/concurrency/ICallback;

    move-result-object p1

    new-instance v0, Lcom/onedrive/sdk/authentication/ClientAuthenticatorException;

    const-string v1, "Authentication Disambiguation Canceled"

    sget-object v2, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->AuthenticationCancelled:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    invoke-direct {v0, v1, v2}, Lcom/onedrive/sdk/authentication/ClientAuthenticatorException;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/core/OneDriveErrorCodes;)V

    invoke-interface {p1, v0}, Lcom/onedrive/sdk/concurrency/ICallback;->failure(Lcom/onedrive/sdk/core/ClientException;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetJavaScriptEnabled"
        }
    .end annotation

    .line 85
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 87
    invoke-virtual {p0, p0}, Lcom/onedrive/sdk/authentication/DisambiguationDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 89
    new-instance p1, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/onedrive/sdk/authentication/DisambiguationDialog;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p1, v0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 90
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/onedrive/sdk/authentication/DisambiguationDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 91
    new-instance v1, Landroid/webkit/WebView;

    invoke-virtual {p0}, Lcom/onedrive/sdk/authentication/DisambiguationDialog;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    .line 93
    new-instance v2, Lcom/onedrive/sdk/authentication/DisambiguationWebView;

    iget-object v3, p0, Lcom/onedrive/sdk/authentication/DisambiguationDialog;->mRequest:Lcom/onedrive/sdk/authentication/DisambiguationRequest;

    invoke-virtual {v3}, Lcom/onedrive/sdk/authentication/DisambiguationRequest;->getCallback()Lcom/onedrive/sdk/concurrency/ICallback;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/onedrive/sdk/authentication/DisambiguationWebView;-><init>(Lcom/onedrive/sdk/authentication/DisambiguationDialog;Lcom/onedrive/sdk/concurrency/ICallback;)V

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 95
    invoke-virtual {v1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v2

    const/4 v3, 0x1

    .line 96
    invoke-virtual {v2, v3}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    const-string v2, "https://onedrive.live.com/picker/accountchooser?ru=https://localhost:777&load_login=false"

    .line 98
    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 99
    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    const/4 v3, -0x1

    invoke-direct {v2, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 101
    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v3, 0x0

    .line 102
    invoke-virtual {v1, v3}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 104
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 105
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 107
    invoke-virtual {p1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 108
    invoke-virtual {p1, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 109
    invoke-virtual {p1}, Landroid/widget/FrameLayout;->forceLayout()V

    .line 110
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->forceLayout()V

    .line 112
    invoke-virtual {p0, p1, v2}, Lcom/onedrive/sdk/authentication/DisambiguationDialog;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

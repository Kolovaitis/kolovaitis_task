.class public Lcom/onedrive/sdk/authentication/AuthorizationInterceptor;
.super Ljava/lang/Object;
.source "AuthorizationInterceptor.java"

# interfaces
.implements Lcom/onedrive/sdk/http/IRequestInterceptor;


# static fields
.field public static final AUTHORIZATION_HEADER_NAME:Ljava/lang/String; = "Authorization"

.field public static final OAUTH_BEARER_PREFIX:Ljava/lang/String; = "bearer "


# instance fields
.field private final mAuthenticator:Lcom/onedrive/sdk/authentication/IAuthenticator;

.field private final mLogger:Lcom/onedrive/sdk/logger/ILogger;


# direct methods
.method public constructor <init>(Lcom/onedrive/sdk/authentication/IAuthenticator;Lcom/onedrive/sdk/logger/ILogger;)V
    .locals 0

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Lcom/onedrive/sdk/authentication/AuthorizationInterceptor;->mAuthenticator:Lcom/onedrive/sdk/authentication/IAuthenticator;

    .line 62
    iput-object p2, p0, Lcom/onedrive/sdk/authentication/AuthorizationInterceptor;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    return-void
.end method


# virtual methods
.method public intercept(Lcom/onedrive/sdk/http/IHttpRequest;)V
    .locals 4

    .line 71
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/AuthorizationInterceptor;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Intercepting request, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1}, Lcom/onedrive/sdk/http/IHttpRequest;->getRequestUrl()Ljava/net/URL;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 74
    invoke-interface {p1}, Lcom/onedrive/sdk/http/IHttpRequest;->getHeaders()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/onedrive/sdk/options/HeaderOption;

    .line 75
    invoke-virtual {v1}, Lcom/onedrive/sdk/options/HeaderOption;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Authorization"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 76
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/AuthorizationInterceptor;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v0, "Found an existing authorization header!"

    invoke-interface {p1, v0}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    return-void

    .line 81
    :cond_1
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/AuthorizationInterceptor;->mAuthenticator:Lcom/onedrive/sdk/authentication/IAuthenticator;

    invoke-interface {v0}, Lcom/onedrive/sdk/authentication/IAuthenticator;->getAccountInfo()Lcom/onedrive/sdk/authentication/IAccountInfo;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 82
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/AuthorizationInterceptor;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Found account information"

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 83
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/AuthorizationInterceptor;->mAuthenticator:Lcom/onedrive/sdk/authentication/IAuthenticator;

    invoke-interface {v0}, Lcom/onedrive/sdk/authentication/IAuthenticator;->getAccountInfo()Lcom/onedrive/sdk/authentication/IAccountInfo;

    move-result-object v0

    invoke-interface {v0}, Lcom/onedrive/sdk/authentication/IAccountInfo;->isExpired()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 84
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/AuthorizationInterceptor;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Account access token is expired, refreshing"

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 85
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/AuthorizationInterceptor;->mAuthenticator:Lcom/onedrive/sdk/authentication/IAuthenticator;

    invoke-interface {v0}, Lcom/onedrive/sdk/authentication/IAuthenticator;->getAccountInfo()Lcom/onedrive/sdk/authentication/IAccountInfo;

    move-result-object v0

    invoke-interface {v0}, Lcom/onedrive/sdk/authentication/IAccountInfo;->refresh()V

    .line 88
    :cond_2
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/AuthorizationInterceptor;->mAuthenticator:Lcom/onedrive/sdk/authentication/IAuthenticator;

    invoke-interface {v0}, Lcom/onedrive/sdk/authentication/IAuthenticator;->getAccountInfo()Lcom/onedrive/sdk/authentication/IAccountInfo;

    move-result-object v0

    invoke-interface {v0}, Lcom/onedrive/sdk/authentication/IAccountInfo;->getAccessToken()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Authorization"

    .line 89
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bearer "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v1, v0}, Lcom/onedrive/sdk/http/IHttpRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 91
    :cond_3
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/AuthorizationInterceptor;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v0, "No active account found, skipping writing auth header"

    invoke-interface {p1, v0}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.class public final enum Lcom/onedrive/sdk/authentication/AccountType;
.super Ljava/lang/Enum;
.source "AccountType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/onedrive/sdk/authentication/AccountType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/onedrive/sdk/authentication/AccountType;

.field public static final enum ActiveDirectory:Lcom/onedrive/sdk/authentication/AccountType;

.field public static final enum MicrosoftAccount:Lcom/onedrive/sdk/authentication/AccountType;


# instance fields
.field private final mRepresentations:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 33
    new-instance v0, Lcom/onedrive/sdk/authentication/AccountType;

    const-string v1, "MicrosoftAccount"

    const-string v2, "MSA"

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3, v2}, Lcom/onedrive/sdk/authentication/AccountType;-><init>(Ljava/lang/String;I[Ljava/lang/String;)V

    sput-object v0, Lcom/onedrive/sdk/authentication/AccountType;->MicrosoftAccount:Lcom/onedrive/sdk/authentication/AccountType;

    .line 38
    new-instance v0, Lcom/onedrive/sdk/authentication/AccountType;

    const-string v1, "ActiveDirectory"

    const-string v2, "AAD"

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x1

    invoke-direct {v0, v1, v4, v2}, Lcom/onedrive/sdk/authentication/AccountType;-><init>(Ljava/lang/String;I[Ljava/lang/String;)V

    sput-object v0, Lcom/onedrive/sdk/authentication/AccountType;->ActiveDirectory:Lcom/onedrive/sdk/authentication/AccountType;

    const/4 v0, 0x2

    .line 28
    new-array v0, v0, [Lcom/onedrive/sdk/authentication/AccountType;

    sget-object v1, Lcom/onedrive/sdk/authentication/AccountType;->MicrosoftAccount:Lcom/onedrive/sdk/authentication/AccountType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/onedrive/sdk/authentication/AccountType;->ActiveDirectory:Lcom/onedrive/sdk/authentication/AccountType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/onedrive/sdk/authentication/AccountType;->$VALUES:[Lcom/onedrive/sdk/authentication/AccountType;

    return-void
.end method

.method private varargs constructor <init>(Ljava/lang/String;I[Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 49
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 50
    iput-object p3, p0, Lcom/onedrive/sdk/authentication/AccountType;->mRepresentations:[Ljava/lang/String;

    return-void
.end method

.method public static fromRepresentation(Ljava/lang/String;)Lcom/onedrive/sdk/authentication/AccountType;
    .locals 9

    .line 59
    invoke-static {}, Lcom/onedrive/sdk/authentication/AccountType;->values()[Lcom/onedrive/sdk/authentication/AccountType;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_2

    aget-object v4, v0, v3

    .line 60
    iget-object v5, v4, Lcom/onedrive/sdk/authentication/AccountType;->mRepresentations:[Ljava/lang/String;

    array-length v6, v5

    const/4 v7, 0x0

    :goto_1
    if-ge v7, v6, :cond_1

    aget-object v8, v5, v7

    .line 61
    invoke-virtual {v8, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    return-object v4

    :cond_0
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    .line 67
    new-array v0, v0, [Ljava/lang/Object;

    aput-object p0, v0, v2

    const-string p0, "Unable to find a representation for \'%s"

    invoke-static {p0, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    .line 68
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0, p0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/onedrive/sdk/authentication/AccountType;
    .locals 1

    .line 28
    const-class v0, Lcom/onedrive/sdk/authentication/AccountType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/onedrive/sdk/authentication/AccountType;

    return-object p0
.end method

.method public static values()[Lcom/onedrive/sdk/authentication/AccountType;
    .locals 1

    .line 28
    sget-object v0, Lcom/onedrive/sdk/authentication/AccountType;->$VALUES:[Lcom/onedrive/sdk/authentication/AccountType;

    invoke-virtual {v0}, [Lcom/onedrive/sdk/authentication/AccountType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/onedrive/sdk/authentication/AccountType;

    return-object v0
.end method

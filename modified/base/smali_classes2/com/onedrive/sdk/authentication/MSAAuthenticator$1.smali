.class Lcom/onedrive/sdk/authentication/MSAAuthenticator$1;
.super Ljava/lang/Object;
.source "MSAAuthenticator.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/onedrive/sdk/authentication/MSAAuthenticator;->login(Ljava/lang/String;Lcom/onedrive/sdk/concurrency/ICallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/onedrive/sdk/authentication/MSAAuthenticator;

.field final synthetic val$emailAddressHint:Ljava/lang/String;

.field final synthetic val$loginCallback:Lcom/onedrive/sdk/concurrency/ICallback;


# direct methods
.method constructor <init>(Lcom/onedrive/sdk/authentication/MSAAuthenticator;Ljava/lang/String;Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 0

    .line 166
    iput-object p1, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator$1;->this$0:Lcom/onedrive/sdk/authentication/MSAAuthenticator;

    iput-object p2, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator$1;->val$emailAddressHint:Ljava/lang/String;

    iput-object p3, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator$1;->val$loginCallback:Lcom/onedrive/sdk/concurrency/ICallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 170
    :try_start_0
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator$1;->this$0:Lcom/onedrive/sdk/authentication/MSAAuthenticator;

    invoke-static {v0}, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->access$000(Lcom/onedrive/sdk/authentication/MSAAuthenticator;)Lcom/onedrive/sdk/concurrency/IExecutors;

    move-result-object v0

    iget-object v1, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator$1;->this$0:Lcom/onedrive/sdk/authentication/MSAAuthenticator;

    iget-object v2, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator$1;->val$emailAddressHint:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->login(Ljava/lang/String;)Lcom/onedrive/sdk/authentication/IAccountInfo;

    move-result-object v1

    iget-object v2, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator$1;->val$loginCallback:Lcom/onedrive/sdk/concurrency/ICallback;

    invoke-interface {v0, v1, v2}, Lcom/onedrive/sdk/concurrency/IExecutors;->performOnForeground(Ljava/lang/Object;Lcom/onedrive/sdk/concurrency/ICallback;)V
    :try_end_0
    .catch Lcom/onedrive/sdk/core/ClientException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 172
    iget-object v1, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator$1;->this$0:Lcom/onedrive/sdk/authentication/MSAAuthenticator;

    invoke-static {v1}, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->access$000(Lcom/onedrive/sdk/authentication/MSAAuthenticator;)Lcom/onedrive/sdk/concurrency/IExecutors;

    move-result-object v1

    iget-object v2, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator$1;->val$loginCallback:Lcom/onedrive/sdk/concurrency/ICallback;

    invoke-interface {v1, v0, v2}, Lcom/onedrive/sdk/concurrency/IExecutors;->performOnForeground(Lcom/onedrive/sdk/core/ClientException;Lcom/onedrive/sdk/concurrency/ICallback;)V

    :goto_0
    return-void
.end method

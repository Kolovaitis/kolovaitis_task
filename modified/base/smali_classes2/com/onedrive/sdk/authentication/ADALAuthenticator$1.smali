.class Lcom/onedrive/sdk/authentication/ADALAuthenticator$1;
.super Ljava/lang/Object;
.source "ADALAuthenticator.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/onedrive/sdk/authentication/ADALAuthenticator;->login(Ljava/lang/String;Lcom/onedrive/sdk/concurrency/ICallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/onedrive/sdk/authentication/ADALAuthenticator;

.field final synthetic val$emailAddressHint:Ljava/lang/String;

.field final synthetic val$loginCallback:Lcom/onedrive/sdk/concurrency/ICallback;


# direct methods
.method constructor <init>(Lcom/onedrive/sdk/authentication/ADALAuthenticator;Lcom/onedrive/sdk/concurrency/ICallback;Ljava/lang/String;)V
    .locals 0

    .line 263
    iput-object p1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator$1;->this$0:Lcom/onedrive/sdk/authentication/ADALAuthenticator;

    iput-object p2, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator$1;->val$loginCallback:Lcom/onedrive/sdk/concurrency/ICallback;

    iput-object p3, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator$1;->val$emailAddressHint:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 267
    :try_start_0
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator$1;->val$loginCallback:Lcom/onedrive/sdk/concurrency/ICallback;

    iget-object v1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator$1;->this$0:Lcom/onedrive/sdk/authentication/ADALAuthenticator;

    iget-object v2, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator$1;->val$emailAddressHint:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->login(Ljava/lang/String;)Lcom/onedrive/sdk/authentication/IAccountInfo;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/concurrency/ICallback;->success(Ljava/lang/Object;)V
    :try_end_0
    .catch Lcom/onedrive/sdk/core/ClientException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 269
    iget-object v1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator$1;->val$loginCallback:Lcom/onedrive/sdk/concurrency/ICallback;

    invoke-interface {v1, v0}, Lcom/onedrive/sdk/concurrency/ICallback;->failure(Lcom/onedrive/sdk/core/ClientException;)V

    :goto_0
    return-void
.end method

.class Lcom/onedrive/sdk/authentication/DisambiguationRequest;
.super Ljava/lang/Object;
.source "DisambiguationRequest.java"


# instance fields
.field private final mActivity:Landroid/app/Activity;

.field private final mCallback:Lcom/onedrive/sdk/concurrency/ICallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/authentication/DisambiguationResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final mLogger:Lcom/onedrive/sdk/logger/ILogger;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/onedrive/sdk/concurrency/ICallback;Lcom/onedrive/sdk/logger/ILogger;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/authentication/DisambiguationResponse;",
            ">;",
            "Lcom/onedrive/sdk/logger/ILogger;",
            ")V"
        }
    .end annotation

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p1, p0, Lcom/onedrive/sdk/authentication/DisambiguationRequest;->mActivity:Landroid/app/Activity;

    .line 60
    iput-object p2, p0, Lcom/onedrive/sdk/authentication/DisambiguationRequest;->mCallback:Lcom/onedrive/sdk/concurrency/ICallback;

    .line 61
    iput-object p3, p0, Lcom/onedrive/sdk/authentication/DisambiguationRequest;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    return-void
.end method

.method static synthetic access$000(Lcom/onedrive/sdk/authentication/DisambiguationRequest;)Landroid/app/Activity;
    .locals 0

    .line 33
    iget-object p0, p0, Lcom/onedrive/sdk/authentication/DisambiguationRequest;->mActivity:Landroid/app/Activity;

    return-object p0
.end method


# virtual methods
.method public execute()V
    .locals 2

    .line 68
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationRequest;->mActivity:Landroid/app/Activity;

    new-instance v1, Lcom/onedrive/sdk/authentication/DisambiguationRequest$1;

    invoke-direct {v1, p0}, Lcom/onedrive/sdk/authentication/DisambiguationRequest$1;-><init>(Lcom/onedrive/sdk/authentication/DisambiguationRequest;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public getCallback()Lcom/onedrive/sdk/concurrency/ICallback;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/authentication/DisambiguationResponse;",
            ">;"
        }
    .end annotation

    .line 81
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationRequest;->mCallback:Lcom/onedrive/sdk/concurrency/ICallback;

    return-object v0
.end method

.method public getLogger()Lcom/onedrive/sdk/logger/ILogger;
    .locals 1

    .line 89
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationRequest;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    return-object v0
.end method

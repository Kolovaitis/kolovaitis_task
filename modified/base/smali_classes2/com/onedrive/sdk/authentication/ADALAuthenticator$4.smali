.class Lcom/onedrive/sdk/authentication/ADALAuthenticator$4;
.super Ljava/lang/Object;
.source "ADALAuthenticator.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/onedrive/sdk/authentication/ADALAuthenticator;->logout(Lcom/onedrive/sdk/concurrency/ICallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/onedrive/sdk/authentication/ADALAuthenticator;

.field final synthetic val$logoutCallback:Lcom/onedrive/sdk/concurrency/ICallback;


# direct methods
.method constructor <init>(Lcom/onedrive/sdk/authentication/ADALAuthenticator;Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 0

    .line 460
    iput-object p1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator$4;->this$0:Lcom/onedrive/sdk/authentication/ADALAuthenticator;

    iput-object p2, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator$4;->val$logoutCallback:Lcom/onedrive/sdk/concurrency/ICallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    const/4 v0, 0x0

    .line 464
    :try_start_0
    iget-object v1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator$4;->this$0:Lcom/onedrive/sdk/authentication/ADALAuthenticator;

    invoke-virtual {v1}, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->logout()V

    .line 465
    iget-object v1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator$4;->this$0:Lcom/onedrive/sdk/authentication/ADALAuthenticator;

    invoke-static {v1}, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->access$000(Lcom/onedrive/sdk/authentication/ADALAuthenticator;)Lcom/onedrive/sdk/concurrency/IExecutors;

    move-result-object v1

    move-object v2, v0

    check-cast v2, Ljava/lang/Void;

    iget-object v3, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator$4;->val$logoutCallback:Lcom/onedrive/sdk/concurrency/ICallback;

    invoke-interface {v1, v2, v3}, Lcom/onedrive/sdk/concurrency/IExecutors;->performOnForeground(Ljava/lang/Object;Lcom/onedrive/sdk/concurrency/ICallback;)V
    :try_end_0
    .catch Lcom/onedrive/sdk/core/ClientException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 467
    :catch_0
    iget-object v1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator$4;->this$0:Lcom/onedrive/sdk/authentication/ADALAuthenticator;

    invoke-static {v1}, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->access$000(Lcom/onedrive/sdk/authentication/ADALAuthenticator;)Lcom/onedrive/sdk/concurrency/IExecutors;

    move-result-object v1

    check-cast v0, Ljava/lang/Void;

    iget-object v2, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator$4;->val$logoutCallback:Lcom/onedrive/sdk/concurrency/ICallback;

    invoke-interface {v1, v0, v2}, Lcom/onedrive/sdk/concurrency/IExecutors;->performOnForeground(Ljava/lang/Object;Lcom/onedrive/sdk/concurrency/ICallback;)V

    :goto_0
    return-void
.end method

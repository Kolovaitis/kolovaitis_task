.class final Lcom/onedrive/sdk/serializer/GsonFactory;
.super Ljava/lang/Object;
.source "GsonFactory.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getGsonInstance(Lcom/onedrive/sdk/logger/ILogger;)Lcom/google/gson/Gson;
    .locals 3

    .line 59
    new-instance v0, Lcom/onedrive/sdk/serializer/GsonFactory$1;

    invoke-direct {v0, p0}, Lcom/onedrive/sdk/serializer/GsonFactory$1;-><init>(Lcom/onedrive/sdk/logger/ILogger;)V

    .line 76
    new-instance v1, Lcom/onedrive/sdk/serializer/GsonFactory$2;

    invoke-direct {v1, p0}, Lcom/onedrive/sdk/serializer/GsonFactory$2;-><init>(Lcom/onedrive/sdk/logger/ILogger;)V

    .line 93
    new-instance p0, Lcom/google/gson/GsonBuilder;

    invoke-direct {p0}, Lcom/google/gson/GsonBuilder;-><init>()V

    const-class v2, Ljava/util/Calendar;

    invoke-virtual {p0, v2, v0}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object p0

    const-class v0, Ljava/util/Calendar;

    invoke-virtual {p0, v0, v1}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object p0

    invoke-virtual {p0}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object p0

    return-object p0
.end method

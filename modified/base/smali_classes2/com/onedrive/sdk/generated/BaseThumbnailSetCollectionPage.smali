.class public Lcom/onedrive/sdk/generated/BaseThumbnailSetCollectionPage;
.super Lcom/onedrive/sdk/http/BaseCollectionPage;
.source "BaseThumbnailSetCollectionPage.java"

# interfaces
.implements Lcom/onedrive/sdk/generated/IBaseThumbnailSetCollectionPage;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/onedrive/sdk/http/BaseCollectionPage<",
        "Lcom/onedrive/sdk/extensions/ThumbnailSet;",
        "Lcom/onedrive/sdk/extensions/IThumbnailSetCollectionRequestBuilder;",
        ">;",
        "Lcom/onedrive/sdk/generated/IBaseThumbnailSetCollectionPage;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/onedrive/sdk/generated/BaseThumbnailSetCollectionResponse;Lcom/onedrive/sdk/extensions/IThumbnailSetCollectionRequestBuilder;)V
    .locals 0

    .line 53
    iget-object p1, p1, Lcom/onedrive/sdk/generated/BaseThumbnailSetCollectionResponse;->value:Ljava/util/List;

    invoke-direct {p0, p1, p2}, Lcom/onedrive/sdk/http/BaseCollectionPage;-><init>(Ljava/util/List;Lcom/onedrive/sdk/http/IRequestBuilder;)V

    return-void
.end method

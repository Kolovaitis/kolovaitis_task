.class public Lcom/onedrive/sdk/generated/BaseItemStreamRequest;
.super Lcom/onedrive/sdk/http/BaseStreamRequest;
.source "BaseItemStreamRequest.java"

# interfaces
.implements Lcom/onedrive/sdk/generated/IBaseItemStreamRequest;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/onedrive/sdk/http/BaseStreamRequest<",
        "Lcom/onedrive/sdk/extensions/Item;",
        ">;",
        "Lcom/onedrive/sdk/generated/IBaseItemStreamRequest;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/onedrive/sdk/extensions/IOneDriveClient;",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;)V"
        }
    .end annotation

    .line 52
    const-class v0, Lcom/onedrive/sdk/extensions/Item;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/onedrive/sdk/http/BaseStreamRequest;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public get()Ljava/io/InputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    .line 60
    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseItemStreamRequest;->send()Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public get(Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Ljava/io/InputStream;",
            ">;)V"
        }
    .end annotation

    .line 56
    invoke-virtual {p0, p1}, Lcom/onedrive/sdk/generated/BaseItemStreamRequest;->send(Lcom/onedrive/sdk/concurrency/ICallback;)V

    return-void
.end method

.method public put([B)Lcom/onedrive/sdk/extensions/Item;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    .line 68
    invoke-virtual {p0, p1}, Lcom/onedrive/sdk/generated/BaseItemStreamRequest;->send([B)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/onedrive/sdk/extensions/Item;

    return-object p1
.end method

.method public put([BLcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/extensions/Item;",
            ">;)V"
        }
    .end annotation

    .line 64
    invoke-virtual {p0, p1, p2}, Lcom/onedrive/sdk/generated/BaseItemStreamRequest;->send([BLcom/onedrive/sdk/concurrency/ICallback;)V

    return-void
.end method

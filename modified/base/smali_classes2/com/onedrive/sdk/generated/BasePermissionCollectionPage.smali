.class public Lcom/onedrive/sdk/generated/BasePermissionCollectionPage;
.super Lcom/onedrive/sdk/http/BaseCollectionPage;
.source "BasePermissionCollectionPage.java"

# interfaces
.implements Lcom/onedrive/sdk/generated/IBasePermissionCollectionPage;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/onedrive/sdk/http/BaseCollectionPage<",
        "Lcom/onedrive/sdk/extensions/Permission;",
        "Lcom/onedrive/sdk/extensions/IPermissionCollectionRequestBuilder;",
        ">;",
        "Lcom/onedrive/sdk/generated/IBasePermissionCollectionPage;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/onedrive/sdk/generated/BasePermissionCollectionResponse;Lcom/onedrive/sdk/extensions/IPermissionCollectionRequestBuilder;)V
    .locals 0

    .line 53
    iget-object p1, p1, Lcom/onedrive/sdk/generated/BasePermissionCollectionResponse;->value:Ljava/util/List;

    invoke-direct {p0, p1, p2}, Lcom/onedrive/sdk/http/BaseCollectionPage;-><init>(Ljava/util/List;Lcom/onedrive/sdk/http/IRequestBuilder;)V

    return-void
.end method

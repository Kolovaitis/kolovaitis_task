.class public interface abstract Lcom/onedrive/sdk/generated/IBaseDriveRequestBuilder;
.super Ljava/lang/Object;
.source "IBaseDriveRequestBuilder.java"

# interfaces
.implements Lcom/onedrive/sdk/http/IRequestBuilder;


# virtual methods
.method public abstract buildRequest()Lcom/onedrive/sdk/extensions/IDriveRequest;
.end method

.method public abstract buildRequest(Ljava/util/List;)Lcom/onedrive/sdk/extensions/IDriveRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;)",
            "Lcom/onedrive/sdk/extensions/IDriveRequest;"
        }
    .end annotation
.end method

.method public abstract getItems(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IItemRequestBuilder;
.end method

.method public abstract getRecent()Lcom/onedrive/sdk/extensions/IRecentRequestBuilder;
.end method

.method public abstract getShared(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IItemRequestBuilder;
.end method

.method public abstract getSpecial(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IItemRequestBuilder;
.end method

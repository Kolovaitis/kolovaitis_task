.class public Lcom/onedrive/sdk/generated/BaseCreateSessionRequest;
.super Lcom/onedrive/sdk/http/BaseRequest;
.source "BaseCreateSessionRequest.java"

# interfaces
.implements Lcom/onedrive/sdk/generated/IBaseCreateSessionRequest;


# instance fields
.field protected final mBody:Lcom/onedrive/sdk/extensions/CreateSessionBody;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;Lcom/onedrive/sdk/extensions/ChunkedUploadSessionDescriptor;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/onedrive/sdk/extensions/IOneDriveClient;",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;",
            "Lcom/onedrive/sdk/extensions/ChunkedUploadSessionDescriptor;",
            ")V"
        }
    .end annotation

    .line 51
    const-class v0, Lcom/onedrive/sdk/extensions/UploadSession;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/onedrive/sdk/http/BaseRequest;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;Ljava/lang/Class;)V

    .line 52
    new-instance p1, Lcom/onedrive/sdk/extensions/CreateSessionBody;

    invoke-direct {p1}, Lcom/onedrive/sdk/extensions/CreateSessionBody;-><init>()V

    iput-object p1, p0, Lcom/onedrive/sdk/generated/BaseCreateSessionRequest;->mBody:Lcom/onedrive/sdk/extensions/CreateSessionBody;

    .line 53
    iget-object p1, p0, Lcom/onedrive/sdk/generated/BaseCreateSessionRequest;->mBody:Lcom/onedrive/sdk/extensions/CreateSessionBody;

    iput-object p4, p1, Lcom/onedrive/sdk/extensions/CreateSessionBody;->item:Lcom/onedrive/sdk/extensions/ChunkedUploadSessionDescriptor;

    return-void
.end method


# virtual methods
.method public create()Lcom/onedrive/sdk/extensions/UploadSession;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 67
    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseCreateSessionRequest;->post()Lcom/onedrive/sdk/extensions/UploadSession;

    move-result-object v0

    return-object v0
.end method

.method public create(Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/extensions/UploadSession;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 60
    invoke-virtual {p0, p1}, Lcom/onedrive/sdk/generated/BaseCreateSessionRequest;->post(Lcom/onedrive/sdk/concurrency/ICallback;)V

    return-void
.end method

.method public expand(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/ICreateSessionRequest;
    .locals 3

    .line 89
    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseCreateSessionRequest;->getQueryOptions()Ljava/util/List;

    move-result-object v0

    new-instance v1, Lcom/onedrive/sdk/options/QueryOption;

    const-string v2, "expand"

    invoke-direct {v1, v2, p1}, Lcom/onedrive/sdk/options/QueryOption;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    move-object p1, p0

    check-cast p1, Lcom/onedrive/sdk/extensions/CreateSessionRequest;

    return-object p1
.end method

.method public post()Lcom/onedrive/sdk/extensions/UploadSession;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    .line 75
    sget-object v0, Lcom/onedrive/sdk/http/HttpMethod;->POST:Lcom/onedrive/sdk/http/HttpMethod;

    iget-object v1, p0, Lcom/onedrive/sdk/generated/BaseCreateSessionRequest;->mBody:Lcom/onedrive/sdk/extensions/CreateSessionBody;

    invoke-virtual {p0, v0, v1}, Lcom/onedrive/sdk/generated/BaseCreateSessionRequest;->send(Lcom/onedrive/sdk/http/HttpMethod;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/onedrive/sdk/extensions/UploadSession;

    return-object v0
.end method

.method public post(Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/extensions/UploadSession;",
            ">;)V"
        }
    .end annotation

    .line 71
    sget-object v0, Lcom/onedrive/sdk/http/HttpMethod;->POST:Lcom/onedrive/sdk/http/HttpMethod;

    iget-object v1, p0, Lcom/onedrive/sdk/generated/BaseCreateSessionRequest;->mBody:Lcom/onedrive/sdk/extensions/CreateSessionBody;

    invoke-virtual {p0, v0, p1, v1}, Lcom/onedrive/sdk/generated/BaseCreateSessionRequest;->send(Lcom/onedrive/sdk/http/HttpMethod;Lcom/onedrive/sdk/concurrency/ICallback;Ljava/lang/Object;)V

    return-void
.end method

.method public select(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/ICreateSessionRequest;
    .locals 3

    .line 79
    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseCreateSessionRequest;->getQueryOptions()Ljava/util/List;

    move-result-object v0

    new-instance v1, Lcom/onedrive/sdk/options/QueryOption;

    const-string v2, "select"

    invoke-direct {v1, v2, p1}, Lcom/onedrive/sdk/options/QueryOption;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 80
    move-object p1, p0

    check-cast p1, Lcom/onedrive/sdk/extensions/CreateSessionRequest;

    return-object p1
.end method

.method public top(I)Lcom/onedrive/sdk/extensions/ICreateSessionRequest;
    .locals 4

    .line 84
    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseCreateSessionRequest;->getQueryOptions()Ljava/util/List;

    move-result-object v0

    new-instance v1, Lcom/onedrive/sdk/options/QueryOption;

    const-string v2, "top"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ""

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, v2, p1}, Lcom/onedrive/sdk/options/QueryOption;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 85
    move-object p1, p0

    check-cast p1, Lcom/onedrive/sdk/extensions/CreateSessionRequest;

    return-object p1
.end method

.class public Lcom/onedrive/sdk/generated/BaseSearchCollectionPage;
.super Lcom/onedrive/sdk/http/BaseCollectionPage;
.source "BaseSearchCollectionPage.java"

# interfaces
.implements Lcom/onedrive/sdk/generated/IBaseSearchCollectionPage;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/onedrive/sdk/http/BaseCollectionPage<",
        "Lcom/onedrive/sdk/extensions/Item;",
        "Lcom/onedrive/sdk/extensions/ISearchRequestBuilder;",
        ">;",
        "Lcom/onedrive/sdk/generated/IBaseSearchCollectionPage;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/onedrive/sdk/generated/BaseSearchCollectionResponse;Lcom/onedrive/sdk/extensions/ISearchRequestBuilder;)V
    .locals 0

    .line 52
    iget-object p1, p1, Lcom/onedrive/sdk/generated/BaseSearchCollectionResponse;->value:Ljava/util/List;

    invoke-direct {p0, p1, p2}, Lcom/onedrive/sdk/http/BaseCollectionPage;-><init>(Ljava/util/List;Lcom/onedrive/sdk/http/IRequestBuilder;)V

    return-void
.end method

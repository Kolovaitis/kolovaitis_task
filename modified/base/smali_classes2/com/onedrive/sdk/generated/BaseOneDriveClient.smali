.class public Lcom/onedrive/sdk/generated/BaseOneDriveClient;
.super Lcom/onedrive/sdk/core/BaseClient;
.source "BaseOneDriveClient.java"

# interfaces
.implements Lcom/onedrive/sdk/generated/IBaseOneDriveClient;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 40
    invoke-direct {p0}, Lcom/onedrive/sdk/core/BaseClient;-><init>()V

    return-void
.end method


# virtual methods
.method public getDrive(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IDriveRequestBuilder;
    .locals 3

    .line 58
    new-instance v0, Lcom/onedrive/sdk/extensions/DriveRequestBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseOneDriveClient;->getServiceRoot()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "/drives/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    move-object v1, p0

    check-cast v1, Lcom/onedrive/sdk/extensions/IOneDriveClient;

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, v2}, Lcom/onedrive/sdk/extensions/DriveRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V

    return-object v0
.end method

.method public getDrives()Lcom/onedrive/sdk/extensions/IDriveCollectionRequestBuilder;
    .locals 4

    .line 48
    new-instance v0, Lcom/onedrive/sdk/extensions/DriveCollectionRequestBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseOneDriveClient;->getServiceRoot()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "/drives"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v2, p0

    check-cast v2, Lcom/onedrive/sdk/extensions/IOneDriveClient;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/onedrive/sdk/extensions/DriveCollectionRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V

    return-object v0
.end method

.method public getShare(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IShareRequestBuilder;
    .locals 3

    .line 77
    new-instance v0, Lcom/onedrive/sdk/extensions/ShareRequestBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseOneDriveClient;->getServiceRoot()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "/shares/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    move-object v1, p0

    check-cast v1, Lcom/onedrive/sdk/extensions/IOneDriveClient;

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, v2}, Lcom/onedrive/sdk/extensions/ShareRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V

    return-object v0
.end method

.method public getShares()Lcom/onedrive/sdk/extensions/IShareCollectionRequestBuilder;
    .locals 4

    .line 67
    new-instance v0, Lcom/onedrive/sdk/extensions/ShareCollectionRequestBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseOneDriveClient;->getServiceRoot()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "/shares"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v2, p0

    check-cast v2, Lcom/onedrive/sdk/extensions/IOneDriveClient;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/onedrive/sdk/extensions/ShareCollectionRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V

    return-object v0
.end method

.class public Lcom/onedrive/sdk/generated/BaseDeltaResponse;
.super Ljava/lang/Object;
.source "BaseDeltaResponse.java"

# interfaces
.implements Lcom/onedrive/sdk/serializer/IJsonBackedObject;


# instance fields
.field public delta_token:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "@delta.token"
    .end annotation
.end field

.field private transient mRawObject:Lcom/google/gson/JsonObject;

.field private transient mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;

.field public odata_deltaLink:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "@odata.deltaLink"
    .end annotation
.end field

.field public transient value:Lcom/onedrive/sdk/extensions/ItemCollectionPage;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getRawObject()Lcom/google/gson/JsonObject;
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/onedrive/sdk/generated/BaseDeltaResponse;->mRawObject:Lcom/google/gson/JsonObject;

    return-object v0
.end method

.method protected getSerializer()Lcom/onedrive/sdk/serializer/ISerializer;
    .locals 1

    .line 86
    iget-object v0, p0, Lcom/onedrive/sdk/generated/BaseDeltaResponse;->mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;

    return-object v0
.end method

.method public setRawObject(Lcom/onedrive/sdk/serializer/ISerializer;Lcom/google/gson/JsonObject;)V
    .locals 0

    .line 96
    iput-object p1, p0, Lcom/onedrive/sdk/generated/BaseDeltaResponse;->mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;

    .line 97
    iput-object p2, p0, Lcom/onedrive/sdk/generated/BaseDeltaResponse;->mRawObject:Lcom/google/gson/JsonObject;

    return-void
.end method

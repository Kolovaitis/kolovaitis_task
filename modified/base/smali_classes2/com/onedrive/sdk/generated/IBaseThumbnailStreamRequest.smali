.class public interface abstract Lcom/onedrive/sdk/generated/IBaseThumbnailStreamRequest;
.super Ljava/lang/Object;
.source "IBaseThumbnailStreamRequest.java"

# interfaces
.implements Lcom/onedrive/sdk/http/IHttpStreamRequest;


# virtual methods
.method public abstract get()Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation
.end method

.method public abstract get(Lcom/onedrive/sdk/concurrency/ICallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Ljava/io/InputStream;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract put([B)Lcom/onedrive/sdk/extensions/Thumbnail;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation
.end method

.method public abstract put([BLcom/onedrive/sdk/concurrency/ICallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/extensions/Thumbnail;",
            ">;)V"
        }
    .end annotation
.end method

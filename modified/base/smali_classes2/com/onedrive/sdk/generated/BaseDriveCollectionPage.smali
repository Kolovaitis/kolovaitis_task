.class public Lcom/onedrive/sdk/generated/BaseDriveCollectionPage;
.super Lcom/onedrive/sdk/http/BaseCollectionPage;
.source "BaseDriveCollectionPage.java"

# interfaces
.implements Lcom/onedrive/sdk/generated/IBaseDriveCollectionPage;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/onedrive/sdk/http/BaseCollectionPage<",
        "Lcom/onedrive/sdk/extensions/Drive;",
        "Lcom/onedrive/sdk/extensions/IDriveCollectionRequestBuilder;",
        ">;",
        "Lcom/onedrive/sdk/generated/IBaseDriveCollectionPage;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/onedrive/sdk/generated/BaseDriveCollectionResponse;Lcom/onedrive/sdk/extensions/IDriveCollectionRequestBuilder;)V
    .locals 0

    .line 53
    iget-object p1, p1, Lcom/onedrive/sdk/generated/BaseDriveCollectionResponse;->value:Ljava/util/List;

    invoke-direct {p0, p1, p2}, Lcom/onedrive/sdk/http/BaseCollectionPage;-><init>(Ljava/util/List;Lcom/onedrive/sdk/http/IRequestBuilder;)V

    return-void
.end method

.class public Lcom/onedrive/sdk/generated/BaseCopyRequestBuilder;
.super Lcom/onedrive/sdk/http/BaseRequestBuilder;
.source "BaseCopyRequestBuilder.java"


# instance fields
.field public final mName:Ljava/lang/String;

.field public final mParentReference:Lcom/onedrive/sdk/extensions/ItemReference;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;Ljava/lang/String;Lcom/onedrive/sdk/extensions/ItemReference;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/onedrive/sdk/extensions/IOneDriveClient;",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/onedrive/sdk/extensions/ItemReference;",
            ")V"
        }
    .end annotation

    .line 53
    invoke-direct {p0, p1, p2, p3}, Lcom/onedrive/sdk/http/BaseRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V

    .line 54
    iput-object p4, p0, Lcom/onedrive/sdk/generated/BaseCopyRequestBuilder;->mName:Ljava/lang/String;

    .line 55
    iput-object p5, p0, Lcom/onedrive/sdk/generated/BaseCopyRequestBuilder;->mParentReference:Lcom/onedrive/sdk/extensions/ItemReference;

    return-void
.end method


# virtual methods
.method public buildRequest()Lcom/onedrive/sdk/extensions/ICopyRequest;
    .locals 1

    .line 59
    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseCopyRequestBuilder;->getOptions()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/onedrive/sdk/generated/BaseCopyRequestBuilder;->buildRequest(Ljava/util/List;)Lcom/onedrive/sdk/extensions/ICopyRequest;

    move-result-object v0

    return-object v0
.end method

.method public buildRequest(Ljava/util/List;)Lcom/onedrive/sdk/extensions/ICopyRequest;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;)",
            "Lcom/onedrive/sdk/extensions/ICopyRequest;"
        }
    .end annotation

    .line 63
    new-instance v6, Lcom/onedrive/sdk/extensions/CopyRequest;

    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseCopyRequestBuilder;->getRequestUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseCopyRequestBuilder;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v2

    iget-object v4, p0, Lcom/onedrive/sdk/generated/BaseCopyRequestBuilder;->mName:Ljava/lang/String;

    iget-object v5, p0, Lcom/onedrive/sdk/generated/BaseCopyRequestBuilder;->mParentReference:Lcom/onedrive/sdk/extensions/ItemReference;

    move-object v0, v6

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/onedrive/sdk/extensions/CopyRequest;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;Ljava/lang/String;Lcom/onedrive/sdk/extensions/ItemReference;)V

    return-object v6
.end method

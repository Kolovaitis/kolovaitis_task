.class public interface abstract Lcom/onedrive/sdk/generated/IBaseItemCollectionRequest;
.super Ljava/lang/Object;
.source "IBaseItemCollectionRequest.java"


# virtual methods
.method public abstract create(Lcom/onedrive/sdk/extensions/Item;)Lcom/onedrive/sdk/extensions/Item;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract create(Lcom/onedrive/sdk/extensions/Item;Lcom/onedrive/sdk/concurrency/ICallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/extensions/Item;",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/extensions/Item;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract expand(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IItemCollectionRequest;
.end method

.method public abstract get()Lcom/onedrive/sdk/extensions/IItemCollectionPage;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation
.end method

.method public abstract get(Lcom/onedrive/sdk/concurrency/ICallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/extensions/IItemCollectionPage;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract post(Lcom/onedrive/sdk/extensions/Item;)Lcom/onedrive/sdk/extensions/Item;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation
.end method

.method public abstract post(Lcom/onedrive/sdk/extensions/Item;Lcom/onedrive/sdk/concurrency/ICallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/extensions/Item;",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/extensions/Item;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract select(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IItemCollectionRequest;
.end method

.method public abstract top(I)Lcom/onedrive/sdk/extensions/IItemCollectionRequest;
.end method

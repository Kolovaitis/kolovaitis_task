.class public Lcom/onedrive/sdk/generated/BaseStringCollectionPage;
.super Lcom/onedrive/sdk/http/BaseCollectionPage;
.source "BaseStringCollectionPage.java"

# interfaces
.implements Lcom/onedrive/sdk/generated/IBaseStringCollectionPage;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/onedrive/sdk/http/BaseCollectionPage<",
        "Ljava/lang/String;",
        "Lcom/onedrive/sdk/extensions/IStringCollectionRequestBuilder;",
        ">;",
        "Lcom/onedrive/sdk/generated/IBaseStringCollectionPage;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/onedrive/sdk/generated/BaseStringCollectionResponse;Lcom/onedrive/sdk/extensions/IStringCollectionRequestBuilder;)V
    .locals 0

    .line 53
    iget-object p1, p1, Lcom/onedrive/sdk/generated/BaseStringCollectionResponse;->value:Ljava/util/List;

    invoke-direct {p0, p1, p2}, Lcom/onedrive/sdk/http/BaseCollectionPage;-><init>(Ljava/util/List;Lcom/onedrive/sdk/http/IRequestBuilder;)V

    return-void
.end method

.class public Lcom/onedrive/sdk/generated/BaseSearchRequestBuilder;
.super Lcom/onedrive/sdk/http/BaseRequestBuilder;
.source "BaseSearchRequestBuilder.java"


# instance fields
.field public final mQ:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/onedrive/sdk/extensions/IOneDriveClient;",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 52
    invoke-direct {p0, p1, p2, p3}, Lcom/onedrive/sdk/http/BaseRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V

    .line 53
    iput-object p4, p0, Lcom/onedrive/sdk/generated/BaseSearchRequestBuilder;->mQ:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public buildRequest()Lcom/onedrive/sdk/extensions/ISearchRequest;
    .locals 1

    .line 57
    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseSearchRequestBuilder;->getOptions()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/onedrive/sdk/generated/BaseSearchRequestBuilder;->buildRequest(Ljava/util/List;)Lcom/onedrive/sdk/extensions/ISearchRequest;

    move-result-object v0

    return-object v0
.end method

.method public buildRequest(Ljava/util/List;)Lcom/onedrive/sdk/extensions/ISearchRequest;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;)",
            "Lcom/onedrive/sdk/extensions/ISearchRequest;"
        }
    .end annotation

    .line 61
    new-instance v0, Lcom/onedrive/sdk/extensions/SearchRequest;

    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseSearchRequestBuilder;->getRequestUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseSearchRequestBuilder;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v2

    iget-object v3, p0, Lcom/onedrive/sdk/generated/BaseSearchRequestBuilder;->mQ:Ljava/lang/String;

    invoke-direct {v0, v1, v2, p1, v3}, Lcom/onedrive/sdk/extensions/SearchRequest;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;Ljava/lang/String;)V

    return-object v0
.end method

.class public Lcom/onedrive/sdk/generated/BaseItemCollectionRequest;
.super Lcom/onedrive/sdk/http/BaseCollectionRequest;
.source "BaseItemCollectionRequest.java"

# interfaces
.implements Lcom/onedrive/sdk/generated/IBaseItemCollectionRequest;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/onedrive/sdk/http/BaseCollectionRequest<",
        "Lcom/onedrive/sdk/generated/BaseItemCollectionResponse;",
        "Lcom/onedrive/sdk/extensions/IItemCollectionPage;",
        ">;",
        "Lcom/onedrive/sdk/generated/IBaseItemCollectionRequest;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/onedrive/sdk/extensions/IOneDriveClient;",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;)V"
        }
    .end annotation

    .line 51
    const-class v4, Lcom/onedrive/sdk/generated/BaseItemCollectionResponse;

    const-class v5, Lcom/onedrive/sdk/extensions/IItemCollectionPage;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/onedrive/sdk/http/BaseCollectionRequest;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;Ljava/lang/Class;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public buildFromResponse(Lcom/onedrive/sdk/generated/BaseItemCollectionResponse;)Lcom/onedrive/sdk/extensions/IItemCollectionPage;
    .locals 4

    .line 117
    iget-object v0, p1, Lcom/onedrive/sdk/generated/BaseItemCollectionResponse;->nextLink:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 118
    new-instance v0, Lcom/onedrive/sdk/extensions/ItemCollectionRequestBuilder;

    iget-object v2, p1, Lcom/onedrive/sdk/generated/BaseItemCollectionResponse;->nextLink:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseItemCollectionRequest;->getBaseRequest()Lcom/onedrive/sdk/http/BaseRequest;

    move-result-object v3

    invoke-virtual {v3}, Lcom/onedrive/sdk/http/BaseRequest;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v3

    invoke-direct {v0, v2, v3, v1}, Lcom/onedrive/sdk/extensions/ItemCollectionRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V

    goto :goto_0

    :cond_0
    move-object v0, v1

    .line 122
    :goto_0
    new-instance v1, Lcom/onedrive/sdk/extensions/ItemCollectionPage;

    invoke-direct {v1, p1, v0}, Lcom/onedrive/sdk/extensions/ItemCollectionPage;-><init>(Lcom/onedrive/sdk/generated/BaseItemCollectionResponse;Lcom/onedrive/sdk/extensions/IItemCollectionRequestBuilder;)V

    .line 123
    invoke-virtual {p1}, Lcom/onedrive/sdk/generated/BaseItemCollectionResponse;->getSerializer()Lcom/onedrive/sdk/serializer/ISerializer;

    move-result-object v0

    invoke-virtual {p1}, Lcom/onedrive/sdk/generated/BaseItemCollectionResponse;->getRawObject()Lcom/google/gson/JsonObject;

    move-result-object p1

    invoke-virtual {v1, v0, p1}, Lcom/onedrive/sdk/extensions/ItemCollectionPage;->setRawObject(Lcom/onedrive/sdk/serializer/ISerializer;Lcom/google/gson/JsonObject;)V

    return-object v1
.end method

.method public create(Lcom/onedrive/sdk/extensions/Item;)Lcom/onedrive/sdk/extensions/Item;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 84
    invoke-virtual {p0, p1}, Lcom/onedrive/sdk/generated/BaseItemCollectionRequest;->post(Lcom/onedrive/sdk/extensions/Item;)Lcom/onedrive/sdk/extensions/Item;

    move-result-object p1

    return-object p1
.end method

.method public create(Lcom/onedrive/sdk/extensions/Item;Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/extensions/Item;",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/extensions/Item;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 77
    invoke-virtual {p0, p1, p2}, Lcom/onedrive/sdk/generated/BaseItemCollectionRequest;->post(Lcom/onedrive/sdk/extensions/Item;Lcom/onedrive/sdk/concurrency/ICallback;)V

    return-void
.end method

.method public expand(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IItemCollectionRequest;
    .locals 2

    .line 101
    new-instance v0, Lcom/onedrive/sdk/options/QueryOption;

    const-string v1, "expand"

    invoke-direct {v0, v1, p1}, Lcom/onedrive/sdk/options/QueryOption;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/onedrive/sdk/generated/BaseItemCollectionRequest;->addQueryOption(Lcom/onedrive/sdk/options/QueryOption;)V

    .line 102
    move-object p1, p0

    check-cast p1, Lcom/onedrive/sdk/extensions/ItemCollectionRequest;

    return-object p1
.end method

.method public get()Lcom/onedrive/sdk/extensions/IItemCollectionPage;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    .line 69
    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseItemCollectionRequest;->send()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/onedrive/sdk/generated/BaseItemCollectionResponse;

    .line 70
    invoke-virtual {p0, v0}, Lcom/onedrive/sdk/generated/BaseItemCollectionRequest;->buildFromResponse(Lcom/onedrive/sdk/generated/BaseItemCollectionResponse;)Lcom/onedrive/sdk/extensions/IItemCollectionPage;

    move-result-object v0

    return-object v0
.end method

.method public get(Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/extensions/IItemCollectionPage;",
            ">;)V"
        }
    .end annotation

    .line 55
    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseItemCollectionRequest;->getBaseRequest()Lcom/onedrive/sdk/http/BaseRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/onedrive/sdk/http/BaseRequest;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v0

    invoke-interface {v0}, Lcom/onedrive/sdk/extensions/IOneDriveClient;->getExecutors()Lcom/onedrive/sdk/concurrency/IExecutors;

    move-result-object v0

    .line 56
    new-instance v1, Lcom/onedrive/sdk/generated/BaseItemCollectionRequest$1;

    invoke-direct {v1, p0, v0, p1}, Lcom/onedrive/sdk/generated/BaseItemCollectionRequest$1;-><init>(Lcom/onedrive/sdk/generated/BaseItemCollectionRequest;Lcom/onedrive/sdk/concurrency/IExecutors;Lcom/onedrive/sdk/concurrency/ICallback;)V

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/concurrency/IExecutors;->performOnBackground(Ljava/lang/Runnable;)V

    return-void
.end method

.method public post(Lcom/onedrive/sdk/extensions/Item;)Lcom/onedrive/sdk/extensions/Item;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    .line 95
    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseItemCollectionRequest;->getBaseRequest()Lcom/onedrive/sdk/http/BaseRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/onedrive/sdk/http/BaseRequest;->getRequestUrl()Ljava/net/URL;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v0

    .line 96
    new-instance v1, Lcom/onedrive/sdk/extensions/ItemRequestBuilder;

    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseItemCollectionRequest;->getBaseRequest()Lcom/onedrive/sdk/http/BaseRequest;

    move-result-object v2

    invoke-virtual {v2}, Lcom/onedrive/sdk/http/BaseRequest;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v0, v2, v3}, Lcom/onedrive/sdk/extensions/ItemRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V

    invoke-virtual {v1}, Lcom/onedrive/sdk/extensions/ItemRequestBuilder;->buildRequest()Lcom/onedrive/sdk/extensions/IItemRequest;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/onedrive/sdk/extensions/IItemRequest;->post(Lcom/onedrive/sdk/extensions/Item;)Lcom/onedrive/sdk/extensions/Item;

    move-result-object p1

    return-object p1
.end method

.method public post(Lcom/onedrive/sdk/extensions/Item;Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/extensions/Item;",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/extensions/Item;",
            ">;)V"
        }
    .end annotation

    .line 88
    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseItemCollectionRequest;->getBaseRequest()Lcom/onedrive/sdk/http/BaseRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/onedrive/sdk/http/BaseRequest;->getRequestUrl()Ljava/net/URL;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v0

    .line 89
    new-instance v1, Lcom/onedrive/sdk/extensions/ItemRequestBuilder;

    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseItemCollectionRequest;->getBaseRequest()Lcom/onedrive/sdk/http/BaseRequest;

    move-result-object v2

    invoke-virtual {v2}, Lcom/onedrive/sdk/http/BaseRequest;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v0, v2, v3}, Lcom/onedrive/sdk/extensions/ItemRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V

    invoke-virtual {v1}, Lcom/onedrive/sdk/extensions/ItemRequestBuilder;->buildRequest()Lcom/onedrive/sdk/extensions/IItemRequest;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/onedrive/sdk/extensions/IItemRequest;->post(Lcom/onedrive/sdk/extensions/Item;Lcom/onedrive/sdk/concurrency/ICallback;)V

    return-void
.end method

.method public select(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IItemCollectionRequest;
    .locals 2

    .line 106
    new-instance v0, Lcom/onedrive/sdk/options/QueryOption;

    const-string v1, "select"

    invoke-direct {v0, v1, p1}, Lcom/onedrive/sdk/options/QueryOption;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/onedrive/sdk/generated/BaseItemCollectionRequest;->addQueryOption(Lcom/onedrive/sdk/options/QueryOption;)V

    .line 107
    move-object p1, p0

    check-cast p1, Lcom/onedrive/sdk/extensions/ItemCollectionRequest;

    return-object p1
.end method

.method public top(I)Lcom/onedrive/sdk/extensions/IItemCollectionRequest;
    .locals 3

    .line 111
    new-instance v0, Lcom/onedrive/sdk/options/QueryOption;

    const-string v1, "top"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ""

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/onedrive/sdk/options/QueryOption;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/onedrive/sdk/generated/BaseItemCollectionRequest;->addQueryOption(Lcom/onedrive/sdk/options/QueryOption;)V

    .line 112
    move-object p1, p0

    check-cast p1, Lcom/onedrive/sdk/extensions/ItemCollectionRequest;

    return-object p1
.end method

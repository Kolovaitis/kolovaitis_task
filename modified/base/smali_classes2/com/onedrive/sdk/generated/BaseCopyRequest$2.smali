.class Lcom/onedrive/sdk/generated/BaseCopyRequest$2;
.super Ljava/lang/Object;
.source "BaseCopyRequest.java"

# interfaces
.implements Lcom/onedrive/sdk/concurrency/ResultGetter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/onedrive/sdk/generated/BaseCopyRequest;->post()Lcom/onedrive/sdk/concurrency/AsyncMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/onedrive/sdk/concurrency/ResultGetter<",
        "Lcom/onedrive/sdk/extensions/Item;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/onedrive/sdk/generated/BaseCopyRequest;


# direct methods
.method constructor <init>(Lcom/onedrive/sdk/generated/BaseCopyRequest;)V
    .locals 0

    .line 88
    iput-object p1, p0, Lcom/onedrive/sdk/generated/BaseCopyRequest$2;->this$0:Lcom/onedrive/sdk/generated/BaseCopyRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getResultFrom(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;)Lcom/onedrive/sdk/extensions/Item;
    .locals 2

    .line 91
    new-instance v0, Lcom/onedrive/sdk/extensions/ItemRequest;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Lcom/onedrive/sdk/extensions/ItemRequest;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V

    invoke-virtual {v0}, Lcom/onedrive/sdk/extensions/ItemRequest;->get()Lcom/onedrive/sdk/extensions/Item;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic getResultFrom(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;)Ljava/lang/Object;
    .locals 0

    .line 88
    invoke-virtual {p0, p1, p2}, Lcom/onedrive/sdk/generated/BaseCopyRequest$2;->getResultFrom(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;)Lcom/onedrive/sdk/extensions/Item;

    move-result-object p1

    return-object p1
.end method

.class public interface abstract Lcom/onedrive/sdk/generated/IBaseSearchRequest;
.super Ljava/lang/Object;
.source "IBaseSearchRequest.java"


# virtual methods
.method public abstract expand(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/ISearchRequest;
.end method

.method public abstract get()Lcom/onedrive/sdk/extensions/ISearchCollectionPage;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation
.end method

.method public abstract get(Lcom/onedrive/sdk/concurrency/ICallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/extensions/ISearchCollectionPage;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract select(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/ISearchRequest;
.end method

.method public abstract top(I)Lcom/onedrive/sdk/extensions/ISearchRequest;
.end method

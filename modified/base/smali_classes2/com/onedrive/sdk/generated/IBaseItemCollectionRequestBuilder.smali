.class public interface abstract Lcom/onedrive/sdk/generated/IBaseItemCollectionRequestBuilder;
.super Ljava/lang/Object;
.source "IBaseItemCollectionRequestBuilder.java"

# interfaces
.implements Lcom/onedrive/sdk/http/IRequestBuilder;


# virtual methods
.method public abstract buildRequest()Lcom/onedrive/sdk/extensions/IItemCollectionRequest;
.end method

.method public abstract buildRequest(Ljava/util/List;)Lcom/onedrive/sdk/extensions/IItemCollectionRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;)",
            "Lcom/onedrive/sdk/extensions/IItemCollectionRequest;"
        }
    .end annotation
.end method

.method public abstract byId(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IItemRequestBuilder;
.end method

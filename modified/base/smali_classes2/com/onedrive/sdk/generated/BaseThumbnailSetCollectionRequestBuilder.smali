.class public Lcom/onedrive/sdk/generated/BaseThumbnailSetCollectionRequestBuilder;
.super Lcom/onedrive/sdk/http/BaseRequestBuilder;
.source "BaseThumbnailSetCollectionRequestBuilder.java"

# interfaces
.implements Lcom/onedrive/sdk/generated/IBaseThumbnailSetCollectionRequestBuilder;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/onedrive/sdk/extensions/IOneDriveClient;",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;)V"
        }
    .end annotation

    .line 50
    invoke-direct {p0, p1, p2, p3}, Lcom/onedrive/sdk/http/BaseRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public buildRequest()Lcom/onedrive/sdk/extensions/IThumbnailSetCollectionRequest;
    .locals 1

    .line 54
    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseThumbnailSetCollectionRequestBuilder;->getOptions()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/onedrive/sdk/generated/BaseThumbnailSetCollectionRequestBuilder;->buildRequest(Ljava/util/List;)Lcom/onedrive/sdk/extensions/IThumbnailSetCollectionRequest;

    move-result-object v0

    return-object v0
.end method

.method public buildRequest(Ljava/util/List;)Lcom/onedrive/sdk/extensions/IThumbnailSetCollectionRequest;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;)",
            "Lcom/onedrive/sdk/extensions/IThumbnailSetCollectionRequest;"
        }
    .end annotation

    .line 58
    new-instance v0, Lcom/onedrive/sdk/extensions/ThumbnailSetCollectionRequest;

    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseThumbnailSetCollectionRequestBuilder;->getRequestUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseThumbnailSetCollectionRequestBuilder;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v2

    invoke-direct {v0, v1, v2, p1}, Lcom/onedrive/sdk/extensions/ThumbnailSetCollectionRequest;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V

    return-object v0
.end method

.method public byId(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IThumbnailSetRequestBuilder;
    .locals 3

    .line 62
    new-instance v0, Lcom/onedrive/sdk/extensions/ThumbnailSetRequestBuilder;

    invoke-virtual {p0, p1}, Lcom/onedrive/sdk/generated/BaseThumbnailSetCollectionRequestBuilder;->getRequestUrlWithAdditionalSegment(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseThumbnailSetCollectionRequestBuilder;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v1

    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseThumbnailSetCollectionRequestBuilder;->getOptions()Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, p1, v1, v2}, Lcom/onedrive/sdk/extensions/ThumbnailSetRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V

    return-object v0
.end method

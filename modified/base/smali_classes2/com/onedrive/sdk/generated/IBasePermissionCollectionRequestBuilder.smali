.class public interface abstract Lcom/onedrive/sdk/generated/IBasePermissionCollectionRequestBuilder;
.super Ljava/lang/Object;
.source "IBasePermissionCollectionRequestBuilder.java"

# interfaces
.implements Lcom/onedrive/sdk/http/IRequestBuilder;


# virtual methods
.method public abstract buildRequest()Lcom/onedrive/sdk/extensions/IPermissionCollectionRequest;
.end method

.method public abstract buildRequest(Ljava/util/List;)Lcom/onedrive/sdk/extensions/IPermissionCollectionRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;)",
            "Lcom/onedrive/sdk/extensions/IPermissionCollectionRequest;"
        }
    .end annotation
.end method

.method public abstract byId(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IPermissionRequestBuilder;
.end method

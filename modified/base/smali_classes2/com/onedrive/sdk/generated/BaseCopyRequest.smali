.class public Lcom/onedrive/sdk/generated/BaseCopyRequest;
.super Lcom/onedrive/sdk/http/BaseRequest;
.source "BaseCopyRequest.java"

# interfaces
.implements Lcom/onedrive/sdk/generated/IBaseCopyRequest;


# instance fields
.field protected final mBody:Lcom/onedrive/sdk/extensions/CopyBody;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;Ljava/lang/String;Lcom/onedrive/sdk/extensions/ItemReference;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/onedrive/sdk/extensions/IOneDriveClient;",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/onedrive/sdk/extensions/ItemReference;",
            ")V"
        }
    .end annotation

    .line 51
    const-class v0, Lcom/onedrive/sdk/concurrency/AsyncMonitorLocation;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/onedrive/sdk/http/BaseRequest;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;Ljava/lang/Class;)V

    .line 52
    new-instance p1, Lcom/onedrive/sdk/extensions/CopyBody;

    invoke-direct {p1}, Lcom/onedrive/sdk/extensions/CopyBody;-><init>()V

    iput-object p1, p0, Lcom/onedrive/sdk/generated/BaseCopyRequest;->mBody:Lcom/onedrive/sdk/extensions/CopyBody;

    .line 53
    iget-object p1, p0, Lcom/onedrive/sdk/generated/BaseCopyRequest;->mBody:Lcom/onedrive/sdk/extensions/CopyBody;

    iput-object p4, p1, Lcom/onedrive/sdk/extensions/CopyBody;->name:Ljava/lang/String;

    .line 54
    iput-object p5, p1, Lcom/onedrive/sdk/extensions/CopyBody;->parentReference:Lcom/onedrive/sdk/extensions/ItemReference;

    const-string p1, "Prefer"

    const-string p2, "respond-async"

    .line 55
    invoke-virtual {p0, p1, p2}, Lcom/onedrive/sdk/generated/BaseCopyRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public create()Lcom/onedrive/sdk/concurrency/AsyncMonitor;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/onedrive/sdk/concurrency/AsyncMonitor<",
            "Lcom/onedrive/sdk/extensions/Item;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 69
    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseCopyRequest;->post()Lcom/onedrive/sdk/concurrency/AsyncMonitor;

    move-result-object v0

    return-object v0
.end method

.method public create(Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/concurrency/AsyncMonitor<",
            "Lcom/onedrive/sdk/extensions/Item;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 62
    invoke-virtual {p0, p1}, Lcom/onedrive/sdk/generated/BaseCopyRequest;->post(Lcom/onedrive/sdk/concurrency/ICallback;)V

    return-void
.end method

.method public expand(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/ICopyRequest;
    .locals 3

    .line 107
    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseCopyRequest;->getQueryOptions()Ljava/util/List;

    move-result-object v0

    new-instance v1, Lcom/onedrive/sdk/options/QueryOption;

    const-string v2, "expand"

    invoke-direct {v1, v2, p1}, Lcom/onedrive/sdk/options/QueryOption;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 108
    move-object p1, p0

    check-cast p1, Lcom/onedrive/sdk/extensions/CopyRequest;

    return-object p1
.end method

.method public post()Lcom/onedrive/sdk/concurrency/AsyncMonitor;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/onedrive/sdk/concurrency/AsyncMonitor<",
            "Lcom/onedrive/sdk/extensions/Item;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    .line 86
    sget-object v0, Lcom/onedrive/sdk/http/HttpMethod;->POST:Lcom/onedrive/sdk/http/HttpMethod;

    iget-object v1, p0, Lcom/onedrive/sdk/generated/BaseCopyRequest;->mBody:Lcom/onedrive/sdk/extensions/CopyBody;

    invoke-virtual {p0, v0, v1}, Lcom/onedrive/sdk/generated/BaseCopyRequest;->send(Lcom/onedrive/sdk/http/HttpMethod;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/onedrive/sdk/concurrency/AsyncMonitorLocation;

    .line 88
    new-instance v1, Lcom/onedrive/sdk/concurrency/AsyncMonitor;

    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseCopyRequest;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v2

    new-instance v3, Lcom/onedrive/sdk/generated/BaseCopyRequest$2;

    invoke-direct {v3, p0}, Lcom/onedrive/sdk/generated/BaseCopyRequest$2;-><init>(Lcom/onedrive/sdk/generated/BaseCopyRequest;)V

    invoke-direct {v1, v2, v0, v3}, Lcom/onedrive/sdk/concurrency/AsyncMonitor;-><init>(Lcom/onedrive/sdk/extensions/IOneDriveClient;Lcom/onedrive/sdk/concurrency/AsyncMonitorLocation;Lcom/onedrive/sdk/concurrency/ResultGetter;)V

    return-object v1
.end method

.method public post(Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/concurrency/AsyncMonitor<",
            "Lcom/onedrive/sdk/extensions/Item;",
            ">;>;)V"
        }
    .end annotation

    .line 73
    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseCopyRequest;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v0

    invoke-interface {v0}, Lcom/onedrive/sdk/extensions/IOneDriveClient;->getExecutors()Lcom/onedrive/sdk/concurrency/IExecutors;

    move-result-object v0

    new-instance v1, Lcom/onedrive/sdk/generated/BaseCopyRequest$1;

    invoke-direct {v1, p0, p1}, Lcom/onedrive/sdk/generated/BaseCopyRequest$1;-><init>(Lcom/onedrive/sdk/generated/BaseCopyRequest;Lcom/onedrive/sdk/concurrency/ICallback;)V

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/concurrency/IExecutors;->performOnBackground(Ljava/lang/Runnable;)V

    return-void
.end method

.method public select(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/ICopyRequest;
    .locals 3

    .line 97
    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseCopyRequest;->getQueryOptions()Ljava/util/List;

    move-result-object v0

    new-instance v1, Lcom/onedrive/sdk/options/QueryOption;

    const-string v2, "select"

    invoke-direct {v1, v2, p1}, Lcom/onedrive/sdk/options/QueryOption;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 98
    move-object p1, p0

    check-cast p1, Lcom/onedrive/sdk/extensions/CopyRequest;

    return-object p1
.end method

.method public top(I)Lcom/onedrive/sdk/extensions/ICopyRequest;
    .locals 4

    .line 102
    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseCopyRequest;->getQueryOptions()Ljava/util/List;

    move-result-object v0

    new-instance v1, Lcom/onedrive/sdk/options/QueryOption;

    const-string v2, "top"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ""

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, v2, p1}, Lcom/onedrive/sdk/options/QueryOption;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 103
    move-object p1, p0

    check-cast p1, Lcom/onedrive/sdk/extensions/CopyRequest;

    return-object p1
.end method

.class public Lcom/onedrive/sdk/generated/BasePhoto;
.super Ljava/lang/Object;
.source "BasePhoto.java"

# interfaces
.implements Lcom/onedrive/sdk/serializer/IJsonBackedObject;


# instance fields
.field public cameraMake:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cameraMake"
    .end annotation
.end field

.field public cameraModel:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cameraModel"
    .end annotation
.end field

.field public exposureDenominator:Ljava/lang/Double;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "exposureDenominator"
    .end annotation
.end field

.field public exposureNumerator:Ljava/lang/Double;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "exposureNumerator"
    .end annotation
.end field

.field public fNumber:Ljava/lang/Double;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "fNumber"
    .end annotation
.end field

.field public focalLength:Ljava/lang/Double;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "focalLength"
    .end annotation
.end field

.field public iso:Ljava/lang/Integer;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "iso"
    .end annotation
.end field

.field private transient mRawObject:Lcom/google/gson/JsonObject;

.field private transient mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;

.field public takenDateTime:Ljava/util/Calendar;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "takenDateTime"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getRawObject()Lcom/google/gson/JsonObject;
    .locals 1

    .line 109
    iget-object v0, p0, Lcom/onedrive/sdk/generated/BasePhoto;->mRawObject:Lcom/google/gson/JsonObject;

    return-object v0
.end method

.method protected getSerializer()Lcom/onedrive/sdk/serializer/ISerializer;
    .locals 1

    .line 117
    iget-object v0, p0, Lcom/onedrive/sdk/generated/BasePhoto;->mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;

    return-object v0
.end method

.method public setRawObject(Lcom/onedrive/sdk/serializer/ISerializer;Lcom/google/gson/JsonObject;)V
    .locals 0

    .line 127
    iput-object p1, p0, Lcom/onedrive/sdk/generated/BasePhoto;->mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;

    .line 128
    iput-object p2, p0, Lcom/onedrive/sdk/generated/BasePhoto;->mRawObject:Lcom/google/gson/JsonObject;

    return-void
.end method

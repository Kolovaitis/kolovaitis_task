.class public interface abstract Lcom/onedrive/sdk/generated/IBaseSearchRequestBuilder;
.super Ljava/lang/Object;
.source "IBaseSearchRequestBuilder.java"

# interfaces
.implements Lcom/onedrive/sdk/http/IRequestBuilder;


# virtual methods
.method public abstract buildRequest()Lcom/onedrive/sdk/extensions/ISearchRequest;
.end method

.method public abstract buildRequest(Ljava/util/List;)Lcom/onedrive/sdk/extensions/ISearchRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;)",
            "Lcom/onedrive/sdk/extensions/ISearchRequest;"
        }
    .end annotation
.end method

.class public Lcom/onedrive/sdk/generated/BaseRecentCollectionPage;
.super Lcom/onedrive/sdk/http/BaseCollectionPage;
.source "BaseRecentCollectionPage.java"

# interfaces
.implements Lcom/onedrive/sdk/generated/IBaseRecentCollectionPage;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/onedrive/sdk/http/BaseCollectionPage<",
        "Lcom/onedrive/sdk/extensions/Item;",
        "Lcom/onedrive/sdk/extensions/IRecentRequestBuilder;",
        ">;",
        "Lcom/onedrive/sdk/generated/IBaseRecentCollectionPage;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/onedrive/sdk/generated/BaseRecentCollectionResponse;Lcom/onedrive/sdk/extensions/IRecentRequestBuilder;)V
    .locals 0

    .line 52
    iget-object p1, p1, Lcom/onedrive/sdk/generated/BaseRecentCollectionResponse;->value:Ljava/util/List;

    invoke-direct {p0, p1, p2}, Lcom/onedrive/sdk/http/BaseCollectionPage;-><init>(Ljava/util/List;Lcom/onedrive/sdk/http/IRequestBuilder;)V

    return-void
.end method

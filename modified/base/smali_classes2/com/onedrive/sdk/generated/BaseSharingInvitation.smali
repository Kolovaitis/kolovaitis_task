.class public Lcom/onedrive/sdk/generated/BaseSharingInvitation;
.super Ljava/lang/Object;
.source "BaseSharingInvitation.java"

# interfaces
.implements Lcom/onedrive/sdk/serializer/IJsonBackedObject;


# instance fields
.field public email:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "email"
    .end annotation
.end field

.field public inviteErrorResolveUrl:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "inviteErrorResolveUrl"
    .end annotation
.end field

.field public invitedBy:Lcom/onedrive/sdk/extensions/IdentitySet;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "invitedBy"
    .end annotation
.end field

.field private transient mRawObject:Lcom/google/gson/JsonObject;

.field private transient mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;

.field public sendInvitationStatus:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "sendInvitationStatus"
    .end annotation
.end field

.field public signInRequired:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "signInRequired"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getRawObject()Lcom/google/gson/JsonObject;
    .locals 1

    .line 91
    iget-object v0, p0, Lcom/onedrive/sdk/generated/BaseSharingInvitation;->mRawObject:Lcom/google/gson/JsonObject;

    return-object v0
.end method

.method protected getSerializer()Lcom/onedrive/sdk/serializer/ISerializer;
    .locals 1

    .line 99
    iget-object v0, p0, Lcom/onedrive/sdk/generated/BaseSharingInvitation;->mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;

    return-object v0
.end method

.method public setRawObject(Lcom/onedrive/sdk/serializer/ISerializer;Lcom/google/gson/JsonObject;)V
    .locals 0

    .line 109
    iput-object p1, p0, Lcom/onedrive/sdk/generated/BaseSharingInvitation;->mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;

    .line 110
    iput-object p2, p0, Lcom/onedrive/sdk/generated/BaseSharingInvitation;->mRawObject:Lcom/google/gson/JsonObject;

    return-void
.end method

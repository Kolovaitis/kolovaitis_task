.class public Lcom/onedrive/sdk/generated/BaseDriveRequest;
.super Lcom/onedrive/sdk/http/BaseRequest;
.source "BaseDriveRequest.java"

# interfaces
.implements Lcom/onedrive/sdk/generated/IBaseDriveRequest;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/onedrive/sdk/extensions/IOneDriveClient;",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;)V"
        }
    .end annotation

    .line 50
    const-class v0, Lcom/onedrive/sdk/extensions/Drive;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/onedrive/sdk/http/BaseRequest;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public create(Lcom/onedrive/sdk/extensions/Drive;)Lcom/onedrive/sdk/extensions/Drive;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 102
    invoke-virtual {p0, p1}, Lcom/onedrive/sdk/generated/BaseDriveRequest;->post(Lcom/onedrive/sdk/extensions/Drive;)Lcom/onedrive/sdk/extensions/Drive;

    move-result-object p1

    return-object p1
.end method

.method public create(Lcom/onedrive/sdk/extensions/Drive;Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/extensions/Drive;",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/extensions/Drive;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 95
    invoke-virtual {p0, p1, p2}, Lcom/onedrive/sdk/generated/BaseDriveRequest;->post(Lcom/onedrive/sdk/extensions/Drive;Lcom/onedrive/sdk/concurrency/ICallback;)V

    return-void
.end method

.method public delete()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    .line 88
    sget-object v0, Lcom/onedrive/sdk/http/HttpMethod;->DELETE:Lcom/onedrive/sdk/http/HttpMethod;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/onedrive/sdk/generated/BaseDriveRequest;->send(Lcom/onedrive/sdk/http/HttpMethod;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public delete(Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .line 84
    sget-object v0, Lcom/onedrive/sdk/http/HttpMethod;->DELETE:Lcom/onedrive/sdk/http/HttpMethod;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Lcom/onedrive/sdk/generated/BaseDriveRequest;->send(Lcom/onedrive/sdk/http/HttpMethod;Lcom/onedrive/sdk/concurrency/ICallback;Ljava/lang/Object;)V

    return-void
.end method

.method public expand(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IDriveRequest;
    .locals 3

    .line 124
    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseDriveRequest;->getQueryOptions()Ljava/util/List;

    move-result-object v0

    new-instance v1, Lcom/onedrive/sdk/options/QueryOption;

    const-string v2, "expand"

    invoke-direct {v1, v2, p1}, Lcom/onedrive/sdk/options/QueryOption;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 125
    move-object p1, p0

    check-cast p1, Lcom/onedrive/sdk/extensions/DriveRequest;

    return-object p1
.end method

.method public bridge synthetic expand(Ljava/lang/String;)Lcom/onedrive/sdk/generated/IBaseDriveRequest;
    .locals 0

    .line 40
    invoke-virtual {p0, p1}, Lcom/onedrive/sdk/generated/BaseDriveRequest;->expand(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IDriveRequest;

    move-result-object p1

    return-object p1
.end method

.method public get()Lcom/onedrive/sdk/extensions/Drive;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    .line 58
    sget-object v0, Lcom/onedrive/sdk/http/HttpMethod;->GET:Lcom/onedrive/sdk/http/HttpMethod;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/onedrive/sdk/generated/BaseDriveRequest;->send(Lcom/onedrive/sdk/http/HttpMethod;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/onedrive/sdk/extensions/Drive;

    return-object v0
.end method

.method public get(Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/extensions/Drive;",
            ">;)V"
        }
    .end annotation

    .line 54
    sget-object v0, Lcom/onedrive/sdk/http/HttpMethod;->GET:Lcom/onedrive/sdk/http/HttpMethod;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Lcom/onedrive/sdk/generated/BaseDriveRequest;->send(Lcom/onedrive/sdk/http/HttpMethod;Lcom/onedrive/sdk/concurrency/ICallback;Ljava/lang/Object;)V

    return-void
.end method

.method public patch(Lcom/onedrive/sdk/extensions/Drive;)Lcom/onedrive/sdk/extensions/Drive;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    .line 80
    sget-object v0, Lcom/onedrive/sdk/http/HttpMethod;->PATCH:Lcom/onedrive/sdk/http/HttpMethod;

    invoke-virtual {p0, v0, p1}, Lcom/onedrive/sdk/generated/BaseDriveRequest;->send(Lcom/onedrive/sdk/http/HttpMethod;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/onedrive/sdk/extensions/Drive;

    return-object p1
.end method

.method public patch(Lcom/onedrive/sdk/extensions/Drive;Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/extensions/Drive;",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/extensions/Drive;",
            ">;)V"
        }
    .end annotation

    .line 76
    sget-object v0, Lcom/onedrive/sdk/http/HttpMethod;->PATCH:Lcom/onedrive/sdk/http/HttpMethod;

    invoke-virtual {p0, v0, p2, p1}, Lcom/onedrive/sdk/generated/BaseDriveRequest;->send(Lcom/onedrive/sdk/http/HttpMethod;Lcom/onedrive/sdk/concurrency/ICallback;Ljava/lang/Object;)V

    return-void
.end method

.method public post(Lcom/onedrive/sdk/extensions/Drive;)Lcom/onedrive/sdk/extensions/Drive;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    .line 110
    sget-object v0, Lcom/onedrive/sdk/http/HttpMethod;->POST:Lcom/onedrive/sdk/http/HttpMethod;

    invoke-virtual {p0, v0, p1}, Lcom/onedrive/sdk/generated/BaseDriveRequest;->send(Lcom/onedrive/sdk/http/HttpMethod;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/onedrive/sdk/extensions/Drive;

    return-object p1
.end method

.method public post(Lcom/onedrive/sdk/extensions/Drive;Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/extensions/Drive;",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/extensions/Drive;",
            ">;)V"
        }
    .end annotation

    .line 106
    sget-object v0, Lcom/onedrive/sdk/http/HttpMethod;->POST:Lcom/onedrive/sdk/http/HttpMethod;

    invoke-virtual {p0, v0, p2, p1}, Lcom/onedrive/sdk/generated/BaseDriveRequest;->send(Lcom/onedrive/sdk/http/HttpMethod;Lcom/onedrive/sdk/concurrency/ICallback;Ljava/lang/Object;)V

    return-void
.end method

.method public select(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IDriveRequest;
    .locals 3

    .line 114
    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseDriveRequest;->getQueryOptions()Ljava/util/List;

    move-result-object v0

    new-instance v1, Lcom/onedrive/sdk/options/QueryOption;

    const-string v2, "select"

    invoke-direct {v1, v2, p1}, Lcom/onedrive/sdk/options/QueryOption;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 115
    move-object p1, p0

    check-cast p1, Lcom/onedrive/sdk/extensions/DriveRequest;

    return-object p1
.end method

.method public bridge synthetic select(Ljava/lang/String;)Lcom/onedrive/sdk/generated/IBaseDriveRequest;
    .locals 0

    .line 40
    invoke-virtual {p0, p1}, Lcom/onedrive/sdk/generated/BaseDriveRequest;->select(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IDriveRequest;

    move-result-object p1

    return-object p1
.end method

.method public top(I)Lcom/onedrive/sdk/extensions/IDriveRequest;
    .locals 4

    .line 119
    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseDriveRequest;->getQueryOptions()Ljava/util/List;

    move-result-object v0

    new-instance v1, Lcom/onedrive/sdk/options/QueryOption;

    const-string v2, "top"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ""

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, v2, p1}, Lcom/onedrive/sdk/options/QueryOption;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 120
    move-object p1, p0

    check-cast p1, Lcom/onedrive/sdk/extensions/DriveRequest;

    return-object p1
.end method

.method public bridge synthetic top(I)Lcom/onedrive/sdk/generated/IBaseDriveRequest;
    .locals 0

    .line 40
    invoke-virtual {p0, p1}, Lcom/onedrive/sdk/generated/BaseDriveRequest;->top(I)Lcom/onedrive/sdk/extensions/IDriveRequest;

    move-result-object p1

    return-object p1
.end method

.method public update(Lcom/onedrive/sdk/extensions/Drive;)Lcom/onedrive/sdk/extensions/Drive;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 72
    invoke-virtual {p0, p1}, Lcom/onedrive/sdk/generated/BaseDriveRequest;->patch(Lcom/onedrive/sdk/extensions/Drive;)Lcom/onedrive/sdk/extensions/Drive;

    move-result-object p1

    return-object p1
.end method

.method public update(Lcom/onedrive/sdk/extensions/Drive;Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/extensions/Drive;",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/extensions/Drive;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 65
    invoke-virtual {p0, p1, p2}, Lcom/onedrive/sdk/generated/BaseDriveRequest;->patch(Lcom/onedrive/sdk/extensions/Drive;Lcom/onedrive/sdk/concurrency/ICallback;)V

    return-void
.end method

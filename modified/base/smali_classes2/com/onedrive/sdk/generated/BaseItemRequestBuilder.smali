.class public Lcom/onedrive/sdk/generated/BaseItemRequestBuilder;
.super Lcom/onedrive/sdk/http/BaseRequestBuilder;
.source "BaseItemRequestBuilder.java"

# interfaces
.implements Lcom/onedrive/sdk/generated/IBaseItemRequestBuilder;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/onedrive/sdk/extensions/IOneDriveClient;",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;)V"
        }
    .end annotation

    .line 50
    invoke-direct {p0, p1, p2, p3}, Lcom/onedrive/sdk/http/BaseRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public buildRequest()Lcom/onedrive/sdk/extensions/IItemRequest;
    .locals 1

    .line 57
    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseItemRequestBuilder;->getOptions()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/onedrive/sdk/generated/BaseItemRequestBuilder;->buildRequest(Ljava/util/List;)Lcom/onedrive/sdk/extensions/IItemRequest;

    move-result-object v0

    return-object v0
.end method

.method public buildRequest(Ljava/util/List;)Lcom/onedrive/sdk/extensions/IItemRequest;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;)",
            "Lcom/onedrive/sdk/extensions/IItemRequest;"
        }
    .end annotation

    .line 64
    new-instance v0, Lcom/onedrive/sdk/extensions/ItemRequest;

    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseItemRequestBuilder;->getRequestUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseItemRequestBuilder;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v2

    invoke-direct {v0, v1, v2, p1}, Lcom/onedrive/sdk/extensions/ItemRequest;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V

    return-object v0
.end method

.method public getChildren()Lcom/onedrive/sdk/extensions/IItemCollectionRequestBuilder;
    .locals 4

    .line 76
    new-instance v0, Lcom/onedrive/sdk/extensions/ItemCollectionRequestBuilder;

    const-string v1, "children"

    invoke-virtual {p0, v1}, Lcom/onedrive/sdk/generated/BaseItemRequestBuilder;->getRequestUrlWithAdditionalSegment(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseItemRequestBuilder;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/onedrive/sdk/extensions/ItemCollectionRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V

    return-object v0
.end method

.method public getChildren(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IItemRequestBuilder;
    .locals 3

    .line 80
    new-instance v0, Lcom/onedrive/sdk/extensions/ItemRequestBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "children"

    invoke-virtual {p0, v2}, Lcom/onedrive/sdk/generated/BaseItemRequestBuilder;->getRequestUrlWithAdditionalSegment(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseItemRequestBuilder;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, v2}, Lcom/onedrive/sdk/extensions/ItemRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V

    return-object v0
.end method

.method public getContent()Lcom/onedrive/sdk/extensions/IItemStreamRequestBuilder;
    .locals 4

    .line 92
    new-instance v0, Lcom/onedrive/sdk/extensions/ItemStreamRequestBuilder;

    const-string v1, "content"

    invoke-virtual {p0, v1}, Lcom/onedrive/sdk/generated/BaseItemRequestBuilder;->getRequestUrlWithAdditionalSegment(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseItemRequestBuilder;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/onedrive/sdk/extensions/ItemStreamRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V

    return-object v0
.end method

.method public getCopy(Ljava/lang/String;Lcom/onedrive/sdk/extensions/ItemReference;)Lcom/onedrive/sdk/extensions/ICopyRequestBuilder;
    .locals 7

    .line 100
    new-instance v6, Lcom/onedrive/sdk/extensions/CopyRequestBuilder;

    const-string v0, "action.copy"

    invoke-virtual {p0, v0}, Lcom/onedrive/sdk/generated/BaseItemRequestBuilder;->getRequestUrlWithAdditionalSegment(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseItemRequestBuilder;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v2

    const/4 v3, 0x0

    move-object v0, v6

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/onedrive/sdk/extensions/CopyRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;Ljava/lang/String;Lcom/onedrive/sdk/extensions/ItemReference;)V

    return-object v6
.end method

.method public getCreateLink(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/ICreateLinkRequestBuilder;
    .locals 4

    .line 104
    new-instance v0, Lcom/onedrive/sdk/extensions/CreateLinkRequestBuilder;

    const-string v1, "action.createLink"

    invoke-virtual {p0, v1}, Lcom/onedrive/sdk/generated/BaseItemRequestBuilder;->getRequestUrlWithAdditionalSegment(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseItemRequestBuilder;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3, p1}, Lcom/onedrive/sdk/extensions/CreateLinkRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;Ljava/lang/String;)V

    return-object v0
.end method

.method public getCreateSession(Lcom/onedrive/sdk/extensions/ChunkedUploadSessionDescriptor;)Lcom/onedrive/sdk/extensions/ICreateSessionRequestBuilder;
    .locals 4

    .line 96
    new-instance v0, Lcom/onedrive/sdk/extensions/CreateSessionRequestBuilder;

    const-string v1, "action.createUploadSession"

    invoke-virtual {p0, v1}, Lcom/onedrive/sdk/generated/BaseItemRequestBuilder;->getRequestUrlWithAdditionalSegment(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseItemRequestBuilder;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3, p1}, Lcom/onedrive/sdk/extensions/CreateSessionRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;Lcom/onedrive/sdk/extensions/ChunkedUploadSessionDescriptor;)V

    return-object v0
.end method

.method public getDelta(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IDeltaRequestBuilder;
    .locals 4

    .line 108
    new-instance v0, Lcom/onedrive/sdk/extensions/DeltaRequestBuilder;

    const-string v1, "view.delta"

    invoke-virtual {p0, v1}, Lcom/onedrive/sdk/generated/BaseItemRequestBuilder;->getRequestUrlWithAdditionalSegment(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseItemRequestBuilder;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3, p1}, Lcom/onedrive/sdk/extensions/DeltaRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;Ljava/lang/String;)V

    return-object v0
.end method

.method public getPermissions()Lcom/onedrive/sdk/extensions/IPermissionCollectionRequestBuilder;
    .locals 4

    .line 68
    new-instance v0, Lcom/onedrive/sdk/extensions/PermissionCollectionRequestBuilder;

    const-string v1, "permissions"

    invoke-virtual {p0, v1}, Lcom/onedrive/sdk/generated/BaseItemRequestBuilder;->getRequestUrlWithAdditionalSegment(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseItemRequestBuilder;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/onedrive/sdk/extensions/PermissionCollectionRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V

    return-object v0
.end method

.method public getPermissions(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IPermissionRequestBuilder;
    .locals 3

    .line 72
    new-instance v0, Lcom/onedrive/sdk/extensions/PermissionRequestBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "permissions"

    invoke-virtual {p0, v2}, Lcom/onedrive/sdk/generated/BaseItemRequestBuilder;->getRequestUrlWithAdditionalSegment(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseItemRequestBuilder;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, v2}, Lcom/onedrive/sdk/extensions/PermissionRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V

    return-object v0
.end method

.method public getSearch(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/ISearchRequestBuilder;
    .locals 4

    .line 112
    new-instance v0, Lcom/onedrive/sdk/extensions/SearchRequestBuilder;

    const-string v1, "view.search"

    invoke-virtual {p0, v1}, Lcom/onedrive/sdk/generated/BaseItemRequestBuilder;->getRequestUrlWithAdditionalSegment(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseItemRequestBuilder;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3, p1}, Lcom/onedrive/sdk/extensions/SearchRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;Ljava/lang/String;)V

    return-object v0
.end method

.method public getThumbnails()Lcom/onedrive/sdk/extensions/IThumbnailSetCollectionRequestBuilder;
    .locals 4

    .line 84
    new-instance v0, Lcom/onedrive/sdk/extensions/ThumbnailSetCollectionRequestBuilder;

    const-string v1, "thumbnails"

    invoke-virtual {p0, v1}, Lcom/onedrive/sdk/generated/BaseItemRequestBuilder;->getRequestUrlWithAdditionalSegment(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseItemRequestBuilder;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/onedrive/sdk/extensions/ThumbnailSetCollectionRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V

    return-object v0
.end method

.method public getThumbnails(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IThumbnailSetRequestBuilder;
    .locals 3

    .line 88
    new-instance v0, Lcom/onedrive/sdk/extensions/ThumbnailSetRequestBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "thumbnails"

    invoke-virtual {p0, v2}, Lcom/onedrive/sdk/generated/BaseItemRequestBuilder;->getRequestUrlWithAdditionalSegment(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseItemRequestBuilder;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, v2}, Lcom/onedrive/sdk/extensions/ThumbnailSetRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V

    return-object v0
.end method

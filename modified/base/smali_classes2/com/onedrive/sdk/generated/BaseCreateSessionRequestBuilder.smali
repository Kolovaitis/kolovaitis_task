.class public Lcom/onedrive/sdk/generated/BaseCreateSessionRequestBuilder;
.super Lcom/onedrive/sdk/http/BaseRequestBuilder;
.source "BaseCreateSessionRequestBuilder.java"


# instance fields
.field public final mItem:Lcom/onedrive/sdk/extensions/ChunkedUploadSessionDescriptor;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;Lcom/onedrive/sdk/extensions/ChunkedUploadSessionDescriptor;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/onedrive/sdk/extensions/IOneDriveClient;",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;",
            "Lcom/onedrive/sdk/extensions/ChunkedUploadSessionDescriptor;",
            ")V"
        }
    .end annotation

    .line 52
    invoke-direct {p0, p1, p2, p3}, Lcom/onedrive/sdk/http/BaseRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V

    .line 53
    iput-object p4, p0, Lcom/onedrive/sdk/generated/BaseCreateSessionRequestBuilder;->mItem:Lcom/onedrive/sdk/extensions/ChunkedUploadSessionDescriptor;

    return-void
.end method


# virtual methods
.method public buildRequest()Lcom/onedrive/sdk/extensions/ICreateSessionRequest;
    .locals 1

    .line 57
    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseCreateSessionRequestBuilder;->getOptions()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/onedrive/sdk/generated/BaseCreateSessionRequestBuilder;->buildRequest(Ljava/util/List;)Lcom/onedrive/sdk/extensions/ICreateSessionRequest;

    move-result-object v0

    return-object v0
.end method

.method public buildRequest(Ljava/util/List;)Lcom/onedrive/sdk/extensions/ICreateSessionRequest;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;)",
            "Lcom/onedrive/sdk/extensions/ICreateSessionRequest;"
        }
    .end annotation

    .line 61
    new-instance v0, Lcom/onedrive/sdk/extensions/CreateSessionRequest;

    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseCreateSessionRequestBuilder;->getRequestUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseCreateSessionRequestBuilder;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v2

    iget-object v3, p0, Lcom/onedrive/sdk/generated/BaseCreateSessionRequestBuilder;->mItem:Lcom/onedrive/sdk/extensions/ChunkedUploadSessionDescriptor;

    invoke-direct {v0, v1, v2, p1, v3}, Lcom/onedrive/sdk/extensions/CreateSessionRequest;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;Lcom/onedrive/sdk/extensions/ChunkedUploadSessionDescriptor;)V

    return-object v0
.end method

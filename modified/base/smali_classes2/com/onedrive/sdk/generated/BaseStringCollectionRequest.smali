.class public Lcom/onedrive/sdk/generated/BaseStringCollectionRequest;
.super Lcom/onedrive/sdk/http/BaseCollectionRequest;
.source "BaseStringCollectionRequest.java"

# interfaces
.implements Lcom/onedrive/sdk/generated/IBaseStringCollectionRequest;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/onedrive/sdk/http/BaseCollectionRequest<",
        "Lcom/onedrive/sdk/generated/BaseStringCollectionResponse;",
        "Lcom/onedrive/sdk/extensions/IStringCollectionPage;",
        ">;",
        "Lcom/onedrive/sdk/generated/IBaseStringCollectionRequest;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/onedrive/sdk/extensions/IOneDriveClient;",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;)V"
        }
    .end annotation

    .line 51
    const-class v4, Lcom/onedrive/sdk/generated/BaseStringCollectionResponse;

    const-class v5, Lcom/onedrive/sdk/extensions/IStringCollectionPage;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/onedrive/sdk/http/BaseCollectionRequest;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;Ljava/lang/Class;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public buildFromResponse(Lcom/onedrive/sdk/generated/BaseStringCollectionResponse;)Lcom/onedrive/sdk/extensions/IStringCollectionPage;
    .locals 4

    .line 71
    iget-object v0, p1, Lcom/onedrive/sdk/generated/BaseStringCollectionResponse;->nextLink:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 72
    new-instance v0, Lcom/onedrive/sdk/extensions/StringCollectionRequestBuilder;

    iget-object v2, p1, Lcom/onedrive/sdk/generated/BaseStringCollectionResponse;->nextLink:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseStringCollectionRequest;->getBaseRequest()Lcom/onedrive/sdk/http/BaseRequest;

    move-result-object v3

    invoke-virtual {v3}, Lcom/onedrive/sdk/http/BaseRequest;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v3

    invoke-direct {v0, v2, v3, v1}, Lcom/onedrive/sdk/extensions/StringCollectionRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V

    goto :goto_0

    :cond_0
    move-object v0, v1

    .line 76
    :goto_0
    new-instance v1, Lcom/onedrive/sdk/extensions/StringCollectionPage;

    invoke-direct {v1, p1, v0}, Lcom/onedrive/sdk/extensions/StringCollectionPage;-><init>(Lcom/onedrive/sdk/generated/BaseStringCollectionResponse;Lcom/onedrive/sdk/extensions/IStringCollectionRequestBuilder;)V

    .line 77
    invoke-virtual {p1}, Lcom/onedrive/sdk/generated/BaseStringCollectionResponse;->getSerializer()Lcom/onedrive/sdk/serializer/ISerializer;

    move-result-object v0

    invoke-virtual {p1}, Lcom/onedrive/sdk/generated/BaseStringCollectionResponse;->getRawObject()Lcom/google/gson/JsonObject;

    move-result-object p1

    invoke-virtual {v1, v0, p1}, Lcom/onedrive/sdk/extensions/StringCollectionPage;->setRawObject(Lcom/onedrive/sdk/serializer/ISerializer;Lcom/google/gson/JsonObject;)V

    return-object v1
.end method

.method public expand(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IStringCollectionRequest;
    .locals 2

    .line 55
    new-instance v0, Lcom/onedrive/sdk/options/QueryOption;

    const-string v1, "expand"

    invoke-direct {v0, v1, p1}, Lcom/onedrive/sdk/options/QueryOption;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/onedrive/sdk/generated/BaseStringCollectionRequest;->addQueryOption(Lcom/onedrive/sdk/options/QueryOption;)V

    .line 56
    move-object p1, p0

    check-cast p1, Lcom/onedrive/sdk/extensions/StringCollectionRequest;

    return-object p1
.end method

.method public select(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IStringCollectionRequest;
    .locals 2

    .line 60
    new-instance v0, Lcom/onedrive/sdk/options/QueryOption;

    const-string v1, "select"

    invoke-direct {v0, v1, p1}, Lcom/onedrive/sdk/options/QueryOption;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/onedrive/sdk/generated/BaseStringCollectionRequest;->addQueryOption(Lcom/onedrive/sdk/options/QueryOption;)V

    .line 61
    move-object p1, p0

    check-cast p1, Lcom/onedrive/sdk/extensions/StringCollectionRequest;

    return-object p1
.end method

.method public top(I)Lcom/onedrive/sdk/extensions/IStringCollectionRequest;
    .locals 3

    .line 65
    new-instance v0, Lcom/onedrive/sdk/options/QueryOption;

    const-string v1, "top"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ""

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/onedrive/sdk/options/QueryOption;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/onedrive/sdk/generated/BaseStringCollectionRequest;->addQueryOption(Lcom/onedrive/sdk/options/QueryOption;)V

    .line 66
    move-object p1, p0

    check-cast p1, Lcom/onedrive/sdk/extensions/StringCollectionRequest;

    return-object p1
.end method

.class public Lcom/onedrive/sdk/generated/BaseShare;
.super Ljava/lang/Object;
.source "BaseShare.java"

# interfaces
.implements Lcom/onedrive/sdk/serializer/IJsonBackedObject;


# instance fields
.field public id:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "id"
    .end annotation
.end field

.field public transient items:Lcom/onedrive/sdk/extensions/ItemCollectionPage;

.field private transient mRawObject:Lcom/google/gson/JsonObject;

.field private transient mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;

.field public name:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "name"
    .end annotation
.end field

.field public owner:Lcom/onedrive/sdk/extensions/IdentitySet;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "owner"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getRawObject()Lcom/google/gson/JsonObject;
    .locals 1

    .line 84
    iget-object v0, p0, Lcom/onedrive/sdk/generated/BaseShare;->mRawObject:Lcom/google/gson/JsonObject;

    return-object v0
.end method

.method protected getSerializer()Lcom/onedrive/sdk/serializer/ISerializer;
    .locals 1

    .line 92
    iget-object v0, p0, Lcom/onedrive/sdk/generated/BaseShare;->mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;

    return-object v0
.end method

.method public setRawObject(Lcom/onedrive/sdk/serializer/ISerializer;Lcom/google/gson/JsonObject;)V
    .locals 5

    .line 102
    iput-object p1, p0, Lcom/onedrive/sdk/generated/BaseShare;->mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;

    .line 103
    iput-object p2, p0, Lcom/onedrive/sdk/generated/BaseShare;->mRawObject:Lcom/google/gson/JsonObject;

    const-string v0, "items"

    .line 106
    invoke-virtual {p2, v0}, Lcom/google/gson/JsonObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 107
    new-instance v0, Lcom/onedrive/sdk/generated/BaseItemCollectionResponse;

    invoke-direct {v0}, Lcom/onedrive/sdk/generated/BaseItemCollectionResponse;-><init>()V

    const-string v1, "items@odata.nextLink"

    .line 108
    invoke-virtual {p2, v1}, Lcom/google/gson/JsonObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "items@odata.nextLink"

    .line 109
    invoke-virtual {p2, v1}, Lcom/google/gson/JsonObject;->get(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/gson/JsonElement;->getAsString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/onedrive/sdk/generated/BaseItemCollectionResponse;->nextLink:Ljava/lang/String;

    :cond_0
    const-string v1, "items"

    .line 112
    invoke-virtual {p2, v1}, Lcom/google/gson/JsonObject;->get(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object p2

    invoke-virtual {p2}, Lcom/google/gson/JsonElement;->toString()Ljava/lang/String;

    move-result-object p2

    const-class v1, [Lcom/google/gson/JsonObject;

    invoke-interface {p1, p2, v1}, Lcom/onedrive/sdk/serializer/ISerializer;->deserializeObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, [Lcom/google/gson/JsonObject;

    .line 113
    array-length v1, p2

    new-array v1, v1, [Lcom/onedrive/sdk/extensions/Item;

    const/4 v2, 0x0

    .line 114
    :goto_0
    array-length v3, p2

    if-ge v2, v3, :cond_1

    .line 115
    aget-object v3, p2, v2

    invoke-virtual {v3}, Lcom/google/gson/JsonObject;->toString()Ljava/lang/String;

    move-result-object v3

    const-class v4, Lcom/onedrive/sdk/extensions/Item;

    invoke-interface {p1, v3, v4}, Lcom/onedrive/sdk/serializer/ISerializer;->deserializeObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/onedrive/sdk/extensions/Item;

    aput-object v3, v1, v2

    .line 116
    aget-object v3, v1, v2

    aget-object v4, p2, v2

    invoke-virtual {v3, p1, v4}, Lcom/onedrive/sdk/extensions/Item;->setRawObject(Lcom/onedrive/sdk/serializer/ISerializer;Lcom/google/gson/JsonObject;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 118
    :cond_1
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    iput-object p1, v0, Lcom/onedrive/sdk/generated/BaseItemCollectionResponse;->value:Ljava/util/List;

    .line 119
    new-instance p1, Lcom/onedrive/sdk/extensions/ItemCollectionPage;

    const/4 p2, 0x0

    invoke-direct {p1, v0, p2}, Lcom/onedrive/sdk/extensions/ItemCollectionPage;-><init>(Lcom/onedrive/sdk/generated/BaseItemCollectionResponse;Lcom/onedrive/sdk/extensions/IItemCollectionRequestBuilder;)V

    iput-object p1, p0, Lcom/onedrive/sdk/generated/BaseShare;->items:Lcom/onedrive/sdk/extensions/ItemCollectionPage;

    :cond_2
    return-void
.end method

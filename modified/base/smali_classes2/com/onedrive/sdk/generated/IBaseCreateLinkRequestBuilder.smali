.class public interface abstract Lcom/onedrive/sdk/generated/IBaseCreateLinkRequestBuilder;
.super Ljava/lang/Object;
.source "IBaseCreateLinkRequestBuilder.java"

# interfaces
.implements Lcom/onedrive/sdk/http/IRequestBuilder;


# virtual methods
.method public abstract buildRequest()Lcom/onedrive/sdk/extensions/ICreateLinkRequest;
.end method

.method public abstract buildRequest(Ljava/util/List;)Lcom/onedrive/sdk/extensions/ICreateLinkRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;)",
            "Lcom/onedrive/sdk/extensions/ICreateLinkRequest;"
        }
    .end annotation
.end method

.class public Lcom/onedrive/sdk/generated/BaseUploadSession;
.super Ljava/lang/Object;
.source "BaseUploadSession.java"

# interfaces
.implements Lcom/onedrive/sdk/serializer/IJsonBackedObject;


# instance fields
.field public expirationDateTime:Ljava/util/Calendar;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "expirationDateTime"
    .end annotation
.end field

.field private transient mRawObject:Lcom/google/gson/JsonObject;

.field private transient mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;

.field public nextExpectedRanges:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nextExpectedRanges"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public uploadUrl:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "uploadUrl"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getRawObject()Lcom/google/gson/JsonObject;
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/onedrive/sdk/generated/BaseUploadSession;->mRawObject:Lcom/google/gson/JsonObject;

    return-object v0
.end method

.method protected getSerializer()Lcom/onedrive/sdk/serializer/ISerializer;
    .locals 1

    .line 87
    iget-object v0, p0, Lcom/onedrive/sdk/generated/BaseUploadSession;->mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;

    return-object v0
.end method

.method public setRawObject(Lcom/onedrive/sdk/serializer/ISerializer;Lcom/google/gson/JsonObject;)V
    .locals 0

    .line 97
    iput-object p1, p0, Lcom/onedrive/sdk/generated/BaseUploadSession;->mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;

    .line 98
    iput-object p2, p0, Lcom/onedrive/sdk/generated/BaseUploadSession;->mRawObject:Lcom/google/gson/JsonObject;

    return-void
.end method

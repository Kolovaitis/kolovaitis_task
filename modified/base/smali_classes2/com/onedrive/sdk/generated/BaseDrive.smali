.class public Lcom/onedrive/sdk/generated/BaseDrive;
.super Ljava/lang/Object;
.source "BaseDrive.java"

# interfaces
.implements Lcom/onedrive/sdk/serializer/IJsonBackedObject;


# instance fields
.field public driveType:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "driveType"
    .end annotation
.end field

.field public id:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "id"
    .end annotation
.end field

.field public transient items:Lcom/onedrive/sdk/extensions/ItemCollectionPage;

.field private transient mRawObject:Lcom/google/gson/JsonObject;

.field private transient mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;

.field public owner:Lcom/onedrive/sdk/extensions/IdentitySet;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "owner"
    .end annotation
.end field

.field public quota:Lcom/onedrive/sdk/extensions/Quota;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "quota"
    .end annotation
.end field

.field public transient shared:Lcom/onedrive/sdk/extensions/ItemCollectionPage;

.field public transient special:Lcom/onedrive/sdk/extensions/ItemCollectionPage;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getRawObject()Lcom/google/gson/JsonObject;
    .locals 1

    .line 100
    iget-object v0, p0, Lcom/onedrive/sdk/generated/BaseDrive;->mRawObject:Lcom/google/gson/JsonObject;

    return-object v0
.end method

.method protected getSerializer()Lcom/onedrive/sdk/serializer/ISerializer;
    .locals 1

    .line 108
    iget-object v0, p0, Lcom/onedrive/sdk/generated/BaseDrive;->mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;

    return-object v0
.end method

.method public setRawObject(Lcom/onedrive/sdk/serializer/ISerializer;Lcom/google/gson/JsonObject;)V
    .locals 8

    .line 118
    iput-object p1, p0, Lcom/onedrive/sdk/generated/BaseDrive;->mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;

    .line 119
    iput-object p2, p0, Lcom/onedrive/sdk/generated/BaseDrive;->mRawObject:Lcom/google/gson/JsonObject;

    const-string v0, "items"

    .line 122
    invoke-virtual {p2, v0}, Lcom/google/gson/JsonObject;->has(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    .line 123
    new-instance v0, Lcom/onedrive/sdk/generated/BaseItemCollectionResponse;

    invoke-direct {v0}, Lcom/onedrive/sdk/generated/BaseItemCollectionResponse;-><init>()V

    const-string v3, "items@odata.nextLink"

    .line 124
    invoke-virtual {p2, v3}, Lcom/google/gson/JsonObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "items@odata.nextLink"

    .line 125
    invoke-virtual {p2, v3}, Lcom/google/gson/JsonObject;->get(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/gson/JsonElement;->getAsString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/onedrive/sdk/generated/BaseItemCollectionResponse;->nextLink:Ljava/lang/String;

    :cond_0
    const-string v3, "items"

    .line 128
    invoke-virtual {p2, v3}, Lcom/google/gson/JsonObject;->get(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/gson/JsonElement;->toString()Ljava/lang/String;

    move-result-object v3

    const-class v4, [Lcom/google/gson/JsonObject;

    invoke-interface {p1, v3, v4}, Lcom/onedrive/sdk/serializer/ISerializer;->deserializeObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lcom/google/gson/JsonObject;

    .line 129
    array-length v4, v3

    new-array v4, v4, [Lcom/onedrive/sdk/extensions/Item;

    const/4 v5, 0x0

    .line 130
    :goto_0
    array-length v6, v3

    if-ge v5, v6, :cond_1

    .line 131
    aget-object v6, v3, v5

    invoke-virtual {v6}, Lcom/google/gson/JsonObject;->toString()Ljava/lang/String;

    move-result-object v6

    const-class v7, Lcom/onedrive/sdk/extensions/Item;

    invoke-interface {p1, v6, v7}, Lcom/onedrive/sdk/serializer/ISerializer;->deserializeObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/onedrive/sdk/extensions/Item;

    aput-object v6, v4, v5

    .line 132
    aget-object v6, v4, v5

    aget-object v7, v3, v5

    invoke-virtual {v6, p1, v7}, Lcom/onedrive/sdk/extensions/Item;->setRawObject(Lcom/onedrive/sdk/serializer/ISerializer;Lcom/google/gson/JsonObject;)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 134
    :cond_1
    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    iput-object v3, v0, Lcom/onedrive/sdk/generated/BaseItemCollectionResponse;->value:Ljava/util/List;

    .line 135
    new-instance v3, Lcom/onedrive/sdk/extensions/ItemCollectionPage;

    invoke-direct {v3, v0, v1}, Lcom/onedrive/sdk/extensions/ItemCollectionPage;-><init>(Lcom/onedrive/sdk/generated/BaseItemCollectionResponse;Lcom/onedrive/sdk/extensions/IItemCollectionRequestBuilder;)V

    iput-object v3, p0, Lcom/onedrive/sdk/generated/BaseDrive;->items:Lcom/onedrive/sdk/extensions/ItemCollectionPage;

    :cond_2
    const-string v0, "shared"

    .line 138
    invoke-virtual {p2, v0}, Lcom/google/gson/JsonObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 139
    new-instance v0, Lcom/onedrive/sdk/generated/BaseItemCollectionResponse;

    invoke-direct {v0}, Lcom/onedrive/sdk/generated/BaseItemCollectionResponse;-><init>()V

    const-string v3, "shared@odata.nextLink"

    .line 140
    invoke-virtual {p2, v3}, Lcom/google/gson/JsonObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "shared@odata.nextLink"

    .line 141
    invoke-virtual {p2, v3}, Lcom/google/gson/JsonObject;->get(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/gson/JsonElement;->getAsString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/onedrive/sdk/generated/BaseItemCollectionResponse;->nextLink:Ljava/lang/String;

    :cond_3
    const-string v3, "shared"

    .line 144
    invoke-virtual {p2, v3}, Lcom/google/gson/JsonObject;->get(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/gson/JsonElement;->toString()Ljava/lang/String;

    move-result-object v3

    const-class v4, [Lcom/google/gson/JsonObject;

    invoke-interface {p1, v3, v4}, Lcom/onedrive/sdk/serializer/ISerializer;->deserializeObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lcom/google/gson/JsonObject;

    .line 145
    array-length v4, v3

    new-array v4, v4, [Lcom/onedrive/sdk/extensions/Item;

    const/4 v5, 0x0

    .line 146
    :goto_1
    array-length v6, v3

    if-ge v5, v6, :cond_4

    .line 147
    aget-object v6, v3, v5

    invoke-virtual {v6}, Lcom/google/gson/JsonObject;->toString()Ljava/lang/String;

    move-result-object v6

    const-class v7, Lcom/onedrive/sdk/extensions/Item;

    invoke-interface {p1, v6, v7}, Lcom/onedrive/sdk/serializer/ISerializer;->deserializeObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/onedrive/sdk/extensions/Item;

    aput-object v6, v4, v5

    .line 148
    aget-object v6, v4, v5

    aget-object v7, v3, v5

    invoke-virtual {v6, p1, v7}, Lcom/onedrive/sdk/extensions/Item;->setRawObject(Lcom/onedrive/sdk/serializer/ISerializer;Lcom/google/gson/JsonObject;)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 150
    :cond_4
    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    iput-object v3, v0, Lcom/onedrive/sdk/generated/BaseItemCollectionResponse;->value:Ljava/util/List;

    .line 151
    new-instance v3, Lcom/onedrive/sdk/extensions/ItemCollectionPage;

    invoke-direct {v3, v0, v1}, Lcom/onedrive/sdk/extensions/ItemCollectionPage;-><init>(Lcom/onedrive/sdk/generated/BaseItemCollectionResponse;Lcom/onedrive/sdk/extensions/IItemCollectionRequestBuilder;)V

    iput-object v3, p0, Lcom/onedrive/sdk/generated/BaseDrive;->shared:Lcom/onedrive/sdk/extensions/ItemCollectionPage;

    :cond_5
    const-string v0, "special"

    .line 154
    invoke-virtual {p2, v0}, Lcom/google/gson/JsonObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 155
    new-instance v0, Lcom/onedrive/sdk/generated/BaseItemCollectionResponse;

    invoke-direct {v0}, Lcom/onedrive/sdk/generated/BaseItemCollectionResponse;-><init>()V

    const-string v3, "special@odata.nextLink"

    .line 156
    invoke-virtual {p2, v3}, Lcom/google/gson/JsonObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    const-string v3, "special@odata.nextLink"

    .line 157
    invoke-virtual {p2, v3}, Lcom/google/gson/JsonObject;->get(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/gson/JsonElement;->getAsString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/onedrive/sdk/generated/BaseItemCollectionResponse;->nextLink:Ljava/lang/String;

    :cond_6
    const-string v3, "special"

    .line 160
    invoke-virtual {p2, v3}, Lcom/google/gson/JsonObject;->get(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object p2

    invoke-virtual {p2}, Lcom/google/gson/JsonElement;->toString()Ljava/lang/String;

    move-result-object p2

    const-class v3, [Lcom/google/gson/JsonObject;

    invoke-interface {p1, p2, v3}, Lcom/onedrive/sdk/serializer/ISerializer;->deserializeObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, [Lcom/google/gson/JsonObject;

    .line 161
    array-length v3, p2

    new-array v3, v3, [Lcom/onedrive/sdk/extensions/Item;

    .line 162
    :goto_2
    array-length v4, p2

    if-ge v2, v4, :cond_7

    .line 163
    aget-object v4, p2, v2

    invoke-virtual {v4}, Lcom/google/gson/JsonObject;->toString()Ljava/lang/String;

    move-result-object v4

    const-class v5, Lcom/onedrive/sdk/extensions/Item;

    invoke-interface {p1, v4, v5}, Lcom/onedrive/sdk/serializer/ISerializer;->deserializeObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/onedrive/sdk/extensions/Item;

    aput-object v4, v3, v2

    .line 164
    aget-object v4, v3, v2

    aget-object v5, p2, v2

    invoke-virtual {v4, p1, v5}, Lcom/onedrive/sdk/extensions/Item;->setRawObject(Lcom/onedrive/sdk/serializer/ISerializer;Lcom/google/gson/JsonObject;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 166
    :cond_7
    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    iput-object p1, v0, Lcom/onedrive/sdk/generated/BaseItemCollectionResponse;->value:Ljava/util/List;

    .line 167
    new-instance p1, Lcom/onedrive/sdk/extensions/ItemCollectionPage;

    invoke-direct {p1, v0, v1}, Lcom/onedrive/sdk/extensions/ItemCollectionPage;-><init>(Lcom/onedrive/sdk/generated/BaseItemCollectionResponse;Lcom/onedrive/sdk/extensions/IItemCollectionRequestBuilder;)V

    iput-object p1, p0, Lcom/onedrive/sdk/generated/BaseDrive;->special:Lcom/onedrive/sdk/extensions/ItemCollectionPage;

    :cond_8
    return-void
.end method

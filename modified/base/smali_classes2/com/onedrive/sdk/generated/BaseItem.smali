.class public Lcom/onedrive/sdk/generated/BaseItem;
.super Ljava/lang/Object;
.source "BaseItem.java"

# interfaces
.implements Lcom/onedrive/sdk/serializer/IJsonBackedObject;


# instance fields
.field public audio:Lcom/onedrive/sdk/extensions/Audio;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "audio"
    .end annotation
.end field

.field public cTag:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cTag"
    .end annotation
.end field

.field public transient children:Lcom/onedrive/sdk/extensions/ItemCollectionPage;

.field public createdBy:Lcom/onedrive/sdk/extensions/IdentitySet;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "createdBy"
    .end annotation
.end field

.field public createdDateTime:Ljava/util/Calendar;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "createdDateTime"
    .end annotation
.end field

.field public deleted:Lcom/onedrive/sdk/extensions/Deleted;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "deleted"
    .end annotation
.end field

.field public description:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "description"
    .end annotation
.end field

.field public eTag:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "eTag"
    .end annotation
.end field

.field public file:Lcom/onedrive/sdk/extensions/File;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "file"
    .end annotation
.end field

.field public fileSystemInfo:Lcom/onedrive/sdk/extensions/FileSystemInfo;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "fileSystemInfo"
    .end annotation
.end field

.field public folder:Lcom/onedrive/sdk/extensions/Folder;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "folder"
    .end annotation
.end field

.field public id:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "id"
    .end annotation
.end field

.field public image:Lcom/onedrive/sdk/extensions/Image;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "image"
    .end annotation
.end field

.field public lastModifiedBy:Lcom/onedrive/sdk/extensions/IdentitySet;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "lastModifiedBy"
    .end annotation
.end field

.field public lastModifiedDateTime:Ljava/util/Calendar;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "lastModifiedDateTime"
    .end annotation
.end field

.field public location:Lcom/onedrive/sdk/extensions/Location;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "location"
    .end annotation
.end field

.field private transient mRawObject:Lcom/google/gson/JsonObject;

.field private transient mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;

.field public name:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "name"
    .end annotation
.end field

.field public openWith:Lcom/onedrive/sdk/extensions/OpenWithSet;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "openWith"
    .end annotation
.end field

.field public parentReference:Lcom/onedrive/sdk/extensions/ItemReference;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "parentReference"
    .end annotation
.end field

.field public transient permissions:Lcom/onedrive/sdk/extensions/PermissionCollectionPage;

.field public photo:Lcom/onedrive/sdk/extensions/Photo;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "photo"
    .end annotation
.end field

.field public remoteItem:Lcom/onedrive/sdk/extensions/Item;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "remoteItem"
    .end annotation
.end field

.field public searchResult:Lcom/onedrive/sdk/extensions/SearchResult;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "searchResult"
    .end annotation
.end field

.field public shared:Lcom/onedrive/sdk/extensions/Shared;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "shared"
    .end annotation
.end field

.field public size:Ljava/lang/Long;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "size"
    .end annotation
.end field

.field public specialFolder:Lcom/onedrive/sdk/extensions/SpecialFolder;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "specialFolder"
    .end annotation
.end field

.field public transient thumbnails:Lcom/onedrive/sdk/extensions/ThumbnailSetCollectionPage;

.field public transient versions:Lcom/onedrive/sdk/extensions/ItemCollectionPage;

.field public video:Lcom/onedrive/sdk/extensions/Video;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "video"
    .end annotation
.end field

.field public webUrl:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "webUrl"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getRawObject()Lcom/google/gson/JsonObject;
    .locals 1

    .line 237
    iget-object v0, p0, Lcom/onedrive/sdk/generated/BaseItem;->mRawObject:Lcom/google/gson/JsonObject;

    return-object v0
.end method

.method protected getSerializer()Lcom/onedrive/sdk/serializer/ISerializer;
    .locals 1

    .line 245
    iget-object v0, p0, Lcom/onedrive/sdk/generated/BaseItem;->mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;

    return-object v0
.end method

.method public setRawObject(Lcom/onedrive/sdk/serializer/ISerializer;Lcom/google/gson/JsonObject;)V
    .locals 8

    .line 255
    iput-object p1, p0, Lcom/onedrive/sdk/generated/BaseItem;->mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;

    .line 256
    iput-object p2, p0, Lcom/onedrive/sdk/generated/BaseItem;->mRawObject:Lcom/google/gson/JsonObject;

    const-string v0, "permissions"

    .line 259
    invoke-virtual {p2, v0}, Lcom/google/gson/JsonObject;->has(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    .line 260
    new-instance v0, Lcom/onedrive/sdk/generated/BasePermissionCollectionResponse;

    invoke-direct {v0}, Lcom/onedrive/sdk/generated/BasePermissionCollectionResponse;-><init>()V

    const-string v3, "permissions@odata.nextLink"

    .line 261
    invoke-virtual {p2, v3}, Lcom/google/gson/JsonObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "permissions@odata.nextLink"

    .line 262
    invoke-virtual {p2, v3}, Lcom/google/gson/JsonObject;->get(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/gson/JsonElement;->getAsString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/onedrive/sdk/generated/BasePermissionCollectionResponse;->nextLink:Ljava/lang/String;

    :cond_0
    const-string v3, "permissions"

    .line 265
    invoke-virtual {p2, v3}, Lcom/google/gson/JsonObject;->get(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/gson/JsonElement;->toString()Ljava/lang/String;

    move-result-object v3

    const-class v4, [Lcom/google/gson/JsonObject;

    invoke-interface {p1, v3, v4}, Lcom/onedrive/sdk/serializer/ISerializer;->deserializeObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lcom/google/gson/JsonObject;

    .line 266
    array-length v4, v3

    new-array v4, v4, [Lcom/onedrive/sdk/extensions/Permission;

    const/4 v5, 0x0

    .line 267
    :goto_0
    array-length v6, v3

    if-ge v5, v6, :cond_1

    .line 268
    aget-object v6, v3, v5

    invoke-virtual {v6}, Lcom/google/gson/JsonObject;->toString()Ljava/lang/String;

    move-result-object v6

    const-class v7, Lcom/onedrive/sdk/extensions/Permission;

    invoke-interface {p1, v6, v7}, Lcom/onedrive/sdk/serializer/ISerializer;->deserializeObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/onedrive/sdk/extensions/Permission;

    aput-object v6, v4, v5

    .line 269
    aget-object v6, v4, v5

    aget-object v7, v3, v5

    invoke-virtual {v6, p1, v7}, Lcom/onedrive/sdk/extensions/Permission;->setRawObject(Lcom/onedrive/sdk/serializer/ISerializer;Lcom/google/gson/JsonObject;)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 271
    :cond_1
    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    iput-object v3, v0, Lcom/onedrive/sdk/generated/BasePermissionCollectionResponse;->value:Ljava/util/List;

    .line 272
    new-instance v3, Lcom/onedrive/sdk/extensions/PermissionCollectionPage;

    invoke-direct {v3, v0, v1}, Lcom/onedrive/sdk/extensions/PermissionCollectionPage;-><init>(Lcom/onedrive/sdk/generated/BasePermissionCollectionResponse;Lcom/onedrive/sdk/extensions/IPermissionCollectionRequestBuilder;)V

    iput-object v3, p0, Lcom/onedrive/sdk/generated/BaseItem;->permissions:Lcom/onedrive/sdk/extensions/PermissionCollectionPage;

    :cond_2
    const-string v0, "versions"

    .line 275
    invoke-virtual {p2, v0}, Lcom/google/gson/JsonObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 276
    new-instance v0, Lcom/onedrive/sdk/generated/BaseItemCollectionResponse;

    invoke-direct {v0}, Lcom/onedrive/sdk/generated/BaseItemCollectionResponse;-><init>()V

    const-string v3, "versions@odata.nextLink"

    .line 277
    invoke-virtual {p2, v3}, Lcom/google/gson/JsonObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "versions@odata.nextLink"

    .line 278
    invoke-virtual {p2, v3}, Lcom/google/gson/JsonObject;->get(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/gson/JsonElement;->getAsString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/onedrive/sdk/generated/BaseItemCollectionResponse;->nextLink:Ljava/lang/String;

    :cond_3
    const-string v3, "versions"

    .line 281
    invoke-virtual {p2, v3}, Lcom/google/gson/JsonObject;->get(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/gson/JsonElement;->toString()Ljava/lang/String;

    move-result-object v3

    const-class v4, [Lcom/google/gson/JsonObject;

    invoke-interface {p1, v3, v4}, Lcom/onedrive/sdk/serializer/ISerializer;->deserializeObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lcom/google/gson/JsonObject;

    .line 282
    array-length v4, v3

    new-array v4, v4, [Lcom/onedrive/sdk/extensions/Item;

    const/4 v5, 0x0

    .line 283
    :goto_1
    array-length v6, v3

    if-ge v5, v6, :cond_4

    .line 284
    aget-object v6, v3, v5

    invoke-virtual {v6}, Lcom/google/gson/JsonObject;->toString()Ljava/lang/String;

    move-result-object v6

    const-class v7, Lcom/onedrive/sdk/extensions/Item;

    invoke-interface {p1, v6, v7}, Lcom/onedrive/sdk/serializer/ISerializer;->deserializeObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/onedrive/sdk/extensions/Item;

    aput-object v6, v4, v5

    .line 285
    aget-object v6, v4, v5

    aget-object v7, v3, v5

    invoke-virtual {v6, p1, v7}, Lcom/onedrive/sdk/extensions/Item;->setRawObject(Lcom/onedrive/sdk/serializer/ISerializer;Lcom/google/gson/JsonObject;)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 287
    :cond_4
    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    iput-object v3, v0, Lcom/onedrive/sdk/generated/BaseItemCollectionResponse;->value:Ljava/util/List;

    .line 288
    new-instance v3, Lcom/onedrive/sdk/extensions/ItemCollectionPage;

    invoke-direct {v3, v0, v1}, Lcom/onedrive/sdk/extensions/ItemCollectionPage;-><init>(Lcom/onedrive/sdk/generated/BaseItemCollectionResponse;Lcom/onedrive/sdk/extensions/IItemCollectionRequestBuilder;)V

    iput-object v3, p0, Lcom/onedrive/sdk/generated/BaseItem;->versions:Lcom/onedrive/sdk/extensions/ItemCollectionPage;

    :cond_5
    const-string v0, "children"

    .line 291
    invoke-virtual {p2, v0}, Lcom/google/gson/JsonObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 292
    new-instance v0, Lcom/onedrive/sdk/generated/BaseItemCollectionResponse;

    invoke-direct {v0}, Lcom/onedrive/sdk/generated/BaseItemCollectionResponse;-><init>()V

    const-string v3, "children@odata.nextLink"

    .line 293
    invoke-virtual {p2, v3}, Lcom/google/gson/JsonObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    const-string v3, "children@odata.nextLink"

    .line 294
    invoke-virtual {p2, v3}, Lcom/google/gson/JsonObject;->get(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/gson/JsonElement;->getAsString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/onedrive/sdk/generated/BaseItemCollectionResponse;->nextLink:Ljava/lang/String;

    :cond_6
    const-string v3, "children"

    .line 297
    invoke-virtual {p2, v3}, Lcom/google/gson/JsonObject;->get(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/gson/JsonElement;->toString()Ljava/lang/String;

    move-result-object v3

    const-class v4, [Lcom/google/gson/JsonObject;

    invoke-interface {p1, v3, v4}, Lcom/onedrive/sdk/serializer/ISerializer;->deserializeObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lcom/google/gson/JsonObject;

    .line 298
    array-length v4, v3

    new-array v4, v4, [Lcom/onedrive/sdk/extensions/Item;

    const/4 v5, 0x0

    .line 299
    :goto_2
    array-length v6, v3

    if-ge v5, v6, :cond_7

    .line 300
    aget-object v6, v3, v5

    invoke-virtual {v6}, Lcom/google/gson/JsonObject;->toString()Ljava/lang/String;

    move-result-object v6

    const-class v7, Lcom/onedrive/sdk/extensions/Item;

    invoke-interface {p1, v6, v7}, Lcom/onedrive/sdk/serializer/ISerializer;->deserializeObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/onedrive/sdk/extensions/Item;

    aput-object v6, v4, v5

    .line 301
    aget-object v6, v4, v5

    aget-object v7, v3, v5

    invoke-virtual {v6, p1, v7}, Lcom/onedrive/sdk/extensions/Item;->setRawObject(Lcom/onedrive/sdk/serializer/ISerializer;Lcom/google/gson/JsonObject;)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 303
    :cond_7
    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    iput-object v3, v0, Lcom/onedrive/sdk/generated/BaseItemCollectionResponse;->value:Ljava/util/List;

    .line 304
    new-instance v3, Lcom/onedrive/sdk/extensions/ItemCollectionPage;

    invoke-direct {v3, v0, v1}, Lcom/onedrive/sdk/extensions/ItemCollectionPage;-><init>(Lcom/onedrive/sdk/generated/BaseItemCollectionResponse;Lcom/onedrive/sdk/extensions/IItemCollectionRequestBuilder;)V

    iput-object v3, p0, Lcom/onedrive/sdk/generated/BaseItem;->children:Lcom/onedrive/sdk/extensions/ItemCollectionPage;

    :cond_8
    const-string v0, "thumbnails"

    .line 307
    invoke-virtual {p2, v0}, Lcom/google/gson/JsonObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 308
    new-instance v0, Lcom/onedrive/sdk/generated/BaseThumbnailSetCollectionResponse;

    invoke-direct {v0}, Lcom/onedrive/sdk/generated/BaseThumbnailSetCollectionResponse;-><init>()V

    const-string v3, "thumbnails@odata.nextLink"

    .line 309
    invoke-virtual {p2, v3}, Lcom/google/gson/JsonObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    const-string v3, "thumbnails@odata.nextLink"

    .line 310
    invoke-virtual {p2, v3}, Lcom/google/gson/JsonObject;->get(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/gson/JsonElement;->getAsString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/onedrive/sdk/generated/BaseThumbnailSetCollectionResponse;->nextLink:Ljava/lang/String;

    :cond_9
    const-string v3, "thumbnails"

    .line 313
    invoke-virtual {p2, v3}, Lcom/google/gson/JsonObject;->get(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object p2

    invoke-virtual {p2}, Lcom/google/gson/JsonElement;->toString()Ljava/lang/String;

    move-result-object p2

    const-class v3, [Lcom/google/gson/JsonObject;

    invoke-interface {p1, p2, v3}, Lcom/onedrive/sdk/serializer/ISerializer;->deserializeObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, [Lcom/google/gson/JsonObject;

    .line 314
    array-length v3, p2

    new-array v3, v3, [Lcom/onedrive/sdk/extensions/ThumbnailSet;

    .line 315
    :goto_3
    array-length v4, p2

    if-ge v2, v4, :cond_a

    .line 316
    aget-object v4, p2, v2

    invoke-virtual {v4}, Lcom/google/gson/JsonObject;->toString()Ljava/lang/String;

    move-result-object v4

    const-class v5, Lcom/onedrive/sdk/extensions/ThumbnailSet;

    invoke-interface {p1, v4, v5}, Lcom/onedrive/sdk/serializer/ISerializer;->deserializeObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/onedrive/sdk/extensions/ThumbnailSet;

    aput-object v4, v3, v2

    .line 317
    aget-object v4, v3, v2

    aget-object v5, p2, v2

    invoke-virtual {v4, p1, v5}, Lcom/onedrive/sdk/extensions/ThumbnailSet;->setRawObject(Lcom/onedrive/sdk/serializer/ISerializer;Lcom/google/gson/JsonObject;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 319
    :cond_a
    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    iput-object p1, v0, Lcom/onedrive/sdk/generated/BaseThumbnailSetCollectionResponse;->value:Ljava/util/List;

    .line 320
    new-instance p1, Lcom/onedrive/sdk/extensions/ThumbnailSetCollectionPage;

    invoke-direct {p1, v0, v1}, Lcom/onedrive/sdk/extensions/ThumbnailSetCollectionPage;-><init>(Lcom/onedrive/sdk/generated/BaseThumbnailSetCollectionResponse;Lcom/onedrive/sdk/extensions/IThumbnailSetCollectionRequestBuilder;)V

    iput-object p1, p0, Lcom/onedrive/sdk/generated/BaseItem;->thumbnails:Lcom/onedrive/sdk/extensions/ThumbnailSetCollectionPage;

    :cond_b
    return-void
.end method

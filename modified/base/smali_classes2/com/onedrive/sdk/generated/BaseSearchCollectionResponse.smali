.class public Lcom/onedrive/sdk/generated/BaseSearchCollectionResponse;
.super Ljava/lang/Object;
.source "BaseSearchCollectionResponse.java"

# interfaces
.implements Lcom/onedrive/sdk/serializer/IJsonBackedObject;


# instance fields
.field private transient mRawObject:Lcom/google/gson/JsonObject;

.field private transient mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;

.field public nextLink:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "@odata.nextLink"
    .end annotation
.end field

.field public value:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "value"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/extensions/Item;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getRawObject()Lcom/google/gson/JsonObject;
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/onedrive/sdk/generated/BaseSearchCollectionResponse;->mRawObject:Lcom/google/gson/JsonObject;

    return-object v0
.end method

.method protected getSerializer()Lcom/onedrive/sdk/serializer/ISerializer;
    .locals 1

    .line 75
    iget-object v0, p0, Lcom/onedrive/sdk/generated/BaseSearchCollectionResponse;->mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;

    return-object v0
.end method

.method public setRawObject(Lcom/onedrive/sdk/serializer/ISerializer;Lcom/google/gson/JsonObject;)V
    .locals 3

    .line 85
    iput-object p1, p0, Lcom/onedrive/sdk/generated/BaseSearchCollectionResponse;->mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;

    .line 86
    iput-object p2, p0, Lcom/onedrive/sdk/generated/BaseSearchCollectionResponse;->mRawObject:Lcom/google/gson/JsonObject;

    const-string p1, "value"

    .line 89
    invoke-virtual {p2, p1}, Lcom/google/gson/JsonObject;->has(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    const-string p1, "value"

    .line 90
    invoke-virtual {p2, p1}, Lcom/google/gson/JsonObject;->getAsJsonArray(Ljava/lang/String;)Lcom/google/gson/JsonArray;

    move-result-object p1

    const/4 p2, 0x0

    .line 91
    :goto_0
    invoke-virtual {p1}, Lcom/google/gson/JsonArray;->size()I

    move-result v0

    if-ge p2, v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/onedrive/sdk/generated/BaseSearchCollectionResponse;->value:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/onedrive/sdk/extensions/Item;

    iget-object v1, p0, Lcom/onedrive/sdk/generated/BaseSearchCollectionResponse;->mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;

    invoke-virtual {p1, p2}, Lcom/google/gson/JsonArray;->get(I)Lcom/google/gson/JsonElement;

    move-result-object v2

    check-cast v2, Lcom/google/gson/JsonObject;

    invoke-virtual {v0, v1, v2}, Lcom/onedrive/sdk/extensions/Item;->setRawObject(Lcom/onedrive/sdk/serializer/ISerializer;Lcom/google/gson/JsonObject;)V

    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

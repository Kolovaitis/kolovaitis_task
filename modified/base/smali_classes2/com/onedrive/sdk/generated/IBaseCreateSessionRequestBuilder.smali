.class public interface abstract Lcom/onedrive/sdk/generated/IBaseCreateSessionRequestBuilder;
.super Ljava/lang/Object;
.source "IBaseCreateSessionRequestBuilder.java"

# interfaces
.implements Lcom/onedrive/sdk/http/IRequestBuilder;


# virtual methods
.method public abstract buildRequest()Lcom/onedrive/sdk/extensions/ICreateSessionRequest;
.end method

.method public abstract buildRequest(Ljava/util/List;)Lcom/onedrive/sdk/extensions/ICreateSessionRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;)",
            "Lcom/onedrive/sdk/extensions/ICreateSessionRequest;"
        }
    .end annotation
.end method

.class public Lcom/onedrive/sdk/generated/BasePermissionCollectionRequest;
.super Lcom/onedrive/sdk/http/BaseCollectionRequest;
.source "BasePermissionCollectionRequest.java"

# interfaces
.implements Lcom/onedrive/sdk/generated/IBasePermissionCollectionRequest;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/onedrive/sdk/http/BaseCollectionRequest<",
        "Lcom/onedrive/sdk/generated/BasePermissionCollectionResponse;",
        "Lcom/onedrive/sdk/extensions/IPermissionCollectionPage;",
        ">;",
        "Lcom/onedrive/sdk/generated/IBasePermissionCollectionRequest;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/onedrive/sdk/extensions/IOneDriveClient;",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;)V"
        }
    .end annotation

    .line 51
    const-class v4, Lcom/onedrive/sdk/generated/BasePermissionCollectionResponse;

    const-class v5, Lcom/onedrive/sdk/extensions/IPermissionCollectionPage;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/onedrive/sdk/http/BaseCollectionRequest;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;Ljava/lang/Class;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public buildFromResponse(Lcom/onedrive/sdk/generated/BasePermissionCollectionResponse;)Lcom/onedrive/sdk/extensions/IPermissionCollectionPage;
    .locals 4

    .line 90
    iget-object v0, p1, Lcom/onedrive/sdk/generated/BasePermissionCollectionResponse;->nextLink:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 91
    new-instance v0, Lcom/onedrive/sdk/extensions/PermissionCollectionRequestBuilder;

    iget-object v2, p1, Lcom/onedrive/sdk/generated/BasePermissionCollectionResponse;->nextLink:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BasePermissionCollectionRequest;->getBaseRequest()Lcom/onedrive/sdk/http/BaseRequest;

    move-result-object v3

    invoke-virtual {v3}, Lcom/onedrive/sdk/http/BaseRequest;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v3

    invoke-direct {v0, v2, v3, v1}, Lcom/onedrive/sdk/extensions/PermissionCollectionRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V

    goto :goto_0

    :cond_0
    move-object v0, v1

    .line 95
    :goto_0
    new-instance v1, Lcom/onedrive/sdk/extensions/PermissionCollectionPage;

    invoke-direct {v1, p1, v0}, Lcom/onedrive/sdk/extensions/PermissionCollectionPage;-><init>(Lcom/onedrive/sdk/generated/BasePermissionCollectionResponse;Lcom/onedrive/sdk/extensions/IPermissionCollectionRequestBuilder;)V

    .line 96
    invoke-virtual {p1}, Lcom/onedrive/sdk/generated/BasePermissionCollectionResponse;->getSerializer()Lcom/onedrive/sdk/serializer/ISerializer;

    move-result-object v0

    invoke-virtual {p1}, Lcom/onedrive/sdk/generated/BasePermissionCollectionResponse;->getRawObject()Lcom/google/gson/JsonObject;

    move-result-object p1

    invoke-virtual {v1, v0, p1}, Lcom/onedrive/sdk/extensions/PermissionCollectionPage;->setRawObject(Lcom/onedrive/sdk/serializer/ISerializer;Lcom/google/gson/JsonObject;)V

    return-object v1
.end method

.method public expand(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IPermissionCollectionRequest;
    .locals 2

    .line 74
    new-instance v0, Lcom/onedrive/sdk/options/QueryOption;

    const-string v1, "expand"

    invoke-direct {v0, v1, p1}, Lcom/onedrive/sdk/options/QueryOption;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/onedrive/sdk/generated/BasePermissionCollectionRequest;->addQueryOption(Lcom/onedrive/sdk/options/QueryOption;)V

    .line 75
    move-object p1, p0

    check-cast p1, Lcom/onedrive/sdk/extensions/PermissionCollectionRequest;

    return-object p1
.end method

.method public get()Lcom/onedrive/sdk/extensions/IPermissionCollectionPage;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    .line 69
    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BasePermissionCollectionRequest;->send()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/onedrive/sdk/generated/BasePermissionCollectionResponse;

    .line 70
    invoke-virtual {p0, v0}, Lcom/onedrive/sdk/generated/BasePermissionCollectionRequest;->buildFromResponse(Lcom/onedrive/sdk/generated/BasePermissionCollectionResponse;)Lcom/onedrive/sdk/extensions/IPermissionCollectionPage;

    move-result-object v0

    return-object v0
.end method

.method public get(Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/extensions/IPermissionCollectionPage;",
            ">;)V"
        }
    .end annotation

    .line 55
    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BasePermissionCollectionRequest;->getBaseRequest()Lcom/onedrive/sdk/http/BaseRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/onedrive/sdk/http/BaseRequest;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v0

    invoke-interface {v0}, Lcom/onedrive/sdk/extensions/IOneDriveClient;->getExecutors()Lcom/onedrive/sdk/concurrency/IExecutors;

    move-result-object v0

    .line 56
    new-instance v1, Lcom/onedrive/sdk/generated/BasePermissionCollectionRequest$1;

    invoke-direct {v1, p0, v0, p1}, Lcom/onedrive/sdk/generated/BasePermissionCollectionRequest$1;-><init>(Lcom/onedrive/sdk/generated/BasePermissionCollectionRequest;Lcom/onedrive/sdk/concurrency/IExecutors;Lcom/onedrive/sdk/concurrency/ICallback;)V

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/concurrency/IExecutors;->performOnBackground(Ljava/lang/Runnable;)V

    return-void
.end method

.method public select(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IPermissionCollectionRequest;
    .locals 2

    .line 79
    new-instance v0, Lcom/onedrive/sdk/options/QueryOption;

    const-string v1, "select"

    invoke-direct {v0, v1, p1}, Lcom/onedrive/sdk/options/QueryOption;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/onedrive/sdk/generated/BasePermissionCollectionRequest;->addQueryOption(Lcom/onedrive/sdk/options/QueryOption;)V

    .line 80
    move-object p1, p0

    check-cast p1, Lcom/onedrive/sdk/extensions/PermissionCollectionRequest;

    return-object p1
.end method

.method public top(I)Lcom/onedrive/sdk/extensions/IPermissionCollectionRequest;
    .locals 3

    .line 84
    new-instance v0, Lcom/onedrive/sdk/options/QueryOption;

    const-string v1, "top"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ""

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/onedrive/sdk/options/QueryOption;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/onedrive/sdk/generated/BasePermissionCollectionRequest;->addQueryOption(Lcom/onedrive/sdk/options/QueryOption;)V

    .line 85
    move-object p1, p0

    check-cast p1, Lcom/onedrive/sdk/extensions/PermissionCollectionRequest;

    return-object p1
.end method

.class public interface abstract Lcom/onedrive/sdk/generated/IBaseDeltaRequest;
.super Ljava/lang/Object;
.source "IBaseDeltaRequest.java"


# virtual methods
.method public abstract expand(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IDeltaRequest;
.end method

.method public abstract get()Lcom/onedrive/sdk/extensions/IDeltaCollectionPage;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation
.end method

.method public abstract get(Lcom/onedrive/sdk/concurrency/ICallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/extensions/IDeltaCollectionPage;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract select(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IDeltaRequest;
.end method

.method public abstract top(I)Lcom/onedrive/sdk/extensions/IDeltaRequest;
.end method

.class public interface abstract Lcom/onedrive/sdk/generated/IBaseItemStreamRequestBuilder;
.super Ljava/lang/Object;
.source "IBaseItemStreamRequestBuilder.java"

# interfaces
.implements Lcom/onedrive/sdk/http/IRequestBuilder;


# virtual methods
.method public abstract buildRequest()Lcom/onedrive/sdk/extensions/IItemStreamRequest;
.end method

.method public abstract buildRequest(Ljava/util/List;)Lcom/onedrive/sdk/extensions/IItemStreamRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;)",
            "Lcom/onedrive/sdk/extensions/IItemStreamRequest;"
        }
    .end annotation
.end method

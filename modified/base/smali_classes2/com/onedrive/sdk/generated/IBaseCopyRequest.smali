.class public interface abstract Lcom/onedrive/sdk/generated/IBaseCopyRequest;
.super Ljava/lang/Object;
.source "IBaseCopyRequest.java"


# virtual methods
.method public abstract create()Lcom/onedrive/sdk/concurrency/AsyncMonitor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/onedrive/sdk/concurrency/AsyncMonitor<",
            "Lcom/onedrive/sdk/extensions/Item;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract create(Lcom/onedrive/sdk/concurrency/ICallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/concurrency/AsyncMonitor<",
            "Lcom/onedrive/sdk/extensions/Item;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract expand(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/ICopyRequest;
.end method

.method public abstract post()Lcom/onedrive/sdk/concurrency/AsyncMonitor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/onedrive/sdk/concurrency/AsyncMonitor<",
            "Lcom/onedrive/sdk/extensions/Item;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation
.end method

.method public abstract post(Lcom/onedrive/sdk/concurrency/ICallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/concurrency/AsyncMonitor<",
            "Lcom/onedrive/sdk/extensions/Item;",
            ">;>;)V"
        }
    .end annotation
.end method

.method public abstract select(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/ICopyRequest;
.end method

.method public abstract top(I)Lcom/onedrive/sdk/extensions/ICopyRequest;
.end method

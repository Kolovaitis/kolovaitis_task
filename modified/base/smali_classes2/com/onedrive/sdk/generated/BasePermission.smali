.class public Lcom/onedrive/sdk/generated/BasePermission;
.super Ljava/lang/Object;
.source "BasePermission.java"

# interfaces
.implements Lcom/onedrive/sdk/serializer/IJsonBackedObject;


# instance fields
.field public grantedTo:Lcom/onedrive/sdk/extensions/IdentitySet;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "grantedTo"
    .end annotation
.end field

.field public id:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "id"
    .end annotation
.end field

.field public inheritedFrom:Lcom/onedrive/sdk/extensions/ItemReference;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "inheritedFrom"
    .end annotation
.end field

.field public invitation:Lcom/onedrive/sdk/extensions/SharingInvitation;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "invitation"
    .end annotation
.end field

.field public link:Lcom/onedrive/sdk/extensions/SharingLink;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "link"
    .end annotation
.end field

.field private transient mRawObject:Lcom/google/gson/JsonObject;

.field private transient mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;

.field public roles:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "roles"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public shareId:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "shareId"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getRawObject()Lcom/google/gson/JsonObject;
    .locals 1

    .line 103
    iget-object v0, p0, Lcom/onedrive/sdk/generated/BasePermission;->mRawObject:Lcom/google/gson/JsonObject;

    return-object v0
.end method

.method protected getSerializer()Lcom/onedrive/sdk/serializer/ISerializer;
    .locals 1

    .line 111
    iget-object v0, p0, Lcom/onedrive/sdk/generated/BasePermission;->mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;

    return-object v0
.end method

.method public setRawObject(Lcom/onedrive/sdk/serializer/ISerializer;Lcom/google/gson/JsonObject;)V
    .locals 0

    .line 121
    iput-object p1, p0, Lcom/onedrive/sdk/generated/BasePermission;->mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;

    .line 122
    iput-object p2, p0, Lcom/onedrive/sdk/generated/BasePermission;->mRawObject:Lcom/google/gson/JsonObject;

    return-void
.end method

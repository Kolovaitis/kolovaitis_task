.class public Lcom/onedrive/sdk/generated/BaseSpecialFolder;
.super Ljava/lang/Object;
.source "BaseSpecialFolder.java"

# interfaces
.implements Lcom/onedrive/sdk/serializer/IJsonBackedObject;


# instance fields
.field private transient mRawObject:Lcom/google/gson/JsonObject;

.field private transient mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;

.field public name:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "name"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getRawObject()Lcom/google/gson/JsonObject;
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/onedrive/sdk/generated/BaseSpecialFolder;->mRawObject:Lcom/google/gson/JsonObject;

    return-object v0
.end method

.method protected getSerializer()Lcom/onedrive/sdk/serializer/ISerializer;
    .locals 1

    .line 75
    iget-object v0, p0, Lcom/onedrive/sdk/generated/BaseSpecialFolder;->mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;

    return-object v0
.end method

.method public setRawObject(Lcom/onedrive/sdk/serializer/ISerializer;Lcom/google/gson/JsonObject;)V
    .locals 0

    .line 85
    iput-object p1, p0, Lcom/onedrive/sdk/generated/BaseSpecialFolder;->mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;

    .line 86
    iput-object p2, p0, Lcom/onedrive/sdk/generated/BaseSpecialFolder;->mRawObject:Lcom/google/gson/JsonObject;

    return-void
.end method

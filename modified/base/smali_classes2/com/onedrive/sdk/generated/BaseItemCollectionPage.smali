.class public Lcom/onedrive/sdk/generated/BaseItemCollectionPage;
.super Lcom/onedrive/sdk/http/BaseCollectionPage;
.source "BaseItemCollectionPage.java"

# interfaces
.implements Lcom/onedrive/sdk/generated/IBaseItemCollectionPage;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/onedrive/sdk/http/BaseCollectionPage<",
        "Lcom/onedrive/sdk/extensions/Item;",
        "Lcom/onedrive/sdk/extensions/IItemCollectionRequestBuilder;",
        ">;",
        "Lcom/onedrive/sdk/generated/IBaseItemCollectionPage;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/onedrive/sdk/generated/BaseItemCollectionResponse;Lcom/onedrive/sdk/extensions/IItemCollectionRequestBuilder;)V
    .locals 0

    .line 53
    iget-object p1, p1, Lcom/onedrive/sdk/generated/BaseItemCollectionResponse;->value:Ljava/util/List;

    invoke-direct {p0, p1, p2}, Lcom/onedrive/sdk/http/BaseCollectionPage;-><init>(Ljava/util/List;Lcom/onedrive/sdk/http/IRequestBuilder;)V

    return-void
.end method

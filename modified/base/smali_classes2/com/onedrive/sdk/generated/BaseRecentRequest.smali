.class public Lcom/onedrive/sdk/generated/BaseRecentRequest;
.super Lcom/onedrive/sdk/http/BaseCollectionRequest;
.source "BaseRecentRequest.java"

# interfaces
.implements Lcom/onedrive/sdk/generated/IBaseRecentRequest;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/onedrive/sdk/http/BaseCollectionRequest<",
        "Lcom/onedrive/sdk/generated/BaseRecentCollectionResponse;",
        "Lcom/onedrive/sdk/extensions/IRecentCollectionPage;",
        ">;",
        "Lcom/onedrive/sdk/generated/IBaseRecentRequest;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/onedrive/sdk/extensions/IOneDriveClient;",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;)V"
        }
    .end annotation

    .line 50
    const-class v4, Lcom/onedrive/sdk/generated/BaseRecentCollectionResponse;

    const-class v5, Lcom/onedrive/sdk/extensions/IRecentCollectionPage;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/onedrive/sdk/http/BaseCollectionRequest;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;Ljava/lang/Class;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public buildFromResponse(Lcom/onedrive/sdk/generated/BaseRecentCollectionResponse;)Lcom/onedrive/sdk/extensions/IRecentCollectionPage;
    .locals 4

    .line 89
    iget-object v0, p1, Lcom/onedrive/sdk/generated/BaseRecentCollectionResponse;->nextLink:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 90
    new-instance v0, Lcom/onedrive/sdk/extensions/RecentRequestBuilder;

    iget-object v2, p1, Lcom/onedrive/sdk/generated/BaseRecentCollectionResponse;->nextLink:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseRecentRequest;->getBaseRequest()Lcom/onedrive/sdk/http/BaseRequest;

    move-result-object v3

    invoke-virtual {v3}, Lcom/onedrive/sdk/http/BaseRequest;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v3

    invoke-direct {v0, v2, v3, v1}, Lcom/onedrive/sdk/extensions/RecentRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V

    goto :goto_0

    :cond_0
    move-object v0, v1

    .line 94
    :goto_0
    new-instance v1, Lcom/onedrive/sdk/extensions/RecentCollectionPage;

    invoke-direct {v1, p1, v0}, Lcom/onedrive/sdk/extensions/RecentCollectionPage;-><init>(Lcom/onedrive/sdk/generated/BaseRecentCollectionResponse;Lcom/onedrive/sdk/extensions/IRecentRequestBuilder;)V

    .line 95
    invoke-virtual {p1}, Lcom/onedrive/sdk/generated/BaseRecentCollectionResponse;->getSerializer()Lcom/onedrive/sdk/serializer/ISerializer;

    move-result-object v0

    invoke-virtual {p1}, Lcom/onedrive/sdk/generated/BaseRecentCollectionResponse;->getRawObject()Lcom/google/gson/JsonObject;

    move-result-object p1

    invoke-interface {v1, v0, p1}, Lcom/onedrive/sdk/extensions/IRecentCollectionPage;->setRawObject(Lcom/onedrive/sdk/serializer/ISerializer;Lcom/google/gson/JsonObject;)V

    return-object v1
.end method

.method public expand(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IRecentRequest;
    .locals 2

    .line 83
    new-instance v0, Lcom/onedrive/sdk/options/QueryOption;

    const-string v1, "expand"

    invoke-direct {v0, v1, p1}, Lcom/onedrive/sdk/options/QueryOption;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/onedrive/sdk/generated/BaseRecentRequest;->addQueryOption(Lcom/onedrive/sdk/options/QueryOption;)V

    .line 84
    move-object p1, p0

    check-cast p1, Lcom/onedrive/sdk/extensions/RecentRequest;

    return-object p1
.end method

.method public get()Lcom/onedrive/sdk/extensions/IRecentCollectionPage;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    .line 68
    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseRecentRequest;->send()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/onedrive/sdk/generated/BaseRecentCollectionResponse;

    .line 69
    invoke-virtual {p0, v0}, Lcom/onedrive/sdk/generated/BaseRecentRequest;->buildFromResponse(Lcom/onedrive/sdk/generated/BaseRecentCollectionResponse;)Lcom/onedrive/sdk/extensions/IRecentCollectionPage;

    move-result-object v0

    return-object v0
.end method

.method public get(Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/extensions/IRecentCollectionPage;",
            ">;)V"
        }
    .end annotation

    .line 54
    invoke-virtual {p0}, Lcom/onedrive/sdk/generated/BaseRecentRequest;->getBaseRequest()Lcom/onedrive/sdk/http/BaseRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/onedrive/sdk/http/BaseRequest;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v0

    invoke-interface {v0}, Lcom/onedrive/sdk/extensions/IOneDriveClient;->getExecutors()Lcom/onedrive/sdk/concurrency/IExecutors;

    move-result-object v0

    .line 55
    new-instance v1, Lcom/onedrive/sdk/generated/BaseRecentRequest$1;

    invoke-direct {v1, p0, v0, p1}, Lcom/onedrive/sdk/generated/BaseRecentRequest$1;-><init>(Lcom/onedrive/sdk/generated/BaseRecentRequest;Lcom/onedrive/sdk/concurrency/IExecutors;Lcom/onedrive/sdk/concurrency/ICallback;)V

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/concurrency/IExecutors;->performOnBackground(Ljava/lang/Runnable;)V

    return-void
.end method

.method public select(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IRecentRequest;
    .locals 2

    .line 73
    new-instance v0, Lcom/onedrive/sdk/options/QueryOption;

    const-string v1, "select"

    invoke-direct {v0, v1, p1}, Lcom/onedrive/sdk/options/QueryOption;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/onedrive/sdk/generated/BaseRecentRequest;->addQueryOption(Lcom/onedrive/sdk/options/QueryOption;)V

    .line 74
    move-object p1, p0

    check-cast p1, Lcom/onedrive/sdk/extensions/RecentRequest;

    return-object p1
.end method

.method public top(I)Lcom/onedrive/sdk/extensions/IRecentRequest;
    .locals 3

    .line 78
    new-instance v0, Lcom/onedrive/sdk/options/QueryOption;

    const-string v1, "top"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ""

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/onedrive/sdk/options/QueryOption;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/onedrive/sdk/generated/BaseRecentRequest;->addQueryOption(Lcom/onedrive/sdk/options/QueryOption;)V

    .line 79
    move-object p1, p0

    check-cast p1, Lcom/onedrive/sdk/extensions/RecentRequest;

    return-object p1
.end method

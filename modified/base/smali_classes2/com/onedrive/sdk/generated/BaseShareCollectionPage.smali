.class public Lcom/onedrive/sdk/generated/BaseShareCollectionPage;
.super Lcom/onedrive/sdk/http/BaseCollectionPage;
.source "BaseShareCollectionPage.java"

# interfaces
.implements Lcom/onedrive/sdk/generated/IBaseShareCollectionPage;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/onedrive/sdk/http/BaseCollectionPage<",
        "Lcom/onedrive/sdk/extensions/Share;",
        "Lcom/onedrive/sdk/extensions/IShareCollectionRequestBuilder;",
        ">;",
        "Lcom/onedrive/sdk/generated/IBaseShareCollectionPage;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/onedrive/sdk/generated/BaseShareCollectionResponse;Lcom/onedrive/sdk/extensions/IShareCollectionRequestBuilder;)V
    .locals 0

    .line 53
    iget-object p1, p1, Lcom/onedrive/sdk/generated/BaseShareCollectionResponse;->value:Ljava/util/List;

    invoke-direct {p0, p1, p2}, Lcom/onedrive/sdk/http/BaseCollectionPage;-><init>(Ljava/util/List;Lcom/onedrive/sdk/http/IRequestBuilder;)V

    return-void
.end method

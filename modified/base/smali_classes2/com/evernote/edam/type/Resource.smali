.class public Lcom/evernote/edam/type/Resource;
.super Ljava/lang/Object;
.source "Resource.java"

# interfaces
.implements Lcom/evernote/thrift/TBase;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/evernote/thrift/TBase<",
        "Lcom/evernote/edam/type/Resource;",
        ">;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# static fields
.field private static final ACTIVE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final ALTERNATE_DATA_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final ATTRIBUTES_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final DATA_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final DURATION_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final GUID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final HEIGHT_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final MIME_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final NOTE_GUID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final RECOGNITION_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

.field private static final UPDATE_SEQUENCE_NUM_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final WIDTH_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final __ACTIVE_ISSET_ID:I = 0x3

.field private static final __DURATION_ISSET_ID:I = 0x2

.field private static final __HEIGHT_ISSET_ID:I = 0x1

.field private static final __UPDATESEQUENCENUM_ISSET_ID:I = 0x4

.field private static final __WIDTH_ISSET_ID:I


# instance fields
.field private __isset_vector:[Z

.field private active:Z

.field private alternateData:Lcom/evernote/edam/type/Data;

.field private attributes:Lcom/evernote/edam/type/ResourceAttributes;

.field private data:Lcom/evernote/edam/type/Data;

.field private duration:S

.field private guid:Ljava/lang/String;

.field private height:S

.field private mime:Ljava/lang/String;

.field private noteGuid:Ljava/lang/String;

.field private recognition:Lcom/evernote/edam/type/Data;

.field private updateSequenceNum:I

.field private width:S


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 99
    new-instance v0, Lcom/evernote/thrift/protocol/TStruct;

    const-string v1, "Resource"

    invoke-direct {v0, v1}, Lcom/evernote/thrift/protocol/TStruct;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/evernote/edam/type/Resource;->STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

    .line 101
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "guid"

    const/16 v2, 0xb

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Resource;->GUID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 102
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "noteGuid"

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Resource;->NOTE_GUID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 103
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "data"

    const/16 v4, 0xc

    const/4 v5, 0x3

    invoke-direct {v0, v1, v4, v5}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Resource;->DATA_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 104
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "mime"

    const/4 v5, 0x4

    invoke-direct {v0, v1, v2, v5}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Resource;->MIME_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 105
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "width"

    const/4 v5, 0x6

    const/4 v6, 0x5

    invoke-direct {v0, v1, v5, v6}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Resource;->WIDTH_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 106
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "height"

    invoke-direct {v0, v1, v5, v5}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Resource;->HEIGHT_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 107
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "duration"

    const/4 v6, 0x7

    invoke-direct {v0, v1, v5, v6}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Resource;->DURATION_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 108
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "active"

    const/16 v5, 0x8

    invoke-direct {v0, v1, v3, v5}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Resource;->ACTIVE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 109
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "recognition"

    const/16 v3, 0x9

    invoke-direct {v0, v1, v4, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Resource;->RECOGNITION_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 110
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "attributes"

    invoke-direct {v0, v1, v4, v2}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Resource;->ATTRIBUTES_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 111
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "updateSequenceNum"

    invoke-direct {v0, v1, v5, v4}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Resource;->UPDATE_SEQUENCE_NUM_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 112
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "alternateData"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v4, v2}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Resource;->ALTERNATE_DATA_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x5

    .line 134
    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/evernote/edam/type/Resource;->__isset_vector:[Z

    return-void
.end method

.method public constructor <init>(Lcom/evernote/edam/type/Resource;)V
    .locals 4

    .line 142
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x5

    .line 134
    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/evernote/edam/type/Resource;->__isset_vector:[Z

    .line 143
    iget-object v0, p1, Lcom/evernote/edam/type/Resource;->__isset_vector:[Z

    iget-object v1, p0, Lcom/evernote/edam/type/Resource;->__isset_vector:[Z

    array-length v2, v0

    const/4 v3, 0x0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 144
    invoke-virtual {p1}, Lcom/evernote/edam/type/Resource;->isSetGuid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p1, Lcom/evernote/edam/type/Resource;->guid:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/type/Resource;->guid:Ljava/lang/String;

    .line 147
    :cond_0
    invoke-virtual {p1}, Lcom/evernote/edam/type/Resource;->isSetNoteGuid()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 148
    iget-object v0, p1, Lcom/evernote/edam/type/Resource;->noteGuid:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/type/Resource;->noteGuid:Ljava/lang/String;

    .line 150
    :cond_1
    invoke-virtual {p1}, Lcom/evernote/edam/type/Resource;->isSetData()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 151
    new-instance v0, Lcom/evernote/edam/type/Data;

    iget-object v1, p1, Lcom/evernote/edam/type/Resource;->data:Lcom/evernote/edam/type/Data;

    invoke-direct {v0, v1}, Lcom/evernote/edam/type/Data;-><init>(Lcom/evernote/edam/type/Data;)V

    iput-object v0, p0, Lcom/evernote/edam/type/Resource;->data:Lcom/evernote/edam/type/Data;

    .line 153
    :cond_2
    invoke-virtual {p1}, Lcom/evernote/edam/type/Resource;->isSetMime()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 154
    iget-object v0, p1, Lcom/evernote/edam/type/Resource;->mime:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/type/Resource;->mime:Ljava/lang/String;

    .line 156
    :cond_3
    iget-short v0, p1, Lcom/evernote/edam/type/Resource;->width:S

    iput-short v0, p0, Lcom/evernote/edam/type/Resource;->width:S

    .line 157
    iget-short v0, p1, Lcom/evernote/edam/type/Resource;->height:S

    iput-short v0, p0, Lcom/evernote/edam/type/Resource;->height:S

    .line 158
    iget-short v0, p1, Lcom/evernote/edam/type/Resource;->duration:S

    iput-short v0, p0, Lcom/evernote/edam/type/Resource;->duration:S

    .line 159
    iget-boolean v0, p1, Lcom/evernote/edam/type/Resource;->active:Z

    iput-boolean v0, p0, Lcom/evernote/edam/type/Resource;->active:Z

    .line 160
    invoke-virtual {p1}, Lcom/evernote/edam/type/Resource;->isSetRecognition()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 161
    new-instance v0, Lcom/evernote/edam/type/Data;

    iget-object v1, p1, Lcom/evernote/edam/type/Resource;->recognition:Lcom/evernote/edam/type/Data;

    invoke-direct {v0, v1}, Lcom/evernote/edam/type/Data;-><init>(Lcom/evernote/edam/type/Data;)V

    iput-object v0, p0, Lcom/evernote/edam/type/Resource;->recognition:Lcom/evernote/edam/type/Data;

    .line 163
    :cond_4
    invoke-virtual {p1}, Lcom/evernote/edam/type/Resource;->isSetAttributes()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 164
    new-instance v0, Lcom/evernote/edam/type/ResourceAttributes;

    iget-object v1, p1, Lcom/evernote/edam/type/Resource;->attributes:Lcom/evernote/edam/type/ResourceAttributes;

    invoke-direct {v0, v1}, Lcom/evernote/edam/type/ResourceAttributes;-><init>(Lcom/evernote/edam/type/ResourceAttributes;)V

    iput-object v0, p0, Lcom/evernote/edam/type/Resource;->attributes:Lcom/evernote/edam/type/ResourceAttributes;

    .line 166
    :cond_5
    iget v0, p1, Lcom/evernote/edam/type/Resource;->updateSequenceNum:I

    iput v0, p0, Lcom/evernote/edam/type/Resource;->updateSequenceNum:I

    .line 167
    invoke-virtual {p1}, Lcom/evernote/edam/type/Resource;->isSetAlternateData()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 168
    new-instance v0, Lcom/evernote/edam/type/Data;

    iget-object p1, p1, Lcom/evernote/edam/type/Resource;->alternateData:Lcom/evernote/edam/type/Data;

    invoke-direct {v0, p1}, Lcom/evernote/edam/type/Data;-><init>(Lcom/evernote/edam/type/Data;)V

    iput-object v0, p0, Lcom/evernote/edam/type/Resource;->alternateData:Lcom/evernote/edam/type/Data;

    :cond_6
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 2

    const/4 v0, 0x0

    .line 177
    iput-object v0, p0, Lcom/evernote/edam/type/Resource;->guid:Ljava/lang/String;

    .line 178
    iput-object v0, p0, Lcom/evernote/edam/type/Resource;->noteGuid:Ljava/lang/String;

    .line 179
    iput-object v0, p0, Lcom/evernote/edam/type/Resource;->data:Lcom/evernote/edam/type/Data;

    .line 180
    iput-object v0, p0, Lcom/evernote/edam/type/Resource;->mime:Ljava/lang/String;

    const/4 v1, 0x0

    .line 181
    invoke-virtual {p0, v1}, Lcom/evernote/edam/type/Resource;->setWidthIsSet(Z)V

    .line 182
    iput-short v1, p0, Lcom/evernote/edam/type/Resource;->width:S

    .line 183
    invoke-virtual {p0, v1}, Lcom/evernote/edam/type/Resource;->setHeightIsSet(Z)V

    .line 184
    iput-short v1, p0, Lcom/evernote/edam/type/Resource;->height:S

    .line 185
    invoke-virtual {p0, v1}, Lcom/evernote/edam/type/Resource;->setDurationIsSet(Z)V

    .line 186
    iput-short v1, p0, Lcom/evernote/edam/type/Resource;->duration:S

    .line 187
    invoke-virtual {p0, v1}, Lcom/evernote/edam/type/Resource;->setActiveIsSet(Z)V

    .line 188
    iput-boolean v1, p0, Lcom/evernote/edam/type/Resource;->active:Z

    .line 189
    iput-object v0, p0, Lcom/evernote/edam/type/Resource;->recognition:Lcom/evernote/edam/type/Data;

    .line 190
    iput-object v0, p0, Lcom/evernote/edam/type/Resource;->attributes:Lcom/evernote/edam/type/ResourceAttributes;

    .line 191
    invoke-virtual {p0, v1}, Lcom/evernote/edam/type/Resource;->setUpdateSequenceNumIsSet(Z)V

    .line 192
    iput v1, p0, Lcom/evernote/edam/type/Resource;->updateSequenceNum:I

    .line 193
    iput-object v0, p0, Lcom/evernote/edam/type/Resource;->alternateData:Lcom/evernote/edam/type/Data;

    return-void
.end method

.method public compareTo(Lcom/evernote/edam/type/Resource;)I
    .locals 2

    .line 597
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 598
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    return p1

    .line 604
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetGuid()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Resource;->isSetGuid()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_1

    return v0

    .line 608
    :cond_1
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetGuid()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->guid:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/type/Resource;->guid:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_2

    return v0

    .line 613
    :cond_2
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetNoteGuid()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Resource;->isSetNoteGuid()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_3

    return v0

    .line 617
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetNoteGuid()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->noteGuid:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/type/Resource;->noteGuid:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_4

    return v0

    .line 622
    :cond_4
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetData()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Resource;->isSetData()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_5

    return v0

    .line 626
    :cond_5
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetData()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->data:Lcom/evernote/edam/type/Data;

    iget-object v1, p1, Lcom/evernote/edam/type/Resource;->data:Lcom/evernote/edam/type/Data;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result v0

    if-eqz v0, :cond_6

    return v0

    .line 631
    :cond_6
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetMime()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Resource;->isSetMime()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_7

    return v0

    .line 635
    :cond_7
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetMime()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->mime:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/type/Resource;->mime:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_8

    return v0

    .line 640
    :cond_8
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetWidth()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Resource;->isSetWidth()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_9

    return v0

    .line 644
    :cond_9
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetWidth()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-short v0, p0, Lcom/evernote/edam/type/Resource;->width:S

    iget-short v1, p1, Lcom/evernote/edam/type/Resource;->width:S

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(SS)I

    move-result v0

    if-eqz v0, :cond_a

    return v0

    .line 649
    :cond_a
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetHeight()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Resource;->isSetHeight()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_b

    return v0

    .line 653
    :cond_b
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetHeight()Z

    move-result v0

    if-eqz v0, :cond_c

    iget-short v0, p0, Lcom/evernote/edam/type/Resource;->height:S

    iget-short v1, p1, Lcom/evernote/edam/type/Resource;->height:S

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(SS)I

    move-result v0

    if-eqz v0, :cond_c

    return v0

    .line 658
    :cond_c
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetDuration()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Resource;->isSetDuration()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_d

    return v0

    .line 662
    :cond_d
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetDuration()Z

    move-result v0

    if-eqz v0, :cond_e

    iget-short v0, p0, Lcom/evernote/edam/type/Resource;->duration:S

    iget-short v1, p1, Lcom/evernote/edam/type/Resource;->duration:S

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(SS)I

    move-result v0

    if-eqz v0, :cond_e

    return v0

    .line 667
    :cond_e
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetActive()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Resource;->isSetActive()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_f

    return v0

    .line 671
    :cond_f
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetActive()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-boolean v0, p0, Lcom/evernote/edam/type/Resource;->active:Z

    iget-boolean v1, p1, Lcom/evernote/edam/type/Resource;->active:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_10

    return v0

    .line 676
    :cond_10
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetRecognition()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Resource;->isSetRecognition()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_11

    return v0

    .line 680
    :cond_11
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetRecognition()Z

    move-result v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->recognition:Lcom/evernote/edam/type/Data;

    iget-object v1, p1, Lcom/evernote/edam/type/Resource;->recognition:Lcom/evernote/edam/type/Data;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result v0

    if-eqz v0, :cond_12

    return v0

    .line 685
    :cond_12
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetAttributes()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Resource;->isSetAttributes()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_13

    return v0

    .line 689
    :cond_13
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetAttributes()Z

    move-result v0

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->attributes:Lcom/evernote/edam/type/ResourceAttributes;

    iget-object v1, p1, Lcom/evernote/edam/type/Resource;->attributes:Lcom/evernote/edam/type/ResourceAttributes;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result v0

    if-eqz v0, :cond_14

    return v0

    .line 694
    :cond_14
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetUpdateSequenceNum()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Resource;->isSetUpdateSequenceNum()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_15

    return v0

    .line 698
    :cond_15
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetUpdateSequenceNum()Z

    move-result v0

    if-eqz v0, :cond_16

    iget v0, p0, Lcom/evernote/edam/type/Resource;->updateSequenceNum:I

    iget v1, p1, Lcom/evernote/edam/type/Resource;->updateSequenceNum:I

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(II)I

    move-result v0

    if-eqz v0, :cond_16

    return v0

    .line 703
    :cond_16
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetAlternateData()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Resource;->isSetAlternateData()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_17

    return v0

    .line 707
    :cond_17
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetAlternateData()Z

    move-result v0

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->alternateData:Lcom/evernote/edam/type/Data;

    iget-object p1, p1, Lcom/evernote/edam/type/Resource;->alternateData:Lcom/evernote/edam/type/Data;

    invoke-static {v0, p1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result p1

    if-eqz p1, :cond_18

    return p1

    :cond_18
    const/4 p1, 0x0

    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 98
    check-cast p1, Lcom/evernote/edam/type/Resource;

    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/Resource;->compareTo(Lcom/evernote/edam/type/Resource;)I

    move-result p1

    return p1
.end method

.method public deepCopy()Lcom/evernote/edam/type/Resource;
    .locals 1

    .line 173
    new-instance v0, Lcom/evernote/edam/type/Resource;

    invoke-direct {v0, p0}, Lcom/evernote/edam/type/Resource;-><init>(Lcom/evernote/edam/type/Resource;)V

    return-object v0
.end method

.method public bridge synthetic deepCopy()Lcom/evernote/thrift/TBase;
    .locals 1

    .line 98
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->deepCopy()Lcom/evernote/edam/type/Resource;

    move-result-object v0

    return-object v0
.end method

.method public equals(Lcom/evernote/edam/type/Resource;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 480
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetGuid()Z

    move-result v1

    .line 481
    invoke-virtual {p1}, Lcom/evernote/edam/type/Resource;->isSetGuid()Z

    move-result v2

    if-nez v1, :cond_1

    if-eqz v2, :cond_3

    :cond_1
    if-eqz v1, :cond_30

    if-nez v2, :cond_2

    goto/16 :goto_b

    .line 485
    :cond_2
    iget-object v1, p0, Lcom/evernote/edam/type/Resource;->guid:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/type/Resource;->guid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    return v0

    .line 489
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetNoteGuid()Z

    move-result v1

    .line 490
    invoke-virtual {p1}, Lcom/evernote/edam/type/Resource;->isSetNoteGuid()Z

    move-result v2

    if-nez v1, :cond_4

    if-eqz v2, :cond_6

    :cond_4
    if-eqz v1, :cond_2f

    if-nez v2, :cond_5

    goto/16 :goto_a

    .line 494
    :cond_5
    iget-object v1, p0, Lcom/evernote/edam/type/Resource;->noteGuid:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/type/Resource;->noteGuid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    return v0

    .line 498
    :cond_6
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetData()Z

    move-result v1

    .line 499
    invoke-virtual {p1}, Lcom/evernote/edam/type/Resource;->isSetData()Z

    move-result v2

    if-nez v1, :cond_7

    if-eqz v2, :cond_9

    :cond_7
    if-eqz v1, :cond_2e

    if-nez v2, :cond_8

    goto/16 :goto_9

    .line 503
    :cond_8
    iget-object v1, p0, Lcom/evernote/edam/type/Resource;->data:Lcom/evernote/edam/type/Data;

    iget-object v2, p1, Lcom/evernote/edam/type/Resource;->data:Lcom/evernote/edam/type/Data;

    invoke-virtual {v1, v2}, Lcom/evernote/edam/type/Data;->equals(Lcom/evernote/edam/type/Data;)Z

    move-result v1

    if-nez v1, :cond_9

    return v0

    .line 507
    :cond_9
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetMime()Z

    move-result v1

    .line 508
    invoke-virtual {p1}, Lcom/evernote/edam/type/Resource;->isSetMime()Z

    move-result v2

    if-nez v1, :cond_a

    if-eqz v2, :cond_c

    :cond_a
    if-eqz v1, :cond_2d

    if-nez v2, :cond_b

    goto/16 :goto_8

    .line 512
    :cond_b
    iget-object v1, p0, Lcom/evernote/edam/type/Resource;->mime:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/type/Resource;->mime:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    return v0

    .line 516
    :cond_c
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetWidth()Z

    move-result v1

    .line 517
    invoke-virtual {p1}, Lcom/evernote/edam/type/Resource;->isSetWidth()Z

    move-result v2

    if-nez v1, :cond_d

    if-eqz v2, :cond_f

    :cond_d
    if-eqz v1, :cond_2c

    if-nez v2, :cond_e

    goto/16 :goto_7

    .line 521
    :cond_e
    iget-short v1, p0, Lcom/evernote/edam/type/Resource;->width:S

    iget-short v2, p1, Lcom/evernote/edam/type/Resource;->width:S

    if-eq v1, v2, :cond_f

    return v0

    .line 525
    :cond_f
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetHeight()Z

    move-result v1

    .line 526
    invoke-virtual {p1}, Lcom/evernote/edam/type/Resource;->isSetHeight()Z

    move-result v2

    if-nez v1, :cond_10

    if-eqz v2, :cond_12

    :cond_10
    if-eqz v1, :cond_2b

    if-nez v2, :cond_11

    goto/16 :goto_6

    .line 530
    :cond_11
    iget-short v1, p0, Lcom/evernote/edam/type/Resource;->height:S

    iget-short v2, p1, Lcom/evernote/edam/type/Resource;->height:S

    if-eq v1, v2, :cond_12

    return v0

    .line 534
    :cond_12
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetDuration()Z

    move-result v1

    .line 535
    invoke-virtual {p1}, Lcom/evernote/edam/type/Resource;->isSetDuration()Z

    move-result v2

    if-nez v1, :cond_13

    if-eqz v2, :cond_15

    :cond_13
    if-eqz v1, :cond_2a

    if-nez v2, :cond_14

    goto/16 :goto_5

    .line 539
    :cond_14
    iget-short v1, p0, Lcom/evernote/edam/type/Resource;->duration:S

    iget-short v2, p1, Lcom/evernote/edam/type/Resource;->duration:S

    if-eq v1, v2, :cond_15

    return v0

    .line 543
    :cond_15
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetActive()Z

    move-result v1

    .line 544
    invoke-virtual {p1}, Lcom/evernote/edam/type/Resource;->isSetActive()Z

    move-result v2

    if-nez v1, :cond_16

    if-eqz v2, :cond_18

    :cond_16
    if-eqz v1, :cond_29

    if-nez v2, :cond_17

    goto/16 :goto_4

    .line 548
    :cond_17
    iget-boolean v1, p0, Lcom/evernote/edam/type/Resource;->active:Z

    iget-boolean v2, p1, Lcom/evernote/edam/type/Resource;->active:Z

    if-eq v1, v2, :cond_18

    return v0

    .line 552
    :cond_18
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetRecognition()Z

    move-result v1

    .line 553
    invoke-virtual {p1}, Lcom/evernote/edam/type/Resource;->isSetRecognition()Z

    move-result v2

    if-nez v1, :cond_19

    if-eqz v2, :cond_1b

    :cond_19
    if-eqz v1, :cond_28

    if-nez v2, :cond_1a

    goto :goto_3

    .line 557
    :cond_1a
    iget-object v1, p0, Lcom/evernote/edam/type/Resource;->recognition:Lcom/evernote/edam/type/Data;

    iget-object v2, p1, Lcom/evernote/edam/type/Resource;->recognition:Lcom/evernote/edam/type/Data;

    invoke-virtual {v1, v2}, Lcom/evernote/edam/type/Data;->equals(Lcom/evernote/edam/type/Data;)Z

    move-result v1

    if-nez v1, :cond_1b

    return v0

    .line 561
    :cond_1b
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetAttributes()Z

    move-result v1

    .line 562
    invoke-virtual {p1}, Lcom/evernote/edam/type/Resource;->isSetAttributes()Z

    move-result v2

    if-nez v1, :cond_1c

    if-eqz v2, :cond_1e

    :cond_1c
    if-eqz v1, :cond_27

    if-nez v2, :cond_1d

    goto :goto_2

    .line 566
    :cond_1d
    iget-object v1, p0, Lcom/evernote/edam/type/Resource;->attributes:Lcom/evernote/edam/type/ResourceAttributes;

    iget-object v2, p1, Lcom/evernote/edam/type/Resource;->attributes:Lcom/evernote/edam/type/ResourceAttributes;

    invoke-virtual {v1, v2}, Lcom/evernote/edam/type/ResourceAttributes;->equals(Lcom/evernote/edam/type/ResourceAttributes;)Z

    move-result v1

    if-nez v1, :cond_1e

    return v0

    .line 570
    :cond_1e
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetUpdateSequenceNum()Z

    move-result v1

    .line 571
    invoke-virtual {p1}, Lcom/evernote/edam/type/Resource;->isSetUpdateSequenceNum()Z

    move-result v2

    if-nez v1, :cond_1f

    if-eqz v2, :cond_21

    :cond_1f
    if-eqz v1, :cond_26

    if-nez v2, :cond_20

    goto :goto_1

    .line 575
    :cond_20
    iget v1, p0, Lcom/evernote/edam/type/Resource;->updateSequenceNum:I

    iget v2, p1, Lcom/evernote/edam/type/Resource;->updateSequenceNum:I

    if-eq v1, v2, :cond_21

    return v0

    .line 579
    :cond_21
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetAlternateData()Z

    move-result v1

    .line 580
    invoke-virtual {p1}, Lcom/evernote/edam/type/Resource;->isSetAlternateData()Z

    move-result v2

    if-nez v1, :cond_22

    if-eqz v2, :cond_24

    :cond_22
    if-eqz v1, :cond_25

    if-nez v2, :cond_23

    goto :goto_0

    .line 584
    :cond_23
    iget-object v1, p0, Lcom/evernote/edam/type/Resource;->alternateData:Lcom/evernote/edam/type/Data;

    iget-object p1, p1, Lcom/evernote/edam/type/Resource;->alternateData:Lcom/evernote/edam/type/Data;

    invoke-virtual {v1, p1}, Lcom/evernote/edam/type/Data;->equals(Lcom/evernote/edam/type/Data;)Z

    move-result p1

    if-nez p1, :cond_24

    return v0

    :cond_24
    const/4 p1, 0x1

    return p1

    :cond_25
    :goto_0
    return v0

    :cond_26
    :goto_1
    return v0

    :cond_27
    :goto_2
    return v0

    :cond_28
    :goto_3
    return v0

    :cond_29
    :goto_4
    return v0

    :cond_2a
    :goto_5
    return v0

    :cond_2b
    :goto_6
    return v0

    :cond_2c
    :goto_7
    return v0

    :cond_2d
    :goto_8
    return v0

    :cond_2e
    :goto_9
    return v0

    :cond_2f
    :goto_a
    return v0

    :cond_30
    :goto_b
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 471
    :cond_0
    instance-of v1, p1, Lcom/evernote/edam/type/Resource;

    if-eqz v1, :cond_1

    .line 472
    check-cast p1, Lcom/evernote/edam/type/Resource;

    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/Resource;->equals(Lcom/evernote/edam/type/Resource;)Z

    move-result p1

    return p1

    :cond_1
    return v0
.end method

.method public getAlternateData()Lcom/evernote/edam/type/Data;
    .locals 1

    .line 445
    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->alternateData:Lcom/evernote/edam/type/Data;

    return-object v0
.end method

.method public getAttributes()Lcom/evernote/edam/type/ResourceAttributes;
    .locals 1

    .line 400
    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->attributes:Lcom/evernote/edam/type/ResourceAttributes;

    return-object v0
.end method

.method public getData()Lcom/evernote/edam/type/Data;
    .locals 1

    .line 243
    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->data:Lcom/evernote/edam/type/Data;

    return-object v0
.end method

.method public getDuration()S
    .locals 1

    .line 333
    iget-short v0, p0, Lcom/evernote/edam/type/Resource;->duration:S

    return v0
.end method

.method public getGuid()Ljava/lang/String;
    .locals 1

    .line 197
    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->guid:Ljava/lang/String;

    return-object v0
.end method

.method public getHeight()S
    .locals 1

    .line 311
    iget-short v0, p0, Lcom/evernote/edam/type/Resource;->height:S

    return v0
.end method

.method public getMime()Ljava/lang/String;
    .locals 1

    .line 266
    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->mime:Ljava/lang/String;

    return-object v0
.end method

.method public getNoteGuid()Ljava/lang/String;
    .locals 1

    .line 220
    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->noteGuid:Ljava/lang/String;

    return-object v0
.end method

.method public getRecognition()Lcom/evernote/edam/type/Data;
    .locals 1

    .line 377
    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->recognition:Lcom/evernote/edam/type/Data;

    return-object v0
.end method

.method public getUpdateSequenceNum()I
    .locals 1

    .line 423
    iget v0, p0, Lcom/evernote/edam/type/Resource;->updateSequenceNum:I

    return v0
.end method

.method public getWidth()S
    .locals 1

    .line 289
    iget-short v0, p0, Lcom/evernote/edam/type/Resource;->width:S

    return v0
.end method

.method public hashCode()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isActive()Z
    .locals 1

    .line 355
    iget-boolean v0, p0, Lcom/evernote/edam/type/Resource;->active:Z

    return v0
.end method

.method public isSetActive()Z
    .locals 2

    .line 369
    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->__isset_vector:[Z

    const/4 v1, 0x3

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetAlternateData()Z
    .locals 1

    .line 458
    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->alternateData:Lcom/evernote/edam/type/Data;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetAttributes()Z
    .locals 1

    .line 413
    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->attributes:Lcom/evernote/edam/type/ResourceAttributes;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetData()Z
    .locals 1

    .line 256
    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->data:Lcom/evernote/edam/type/Data;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetDuration()Z
    .locals 2

    .line 347
    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->__isset_vector:[Z

    const/4 v1, 0x2

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetGuid()Z
    .locals 1

    .line 210
    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->guid:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetHeight()Z
    .locals 2

    .line 325
    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->__isset_vector:[Z

    const/4 v1, 0x1

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetMime()Z
    .locals 1

    .line 279
    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->mime:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetNoteGuid()Z
    .locals 1

    .line 233
    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->noteGuid:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetRecognition()Z
    .locals 1

    .line 390
    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->recognition:Lcom/evernote/edam/type/Data;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetUpdateSequenceNum()Z
    .locals 2

    .line 437
    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->__isset_vector:[Z

    const/4 v1, 0x4

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetWidth()Z
    .locals 2

    .line 303
    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->__isset_vector:[Z

    const/4 v1, 0x0

    aget-boolean v0, v0, v1

    return v0
.end method

.method public read(Lcom/evernote/thrift/protocol/TProtocol;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 717
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readStructBegin()Lcom/evernote/thrift/protocol/TStruct;

    .line 720
    :goto_0
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readFieldBegin()Lcom/evernote/thrift/protocol/TField;

    move-result-object v0

    .line 721
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-nez v1, :cond_0

    .line 823
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readStructEnd()V

    .line 824
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->validate()V

    return-void

    .line 724
    :cond_0
    iget-short v1, v0, Lcom/evernote/thrift/protocol/TField;->id:S

    const/4 v2, 0x6

    const/16 v3, 0xb

    const/16 v4, 0xc

    const/4 v5, 0x1

    packed-switch v1, :pswitch_data_0

    .line 819
    :pswitch_0
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 811
    :pswitch_1
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_1

    .line 812
    new-instance v0, Lcom/evernote/edam/type/Data;

    invoke-direct {v0}, Lcom/evernote/edam/type/Data;-><init>()V

    iput-object v0, p0, Lcom/evernote/edam/type/Resource;->alternateData:Lcom/evernote/edam/type/Data;

    .line 813
    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->alternateData:Lcom/evernote/edam/type/Data;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/type/Data;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    goto/16 :goto_1

    .line 815
    :cond_1
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 803
    :pswitch_2
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    const/16 v2, 0x8

    if-ne v1, v2, :cond_2

    .line 804
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI32()I

    move-result v0

    iput v0, p0, Lcom/evernote/edam/type/Resource;->updateSequenceNum:I

    .line 805
    invoke-virtual {p0, v5}, Lcom/evernote/edam/type/Resource;->setUpdateSequenceNumIsSet(Z)V

    goto/16 :goto_1

    .line 807
    :cond_2
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 795
    :pswitch_3
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_3

    .line 796
    new-instance v0, Lcom/evernote/edam/type/ResourceAttributes;

    invoke-direct {v0}, Lcom/evernote/edam/type/ResourceAttributes;-><init>()V

    iput-object v0, p0, Lcom/evernote/edam/type/Resource;->attributes:Lcom/evernote/edam/type/ResourceAttributes;

    .line 797
    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->attributes:Lcom/evernote/edam/type/ResourceAttributes;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/type/ResourceAttributes;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    goto/16 :goto_1

    .line 799
    :cond_3
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 787
    :pswitch_4
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_4

    .line 788
    new-instance v0, Lcom/evernote/edam/type/Data;

    invoke-direct {v0}, Lcom/evernote/edam/type/Data;-><init>()V

    iput-object v0, p0, Lcom/evernote/edam/type/Resource;->recognition:Lcom/evernote/edam/type/Data;

    .line 789
    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->recognition:Lcom/evernote/edam/type/Data;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/type/Data;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    goto/16 :goto_1

    .line 791
    :cond_4
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 779
    :pswitch_5
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    const/4 v2, 0x2

    if-ne v1, v2, :cond_5

    .line 780
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/type/Resource;->active:Z

    .line 781
    invoke-virtual {p0, v5}, Lcom/evernote/edam/type/Resource;->setActiveIsSet(Z)V

    goto/16 :goto_1

    .line 783
    :cond_5
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 771
    :pswitch_6
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v2, :cond_6

    .line 772
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI16()S

    move-result v0

    iput-short v0, p0, Lcom/evernote/edam/type/Resource;->duration:S

    .line 773
    invoke-virtual {p0, v5}, Lcom/evernote/edam/type/Resource;->setDurationIsSet(Z)V

    goto/16 :goto_1

    .line 775
    :cond_6
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 763
    :pswitch_7
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v2, :cond_7

    .line 764
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI16()S

    move-result v0

    iput-short v0, p0, Lcom/evernote/edam/type/Resource;->height:S

    .line 765
    invoke-virtual {p0, v5}, Lcom/evernote/edam/type/Resource;->setHeightIsSet(Z)V

    goto :goto_1

    .line 767
    :cond_7
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 755
    :pswitch_8
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v2, :cond_8

    .line 756
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI16()S

    move-result v0

    iput-short v0, p0, Lcom/evernote/edam/type/Resource;->width:S

    .line 757
    invoke-virtual {p0, v5}, Lcom/evernote/edam/type/Resource;->setWidthIsSet(Z)V

    goto :goto_1

    .line 759
    :cond_8
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 748
    :pswitch_9
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v3, :cond_9

    .line 749
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/type/Resource;->mime:Ljava/lang/String;

    goto :goto_1

    .line 751
    :cond_9
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 740
    :pswitch_a
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_a

    .line 741
    new-instance v0, Lcom/evernote/edam/type/Data;

    invoke-direct {v0}, Lcom/evernote/edam/type/Data;-><init>()V

    iput-object v0, p0, Lcom/evernote/edam/type/Resource;->data:Lcom/evernote/edam/type/Data;

    .line 742
    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->data:Lcom/evernote/edam/type/Data;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/type/Data;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    goto :goto_1

    .line 744
    :cond_a
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 733
    :pswitch_b
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v3, :cond_b

    .line 734
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/type/Resource;->noteGuid:Ljava/lang/String;

    goto :goto_1

    .line 736
    :cond_b
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 726
    :pswitch_c
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v3, :cond_c

    .line 727
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/type/Resource;->guid:Ljava/lang/String;

    goto :goto_1

    .line 729
    :cond_c
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    .line 821
    :goto_1
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readFieldEnd()V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public setActive(Z)V
    .locals 0

    .line 359
    iput-boolean p1, p0, Lcom/evernote/edam/type/Resource;->active:Z

    const/4 p1, 0x1

    .line 360
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/Resource;->setActiveIsSet(Z)V

    return-void
.end method

.method public setActiveIsSet(Z)V
    .locals 2

    .line 373
    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->__isset_vector:[Z

    const/4 v1, 0x3

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setAlternateData(Lcom/evernote/edam/type/Data;)V
    .locals 0

    .line 449
    iput-object p1, p0, Lcom/evernote/edam/type/Resource;->alternateData:Lcom/evernote/edam/type/Data;

    return-void
.end method

.method public setAlternateDataIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 463
    iput-object p1, p0, Lcom/evernote/edam/type/Resource;->alternateData:Lcom/evernote/edam/type/Data;

    :cond_0
    return-void
.end method

.method public setAttributes(Lcom/evernote/edam/type/ResourceAttributes;)V
    .locals 0

    .line 404
    iput-object p1, p0, Lcom/evernote/edam/type/Resource;->attributes:Lcom/evernote/edam/type/ResourceAttributes;

    return-void
.end method

.method public setAttributesIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 418
    iput-object p1, p0, Lcom/evernote/edam/type/Resource;->attributes:Lcom/evernote/edam/type/ResourceAttributes;

    :cond_0
    return-void
.end method

.method public setData(Lcom/evernote/edam/type/Data;)V
    .locals 0

    .line 247
    iput-object p1, p0, Lcom/evernote/edam/type/Resource;->data:Lcom/evernote/edam/type/Data;

    return-void
.end method

.method public setDataIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 261
    iput-object p1, p0, Lcom/evernote/edam/type/Resource;->data:Lcom/evernote/edam/type/Data;

    :cond_0
    return-void
.end method

.method public setDuration(S)V
    .locals 0

    .line 337
    iput-short p1, p0, Lcom/evernote/edam/type/Resource;->duration:S

    const/4 p1, 0x1

    .line 338
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/Resource;->setDurationIsSet(Z)V

    return-void
.end method

.method public setDurationIsSet(Z)V
    .locals 2

    .line 351
    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->__isset_vector:[Z

    const/4 v1, 0x2

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setGuid(Ljava/lang/String;)V
    .locals 0

    .line 201
    iput-object p1, p0, Lcom/evernote/edam/type/Resource;->guid:Ljava/lang/String;

    return-void
.end method

.method public setGuidIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 215
    iput-object p1, p0, Lcom/evernote/edam/type/Resource;->guid:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setHeight(S)V
    .locals 0

    .line 315
    iput-short p1, p0, Lcom/evernote/edam/type/Resource;->height:S

    const/4 p1, 0x1

    .line 316
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/Resource;->setHeightIsSet(Z)V

    return-void
.end method

.method public setHeightIsSet(Z)V
    .locals 2

    .line 329
    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->__isset_vector:[Z

    const/4 v1, 0x1

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setMime(Ljava/lang/String;)V
    .locals 0

    .line 270
    iput-object p1, p0, Lcom/evernote/edam/type/Resource;->mime:Ljava/lang/String;

    return-void
.end method

.method public setMimeIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 284
    iput-object p1, p0, Lcom/evernote/edam/type/Resource;->mime:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setNoteGuid(Ljava/lang/String;)V
    .locals 0

    .line 224
    iput-object p1, p0, Lcom/evernote/edam/type/Resource;->noteGuid:Ljava/lang/String;

    return-void
.end method

.method public setNoteGuidIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 238
    iput-object p1, p0, Lcom/evernote/edam/type/Resource;->noteGuid:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setRecognition(Lcom/evernote/edam/type/Data;)V
    .locals 0

    .line 381
    iput-object p1, p0, Lcom/evernote/edam/type/Resource;->recognition:Lcom/evernote/edam/type/Data;

    return-void
.end method

.method public setRecognitionIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 395
    iput-object p1, p0, Lcom/evernote/edam/type/Resource;->recognition:Lcom/evernote/edam/type/Data;

    :cond_0
    return-void
.end method

.method public setUpdateSequenceNum(I)V
    .locals 0

    .line 427
    iput p1, p0, Lcom/evernote/edam/type/Resource;->updateSequenceNum:I

    const/4 p1, 0x1

    .line 428
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/Resource;->setUpdateSequenceNumIsSet(Z)V

    return-void
.end method

.method public setUpdateSequenceNumIsSet(Z)V
    .locals 2

    .line 441
    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->__isset_vector:[Z

    const/4 v1, 0x4

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setWidth(S)V
    .locals 0

    .line 293
    iput-short p1, p0, Lcom/evernote/edam/type/Resource;->width:S

    const/4 p1, 0x1

    .line 294
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/Resource;->setWidthIsSet(Z)V

    return-void
.end method

.method public setWidthIsSet(Z)V
    .locals 2

    .line 307
    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->__isset_vector:[Z

    const/4 v1, 0x0

    aput-boolean p1, v0, v1

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 911
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Resource("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 914
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetGuid()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    const-string v1, "guid:"

    .line 915
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 916
    iget-object v1, p0, Lcom/evernote/edam/type/Resource;->guid:Ljava/lang/String;

    if-nez v1, :cond_0

    const-string v1, "null"

    .line 917
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 919
    :cond_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    const/4 v1, 0x1

    .line 923
    :goto_1
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetNoteGuid()Z

    move-result v3

    if-eqz v3, :cond_4

    if-nez v1, :cond_2

    const-string v1, ", "

    .line 924
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const-string v1, "noteGuid:"

    .line 925
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 926
    iget-object v1, p0, Lcom/evernote/edam/type/Resource;->noteGuid:Ljava/lang/String;

    if-nez v1, :cond_3

    const-string v1, "null"

    .line 927
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 929
    :cond_3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_2
    const/4 v1, 0x0

    .line 933
    :cond_4
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetData()Z

    move-result v3

    if-eqz v3, :cond_7

    if-nez v1, :cond_5

    const-string v1, ", "

    .line 934
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    const-string v1, "data:"

    .line 935
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 936
    iget-object v1, p0, Lcom/evernote/edam/type/Resource;->data:Lcom/evernote/edam/type/Data;

    if-nez v1, :cond_6

    const-string v1, "null"

    .line 937
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 939
    :cond_6
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :goto_3
    const/4 v1, 0x0

    .line 943
    :cond_7
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetMime()Z

    move-result v3

    if-eqz v3, :cond_a

    if-nez v1, :cond_8

    const-string v1, ", "

    .line 944
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_8
    const-string v1, "mime:"

    .line 945
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 946
    iget-object v1, p0, Lcom/evernote/edam/type/Resource;->mime:Ljava/lang/String;

    if-nez v1, :cond_9

    const-string v1, "null"

    .line 947
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 949
    :cond_9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_4
    const/4 v1, 0x0

    .line 953
    :cond_a
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetWidth()Z

    move-result v3

    if-eqz v3, :cond_c

    if-nez v1, :cond_b

    const-string v1, ", "

    .line 954
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_b
    const-string v1, "width:"

    .line 955
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 956
    iget-short v1, p0, Lcom/evernote/edam/type/Resource;->width:S

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 959
    :cond_c
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetHeight()Z

    move-result v3

    if-eqz v3, :cond_e

    if-nez v1, :cond_d

    const-string v1, ", "

    .line 960
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_d
    const-string v1, "height:"

    .line 961
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 962
    iget-short v1, p0, Lcom/evernote/edam/type/Resource;->height:S

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 965
    :cond_e
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetDuration()Z

    move-result v3

    if-eqz v3, :cond_10

    if-nez v1, :cond_f

    const-string v1, ", "

    .line 966
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_f
    const-string v1, "duration:"

    .line 967
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 968
    iget-short v1, p0, Lcom/evernote/edam/type/Resource;->duration:S

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 971
    :cond_10
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetActive()Z

    move-result v3

    if-eqz v3, :cond_12

    if-nez v1, :cond_11

    const-string v1, ", "

    .line 972
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_11
    const-string v1, "active:"

    .line 973
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 974
    iget-boolean v1, p0, Lcom/evernote/edam/type/Resource;->active:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 977
    :cond_12
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetRecognition()Z

    move-result v3

    if-eqz v3, :cond_15

    if-nez v1, :cond_13

    const-string v1, ", "

    .line 978
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_13
    const-string v1, "recognition:"

    .line 979
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 980
    iget-object v1, p0, Lcom/evernote/edam/type/Resource;->recognition:Lcom/evernote/edam/type/Data;

    if-nez v1, :cond_14

    const-string v1, "null"

    .line 981
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5

    .line 983
    :cond_14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :goto_5
    const/4 v1, 0x0

    .line 987
    :cond_15
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetAttributes()Z

    move-result v3

    if-eqz v3, :cond_18

    if-nez v1, :cond_16

    const-string v1, ", "

    .line 988
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_16
    const-string v1, "attributes:"

    .line 989
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 990
    iget-object v1, p0, Lcom/evernote/edam/type/Resource;->attributes:Lcom/evernote/edam/type/ResourceAttributes;

    if-nez v1, :cond_17

    const-string v1, "null"

    .line 991
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6

    .line 993
    :cond_17
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :goto_6
    const/4 v1, 0x0

    .line 997
    :cond_18
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetUpdateSequenceNum()Z

    move-result v3

    if-eqz v3, :cond_1a

    if-nez v1, :cond_19

    const-string v1, ", "

    .line 998
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_19
    const-string v1, "updateSequenceNum:"

    .line 999
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1000
    iget v1, p0, Lcom/evernote/edam/type/Resource;->updateSequenceNum:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1003
    :cond_1a
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetAlternateData()Z

    move-result v2

    if-eqz v2, :cond_1d

    if-nez v1, :cond_1b

    const-string v1, ", "

    .line 1004
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1b
    const-string v1, "alternateData:"

    .line 1005
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1006
    iget-object v1, p0, Lcom/evernote/edam/type/Resource;->alternateData:Lcom/evernote/edam/type/Data;

    if-nez v1, :cond_1c

    const-string v1, "null"

    .line 1007
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_7

    .line 1009
    :cond_1c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1d
    :goto_7
    const-string v1, ")"

    .line 1013
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1014
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public unsetActive()V
    .locals 3

    .line 364
    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->__isset_vector:[Z

    const/4 v1, 0x3

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetAlternateData()V
    .locals 1

    const/4 v0, 0x0

    .line 453
    iput-object v0, p0, Lcom/evernote/edam/type/Resource;->alternateData:Lcom/evernote/edam/type/Data;

    return-void
.end method

.method public unsetAttributes()V
    .locals 1

    const/4 v0, 0x0

    .line 408
    iput-object v0, p0, Lcom/evernote/edam/type/Resource;->attributes:Lcom/evernote/edam/type/ResourceAttributes;

    return-void
.end method

.method public unsetData()V
    .locals 1

    const/4 v0, 0x0

    .line 251
    iput-object v0, p0, Lcom/evernote/edam/type/Resource;->data:Lcom/evernote/edam/type/Data;

    return-void
.end method

.method public unsetDuration()V
    .locals 3

    .line 342
    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->__isset_vector:[Z

    const/4 v1, 0x2

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetGuid()V
    .locals 1

    const/4 v0, 0x0

    .line 205
    iput-object v0, p0, Lcom/evernote/edam/type/Resource;->guid:Ljava/lang/String;

    return-void
.end method

.method public unsetHeight()V
    .locals 3

    .line 320
    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->__isset_vector:[Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetMime()V
    .locals 1

    const/4 v0, 0x0

    .line 274
    iput-object v0, p0, Lcom/evernote/edam/type/Resource;->mime:Ljava/lang/String;

    return-void
.end method

.method public unsetNoteGuid()V
    .locals 1

    const/4 v0, 0x0

    .line 228
    iput-object v0, p0, Lcom/evernote/edam/type/Resource;->noteGuid:Ljava/lang/String;

    return-void
.end method

.method public unsetRecognition()V
    .locals 1

    const/4 v0, 0x0

    .line 385
    iput-object v0, p0, Lcom/evernote/edam/type/Resource;->recognition:Lcom/evernote/edam/type/Data;

    return-void
.end method

.method public unsetUpdateSequenceNum()V
    .locals 3

    .line 432
    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->__isset_vector:[Z

    const/4 v1, 0x4

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetWidth()V
    .locals 2

    .line 298
    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->__isset_vector:[Z

    const/4 v1, 0x0

    aput-boolean v1, v0, v1

    return-void
.end method

.method public validate()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    return-void
.end method

.method public write(Lcom/evernote/thrift/protocol/TProtocol;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 828
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->validate()V

    .line 830
    sget-object v0, Lcom/evernote/edam/type/Resource;->STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeStructBegin(Lcom/evernote/thrift/protocol/TStruct;)V

    .line 831
    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->guid:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 832
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetGuid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 833
    sget-object v0, Lcom/evernote/edam/type/Resource;->GUID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 834
    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->guid:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 835
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 838
    :cond_0
    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->noteGuid:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 839
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetNoteGuid()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 840
    sget-object v0, Lcom/evernote/edam/type/Resource;->NOTE_GUID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 841
    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->noteGuid:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 842
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 845
    :cond_1
    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->data:Lcom/evernote/edam/type/Data;

    if-eqz v0, :cond_2

    .line 846
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetData()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 847
    sget-object v0, Lcom/evernote/edam/type/Resource;->DATA_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 848
    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->data:Lcom/evernote/edam/type/Data;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/type/Data;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 849
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 852
    :cond_2
    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->mime:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 853
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetMime()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 854
    sget-object v0, Lcom/evernote/edam/type/Resource;->MIME_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 855
    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->mime:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 856
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 859
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetWidth()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 860
    sget-object v0, Lcom/evernote/edam/type/Resource;->WIDTH_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 861
    iget-short v0, p0, Lcom/evernote/edam/type/Resource;->width:S

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeI16(S)V

    .line 862
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 864
    :cond_4
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetHeight()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 865
    sget-object v0, Lcom/evernote/edam/type/Resource;->HEIGHT_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 866
    iget-short v0, p0, Lcom/evernote/edam/type/Resource;->height:S

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeI16(S)V

    .line 867
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 869
    :cond_5
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetDuration()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 870
    sget-object v0, Lcom/evernote/edam/type/Resource;->DURATION_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 871
    iget-short v0, p0, Lcom/evernote/edam/type/Resource;->duration:S

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeI16(S)V

    .line 872
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 874
    :cond_6
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetActive()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 875
    sget-object v0, Lcom/evernote/edam/type/Resource;->ACTIVE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 876
    iget-boolean v0, p0, Lcom/evernote/edam/type/Resource;->active:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 877
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 879
    :cond_7
    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->recognition:Lcom/evernote/edam/type/Data;

    if-eqz v0, :cond_8

    .line 880
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetRecognition()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 881
    sget-object v0, Lcom/evernote/edam/type/Resource;->RECOGNITION_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 882
    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->recognition:Lcom/evernote/edam/type/Data;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/type/Data;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 883
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 886
    :cond_8
    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->attributes:Lcom/evernote/edam/type/ResourceAttributes;

    if-eqz v0, :cond_9

    .line 887
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetAttributes()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 888
    sget-object v0, Lcom/evernote/edam/type/Resource;->ATTRIBUTES_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 889
    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->attributes:Lcom/evernote/edam/type/ResourceAttributes;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/type/ResourceAttributes;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 890
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 893
    :cond_9
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetUpdateSequenceNum()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 894
    sget-object v0, Lcom/evernote/edam/type/Resource;->UPDATE_SEQUENCE_NUM_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 895
    iget v0, p0, Lcom/evernote/edam/type/Resource;->updateSequenceNum:I

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeI32(I)V

    .line 896
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 898
    :cond_a
    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->alternateData:Lcom/evernote/edam/type/Data;

    if-eqz v0, :cond_b

    .line 899
    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->isSetAlternateData()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 900
    sget-object v0, Lcom/evernote/edam/type/Resource;->ALTERNATE_DATA_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 901
    iget-object v0, p0, Lcom/evernote/edam/type/Resource;->alternateData:Lcom/evernote/edam/type/Data;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/type/Data;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 902
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 905
    :cond_b
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldStop()V

    .line 906
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeStructEnd()V

    return-void
.end method

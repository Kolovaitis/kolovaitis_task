.class public Lcom/evernote/edam/type/SharedNotebook;
.super Ljava/lang/Object;
.source "SharedNotebook.java"

# interfaces
.implements Lcom/evernote/thrift/TBase;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/evernote/thrift/TBase<",
        "Lcom/evernote/edam/type/SharedNotebook;",
        ">;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# static fields
.field private static final ALLOW_PREVIEW_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final EMAIL_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final ID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final NOTEBOOK_GUID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final NOTEBOOK_MODIFIABLE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final PRIVILEGE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final RECIPIENT_SETTINGS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final REQUIRE_LOGIN_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final SERVICE_CREATED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final SERVICE_UPDATED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final SHARE_KEY_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

.field private static final USERNAME_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final USER_ID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final __ALLOWPREVIEW_ISSET_ID:I = 0x6

.field private static final __ID_ISSET_ID:I = 0x0

.field private static final __NOTEBOOKMODIFIABLE_ISSET_ID:I = 0x2

.field private static final __REQUIRELOGIN_ISSET_ID:I = 0x3

.field private static final __SERVICECREATED_ISSET_ID:I = 0x4

.field private static final __SERVICEUPDATED_ISSET_ID:I = 0x5

.field private static final __USERID_ISSET_ID:I = 0x1


# instance fields
.field private __isset_vector:[Z

.field private allowPreview:Z

.field private email:Ljava/lang/String;

.field private id:J

.field private notebookGuid:Ljava/lang/String;

.field private notebookModifiable:Z

.field private privilege:Lcom/evernote/edam/type/SharedNotebookPrivilegeLevel;

.field private recipientSettings:Lcom/evernote/edam/type/SharedNotebookRecipientSettings;

.field private requireLogin:Z

.field private serviceCreated:J

.field private serviceUpdated:J

.field private shareKey:Ljava/lang/String;

.field private userId:I

.field private username:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 81
    new-instance v0, Lcom/evernote/thrift/protocol/TStruct;

    const-string v1, "SharedNotebook"

    invoke-direct {v0, v1}, Lcom/evernote/thrift/protocol/TStruct;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/evernote/edam/type/SharedNotebook;->STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

    .line 83
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "id"

    const/16 v2, 0xa

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/SharedNotebook;->ID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 84
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "userId"

    const/16 v3, 0x8

    const/4 v4, 0x2

    invoke-direct {v0, v1, v3, v4}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/SharedNotebook;->USER_ID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 85
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "notebookGuid"

    const/16 v5, 0xb

    const/4 v6, 0x3

    invoke-direct {v0, v1, v5, v6}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/SharedNotebook;->NOTEBOOK_GUID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 86
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "email"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v5, v6}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/SharedNotebook;->EMAIL_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 87
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "notebookModifiable"

    const/4 v6, 0x5

    invoke-direct {v0, v1, v4, v6}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/SharedNotebook;->NOTEBOOK_MODIFIABLE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 88
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "requireLogin"

    const/4 v6, 0x6

    invoke-direct {v0, v1, v4, v6}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/SharedNotebook;->REQUIRE_LOGIN_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 89
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "serviceCreated"

    const/4 v6, 0x7

    invoke-direct {v0, v1, v2, v6}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/SharedNotebook;->SERVICE_CREATED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 90
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "serviceUpdated"

    invoke-direct {v0, v1, v2, v2}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/SharedNotebook;->SERVICE_UPDATED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 91
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "shareKey"

    invoke-direct {v0, v1, v5, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/SharedNotebook;->SHARE_KEY_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 92
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "username"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v5, v2}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/SharedNotebook;->USERNAME_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 93
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "privilege"

    invoke-direct {v0, v1, v3, v5}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/SharedNotebook;->PRIVILEGE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 94
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "allowPreview"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v4, v2}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/SharedNotebook;->ALLOW_PREVIEW_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 95
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "recipientSettings"

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/SharedNotebook;->RECIPIENT_SETTINGS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x7

    .line 120
    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->__isset_vector:[Z

    return-void
.end method

.method public constructor <init>(Lcom/evernote/edam/type/SharedNotebook;)V
    .locals 4

    .line 128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x7

    .line 120
    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->__isset_vector:[Z

    .line 129
    iget-object v0, p1, Lcom/evernote/edam/type/SharedNotebook;->__isset_vector:[Z

    iget-object v1, p0, Lcom/evernote/edam/type/SharedNotebook;->__isset_vector:[Z

    array-length v2, v0

    const/4 v3, 0x0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 130
    iget-wide v0, p1, Lcom/evernote/edam/type/SharedNotebook;->id:J

    iput-wide v0, p0, Lcom/evernote/edam/type/SharedNotebook;->id:J

    .line 131
    iget v0, p1, Lcom/evernote/edam/type/SharedNotebook;->userId:I

    iput v0, p0, Lcom/evernote/edam/type/SharedNotebook;->userId:I

    .line 132
    invoke-virtual {p1}, Lcom/evernote/edam/type/SharedNotebook;->isSetNotebookGuid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p1, Lcom/evernote/edam/type/SharedNotebook;->notebookGuid:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->notebookGuid:Ljava/lang/String;

    .line 135
    :cond_0
    invoke-virtual {p1}, Lcom/evernote/edam/type/SharedNotebook;->isSetEmail()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 136
    iget-object v0, p1, Lcom/evernote/edam/type/SharedNotebook;->email:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->email:Ljava/lang/String;

    .line 138
    :cond_1
    iget-boolean v0, p1, Lcom/evernote/edam/type/SharedNotebook;->notebookModifiable:Z

    iput-boolean v0, p0, Lcom/evernote/edam/type/SharedNotebook;->notebookModifiable:Z

    .line 139
    iget-boolean v0, p1, Lcom/evernote/edam/type/SharedNotebook;->requireLogin:Z

    iput-boolean v0, p0, Lcom/evernote/edam/type/SharedNotebook;->requireLogin:Z

    .line 140
    iget-wide v0, p1, Lcom/evernote/edam/type/SharedNotebook;->serviceCreated:J

    iput-wide v0, p0, Lcom/evernote/edam/type/SharedNotebook;->serviceCreated:J

    .line 141
    iget-wide v0, p1, Lcom/evernote/edam/type/SharedNotebook;->serviceUpdated:J

    iput-wide v0, p0, Lcom/evernote/edam/type/SharedNotebook;->serviceUpdated:J

    .line 142
    invoke-virtual {p1}, Lcom/evernote/edam/type/SharedNotebook;->isSetShareKey()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 143
    iget-object v0, p1, Lcom/evernote/edam/type/SharedNotebook;->shareKey:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->shareKey:Ljava/lang/String;

    .line 145
    :cond_2
    invoke-virtual {p1}, Lcom/evernote/edam/type/SharedNotebook;->isSetUsername()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 146
    iget-object v0, p1, Lcom/evernote/edam/type/SharedNotebook;->username:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->username:Ljava/lang/String;

    .line 148
    :cond_3
    invoke-virtual {p1}, Lcom/evernote/edam/type/SharedNotebook;->isSetPrivilege()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 149
    iget-object v0, p1, Lcom/evernote/edam/type/SharedNotebook;->privilege:Lcom/evernote/edam/type/SharedNotebookPrivilegeLevel;

    iput-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->privilege:Lcom/evernote/edam/type/SharedNotebookPrivilegeLevel;

    .line 151
    :cond_4
    iget-boolean v0, p1, Lcom/evernote/edam/type/SharedNotebook;->allowPreview:Z

    iput-boolean v0, p0, Lcom/evernote/edam/type/SharedNotebook;->allowPreview:Z

    .line 152
    invoke-virtual {p1}, Lcom/evernote/edam/type/SharedNotebook;->isSetRecipientSettings()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 153
    new-instance v0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;

    iget-object p1, p1, Lcom/evernote/edam/type/SharedNotebook;->recipientSettings:Lcom/evernote/edam/type/SharedNotebookRecipientSettings;

    invoke-direct {v0, p1}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;-><init>(Lcom/evernote/edam/type/SharedNotebookRecipientSettings;)V

    iput-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->recipientSettings:Lcom/evernote/edam/type/SharedNotebookRecipientSettings;

    :cond_5
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 4

    const/4 v0, 0x0

    .line 162
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/SharedNotebook;->setIdIsSet(Z)V

    const-wide/16 v1, 0x0

    .line 163
    iput-wide v1, p0, Lcom/evernote/edam/type/SharedNotebook;->id:J

    .line 164
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/SharedNotebook;->setUserIdIsSet(Z)V

    .line 165
    iput v0, p0, Lcom/evernote/edam/type/SharedNotebook;->userId:I

    const/4 v3, 0x0

    .line 166
    iput-object v3, p0, Lcom/evernote/edam/type/SharedNotebook;->notebookGuid:Ljava/lang/String;

    .line 167
    iput-object v3, p0, Lcom/evernote/edam/type/SharedNotebook;->email:Ljava/lang/String;

    .line 168
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/SharedNotebook;->setNotebookModifiableIsSet(Z)V

    .line 169
    iput-boolean v0, p0, Lcom/evernote/edam/type/SharedNotebook;->notebookModifiable:Z

    .line 170
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/SharedNotebook;->setRequireLoginIsSet(Z)V

    .line 171
    iput-boolean v0, p0, Lcom/evernote/edam/type/SharedNotebook;->requireLogin:Z

    .line 172
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/SharedNotebook;->setServiceCreatedIsSet(Z)V

    .line 173
    iput-wide v1, p0, Lcom/evernote/edam/type/SharedNotebook;->serviceCreated:J

    .line 174
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/SharedNotebook;->setServiceUpdatedIsSet(Z)V

    .line 175
    iput-wide v1, p0, Lcom/evernote/edam/type/SharedNotebook;->serviceUpdated:J

    .line 176
    iput-object v3, p0, Lcom/evernote/edam/type/SharedNotebook;->shareKey:Ljava/lang/String;

    .line 177
    iput-object v3, p0, Lcom/evernote/edam/type/SharedNotebook;->username:Ljava/lang/String;

    .line 178
    iput-object v3, p0, Lcom/evernote/edam/type/SharedNotebook;->privilege:Lcom/evernote/edam/type/SharedNotebookPrivilegeLevel;

    .line 179
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/SharedNotebook;->setAllowPreviewIsSet(Z)V

    .line 180
    iput-boolean v0, p0, Lcom/evernote/edam/type/SharedNotebook;->allowPreview:Z

    .line 181
    iput-object v3, p0, Lcom/evernote/edam/type/SharedNotebook;->recipientSettings:Lcom/evernote/edam/type/SharedNotebookRecipientSettings;

    return-void
.end method

.method public compareTo(Lcom/evernote/edam/type/SharedNotebook;)I
    .locals 4

    .line 623
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 624
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    return p1

    .line 630
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetId()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/SharedNotebook;->isSetId()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_1

    return v0

    .line 634
    :cond_1
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetId()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-wide v0, p0, Lcom/evernote/edam/type/SharedNotebook;->id:J

    iget-wide v2, p1, Lcom/evernote/edam/type/SharedNotebook;->id:J

    invoke-static {v0, v1, v2, v3}, Lcom/evernote/thrift/TBaseHelper;->compareTo(JJ)I

    move-result v0

    if-eqz v0, :cond_2

    return v0

    .line 639
    :cond_2
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetUserId()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/SharedNotebook;->isSetUserId()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_3

    return v0

    .line 643
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetUserId()Z

    move-result v0

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/evernote/edam/type/SharedNotebook;->userId:I

    iget v1, p1, Lcom/evernote/edam/type/SharedNotebook;->userId:I

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(II)I

    move-result v0

    if-eqz v0, :cond_4

    return v0

    .line 648
    :cond_4
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetNotebookGuid()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/SharedNotebook;->isSetNotebookGuid()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_5

    return v0

    .line 652
    :cond_5
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetNotebookGuid()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->notebookGuid:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/type/SharedNotebook;->notebookGuid:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_6

    return v0

    .line 657
    :cond_6
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetEmail()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/SharedNotebook;->isSetEmail()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_7

    return v0

    .line 661
    :cond_7
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetEmail()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->email:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/type/SharedNotebook;->email:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_8

    return v0

    .line 666
    :cond_8
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetNotebookModifiable()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/SharedNotebook;->isSetNotebookModifiable()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_9

    return v0

    .line 670
    :cond_9
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetNotebookModifiable()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-boolean v0, p0, Lcom/evernote/edam/type/SharedNotebook;->notebookModifiable:Z

    iget-boolean v1, p1, Lcom/evernote/edam/type/SharedNotebook;->notebookModifiable:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_a

    return v0

    .line 675
    :cond_a
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetRequireLogin()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/SharedNotebook;->isSetRequireLogin()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_b

    return v0

    .line 679
    :cond_b
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetRequireLogin()Z

    move-result v0

    if-eqz v0, :cond_c

    iget-boolean v0, p0, Lcom/evernote/edam/type/SharedNotebook;->requireLogin:Z

    iget-boolean v1, p1, Lcom/evernote/edam/type/SharedNotebook;->requireLogin:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_c

    return v0

    .line 684
    :cond_c
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetServiceCreated()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/SharedNotebook;->isSetServiceCreated()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_d

    return v0

    .line 688
    :cond_d
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetServiceCreated()Z

    move-result v0

    if-eqz v0, :cond_e

    iget-wide v0, p0, Lcom/evernote/edam/type/SharedNotebook;->serviceCreated:J

    iget-wide v2, p1, Lcom/evernote/edam/type/SharedNotebook;->serviceCreated:J

    invoke-static {v0, v1, v2, v3}, Lcom/evernote/thrift/TBaseHelper;->compareTo(JJ)I

    move-result v0

    if-eqz v0, :cond_e

    return v0

    .line 693
    :cond_e
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetServiceUpdated()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/SharedNotebook;->isSetServiceUpdated()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_f

    return v0

    .line 697
    :cond_f
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetServiceUpdated()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-wide v0, p0, Lcom/evernote/edam/type/SharedNotebook;->serviceUpdated:J

    iget-wide v2, p1, Lcom/evernote/edam/type/SharedNotebook;->serviceUpdated:J

    invoke-static {v0, v1, v2, v3}, Lcom/evernote/thrift/TBaseHelper;->compareTo(JJ)I

    move-result v0

    if-eqz v0, :cond_10

    return v0

    .line 702
    :cond_10
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetShareKey()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/SharedNotebook;->isSetShareKey()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_11

    return v0

    .line 706
    :cond_11
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetShareKey()Z

    move-result v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->shareKey:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/type/SharedNotebook;->shareKey:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_12

    return v0

    .line 711
    :cond_12
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetUsername()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/SharedNotebook;->isSetUsername()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_13

    return v0

    .line 715
    :cond_13
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetUsername()Z

    move-result v0

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->username:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/type/SharedNotebook;->username:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_14

    return v0

    .line 720
    :cond_14
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetPrivilege()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/SharedNotebook;->isSetPrivilege()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_15

    return v0

    .line 724
    :cond_15
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetPrivilege()Z

    move-result v0

    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->privilege:Lcom/evernote/edam/type/SharedNotebookPrivilegeLevel;

    iget-object v1, p1, Lcom/evernote/edam/type/SharedNotebook;->privilege:Lcom/evernote/edam/type/SharedNotebookPrivilegeLevel;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result v0

    if-eqz v0, :cond_16

    return v0

    .line 729
    :cond_16
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetAllowPreview()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/SharedNotebook;->isSetAllowPreview()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_17

    return v0

    .line 733
    :cond_17
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetAllowPreview()Z

    move-result v0

    if-eqz v0, :cond_18

    iget-boolean v0, p0, Lcom/evernote/edam/type/SharedNotebook;->allowPreview:Z

    iget-boolean v1, p1, Lcom/evernote/edam/type/SharedNotebook;->allowPreview:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_18

    return v0

    .line 738
    :cond_18
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetRecipientSettings()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/SharedNotebook;->isSetRecipientSettings()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_19

    return v0

    .line 742
    :cond_19
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetRecipientSettings()Z

    move-result v0

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->recipientSettings:Lcom/evernote/edam/type/SharedNotebookRecipientSettings;

    iget-object p1, p1, Lcom/evernote/edam/type/SharedNotebook;->recipientSettings:Lcom/evernote/edam/type/SharedNotebookRecipientSettings;

    invoke-static {v0, p1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result p1

    if-eqz p1, :cond_1a

    return p1

    :cond_1a
    const/4 p1, 0x0

    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 80
    check-cast p1, Lcom/evernote/edam/type/SharedNotebook;

    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/SharedNotebook;->compareTo(Lcom/evernote/edam/type/SharedNotebook;)I

    move-result p1

    return p1
.end method

.method public deepCopy()Lcom/evernote/edam/type/SharedNotebook;
    .locals 1

    .line 158
    new-instance v0, Lcom/evernote/edam/type/SharedNotebook;

    invoke-direct {v0, p0}, Lcom/evernote/edam/type/SharedNotebook;-><init>(Lcom/evernote/edam/type/SharedNotebook;)V

    return-object v0
.end method

.method public bridge synthetic deepCopy()Lcom/evernote/thrift/TBase;
    .locals 1

    .line 80
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->deepCopy()Lcom/evernote/edam/type/SharedNotebook;

    move-result-object v0

    return-object v0
.end method

.method public equals(Lcom/evernote/edam/type/SharedNotebook;)Z
    .locals 6

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 497
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetId()Z

    move-result v1

    .line 498
    invoke-virtual {p1}, Lcom/evernote/edam/type/SharedNotebook;->isSetId()Z

    move-result v2

    if-nez v1, :cond_1

    if-eqz v2, :cond_3

    :cond_1
    if-eqz v1, :cond_34

    if-nez v2, :cond_2

    goto/16 :goto_c

    .line 502
    :cond_2
    iget-wide v1, p0, Lcom/evernote/edam/type/SharedNotebook;->id:J

    iget-wide v3, p1, Lcom/evernote/edam/type/SharedNotebook;->id:J

    cmp-long v5, v1, v3

    if-eqz v5, :cond_3

    return v0

    .line 506
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetUserId()Z

    move-result v1

    .line 507
    invoke-virtual {p1}, Lcom/evernote/edam/type/SharedNotebook;->isSetUserId()Z

    move-result v2

    if-nez v1, :cond_4

    if-eqz v2, :cond_6

    :cond_4
    if-eqz v1, :cond_33

    if-nez v2, :cond_5

    goto/16 :goto_b

    .line 511
    :cond_5
    iget v1, p0, Lcom/evernote/edam/type/SharedNotebook;->userId:I

    iget v2, p1, Lcom/evernote/edam/type/SharedNotebook;->userId:I

    if-eq v1, v2, :cond_6

    return v0

    .line 515
    :cond_6
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetNotebookGuid()Z

    move-result v1

    .line 516
    invoke-virtual {p1}, Lcom/evernote/edam/type/SharedNotebook;->isSetNotebookGuid()Z

    move-result v2

    if-nez v1, :cond_7

    if-eqz v2, :cond_9

    :cond_7
    if-eqz v1, :cond_32

    if-nez v2, :cond_8

    goto/16 :goto_a

    .line 520
    :cond_8
    iget-object v1, p0, Lcom/evernote/edam/type/SharedNotebook;->notebookGuid:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/type/SharedNotebook;->notebookGuid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    return v0

    .line 524
    :cond_9
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetEmail()Z

    move-result v1

    .line 525
    invoke-virtual {p1}, Lcom/evernote/edam/type/SharedNotebook;->isSetEmail()Z

    move-result v2

    if-nez v1, :cond_a

    if-eqz v2, :cond_c

    :cond_a
    if-eqz v1, :cond_31

    if-nez v2, :cond_b

    goto/16 :goto_9

    .line 529
    :cond_b
    iget-object v1, p0, Lcom/evernote/edam/type/SharedNotebook;->email:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/type/SharedNotebook;->email:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    return v0

    .line 533
    :cond_c
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetNotebookModifiable()Z

    move-result v1

    .line 534
    invoke-virtual {p1}, Lcom/evernote/edam/type/SharedNotebook;->isSetNotebookModifiable()Z

    move-result v2

    if-nez v1, :cond_d

    if-eqz v2, :cond_f

    :cond_d
    if-eqz v1, :cond_30

    if-nez v2, :cond_e

    goto/16 :goto_8

    .line 538
    :cond_e
    iget-boolean v1, p0, Lcom/evernote/edam/type/SharedNotebook;->notebookModifiable:Z

    iget-boolean v2, p1, Lcom/evernote/edam/type/SharedNotebook;->notebookModifiable:Z

    if-eq v1, v2, :cond_f

    return v0

    .line 542
    :cond_f
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetRequireLogin()Z

    move-result v1

    .line 543
    invoke-virtual {p1}, Lcom/evernote/edam/type/SharedNotebook;->isSetRequireLogin()Z

    move-result v2

    if-nez v1, :cond_10

    if-eqz v2, :cond_12

    :cond_10
    if-eqz v1, :cond_2f

    if-nez v2, :cond_11

    goto/16 :goto_7

    .line 547
    :cond_11
    iget-boolean v1, p0, Lcom/evernote/edam/type/SharedNotebook;->requireLogin:Z

    iget-boolean v2, p1, Lcom/evernote/edam/type/SharedNotebook;->requireLogin:Z

    if-eq v1, v2, :cond_12

    return v0

    .line 551
    :cond_12
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetServiceCreated()Z

    move-result v1

    .line 552
    invoke-virtual {p1}, Lcom/evernote/edam/type/SharedNotebook;->isSetServiceCreated()Z

    move-result v2

    if-nez v1, :cond_13

    if-eqz v2, :cond_15

    :cond_13
    if-eqz v1, :cond_2e

    if-nez v2, :cond_14

    goto/16 :goto_6

    .line 556
    :cond_14
    iget-wide v1, p0, Lcom/evernote/edam/type/SharedNotebook;->serviceCreated:J

    iget-wide v3, p1, Lcom/evernote/edam/type/SharedNotebook;->serviceCreated:J

    cmp-long v5, v1, v3

    if-eqz v5, :cond_15

    return v0

    .line 560
    :cond_15
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetServiceUpdated()Z

    move-result v1

    .line 561
    invoke-virtual {p1}, Lcom/evernote/edam/type/SharedNotebook;->isSetServiceUpdated()Z

    move-result v2

    if-nez v1, :cond_16

    if-eqz v2, :cond_18

    :cond_16
    if-eqz v1, :cond_2d

    if-nez v2, :cond_17

    goto/16 :goto_5

    .line 565
    :cond_17
    iget-wide v1, p0, Lcom/evernote/edam/type/SharedNotebook;->serviceUpdated:J

    iget-wide v3, p1, Lcom/evernote/edam/type/SharedNotebook;->serviceUpdated:J

    cmp-long v5, v1, v3

    if-eqz v5, :cond_18

    return v0

    .line 569
    :cond_18
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetShareKey()Z

    move-result v1

    .line 570
    invoke-virtual {p1}, Lcom/evernote/edam/type/SharedNotebook;->isSetShareKey()Z

    move-result v2

    if-nez v1, :cond_19

    if-eqz v2, :cond_1b

    :cond_19
    if-eqz v1, :cond_2c

    if-nez v2, :cond_1a

    goto/16 :goto_4

    .line 574
    :cond_1a
    iget-object v1, p0, Lcom/evernote/edam/type/SharedNotebook;->shareKey:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/type/SharedNotebook;->shareKey:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1b

    return v0

    .line 578
    :cond_1b
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetUsername()Z

    move-result v1

    .line 579
    invoke-virtual {p1}, Lcom/evernote/edam/type/SharedNotebook;->isSetUsername()Z

    move-result v2

    if-nez v1, :cond_1c

    if-eqz v2, :cond_1e

    :cond_1c
    if-eqz v1, :cond_2b

    if-nez v2, :cond_1d

    goto :goto_3

    .line 583
    :cond_1d
    iget-object v1, p0, Lcom/evernote/edam/type/SharedNotebook;->username:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/type/SharedNotebook;->username:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1e

    return v0

    .line 587
    :cond_1e
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetPrivilege()Z

    move-result v1

    .line 588
    invoke-virtual {p1}, Lcom/evernote/edam/type/SharedNotebook;->isSetPrivilege()Z

    move-result v2

    if-nez v1, :cond_1f

    if-eqz v2, :cond_21

    :cond_1f
    if-eqz v1, :cond_2a

    if-nez v2, :cond_20

    goto :goto_2

    .line 592
    :cond_20
    iget-object v1, p0, Lcom/evernote/edam/type/SharedNotebook;->privilege:Lcom/evernote/edam/type/SharedNotebookPrivilegeLevel;

    iget-object v2, p1, Lcom/evernote/edam/type/SharedNotebook;->privilege:Lcom/evernote/edam/type/SharedNotebookPrivilegeLevel;

    invoke-virtual {v1, v2}, Lcom/evernote/edam/type/SharedNotebookPrivilegeLevel;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_21

    return v0

    .line 596
    :cond_21
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetAllowPreview()Z

    move-result v1

    .line 597
    invoke-virtual {p1}, Lcom/evernote/edam/type/SharedNotebook;->isSetAllowPreview()Z

    move-result v2

    if-nez v1, :cond_22

    if-eqz v2, :cond_24

    :cond_22
    if-eqz v1, :cond_29

    if-nez v2, :cond_23

    goto :goto_1

    .line 601
    :cond_23
    iget-boolean v1, p0, Lcom/evernote/edam/type/SharedNotebook;->allowPreview:Z

    iget-boolean v2, p1, Lcom/evernote/edam/type/SharedNotebook;->allowPreview:Z

    if-eq v1, v2, :cond_24

    return v0

    .line 605
    :cond_24
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetRecipientSettings()Z

    move-result v1

    .line 606
    invoke-virtual {p1}, Lcom/evernote/edam/type/SharedNotebook;->isSetRecipientSettings()Z

    move-result v2

    if-nez v1, :cond_25

    if-eqz v2, :cond_27

    :cond_25
    if-eqz v1, :cond_28

    if-nez v2, :cond_26

    goto :goto_0

    .line 610
    :cond_26
    iget-object v1, p0, Lcom/evernote/edam/type/SharedNotebook;->recipientSettings:Lcom/evernote/edam/type/SharedNotebookRecipientSettings;

    iget-object p1, p1, Lcom/evernote/edam/type/SharedNotebook;->recipientSettings:Lcom/evernote/edam/type/SharedNotebookRecipientSettings;

    invoke-virtual {v1, p1}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->equals(Lcom/evernote/edam/type/SharedNotebookRecipientSettings;)Z

    move-result p1

    if-nez p1, :cond_27

    return v0

    :cond_27
    const/4 p1, 0x1

    return p1

    :cond_28
    :goto_0
    return v0

    :cond_29
    :goto_1
    return v0

    :cond_2a
    :goto_2
    return v0

    :cond_2b
    :goto_3
    return v0

    :cond_2c
    :goto_4
    return v0

    :cond_2d
    :goto_5
    return v0

    :cond_2e
    :goto_6
    return v0

    :cond_2f
    :goto_7
    return v0

    :cond_30
    :goto_8
    return v0

    :cond_31
    :goto_9
    return v0

    :cond_32
    :goto_a
    return v0

    :cond_33
    :goto_b
    return v0

    :cond_34
    :goto_c
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 488
    :cond_0
    instance-of v1, p1, Lcom/evernote/edam/type/SharedNotebook;

    if-eqz v1, :cond_1

    .line 489
    check-cast p1, Lcom/evernote/edam/type/SharedNotebook;

    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/SharedNotebook;->equals(Lcom/evernote/edam/type/SharedNotebook;)Z

    move-result p1

    return p1

    :cond_1
    return v0
.end method

.method public getEmail()Ljava/lang/String;
    .locals 1

    .line 252
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->email:Ljava/lang/String;

    return-object v0
.end method

.method public getId()J
    .locals 2

    .line 185
    iget-wide v0, p0, Lcom/evernote/edam/type/SharedNotebook;->id:J

    return-wide v0
.end method

.method public getNotebookGuid()Ljava/lang/String;
    .locals 1

    .line 229
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->notebookGuid:Ljava/lang/String;

    return-object v0
.end method

.method public getPrivilege()Lcom/evernote/edam/type/SharedNotebookPrivilegeLevel;
    .locals 1

    .line 413
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->privilege:Lcom/evernote/edam/type/SharedNotebookPrivilegeLevel;

    return-object v0
.end method

.method public getRecipientSettings()Lcom/evernote/edam/type/SharedNotebookRecipientSettings;
    .locals 1

    .line 462
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->recipientSettings:Lcom/evernote/edam/type/SharedNotebookRecipientSettings;

    return-object v0
.end method

.method public getServiceCreated()J
    .locals 2

    .line 319
    iget-wide v0, p0, Lcom/evernote/edam/type/SharedNotebook;->serviceCreated:J

    return-wide v0
.end method

.method public getServiceUpdated()J
    .locals 2

    .line 341
    iget-wide v0, p0, Lcom/evernote/edam/type/SharedNotebook;->serviceUpdated:J

    return-wide v0
.end method

.method public getShareKey()Ljava/lang/String;
    .locals 1

    .line 363
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->shareKey:Ljava/lang/String;

    return-object v0
.end method

.method public getUserId()I
    .locals 1

    .line 207
    iget v0, p0, Lcom/evernote/edam/type/SharedNotebook;->userId:I

    return v0
.end method

.method public getUsername()Ljava/lang/String;
    .locals 1

    .line 386
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->username:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isAllowPreview()Z
    .locals 1

    .line 440
    iget-boolean v0, p0, Lcom/evernote/edam/type/SharedNotebook;->allowPreview:Z

    return v0
.end method

.method public isNotebookModifiable()Z
    .locals 1

    .line 275
    iget-boolean v0, p0, Lcom/evernote/edam/type/SharedNotebook;->notebookModifiable:Z

    return v0
.end method

.method public isRequireLogin()Z
    .locals 1

    .line 297
    iget-boolean v0, p0, Lcom/evernote/edam/type/SharedNotebook;->requireLogin:Z

    return v0
.end method

.method public isSetAllowPreview()Z
    .locals 2

    .line 454
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->__isset_vector:[Z

    const/4 v1, 0x6

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetEmail()Z
    .locals 1

    .line 265
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->email:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetId()Z
    .locals 2

    .line 199
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->__isset_vector:[Z

    const/4 v1, 0x0

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetNotebookGuid()Z
    .locals 1

    .line 242
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->notebookGuid:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetNotebookModifiable()Z
    .locals 2

    .line 289
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->__isset_vector:[Z

    const/4 v1, 0x2

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetPrivilege()Z
    .locals 1

    .line 430
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->privilege:Lcom/evernote/edam/type/SharedNotebookPrivilegeLevel;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetRecipientSettings()Z
    .locals 1

    .line 475
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->recipientSettings:Lcom/evernote/edam/type/SharedNotebookRecipientSettings;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetRequireLogin()Z
    .locals 2

    .line 311
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->__isset_vector:[Z

    const/4 v1, 0x3

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetServiceCreated()Z
    .locals 2

    .line 333
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->__isset_vector:[Z

    const/4 v1, 0x4

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetServiceUpdated()Z
    .locals 2

    .line 355
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->__isset_vector:[Z

    const/4 v1, 0x5

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetShareKey()Z
    .locals 1

    .line 376
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->shareKey:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetUserId()Z
    .locals 2

    .line 221
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->__isset_vector:[Z

    const/4 v1, 0x1

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetUsername()Z
    .locals 1

    .line 399
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->username:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public read(Lcom/evernote/thrift/protocol/TProtocol;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 752
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readStructBegin()Lcom/evernote/thrift/protocol/TStruct;

    .line 755
    :goto_0
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readFieldBegin()Lcom/evernote/thrift/protocol/TField;

    move-result-object v0

    .line 756
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-nez v1, :cond_0

    .line 864
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readStructEnd()V

    .line 865
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->validate()V

    return-void

    .line 759
    :cond_0
    iget-short v1, v0, Lcom/evernote/thrift/protocol/TField;->id:S

    const/16 v2, 0x8

    const/4 v3, 0x2

    const/16 v4, 0xa

    const/16 v5, 0xb

    const/4 v6, 0x1

    packed-switch v1, :pswitch_data_0

    .line 860
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 852
    :pswitch_0
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    const/16 v2, 0xc

    if-ne v1, v2, :cond_1

    .line 853
    new-instance v0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;

    invoke-direct {v0}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;-><init>()V

    iput-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->recipientSettings:Lcom/evernote/edam/type/SharedNotebookRecipientSettings;

    .line 854
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->recipientSettings:Lcom/evernote/edam/type/SharedNotebookRecipientSettings;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    goto/16 :goto_1

    .line 856
    :cond_1
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 844
    :pswitch_1
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v3, :cond_2

    .line 845
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/type/SharedNotebook;->allowPreview:Z

    .line 846
    invoke-virtual {p0, v6}, Lcom/evernote/edam/type/SharedNotebook;->setAllowPreviewIsSet(Z)V

    goto/16 :goto_1

    .line 848
    :cond_2
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 837
    :pswitch_2
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v2, :cond_3

    .line 838
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI32()I

    move-result v0

    invoke-static {v0}, Lcom/evernote/edam/type/SharedNotebookPrivilegeLevel;->findByValue(I)Lcom/evernote/edam/type/SharedNotebookPrivilegeLevel;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->privilege:Lcom/evernote/edam/type/SharedNotebookPrivilegeLevel;

    goto/16 :goto_1

    .line 840
    :cond_3
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 815
    :pswitch_3
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_4

    .line 816
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/evernote/edam/type/SharedNotebook;->serviceUpdated:J

    .line 817
    invoke-virtual {p0, v6}, Lcom/evernote/edam/type/SharedNotebook;->setServiceUpdatedIsSet(Z)V

    goto/16 :goto_1

    .line 819
    :cond_4
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 830
    :pswitch_4
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v5, :cond_5

    .line 831
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->username:Ljava/lang/String;

    goto/16 :goto_1

    .line 833
    :cond_5
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 823
    :pswitch_5
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v5, :cond_6

    .line 824
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->shareKey:Ljava/lang/String;

    goto/16 :goto_1

    .line 826
    :cond_6
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 807
    :pswitch_6
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_7

    .line 808
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/evernote/edam/type/SharedNotebook;->serviceCreated:J

    .line 809
    invoke-virtual {p0, v6}, Lcom/evernote/edam/type/SharedNotebook;->setServiceCreatedIsSet(Z)V

    goto/16 :goto_1

    .line 811
    :cond_7
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 799
    :pswitch_7
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v3, :cond_8

    .line 800
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/type/SharedNotebook;->requireLogin:Z

    .line 801
    invoke-virtual {p0, v6}, Lcom/evernote/edam/type/SharedNotebook;->setRequireLoginIsSet(Z)V

    goto :goto_1

    .line 803
    :cond_8
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 791
    :pswitch_8
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v3, :cond_9

    .line 792
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/type/SharedNotebook;->notebookModifiable:Z

    .line 793
    invoke-virtual {p0, v6}, Lcom/evernote/edam/type/SharedNotebook;->setNotebookModifiableIsSet(Z)V

    goto :goto_1

    .line 795
    :cond_9
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 784
    :pswitch_9
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v5, :cond_a

    .line 785
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->email:Ljava/lang/String;

    goto :goto_1

    .line 787
    :cond_a
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 777
    :pswitch_a
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v5, :cond_b

    .line 778
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->notebookGuid:Ljava/lang/String;

    goto :goto_1

    .line 780
    :cond_b
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 769
    :pswitch_b
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v2, :cond_c

    .line 770
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI32()I

    move-result v0

    iput v0, p0, Lcom/evernote/edam/type/SharedNotebook;->userId:I

    .line 771
    invoke-virtual {p0, v6}, Lcom/evernote/edam/type/SharedNotebook;->setUserIdIsSet(Z)V

    goto :goto_1

    .line 773
    :cond_c
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 761
    :pswitch_c
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_d

    .line 762
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/evernote/edam/type/SharedNotebook;->id:J

    .line 763
    invoke-virtual {p0, v6}, Lcom/evernote/edam/type/SharedNotebook;->setIdIsSet(Z)V

    goto :goto_1

    .line 765
    :cond_d
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    .line 862
    :goto_1
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readFieldEnd()V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setAllowPreview(Z)V
    .locals 0

    .line 444
    iput-boolean p1, p0, Lcom/evernote/edam/type/SharedNotebook;->allowPreview:Z

    const/4 p1, 0x1

    .line 445
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/SharedNotebook;->setAllowPreviewIsSet(Z)V

    return-void
.end method

.method public setAllowPreviewIsSet(Z)V
    .locals 2

    .line 458
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->__isset_vector:[Z

    const/4 v1, 0x6

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setEmail(Ljava/lang/String;)V
    .locals 0

    .line 256
    iput-object p1, p0, Lcom/evernote/edam/type/SharedNotebook;->email:Ljava/lang/String;

    return-void
.end method

.method public setEmailIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 270
    iput-object p1, p0, Lcom/evernote/edam/type/SharedNotebook;->email:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setId(J)V
    .locals 0

    .line 189
    iput-wide p1, p0, Lcom/evernote/edam/type/SharedNotebook;->id:J

    const/4 p1, 0x1

    .line 190
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/SharedNotebook;->setIdIsSet(Z)V

    return-void
.end method

.method public setIdIsSet(Z)V
    .locals 2

    .line 203
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->__isset_vector:[Z

    const/4 v1, 0x0

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setNotebookGuid(Ljava/lang/String;)V
    .locals 0

    .line 233
    iput-object p1, p0, Lcom/evernote/edam/type/SharedNotebook;->notebookGuid:Ljava/lang/String;

    return-void
.end method

.method public setNotebookGuidIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 247
    iput-object p1, p0, Lcom/evernote/edam/type/SharedNotebook;->notebookGuid:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setNotebookModifiable(Z)V
    .locals 0

    .line 279
    iput-boolean p1, p0, Lcom/evernote/edam/type/SharedNotebook;->notebookModifiable:Z

    const/4 p1, 0x1

    .line 280
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/SharedNotebook;->setNotebookModifiableIsSet(Z)V

    return-void
.end method

.method public setNotebookModifiableIsSet(Z)V
    .locals 2

    .line 293
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->__isset_vector:[Z

    const/4 v1, 0x2

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setPrivilege(Lcom/evernote/edam/type/SharedNotebookPrivilegeLevel;)V
    .locals 0

    .line 421
    iput-object p1, p0, Lcom/evernote/edam/type/SharedNotebook;->privilege:Lcom/evernote/edam/type/SharedNotebookPrivilegeLevel;

    return-void
.end method

.method public setPrivilegeIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 435
    iput-object p1, p0, Lcom/evernote/edam/type/SharedNotebook;->privilege:Lcom/evernote/edam/type/SharedNotebookPrivilegeLevel;

    :cond_0
    return-void
.end method

.method public setRecipientSettings(Lcom/evernote/edam/type/SharedNotebookRecipientSettings;)V
    .locals 0

    .line 466
    iput-object p1, p0, Lcom/evernote/edam/type/SharedNotebook;->recipientSettings:Lcom/evernote/edam/type/SharedNotebookRecipientSettings;

    return-void
.end method

.method public setRecipientSettingsIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 480
    iput-object p1, p0, Lcom/evernote/edam/type/SharedNotebook;->recipientSettings:Lcom/evernote/edam/type/SharedNotebookRecipientSettings;

    :cond_0
    return-void
.end method

.method public setRequireLogin(Z)V
    .locals 0

    .line 301
    iput-boolean p1, p0, Lcom/evernote/edam/type/SharedNotebook;->requireLogin:Z

    const/4 p1, 0x1

    .line 302
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/SharedNotebook;->setRequireLoginIsSet(Z)V

    return-void
.end method

.method public setRequireLoginIsSet(Z)V
    .locals 2

    .line 315
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->__isset_vector:[Z

    const/4 v1, 0x3

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setServiceCreated(J)V
    .locals 0

    .line 323
    iput-wide p1, p0, Lcom/evernote/edam/type/SharedNotebook;->serviceCreated:J

    const/4 p1, 0x1

    .line 324
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/SharedNotebook;->setServiceCreatedIsSet(Z)V

    return-void
.end method

.method public setServiceCreatedIsSet(Z)V
    .locals 2

    .line 337
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->__isset_vector:[Z

    const/4 v1, 0x4

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setServiceUpdated(J)V
    .locals 0

    .line 345
    iput-wide p1, p0, Lcom/evernote/edam/type/SharedNotebook;->serviceUpdated:J

    const/4 p1, 0x1

    .line 346
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/SharedNotebook;->setServiceUpdatedIsSet(Z)V

    return-void
.end method

.method public setServiceUpdatedIsSet(Z)V
    .locals 2

    .line 359
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->__isset_vector:[Z

    const/4 v1, 0x5

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setShareKey(Ljava/lang/String;)V
    .locals 0

    .line 367
    iput-object p1, p0, Lcom/evernote/edam/type/SharedNotebook;->shareKey:Ljava/lang/String;

    return-void
.end method

.method public setShareKeyIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 381
    iput-object p1, p0, Lcom/evernote/edam/type/SharedNotebook;->shareKey:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setUserId(I)V
    .locals 0

    .line 211
    iput p1, p0, Lcom/evernote/edam/type/SharedNotebook;->userId:I

    const/4 p1, 0x1

    .line 212
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/SharedNotebook;->setUserIdIsSet(Z)V

    return-void
.end method

.method public setUserIdIsSet(Z)V
    .locals 2

    .line 225
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->__isset_vector:[Z

    const/4 v1, 0x1

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setUsername(Ljava/lang/String;)V
    .locals 0

    .line 390
    iput-object p1, p0, Lcom/evernote/edam/type/SharedNotebook;->username:Ljava/lang/String;

    return-void
.end method

.method public setUsernameIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 404
    iput-object p1, p0, Lcom/evernote/edam/type/SharedNotebook;->username:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .line 955
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SharedNotebook("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 958
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetId()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    const-string v1, "id:"

    .line 959
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 960
    iget-wide v3, p0, Lcom/evernote/edam/type/SharedNotebook;->id:J

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    const/4 v1, 0x1

    .line 963
    :goto_0
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetUserId()Z

    move-result v3

    if-eqz v3, :cond_2

    if-nez v1, :cond_1

    const-string v1, ", "

    .line 964
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const-string v1, "userId:"

    .line 965
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 966
    iget v1, p0, Lcom/evernote/edam/type/SharedNotebook;->userId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 969
    :cond_2
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetNotebookGuid()Z

    move-result v3

    if-eqz v3, :cond_5

    if-nez v1, :cond_3

    const-string v1, ", "

    .line 970
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    const-string v1, "notebookGuid:"

    .line 971
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 972
    iget-object v1, p0, Lcom/evernote/edam/type/SharedNotebook;->notebookGuid:Ljava/lang/String;

    if-nez v1, :cond_4

    const-string v1, "null"

    .line 973
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 975
    :cond_4
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_1
    const/4 v1, 0x0

    .line 979
    :cond_5
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetEmail()Z

    move-result v3

    if-eqz v3, :cond_8

    if-nez v1, :cond_6

    const-string v1, ", "

    .line 980
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_6
    const-string v1, "email:"

    .line 981
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 982
    iget-object v1, p0, Lcom/evernote/edam/type/SharedNotebook;->email:Ljava/lang/String;

    if-nez v1, :cond_7

    const-string v1, "null"

    .line 983
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 985
    :cond_7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_2
    const/4 v1, 0x0

    .line 989
    :cond_8
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetNotebookModifiable()Z

    move-result v3

    if-eqz v3, :cond_a

    if-nez v1, :cond_9

    const-string v1, ", "

    .line 990
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_9
    const-string v1, "notebookModifiable:"

    .line 991
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 992
    iget-boolean v1, p0, Lcom/evernote/edam/type/SharedNotebook;->notebookModifiable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 995
    :cond_a
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetRequireLogin()Z

    move-result v3

    if-eqz v3, :cond_c

    if-nez v1, :cond_b

    const-string v1, ", "

    .line 996
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_b
    const-string v1, "requireLogin:"

    .line 997
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 998
    iget-boolean v1, p0, Lcom/evernote/edam/type/SharedNotebook;->requireLogin:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1001
    :cond_c
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetServiceCreated()Z

    move-result v3

    if-eqz v3, :cond_e

    if-nez v1, :cond_d

    const-string v1, ", "

    .line 1002
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_d
    const-string v1, "serviceCreated:"

    .line 1003
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1004
    iget-wide v3, p0, Lcom/evernote/edam/type/SharedNotebook;->serviceCreated:J

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1007
    :cond_e
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetServiceUpdated()Z

    move-result v3

    if-eqz v3, :cond_10

    if-nez v1, :cond_f

    const-string v1, ", "

    .line 1008
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_f
    const-string v1, "serviceUpdated:"

    .line 1009
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1010
    iget-wide v3, p0, Lcom/evernote/edam/type/SharedNotebook;->serviceUpdated:J

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1013
    :cond_10
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetShareKey()Z

    move-result v3

    if-eqz v3, :cond_13

    if-nez v1, :cond_11

    const-string v1, ", "

    .line 1014
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_11
    const-string v1, "shareKey:"

    .line 1015
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1016
    iget-object v1, p0, Lcom/evernote/edam/type/SharedNotebook;->shareKey:Ljava/lang/String;

    if-nez v1, :cond_12

    const-string v1, "null"

    .line 1017
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 1019
    :cond_12
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    const/4 v1, 0x0

    .line 1023
    :cond_13
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetUsername()Z

    move-result v3

    if-eqz v3, :cond_16

    if-nez v1, :cond_14

    const-string v1, ", "

    .line 1024
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_14
    const-string v1, "username:"

    .line 1025
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1026
    iget-object v1, p0, Lcom/evernote/edam/type/SharedNotebook;->username:Ljava/lang/String;

    if-nez v1, :cond_15

    const-string v1, "null"

    .line 1027
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 1029
    :cond_15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_4
    const/4 v1, 0x0

    .line 1033
    :cond_16
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetPrivilege()Z

    move-result v3

    if-eqz v3, :cond_19

    if-nez v1, :cond_17

    const-string v1, ", "

    .line 1034
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_17
    const-string v1, "privilege:"

    .line 1035
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1036
    iget-object v1, p0, Lcom/evernote/edam/type/SharedNotebook;->privilege:Lcom/evernote/edam/type/SharedNotebookPrivilegeLevel;

    if-nez v1, :cond_18

    const-string v1, "null"

    .line 1037
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5

    .line 1039
    :cond_18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :goto_5
    const/4 v1, 0x0

    .line 1043
    :cond_19
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetAllowPreview()Z

    move-result v3

    if-eqz v3, :cond_1b

    if-nez v1, :cond_1a

    const-string v1, ", "

    .line 1044
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1a
    const-string v1, "allowPreview:"

    .line 1045
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1046
    iget-boolean v1, p0, Lcom/evernote/edam/type/SharedNotebook;->allowPreview:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1049
    :cond_1b
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetRecipientSettings()Z

    move-result v2

    if-eqz v2, :cond_1e

    if-nez v1, :cond_1c

    const-string v1, ", "

    .line 1050
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1c
    const-string v1, "recipientSettings:"

    .line 1051
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1052
    iget-object v1, p0, Lcom/evernote/edam/type/SharedNotebook;->recipientSettings:Lcom/evernote/edam/type/SharedNotebookRecipientSettings;

    if-nez v1, :cond_1d

    const-string v1, "null"

    .line 1053
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6

    .line 1055
    :cond_1d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1e
    :goto_6
    const-string v1, ")"

    .line 1059
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1060
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public unsetAllowPreview()V
    .locals 3

    .line 449
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->__isset_vector:[Z

    const/4 v1, 0x6

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetEmail()V
    .locals 1

    const/4 v0, 0x0

    .line 260
    iput-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->email:Ljava/lang/String;

    return-void
.end method

.method public unsetId()V
    .locals 2

    .line 194
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->__isset_vector:[Z

    const/4 v1, 0x0

    aput-boolean v1, v0, v1

    return-void
.end method

.method public unsetNotebookGuid()V
    .locals 1

    const/4 v0, 0x0

    .line 237
    iput-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->notebookGuid:Ljava/lang/String;

    return-void
.end method

.method public unsetNotebookModifiable()V
    .locals 3

    .line 284
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->__isset_vector:[Z

    const/4 v1, 0x2

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetPrivilege()V
    .locals 1

    const/4 v0, 0x0

    .line 425
    iput-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->privilege:Lcom/evernote/edam/type/SharedNotebookPrivilegeLevel;

    return-void
.end method

.method public unsetRecipientSettings()V
    .locals 1

    const/4 v0, 0x0

    .line 470
    iput-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->recipientSettings:Lcom/evernote/edam/type/SharedNotebookRecipientSettings;

    return-void
.end method

.method public unsetRequireLogin()V
    .locals 3

    .line 306
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->__isset_vector:[Z

    const/4 v1, 0x3

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetServiceCreated()V
    .locals 3

    .line 328
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->__isset_vector:[Z

    const/4 v1, 0x4

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetServiceUpdated()V
    .locals 3

    .line 350
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->__isset_vector:[Z

    const/4 v1, 0x5

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetShareKey()V
    .locals 1

    const/4 v0, 0x0

    .line 371
    iput-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->shareKey:Ljava/lang/String;

    return-void
.end method

.method public unsetUserId()V
    .locals 3

    .line 216
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->__isset_vector:[Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetUsername()V
    .locals 1

    const/4 v0, 0x0

    .line 394
    iput-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->username:Ljava/lang/String;

    return-void
.end method

.method public validate()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    return-void
.end method

.method public write(Lcom/evernote/thrift/protocol/TProtocol;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 869
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->validate()V

    .line 871
    sget-object v0, Lcom/evernote/edam/type/SharedNotebook;->STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeStructBegin(Lcom/evernote/thrift/protocol/TStruct;)V

    .line 872
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetId()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 873
    sget-object v0, Lcom/evernote/edam/type/SharedNotebook;->ID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 874
    iget-wide v0, p0, Lcom/evernote/edam/type/SharedNotebook;->id:J

    invoke-virtual {p1, v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeI64(J)V

    .line 875
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 877
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetUserId()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 878
    sget-object v0, Lcom/evernote/edam/type/SharedNotebook;->USER_ID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 879
    iget v0, p0, Lcom/evernote/edam/type/SharedNotebook;->userId:I

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeI32(I)V

    .line 880
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 882
    :cond_1
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->notebookGuid:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 883
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetNotebookGuid()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 884
    sget-object v0, Lcom/evernote/edam/type/SharedNotebook;->NOTEBOOK_GUID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 885
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->notebookGuid:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 886
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 889
    :cond_2
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->email:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 890
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetEmail()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 891
    sget-object v0, Lcom/evernote/edam/type/SharedNotebook;->EMAIL_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 892
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->email:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 893
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 896
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetNotebookModifiable()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 897
    sget-object v0, Lcom/evernote/edam/type/SharedNotebook;->NOTEBOOK_MODIFIABLE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 898
    iget-boolean v0, p0, Lcom/evernote/edam/type/SharedNotebook;->notebookModifiable:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 899
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 901
    :cond_4
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetRequireLogin()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 902
    sget-object v0, Lcom/evernote/edam/type/SharedNotebook;->REQUIRE_LOGIN_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 903
    iget-boolean v0, p0, Lcom/evernote/edam/type/SharedNotebook;->requireLogin:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 904
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 906
    :cond_5
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetServiceCreated()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 907
    sget-object v0, Lcom/evernote/edam/type/SharedNotebook;->SERVICE_CREATED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 908
    iget-wide v0, p0, Lcom/evernote/edam/type/SharedNotebook;->serviceCreated:J

    invoke-virtual {p1, v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeI64(J)V

    .line 909
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 911
    :cond_6
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->shareKey:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 912
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetShareKey()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 913
    sget-object v0, Lcom/evernote/edam/type/SharedNotebook;->SHARE_KEY_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 914
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->shareKey:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 915
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 918
    :cond_7
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->username:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 919
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetUsername()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 920
    sget-object v0, Lcom/evernote/edam/type/SharedNotebook;->USERNAME_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 921
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->username:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 922
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 925
    :cond_8
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetServiceUpdated()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 926
    sget-object v0, Lcom/evernote/edam/type/SharedNotebook;->SERVICE_UPDATED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 927
    iget-wide v0, p0, Lcom/evernote/edam/type/SharedNotebook;->serviceUpdated:J

    invoke-virtual {p1, v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeI64(J)V

    .line 928
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 930
    :cond_9
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->privilege:Lcom/evernote/edam/type/SharedNotebookPrivilegeLevel;

    if-eqz v0, :cond_a

    .line 931
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetPrivilege()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 932
    sget-object v0, Lcom/evernote/edam/type/SharedNotebook;->PRIVILEGE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 933
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->privilege:Lcom/evernote/edam/type/SharedNotebookPrivilegeLevel;

    invoke-virtual {v0}, Lcom/evernote/edam/type/SharedNotebookPrivilegeLevel;->getValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeI32(I)V

    .line 934
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 937
    :cond_a
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetAllowPreview()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 938
    sget-object v0, Lcom/evernote/edam/type/SharedNotebook;->ALLOW_PREVIEW_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 939
    iget-boolean v0, p0, Lcom/evernote/edam/type/SharedNotebook;->allowPreview:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 940
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 942
    :cond_b
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->recipientSettings:Lcom/evernote/edam/type/SharedNotebookRecipientSettings;

    if-eqz v0, :cond_c

    .line 943
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebook;->isSetRecipientSettings()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 944
    sget-object v0, Lcom/evernote/edam/type/SharedNotebook;->RECIPIENT_SETTINGS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 945
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebook;->recipientSettings:Lcom/evernote/edam/type/SharedNotebookRecipientSettings;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 946
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 949
    :cond_c
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldStop()V

    .line 950
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeStructEnd()V

    return-void
.end method

.class public Lcom/evernote/edam/type/PremiumInfo;
.super Ljava/lang/Object;
.source "PremiumInfo.java"

# interfaces
.implements Lcom/evernote/thrift/TBase;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/evernote/thrift/TBase<",
        "Lcom/evernote/edam/type/PremiumInfo;",
        ">;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# static fields
.field private static final CAN_PURCHASE_UPLOAD_ALLOWANCE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final CURRENT_TIME_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final PREMIUM_CANCELLATION_PENDING_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final PREMIUM_EXPIRATION_DATE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final PREMIUM_EXTENDABLE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final PREMIUM_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final PREMIUM_PENDING_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final PREMIUM_RECURRING_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final PREMIUM_UPGRADABLE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final SPONSORED_GROUP_NAME_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final SPONSORED_GROUP_ROLE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

.field private static final __CANPURCHASEUPLOADALLOWANCE_ISSET_ID:I = 0x7

.field private static final __CURRENTTIME_ISSET_ID:I = 0x0

.field private static final __PREMIUMCANCELLATIONPENDING_ISSET_ID:I = 0x6

.field private static final __PREMIUMEXPIRATIONDATE_ISSET_ID:I = 0x3

.field private static final __PREMIUMEXTENDABLE_ISSET_ID:I = 0x4

.field private static final __PREMIUMPENDING_ISSET_ID:I = 0x5

.field private static final __PREMIUMRECURRING_ISSET_ID:I = 0x2

.field private static final __PREMIUMUPGRADABLE_ISSET_ID:I = 0x8

.field private static final __PREMIUM_ISSET_ID:I = 0x1


# instance fields
.field private __isset_vector:[Z

.field private canPurchaseUploadAllowance:Z

.field private currentTime:J

.field private premium:Z

.field private premiumCancellationPending:Z

.field private premiumExpirationDate:J

.field private premiumExtendable:Z

.field private premiumPending:Z

.field private premiumRecurring:Z

.field private premiumUpgradable:Z

.field private sponsoredGroupName:Ljava/lang/String;

.field private sponsoredGroupRole:Lcom/evernote/edam/type/SponsoredGroupRole;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 70
    new-instance v0, Lcom/evernote/thrift/protocol/TStruct;

    const-string v1, "PremiumInfo"

    invoke-direct {v0, v1}, Lcom/evernote/thrift/protocol/TStruct;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/evernote/edam/type/PremiumInfo;->STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

    .line 72
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "currentTime"

    const/16 v2, 0xa

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/PremiumInfo;->CURRENT_TIME_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 73
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "premium"

    const/4 v3, 0x2

    invoke-direct {v0, v1, v3, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/PremiumInfo;->PREMIUM_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 74
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "premiumRecurring"

    const/4 v4, 0x3

    invoke-direct {v0, v1, v3, v4}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/PremiumInfo;->PREMIUM_RECURRING_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 75
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "premiumExpirationDate"

    const/4 v4, 0x4

    invoke-direct {v0, v1, v2, v4}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/PremiumInfo;->PREMIUM_EXPIRATION_DATE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 76
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "premiumExtendable"

    const/4 v4, 0x5

    invoke-direct {v0, v1, v3, v4}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/PremiumInfo;->PREMIUM_EXTENDABLE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 77
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "premiumPending"

    const/4 v4, 0x6

    invoke-direct {v0, v1, v3, v4}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/PremiumInfo;->PREMIUM_PENDING_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 78
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "premiumCancellationPending"

    const/4 v4, 0x7

    invoke-direct {v0, v1, v3, v4}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/PremiumInfo;->PREMIUM_CANCELLATION_PENDING_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 79
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "canPurchaseUploadAllowance"

    const/16 v4, 0x8

    invoke-direct {v0, v1, v3, v4}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/PremiumInfo;->CAN_PURCHASE_UPLOAD_ALLOWANCE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 80
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "sponsoredGroupName"

    const/16 v5, 0xb

    const/16 v6, 0x9

    invoke-direct {v0, v1, v5, v6}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/PremiumInfo;->SPONSORED_GROUP_NAME_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 81
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "sponsoredGroupRole"

    invoke-direct {v0, v1, v4, v2}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/PremiumInfo;->SPONSORED_GROUP_ROLE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 82
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "premiumUpgradable"

    invoke-direct {v0, v1, v3, v5}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/PremiumInfo;->PREMIUM_UPGRADABLE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x9

    .line 107
    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/evernote/edam/type/PremiumInfo;->__isset_vector:[Z

    return-void
.end method

.method public constructor <init>(JZZZZZZ)V
    .locals 0

    .line 121
    invoke-direct {p0}, Lcom/evernote/edam/type/PremiumInfo;-><init>()V

    .line 122
    iput-wide p1, p0, Lcom/evernote/edam/type/PremiumInfo;->currentTime:J

    const/4 p1, 0x1

    .line 123
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/PremiumInfo;->setCurrentTimeIsSet(Z)V

    .line 124
    iput-boolean p3, p0, Lcom/evernote/edam/type/PremiumInfo;->premium:Z

    .line 125
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/PremiumInfo;->setPremiumIsSet(Z)V

    .line 126
    iput-boolean p4, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumRecurring:Z

    .line 127
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/PremiumInfo;->setPremiumRecurringIsSet(Z)V

    .line 128
    iput-boolean p5, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumExtendable:Z

    .line 129
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/PremiumInfo;->setPremiumExtendableIsSet(Z)V

    .line 130
    iput-boolean p6, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumPending:Z

    .line 131
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/PremiumInfo;->setPremiumPendingIsSet(Z)V

    .line 132
    iput-boolean p7, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumCancellationPending:Z

    .line 133
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/PremiumInfo;->setPremiumCancellationPendingIsSet(Z)V

    .line 134
    iput-boolean p8, p0, Lcom/evernote/edam/type/PremiumInfo;->canPurchaseUploadAllowance:Z

    .line 135
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/PremiumInfo;->setCanPurchaseUploadAllowanceIsSet(Z)V

    return-void
.end method

.method public constructor <init>(Lcom/evernote/edam/type/PremiumInfo;)V
    .locals 4

    .line 141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x9

    .line 107
    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/evernote/edam/type/PremiumInfo;->__isset_vector:[Z

    .line 142
    iget-object v0, p1, Lcom/evernote/edam/type/PremiumInfo;->__isset_vector:[Z

    iget-object v1, p0, Lcom/evernote/edam/type/PremiumInfo;->__isset_vector:[Z

    array-length v2, v0

    const/4 v3, 0x0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 143
    iget-wide v0, p1, Lcom/evernote/edam/type/PremiumInfo;->currentTime:J

    iput-wide v0, p0, Lcom/evernote/edam/type/PremiumInfo;->currentTime:J

    .line 144
    iget-boolean v0, p1, Lcom/evernote/edam/type/PremiumInfo;->premium:Z

    iput-boolean v0, p0, Lcom/evernote/edam/type/PremiumInfo;->premium:Z

    .line 145
    iget-boolean v0, p1, Lcom/evernote/edam/type/PremiumInfo;->premiumRecurring:Z

    iput-boolean v0, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumRecurring:Z

    .line 146
    iget-wide v0, p1, Lcom/evernote/edam/type/PremiumInfo;->premiumExpirationDate:J

    iput-wide v0, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumExpirationDate:J

    .line 147
    iget-boolean v0, p1, Lcom/evernote/edam/type/PremiumInfo;->premiumExtendable:Z

    iput-boolean v0, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumExtendable:Z

    .line 148
    iget-boolean v0, p1, Lcom/evernote/edam/type/PremiumInfo;->premiumPending:Z

    iput-boolean v0, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumPending:Z

    .line 149
    iget-boolean v0, p1, Lcom/evernote/edam/type/PremiumInfo;->premiumCancellationPending:Z

    iput-boolean v0, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumCancellationPending:Z

    .line 150
    iget-boolean v0, p1, Lcom/evernote/edam/type/PremiumInfo;->canPurchaseUploadAllowance:Z

    iput-boolean v0, p0, Lcom/evernote/edam/type/PremiumInfo;->canPurchaseUploadAllowance:Z

    .line 151
    invoke-virtual {p1}, Lcom/evernote/edam/type/PremiumInfo;->isSetSponsoredGroupName()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p1, Lcom/evernote/edam/type/PremiumInfo;->sponsoredGroupName:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/type/PremiumInfo;->sponsoredGroupName:Ljava/lang/String;

    .line 154
    :cond_0
    invoke-virtual {p1}, Lcom/evernote/edam/type/PremiumInfo;->isSetSponsoredGroupRole()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 155
    iget-object v0, p1, Lcom/evernote/edam/type/PremiumInfo;->sponsoredGroupRole:Lcom/evernote/edam/type/SponsoredGroupRole;

    iput-object v0, p0, Lcom/evernote/edam/type/PremiumInfo;->sponsoredGroupRole:Lcom/evernote/edam/type/SponsoredGroupRole;

    .line 157
    :cond_1
    iget-boolean p1, p1, Lcom/evernote/edam/type/PremiumInfo;->premiumUpgradable:Z

    iput-boolean p1, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumUpgradable:Z

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 3

    const/4 v0, 0x0

    .line 165
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/PremiumInfo;->setCurrentTimeIsSet(Z)V

    const-wide/16 v1, 0x0

    .line 166
    iput-wide v1, p0, Lcom/evernote/edam/type/PremiumInfo;->currentTime:J

    .line 167
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/PremiumInfo;->setPremiumIsSet(Z)V

    .line 168
    iput-boolean v0, p0, Lcom/evernote/edam/type/PremiumInfo;->premium:Z

    .line 169
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/PremiumInfo;->setPremiumRecurringIsSet(Z)V

    .line 170
    iput-boolean v0, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumRecurring:Z

    .line 171
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/PremiumInfo;->setPremiumExpirationDateIsSet(Z)V

    .line 172
    iput-wide v1, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumExpirationDate:J

    .line 173
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/PremiumInfo;->setPremiumExtendableIsSet(Z)V

    .line 174
    iput-boolean v0, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumExtendable:Z

    .line 175
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/PremiumInfo;->setPremiumPendingIsSet(Z)V

    .line 176
    iput-boolean v0, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumPending:Z

    .line 177
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/PremiumInfo;->setPremiumCancellationPendingIsSet(Z)V

    .line 178
    iput-boolean v0, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumCancellationPending:Z

    .line 179
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/PremiumInfo;->setCanPurchaseUploadAllowanceIsSet(Z)V

    .line 180
    iput-boolean v0, p0, Lcom/evernote/edam/type/PremiumInfo;->canPurchaseUploadAllowance:Z

    const/4 v1, 0x0

    .line 181
    iput-object v1, p0, Lcom/evernote/edam/type/PremiumInfo;->sponsoredGroupName:Ljava/lang/String;

    .line 182
    iput-object v1, p0, Lcom/evernote/edam/type/PremiumInfo;->sponsoredGroupRole:Lcom/evernote/edam/type/SponsoredGroupRole;

    .line 183
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/PremiumInfo;->setPremiumUpgradableIsSet(Z)V

    .line 184
    iput-boolean v0, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumUpgradable:Z

    return-void
.end method

.method public compareTo(Lcom/evernote/edam/type/PremiumInfo;)I
    .locals 4

    .line 560
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 561
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    return p1

    .line 567
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/edam/type/PremiumInfo;->isSetCurrentTime()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/PremiumInfo;->isSetCurrentTime()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_1

    return v0

    .line 571
    :cond_1
    invoke-virtual {p0}, Lcom/evernote/edam/type/PremiumInfo;->isSetCurrentTime()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-wide v0, p0, Lcom/evernote/edam/type/PremiumInfo;->currentTime:J

    iget-wide v2, p1, Lcom/evernote/edam/type/PremiumInfo;->currentTime:J

    invoke-static {v0, v1, v2, v3}, Lcom/evernote/thrift/TBaseHelper;->compareTo(JJ)I

    move-result v0

    if-eqz v0, :cond_2

    return v0

    .line 576
    :cond_2
    invoke-virtual {p0}, Lcom/evernote/edam/type/PremiumInfo;->isSetPremium()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/PremiumInfo;->isSetPremium()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_3

    return v0

    .line 580
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/type/PremiumInfo;->isSetPremium()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/evernote/edam/type/PremiumInfo;->premium:Z

    iget-boolean v1, p1, Lcom/evernote/edam/type/PremiumInfo;->premium:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_4

    return v0

    .line 585
    :cond_4
    invoke-virtual {p0}, Lcom/evernote/edam/type/PremiumInfo;->isSetPremiumRecurring()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/PremiumInfo;->isSetPremiumRecurring()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_5

    return v0

    .line 589
    :cond_5
    invoke-virtual {p0}, Lcom/evernote/edam/type/PremiumInfo;->isSetPremiumRecurring()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-boolean v0, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumRecurring:Z

    iget-boolean v1, p1, Lcom/evernote/edam/type/PremiumInfo;->premiumRecurring:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_6

    return v0

    .line 594
    :cond_6
    invoke-virtual {p0}, Lcom/evernote/edam/type/PremiumInfo;->isSetPremiumExpirationDate()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/PremiumInfo;->isSetPremiumExpirationDate()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_7

    return v0

    .line 598
    :cond_7
    invoke-virtual {p0}, Lcom/evernote/edam/type/PremiumInfo;->isSetPremiumExpirationDate()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-wide v0, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumExpirationDate:J

    iget-wide v2, p1, Lcom/evernote/edam/type/PremiumInfo;->premiumExpirationDate:J

    invoke-static {v0, v1, v2, v3}, Lcom/evernote/thrift/TBaseHelper;->compareTo(JJ)I

    move-result v0

    if-eqz v0, :cond_8

    return v0

    .line 603
    :cond_8
    invoke-virtual {p0}, Lcom/evernote/edam/type/PremiumInfo;->isSetPremiumExtendable()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/PremiumInfo;->isSetPremiumExtendable()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_9

    return v0

    .line 607
    :cond_9
    invoke-virtual {p0}, Lcom/evernote/edam/type/PremiumInfo;->isSetPremiumExtendable()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-boolean v0, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumExtendable:Z

    iget-boolean v1, p1, Lcom/evernote/edam/type/PremiumInfo;->premiumExtendable:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_a

    return v0

    .line 612
    :cond_a
    invoke-virtual {p0}, Lcom/evernote/edam/type/PremiumInfo;->isSetPremiumPending()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/PremiumInfo;->isSetPremiumPending()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_b

    return v0

    .line 616
    :cond_b
    invoke-virtual {p0}, Lcom/evernote/edam/type/PremiumInfo;->isSetPremiumPending()Z

    move-result v0

    if-eqz v0, :cond_c

    iget-boolean v0, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumPending:Z

    iget-boolean v1, p1, Lcom/evernote/edam/type/PremiumInfo;->premiumPending:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_c

    return v0

    .line 621
    :cond_c
    invoke-virtual {p0}, Lcom/evernote/edam/type/PremiumInfo;->isSetPremiumCancellationPending()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/PremiumInfo;->isSetPremiumCancellationPending()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_d

    return v0

    .line 625
    :cond_d
    invoke-virtual {p0}, Lcom/evernote/edam/type/PremiumInfo;->isSetPremiumCancellationPending()Z

    move-result v0

    if-eqz v0, :cond_e

    iget-boolean v0, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumCancellationPending:Z

    iget-boolean v1, p1, Lcom/evernote/edam/type/PremiumInfo;->premiumCancellationPending:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_e

    return v0

    .line 630
    :cond_e
    invoke-virtual {p0}, Lcom/evernote/edam/type/PremiumInfo;->isSetCanPurchaseUploadAllowance()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/PremiumInfo;->isSetCanPurchaseUploadAllowance()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_f

    return v0

    .line 634
    :cond_f
    invoke-virtual {p0}, Lcom/evernote/edam/type/PremiumInfo;->isSetCanPurchaseUploadAllowance()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-boolean v0, p0, Lcom/evernote/edam/type/PremiumInfo;->canPurchaseUploadAllowance:Z

    iget-boolean v1, p1, Lcom/evernote/edam/type/PremiumInfo;->canPurchaseUploadAllowance:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_10

    return v0

    .line 639
    :cond_10
    invoke-virtual {p0}, Lcom/evernote/edam/type/PremiumInfo;->isSetSponsoredGroupName()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/PremiumInfo;->isSetSponsoredGroupName()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_11

    return v0

    .line 643
    :cond_11
    invoke-virtual {p0}, Lcom/evernote/edam/type/PremiumInfo;->isSetSponsoredGroupName()Z

    move-result v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/evernote/edam/type/PremiumInfo;->sponsoredGroupName:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/type/PremiumInfo;->sponsoredGroupName:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_12

    return v0

    .line 648
    :cond_12
    invoke-virtual {p0}, Lcom/evernote/edam/type/PremiumInfo;->isSetSponsoredGroupRole()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/PremiumInfo;->isSetSponsoredGroupRole()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_13

    return v0

    .line 652
    :cond_13
    invoke-virtual {p0}, Lcom/evernote/edam/type/PremiumInfo;->isSetSponsoredGroupRole()Z

    move-result v0

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/evernote/edam/type/PremiumInfo;->sponsoredGroupRole:Lcom/evernote/edam/type/SponsoredGroupRole;

    iget-object v1, p1, Lcom/evernote/edam/type/PremiumInfo;->sponsoredGroupRole:Lcom/evernote/edam/type/SponsoredGroupRole;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result v0

    if-eqz v0, :cond_14

    return v0

    .line 657
    :cond_14
    invoke-virtual {p0}, Lcom/evernote/edam/type/PremiumInfo;->isSetPremiumUpgradable()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/PremiumInfo;->isSetPremiumUpgradable()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_15

    return v0

    .line 661
    :cond_15
    invoke-virtual {p0}, Lcom/evernote/edam/type/PremiumInfo;->isSetPremiumUpgradable()Z

    move-result v0

    if-eqz v0, :cond_16

    iget-boolean v0, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumUpgradable:Z

    iget-boolean p1, p1, Lcom/evernote/edam/type/PremiumInfo;->premiumUpgradable:Z

    invoke-static {v0, p1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result p1

    if-eqz p1, :cond_16

    return p1

    :cond_16
    const/4 p1, 0x0

    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 69
    check-cast p1, Lcom/evernote/edam/type/PremiumInfo;

    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/PremiumInfo;->compareTo(Lcom/evernote/edam/type/PremiumInfo;)I

    move-result p1

    return p1
.end method

.method public deepCopy()Lcom/evernote/edam/type/PremiumInfo;
    .locals 1

    .line 161
    new-instance v0, Lcom/evernote/edam/type/PremiumInfo;

    invoke-direct {v0, p0}, Lcom/evernote/edam/type/PremiumInfo;-><init>(Lcom/evernote/edam/type/PremiumInfo;)V

    return-object v0
.end method

.method public bridge synthetic deepCopy()Lcom/evernote/thrift/TBase;
    .locals 1

    .line 69
    invoke-virtual {p0}, Lcom/evernote/edam/type/PremiumInfo;->deepCopy()Lcom/evernote/edam/type/PremiumInfo;

    move-result-object v0

    return-object v0
.end method

.method public equals(Lcom/evernote/edam/type/PremiumInfo;)Z
    .locals 6

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 457
    :cond_0
    iget-wide v1, p0, Lcom/evernote/edam/type/PremiumInfo;->currentTime:J

    iget-wide v3, p1, Lcom/evernote/edam/type/PremiumInfo;->currentTime:J

    cmp-long v5, v1, v3

    if-eqz v5, :cond_1

    return v0

    .line 466
    :cond_1
    iget-boolean v1, p0, Lcom/evernote/edam/type/PremiumInfo;->premium:Z

    iget-boolean v2, p1, Lcom/evernote/edam/type/PremiumInfo;->premium:Z

    if-eq v1, v2, :cond_2

    return v0

    .line 475
    :cond_2
    iget-boolean v1, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumRecurring:Z

    iget-boolean v2, p1, Lcom/evernote/edam/type/PremiumInfo;->premiumRecurring:Z

    if-eq v1, v2, :cond_3

    return v0

    .line 479
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/type/PremiumInfo;->isSetPremiumExpirationDate()Z

    move-result v1

    .line 480
    invoke-virtual {p1}, Lcom/evernote/edam/type/PremiumInfo;->isSetPremiumExpirationDate()Z

    move-result v2

    if-nez v1, :cond_4

    if-eqz v2, :cond_6

    :cond_4
    if-eqz v1, :cond_17

    if-nez v2, :cond_5

    goto/16 :goto_3

    .line 484
    :cond_5
    iget-wide v1, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumExpirationDate:J

    iget-wide v3, p1, Lcom/evernote/edam/type/PremiumInfo;->premiumExpirationDate:J

    cmp-long v5, v1, v3

    if-eqz v5, :cond_6

    return v0

    .line 493
    :cond_6
    iget-boolean v1, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumExtendable:Z

    iget-boolean v2, p1, Lcom/evernote/edam/type/PremiumInfo;->premiumExtendable:Z

    if-eq v1, v2, :cond_7

    return v0

    .line 502
    :cond_7
    iget-boolean v1, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumPending:Z

    iget-boolean v2, p1, Lcom/evernote/edam/type/PremiumInfo;->premiumPending:Z

    if-eq v1, v2, :cond_8

    return v0

    .line 511
    :cond_8
    iget-boolean v1, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumCancellationPending:Z

    iget-boolean v2, p1, Lcom/evernote/edam/type/PremiumInfo;->premiumCancellationPending:Z

    if-eq v1, v2, :cond_9

    return v0

    .line 520
    :cond_9
    iget-boolean v1, p0, Lcom/evernote/edam/type/PremiumInfo;->canPurchaseUploadAllowance:Z

    iget-boolean v2, p1, Lcom/evernote/edam/type/PremiumInfo;->canPurchaseUploadAllowance:Z

    if-eq v1, v2, :cond_a

    return v0

    .line 524
    :cond_a
    invoke-virtual {p0}, Lcom/evernote/edam/type/PremiumInfo;->isSetSponsoredGroupName()Z

    move-result v1

    .line 525
    invoke-virtual {p1}, Lcom/evernote/edam/type/PremiumInfo;->isSetSponsoredGroupName()Z

    move-result v2

    if-nez v1, :cond_b

    if-eqz v2, :cond_d

    :cond_b
    if-eqz v1, :cond_16

    if-nez v2, :cond_c

    goto :goto_2

    .line 529
    :cond_c
    iget-object v1, p0, Lcom/evernote/edam/type/PremiumInfo;->sponsoredGroupName:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/type/PremiumInfo;->sponsoredGroupName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    return v0

    .line 533
    :cond_d
    invoke-virtual {p0}, Lcom/evernote/edam/type/PremiumInfo;->isSetSponsoredGroupRole()Z

    move-result v1

    .line 534
    invoke-virtual {p1}, Lcom/evernote/edam/type/PremiumInfo;->isSetSponsoredGroupRole()Z

    move-result v2

    if-nez v1, :cond_e

    if-eqz v2, :cond_10

    :cond_e
    if-eqz v1, :cond_15

    if-nez v2, :cond_f

    goto :goto_1

    .line 538
    :cond_f
    iget-object v1, p0, Lcom/evernote/edam/type/PremiumInfo;->sponsoredGroupRole:Lcom/evernote/edam/type/SponsoredGroupRole;

    iget-object v2, p1, Lcom/evernote/edam/type/PremiumInfo;->sponsoredGroupRole:Lcom/evernote/edam/type/SponsoredGroupRole;

    invoke-virtual {v1, v2}, Lcom/evernote/edam/type/SponsoredGroupRole;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_10

    return v0

    .line 542
    :cond_10
    invoke-virtual {p0}, Lcom/evernote/edam/type/PremiumInfo;->isSetPremiumUpgradable()Z

    move-result v1

    .line 543
    invoke-virtual {p1}, Lcom/evernote/edam/type/PremiumInfo;->isSetPremiumUpgradable()Z

    move-result v2

    if-nez v1, :cond_11

    if-eqz v2, :cond_13

    :cond_11
    if-eqz v1, :cond_14

    if-nez v2, :cond_12

    goto :goto_0

    .line 547
    :cond_12
    iget-boolean v1, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumUpgradable:Z

    iget-boolean p1, p1, Lcom/evernote/edam/type/PremiumInfo;->premiumUpgradable:Z

    if-eq v1, p1, :cond_13

    return v0

    :cond_13
    const/4 p1, 0x1

    return p1

    :cond_14
    :goto_0
    return v0

    :cond_15
    :goto_1
    return v0

    :cond_16
    :goto_2
    return v0

    :cond_17
    :goto_3
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 443
    :cond_0
    instance-of v1, p1, Lcom/evernote/edam/type/PremiumInfo;

    if-eqz v1, :cond_1

    .line 444
    check-cast p1, Lcom/evernote/edam/type/PremiumInfo;

    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/PremiumInfo;->equals(Lcom/evernote/edam/type/PremiumInfo;)Z

    move-result p1

    return p1

    :cond_1
    return v0
.end method

.method public getCurrentTime()J
    .locals 2

    .line 188
    iget-wide v0, p0, Lcom/evernote/edam/type/PremiumInfo;->currentTime:J

    return-wide v0
.end method

.method public getPremiumExpirationDate()J
    .locals 2

    .line 254
    iget-wide v0, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumExpirationDate:J

    return-wide v0
.end method

.method public getSponsoredGroupName()Ljava/lang/String;
    .locals 1

    .line 364
    iget-object v0, p0, Lcom/evernote/edam/type/PremiumInfo;->sponsoredGroupName:Ljava/lang/String;

    return-object v0
.end method

.method public getSponsoredGroupRole()Lcom/evernote/edam/type/SponsoredGroupRole;
    .locals 1

    .line 391
    iget-object v0, p0, Lcom/evernote/edam/type/PremiumInfo;->sponsoredGroupRole:Lcom/evernote/edam/type/SponsoredGroupRole;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isCanPurchaseUploadAllowance()Z
    .locals 1

    .line 342
    iget-boolean v0, p0, Lcom/evernote/edam/type/PremiumInfo;->canPurchaseUploadAllowance:Z

    return v0
.end method

.method public isPremium()Z
    .locals 1

    .line 210
    iget-boolean v0, p0, Lcom/evernote/edam/type/PremiumInfo;->premium:Z

    return v0
.end method

.method public isPremiumCancellationPending()Z
    .locals 1

    .line 320
    iget-boolean v0, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumCancellationPending:Z

    return v0
.end method

.method public isPremiumExtendable()Z
    .locals 1

    .line 276
    iget-boolean v0, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumExtendable:Z

    return v0
.end method

.method public isPremiumPending()Z
    .locals 1

    .line 298
    iget-boolean v0, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumPending:Z

    return v0
.end method

.method public isPremiumRecurring()Z
    .locals 1

    .line 232
    iget-boolean v0, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumRecurring:Z

    return v0
.end method

.method public isPremiumUpgradable()Z
    .locals 1

    .line 418
    iget-boolean v0, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumUpgradable:Z

    return v0
.end method

.method public isSetCanPurchaseUploadAllowance()Z
    .locals 2

    .line 356
    iget-object v0, p0, Lcom/evernote/edam/type/PremiumInfo;->__isset_vector:[Z

    const/4 v1, 0x7

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetCurrentTime()Z
    .locals 2

    .line 202
    iget-object v0, p0, Lcom/evernote/edam/type/PremiumInfo;->__isset_vector:[Z

    const/4 v1, 0x0

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetPremium()Z
    .locals 2

    .line 224
    iget-object v0, p0, Lcom/evernote/edam/type/PremiumInfo;->__isset_vector:[Z

    const/4 v1, 0x1

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetPremiumCancellationPending()Z
    .locals 2

    .line 334
    iget-object v0, p0, Lcom/evernote/edam/type/PremiumInfo;->__isset_vector:[Z

    const/4 v1, 0x6

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetPremiumExpirationDate()Z
    .locals 2

    .line 268
    iget-object v0, p0, Lcom/evernote/edam/type/PremiumInfo;->__isset_vector:[Z

    const/4 v1, 0x3

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetPremiumExtendable()Z
    .locals 2

    .line 290
    iget-object v0, p0, Lcom/evernote/edam/type/PremiumInfo;->__isset_vector:[Z

    const/4 v1, 0x4

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetPremiumPending()Z
    .locals 2

    .line 312
    iget-object v0, p0, Lcom/evernote/edam/type/PremiumInfo;->__isset_vector:[Z

    const/4 v1, 0x5

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetPremiumRecurring()Z
    .locals 2

    .line 246
    iget-object v0, p0, Lcom/evernote/edam/type/PremiumInfo;->__isset_vector:[Z

    const/4 v1, 0x2

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetPremiumUpgradable()Z
    .locals 2

    .line 432
    iget-object v0, p0, Lcom/evernote/edam/type/PremiumInfo;->__isset_vector:[Z

    const/16 v1, 0x8

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetSponsoredGroupName()Z
    .locals 1

    .line 377
    iget-object v0, p0, Lcom/evernote/edam/type/PremiumInfo;->sponsoredGroupName:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetSponsoredGroupRole()Z
    .locals 1

    .line 408
    iget-object v0, p0, Lcom/evernote/edam/type/PremiumInfo;->sponsoredGroupRole:Lcom/evernote/edam/type/SponsoredGroupRole;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public read(Lcom/evernote/thrift/protocol/TProtocol;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 671
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readStructBegin()Lcom/evernote/thrift/protocol/TStruct;

    .line 674
    :goto_0
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readFieldBegin()Lcom/evernote/thrift/protocol/TField;

    move-result-object v0

    .line 675
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-nez v1, :cond_0

    .line 770
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readStructEnd()V

    .line 771
    invoke-virtual {p0}, Lcom/evernote/edam/type/PremiumInfo;->validate()V

    return-void

    .line 678
    :cond_0
    iget-short v1, v0, Lcom/evernote/thrift/protocol/TField;->id:S

    const/16 v2, 0xa

    const/4 v3, 0x2

    const/4 v4, 0x1

    packed-switch v1, :pswitch_data_0

    .line 766
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 758
    :pswitch_0
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v3, :cond_1

    .line 759
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumUpgradable:Z

    .line 760
    invoke-virtual {p0, v4}, Lcom/evernote/edam/type/PremiumInfo;->setPremiumUpgradableIsSet(Z)V

    goto/16 :goto_1

    .line 762
    :cond_1
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 751
    :pswitch_1
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    const/16 v2, 0x8

    if-ne v1, v2, :cond_2

    .line 752
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI32()I

    move-result v0

    invoke-static {v0}, Lcom/evernote/edam/type/SponsoredGroupRole;->findByValue(I)Lcom/evernote/edam/type/SponsoredGroupRole;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/type/PremiumInfo;->sponsoredGroupRole:Lcom/evernote/edam/type/SponsoredGroupRole;

    goto/16 :goto_1

    .line 754
    :cond_2
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 744
    :pswitch_2
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    const/16 v2, 0xb

    if-ne v1, v2, :cond_3

    .line 745
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/type/PremiumInfo;->sponsoredGroupName:Ljava/lang/String;

    goto/16 :goto_1

    .line 747
    :cond_3
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 736
    :pswitch_3
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v3, :cond_4

    .line 737
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/type/PremiumInfo;->canPurchaseUploadAllowance:Z

    .line 738
    invoke-virtual {p0, v4}, Lcom/evernote/edam/type/PremiumInfo;->setCanPurchaseUploadAllowanceIsSet(Z)V

    goto/16 :goto_1

    .line 740
    :cond_4
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 728
    :pswitch_4
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v3, :cond_5

    .line 729
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumCancellationPending:Z

    .line 730
    invoke-virtual {p0, v4}, Lcom/evernote/edam/type/PremiumInfo;->setPremiumCancellationPendingIsSet(Z)V

    goto/16 :goto_1

    .line 732
    :cond_5
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 720
    :pswitch_5
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v3, :cond_6

    .line 721
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumPending:Z

    .line 722
    invoke-virtual {p0, v4}, Lcom/evernote/edam/type/PremiumInfo;->setPremiumPendingIsSet(Z)V

    goto/16 :goto_1

    .line 724
    :cond_6
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 712
    :pswitch_6
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v3, :cond_7

    .line 713
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumExtendable:Z

    .line 714
    invoke-virtual {p0, v4}, Lcom/evernote/edam/type/PremiumInfo;->setPremiumExtendableIsSet(Z)V

    goto :goto_1

    .line 716
    :cond_7
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 704
    :pswitch_7
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v2, :cond_8

    .line 705
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumExpirationDate:J

    .line 706
    invoke-virtual {p0, v4}, Lcom/evernote/edam/type/PremiumInfo;->setPremiumExpirationDateIsSet(Z)V

    goto :goto_1

    .line 708
    :cond_8
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 696
    :pswitch_8
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v3, :cond_9

    .line 697
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumRecurring:Z

    .line 698
    invoke-virtual {p0, v4}, Lcom/evernote/edam/type/PremiumInfo;->setPremiumRecurringIsSet(Z)V

    goto :goto_1

    .line 700
    :cond_9
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 688
    :pswitch_9
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v3, :cond_a

    .line 689
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/type/PremiumInfo;->premium:Z

    .line 690
    invoke-virtual {p0, v4}, Lcom/evernote/edam/type/PremiumInfo;->setPremiumIsSet(Z)V

    goto :goto_1

    .line 692
    :cond_a
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 680
    :pswitch_a
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v2, :cond_b

    .line 681
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/evernote/edam/type/PremiumInfo;->currentTime:J

    .line 682
    invoke-virtual {p0, v4}, Lcom/evernote/edam/type/PremiumInfo;->setCurrentTimeIsSet(Z)V

    goto :goto_1

    .line 684
    :cond_b
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    .line 768
    :goto_1
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readFieldEnd()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setCanPurchaseUploadAllowance(Z)V
    .locals 0

    .line 346
    iput-boolean p1, p0, Lcom/evernote/edam/type/PremiumInfo;->canPurchaseUploadAllowance:Z

    const/4 p1, 0x1

    .line 347
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/PremiumInfo;->setCanPurchaseUploadAllowanceIsSet(Z)V

    return-void
.end method

.method public setCanPurchaseUploadAllowanceIsSet(Z)V
    .locals 2

    .line 360
    iget-object v0, p0, Lcom/evernote/edam/type/PremiumInfo;->__isset_vector:[Z

    const/4 v1, 0x7

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setCurrentTime(J)V
    .locals 0

    .line 192
    iput-wide p1, p0, Lcom/evernote/edam/type/PremiumInfo;->currentTime:J

    const/4 p1, 0x1

    .line 193
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/PremiumInfo;->setCurrentTimeIsSet(Z)V

    return-void
.end method

.method public setCurrentTimeIsSet(Z)V
    .locals 2

    .line 206
    iget-object v0, p0, Lcom/evernote/edam/type/PremiumInfo;->__isset_vector:[Z

    const/4 v1, 0x0

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setPremium(Z)V
    .locals 0

    .line 214
    iput-boolean p1, p0, Lcom/evernote/edam/type/PremiumInfo;->premium:Z

    const/4 p1, 0x1

    .line 215
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/PremiumInfo;->setPremiumIsSet(Z)V

    return-void
.end method

.method public setPremiumCancellationPending(Z)V
    .locals 0

    .line 324
    iput-boolean p1, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumCancellationPending:Z

    const/4 p1, 0x1

    .line 325
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/PremiumInfo;->setPremiumCancellationPendingIsSet(Z)V

    return-void
.end method

.method public setPremiumCancellationPendingIsSet(Z)V
    .locals 2

    .line 338
    iget-object v0, p0, Lcom/evernote/edam/type/PremiumInfo;->__isset_vector:[Z

    const/4 v1, 0x6

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setPremiumExpirationDate(J)V
    .locals 0

    .line 258
    iput-wide p1, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumExpirationDate:J

    const/4 p1, 0x1

    .line 259
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/PremiumInfo;->setPremiumExpirationDateIsSet(Z)V

    return-void
.end method

.method public setPremiumExpirationDateIsSet(Z)V
    .locals 2

    .line 272
    iget-object v0, p0, Lcom/evernote/edam/type/PremiumInfo;->__isset_vector:[Z

    const/4 v1, 0x3

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setPremiumExtendable(Z)V
    .locals 0

    .line 280
    iput-boolean p1, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumExtendable:Z

    const/4 p1, 0x1

    .line 281
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/PremiumInfo;->setPremiumExtendableIsSet(Z)V

    return-void
.end method

.method public setPremiumExtendableIsSet(Z)V
    .locals 2

    .line 294
    iget-object v0, p0, Lcom/evernote/edam/type/PremiumInfo;->__isset_vector:[Z

    const/4 v1, 0x4

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setPremiumIsSet(Z)V
    .locals 2

    .line 228
    iget-object v0, p0, Lcom/evernote/edam/type/PremiumInfo;->__isset_vector:[Z

    const/4 v1, 0x1

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setPremiumPending(Z)V
    .locals 0

    .line 302
    iput-boolean p1, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumPending:Z

    const/4 p1, 0x1

    .line 303
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/PremiumInfo;->setPremiumPendingIsSet(Z)V

    return-void
.end method

.method public setPremiumPendingIsSet(Z)V
    .locals 2

    .line 316
    iget-object v0, p0, Lcom/evernote/edam/type/PremiumInfo;->__isset_vector:[Z

    const/4 v1, 0x5

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setPremiumRecurring(Z)V
    .locals 0

    .line 236
    iput-boolean p1, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumRecurring:Z

    const/4 p1, 0x1

    .line 237
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/PremiumInfo;->setPremiumRecurringIsSet(Z)V

    return-void
.end method

.method public setPremiumRecurringIsSet(Z)V
    .locals 2

    .line 250
    iget-object v0, p0, Lcom/evernote/edam/type/PremiumInfo;->__isset_vector:[Z

    const/4 v1, 0x2

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setPremiumUpgradable(Z)V
    .locals 0

    .line 422
    iput-boolean p1, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumUpgradable:Z

    const/4 p1, 0x1

    .line 423
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/PremiumInfo;->setPremiumUpgradableIsSet(Z)V

    return-void
.end method

.method public setPremiumUpgradableIsSet(Z)V
    .locals 2

    .line 436
    iget-object v0, p0, Lcom/evernote/edam/type/PremiumInfo;->__isset_vector:[Z

    const/16 v1, 0x8

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setSponsoredGroupName(Ljava/lang/String;)V
    .locals 0

    .line 368
    iput-object p1, p0, Lcom/evernote/edam/type/PremiumInfo;->sponsoredGroupName:Ljava/lang/String;

    return-void
.end method

.method public setSponsoredGroupNameIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 382
    iput-object p1, p0, Lcom/evernote/edam/type/PremiumInfo;->sponsoredGroupName:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setSponsoredGroupRole(Lcom/evernote/edam/type/SponsoredGroupRole;)V
    .locals 0

    .line 399
    iput-object p1, p0, Lcom/evernote/edam/type/PremiumInfo;->sponsoredGroupRole:Lcom/evernote/edam/type/SponsoredGroupRole;

    return-void
.end method

.method public setSponsoredGroupRoleIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 413
    iput-object p1, p0, Lcom/evernote/edam/type/PremiumInfo;->sponsoredGroupRole:Lcom/evernote/edam/type/SponsoredGroupRole;

    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 829
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PremiumInfo("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "currentTime:"

    .line 832
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 833
    iget-wide v1, p0, Lcom/evernote/edam/type/PremiumInfo;->currentTime:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", "

    .line 835
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "premium:"

    .line 836
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 837
    iget-boolean v1, p0, Lcom/evernote/edam/type/PremiumInfo;->premium:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", "

    .line 839
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "premiumRecurring:"

    .line 840
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 841
    iget-boolean v1, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumRecurring:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 843
    invoke-virtual {p0}, Lcom/evernote/edam/type/PremiumInfo;->isSetPremiumExpirationDate()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, ", "

    .line 844
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "premiumExpirationDate:"

    .line 845
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 846
    iget-wide v1, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumExpirationDate:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    :cond_0
    const-string v1, ", "

    .line 849
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "premiumExtendable:"

    .line 850
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 851
    iget-boolean v1, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumExtendable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", "

    .line 853
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "premiumPending:"

    .line 854
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 855
    iget-boolean v1, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumPending:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", "

    .line 857
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "premiumCancellationPending:"

    .line 858
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 859
    iget-boolean v1, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumCancellationPending:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", "

    .line 861
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "canPurchaseUploadAllowance:"

    .line 862
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 863
    iget-boolean v1, p0, Lcom/evernote/edam/type/PremiumInfo;->canPurchaseUploadAllowance:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 865
    invoke-virtual {p0}, Lcom/evernote/edam/type/PremiumInfo;->isSetSponsoredGroupName()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, ", "

    .line 866
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "sponsoredGroupName:"

    .line 867
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 868
    iget-object v1, p0, Lcom/evernote/edam/type/PremiumInfo;->sponsoredGroupName:Ljava/lang/String;

    if-nez v1, :cond_1

    const-string v1, "null"

    .line 869
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 871
    :cond_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 875
    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/evernote/edam/type/PremiumInfo;->isSetSponsoredGroupRole()Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, ", "

    .line 876
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "sponsoredGroupRole:"

    .line 877
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 878
    iget-object v1, p0, Lcom/evernote/edam/type/PremiumInfo;->sponsoredGroupRole:Lcom/evernote/edam/type/SponsoredGroupRole;

    if-nez v1, :cond_3

    const-string v1, "null"

    .line 879
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 881
    :cond_3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 885
    :cond_4
    :goto_1
    invoke-virtual {p0}, Lcom/evernote/edam/type/PremiumInfo;->isSetPremiumUpgradable()Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, ", "

    .line 886
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "premiumUpgradable:"

    .line 887
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 888
    iget-boolean v1, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumUpgradable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    :cond_5
    const-string v1, ")"

    .line 891
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 892
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public unsetCanPurchaseUploadAllowance()V
    .locals 3

    .line 351
    iget-object v0, p0, Lcom/evernote/edam/type/PremiumInfo;->__isset_vector:[Z

    const/4 v1, 0x7

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetCurrentTime()V
    .locals 2

    .line 197
    iget-object v0, p0, Lcom/evernote/edam/type/PremiumInfo;->__isset_vector:[Z

    const/4 v1, 0x0

    aput-boolean v1, v0, v1

    return-void
.end method

.method public unsetPremium()V
    .locals 3

    .line 219
    iget-object v0, p0, Lcom/evernote/edam/type/PremiumInfo;->__isset_vector:[Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetPremiumCancellationPending()V
    .locals 3

    .line 329
    iget-object v0, p0, Lcom/evernote/edam/type/PremiumInfo;->__isset_vector:[Z

    const/4 v1, 0x6

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetPremiumExpirationDate()V
    .locals 3

    .line 263
    iget-object v0, p0, Lcom/evernote/edam/type/PremiumInfo;->__isset_vector:[Z

    const/4 v1, 0x3

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetPremiumExtendable()V
    .locals 3

    .line 285
    iget-object v0, p0, Lcom/evernote/edam/type/PremiumInfo;->__isset_vector:[Z

    const/4 v1, 0x4

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetPremiumPending()V
    .locals 3

    .line 307
    iget-object v0, p0, Lcom/evernote/edam/type/PremiumInfo;->__isset_vector:[Z

    const/4 v1, 0x5

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetPremiumRecurring()V
    .locals 3

    .line 241
    iget-object v0, p0, Lcom/evernote/edam/type/PremiumInfo;->__isset_vector:[Z

    const/4 v1, 0x2

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetPremiumUpgradable()V
    .locals 3

    .line 427
    iget-object v0, p0, Lcom/evernote/edam/type/PremiumInfo;->__isset_vector:[Z

    const/16 v1, 0x8

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetSponsoredGroupName()V
    .locals 1

    const/4 v0, 0x0

    .line 372
    iput-object v0, p0, Lcom/evernote/edam/type/PremiumInfo;->sponsoredGroupName:Ljava/lang/String;

    return-void
.end method

.method public unsetSponsoredGroupRole()V
    .locals 1

    const/4 v0, 0x0

    .line 403
    iput-object v0, p0, Lcom/evernote/edam/type/PremiumInfo;->sponsoredGroupRole:Lcom/evernote/edam/type/SponsoredGroupRole;

    return-void
.end method

.method public validate()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 897
    invoke-virtual {p0}, Lcom/evernote/edam/type/PremiumInfo;->isSetCurrentTime()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 901
    invoke-virtual {p0}, Lcom/evernote/edam/type/PremiumInfo;->isSetPremium()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 905
    invoke-virtual {p0}, Lcom/evernote/edam/type/PremiumInfo;->isSetPremiumRecurring()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 909
    invoke-virtual {p0}, Lcom/evernote/edam/type/PremiumInfo;->isSetPremiumExtendable()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 913
    invoke-virtual {p0}, Lcom/evernote/edam/type/PremiumInfo;->isSetPremiumPending()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 917
    invoke-virtual {p0}, Lcom/evernote/edam/type/PremiumInfo;->isSetPremiumCancellationPending()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 921
    invoke-virtual {p0}, Lcom/evernote/edam/type/PremiumInfo;->isSetCanPurchaseUploadAllowance()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 922
    :cond_0
    new-instance v0, Lcom/evernote/thrift/protocol/TProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Required field \'canPurchaseUploadAllowance\' is unset! Struct:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/evernote/edam/type/PremiumInfo;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/evernote/thrift/protocol/TProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 918
    :cond_1
    new-instance v0, Lcom/evernote/thrift/protocol/TProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Required field \'premiumCancellationPending\' is unset! Struct:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/evernote/edam/type/PremiumInfo;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/evernote/thrift/protocol/TProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 914
    :cond_2
    new-instance v0, Lcom/evernote/thrift/protocol/TProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Required field \'premiumPending\' is unset! Struct:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/evernote/edam/type/PremiumInfo;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/evernote/thrift/protocol/TProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 910
    :cond_3
    new-instance v0, Lcom/evernote/thrift/protocol/TProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Required field \'premiumExtendable\' is unset! Struct:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/evernote/edam/type/PremiumInfo;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/evernote/thrift/protocol/TProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 906
    :cond_4
    new-instance v0, Lcom/evernote/thrift/protocol/TProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Required field \'premiumRecurring\' is unset! Struct:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/evernote/edam/type/PremiumInfo;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/evernote/thrift/protocol/TProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 902
    :cond_5
    new-instance v0, Lcom/evernote/thrift/protocol/TProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Required field \'premium\' is unset! Struct:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/evernote/edam/type/PremiumInfo;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/evernote/thrift/protocol/TProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 898
    :cond_6
    new-instance v0, Lcom/evernote/thrift/protocol/TProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Required field \'currentTime\' is unset! Struct:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/evernote/edam/type/PremiumInfo;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/evernote/thrift/protocol/TProtocolException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public write(Lcom/evernote/thrift/protocol/TProtocol;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 775
    invoke-virtual {p0}, Lcom/evernote/edam/type/PremiumInfo;->validate()V

    .line 777
    sget-object v0, Lcom/evernote/edam/type/PremiumInfo;->STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeStructBegin(Lcom/evernote/thrift/protocol/TStruct;)V

    .line 778
    sget-object v0, Lcom/evernote/edam/type/PremiumInfo;->CURRENT_TIME_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 779
    iget-wide v0, p0, Lcom/evernote/edam/type/PremiumInfo;->currentTime:J

    invoke-virtual {p1, v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeI64(J)V

    .line 780
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 781
    sget-object v0, Lcom/evernote/edam/type/PremiumInfo;->PREMIUM_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 782
    iget-boolean v0, p0, Lcom/evernote/edam/type/PremiumInfo;->premium:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 783
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 784
    sget-object v0, Lcom/evernote/edam/type/PremiumInfo;->PREMIUM_RECURRING_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 785
    iget-boolean v0, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumRecurring:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 786
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 787
    invoke-virtual {p0}, Lcom/evernote/edam/type/PremiumInfo;->isSetPremiumExpirationDate()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 788
    sget-object v0, Lcom/evernote/edam/type/PremiumInfo;->PREMIUM_EXPIRATION_DATE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 789
    iget-wide v0, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumExpirationDate:J

    invoke-virtual {p1, v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeI64(J)V

    .line 790
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 792
    :cond_0
    sget-object v0, Lcom/evernote/edam/type/PremiumInfo;->PREMIUM_EXTENDABLE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 793
    iget-boolean v0, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumExtendable:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 794
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 795
    sget-object v0, Lcom/evernote/edam/type/PremiumInfo;->PREMIUM_PENDING_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 796
    iget-boolean v0, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumPending:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 797
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 798
    sget-object v0, Lcom/evernote/edam/type/PremiumInfo;->PREMIUM_CANCELLATION_PENDING_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 799
    iget-boolean v0, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumCancellationPending:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 800
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 801
    sget-object v0, Lcom/evernote/edam/type/PremiumInfo;->CAN_PURCHASE_UPLOAD_ALLOWANCE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 802
    iget-boolean v0, p0, Lcom/evernote/edam/type/PremiumInfo;->canPurchaseUploadAllowance:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 803
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 804
    iget-object v0, p0, Lcom/evernote/edam/type/PremiumInfo;->sponsoredGroupName:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 805
    invoke-virtual {p0}, Lcom/evernote/edam/type/PremiumInfo;->isSetSponsoredGroupName()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 806
    sget-object v0, Lcom/evernote/edam/type/PremiumInfo;->SPONSORED_GROUP_NAME_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 807
    iget-object v0, p0, Lcom/evernote/edam/type/PremiumInfo;->sponsoredGroupName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 808
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 811
    :cond_1
    iget-object v0, p0, Lcom/evernote/edam/type/PremiumInfo;->sponsoredGroupRole:Lcom/evernote/edam/type/SponsoredGroupRole;

    if-eqz v0, :cond_2

    .line 812
    invoke-virtual {p0}, Lcom/evernote/edam/type/PremiumInfo;->isSetSponsoredGroupRole()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 813
    sget-object v0, Lcom/evernote/edam/type/PremiumInfo;->SPONSORED_GROUP_ROLE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 814
    iget-object v0, p0, Lcom/evernote/edam/type/PremiumInfo;->sponsoredGroupRole:Lcom/evernote/edam/type/SponsoredGroupRole;

    invoke-virtual {v0}, Lcom/evernote/edam/type/SponsoredGroupRole;->getValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeI32(I)V

    .line 815
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 818
    :cond_2
    invoke-virtual {p0}, Lcom/evernote/edam/type/PremiumInfo;->isSetPremiumUpgradable()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 819
    sget-object v0, Lcom/evernote/edam/type/PremiumInfo;->PREMIUM_UPGRADABLE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 820
    iget-boolean v0, p0, Lcom/evernote/edam/type/PremiumInfo;->premiumUpgradable:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 821
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 823
    :cond_3
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldStop()V

    .line 824
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeStructEnd()V

    return-void
.end method

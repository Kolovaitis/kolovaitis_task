.class public final enum Lcom/evernote/edam/type/PremiumOrderStatus;
.super Ljava/lang/Enum;
.source "PremiumOrderStatus.java"

# interfaces
.implements Lcom/evernote/thrift/TEnum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/evernote/edam/type/PremiumOrderStatus;",
        ">;",
        "Lcom/evernote/thrift/TEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/evernote/edam/type/PremiumOrderStatus;

.field public static final enum ACTIVE:Lcom/evernote/edam/type/PremiumOrderStatus;

.field public static final enum CANCELED:Lcom/evernote/edam/type/PremiumOrderStatus;

.field public static final enum CANCELLATION_PENDING:Lcom/evernote/edam/type/PremiumOrderStatus;

.field public static final enum FAILED:Lcom/evernote/edam/type/PremiumOrderStatus;

.field public static final enum NONE:Lcom/evernote/edam/type/PremiumOrderStatus;

.field public static final enum PENDING:Lcom/evernote/edam/type/PremiumOrderStatus;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 35
    new-instance v0, Lcom/evernote/edam/type/PremiumOrderStatus;

    const-string v1, "NONE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v2}, Lcom/evernote/edam/type/PremiumOrderStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/evernote/edam/type/PremiumOrderStatus;->NONE:Lcom/evernote/edam/type/PremiumOrderStatus;

    .line 36
    new-instance v0, Lcom/evernote/edam/type/PremiumOrderStatus;

    const-string v1, "PENDING"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3, v3}, Lcom/evernote/edam/type/PremiumOrderStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/evernote/edam/type/PremiumOrderStatus;->PENDING:Lcom/evernote/edam/type/PremiumOrderStatus;

    .line 37
    new-instance v0, Lcom/evernote/edam/type/PremiumOrderStatus;

    const-string v1, "ACTIVE"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4, v4}, Lcom/evernote/edam/type/PremiumOrderStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/evernote/edam/type/PremiumOrderStatus;->ACTIVE:Lcom/evernote/edam/type/PremiumOrderStatus;

    .line 38
    new-instance v0, Lcom/evernote/edam/type/PremiumOrderStatus;

    const-string v1, "FAILED"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5, v5}, Lcom/evernote/edam/type/PremiumOrderStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/evernote/edam/type/PremiumOrderStatus;->FAILED:Lcom/evernote/edam/type/PremiumOrderStatus;

    .line 39
    new-instance v0, Lcom/evernote/edam/type/PremiumOrderStatus;

    const-string v1, "CANCELLATION_PENDING"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6, v6}, Lcom/evernote/edam/type/PremiumOrderStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/evernote/edam/type/PremiumOrderStatus;->CANCELLATION_PENDING:Lcom/evernote/edam/type/PremiumOrderStatus;

    .line 40
    new-instance v0, Lcom/evernote/edam/type/PremiumOrderStatus;

    const-string v1, "CANCELED"

    const/4 v7, 0x5

    invoke-direct {v0, v1, v7, v7}, Lcom/evernote/edam/type/PremiumOrderStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/evernote/edam/type/PremiumOrderStatus;->CANCELED:Lcom/evernote/edam/type/PremiumOrderStatus;

    const/4 v0, 0x6

    .line 34
    new-array v0, v0, [Lcom/evernote/edam/type/PremiumOrderStatus;

    sget-object v1, Lcom/evernote/edam/type/PremiumOrderStatus;->NONE:Lcom/evernote/edam/type/PremiumOrderStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/evernote/edam/type/PremiumOrderStatus;->PENDING:Lcom/evernote/edam/type/PremiumOrderStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/evernote/edam/type/PremiumOrderStatus;->ACTIVE:Lcom/evernote/edam/type/PremiumOrderStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/evernote/edam/type/PremiumOrderStatus;->FAILED:Lcom/evernote/edam/type/PremiumOrderStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/evernote/edam/type/PremiumOrderStatus;->CANCELLATION_PENDING:Lcom/evernote/edam/type/PremiumOrderStatus;

    aput-object v1, v0, v6

    sget-object v1, Lcom/evernote/edam/type/PremiumOrderStatus;->CANCELED:Lcom/evernote/edam/type/PremiumOrderStatus;

    aput-object v1, v0, v7

    sput-object v0, Lcom/evernote/edam/type/PremiumOrderStatus;->$VALUES:[Lcom/evernote/edam/type/PremiumOrderStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 44
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 45
    iput p3, p0, Lcom/evernote/edam/type/PremiumOrderStatus;->value:I

    return-void
.end method

.method public static findByValue(I)Lcom/evernote/edam/type/PremiumOrderStatus;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 72
    :pswitch_0
    sget-object p0, Lcom/evernote/edam/type/PremiumOrderStatus;->CANCELED:Lcom/evernote/edam/type/PremiumOrderStatus;

    return-object p0

    .line 70
    :pswitch_1
    sget-object p0, Lcom/evernote/edam/type/PremiumOrderStatus;->CANCELLATION_PENDING:Lcom/evernote/edam/type/PremiumOrderStatus;

    return-object p0

    .line 68
    :pswitch_2
    sget-object p0, Lcom/evernote/edam/type/PremiumOrderStatus;->FAILED:Lcom/evernote/edam/type/PremiumOrderStatus;

    return-object p0

    .line 66
    :pswitch_3
    sget-object p0, Lcom/evernote/edam/type/PremiumOrderStatus;->ACTIVE:Lcom/evernote/edam/type/PremiumOrderStatus;

    return-object p0

    .line 64
    :pswitch_4
    sget-object p0, Lcom/evernote/edam/type/PremiumOrderStatus;->PENDING:Lcom/evernote/edam/type/PremiumOrderStatus;

    return-object p0

    .line 62
    :pswitch_5
    sget-object p0, Lcom/evernote/edam/type/PremiumOrderStatus;->NONE:Lcom/evernote/edam/type/PremiumOrderStatus;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/evernote/edam/type/PremiumOrderStatus;
    .locals 1

    .line 34
    const-class v0, Lcom/evernote/edam/type/PremiumOrderStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/evernote/edam/type/PremiumOrderStatus;

    return-object p0
.end method

.method public static values()[Lcom/evernote/edam/type/PremiumOrderStatus;
    .locals 1

    .line 34
    sget-object v0, Lcom/evernote/edam/type/PremiumOrderStatus;->$VALUES:[Lcom/evernote/edam/type/PremiumOrderStatus;

    invoke-virtual {v0}, [Lcom/evernote/edam/type/PremiumOrderStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/evernote/edam/type/PremiumOrderStatus;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 52
    iget v0, p0, Lcom/evernote/edam/type/PremiumOrderStatus;->value:I

    return v0
.end method

.class public Lcom/evernote/edam/type/Constants;
.super Ljava/lang/Object;
.source "Constants.java"


# static fields
.field public static final CLASSIFICATION_RECIPE_SERVICE_RECIPE:Ljava/lang/String; = "002"

.field public static final CLASSIFICATION_RECIPE_USER_NON_RECIPE:Ljava/lang/String; = "000"

.field public static final CLASSIFICATION_RECIPE_USER_RECIPE:Ljava/lang/String; = "001"

.field public static final EDAM_NOTE_SOURCE_MAIL_CLIP:Ljava/lang/String; = "mail.clip"

.field public static final EDAM_NOTE_SOURCE_MAIL_SMTP_GATEWAY:Ljava/lang/String; = "mail.smtp"

.field public static final EDAM_NOTE_SOURCE_WEB_CLIP:Ljava/lang/String; = "web.clip"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

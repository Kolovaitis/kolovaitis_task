.class public Lcom/evernote/edam/type/User;
.super Ljava/lang/Object;
.source "User.java"

# interfaces
.implements Lcom/evernote/thrift/TBase;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/evernote/thrift/TBase<",
        "Lcom/evernote/edam/type/User;",
        ">;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# static fields
.field private static final ACCOUNTING_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final ACTIVE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final ATTRIBUTES_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final BUSINESS_USER_INFO_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final CREATED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final DELETED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final EMAIL_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final ID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final NAME_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final PREMIUM_INFO_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final PRIVILEGE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final SHARD_ID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

.field private static final TIMEZONE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final UPDATED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final USERNAME_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final __ACTIVE_ISSET_ID:I = 0x4

.field private static final __CREATED_ISSET_ID:I = 0x1

.field private static final __DELETED_ISSET_ID:I = 0x3

.field private static final __ID_ISSET_ID:I = 0x0

.field private static final __UPDATED_ISSET_ID:I = 0x2


# instance fields
.field private __isset_vector:[Z

.field private accounting:Lcom/evernote/edam/type/Accounting;

.field private active:Z

.field private attributes:Lcom/evernote/edam/type/UserAttributes;

.field private businessUserInfo:Lcom/evernote/edam/type/BusinessUserInfo;

.field private created:J

.field private deleted:J

.field private email:Ljava/lang/String;

.field private id:I

.field private name:Ljava/lang/String;

.field private premiumInfo:Lcom/evernote/edam/type/PremiumInfo;

.field private privilege:Lcom/evernote/edam/type/PrivilegeLevel;

.field private shardId:Ljava/lang/String;

.field private timezone:Ljava/lang/String;

.field private updated:J

.field private username:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 122
    new-instance v0, Lcom/evernote/thrift/protocol/TStruct;

    const-string v1, "User"

    invoke-direct {v0, v1}, Lcom/evernote/thrift/protocol/TStruct;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/evernote/edam/type/User;->STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

    .line 124
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "id"

    const/16 v2, 0x8

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/User;->ID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 125
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "username"

    const/4 v3, 0x2

    const/16 v4, 0xb

    invoke-direct {v0, v1, v4, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/User;->USERNAME_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 126
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "email"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v4, v5}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/User;->EMAIL_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 127
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "name"

    const/4 v5, 0x4

    invoke-direct {v0, v1, v4, v5}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/User;->NAME_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 128
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "timezone"

    const/4 v5, 0x6

    invoke-direct {v0, v1, v4, v5}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/User;->TIMEZONE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 129
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "privilege"

    const/4 v5, 0x7

    invoke-direct {v0, v1, v2, v5}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/User;->PRIVILEGE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 130
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "created"

    const/16 v2, 0xa

    const/16 v5, 0x9

    invoke-direct {v0, v1, v2, v5}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/User;->CREATED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 131
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "updated"

    invoke-direct {v0, v1, v2, v2}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/User;->UPDATED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 132
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "deleted"

    invoke-direct {v0, v1, v2, v4}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/User;->DELETED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 133
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "active"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v3, v2}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/User;->ACTIVE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 134
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "shardId"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v4, v2}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/User;->SHARD_ID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 135
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "attributes"

    const/16 v2, 0xc

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/User;->ATTRIBUTES_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 136
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "accounting"

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/User;->ACCOUNTING_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 137
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "premiumInfo"

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/User;->PREMIUM_INFO_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 138
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "businessUserInfo"

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/User;->BUSINESS_USER_INFO_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x5

    .line 163
    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/evernote/edam/type/User;->__isset_vector:[Z

    return-void
.end method

.method public constructor <init>(Lcom/evernote/edam/type/User;)V
    .locals 4

    .line 171
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x5

    .line 163
    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/evernote/edam/type/User;->__isset_vector:[Z

    .line 172
    iget-object v0, p1, Lcom/evernote/edam/type/User;->__isset_vector:[Z

    iget-object v1, p0, Lcom/evernote/edam/type/User;->__isset_vector:[Z

    array-length v2, v0

    const/4 v3, 0x0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 173
    iget v0, p1, Lcom/evernote/edam/type/User;->id:I

    iput v0, p0, Lcom/evernote/edam/type/User;->id:I

    .line 174
    invoke-virtual {p1}, Lcom/evernote/edam/type/User;->isSetUsername()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p1, Lcom/evernote/edam/type/User;->username:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/type/User;->username:Ljava/lang/String;

    .line 177
    :cond_0
    invoke-virtual {p1}, Lcom/evernote/edam/type/User;->isSetEmail()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 178
    iget-object v0, p1, Lcom/evernote/edam/type/User;->email:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/type/User;->email:Ljava/lang/String;

    .line 180
    :cond_1
    invoke-virtual {p1}, Lcom/evernote/edam/type/User;->isSetName()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 181
    iget-object v0, p1, Lcom/evernote/edam/type/User;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/type/User;->name:Ljava/lang/String;

    .line 183
    :cond_2
    invoke-virtual {p1}, Lcom/evernote/edam/type/User;->isSetTimezone()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 184
    iget-object v0, p1, Lcom/evernote/edam/type/User;->timezone:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/type/User;->timezone:Ljava/lang/String;

    .line 186
    :cond_3
    invoke-virtual {p1}, Lcom/evernote/edam/type/User;->isSetPrivilege()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 187
    iget-object v0, p1, Lcom/evernote/edam/type/User;->privilege:Lcom/evernote/edam/type/PrivilegeLevel;

    iput-object v0, p0, Lcom/evernote/edam/type/User;->privilege:Lcom/evernote/edam/type/PrivilegeLevel;

    .line 189
    :cond_4
    iget-wide v0, p1, Lcom/evernote/edam/type/User;->created:J

    iput-wide v0, p0, Lcom/evernote/edam/type/User;->created:J

    .line 190
    iget-wide v0, p1, Lcom/evernote/edam/type/User;->updated:J

    iput-wide v0, p0, Lcom/evernote/edam/type/User;->updated:J

    .line 191
    iget-wide v0, p1, Lcom/evernote/edam/type/User;->deleted:J

    iput-wide v0, p0, Lcom/evernote/edam/type/User;->deleted:J

    .line 192
    iget-boolean v0, p1, Lcom/evernote/edam/type/User;->active:Z

    iput-boolean v0, p0, Lcom/evernote/edam/type/User;->active:Z

    .line 193
    invoke-virtual {p1}, Lcom/evernote/edam/type/User;->isSetShardId()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 194
    iget-object v0, p1, Lcom/evernote/edam/type/User;->shardId:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/type/User;->shardId:Ljava/lang/String;

    .line 196
    :cond_5
    invoke-virtual {p1}, Lcom/evernote/edam/type/User;->isSetAttributes()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 197
    new-instance v0, Lcom/evernote/edam/type/UserAttributes;

    iget-object v1, p1, Lcom/evernote/edam/type/User;->attributes:Lcom/evernote/edam/type/UserAttributes;

    invoke-direct {v0, v1}, Lcom/evernote/edam/type/UserAttributes;-><init>(Lcom/evernote/edam/type/UserAttributes;)V

    iput-object v0, p0, Lcom/evernote/edam/type/User;->attributes:Lcom/evernote/edam/type/UserAttributes;

    .line 199
    :cond_6
    invoke-virtual {p1}, Lcom/evernote/edam/type/User;->isSetAccounting()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 200
    new-instance v0, Lcom/evernote/edam/type/Accounting;

    iget-object v1, p1, Lcom/evernote/edam/type/User;->accounting:Lcom/evernote/edam/type/Accounting;

    invoke-direct {v0, v1}, Lcom/evernote/edam/type/Accounting;-><init>(Lcom/evernote/edam/type/Accounting;)V

    iput-object v0, p0, Lcom/evernote/edam/type/User;->accounting:Lcom/evernote/edam/type/Accounting;

    .line 202
    :cond_7
    invoke-virtual {p1}, Lcom/evernote/edam/type/User;->isSetPremiumInfo()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 203
    new-instance v0, Lcom/evernote/edam/type/PremiumInfo;

    iget-object v1, p1, Lcom/evernote/edam/type/User;->premiumInfo:Lcom/evernote/edam/type/PremiumInfo;

    invoke-direct {v0, v1}, Lcom/evernote/edam/type/PremiumInfo;-><init>(Lcom/evernote/edam/type/PremiumInfo;)V

    iput-object v0, p0, Lcom/evernote/edam/type/User;->premiumInfo:Lcom/evernote/edam/type/PremiumInfo;

    .line 205
    :cond_8
    invoke-virtual {p1}, Lcom/evernote/edam/type/User;->isSetBusinessUserInfo()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 206
    new-instance v0, Lcom/evernote/edam/type/BusinessUserInfo;

    iget-object p1, p1, Lcom/evernote/edam/type/User;->businessUserInfo:Lcom/evernote/edam/type/BusinessUserInfo;

    invoke-direct {v0, p1}, Lcom/evernote/edam/type/BusinessUserInfo;-><init>(Lcom/evernote/edam/type/BusinessUserInfo;)V

    iput-object v0, p0, Lcom/evernote/edam/type/User;->businessUserInfo:Lcom/evernote/edam/type/BusinessUserInfo;

    :cond_9
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 4

    const/4 v0, 0x0

    .line 215
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/User;->setIdIsSet(Z)V

    .line 216
    iput v0, p0, Lcom/evernote/edam/type/User;->id:I

    const/4 v1, 0x0

    .line 217
    iput-object v1, p0, Lcom/evernote/edam/type/User;->username:Ljava/lang/String;

    .line 218
    iput-object v1, p0, Lcom/evernote/edam/type/User;->email:Ljava/lang/String;

    .line 219
    iput-object v1, p0, Lcom/evernote/edam/type/User;->name:Ljava/lang/String;

    .line 220
    iput-object v1, p0, Lcom/evernote/edam/type/User;->timezone:Ljava/lang/String;

    .line 221
    iput-object v1, p0, Lcom/evernote/edam/type/User;->privilege:Lcom/evernote/edam/type/PrivilegeLevel;

    .line 222
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/User;->setCreatedIsSet(Z)V

    const-wide/16 v2, 0x0

    .line 223
    iput-wide v2, p0, Lcom/evernote/edam/type/User;->created:J

    .line 224
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/User;->setUpdatedIsSet(Z)V

    .line 225
    iput-wide v2, p0, Lcom/evernote/edam/type/User;->updated:J

    .line 226
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/User;->setDeletedIsSet(Z)V

    .line 227
    iput-wide v2, p0, Lcom/evernote/edam/type/User;->deleted:J

    .line 228
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/User;->setActiveIsSet(Z)V

    .line 229
    iput-boolean v0, p0, Lcom/evernote/edam/type/User;->active:Z

    .line 230
    iput-object v1, p0, Lcom/evernote/edam/type/User;->shardId:Ljava/lang/String;

    .line 231
    iput-object v1, p0, Lcom/evernote/edam/type/User;->attributes:Lcom/evernote/edam/type/UserAttributes;

    .line 232
    iput-object v1, p0, Lcom/evernote/edam/type/User;->accounting:Lcom/evernote/edam/type/Accounting;

    .line 233
    iput-object v1, p0, Lcom/evernote/edam/type/User;->premiumInfo:Lcom/evernote/edam/type/PremiumInfo;

    .line 234
    iput-object v1, p0, Lcom/evernote/edam/type/User;->businessUserInfo:Lcom/evernote/edam/type/BusinessUserInfo;

    return-void
.end method

.method public compareTo(Lcom/evernote/edam/type/User;)I
    .locals 4

    .line 742
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 743
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    return p1

    .line 749
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetId()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/User;->isSetId()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_1

    return v0

    .line 753
    :cond_1
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetId()Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/evernote/edam/type/User;->id:I

    iget v1, p1, Lcom/evernote/edam/type/User;->id:I

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(II)I

    move-result v0

    if-eqz v0, :cond_2

    return v0

    .line 758
    :cond_2
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetUsername()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/User;->isSetUsername()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_3

    return v0

    .line 762
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetUsername()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/evernote/edam/type/User;->username:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/type/User;->username:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_4

    return v0

    .line 767
    :cond_4
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetEmail()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/User;->isSetEmail()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_5

    return v0

    .line 771
    :cond_5
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetEmail()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/evernote/edam/type/User;->email:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/type/User;->email:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_6

    return v0

    .line 776
    :cond_6
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetName()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/User;->isSetName()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_7

    return v0

    .line 780
    :cond_7
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetName()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/evernote/edam/type/User;->name:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/type/User;->name:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_8

    return v0

    .line 785
    :cond_8
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetTimezone()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/User;->isSetTimezone()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_9

    return v0

    .line 789
    :cond_9
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetTimezone()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/evernote/edam/type/User;->timezone:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/type/User;->timezone:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_a

    return v0

    .line 794
    :cond_a
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetPrivilege()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/User;->isSetPrivilege()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_b

    return v0

    .line 798
    :cond_b
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetPrivilege()Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/evernote/edam/type/User;->privilege:Lcom/evernote/edam/type/PrivilegeLevel;

    iget-object v1, p1, Lcom/evernote/edam/type/User;->privilege:Lcom/evernote/edam/type/PrivilegeLevel;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result v0

    if-eqz v0, :cond_c

    return v0

    .line 803
    :cond_c
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetCreated()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/User;->isSetCreated()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_d

    return v0

    .line 807
    :cond_d
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetCreated()Z

    move-result v0

    if-eqz v0, :cond_e

    iget-wide v0, p0, Lcom/evernote/edam/type/User;->created:J

    iget-wide v2, p1, Lcom/evernote/edam/type/User;->created:J

    invoke-static {v0, v1, v2, v3}, Lcom/evernote/thrift/TBaseHelper;->compareTo(JJ)I

    move-result v0

    if-eqz v0, :cond_e

    return v0

    .line 812
    :cond_e
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetUpdated()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/User;->isSetUpdated()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_f

    return v0

    .line 816
    :cond_f
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetUpdated()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-wide v0, p0, Lcom/evernote/edam/type/User;->updated:J

    iget-wide v2, p1, Lcom/evernote/edam/type/User;->updated:J

    invoke-static {v0, v1, v2, v3}, Lcom/evernote/thrift/TBaseHelper;->compareTo(JJ)I

    move-result v0

    if-eqz v0, :cond_10

    return v0

    .line 821
    :cond_10
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetDeleted()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/User;->isSetDeleted()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_11

    return v0

    .line 825
    :cond_11
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetDeleted()Z

    move-result v0

    if-eqz v0, :cond_12

    iget-wide v0, p0, Lcom/evernote/edam/type/User;->deleted:J

    iget-wide v2, p1, Lcom/evernote/edam/type/User;->deleted:J

    invoke-static {v0, v1, v2, v3}, Lcom/evernote/thrift/TBaseHelper;->compareTo(JJ)I

    move-result v0

    if-eqz v0, :cond_12

    return v0

    .line 830
    :cond_12
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetActive()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/User;->isSetActive()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_13

    return v0

    .line 834
    :cond_13
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetActive()Z

    move-result v0

    if-eqz v0, :cond_14

    iget-boolean v0, p0, Lcom/evernote/edam/type/User;->active:Z

    iget-boolean v1, p1, Lcom/evernote/edam/type/User;->active:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_14

    return v0

    .line 839
    :cond_14
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetShardId()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/User;->isSetShardId()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_15

    return v0

    .line 843
    :cond_15
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetShardId()Z

    move-result v0

    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/evernote/edam/type/User;->shardId:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/type/User;->shardId:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_16

    return v0

    .line 848
    :cond_16
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetAttributes()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/User;->isSetAttributes()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_17

    return v0

    .line 852
    :cond_17
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetAttributes()Z

    move-result v0

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/evernote/edam/type/User;->attributes:Lcom/evernote/edam/type/UserAttributes;

    iget-object v1, p1, Lcom/evernote/edam/type/User;->attributes:Lcom/evernote/edam/type/UserAttributes;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result v0

    if-eqz v0, :cond_18

    return v0

    .line 857
    :cond_18
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetAccounting()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/User;->isSetAccounting()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_19

    return v0

    .line 861
    :cond_19
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetAccounting()Z

    move-result v0

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lcom/evernote/edam/type/User;->accounting:Lcom/evernote/edam/type/Accounting;

    iget-object v1, p1, Lcom/evernote/edam/type/User;->accounting:Lcom/evernote/edam/type/Accounting;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result v0

    if-eqz v0, :cond_1a

    return v0

    .line 866
    :cond_1a
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetPremiumInfo()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/User;->isSetPremiumInfo()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_1b

    return v0

    .line 870
    :cond_1b
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetPremiumInfo()Z

    move-result v0

    if-eqz v0, :cond_1c

    iget-object v0, p0, Lcom/evernote/edam/type/User;->premiumInfo:Lcom/evernote/edam/type/PremiumInfo;

    iget-object v1, p1, Lcom/evernote/edam/type/User;->premiumInfo:Lcom/evernote/edam/type/PremiumInfo;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result v0

    if-eqz v0, :cond_1c

    return v0

    .line 875
    :cond_1c
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetBusinessUserInfo()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/User;->isSetBusinessUserInfo()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_1d

    return v0

    .line 879
    :cond_1d
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetBusinessUserInfo()Z

    move-result v0

    if-eqz v0, :cond_1e

    iget-object v0, p0, Lcom/evernote/edam/type/User;->businessUserInfo:Lcom/evernote/edam/type/BusinessUserInfo;

    iget-object p1, p1, Lcom/evernote/edam/type/User;->businessUserInfo:Lcom/evernote/edam/type/BusinessUserInfo;

    invoke-static {v0, p1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result p1

    if-eqz p1, :cond_1e

    return p1

    :cond_1e
    const/4 p1, 0x0

    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 121
    check-cast p1, Lcom/evernote/edam/type/User;

    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/User;->compareTo(Lcom/evernote/edam/type/User;)I

    move-result p1

    return p1
.end method

.method public deepCopy()Lcom/evernote/edam/type/User;
    .locals 1

    .line 211
    new-instance v0, Lcom/evernote/edam/type/User;

    invoke-direct {v0, p0}, Lcom/evernote/edam/type/User;-><init>(Lcom/evernote/edam/type/User;)V

    return-object v0
.end method

.method public bridge synthetic deepCopy()Lcom/evernote/thrift/TBase;
    .locals 1

    .line 121
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->deepCopy()Lcom/evernote/edam/type/User;

    move-result-object v0

    return-object v0
.end method

.method public equals(Lcom/evernote/edam/type/User;)Z
    .locals 6

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 598
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetId()Z

    move-result v1

    .line 599
    invoke-virtual {p1}, Lcom/evernote/edam/type/User;->isSetId()Z

    move-result v2

    if-nez v1, :cond_1

    if-eqz v2, :cond_3

    :cond_1
    if-eqz v1, :cond_3c

    if-nez v2, :cond_2

    goto/16 :goto_e

    .line 603
    :cond_2
    iget v1, p0, Lcom/evernote/edam/type/User;->id:I

    iget v2, p1, Lcom/evernote/edam/type/User;->id:I

    if-eq v1, v2, :cond_3

    return v0

    .line 607
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetUsername()Z

    move-result v1

    .line 608
    invoke-virtual {p1}, Lcom/evernote/edam/type/User;->isSetUsername()Z

    move-result v2

    if-nez v1, :cond_4

    if-eqz v2, :cond_6

    :cond_4
    if-eqz v1, :cond_3b

    if-nez v2, :cond_5

    goto/16 :goto_d

    .line 612
    :cond_5
    iget-object v1, p0, Lcom/evernote/edam/type/User;->username:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/type/User;->username:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    return v0

    .line 616
    :cond_6
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetEmail()Z

    move-result v1

    .line 617
    invoke-virtual {p1}, Lcom/evernote/edam/type/User;->isSetEmail()Z

    move-result v2

    if-nez v1, :cond_7

    if-eqz v2, :cond_9

    :cond_7
    if-eqz v1, :cond_3a

    if-nez v2, :cond_8

    goto/16 :goto_c

    .line 621
    :cond_8
    iget-object v1, p0, Lcom/evernote/edam/type/User;->email:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/type/User;->email:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    return v0

    .line 625
    :cond_9
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetName()Z

    move-result v1

    .line 626
    invoke-virtual {p1}, Lcom/evernote/edam/type/User;->isSetName()Z

    move-result v2

    if-nez v1, :cond_a

    if-eqz v2, :cond_c

    :cond_a
    if-eqz v1, :cond_39

    if-nez v2, :cond_b

    goto/16 :goto_b

    .line 630
    :cond_b
    iget-object v1, p0, Lcom/evernote/edam/type/User;->name:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/type/User;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    return v0

    .line 634
    :cond_c
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetTimezone()Z

    move-result v1

    .line 635
    invoke-virtual {p1}, Lcom/evernote/edam/type/User;->isSetTimezone()Z

    move-result v2

    if-nez v1, :cond_d

    if-eqz v2, :cond_f

    :cond_d
    if-eqz v1, :cond_38

    if-nez v2, :cond_e

    goto/16 :goto_a

    .line 639
    :cond_e
    iget-object v1, p0, Lcom/evernote/edam/type/User;->timezone:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/type/User;->timezone:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    return v0

    .line 643
    :cond_f
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetPrivilege()Z

    move-result v1

    .line 644
    invoke-virtual {p1}, Lcom/evernote/edam/type/User;->isSetPrivilege()Z

    move-result v2

    if-nez v1, :cond_10

    if-eqz v2, :cond_12

    :cond_10
    if-eqz v1, :cond_37

    if-nez v2, :cond_11

    goto/16 :goto_9

    .line 648
    :cond_11
    iget-object v1, p0, Lcom/evernote/edam/type/User;->privilege:Lcom/evernote/edam/type/PrivilegeLevel;

    iget-object v2, p1, Lcom/evernote/edam/type/User;->privilege:Lcom/evernote/edam/type/PrivilegeLevel;

    invoke-virtual {v1, v2}, Lcom/evernote/edam/type/PrivilegeLevel;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_12

    return v0

    .line 652
    :cond_12
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetCreated()Z

    move-result v1

    .line 653
    invoke-virtual {p1}, Lcom/evernote/edam/type/User;->isSetCreated()Z

    move-result v2

    if-nez v1, :cond_13

    if-eqz v2, :cond_15

    :cond_13
    if-eqz v1, :cond_36

    if-nez v2, :cond_14

    goto/16 :goto_8

    .line 657
    :cond_14
    iget-wide v1, p0, Lcom/evernote/edam/type/User;->created:J

    iget-wide v3, p1, Lcom/evernote/edam/type/User;->created:J

    cmp-long v5, v1, v3

    if-eqz v5, :cond_15

    return v0

    .line 661
    :cond_15
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetUpdated()Z

    move-result v1

    .line 662
    invoke-virtual {p1}, Lcom/evernote/edam/type/User;->isSetUpdated()Z

    move-result v2

    if-nez v1, :cond_16

    if-eqz v2, :cond_18

    :cond_16
    if-eqz v1, :cond_35

    if-nez v2, :cond_17

    goto/16 :goto_7

    .line 666
    :cond_17
    iget-wide v1, p0, Lcom/evernote/edam/type/User;->updated:J

    iget-wide v3, p1, Lcom/evernote/edam/type/User;->updated:J

    cmp-long v5, v1, v3

    if-eqz v5, :cond_18

    return v0

    .line 670
    :cond_18
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetDeleted()Z

    move-result v1

    .line 671
    invoke-virtual {p1}, Lcom/evernote/edam/type/User;->isSetDeleted()Z

    move-result v2

    if-nez v1, :cond_19

    if-eqz v2, :cond_1b

    :cond_19
    if-eqz v1, :cond_34

    if-nez v2, :cond_1a

    goto/16 :goto_6

    .line 675
    :cond_1a
    iget-wide v1, p0, Lcom/evernote/edam/type/User;->deleted:J

    iget-wide v3, p1, Lcom/evernote/edam/type/User;->deleted:J

    cmp-long v5, v1, v3

    if-eqz v5, :cond_1b

    return v0

    .line 679
    :cond_1b
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetActive()Z

    move-result v1

    .line 680
    invoke-virtual {p1}, Lcom/evernote/edam/type/User;->isSetActive()Z

    move-result v2

    if-nez v1, :cond_1c

    if-eqz v2, :cond_1e

    :cond_1c
    if-eqz v1, :cond_33

    if-nez v2, :cond_1d

    goto/16 :goto_5

    .line 684
    :cond_1d
    iget-boolean v1, p0, Lcom/evernote/edam/type/User;->active:Z

    iget-boolean v2, p1, Lcom/evernote/edam/type/User;->active:Z

    if-eq v1, v2, :cond_1e

    return v0

    .line 688
    :cond_1e
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetShardId()Z

    move-result v1

    .line 689
    invoke-virtual {p1}, Lcom/evernote/edam/type/User;->isSetShardId()Z

    move-result v2

    if-nez v1, :cond_1f

    if-eqz v2, :cond_21

    :cond_1f
    if-eqz v1, :cond_32

    if-nez v2, :cond_20

    goto/16 :goto_4

    .line 693
    :cond_20
    iget-object v1, p0, Lcom/evernote/edam/type/User;->shardId:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/type/User;->shardId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_21

    return v0

    .line 697
    :cond_21
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetAttributes()Z

    move-result v1

    .line 698
    invoke-virtual {p1}, Lcom/evernote/edam/type/User;->isSetAttributes()Z

    move-result v2

    if-nez v1, :cond_22

    if-eqz v2, :cond_24

    :cond_22
    if-eqz v1, :cond_31

    if-nez v2, :cond_23

    goto :goto_3

    .line 702
    :cond_23
    iget-object v1, p0, Lcom/evernote/edam/type/User;->attributes:Lcom/evernote/edam/type/UserAttributes;

    iget-object v2, p1, Lcom/evernote/edam/type/User;->attributes:Lcom/evernote/edam/type/UserAttributes;

    invoke-virtual {v1, v2}, Lcom/evernote/edam/type/UserAttributes;->equals(Lcom/evernote/edam/type/UserAttributes;)Z

    move-result v1

    if-nez v1, :cond_24

    return v0

    .line 706
    :cond_24
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetAccounting()Z

    move-result v1

    .line 707
    invoke-virtual {p1}, Lcom/evernote/edam/type/User;->isSetAccounting()Z

    move-result v2

    if-nez v1, :cond_25

    if-eqz v2, :cond_27

    :cond_25
    if-eqz v1, :cond_30

    if-nez v2, :cond_26

    goto :goto_2

    .line 711
    :cond_26
    iget-object v1, p0, Lcom/evernote/edam/type/User;->accounting:Lcom/evernote/edam/type/Accounting;

    iget-object v2, p1, Lcom/evernote/edam/type/User;->accounting:Lcom/evernote/edam/type/Accounting;

    invoke-virtual {v1, v2}, Lcom/evernote/edam/type/Accounting;->equals(Lcom/evernote/edam/type/Accounting;)Z

    move-result v1

    if-nez v1, :cond_27

    return v0

    .line 715
    :cond_27
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetPremiumInfo()Z

    move-result v1

    .line 716
    invoke-virtual {p1}, Lcom/evernote/edam/type/User;->isSetPremiumInfo()Z

    move-result v2

    if-nez v1, :cond_28

    if-eqz v2, :cond_2a

    :cond_28
    if-eqz v1, :cond_2f

    if-nez v2, :cond_29

    goto :goto_1

    .line 720
    :cond_29
    iget-object v1, p0, Lcom/evernote/edam/type/User;->premiumInfo:Lcom/evernote/edam/type/PremiumInfo;

    iget-object v2, p1, Lcom/evernote/edam/type/User;->premiumInfo:Lcom/evernote/edam/type/PremiumInfo;

    invoke-virtual {v1, v2}, Lcom/evernote/edam/type/PremiumInfo;->equals(Lcom/evernote/edam/type/PremiumInfo;)Z

    move-result v1

    if-nez v1, :cond_2a

    return v0

    .line 724
    :cond_2a
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetBusinessUserInfo()Z

    move-result v1

    .line 725
    invoke-virtual {p1}, Lcom/evernote/edam/type/User;->isSetBusinessUserInfo()Z

    move-result v2

    if-nez v1, :cond_2b

    if-eqz v2, :cond_2d

    :cond_2b
    if-eqz v1, :cond_2e

    if-nez v2, :cond_2c

    goto :goto_0

    .line 729
    :cond_2c
    iget-object v1, p0, Lcom/evernote/edam/type/User;->businessUserInfo:Lcom/evernote/edam/type/BusinessUserInfo;

    iget-object p1, p1, Lcom/evernote/edam/type/User;->businessUserInfo:Lcom/evernote/edam/type/BusinessUserInfo;

    invoke-virtual {v1, p1}, Lcom/evernote/edam/type/BusinessUserInfo;->equals(Lcom/evernote/edam/type/BusinessUserInfo;)Z

    move-result p1

    if-nez p1, :cond_2d

    return v0

    :cond_2d
    const/4 p1, 0x1

    return p1

    :cond_2e
    :goto_0
    return v0

    :cond_2f
    :goto_1
    return v0

    :cond_30
    :goto_2
    return v0

    :cond_31
    :goto_3
    return v0

    :cond_32
    :goto_4
    return v0

    :cond_33
    :goto_5
    return v0

    :cond_34
    :goto_6
    return v0

    :cond_35
    :goto_7
    return v0

    :cond_36
    :goto_8
    return v0

    :cond_37
    :goto_9
    return v0

    :cond_38
    :goto_a
    return v0

    :cond_39
    :goto_b
    return v0

    :cond_3a
    :goto_c
    return v0

    :cond_3b
    :goto_d
    return v0

    :cond_3c
    :goto_e
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 589
    :cond_0
    instance-of v1, p1, Lcom/evernote/edam/type/User;

    if-eqz v1, :cond_1

    .line 590
    check-cast p1, Lcom/evernote/edam/type/User;

    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/User;->equals(Lcom/evernote/edam/type/User;)Z

    move-result p1

    return p1

    :cond_1
    return v0
.end method

.method public getAccounting()Lcom/evernote/edam/type/Accounting;
    .locals 1

    .line 517
    iget-object v0, p0, Lcom/evernote/edam/type/User;->accounting:Lcom/evernote/edam/type/Accounting;

    return-object v0
.end method

.method public getAttributes()Lcom/evernote/edam/type/UserAttributes;
    .locals 1

    .line 494
    iget-object v0, p0, Lcom/evernote/edam/type/User;->attributes:Lcom/evernote/edam/type/UserAttributes;

    return-object v0
.end method

.method public getBusinessUserInfo()Lcom/evernote/edam/type/BusinessUserInfo;
    .locals 1

    .line 563
    iget-object v0, p0, Lcom/evernote/edam/type/User;->businessUserInfo:Lcom/evernote/edam/type/BusinessUserInfo;

    return-object v0
.end method

.method public getCreated()J
    .locals 2

    .line 383
    iget-wide v0, p0, Lcom/evernote/edam/type/User;->created:J

    return-wide v0
.end method

.method public getDeleted()J
    .locals 2

    .line 427
    iget-wide v0, p0, Lcom/evernote/edam/type/User;->deleted:J

    return-wide v0
.end method

.method public getEmail()Ljava/lang/String;
    .locals 1

    .line 283
    iget-object v0, p0, Lcom/evernote/edam/type/User;->email:Ljava/lang/String;

    return-object v0
.end method

.method public getId()I
    .locals 1

    .line 238
    iget v0, p0, Lcom/evernote/edam/type/User;->id:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 306
    iget-object v0, p0, Lcom/evernote/edam/type/User;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getPremiumInfo()Lcom/evernote/edam/type/PremiumInfo;
    .locals 1

    .line 540
    iget-object v0, p0, Lcom/evernote/edam/type/User;->premiumInfo:Lcom/evernote/edam/type/PremiumInfo;

    return-object v0
.end method

.method public getPrivilege()Lcom/evernote/edam/type/PrivilegeLevel;
    .locals 1

    .line 356
    iget-object v0, p0, Lcom/evernote/edam/type/User;->privilege:Lcom/evernote/edam/type/PrivilegeLevel;

    return-object v0
.end method

.method public getShardId()Ljava/lang/String;
    .locals 1

    .line 471
    iget-object v0, p0, Lcom/evernote/edam/type/User;->shardId:Ljava/lang/String;

    return-object v0
.end method

.method public getTimezone()Ljava/lang/String;
    .locals 1

    .line 329
    iget-object v0, p0, Lcom/evernote/edam/type/User;->timezone:Ljava/lang/String;

    return-object v0
.end method

.method public getUpdated()J
    .locals 2

    .line 405
    iget-wide v0, p0, Lcom/evernote/edam/type/User;->updated:J

    return-wide v0
.end method

.method public getUsername()Ljava/lang/String;
    .locals 1

    .line 260
    iget-object v0, p0, Lcom/evernote/edam/type/User;->username:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isActive()Z
    .locals 1

    .line 449
    iget-boolean v0, p0, Lcom/evernote/edam/type/User;->active:Z

    return v0
.end method

.method public isSetAccounting()Z
    .locals 1

    .line 530
    iget-object v0, p0, Lcom/evernote/edam/type/User;->accounting:Lcom/evernote/edam/type/Accounting;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetActive()Z
    .locals 2

    .line 463
    iget-object v0, p0, Lcom/evernote/edam/type/User;->__isset_vector:[Z

    const/4 v1, 0x4

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetAttributes()Z
    .locals 1

    .line 507
    iget-object v0, p0, Lcom/evernote/edam/type/User;->attributes:Lcom/evernote/edam/type/UserAttributes;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetBusinessUserInfo()Z
    .locals 1

    .line 576
    iget-object v0, p0, Lcom/evernote/edam/type/User;->businessUserInfo:Lcom/evernote/edam/type/BusinessUserInfo;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetCreated()Z
    .locals 2

    .line 397
    iget-object v0, p0, Lcom/evernote/edam/type/User;->__isset_vector:[Z

    const/4 v1, 0x1

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetDeleted()Z
    .locals 2

    .line 441
    iget-object v0, p0, Lcom/evernote/edam/type/User;->__isset_vector:[Z

    const/4 v1, 0x3

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetEmail()Z
    .locals 1

    .line 296
    iget-object v0, p0, Lcom/evernote/edam/type/User;->email:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetId()Z
    .locals 2

    .line 252
    iget-object v0, p0, Lcom/evernote/edam/type/User;->__isset_vector:[Z

    const/4 v1, 0x0

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetName()Z
    .locals 1

    .line 319
    iget-object v0, p0, Lcom/evernote/edam/type/User;->name:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetPremiumInfo()Z
    .locals 1

    .line 553
    iget-object v0, p0, Lcom/evernote/edam/type/User;->premiumInfo:Lcom/evernote/edam/type/PremiumInfo;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetPrivilege()Z
    .locals 1

    .line 373
    iget-object v0, p0, Lcom/evernote/edam/type/User;->privilege:Lcom/evernote/edam/type/PrivilegeLevel;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetShardId()Z
    .locals 1

    .line 484
    iget-object v0, p0, Lcom/evernote/edam/type/User;->shardId:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetTimezone()Z
    .locals 1

    .line 342
    iget-object v0, p0, Lcom/evernote/edam/type/User;->timezone:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetUpdated()Z
    .locals 2

    .line 419
    iget-object v0, p0, Lcom/evernote/edam/type/User;->__isset_vector:[Z

    const/4 v1, 0x2

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetUsername()Z
    .locals 1

    .line 273
    iget-object v0, p0, Lcom/evernote/edam/type/User;->username:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public read(Lcom/evernote/thrift/protocol/TProtocol;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 889
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readStructBegin()Lcom/evernote/thrift/protocol/TStruct;

    .line 892
    :goto_0
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readFieldBegin()Lcom/evernote/thrift/protocol/TField;

    move-result-object v0

    .line 893
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-nez v1, :cond_0

    .line 1016
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readStructEnd()V

    .line 1017
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->validate()V

    return-void

    .line 896
    :cond_0
    iget-short v1, v0, Lcom/evernote/thrift/protocol/TField;->id:S

    const/16 v2, 0x8

    const/16 v3, 0xa

    const/16 v4, 0xc

    const/4 v5, 0x1

    const/16 v6, 0xb

    packed-switch v1, :pswitch_data_0

    .line 1012
    :pswitch_0
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 1004
    :pswitch_1
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_1

    .line 1005
    new-instance v0, Lcom/evernote/edam/type/BusinessUserInfo;

    invoke-direct {v0}, Lcom/evernote/edam/type/BusinessUserInfo;-><init>()V

    iput-object v0, p0, Lcom/evernote/edam/type/User;->businessUserInfo:Lcom/evernote/edam/type/BusinessUserInfo;

    .line 1006
    iget-object v0, p0, Lcom/evernote/edam/type/User;->businessUserInfo:Lcom/evernote/edam/type/BusinessUserInfo;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/type/BusinessUserInfo;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    goto/16 :goto_1

    .line 1008
    :cond_1
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 996
    :pswitch_2
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_2

    .line 997
    new-instance v0, Lcom/evernote/edam/type/PremiumInfo;

    invoke-direct {v0}, Lcom/evernote/edam/type/PremiumInfo;-><init>()V

    iput-object v0, p0, Lcom/evernote/edam/type/User;->premiumInfo:Lcom/evernote/edam/type/PremiumInfo;

    .line 998
    iget-object v0, p0, Lcom/evernote/edam/type/User;->premiumInfo:Lcom/evernote/edam/type/PremiumInfo;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/type/PremiumInfo;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    goto/16 :goto_1

    .line 1000
    :cond_2
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 988
    :pswitch_3
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_3

    .line 989
    new-instance v0, Lcom/evernote/edam/type/Accounting;

    invoke-direct {v0}, Lcom/evernote/edam/type/Accounting;-><init>()V

    iput-object v0, p0, Lcom/evernote/edam/type/User;->accounting:Lcom/evernote/edam/type/Accounting;

    .line 990
    iget-object v0, p0, Lcom/evernote/edam/type/User;->accounting:Lcom/evernote/edam/type/Accounting;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/type/Accounting;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    goto/16 :goto_1

    .line 992
    :cond_3
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 980
    :pswitch_4
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_4

    .line 981
    new-instance v0, Lcom/evernote/edam/type/UserAttributes;

    invoke-direct {v0}, Lcom/evernote/edam/type/UserAttributes;-><init>()V

    iput-object v0, p0, Lcom/evernote/edam/type/User;->attributes:Lcom/evernote/edam/type/UserAttributes;

    .line 982
    iget-object v0, p0, Lcom/evernote/edam/type/User;->attributes:Lcom/evernote/edam/type/UserAttributes;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/type/UserAttributes;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    goto/16 :goto_1

    .line 984
    :cond_4
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 973
    :pswitch_5
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v6, :cond_5

    .line 974
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/type/User;->shardId:Ljava/lang/String;

    goto/16 :goto_1

    .line 976
    :cond_5
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 965
    :pswitch_6
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    const/4 v2, 0x2

    if-ne v1, v2, :cond_6

    .line 966
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/type/User;->active:Z

    .line 967
    invoke-virtual {p0, v5}, Lcom/evernote/edam/type/User;->setActiveIsSet(Z)V

    goto/16 :goto_1

    .line 969
    :cond_6
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 957
    :pswitch_7
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v3, :cond_7

    .line 958
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/evernote/edam/type/User;->deleted:J

    .line 959
    invoke-virtual {p0, v5}, Lcom/evernote/edam/type/User;->setDeletedIsSet(Z)V

    goto/16 :goto_1

    .line 961
    :cond_7
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 949
    :pswitch_8
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v3, :cond_8

    .line 950
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/evernote/edam/type/User;->updated:J

    .line 951
    invoke-virtual {p0, v5}, Lcom/evernote/edam/type/User;->setUpdatedIsSet(Z)V

    goto/16 :goto_1

    .line 953
    :cond_8
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 941
    :pswitch_9
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v3, :cond_9

    .line 942
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/evernote/edam/type/User;->created:J

    .line 943
    invoke-virtual {p0, v5}, Lcom/evernote/edam/type/User;->setCreatedIsSet(Z)V

    goto/16 :goto_1

    .line 945
    :cond_9
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 934
    :pswitch_a
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v2, :cond_a

    .line 935
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI32()I

    move-result v0

    invoke-static {v0}, Lcom/evernote/edam/type/PrivilegeLevel;->findByValue(I)Lcom/evernote/edam/type/PrivilegeLevel;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/type/User;->privilege:Lcom/evernote/edam/type/PrivilegeLevel;

    goto :goto_1

    .line 937
    :cond_a
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 927
    :pswitch_b
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v6, :cond_b

    .line 928
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/type/User;->timezone:Ljava/lang/String;

    goto :goto_1

    .line 930
    :cond_b
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 920
    :pswitch_c
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v6, :cond_c

    .line 921
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/type/User;->name:Ljava/lang/String;

    goto :goto_1

    .line 923
    :cond_c
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 913
    :pswitch_d
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v6, :cond_d

    .line 914
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/type/User;->email:Ljava/lang/String;

    goto :goto_1

    .line 916
    :cond_d
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 906
    :pswitch_e
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v6, :cond_e

    .line 907
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/type/User;->username:Ljava/lang/String;

    goto :goto_1

    .line 909
    :cond_e
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 898
    :pswitch_f
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v2, :cond_f

    .line 899
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI32()I

    move-result v0

    iput v0, p0, Lcom/evernote/edam/type/User;->id:I

    .line 900
    invoke-virtual {p0, v5}, Lcom/evernote/edam/type/User;->setIdIsSet(Z)V

    goto :goto_1

    .line 902
    :cond_f
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    .line 1014
    :goto_1
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readFieldEnd()V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_0
        :pswitch_b
        :pswitch_a
        :pswitch_0
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public setAccounting(Lcom/evernote/edam/type/Accounting;)V
    .locals 0

    .line 521
    iput-object p1, p0, Lcom/evernote/edam/type/User;->accounting:Lcom/evernote/edam/type/Accounting;

    return-void
.end method

.method public setAccountingIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 535
    iput-object p1, p0, Lcom/evernote/edam/type/User;->accounting:Lcom/evernote/edam/type/Accounting;

    :cond_0
    return-void
.end method

.method public setActive(Z)V
    .locals 0

    .line 453
    iput-boolean p1, p0, Lcom/evernote/edam/type/User;->active:Z

    const/4 p1, 0x1

    .line 454
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/User;->setActiveIsSet(Z)V

    return-void
.end method

.method public setActiveIsSet(Z)V
    .locals 2

    .line 467
    iget-object v0, p0, Lcom/evernote/edam/type/User;->__isset_vector:[Z

    const/4 v1, 0x4

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setAttributes(Lcom/evernote/edam/type/UserAttributes;)V
    .locals 0

    .line 498
    iput-object p1, p0, Lcom/evernote/edam/type/User;->attributes:Lcom/evernote/edam/type/UserAttributes;

    return-void
.end method

.method public setAttributesIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 512
    iput-object p1, p0, Lcom/evernote/edam/type/User;->attributes:Lcom/evernote/edam/type/UserAttributes;

    :cond_0
    return-void
.end method

.method public setBusinessUserInfo(Lcom/evernote/edam/type/BusinessUserInfo;)V
    .locals 0

    .line 567
    iput-object p1, p0, Lcom/evernote/edam/type/User;->businessUserInfo:Lcom/evernote/edam/type/BusinessUserInfo;

    return-void
.end method

.method public setBusinessUserInfoIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 581
    iput-object p1, p0, Lcom/evernote/edam/type/User;->businessUserInfo:Lcom/evernote/edam/type/BusinessUserInfo;

    :cond_0
    return-void
.end method

.method public setCreated(J)V
    .locals 0

    .line 387
    iput-wide p1, p0, Lcom/evernote/edam/type/User;->created:J

    const/4 p1, 0x1

    .line 388
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/User;->setCreatedIsSet(Z)V

    return-void
.end method

.method public setCreatedIsSet(Z)V
    .locals 2

    .line 401
    iget-object v0, p0, Lcom/evernote/edam/type/User;->__isset_vector:[Z

    const/4 v1, 0x1

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setDeleted(J)V
    .locals 0

    .line 431
    iput-wide p1, p0, Lcom/evernote/edam/type/User;->deleted:J

    const/4 p1, 0x1

    .line 432
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/User;->setDeletedIsSet(Z)V

    return-void
.end method

.method public setDeletedIsSet(Z)V
    .locals 2

    .line 445
    iget-object v0, p0, Lcom/evernote/edam/type/User;->__isset_vector:[Z

    const/4 v1, 0x3

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setEmail(Ljava/lang/String;)V
    .locals 0

    .line 287
    iput-object p1, p0, Lcom/evernote/edam/type/User;->email:Ljava/lang/String;

    return-void
.end method

.method public setEmailIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 301
    iput-object p1, p0, Lcom/evernote/edam/type/User;->email:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setId(I)V
    .locals 0

    .line 242
    iput p1, p0, Lcom/evernote/edam/type/User;->id:I

    const/4 p1, 0x1

    .line 243
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/User;->setIdIsSet(Z)V

    return-void
.end method

.method public setIdIsSet(Z)V
    .locals 2

    .line 256
    iget-object v0, p0, Lcom/evernote/edam/type/User;->__isset_vector:[Z

    const/4 v1, 0x0

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0

    .line 310
    iput-object p1, p0, Lcom/evernote/edam/type/User;->name:Ljava/lang/String;

    return-void
.end method

.method public setNameIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 324
    iput-object p1, p0, Lcom/evernote/edam/type/User;->name:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setPremiumInfo(Lcom/evernote/edam/type/PremiumInfo;)V
    .locals 0

    .line 544
    iput-object p1, p0, Lcom/evernote/edam/type/User;->premiumInfo:Lcom/evernote/edam/type/PremiumInfo;

    return-void
.end method

.method public setPremiumInfoIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 558
    iput-object p1, p0, Lcom/evernote/edam/type/User;->premiumInfo:Lcom/evernote/edam/type/PremiumInfo;

    :cond_0
    return-void
.end method

.method public setPrivilege(Lcom/evernote/edam/type/PrivilegeLevel;)V
    .locals 0

    .line 364
    iput-object p1, p0, Lcom/evernote/edam/type/User;->privilege:Lcom/evernote/edam/type/PrivilegeLevel;

    return-void
.end method

.method public setPrivilegeIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 378
    iput-object p1, p0, Lcom/evernote/edam/type/User;->privilege:Lcom/evernote/edam/type/PrivilegeLevel;

    :cond_0
    return-void
.end method

.method public setShardId(Ljava/lang/String;)V
    .locals 0

    .line 475
    iput-object p1, p0, Lcom/evernote/edam/type/User;->shardId:Ljava/lang/String;

    return-void
.end method

.method public setShardIdIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 489
    iput-object p1, p0, Lcom/evernote/edam/type/User;->shardId:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setTimezone(Ljava/lang/String;)V
    .locals 0

    .line 333
    iput-object p1, p0, Lcom/evernote/edam/type/User;->timezone:Ljava/lang/String;

    return-void
.end method

.method public setTimezoneIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 347
    iput-object p1, p0, Lcom/evernote/edam/type/User;->timezone:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setUpdated(J)V
    .locals 0

    .line 409
    iput-wide p1, p0, Lcom/evernote/edam/type/User;->updated:J

    const/4 p1, 0x1

    .line 410
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/User;->setUpdatedIsSet(Z)V

    return-void
.end method

.method public setUpdatedIsSet(Z)V
    .locals 2

    .line 423
    iget-object v0, p0, Lcom/evernote/edam/type/User;->__isset_vector:[Z

    const/4 v1, 0x2

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setUsername(Ljava/lang/String;)V
    .locals 0

    .line 264
    iput-object p1, p0, Lcom/evernote/edam/type/User;->username:Ljava/lang/String;

    return-void
.end method

.method public setUsernameIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 278
    iput-object p1, p0, Lcom/evernote/edam/type/User;->username:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .line 1125
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "User("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1128
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetId()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    const-string v1, "id:"

    .line 1129
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1130
    iget v1, p0, Lcom/evernote/edam/type/User;->id:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    const/4 v1, 0x1

    .line 1133
    :goto_0
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetUsername()Z

    move-result v3

    if-eqz v3, :cond_3

    if-nez v1, :cond_1

    const-string v1, ", "

    .line 1134
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const-string v1, "username:"

    .line 1135
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1136
    iget-object v1, p0, Lcom/evernote/edam/type/User;->username:Ljava/lang/String;

    if-nez v1, :cond_2

    const-string v1, "null"

    .line 1137
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1139
    :cond_2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_1
    const/4 v1, 0x0

    .line 1143
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetEmail()Z

    move-result v3

    if-eqz v3, :cond_6

    if-nez v1, :cond_4

    const-string v1, ", "

    .line 1144
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    const-string v1, "email:"

    .line 1145
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146
    iget-object v1, p0, Lcom/evernote/edam/type/User;->email:Ljava/lang/String;

    if-nez v1, :cond_5

    const-string v1, "null"

    .line 1147
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 1149
    :cond_5
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_2
    const/4 v1, 0x0

    .line 1153
    :cond_6
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetName()Z

    move-result v3

    if-eqz v3, :cond_9

    if-nez v1, :cond_7

    const-string v1, ", "

    .line 1154
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_7
    const-string v1, "name:"

    .line 1155
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1156
    iget-object v1, p0, Lcom/evernote/edam/type/User;->name:Ljava/lang/String;

    if-nez v1, :cond_8

    const-string v1, "null"

    .line 1157
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 1159
    :cond_8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    const/4 v1, 0x0

    .line 1163
    :cond_9
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetTimezone()Z

    move-result v3

    if-eqz v3, :cond_c

    if-nez v1, :cond_a

    const-string v1, ", "

    .line 1164
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_a
    const-string v1, "timezone:"

    .line 1165
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1166
    iget-object v1, p0, Lcom/evernote/edam/type/User;->timezone:Ljava/lang/String;

    if-nez v1, :cond_b

    const-string v1, "null"

    .line 1167
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 1169
    :cond_b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_4
    const/4 v1, 0x0

    .line 1173
    :cond_c
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetPrivilege()Z

    move-result v3

    if-eqz v3, :cond_f

    if-nez v1, :cond_d

    const-string v1, ", "

    .line 1174
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_d
    const-string v1, "privilege:"

    .line 1175
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1176
    iget-object v1, p0, Lcom/evernote/edam/type/User;->privilege:Lcom/evernote/edam/type/PrivilegeLevel;

    if-nez v1, :cond_e

    const-string v1, "null"

    .line 1177
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5

    .line 1179
    :cond_e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :goto_5
    const/4 v1, 0x0

    .line 1183
    :cond_f
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetCreated()Z

    move-result v3

    if-eqz v3, :cond_11

    if-nez v1, :cond_10

    const-string v1, ", "

    .line 1184
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_10
    const-string v1, "created:"

    .line 1185
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1186
    iget-wide v3, p0, Lcom/evernote/edam/type/User;->created:J

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1189
    :cond_11
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetUpdated()Z

    move-result v3

    if-eqz v3, :cond_13

    if-nez v1, :cond_12

    const-string v1, ", "

    .line 1190
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_12
    const-string v1, "updated:"

    .line 1191
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1192
    iget-wide v3, p0, Lcom/evernote/edam/type/User;->updated:J

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1195
    :cond_13
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetDeleted()Z

    move-result v3

    if-eqz v3, :cond_15

    if-nez v1, :cond_14

    const-string v1, ", "

    .line 1196
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_14
    const-string v1, "deleted:"

    .line 1197
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1198
    iget-wide v3, p0, Lcom/evernote/edam/type/User;->deleted:J

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1201
    :cond_15
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetActive()Z

    move-result v3

    if-eqz v3, :cond_17

    if-nez v1, :cond_16

    const-string v1, ", "

    .line 1202
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_16
    const-string v1, "active:"

    .line 1203
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1204
    iget-boolean v1, p0, Lcom/evernote/edam/type/User;->active:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1207
    :cond_17
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetShardId()Z

    move-result v3

    if-eqz v3, :cond_1a

    if-nez v1, :cond_18

    const-string v1, ", "

    .line 1208
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_18
    const-string v1, "shardId:"

    .line 1209
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1210
    iget-object v1, p0, Lcom/evernote/edam/type/User;->shardId:Ljava/lang/String;

    if-nez v1, :cond_19

    const-string v1, "null"

    .line 1211
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6

    .line 1213
    :cond_19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_6
    const/4 v1, 0x0

    .line 1217
    :cond_1a
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetAttributes()Z

    move-result v3

    if-eqz v3, :cond_1d

    if-nez v1, :cond_1b

    const-string v1, ", "

    .line 1218
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1b
    const-string v1, "attributes:"

    .line 1219
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1220
    iget-object v1, p0, Lcom/evernote/edam/type/User;->attributes:Lcom/evernote/edam/type/UserAttributes;

    if-nez v1, :cond_1c

    const-string v1, "null"

    .line 1221
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_7

    .line 1223
    :cond_1c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :goto_7
    const/4 v1, 0x0

    .line 1227
    :cond_1d
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetAccounting()Z

    move-result v3

    if-eqz v3, :cond_20

    if-nez v1, :cond_1e

    const-string v1, ", "

    .line 1228
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1e
    const-string v1, "accounting:"

    .line 1229
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1230
    iget-object v1, p0, Lcom/evernote/edam/type/User;->accounting:Lcom/evernote/edam/type/Accounting;

    if-nez v1, :cond_1f

    const-string v1, "null"

    .line 1231
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_8

    .line 1233
    :cond_1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :goto_8
    const/4 v1, 0x0

    .line 1237
    :cond_20
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetPremiumInfo()Z

    move-result v3

    if-eqz v3, :cond_23

    if-nez v1, :cond_21

    const-string v1, ", "

    .line 1238
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_21
    const-string v1, "premiumInfo:"

    .line 1239
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1240
    iget-object v1, p0, Lcom/evernote/edam/type/User;->premiumInfo:Lcom/evernote/edam/type/PremiumInfo;

    if-nez v1, :cond_22

    const-string v1, "null"

    .line 1241
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_9

    .line 1243
    :cond_22
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :goto_9
    const/4 v1, 0x0

    .line 1247
    :cond_23
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetBusinessUserInfo()Z

    move-result v2

    if-eqz v2, :cond_26

    if-nez v1, :cond_24

    const-string v1, ", "

    .line 1248
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_24
    const-string v1, "businessUserInfo:"

    .line 1249
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1250
    iget-object v1, p0, Lcom/evernote/edam/type/User;->businessUserInfo:Lcom/evernote/edam/type/BusinessUserInfo;

    if-nez v1, :cond_25

    const-string v1, "null"

    .line 1251
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_a

    .line 1253
    :cond_25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_26
    :goto_a
    const-string v1, ")"

    .line 1257
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1258
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public unsetAccounting()V
    .locals 1

    const/4 v0, 0x0

    .line 525
    iput-object v0, p0, Lcom/evernote/edam/type/User;->accounting:Lcom/evernote/edam/type/Accounting;

    return-void
.end method

.method public unsetActive()V
    .locals 3

    .line 458
    iget-object v0, p0, Lcom/evernote/edam/type/User;->__isset_vector:[Z

    const/4 v1, 0x4

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetAttributes()V
    .locals 1

    const/4 v0, 0x0

    .line 502
    iput-object v0, p0, Lcom/evernote/edam/type/User;->attributes:Lcom/evernote/edam/type/UserAttributes;

    return-void
.end method

.method public unsetBusinessUserInfo()V
    .locals 1

    const/4 v0, 0x0

    .line 571
    iput-object v0, p0, Lcom/evernote/edam/type/User;->businessUserInfo:Lcom/evernote/edam/type/BusinessUserInfo;

    return-void
.end method

.method public unsetCreated()V
    .locals 3

    .line 392
    iget-object v0, p0, Lcom/evernote/edam/type/User;->__isset_vector:[Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetDeleted()V
    .locals 3

    .line 436
    iget-object v0, p0, Lcom/evernote/edam/type/User;->__isset_vector:[Z

    const/4 v1, 0x3

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetEmail()V
    .locals 1

    const/4 v0, 0x0

    .line 291
    iput-object v0, p0, Lcom/evernote/edam/type/User;->email:Ljava/lang/String;

    return-void
.end method

.method public unsetId()V
    .locals 2

    .line 247
    iget-object v0, p0, Lcom/evernote/edam/type/User;->__isset_vector:[Z

    const/4 v1, 0x0

    aput-boolean v1, v0, v1

    return-void
.end method

.method public unsetName()V
    .locals 1

    const/4 v0, 0x0

    .line 314
    iput-object v0, p0, Lcom/evernote/edam/type/User;->name:Ljava/lang/String;

    return-void
.end method

.method public unsetPremiumInfo()V
    .locals 1

    const/4 v0, 0x0

    .line 548
    iput-object v0, p0, Lcom/evernote/edam/type/User;->premiumInfo:Lcom/evernote/edam/type/PremiumInfo;

    return-void
.end method

.method public unsetPrivilege()V
    .locals 1

    const/4 v0, 0x0

    .line 368
    iput-object v0, p0, Lcom/evernote/edam/type/User;->privilege:Lcom/evernote/edam/type/PrivilegeLevel;

    return-void
.end method

.method public unsetShardId()V
    .locals 1

    const/4 v0, 0x0

    .line 479
    iput-object v0, p0, Lcom/evernote/edam/type/User;->shardId:Ljava/lang/String;

    return-void
.end method

.method public unsetTimezone()V
    .locals 1

    const/4 v0, 0x0

    .line 337
    iput-object v0, p0, Lcom/evernote/edam/type/User;->timezone:Ljava/lang/String;

    return-void
.end method

.method public unsetUpdated()V
    .locals 3

    .line 414
    iget-object v0, p0, Lcom/evernote/edam/type/User;->__isset_vector:[Z

    const/4 v1, 0x2

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetUsername()V
    .locals 1

    const/4 v0, 0x0

    .line 268
    iput-object v0, p0, Lcom/evernote/edam/type/User;->username:Ljava/lang/String;

    return-void
.end method

.method public validate()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    return-void
.end method

.method public write(Lcom/evernote/thrift/protocol/TProtocol;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1021
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->validate()V

    .line 1023
    sget-object v0, Lcom/evernote/edam/type/User;->STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeStructBegin(Lcom/evernote/thrift/protocol/TStruct;)V

    .line 1024
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetId()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1025
    sget-object v0, Lcom/evernote/edam/type/User;->ID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1026
    iget v0, p0, Lcom/evernote/edam/type/User;->id:I

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeI32(I)V

    .line 1027
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1029
    :cond_0
    iget-object v0, p0, Lcom/evernote/edam/type/User;->username:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1030
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetUsername()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1031
    sget-object v0, Lcom/evernote/edam/type/User;->USERNAME_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1032
    iget-object v0, p0, Lcom/evernote/edam/type/User;->username:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 1033
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1036
    :cond_1
    iget-object v0, p0, Lcom/evernote/edam/type/User;->email:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1037
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetEmail()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1038
    sget-object v0, Lcom/evernote/edam/type/User;->EMAIL_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1039
    iget-object v0, p0, Lcom/evernote/edam/type/User;->email:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 1040
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1043
    :cond_2
    iget-object v0, p0, Lcom/evernote/edam/type/User;->name:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1044
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetName()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1045
    sget-object v0, Lcom/evernote/edam/type/User;->NAME_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1046
    iget-object v0, p0, Lcom/evernote/edam/type/User;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 1047
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1050
    :cond_3
    iget-object v0, p0, Lcom/evernote/edam/type/User;->timezone:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 1051
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetTimezone()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1052
    sget-object v0, Lcom/evernote/edam/type/User;->TIMEZONE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1053
    iget-object v0, p0, Lcom/evernote/edam/type/User;->timezone:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 1054
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1057
    :cond_4
    iget-object v0, p0, Lcom/evernote/edam/type/User;->privilege:Lcom/evernote/edam/type/PrivilegeLevel;

    if-eqz v0, :cond_5

    .line 1058
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetPrivilege()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1059
    sget-object v0, Lcom/evernote/edam/type/User;->PRIVILEGE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1060
    iget-object v0, p0, Lcom/evernote/edam/type/User;->privilege:Lcom/evernote/edam/type/PrivilegeLevel;

    invoke-virtual {v0}, Lcom/evernote/edam/type/PrivilegeLevel;->getValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeI32(I)V

    .line 1061
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1064
    :cond_5
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetCreated()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1065
    sget-object v0, Lcom/evernote/edam/type/User;->CREATED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1066
    iget-wide v0, p0, Lcom/evernote/edam/type/User;->created:J

    invoke-virtual {p1, v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeI64(J)V

    .line 1067
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1069
    :cond_6
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetUpdated()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1070
    sget-object v0, Lcom/evernote/edam/type/User;->UPDATED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1071
    iget-wide v0, p0, Lcom/evernote/edam/type/User;->updated:J

    invoke-virtual {p1, v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeI64(J)V

    .line 1072
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1074
    :cond_7
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetDeleted()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1075
    sget-object v0, Lcom/evernote/edam/type/User;->DELETED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1076
    iget-wide v0, p0, Lcom/evernote/edam/type/User;->deleted:J

    invoke-virtual {p1, v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeI64(J)V

    .line 1077
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1079
    :cond_8
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetActive()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1080
    sget-object v0, Lcom/evernote/edam/type/User;->ACTIVE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1081
    iget-boolean v0, p0, Lcom/evernote/edam/type/User;->active:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 1082
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1084
    :cond_9
    iget-object v0, p0, Lcom/evernote/edam/type/User;->shardId:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 1085
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetShardId()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1086
    sget-object v0, Lcom/evernote/edam/type/User;->SHARD_ID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1087
    iget-object v0, p0, Lcom/evernote/edam/type/User;->shardId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 1088
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1091
    :cond_a
    iget-object v0, p0, Lcom/evernote/edam/type/User;->attributes:Lcom/evernote/edam/type/UserAttributes;

    if-eqz v0, :cond_b

    .line 1092
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetAttributes()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1093
    sget-object v0, Lcom/evernote/edam/type/User;->ATTRIBUTES_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1094
    iget-object v0, p0, Lcom/evernote/edam/type/User;->attributes:Lcom/evernote/edam/type/UserAttributes;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/type/UserAttributes;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1095
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1098
    :cond_b
    iget-object v0, p0, Lcom/evernote/edam/type/User;->accounting:Lcom/evernote/edam/type/Accounting;

    if-eqz v0, :cond_c

    .line 1099
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetAccounting()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1100
    sget-object v0, Lcom/evernote/edam/type/User;->ACCOUNTING_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1101
    iget-object v0, p0, Lcom/evernote/edam/type/User;->accounting:Lcom/evernote/edam/type/Accounting;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/type/Accounting;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1102
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1105
    :cond_c
    iget-object v0, p0, Lcom/evernote/edam/type/User;->premiumInfo:Lcom/evernote/edam/type/PremiumInfo;

    if-eqz v0, :cond_d

    .line 1106
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetPremiumInfo()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1107
    sget-object v0, Lcom/evernote/edam/type/User;->PREMIUM_INFO_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1108
    iget-object v0, p0, Lcom/evernote/edam/type/User;->premiumInfo:Lcom/evernote/edam/type/PremiumInfo;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/type/PremiumInfo;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1109
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1112
    :cond_d
    iget-object v0, p0, Lcom/evernote/edam/type/User;->businessUserInfo:Lcom/evernote/edam/type/BusinessUserInfo;

    if-eqz v0, :cond_e

    .line 1113
    invoke-virtual {p0}, Lcom/evernote/edam/type/User;->isSetBusinessUserInfo()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1114
    sget-object v0, Lcom/evernote/edam/type/User;->BUSINESS_USER_INFO_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1115
    iget-object v0, p0, Lcom/evernote/edam/type/User;->businessUserInfo:Lcom/evernote/edam/type/BusinessUserInfo;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/type/BusinessUserInfo;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1116
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1119
    :cond_e
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldStop()V

    .line 1120
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeStructEnd()V

    return-void
.end method

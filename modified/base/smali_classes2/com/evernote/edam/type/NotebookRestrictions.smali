.class public Lcom/evernote/edam/type/NotebookRestrictions;
.super Ljava/lang/Object;
.source "NotebookRestrictions.java"

# interfaces
.implements Lcom/evernote/thrift/TBase;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/evernote/thrift/TBase<",
        "Lcom/evernote/edam/type/NotebookRestrictions;",
        ">;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# static fields
.field private static final EXPUNGE_WHICH_SHARED_NOTEBOOK_RESTRICTIONS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final NO_CREATE_NOTES_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final NO_CREATE_SHARED_NOTEBOOKS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final NO_CREATE_TAGS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final NO_EMAIL_NOTES_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final NO_EXPUNGE_NOTEBOOK_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final NO_EXPUNGE_NOTES_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final NO_EXPUNGE_TAGS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final NO_PUBLISH_TO_BUSINESS_LIBRARY_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final NO_PUBLISH_TO_PUBLIC_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final NO_READ_NOTES_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final NO_SEND_MESSAGE_TO_RECIPIENTS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final NO_SET_DEFAULT_NOTEBOOK_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final NO_SET_NOTEBOOK_STACK_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final NO_SET_PARENT_TAG_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final NO_SHARE_NOTES_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final NO_UPDATE_NOTEBOOK_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final NO_UPDATE_NOTES_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final NO_UPDATE_TAGS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

.field private static final UPDATE_WHICH_SHARED_NOTEBOOK_RESTRICTIONS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final __NOCREATENOTES_ISSET_ID:I = 0x1

.field private static final __NOCREATESHAREDNOTEBOOKS_ISSET_ID:I = 0x11

.field private static final __NOCREATETAGS_ISSET_ID:I = 0xd

.field private static final __NOEMAILNOTES_ISSET_ID:I = 0x5

.field private static final __NOEXPUNGENOTEBOOK_ISSET_ID:I = 0x8

.field private static final __NOEXPUNGENOTES_ISSET_ID:I = 0x3

.field private static final __NOEXPUNGETAGS_ISSET_ID:I = 0xf

.field private static final __NOPUBLISHTOBUSINESSLIBRARY_ISSET_ID:I = 0xc

.field private static final __NOPUBLISHTOPUBLIC_ISSET_ID:I = 0xb

.field private static final __NOREADNOTES_ISSET_ID:I = 0x0

.field private static final __NOSENDMESSAGETORECIPIENTS_ISSET_ID:I = 0x6

.field private static final __NOSETDEFAULTNOTEBOOK_ISSET_ID:I = 0x9

.field private static final __NOSETNOTEBOOKSTACK_ISSET_ID:I = 0xa

.field private static final __NOSETPARENTTAG_ISSET_ID:I = 0x10

.field private static final __NOSHARENOTES_ISSET_ID:I = 0x4

.field private static final __NOUPDATENOTEBOOK_ISSET_ID:I = 0x7

.field private static final __NOUPDATENOTES_ISSET_ID:I = 0x2

.field private static final __NOUPDATETAGS_ISSET_ID:I = 0xe


# instance fields
.field private __isset_vector:[Z

.field private expungeWhichSharedNotebookRestrictions:Lcom/evernote/edam/type/SharedNotebookInstanceRestrictions;

.field private noCreateNotes:Z

.field private noCreateSharedNotebooks:Z

.field private noCreateTags:Z

.field private noEmailNotes:Z

.field private noExpungeNotebook:Z

.field private noExpungeNotes:Z

.field private noExpungeTags:Z

.field private noPublishToBusinessLibrary:Z

.field private noPublishToPublic:Z

.field private noReadNotes:Z

.field private noSendMessageToRecipients:Z

.field private noSetDefaultNotebook:Z

.field private noSetNotebookStack:Z

.field private noSetParentTag:Z

.field private noShareNotes:Z

.field private noUpdateNotebook:Z

.field private noUpdateNotes:Z

.field private noUpdateTags:Z

.field private updateWhichSharedNotebookRestrictions:Lcom/evernote/edam/type/SharedNotebookInstanceRestrictions;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 122
    new-instance v0, Lcom/evernote/thrift/protocol/TStruct;

    const-string v1, "NotebookRestrictions"

    invoke-direct {v0, v1}, Lcom/evernote/thrift/protocol/TStruct;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/evernote/edam/type/NotebookRestrictions;->STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

    .line 124
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "noReadNotes"

    const/4 v2, 0x2

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/NotebookRestrictions;->NO_READ_NOTES_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 125
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "noCreateNotes"

    invoke-direct {v0, v1, v2, v2}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/NotebookRestrictions;->NO_CREATE_NOTES_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 126
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "noUpdateNotes"

    const/4 v3, 0x3

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/NotebookRestrictions;->NO_UPDATE_NOTES_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 127
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "noExpungeNotes"

    const/4 v3, 0x4

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/NotebookRestrictions;->NO_EXPUNGE_NOTES_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 128
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "noShareNotes"

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/NotebookRestrictions;->NO_SHARE_NOTES_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 129
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "noEmailNotes"

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/NotebookRestrictions;->NO_EMAIL_NOTES_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 130
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "noSendMessageToRecipients"

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/NotebookRestrictions;->NO_SEND_MESSAGE_TO_RECIPIENTS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 131
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "noUpdateNotebook"

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/NotebookRestrictions;->NO_UPDATE_NOTEBOOK_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 132
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "noExpungeNotebook"

    const/16 v4, 0x9

    invoke-direct {v0, v1, v2, v4}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/NotebookRestrictions;->NO_EXPUNGE_NOTEBOOK_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 133
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "noSetDefaultNotebook"

    const/16 v4, 0xa

    invoke-direct {v0, v1, v2, v4}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/NotebookRestrictions;->NO_SET_DEFAULT_NOTEBOOK_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 134
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "noSetNotebookStack"

    const/16 v4, 0xb

    invoke-direct {v0, v1, v2, v4}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/NotebookRestrictions;->NO_SET_NOTEBOOK_STACK_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 135
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "noPublishToPublic"

    const/16 v4, 0xc

    invoke-direct {v0, v1, v2, v4}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/NotebookRestrictions;->NO_PUBLISH_TO_PUBLIC_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 136
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "noPublishToBusinessLibrary"

    const/16 v4, 0xd

    invoke-direct {v0, v1, v2, v4}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/NotebookRestrictions;->NO_PUBLISH_TO_BUSINESS_LIBRARY_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 137
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "noCreateTags"

    const/16 v4, 0xe

    invoke-direct {v0, v1, v2, v4}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/NotebookRestrictions;->NO_CREATE_TAGS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 138
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "noUpdateTags"

    const/16 v4, 0xf

    invoke-direct {v0, v1, v2, v4}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/NotebookRestrictions;->NO_UPDATE_TAGS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 139
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "noExpungeTags"

    const/16 v4, 0x10

    invoke-direct {v0, v1, v2, v4}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/NotebookRestrictions;->NO_EXPUNGE_TAGS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 140
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "noSetParentTag"

    const/16 v4, 0x11

    invoke-direct {v0, v1, v2, v4}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/NotebookRestrictions;->NO_SET_PARENT_TAG_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 141
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "noCreateSharedNotebooks"

    const/16 v4, 0x12

    invoke-direct {v0, v1, v2, v4}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/NotebookRestrictions;->NO_CREATE_SHARED_NOTEBOOKS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 142
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "updateWhichSharedNotebookRestrictions"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v3, v2}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/NotebookRestrictions;->UPDATE_WHICH_SHARED_NOTEBOOK_RESTRICTIONS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 143
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "expungeWhichSharedNotebookRestrictions"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v3, v2}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/NotebookRestrictions;->EXPUNGE_WHICH_SHARED_NOTEBOOK_RESTRICTIONS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 188
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x12

    .line 186
    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    return-void
.end method

.method public constructor <init>(Lcom/evernote/edam/type/NotebookRestrictions;)V
    .locals 4

    .line 194
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x12

    .line 186
    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    .line 195
    iget-object v0, p1, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    iget-object v1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    array-length v2, v0

    const/4 v3, 0x0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 196
    iget-boolean v0, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noReadNotes:Z

    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noReadNotes:Z

    .line 197
    iget-boolean v0, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noCreateNotes:Z

    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noCreateNotes:Z

    .line 198
    iget-boolean v0, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noUpdateNotes:Z

    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noUpdateNotes:Z

    .line 199
    iget-boolean v0, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noExpungeNotes:Z

    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noExpungeNotes:Z

    .line 200
    iget-boolean v0, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noShareNotes:Z

    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noShareNotes:Z

    .line 201
    iget-boolean v0, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noEmailNotes:Z

    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noEmailNotes:Z

    .line 202
    iget-boolean v0, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noSendMessageToRecipients:Z

    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noSendMessageToRecipients:Z

    .line 203
    iget-boolean v0, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noUpdateNotebook:Z

    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noUpdateNotebook:Z

    .line 204
    iget-boolean v0, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noExpungeNotebook:Z

    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noExpungeNotebook:Z

    .line 205
    iget-boolean v0, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noSetDefaultNotebook:Z

    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noSetDefaultNotebook:Z

    .line 206
    iget-boolean v0, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noSetNotebookStack:Z

    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noSetNotebookStack:Z

    .line 207
    iget-boolean v0, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noPublishToPublic:Z

    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noPublishToPublic:Z

    .line 208
    iget-boolean v0, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noPublishToBusinessLibrary:Z

    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noPublishToBusinessLibrary:Z

    .line 209
    iget-boolean v0, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noCreateTags:Z

    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noCreateTags:Z

    .line 210
    iget-boolean v0, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noUpdateTags:Z

    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noUpdateTags:Z

    .line 211
    iget-boolean v0, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noExpungeTags:Z

    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noExpungeTags:Z

    .line 212
    iget-boolean v0, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noSetParentTag:Z

    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noSetParentTag:Z

    .line 213
    iget-boolean v0, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noCreateSharedNotebooks:Z

    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noCreateSharedNotebooks:Z

    .line 214
    invoke-virtual {p1}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetUpdateWhichSharedNotebookRestrictions()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 215
    iget-object v0, p1, Lcom/evernote/edam/type/NotebookRestrictions;->updateWhichSharedNotebookRestrictions:Lcom/evernote/edam/type/SharedNotebookInstanceRestrictions;

    iput-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->updateWhichSharedNotebookRestrictions:Lcom/evernote/edam/type/SharedNotebookInstanceRestrictions;

    .line 217
    :cond_0
    invoke-virtual {p1}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetExpungeWhichSharedNotebookRestrictions()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 218
    iget-object p1, p1, Lcom/evernote/edam/type/NotebookRestrictions;->expungeWhichSharedNotebookRestrictions:Lcom/evernote/edam/type/SharedNotebookInstanceRestrictions;

    iput-object p1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->expungeWhichSharedNotebookRestrictions:Lcom/evernote/edam/type/SharedNotebookInstanceRestrictions;

    :cond_1
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    const/4 v0, 0x0

    .line 227
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoReadNotesIsSet(Z)V

    .line 228
    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noReadNotes:Z

    .line 229
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoCreateNotesIsSet(Z)V

    .line 230
    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noCreateNotes:Z

    .line 231
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoUpdateNotesIsSet(Z)V

    .line 232
    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noUpdateNotes:Z

    .line 233
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoExpungeNotesIsSet(Z)V

    .line 234
    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noExpungeNotes:Z

    .line 235
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoShareNotesIsSet(Z)V

    .line 236
    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noShareNotes:Z

    .line 237
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoEmailNotesIsSet(Z)V

    .line 238
    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noEmailNotes:Z

    .line 239
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoSendMessageToRecipientsIsSet(Z)V

    .line 240
    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noSendMessageToRecipients:Z

    .line 241
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoUpdateNotebookIsSet(Z)V

    .line 242
    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noUpdateNotebook:Z

    .line 243
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoExpungeNotebookIsSet(Z)V

    .line 244
    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noExpungeNotebook:Z

    .line 245
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoSetDefaultNotebookIsSet(Z)V

    .line 246
    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noSetDefaultNotebook:Z

    .line 247
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoSetNotebookStackIsSet(Z)V

    .line 248
    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noSetNotebookStack:Z

    .line 249
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoPublishToPublicIsSet(Z)V

    .line 250
    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noPublishToPublic:Z

    .line 251
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoPublishToBusinessLibraryIsSet(Z)V

    .line 252
    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noPublishToBusinessLibrary:Z

    .line 253
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoCreateTagsIsSet(Z)V

    .line 254
    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noCreateTags:Z

    .line 255
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoUpdateTagsIsSet(Z)V

    .line 256
    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noUpdateTags:Z

    .line 257
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoExpungeTagsIsSet(Z)V

    .line 258
    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noExpungeTags:Z

    .line 259
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoSetParentTagIsSet(Z)V

    .line 260
    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noSetParentTag:Z

    .line 261
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoCreateSharedNotebooksIsSet(Z)V

    .line 262
    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noCreateSharedNotebooks:Z

    const/4 v0, 0x0

    .line 263
    iput-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->updateWhichSharedNotebookRestrictions:Lcom/evernote/edam/type/SharedNotebookInstanceRestrictions;

    .line 264
    iput-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->expungeWhichSharedNotebookRestrictions:Lcom/evernote/edam/type/SharedNotebookInstanceRestrictions;

    return-void
.end method

.method public compareTo(Lcom/evernote/edam/type/NotebookRestrictions;)I
    .locals 2

    .line 927
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 928
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    return p1

    .line 934
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoReadNotes()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoReadNotes()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_1

    return v0

    .line 938
    :cond_1
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoReadNotes()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noReadNotes:Z

    iget-boolean v1, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noReadNotes:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_2

    return v0

    .line 943
    :cond_2
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoCreateNotes()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoCreateNotes()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_3

    return v0

    .line 947
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoCreateNotes()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noCreateNotes:Z

    iget-boolean v1, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noCreateNotes:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_4

    return v0

    .line 952
    :cond_4
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoUpdateNotes()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoUpdateNotes()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_5

    return v0

    .line 956
    :cond_5
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoUpdateNotes()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noUpdateNotes:Z

    iget-boolean v1, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noUpdateNotes:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_6

    return v0

    .line 961
    :cond_6
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoExpungeNotes()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoExpungeNotes()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_7

    return v0

    .line 965
    :cond_7
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoExpungeNotes()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noExpungeNotes:Z

    iget-boolean v1, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noExpungeNotes:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_8

    return v0

    .line 970
    :cond_8
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoShareNotes()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoShareNotes()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_9

    return v0

    .line 974
    :cond_9
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoShareNotes()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noShareNotes:Z

    iget-boolean v1, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noShareNotes:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_a

    return v0

    .line 979
    :cond_a
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoEmailNotes()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoEmailNotes()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_b

    return v0

    .line 983
    :cond_b
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoEmailNotes()Z

    move-result v0

    if-eqz v0, :cond_c

    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noEmailNotes:Z

    iget-boolean v1, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noEmailNotes:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_c

    return v0

    .line 988
    :cond_c
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoSendMessageToRecipients()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoSendMessageToRecipients()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_d

    return v0

    .line 992
    :cond_d
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoSendMessageToRecipients()Z

    move-result v0

    if-eqz v0, :cond_e

    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noSendMessageToRecipients:Z

    iget-boolean v1, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noSendMessageToRecipients:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_e

    return v0

    .line 997
    :cond_e
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoUpdateNotebook()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoUpdateNotebook()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_f

    return v0

    .line 1001
    :cond_f
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoUpdateNotebook()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noUpdateNotebook:Z

    iget-boolean v1, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noUpdateNotebook:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_10

    return v0

    .line 1006
    :cond_10
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoExpungeNotebook()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoExpungeNotebook()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_11

    return v0

    .line 1010
    :cond_11
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoExpungeNotebook()Z

    move-result v0

    if-eqz v0, :cond_12

    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noExpungeNotebook:Z

    iget-boolean v1, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noExpungeNotebook:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_12

    return v0

    .line 1015
    :cond_12
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoSetDefaultNotebook()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoSetDefaultNotebook()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_13

    return v0

    .line 1019
    :cond_13
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoSetDefaultNotebook()Z

    move-result v0

    if-eqz v0, :cond_14

    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noSetDefaultNotebook:Z

    iget-boolean v1, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noSetDefaultNotebook:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_14

    return v0

    .line 1024
    :cond_14
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoSetNotebookStack()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoSetNotebookStack()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_15

    return v0

    .line 1028
    :cond_15
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoSetNotebookStack()Z

    move-result v0

    if-eqz v0, :cond_16

    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noSetNotebookStack:Z

    iget-boolean v1, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noSetNotebookStack:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_16

    return v0

    .line 1033
    :cond_16
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoPublishToPublic()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoPublishToPublic()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_17

    return v0

    .line 1037
    :cond_17
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoPublishToPublic()Z

    move-result v0

    if-eqz v0, :cond_18

    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noPublishToPublic:Z

    iget-boolean v1, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noPublishToPublic:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_18

    return v0

    .line 1042
    :cond_18
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoPublishToBusinessLibrary()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoPublishToBusinessLibrary()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_19

    return v0

    .line 1046
    :cond_19
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoPublishToBusinessLibrary()Z

    move-result v0

    if-eqz v0, :cond_1a

    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noPublishToBusinessLibrary:Z

    iget-boolean v1, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noPublishToBusinessLibrary:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_1a

    return v0

    .line 1051
    :cond_1a
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoCreateTags()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoCreateTags()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_1b

    return v0

    .line 1055
    :cond_1b
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoCreateTags()Z

    move-result v0

    if-eqz v0, :cond_1c

    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noCreateTags:Z

    iget-boolean v1, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noCreateTags:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_1c

    return v0

    .line 1060
    :cond_1c
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoUpdateTags()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoUpdateTags()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_1d

    return v0

    .line 1064
    :cond_1d
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoUpdateTags()Z

    move-result v0

    if-eqz v0, :cond_1e

    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noUpdateTags:Z

    iget-boolean v1, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noUpdateTags:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_1e

    return v0

    .line 1069
    :cond_1e
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoExpungeTags()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoExpungeTags()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_1f

    return v0

    .line 1073
    :cond_1f
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoExpungeTags()Z

    move-result v0

    if-eqz v0, :cond_20

    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noExpungeTags:Z

    iget-boolean v1, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noExpungeTags:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_20

    return v0

    .line 1078
    :cond_20
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoSetParentTag()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoSetParentTag()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_21

    return v0

    .line 1082
    :cond_21
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoSetParentTag()Z

    move-result v0

    if-eqz v0, :cond_22

    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noSetParentTag:Z

    iget-boolean v1, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noSetParentTag:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_22

    return v0

    .line 1087
    :cond_22
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoCreateSharedNotebooks()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoCreateSharedNotebooks()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_23

    return v0

    .line 1091
    :cond_23
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoCreateSharedNotebooks()Z

    move-result v0

    if-eqz v0, :cond_24

    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noCreateSharedNotebooks:Z

    iget-boolean v1, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noCreateSharedNotebooks:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_24

    return v0

    .line 1096
    :cond_24
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetUpdateWhichSharedNotebookRestrictions()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetUpdateWhichSharedNotebookRestrictions()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_25

    return v0

    .line 1100
    :cond_25
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetUpdateWhichSharedNotebookRestrictions()Z

    move-result v0

    if-eqz v0, :cond_26

    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->updateWhichSharedNotebookRestrictions:Lcom/evernote/edam/type/SharedNotebookInstanceRestrictions;

    iget-object v1, p1, Lcom/evernote/edam/type/NotebookRestrictions;->updateWhichSharedNotebookRestrictions:Lcom/evernote/edam/type/SharedNotebookInstanceRestrictions;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result v0

    if-eqz v0, :cond_26

    return v0

    .line 1105
    :cond_26
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetExpungeWhichSharedNotebookRestrictions()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetExpungeWhichSharedNotebookRestrictions()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_27

    return v0

    .line 1109
    :cond_27
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetExpungeWhichSharedNotebookRestrictions()Z

    move-result v0

    if-eqz v0, :cond_28

    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->expungeWhichSharedNotebookRestrictions:Lcom/evernote/edam/type/SharedNotebookInstanceRestrictions;

    iget-object p1, p1, Lcom/evernote/edam/type/NotebookRestrictions;->expungeWhichSharedNotebookRestrictions:Lcom/evernote/edam/type/SharedNotebookInstanceRestrictions;

    invoke-static {v0, p1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result p1

    if-eqz p1, :cond_28

    return p1

    :cond_28
    const/4 p1, 0x0

    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 121
    check-cast p1, Lcom/evernote/edam/type/NotebookRestrictions;

    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/NotebookRestrictions;->compareTo(Lcom/evernote/edam/type/NotebookRestrictions;)I

    move-result p1

    return p1
.end method

.method public deepCopy()Lcom/evernote/edam/type/NotebookRestrictions;
    .locals 1

    .line 223
    new-instance v0, Lcom/evernote/edam/type/NotebookRestrictions;

    invoke-direct {v0, p0}, Lcom/evernote/edam/type/NotebookRestrictions;-><init>(Lcom/evernote/edam/type/NotebookRestrictions;)V

    return-object v0
.end method

.method public bridge synthetic deepCopy()Lcom/evernote/thrift/TBase;
    .locals 1

    .line 121
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->deepCopy()Lcom/evernote/edam/type/NotebookRestrictions;

    move-result-object v0

    return-object v0
.end method

.method public equals(Lcom/evernote/edam/type/NotebookRestrictions;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 738
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoReadNotes()Z

    move-result v1

    .line 739
    invoke-virtual {p1}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoReadNotes()Z

    move-result v2

    if-nez v1, :cond_1

    if-eqz v2, :cond_3

    :cond_1
    if-eqz v1, :cond_50

    if-nez v2, :cond_2

    goto/16 :goto_13

    .line 743
    :cond_2
    iget-boolean v1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noReadNotes:Z

    iget-boolean v2, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noReadNotes:Z

    if-eq v1, v2, :cond_3

    return v0

    .line 747
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoCreateNotes()Z

    move-result v1

    .line 748
    invoke-virtual {p1}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoCreateNotes()Z

    move-result v2

    if-nez v1, :cond_4

    if-eqz v2, :cond_6

    :cond_4
    if-eqz v1, :cond_4f

    if-nez v2, :cond_5

    goto/16 :goto_12

    .line 752
    :cond_5
    iget-boolean v1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noCreateNotes:Z

    iget-boolean v2, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noCreateNotes:Z

    if-eq v1, v2, :cond_6

    return v0

    .line 756
    :cond_6
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoUpdateNotes()Z

    move-result v1

    .line 757
    invoke-virtual {p1}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoUpdateNotes()Z

    move-result v2

    if-nez v1, :cond_7

    if-eqz v2, :cond_9

    :cond_7
    if-eqz v1, :cond_4e

    if-nez v2, :cond_8

    goto/16 :goto_11

    .line 761
    :cond_8
    iget-boolean v1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noUpdateNotes:Z

    iget-boolean v2, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noUpdateNotes:Z

    if-eq v1, v2, :cond_9

    return v0

    .line 765
    :cond_9
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoExpungeNotes()Z

    move-result v1

    .line 766
    invoke-virtual {p1}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoExpungeNotes()Z

    move-result v2

    if-nez v1, :cond_a

    if-eqz v2, :cond_c

    :cond_a
    if-eqz v1, :cond_4d

    if-nez v2, :cond_b

    goto/16 :goto_10

    .line 770
    :cond_b
    iget-boolean v1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noExpungeNotes:Z

    iget-boolean v2, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noExpungeNotes:Z

    if-eq v1, v2, :cond_c

    return v0

    .line 774
    :cond_c
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoShareNotes()Z

    move-result v1

    .line 775
    invoke-virtual {p1}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoShareNotes()Z

    move-result v2

    if-nez v1, :cond_d

    if-eqz v2, :cond_f

    :cond_d
    if-eqz v1, :cond_4c

    if-nez v2, :cond_e

    goto/16 :goto_f

    .line 779
    :cond_e
    iget-boolean v1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noShareNotes:Z

    iget-boolean v2, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noShareNotes:Z

    if-eq v1, v2, :cond_f

    return v0

    .line 783
    :cond_f
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoEmailNotes()Z

    move-result v1

    .line 784
    invoke-virtual {p1}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoEmailNotes()Z

    move-result v2

    if-nez v1, :cond_10

    if-eqz v2, :cond_12

    :cond_10
    if-eqz v1, :cond_4b

    if-nez v2, :cond_11

    goto/16 :goto_e

    .line 788
    :cond_11
    iget-boolean v1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noEmailNotes:Z

    iget-boolean v2, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noEmailNotes:Z

    if-eq v1, v2, :cond_12

    return v0

    .line 792
    :cond_12
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoSendMessageToRecipients()Z

    move-result v1

    .line 793
    invoke-virtual {p1}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoSendMessageToRecipients()Z

    move-result v2

    if-nez v1, :cond_13

    if-eqz v2, :cond_15

    :cond_13
    if-eqz v1, :cond_4a

    if-nez v2, :cond_14

    goto/16 :goto_d

    .line 797
    :cond_14
    iget-boolean v1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noSendMessageToRecipients:Z

    iget-boolean v2, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noSendMessageToRecipients:Z

    if-eq v1, v2, :cond_15

    return v0

    .line 801
    :cond_15
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoUpdateNotebook()Z

    move-result v1

    .line 802
    invoke-virtual {p1}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoUpdateNotebook()Z

    move-result v2

    if-nez v1, :cond_16

    if-eqz v2, :cond_18

    :cond_16
    if-eqz v1, :cond_49

    if-nez v2, :cond_17

    goto/16 :goto_c

    .line 806
    :cond_17
    iget-boolean v1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noUpdateNotebook:Z

    iget-boolean v2, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noUpdateNotebook:Z

    if-eq v1, v2, :cond_18

    return v0

    .line 810
    :cond_18
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoExpungeNotebook()Z

    move-result v1

    .line 811
    invoke-virtual {p1}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoExpungeNotebook()Z

    move-result v2

    if-nez v1, :cond_19

    if-eqz v2, :cond_1b

    :cond_19
    if-eqz v1, :cond_48

    if-nez v2, :cond_1a

    goto/16 :goto_b

    .line 815
    :cond_1a
    iget-boolean v1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noExpungeNotebook:Z

    iget-boolean v2, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noExpungeNotebook:Z

    if-eq v1, v2, :cond_1b

    return v0

    .line 819
    :cond_1b
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoSetDefaultNotebook()Z

    move-result v1

    .line 820
    invoke-virtual {p1}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoSetDefaultNotebook()Z

    move-result v2

    if-nez v1, :cond_1c

    if-eqz v2, :cond_1e

    :cond_1c
    if-eqz v1, :cond_47

    if-nez v2, :cond_1d

    goto/16 :goto_a

    .line 824
    :cond_1d
    iget-boolean v1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noSetDefaultNotebook:Z

    iget-boolean v2, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noSetDefaultNotebook:Z

    if-eq v1, v2, :cond_1e

    return v0

    .line 828
    :cond_1e
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoSetNotebookStack()Z

    move-result v1

    .line 829
    invoke-virtual {p1}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoSetNotebookStack()Z

    move-result v2

    if-nez v1, :cond_1f

    if-eqz v2, :cond_21

    :cond_1f
    if-eqz v1, :cond_46

    if-nez v2, :cond_20

    goto/16 :goto_9

    .line 833
    :cond_20
    iget-boolean v1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noSetNotebookStack:Z

    iget-boolean v2, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noSetNotebookStack:Z

    if-eq v1, v2, :cond_21

    return v0

    .line 837
    :cond_21
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoPublishToPublic()Z

    move-result v1

    .line 838
    invoke-virtual {p1}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoPublishToPublic()Z

    move-result v2

    if-nez v1, :cond_22

    if-eqz v2, :cond_24

    :cond_22
    if-eqz v1, :cond_45

    if-nez v2, :cond_23

    goto/16 :goto_8

    .line 842
    :cond_23
    iget-boolean v1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noPublishToPublic:Z

    iget-boolean v2, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noPublishToPublic:Z

    if-eq v1, v2, :cond_24

    return v0

    .line 846
    :cond_24
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoPublishToBusinessLibrary()Z

    move-result v1

    .line 847
    invoke-virtual {p1}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoPublishToBusinessLibrary()Z

    move-result v2

    if-nez v1, :cond_25

    if-eqz v2, :cond_27

    :cond_25
    if-eqz v1, :cond_44

    if-nez v2, :cond_26

    goto/16 :goto_7

    .line 851
    :cond_26
    iget-boolean v1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noPublishToBusinessLibrary:Z

    iget-boolean v2, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noPublishToBusinessLibrary:Z

    if-eq v1, v2, :cond_27

    return v0

    .line 855
    :cond_27
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoCreateTags()Z

    move-result v1

    .line 856
    invoke-virtual {p1}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoCreateTags()Z

    move-result v2

    if-nez v1, :cond_28

    if-eqz v2, :cond_2a

    :cond_28
    if-eqz v1, :cond_43

    if-nez v2, :cond_29

    goto/16 :goto_6

    .line 860
    :cond_29
    iget-boolean v1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noCreateTags:Z

    iget-boolean v2, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noCreateTags:Z

    if-eq v1, v2, :cond_2a

    return v0

    .line 864
    :cond_2a
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoUpdateTags()Z

    move-result v1

    .line 865
    invoke-virtual {p1}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoUpdateTags()Z

    move-result v2

    if-nez v1, :cond_2b

    if-eqz v2, :cond_2d

    :cond_2b
    if-eqz v1, :cond_42

    if-nez v2, :cond_2c

    goto/16 :goto_5

    .line 869
    :cond_2c
    iget-boolean v1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noUpdateTags:Z

    iget-boolean v2, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noUpdateTags:Z

    if-eq v1, v2, :cond_2d

    return v0

    .line 873
    :cond_2d
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoExpungeTags()Z

    move-result v1

    .line 874
    invoke-virtual {p1}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoExpungeTags()Z

    move-result v2

    if-nez v1, :cond_2e

    if-eqz v2, :cond_30

    :cond_2e
    if-eqz v1, :cond_41

    if-nez v2, :cond_2f

    goto/16 :goto_4

    .line 878
    :cond_2f
    iget-boolean v1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noExpungeTags:Z

    iget-boolean v2, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noExpungeTags:Z

    if-eq v1, v2, :cond_30

    return v0

    .line 882
    :cond_30
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoSetParentTag()Z

    move-result v1

    .line 883
    invoke-virtual {p1}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoSetParentTag()Z

    move-result v2

    if-nez v1, :cond_31

    if-eqz v2, :cond_33

    :cond_31
    if-eqz v1, :cond_40

    if-nez v2, :cond_32

    goto :goto_3

    .line 887
    :cond_32
    iget-boolean v1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noSetParentTag:Z

    iget-boolean v2, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noSetParentTag:Z

    if-eq v1, v2, :cond_33

    return v0

    .line 891
    :cond_33
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoCreateSharedNotebooks()Z

    move-result v1

    .line 892
    invoke-virtual {p1}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoCreateSharedNotebooks()Z

    move-result v2

    if-nez v1, :cond_34

    if-eqz v2, :cond_36

    :cond_34
    if-eqz v1, :cond_3f

    if-nez v2, :cond_35

    goto :goto_2

    .line 896
    :cond_35
    iget-boolean v1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noCreateSharedNotebooks:Z

    iget-boolean v2, p1, Lcom/evernote/edam/type/NotebookRestrictions;->noCreateSharedNotebooks:Z

    if-eq v1, v2, :cond_36

    return v0

    .line 900
    :cond_36
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetUpdateWhichSharedNotebookRestrictions()Z

    move-result v1

    .line 901
    invoke-virtual {p1}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetUpdateWhichSharedNotebookRestrictions()Z

    move-result v2

    if-nez v1, :cond_37

    if-eqz v2, :cond_39

    :cond_37
    if-eqz v1, :cond_3e

    if-nez v2, :cond_38

    goto :goto_1

    .line 905
    :cond_38
    iget-object v1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->updateWhichSharedNotebookRestrictions:Lcom/evernote/edam/type/SharedNotebookInstanceRestrictions;

    iget-object v2, p1, Lcom/evernote/edam/type/NotebookRestrictions;->updateWhichSharedNotebookRestrictions:Lcom/evernote/edam/type/SharedNotebookInstanceRestrictions;

    invoke-virtual {v1, v2}, Lcom/evernote/edam/type/SharedNotebookInstanceRestrictions;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_39

    return v0

    .line 909
    :cond_39
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetExpungeWhichSharedNotebookRestrictions()Z

    move-result v1

    .line 910
    invoke-virtual {p1}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetExpungeWhichSharedNotebookRestrictions()Z

    move-result v2

    if-nez v1, :cond_3a

    if-eqz v2, :cond_3c

    :cond_3a
    if-eqz v1, :cond_3d

    if-nez v2, :cond_3b

    goto :goto_0

    .line 914
    :cond_3b
    iget-object v1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->expungeWhichSharedNotebookRestrictions:Lcom/evernote/edam/type/SharedNotebookInstanceRestrictions;

    iget-object p1, p1, Lcom/evernote/edam/type/NotebookRestrictions;->expungeWhichSharedNotebookRestrictions:Lcom/evernote/edam/type/SharedNotebookInstanceRestrictions;

    invoke-virtual {v1, p1}, Lcom/evernote/edam/type/SharedNotebookInstanceRestrictions;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_3c

    return v0

    :cond_3c
    const/4 p1, 0x1

    return p1

    :cond_3d
    :goto_0
    return v0

    :cond_3e
    :goto_1
    return v0

    :cond_3f
    :goto_2
    return v0

    :cond_40
    :goto_3
    return v0

    :cond_41
    :goto_4
    return v0

    :cond_42
    :goto_5
    return v0

    :cond_43
    :goto_6
    return v0

    :cond_44
    :goto_7
    return v0

    :cond_45
    :goto_8
    return v0

    :cond_46
    :goto_9
    return v0

    :cond_47
    :goto_a
    return v0

    :cond_48
    :goto_b
    return v0

    :cond_49
    :goto_c
    return v0

    :cond_4a
    :goto_d
    return v0

    :cond_4b
    :goto_e
    return v0

    :cond_4c
    :goto_f
    return v0

    :cond_4d
    :goto_10
    return v0

    :cond_4e
    :goto_11
    return v0

    :cond_4f
    :goto_12
    return v0

    :cond_50
    :goto_13
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 729
    :cond_0
    instance-of v1, p1, Lcom/evernote/edam/type/NotebookRestrictions;

    if-eqz v1, :cond_1

    .line 730
    check-cast p1, Lcom/evernote/edam/type/NotebookRestrictions;

    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/NotebookRestrictions;->equals(Lcom/evernote/edam/type/NotebookRestrictions;)Z

    move-result p1

    return p1

    :cond_1
    return v0
.end method

.method public getExpungeWhichSharedNotebookRestrictions()Lcom/evernote/edam/type/SharedNotebookInstanceRestrictions;
    .locals 1

    .line 699
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->expungeWhichSharedNotebookRestrictions:Lcom/evernote/edam/type/SharedNotebookInstanceRestrictions;

    return-object v0
.end method

.method public getUpdateWhichSharedNotebookRestrictions()Lcom/evernote/edam/type/SharedNotebookInstanceRestrictions;
    .locals 1

    .line 668
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->updateWhichSharedNotebookRestrictions:Lcom/evernote/edam/type/SharedNotebookInstanceRestrictions;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isNoCreateNotes()Z
    .locals 1

    .line 290
    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noCreateNotes:Z

    return v0
.end method

.method public isNoCreateSharedNotebooks()Z
    .locals 1

    .line 642
    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noCreateSharedNotebooks:Z

    return v0
.end method

.method public isNoCreateTags()Z
    .locals 1

    .line 554
    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noCreateTags:Z

    return v0
.end method

.method public isNoEmailNotes()Z
    .locals 1

    .line 378
    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noEmailNotes:Z

    return v0
.end method

.method public isNoExpungeNotebook()Z
    .locals 1

    .line 444
    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noExpungeNotebook:Z

    return v0
.end method

.method public isNoExpungeNotes()Z
    .locals 1

    .line 334
    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noExpungeNotes:Z

    return v0
.end method

.method public isNoExpungeTags()Z
    .locals 1

    .line 598
    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noExpungeTags:Z

    return v0
.end method

.method public isNoPublishToBusinessLibrary()Z
    .locals 1

    .line 532
    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noPublishToBusinessLibrary:Z

    return v0
.end method

.method public isNoPublishToPublic()Z
    .locals 1

    .line 510
    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noPublishToPublic:Z

    return v0
.end method

.method public isNoReadNotes()Z
    .locals 1

    .line 268
    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noReadNotes:Z

    return v0
.end method

.method public isNoSendMessageToRecipients()Z
    .locals 1

    .line 400
    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noSendMessageToRecipients:Z

    return v0
.end method

.method public isNoSetDefaultNotebook()Z
    .locals 1

    .line 466
    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noSetDefaultNotebook:Z

    return v0
.end method

.method public isNoSetNotebookStack()Z
    .locals 1

    .line 488
    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noSetNotebookStack:Z

    return v0
.end method

.method public isNoSetParentTag()Z
    .locals 1

    .line 620
    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noSetParentTag:Z

    return v0
.end method

.method public isNoShareNotes()Z
    .locals 1

    .line 356
    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noShareNotes:Z

    return v0
.end method

.method public isNoUpdateNotebook()Z
    .locals 1

    .line 422
    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noUpdateNotebook:Z

    return v0
.end method

.method public isNoUpdateNotes()Z
    .locals 1

    .line 312
    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noUpdateNotes:Z

    return v0
.end method

.method public isNoUpdateTags()Z
    .locals 1

    .line 576
    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noUpdateTags:Z

    return v0
.end method

.method public isSetExpungeWhichSharedNotebookRestrictions()Z
    .locals 1

    .line 716
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->expungeWhichSharedNotebookRestrictions:Lcom/evernote/edam/type/SharedNotebookInstanceRestrictions;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetNoCreateNotes()Z
    .locals 2

    .line 304
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/4 v1, 0x1

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetNoCreateSharedNotebooks()Z
    .locals 2

    .line 656
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/16 v1, 0x11

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetNoCreateTags()Z
    .locals 2

    .line 568
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/16 v1, 0xd

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetNoEmailNotes()Z
    .locals 2

    .line 392
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/4 v1, 0x5

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetNoExpungeNotebook()Z
    .locals 2

    .line 458
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/16 v1, 0x8

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetNoExpungeNotes()Z
    .locals 2

    .line 348
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/4 v1, 0x3

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetNoExpungeTags()Z
    .locals 2

    .line 612
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/16 v1, 0xf

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetNoPublishToBusinessLibrary()Z
    .locals 2

    .line 546
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/16 v1, 0xc

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetNoPublishToPublic()Z
    .locals 2

    .line 524
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/16 v1, 0xb

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetNoReadNotes()Z
    .locals 2

    .line 282
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/4 v1, 0x0

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetNoSendMessageToRecipients()Z
    .locals 2

    .line 414
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/4 v1, 0x6

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetNoSetDefaultNotebook()Z
    .locals 2

    .line 480
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/16 v1, 0x9

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetNoSetNotebookStack()Z
    .locals 2

    .line 502
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/16 v1, 0xa

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetNoSetParentTag()Z
    .locals 2

    .line 634
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/16 v1, 0x10

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetNoShareNotes()Z
    .locals 2

    .line 370
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/4 v1, 0x4

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetNoUpdateNotebook()Z
    .locals 2

    .line 436
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/4 v1, 0x7

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetNoUpdateNotes()Z
    .locals 2

    .line 326
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/4 v1, 0x2

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetNoUpdateTags()Z
    .locals 2

    .line 590
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/16 v1, 0xe

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetUpdateWhichSharedNotebookRestrictions()Z
    .locals 1

    .line 685
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->updateWhichSharedNotebookRestrictions:Lcom/evernote/edam/type/SharedNotebookInstanceRestrictions;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public read(Lcom/evernote/thrift/protocol/TProtocol;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1119
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readStructBegin()Lcom/evernote/thrift/protocol/TStruct;

    .line 1122
    :goto_0
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readFieldBegin()Lcom/evernote/thrift/protocol/TField;

    move-result-object v0

    .line 1123
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-nez v1, :cond_0

    .line 1290
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readStructEnd()V

    .line 1291
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->validate()V

    return-void

    .line 1126
    :cond_0
    iget-short v1, v0, Lcom/evernote/thrift/protocol/TField;->id:S

    const/16 v2, 0x8

    const/4 v3, 0x1

    const/4 v4, 0x2

    packed-switch v1, :pswitch_data_0

    .line 1286
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 1279
    :pswitch_0
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v2, :cond_1

    .line 1280
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI32()I

    move-result v0

    invoke-static {v0}, Lcom/evernote/edam/type/SharedNotebookInstanceRestrictions;->findByValue(I)Lcom/evernote/edam/type/SharedNotebookInstanceRestrictions;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->expungeWhichSharedNotebookRestrictions:Lcom/evernote/edam/type/SharedNotebookInstanceRestrictions;

    goto/16 :goto_1

    .line 1282
    :cond_1
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 1272
    :pswitch_1
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v2, :cond_2

    .line 1273
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI32()I

    move-result v0

    invoke-static {v0}, Lcom/evernote/edam/type/SharedNotebookInstanceRestrictions;->findByValue(I)Lcom/evernote/edam/type/SharedNotebookInstanceRestrictions;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->updateWhichSharedNotebookRestrictions:Lcom/evernote/edam/type/SharedNotebookInstanceRestrictions;

    goto/16 :goto_1

    .line 1275
    :cond_2
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 1264
    :pswitch_2
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_3

    .line 1265
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noCreateSharedNotebooks:Z

    .line 1266
    invoke-virtual {p0, v3}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoCreateSharedNotebooksIsSet(Z)V

    goto/16 :goto_1

    .line 1268
    :cond_3
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 1256
    :pswitch_3
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_4

    .line 1257
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noSetParentTag:Z

    .line 1258
    invoke-virtual {p0, v3}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoSetParentTagIsSet(Z)V

    goto/16 :goto_1

    .line 1260
    :cond_4
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 1248
    :pswitch_4
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_5

    .line 1249
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noExpungeTags:Z

    .line 1250
    invoke-virtual {p0, v3}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoExpungeTagsIsSet(Z)V

    goto/16 :goto_1

    .line 1252
    :cond_5
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 1240
    :pswitch_5
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_6

    .line 1241
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noUpdateTags:Z

    .line 1242
    invoke-virtual {p0, v3}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoUpdateTagsIsSet(Z)V

    goto/16 :goto_1

    .line 1244
    :cond_6
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 1232
    :pswitch_6
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_7

    .line 1233
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noCreateTags:Z

    .line 1234
    invoke-virtual {p0, v3}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoCreateTagsIsSet(Z)V

    goto/16 :goto_1

    .line 1236
    :cond_7
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 1224
    :pswitch_7
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_8

    .line 1225
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noPublishToBusinessLibrary:Z

    .line 1226
    invoke-virtual {p0, v3}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoPublishToBusinessLibraryIsSet(Z)V

    goto/16 :goto_1

    .line 1228
    :cond_8
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 1216
    :pswitch_8
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_9

    .line 1217
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noPublishToPublic:Z

    .line 1218
    invoke-virtual {p0, v3}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoPublishToPublicIsSet(Z)V

    goto/16 :goto_1

    .line 1220
    :cond_9
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 1208
    :pswitch_9
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_a

    .line 1209
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noSetNotebookStack:Z

    .line 1210
    invoke-virtual {p0, v3}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoSetNotebookStackIsSet(Z)V

    goto/16 :goto_1

    .line 1212
    :cond_a
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 1200
    :pswitch_a
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_b

    .line 1201
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noSetDefaultNotebook:Z

    .line 1202
    invoke-virtual {p0, v3}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoSetDefaultNotebookIsSet(Z)V

    goto/16 :goto_1

    .line 1204
    :cond_b
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 1192
    :pswitch_b
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_c

    .line 1193
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noExpungeNotebook:Z

    .line 1194
    invoke-virtual {p0, v3}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoExpungeNotebookIsSet(Z)V

    goto/16 :goto_1

    .line 1196
    :cond_c
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 1184
    :pswitch_c
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_d

    .line 1185
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noUpdateNotebook:Z

    .line 1186
    invoke-virtual {p0, v3}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoUpdateNotebookIsSet(Z)V

    goto/16 :goto_1

    .line 1188
    :cond_d
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 1176
    :pswitch_d
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_e

    .line 1177
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noSendMessageToRecipients:Z

    .line 1178
    invoke-virtual {p0, v3}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoSendMessageToRecipientsIsSet(Z)V

    goto/16 :goto_1

    .line 1180
    :cond_e
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 1168
    :pswitch_e
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_f

    .line 1169
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noEmailNotes:Z

    .line 1170
    invoke-virtual {p0, v3}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoEmailNotesIsSet(Z)V

    goto/16 :goto_1

    .line 1172
    :cond_f
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 1160
    :pswitch_f
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_10

    .line 1161
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noShareNotes:Z

    .line 1162
    invoke-virtual {p0, v3}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoShareNotesIsSet(Z)V

    goto :goto_1

    .line 1164
    :cond_10
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 1152
    :pswitch_10
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_11

    .line 1153
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noExpungeNotes:Z

    .line 1154
    invoke-virtual {p0, v3}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoExpungeNotesIsSet(Z)V

    goto :goto_1

    .line 1156
    :cond_11
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 1144
    :pswitch_11
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_12

    .line 1145
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noUpdateNotes:Z

    .line 1146
    invoke-virtual {p0, v3}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoUpdateNotesIsSet(Z)V

    goto :goto_1

    .line 1148
    :cond_12
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 1136
    :pswitch_12
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_13

    .line 1137
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noCreateNotes:Z

    .line 1138
    invoke-virtual {p0, v3}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoCreateNotesIsSet(Z)V

    goto :goto_1

    .line 1140
    :cond_13
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 1128
    :pswitch_13
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_14

    .line 1129
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noReadNotes:Z

    .line 1130
    invoke-virtual {p0, v3}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoReadNotesIsSet(Z)V

    goto :goto_1

    .line 1132
    :cond_14
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    .line 1288
    :goto_1
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readFieldEnd()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setExpungeWhichSharedNotebookRestrictions(Lcom/evernote/edam/type/SharedNotebookInstanceRestrictions;)V
    .locals 0

    .line 707
    iput-object p1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->expungeWhichSharedNotebookRestrictions:Lcom/evernote/edam/type/SharedNotebookInstanceRestrictions;

    return-void
.end method

.method public setExpungeWhichSharedNotebookRestrictionsIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 721
    iput-object p1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->expungeWhichSharedNotebookRestrictions:Lcom/evernote/edam/type/SharedNotebookInstanceRestrictions;

    :cond_0
    return-void
.end method

.method public setNoCreateNotes(Z)V
    .locals 0

    .line 294
    iput-boolean p1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noCreateNotes:Z

    const/4 p1, 0x1

    .line 295
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoCreateNotesIsSet(Z)V

    return-void
.end method

.method public setNoCreateNotesIsSet(Z)V
    .locals 2

    .line 308
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/4 v1, 0x1

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setNoCreateSharedNotebooks(Z)V
    .locals 0

    .line 646
    iput-boolean p1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noCreateSharedNotebooks:Z

    const/4 p1, 0x1

    .line 647
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoCreateSharedNotebooksIsSet(Z)V

    return-void
.end method

.method public setNoCreateSharedNotebooksIsSet(Z)V
    .locals 2

    .line 660
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/16 v1, 0x11

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setNoCreateTags(Z)V
    .locals 0

    .line 558
    iput-boolean p1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noCreateTags:Z

    const/4 p1, 0x1

    .line 559
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoCreateTagsIsSet(Z)V

    return-void
.end method

.method public setNoCreateTagsIsSet(Z)V
    .locals 2

    .line 572
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/16 v1, 0xd

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setNoEmailNotes(Z)V
    .locals 0

    .line 382
    iput-boolean p1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noEmailNotes:Z

    const/4 p1, 0x1

    .line 383
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoEmailNotesIsSet(Z)V

    return-void
.end method

.method public setNoEmailNotesIsSet(Z)V
    .locals 2

    .line 396
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/4 v1, 0x5

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setNoExpungeNotebook(Z)V
    .locals 0

    .line 448
    iput-boolean p1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noExpungeNotebook:Z

    const/4 p1, 0x1

    .line 449
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoExpungeNotebookIsSet(Z)V

    return-void
.end method

.method public setNoExpungeNotebookIsSet(Z)V
    .locals 2

    .line 462
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/16 v1, 0x8

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setNoExpungeNotes(Z)V
    .locals 0

    .line 338
    iput-boolean p1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noExpungeNotes:Z

    const/4 p1, 0x1

    .line 339
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoExpungeNotesIsSet(Z)V

    return-void
.end method

.method public setNoExpungeNotesIsSet(Z)V
    .locals 2

    .line 352
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/4 v1, 0x3

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setNoExpungeTags(Z)V
    .locals 0

    .line 602
    iput-boolean p1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noExpungeTags:Z

    const/4 p1, 0x1

    .line 603
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoExpungeTagsIsSet(Z)V

    return-void
.end method

.method public setNoExpungeTagsIsSet(Z)V
    .locals 2

    .line 616
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/16 v1, 0xf

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setNoPublishToBusinessLibrary(Z)V
    .locals 0

    .line 536
    iput-boolean p1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noPublishToBusinessLibrary:Z

    const/4 p1, 0x1

    .line 537
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoPublishToBusinessLibraryIsSet(Z)V

    return-void
.end method

.method public setNoPublishToBusinessLibraryIsSet(Z)V
    .locals 2

    .line 550
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/16 v1, 0xc

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setNoPublishToPublic(Z)V
    .locals 0

    .line 514
    iput-boolean p1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noPublishToPublic:Z

    const/4 p1, 0x1

    .line 515
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoPublishToPublicIsSet(Z)V

    return-void
.end method

.method public setNoPublishToPublicIsSet(Z)V
    .locals 2

    .line 528
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/16 v1, 0xb

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setNoReadNotes(Z)V
    .locals 0

    .line 272
    iput-boolean p1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noReadNotes:Z

    const/4 p1, 0x1

    .line 273
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoReadNotesIsSet(Z)V

    return-void
.end method

.method public setNoReadNotesIsSet(Z)V
    .locals 2

    .line 286
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/4 v1, 0x0

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setNoSendMessageToRecipients(Z)V
    .locals 0

    .line 404
    iput-boolean p1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noSendMessageToRecipients:Z

    const/4 p1, 0x1

    .line 405
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoSendMessageToRecipientsIsSet(Z)V

    return-void
.end method

.method public setNoSendMessageToRecipientsIsSet(Z)V
    .locals 2

    .line 418
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/4 v1, 0x6

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setNoSetDefaultNotebook(Z)V
    .locals 0

    .line 470
    iput-boolean p1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noSetDefaultNotebook:Z

    const/4 p1, 0x1

    .line 471
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoSetDefaultNotebookIsSet(Z)V

    return-void
.end method

.method public setNoSetDefaultNotebookIsSet(Z)V
    .locals 2

    .line 484
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/16 v1, 0x9

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setNoSetNotebookStack(Z)V
    .locals 0

    .line 492
    iput-boolean p1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noSetNotebookStack:Z

    const/4 p1, 0x1

    .line 493
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoSetNotebookStackIsSet(Z)V

    return-void
.end method

.method public setNoSetNotebookStackIsSet(Z)V
    .locals 2

    .line 506
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/16 v1, 0xa

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setNoSetParentTag(Z)V
    .locals 0

    .line 624
    iput-boolean p1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noSetParentTag:Z

    const/4 p1, 0x1

    .line 625
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoSetParentTagIsSet(Z)V

    return-void
.end method

.method public setNoSetParentTagIsSet(Z)V
    .locals 2

    .line 638
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/16 v1, 0x10

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setNoShareNotes(Z)V
    .locals 0

    .line 360
    iput-boolean p1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noShareNotes:Z

    const/4 p1, 0x1

    .line 361
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoShareNotesIsSet(Z)V

    return-void
.end method

.method public setNoShareNotesIsSet(Z)V
    .locals 2

    .line 374
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/4 v1, 0x4

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setNoUpdateNotebook(Z)V
    .locals 0

    .line 426
    iput-boolean p1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noUpdateNotebook:Z

    const/4 p1, 0x1

    .line 427
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoUpdateNotebookIsSet(Z)V

    return-void
.end method

.method public setNoUpdateNotebookIsSet(Z)V
    .locals 2

    .line 440
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/4 v1, 0x7

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setNoUpdateNotes(Z)V
    .locals 0

    .line 316
    iput-boolean p1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noUpdateNotes:Z

    const/4 p1, 0x1

    .line 317
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoUpdateNotesIsSet(Z)V

    return-void
.end method

.method public setNoUpdateNotesIsSet(Z)V
    .locals 2

    .line 330
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/4 v1, 0x2

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setNoUpdateTags(Z)V
    .locals 0

    .line 580
    iput-boolean p1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noUpdateTags:Z

    const/4 p1, 0x1

    .line 581
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/NotebookRestrictions;->setNoUpdateTagsIsSet(Z)V

    return-void
.end method

.method public setNoUpdateTagsIsSet(Z)V
    .locals 2

    .line 594
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/16 v1, 0xe

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setUpdateWhichSharedNotebookRestrictions(Lcom/evernote/edam/type/SharedNotebookInstanceRestrictions;)V
    .locals 0

    .line 676
    iput-object p1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->updateWhichSharedNotebookRestrictions:Lcom/evernote/edam/type/SharedNotebookInstanceRestrictions;

    return-void
.end method

.method public setUpdateWhichSharedNotebookRestrictionsIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 690
    iput-object p1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->updateWhichSharedNotebookRestrictions:Lcom/evernote/edam/type/SharedNotebookInstanceRestrictions;

    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 1408
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NotebookRestrictions("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1411
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoReadNotes()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    const-string v1, "noReadNotes:"

    .line 1412
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1413
    iget-boolean v1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noReadNotes:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    const/4 v1, 0x1

    .line 1416
    :goto_0
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoCreateNotes()Z

    move-result v3

    if-eqz v3, :cond_2

    if-nez v1, :cond_1

    const-string v1, ", "

    .line 1417
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const-string v1, "noCreateNotes:"

    .line 1418
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1419
    iget-boolean v1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noCreateNotes:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1422
    :cond_2
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoUpdateNotes()Z

    move-result v3

    if-eqz v3, :cond_4

    if-nez v1, :cond_3

    const-string v1, ", "

    .line 1423
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    const-string v1, "noUpdateNotes:"

    .line 1424
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1425
    iget-boolean v1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noUpdateNotes:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1428
    :cond_4
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoExpungeNotes()Z

    move-result v3

    if-eqz v3, :cond_6

    if-nez v1, :cond_5

    const-string v1, ", "

    .line 1429
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    const-string v1, "noExpungeNotes:"

    .line 1430
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1431
    iget-boolean v1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noExpungeNotes:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1434
    :cond_6
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoShareNotes()Z

    move-result v3

    if-eqz v3, :cond_8

    if-nez v1, :cond_7

    const-string v1, ", "

    .line 1435
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_7
    const-string v1, "noShareNotes:"

    .line 1436
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1437
    iget-boolean v1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noShareNotes:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1440
    :cond_8
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoEmailNotes()Z

    move-result v3

    if-eqz v3, :cond_a

    if-nez v1, :cond_9

    const-string v1, ", "

    .line 1441
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_9
    const-string v1, "noEmailNotes:"

    .line 1442
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1443
    iget-boolean v1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noEmailNotes:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1446
    :cond_a
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoSendMessageToRecipients()Z

    move-result v3

    if-eqz v3, :cond_c

    if-nez v1, :cond_b

    const-string v1, ", "

    .line 1447
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_b
    const-string v1, "noSendMessageToRecipients:"

    .line 1448
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1449
    iget-boolean v1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noSendMessageToRecipients:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1452
    :cond_c
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoUpdateNotebook()Z

    move-result v3

    if-eqz v3, :cond_e

    if-nez v1, :cond_d

    const-string v1, ", "

    .line 1453
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_d
    const-string v1, "noUpdateNotebook:"

    .line 1454
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1455
    iget-boolean v1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noUpdateNotebook:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1458
    :cond_e
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoExpungeNotebook()Z

    move-result v3

    if-eqz v3, :cond_10

    if-nez v1, :cond_f

    const-string v1, ", "

    .line 1459
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_f
    const-string v1, "noExpungeNotebook:"

    .line 1460
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1461
    iget-boolean v1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noExpungeNotebook:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1464
    :cond_10
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoSetDefaultNotebook()Z

    move-result v3

    if-eqz v3, :cond_12

    if-nez v1, :cond_11

    const-string v1, ", "

    .line 1465
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_11
    const-string v1, "noSetDefaultNotebook:"

    .line 1466
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1467
    iget-boolean v1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noSetDefaultNotebook:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1470
    :cond_12
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoSetNotebookStack()Z

    move-result v3

    if-eqz v3, :cond_14

    if-nez v1, :cond_13

    const-string v1, ", "

    .line 1471
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_13
    const-string v1, "noSetNotebookStack:"

    .line 1472
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1473
    iget-boolean v1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noSetNotebookStack:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1476
    :cond_14
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoPublishToPublic()Z

    move-result v3

    if-eqz v3, :cond_16

    if-nez v1, :cond_15

    const-string v1, ", "

    .line 1477
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_15
    const-string v1, "noPublishToPublic:"

    .line 1478
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1479
    iget-boolean v1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noPublishToPublic:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1482
    :cond_16
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoPublishToBusinessLibrary()Z

    move-result v3

    if-eqz v3, :cond_18

    if-nez v1, :cond_17

    const-string v1, ", "

    .line 1483
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_17
    const-string v1, "noPublishToBusinessLibrary:"

    .line 1484
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1485
    iget-boolean v1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noPublishToBusinessLibrary:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1488
    :cond_18
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoCreateTags()Z

    move-result v3

    if-eqz v3, :cond_1a

    if-nez v1, :cond_19

    const-string v1, ", "

    .line 1489
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_19
    const-string v1, "noCreateTags:"

    .line 1490
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1491
    iget-boolean v1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noCreateTags:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1494
    :cond_1a
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoUpdateTags()Z

    move-result v3

    if-eqz v3, :cond_1c

    if-nez v1, :cond_1b

    const-string v1, ", "

    .line 1495
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1b
    const-string v1, "noUpdateTags:"

    .line 1496
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1497
    iget-boolean v1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noUpdateTags:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1500
    :cond_1c
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoExpungeTags()Z

    move-result v3

    if-eqz v3, :cond_1e

    if-nez v1, :cond_1d

    const-string v1, ", "

    .line 1501
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1d
    const-string v1, "noExpungeTags:"

    .line 1502
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1503
    iget-boolean v1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noExpungeTags:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1506
    :cond_1e
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoSetParentTag()Z

    move-result v3

    if-eqz v3, :cond_20

    if-nez v1, :cond_1f

    const-string v1, ", "

    .line 1507
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1f
    const-string v1, "noSetParentTag:"

    .line 1508
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1509
    iget-boolean v1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noSetParentTag:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1512
    :cond_20
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoCreateSharedNotebooks()Z

    move-result v3

    if-eqz v3, :cond_22

    if-nez v1, :cond_21

    const-string v1, ", "

    .line 1513
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_21
    const-string v1, "noCreateSharedNotebooks:"

    .line 1514
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1515
    iget-boolean v1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noCreateSharedNotebooks:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1518
    :cond_22
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetUpdateWhichSharedNotebookRestrictions()Z

    move-result v3

    if-eqz v3, :cond_25

    if-nez v1, :cond_23

    const-string v1, ", "

    .line 1519
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_23
    const-string v1, "updateWhichSharedNotebookRestrictions:"

    .line 1520
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1521
    iget-object v1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->updateWhichSharedNotebookRestrictions:Lcom/evernote/edam/type/SharedNotebookInstanceRestrictions;

    if-nez v1, :cond_24

    const-string v1, "null"

    .line 1522
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1524
    :cond_24
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :goto_1
    const/4 v1, 0x0

    .line 1528
    :cond_25
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetExpungeWhichSharedNotebookRestrictions()Z

    move-result v2

    if-eqz v2, :cond_28

    if-nez v1, :cond_26

    const-string v1, ", "

    .line 1529
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_26
    const-string v1, "expungeWhichSharedNotebookRestrictions:"

    .line 1530
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1531
    iget-object v1, p0, Lcom/evernote/edam/type/NotebookRestrictions;->expungeWhichSharedNotebookRestrictions:Lcom/evernote/edam/type/SharedNotebookInstanceRestrictions;

    if-nez v1, :cond_27

    const-string v1, "null"

    .line 1532
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 1534
    :cond_27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_28
    :goto_2
    const-string v1, ")"

    .line 1538
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1539
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public unsetExpungeWhichSharedNotebookRestrictions()V
    .locals 1

    const/4 v0, 0x0

    .line 711
    iput-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->expungeWhichSharedNotebookRestrictions:Lcom/evernote/edam/type/SharedNotebookInstanceRestrictions;

    return-void
.end method

.method public unsetNoCreateNotes()V
    .locals 3

    .line 299
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetNoCreateSharedNotebooks()V
    .locals 3

    .line 651
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/16 v1, 0x11

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetNoCreateTags()V
    .locals 3

    .line 563
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/16 v1, 0xd

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetNoEmailNotes()V
    .locals 3

    .line 387
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/4 v1, 0x5

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetNoExpungeNotebook()V
    .locals 3

    .line 453
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/16 v1, 0x8

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetNoExpungeNotes()V
    .locals 3

    .line 343
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/4 v1, 0x3

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetNoExpungeTags()V
    .locals 3

    .line 607
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/16 v1, 0xf

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetNoPublishToBusinessLibrary()V
    .locals 3

    .line 541
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/16 v1, 0xc

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetNoPublishToPublic()V
    .locals 3

    .line 519
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/16 v1, 0xb

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetNoReadNotes()V
    .locals 2

    .line 277
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/4 v1, 0x0

    aput-boolean v1, v0, v1

    return-void
.end method

.method public unsetNoSendMessageToRecipients()V
    .locals 3

    .line 409
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/4 v1, 0x6

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetNoSetDefaultNotebook()V
    .locals 3

    .line 475
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/16 v1, 0x9

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetNoSetNotebookStack()V
    .locals 3

    .line 497
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/16 v1, 0xa

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetNoSetParentTag()V
    .locals 3

    .line 629
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/16 v1, 0x10

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetNoShareNotes()V
    .locals 3

    .line 365
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/4 v1, 0x4

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetNoUpdateNotebook()V
    .locals 3

    .line 431
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/4 v1, 0x7

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetNoUpdateNotes()V
    .locals 3

    .line 321
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/4 v1, 0x2

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetNoUpdateTags()V
    .locals 3

    .line 585
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->__isset_vector:[Z

    const/16 v1, 0xe

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetUpdateWhichSharedNotebookRestrictions()V
    .locals 1

    const/4 v0, 0x0

    .line 680
    iput-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->updateWhichSharedNotebookRestrictions:Lcom/evernote/edam/type/SharedNotebookInstanceRestrictions;

    return-void
.end method

.method public validate()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    return-void
.end method

.method public write(Lcom/evernote/thrift/protocol/TProtocol;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1295
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->validate()V

    .line 1297
    sget-object v0, Lcom/evernote/edam/type/NotebookRestrictions;->STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeStructBegin(Lcom/evernote/thrift/protocol/TStruct;)V

    .line 1298
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoReadNotes()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1299
    sget-object v0, Lcom/evernote/edam/type/NotebookRestrictions;->NO_READ_NOTES_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1300
    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noReadNotes:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 1301
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1303
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoCreateNotes()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1304
    sget-object v0, Lcom/evernote/edam/type/NotebookRestrictions;->NO_CREATE_NOTES_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1305
    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noCreateNotes:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 1306
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1308
    :cond_1
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoUpdateNotes()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1309
    sget-object v0, Lcom/evernote/edam/type/NotebookRestrictions;->NO_UPDATE_NOTES_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1310
    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noUpdateNotes:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 1311
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1313
    :cond_2
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoExpungeNotes()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1314
    sget-object v0, Lcom/evernote/edam/type/NotebookRestrictions;->NO_EXPUNGE_NOTES_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1315
    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noExpungeNotes:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 1316
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1318
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoShareNotes()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1319
    sget-object v0, Lcom/evernote/edam/type/NotebookRestrictions;->NO_SHARE_NOTES_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1320
    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noShareNotes:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 1321
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1323
    :cond_4
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoEmailNotes()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1324
    sget-object v0, Lcom/evernote/edam/type/NotebookRestrictions;->NO_EMAIL_NOTES_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1325
    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noEmailNotes:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 1326
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1328
    :cond_5
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoSendMessageToRecipients()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1329
    sget-object v0, Lcom/evernote/edam/type/NotebookRestrictions;->NO_SEND_MESSAGE_TO_RECIPIENTS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1330
    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noSendMessageToRecipients:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 1331
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1333
    :cond_6
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoUpdateNotebook()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1334
    sget-object v0, Lcom/evernote/edam/type/NotebookRestrictions;->NO_UPDATE_NOTEBOOK_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1335
    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noUpdateNotebook:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 1336
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1338
    :cond_7
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoExpungeNotebook()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1339
    sget-object v0, Lcom/evernote/edam/type/NotebookRestrictions;->NO_EXPUNGE_NOTEBOOK_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1340
    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noExpungeNotebook:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 1341
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1343
    :cond_8
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoSetDefaultNotebook()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1344
    sget-object v0, Lcom/evernote/edam/type/NotebookRestrictions;->NO_SET_DEFAULT_NOTEBOOK_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1345
    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noSetDefaultNotebook:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 1346
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1348
    :cond_9
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoSetNotebookStack()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1349
    sget-object v0, Lcom/evernote/edam/type/NotebookRestrictions;->NO_SET_NOTEBOOK_STACK_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1350
    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noSetNotebookStack:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 1351
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1353
    :cond_a
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoPublishToPublic()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1354
    sget-object v0, Lcom/evernote/edam/type/NotebookRestrictions;->NO_PUBLISH_TO_PUBLIC_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1355
    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noPublishToPublic:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 1356
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1358
    :cond_b
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoPublishToBusinessLibrary()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1359
    sget-object v0, Lcom/evernote/edam/type/NotebookRestrictions;->NO_PUBLISH_TO_BUSINESS_LIBRARY_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1360
    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noPublishToBusinessLibrary:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 1361
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1363
    :cond_c
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoCreateTags()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1364
    sget-object v0, Lcom/evernote/edam/type/NotebookRestrictions;->NO_CREATE_TAGS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1365
    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noCreateTags:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 1366
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1368
    :cond_d
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoUpdateTags()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1369
    sget-object v0, Lcom/evernote/edam/type/NotebookRestrictions;->NO_UPDATE_TAGS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1370
    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noUpdateTags:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 1371
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1373
    :cond_e
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoExpungeTags()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1374
    sget-object v0, Lcom/evernote/edam/type/NotebookRestrictions;->NO_EXPUNGE_TAGS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1375
    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noExpungeTags:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 1376
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1378
    :cond_f
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoSetParentTag()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1379
    sget-object v0, Lcom/evernote/edam/type/NotebookRestrictions;->NO_SET_PARENT_TAG_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1380
    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noSetParentTag:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 1381
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1383
    :cond_10
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetNoCreateSharedNotebooks()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 1384
    sget-object v0, Lcom/evernote/edam/type/NotebookRestrictions;->NO_CREATE_SHARED_NOTEBOOKS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1385
    iget-boolean v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->noCreateSharedNotebooks:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 1386
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1388
    :cond_11
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->updateWhichSharedNotebookRestrictions:Lcom/evernote/edam/type/SharedNotebookInstanceRestrictions;

    if-eqz v0, :cond_12

    .line 1389
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetUpdateWhichSharedNotebookRestrictions()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 1390
    sget-object v0, Lcom/evernote/edam/type/NotebookRestrictions;->UPDATE_WHICH_SHARED_NOTEBOOK_RESTRICTIONS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1391
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->updateWhichSharedNotebookRestrictions:Lcom/evernote/edam/type/SharedNotebookInstanceRestrictions;

    invoke-virtual {v0}, Lcom/evernote/edam/type/SharedNotebookInstanceRestrictions;->getValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeI32(I)V

    .line 1392
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1395
    :cond_12
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->expungeWhichSharedNotebookRestrictions:Lcom/evernote/edam/type/SharedNotebookInstanceRestrictions;

    if-eqz v0, :cond_13

    .line 1396
    invoke-virtual {p0}, Lcom/evernote/edam/type/NotebookRestrictions;->isSetExpungeWhichSharedNotebookRestrictions()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 1397
    sget-object v0, Lcom/evernote/edam/type/NotebookRestrictions;->EXPUNGE_WHICH_SHARED_NOTEBOOK_RESTRICTIONS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1398
    iget-object v0, p0, Lcom/evernote/edam/type/NotebookRestrictions;->expungeWhichSharedNotebookRestrictions:Lcom/evernote/edam/type/SharedNotebookInstanceRestrictions;

    invoke-virtual {v0}, Lcom/evernote/edam/type/SharedNotebookInstanceRestrictions;->getValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeI32(I)V

    .line 1399
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1402
    :cond_13
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldStop()V

    .line 1403
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeStructEnd()V

    return-void
.end method

.class public Lcom/evernote/edam/type/SavedSearchScope;
.super Ljava/lang/Object;
.source "SavedSearchScope.java"

# interfaces
.implements Lcom/evernote/thrift/TBase;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/evernote/thrift/TBase<",
        "Lcom/evernote/edam/type/SavedSearchScope;",
        ">;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# static fields
.field private static final INCLUDE_ACCOUNT_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final INCLUDE_BUSINESS_LINKED_NOTEBOOKS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final INCLUDE_PERSONAL_LINKED_NOTEBOOKS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

.field private static final __INCLUDEACCOUNT_ISSET_ID:I = 0x0

.field private static final __INCLUDEBUSINESSLINKEDNOTEBOOKS_ISSET_ID:I = 0x2

.field private static final __INCLUDEPERSONALLINKEDNOTEBOOKS_ISSET_ID:I = 0x1


# instance fields
.field private __isset_vector:[Z

.field private includeAccount:Z

.field private includeBusinessLinkedNotebooks:Z

.field private includePersonalLinkedNotebooks:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 36
    new-instance v0, Lcom/evernote/thrift/protocol/TStruct;

    const-string v1, "SavedSearchScope"

    invoke-direct {v0, v1}, Lcom/evernote/thrift/protocol/TStruct;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/evernote/edam/type/SavedSearchScope;->STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

    .line 38
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "includeAccount"

    const/4 v2, 0x2

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/SavedSearchScope;->INCLUDE_ACCOUNT_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 39
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "includePersonalLinkedNotebooks"

    invoke-direct {v0, v1, v2, v2}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/SavedSearchScope;->INCLUDE_PERSONAL_LINKED_NOTEBOOKS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 40
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "includeBusinessLinkedNotebooks"

    const/4 v3, 0x3

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/SavedSearchScope;->INCLUDE_BUSINESS_LINKED_NOTEBOOKS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x3

    .line 51
    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/evernote/edam/type/SavedSearchScope;->__isset_vector:[Z

    return-void
.end method

.method public constructor <init>(Lcom/evernote/edam/type/SavedSearchScope;)V
    .locals 4

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x3

    .line 51
    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/evernote/edam/type/SavedSearchScope;->__isset_vector:[Z

    .line 60
    iget-object v0, p1, Lcom/evernote/edam/type/SavedSearchScope;->__isset_vector:[Z

    iget-object v1, p0, Lcom/evernote/edam/type/SavedSearchScope;->__isset_vector:[Z

    array-length v2, v0

    const/4 v3, 0x0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 61
    iget-boolean v0, p1, Lcom/evernote/edam/type/SavedSearchScope;->includeAccount:Z

    iput-boolean v0, p0, Lcom/evernote/edam/type/SavedSearchScope;->includeAccount:Z

    .line 62
    iget-boolean v0, p1, Lcom/evernote/edam/type/SavedSearchScope;->includePersonalLinkedNotebooks:Z

    iput-boolean v0, p0, Lcom/evernote/edam/type/SavedSearchScope;->includePersonalLinkedNotebooks:Z

    .line 63
    iget-boolean p1, p1, Lcom/evernote/edam/type/SavedSearchScope;->includeBusinessLinkedNotebooks:Z

    iput-boolean p1, p0, Lcom/evernote/edam/type/SavedSearchScope;->includeBusinessLinkedNotebooks:Z

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    const/4 v0, 0x0

    .line 71
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/SavedSearchScope;->setIncludeAccountIsSet(Z)V

    .line 72
    iput-boolean v0, p0, Lcom/evernote/edam/type/SavedSearchScope;->includeAccount:Z

    .line 73
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/SavedSearchScope;->setIncludePersonalLinkedNotebooksIsSet(Z)V

    .line 74
    iput-boolean v0, p0, Lcom/evernote/edam/type/SavedSearchScope;->includePersonalLinkedNotebooks:Z

    .line 75
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/SavedSearchScope;->setIncludeBusinessLinkedNotebooksIsSet(Z)V

    .line 76
    iput-boolean v0, p0, Lcom/evernote/edam/type/SavedSearchScope;->includeBusinessLinkedNotebooks:Z

    return-void
.end method

.method public compareTo(Lcom/evernote/edam/type/SavedSearchScope;)I
    .locals 2

    .line 194
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 195
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    return p1

    .line 201
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/edam/type/SavedSearchScope;->isSetIncludeAccount()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/SavedSearchScope;->isSetIncludeAccount()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_1

    return v0

    .line 205
    :cond_1
    invoke-virtual {p0}, Lcom/evernote/edam/type/SavedSearchScope;->isSetIncludeAccount()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/evernote/edam/type/SavedSearchScope;->includeAccount:Z

    iget-boolean v1, p1, Lcom/evernote/edam/type/SavedSearchScope;->includeAccount:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_2

    return v0

    .line 210
    :cond_2
    invoke-virtual {p0}, Lcom/evernote/edam/type/SavedSearchScope;->isSetIncludePersonalLinkedNotebooks()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/SavedSearchScope;->isSetIncludePersonalLinkedNotebooks()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_3

    return v0

    .line 214
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/type/SavedSearchScope;->isSetIncludePersonalLinkedNotebooks()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/evernote/edam/type/SavedSearchScope;->includePersonalLinkedNotebooks:Z

    iget-boolean v1, p1, Lcom/evernote/edam/type/SavedSearchScope;->includePersonalLinkedNotebooks:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_4

    return v0

    .line 219
    :cond_4
    invoke-virtual {p0}, Lcom/evernote/edam/type/SavedSearchScope;->isSetIncludeBusinessLinkedNotebooks()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/SavedSearchScope;->isSetIncludeBusinessLinkedNotebooks()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_5

    return v0

    .line 223
    :cond_5
    invoke-virtual {p0}, Lcom/evernote/edam/type/SavedSearchScope;->isSetIncludeBusinessLinkedNotebooks()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-boolean v0, p0, Lcom/evernote/edam/type/SavedSearchScope;->includeBusinessLinkedNotebooks:Z

    iget-boolean p1, p1, Lcom/evernote/edam/type/SavedSearchScope;->includeBusinessLinkedNotebooks:Z

    invoke-static {v0, p1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result p1

    if-eqz p1, :cond_6

    return p1

    :cond_6
    const/4 p1, 0x0

    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 35
    check-cast p1, Lcom/evernote/edam/type/SavedSearchScope;

    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/SavedSearchScope;->compareTo(Lcom/evernote/edam/type/SavedSearchScope;)I

    move-result p1

    return p1
.end method

.method public deepCopy()Lcom/evernote/edam/type/SavedSearchScope;
    .locals 1

    .line 67
    new-instance v0, Lcom/evernote/edam/type/SavedSearchScope;

    invoke-direct {v0, p0}, Lcom/evernote/edam/type/SavedSearchScope;-><init>(Lcom/evernote/edam/type/SavedSearchScope;)V

    return-object v0
.end method

.method public bridge synthetic deepCopy()Lcom/evernote/thrift/TBase;
    .locals 1

    .line 35
    invoke-virtual {p0}, Lcom/evernote/edam/type/SavedSearchScope;->deepCopy()Lcom/evernote/edam/type/SavedSearchScope;

    move-result-object v0

    return-object v0
.end method

.method public equals(Lcom/evernote/edam/type/SavedSearchScope;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 158
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/edam/type/SavedSearchScope;->isSetIncludeAccount()Z

    move-result v1

    .line 159
    invoke-virtual {p1}, Lcom/evernote/edam/type/SavedSearchScope;->isSetIncludeAccount()Z

    move-result v2

    if-nez v1, :cond_1

    if-eqz v2, :cond_3

    :cond_1
    if-eqz v1, :cond_c

    if-nez v2, :cond_2

    goto :goto_2

    .line 163
    :cond_2
    iget-boolean v1, p0, Lcom/evernote/edam/type/SavedSearchScope;->includeAccount:Z

    iget-boolean v2, p1, Lcom/evernote/edam/type/SavedSearchScope;->includeAccount:Z

    if-eq v1, v2, :cond_3

    return v0

    .line 167
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/type/SavedSearchScope;->isSetIncludePersonalLinkedNotebooks()Z

    move-result v1

    .line 168
    invoke-virtual {p1}, Lcom/evernote/edam/type/SavedSearchScope;->isSetIncludePersonalLinkedNotebooks()Z

    move-result v2

    if-nez v1, :cond_4

    if-eqz v2, :cond_6

    :cond_4
    if-eqz v1, :cond_b

    if-nez v2, :cond_5

    goto :goto_1

    .line 172
    :cond_5
    iget-boolean v1, p0, Lcom/evernote/edam/type/SavedSearchScope;->includePersonalLinkedNotebooks:Z

    iget-boolean v2, p1, Lcom/evernote/edam/type/SavedSearchScope;->includePersonalLinkedNotebooks:Z

    if-eq v1, v2, :cond_6

    return v0

    .line 176
    :cond_6
    invoke-virtual {p0}, Lcom/evernote/edam/type/SavedSearchScope;->isSetIncludeBusinessLinkedNotebooks()Z

    move-result v1

    .line 177
    invoke-virtual {p1}, Lcom/evernote/edam/type/SavedSearchScope;->isSetIncludeBusinessLinkedNotebooks()Z

    move-result v2

    if-nez v1, :cond_7

    if-eqz v2, :cond_9

    :cond_7
    if-eqz v1, :cond_a

    if-nez v2, :cond_8

    goto :goto_0

    .line 181
    :cond_8
    iget-boolean v1, p0, Lcom/evernote/edam/type/SavedSearchScope;->includeBusinessLinkedNotebooks:Z

    iget-boolean p1, p1, Lcom/evernote/edam/type/SavedSearchScope;->includeBusinessLinkedNotebooks:Z

    if-eq v1, p1, :cond_9

    return v0

    :cond_9
    const/4 p1, 0x1

    return p1

    :cond_a
    :goto_0
    return v0

    :cond_b
    :goto_1
    return v0

    :cond_c
    :goto_2
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 149
    :cond_0
    instance-of v1, p1, Lcom/evernote/edam/type/SavedSearchScope;

    if-eqz v1, :cond_1

    .line 150
    check-cast p1, Lcom/evernote/edam/type/SavedSearchScope;

    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/SavedSearchScope;->equals(Lcom/evernote/edam/type/SavedSearchScope;)Z

    move-result p1

    return p1

    :cond_1
    return v0
.end method

.method public hashCode()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isIncludeAccount()Z
    .locals 1

    .line 80
    iget-boolean v0, p0, Lcom/evernote/edam/type/SavedSearchScope;->includeAccount:Z

    return v0
.end method

.method public isIncludeBusinessLinkedNotebooks()Z
    .locals 1

    .line 124
    iget-boolean v0, p0, Lcom/evernote/edam/type/SavedSearchScope;->includeBusinessLinkedNotebooks:Z

    return v0
.end method

.method public isIncludePersonalLinkedNotebooks()Z
    .locals 1

    .line 102
    iget-boolean v0, p0, Lcom/evernote/edam/type/SavedSearchScope;->includePersonalLinkedNotebooks:Z

    return v0
.end method

.method public isSetIncludeAccount()Z
    .locals 2

    .line 94
    iget-object v0, p0, Lcom/evernote/edam/type/SavedSearchScope;->__isset_vector:[Z

    const/4 v1, 0x0

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetIncludeBusinessLinkedNotebooks()Z
    .locals 2

    .line 138
    iget-object v0, p0, Lcom/evernote/edam/type/SavedSearchScope;->__isset_vector:[Z

    const/4 v1, 0x2

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetIncludePersonalLinkedNotebooks()Z
    .locals 2

    .line 116
    iget-object v0, p0, Lcom/evernote/edam/type/SavedSearchScope;->__isset_vector:[Z

    const/4 v1, 0x1

    aget-boolean v0, v0, v1

    return v0
.end method

.method public read(Lcom/evernote/thrift/protocol/TProtocol;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 233
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readStructBegin()Lcom/evernote/thrift/protocol/TStruct;

    .line 236
    :goto_0
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readFieldBegin()Lcom/evernote/thrift/protocol/TField;

    move-result-object v0

    .line 237
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-nez v1, :cond_0

    .line 270
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readStructEnd()V

    .line 271
    invoke-virtual {p0}, Lcom/evernote/edam/type/SavedSearchScope;->validate()V

    return-void

    .line 240
    :cond_0
    iget-short v1, v0, Lcom/evernote/thrift/protocol/TField;->id:S

    const/4 v2, 0x1

    const/4 v3, 0x2

    packed-switch v1, :pswitch_data_0

    .line 266
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 258
    :pswitch_0
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v3, :cond_1

    .line 259
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/type/SavedSearchScope;->includeBusinessLinkedNotebooks:Z

    .line 260
    invoke-virtual {p0, v2}, Lcom/evernote/edam/type/SavedSearchScope;->setIncludeBusinessLinkedNotebooksIsSet(Z)V

    goto :goto_1

    .line 262
    :cond_1
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 250
    :pswitch_1
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v3, :cond_2

    .line 251
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/type/SavedSearchScope;->includePersonalLinkedNotebooks:Z

    .line 252
    invoke-virtual {p0, v2}, Lcom/evernote/edam/type/SavedSearchScope;->setIncludePersonalLinkedNotebooksIsSet(Z)V

    goto :goto_1

    .line 254
    :cond_2
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 242
    :pswitch_2
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v3, :cond_3

    .line 243
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/type/SavedSearchScope;->includeAccount:Z

    .line 244
    invoke-virtual {p0, v2}, Lcom/evernote/edam/type/SavedSearchScope;->setIncludeAccountIsSet(Z)V

    goto :goto_1

    .line 246
    :cond_3
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    .line 268
    :goto_1
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readFieldEnd()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setIncludeAccount(Z)V
    .locals 0

    .line 84
    iput-boolean p1, p0, Lcom/evernote/edam/type/SavedSearchScope;->includeAccount:Z

    const/4 p1, 0x1

    .line 85
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/SavedSearchScope;->setIncludeAccountIsSet(Z)V

    return-void
.end method

.method public setIncludeAccountIsSet(Z)V
    .locals 2

    .line 98
    iget-object v0, p0, Lcom/evernote/edam/type/SavedSearchScope;->__isset_vector:[Z

    const/4 v1, 0x0

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setIncludeBusinessLinkedNotebooks(Z)V
    .locals 0

    .line 128
    iput-boolean p1, p0, Lcom/evernote/edam/type/SavedSearchScope;->includeBusinessLinkedNotebooks:Z

    const/4 p1, 0x1

    .line 129
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/SavedSearchScope;->setIncludeBusinessLinkedNotebooksIsSet(Z)V

    return-void
.end method

.method public setIncludeBusinessLinkedNotebooksIsSet(Z)V
    .locals 2

    .line 142
    iget-object v0, p0, Lcom/evernote/edam/type/SavedSearchScope;->__isset_vector:[Z

    const/4 v1, 0x2

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setIncludePersonalLinkedNotebooks(Z)V
    .locals 0

    .line 106
    iput-boolean p1, p0, Lcom/evernote/edam/type/SavedSearchScope;->includePersonalLinkedNotebooks:Z

    const/4 p1, 0x1

    .line 107
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/SavedSearchScope;->setIncludePersonalLinkedNotebooksIsSet(Z)V

    return-void
.end method

.method public setIncludePersonalLinkedNotebooksIsSet(Z)V
    .locals 2

    .line 120
    iget-object v0, p0, Lcom/evernote/edam/type/SavedSearchScope;->__isset_vector:[Z

    const/4 v1, 0x1

    aput-boolean p1, v0, v1

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 299
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SavedSearchScope("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 302
    invoke-virtual {p0}, Lcom/evernote/edam/type/SavedSearchScope;->isSetIncludeAccount()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    const-string v1, "includeAccount:"

    .line 303
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 304
    iget-boolean v1, p0, Lcom/evernote/edam/type/SavedSearchScope;->includeAccount:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    const/4 v1, 0x1

    .line 307
    :goto_0
    invoke-virtual {p0}, Lcom/evernote/edam/type/SavedSearchScope;->isSetIncludePersonalLinkedNotebooks()Z

    move-result v3

    if-eqz v3, :cond_2

    if-nez v1, :cond_1

    const-string v1, ", "

    .line 308
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const-string v1, "includePersonalLinkedNotebooks:"

    .line 309
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 310
    iget-boolean v1, p0, Lcom/evernote/edam/type/SavedSearchScope;->includePersonalLinkedNotebooks:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 313
    :cond_2
    invoke-virtual {p0}, Lcom/evernote/edam/type/SavedSearchScope;->isSetIncludeBusinessLinkedNotebooks()Z

    move-result v2

    if-eqz v2, :cond_4

    if-nez v1, :cond_3

    const-string v1, ", "

    .line 314
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    const-string v1, "includeBusinessLinkedNotebooks:"

    .line 315
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 316
    iget-boolean v1, p0, Lcom/evernote/edam/type/SavedSearchScope;->includeBusinessLinkedNotebooks:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    :cond_4
    const-string v1, ")"

    .line 319
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 320
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public unsetIncludeAccount()V
    .locals 2

    .line 89
    iget-object v0, p0, Lcom/evernote/edam/type/SavedSearchScope;->__isset_vector:[Z

    const/4 v1, 0x0

    aput-boolean v1, v0, v1

    return-void
.end method

.method public unsetIncludeBusinessLinkedNotebooks()V
    .locals 3

    .line 133
    iget-object v0, p0, Lcom/evernote/edam/type/SavedSearchScope;->__isset_vector:[Z

    const/4 v1, 0x2

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetIncludePersonalLinkedNotebooks()V
    .locals 3

    .line 111
    iget-object v0, p0, Lcom/evernote/edam/type/SavedSearchScope;->__isset_vector:[Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public validate()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    return-void
.end method

.method public write(Lcom/evernote/thrift/protocol/TProtocol;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 275
    invoke-virtual {p0}, Lcom/evernote/edam/type/SavedSearchScope;->validate()V

    .line 277
    sget-object v0, Lcom/evernote/edam/type/SavedSearchScope;->STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeStructBegin(Lcom/evernote/thrift/protocol/TStruct;)V

    .line 278
    invoke-virtual {p0}, Lcom/evernote/edam/type/SavedSearchScope;->isSetIncludeAccount()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 279
    sget-object v0, Lcom/evernote/edam/type/SavedSearchScope;->INCLUDE_ACCOUNT_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 280
    iget-boolean v0, p0, Lcom/evernote/edam/type/SavedSearchScope;->includeAccount:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 281
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 283
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/edam/type/SavedSearchScope;->isSetIncludePersonalLinkedNotebooks()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 284
    sget-object v0, Lcom/evernote/edam/type/SavedSearchScope;->INCLUDE_PERSONAL_LINKED_NOTEBOOKS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 285
    iget-boolean v0, p0, Lcom/evernote/edam/type/SavedSearchScope;->includePersonalLinkedNotebooks:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 286
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 288
    :cond_1
    invoke-virtual {p0}, Lcom/evernote/edam/type/SavedSearchScope;->isSetIncludeBusinessLinkedNotebooks()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 289
    sget-object v0, Lcom/evernote/edam/type/SavedSearchScope;->INCLUDE_BUSINESS_LINKED_NOTEBOOKS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 290
    iget-boolean v0, p0, Lcom/evernote/edam/type/SavedSearchScope;->includeBusinessLinkedNotebooks:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 291
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 293
    :cond_2
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldStop()V

    .line 294
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeStructEnd()V

    return-void
.end method

.class public Lcom/evernote/edam/type/SavedSearch;
.super Ljava/lang/Object;
.source "SavedSearch.java"

# interfaces
.implements Lcom/evernote/thrift/TBase;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/evernote/thrift/TBase<",
        "Lcom/evernote/edam/type/SavedSearch;",
        ">;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# static fields
.field private static final FORMAT_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final GUID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final NAME_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final QUERY_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final SCOPE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

.field private static final UPDATE_SEQUENCE_NUM_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final __UPDATESEQUENCENUM_ISSET_ID:I


# instance fields
.field private __isset_vector:[Z

.field private format:Lcom/evernote/edam/type/QueryFormat;

.field private guid:Ljava/lang/String;

.field private name:Ljava/lang/String;

.field private query:Ljava/lang/String;

.field private scope:Lcom/evernote/edam/type/SavedSearchScope;

.field private updateSequenceNum:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 74
    new-instance v0, Lcom/evernote/thrift/protocol/TStruct;

    const-string v1, "SavedSearch"

    invoke-direct {v0, v1}, Lcom/evernote/thrift/protocol/TStruct;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/evernote/edam/type/SavedSearch;->STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

    .line 76
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "guid"

    const/16 v2, 0xb

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/SavedSearch;->GUID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 77
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "name"

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/SavedSearch;->NAME_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 78
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "query"

    const/4 v3, 0x3

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/SavedSearch;->QUERY_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 79
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "format"

    const/16 v2, 0x8

    const/4 v3, 0x4

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/SavedSearch;->FORMAT_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 80
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "updateSequenceNum"

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/SavedSearch;->UPDATE_SEQUENCE_NUM_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 81
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "scope"

    const/16 v2, 0xc

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/SavedSearch;->SCOPE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 93
    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/evernote/edam/type/SavedSearch;->__isset_vector:[Z

    return-void
.end method

.method public constructor <init>(Lcom/evernote/edam/type/SavedSearch;)V
    .locals 4

    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 93
    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/evernote/edam/type/SavedSearch;->__isset_vector:[Z

    .line 102
    iget-object v0, p1, Lcom/evernote/edam/type/SavedSearch;->__isset_vector:[Z

    iget-object v1, p0, Lcom/evernote/edam/type/SavedSearch;->__isset_vector:[Z

    array-length v2, v0

    const/4 v3, 0x0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 103
    invoke-virtual {p1}, Lcom/evernote/edam/type/SavedSearch;->isSetGuid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p1, Lcom/evernote/edam/type/SavedSearch;->guid:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/type/SavedSearch;->guid:Ljava/lang/String;

    .line 106
    :cond_0
    invoke-virtual {p1}, Lcom/evernote/edam/type/SavedSearch;->isSetName()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 107
    iget-object v0, p1, Lcom/evernote/edam/type/SavedSearch;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/type/SavedSearch;->name:Ljava/lang/String;

    .line 109
    :cond_1
    invoke-virtual {p1}, Lcom/evernote/edam/type/SavedSearch;->isSetQuery()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 110
    iget-object v0, p1, Lcom/evernote/edam/type/SavedSearch;->query:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/type/SavedSearch;->query:Ljava/lang/String;

    .line 112
    :cond_2
    invoke-virtual {p1}, Lcom/evernote/edam/type/SavedSearch;->isSetFormat()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 113
    iget-object v0, p1, Lcom/evernote/edam/type/SavedSearch;->format:Lcom/evernote/edam/type/QueryFormat;

    iput-object v0, p0, Lcom/evernote/edam/type/SavedSearch;->format:Lcom/evernote/edam/type/QueryFormat;

    .line 115
    :cond_3
    iget v0, p1, Lcom/evernote/edam/type/SavedSearch;->updateSequenceNum:I

    iput v0, p0, Lcom/evernote/edam/type/SavedSearch;->updateSequenceNum:I

    .line 116
    invoke-virtual {p1}, Lcom/evernote/edam/type/SavedSearch;->isSetScope()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 117
    new-instance v0, Lcom/evernote/edam/type/SavedSearchScope;

    iget-object p1, p1, Lcom/evernote/edam/type/SavedSearch;->scope:Lcom/evernote/edam/type/SavedSearchScope;

    invoke-direct {v0, p1}, Lcom/evernote/edam/type/SavedSearchScope;-><init>(Lcom/evernote/edam/type/SavedSearchScope;)V

    iput-object v0, p0, Lcom/evernote/edam/type/SavedSearch;->scope:Lcom/evernote/edam/type/SavedSearchScope;

    :cond_4
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 2

    const/4 v0, 0x0

    .line 126
    iput-object v0, p0, Lcom/evernote/edam/type/SavedSearch;->guid:Ljava/lang/String;

    .line 127
    iput-object v0, p0, Lcom/evernote/edam/type/SavedSearch;->name:Ljava/lang/String;

    .line 128
    iput-object v0, p0, Lcom/evernote/edam/type/SavedSearch;->query:Ljava/lang/String;

    .line 129
    iput-object v0, p0, Lcom/evernote/edam/type/SavedSearch;->format:Lcom/evernote/edam/type/QueryFormat;

    const/4 v1, 0x0

    .line 130
    invoke-virtual {p0, v1}, Lcom/evernote/edam/type/SavedSearch;->setUpdateSequenceNumIsSet(Z)V

    .line 131
    iput v1, p0, Lcom/evernote/edam/type/SavedSearch;->updateSequenceNum:I

    .line 132
    iput-object v0, p0, Lcom/evernote/edam/type/SavedSearch;->scope:Lcom/evernote/edam/type/SavedSearchScope;

    return-void
.end method

.method public compareTo(Lcom/evernote/edam/type/SavedSearch;)I
    .locals 2

    .line 356
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 357
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    return p1

    .line 363
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/edam/type/SavedSearch;->isSetGuid()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/SavedSearch;->isSetGuid()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_1

    return v0

    .line 367
    :cond_1
    invoke-virtual {p0}, Lcom/evernote/edam/type/SavedSearch;->isSetGuid()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/evernote/edam/type/SavedSearch;->guid:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/type/SavedSearch;->guid:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_2

    return v0

    .line 372
    :cond_2
    invoke-virtual {p0}, Lcom/evernote/edam/type/SavedSearch;->isSetName()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/SavedSearch;->isSetName()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_3

    return v0

    .line 376
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/type/SavedSearch;->isSetName()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/evernote/edam/type/SavedSearch;->name:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/type/SavedSearch;->name:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_4

    return v0

    .line 381
    :cond_4
    invoke-virtual {p0}, Lcom/evernote/edam/type/SavedSearch;->isSetQuery()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/SavedSearch;->isSetQuery()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_5

    return v0

    .line 385
    :cond_5
    invoke-virtual {p0}, Lcom/evernote/edam/type/SavedSearch;->isSetQuery()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/evernote/edam/type/SavedSearch;->query:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/type/SavedSearch;->query:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_6

    return v0

    .line 390
    :cond_6
    invoke-virtual {p0}, Lcom/evernote/edam/type/SavedSearch;->isSetFormat()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/SavedSearch;->isSetFormat()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_7

    return v0

    .line 394
    :cond_7
    invoke-virtual {p0}, Lcom/evernote/edam/type/SavedSearch;->isSetFormat()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/evernote/edam/type/SavedSearch;->format:Lcom/evernote/edam/type/QueryFormat;

    iget-object v1, p1, Lcom/evernote/edam/type/SavedSearch;->format:Lcom/evernote/edam/type/QueryFormat;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result v0

    if-eqz v0, :cond_8

    return v0

    .line 399
    :cond_8
    invoke-virtual {p0}, Lcom/evernote/edam/type/SavedSearch;->isSetUpdateSequenceNum()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/SavedSearch;->isSetUpdateSequenceNum()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_9

    return v0

    .line 403
    :cond_9
    invoke-virtual {p0}, Lcom/evernote/edam/type/SavedSearch;->isSetUpdateSequenceNum()Z

    move-result v0

    if-eqz v0, :cond_a

    iget v0, p0, Lcom/evernote/edam/type/SavedSearch;->updateSequenceNum:I

    iget v1, p1, Lcom/evernote/edam/type/SavedSearch;->updateSequenceNum:I

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(II)I

    move-result v0

    if-eqz v0, :cond_a

    return v0

    .line 408
    :cond_a
    invoke-virtual {p0}, Lcom/evernote/edam/type/SavedSearch;->isSetScope()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/SavedSearch;->isSetScope()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_b

    return v0

    .line 412
    :cond_b
    invoke-virtual {p0}, Lcom/evernote/edam/type/SavedSearch;->isSetScope()Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/evernote/edam/type/SavedSearch;->scope:Lcom/evernote/edam/type/SavedSearchScope;

    iget-object p1, p1, Lcom/evernote/edam/type/SavedSearch;->scope:Lcom/evernote/edam/type/SavedSearchScope;

    invoke-static {v0, p1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result p1

    if-eqz p1, :cond_c

    return p1

    :cond_c
    const/4 p1, 0x0

    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 73
    check-cast p1, Lcom/evernote/edam/type/SavedSearch;

    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/SavedSearch;->compareTo(Lcom/evernote/edam/type/SavedSearch;)I

    move-result p1

    return p1
.end method

.method public deepCopy()Lcom/evernote/edam/type/SavedSearch;
    .locals 1

    .line 122
    new-instance v0, Lcom/evernote/edam/type/SavedSearch;

    invoke-direct {v0, p0}, Lcom/evernote/edam/type/SavedSearch;-><init>(Lcom/evernote/edam/type/SavedSearch;)V

    return-object v0
.end method

.method public bridge synthetic deepCopy()Lcom/evernote/thrift/TBase;
    .locals 1

    .line 73
    invoke-virtual {p0}, Lcom/evernote/edam/type/SavedSearch;->deepCopy()Lcom/evernote/edam/type/SavedSearch;

    move-result-object v0

    return-object v0
.end method

.method public equals(Lcom/evernote/edam/type/SavedSearch;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 293
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/edam/type/SavedSearch;->isSetGuid()Z

    move-result v1

    .line 294
    invoke-virtual {p1}, Lcom/evernote/edam/type/SavedSearch;->isSetGuid()Z

    move-result v2

    if-nez v1, :cond_1

    if-eqz v2, :cond_3

    :cond_1
    if-eqz v1, :cond_18

    if-nez v2, :cond_2

    goto/16 :goto_5

    .line 298
    :cond_2
    iget-object v1, p0, Lcom/evernote/edam/type/SavedSearch;->guid:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/type/SavedSearch;->guid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    return v0

    .line 302
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/type/SavedSearch;->isSetName()Z

    move-result v1

    .line 303
    invoke-virtual {p1}, Lcom/evernote/edam/type/SavedSearch;->isSetName()Z

    move-result v2

    if-nez v1, :cond_4

    if-eqz v2, :cond_6

    :cond_4
    if-eqz v1, :cond_17

    if-nez v2, :cond_5

    goto/16 :goto_4

    .line 307
    :cond_5
    iget-object v1, p0, Lcom/evernote/edam/type/SavedSearch;->name:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/type/SavedSearch;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    return v0

    .line 311
    :cond_6
    invoke-virtual {p0}, Lcom/evernote/edam/type/SavedSearch;->isSetQuery()Z

    move-result v1

    .line 312
    invoke-virtual {p1}, Lcom/evernote/edam/type/SavedSearch;->isSetQuery()Z

    move-result v2

    if-nez v1, :cond_7

    if-eqz v2, :cond_9

    :cond_7
    if-eqz v1, :cond_16

    if-nez v2, :cond_8

    goto :goto_3

    .line 316
    :cond_8
    iget-object v1, p0, Lcom/evernote/edam/type/SavedSearch;->query:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/type/SavedSearch;->query:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    return v0

    .line 320
    :cond_9
    invoke-virtual {p0}, Lcom/evernote/edam/type/SavedSearch;->isSetFormat()Z

    move-result v1

    .line 321
    invoke-virtual {p1}, Lcom/evernote/edam/type/SavedSearch;->isSetFormat()Z

    move-result v2

    if-nez v1, :cond_a

    if-eqz v2, :cond_c

    :cond_a
    if-eqz v1, :cond_15

    if-nez v2, :cond_b

    goto :goto_2

    .line 325
    :cond_b
    iget-object v1, p0, Lcom/evernote/edam/type/SavedSearch;->format:Lcom/evernote/edam/type/QueryFormat;

    iget-object v2, p1, Lcom/evernote/edam/type/SavedSearch;->format:Lcom/evernote/edam/type/QueryFormat;

    invoke-virtual {v1, v2}, Lcom/evernote/edam/type/QueryFormat;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    return v0

    .line 329
    :cond_c
    invoke-virtual {p0}, Lcom/evernote/edam/type/SavedSearch;->isSetUpdateSequenceNum()Z

    move-result v1

    .line 330
    invoke-virtual {p1}, Lcom/evernote/edam/type/SavedSearch;->isSetUpdateSequenceNum()Z

    move-result v2

    if-nez v1, :cond_d

    if-eqz v2, :cond_f

    :cond_d
    if-eqz v1, :cond_14

    if-nez v2, :cond_e

    goto :goto_1

    .line 334
    :cond_e
    iget v1, p0, Lcom/evernote/edam/type/SavedSearch;->updateSequenceNum:I

    iget v2, p1, Lcom/evernote/edam/type/SavedSearch;->updateSequenceNum:I

    if-eq v1, v2, :cond_f

    return v0

    .line 338
    :cond_f
    invoke-virtual {p0}, Lcom/evernote/edam/type/SavedSearch;->isSetScope()Z

    move-result v1

    .line 339
    invoke-virtual {p1}, Lcom/evernote/edam/type/SavedSearch;->isSetScope()Z

    move-result v2

    if-nez v1, :cond_10

    if-eqz v2, :cond_12

    :cond_10
    if-eqz v1, :cond_13

    if-nez v2, :cond_11

    goto :goto_0

    .line 343
    :cond_11
    iget-object v1, p0, Lcom/evernote/edam/type/SavedSearch;->scope:Lcom/evernote/edam/type/SavedSearchScope;

    iget-object p1, p1, Lcom/evernote/edam/type/SavedSearch;->scope:Lcom/evernote/edam/type/SavedSearchScope;

    invoke-virtual {v1, p1}, Lcom/evernote/edam/type/SavedSearchScope;->equals(Lcom/evernote/edam/type/SavedSearchScope;)Z

    move-result p1

    if-nez p1, :cond_12

    return v0

    :cond_12
    const/4 p1, 0x1

    return p1

    :cond_13
    :goto_0
    return v0

    :cond_14
    :goto_1
    return v0

    :cond_15
    :goto_2
    return v0

    :cond_16
    :goto_3
    return v0

    :cond_17
    :goto_4
    return v0

    :cond_18
    :goto_5
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 284
    :cond_0
    instance-of v1, p1, Lcom/evernote/edam/type/SavedSearch;

    if-eqz v1, :cond_1

    .line 285
    check-cast p1, Lcom/evernote/edam/type/SavedSearch;

    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/SavedSearch;->equals(Lcom/evernote/edam/type/SavedSearch;)Z

    move-result p1

    return p1

    :cond_1
    return v0
.end method

.method public getFormat()Lcom/evernote/edam/type/QueryFormat;
    .locals 1

    .line 209
    iget-object v0, p0, Lcom/evernote/edam/type/SavedSearch;->format:Lcom/evernote/edam/type/QueryFormat;

    return-object v0
.end method

.method public getGuid()Ljava/lang/String;
    .locals 1

    .line 136
    iget-object v0, p0, Lcom/evernote/edam/type/SavedSearch;->guid:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 159
    iget-object v0, p0, Lcom/evernote/edam/type/SavedSearch;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getQuery()Ljava/lang/String;
    .locals 1

    .line 182
    iget-object v0, p0, Lcom/evernote/edam/type/SavedSearch;->query:Ljava/lang/String;

    return-object v0
.end method

.method public getScope()Lcom/evernote/edam/type/SavedSearchScope;
    .locals 1

    .line 258
    iget-object v0, p0, Lcom/evernote/edam/type/SavedSearch;->scope:Lcom/evernote/edam/type/SavedSearchScope;

    return-object v0
.end method

.method public getUpdateSequenceNum()I
    .locals 1

    .line 236
    iget v0, p0, Lcom/evernote/edam/type/SavedSearch;->updateSequenceNum:I

    return v0
.end method

.method public hashCode()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isSetFormat()Z
    .locals 1

    .line 226
    iget-object v0, p0, Lcom/evernote/edam/type/SavedSearch;->format:Lcom/evernote/edam/type/QueryFormat;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetGuid()Z
    .locals 1

    .line 149
    iget-object v0, p0, Lcom/evernote/edam/type/SavedSearch;->guid:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetName()Z
    .locals 1

    .line 172
    iget-object v0, p0, Lcom/evernote/edam/type/SavedSearch;->name:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetQuery()Z
    .locals 1

    .line 195
    iget-object v0, p0, Lcom/evernote/edam/type/SavedSearch;->query:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetScope()Z
    .locals 1

    .line 271
    iget-object v0, p0, Lcom/evernote/edam/type/SavedSearch;->scope:Lcom/evernote/edam/type/SavedSearchScope;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetUpdateSequenceNum()Z
    .locals 2

    .line 250
    iget-object v0, p0, Lcom/evernote/edam/type/SavedSearch;->__isset_vector:[Z

    const/4 v1, 0x0

    aget-boolean v0, v0, v1

    return v0
.end method

.method public read(Lcom/evernote/thrift/protocol/TProtocol;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 422
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readStructBegin()Lcom/evernote/thrift/protocol/TStruct;

    .line 425
    :goto_0
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readFieldBegin()Lcom/evernote/thrift/protocol/TField;

    move-result-object v0

    .line 426
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-nez v1, :cond_0

    .line 479
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readStructEnd()V

    .line 480
    invoke-virtual {p0}, Lcom/evernote/edam/type/SavedSearch;->validate()V

    return-void

    .line 429
    :cond_0
    iget-short v1, v0, Lcom/evernote/thrift/protocol/TField;->id:S

    const/16 v2, 0x8

    const/16 v3, 0xb

    packed-switch v1, :pswitch_data_0

    .line 475
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 467
    :pswitch_0
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    const/16 v2, 0xc

    if-ne v1, v2, :cond_1

    .line 468
    new-instance v0, Lcom/evernote/edam/type/SavedSearchScope;

    invoke-direct {v0}, Lcom/evernote/edam/type/SavedSearchScope;-><init>()V

    iput-object v0, p0, Lcom/evernote/edam/type/SavedSearch;->scope:Lcom/evernote/edam/type/SavedSearchScope;

    .line 469
    iget-object v0, p0, Lcom/evernote/edam/type/SavedSearch;->scope:Lcom/evernote/edam/type/SavedSearchScope;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/type/SavedSearchScope;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    goto :goto_1

    .line 471
    :cond_1
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 459
    :pswitch_1
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v2, :cond_2

    .line 460
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI32()I

    move-result v0

    iput v0, p0, Lcom/evernote/edam/type/SavedSearch;->updateSequenceNum:I

    const/4 v0, 0x1

    .line 461
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/SavedSearch;->setUpdateSequenceNumIsSet(Z)V

    goto :goto_1

    .line 463
    :cond_2
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 452
    :pswitch_2
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v2, :cond_3

    .line 453
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI32()I

    move-result v0

    invoke-static {v0}, Lcom/evernote/edam/type/QueryFormat;->findByValue(I)Lcom/evernote/edam/type/QueryFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/type/SavedSearch;->format:Lcom/evernote/edam/type/QueryFormat;

    goto :goto_1

    .line 455
    :cond_3
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 445
    :pswitch_3
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v3, :cond_4

    .line 446
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/type/SavedSearch;->query:Ljava/lang/String;

    goto :goto_1

    .line 448
    :cond_4
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 438
    :pswitch_4
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v3, :cond_5

    .line 439
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/type/SavedSearch;->name:Ljava/lang/String;

    goto :goto_1

    .line 441
    :cond_5
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 431
    :pswitch_5
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v3, :cond_6

    .line 432
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/type/SavedSearch;->guid:Ljava/lang/String;

    goto :goto_1

    .line 434
    :cond_6
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    .line 477
    :goto_1
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readFieldEnd()V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setFormat(Lcom/evernote/edam/type/QueryFormat;)V
    .locals 0

    .line 217
    iput-object p1, p0, Lcom/evernote/edam/type/SavedSearch;->format:Lcom/evernote/edam/type/QueryFormat;

    return-void
.end method

.method public setFormatIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 231
    iput-object p1, p0, Lcom/evernote/edam/type/SavedSearch;->format:Lcom/evernote/edam/type/QueryFormat;

    :cond_0
    return-void
.end method

.method public setGuid(Ljava/lang/String;)V
    .locals 0

    .line 140
    iput-object p1, p0, Lcom/evernote/edam/type/SavedSearch;->guid:Ljava/lang/String;

    return-void
.end method

.method public setGuidIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 154
    iput-object p1, p0, Lcom/evernote/edam/type/SavedSearch;->guid:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0

    .line 163
    iput-object p1, p0, Lcom/evernote/edam/type/SavedSearch;->name:Ljava/lang/String;

    return-void
.end method

.method public setNameIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 177
    iput-object p1, p0, Lcom/evernote/edam/type/SavedSearch;->name:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setQuery(Ljava/lang/String;)V
    .locals 0

    .line 186
    iput-object p1, p0, Lcom/evernote/edam/type/SavedSearch;->query:Ljava/lang/String;

    return-void
.end method

.method public setQueryIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 200
    iput-object p1, p0, Lcom/evernote/edam/type/SavedSearch;->query:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setScope(Lcom/evernote/edam/type/SavedSearchScope;)V
    .locals 0

    .line 262
    iput-object p1, p0, Lcom/evernote/edam/type/SavedSearch;->scope:Lcom/evernote/edam/type/SavedSearchScope;

    return-void
.end method

.method public setScopeIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 276
    iput-object p1, p0, Lcom/evernote/edam/type/SavedSearch;->scope:Lcom/evernote/edam/type/SavedSearchScope;

    :cond_0
    return-void
.end method

.method public setUpdateSequenceNum(I)V
    .locals 0

    .line 240
    iput p1, p0, Lcom/evernote/edam/type/SavedSearch;->updateSequenceNum:I

    const/4 p1, 0x1

    .line 241
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/SavedSearch;->setUpdateSequenceNumIsSet(Z)V

    return-void
.end method

.method public setUpdateSequenceNumIsSet(Z)V
    .locals 2

    .line 254
    iget-object v0, p0, Lcom/evernote/edam/type/SavedSearch;->__isset_vector:[Z

    const/4 v1, 0x0

    aput-boolean p1, v0, v1

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 533
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SavedSearch("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 536
    invoke-virtual {p0}, Lcom/evernote/edam/type/SavedSearch;->isSetGuid()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    const-string v1, "guid:"

    .line 537
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 538
    iget-object v1, p0, Lcom/evernote/edam/type/SavedSearch;->guid:Ljava/lang/String;

    if-nez v1, :cond_0

    const-string v1, "null"

    .line 539
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 541
    :cond_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    const/4 v1, 0x1

    .line 545
    :goto_1
    invoke-virtual {p0}, Lcom/evernote/edam/type/SavedSearch;->isSetName()Z

    move-result v3

    if-eqz v3, :cond_4

    if-nez v1, :cond_2

    const-string v1, ", "

    .line 546
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const-string v1, "name:"

    .line 547
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 548
    iget-object v1, p0, Lcom/evernote/edam/type/SavedSearch;->name:Ljava/lang/String;

    if-nez v1, :cond_3

    const-string v1, "null"

    .line 549
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 551
    :cond_3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_2
    const/4 v1, 0x0

    .line 555
    :cond_4
    invoke-virtual {p0}, Lcom/evernote/edam/type/SavedSearch;->isSetQuery()Z

    move-result v3

    if-eqz v3, :cond_7

    if-nez v1, :cond_5

    const-string v1, ", "

    .line 556
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    const-string v1, "query:"

    .line 557
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 558
    iget-object v1, p0, Lcom/evernote/edam/type/SavedSearch;->query:Ljava/lang/String;

    if-nez v1, :cond_6

    const-string v1, "null"

    .line 559
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 561
    :cond_6
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    const/4 v1, 0x0

    .line 565
    :cond_7
    invoke-virtual {p0}, Lcom/evernote/edam/type/SavedSearch;->isSetFormat()Z

    move-result v3

    if-eqz v3, :cond_a

    if-nez v1, :cond_8

    const-string v1, ", "

    .line 566
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_8
    const-string v1, "format:"

    .line 567
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 568
    iget-object v1, p0, Lcom/evernote/edam/type/SavedSearch;->format:Lcom/evernote/edam/type/QueryFormat;

    if-nez v1, :cond_9

    const-string v1, "null"

    .line 569
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 571
    :cond_9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :goto_4
    const/4 v1, 0x0

    .line 575
    :cond_a
    invoke-virtual {p0}, Lcom/evernote/edam/type/SavedSearch;->isSetUpdateSequenceNum()Z

    move-result v3

    if-eqz v3, :cond_c

    if-nez v1, :cond_b

    const-string v1, ", "

    .line 576
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_b
    const-string v1, "updateSequenceNum:"

    .line 577
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 578
    iget v1, p0, Lcom/evernote/edam/type/SavedSearch;->updateSequenceNum:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 581
    :cond_c
    invoke-virtual {p0}, Lcom/evernote/edam/type/SavedSearch;->isSetScope()Z

    move-result v2

    if-eqz v2, :cond_f

    if-nez v1, :cond_d

    const-string v1, ", "

    .line 582
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_d
    const-string v1, "scope:"

    .line 583
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 584
    iget-object v1, p0, Lcom/evernote/edam/type/SavedSearch;->scope:Lcom/evernote/edam/type/SavedSearchScope;

    if-nez v1, :cond_e

    const-string v1, "null"

    .line 585
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5

    .line 587
    :cond_e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_f
    :goto_5
    const-string v1, ")"

    .line 591
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 592
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public unsetFormat()V
    .locals 1

    const/4 v0, 0x0

    .line 221
    iput-object v0, p0, Lcom/evernote/edam/type/SavedSearch;->format:Lcom/evernote/edam/type/QueryFormat;

    return-void
.end method

.method public unsetGuid()V
    .locals 1

    const/4 v0, 0x0

    .line 144
    iput-object v0, p0, Lcom/evernote/edam/type/SavedSearch;->guid:Ljava/lang/String;

    return-void
.end method

.method public unsetName()V
    .locals 1

    const/4 v0, 0x0

    .line 167
    iput-object v0, p0, Lcom/evernote/edam/type/SavedSearch;->name:Ljava/lang/String;

    return-void
.end method

.method public unsetQuery()V
    .locals 1

    const/4 v0, 0x0

    .line 190
    iput-object v0, p0, Lcom/evernote/edam/type/SavedSearch;->query:Ljava/lang/String;

    return-void
.end method

.method public unsetScope()V
    .locals 1

    const/4 v0, 0x0

    .line 266
    iput-object v0, p0, Lcom/evernote/edam/type/SavedSearch;->scope:Lcom/evernote/edam/type/SavedSearchScope;

    return-void
.end method

.method public unsetUpdateSequenceNum()V
    .locals 2

    .line 245
    iget-object v0, p0, Lcom/evernote/edam/type/SavedSearch;->__isset_vector:[Z

    const/4 v1, 0x0

    aput-boolean v1, v0, v1

    return-void
.end method

.method public validate()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    return-void
.end method

.method public write(Lcom/evernote/thrift/protocol/TProtocol;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 484
    invoke-virtual {p0}, Lcom/evernote/edam/type/SavedSearch;->validate()V

    .line 486
    sget-object v0, Lcom/evernote/edam/type/SavedSearch;->STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeStructBegin(Lcom/evernote/thrift/protocol/TStruct;)V

    .line 487
    iget-object v0, p0, Lcom/evernote/edam/type/SavedSearch;->guid:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 488
    invoke-virtual {p0}, Lcom/evernote/edam/type/SavedSearch;->isSetGuid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 489
    sget-object v0, Lcom/evernote/edam/type/SavedSearch;->GUID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 490
    iget-object v0, p0, Lcom/evernote/edam/type/SavedSearch;->guid:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 491
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 494
    :cond_0
    iget-object v0, p0, Lcom/evernote/edam/type/SavedSearch;->name:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 495
    invoke-virtual {p0}, Lcom/evernote/edam/type/SavedSearch;->isSetName()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 496
    sget-object v0, Lcom/evernote/edam/type/SavedSearch;->NAME_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 497
    iget-object v0, p0, Lcom/evernote/edam/type/SavedSearch;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 498
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 501
    :cond_1
    iget-object v0, p0, Lcom/evernote/edam/type/SavedSearch;->query:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 502
    invoke-virtual {p0}, Lcom/evernote/edam/type/SavedSearch;->isSetQuery()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 503
    sget-object v0, Lcom/evernote/edam/type/SavedSearch;->QUERY_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 504
    iget-object v0, p0, Lcom/evernote/edam/type/SavedSearch;->query:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 505
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 508
    :cond_2
    iget-object v0, p0, Lcom/evernote/edam/type/SavedSearch;->format:Lcom/evernote/edam/type/QueryFormat;

    if-eqz v0, :cond_3

    .line 509
    invoke-virtual {p0}, Lcom/evernote/edam/type/SavedSearch;->isSetFormat()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 510
    sget-object v0, Lcom/evernote/edam/type/SavedSearch;->FORMAT_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 511
    iget-object v0, p0, Lcom/evernote/edam/type/SavedSearch;->format:Lcom/evernote/edam/type/QueryFormat;

    invoke-virtual {v0}, Lcom/evernote/edam/type/QueryFormat;->getValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeI32(I)V

    .line 512
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 515
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/type/SavedSearch;->isSetUpdateSequenceNum()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 516
    sget-object v0, Lcom/evernote/edam/type/SavedSearch;->UPDATE_SEQUENCE_NUM_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 517
    iget v0, p0, Lcom/evernote/edam/type/SavedSearch;->updateSequenceNum:I

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeI32(I)V

    .line 518
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 520
    :cond_4
    iget-object v0, p0, Lcom/evernote/edam/type/SavedSearch;->scope:Lcom/evernote/edam/type/SavedSearchScope;

    if-eqz v0, :cond_5

    .line 521
    invoke-virtual {p0}, Lcom/evernote/edam/type/SavedSearch;->isSetScope()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 522
    sget-object v0, Lcom/evernote/edam/type/SavedSearch;->SCOPE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 523
    iget-object v0, p0, Lcom/evernote/edam/type/SavedSearch;->scope:Lcom/evernote/edam/type/SavedSearchScope;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/type/SavedSearchScope;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 524
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 527
    :cond_5
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldStop()V

    .line 528
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeStructEnd()V

    return-void
.end method

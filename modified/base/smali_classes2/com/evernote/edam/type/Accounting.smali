.class public Lcom/evernote/edam/type/Accounting;
.super Ljava/lang/Object;
.source "Accounting.java"

# interfaces
.implements Lcom/evernote/thrift/TBase;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/evernote/thrift/TBase<",
        "Lcom/evernote/edam/type/Accounting;",
        ">;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# static fields
.field private static final BUSINESS_ID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final BUSINESS_NAME_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final BUSINESS_ROLE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final CURRENCY_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final LAST_FAILED_CHARGE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final LAST_FAILED_CHARGE_REASON_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final LAST_REQUESTED_CHARGE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final LAST_SUCCESSFUL_CHARGE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final NEXT_CHARGE_DATE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final NEXT_PAYMENT_DUE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final PREMIUM_COMMERCE_SERVICE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final PREMIUM_LOCK_UNTIL_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final PREMIUM_ORDER_NUMBER_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final PREMIUM_SERVICE_SKU_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final PREMIUM_SERVICE_START_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final PREMIUM_SERVICE_STATUS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final PREMIUM_SUBSCRIPTION_NUMBER_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

.field private static final UNIT_DISCOUNT_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final UNIT_PRICE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final UPDATED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final UPLOAD_LIMIT_END_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final UPLOAD_LIMIT_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final UPLOAD_LIMIT_NEXT_MONTH_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final __BUSINESSID_ISSET_ID:I = 0xb

.field private static final __LASTFAILEDCHARGE_ISSET_ID:I = 0x5

.field private static final __LASTREQUESTEDCHARGE_ISSET_ID:I = 0x9

.field private static final __LASTSUCCESSFULCHARGE_ISSET_ID:I = 0x4

.field private static final __NEXTCHARGEDATE_ISSET_ID:I = 0xd

.field private static final __NEXTPAYMENTDUE_ISSET_ID:I = 0x6

.field private static final __PREMIUMLOCKUNTIL_ISSET_ID:I = 0x7

.field private static final __PREMIUMSERVICESTART_ISSET_ID:I = 0x3

.field private static final __UNITDISCOUNT_ISSET_ID:I = 0xc

.field private static final __UNITPRICE_ISSET_ID:I = 0xa

.field private static final __UPDATED_ISSET_ID:I = 0x8

.field private static final __UPLOADLIMITEND_ISSET_ID:I = 0x1

.field private static final __UPLOADLIMITNEXTMONTH_ISSET_ID:I = 0x2

.field private static final __UPLOADLIMIT_ISSET_ID:I


# instance fields
.field private __isset_vector:[Z

.field private businessId:I

.field private businessName:Ljava/lang/String;

.field private businessRole:Lcom/evernote/edam/type/BusinessUserRole;

.field private currency:Ljava/lang/String;

.field private lastFailedCharge:J

.field private lastFailedChargeReason:Ljava/lang/String;

.field private lastRequestedCharge:J

.field private lastSuccessfulCharge:J

.field private nextChargeDate:J

.field private nextPaymentDue:J

.field private premiumCommerceService:Ljava/lang/String;

.field private premiumLockUntil:J

.field private premiumOrderNumber:Ljava/lang/String;

.field private premiumServiceSKU:Ljava/lang/String;

.field private premiumServiceStart:J

.field private premiumServiceStatus:Lcom/evernote/edam/type/PremiumOrderStatus;

.field private premiumSubscriptionNumber:Ljava/lang/String;

.field private unitDiscount:I

.field private unitPrice:I

.field private updated:J

.field private uploadLimit:J

.field private uploadLimitEnd:J

.field private uploadLimitNextMonth:J


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 107
    new-instance v0, Lcom/evernote/thrift/protocol/TStruct;

    const-string v1, "Accounting"

    invoke-direct {v0, v1}, Lcom/evernote/thrift/protocol/TStruct;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/evernote/edam/type/Accounting;->STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

    .line 109
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "uploadLimit"

    const/16 v2, 0xa

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Accounting;->UPLOAD_LIMIT_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 110
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "uploadLimitEnd"

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Accounting;->UPLOAD_LIMIT_END_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 111
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "uploadLimitNextMonth"

    const/4 v3, 0x3

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Accounting;->UPLOAD_LIMIT_NEXT_MONTH_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 112
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "premiumServiceStatus"

    const/16 v3, 0x8

    const/4 v4, 0x4

    invoke-direct {v0, v1, v3, v4}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Accounting;->PREMIUM_SERVICE_STATUS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 113
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "premiumOrderNumber"

    const/16 v4, 0xb

    const/4 v5, 0x5

    invoke-direct {v0, v1, v4, v5}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Accounting;->PREMIUM_ORDER_NUMBER_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 114
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "premiumCommerceService"

    const/4 v5, 0x6

    invoke-direct {v0, v1, v4, v5}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Accounting;->PREMIUM_COMMERCE_SERVICE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 115
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "premiumServiceStart"

    const/4 v5, 0x7

    invoke-direct {v0, v1, v2, v5}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Accounting;->PREMIUM_SERVICE_START_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 116
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "premiumServiceSKU"

    invoke-direct {v0, v1, v4, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Accounting;->PREMIUM_SERVICE_SKU_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 117
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "lastSuccessfulCharge"

    const/16 v5, 0x9

    invoke-direct {v0, v1, v2, v5}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Accounting;->LAST_SUCCESSFUL_CHARGE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 118
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "lastFailedCharge"

    invoke-direct {v0, v1, v2, v2}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Accounting;->LAST_FAILED_CHARGE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 119
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "lastFailedChargeReason"

    invoke-direct {v0, v1, v4, v4}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Accounting;->LAST_FAILED_CHARGE_REASON_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 120
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "nextPaymentDue"

    const/16 v5, 0xc

    invoke-direct {v0, v1, v2, v5}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Accounting;->NEXT_PAYMENT_DUE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 121
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "premiumLockUntil"

    const/16 v5, 0xd

    invoke-direct {v0, v1, v2, v5}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Accounting;->PREMIUM_LOCK_UNTIL_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 122
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "updated"

    const/16 v5, 0xe

    invoke-direct {v0, v1, v2, v5}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Accounting;->UPDATED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 123
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "premiumSubscriptionNumber"

    const/16 v5, 0x10

    invoke-direct {v0, v1, v4, v5}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Accounting;->PREMIUM_SUBSCRIPTION_NUMBER_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 124
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "lastRequestedCharge"

    const/16 v5, 0x11

    invoke-direct {v0, v1, v2, v5}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Accounting;->LAST_REQUESTED_CHARGE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 125
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "currency"

    const/16 v5, 0x12

    invoke-direct {v0, v1, v4, v5}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Accounting;->CURRENCY_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 126
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "unitPrice"

    const/16 v5, 0x13

    invoke-direct {v0, v1, v3, v5}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Accounting;->UNIT_PRICE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 127
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "businessId"

    const/16 v5, 0x14

    invoke-direct {v0, v1, v3, v5}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Accounting;->BUSINESS_ID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 128
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "businessName"

    const/16 v5, 0x15

    invoke-direct {v0, v1, v4, v5}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Accounting;->BUSINESS_NAME_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 129
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "businessRole"

    const/16 v4, 0x16

    invoke-direct {v0, v1, v3, v4}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Accounting;->BUSINESS_ROLE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 130
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "unitDiscount"

    const/16 v4, 0x17

    invoke-direct {v0, v1, v3, v4}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Accounting;->UNIT_DISCOUNT_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 131
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "nextChargeDate"

    const/16 v3, 0x18

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Accounting;->NEXT_CHARGE_DATE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0xe

    .line 173
    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/evernote/edam/type/Accounting;->__isset_vector:[Z

    return-void
.end method

.method public constructor <init>(Lcom/evernote/edam/type/Accounting;)V
    .locals 4

    .line 181
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0xe

    .line 173
    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/evernote/edam/type/Accounting;->__isset_vector:[Z

    .line 182
    iget-object v0, p1, Lcom/evernote/edam/type/Accounting;->__isset_vector:[Z

    iget-object v1, p0, Lcom/evernote/edam/type/Accounting;->__isset_vector:[Z

    array-length v2, v0

    const/4 v3, 0x0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 183
    iget-wide v0, p1, Lcom/evernote/edam/type/Accounting;->uploadLimit:J

    iput-wide v0, p0, Lcom/evernote/edam/type/Accounting;->uploadLimit:J

    .line 184
    iget-wide v0, p1, Lcom/evernote/edam/type/Accounting;->uploadLimitEnd:J

    iput-wide v0, p0, Lcom/evernote/edam/type/Accounting;->uploadLimitEnd:J

    .line 185
    iget-wide v0, p1, Lcom/evernote/edam/type/Accounting;->uploadLimitNextMonth:J

    iput-wide v0, p0, Lcom/evernote/edam/type/Accounting;->uploadLimitNextMonth:J

    .line 186
    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetPremiumServiceStatus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p1, Lcom/evernote/edam/type/Accounting;->premiumServiceStatus:Lcom/evernote/edam/type/PremiumOrderStatus;

    iput-object v0, p0, Lcom/evernote/edam/type/Accounting;->premiumServiceStatus:Lcom/evernote/edam/type/PremiumOrderStatus;

    .line 189
    :cond_0
    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetPremiumOrderNumber()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 190
    iget-object v0, p1, Lcom/evernote/edam/type/Accounting;->premiumOrderNumber:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/type/Accounting;->premiumOrderNumber:Ljava/lang/String;

    .line 192
    :cond_1
    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetPremiumCommerceService()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 193
    iget-object v0, p1, Lcom/evernote/edam/type/Accounting;->premiumCommerceService:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/type/Accounting;->premiumCommerceService:Ljava/lang/String;

    .line 195
    :cond_2
    iget-wide v0, p1, Lcom/evernote/edam/type/Accounting;->premiumServiceStart:J

    iput-wide v0, p0, Lcom/evernote/edam/type/Accounting;->premiumServiceStart:J

    .line 196
    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetPremiumServiceSKU()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 197
    iget-object v0, p1, Lcom/evernote/edam/type/Accounting;->premiumServiceSKU:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/type/Accounting;->premiumServiceSKU:Ljava/lang/String;

    .line 199
    :cond_3
    iget-wide v0, p1, Lcom/evernote/edam/type/Accounting;->lastSuccessfulCharge:J

    iput-wide v0, p0, Lcom/evernote/edam/type/Accounting;->lastSuccessfulCharge:J

    .line 200
    iget-wide v0, p1, Lcom/evernote/edam/type/Accounting;->lastFailedCharge:J

    iput-wide v0, p0, Lcom/evernote/edam/type/Accounting;->lastFailedCharge:J

    .line 201
    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetLastFailedChargeReason()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 202
    iget-object v0, p1, Lcom/evernote/edam/type/Accounting;->lastFailedChargeReason:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/type/Accounting;->lastFailedChargeReason:Ljava/lang/String;

    .line 204
    :cond_4
    iget-wide v0, p1, Lcom/evernote/edam/type/Accounting;->nextPaymentDue:J

    iput-wide v0, p0, Lcom/evernote/edam/type/Accounting;->nextPaymentDue:J

    .line 205
    iget-wide v0, p1, Lcom/evernote/edam/type/Accounting;->premiumLockUntil:J

    iput-wide v0, p0, Lcom/evernote/edam/type/Accounting;->premiumLockUntil:J

    .line 206
    iget-wide v0, p1, Lcom/evernote/edam/type/Accounting;->updated:J

    iput-wide v0, p0, Lcom/evernote/edam/type/Accounting;->updated:J

    .line 207
    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetPremiumSubscriptionNumber()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 208
    iget-object v0, p1, Lcom/evernote/edam/type/Accounting;->premiumSubscriptionNumber:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/type/Accounting;->premiumSubscriptionNumber:Ljava/lang/String;

    .line 210
    :cond_5
    iget-wide v0, p1, Lcom/evernote/edam/type/Accounting;->lastRequestedCharge:J

    iput-wide v0, p0, Lcom/evernote/edam/type/Accounting;->lastRequestedCharge:J

    .line 211
    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetCurrency()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 212
    iget-object v0, p1, Lcom/evernote/edam/type/Accounting;->currency:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/type/Accounting;->currency:Ljava/lang/String;

    .line 214
    :cond_6
    iget v0, p1, Lcom/evernote/edam/type/Accounting;->unitPrice:I

    iput v0, p0, Lcom/evernote/edam/type/Accounting;->unitPrice:I

    .line 215
    iget v0, p1, Lcom/evernote/edam/type/Accounting;->businessId:I

    iput v0, p0, Lcom/evernote/edam/type/Accounting;->businessId:I

    .line 216
    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetBusinessName()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 217
    iget-object v0, p1, Lcom/evernote/edam/type/Accounting;->businessName:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/type/Accounting;->businessName:Ljava/lang/String;

    .line 219
    :cond_7
    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetBusinessRole()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 220
    iget-object v0, p1, Lcom/evernote/edam/type/Accounting;->businessRole:Lcom/evernote/edam/type/BusinessUserRole;

    iput-object v0, p0, Lcom/evernote/edam/type/Accounting;->businessRole:Lcom/evernote/edam/type/BusinessUserRole;

    .line 222
    :cond_8
    iget v0, p1, Lcom/evernote/edam/type/Accounting;->unitDiscount:I

    iput v0, p0, Lcom/evernote/edam/type/Accounting;->unitDiscount:I

    .line 223
    iget-wide v0, p1, Lcom/evernote/edam/type/Accounting;->nextChargeDate:J

    iput-wide v0, p0, Lcom/evernote/edam/type/Accounting;->nextChargeDate:J

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 4

    const/4 v0, 0x0

    .line 231
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/Accounting;->setUploadLimitIsSet(Z)V

    const-wide/16 v1, 0x0

    .line 232
    iput-wide v1, p0, Lcom/evernote/edam/type/Accounting;->uploadLimit:J

    .line 233
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/Accounting;->setUploadLimitEndIsSet(Z)V

    .line 234
    iput-wide v1, p0, Lcom/evernote/edam/type/Accounting;->uploadLimitEnd:J

    .line 235
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/Accounting;->setUploadLimitNextMonthIsSet(Z)V

    .line 236
    iput-wide v1, p0, Lcom/evernote/edam/type/Accounting;->uploadLimitNextMonth:J

    const/4 v3, 0x0

    .line 237
    iput-object v3, p0, Lcom/evernote/edam/type/Accounting;->premiumServiceStatus:Lcom/evernote/edam/type/PremiumOrderStatus;

    .line 238
    iput-object v3, p0, Lcom/evernote/edam/type/Accounting;->premiumOrderNumber:Ljava/lang/String;

    .line 239
    iput-object v3, p0, Lcom/evernote/edam/type/Accounting;->premiumCommerceService:Ljava/lang/String;

    .line 240
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/Accounting;->setPremiumServiceStartIsSet(Z)V

    .line 241
    iput-wide v1, p0, Lcom/evernote/edam/type/Accounting;->premiumServiceStart:J

    .line 242
    iput-object v3, p0, Lcom/evernote/edam/type/Accounting;->premiumServiceSKU:Ljava/lang/String;

    .line 243
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/Accounting;->setLastSuccessfulChargeIsSet(Z)V

    .line 244
    iput-wide v1, p0, Lcom/evernote/edam/type/Accounting;->lastSuccessfulCharge:J

    .line 245
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/Accounting;->setLastFailedChargeIsSet(Z)V

    .line 246
    iput-wide v1, p0, Lcom/evernote/edam/type/Accounting;->lastFailedCharge:J

    .line 247
    iput-object v3, p0, Lcom/evernote/edam/type/Accounting;->lastFailedChargeReason:Ljava/lang/String;

    .line 248
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/Accounting;->setNextPaymentDueIsSet(Z)V

    .line 249
    iput-wide v1, p0, Lcom/evernote/edam/type/Accounting;->nextPaymentDue:J

    .line 250
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/Accounting;->setPremiumLockUntilIsSet(Z)V

    .line 251
    iput-wide v1, p0, Lcom/evernote/edam/type/Accounting;->premiumLockUntil:J

    .line 252
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/Accounting;->setUpdatedIsSet(Z)V

    .line 253
    iput-wide v1, p0, Lcom/evernote/edam/type/Accounting;->updated:J

    .line 254
    iput-object v3, p0, Lcom/evernote/edam/type/Accounting;->premiumSubscriptionNumber:Ljava/lang/String;

    .line 255
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/Accounting;->setLastRequestedChargeIsSet(Z)V

    .line 256
    iput-wide v1, p0, Lcom/evernote/edam/type/Accounting;->lastRequestedCharge:J

    .line 257
    iput-object v3, p0, Lcom/evernote/edam/type/Accounting;->currency:Ljava/lang/String;

    .line 258
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/Accounting;->setUnitPriceIsSet(Z)V

    .line 259
    iput v0, p0, Lcom/evernote/edam/type/Accounting;->unitPrice:I

    .line 260
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/Accounting;->setBusinessIdIsSet(Z)V

    .line 261
    iput v0, p0, Lcom/evernote/edam/type/Accounting;->businessId:I

    .line 262
    iput-object v3, p0, Lcom/evernote/edam/type/Accounting;->businessName:Ljava/lang/String;

    .line 263
    iput-object v3, p0, Lcom/evernote/edam/type/Accounting;->businessRole:Lcom/evernote/edam/type/BusinessUserRole;

    .line 264
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/Accounting;->setUnitDiscountIsSet(Z)V

    .line 265
    iput v0, p0, Lcom/evernote/edam/type/Accounting;->unitDiscount:I

    .line 266
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/Accounting;->setNextChargeDateIsSet(Z)V

    .line 267
    iput-wide v1, p0, Lcom/evernote/edam/type/Accounting;->nextChargeDate:J

    return-void
.end method

.method public compareTo(Lcom/evernote/edam/type/Accounting;)I
    .locals 4

    .line 1030
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1031
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    return p1

    .line 1037
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetUploadLimit()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetUploadLimit()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_1

    return v0

    .line 1041
    :cond_1
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetUploadLimit()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-wide v0, p0, Lcom/evernote/edam/type/Accounting;->uploadLimit:J

    iget-wide v2, p1, Lcom/evernote/edam/type/Accounting;->uploadLimit:J

    invoke-static {v0, v1, v2, v3}, Lcom/evernote/thrift/TBaseHelper;->compareTo(JJ)I

    move-result v0

    if-eqz v0, :cond_2

    return v0

    .line 1046
    :cond_2
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetUploadLimitEnd()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetUploadLimitEnd()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_3

    return v0

    .line 1050
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetUploadLimitEnd()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-wide v0, p0, Lcom/evernote/edam/type/Accounting;->uploadLimitEnd:J

    iget-wide v2, p1, Lcom/evernote/edam/type/Accounting;->uploadLimitEnd:J

    invoke-static {v0, v1, v2, v3}, Lcom/evernote/thrift/TBaseHelper;->compareTo(JJ)I

    move-result v0

    if-eqz v0, :cond_4

    return v0

    .line 1055
    :cond_4
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetUploadLimitNextMonth()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetUploadLimitNextMonth()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_5

    return v0

    .line 1059
    :cond_5
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetUploadLimitNextMonth()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-wide v0, p0, Lcom/evernote/edam/type/Accounting;->uploadLimitNextMonth:J

    iget-wide v2, p1, Lcom/evernote/edam/type/Accounting;->uploadLimitNextMonth:J

    invoke-static {v0, v1, v2, v3}, Lcom/evernote/thrift/TBaseHelper;->compareTo(JJ)I

    move-result v0

    if-eqz v0, :cond_6

    return v0

    .line 1064
    :cond_6
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetPremiumServiceStatus()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetPremiumServiceStatus()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_7

    return v0

    .line 1068
    :cond_7
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetPremiumServiceStatus()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->premiumServiceStatus:Lcom/evernote/edam/type/PremiumOrderStatus;

    iget-object v1, p1, Lcom/evernote/edam/type/Accounting;->premiumServiceStatus:Lcom/evernote/edam/type/PremiumOrderStatus;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result v0

    if-eqz v0, :cond_8

    return v0

    .line 1073
    :cond_8
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetPremiumOrderNumber()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetPremiumOrderNumber()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_9

    return v0

    .line 1077
    :cond_9
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetPremiumOrderNumber()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->premiumOrderNumber:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/type/Accounting;->premiumOrderNumber:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_a

    return v0

    .line 1082
    :cond_a
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetPremiumCommerceService()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetPremiumCommerceService()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_b

    return v0

    .line 1086
    :cond_b
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetPremiumCommerceService()Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->premiumCommerceService:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/type/Accounting;->premiumCommerceService:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_c

    return v0

    .line 1091
    :cond_c
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetPremiumServiceStart()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetPremiumServiceStart()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_d

    return v0

    .line 1095
    :cond_d
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetPremiumServiceStart()Z

    move-result v0

    if-eqz v0, :cond_e

    iget-wide v0, p0, Lcom/evernote/edam/type/Accounting;->premiumServiceStart:J

    iget-wide v2, p1, Lcom/evernote/edam/type/Accounting;->premiumServiceStart:J

    invoke-static {v0, v1, v2, v3}, Lcom/evernote/thrift/TBaseHelper;->compareTo(JJ)I

    move-result v0

    if-eqz v0, :cond_e

    return v0

    .line 1100
    :cond_e
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetPremiumServiceSKU()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetPremiumServiceSKU()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_f

    return v0

    .line 1104
    :cond_f
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetPremiumServiceSKU()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->premiumServiceSKU:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/type/Accounting;->premiumServiceSKU:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_10

    return v0

    .line 1109
    :cond_10
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetLastSuccessfulCharge()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetLastSuccessfulCharge()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_11

    return v0

    .line 1113
    :cond_11
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetLastSuccessfulCharge()Z

    move-result v0

    if-eqz v0, :cond_12

    iget-wide v0, p0, Lcom/evernote/edam/type/Accounting;->lastSuccessfulCharge:J

    iget-wide v2, p1, Lcom/evernote/edam/type/Accounting;->lastSuccessfulCharge:J

    invoke-static {v0, v1, v2, v3}, Lcom/evernote/thrift/TBaseHelper;->compareTo(JJ)I

    move-result v0

    if-eqz v0, :cond_12

    return v0

    .line 1118
    :cond_12
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetLastFailedCharge()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetLastFailedCharge()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_13

    return v0

    .line 1122
    :cond_13
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetLastFailedCharge()Z

    move-result v0

    if-eqz v0, :cond_14

    iget-wide v0, p0, Lcom/evernote/edam/type/Accounting;->lastFailedCharge:J

    iget-wide v2, p1, Lcom/evernote/edam/type/Accounting;->lastFailedCharge:J

    invoke-static {v0, v1, v2, v3}, Lcom/evernote/thrift/TBaseHelper;->compareTo(JJ)I

    move-result v0

    if-eqz v0, :cond_14

    return v0

    .line 1127
    :cond_14
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetLastFailedChargeReason()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetLastFailedChargeReason()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_15

    return v0

    .line 1131
    :cond_15
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetLastFailedChargeReason()Z

    move-result v0

    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->lastFailedChargeReason:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/type/Accounting;->lastFailedChargeReason:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_16

    return v0

    .line 1136
    :cond_16
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetNextPaymentDue()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetNextPaymentDue()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_17

    return v0

    .line 1140
    :cond_17
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetNextPaymentDue()Z

    move-result v0

    if-eqz v0, :cond_18

    iget-wide v0, p0, Lcom/evernote/edam/type/Accounting;->nextPaymentDue:J

    iget-wide v2, p1, Lcom/evernote/edam/type/Accounting;->nextPaymentDue:J

    invoke-static {v0, v1, v2, v3}, Lcom/evernote/thrift/TBaseHelper;->compareTo(JJ)I

    move-result v0

    if-eqz v0, :cond_18

    return v0

    .line 1145
    :cond_18
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetPremiumLockUntil()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetPremiumLockUntil()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_19

    return v0

    .line 1149
    :cond_19
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetPremiumLockUntil()Z

    move-result v0

    if-eqz v0, :cond_1a

    iget-wide v0, p0, Lcom/evernote/edam/type/Accounting;->premiumLockUntil:J

    iget-wide v2, p1, Lcom/evernote/edam/type/Accounting;->premiumLockUntil:J

    invoke-static {v0, v1, v2, v3}, Lcom/evernote/thrift/TBaseHelper;->compareTo(JJ)I

    move-result v0

    if-eqz v0, :cond_1a

    return v0

    .line 1154
    :cond_1a
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetUpdated()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetUpdated()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_1b

    return v0

    .line 1158
    :cond_1b
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetUpdated()Z

    move-result v0

    if-eqz v0, :cond_1c

    iget-wide v0, p0, Lcom/evernote/edam/type/Accounting;->updated:J

    iget-wide v2, p1, Lcom/evernote/edam/type/Accounting;->updated:J

    invoke-static {v0, v1, v2, v3}, Lcom/evernote/thrift/TBaseHelper;->compareTo(JJ)I

    move-result v0

    if-eqz v0, :cond_1c

    return v0

    .line 1163
    :cond_1c
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetPremiumSubscriptionNumber()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetPremiumSubscriptionNumber()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_1d

    return v0

    .line 1167
    :cond_1d
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetPremiumSubscriptionNumber()Z

    move-result v0

    if-eqz v0, :cond_1e

    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->premiumSubscriptionNumber:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/type/Accounting;->premiumSubscriptionNumber:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_1e

    return v0

    .line 1172
    :cond_1e
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetLastRequestedCharge()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetLastRequestedCharge()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_1f

    return v0

    .line 1176
    :cond_1f
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetLastRequestedCharge()Z

    move-result v0

    if-eqz v0, :cond_20

    iget-wide v0, p0, Lcom/evernote/edam/type/Accounting;->lastRequestedCharge:J

    iget-wide v2, p1, Lcom/evernote/edam/type/Accounting;->lastRequestedCharge:J

    invoke-static {v0, v1, v2, v3}, Lcom/evernote/thrift/TBaseHelper;->compareTo(JJ)I

    move-result v0

    if-eqz v0, :cond_20

    return v0

    .line 1181
    :cond_20
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetCurrency()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetCurrency()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_21

    return v0

    .line 1185
    :cond_21
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetCurrency()Z

    move-result v0

    if-eqz v0, :cond_22

    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->currency:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/type/Accounting;->currency:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_22

    return v0

    .line 1190
    :cond_22
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetUnitPrice()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetUnitPrice()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_23

    return v0

    .line 1194
    :cond_23
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetUnitPrice()Z

    move-result v0

    if-eqz v0, :cond_24

    iget v0, p0, Lcom/evernote/edam/type/Accounting;->unitPrice:I

    iget v1, p1, Lcom/evernote/edam/type/Accounting;->unitPrice:I

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(II)I

    move-result v0

    if-eqz v0, :cond_24

    return v0

    .line 1199
    :cond_24
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetBusinessId()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetBusinessId()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_25

    return v0

    .line 1203
    :cond_25
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetBusinessId()Z

    move-result v0

    if-eqz v0, :cond_26

    iget v0, p0, Lcom/evernote/edam/type/Accounting;->businessId:I

    iget v1, p1, Lcom/evernote/edam/type/Accounting;->businessId:I

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(II)I

    move-result v0

    if-eqz v0, :cond_26

    return v0

    .line 1208
    :cond_26
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetBusinessName()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetBusinessName()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_27

    return v0

    .line 1212
    :cond_27
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetBusinessName()Z

    move-result v0

    if-eqz v0, :cond_28

    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->businessName:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/type/Accounting;->businessName:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_28

    return v0

    .line 1217
    :cond_28
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetBusinessRole()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetBusinessRole()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_29

    return v0

    .line 1221
    :cond_29
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetBusinessRole()Z

    move-result v0

    if-eqz v0, :cond_2a

    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->businessRole:Lcom/evernote/edam/type/BusinessUserRole;

    iget-object v1, p1, Lcom/evernote/edam/type/Accounting;->businessRole:Lcom/evernote/edam/type/BusinessUserRole;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result v0

    if-eqz v0, :cond_2a

    return v0

    .line 1226
    :cond_2a
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetUnitDiscount()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetUnitDiscount()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_2b

    return v0

    .line 1230
    :cond_2b
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetUnitDiscount()Z

    move-result v0

    if-eqz v0, :cond_2c

    iget v0, p0, Lcom/evernote/edam/type/Accounting;->unitDiscount:I

    iget v1, p1, Lcom/evernote/edam/type/Accounting;->unitDiscount:I

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(II)I

    move-result v0

    if-eqz v0, :cond_2c

    return v0

    .line 1235
    :cond_2c
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetNextChargeDate()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetNextChargeDate()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_2d

    return v0

    .line 1239
    :cond_2d
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetNextChargeDate()Z

    move-result v0

    if-eqz v0, :cond_2e

    iget-wide v0, p0, Lcom/evernote/edam/type/Accounting;->nextChargeDate:J

    iget-wide v2, p1, Lcom/evernote/edam/type/Accounting;->nextChargeDate:J

    invoke-static {v0, v1, v2, v3}, Lcom/evernote/thrift/TBaseHelper;->compareTo(JJ)I

    move-result p1

    if-eqz p1, :cond_2e

    return p1

    :cond_2e
    const/4 p1, 0x0

    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 106
    check-cast p1, Lcom/evernote/edam/type/Accounting;

    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/Accounting;->compareTo(Lcom/evernote/edam/type/Accounting;)I

    move-result p1

    return p1
.end method

.method public deepCopy()Lcom/evernote/edam/type/Accounting;
    .locals 1

    .line 227
    new-instance v0, Lcom/evernote/edam/type/Accounting;

    invoke-direct {v0, p0}, Lcom/evernote/edam/type/Accounting;-><init>(Lcom/evernote/edam/type/Accounting;)V

    return-object v0
.end method

.method public bridge synthetic deepCopy()Lcom/evernote/thrift/TBase;
    .locals 1

    .line 106
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->deepCopy()Lcom/evernote/edam/type/Accounting;

    move-result-object v0

    return-object v0
.end method

.method public equals(Lcom/evernote/edam/type/Accounting;)Z
    .locals 6

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 814
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetUploadLimit()Z

    move-result v1

    .line 815
    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetUploadLimit()Z

    move-result v2

    if-nez v1, :cond_1

    if-eqz v2, :cond_3

    :cond_1
    if-eqz v1, :cond_5c

    if-nez v2, :cond_2

    goto/16 :goto_16

    .line 819
    :cond_2
    iget-wide v1, p0, Lcom/evernote/edam/type/Accounting;->uploadLimit:J

    iget-wide v3, p1, Lcom/evernote/edam/type/Accounting;->uploadLimit:J

    cmp-long v5, v1, v3

    if-eqz v5, :cond_3

    return v0

    .line 823
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetUploadLimitEnd()Z

    move-result v1

    .line 824
    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetUploadLimitEnd()Z

    move-result v2

    if-nez v1, :cond_4

    if-eqz v2, :cond_6

    :cond_4
    if-eqz v1, :cond_5b

    if-nez v2, :cond_5

    goto/16 :goto_15

    .line 828
    :cond_5
    iget-wide v1, p0, Lcom/evernote/edam/type/Accounting;->uploadLimitEnd:J

    iget-wide v3, p1, Lcom/evernote/edam/type/Accounting;->uploadLimitEnd:J

    cmp-long v5, v1, v3

    if-eqz v5, :cond_6

    return v0

    .line 832
    :cond_6
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetUploadLimitNextMonth()Z

    move-result v1

    .line 833
    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetUploadLimitNextMonth()Z

    move-result v2

    if-nez v1, :cond_7

    if-eqz v2, :cond_9

    :cond_7
    if-eqz v1, :cond_5a

    if-nez v2, :cond_8

    goto/16 :goto_14

    .line 837
    :cond_8
    iget-wide v1, p0, Lcom/evernote/edam/type/Accounting;->uploadLimitNextMonth:J

    iget-wide v3, p1, Lcom/evernote/edam/type/Accounting;->uploadLimitNextMonth:J

    cmp-long v5, v1, v3

    if-eqz v5, :cond_9

    return v0

    .line 841
    :cond_9
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetPremiumServiceStatus()Z

    move-result v1

    .line 842
    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetPremiumServiceStatus()Z

    move-result v2

    if-nez v1, :cond_a

    if-eqz v2, :cond_c

    :cond_a
    if-eqz v1, :cond_59

    if-nez v2, :cond_b

    goto/16 :goto_13

    .line 846
    :cond_b
    iget-object v1, p0, Lcom/evernote/edam/type/Accounting;->premiumServiceStatus:Lcom/evernote/edam/type/PremiumOrderStatus;

    iget-object v2, p1, Lcom/evernote/edam/type/Accounting;->premiumServiceStatus:Lcom/evernote/edam/type/PremiumOrderStatus;

    invoke-virtual {v1, v2}, Lcom/evernote/edam/type/PremiumOrderStatus;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    return v0

    .line 850
    :cond_c
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetPremiumOrderNumber()Z

    move-result v1

    .line 851
    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetPremiumOrderNumber()Z

    move-result v2

    if-nez v1, :cond_d

    if-eqz v2, :cond_f

    :cond_d
    if-eqz v1, :cond_58

    if-nez v2, :cond_e

    goto/16 :goto_12

    .line 855
    :cond_e
    iget-object v1, p0, Lcom/evernote/edam/type/Accounting;->premiumOrderNumber:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/type/Accounting;->premiumOrderNumber:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    return v0

    .line 859
    :cond_f
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetPremiumCommerceService()Z

    move-result v1

    .line 860
    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetPremiumCommerceService()Z

    move-result v2

    if-nez v1, :cond_10

    if-eqz v2, :cond_12

    :cond_10
    if-eqz v1, :cond_57

    if-nez v2, :cond_11

    goto/16 :goto_11

    .line 864
    :cond_11
    iget-object v1, p0, Lcom/evernote/edam/type/Accounting;->premiumCommerceService:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/type/Accounting;->premiumCommerceService:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_12

    return v0

    .line 868
    :cond_12
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetPremiumServiceStart()Z

    move-result v1

    .line 869
    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetPremiumServiceStart()Z

    move-result v2

    if-nez v1, :cond_13

    if-eqz v2, :cond_15

    :cond_13
    if-eqz v1, :cond_56

    if-nez v2, :cond_14

    goto/16 :goto_10

    .line 873
    :cond_14
    iget-wide v1, p0, Lcom/evernote/edam/type/Accounting;->premiumServiceStart:J

    iget-wide v3, p1, Lcom/evernote/edam/type/Accounting;->premiumServiceStart:J

    cmp-long v5, v1, v3

    if-eqz v5, :cond_15

    return v0

    .line 877
    :cond_15
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetPremiumServiceSKU()Z

    move-result v1

    .line 878
    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetPremiumServiceSKU()Z

    move-result v2

    if-nez v1, :cond_16

    if-eqz v2, :cond_18

    :cond_16
    if-eqz v1, :cond_55

    if-nez v2, :cond_17

    goto/16 :goto_f

    .line 882
    :cond_17
    iget-object v1, p0, Lcom/evernote/edam/type/Accounting;->premiumServiceSKU:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/type/Accounting;->premiumServiceSKU:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_18

    return v0

    .line 886
    :cond_18
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetLastSuccessfulCharge()Z

    move-result v1

    .line 887
    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetLastSuccessfulCharge()Z

    move-result v2

    if-nez v1, :cond_19

    if-eqz v2, :cond_1b

    :cond_19
    if-eqz v1, :cond_54

    if-nez v2, :cond_1a

    goto/16 :goto_e

    .line 891
    :cond_1a
    iget-wide v1, p0, Lcom/evernote/edam/type/Accounting;->lastSuccessfulCharge:J

    iget-wide v3, p1, Lcom/evernote/edam/type/Accounting;->lastSuccessfulCharge:J

    cmp-long v5, v1, v3

    if-eqz v5, :cond_1b

    return v0

    .line 895
    :cond_1b
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetLastFailedCharge()Z

    move-result v1

    .line 896
    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetLastFailedCharge()Z

    move-result v2

    if-nez v1, :cond_1c

    if-eqz v2, :cond_1e

    :cond_1c
    if-eqz v1, :cond_53

    if-nez v2, :cond_1d

    goto/16 :goto_d

    .line 900
    :cond_1d
    iget-wide v1, p0, Lcom/evernote/edam/type/Accounting;->lastFailedCharge:J

    iget-wide v3, p1, Lcom/evernote/edam/type/Accounting;->lastFailedCharge:J

    cmp-long v5, v1, v3

    if-eqz v5, :cond_1e

    return v0

    .line 904
    :cond_1e
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetLastFailedChargeReason()Z

    move-result v1

    .line 905
    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetLastFailedChargeReason()Z

    move-result v2

    if-nez v1, :cond_1f

    if-eqz v2, :cond_21

    :cond_1f
    if-eqz v1, :cond_52

    if-nez v2, :cond_20

    goto/16 :goto_c

    .line 909
    :cond_20
    iget-object v1, p0, Lcom/evernote/edam/type/Accounting;->lastFailedChargeReason:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/type/Accounting;->lastFailedChargeReason:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_21

    return v0

    .line 913
    :cond_21
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetNextPaymentDue()Z

    move-result v1

    .line 914
    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetNextPaymentDue()Z

    move-result v2

    if-nez v1, :cond_22

    if-eqz v2, :cond_24

    :cond_22
    if-eqz v1, :cond_51

    if-nez v2, :cond_23

    goto/16 :goto_b

    .line 918
    :cond_23
    iget-wide v1, p0, Lcom/evernote/edam/type/Accounting;->nextPaymentDue:J

    iget-wide v3, p1, Lcom/evernote/edam/type/Accounting;->nextPaymentDue:J

    cmp-long v5, v1, v3

    if-eqz v5, :cond_24

    return v0

    .line 922
    :cond_24
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetPremiumLockUntil()Z

    move-result v1

    .line 923
    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetPremiumLockUntil()Z

    move-result v2

    if-nez v1, :cond_25

    if-eqz v2, :cond_27

    :cond_25
    if-eqz v1, :cond_50

    if-nez v2, :cond_26

    goto/16 :goto_a

    .line 927
    :cond_26
    iget-wide v1, p0, Lcom/evernote/edam/type/Accounting;->premiumLockUntil:J

    iget-wide v3, p1, Lcom/evernote/edam/type/Accounting;->premiumLockUntil:J

    cmp-long v5, v1, v3

    if-eqz v5, :cond_27

    return v0

    .line 931
    :cond_27
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetUpdated()Z

    move-result v1

    .line 932
    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetUpdated()Z

    move-result v2

    if-nez v1, :cond_28

    if-eqz v2, :cond_2a

    :cond_28
    if-eqz v1, :cond_4f

    if-nez v2, :cond_29

    goto/16 :goto_9

    .line 936
    :cond_29
    iget-wide v1, p0, Lcom/evernote/edam/type/Accounting;->updated:J

    iget-wide v3, p1, Lcom/evernote/edam/type/Accounting;->updated:J

    cmp-long v5, v1, v3

    if-eqz v5, :cond_2a

    return v0

    .line 940
    :cond_2a
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetPremiumSubscriptionNumber()Z

    move-result v1

    .line 941
    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetPremiumSubscriptionNumber()Z

    move-result v2

    if-nez v1, :cond_2b

    if-eqz v2, :cond_2d

    :cond_2b
    if-eqz v1, :cond_4e

    if-nez v2, :cond_2c

    goto/16 :goto_8

    .line 945
    :cond_2c
    iget-object v1, p0, Lcom/evernote/edam/type/Accounting;->premiumSubscriptionNumber:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/type/Accounting;->premiumSubscriptionNumber:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2d

    return v0

    .line 949
    :cond_2d
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetLastRequestedCharge()Z

    move-result v1

    .line 950
    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetLastRequestedCharge()Z

    move-result v2

    if-nez v1, :cond_2e

    if-eqz v2, :cond_30

    :cond_2e
    if-eqz v1, :cond_4d

    if-nez v2, :cond_2f

    goto/16 :goto_7

    .line 954
    :cond_2f
    iget-wide v1, p0, Lcom/evernote/edam/type/Accounting;->lastRequestedCharge:J

    iget-wide v3, p1, Lcom/evernote/edam/type/Accounting;->lastRequestedCharge:J

    cmp-long v5, v1, v3

    if-eqz v5, :cond_30

    return v0

    .line 958
    :cond_30
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetCurrency()Z

    move-result v1

    .line 959
    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetCurrency()Z

    move-result v2

    if-nez v1, :cond_31

    if-eqz v2, :cond_33

    :cond_31
    if-eqz v1, :cond_4c

    if-nez v2, :cond_32

    goto/16 :goto_6

    .line 963
    :cond_32
    iget-object v1, p0, Lcom/evernote/edam/type/Accounting;->currency:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/type/Accounting;->currency:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_33

    return v0

    .line 967
    :cond_33
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetUnitPrice()Z

    move-result v1

    .line 968
    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetUnitPrice()Z

    move-result v2

    if-nez v1, :cond_34

    if-eqz v2, :cond_36

    :cond_34
    if-eqz v1, :cond_4b

    if-nez v2, :cond_35

    goto/16 :goto_5

    .line 972
    :cond_35
    iget v1, p0, Lcom/evernote/edam/type/Accounting;->unitPrice:I

    iget v2, p1, Lcom/evernote/edam/type/Accounting;->unitPrice:I

    if-eq v1, v2, :cond_36

    return v0

    .line 976
    :cond_36
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetBusinessId()Z

    move-result v1

    .line 977
    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetBusinessId()Z

    move-result v2

    if-nez v1, :cond_37

    if-eqz v2, :cond_39

    :cond_37
    if-eqz v1, :cond_4a

    if-nez v2, :cond_38

    goto/16 :goto_4

    .line 981
    :cond_38
    iget v1, p0, Lcom/evernote/edam/type/Accounting;->businessId:I

    iget v2, p1, Lcom/evernote/edam/type/Accounting;->businessId:I

    if-eq v1, v2, :cond_39

    return v0

    .line 985
    :cond_39
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetBusinessName()Z

    move-result v1

    .line 986
    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetBusinessName()Z

    move-result v2

    if-nez v1, :cond_3a

    if-eqz v2, :cond_3c

    :cond_3a
    if-eqz v1, :cond_49

    if-nez v2, :cond_3b

    goto :goto_3

    .line 990
    :cond_3b
    iget-object v1, p0, Lcom/evernote/edam/type/Accounting;->businessName:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/type/Accounting;->businessName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3c

    return v0

    .line 994
    :cond_3c
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetBusinessRole()Z

    move-result v1

    .line 995
    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetBusinessRole()Z

    move-result v2

    if-nez v1, :cond_3d

    if-eqz v2, :cond_3f

    :cond_3d
    if-eqz v1, :cond_48

    if-nez v2, :cond_3e

    goto :goto_2

    .line 999
    :cond_3e
    iget-object v1, p0, Lcom/evernote/edam/type/Accounting;->businessRole:Lcom/evernote/edam/type/BusinessUserRole;

    iget-object v2, p1, Lcom/evernote/edam/type/Accounting;->businessRole:Lcom/evernote/edam/type/BusinessUserRole;

    invoke-virtual {v1, v2}, Lcom/evernote/edam/type/BusinessUserRole;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3f

    return v0

    .line 1003
    :cond_3f
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetUnitDiscount()Z

    move-result v1

    .line 1004
    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetUnitDiscount()Z

    move-result v2

    if-nez v1, :cond_40

    if-eqz v2, :cond_42

    :cond_40
    if-eqz v1, :cond_47

    if-nez v2, :cond_41

    goto :goto_1

    .line 1008
    :cond_41
    iget v1, p0, Lcom/evernote/edam/type/Accounting;->unitDiscount:I

    iget v2, p1, Lcom/evernote/edam/type/Accounting;->unitDiscount:I

    if-eq v1, v2, :cond_42

    return v0

    .line 1012
    :cond_42
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetNextChargeDate()Z

    move-result v1

    .line 1013
    invoke-virtual {p1}, Lcom/evernote/edam/type/Accounting;->isSetNextChargeDate()Z

    move-result v2

    if-nez v1, :cond_43

    if-eqz v2, :cond_45

    :cond_43
    if-eqz v1, :cond_46

    if-nez v2, :cond_44

    goto :goto_0

    .line 1017
    :cond_44
    iget-wide v1, p0, Lcom/evernote/edam/type/Accounting;->nextChargeDate:J

    iget-wide v3, p1, Lcom/evernote/edam/type/Accounting;->nextChargeDate:J

    cmp-long p1, v1, v3

    if-eqz p1, :cond_45

    return v0

    :cond_45
    const/4 p1, 0x1

    return p1

    :cond_46
    :goto_0
    return v0

    :cond_47
    :goto_1
    return v0

    :cond_48
    :goto_2
    return v0

    :cond_49
    :goto_3
    return v0

    :cond_4a
    :goto_4
    return v0

    :cond_4b
    :goto_5
    return v0

    :cond_4c
    :goto_6
    return v0

    :cond_4d
    :goto_7
    return v0

    :cond_4e
    :goto_8
    return v0

    :cond_4f
    :goto_9
    return v0

    :cond_50
    :goto_a
    return v0

    :cond_51
    :goto_b
    return v0

    :cond_52
    :goto_c
    return v0

    :cond_53
    :goto_d
    return v0

    :cond_54
    :goto_e
    return v0

    :cond_55
    :goto_f
    return v0

    :cond_56
    :goto_10
    return v0

    :cond_57
    :goto_11
    return v0

    :cond_58
    :goto_12
    return v0

    :cond_59
    :goto_13
    return v0

    :cond_5a
    :goto_14
    return v0

    :cond_5b
    :goto_15
    return v0

    :cond_5c
    :goto_16
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 805
    :cond_0
    instance-of v1, p1, Lcom/evernote/edam/type/Accounting;

    if-eqz v1, :cond_1

    .line 806
    check-cast p1, Lcom/evernote/edam/type/Accounting;

    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/Accounting;->equals(Lcom/evernote/edam/type/Accounting;)Z

    move-result p1

    return p1

    :cond_1
    return v0
.end method

.method public getBusinessId()I
    .locals 1

    .line 682
    iget v0, p0, Lcom/evernote/edam/type/Accounting;->businessId:I

    return v0
.end method

.method public getBusinessName()Ljava/lang/String;
    .locals 1

    .line 704
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->businessName:Ljava/lang/String;

    return-object v0
.end method

.method public getBusinessRole()Lcom/evernote/edam/type/BusinessUserRole;
    .locals 1

    .line 731
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->businessRole:Lcom/evernote/edam/type/BusinessUserRole;

    return-object v0
.end method

.method public getCurrency()Ljava/lang/String;
    .locals 1

    .line 637
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->currency:Ljava/lang/String;

    return-object v0
.end method

.method public getLastFailedCharge()J
    .locals 2

    .line 481
    iget-wide v0, p0, Lcom/evernote/edam/type/Accounting;->lastFailedCharge:J

    return-wide v0
.end method

.method public getLastFailedChargeReason()Ljava/lang/String;
    .locals 1

    .line 503
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->lastFailedChargeReason:Ljava/lang/String;

    return-object v0
.end method

.method public getLastRequestedCharge()J
    .locals 2

    .line 615
    iget-wide v0, p0, Lcom/evernote/edam/type/Accounting;->lastRequestedCharge:J

    return-wide v0
.end method

.method public getLastSuccessfulCharge()J
    .locals 2

    .line 459
    iget-wide v0, p0, Lcom/evernote/edam/type/Accounting;->lastSuccessfulCharge:J

    return-wide v0
.end method

.method public getNextChargeDate()J
    .locals 2

    .line 780
    iget-wide v0, p0, Lcom/evernote/edam/type/Accounting;->nextChargeDate:J

    return-wide v0
.end method

.method public getNextPaymentDue()J
    .locals 2

    .line 526
    iget-wide v0, p0, Lcom/evernote/edam/type/Accounting;->nextPaymentDue:J

    return-wide v0
.end method

.method public getPremiumCommerceService()Ljava/lang/String;
    .locals 1

    .line 391
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->premiumCommerceService:Ljava/lang/String;

    return-object v0
.end method

.method public getPremiumLockUntil()J
    .locals 2

    .line 548
    iget-wide v0, p0, Lcom/evernote/edam/type/Accounting;->premiumLockUntil:J

    return-wide v0
.end method

.method public getPremiumOrderNumber()Ljava/lang/String;
    .locals 1

    .line 368
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->premiumOrderNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getPremiumServiceSKU()Ljava/lang/String;
    .locals 1

    .line 436
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->premiumServiceSKU:Ljava/lang/String;

    return-object v0
.end method

.method public getPremiumServiceStart()J
    .locals 2

    .line 414
    iget-wide v0, p0, Lcom/evernote/edam/type/Accounting;->premiumServiceStart:J

    return-wide v0
.end method

.method public getPremiumServiceStatus()Lcom/evernote/edam/type/PremiumOrderStatus;
    .locals 1

    .line 341
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->premiumServiceStatus:Lcom/evernote/edam/type/PremiumOrderStatus;

    return-object v0
.end method

.method public getPremiumSubscriptionNumber()Ljava/lang/String;
    .locals 1

    .line 592
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->premiumSubscriptionNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getUnitDiscount()I
    .locals 1

    .line 758
    iget v0, p0, Lcom/evernote/edam/type/Accounting;->unitDiscount:I

    return v0
.end method

.method public getUnitPrice()I
    .locals 1

    .line 660
    iget v0, p0, Lcom/evernote/edam/type/Accounting;->unitPrice:I

    return v0
.end method

.method public getUpdated()J
    .locals 2

    .line 570
    iget-wide v0, p0, Lcom/evernote/edam/type/Accounting;->updated:J

    return-wide v0
.end method

.method public getUploadLimit()J
    .locals 2

    .line 271
    iget-wide v0, p0, Lcom/evernote/edam/type/Accounting;->uploadLimit:J

    return-wide v0
.end method

.method public getUploadLimitEnd()J
    .locals 2

    .line 293
    iget-wide v0, p0, Lcom/evernote/edam/type/Accounting;->uploadLimitEnd:J

    return-wide v0
.end method

.method public getUploadLimitNextMonth()J
    .locals 2

    .line 315
    iget-wide v0, p0, Lcom/evernote/edam/type/Accounting;->uploadLimitNextMonth:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isSetBusinessId()Z
    .locals 2

    .line 696
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->__isset_vector:[Z

    const/16 v1, 0xb

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetBusinessName()Z
    .locals 1

    .line 717
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->businessName:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetBusinessRole()Z
    .locals 1

    .line 748
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->businessRole:Lcom/evernote/edam/type/BusinessUserRole;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetCurrency()Z
    .locals 1

    .line 650
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->currency:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetLastFailedCharge()Z
    .locals 2

    .line 495
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->__isset_vector:[Z

    const/4 v1, 0x5

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetLastFailedChargeReason()Z
    .locals 1

    .line 516
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->lastFailedChargeReason:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetLastRequestedCharge()Z
    .locals 2

    .line 629
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->__isset_vector:[Z

    const/16 v1, 0x9

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetLastSuccessfulCharge()Z
    .locals 2

    .line 473
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->__isset_vector:[Z

    const/4 v1, 0x4

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetNextChargeDate()Z
    .locals 2

    .line 794
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->__isset_vector:[Z

    const/16 v1, 0xd

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetNextPaymentDue()Z
    .locals 2

    .line 540
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->__isset_vector:[Z

    const/4 v1, 0x6

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetPremiumCommerceService()Z
    .locals 1

    .line 404
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->premiumCommerceService:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetPremiumLockUntil()Z
    .locals 2

    .line 562
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->__isset_vector:[Z

    const/4 v1, 0x7

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetPremiumOrderNumber()Z
    .locals 1

    .line 381
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->premiumOrderNumber:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetPremiumServiceSKU()Z
    .locals 1

    .line 449
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->premiumServiceSKU:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetPremiumServiceStart()Z
    .locals 2

    .line 428
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->__isset_vector:[Z

    const/4 v1, 0x3

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetPremiumServiceStatus()Z
    .locals 1

    .line 358
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->premiumServiceStatus:Lcom/evernote/edam/type/PremiumOrderStatus;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetPremiumSubscriptionNumber()Z
    .locals 1

    .line 605
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->premiumSubscriptionNumber:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetUnitDiscount()Z
    .locals 2

    .line 772
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->__isset_vector:[Z

    const/16 v1, 0xc

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetUnitPrice()Z
    .locals 2

    .line 674
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->__isset_vector:[Z

    const/16 v1, 0xa

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetUpdated()Z
    .locals 2

    .line 584
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->__isset_vector:[Z

    const/16 v1, 0x8

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetUploadLimit()Z
    .locals 2

    .line 285
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->__isset_vector:[Z

    const/4 v1, 0x0

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetUploadLimitEnd()Z
    .locals 2

    .line 307
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->__isset_vector:[Z

    const/4 v1, 0x1

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetUploadLimitNextMonth()Z
    .locals 2

    .line 329
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->__isset_vector:[Z

    const/4 v1, 0x2

    aget-boolean v0, v0, v1

    return v0
.end method

.method public read(Lcom/evernote/thrift/protocol/TProtocol;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1249
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readStructBegin()Lcom/evernote/thrift/protocol/TStruct;

    .line 1252
    :goto_0
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readFieldBegin()Lcom/evernote/thrift/protocol/TField;

    move-result-object v0

    .line 1253
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-nez v1, :cond_0

    .line 1437
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readStructEnd()V

    .line 1438
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->validate()V

    return-void

    .line 1256
    :cond_0
    iget-short v1, v0, Lcom/evernote/thrift/protocol/TField;->id:S

    const/16 v2, 0x8

    const/16 v3, 0xb

    const/16 v4, 0xa

    const/4 v5, 0x1

    packed-switch v1, :pswitch_data_0

    .line 1433
    :pswitch_0
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 1425
    :pswitch_1
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_1

    .line 1426
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/evernote/edam/type/Accounting;->nextChargeDate:J

    .line 1427
    invoke-virtual {p0, v5}, Lcom/evernote/edam/type/Accounting;->setNextChargeDateIsSet(Z)V

    goto/16 :goto_1

    .line 1429
    :cond_1
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 1417
    :pswitch_2
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v2, :cond_2

    .line 1418
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI32()I

    move-result v0

    iput v0, p0, Lcom/evernote/edam/type/Accounting;->unitDiscount:I

    .line 1419
    invoke-virtual {p0, v5}, Lcom/evernote/edam/type/Accounting;->setUnitDiscountIsSet(Z)V

    goto/16 :goto_1

    .line 1421
    :cond_2
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 1410
    :pswitch_3
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v2, :cond_3

    .line 1411
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI32()I

    move-result v0

    invoke-static {v0}, Lcom/evernote/edam/type/BusinessUserRole;->findByValue(I)Lcom/evernote/edam/type/BusinessUserRole;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/type/Accounting;->businessRole:Lcom/evernote/edam/type/BusinessUserRole;

    goto/16 :goto_1

    .line 1413
    :cond_3
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 1403
    :pswitch_4
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v3, :cond_4

    .line 1404
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/type/Accounting;->businessName:Ljava/lang/String;

    goto/16 :goto_1

    .line 1406
    :cond_4
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 1395
    :pswitch_5
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v2, :cond_5

    .line 1396
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI32()I

    move-result v0

    iput v0, p0, Lcom/evernote/edam/type/Accounting;->businessId:I

    .line 1397
    invoke-virtual {p0, v5}, Lcom/evernote/edam/type/Accounting;->setBusinessIdIsSet(Z)V

    goto/16 :goto_1

    .line 1399
    :cond_5
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 1387
    :pswitch_6
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v2, :cond_6

    .line 1388
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI32()I

    move-result v0

    iput v0, p0, Lcom/evernote/edam/type/Accounting;->unitPrice:I

    .line 1389
    invoke-virtual {p0, v5}, Lcom/evernote/edam/type/Accounting;->setUnitPriceIsSet(Z)V

    goto/16 :goto_1

    .line 1391
    :cond_6
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 1380
    :pswitch_7
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v3, :cond_7

    .line 1381
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/type/Accounting;->currency:Ljava/lang/String;

    goto/16 :goto_1

    .line 1383
    :cond_7
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 1372
    :pswitch_8
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_8

    .line 1373
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/evernote/edam/type/Accounting;->lastRequestedCharge:J

    .line 1374
    invoke-virtual {p0, v5}, Lcom/evernote/edam/type/Accounting;->setLastRequestedChargeIsSet(Z)V

    goto/16 :goto_1

    .line 1376
    :cond_8
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 1365
    :pswitch_9
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v3, :cond_9

    .line 1366
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/type/Accounting;->premiumSubscriptionNumber:Ljava/lang/String;

    goto/16 :goto_1

    .line 1368
    :cond_9
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 1357
    :pswitch_a
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_a

    .line 1358
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/evernote/edam/type/Accounting;->updated:J

    .line 1359
    invoke-virtual {p0, v5}, Lcom/evernote/edam/type/Accounting;->setUpdatedIsSet(Z)V

    goto/16 :goto_1

    .line 1361
    :cond_a
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 1349
    :pswitch_b
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_b

    .line 1350
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/evernote/edam/type/Accounting;->premiumLockUntil:J

    .line 1351
    invoke-virtual {p0, v5}, Lcom/evernote/edam/type/Accounting;->setPremiumLockUntilIsSet(Z)V

    goto/16 :goto_1

    .line 1353
    :cond_b
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 1341
    :pswitch_c
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_c

    .line 1342
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/evernote/edam/type/Accounting;->nextPaymentDue:J

    .line 1343
    invoke-virtual {p0, v5}, Lcom/evernote/edam/type/Accounting;->setNextPaymentDueIsSet(Z)V

    goto/16 :goto_1

    .line 1345
    :cond_c
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 1334
    :pswitch_d
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v3, :cond_d

    .line 1335
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/type/Accounting;->lastFailedChargeReason:Ljava/lang/String;

    goto/16 :goto_1

    .line 1337
    :cond_d
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 1326
    :pswitch_e
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_e

    .line 1327
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/evernote/edam/type/Accounting;->lastFailedCharge:J

    .line 1328
    invoke-virtual {p0, v5}, Lcom/evernote/edam/type/Accounting;->setLastFailedChargeIsSet(Z)V

    goto/16 :goto_1

    .line 1330
    :cond_e
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 1318
    :pswitch_f
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_f

    .line 1319
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/evernote/edam/type/Accounting;->lastSuccessfulCharge:J

    .line 1320
    invoke-virtual {p0, v5}, Lcom/evernote/edam/type/Accounting;->setLastSuccessfulChargeIsSet(Z)V

    goto/16 :goto_1

    .line 1322
    :cond_f
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 1311
    :pswitch_10
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v3, :cond_10

    .line 1312
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/type/Accounting;->premiumServiceSKU:Ljava/lang/String;

    goto/16 :goto_1

    .line 1314
    :cond_10
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 1303
    :pswitch_11
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_11

    .line 1304
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/evernote/edam/type/Accounting;->premiumServiceStart:J

    .line 1305
    invoke-virtual {p0, v5}, Lcom/evernote/edam/type/Accounting;->setPremiumServiceStartIsSet(Z)V

    goto/16 :goto_1

    .line 1307
    :cond_11
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 1296
    :pswitch_12
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v3, :cond_12

    .line 1297
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/type/Accounting;->premiumCommerceService:Ljava/lang/String;

    goto/16 :goto_1

    .line 1299
    :cond_12
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 1289
    :pswitch_13
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v3, :cond_13

    .line 1290
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/type/Accounting;->premiumOrderNumber:Ljava/lang/String;

    goto :goto_1

    .line 1292
    :cond_13
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 1282
    :pswitch_14
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v2, :cond_14

    .line 1283
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI32()I

    move-result v0

    invoke-static {v0}, Lcom/evernote/edam/type/PremiumOrderStatus;->findByValue(I)Lcom/evernote/edam/type/PremiumOrderStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/type/Accounting;->premiumServiceStatus:Lcom/evernote/edam/type/PremiumOrderStatus;

    goto :goto_1

    .line 1285
    :cond_14
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 1274
    :pswitch_15
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_15

    .line 1275
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/evernote/edam/type/Accounting;->uploadLimitNextMonth:J

    .line 1276
    invoke-virtual {p0, v5}, Lcom/evernote/edam/type/Accounting;->setUploadLimitNextMonthIsSet(Z)V

    goto :goto_1

    .line 1278
    :cond_15
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 1266
    :pswitch_16
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_16

    .line 1267
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/evernote/edam/type/Accounting;->uploadLimitEnd:J

    .line 1268
    invoke-virtual {p0, v5}, Lcom/evernote/edam/type/Accounting;->setUploadLimitEndIsSet(Z)V

    goto :goto_1

    .line 1270
    :cond_16
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 1258
    :pswitch_17
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_17

    .line 1259
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/evernote/edam/type/Accounting;->uploadLimit:J

    .line 1260
    invoke-virtual {p0, v5}, Lcom/evernote/edam/type/Accounting;->setUploadLimitIsSet(Z)V

    goto :goto_1

    .line 1262
    :cond_17
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    .line 1435
    :goto_1
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readFieldEnd()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_0
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public setBusinessId(I)V
    .locals 0

    .line 686
    iput p1, p0, Lcom/evernote/edam/type/Accounting;->businessId:I

    const/4 p1, 0x1

    .line 687
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/Accounting;->setBusinessIdIsSet(Z)V

    return-void
.end method

.method public setBusinessIdIsSet(Z)V
    .locals 2

    .line 700
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->__isset_vector:[Z

    const/16 v1, 0xb

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setBusinessName(Ljava/lang/String;)V
    .locals 0

    .line 708
    iput-object p1, p0, Lcom/evernote/edam/type/Accounting;->businessName:Ljava/lang/String;

    return-void
.end method

.method public setBusinessNameIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 722
    iput-object p1, p0, Lcom/evernote/edam/type/Accounting;->businessName:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setBusinessRole(Lcom/evernote/edam/type/BusinessUserRole;)V
    .locals 0

    .line 739
    iput-object p1, p0, Lcom/evernote/edam/type/Accounting;->businessRole:Lcom/evernote/edam/type/BusinessUserRole;

    return-void
.end method

.method public setBusinessRoleIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 753
    iput-object p1, p0, Lcom/evernote/edam/type/Accounting;->businessRole:Lcom/evernote/edam/type/BusinessUserRole;

    :cond_0
    return-void
.end method

.method public setCurrency(Ljava/lang/String;)V
    .locals 0

    .line 641
    iput-object p1, p0, Lcom/evernote/edam/type/Accounting;->currency:Ljava/lang/String;

    return-void
.end method

.method public setCurrencyIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 655
    iput-object p1, p0, Lcom/evernote/edam/type/Accounting;->currency:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setLastFailedCharge(J)V
    .locals 0

    .line 485
    iput-wide p1, p0, Lcom/evernote/edam/type/Accounting;->lastFailedCharge:J

    const/4 p1, 0x1

    .line 486
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/Accounting;->setLastFailedChargeIsSet(Z)V

    return-void
.end method

.method public setLastFailedChargeIsSet(Z)V
    .locals 2

    .line 499
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->__isset_vector:[Z

    const/4 v1, 0x5

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setLastFailedChargeReason(Ljava/lang/String;)V
    .locals 0

    .line 507
    iput-object p1, p0, Lcom/evernote/edam/type/Accounting;->lastFailedChargeReason:Ljava/lang/String;

    return-void
.end method

.method public setLastFailedChargeReasonIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 521
    iput-object p1, p0, Lcom/evernote/edam/type/Accounting;->lastFailedChargeReason:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setLastRequestedCharge(J)V
    .locals 0

    .line 619
    iput-wide p1, p0, Lcom/evernote/edam/type/Accounting;->lastRequestedCharge:J

    const/4 p1, 0x1

    .line 620
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/Accounting;->setLastRequestedChargeIsSet(Z)V

    return-void
.end method

.method public setLastRequestedChargeIsSet(Z)V
    .locals 2

    .line 633
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->__isset_vector:[Z

    const/16 v1, 0x9

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setLastSuccessfulCharge(J)V
    .locals 0

    .line 463
    iput-wide p1, p0, Lcom/evernote/edam/type/Accounting;->lastSuccessfulCharge:J

    const/4 p1, 0x1

    .line 464
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/Accounting;->setLastSuccessfulChargeIsSet(Z)V

    return-void
.end method

.method public setLastSuccessfulChargeIsSet(Z)V
    .locals 2

    .line 477
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->__isset_vector:[Z

    const/4 v1, 0x4

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setNextChargeDate(J)V
    .locals 0

    .line 784
    iput-wide p1, p0, Lcom/evernote/edam/type/Accounting;->nextChargeDate:J

    const/4 p1, 0x1

    .line 785
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/Accounting;->setNextChargeDateIsSet(Z)V

    return-void
.end method

.method public setNextChargeDateIsSet(Z)V
    .locals 2

    .line 798
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->__isset_vector:[Z

    const/16 v1, 0xd

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setNextPaymentDue(J)V
    .locals 0

    .line 530
    iput-wide p1, p0, Lcom/evernote/edam/type/Accounting;->nextPaymentDue:J

    const/4 p1, 0x1

    .line 531
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/Accounting;->setNextPaymentDueIsSet(Z)V

    return-void
.end method

.method public setNextPaymentDueIsSet(Z)V
    .locals 2

    .line 544
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->__isset_vector:[Z

    const/4 v1, 0x6

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setPremiumCommerceService(Ljava/lang/String;)V
    .locals 0

    .line 395
    iput-object p1, p0, Lcom/evernote/edam/type/Accounting;->premiumCommerceService:Ljava/lang/String;

    return-void
.end method

.method public setPremiumCommerceServiceIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 409
    iput-object p1, p0, Lcom/evernote/edam/type/Accounting;->premiumCommerceService:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setPremiumLockUntil(J)V
    .locals 0

    .line 552
    iput-wide p1, p0, Lcom/evernote/edam/type/Accounting;->premiumLockUntil:J

    const/4 p1, 0x1

    .line 553
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/Accounting;->setPremiumLockUntilIsSet(Z)V

    return-void
.end method

.method public setPremiumLockUntilIsSet(Z)V
    .locals 2

    .line 566
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->__isset_vector:[Z

    const/4 v1, 0x7

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setPremiumOrderNumber(Ljava/lang/String;)V
    .locals 0

    .line 372
    iput-object p1, p0, Lcom/evernote/edam/type/Accounting;->premiumOrderNumber:Ljava/lang/String;

    return-void
.end method

.method public setPremiumOrderNumberIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 386
    iput-object p1, p0, Lcom/evernote/edam/type/Accounting;->premiumOrderNumber:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setPremiumServiceSKU(Ljava/lang/String;)V
    .locals 0

    .line 440
    iput-object p1, p0, Lcom/evernote/edam/type/Accounting;->premiumServiceSKU:Ljava/lang/String;

    return-void
.end method

.method public setPremiumServiceSKUIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 454
    iput-object p1, p0, Lcom/evernote/edam/type/Accounting;->premiumServiceSKU:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setPremiumServiceStart(J)V
    .locals 0

    .line 418
    iput-wide p1, p0, Lcom/evernote/edam/type/Accounting;->premiumServiceStart:J

    const/4 p1, 0x1

    .line 419
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/Accounting;->setPremiumServiceStartIsSet(Z)V

    return-void
.end method

.method public setPremiumServiceStartIsSet(Z)V
    .locals 2

    .line 432
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->__isset_vector:[Z

    const/4 v1, 0x3

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setPremiumServiceStatus(Lcom/evernote/edam/type/PremiumOrderStatus;)V
    .locals 0

    .line 349
    iput-object p1, p0, Lcom/evernote/edam/type/Accounting;->premiumServiceStatus:Lcom/evernote/edam/type/PremiumOrderStatus;

    return-void
.end method

.method public setPremiumServiceStatusIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 363
    iput-object p1, p0, Lcom/evernote/edam/type/Accounting;->premiumServiceStatus:Lcom/evernote/edam/type/PremiumOrderStatus;

    :cond_0
    return-void
.end method

.method public setPremiumSubscriptionNumber(Ljava/lang/String;)V
    .locals 0

    .line 596
    iput-object p1, p0, Lcom/evernote/edam/type/Accounting;->premiumSubscriptionNumber:Ljava/lang/String;

    return-void
.end method

.method public setPremiumSubscriptionNumberIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 610
    iput-object p1, p0, Lcom/evernote/edam/type/Accounting;->premiumSubscriptionNumber:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setUnitDiscount(I)V
    .locals 0

    .line 762
    iput p1, p0, Lcom/evernote/edam/type/Accounting;->unitDiscount:I

    const/4 p1, 0x1

    .line 763
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/Accounting;->setUnitDiscountIsSet(Z)V

    return-void
.end method

.method public setUnitDiscountIsSet(Z)V
    .locals 2

    .line 776
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->__isset_vector:[Z

    const/16 v1, 0xc

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setUnitPrice(I)V
    .locals 0

    .line 664
    iput p1, p0, Lcom/evernote/edam/type/Accounting;->unitPrice:I

    const/4 p1, 0x1

    .line 665
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/Accounting;->setUnitPriceIsSet(Z)V

    return-void
.end method

.method public setUnitPriceIsSet(Z)V
    .locals 2

    .line 678
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->__isset_vector:[Z

    const/16 v1, 0xa

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setUpdated(J)V
    .locals 0

    .line 574
    iput-wide p1, p0, Lcom/evernote/edam/type/Accounting;->updated:J

    const/4 p1, 0x1

    .line 575
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/Accounting;->setUpdatedIsSet(Z)V

    return-void
.end method

.method public setUpdatedIsSet(Z)V
    .locals 2

    .line 588
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->__isset_vector:[Z

    const/16 v1, 0x8

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setUploadLimit(J)V
    .locals 0

    .line 275
    iput-wide p1, p0, Lcom/evernote/edam/type/Accounting;->uploadLimit:J

    const/4 p1, 0x1

    .line 276
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/Accounting;->setUploadLimitIsSet(Z)V

    return-void
.end method

.method public setUploadLimitEnd(J)V
    .locals 0

    .line 297
    iput-wide p1, p0, Lcom/evernote/edam/type/Accounting;->uploadLimitEnd:J

    const/4 p1, 0x1

    .line 298
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/Accounting;->setUploadLimitEndIsSet(Z)V

    return-void
.end method

.method public setUploadLimitEndIsSet(Z)V
    .locals 2

    .line 311
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->__isset_vector:[Z

    const/4 v1, 0x1

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setUploadLimitIsSet(Z)V
    .locals 2

    .line 289
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->__isset_vector:[Z

    const/4 v1, 0x0

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setUploadLimitNextMonth(J)V
    .locals 0

    .line 319
    iput-wide p1, p0, Lcom/evernote/edam/type/Accounting;->uploadLimitNextMonth:J

    const/4 p1, 0x1

    .line 320
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/Accounting;->setUploadLimitNextMonthIsSet(Z)V

    return-void
.end method

.method public setUploadLimitNextMonthIsSet(Z)V
    .locals 2

    .line 333
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->__isset_vector:[Z

    const/4 v1, 0x2

    aput-boolean p1, v0, v1

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .line 1584
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Accounting("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1587
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetUploadLimit()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    const-string v1, "uploadLimit:"

    .line 1588
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1589
    iget-wide v3, p0, Lcom/evernote/edam/type/Accounting;->uploadLimit:J

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    const/4 v1, 0x1

    .line 1592
    :goto_0
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetUploadLimitEnd()Z

    move-result v3

    if-eqz v3, :cond_2

    if-nez v1, :cond_1

    const-string v1, ", "

    .line 1593
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const-string v1, "uploadLimitEnd:"

    .line 1594
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1595
    iget-wide v3, p0, Lcom/evernote/edam/type/Accounting;->uploadLimitEnd:J

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1598
    :cond_2
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetUploadLimitNextMonth()Z

    move-result v3

    if-eqz v3, :cond_4

    if-nez v1, :cond_3

    const-string v1, ", "

    .line 1599
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    const-string v1, "uploadLimitNextMonth:"

    .line 1600
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1601
    iget-wide v3, p0, Lcom/evernote/edam/type/Accounting;->uploadLimitNextMonth:J

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1604
    :cond_4
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetPremiumServiceStatus()Z

    move-result v3

    if-eqz v3, :cond_7

    if-nez v1, :cond_5

    const-string v1, ", "

    .line 1605
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    const-string v1, "premiumServiceStatus:"

    .line 1606
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1607
    iget-object v1, p0, Lcom/evernote/edam/type/Accounting;->premiumServiceStatus:Lcom/evernote/edam/type/PremiumOrderStatus;

    if-nez v1, :cond_6

    const-string v1, "null"

    .line 1608
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1610
    :cond_6
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :goto_1
    const/4 v1, 0x0

    .line 1614
    :cond_7
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetPremiumOrderNumber()Z

    move-result v3

    if-eqz v3, :cond_a

    if-nez v1, :cond_8

    const-string v1, ", "

    .line 1615
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_8
    const-string v1, "premiumOrderNumber:"

    .line 1616
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1617
    iget-object v1, p0, Lcom/evernote/edam/type/Accounting;->premiumOrderNumber:Ljava/lang/String;

    if-nez v1, :cond_9

    const-string v1, "null"

    .line 1618
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 1620
    :cond_9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_2
    const/4 v1, 0x0

    .line 1624
    :cond_a
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetPremiumCommerceService()Z

    move-result v3

    if-eqz v3, :cond_d

    if-nez v1, :cond_b

    const-string v1, ", "

    .line 1625
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_b
    const-string v1, "premiumCommerceService:"

    .line 1626
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1627
    iget-object v1, p0, Lcom/evernote/edam/type/Accounting;->premiumCommerceService:Ljava/lang/String;

    if-nez v1, :cond_c

    const-string v1, "null"

    .line 1628
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 1630
    :cond_c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    const/4 v1, 0x0

    .line 1634
    :cond_d
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetPremiumServiceStart()Z

    move-result v3

    if-eqz v3, :cond_f

    if-nez v1, :cond_e

    const-string v1, ", "

    .line 1635
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_e
    const-string v1, "premiumServiceStart:"

    .line 1636
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1637
    iget-wide v3, p0, Lcom/evernote/edam/type/Accounting;->premiumServiceStart:J

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1640
    :cond_f
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetPremiumServiceSKU()Z

    move-result v3

    if-eqz v3, :cond_12

    if-nez v1, :cond_10

    const-string v1, ", "

    .line 1641
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_10
    const-string v1, "premiumServiceSKU:"

    .line 1642
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1643
    iget-object v1, p0, Lcom/evernote/edam/type/Accounting;->premiumServiceSKU:Ljava/lang/String;

    if-nez v1, :cond_11

    const-string v1, "null"

    .line 1644
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 1646
    :cond_11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_4
    const/4 v1, 0x0

    .line 1650
    :cond_12
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetLastSuccessfulCharge()Z

    move-result v3

    if-eqz v3, :cond_14

    if-nez v1, :cond_13

    const-string v1, ", "

    .line 1651
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_13
    const-string v1, "lastSuccessfulCharge:"

    .line 1652
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1653
    iget-wide v3, p0, Lcom/evernote/edam/type/Accounting;->lastSuccessfulCharge:J

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1656
    :cond_14
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetLastFailedCharge()Z

    move-result v3

    if-eqz v3, :cond_16

    if-nez v1, :cond_15

    const-string v1, ", "

    .line 1657
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_15
    const-string v1, "lastFailedCharge:"

    .line 1658
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1659
    iget-wide v3, p0, Lcom/evernote/edam/type/Accounting;->lastFailedCharge:J

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1662
    :cond_16
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetLastFailedChargeReason()Z

    move-result v3

    if-eqz v3, :cond_19

    if-nez v1, :cond_17

    const-string v1, ", "

    .line 1663
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_17
    const-string v1, "lastFailedChargeReason:"

    .line 1664
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1665
    iget-object v1, p0, Lcom/evernote/edam/type/Accounting;->lastFailedChargeReason:Ljava/lang/String;

    if-nez v1, :cond_18

    const-string v1, "null"

    .line 1666
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5

    .line 1668
    :cond_18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_5
    const/4 v1, 0x0

    .line 1672
    :cond_19
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetNextPaymentDue()Z

    move-result v3

    if-eqz v3, :cond_1b

    if-nez v1, :cond_1a

    const-string v1, ", "

    .line 1673
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1a
    const-string v1, "nextPaymentDue:"

    .line 1674
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1675
    iget-wide v3, p0, Lcom/evernote/edam/type/Accounting;->nextPaymentDue:J

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1678
    :cond_1b
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetPremiumLockUntil()Z

    move-result v3

    if-eqz v3, :cond_1d

    if-nez v1, :cond_1c

    const-string v1, ", "

    .line 1679
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1c
    const-string v1, "premiumLockUntil:"

    .line 1680
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1681
    iget-wide v3, p0, Lcom/evernote/edam/type/Accounting;->premiumLockUntil:J

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1684
    :cond_1d
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetUpdated()Z

    move-result v3

    if-eqz v3, :cond_1f

    if-nez v1, :cond_1e

    const-string v1, ", "

    .line 1685
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1e
    const-string v1, "updated:"

    .line 1686
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1687
    iget-wide v3, p0, Lcom/evernote/edam/type/Accounting;->updated:J

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1690
    :cond_1f
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetPremiumSubscriptionNumber()Z

    move-result v3

    if-eqz v3, :cond_22

    if-nez v1, :cond_20

    const-string v1, ", "

    .line 1691
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_20
    const-string v1, "premiumSubscriptionNumber:"

    .line 1692
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1693
    iget-object v1, p0, Lcom/evernote/edam/type/Accounting;->premiumSubscriptionNumber:Ljava/lang/String;

    if-nez v1, :cond_21

    const-string v1, "null"

    .line 1694
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6

    .line 1696
    :cond_21
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_6
    const/4 v1, 0x0

    .line 1700
    :cond_22
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetLastRequestedCharge()Z

    move-result v3

    if-eqz v3, :cond_24

    if-nez v1, :cond_23

    const-string v1, ", "

    .line 1701
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_23
    const-string v1, "lastRequestedCharge:"

    .line 1702
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1703
    iget-wide v3, p0, Lcom/evernote/edam/type/Accounting;->lastRequestedCharge:J

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1706
    :cond_24
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetCurrency()Z

    move-result v3

    if-eqz v3, :cond_27

    if-nez v1, :cond_25

    const-string v1, ", "

    .line 1707
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_25
    const-string v1, "currency:"

    .line 1708
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1709
    iget-object v1, p0, Lcom/evernote/edam/type/Accounting;->currency:Ljava/lang/String;

    if-nez v1, :cond_26

    const-string v1, "null"

    .line 1710
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_7

    .line 1712
    :cond_26
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_7
    const/4 v1, 0x0

    .line 1716
    :cond_27
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetUnitPrice()Z

    move-result v3

    if-eqz v3, :cond_29

    if-nez v1, :cond_28

    const-string v1, ", "

    .line 1717
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_28
    const-string v1, "unitPrice:"

    .line 1718
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1719
    iget v1, p0, Lcom/evernote/edam/type/Accounting;->unitPrice:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1722
    :cond_29
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetBusinessId()Z

    move-result v3

    if-eqz v3, :cond_2b

    if-nez v1, :cond_2a

    const-string v1, ", "

    .line 1723
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2a
    const-string v1, "businessId:"

    .line 1724
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1725
    iget v1, p0, Lcom/evernote/edam/type/Accounting;->businessId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1728
    :cond_2b
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetBusinessName()Z

    move-result v3

    if-eqz v3, :cond_2e

    if-nez v1, :cond_2c

    const-string v1, ", "

    .line 1729
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2c
    const-string v1, "businessName:"

    .line 1730
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1731
    iget-object v1, p0, Lcom/evernote/edam/type/Accounting;->businessName:Ljava/lang/String;

    if-nez v1, :cond_2d

    const-string v1, "null"

    .line 1732
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_8

    .line 1734
    :cond_2d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_8
    const/4 v1, 0x0

    .line 1738
    :cond_2e
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetBusinessRole()Z

    move-result v3

    if-eqz v3, :cond_31

    if-nez v1, :cond_2f

    const-string v1, ", "

    .line 1739
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2f
    const-string v1, "businessRole:"

    .line 1740
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1741
    iget-object v1, p0, Lcom/evernote/edam/type/Accounting;->businessRole:Lcom/evernote/edam/type/BusinessUserRole;

    if-nez v1, :cond_30

    const-string v1, "null"

    .line 1742
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_9

    .line 1744
    :cond_30
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :goto_9
    const/4 v1, 0x0

    .line 1748
    :cond_31
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetUnitDiscount()Z

    move-result v3

    if-eqz v3, :cond_33

    if-nez v1, :cond_32

    const-string v1, ", "

    .line 1749
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_32
    const-string v1, "unitDiscount:"

    .line 1750
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1751
    iget v1, p0, Lcom/evernote/edam/type/Accounting;->unitDiscount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1754
    :cond_33
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetNextChargeDate()Z

    move-result v2

    if-eqz v2, :cond_35

    if-nez v1, :cond_34

    const-string v1, ", "

    .line 1755
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_34
    const-string v1, "nextChargeDate:"

    .line 1756
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1757
    iget-wide v1, p0, Lcom/evernote/edam/type/Accounting;->nextChargeDate:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    :cond_35
    const-string v1, ")"

    .line 1760
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1761
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public unsetBusinessId()V
    .locals 3

    .line 691
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->__isset_vector:[Z

    const/16 v1, 0xb

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetBusinessName()V
    .locals 1

    const/4 v0, 0x0

    .line 712
    iput-object v0, p0, Lcom/evernote/edam/type/Accounting;->businessName:Ljava/lang/String;

    return-void
.end method

.method public unsetBusinessRole()V
    .locals 1

    const/4 v0, 0x0

    .line 743
    iput-object v0, p0, Lcom/evernote/edam/type/Accounting;->businessRole:Lcom/evernote/edam/type/BusinessUserRole;

    return-void
.end method

.method public unsetCurrency()V
    .locals 1

    const/4 v0, 0x0

    .line 645
    iput-object v0, p0, Lcom/evernote/edam/type/Accounting;->currency:Ljava/lang/String;

    return-void
.end method

.method public unsetLastFailedCharge()V
    .locals 3

    .line 490
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->__isset_vector:[Z

    const/4 v1, 0x5

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetLastFailedChargeReason()V
    .locals 1

    const/4 v0, 0x0

    .line 511
    iput-object v0, p0, Lcom/evernote/edam/type/Accounting;->lastFailedChargeReason:Ljava/lang/String;

    return-void
.end method

.method public unsetLastRequestedCharge()V
    .locals 3

    .line 624
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->__isset_vector:[Z

    const/16 v1, 0x9

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetLastSuccessfulCharge()V
    .locals 3

    .line 468
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->__isset_vector:[Z

    const/4 v1, 0x4

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetNextChargeDate()V
    .locals 3

    .line 789
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->__isset_vector:[Z

    const/16 v1, 0xd

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetNextPaymentDue()V
    .locals 3

    .line 535
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->__isset_vector:[Z

    const/4 v1, 0x6

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetPremiumCommerceService()V
    .locals 1

    const/4 v0, 0x0

    .line 399
    iput-object v0, p0, Lcom/evernote/edam/type/Accounting;->premiumCommerceService:Ljava/lang/String;

    return-void
.end method

.method public unsetPremiumLockUntil()V
    .locals 3

    .line 557
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->__isset_vector:[Z

    const/4 v1, 0x7

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetPremiumOrderNumber()V
    .locals 1

    const/4 v0, 0x0

    .line 376
    iput-object v0, p0, Lcom/evernote/edam/type/Accounting;->premiumOrderNumber:Ljava/lang/String;

    return-void
.end method

.method public unsetPremiumServiceSKU()V
    .locals 1

    const/4 v0, 0x0

    .line 444
    iput-object v0, p0, Lcom/evernote/edam/type/Accounting;->premiumServiceSKU:Ljava/lang/String;

    return-void
.end method

.method public unsetPremiumServiceStart()V
    .locals 3

    .line 423
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->__isset_vector:[Z

    const/4 v1, 0x3

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetPremiumServiceStatus()V
    .locals 1

    const/4 v0, 0x0

    .line 353
    iput-object v0, p0, Lcom/evernote/edam/type/Accounting;->premiumServiceStatus:Lcom/evernote/edam/type/PremiumOrderStatus;

    return-void
.end method

.method public unsetPremiumSubscriptionNumber()V
    .locals 1

    const/4 v0, 0x0

    .line 600
    iput-object v0, p0, Lcom/evernote/edam/type/Accounting;->premiumSubscriptionNumber:Ljava/lang/String;

    return-void
.end method

.method public unsetUnitDiscount()V
    .locals 3

    .line 767
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->__isset_vector:[Z

    const/16 v1, 0xc

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetUnitPrice()V
    .locals 3

    .line 669
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->__isset_vector:[Z

    const/16 v1, 0xa

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetUpdated()V
    .locals 3

    .line 579
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->__isset_vector:[Z

    const/16 v1, 0x8

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetUploadLimit()V
    .locals 2

    .line 280
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->__isset_vector:[Z

    const/4 v1, 0x0

    aput-boolean v1, v0, v1

    return-void
.end method

.method public unsetUploadLimitEnd()V
    .locals 3

    .line 302
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->__isset_vector:[Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetUploadLimitNextMonth()V
    .locals 3

    .line 324
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->__isset_vector:[Z

    const/4 v1, 0x2

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public validate()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    return-void
.end method

.method public write(Lcom/evernote/thrift/protocol/TProtocol;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1442
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->validate()V

    .line 1444
    sget-object v0, Lcom/evernote/edam/type/Accounting;->STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeStructBegin(Lcom/evernote/thrift/protocol/TStruct;)V

    .line 1445
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetUploadLimit()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1446
    sget-object v0, Lcom/evernote/edam/type/Accounting;->UPLOAD_LIMIT_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1447
    iget-wide v0, p0, Lcom/evernote/edam/type/Accounting;->uploadLimit:J

    invoke-virtual {p1, v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeI64(J)V

    .line 1448
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1450
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetUploadLimitEnd()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1451
    sget-object v0, Lcom/evernote/edam/type/Accounting;->UPLOAD_LIMIT_END_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1452
    iget-wide v0, p0, Lcom/evernote/edam/type/Accounting;->uploadLimitEnd:J

    invoke-virtual {p1, v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeI64(J)V

    .line 1453
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1455
    :cond_1
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetUploadLimitNextMonth()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1456
    sget-object v0, Lcom/evernote/edam/type/Accounting;->UPLOAD_LIMIT_NEXT_MONTH_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1457
    iget-wide v0, p0, Lcom/evernote/edam/type/Accounting;->uploadLimitNextMonth:J

    invoke-virtual {p1, v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeI64(J)V

    .line 1458
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1460
    :cond_2
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->premiumServiceStatus:Lcom/evernote/edam/type/PremiumOrderStatus;

    if-eqz v0, :cond_3

    .line 1461
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetPremiumServiceStatus()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1462
    sget-object v0, Lcom/evernote/edam/type/Accounting;->PREMIUM_SERVICE_STATUS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1463
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->premiumServiceStatus:Lcom/evernote/edam/type/PremiumOrderStatus;

    invoke-virtual {v0}, Lcom/evernote/edam/type/PremiumOrderStatus;->getValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeI32(I)V

    .line 1464
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1467
    :cond_3
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->premiumOrderNumber:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 1468
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetPremiumOrderNumber()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1469
    sget-object v0, Lcom/evernote/edam/type/Accounting;->PREMIUM_ORDER_NUMBER_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1470
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->premiumOrderNumber:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 1471
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1474
    :cond_4
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->premiumCommerceService:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 1475
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetPremiumCommerceService()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1476
    sget-object v0, Lcom/evernote/edam/type/Accounting;->PREMIUM_COMMERCE_SERVICE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1477
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->premiumCommerceService:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 1478
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1481
    :cond_5
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetPremiumServiceStart()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1482
    sget-object v0, Lcom/evernote/edam/type/Accounting;->PREMIUM_SERVICE_START_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1483
    iget-wide v0, p0, Lcom/evernote/edam/type/Accounting;->premiumServiceStart:J

    invoke-virtual {p1, v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeI64(J)V

    .line 1484
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1486
    :cond_6
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->premiumServiceSKU:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 1487
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetPremiumServiceSKU()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1488
    sget-object v0, Lcom/evernote/edam/type/Accounting;->PREMIUM_SERVICE_SKU_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1489
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->premiumServiceSKU:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 1490
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1493
    :cond_7
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetLastSuccessfulCharge()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1494
    sget-object v0, Lcom/evernote/edam/type/Accounting;->LAST_SUCCESSFUL_CHARGE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1495
    iget-wide v0, p0, Lcom/evernote/edam/type/Accounting;->lastSuccessfulCharge:J

    invoke-virtual {p1, v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeI64(J)V

    .line 1496
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1498
    :cond_8
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetLastFailedCharge()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1499
    sget-object v0, Lcom/evernote/edam/type/Accounting;->LAST_FAILED_CHARGE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1500
    iget-wide v0, p0, Lcom/evernote/edam/type/Accounting;->lastFailedCharge:J

    invoke-virtual {p1, v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeI64(J)V

    .line 1501
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1503
    :cond_9
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->lastFailedChargeReason:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 1504
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetLastFailedChargeReason()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1505
    sget-object v0, Lcom/evernote/edam/type/Accounting;->LAST_FAILED_CHARGE_REASON_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1506
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->lastFailedChargeReason:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 1507
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1510
    :cond_a
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetNextPaymentDue()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1511
    sget-object v0, Lcom/evernote/edam/type/Accounting;->NEXT_PAYMENT_DUE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1512
    iget-wide v0, p0, Lcom/evernote/edam/type/Accounting;->nextPaymentDue:J

    invoke-virtual {p1, v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeI64(J)V

    .line 1513
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1515
    :cond_b
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetPremiumLockUntil()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1516
    sget-object v0, Lcom/evernote/edam/type/Accounting;->PREMIUM_LOCK_UNTIL_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1517
    iget-wide v0, p0, Lcom/evernote/edam/type/Accounting;->premiumLockUntil:J

    invoke-virtual {p1, v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeI64(J)V

    .line 1518
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1520
    :cond_c
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetUpdated()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1521
    sget-object v0, Lcom/evernote/edam/type/Accounting;->UPDATED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1522
    iget-wide v0, p0, Lcom/evernote/edam/type/Accounting;->updated:J

    invoke-virtual {p1, v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeI64(J)V

    .line 1523
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1525
    :cond_d
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->premiumSubscriptionNumber:Ljava/lang/String;

    if-eqz v0, :cond_e

    .line 1526
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetPremiumSubscriptionNumber()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1527
    sget-object v0, Lcom/evernote/edam/type/Accounting;->PREMIUM_SUBSCRIPTION_NUMBER_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1528
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->premiumSubscriptionNumber:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 1529
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1532
    :cond_e
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetLastRequestedCharge()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1533
    sget-object v0, Lcom/evernote/edam/type/Accounting;->LAST_REQUESTED_CHARGE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1534
    iget-wide v0, p0, Lcom/evernote/edam/type/Accounting;->lastRequestedCharge:J

    invoke-virtual {p1, v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeI64(J)V

    .line 1535
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1537
    :cond_f
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->currency:Ljava/lang/String;

    if-eqz v0, :cond_10

    .line 1538
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetCurrency()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1539
    sget-object v0, Lcom/evernote/edam/type/Accounting;->CURRENCY_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1540
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->currency:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 1541
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1544
    :cond_10
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetUnitPrice()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 1545
    sget-object v0, Lcom/evernote/edam/type/Accounting;->UNIT_PRICE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1546
    iget v0, p0, Lcom/evernote/edam/type/Accounting;->unitPrice:I

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeI32(I)V

    .line 1547
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1549
    :cond_11
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetBusinessId()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 1550
    sget-object v0, Lcom/evernote/edam/type/Accounting;->BUSINESS_ID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1551
    iget v0, p0, Lcom/evernote/edam/type/Accounting;->businessId:I

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeI32(I)V

    .line 1552
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1554
    :cond_12
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->businessName:Ljava/lang/String;

    if-eqz v0, :cond_13

    .line 1555
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetBusinessName()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 1556
    sget-object v0, Lcom/evernote/edam/type/Accounting;->BUSINESS_NAME_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1557
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->businessName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 1558
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1561
    :cond_13
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->businessRole:Lcom/evernote/edam/type/BusinessUserRole;

    if-eqz v0, :cond_14

    .line 1562
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetBusinessRole()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 1563
    sget-object v0, Lcom/evernote/edam/type/Accounting;->BUSINESS_ROLE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1564
    iget-object v0, p0, Lcom/evernote/edam/type/Accounting;->businessRole:Lcom/evernote/edam/type/BusinessUserRole;

    invoke-virtual {v0}, Lcom/evernote/edam/type/BusinessUserRole;->getValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeI32(I)V

    .line 1565
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1568
    :cond_14
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetUnitDiscount()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 1569
    sget-object v0, Lcom/evernote/edam/type/Accounting;->UNIT_DISCOUNT_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1570
    iget v0, p0, Lcom/evernote/edam/type/Accounting;->unitDiscount:I

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeI32(I)V

    .line 1571
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1573
    :cond_15
    invoke-virtual {p0}, Lcom/evernote/edam/type/Accounting;->isSetNextChargeDate()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 1574
    sget-object v0, Lcom/evernote/edam/type/Accounting;->NEXT_CHARGE_DATE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1575
    iget-wide v0, p0, Lcom/evernote/edam/type/Accounting;->nextChargeDate:J

    invoke-virtual {p1, v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeI64(J)V

    .line 1576
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1578
    :cond_16
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldStop()V

    .line 1579
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeStructEnd()V

    return-void
.end method

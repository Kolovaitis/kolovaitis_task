.class public Lcom/evernote/edam/type/Note;
.super Ljava/lang/Object;
.source "Note.java"

# interfaces
.implements Lcom/evernote/thrift/TBase;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/evernote/thrift/TBase<",
        "Lcom/evernote/edam/type/Note;",
        ">;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# static fields
.field private static final ACTIVE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final ATTRIBUTES_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final CONTENT_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final CONTENT_HASH_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final CONTENT_LENGTH_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final CREATED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final DELETED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final GUID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final NOTEBOOK_GUID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final RESOURCES_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

.field private static final TAG_GUIDS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final TAG_NAMES_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final TITLE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final UPDATED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final UPDATE_SEQUENCE_NUM_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final __ACTIVE_ISSET_ID:I = 0x4

.field private static final __CONTENTLENGTH_ISSET_ID:I = 0x0

.field private static final __CREATED_ISSET_ID:I = 0x1

.field private static final __DELETED_ISSET_ID:I = 0x3

.field private static final __UPDATED_ISSET_ID:I = 0x2

.field private static final __UPDATESEQUENCENUM_ISSET_ID:I = 0x5


# instance fields
.field private __isset_vector:[Z

.field private active:Z

.field private attributes:Lcom/evernote/edam/type/NoteAttributes;

.field private content:Ljava/lang/String;

.field private contentHash:[B

.field private contentLength:I

.field private created:J

.field private deleted:J

.field private guid:Ljava/lang/String;

.field private notebookGuid:Ljava/lang/String;

.field private resources:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/evernote/edam/type/Resource;",
            ">;"
        }
    .end annotation
.end field

.field private tagGuids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private tagNames:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private title:Ljava/lang/String;

.field private updateSequenceNum:I

.field private updated:J


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 148
    new-instance v0, Lcom/evernote/thrift/protocol/TStruct;

    const-string v1, "Note"

    invoke-direct {v0, v1}, Lcom/evernote/thrift/protocol/TStruct;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/evernote/edam/type/Note;->STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

    .line 150
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "guid"

    const/16 v2, 0xb

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Note;->GUID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 151
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "title"

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Note;->TITLE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 152
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "content"

    const/4 v4, 0x3

    invoke-direct {v0, v1, v2, v4}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Note;->CONTENT_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 153
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "contentHash"

    const/4 v4, 0x4

    invoke-direct {v0, v1, v2, v4}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Note;->CONTENT_HASH_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 154
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "contentLength"

    const/16 v4, 0x8

    const/4 v5, 0x5

    invoke-direct {v0, v1, v4, v5}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Note;->CONTENT_LENGTH_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 155
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "created"

    const/16 v5, 0xa

    const/4 v6, 0x6

    invoke-direct {v0, v1, v5, v6}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Note;->CREATED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 156
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "updated"

    const/4 v6, 0x7

    invoke-direct {v0, v1, v5, v6}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Note;->UPDATED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 157
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "deleted"

    invoke-direct {v0, v1, v5, v4}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Note;->DELETED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 158
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "active"

    const/16 v6, 0x9

    invoke-direct {v0, v1, v3, v6}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Note;->ACTIVE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 159
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "updateSequenceNum"

    invoke-direct {v0, v1, v4, v5}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Note;->UPDATE_SEQUENCE_NUM_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 160
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "notebookGuid"

    invoke-direct {v0, v1, v2, v2}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Note;->NOTEBOOK_GUID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 161
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "tagGuids"

    const/16 v2, 0xc

    const/16 v3, 0xf

    invoke-direct {v0, v1, v3, v2}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Note;->TAG_GUIDS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 162
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "resources"

    const/16 v4, 0xd

    invoke-direct {v0, v1, v3, v4}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Note;->RESOURCES_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 163
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "attributes"

    const/16 v4, 0xe

    invoke-direct {v0, v1, v2, v4}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Note;->ATTRIBUTES_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 164
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "tagNames"

    invoke-direct {v0, v1, v3, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Note;->TAG_NAMES_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 192
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x6

    .line 190
    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/evernote/edam/type/Note;->__isset_vector:[Z

    return-void
.end method

.method public constructor <init>(Lcom/evernote/edam/type/Note;)V
    .locals 4

    .line 198
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x6

    .line 190
    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/evernote/edam/type/Note;->__isset_vector:[Z

    .line 199
    iget-object v0, p1, Lcom/evernote/edam/type/Note;->__isset_vector:[Z

    iget-object v1, p0, Lcom/evernote/edam/type/Note;->__isset_vector:[Z

    array-length v2, v0

    const/4 v3, 0x0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 200
    invoke-virtual {p1}, Lcom/evernote/edam/type/Note;->isSetGuid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 201
    iget-object v0, p1, Lcom/evernote/edam/type/Note;->guid:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/type/Note;->guid:Ljava/lang/String;

    .line 203
    :cond_0
    invoke-virtual {p1}, Lcom/evernote/edam/type/Note;->isSetTitle()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 204
    iget-object v0, p1, Lcom/evernote/edam/type/Note;->title:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/type/Note;->title:Ljava/lang/String;

    .line 206
    :cond_1
    invoke-virtual {p1}, Lcom/evernote/edam/type/Note;->isSetContent()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 207
    iget-object v0, p1, Lcom/evernote/edam/type/Note;->content:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/type/Note;->content:Ljava/lang/String;

    .line 209
    :cond_2
    invoke-virtual {p1}, Lcom/evernote/edam/type/Note;->isSetContentHash()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 210
    iget-object v0, p1, Lcom/evernote/edam/type/Note;->contentHash:[B

    array-length v0, v0

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/evernote/edam/type/Note;->contentHash:[B

    .line 211
    iget-object v0, p1, Lcom/evernote/edam/type/Note;->contentHash:[B

    iget-object v1, p0, Lcom/evernote/edam/type/Note;->contentHash:[B

    array-length v2, v0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 213
    :cond_3
    iget v0, p1, Lcom/evernote/edam/type/Note;->contentLength:I

    iput v0, p0, Lcom/evernote/edam/type/Note;->contentLength:I

    .line 214
    iget-wide v0, p1, Lcom/evernote/edam/type/Note;->created:J

    iput-wide v0, p0, Lcom/evernote/edam/type/Note;->created:J

    .line 215
    iget-wide v0, p1, Lcom/evernote/edam/type/Note;->updated:J

    iput-wide v0, p0, Lcom/evernote/edam/type/Note;->updated:J

    .line 216
    iget-wide v0, p1, Lcom/evernote/edam/type/Note;->deleted:J

    iput-wide v0, p0, Lcom/evernote/edam/type/Note;->deleted:J

    .line 217
    iget-boolean v0, p1, Lcom/evernote/edam/type/Note;->active:Z

    iput-boolean v0, p0, Lcom/evernote/edam/type/Note;->active:Z

    .line 218
    iget v0, p1, Lcom/evernote/edam/type/Note;->updateSequenceNum:I

    iput v0, p0, Lcom/evernote/edam/type/Note;->updateSequenceNum:I

    .line 219
    invoke-virtual {p1}, Lcom/evernote/edam/type/Note;->isSetNotebookGuid()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 220
    iget-object v0, p1, Lcom/evernote/edam/type/Note;->notebookGuid:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/type/Note;->notebookGuid:Ljava/lang/String;

    .line 222
    :cond_4
    invoke-virtual {p1}, Lcom/evernote/edam/type/Note;->isSetTagGuids()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 223
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 224
    iget-object v1, p1, Lcom/evernote/edam/type/Note;->tagGuids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 225
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 227
    :cond_5
    iput-object v0, p0, Lcom/evernote/edam/type/Note;->tagGuids:Ljava/util/List;

    .line 229
    :cond_6
    invoke-virtual {p1}, Lcom/evernote/edam/type/Note;->isSetResources()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 230
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 231
    iget-object v1, p1, Lcom/evernote/edam/type/Note;->resources:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/evernote/edam/type/Resource;

    .line 232
    new-instance v3, Lcom/evernote/edam/type/Resource;

    invoke-direct {v3, v2}, Lcom/evernote/edam/type/Resource;-><init>(Lcom/evernote/edam/type/Resource;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 234
    :cond_7
    iput-object v0, p0, Lcom/evernote/edam/type/Note;->resources:Ljava/util/List;

    .line 236
    :cond_8
    invoke-virtual {p1}, Lcom/evernote/edam/type/Note;->isSetAttributes()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 237
    new-instance v0, Lcom/evernote/edam/type/NoteAttributes;

    iget-object v1, p1, Lcom/evernote/edam/type/Note;->attributes:Lcom/evernote/edam/type/NoteAttributes;

    invoke-direct {v0, v1}, Lcom/evernote/edam/type/NoteAttributes;-><init>(Lcom/evernote/edam/type/NoteAttributes;)V

    iput-object v0, p0, Lcom/evernote/edam/type/Note;->attributes:Lcom/evernote/edam/type/NoteAttributes;

    .line 239
    :cond_9
    invoke-virtual {p1}, Lcom/evernote/edam/type/Note;->isSetTagNames()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 240
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 241
    iget-object p1, p1, Lcom/evernote/edam/type/Note;->tagNames:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 242
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 244
    :cond_a
    iput-object v0, p0, Lcom/evernote/edam/type/Note;->tagNames:Ljava/util/List;

    :cond_b
    return-void
.end method


# virtual methods
.method public addToResources(Lcom/evernote/edam/type/Resource;)V
    .locals 1

    .line 570
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->resources:Ljava/util/List;

    if-nez v0, :cond_0

    .line 571
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/evernote/edam/type/Note;->resources:Ljava/util/List;

    .line 573
    :cond_0
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->resources:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public addToTagGuids(Ljava/lang/String;)V
    .locals 1

    .line 532
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->tagGuids:Ljava/util/List;

    if-nez v0, :cond_0

    .line 533
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/evernote/edam/type/Note;->tagGuids:Ljava/util/List;

    .line 535
    :cond_0
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->tagGuids:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public addToTagNames(Ljava/lang/String;)V
    .locals 1

    .line 631
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->tagNames:Ljava/util/List;

    if-nez v0, :cond_0

    .line 632
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/evernote/edam/type/Note;->tagNames:Ljava/util/List;

    .line 634
    :cond_0
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->tagNames:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public clear()V
    .locals 4

    const/4 v0, 0x0

    .line 253
    iput-object v0, p0, Lcom/evernote/edam/type/Note;->guid:Ljava/lang/String;

    .line 254
    iput-object v0, p0, Lcom/evernote/edam/type/Note;->title:Ljava/lang/String;

    .line 255
    iput-object v0, p0, Lcom/evernote/edam/type/Note;->content:Ljava/lang/String;

    .line 256
    iput-object v0, p0, Lcom/evernote/edam/type/Note;->contentHash:[B

    const/4 v1, 0x0

    .line 257
    invoke-virtual {p0, v1}, Lcom/evernote/edam/type/Note;->setContentLengthIsSet(Z)V

    .line 258
    iput v1, p0, Lcom/evernote/edam/type/Note;->contentLength:I

    .line 259
    invoke-virtual {p0, v1}, Lcom/evernote/edam/type/Note;->setCreatedIsSet(Z)V

    const-wide/16 v2, 0x0

    .line 260
    iput-wide v2, p0, Lcom/evernote/edam/type/Note;->created:J

    .line 261
    invoke-virtual {p0, v1}, Lcom/evernote/edam/type/Note;->setUpdatedIsSet(Z)V

    .line 262
    iput-wide v2, p0, Lcom/evernote/edam/type/Note;->updated:J

    .line 263
    invoke-virtual {p0, v1}, Lcom/evernote/edam/type/Note;->setDeletedIsSet(Z)V

    .line 264
    iput-wide v2, p0, Lcom/evernote/edam/type/Note;->deleted:J

    .line 265
    invoke-virtual {p0, v1}, Lcom/evernote/edam/type/Note;->setActiveIsSet(Z)V

    .line 266
    iput-boolean v1, p0, Lcom/evernote/edam/type/Note;->active:Z

    .line 267
    invoke-virtual {p0, v1}, Lcom/evernote/edam/type/Note;->setUpdateSequenceNumIsSet(Z)V

    .line 268
    iput v1, p0, Lcom/evernote/edam/type/Note;->updateSequenceNum:I

    .line 269
    iput-object v0, p0, Lcom/evernote/edam/type/Note;->notebookGuid:Ljava/lang/String;

    .line 270
    iput-object v0, p0, Lcom/evernote/edam/type/Note;->tagGuids:Ljava/util/List;

    .line 271
    iput-object v0, p0, Lcom/evernote/edam/type/Note;->resources:Ljava/util/List;

    .line 272
    iput-object v0, p0, Lcom/evernote/edam/type/Note;->attributes:Lcom/evernote/edam/type/NoteAttributes;

    .line 273
    iput-object v0, p0, Lcom/evernote/edam/type/Note;->tagNames:Ljava/util/List;

    return-void
.end method

.method public compareTo(Lcom/evernote/edam/type/Note;)I
    .locals 4

    .line 817
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 818
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    return p1

    .line 824
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetGuid()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Note;->isSetGuid()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_1

    return v0

    .line 828
    :cond_1
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetGuid()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/evernote/edam/type/Note;->guid:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/type/Note;->guid:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_2

    return v0

    .line 833
    :cond_2
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetTitle()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Note;->isSetTitle()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_3

    return v0

    .line 837
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetTitle()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/evernote/edam/type/Note;->title:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/type/Note;->title:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_4

    return v0

    .line 842
    :cond_4
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetContent()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Note;->isSetContent()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_5

    return v0

    .line 846
    :cond_5
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetContent()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/evernote/edam/type/Note;->content:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/type/Note;->content:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_6

    return v0

    .line 851
    :cond_6
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetContentHash()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Note;->isSetContentHash()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_7

    return v0

    .line 855
    :cond_7
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetContentHash()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/evernote/edam/type/Note;->contentHash:[B

    iget-object v1, p1, Lcom/evernote/edam/type/Note;->contentHash:[B

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo([B[B)I

    move-result v0

    if-eqz v0, :cond_8

    return v0

    .line 860
    :cond_8
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetContentLength()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Note;->isSetContentLength()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_9

    return v0

    .line 864
    :cond_9
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetContentLength()Z

    move-result v0

    if-eqz v0, :cond_a

    iget v0, p0, Lcom/evernote/edam/type/Note;->contentLength:I

    iget v1, p1, Lcom/evernote/edam/type/Note;->contentLength:I

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(II)I

    move-result v0

    if-eqz v0, :cond_a

    return v0

    .line 869
    :cond_a
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetCreated()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Note;->isSetCreated()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_b

    return v0

    .line 873
    :cond_b
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetCreated()Z

    move-result v0

    if-eqz v0, :cond_c

    iget-wide v0, p0, Lcom/evernote/edam/type/Note;->created:J

    iget-wide v2, p1, Lcom/evernote/edam/type/Note;->created:J

    invoke-static {v0, v1, v2, v3}, Lcom/evernote/thrift/TBaseHelper;->compareTo(JJ)I

    move-result v0

    if-eqz v0, :cond_c

    return v0

    .line 878
    :cond_c
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetUpdated()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Note;->isSetUpdated()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_d

    return v0

    .line 882
    :cond_d
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetUpdated()Z

    move-result v0

    if-eqz v0, :cond_e

    iget-wide v0, p0, Lcom/evernote/edam/type/Note;->updated:J

    iget-wide v2, p1, Lcom/evernote/edam/type/Note;->updated:J

    invoke-static {v0, v1, v2, v3}, Lcom/evernote/thrift/TBaseHelper;->compareTo(JJ)I

    move-result v0

    if-eqz v0, :cond_e

    return v0

    .line 887
    :cond_e
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetDeleted()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Note;->isSetDeleted()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_f

    return v0

    .line 891
    :cond_f
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetDeleted()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-wide v0, p0, Lcom/evernote/edam/type/Note;->deleted:J

    iget-wide v2, p1, Lcom/evernote/edam/type/Note;->deleted:J

    invoke-static {v0, v1, v2, v3}, Lcom/evernote/thrift/TBaseHelper;->compareTo(JJ)I

    move-result v0

    if-eqz v0, :cond_10

    return v0

    .line 896
    :cond_10
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetActive()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Note;->isSetActive()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_11

    return v0

    .line 900
    :cond_11
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetActive()Z

    move-result v0

    if-eqz v0, :cond_12

    iget-boolean v0, p0, Lcom/evernote/edam/type/Note;->active:Z

    iget-boolean v1, p1, Lcom/evernote/edam/type/Note;->active:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_12

    return v0

    .line 905
    :cond_12
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetUpdateSequenceNum()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Note;->isSetUpdateSequenceNum()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_13

    return v0

    .line 909
    :cond_13
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetUpdateSequenceNum()Z

    move-result v0

    if-eqz v0, :cond_14

    iget v0, p0, Lcom/evernote/edam/type/Note;->updateSequenceNum:I

    iget v1, p1, Lcom/evernote/edam/type/Note;->updateSequenceNum:I

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(II)I

    move-result v0

    if-eqz v0, :cond_14

    return v0

    .line 914
    :cond_14
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetNotebookGuid()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Note;->isSetNotebookGuid()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_15

    return v0

    .line 918
    :cond_15
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetNotebookGuid()Z

    move-result v0

    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/evernote/edam/type/Note;->notebookGuid:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/type/Note;->notebookGuid:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_16

    return v0

    .line 923
    :cond_16
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetTagGuids()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Note;->isSetTagGuids()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_17

    return v0

    .line 927
    :cond_17
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetTagGuids()Z

    move-result v0

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/evernote/edam/type/Note;->tagGuids:Ljava/util/List;

    iget-object v1, p1, Lcom/evernote/edam/type/Note;->tagGuids:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/util/List;Ljava/util/List;)I

    move-result v0

    if-eqz v0, :cond_18

    return v0

    .line 932
    :cond_18
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetResources()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Note;->isSetResources()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_19

    return v0

    .line 936
    :cond_19
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetResources()Z

    move-result v0

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lcom/evernote/edam/type/Note;->resources:Ljava/util/List;

    iget-object v1, p1, Lcom/evernote/edam/type/Note;->resources:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/util/List;Ljava/util/List;)I

    move-result v0

    if-eqz v0, :cond_1a

    return v0

    .line 941
    :cond_1a
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetAttributes()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Note;->isSetAttributes()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_1b

    return v0

    .line 945
    :cond_1b
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetAttributes()Z

    move-result v0

    if-eqz v0, :cond_1c

    iget-object v0, p0, Lcom/evernote/edam/type/Note;->attributes:Lcom/evernote/edam/type/NoteAttributes;

    iget-object v1, p1, Lcom/evernote/edam/type/Note;->attributes:Lcom/evernote/edam/type/NoteAttributes;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result v0

    if-eqz v0, :cond_1c

    return v0

    .line 950
    :cond_1c
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetTagNames()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Note;->isSetTagNames()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_1d

    return v0

    .line 954
    :cond_1d
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetTagNames()Z

    move-result v0

    if-eqz v0, :cond_1e

    iget-object v0, p0, Lcom/evernote/edam/type/Note;->tagNames:Ljava/util/List;

    iget-object p1, p1, Lcom/evernote/edam/type/Note;->tagNames:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/util/List;Ljava/util/List;)I

    move-result p1

    if-eqz p1, :cond_1e

    return p1

    :cond_1e
    const/4 p1, 0x0

    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 147
    check-cast p1, Lcom/evernote/edam/type/Note;

    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/Note;->compareTo(Lcom/evernote/edam/type/Note;)I

    move-result p1

    return p1
.end method

.method public deepCopy()Lcom/evernote/edam/type/Note;
    .locals 1

    .line 249
    new-instance v0, Lcom/evernote/edam/type/Note;

    invoke-direct {v0, p0}, Lcom/evernote/edam/type/Note;-><init>(Lcom/evernote/edam/type/Note;)V

    return-object v0
.end method

.method public bridge synthetic deepCopy()Lcom/evernote/thrift/TBase;
    .locals 1

    .line 147
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->deepCopy()Lcom/evernote/edam/type/Note;

    move-result-object v0

    return-object v0
.end method

.method public equals(Lcom/evernote/edam/type/Note;)Z
    .locals 6

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 673
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetGuid()Z

    move-result v1

    .line 674
    invoke-virtual {p1}, Lcom/evernote/edam/type/Note;->isSetGuid()Z

    move-result v2

    if-nez v1, :cond_1

    if-eqz v2, :cond_3

    :cond_1
    if-eqz v1, :cond_3c

    if-nez v2, :cond_2

    goto/16 :goto_e

    .line 678
    :cond_2
    iget-object v1, p0, Lcom/evernote/edam/type/Note;->guid:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/type/Note;->guid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    return v0

    .line 682
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetTitle()Z

    move-result v1

    .line 683
    invoke-virtual {p1}, Lcom/evernote/edam/type/Note;->isSetTitle()Z

    move-result v2

    if-nez v1, :cond_4

    if-eqz v2, :cond_6

    :cond_4
    if-eqz v1, :cond_3b

    if-nez v2, :cond_5

    goto/16 :goto_d

    .line 687
    :cond_5
    iget-object v1, p0, Lcom/evernote/edam/type/Note;->title:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/type/Note;->title:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    return v0

    .line 691
    :cond_6
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetContent()Z

    move-result v1

    .line 692
    invoke-virtual {p1}, Lcom/evernote/edam/type/Note;->isSetContent()Z

    move-result v2

    if-nez v1, :cond_7

    if-eqz v2, :cond_9

    :cond_7
    if-eqz v1, :cond_3a

    if-nez v2, :cond_8

    goto/16 :goto_c

    .line 696
    :cond_8
    iget-object v1, p0, Lcom/evernote/edam/type/Note;->content:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/type/Note;->content:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    return v0

    .line 700
    :cond_9
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetContentHash()Z

    move-result v1

    .line 701
    invoke-virtual {p1}, Lcom/evernote/edam/type/Note;->isSetContentHash()Z

    move-result v2

    if-nez v1, :cond_a

    if-eqz v2, :cond_c

    :cond_a
    if-eqz v1, :cond_39

    if-nez v2, :cond_b

    goto/16 :goto_b

    .line 705
    :cond_b
    iget-object v1, p0, Lcom/evernote/edam/type/Note;->contentHash:[B

    iget-object v2, p1, Lcom/evernote/edam/type/Note;->contentHash:[B

    invoke-static {v1, v2}, Lcom/evernote/thrift/TBaseHelper;->compareTo([B[B)I

    move-result v1

    if-eqz v1, :cond_c

    return v0

    .line 709
    :cond_c
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetContentLength()Z

    move-result v1

    .line 710
    invoke-virtual {p1}, Lcom/evernote/edam/type/Note;->isSetContentLength()Z

    move-result v2

    if-nez v1, :cond_d

    if-eqz v2, :cond_f

    :cond_d
    if-eqz v1, :cond_38

    if-nez v2, :cond_e

    goto/16 :goto_a

    .line 714
    :cond_e
    iget v1, p0, Lcom/evernote/edam/type/Note;->contentLength:I

    iget v2, p1, Lcom/evernote/edam/type/Note;->contentLength:I

    if-eq v1, v2, :cond_f

    return v0

    .line 718
    :cond_f
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetCreated()Z

    move-result v1

    .line 719
    invoke-virtual {p1}, Lcom/evernote/edam/type/Note;->isSetCreated()Z

    move-result v2

    if-nez v1, :cond_10

    if-eqz v2, :cond_12

    :cond_10
    if-eqz v1, :cond_37

    if-nez v2, :cond_11

    goto/16 :goto_9

    .line 723
    :cond_11
    iget-wide v1, p0, Lcom/evernote/edam/type/Note;->created:J

    iget-wide v3, p1, Lcom/evernote/edam/type/Note;->created:J

    cmp-long v5, v1, v3

    if-eqz v5, :cond_12

    return v0

    .line 727
    :cond_12
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetUpdated()Z

    move-result v1

    .line 728
    invoke-virtual {p1}, Lcom/evernote/edam/type/Note;->isSetUpdated()Z

    move-result v2

    if-nez v1, :cond_13

    if-eqz v2, :cond_15

    :cond_13
    if-eqz v1, :cond_36

    if-nez v2, :cond_14

    goto/16 :goto_8

    .line 732
    :cond_14
    iget-wide v1, p0, Lcom/evernote/edam/type/Note;->updated:J

    iget-wide v3, p1, Lcom/evernote/edam/type/Note;->updated:J

    cmp-long v5, v1, v3

    if-eqz v5, :cond_15

    return v0

    .line 736
    :cond_15
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetDeleted()Z

    move-result v1

    .line 737
    invoke-virtual {p1}, Lcom/evernote/edam/type/Note;->isSetDeleted()Z

    move-result v2

    if-nez v1, :cond_16

    if-eqz v2, :cond_18

    :cond_16
    if-eqz v1, :cond_35

    if-nez v2, :cond_17

    goto/16 :goto_7

    .line 741
    :cond_17
    iget-wide v1, p0, Lcom/evernote/edam/type/Note;->deleted:J

    iget-wide v3, p1, Lcom/evernote/edam/type/Note;->deleted:J

    cmp-long v5, v1, v3

    if-eqz v5, :cond_18

    return v0

    .line 745
    :cond_18
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetActive()Z

    move-result v1

    .line 746
    invoke-virtual {p1}, Lcom/evernote/edam/type/Note;->isSetActive()Z

    move-result v2

    if-nez v1, :cond_19

    if-eqz v2, :cond_1b

    :cond_19
    if-eqz v1, :cond_34

    if-nez v2, :cond_1a

    goto/16 :goto_6

    .line 750
    :cond_1a
    iget-boolean v1, p0, Lcom/evernote/edam/type/Note;->active:Z

    iget-boolean v2, p1, Lcom/evernote/edam/type/Note;->active:Z

    if-eq v1, v2, :cond_1b

    return v0

    .line 754
    :cond_1b
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetUpdateSequenceNum()Z

    move-result v1

    .line 755
    invoke-virtual {p1}, Lcom/evernote/edam/type/Note;->isSetUpdateSequenceNum()Z

    move-result v2

    if-nez v1, :cond_1c

    if-eqz v2, :cond_1e

    :cond_1c
    if-eqz v1, :cond_33

    if-nez v2, :cond_1d

    goto/16 :goto_5

    .line 759
    :cond_1d
    iget v1, p0, Lcom/evernote/edam/type/Note;->updateSequenceNum:I

    iget v2, p1, Lcom/evernote/edam/type/Note;->updateSequenceNum:I

    if-eq v1, v2, :cond_1e

    return v0

    .line 763
    :cond_1e
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetNotebookGuid()Z

    move-result v1

    .line 764
    invoke-virtual {p1}, Lcom/evernote/edam/type/Note;->isSetNotebookGuid()Z

    move-result v2

    if-nez v1, :cond_1f

    if-eqz v2, :cond_21

    :cond_1f
    if-eqz v1, :cond_32

    if-nez v2, :cond_20

    goto/16 :goto_4

    .line 768
    :cond_20
    iget-object v1, p0, Lcom/evernote/edam/type/Note;->notebookGuid:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/type/Note;->notebookGuid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_21

    return v0

    .line 772
    :cond_21
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetTagGuids()Z

    move-result v1

    .line 773
    invoke-virtual {p1}, Lcom/evernote/edam/type/Note;->isSetTagGuids()Z

    move-result v2

    if-nez v1, :cond_22

    if-eqz v2, :cond_24

    :cond_22
    if-eqz v1, :cond_31

    if-nez v2, :cond_23

    goto :goto_3

    .line 777
    :cond_23
    iget-object v1, p0, Lcom/evernote/edam/type/Note;->tagGuids:Ljava/util/List;

    iget-object v2, p1, Lcom/evernote/edam/type/Note;->tagGuids:Ljava/util/List;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_24

    return v0

    .line 781
    :cond_24
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetResources()Z

    move-result v1

    .line 782
    invoke-virtual {p1}, Lcom/evernote/edam/type/Note;->isSetResources()Z

    move-result v2

    if-nez v1, :cond_25

    if-eqz v2, :cond_27

    :cond_25
    if-eqz v1, :cond_30

    if-nez v2, :cond_26

    goto :goto_2

    .line 786
    :cond_26
    iget-object v1, p0, Lcom/evernote/edam/type/Note;->resources:Ljava/util/List;

    iget-object v2, p1, Lcom/evernote/edam/type/Note;->resources:Ljava/util/List;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_27

    return v0

    .line 790
    :cond_27
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetAttributes()Z

    move-result v1

    .line 791
    invoke-virtual {p1}, Lcom/evernote/edam/type/Note;->isSetAttributes()Z

    move-result v2

    if-nez v1, :cond_28

    if-eqz v2, :cond_2a

    :cond_28
    if-eqz v1, :cond_2f

    if-nez v2, :cond_29

    goto :goto_1

    .line 795
    :cond_29
    iget-object v1, p0, Lcom/evernote/edam/type/Note;->attributes:Lcom/evernote/edam/type/NoteAttributes;

    iget-object v2, p1, Lcom/evernote/edam/type/Note;->attributes:Lcom/evernote/edam/type/NoteAttributes;

    invoke-virtual {v1, v2}, Lcom/evernote/edam/type/NoteAttributes;->equals(Lcom/evernote/edam/type/NoteAttributes;)Z

    move-result v1

    if-nez v1, :cond_2a

    return v0

    .line 799
    :cond_2a
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetTagNames()Z

    move-result v1

    .line 800
    invoke-virtual {p1}, Lcom/evernote/edam/type/Note;->isSetTagNames()Z

    move-result v2

    if-nez v1, :cond_2b

    if-eqz v2, :cond_2d

    :cond_2b
    if-eqz v1, :cond_2e

    if-nez v2, :cond_2c

    goto :goto_0

    .line 804
    :cond_2c
    iget-object v1, p0, Lcom/evernote/edam/type/Note;->tagNames:Ljava/util/List;

    iget-object p1, p1, Lcom/evernote/edam/type/Note;->tagNames:Ljava/util/List;

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_2d

    return v0

    :cond_2d
    const/4 p1, 0x1

    return p1

    :cond_2e
    :goto_0
    return v0

    :cond_2f
    :goto_1
    return v0

    :cond_30
    :goto_2
    return v0

    :cond_31
    :goto_3
    return v0

    :cond_32
    :goto_4
    return v0

    :cond_33
    :goto_5
    return v0

    :cond_34
    :goto_6
    return v0

    :cond_35
    :goto_7
    return v0

    :cond_36
    :goto_8
    return v0

    :cond_37
    :goto_9
    return v0

    :cond_38
    :goto_a
    return v0

    :cond_39
    :goto_b
    return v0

    :cond_3a
    :goto_c
    return v0

    :cond_3b
    :goto_d
    return v0

    :cond_3c
    :goto_e
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 664
    :cond_0
    instance-of v1, p1, Lcom/evernote/edam/type/Note;

    if-eqz v1, :cond_1

    .line 665
    check-cast p1, Lcom/evernote/edam/type/Note;

    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/Note;->equals(Lcom/evernote/edam/type/Note;)Z

    move-result p1

    return p1

    :cond_1
    return v0
.end method

.method public getAttributes()Lcom/evernote/edam/type/NoteAttributes;
    .locals 1

    .line 600
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->attributes:Lcom/evernote/edam/type/NoteAttributes;

    return-object v0
.end method

.method public getContent()Ljava/lang/String;
    .locals 1

    .line 323
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->content:Ljava/lang/String;

    return-object v0
.end method

.method public getContentHash()[B
    .locals 1

    .line 346
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->contentHash:[B

    return-object v0
.end method

.method public getContentLength()I
    .locals 1

    .line 369
    iget v0, p0, Lcom/evernote/edam/type/Note;->contentLength:I

    return v0
.end method

.method public getCreated()J
    .locals 2

    .line 391
    iget-wide v0, p0, Lcom/evernote/edam/type/Note;->created:J

    return-wide v0
.end method

.method public getDeleted()J
    .locals 2

    .line 435
    iget-wide v0, p0, Lcom/evernote/edam/type/Note;->deleted:J

    return-wide v0
.end method

.method public getGuid()Ljava/lang/String;
    .locals 1

    .line 277
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->guid:Ljava/lang/String;

    return-object v0
.end method

.method public getNotebookGuid()Ljava/lang/String;
    .locals 1

    .line 501
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->notebookGuid:Ljava/lang/String;

    return-object v0
.end method

.method public getResources()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/evernote/edam/type/Resource;",
            ">;"
        }
    .end annotation

    .line 577
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->resources:Ljava/util/List;

    return-object v0
.end method

.method public getResourcesIterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Lcom/evernote/edam/type/Resource;",
            ">;"
        }
    .end annotation

    .line 566
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->resources:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public getResourcesSize()I
    .locals 1

    .line 562
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->resources:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    return v0
.end method

.method public getTagGuids()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 539
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->tagGuids:Ljava/util/List;

    return-object v0
.end method

.method public getTagGuidsIterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 528
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->tagGuids:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public getTagGuidsSize()I
    .locals 1

    .line 524
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->tagGuids:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    return v0
.end method

.method public getTagNames()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 638
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->tagNames:Ljava/util/List;

    return-object v0
.end method

.method public getTagNamesIterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 627
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->tagNames:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public getTagNamesSize()I
    .locals 1

    .line 623
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->tagNames:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .line 300
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->title:Ljava/lang/String;

    return-object v0
.end method

.method public getUpdateSequenceNum()I
    .locals 1

    .line 479
    iget v0, p0, Lcom/evernote/edam/type/Note;->updateSequenceNum:I

    return v0
.end method

.method public getUpdated()J
    .locals 2

    .line 413
    iget-wide v0, p0, Lcom/evernote/edam/type/Note;->updated:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isActive()Z
    .locals 1

    .line 457
    iget-boolean v0, p0, Lcom/evernote/edam/type/Note;->active:Z

    return v0
.end method

.method public isSetActive()Z
    .locals 2

    .line 471
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->__isset_vector:[Z

    const/4 v1, 0x4

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetAttributes()Z
    .locals 1

    .line 613
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->attributes:Lcom/evernote/edam/type/NoteAttributes;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetContent()Z
    .locals 1

    .line 336
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->content:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetContentHash()Z
    .locals 1

    .line 359
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->contentHash:[B

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetContentLength()Z
    .locals 2

    .line 383
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->__isset_vector:[Z

    const/4 v1, 0x0

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetCreated()Z
    .locals 2

    .line 405
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->__isset_vector:[Z

    const/4 v1, 0x1

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetDeleted()Z
    .locals 2

    .line 449
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->__isset_vector:[Z

    const/4 v1, 0x3

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetGuid()Z
    .locals 1

    .line 290
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->guid:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetNotebookGuid()Z
    .locals 1

    .line 514
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->notebookGuid:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetResources()Z
    .locals 1

    .line 590
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->resources:Ljava/util/List;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetTagGuids()Z
    .locals 1

    .line 552
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->tagGuids:Ljava/util/List;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetTagNames()Z
    .locals 1

    .line 651
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->tagNames:Ljava/util/List;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetTitle()Z
    .locals 1

    .line 313
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->title:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetUpdateSequenceNum()Z
    .locals 2

    .line 493
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->__isset_vector:[Z

    const/4 v1, 0x5

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetUpdated()Z
    .locals 2

    .line 427
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->__isset_vector:[Z

    const/4 v1, 0x2

    aget-boolean v0, v0, v1

    return v0
.end method

.method public read(Lcom/evernote/thrift/protocol/TProtocol;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 964
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readStructBegin()Lcom/evernote/thrift/protocol/TStruct;

    .line 967
    :goto_0
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readFieldBegin()Lcom/evernote/thrift/protocol/TField;

    move-result-object v0

    .line 968
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-nez v1, :cond_0

    .line 1120
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readStructEnd()V

    .line 1121
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->validate()V

    return-void

    .line 971
    :cond_0
    iget-short v1, v0, Lcom/evernote/thrift/protocol/TField;->id:S

    const/16 v2, 0x8

    const/4 v3, 0x0

    const/16 v4, 0xf

    const/16 v5, 0xa

    const/16 v6, 0xb

    const/4 v7, 0x1

    packed-switch v1, :pswitch_data_0

    .line 1116
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_4

    .line 1099
    :pswitch_0
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_2

    .line 1101
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readListBegin()Lcom/evernote/thrift/protocol/TList;

    move-result-object v0

    .line 1102
    new-instance v1, Ljava/util/ArrayList;

    iget v2, v0, Lcom/evernote/thrift/protocol/TList;->size:I

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/evernote/edam/type/Note;->tagNames:Ljava/util/List;

    .line 1103
    :goto_1
    iget v1, v0, Lcom/evernote/thrift/protocol/TList;->size:I

    if-ge v3, v1, :cond_1

    .line 1106
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v1

    .line 1107
    iget-object v2, p0, Lcom/evernote/edam/type/Note;->tagNames:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1109
    :cond_1
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readListEnd()V

    goto/16 :goto_4

    .line 1112
    :cond_2
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_4

    .line 1091
    :pswitch_1
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    const/16 v2, 0xc

    if-ne v1, v2, :cond_3

    .line 1092
    new-instance v0, Lcom/evernote/edam/type/NoteAttributes;

    invoke-direct {v0}, Lcom/evernote/edam/type/NoteAttributes;-><init>()V

    iput-object v0, p0, Lcom/evernote/edam/type/Note;->attributes:Lcom/evernote/edam/type/NoteAttributes;

    .line 1093
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->attributes:Lcom/evernote/edam/type/NoteAttributes;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/type/NoteAttributes;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    goto/16 :goto_4

    .line 1095
    :cond_3
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_4

    .line 1073
    :pswitch_2
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_5

    .line 1075
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readListBegin()Lcom/evernote/thrift/protocol/TList;

    move-result-object v0

    .line 1076
    new-instance v1, Ljava/util/ArrayList;

    iget v2, v0, Lcom/evernote/thrift/protocol/TList;->size:I

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/evernote/edam/type/Note;->resources:Ljava/util/List;

    .line 1077
    :goto_2
    iget v1, v0, Lcom/evernote/thrift/protocol/TList;->size:I

    if-ge v3, v1, :cond_4

    .line 1080
    new-instance v1, Lcom/evernote/edam/type/Resource;

    invoke-direct {v1}, Lcom/evernote/edam/type/Resource;-><init>()V

    .line 1081
    invoke-virtual {v1, p1}, Lcom/evernote/edam/type/Resource;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1082
    iget-object v2, p0, Lcom/evernote/edam/type/Note;->resources:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 1084
    :cond_4
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readListEnd()V

    goto/16 :goto_4

    .line 1087
    :cond_5
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_4

    .line 1056
    :pswitch_3
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_7

    .line 1058
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readListBegin()Lcom/evernote/thrift/protocol/TList;

    move-result-object v0

    .line 1059
    new-instance v1, Ljava/util/ArrayList;

    iget v2, v0, Lcom/evernote/thrift/protocol/TList;->size:I

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/evernote/edam/type/Note;->tagGuids:Ljava/util/List;

    .line 1060
    :goto_3
    iget v1, v0, Lcom/evernote/thrift/protocol/TList;->size:I

    if-ge v3, v1, :cond_6

    .line 1063
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v1

    .line 1064
    iget-object v2, p0, Lcom/evernote/edam/type/Note;->tagGuids:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 1066
    :cond_6
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readListEnd()V

    goto/16 :goto_4

    .line 1069
    :cond_7
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_4

    .line 1049
    :pswitch_4
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v6, :cond_8

    .line 1050
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/type/Note;->notebookGuid:Ljava/lang/String;

    goto/16 :goto_4

    .line 1052
    :cond_8
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_4

    .line 1041
    :pswitch_5
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v2, :cond_9

    .line 1042
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI32()I

    move-result v0

    iput v0, p0, Lcom/evernote/edam/type/Note;->updateSequenceNum:I

    .line 1043
    invoke-virtual {p0, v7}, Lcom/evernote/edam/type/Note;->setUpdateSequenceNumIsSet(Z)V

    goto/16 :goto_4

    .line 1045
    :cond_9
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_4

    .line 1033
    :pswitch_6
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    const/4 v2, 0x2

    if-ne v1, v2, :cond_a

    .line 1034
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/type/Note;->active:Z

    .line 1035
    invoke-virtual {p0, v7}, Lcom/evernote/edam/type/Note;->setActiveIsSet(Z)V

    goto/16 :goto_4

    .line 1037
    :cond_a
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_4

    .line 1025
    :pswitch_7
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v5, :cond_b

    .line 1026
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/evernote/edam/type/Note;->deleted:J

    .line 1027
    invoke-virtual {p0, v7}, Lcom/evernote/edam/type/Note;->setDeletedIsSet(Z)V

    goto/16 :goto_4

    .line 1029
    :cond_b
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_4

    .line 1017
    :pswitch_8
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v5, :cond_c

    .line 1018
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/evernote/edam/type/Note;->updated:J

    .line 1019
    invoke-virtual {p0, v7}, Lcom/evernote/edam/type/Note;->setUpdatedIsSet(Z)V

    goto/16 :goto_4

    .line 1021
    :cond_c
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_4

    .line 1009
    :pswitch_9
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v5, :cond_d

    .line 1010
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/evernote/edam/type/Note;->created:J

    .line 1011
    invoke-virtual {p0, v7}, Lcom/evernote/edam/type/Note;->setCreatedIsSet(Z)V

    goto :goto_4

    .line 1013
    :cond_d
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_4

    .line 1001
    :pswitch_a
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v2, :cond_e

    .line 1002
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI32()I

    move-result v0

    iput v0, p0, Lcom/evernote/edam/type/Note;->contentLength:I

    .line 1003
    invoke-virtual {p0, v7}, Lcom/evernote/edam/type/Note;->setContentLengthIsSet(Z)V

    goto :goto_4

    .line 1005
    :cond_e
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_4

    .line 994
    :pswitch_b
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v6, :cond_f

    .line 995
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBytes()[B

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/type/Note;->contentHash:[B

    goto :goto_4

    .line 997
    :cond_f
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_4

    .line 987
    :pswitch_c
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v6, :cond_10

    .line 988
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/type/Note;->content:Ljava/lang/String;

    goto :goto_4

    .line 990
    :cond_10
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_4

    .line 980
    :pswitch_d
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v6, :cond_11

    .line 981
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/type/Note;->title:Ljava/lang/String;

    goto :goto_4

    .line 983
    :cond_11
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_4

    .line 973
    :pswitch_e
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v6, :cond_12

    .line 974
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/type/Note;->guid:Ljava/lang/String;

    goto :goto_4

    .line 976
    :cond_12
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    .line 1118
    :goto_4
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readFieldEnd()V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setActive(Z)V
    .locals 0

    .line 461
    iput-boolean p1, p0, Lcom/evernote/edam/type/Note;->active:Z

    const/4 p1, 0x1

    .line 462
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/Note;->setActiveIsSet(Z)V

    return-void
.end method

.method public setActiveIsSet(Z)V
    .locals 2

    .line 475
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->__isset_vector:[Z

    const/4 v1, 0x4

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setAttributes(Lcom/evernote/edam/type/NoteAttributes;)V
    .locals 0

    .line 604
    iput-object p1, p0, Lcom/evernote/edam/type/Note;->attributes:Lcom/evernote/edam/type/NoteAttributes;

    return-void
.end method

.method public setAttributesIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 618
    iput-object p1, p0, Lcom/evernote/edam/type/Note;->attributes:Lcom/evernote/edam/type/NoteAttributes;

    :cond_0
    return-void
.end method

.method public setContent(Ljava/lang/String;)V
    .locals 0

    .line 327
    iput-object p1, p0, Lcom/evernote/edam/type/Note;->content:Ljava/lang/String;

    return-void
.end method

.method public setContentHash([B)V
    .locals 0

    .line 350
    iput-object p1, p0, Lcom/evernote/edam/type/Note;->contentHash:[B

    return-void
.end method

.method public setContentHashIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 364
    iput-object p1, p0, Lcom/evernote/edam/type/Note;->contentHash:[B

    :cond_0
    return-void
.end method

.method public setContentIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 341
    iput-object p1, p0, Lcom/evernote/edam/type/Note;->content:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setContentLength(I)V
    .locals 0

    .line 373
    iput p1, p0, Lcom/evernote/edam/type/Note;->contentLength:I

    const/4 p1, 0x1

    .line 374
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/Note;->setContentLengthIsSet(Z)V

    return-void
.end method

.method public setContentLengthIsSet(Z)V
    .locals 2

    .line 387
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->__isset_vector:[Z

    const/4 v1, 0x0

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setCreated(J)V
    .locals 0

    .line 395
    iput-wide p1, p0, Lcom/evernote/edam/type/Note;->created:J

    const/4 p1, 0x1

    .line 396
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/Note;->setCreatedIsSet(Z)V

    return-void
.end method

.method public setCreatedIsSet(Z)V
    .locals 2

    .line 409
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->__isset_vector:[Z

    const/4 v1, 0x1

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setDeleted(J)V
    .locals 0

    .line 439
    iput-wide p1, p0, Lcom/evernote/edam/type/Note;->deleted:J

    const/4 p1, 0x1

    .line 440
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/Note;->setDeletedIsSet(Z)V

    return-void
.end method

.method public setDeletedIsSet(Z)V
    .locals 2

    .line 453
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->__isset_vector:[Z

    const/4 v1, 0x3

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setGuid(Ljava/lang/String;)V
    .locals 0

    .line 281
    iput-object p1, p0, Lcom/evernote/edam/type/Note;->guid:Ljava/lang/String;

    return-void
.end method

.method public setGuidIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 295
    iput-object p1, p0, Lcom/evernote/edam/type/Note;->guid:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setNotebookGuid(Ljava/lang/String;)V
    .locals 0

    .line 505
    iput-object p1, p0, Lcom/evernote/edam/type/Note;->notebookGuid:Ljava/lang/String;

    return-void
.end method

.method public setNotebookGuidIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 519
    iput-object p1, p0, Lcom/evernote/edam/type/Note;->notebookGuid:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setResources(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/evernote/edam/type/Resource;",
            ">;)V"
        }
    .end annotation

    .line 581
    iput-object p1, p0, Lcom/evernote/edam/type/Note;->resources:Ljava/util/List;

    return-void
.end method

.method public setResourcesIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 595
    iput-object p1, p0, Lcom/evernote/edam/type/Note;->resources:Ljava/util/List;

    :cond_0
    return-void
.end method

.method public setTagGuids(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 543
    iput-object p1, p0, Lcom/evernote/edam/type/Note;->tagGuids:Ljava/util/List;

    return-void
.end method

.method public setTagGuidsIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 557
    iput-object p1, p0, Lcom/evernote/edam/type/Note;->tagGuids:Ljava/util/List;

    :cond_0
    return-void
.end method

.method public setTagNames(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 642
    iput-object p1, p0, Lcom/evernote/edam/type/Note;->tagNames:Ljava/util/List;

    return-void
.end method

.method public setTagNamesIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 656
    iput-object p1, p0, Lcom/evernote/edam/type/Note;->tagNames:Ljava/util/List;

    :cond_0
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0

    .line 304
    iput-object p1, p0, Lcom/evernote/edam/type/Note;->title:Ljava/lang/String;

    return-void
.end method

.method public setTitleIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 318
    iput-object p1, p0, Lcom/evernote/edam/type/Note;->title:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setUpdateSequenceNum(I)V
    .locals 0

    .line 483
    iput p1, p0, Lcom/evernote/edam/type/Note;->updateSequenceNum:I

    const/4 p1, 0x1

    .line 484
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/Note;->setUpdateSequenceNumIsSet(Z)V

    return-void
.end method

.method public setUpdateSequenceNumIsSet(Z)V
    .locals 2

    .line 497
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->__isset_vector:[Z

    const/4 v1, 0x5

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setUpdated(J)V
    .locals 0

    .line 417
    iput-wide p1, p0, Lcom/evernote/edam/type/Note;->updated:J

    const/4 p1, 0x1

    .line 418
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/Note;->setUpdatedIsSet(Z)V

    return-void
.end method

.method public setUpdatedIsSet(Z)V
    .locals 2

    .line 431
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->__isset_vector:[Z

    const/4 v1, 0x2

    aput-boolean p1, v0, v1

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .line 1248
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Note("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1251
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetGuid()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    const-string v1, "guid:"

    .line 1252
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1253
    iget-object v1, p0, Lcom/evernote/edam/type/Note;->guid:Ljava/lang/String;

    if-nez v1, :cond_0

    const-string v1, "null"

    .line 1254
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1256
    :cond_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    const/4 v1, 0x1

    .line 1260
    :goto_1
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetTitle()Z

    move-result v3

    if-eqz v3, :cond_4

    if-nez v1, :cond_2

    const-string v1, ", "

    .line 1261
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const-string v1, "title:"

    .line 1262
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1263
    iget-object v1, p0, Lcom/evernote/edam/type/Note;->title:Ljava/lang/String;

    if-nez v1, :cond_3

    const-string v1, "null"

    .line 1264
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 1266
    :cond_3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_2
    const/4 v1, 0x0

    .line 1270
    :cond_4
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetContent()Z

    move-result v3

    if-eqz v3, :cond_7

    if-nez v1, :cond_5

    const-string v1, ", "

    .line 1271
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    const-string v1, "content:"

    .line 1272
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1273
    iget-object v1, p0, Lcom/evernote/edam/type/Note;->content:Ljava/lang/String;

    if-nez v1, :cond_6

    const-string v1, "null"

    .line 1274
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 1276
    :cond_6
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    const/4 v1, 0x0

    .line 1280
    :cond_7
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetContentHash()Z

    move-result v3

    if-eqz v3, :cond_a

    if-nez v1, :cond_8

    const-string v1, ", "

    .line 1281
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_8
    const-string v1, "contentHash:"

    .line 1282
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1283
    iget-object v1, p0, Lcom/evernote/edam/type/Note;->contentHash:[B

    if-nez v1, :cond_9

    const-string v1, "null"

    .line 1284
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 1286
    :cond_9
    invoke-static {v1, v0}, Lcom/evernote/thrift/TBaseHelper;->toString([BLjava/lang/StringBuilder;)V

    :goto_4
    const/4 v1, 0x0

    .line 1290
    :cond_a
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetContentLength()Z

    move-result v3

    if-eqz v3, :cond_c

    if-nez v1, :cond_b

    const-string v1, ", "

    .line 1291
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_b
    const-string v1, "contentLength:"

    .line 1292
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1293
    iget v1, p0, Lcom/evernote/edam/type/Note;->contentLength:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1296
    :cond_c
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetCreated()Z

    move-result v3

    if-eqz v3, :cond_e

    if-nez v1, :cond_d

    const-string v1, ", "

    .line 1297
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_d
    const-string v1, "created:"

    .line 1298
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1299
    iget-wide v3, p0, Lcom/evernote/edam/type/Note;->created:J

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1302
    :cond_e
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetUpdated()Z

    move-result v3

    if-eqz v3, :cond_10

    if-nez v1, :cond_f

    const-string v1, ", "

    .line 1303
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_f
    const-string v1, "updated:"

    .line 1304
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1305
    iget-wide v3, p0, Lcom/evernote/edam/type/Note;->updated:J

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1308
    :cond_10
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetDeleted()Z

    move-result v3

    if-eqz v3, :cond_12

    if-nez v1, :cond_11

    const-string v1, ", "

    .line 1309
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_11
    const-string v1, "deleted:"

    .line 1310
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1311
    iget-wide v3, p0, Lcom/evernote/edam/type/Note;->deleted:J

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1314
    :cond_12
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetActive()Z

    move-result v3

    if-eqz v3, :cond_14

    if-nez v1, :cond_13

    const-string v1, ", "

    .line 1315
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_13
    const-string v1, "active:"

    .line 1316
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1317
    iget-boolean v1, p0, Lcom/evernote/edam/type/Note;->active:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1320
    :cond_14
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetUpdateSequenceNum()Z

    move-result v3

    if-eqz v3, :cond_16

    if-nez v1, :cond_15

    const-string v1, ", "

    .line 1321
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_15
    const-string v1, "updateSequenceNum:"

    .line 1322
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1323
    iget v1, p0, Lcom/evernote/edam/type/Note;->updateSequenceNum:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1326
    :cond_16
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetNotebookGuid()Z

    move-result v3

    if-eqz v3, :cond_19

    if-nez v1, :cond_17

    const-string v1, ", "

    .line 1327
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_17
    const-string v1, "notebookGuid:"

    .line 1328
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1329
    iget-object v1, p0, Lcom/evernote/edam/type/Note;->notebookGuid:Ljava/lang/String;

    if-nez v1, :cond_18

    const-string v1, "null"

    .line 1330
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5

    .line 1332
    :cond_18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_5
    const/4 v1, 0x0

    .line 1336
    :cond_19
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetTagGuids()Z

    move-result v3

    if-eqz v3, :cond_1c

    if-nez v1, :cond_1a

    const-string v1, ", "

    .line 1337
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1a
    const-string v1, "tagGuids:"

    .line 1338
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1339
    iget-object v1, p0, Lcom/evernote/edam/type/Note;->tagGuids:Ljava/util/List;

    if-nez v1, :cond_1b

    const-string v1, "null"

    .line 1340
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6

    .line 1342
    :cond_1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :goto_6
    const/4 v1, 0x0

    .line 1346
    :cond_1c
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetResources()Z

    move-result v3

    if-eqz v3, :cond_1f

    if-nez v1, :cond_1d

    const-string v1, ", "

    .line 1347
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1d
    const-string v1, "resources:"

    .line 1348
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1349
    iget-object v1, p0, Lcom/evernote/edam/type/Note;->resources:Ljava/util/List;

    if-nez v1, :cond_1e

    const-string v1, "null"

    .line 1350
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_7

    .line 1352
    :cond_1e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :goto_7
    const/4 v1, 0x0

    .line 1356
    :cond_1f
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetAttributes()Z

    move-result v3

    if-eqz v3, :cond_22

    if-nez v1, :cond_20

    const-string v1, ", "

    .line 1357
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_20
    const-string v1, "attributes:"

    .line 1358
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1359
    iget-object v1, p0, Lcom/evernote/edam/type/Note;->attributes:Lcom/evernote/edam/type/NoteAttributes;

    if-nez v1, :cond_21

    const-string v1, "null"

    .line 1360
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_8

    .line 1362
    :cond_21
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :goto_8
    const/4 v1, 0x0

    .line 1366
    :cond_22
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetTagNames()Z

    move-result v2

    if-eqz v2, :cond_25

    if-nez v1, :cond_23

    const-string v1, ", "

    .line 1367
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_23
    const-string v1, "tagNames:"

    .line 1368
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1369
    iget-object v1, p0, Lcom/evernote/edam/type/Note;->tagNames:Ljava/util/List;

    if-nez v1, :cond_24

    const-string v1, "null"

    .line 1370
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_9

    .line 1372
    :cond_24
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_25
    :goto_9
    const-string v1, ")"

    .line 1376
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1377
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public unsetActive()V
    .locals 3

    .line 466
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->__isset_vector:[Z

    const/4 v1, 0x4

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetAttributes()V
    .locals 1

    const/4 v0, 0x0

    .line 608
    iput-object v0, p0, Lcom/evernote/edam/type/Note;->attributes:Lcom/evernote/edam/type/NoteAttributes;

    return-void
.end method

.method public unsetContent()V
    .locals 1

    const/4 v0, 0x0

    .line 331
    iput-object v0, p0, Lcom/evernote/edam/type/Note;->content:Ljava/lang/String;

    return-void
.end method

.method public unsetContentHash()V
    .locals 1

    const/4 v0, 0x0

    .line 354
    iput-object v0, p0, Lcom/evernote/edam/type/Note;->contentHash:[B

    return-void
.end method

.method public unsetContentLength()V
    .locals 2

    .line 378
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->__isset_vector:[Z

    const/4 v1, 0x0

    aput-boolean v1, v0, v1

    return-void
.end method

.method public unsetCreated()V
    .locals 3

    .line 400
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->__isset_vector:[Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetDeleted()V
    .locals 3

    .line 444
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->__isset_vector:[Z

    const/4 v1, 0x3

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetGuid()V
    .locals 1

    const/4 v0, 0x0

    .line 285
    iput-object v0, p0, Lcom/evernote/edam/type/Note;->guid:Ljava/lang/String;

    return-void
.end method

.method public unsetNotebookGuid()V
    .locals 1

    const/4 v0, 0x0

    .line 509
    iput-object v0, p0, Lcom/evernote/edam/type/Note;->notebookGuid:Ljava/lang/String;

    return-void
.end method

.method public unsetResources()V
    .locals 1

    const/4 v0, 0x0

    .line 585
    iput-object v0, p0, Lcom/evernote/edam/type/Note;->resources:Ljava/util/List;

    return-void
.end method

.method public unsetTagGuids()V
    .locals 1

    const/4 v0, 0x0

    .line 547
    iput-object v0, p0, Lcom/evernote/edam/type/Note;->tagGuids:Ljava/util/List;

    return-void
.end method

.method public unsetTagNames()V
    .locals 1

    const/4 v0, 0x0

    .line 646
    iput-object v0, p0, Lcom/evernote/edam/type/Note;->tagNames:Ljava/util/List;

    return-void
.end method

.method public unsetTitle()V
    .locals 1

    const/4 v0, 0x0

    .line 308
    iput-object v0, p0, Lcom/evernote/edam/type/Note;->title:Ljava/lang/String;

    return-void
.end method

.method public unsetUpdateSequenceNum()V
    .locals 3

    .line 488
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->__isset_vector:[Z

    const/4 v1, 0x5

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetUpdated()V
    .locals 3

    .line 422
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->__isset_vector:[Z

    const/4 v1, 0x2

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public validate()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    return-void
.end method

.method public write(Lcom/evernote/thrift/protocol/TProtocol;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1125
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->validate()V

    .line 1127
    sget-object v0, Lcom/evernote/edam/type/Note;->STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeStructBegin(Lcom/evernote/thrift/protocol/TStruct;)V

    .line 1128
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->guid:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1129
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetGuid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1130
    sget-object v0, Lcom/evernote/edam/type/Note;->GUID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1131
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->guid:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 1132
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1135
    :cond_0
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->title:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1136
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetTitle()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1137
    sget-object v0, Lcom/evernote/edam/type/Note;->TITLE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1138
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->title:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 1139
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1142
    :cond_1
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->content:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1143
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetContent()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1144
    sget-object v0, Lcom/evernote/edam/type/Note;->CONTENT_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1145
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->content:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 1146
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1149
    :cond_2
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->contentHash:[B

    if-eqz v0, :cond_3

    .line 1150
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetContentHash()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1151
    sget-object v0, Lcom/evernote/edam/type/Note;->CONTENT_HASH_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1152
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->contentHash:[B

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBinary([B)V

    .line 1153
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1156
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetContentLength()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1157
    sget-object v0, Lcom/evernote/edam/type/Note;->CONTENT_LENGTH_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1158
    iget v0, p0, Lcom/evernote/edam/type/Note;->contentLength:I

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeI32(I)V

    .line 1159
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1161
    :cond_4
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetCreated()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1162
    sget-object v0, Lcom/evernote/edam/type/Note;->CREATED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1163
    iget-wide v0, p0, Lcom/evernote/edam/type/Note;->created:J

    invoke-virtual {p1, v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeI64(J)V

    .line 1164
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1166
    :cond_5
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetUpdated()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1167
    sget-object v0, Lcom/evernote/edam/type/Note;->UPDATED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1168
    iget-wide v0, p0, Lcom/evernote/edam/type/Note;->updated:J

    invoke-virtual {p1, v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeI64(J)V

    .line 1169
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1171
    :cond_6
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetDeleted()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1172
    sget-object v0, Lcom/evernote/edam/type/Note;->DELETED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1173
    iget-wide v0, p0, Lcom/evernote/edam/type/Note;->deleted:J

    invoke-virtual {p1, v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeI64(J)V

    .line 1174
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1176
    :cond_7
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetActive()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1177
    sget-object v0, Lcom/evernote/edam/type/Note;->ACTIVE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1178
    iget-boolean v0, p0, Lcom/evernote/edam/type/Note;->active:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 1179
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1181
    :cond_8
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetUpdateSequenceNum()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1182
    sget-object v0, Lcom/evernote/edam/type/Note;->UPDATE_SEQUENCE_NUM_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1183
    iget v0, p0, Lcom/evernote/edam/type/Note;->updateSequenceNum:I

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeI32(I)V

    .line 1184
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1186
    :cond_9
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->notebookGuid:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 1187
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetNotebookGuid()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1188
    sget-object v0, Lcom/evernote/edam/type/Note;->NOTEBOOK_GUID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1189
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->notebookGuid:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 1190
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1193
    :cond_a
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->tagGuids:Ljava/util/List;

    const/16 v1, 0xb

    if-eqz v0, :cond_c

    .line 1194
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetTagGuids()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1195
    sget-object v0, Lcom/evernote/edam/type/Note;->TAG_GUIDS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1197
    new-instance v0, Lcom/evernote/thrift/protocol/TList;

    iget-object v2, p0, Lcom/evernote/edam/type/Note;->tagGuids:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/protocol/TList;-><init>(BI)V

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeListBegin(Lcom/evernote/thrift/protocol/TList;)V

    .line 1198
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->tagGuids:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1200
    invoke-virtual {p1, v2}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 1202
    :cond_b
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeListEnd()V

    .line 1204
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1207
    :cond_c
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->resources:Ljava/util/List;

    if-eqz v0, :cond_e

    .line 1208
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetResources()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1209
    sget-object v0, Lcom/evernote/edam/type/Note;->RESOURCES_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1211
    new-instance v0, Lcom/evernote/thrift/protocol/TList;

    const/16 v2, 0xc

    iget-object v3, p0, Lcom/evernote/edam/type/Note;->resources:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v0, v2, v3}, Lcom/evernote/thrift/protocol/TList;-><init>(BI)V

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeListBegin(Lcom/evernote/thrift/protocol/TList;)V

    .line 1212
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->resources:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_d

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/evernote/edam/type/Resource;

    .line 1214
    invoke-virtual {v2, p1}, Lcom/evernote/edam/type/Resource;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    goto :goto_1

    .line 1216
    :cond_d
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeListEnd()V

    .line 1218
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1221
    :cond_e
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->attributes:Lcom/evernote/edam/type/NoteAttributes;

    if-eqz v0, :cond_f

    .line 1222
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetAttributes()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1223
    sget-object v0, Lcom/evernote/edam/type/Note;->ATTRIBUTES_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1224
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->attributes:Lcom/evernote/edam/type/NoteAttributes;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/type/NoteAttributes;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1225
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1228
    :cond_f
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->tagNames:Ljava/util/List;

    if-eqz v0, :cond_11

    .line 1229
    invoke-virtual {p0}, Lcom/evernote/edam/type/Note;->isSetTagNames()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 1230
    sget-object v0, Lcom/evernote/edam/type/Note;->TAG_NAMES_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1232
    new-instance v0, Lcom/evernote/thrift/protocol/TList;

    iget-object v2, p0, Lcom/evernote/edam/type/Note;->tagNames:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/protocol/TList;-><init>(BI)V

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeListBegin(Lcom/evernote/thrift/protocol/TList;)V

    .line 1233
    iget-object v0, p0, Lcom/evernote/edam/type/Note;->tagNames:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_10

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1235
    invoke-virtual {p1, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    goto :goto_2

    .line 1237
    :cond_10
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeListEnd()V

    .line 1239
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1242
    :cond_11
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldStop()V

    .line 1243
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeStructEnd()V

    return-void
.end method

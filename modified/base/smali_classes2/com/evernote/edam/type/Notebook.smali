.class public Lcom/evernote/edam/type/Notebook;
.super Ljava/lang/Object;
.source "Notebook.java"

# interfaces
.implements Lcom/evernote/thrift/TBase;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/evernote/thrift/TBase<",
        "Lcom/evernote/edam/type/Notebook;",
        ">;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# static fields
.field private static final BUSINESS_NOTEBOOK_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final CONTACT_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final DEFAULT_NOTEBOOK_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final GUID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final NAME_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final PUBLISHED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final PUBLISHING_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final RESTRICTIONS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final SERVICE_CREATED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final SERVICE_UPDATED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final SHARED_NOTEBOOKS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final SHARED_NOTEBOOK_IDS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final STACK_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

.field private static final UPDATE_SEQUENCE_NUM_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final __DEFAULTNOTEBOOK_ISSET_ID:I = 0x1

.field private static final __PUBLISHED_ISSET_ID:I = 0x4

.field private static final __SERVICECREATED_ISSET_ID:I = 0x2

.field private static final __SERVICEUPDATED_ISSET_ID:I = 0x3

.field private static final __UPDATESEQUENCENUM_ISSET_ID:I


# instance fields
.field private __isset_vector:[Z

.field private businessNotebook:Lcom/evernote/edam/type/BusinessNotebook;

.field private contact:Lcom/evernote/edam/type/User;

.field private defaultNotebook:Z

.field private guid:Ljava/lang/String;

.field private name:Ljava/lang/String;

.field private published:Z

.field private publishing:Lcom/evernote/edam/type/Publishing;

.field private restrictions:Lcom/evernote/edam/type/NotebookRestrictions;

.field private serviceCreated:J

.field private serviceUpdated:J

.field private sharedNotebookIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private sharedNotebooks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/evernote/edam/type/SharedNotebook;",
            ">;"
        }
    .end annotation
.end field

.field private stack:Ljava/lang/String;

.field private updateSequenceNum:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 137
    new-instance v0, Lcom/evernote/thrift/protocol/TStruct;

    const-string v1, "Notebook"

    invoke-direct {v0, v1}, Lcom/evernote/thrift/protocol/TStruct;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/evernote/edam/type/Notebook;->STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

    .line 139
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "guid"

    const/16 v2, 0xb

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Notebook;->GUID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 140
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "name"

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Notebook;->NAME_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 141
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "updateSequenceNum"

    const/16 v4, 0x8

    const/4 v5, 0x5

    invoke-direct {v0, v1, v4, v5}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Notebook;->UPDATE_SEQUENCE_NUM_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 142
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "defaultNotebook"

    const/4 v5, 0x6

    invoke-direct {v0, v1, v3, v5}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Notebook;->DEFAULT_NOTEBOOK_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 143
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "serviceCreated"

    const/16 v5, 0xa

    const/4 v6, 0x7

    invoke-direct {v0, v1, v5, v6}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Notebook;->SERVICE_CREATED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 144
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "serviceUpdated"

    invoke-direct {v0, v1, v5, v4}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Notebook;->SERVICE_UPDATED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 145
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "publishing"

    const/16 v4, 0xc

    invoke-direct {v0, v1, v4, v5}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Notebook;->PUBLISHING_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 146
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "published"

    invoke-direct {v0, v1, v3, v2}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Notebook;->PUBLISHED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 147
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "stack"

    invoke-direct {v0, v1, v2, v4}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Notebook;->STACK_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 148
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "sharedNotebookIds"

    const/16 v2, 0xf

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Notebook;->SHARED_NOTEBOOK_IDS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 149
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "sharedNotebooks"

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Notebook;->SHARED_NOTEBOOKS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 150
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "businessNotebook"

    invoke-direct {v0, v1, v4, v2}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Notebook;->BUSINESS_NOTEBOOK_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 151
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "contact"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v4, v2}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Notebook;->CONTACT_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 152
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "restrictions"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v4, v2}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Notebook;->RESTRICTIONS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 178
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x5

    .line 176
    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/evernote/edam/type/Notebook;->__isset_vector:[Z

    return-void
.end method

.method public constructor <init>(Lcom/evernote/edam/type/Notebook;)V
    .locals 4

    .line 184
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x5

    .line 176
    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/evernote/edam/type/Notebook;->__isset_vector:[Z

    .line 185
    iget-object v0, p1, Lcom/evernote/edam/type/Notebook;->__isset_vector:[Z

    iget-object v1, p0, Lcom/evernote/edam/type/Notebook;->__isset_vector:[Z

    array-length v2, v0

    const/4 v3, 0x0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 186
    invoke-virtual {p1}, Lcom/evernote/edam/type/Notebook;->isSetGuid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p1, Lcom/evernote/edam/type/Notebook;->guid:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/type/Notebook;->guid:Ljava/lang/String;

    .line 189
    :cond_0
    invoke-virtual {p1}, Lcom/evernote/edam/type/Notebook;->isSetName()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 190
    iget-object v0, p1, Lcom/evernote/edam/type/Notebook;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/type/Notebook;->name:Ljava/lang/String;

    .line 192
    :cond_1
    iget v0, p1, Lcom/evernote/edam/type/Notebook;->updateSequenceNum:I

    iput v0, p0, Lcom/evernote/edam/type/Notebook;->updateSequenceNum:I

    .line 193
    iget-boolean v0, p1, Lcom/evernote/edam/type/Notebook;->defaultNotebook:Z

    iput-boolean v0, p0, Lcom/evernote/edam/type/Notebook;->defaultNotebook:Z

    .line 194
    iget-wide v0, p1, Lcom/evernote/edam/type/Notebook;->serviceCreated:J

    iput-wide v0, p0, Lcom/evernote/edam/type/Notebook;->serviceCreated:J

    .line 195
    iget-wide v0, p1, Lcom/evernote/edam/type/Notebook;->serviceUpdated:J

    iput-wide v0, p0, Lcom/evernote/edam/type/Notebook;->serviceUpdated:J

    .line 196
    invoke-virtual {p1}, Lcom/evernote/edam/type/Notebook;->isSetPublishing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 197
    new-instance v0, Lcom/evernote/edam/type/Publishing;

    iget-object v1, p1, Lcom/evernote/edam/type/Notebook;->publishing:Lcom/evernote/edam/type/Publishing;

    invoke-direct {v0, v1}, Lcom/evernote/edam/type/Publishing;-><init>(Lcom/evernote/edam/type/Publishing;)V

    iput-object v0, p0, Lcom/evernote/edam/type/Notebook;->publishing:Lcom/evernote/edam/type/Publishing;

    .line 199
    :cond_2
    iget-boolean v0, p1, Lcom/evernote/edam/type/Notebook;->published:Z

    iput-boolean v0, p0, Lcom/evernote/edam/type/Notebook;->published:Z

    .line 200
    invoke-virtual {p1}, Lcom/evernote/edam/type/Notebook;->isSetStack()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 201
    iget-object v0, p1, Lcom/evernote/edam/type/Notebook;->stack:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/type/Notebook;->stack:Ljava/lang/String;

    .line 203
    :cond_3
    invoke-virtual {p1}, Lcom/evernote/edam/type/Notebook;->isSetSharedNotebookIds()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 204
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 205
    iget-object v1, p1, Lcom/evernote/edam/type/Notebook;->sharedNotebookIds:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 206
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 208
    :cond_4
    iput-object v0, p0, Lcom/evernote/edam/type/Notebook;->sharedNotebookIds:Ljava/util/List;

    .line 210
    :cond_5
    invoke-virtual {p1}, Lcom/evernote/edam/type/Notebook;->isSetSharedNotebooks()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 211
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 212
    iget-object v1, p1, Lcom/evernote/edam/type/Notebook;->sharedNotebooks:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/evernote/edam/type/SharedNotebook;

    .line 213
    new-instance v3, Lcom/evernote/edam/type/SharedNotebook;

    invoke-direct {v3, v2}, Lcom/evernote/edam/type/SharedNotebook;-><init>(Lcom/evernote/edam/type/SharedNotebook;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 215
    :cond_6
    iput-object v0, p0, Lcom/evernote/edam/type/Notebook;->sharedNotebooks:Ljava/util/List;

    .line 217
    :cond_7
    invoke-virtual {p1}, Lcom/evernote/edam/type/Notebook;->isSetBusinessNotebook()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 218
    new-instance v0, Lcom/evernote/edam/type/BusinessNotebook;

    iget-object v1, p1, Lcom/evernote/edam/type/Notebook;->businessNotebook:Lcom/evernote/edam/type/BusinessNotebook;

    invoke-direct {v0, v1}, Lcom/evernote/edam/type/BusinessNotebook;-><init>(Lcom/evernote/edam/type/BusinessNotebook;)V

    iput-object v0, p0, Lcom/evernote/edam/type/Notebook;->businessNotebook:Lcom/evernote/edam/type/BusinessNotebook;

    .line 220
    :cond_8
    invoke-virtual {p1}, Lcom/evernote/edam/type/Notebook;->isSetContact()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 221
    new-instance v0, Lcom/evernote/edam/type/User;

    iget-object v1, p1, Lcom/evernote/edam/type/Notebook;->contact:Lcom/evernote/edam/type/User;

    invoke-direct {v0, v1}, Lcom/evernote/edam/type/User;-><init>(Lcom/evernote/edam/type/User;)V

    iput-object v0, p0, Lcom/evernote/edam/type/Notebook;->contact:Lcom/evernote/edam/type/User;

    .line 223
    :cond_9
    invoke-virtual {p1}, Lcom/evernote/edam/type/Notebook;->isSetRestrictions()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 224
    new-instance v0, Lcom/evernote/edam/type/NotebookRestrictions;

    iget-object p1, p1, Lcom/evernote/edam/type/Notebook;->restrictions:Lcom/evernote/edam/type/NotebookRestrictions;

    invoke-direct {v0, p1}, Lcom/evernote/edam/type/NotebookRestrictions;-><init>(Lcom/evernote/edam/type/NotebookRestrictions;)V

    iput-object v0, p0, Lcom/evernote/edam/type/Notebook;->restrictions:Lcom/evernote/edam/type/NotebookRestrictions;

    :cond_a
    return-void
.end method


# virtual methods
.method public addToSharedNotebookIds(J)V
    .locals 1

    .line 465
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->sharedNotebookIds:Ljava/util/List;

    if-nez v0, :cond_0

    .line 466
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/evernote/edam/type/Notebook;->sharedNotebookIds:Ljava/util/List;

    .line 468
    :cond_0
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->sharedNotebookIds:Ljava/util/List;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public addToSharedNotebooks(Lcom/evernote/edam/type/SharedNotebook;)V
    .locals 1

    .line 503
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->sharedNotebooks:Ljava/util/List;

    if-nez v0, :cond_0

    .line 504
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/evernote/edam/type/Notebook;->sharedNotebooks:Ljava/util/List;

    .line 506
    :cond_0
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->sharedNotebooks:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public clear()V
    .locals 4

    const/4 v0, 0x0

    .line 233
    iput-object v0, p0, Lcom/evernote/edam/type/Notebook;->guid:Ljava/lang/String;

    .line 234
    iput-object v0, p0, Lcom/evernote/edam/type/Notebook;->name:Ljava/lang/String;

    const/4 v1, 0x0

    .line 235
    invoke-virtual {p0, v1}, Lcom/evernote/edam/type/Notebook;->setUpdateSequenceNumIsSet(Z)V

    .line 236
    iput v1, p0, Lcom/evernote/edam/type/Notebook;->updateSequenceNum:I

    .line 237
    invoke-virtual {p0, v1}, Lcom/evernote/edam/type/Notebook;->setDefaultNotebookIsSet(Z)V

    .line 238
    iput-boolean v1, p0, Lcom/evernote/edam/type/Notebook;->defaultNotebook:Z

    .line 239
    invoke-virtual {p0, v1}, Lcom/evernote/edam/type/Notebook;->setServiceCreatedIsSet(Z)V

    const-wide/16 v2, 0x0

    .line 240
    iput-wide v2, p0, Lcom/evernote/edam/type/Notebook;->serviceCreated:J

    .line 241
    invoke-virtual {p0, v1}, Lcom/evernote/edam/type/Notebook;->setServiceUpdatedIsSet(Z)V

    .line 242
    iput-wide v2, p0, Lcom/evernote/edam/type/Notebook;->serviceUpdated:J

    .line 243
    iput-object v0, p0, Lcom/evernote/edam/type/Notebook;->publishing:Lcom/evernote/edam/type/Publishing;

    .line 244
    invoke-virtual {p0, v1}, Lcom/evernote/edam/type/Notebook;->setPublishedIsSet(Z)V

    .line 245
    iput-boolean v1, p0, Lcom/evernote/edam/type/Notebook;->published:Z

    .line 246
    iput-object v0, p0, Lcom/evernote/edam/type/Notebook;->stack:Ljava/lang/String;

    .line 247
    iput-object v0, p0, Lcom/evernote/edam/type/Notebook;->sharedNotebookIds:Ljava/util/List;

    .line 248
    iput-object v0, p0, Lcom/evernote/edam/type/Notebook;->sharedNotebooks:Ljava/util/List;

    .line 249
    iput-object v0, p0, Lcom/evernote/edam/type/Notebook;->businessNotebook:Lcom/evernote/edam/type/BusinessNotebook;

    .line 250
    iput-object v0, p0, Lcom/evernote/edam/type/Notebook;->contact:Lcom/evernote/edam/type/User;

    .line 251
    iput-object v0, p0, Lcom/evernote/edam/type/Notebook;->restrictions:Lcom/evernote/edam/type/NotebookRestrictions;

    return-void
.end method

.method public compareTo(Lcom/evernote/edam/type/Notebook;)I
    .locals 4

    .line 749
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 750
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    return p1

    .line 756
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetGuid()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Notebook;->isSetGuid()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_1

    return v0

    .line 760
    :cond_1
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetGuid()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->guid:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/type/Notebook;->guid:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_2

    return v0

    .line 765
    :cond_2
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetName()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Notebook;->isSetName()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_3

    return v0

    .line 769
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetName()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->name:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/type/Notebook;->name:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_4

    return v0

    .line 774
    :cond_4
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetUpdateSequenceNum()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Notebook;->isSetUpdateSequenceNum()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_5

    return v0

    .line 778
    :cond_5
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetUpdateSequenceNum()Z

    move-result v0

    if-eqz v0, :cond_6

    iget v0, p0, Lcom/evernote/edam/type/Notebook;->updateSequenceNum:I

    iget v1, p1, Lcom/evernote/edam/type/Notebook;->updateSequenceNum:I

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(II)I

    move-result v0

    if-eqz v0, :cond_6

    return v0

    .line 783
    :cond_6
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetDefaultNotebook()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Notebook;->isSetDefaultNotebook()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_7

    return v0

    .line 787
    :cond_7
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetDefaultNotebook()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-boolean v0, p0, Lcom/evernote/edam/type/Notebook;->defaultNotebook:Z

    iget-boolean v1, p1, Lcom/evernote/edam/type/Notebook;->defaultNotebook:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_8

    return v0

    .line 792
    :cond_8
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetServiceCreated()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Notebook;->isSetServiceCreated()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_9

    return v0

    .line 796
    :cond_9
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetServiceCreated()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-wide v0, p0, Lcom/evernote/edam/type/Notebook;->serviceCreated:J

    iget-wide v2, p1, Lcom/evernote/edam/type/Notebook;->serviceCreated:J

    invoke-static {v0, v1, v2, v3}, Lcom/evernote/thrift/TBaseHelper;->compareTo(JJ)I

    move-result v0

    if-eqz v0, :cond_a

    return v0

    .line 801
    :cond_a
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetServiceUpdated()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Notebook;->isSetServiceUpdated()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_b

    return v0

    .line 805
    :cond_b
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetServiceUpdated()Z

    move-result v0

    if-eqz v0, :cond_c

    iget-wide v0, p0, Lcom/evernote/edam/type/Notebook;->serviceUpdated:J

    iget-wide v2, p1, Lcom/evernote/edam/type/Notebook;->serviceUpdated:J

    invoke-static {v0, v1, v2, v3}, Lcom/evernote/thrift/TBaseHelper;->compareTo(JJ)I

    move-result v0

    if-eqz v0, :cond_c

    return v0

    .line 810
    :cond_c
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetPublishing()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Notebook;->isSetPublishing()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_d

    return v0

    .line 814
    :cond_d
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetPublishing()Z

    move-result v0

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->publishing:Lcom/evernote/edam/type/Publishing;

    iget-object v1, p1, Lcom/evernote/edam/type/Notebook;->publishing:Lcom/evernote/edam/type/Publishing;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result v0

    if-eqz v0, :cond_e

    return v0

    .line 819
    :cond_e
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetPublished()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Notebook;->isSetPublished()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_f

    return v0

    .line 823
    :cond_f
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetPublished()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-boolean v0, p0, Lcom/evernote/edam/type/Notebook;->published:Z

    iget-boolean v1, p1, Lcom/evernote/edam/type/Notebook;->published:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_10

    return v0

    .line 828
    :cond_10
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetStack()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Notebook;->isSetStack()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_11

    return v0

    .line 832
    :cond_11
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetStack()Z

    move-result v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->stack:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/type/Notebook;->stack:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_12

    return v0

    .line 837
    :cond_12
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetSharedNotebookIds()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Notebook;->isSetSharedNotebookIds()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_13

    return v0

    .line 841
    :cond_13
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetSharedNotebookIds()Z

    move-result v0

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->sharedNotebookIds:Ljava/util/List;

    iget-object v1, p1, Lcom/evernote/edam/type/Notebook;->sharedNotebookIds:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/util/List;Ljava/util/List;)I

    move-result v0

    if-eqz v0, :cond_14

    return v0

    .line 846
    :cond_14
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetSharedNotebooks()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Notebook;->isSetSharedNotebooks()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_15

    return v0

    .line 850
    :cond_15
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetSharedNotebooks()Z

    move-result v0

    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->sharedNotebooks:Ljava/util/List;

    iget-object v1, p1, Lcom/evernote/edam/type/Notebook;->sharedNotebooks:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/util/List;Ljava/util/List;)I

    move-result v0

    if-eqz v0, :cond_16

    return v0

    .line 855
    :cond_16
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetBusinessNotebook()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Notebook;->isSetBusinessNotebook()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_17

    return v0

    .line 859
    :cond_17
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetBusinessNotebook()Z

    move-result v0

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->businessNotebook:Lcom/evernote/edam/type/BusinessNotebook;

    iget-object v1, p1, Lcom/evernote/edam/type/Notebook;->businessNotebook:Lcom/evernote/edam/type/BusinessNotebook;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result v0

    if-eqz v0, :cond_18

    return v0

    .line 864
    :cond_18
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetContact()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Notebook;->isSetContact()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_19

    return v0

    .line 868
    :cond_19
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetContact()Z

    move-result v0

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->contact:Lcom/evernote/edam/type/User;

    iget-object v1, p1, Lcom/evernote/edam/type/Notebook;->contact:Lcom/evernote/edam/type/User;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result v0

    if-eqz v0, :cond_1a

    return v0

    .line 873
    :cond_1a
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetRestrictions()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Notebook;->isSetRestrictions()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_1b

    return v0

    .line 877
    :cond_1b
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetRestrictions()Z

    move-result v0

    if-eqz v0, :cond_1c

    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->restrictions:Lcom/evernote/edam/type/NotebookRestrictions;

    iget-object p1, p1, Lcom/evernote/edam/type/Notebook;->restrictions:Lcom/evernote/edam/type/NotebookRestrictions;

    invoke-static {v0, p1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result p1

    if-eqz p1, :cond_1c

    return p1

    :cond_1c
    const/4 p1, 0x0

    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 136
    check-cast p1, Lcom/evernote/edam/type/Notebook;

    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/Notebook;->compareTo(Lcom/evernote/edam/type/Notebook;)I

    move-result p1

    return p1
.end method

.method public deepCopy()Lcom/evernote/edam/type/Notebook;
    .locals 1

    .line 229
    new-instance v0, Lcom/evernote/edam/type/Notebook;

    invoke-direct {v0, p0}, Lcom/evernote/edam/type/Notebook;-><init>(Lcom/evernote/edam/type/Notebook;)V

    return-object v0
.end method

.method public bridge synthetic deepCopy()Lcom/evernote/thrift/TBase;
    .locals 1

    .line 136
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->deepCopy()Lcom/evernote/edam/type/Notebook;

    move-result-object v0

    return-object v0
.end method

.method public equals(Lcom/evernote/edam/type/Notebook;)Z
    .locals 6

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 614
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetGuid()Z

    move-result v1

    .line 615
    invoke-virtual {p1}, Lcom/evernote/edam/type/Notebook;->isSetGuid()Z

    move-result v2

    if-nez v1, :cond_1

    if-eqz v2, :cond_3

    :cond_1
    if-eqz v1, :cond_38

    if-nez v2, :cond_2

    goto/16 :goto_d

    .line 619
    :cond_2
    iget-object v1, p0, Lcom/evernote/edam/type/Notebook;->guid:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/type/Notebook;->guid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    return v0

    .line 623
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetName()Z

    move-result v1

    .line 624
    invoke-virtual {p1}, Lcom/evernote/edam/type/Notebook;->isSetName()Z

    move-result v2

    if-nez v1, :cond_4

    if-eqz v2, :cond_6

    :cond_4
    if-eqz v1, :cond_37

    if-nez v2, :cond_5

    goto/16 :goto_c

    .line 628
    :cond_5
    iget-object v1, p0, Lcom/evernote/edam/type/Notebook;->name:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/type/Notebook;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    return v0

    .line 632
    :cond_6
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetUpdateSequenceNum()Z

    move-result v1

    .line 633
    invoke-virtual {p1}, Lcom/evernote/edam/type/Notebook;->isSetUpdateSequenceNum()Z

    move-result v2

    if-nez v1, :cond_7

    if-eqz v2, :cond_9

    :cond_7
    if-eqz v1, :cond_36

    if-nez v2, :cond_8

    goto/16 :goto_b

    .line 637
    :cond_8
    iget v1, p0, Lcom/evernote/edam/type/Notebook;->updateSequenceNum:I

    iget v2, p1, Lcom/evernote/edam/type/Notebook;->updateSequenceNum:I

    if-eq v1, v2, :cond_9

    return v0

    .line 641
    :cond_9
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetDefaultNotebook()Z

    move-result v1

    .line 642
    invoke-virtual {p1}, Lcom/evernote/edam/type/Notebook;->isSetDefaultNotebook()Z

    move-result v2

    if-nez v1, :cond_a

    if-eqz v2, :cond_c

    :cond_a
    if-eqz v1, :cond_35

    if-nez v2, :cond_b

    goto/16 :goto_a

    .line 646
    :cond_b
    iget-boolean v1, p0, Lcom/evernote/edam/type/Notebook;->defaultNotebook:Z

    iget-boolean v2, p1, Lcom/evernote/edam/type/Notebook;->defaultNotebook:Z

    if-eq v1, v2, :cond_c

    return v0

    .line 650
    :cond_c
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetServiceCreated()Z

    move-result v1

    .line 651
    invoke-virtual {p1}, Lcom/evernote/edam/type/Notebook;->isSetServiceCreated()Z

    move-result v2

    if-nez v1, :cond_d

    if-eqz v2, :cond_f

    :cond_d
    if-eqz v1, :cond_34

    if-nez v2, :cond_e

    goto/16 :goto_9

    .line 655
    :cond_e
    iget-wide v1, p0, Lcom/evernote/edam/type/Notebook;->serviceCreated:J

    iget-wide v3, p1, Lcom/evernote/edam/type/Notebook;->serviceCreated:J

    cmp-long v5, v1, v3

    if-eqz v5, :cond_f

    return v0

    .line 659
    :cond_f
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetServiceUpdated()Z

    move-result v1

    .line 660
    invoke-virtual {p1}, Lcom/evernote/edam/type/Notebook;->isSetServiceUpdated()Z

    move-result v2

    if-nez v1, :cond_10

    if-eqz v2, :cond_12

    :cond_10
    if-eqz v1, :cond_33

    if-nez v2, :cond_11

    goto/16 :goto_8

    .line 664
    :cond_11
    iget-wide v1, p0, Lcom/evernote/edam/type/Notebook;->serviceUpdated:J

    iget-wide v3, p1, Lcom/evernote/edam/type/Notebook;->serviceUpdated:J

    cmp-long v5, v1, v3

    if-eqz v5, :cond_12

    return v0

    .line 668
    :cond_12
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetPublishing()Z

    move-result v1

    .line 669
    invoke-virtual {p1}, Lcom/evernote/edam/type/Notebook;->isSetPublishing()Z

    move-result v2

    if-nez v1, :cond_13

    if-eqz v2, :cond_15

    :cond_13
    if-eqz v1, :cond_32

    if-nez v2, :cond_14

    goto/16 :goto_7

    .line 673
    :cond_14
    iget-object v1, p0, Lcom/evernote/edam/type/Notebook;->publishing:Lcom/evernote/edam/type/Publishing;

    iget-object v2, p1, Lcom/evernote/edam/type/Notebook;->publishing:Lcom/evernote/edam/type/Publishing;

    invoke-virtual {v1, v2}, Lcom/evernote/edam/type/Publishing;->equals(Lcom/evernote/edam/type/Publishing;)Z

    move-result v1

    if-nez v1, :cond_15

    return v0

    .line 677
    :cond_15
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetPublished()Z

    move-result v1

    .line 678
    invoke-virtual {p1}, Lcom/evernote/edam/type/Notebook;->isSetPublished()Z

    move-result v2

    if-nez v1, :cond_16

    if-eqz v2, :cond_18

    :cond_16
    if-eqz v1, :cond_31

    if-nez v2, :cond_17

    goto/16 :goto_6

    .line 682
    :cond_17
    iget-boolean v1, p0, Lcom/evernote/edam/type/Notebook;->published:Z

    iget-boolean v2, p1, Lcom/evernote/edam/type/Notebook;->published:Z

    if-eq v1, v2, :cond_18

    return v0

    .line 686
    :cond_18
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetStack()Z

    move-result v1

    .line 687
    invoke-virtual {p1}, Lcom/evernote/edam/type/Notebook;->isSetStack()Z

    move-result v2

    if-nez v1, :cond_19

    if-eqz v2, :cond_1b

    :cond_19
    if-eqz v1, :cond_30

    if-nez v2, :cond_1a

    goto/16 :goto_5

    .line 691
    :cond_1a
    iget-object v1, p0, Lcom/evernote/edam/type/Notebook;->stack:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/type/Notebook;->stack:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1b

    return v0

    .line 695
    :cond_1b
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetSharedNotebookIds()Z

    move-result v1

    .line 696
    invoke-virtual {p1}, Lcom/evernote/edam/type/Notebook;->isSetSharedNotebookIds()Z

    move-result v2

    if-nez v1, :cond_1c

    if-eqz v2, :cond_1e

    :cond_1c
    if-eqz v1, :cond_2f

    if-nez v2, :cond_1d

    goto/16 :goto_4

    .line 700
    :cond_1d
    iget-object v1, p0, Lcom/evernote/edam/type/Notebook;->sharedNotebookIds:Ljava/util/List;

    iget-object v2, p1, Lcom/evernote/edam/type/Notebook;->sharedNotebookIds:Ljava/util/List;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1e

    return v0

    .line 704
    :cond_1e
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetSharedNotebooks()Z

    move-result v1

    .line 705
    invoke-virtual {p1}, Lcom/evernote/edam/type/Notebook;->isSetSharedNotebooks()Z

    move-result v2

    if-nez v1, :cond_1f

    if-eqz v2, :cond_21

    :cond_1f
    if-eqz v1, :cond_2e

    if-nez v2, :cond_20

    goto :goto_3

    .line 709
    :cond_20
    iget-object v1, p0, Lcom/evernote/edam/type/Notebook;->sharedNotebooks:Ljava/util/List;

    iget-object v2, p1, Lcom/evernote/edam/type/Notebook;->sharedNotebooks:Ljava/util/List;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_21

    return v0

    .line 713
    :cond_21
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetBusinessNotebook()Z

    move-result v1

    .line 714
    invoke-virtual {p1}, Lcom/evernote/edam/type/Notebook;->isSetBusinessNotebook()Z

    move-result v2

    if-nez v1, :cond_22

    if-eqz v2, :cond_24

    :cond_22
    if-eqz v1, :cond_2d

    if-nez v2, :cond_23

    goto :goto_2

    .line 718
    :cond_23
    iget-object v1, p0, Lcom/evernote/edam/type/Notebook;->businessNotebook:Lcom/evernote/edam/type/BusinessNotebook;

    iget-object v2, p1, Lcom/evernote/edam/type/Notebook;->businessNotebook:Lcom/evernote/edam/type/BusinessNotebook;

    invoke-virtual {v1, v2}, Lcom/evernote/edam/type/BusinessNotebook;->equals(Lcom/evernote/edam/type/BusinessNotebook;)Z

    move-result v1

    if-nez v1, :cond_24

    return v0

    .line 722
    :cond_24
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetContact()Z

    move-result v1

    .line 723
    invoke-virtual {p1}, Lcom/evernote/edam/type/Notebook;->isSetContact()Z

    move-result v2

    if-nez v1, :cond_25

    if-eqz v2, :cond_27

    :cond_25
    if-eqz v1, :cond_2c

    if-nez v2, :cond_26

    goto :goto_1

    .line 727
    :cond_26
    iget-object v1, p0, Lcom/evernote/edam/type/Notebook;->contact:Lcom/evernote/edam/type/User;

    iget-object v2, p1, Lcom/evernote/edam/type/Notebook;->contact:Lcom/evernote/edam/type/User;

    invoke-virtual {v1, v2}, Lcom/evernote/edam/type/User;->equals(Lcom/evernote/edam/type/User;)Z

    move-result v1

    if-nez v1, :cond_27

    return v0

    .line 731
    :cond_27
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetRestrictions()Z

    move-result v1

    .line 732
    invoke-virtual {p1}, Lcom/evernote/edam/type/Notebook;->isSetRestrictions()Z

    move-result v2

    if-nez v1, :cond_28

    if-eqz v2, :cond_2a

    :cond_28
    if-eqz v1, :cond_2b

    if-nez v2, :cond_29

    goto :goto_0

    .line 736
    :cond_29
    iget-object v1, p0, Lcom/evernote/edam/type/Notebook;->restrictions:Lcom/evernote/edam/type/NotebookRestrictions;

    iget-object p1, p1, Lcom/evernote/edam/type/Notebook;->restrictions:Lcom/evernote/edam/type/NotebookRestrictions;

    invoke-virtual {v1, p1}, Lcom/evernote/edam/type/NotebookRestrictions;->equals(Lcom/evernote/edam/type/NotebookRestrictions;)Z

    move-result p1

    if-nez p1, :cond_2a

    return v0

    :cond_2a
    const/4 p1, 0x1

    return p1

    :cond_2b
    :goto_0
    return v0

    :cond_2c
    :goto_1
    return v0

    :cond_2d
    :goto_2
    return v0

    :cond_2e
    :goto_3
    return v0

    :cond_2f
    :goto_4
    return v0

    :cond_30
    :goto_5
    return v0

    :cond_31
    :goto_6
    return v0

    :cond_32
    :goto_7
    return v0

    :cond_33
    :goto_8
    return v0

    :cond_34
    :goto_9
    return v0

    :cond_35
    :goto_a
    return v0

    :cond_36
    :goto_b
    return v0

    :cond_37
    :goto_c
    return v0

    :cond_38
    :goto_d
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 605
    :cond_0
    instance-of v1, p1, Lcom/evernote/edam/type/Notebook;

    if-eqz v1, :cond_1

    .line 606
    check-cast p1, Lcom/evernote/edam/type/Notebook;

    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/Notebook;->equals(Lcom/evernote/edam/type/Notebook;)Z

    move-result p1

    return p1

    :cond_1
    return v0
.end method

.method public getBusinessNotebook()Lcom/evernote/edam/type/BusinessNotebook;
    .locals 1

    .line 533
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->businessNotebook:Lcom/evernote/edam/type/BusinessNotebook;

    return-object v0
.end method

.method public getContact()Lcom/evernote/edam/type/User;
    .locals 1

    .line 556
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->contact:Lcom/evernote/edam/type/User;

    return-object v0
.end method

.method public getGuid()Ljava/lang/String;
    .locals 1

    .line 255
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->guid:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 278
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getPublishing()Lcom/evernote/edam/type/Publishing;
    .locals 1

    .line 389
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->publishing:Lcom/evernote/edam/type/Publishing;

    return-object v0
.end method

.method public getRestrictions()Lcom/evernote/edam/type/NotebookRestrictions;
    .locals 1

    .line 579
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->restrictions:Lcom/evernote/edam/type/NotebookRestrictions;

    return-object v0
.end method

.method public getServiceCreated()J
    .locals 2

    .line 345
    iget-wide v0, p0, Lcom/evernote/edam/type/Notebook;->serviceCreated:J

    return-wide v0
.end method

.method public getServiceUpdated()J
    .locals 2

    .line 367
    iget-wide v0, p0, Lcom/evernote/edam/type/Notebook;->serviceUpdated:J

    return-wide v0
.end method

.method public getSharedNotebookIds()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 472
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->sharedNotebookIds:Ljava/util/List;

    return-object v0
.end method

.method public getSharedNotebookIdsIterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 461
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->sharedNotebookIds:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public getSharedNotebookIdsSize()I
    .locals 1

    .line 457
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->sharedNotebookIds:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    return v0
.end method

.method public getSharedNotebooks()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/evernote/edam/type/SharedNotebook;",
            ">;"
        }
    .end annotation

    .line 510
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->sharedNotebooks:Ljava/util/List;

    return-object v0
.end method

.method public getSharedNotebooksIterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Lcom/evernote/edam/type/SharedNotebook;",
            ">;"
        }
    .end annotation

    .line 499
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->sharedNotebooks:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public getSharedNotebooksSize()I
    .locals 1

    .line 495
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->sharedNotebooks:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    return v0
.end method

.method public getStack()Ljava/lang/String;
    .locals 1

    .line 434
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->stack:Ljava/lang/String;

    return-object v0
.end method

.method public getUpdateSequenceNum()I
    .locals 1

    .line 301
    iget v0, p0, Lcom/evernote/edam/type/Notebook;->updateSequenceNum:I

    return v0
.end method

.method public hashCode()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isDefaultNotebook()Z
    .locals 1

    .line 323
    iget-boolean v0, p0, Lcom/evernote/edam/type/Notebook;->defaultNotebook:Z

    return v0
.end method

.method public isPublished()Z
    .locals 1

    .line 412
    iget-boolean v0, p0, Lcom/evernote/edam/type/Notebook;->published:Z

    return v0
.end method

.method public isSetBusinessNotebook()Z
    .locals 1

    .line 546
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->businessNotebook:Lcom/evernote/edam/type/BusinessNotebook;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetContact()Z
    .locals 1

    .line 569
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->contact:Lcom/evernote/edam/type/User;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetDefaultNotebook()Z
    .locals 2

    .line 337
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->__isset_vector:[Z

    const/4 v1, 0x1

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetGuid()Z
    .locals 1

    .line 268
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->guid:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetName()Z
    .locals 1

    .line 291
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->name:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetPublished()Z
    .locals 2

    .line 426
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->__isset_vector:[Z

    const/4 v1, 0x4

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetPublishing()Z
    .locals 1

    .line 402
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->publishing:Lcom/evernote/edam/type/Publishing;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetRestrictions()Z
    .locals 1

    .line 592
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->restrictions:Lcom/evernote/edam/type/NotebookRestrictions;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetServiceCreated()Z
    .locals 2

    .line 359
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->__isset_vector:[Z

    const/4 v1, 0x2

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetServiceUpdated()Z
    .locals 2

    .line 381
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->__isset_vector:[Z

    const/4 v1, 0x3

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetSharedNotebookIds()Z
    .locals 1

    .line 485
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->sharedNotebookIds:Ljava/util/List;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetSharedNotebooks()Z
    .locals 1

    .line 523
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->sharedNotebooks:Ljava/util/List;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetStack()Z
    .locals 1

    .line 447
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->stack:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetUpdateSequenceNum()Z
    .locals 2

    .line 315
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->__isset_vector:[Z

    const/4 v1, 0x0

    aget-boolean v0, v0, v1

    return v0
.end method

.method public read(Lcom/evernote/thrift/protocol/TProtocol;)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 887
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readStructBegin()Lcom/evernote/thrift/protocol/TStruct;

    .line 890
    :goto_0
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readFieldBegin()Lcom/evernote/thrift/protocol/TField;

    move-result-object v0

    .line 891
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-nez v1, :cond_0

    .line 1028
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readStructEnd()V

    .line 1029
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->validate()V

    return-void

    .line 894
    :cond_0
    iget-short v1, v0, Lcom/evernote/thrift/protocol/TField;->id:S

    const/4 v2, 0x0

    const/16 v3, 0xf

    const/16 v4, 0xa

    const/4 v5, 0x2

    const/16 v6, 0xb

    const/16 v7, 0xc

    const/4 v8, 0x1

    packed-switch v1, :pswitch_data_0

    .line 1024
    :pswitch_0
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_3

    .line 1016
    :pswitch_1
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v7, :cond_1

    .line 1017
    new-instance v0, Lcom/evernote/edam/type/NotebookRestrictions;

    invoke-direct {v0}, Lcom/evernote/edam/type/NotebookRestrictions;-><init>()V

    iput-object v0, p0, Lcom/evernote/edam/type/Notebook;->restrictions:Lcom/evernote/edam/type/NotebookRestrictions;

    .line 1018
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->restrictions:Lcom/evernote/edam/type/NotebookRestrictions;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/type/NotebookRestrictions;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    goto/16 :goto_3

    .line 1020
    :cond_1
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_3

    .line 1008
    :pswitch_2
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v7, :cond_2

    .line 1009
    new-instance v0, Lcom/evernote/edam/type/User;

    invoke-direct {v0}, Lcom/evernote/edam/type/User;-><init>()V

    iput-object v0, p0, Lcom/evernote/edam/type/Notebook;->contact:Lcom/evernote/edam/type/User;

    .line 1010
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->contact:Lcom/evernote/edam/type/User;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/type/User;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    goto/16 :goto_3

    .line 1012
    :cond_2
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_3

    .line 1000
    :pswitch_3
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v7, :cond_3

    .line 1001
    new-instance v0, Lcom/evernote/edam/type/BusinessNotebook;

    invoke-direct {v0}, Lcom/evernote/edam/type/BusinessNotebook;-><init>()V

    iput-object v0, p0, Lcom/evernote/edam/type/Notebook;->businessNotebook:Lcom/evernote/edam/type/BusinessNotebook;

    .line 1002
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->businessNotebook:Lcom/evernote/edam/type/BusinessNotebook;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/type/BusinessNotebook;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    goto/16 :goto_3

    .line 1004
    :cond_3
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_3

    .line 982
    :pswitch_4
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v3, :cond_5

    .line 984
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readListBegin()Lcom/evernote/thrift/protocol/TList;

    move-result-object v0

    .line 985
    new-instance v1, Ljava/util/ArrayList;

    iget v3, v0, Lcom/evernote/thrift/protocol/TList;->size:I

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/evernote/edam/type/Notebook;->sharedNotebooks:Ljava/util/List;

    .line 986
    :goto_1
    iget v1, v0, Lcom/evernote/thrift/protocol/TList;->size:I

    if-ge v2, v1, :cond_4

    .line 989
    new-instance v1, Lcom/evernote/edam/type/SharedNotebook;

    invoke-direct {v1}, Lcom/evernote/edam/type/SharedNotebook;-><init>()V

    .line 990
    invoke-virtual {v1, p1}, Lcom/evernote/edam/type/SharedNotebook;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 991
    iget-object v3, p0, Lcom/evernote/edam/type/Notebook;->sharedNotebooks:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 993
    :cond_4
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readListEnd()V

    goto/16 :goto_3

    .line 996
    :cond_5
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_3

    .line 965
    :pswitch_5
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v3, :cond_7

    .line 967
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readListBegin()Lcom/evernote/thrift/protocol/TList;

    move-result-object v0

    .line 968
    new-instance v1, Ljava/util/ArrayList;

    iget v3, v0, Lcom/evernote/thrift/protocol/TList;->size:I

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/evernote/edam/type/Notebook;->sharedNotebookIds:Ljava/util/List;

    .line 969
    :goto_2
    iget v1, v0, Lcom/evernote/thrift/protocol/TList;->size:I

    if-ge v2, v1, :cond_6

    .line 972
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI64()J

    move-result-wide v3

    .line 973
    iget-object v1, p0, Lcom/evernote/edam/type/Notebook;->sharedNotebookIds:Ljava/util/List;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 975
    :cond_6
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readListEnd()V

    goto/16 :goto_3

    .line 978
    :cond_7
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_3

    .line 958
    :pswitch_6
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v6, :cond_8

    .line 959
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/type/Notebook;->stack:Ljava/lang/String;

    goto/16 :goto_3

    .line 961
    :cond_8
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_3

    .line 950
    :pswitch_7
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v5, :cond_9

    .line 951
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/type/Notebook;->published:Z

    .line 952
    invoke-virtual {p0, v8}, Lcom/evernote/edam/type/Notebook;->setPublishedIsSet(Z)V

    goto/16 :goto_3

    .line 954
    :cond_9
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_3

    .line 942
    :pswitch_8
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v7, :cond_a

    .line 943
    new-instance v0, Lcom/evernote/edam/type/Publishing;

    invoke-direct {v0}, Lcom/evernote/edam/type/Publishing;-><init>()V

    iput-object v0, p0, Lcom/evernote/edam/type/Notebook;->publishing:Lcom/evernote/edam/type/Publishing;

    .line 944
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->publishing:Lcom/evernote/edam/type/Publishing;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/type/Publishing;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    goto/16 :goto_3

    .line 946
    :cond_a
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_3

    .line 934
    :pswitch_9
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_b

    .line 935
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/evernote/edam/type/Notebook;->serviceUpdated:J

    .line 936
    invoke-virtual {p0, v8}, Lcom/evernote/edam/type/Notebook;->setServiceUpdatedIsSet(Z)V

    goto/16 :goto_3

    .line 938
    :cond_b
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_3

    .line 926
    :pswitch_a
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_c

    .line 927
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/evernote/edam/type/Notebook;->serviceCreated:J

    .line 928
    invoke-virtual {p0, v8}, Lcom/evernote/edam/type/Notebook;->setServiceCreatedIsSet(Z)V

    goto :goto_3

    .line 930
    :cond_c
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_3

    .line 918
    :pswitch_b
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v5, :cond_d

    .line 919
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/type/Notebook;->defaultNotebook:Z

    .line 920
    invoke-virtual {p0, v8}, Lcom/evernote/edam/type/Notebook;->setDefaultNotebookIsSet(Z)V

    goto :goto_3

    .line 922
    :cond_d
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_3

    .line 910
    :pswitch_c
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    const/16 v2, 0x8

    if-ne v1, v2, :cond_e

    .line 911
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI32()I

    move-result v0

    iput v0, p0, Lcom/evernote/edam/type/Notebook;->updateSequenceNum:I

    .line 912
    invoke-virtual {p0, v8}, Lcom/evernote/edam/type/Notebook;->setUpdateSequenceNumIsSet(Z)V

    goto :goto_3

    .line 914
    :cond_e
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_3

    .line 903
    :pswitch_d
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v6, :cond_f

    .line 904
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/type/Notebook;->name:Ljava/lang/String;

    goto :goto_3

    .line 906
    :cond_f
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_3

    .line 896
    :pswitch_e
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v6, :cond_10

    .line 897
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/type/Notebook;->guid:Ljava/lang/String;

    goto :goto_3

    .line 899
    :cond_10
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    .line 1026
    :goto_3
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readFieldEnd()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_e
        :pswitch_d
        :pswitch_0
        :pswitch_0
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_0
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public setBusinessNotebook(Lcom/evernote/edam/type/BusinessNotebook;)V
    .locals 0

    .line 537
    iput-object p1, p0, Lcom/evernote/edam/type/Notebook;->businessNotebook:Lcom/evernote/edam/type/BusinessNotebook;

    return-void
.end method

.method public setBusinessNotebookIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 551
    iput-object p1, p0, Lcom/evernote/edam/type/Notebook;->businessNotebook:Lcom/evernote/edam/type/BusinessNotebook;

    :cond_0
    return-void
.end method

.method public setContact(Lcom/evernote/edam/type/User;)V
    .locals 0

    .line 560
    iput-object p1, p0, Lcom/evernote/edam/type/Notebook;->contact:Lcom/evernote/edam/type/User;

    return-void
.end method

.method public setContactIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 574
    iput-object p1, p0, Lcom/evernote/edam/type/Notebook;->contact:Lcom/evernote/edam/type/User;

    :cond_0
    return-void
.end method

.method public setDefaultNotebook(Z)V
    .locals 0

    .line 327
    iput-boolean p1, p0, Lcom/evernote/edam/type/Notebook;->defaultNotebook:Z

    const/4 p1, 0x1

    .line 328
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/Notebook;->setDefaultNotebookIsSet(Z)V

    return-void
.end method

.method public setDefaultNotebookIsSet(Z)V
    .locals 2

    .line 341
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->__isset_vector:[Z

    const/4 v1, 0x1

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setGuid(Ljava/lang/String;)V
    .locals 0

    .line 259
    iput-object p1, p0, Lcom/evernote/edam/type/Notebook;->guid:Ljava/lang/String;

    return-void
.end method

.method public setGuidIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 273
    iput-object p1, p0, Lcom/evernote/edam/type/Notebook;->guid:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0

    .line 282
    iput-object p1, p0, Lcom/evernote/edam/type/Notebook;->name:Ljava/lang/String;

    return-void
.end method

.method public setNameIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 296
    iput-object p1, p0, Lcom/evernote/edam/type/Notebook;->name:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setPublished(Z)V
    .locals 0

    .line 416
    iput-boolean p1, p0, Lcom/evernote/edam/type/Notebook;->published:Z

    const/4 p1, 0x1

    .line 417
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/Notebook;->setPublishedIsSet(Z)V

    return-void
.end method

.method public setPublishedIsSet(Z)V
    .locals 2

    .line 430
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->__isset_vector:[Z

    const/4 v1, 0x4

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setPublishing(Lcom/evernote/edam/type/Publishing;)V
    .locals 0

    .line 393
    iput-object p1, p0, Lcom/evernote/edam/type/Notebook;->publishing:Lcom/evernote/edam/type/Publishing;

    return-void
.end method

.method public setPublishingIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 407
    iput-object p1, p0, Lcom/evernote/edam/type/Notebook;->publishing:Lcom/evernote/edam/type/Publishing;

    :cond_0
    return-void
.end method

.method public setRestrictions(Lcom/evernote/edam/type/NotebookRestrictions;)V
    .locals 0

    .line 583
    iput-object p1, p0, Lcom/evernote/edam/type/Notebook;->restrictions:Lcom/evernote/edam/type/NotebookRestrictions;

    return-void
.end method

.method public setRestrictionsIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 597
    iput-object p1, p0, Lcom/evernote/edam/type/Notebook;->restrictions:Lcom/evernote/edam/type/NotebookRestrictions;

    :cond_0
    return-void
.end method

.method public setServiceCreated(J)V
    .locals 0

    .line 349
    iput-wide p1, p0, Lcom/evernote/edam/type/Notebook;->serviceCreated:J

    const/4 p1, 0x1

    .line 350
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/Notebook;->setServiceCreatedIsSet(Z)V

    return-void
.end method

.method public setServiceCreatedIsSet(Z)V
    .locals 2

    .line 363
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->__isset_vector:[Z

    const/4 v1, 0x2

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setServiceUpdated(J)V
    .locals 0

    .line 371
    iput-wide p1, p0, Lcom/evernote/edam/type/Notebook;->serviceUpdated:J

    const/4 p1, 0x1

    .line 372
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/Notebook;->setServiceUpdatedIsSet(Z)V

    return-void
.end method

.method public setServiceUpdatedIsSet(Z)V
    .locals 2

    .line 385
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->__isset_vector:[Z

    const/4 v1, 0x3

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setSharedNotebookIds(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .line 476
    iput-object p1, p0, Lcom/evernote/edam/type/Notebook;->sharedNotebookIds:Ljava/util/List;

    return-void
.end method

.method public setSharedNotebookIdsIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 490
    iput-object p1, p0, Lcom/evernote/edam/type/Notebook;->sharedNotebookIds:Ljava/util/List;

    :cond_0
    return-void
.end method

.method public setSharedNotebooks(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/evernote/edam/type/SharedNotebook;",
            ">;)V"
        }
    .end annotation

    .line 514
    iput-object p1, p0, Lcom/evernote/edam/type/Notebook;->sharedNotebooks:Ljava/util/List;

    return-void
.end method

.method public setSharedNotebooksIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 528
    iput-object p1, p0, Lcom/evernote/edam/type/Notebook;->sharedNotebooks:Ljava/util/List;

    :cond_0
    return-void
.end method

.method public setStack(Ljava/lang/String;)V
    .locals 0

    .line 438
    iput-object p1, p0, Lcom/evernote/edam/type/Notebook;->stack:Ljava/lang/String;

    return-void
.end method

.method public setStackIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 452
    iput-object p1, p0, Lcom/evernote/edam/type/Notebook;->stack:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setUpdateSequenceNum(I)V
    .locals 0

    .line 305
    iput p1, p0, Lcom/evernote/edam/type/Notebook;->updateSequenceNum:I

    const/4 p1, 0x1

    .line 306
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/Notebook;->setUpdateSequenceNumIsSet(Z)V

    return-void
.end method

.method public setUpdateSequenceNumIsSet(Z)V
    .locals 2

    .line 319
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->__isset_vector:[Z

    const/4 v1, 0x0

    aput-boolean p1, v0, v1

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .line 1144
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Notebook("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1147
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetGuid()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    const-string v1, "guid:"

    .line 1148
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1149
    iget-object v1, p0, Lcom/evernote/edam/type/Notebook;->guid:Ljava/lang/String;

    if-nez v1, :cond_0

    const-string v1, "null"

    .line 1150
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1152
    :cond_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    const/4 v1, 0x1

    .line 1156
    :goto_1
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetName()Z

    move-result v3

    if-eqz v3, :cond_4

    if-nez v1, :cond_2

    const-string v1, ", "

    .line 1157
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const-string v1, "name:"

    .line 1158
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1159
    iget-object v1, p0, Lcom/evernote/edam/type/Notebook;->name:Ljava/lang/String;

    if-nez v1, :cond_3

    const-string v1, "null"

    .line 1160
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 1162
    :cond_3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_2
    const/4 v1, 0x0

    .line 1166
    :cond_4
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetUpdateSequenceNum()Z

    move-result v3

    if-eqz v3, :cond_6

    if-nez v1, :cond_5

    const-string v1, ", "

    .line 1167
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    const-string v1, "updateSequenceNum:"

    .line 1168
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1169
    iget v1, p0, Lcom/evernote/edam/type/Notebook;->updateSequenceNum:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1172
    :cond_6
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetDefaultNotebook()Z

    move-result v3

    if-eqz v3, :cond_8

    if-nez v1, :cond_7

    const-string v1, ", "

    .line 1173
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_7
    const-string v1, "defaultNotebook:"

    .line 1174
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1175
    iget-boolean v1, p0, Lcom/evernote/edam/type/Notebook;->defaultNotebook:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1178
    :cond_8
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetServiceCreated()Z

    move-result v3

    if-eqz v3, :cond_a

    if-nez v1, :cond_9

    const-string v1, ", "

    .line 1179
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_9
    const-string v1, "serviceCreated:"

    .line 1180
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1181
    iget-wide v3, p0, Lcom/evernote/edam/type/Notebook;->serviceCreated:J

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1184
    :cond_a
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetServiceUpdated()Z

    move-result v3

    if-eqz v3, :cond_c

    if-nez v1, :cond_b

    const-string v1, ", "

    .line 1185
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_b
    const-string v1, "serviceUpdated:"

    .line 1186
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1187
    iget-wide v3, p0, Lcom/evernote/edam/type/Notebook;->serviceUpdated:J

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1190
    :cond_c
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetPublishing()Z

    move-result v3

    if-eqz v3, :cond_f

    if-nez v1, :cond_d

    const-string v1, ", "

    .line 1191
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_d
    const-string v1, "publishing:"

    .line 1192
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1193
    iget-object v1, p0, Lcom/evernote/edam/type/Notebook;->publishing:Lcom/evernote/edam/type/Publishing;

    if-nez v1, :cond_e

    const-string v1, "null"

    .line 1194
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 1196
    :cond_e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :goto_3
    const/4 v1, 0x0

    .line 1200
    :cond_f
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetPublished()Z

    move-result v3

    if-eqz v3, :cond_11

    if-nez v1, :cond_10

    const-string v1, ", "

    .line 1201
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_10
    const-string v1, "published:"

    .line 1202
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1203
    iget-boolean v1, p0, Lcom/evernote/edam/type/Notebook;->published:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1206
    :cond_11
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetStack()Z

    move-result v3

    if-eqz v3, :cond_14

    if-nez v1, :cond_12

    const-string v1, ", "

    .line 1207
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_12
    const-string v1, "stack:"

    .line 1208
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1209
    iget-object v1, p0, Lcom/evernote/edam/type/Notebook;->stack:Ljava/lang/String;

    if-nez v1, :cond_13

    const-string v1, "null"

    .line 1210
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 1212
    :cond_13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_4
    const/4 v1, 0x0

    .line 1216
    :cond_14
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetSharedNotebookIds()Z

    move-result v3

    if-eqz v3, :cond_17

    if-nez v1, :cond_15

    const-string v1, ", "

    .line 1217
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_15
    const-string v1, "sharedNotebookIds:"

    .line 1218
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1219
    iget-object v1, p0, Lcom/evernote/edam/type/Notebook;->sharedNotebookIds:Ljava/util/List;

    if-nez v1, :cond_16

    const-string v1, "null"

    .line 1220
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5

    .line 1222
    :cond_16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :goto_5
    const/4 v1, 0x0

    .line 1226
    :cond_17
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetSharedNotebooks()Z

    move-result v3

    if-eqz v3, :cond_1a

    if-nez v1, :cond_18

    const-string v1, ", "

    .line 1227
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_18
    const-string v1, "sharedNotebooks:"

    .line 1228
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1229
    iget-object v1, p0, Lcom/evernote/edam/type/Notebook;->sharedNotebooks:Ljava/util/List;

    if-nez v1, :cond_19

    const-string v1, "null"

    .line 1230
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6

    .line 1232
    :cond_19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :goto_6
    const/4 v1, 0x0

    .line 1236
    :cond_1a
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetBusinessNotebook()Z

    move-result v3

    if-eqz v3, :cond_1d

    if-nez v1, :cond_1b

    const-string v1, ", "

    .line 1237
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1b
    const-string v1, "businessNotebook:"

    .line 1238
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1239
    iget-object v1, p0, Lcom/evernote/edam/type/Notebook;->businessNotebook:Lcom/evernote/edam/type/BusinessNotebook;

    if-nez v1, :cond_1c

    const-string v1, "null"

    .line 1240
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_7

    .line 1242
    :cond_1c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :goto_7
    const/4 v1, 0x0

    .line 1246
    :cond_1d
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetContact()Z

    move-result v3

    if-eqz v3, :cond_20

    if-nez v1, :cond_1e

    const-string v1, ", "

    .line 1247
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1e
    const-string v1, "contact:"

    .line 1248
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1249
    iget-object v1, p0, Lcom/evernote/edam/type/Notebook;->contact:Lcom/evernote/edam/type/User;

    if-nez v1, :cond_1f

    const-string v1, "null"

    .line 1250
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_8

    .line 1252
    :cond_1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :goto_8
    const/4 v1, 0x0

    .line 1256
    :cond_20
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetRestrictions()Z

    move-result v2

    if-eqz v2, :cond_23

    if-nez v1, :cond_21

    const-string v1, ", "

    .line 1257
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_21
    const-string v1, "restrictions:"

    .line 1258
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1259
    iget-object v1, p0, Lcom/evernote/edam/type/Notebook;->restrictions:Lcom/evernote/edam/type/NotebookRestrictions;

    if-nez v1, :cond_22

    const-string v1, "null"

    .line 1260
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_9

    .line 1262
    :cond_22
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_23
    :goto_9
    const-string v1, ")"

    .line 1266
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1267
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public unsetBusinessNotebook()V
    .locals 1

    const/4 v0, 0x0

    .line 541
    iput-object v0, p0, Lcom/evernote/edam/type/Notebook;->businessNotebook:Lcom/evernote/edam/type/BusinessNotebook;

    return-void
.end method

.method public unsetContact()V
    .locals 1

    const/4 v0, 0x0

    .line 564
    iput-object v0, p0, Lcom/evernote/edam/type/Notebook;->contact:Lcom/evernote/edam/type/User;

    return-void
.end method

.method public unsetDefaultNotebook()V
    .locals 3

    .line 332
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->__isset_vector:[Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetGuid()V
    .locals 1

    const/4 v0, 0x0

    .line 263
    iput-object v0, p0, Lcom/evernote/edam/type/Notebook;->guid:Ljava/lang/String;

    return-void
.end method

.method public unsetName()V
    .locals 1

    const/4 v0, 0x0

    .line 286
    iput-object v0, p0, Lcom/evernote/edam/type/Notebook;->name:Ljava/lang/String;

    return-void
.end method

.method public unsetPublished()V
    .locals 3

    .line 421
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->__isset_vector:[Z

    const/4 v1, 0x4

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetPublishing()V
    .locals 1

    const/4 v0, 0x0

    .line 397
    iput-object v0, p0, Lcom/evernote/edam/type/Notebook;->publishing:Lcom/evernote/edam/type/Publishing;

    return-void
.end method

.method public unsetRestrictions()V
    .locals 1

    const/4 v0, 0x0

    .line 587
    iput-object v0, p0, Lcom/evernote/edam/type/Notebook;->restrictions:Lcom/evernote/edam/type/NotebookRestrictions;

    return-void
.end method

.method public unsetServiceCreated()V
    .locals 3

    .line 354
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->__isset_vector:[Z

    const/4 v1, 0x2

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetServiceUpdated()V
    .locals 3

    .line 376
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->__isset_vector:[Z

    const/4 v1, 0x3

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetSharedNotebookIds()V
    .locals 1

    const/4 v0, 0x0

    .line 480
    iput-object v0, p0, Lcom/evernote/edam/type/Notebook;->sharedNotebookIds:Ljava/util/List;

    return-void
.end method

.method public unsetSharedNotebooks()V
    .locals 1

    const/4 v0, 0x0

    .line 518
    iput-object v0, p0, Lcom/evernote/edam/type/Notebook;->sharedNotebooks:Ljava/util/List;

    return-void
.end method

.method public unsetStack()V
    .locals 1

    const/4 v0, 0x0

    .line 442
    iput-object v0, p0, Lcom/evernote/edam/type/Notebook;->stack:Ljava/lang/String;

    return-void
.end method

.method public unsetUpdateSequenceNum()V
    .locals 2

    .line 310
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->__isset_vector:[Z

    const/4 v1, 0x0

    aput-boolean v1, v0, v1

    return-void
.end method

.method public validate()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    return-void
.end method

.method public write(Lcom/evernote/thrift/protocol/TProtocol;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1033
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->validate()V

    .line 1035
    sget-object v0, Lcom/evernote/edam/type/Notebook;->STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeStructBegin(Lcom/evernote/thrift/protocol/TStruct;)V

    .line 1036
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->guid:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1037
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetGuid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1038
    sget-object v0, Lcom/evernote/edam/type/Notebook;->GUID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1039
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->guid:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 1040
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1043
    :cond_0
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->name:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1044
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetName()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1045
    sget-object v0, Lcom/evernote/edam/type/Notebook;->NAME_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1046
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 1047
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1050
    :cond_1
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetUpdateSequenceNum()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1051
    sget-object v0, Lcom/evernote/edam/type/Notebook;->UPDATE_SEQUENCE_NUM_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1052
    iget v0, p0, Lcom/evernote/edam/type/Notebook;->updateSequenceNum:I

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeI32(I)V

    .line 1053
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1055
    :cond_2
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetDefaultNotebook()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1056
    sget-object v0, Lcom/evernote/edam/type/Notebook;->DEFAULT_NOTEBOOK_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1057
    iget-boolean v0, p0, Lcom/evernote/edam/type/Notebook;->defaultNotebook:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 1058
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1060
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetServiceCreated()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1061
    sget-object v0, Lcom/evernote/edam/type/Notebook;->SERVICE_CREATED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1062
    iget-wide v0, p0, Lcom/evernote/edam/type/Notebook;->serviceCreated:J

    invoke-virtual {p1, v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeI64(J)V

    .line 1063
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1065
    :cond_4
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetServiceUpdated()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1066
    sget-object v0, Lcom/evernote/edam/type/Notebook;->SERVICE_UPDATED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1067
    iget-wide v0, p0, Lcom/evernote/edam/type/Notebook;->serviceUpdated:J

    invoke-virtual {p1, v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeI64(J)V

    .line 1068
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1070
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->publishing:Lcom/evernote/edam/type/Publishing;

    if-eqz v0, :cond_6

    .line 1071
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetPublishing()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1072
    sget-object v0, Lcom/evernote/edam/type/Notebook;->PUBLISHING_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1073
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->publishing:Lcom/evernote/edam/type/Publishing;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/type/Publishing;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1074
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1077
    :cond_6
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetPublished()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1078
    sget-object v0, Lcom/evernote/edam/type/Notebook;->PUBLISHED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1079
    iget-boolean v0, p0, Lcom/evernote/edam/type/Notebook;->published:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 1080
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1082
    :cond_7
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->stack:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 1083
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetStack()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1084
    sget-object v0, Lcom/evernote/edam/type/Notebook;->STACK_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1085
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->stack:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 1086
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1089
    :cond_8
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->sharedNotebookIds:Ljava/util/List;

    if-eqz v0, :cond_a

    .line 1090
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetSharedNotebookIds()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1091
    sget-object v0, Lcom/evernote/edam/type/Notebook;->SHARED_NOTEBOOK_IDS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1093
    new-instance v0, Lcom/evernote/thrift/protocol/TList;

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/evernote/edam/type/Notebook;->sharedNotebookIds:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/protocol/TList;-><init>(BI)V

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeListBegin(Lcom/evernote/thrift/protocol/TList;)V

    .line 1094
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->sharedNotebookIds:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    .line 1096
    invoke-virtual {p1, v1, v2}, Lcom/evernote/thrift/protocol/TProtocol;->writeI64(J)V

    goto :goto_0

    .line 1098
    :cond_9
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeListEnd()V

    .line 1100
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1103
    :cond_a
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->sharedNotebooks:Ljava/util/List;

    if-eqz v0, :cond_c

    .line 1104
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetSharedNotebooks()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1105
    sget-object v0, Lcom/evernote/edam/type/Notebook;->SHARED_NOTEBOOKS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1107
    new-instance v0, Lcom/evernote/thrift/protocol/TList;

    const/16 v1, 0xc

    iget-object v2, p0, Lcom/evernote/edam/type/Notebook;->sharedNotebooks:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/protocol/TList;-><init>(BI)V

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeListBegin(Lcom/evernote/thrift/protocol/TList;)V

    .line 1108
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->sharedNotebooks:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/evernote/edam/type/SharedNotebook;

    .line 1110
    invoke-virtual {v1, p1}, Lcom/evernote/edam/type/SharedNotebook;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    goto :goto_1

    .line 1112
    :cond_b
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeListEnd()V

    .line 1114
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1117
    :cond_c
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->businessNotebook:Lcom/evernote/edam/type/BusinessNotebook;

    if-eqz v0, :cond_d

    .line 1118
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetBusinessNotebook()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1119
    sget-object v0, Lcom/evernote/edam/type/Notebook;->BUSINESS_NOTEBOOK_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1120
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->businessNotebook:Lcom/evernote/edam/type/BusinessNotebook;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/type/BusinessNotebook;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1121
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1124
    :cond_d
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->contact:Lcom/evernote/edam/type/User;

    if-eqz v0, :cond_e

    .line 1125
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetContact()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1126
    sget-object v0, Lcom/evernote/edam/type/Notebook;->CONTACT_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1127
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->contact:Lcom/evernote/edam/type/User;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/type/User;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1128
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1131
    :cond_e
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->restrictions:Lcom/evernote/edam/type/NotebookRestrictions;

    if-eqz v0, :cond_f

    .line 1132
    invoke-virtual {p0}, Lcom/evernote/edam/type/Notebook;->isSetRestrictions()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1133
    sget-object v0, Lcom/evernote/edam/type/Notebook;->RESTRICTIONS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1134
    iget-object v0, p0, Lcom/evernote/edam/type/Notebook;->restrictions:Lcom/evernote/edam/type/NotebookRestrictions;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/type/NotebookRestrictions;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1135
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1138
    :cond_f
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldStop()V

    .line 1139
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeStructEnd()V

    return-void
.end method

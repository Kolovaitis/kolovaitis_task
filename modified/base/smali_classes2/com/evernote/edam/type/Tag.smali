.class public Lcom/evernote/edam/type/Tag;
.super Ljava/lang/Object;
.source "Tag.java"

# interfaces
.implements Lcom/evernote/thrift/TBase;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/evernote/thrift/TBase<",
        "Lcom/evernote/edam/type/Tag;",
        ">;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# static fields
.field private static final GUID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final NAME_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final PARENT_GUID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

.field private static final UPDATE_SEQUENCE_NUM_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final __UPDATESEQUENCENUM_ISSET_ID:I


# instance fields
.field private __isset_vector:[Z

.field private guid:Ljava/lang/String;

.field private name:Ljava/lang/String;

.field private parentGuid:Ljava/lang/String;

.field private updateSequenceNum:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 65
    new-instance v0, Lcom/evernote/thrift/protocol/TStruct;

    const-string v1, "Tag"

    invoke-direct {v0, v1}, Lcom/evernote/thrift/protocol/TStruct;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/evernote/edam/type/Tag;->STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

    .line 67
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "guid"

    const/16 v2, 0xb

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Tag;->GUID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 68
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "name"

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Tag;->NAME_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 69
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "parentGuid"

    const/4 v3, 0x3

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Tag;->PARENT_GUID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 70
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "updateSequenceNum"

    const/16 v2, 0x8

    const/4 v3, 0x4

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/Tag;->UPDATE_SEQUENCE_NUM_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 80
    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/evernote/edam/type/Tag;->__isset_vector:[Z

    return-void
.end method

.method public constructor <init>(Lcom/evernote/edam/type/Tag;)V
    .locals 4

    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 80
    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/evernote/edam/type/Tag;->__isset_vector:[Z

    .line 89
    iget-object v0, p1, Lcom/evernote/edam/type/Tag;->__isset_vector:[Z

    iget-object v1, p0, Lcom/evernote/edam/type/Tag;->__isset_vector:[Z

    array-length v2, v0

    const/4 v3, 0x0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 90
    invoke-virtual {p1}, Lcom/evernote/edam/type/Tag;->isSetGuid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p1, Lcom/evernote/edam/type/Tag;->guid:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/type/Tag;->guid:Ljava/lang/String;

    .line 93
    :cond_0
    invoke-virtual {p1}, Lcom/evernote/edam/type/Tag;->isSetName()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 94
    iget-object v0, p1, Lcom/evernote/edam/type/Tag;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/type/Tag;->name:Ljava/lang/String;

    .line 96
    :cond_1
    invoke-virtual {p1}, Lcom/evernote/edam/type/Tag;->isSetParentGuid()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 97
    iget-object v0, p1, Lcom/evernote/edam/type/Tag;->parentGuid:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/type/Tag;->parentGuid:Ljava/lang/String;

    .line 99
    :cond_2
    iget p1, p1, Lcom/evernote/edam/type/Tag;->updateSequenceNum:I

    iput p1, p0, Lcom/evernote/edam/type/Tag;->updateSequenceNum:I

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    const/4 v0, 0x0

    .line 107
    iput-object v0, p0, Lcom/evernote/edam/type/Tag;->guid:Ljava/lang/String;

    .line 108
    iput-object v0, p0, Lcom/evernote/edam/type/Tag;->name:Ljava/lang/String;

    .line 109
    iput-object v0, p0, Lcom/evernote/edam/type/Tag;->parentGuid:Ljava/lang/String;

    const/4 v0, 0x0

    .line 110
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/Tag;->setUpdateSequenceNumIsSet(Z)V

    .line 111
    iput v0, p0, Lcom/evernote/edam/type/Tag;->updateSequenceNum:I

    return-void
.end method

.method public compareTo(Lcom/evernote/edam/type/Tag;)I
    .locals 2

    .line 263
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 264
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    return p1

    .line 270
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/edam/type/Tag;->isSetGuid()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Tag;->isSetGuid()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_1

    return v0

    .line 274
    :cond_1
    invoke-virtual {p0}, Lcom/evernote/edam/type/Tag;->isSetGuid()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/evernote/edam/type/Tag;->guid:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/type/Tag;->guid:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_2

    return v0

    .line 279
    :cond_2
    invoke-virtual {p0}, Lcom/evernote/edam/type/Tag;->isSetName()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Tag;->isSetName()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_3

    return v0

    .line 283
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/type/Tag;->isSetName()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/evernote/edam/type/Tag;->name:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/type/Tag;->name:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_4

    return v0

    .line 288
    :cond_4
    invoke-virtual {p0}, Lcom/evernote/edam/type/Tag;->isSetParentGuid()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Tag;->isSetParentGuid()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_5

    return v0

    .line 292
    :cond_5
    invoke-virtual {p0}, Lcom/evernote/edam/type/Tag;->isSetParentGuid()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/evernote/edam/type/Tag;->parentGuid:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/type/Tag;->parentGuid:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_6

    return v0

    .line 297
    :cond_6
    invoke-virtual {p0}, Lcom/evernote/edam/type/Tag;->isSetUpdateSequenceNum()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/Tag;->isSetUpdateSequenceNum()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_7

    return v0

    .line 301
    :cond_7
    invoke-virtual {p0}, Lcom/evernote/edam/type/Tag;->isSetUpdateSequenceNum()Z

    move-result v0

    if-eqz v0, :cond_8

    iget v0, p0, Lcom/evernote/edam/type/Tag;->updateSequenceNum:I

    iget p1, p1, Lcom/evernote/edam/type/Tag;->updateSequenceNum:I

    invoke-static {v0, p1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(II)I

    move-result p1

    if-eqz p1, :cond_8

    return p1

    :cond_8
    const/4 p1, 0x0

    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 64
    check-cast p1, Lcom/evernote/edam/type/Tag;

    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/Tag;->compareTo(Lcom/evernote/edam/type/Tag;)I

    move-result p1

    return p1
.end method

.method public deepCopy()Lcom/evernote/edam/type/Tag;
    .locals 1

    .line 103
    new-instance v0, Lcom/evernote/edam/type/Tag;

    invoke-direct {v0, p0}, Lcom/evernote/edam/type/Tag;-><init>(Lcom/evernote/edam/type/Tag;)V

    return-object v0
.end method

.method public bridge synthetic deepCopy()Lcom/evernote/thrift/TBase;
    .locals 1

    .line 64
    invoke-virtual {p0}, Lcom/evernote/edam/type/Tag;->deepCopy()Lcom/evernote/edam/type/Tag;

    move-result-object v0

    return-object v0
.end method

.method public equals(Lcom/evernote/edam/type/Tag;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 218
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/edam/type/Tag;->isSetGuid()Z

    move-result v1

    .line 219
    invoke-virtual {p1}, Lcom/evernote/edam/type/Tag;->isSetGuid()Z

    move-result v2

    if-nez v1, :cond_1

    if-eqz v2, :cond_3

    :cond_1
    if-eqz v1, :cond_10

    if-nez v2, :cond_2

    goto :goto_3

    .line 223
    :cond_2
    iget-object v1, p0, Lcom/evernote/edam/type/Tag;->guid:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/type/Tag;->guid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    return v0

    .line 227
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/type/Tag;->isSetName()Z

    move-result v1

    .line 228
    invoke-virtual {p1}, Lcom/evernote/edam/type/Tag;->isSetName()Z

    move-result v2

    if-nez v1, :cond_4

    if-eqz v2, :cond_6

    :cond_4
    if-eqz v1, :cond_f

    if-nez v2, :cond_5

    goto :goto_2

    .line 232
    :cond_5
    iget-object v1, p0, Lcom/evernote/edam/type/Tag;->name:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/type/Tag;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    return v0

    .line 236
    :cond_6
    invoke-virtual {p0}, Lcom/evernote/edam/type/Tag;->isSetParentGuid()Z

    move-result v1

    .line 237
    invoke-virtual {p1}, Lcom/evernote/edam/type/Tag;->isSetParentGuid()Z

    move-result v2

    if-nez v1, :cond_7

    if-eqz v2, :cond_9

    :cond_7
    if-eqz v1, :cond_e

    if-nez v2, :cond_8

    goto :goto_1

    .line 241
    :cond_8
    iget-object v1, p0, Lcom/evernote/edam/type/Tag;->parentGuid:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/type/Tag;->parentGuid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    return v0

    .line 245
    :cond_9
    invoke-virtual {p0}, Lcom/evernote/edam/type/Tag;->isSetUpdateSequenceNum()Z

    move-result v1

    .line 246
    invoke-virtual {p1}, Lcom/evernote/edam/type/Tag;->isSetUpdateSequenceNum()Z

    move-result v2

    if-nez v1, :cond_a

    if-eqz v2, :cond_c

    :cond_a
    if-eqz v1, :cond_d

    if-nez v2, :cond_b

    goto :goto_0

    .line 250
    :cond_b
    iget v1, p0, Lcom/evernote/edam/type/Tag;->updateSequenceNum:I

    iget p1, p1, Lcom/evernote/edam/type/Tag;->updateSequenceNum:I

    if-eq v1, p1, :cond_c

    return v0

    :cond_c
    const/4 p1, 0x1

    return p1

    :cond_d
    :goto_0
    return v0

    :cond_e
    :goto_1
    return v0

    :cond_f
    :goto_2
    return v0

    :cond_10
    :goto_3
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 209
    :cond_0
    instance-of v1, p1, Lcom/evernote/edam/type/Tag;

    if-eqz v1, :cond_1

    .line 210
    check-cast p1, Lcom/evernote/edam/type/Tag;

    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/Tag;->equals(Lcom/evernote/edam/type/Tag;)Z

    move-result p1

    return p1

    :cond_1
    return v0
.end method

.method public getGuid()Ljava/lang/String;
    .locals 1

    .line 115
    iget-object v0, p0, Lcom/evernote/edam/type/Tag;->guid:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 138
    iget-object v0, p0, Lcom/evernote/edam/type/Tag;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getParentGuid()Ljava/lang/String;
    .locals 1

    .line 161
    iget-object v0, p0, Lcom/evernote/edam/type/Tag;->parentGuid:Ljava/lang/String;

    return-object v0
.end method

.method public getUpdateSequenceNum()I
    .locals 1

    .line 184
    iget v0, p0, Lcom/evernote/edam/type/Tag;->updateSequenceNum:I

    return v0
.end method

.method public hashCode()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isSetGuid()Z
    .locals 1

    .line 128
    iget-object v0, p0, Lcom/evernote/edam/type/Tag;->guid:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetName()Z
    .locals 1

    .line 151
    iget-object v0, p0, Lcom/evernote/edam/type/Tag;->name:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetParentGuid()Z
    .locals 1

    .line 174
    iget-object v0, p0, Lcom/evernote/edam/type/Tag;->parentGuid:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetUpdateSequenceNum()Z
    .locals 2

    .line 198
    iget-object v0, p0, Lcom/evernote/edam/type/Tag;->__isset_vector:[Z

    const/4 v1, 0x0

    aget-boolean v0, v0, v1

    return v0
.end method

.method public read(Lcom/evernote/thrift/protocol/TProtocol;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 311
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readStructBegin()Lcom/evernote/thrift/protocol/TStruct;

    .line 314
    :goto_0
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readFieldBegin()Lcom/evernote/thrift/protocol/TField;

    move-result-object v0

    .line 315
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-nez v1, :cond_0

    .line 353
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readStructEnd()V

    .line 354
    invoke-virtual {p0}, Lcom/evernote/edam/type/Tag;->validate()V

    return-void

    .line 318
    :cond_0
    iget-short v1, v0, Lcom/evernote/thrift/protocol/TField;->id:S

    const/16 v2, 0xb

    packed-switch v1, :pswitch_data_0

    .line 349
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 341
    :pswitch_0
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    const/16 v2, 0x8

    if-ne v1, v2, :cond_1

    .line 342
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI32()I

    move-result v0

    iput v0, p0, Lcom/evernote/edam/type/Tag;->updateSequenceNum:I

    const/4 v0, 0x1

    .line 343
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/Tag;->setUpdateSequenceNumIsSet(Z)V

    goto :goto_1

    .line 345
    :cond_1
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 334
    :pswitch_1
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v2, :cond_2

    .line 335
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/type/Tag;->parentGuid:Ljava/lang/String;

    goto :goto_1

    .line 337
    :cond_2
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 327
    :pswitch_2
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v2, :cond_3

    .line 328
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/type/Tag;->name:Ljava/lang/String;

    goto :goto_1

    .line 330
    :cond_3
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 320
    :pswitch_3
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v2, :cond_4

    .line 321
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/type/Tag;->guid:Ljava/lang/String;

    goto :goto_1

    .line 323
    :cond_4
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    .line 351
    :goto_1
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readFieldEnd()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setGuid(Ljava/lang/String;)V
    .locals 0

    .line 119
    iput-object p1, p0, Lcom/evernote/edam/type/Tag;->guid:Ljava/lang/String;

    return-void
.end method

.method public setGuidIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 133
    iput-object p1, p0, Lcom/evernote/edam/type/Tag;->guid:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0

    .line 142
    iput-object p1, p0, Lcom/evernote/edam/type/Tag;->name:Ljava/lang/String;

    return-void
.end method

.method public setNameIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 156
    iput-object p1, p0, Lcom/evernote/edam/type/Tag;->name:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setParentGuid(Ljava/lang/String;)V
    .locals 0

    .line 165
    iput-object p1, p0, Lcom/evernote/edam/type/Tag;->parentGuid:Ljava/lang/String;

    return-void
.end method

.method public setParentGuidIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 179
    iput-object p1, p0, Lcom/evernote/edam/type/Tag;->parentGuid:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setUpdateSequenceNum(I)V
    .locals 0

    .line 188
    iput p1, p0, Lcom/evernote/edam/type/Tag;->updateSequenceNum:I

    const/4 p1, 0x1

    .line 189
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/Tag;->setUpdateSequenceNumIsSet(Z)V

    return-void
.end method

.method public setUpdateSequenceNumIsSet(Z)V
    .locals 2

    .line 202
    iget-object v0, p0, Lcom/evernote/edam/type/Tag;->__isset_vector:[Z

    const/4 v1, 0x0

    aput-boolean p1, v0, v1

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 393
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Tag("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 396
    invoke-virtual {p0}, Lcom/evernote/edam/type/Tag;->isSetGuid()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    const-string v1, "guid:"

    .line 397
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 398
    iget-object v1, p0, Lcom/evernote/edam/type/Tag;->guid:Ljava/lang/String;

    if-nez v1, :cond_0

    const-string v1, "null"

    .line 399
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 401
    :cond_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    const/4 v1, 0x1

    .line 405
    :goto_1
    invoke-virtual {p0}, Lcom/evernote/edam/type/Tag;->isSetName()Z

    move-result v3

    if-eqz v3, :cond_4

    if-nez v1, :cond_2

    const-string v1, ", "

    .line 406
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const-string v1, "name:"

    .line 407
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 408
    iget-object v1, p0, Lcom/evernote/edam/type/Tag;->name:Ljava/lang/String;

    if-nez v1, :cond_3

    const-string v1, "null"

    .line 409
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 411
    :cond_3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_2
    const/4 v1, 0x0

    .line 415
    :cond_4
    invoke-virtual {p0}, Lcom/evernote/edam/type/Tag;->isSetParentGuid()Z

    move-result v3

    if-eqz v3, :cond_7

    if-nez v1, :cond_5

    const-string v1, ", "

    .line 416
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    const-string v1, "parentGuid:"

    .line 417
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 418
    iget-object v1, p0, Lcom/evernote/edam/type/Tag;->parentGuid:Ljava/lang/String;

    if-nez v1, :cond_6

    const-string v1, "null"

    .line 419
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 421
    :cond_6
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    const/4 v1, 0x0

    .line 425
    :cond_7
    invoke-virtual {p0}, Lcom/evernote/edam/type/Tag;->isSetUpdateSequenceNum()Z

    move-result v2

    if-eqz v2, :cond_9

    if-nez v1, :cond_8

    const-string v1, ", "

    .line 426
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_8
    const-string v1, "updateSequenceNum:"

    .line 427
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 428
    iget v1, p0, Lcom/evernote/edam/type/Tag;->updateSequenceNum:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_9
    const-string v1, ")"

    .line 431
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 432
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public unsetGuid()V
    .locals 1

    const/4 v0, 0x0

    .line 123
    iput-object v0, p0, Lcom/evernote/edam/type/Tag;->guid:Ljava/lang/String;

    return-void
.end method

.method public unsetName()V
    .locals 1

    const/4 v0, 0x0

    .line 146
    iput-object v0, p0, Lcom/evernote/edam/type/Tag;->name:Ljava/lang/String;

    return-void
.end method

.method public unsetParentGuid()V
    .locals 1

    const/4 v0, 0x0

    .line 169
    iput-object v0, p0, Lcom/evernote/edam/type/Tag;->parentGuid:Ljava/lang/String;

    return-void
.end method

.method public unsetUpdateSequenceNum()V
    .locals 2

    .line 193
    iget-object v0, p0, Lcom/evernote/edam/type/Tag;->__isset_vector:[Z

    const/4 v1, 0x0

    aput-boolean v1, v0, v1

    return-void
.end method

.method public validate()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    return-void
.end method

.method public write(Lcom/evernote/thrift/protocol/TProtocol;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 358
    invoke-virtual {p0}, Lcom/evernote/edam/type/Tag;->validate()V

    .line 360
    sget-object v0, Lcom/evernote/edam/type/Tag;->STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeStructBegin(Lcom/evernote/thrift/protocol/TStruct;)V

    .line 361
    iget-object v0, p0, Lcom/evernote/edam/type/Tag;->guid:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 362
    invoke-virtual {p0}, Lcom/evernote/edam/type/Tag;->isSetGuid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 363
    sget-object v0, Lcom/evernote/edam/type/Tag;->GUID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 364
    iget-object v0, p0, Lcom/evernote/edam/type/Tag;->guid:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 365
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 368
    :cond_0
    iget-object v0, p0, Lcom/evernote/edam/type/Tag;->name:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 369
    invoke-virtual {p0}, Lcom/evernote/edam/type/Tag;->isSetName()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 370
    sget-object v0, Lcom/evernote/edam/type/Tag;->NAME_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 371
    iget-object v0, p0, Lcom/evernote/edam/type/Tag;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 372
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 375
    :cond_1
    iget-object v0, p0, Lcom/evernote/edam/type/Tag;->parentGuid:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 376
    invoke-virtual {p0}, Lcom/evernote/edam/type/Tag;->isSetParentGuid()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 377
    sget-object v0, Lcom/evernote/edam/type/Tag;->PARENT_GUID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 378
    iget-object v0, p0, Lcom/evernote/edam/type/Tag;->parentGuid:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 379
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 382
    :cond_2
    invoke-virtual {p0}, Lcom/evernote/edam/type/Tag;->isSetUpdateSequenceNum()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 383
    sget-object v0, Lcom/evernote/edam/type/Tag;->UPDATE_SEQUENCE_NUM_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 384
    iget v0, p0, Lcom/evernote/edam/type/Tag;->updateSequenceNum:I

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeI32(I)V

    .line 385
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 387
    :cond_3
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldStop()V

    .line 388
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeStructEnd()V

    return-void
.end method

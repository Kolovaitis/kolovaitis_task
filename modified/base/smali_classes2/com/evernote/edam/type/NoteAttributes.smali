.class public Lcom/evernote/edam/type/NoteAttributes;
.super Ljava/lang/Object;
.source "NoteAttributes.java"

# interfaces
.implements Lcom/evernote/thrift/TBase;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/evernote/thrift/TBase<",
        "Lcom/evernote/edam/type/NoteAttributes;",
        ">;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# static fields
.field private static final ALTITUDE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final APPLICATION_DATA_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final AUTHOR_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final CLASSIFICATIONS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final CONTENT_CLASS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final CREATOR_ID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final LAST_EDITED_BY_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final LAST_EDITOR_ID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final LATITUDE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final LONGITUDE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final PLACE_NAME_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final REMINDER_DONE_TIME_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final REMINDER_ORDER_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final REMINDER_TIME_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final SHARE_DATE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final SOURCE_APPLICATION_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final SOURCE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final SOURCE_URL_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

.field private static final SUBJECT_DATE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final __ALTITUDE_ISSET_ID:I = 0x3

.field private static final __CREATORID_ISSET_ID:I = 0x8

.field private static final __LASTEDITORID_ISSET_ID:I = 0x9

.field private static final __LATITUDE_ISSET_ID:I = 0x1

.field private static final __LONGITUDE_ISSET_ID:I = 0x2

.field private static final __REMINDERDONETIME_ISSET_ID:I = 0x6

.field private static final __REMINDERORDER_ISSET_ID:I = 0x5

.field private static final __REMINDERTIME_ISSET_ID:I = 0x7

.field private static final __SHAREDATE_ISSET_ID:I = 0x4

.field private static final __SUBJECTDATE_ISSET_ID:I


# instance fields
.field private __isset_vector:[Z

.field private altitude:D

.field private applicationData:Lcom/evernote/edam/type/LazyMap;

.field private author:Ljava/lang/String;

.field private classifications:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private contentClass:Ljava/lang/String;

.field private creatorId:I

.field private lastEditedBy:Ljava/lang/String;

.field private lastEditorId:I

.field private latitude:D

.field private longitude:D

.field private placeName:Ljava/lang/String;

.field private reminderDoneTime:J

.field private reminderOrder:J

.field private reminderTime:J

.field private shareDate:J

.field private source:Ljava/lang/String;

.field private sourceApplication:Ljava/lang/String;

.field private sourceURL:Ljava/lang/String;

.field private subjectDate:J


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 191
    new-instance v0, Lcom/evernote/thrift/protocol/TStruct;

    const-string v1, "NoteAttributes"

    invoke-direct {v0, v1}, Lcom/evernote/thrift/protocol/TStruct;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/evernote/edam/type/NoteAttributes;->STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

    .line 193
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "subjectDate"

    const/16 v2, 0xa

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/NoteAttributes;->SUBJECT_DATE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 194
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "latitude"

    const/4 v3, 0x4

    invoke-direct {v0, v1, v3, v2}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/NoteAttributes;->LATITUDE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 195
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "longitude"

    const/16 v4, 0xb

    invoke-direct {v0, v1, v3, v4}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/NoteAttributes;->LONGITUDE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 196
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "altitude"

    const/16 v5, 0xc

    invoke-direct {v0, v1, v3, v5}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/NoteAttributes;->ALTITUDE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 197
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "author"

    const/16 v3, 0xd

    invoke-direct {v0, v1, v4, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/NoteAttributes;->AUTHOR_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 198
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "source"

    const/16 v6, 0xe

    invoke-direct {v0, v1, v4, v6}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/NoteAttributes;->SOURCE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 199
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "sourceURL"

    const/16 v6, 0xf

    invoke-direct {v0, v1, v4, v6}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/NoteAttributes;->SOURCE_URL_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 200
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "sourceApplication"

    const/16 v6, 0x10

    invoke-direct {v0, v1, v4, v6}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/NoteAttributes;->SOURCE_APPLICATION_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 201
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "shareDate"

    const/16 v6, 0x11

    invoke-direct {v0, v1, v2, v6}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/NoteAttributes;->SHARE_DATE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 202
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "reminderOrder"

    const/16 v6, 0x12

    invoke-direct {v0, v1, v2, v6}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/NoteAttributes;->REMINDER_ORDER_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 203
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "reminderDoneTime"

    const/16 v6, 0x13

    invoke-direct {v0, v1, v2, v6}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/NoteAttributes;->REMINDER_DONE_TIME_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 204
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "reminderTime"

    const/16 v6, 0x14

    invoke-direct {v0, v1, v2, v6}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/NoteAttributes;->REMINDER_TIME_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 205
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "placeName"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v4, v2}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/NoteAttributes;->PLACE_NAME_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 206
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "contentClass"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v4, v2}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/NoteAttributes;->CONTENT_CLASS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 207
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "applicationData"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v5, v2}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/NoteAttributes;->APPLICATION_DATA_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 208
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "lastEditedBy"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v4, v2}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/NoteAttributes;->LAST_EDITED_BY_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 209
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "classifications"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v3, v2}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/NoteAttributes;->CLASSIFICATIONS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 210
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "creatorId"

    const/16 v2, 0x8

    const/16 v3, 0x1b

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/NoteAttributes;->CREATOR_ID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 211
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "lastEditorId"

    const/16 v3, 0x1c

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/NoteAttributes;->LAST_EDITOR_ID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 247
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0xa

    .line 245
    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->__isset_vector:[Z

    return-void
.end method

.method public constructor <init>(Lcom/evernote/edam/type/NoteAttributes;)V
    .locals 4

    .line 253
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0xa

    .line 245
    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->__isset_vector:[Z

    .line 254
    iget-object v0, p1, Lcom/evernote/edam/type/NoteAttributes;->__isset_vector:[Z

    iget-object v1, p0, Lcom/evernote/edam/type/NoteAttributes;->__isset_vector:[Z

    array-length v2, v0

    const/4 v3, 0x0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 255
    iget-wide v0, p1, Lcom/evernote/edam/type/NoteAttributes;->subjectDate:J

    iput-wide v0, p0, Lcom/evernote/edam/type/NoteAttributes;->subjectDate:J

    .line 256
    iget-wide v0, p1, Lcom/evernote/edam/type/NoteAttributes;->latitude:D

    iput-wide v0, p0, Lcom/evernote/edam/type/NoteAttributes;->latitude:D

    .line 257
    iget-wide v0, p1, Lcom/evernote/edam/type/NoteAttributes;->longitude:D

    iput-wide v0, p0, Lcom/evernote/edam/type/NoteAttributes;->longitude:D

    .line 258
    iget-wide v0, p1, Lcom/evernote/edam/type/NoteAttributes;->altitude:D

    iput-wide v0, p0, Lcom/evernote/edam/type/NoteAttributes;->altitude:D

    .line 259
    invoke-virtual {p1}, Lcom/evernote/edam/type/NoteAttributes;->isSetAuthor()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 260
    iget-object v0, p1, Lcom/evernote/edam/type/NoteAttributes;->author:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->author:Ljava/lang/String;

    .line 262
    :cond_0
    invoke-virtual {p1}, Lcom/evernote/edam/type/NoteAttributes;->isSetSource()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 263
    iget-object v0, p1, Lcom/evernote/edam/type/NoteAttributes;->source:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->source:Ljava/lang/String;

    .line 265
    :cond_1
    invoke-virtual {p1}, Lcom/evernote/edam/type/NoteAttributes;->isSetSourceURL()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 266
    iget-object v0, p1, Lcom/evernote/edam/type/NoteAttributes;->sourceURL:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->sourceURL:Ljava/lang/String;

    .line 268
    :cond_2
    invoke-virtual {p1}, Lcom/evernote/edam/type/NoteAttributes;->isSetSourceApplication()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 269
    iget-object v0, p1, Lcom/evernote/edam/type/NoteAttributes;->sourceApplication:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->sourceApplication:Ljava/lang/String;

    .line 271
    :cond_3
    iget-wide v0, p1, Lcom/evernote/edam/type/NoteAttributes;->shareDate:J

    iput-wide v0, p0, Lcom/evernote/edam/type/NoteAttributes;->shareDate:J

    .line 272
    iget-wide v0, p1, Lcom/evernote/edam/type/NoteAttributes;->reminderOrder:J

    iput-wide v0, p0, Lcom/evernote/edam/type/NoteAttributes;->reminderOrder:J

    .line 273
    iget-wide v0, p1, Lcom/evernote/edam/type/NoteAttributes;->reminderDoneTime:J

    iput-wide v0, p0, Lcom/evernote/edam/type/NoteAttributes;->reminderDoneTime:J

    .line 274
    iget-wide v0, p1, Lcom/evernote/edam/type/NoteAttributes;->reminderTime:J

    iput-wide v0, p0, Lcom/evernote/edam/type/NoteAttributes;->reminderTime:J

    .line 275
    invoke-virtual {p1}, Lcom/evernote/edam/type/NoteAttributes;->isSetPlaceName()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 276
    iget-object v0, p1, Lcom/evernote/edam/type/NoteAttributes;->placeName:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->placeName:Ljava/lang/String;

    .line 278
    :cond_4
    invoke-virtual {p1}, Lcom/evernote/edam/type/NoteAttributes;->isSetContentClass()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 279
    iget-object v0, p1, Lcom/evernote/edam/type/NoteAttributes;->contentClass:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->contentClass:Ljava/lang/String;

    .line 281
    :cond_5
    invoke-virtual {p1}, Lcom/evernote/edam/type/NoteAttributes;->isSetApplicationData()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 282
    new-instance v0, Lcom/evernote/edam/type/LazyMap;

    iget-object v1, p1, Lcom/evernote/edam/type/NoteAttributes;->applicationData:Lcom/evernote/edam/type/LazyMap;

    invoke-direct {v0, v1}, Lcom/evernote/edam/type/LazyMap;-><init>(Lcom/evernote/edam/type/LazyMap;)V

    iput-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->applicationData:Lcom/evernote/edam/type/LazyMap;

    .line 284
    :cond_6
    invoke-virtual {p1}, Lcom/evernote/edam/type/NoteAttributes;->isSetLastEditedBy()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 285
    iget-object v0, p1, Lcom/evernote/edam/type/NoteAttributes;->lastEditedBy:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->lastEditedBy:Ljava/lang/String;

    .line 287
    :cond_7
    invoke-virtual {p1}, Lcom/evernote/edam/type/NoteAttributes;->isSetClassifications()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 288
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 289
    iget-object v1, p1, Lcom/evernote/edam/type/NoteAttributes;->classifications:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 291
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 292
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 298
    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 300
    :cond_8
    iput-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->classifications:Ljava/util/Map;

    .line 302
    :cond_9
    iget v0, p1, Lcom/evernote/edam/type/NoteAttributes;->creatorId:I

    iput v0, p0, Lcom/evernote/edam/type/NoteAttributes;->creatorId:I

    .line 303
    iget p1, p1, Lcom/evernote/edam/type/NoteAttributes;->lastEditorId:I

    iput p1, p0, Lcom/evernote/edam/type/NoteAttributes;->lastEditorId:I

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 5

    const/4 v0, 0x0

    .line 311
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/NoteAttributes;->setSubjectDateIsSet(Z)V

    const-wide/16 v1, 0x0

    .line 312
    iput-wide v1, p0, Lcom/evernote/edam/type/NoteAttributes;->subjectDate:J

    .line 313
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/NoteAttributes;->setLatitudeIsSet(Z)V

    const-wide/16 v3, 0x0

    .line 314
    iput-wide v3, p0, Lcom/evernote/edam/type/NoteAttributes;->latitude:D

    .line 315
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/NoteAttributes;->setLongitudeIsSet(Z)V

    .line 316
    iput-wide v3, p0, Lcom/evernote/edam/type/NoteAttributes;->longitude:D

    .line 317
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/NoteAttributes;->setAltitudeIsSet(Z)V

    .line 318
    iput-wide v3, p0, Lcom/evernote/edam/type/NoteAttributes;->altitude:D

    const/4 v3, 0x0

    .line 319
    iput-object v3, p0, Lcom/evernote/edam/type/NoteAttributes;->author:Ljava/lang/String;

    .line 320
    iput-object v3, p0, Lcom/evernote/edam/type/NoteAttributes;->source:Ljava/lang/String;

    .line 321
    iput-object v3, p0, Lcom/evernote/edam/type/NoteAttributes;->sourceURL:Ljava/lang/String;

    .line 322
    iput-object v3, p0, Lcom/evernote/edam/type/NoteAttributes;->sourceApplication:Ljava/lang/String;

    .line 323
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/NoteAttributes;->setShareDateIsSet(Z)V

    .line 324
    iput-wide v1, p0, Lcom/evernote/edam/type/NoteAttributes;->shareDate:J

    .line 325
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/NoteAttributes;->setReminderOrderIsSet(Z)V

    .line 326
    iput-wide v1, p0, Lcom/evernote/edam/type/NoteAttributes;->reminderOrder:J

    .line 327
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/NoteAttributes;->setReminderDoneTimeIsSet(Z)V

    .line 328
    iput-wide v1, p0, Lcom/evernote/edam/type/NoteAttributes;->reminderDoneTime:J

    .line 329
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/NoteAttributes;->setReminderTimeIsSet(Z)V

    .line 330
    iput-wide v1, p0, Lcom/evernote/edam/type/NoteAttributes;->reminderTime:J

    .line 331
    iput-object v3, p0, Lcom/evernote/edam/type/NoteAttributes;->placeName:Ljava/lang/String;

    .line 332
    iput-object v3, p0, Lcom/evernote/edam/type/NoteAttributes;->contentClass:Ljava/lang/String;

    .line 333
    iput-object v3, p0, Lcom/evernote/edam/type/NoteAttributes;->applicationData:Lcom/evernote/edam/type/LazyMap;

    .line 334
    iput-object v3, p0, Lcom/evernote/edam/type/NoteAttributes;->lastEditedBy:Ljava/lang/String;

    .line 335
    iput-object v3, p0, Lcom/evernote/edam/type/NoteAttributes;->classifications:Ljava/util/Map;

    .line 336
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/NoteAttributes;->setCreatorIdIsSet(Z)V

    .line 337
    iput v0, p0, Lcom/evernote/edam/type/NoteAttributes;->creatorId:I

    .line 338
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/NoteAttributes;->setLastEditorIdIsSet(Z)V

    .line 339
    iput v0, p0, Lcom/evernote/edam/type/NoteAttributes;->lastEditorId:I

    return-void
.end method

.method public compareTo(Lcom/evernote/edam/type/NoteAttributes;)I
    .locals 4

    .line 973
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 974
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    return p1

    .line 980
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetSubjectDate()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/NoteAttributes;->isSetSubjectDate()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_1

    return v0

    .line 984
    :cond_1
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetSubjectDate()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-wide v0, p0, Lcom/evernote/edam/type/NoteAttributes;->subjectDate:J

    iget-wide v2, p1, Lcom/evernote/edam/type/NoteAttributes;->subjectDate:J

    invoke-static {v0, v1, v2, v3}, Lcom/evernote/thrift/TBaseHelper;->compareTo(JJ)I

    move-result v0

    if-eqz v0, :cond_2

    return v0

    .line 989
    :cond_2
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetLatitude()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/NoteAttributes;->isSetLatitude()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_3

    return v0

    .line 993
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetLatitude()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-wide v0, p0, Lcom/evernote/edam/type/NoteAttributes;->latitude:D

    iget-wide v2, p1, Lcom/evernote/edam/type/NoteAttributes;->latitude:D

    invoke-static {v0, v1, v2, v3}, Lcom/evernote/thrift/TBaseHelper;->compareTo(DD)I

    move-result v0

    if-eqz v0, :cond_4

    return v0

    .line 998
    :cond_4
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetLongitude()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/NoteAttributes;->isSetLongitude()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_5

    return v0

    .line 1002
    :cond_5
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetLongitude()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-wide v0, p0, Lcom/evernote/edam/type/NoteAttributes;->longitude:D

    iget-wide v2, p1, Lcom/evernote/edam/type/NoteAttributes;->longitude:D

    invoke-static {v0, v1, v2, v3}, Lcom/evernote/thrift/TBaseHelper;->compareTo(DD)I

    move-result v0

    if-eqz v0, :cond_6

    return v0

    .line 1007
    :cond_6
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetAltitude()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/NoteAttributes;->isSetAltitude()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_7

    return v0

    .line 1011
    :cond_7
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetAltitude()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-wide v0, p0, Lcom/evernote/edam/type/NoteAttributes;->altitude:D

    iget-wide v2, p1, Lcom/evernote/edam/type/NoteAttributes;->altitude:D

    invoke-static {v0, v1, v2, v3}, Lcom/evernote/thrift/TBaseHelper;->compareTo(DD)I

    move-result v0

    if-eqz v0, :cond_8

    return v0

    .line 1016
    :cond_8
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetAuthor()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/NoteAttributes;->isSetAuthor()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_9

    return v0

    .line 1020
    :cond_9
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetAuthor()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->author:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/type/NoteAttributes;->author:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_a

    return v0

    .line 1025
    :cond_a
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetSource()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/NoteAttributes;->isSetSource()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_b

    return v0

    .line 1029
    :cond_b
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetSource()Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->source:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/type/NoteAttributes;->source:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_c

    return v0

    .line 1034
    :cond_c
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetSourceURL()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/NoteAttributes;->isSetSourceURL()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_d

    return v0

    .line 1038
    :cond_d
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetSourceURL()Z

    move-result v0

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->sourceURL:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/type/NoteAttributes;->sourceURL:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_e

    return v0

    .line 1043
    :cond_e
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetSourceApplication()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/NoteAttributes;->isSetSourceApplication()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_f

    return v0

    .line 1047
    :cond_f
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetSourceApplication()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->sourceApplication:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/type/NoteAttributes;->sourceApplication:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_10

    return v0

    .line 1052
    :cond_10
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetShareDate()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/NoteAttributes;->isSetShareDate()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_11

    return v0

    .line 1056
    :cond_11
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetShareDate()Z

    move-result v0

    if-eqz v0, :cond_12

    iget-wide v0, p0, Lcom/evernote/edam/type/NoteAttributes;->shareDate:J

    iget-wide v2, p1, Lcom/evernote/edam/type/NoteAttributes;->shareDate:J

    invoke-static {v0, v1, v2, v3}, Lcom/evernote/thrift/TBaseHelper;->compareTo(JJ)I

    move-result v0

    if-eqz v0, :cond_12

    return v0

    .line 1061
    :cond_12
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetReminderOrder()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/NoteAttributes;->isSetReminderOrder()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_13

    return v0

    .line 1065
    :cond_13
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetReminderOrder()Z

    move-result v0

    if-eqz v0, :cond_14

    iget-wide v0, p0, Lcom/evernote/edam/type/NoteAttributes;->reminderOrder:J

    iget-wide v2, p1, Lcom/evernote/edam/type/NoteAttributes;->reminderOrder:J

    invoke-static {v0, v1, v2, v3}, Lcom/evernote/thrift/TBaseHelper;->compareTo(JJ)I

    move-result v0

    if-eqz v0, :cond_14

    return v0

    .line 1070
    :cond_14
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetReminderDoneTime()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/NoteAttributes;->isSetReminderDoneTime()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_15

    return v0

    .line 1074
    :cond_15
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetReminderDoneTime()Z

    move-result v0

    if-eqz v0, :cond_16

    iget-wide v0, p0, Lcom/evernote/edam/type/NoteAttributes;->reminderDoneTime:J

    iget-wide v2, p1, Lcom/evernote/edam/type/NoteAttributes;->reminderDoneTime:J

    invoke-static {v0, v1, v2, v3}, Lcom/evernote/thrift/TBaseHelper;->compareTo(JJ)I

    move-result v0

    if-eqz v0, :cond_16

    return v0

    .line 1079
    :cond_16
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetReminderTime()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/NoteAttributes;->isSetReminderTime()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_17

    return v0

    .line 1083
    :cond_17
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetReminderTime()Z

    move-result v0

    if-eqz v0, :cond_18

    iget-wide v0, p0, Lcom/evernote/edam/type/NoteAttributes;->reminderTime:J

    iget-wide v2, p1, Lcom/evernote/edam/type/NoteAttributes;->reminderTime:J

    invoke-static {v0, v1, v2, v3}, Lcom/evernote/thrift/TBaseHelper;->compareTo(JJ)I

    move-result v0

    if-eqz v0, :cond_18

    return v0

    .line 1088
    :cond_18
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetPlaceName()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/NoteAttributes;->isSetPlaceName()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_19

    return v0

    .line 1092
    :cond_19
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetPlaceName()Z

    move-result v0

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->placeName:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/type/NoteAttributes;->placeName:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_1a

    return v0

    .line 1097
    :cond_1a
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetContentClass()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/NoteAttributes;->isSetContentClass()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_1b

    return v0

    .line 1101
    :cond_1b
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetContentClass()Z

    move-result v0

    if-eqz v0, :cond_1c

    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->contentClass:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/type/NoteAttributes;->contentClass:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_1c

    return v0

    .line 1106
    :cond_1c
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetApplicationData()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/NoteAttributes;->isSetApplicationData()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_1d

    return v0

    .line 1110
    :cond_1d
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetApplicationData()Z

    move-result v0

    if-eqz v0, :cond_1e

    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->applicationData:Lcom/evernote/edam/type/LazyMap;

    iget-object v1, p1, Lcom/evernote/edam/type/NoteAttributes;->applicationData:Lcom/evernote/edam/type/LazyMap;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result v0

    if-eqz v0, :cond_1e

    return v0

    .line 1115
    :cond_1e
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetLastEditedBy()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/NoteAttributes;->isSetLastEditedBy()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_1f

    return v0

    .line 1119
    :cond_1f
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetLastEditedBy()Z

    move-result v0

    if-eqz v0, :cond_20

    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->lastEditedBy:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/type/NoteAttributes;->lastEditedBy:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_20

    return v0

    .line 1124
    :cond_20
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetClassifications()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/NoteAttributes;->isSetClassifications()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_21

    return v0

    .line 1128
    :cond_21
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetClassifications()Z

    move-result v0

    if-eqz v0, :cond_22

    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->classifications:Ljava/util/Map;

    iget-object v1, p1, Lcom/evernote/edam/type/NoteAttributes;->classifications:Ljava/util/Map;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/util/Map;Ljava/util/Map;)I

    move-result v0

    if-eqz v0, :cond_22

    return v0

    .line 1133
    :cond_22
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetCreatorId()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/NoteAttributes;->isSetCreatorId()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_23

    return v0

    .line 1137
    :cond_23
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetCreatorId()Z

    move-result v0

    if-eqz v0, :cond_24

    iget v0, p0, Lcom/evernote/edam/type/NoteAttributes;->creatorId:I

    iget v1, p1, Lcom/evernote/edam/type/NoteAttributes;->creatorId:I

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(II)I

    move-result v0

    if-eqz v0, :cond_24

    return v0

    .line 1142
    :cond_24
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetLastEditorId()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/NoteAttributes;->isSetLastEditorId()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_25

    return v0

    .line 1146
    :cond_25
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetLastEditorId()Z

    move-result v0

    if-eqz v0, :cond_26

    iget v0, p0, Lcom/evernote/edam/type/NoteAttributes;->lastEditorId:I

    iget p1, p1, Lcom/evernote/edam/type/NoteAttributes;->lastEditorId:I

    invoke-static {v0, p1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(II)I

    move-result p1

    if-eqz p1, :cond_26

    return p1

    :cond_26
    const/4 p1, 0x0

    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 190
    check-cast p1, Lcom/evernote/edam/type/NoteAttributes;

    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/NoteAttributes;->compareTo(Lcom/evernote/edam/type/NoteAttributes;)I

    move-result p1

    return p1
.end method

.method public deepCopy()Lcom/evernote/edam/type/NoteAttributes;
    .locals 1

    .line 307
    new-instance v0, Lcom/evernote/edam/type/NoteAttributes;

    invoke-direct {v0, p0}, Lcom/evernote/edam/type/NoteAttributes;-><init>(Lcom/evernote/edam/type/NoteAttributes;)V

    return-object v0
.end method

.method public bridge synthetic deepCopy()Lcom/evernote/thrift/TBase;
    .locals 1

    .line 190
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->deepCopy()Lcom/evernote/edam/type/NoteAttributes;

    move-result-object v0

    return-object v0
.end method

.method public equals(Lcom/evernote/edam/type/NoteAttributes;)Z
    .locals 6

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 793
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetSubjectDate()Z

    move-result v1

    .line 794
    invoke-virtual {p1}, Lcom/evernote/edam/type/NoteAttributes;->isSetSubjectDate()Z

    move-result v2

    if-nez v1, :cond_1

    if-eqz v2, :cond_3

    :cond_1
    if-eqz v1, :cond_4c

    if-nez v2, :cond_2

    goto/16 :goto_12

    .line 798
    :cond_2
    iget-wide v1, p0, Lcom/evernote/edam/type/NoteAttributes;->subjectDate:J

    iget-wide v3, p1, Lcom/evernote/edam/type/NoteAttributes;->subjectDate:J

    cmp-long v5, v1, v3

    if-eqz v5, :cond_3

    return v0

    .line 802
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetLatitude()Z

    move-result v1

    .line 803
    invoke-virtual {p1}, Lcom/evernote/edam/type/NoteAttributes;->isSetLatitude()Z

    move-result v2

    if-nez v1, :cond_4

    if-eqz v2, :cond_6

    :cond_4
    if-eqz v1, :cond_4b

    if-nez v2, :cond_5

    goto/16 :goto_11

    .line 807
    :cond_5
    iget-wide v1, p0, Lcom/evernote/edam/type/NoteAttributes;->latitude:D

    iget-wide v3, p1, Lcom/evernote/edam/type/NoteAttributes;->latitude:D

    cmpl-double v5, v1, v3

    if-eqz v5, :cond_6

    return v0

    .line 811
    :cond_6
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetLongitude()Z

    move-result v1

    .line 812
    invoke-virtual {p1}, Lcom/evernote/edam/type/NoteAttributes;->isSetLongitude()Z

    move-result v2

    if-nez v1, :cond_7

    if-eqz v2, :cond_9

    :cond_7
    if-eqz v1, :cond_4a

    if-nez v2, :cond_8

    goto/16 :goto_10

    .line 816
    :cond_8
    iget-wide v1, p0, Lcom/evernote/edam/type/NoteAttributes;->longitude:D

    iget-wide v3, p1, Lcom/evernote/edam/type/NoteAttributes;->longitude:D

    cmpl-double v5, v1, v3

    if-eqz v5, :cond_9

    return v0

    .line 820
    :cond_9
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetAltitude()Z

    move-result v1

    .line 821
    invoke-virtual {p1}, Lcom/evernote/edam/type/NoteAttributes;->isSetAltitude()Z

    move-result v2

    if-nez v1, :cond_a

    if-eqz v2, :cond_c

    :cond_a
    if-eqz v1, :cond_49

    if-nez v2, :cond_b

    goto/16 :goto_f

    .line 825
    :cond_b
    iget-wide v1, p0, Lcom/evernote/edam/type/NoteAttributes;->altitude:D

    iget-wide v3, p1, Lcom/evernote/edam/type/NoteAttributes;->altitude:D

    cmpl-double v5, v1, v3

    if-eqz v5, :cond_c

    return v0

    .line 829
    :cond_c
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetAuthor()Z

    move-result v1

    .line 830
    invoke-virtual {p1}, Lcom/evernote/edam/type/NoteAttributes;->isSetAuthor()Z

    move-result v2

    if-nez v1, :cond_d

    if-eqz v2, :cond_f

    :cond_d
    if-eqz v1, :cond_48

    if-nez v2, :cond_e

    goto/16 :goto_e

    .line 834
    :cond_e
    iget-object v1, p0, Lcom/evernote/edam/type/NoteAttributes;->author:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/type/NoteAttributes;->author:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    return v0

    .line 838
    :cond_f
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetSource()Z

    move-result v1

    .line 839
    invoke-virtual {p1}, Lcom/evernote/edam/type/NoteAttributes;->isSetSource()Z

    move-result v2

    if-nez v1, :cond_10

    if-eqz v2, :cond_12

    :cond_10
    if-eqz v1, :cond_47

    if-nez v2, :cond_11

    goto/16 :goto_d

    .line 843
    :cond_11
    iget-object v1, p0, Lcom/evernote/edam/type/NoteAttributes;->source:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/type/NoteAttributes;->source:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_12

    return v0

    .line 847
    :cond_12
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetSourceURL()Z

    move-result v1

    .line 848
    invoke-virtual {p1}, Lcom/evernote/edam/type/NoteAttributes;->isSetSourceURL()Z

    move-result v2

    if-nez v1, :cond_13

    if-eqz v2, :cond_15

    :cond_13
    if-eqz v1, :cond_46

    if-nez v2, :cond_14

    goto/16 :goto_c

    .line 852
    :cond_14
    iget-object v1, p0, Lcom/evernote/edam/type/NoteAttributes;->sourceURL:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/type/NoteAttributes;->sourceURL:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_15

    return v0

    .line 856
    :cond_15
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetSourceApplication()Z

    move-result v1

    .line 857
    invoke-virtual {p1}, Lcom/evernote/edam/type/NoteAttributes;->isSetSourceApplication()Z

    move-result v2

    if-nez v1, :cond_16

    if-eqz v2, :cond_18

    :cond_16
    if-eqz v1, :cond_45

    if-nez v2, :cond_17

    goto/16 :goto_b

    .line 861
    :cond_17
    iget-object v1, p0, Lcom/evernote/edam/type/NoteAttributes;->sourceApplication:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/type/NoteAttributes;->sourceApplication:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_18

    return v0

    .line 865
    :cond_18
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetShareDate()Z

    move-result v1

    .line 866
    invoke-virtual {p1}, Lcom/evernote/edam/type/NoteAttributes;->isSetShareDate()Z

    move-result v2

    if-nez v1, :cond_19

    if-eqz v2, :cond_1b

    :cond_19
    if-eqz v1, :cond_44

    if-nez v2, :cond_1a

    goto/16 :goto_a

    .line 870
    :cond_1a
    iget-wide v1, p0, Lcom/evernote/edam/type/NoteAttributes;->shareDate:J

    iget-wide v3, p1, Lcom/evernote/edam/type/NoteAttributes;->shareDate:J

    cmp-long v5, v1, v3

    if-eqz v5, :cond_1b

    return v0

    .line 874
    :cond_1b
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetReminderOrder()Z

    move-result v1

    .line 875
    invoke-virtual {p1}, Lcom/evernote/edam/type/NoteAttributes;->isSetReminderOrder()Z

    move-result v2

    if-nez v1, :cond_1c

    if-eqz v2, :cond_1e

    :cond_1c
    if-eqz v1, :cond_43

    if-nez v2, :cond_1d

    goto/16 :goto_9

    .line 879
    :cond_1d
    iget-wide v1, p0, Lcom/evernote/edam/type/NoteAttributes;->reminderOrder:J

    iget-wide v3, p1, Lcom/evernote/edam/type/NoteAttributes;->reminderOrder:J

    cmp-long v5, v1, v3

    if-eqz v5, :cond_1e

    return v0

    .line 883
    :cond_1e
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetReminderDoneTime()Z

    move-result v1

    .line 884
    invoke-virtual {p1}, Lcom/evernote/edam/type/NoteAttributes;->isSetReminderDoneTime()Z

    move-result v2

    if-nez v1, :cond_1f

    if-eqz v2, :cond_21

    :cond_1f
    if-eqz v1, :cond_42

    if-nez v2, :cond_20

    goto/16 :goto_8

    .line 888
    :cond_20
    iget-wide v1, p0, Lcom/evernote/edam/type/NoteAttributes;->reminderDoneTime:J

    iget-wide v3, p1, Lcom/evernote/edam/type/NoteAttributes;->reminderDoneTime:J

    cmp-long v5, v1, v3

    if-eqz v5, :cond_21

    return v0

    .line 892
    :cond_21
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetReminderTime()Z

    move-result v1

    .line 893
    invoke-virtual {p1}, Lcom/evernote/edam/type/NoteAttributes;->isSetReminderTime()Z

    move-result v2

    if-nez v1, :cond_22

    if-eqz v2, :cond_24

    :cond_22
    if-eqz v1, :cond_41

    if-nez v2, :cond_23

    goto/16 :goto_7

    .line 897
    :cond_23
    iget-wide v1, p0, Lcom/evernote/edam/type/NoteAttributes;->reminderTime:J

    iget-wide v3, p1, Lcom/evernote/edam/type/NoteAttributes;->reminderTime:J

    cmp-long v5, v1, v3

    if-eqz v5, :cond_24

    return v0

    .line 901
    :cond_24
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetPlaceName()Z

    move-result v1

    .line 902
    invoke-virtual {p1}, Lcom/evernote/edam/type/NoteAttributes;->isSetPlaceName()Z

    move-result v2

    if-nez v1, :cond_25

    if-eqz v2, :cond_27

    :cond_25
    if-eqz v1, :cond_40

    if-nez v2, :cond_26

    goto/16 :goto_6

    .line 906
    :cond_26
    iget-object v1, p0, Lcom/evernote/edam/type/NoteAttributes;->placeName:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/type/NoteAttributes;->placeName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_27

    return v0

    .line 910
    :cond_27
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetContentClass()Z

    move-result v1

    .line 911
    invoke-virtual {p1}, Lcom/evernote/edam/type/NoteAttributes;->isSetContentClass()Z

    move-result v2

    if-nez v1, :cond_28

    if-eqz v2, :cond_2a

    :cond_28
    if-eqz v1, :cond_3f

    if-nez v2, :cond_29

    goto/16 :goto_5

    .line 915
    :cond_29
    iget-object v1, p0, Lcom/evernote/edam/type/NoteAttributes;->contentClass:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/type/NoteAttributes;->contentClass:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2a

    return v0

    .line 919
    :cond_2a
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetApplicationData()Z

    move-result v1

    .line 920
    invoke-virtual {p1}, Lcom/evernote/edam/type/NoteAttributes;->isSetApplicationData()Z

    move-result v2

    if-nez v1, :cond_2b

    if-eqz v2, :cond_2d

    :cond_2b
    if-eqz v1, :cond_3e

    if-nez v2, :cond_2c

    goto/16 :goto_4

    .line 924
    :cond_2c
    iget-object v1, p0, Lcom/evernote/edam/type/NoteAttributes;->applicationData:Lcom/evernote/edam/type/LazyMap;

    iget-object v2, p1, Lcom/evernote/edam/type/NoteAttributes;->applicationData:Lcom/evernote/edam/type/LazyMap;

    invoke-virtual {v1, v2}, Lcom/evernote/edam/type/LazyMap;->equals(Lcom/evernote/edam/type/LazyMap;)Z

    move-result v1

    if-nez v1, :cond_2d

    return v0

    .line 928
    :cond_2d
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetLastEditedBy()Z

    move-result v1

    .line 929
    invoke-virtual {p1}, Lcom/evernote/edam/type/NoteAttributes;->isSetLastEditedBy()Z

    move-result v2

    if-nez v1, :cond_2e

    if-eqz v2, :cond_30

    :cond_2e
    if-eqz v1, :cond_3d

    if-nez v2, :cond_2f

    goto :goto_3

    .line 933
    :cond_2f
    iget-object v1, p0, Lcom/evernote/edam/type/NoteAttributes;->lastEditedBy:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/type/NoteAttributes;->lastEditedBy:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_30

    return v0

    .line 937
    :cond_30
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetClassifications()Z

    move-result v1

    .line 938
    invoke-virtual {p1}, Lcom/evernote/edam/type/NoteAttributes;->isSetClassifications()Z

    move-result v2

    if-nez v1, :cond_31

    if-eqz v2, :cond_33

    :cond_31
    if-eqz v1, :cond_3c

    if-nez v2, :cond_32

    goto :goto_2

    .line 942
    :cond_32
    iget-object v1, p0, Lcom/evernote/edam/type/NoteAttributes;->classifications:Ljava/util/Map;

    iget-object v2, p1, Lcom/evernote/edam/type/NoteAttributes;->classifications:Ljava/util/Map;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_33

    return v0

    .line 946
    :cond_33
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetCreatorId()Z

    move-result v1

    .line 947
    invoke-virtual {p1}, Lcom/evernote/edam/type/NoteAttributes;->isSetCreatorId()Z

    move-result v2

    if-nez v1, :cond_34

    if-eqz v2, :cond_36

    :cond_34
    if-eqz v1, :cond_3b

    if-nez v2, :cond_35

    goto :goto_1

    .line 951
    :cond_35
    iget v1, p0, Lcom/evernote/edam/type/NoteAttributes;->creatorId:I

    iget v2, p1, Lcom/evernote/edam/type/NoteAttributes;->creatorId:I

    if-eq v1, v2, :cond_36

    return v0

    .line 955
    :cond_36
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetLastEditorId()Z

    move-result v1

    .line 956
    invoke-virtual {p1}, Lcom/evernote/edam/type/NoteAttributes;->isSetLastEditorId()Z

    move-result v2

    if-nez v1, :cond_37

    if-eqz v2, :cond_39

    :cond_37
    if-eqz v1, :cond_3a

    if-nez v2, :cond_38

    goto :goto_0

    .line 960
    :cond_38
    iget v1, p0, Lcom/evernote/edam/type/NoteAttributes;->lastEditorId:I

    iget p1, p1, Lcom/evernote/edam/type/NoteAttributes;->lastEditorId:I

    if-eq v1, p1, :cond_39

    return v0

    :cond_39
    const/4 p1, 0x1

    return p1

    :cond_3a
    :goto_0
    return v0

    :cond_3b
    :goto_1
    return v0

    :cond_3c
    :goto_2
    return v0

    :cond_3d
    :goto_3
    return v0

    :cond_3e
    :goto_4
    return v0

    :cond_3f
    :goto_5
    return v0

    :cond_40
    :goto_6
    return v0

    :cond_41
    :goto_7
    return v0

    :cond_42
    :goto_8
    return v0

    :cond_43
    :goto_9
    return v0

    :cond_44
    :goto_a
    return v0

    :cond_45
    :goto_b
    return v0

    :cond_46
    :goto_c
    return v0

    :cond_47
    :goto_d
    return v0

    :cond_48
    :goto_e
    return v0

    :cond_49
    :goto_f
    return v0

    :cond_4a
    :goto_10
    return v0

    :cond_4b
    :goto_11
    return v0

    :cond_4c
    :goto_12
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 784
    :cond_0
    instance-of v1, p1, Lcom/evernote/edam/type/NoteAttributes;

    if-eqz v1, :cond_1

    .line 785
    check-cast p1, Lcom/evernote/edam/type/NoteAttributes;

    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/NoteAttributes;->equals(Lcom/evernote/edam/type/NoteAttributes;)Z

    move-result p1

    return p1

    :cond_1
    return v0
.end method

.method public getAltitude()D
    .locals 2

    .line 409
    iget-wide v0, p0, Lcom/evernote/edam/type/NoteAttributes;->altitude:D

    return-wide v0
.end method

.method public getApplicationData()Lcom/evernote/edam/type/LazyMap;
    .locals 1

    .line 657
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->applicationData:Lcom/evernote/edam/type/LazyMap;

    return-object v0
.end method

.method public getAuthor()Ljava/lang/String;
    .locals 1

    .line 431
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->author:Ljava/lang/String;

    return-object v0
.end method

.method public getClassifications()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 714
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->classifications:Ljava/util/Map;

    return-object v0
.end method

.method public getClassificationsSize()I
    .locals 1

    .line 703
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->classifications:Ljava/util/Map;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    :goto_0
    return v0
.end method

.method public getContentClass()Ljava/lang/String;
    .locals 1

    .line 634
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->contentClass:Ljava/lang/String;

    return-object v0
.end method

.method public getCreatorId()I
    .locals 1

    .line 737
    iget v0, p0, Lcom/evernote/edam/type/NoteAttributes;->creatorId:I

    return v0
.end method

.method public getLastEditedBy()Ljava/lang/String;
    .locals 1

    .line 680
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->lastEditedBy:Ljava/lang/String;

    return-object v0
.end method

.method public getLastEditorId()I
    .locals 1

    .line 759
    iget v0, p0, Lcom/evernote/edam/type/NoteAttributes;->lastEditorId:I

    return v0
.end method

.method public getLatitude()D
    .locals 2

    .line 365
    iget-wide v0, p0, Lcom/evernote/edam/type/NoteAttributes;->latitude:D

    return-wide v0
.end method

.method public getLongitude()D
    .locals 2

    .line 387
    iget-wide v0, p0, Lcom/evernote/edam/type/NoteAttributes;->longitude:D

    return-wide v0
.end method

.method public getPlaceName()Ljava/lang/String;
    .locals 1

    .line 611
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->placeName:Ljava/lang/String;

    return-object v0
.end method

.method public getReminderDoneTime()J
    .locals 2

    .line 567
    iget-wide v0, p0, Lcom/evernote/edam/type/NoteAttributes;->reminderDoneTime:J

    return-wide v0
.end method

.method public getReminderOrder()J
    .locals 2

    .line 545
    iget-wide v0, p0, Lcom/evernote/edam/type/NoteAttributes;->reminderOrder:J

    return-wide v0
.end method

.method public getReminderTime()J
    .locals 2

    .line 589
    iget-wide v0, p0, Lcom/evernote/edam/type/NoteAttributes;->reminderTime:J

    return-wide v0
.end method

.method public getShareDate()J
    .locals 2

    .line 523
    iget-wide v0, p0, Lcom/evernote/edam/type/NoteAttributes;->shareDate:J

    return-wide v0
.end method

.method public getSource()Ljava/lang/String;
    .locals 1

    .line 454
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->source:Ljava/lang/String;

    return-object v0
.end method

.method public getSourceApplication()Ljava/lang/String;
    .locals 1

    .line 500
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->sourceApplication:Ljava/lang/String;

    return-object v0
.end method

.method public getSourceURL()Ljava/lang/String;
    .locals 1

    .line 477
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->sourceURL:Ljava/lang/String;

    return-object v0
.end method

.method public getSubjectDate()J
    .locals 2

    .line 343
    iget-wide v0, p0, Lcom/evernote/edam/type/NoteAttributes;->subjectDate:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isSetAltitude()Z
    .locals 2

    .line 423
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->__isset_vector:[Z

    const/4 v1, 0x3

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetApplicationData()Z
    .locals 1

    .line 670
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->applicationData:Lcom/evernote/edam/type/LazyMap;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetAuthor()Z
    .locals 1

    .line 444
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->author:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetClassifications()Z
    .locals 1

    .line 727
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->classifications:Ljava/util/Map;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetContentClass()Z
    .locals 1

    .line 647
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->contentClass:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetCreatorId()Z
    .locals 2

    .line 751
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->__isset_vector:[Z

    const/16 v1, 0x8

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetLastEditedBy()Z
    .locals 1

    .line 693
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->lastEditedBy:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetLastEditorId()Z
    .locals 2

    .line 773
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->__isset_vector:[Z

    const/16 v1, 0x9

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetLatitude()Z
    .locals 2

    .line 379
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->__isset_vector:[Z

    const/4 v1, 0x1

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetLongitude()Z
    .locals 2

    .line 401
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->__isset_vector:[Z

    const/4 v1, 0x2

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetPlaceName()Z
    .locals 1

    .line 624
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->placeName:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetReminderDoneTime()Z
    .locals 2

    .line 581
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->__isset_vector:[Z

    const/4 v1, 0x6

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetReminderOrder()Z
    .locals 2

    .line 559
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->__isset_vector:[Z

    const/4 v1, 0x5

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetReminderTime()Z
    .locals 2

    .line 603
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->__isset_vector:[Z

    const/4 v1, 0x7

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetShareDate()Z
    .locals 2

    .line 537
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->__isset_vector:[Z

    const/4 v1, 0x4

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetSource()Z
    .locals 1

    .line 467
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->source:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetSourceApplication()Z
    .locals 1

    .line 513
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->sourceApplication:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetSourceURL()Z
    .locals 1

    .line 490
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->sourceURL:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetSubjectDate()Z
    .locals 2

    .line 357
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->__isset_vector:[Z

    const/4 v1, 0x0

    aget-boolean v0, v0, v1

    return v0
.end method

.method public putToClassifications(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 707
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->classifications:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 708
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->classifications:Ljava/util/Map;

    .line 710
    :cond_0
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->classifications:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public read(Lcom/evernote/thrift/protocol/TProtocol;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1156
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readStructBegin()Lcom/evernote/thrift/protocol/TStruct;

    .line 1159
    :goto_0
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readFieldBegin()Lcom/evernote/thrift/protocol/TField;

    move-result-object v0

    .line 1160
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-nez v1, :cond_0

    .line 1325
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readStructEnd()V

    .line 1326
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->validate()V

    return-void

    .line 1163
    :cond_0
    iget-short v1, v0, Lcom/evernote/thrift/protocol/TField;->id:S

    const/16 v2, 0xa

    const/4 v3, 0x1

    if-eq v1, v3, :cond_14

    const/4 v4, 0x4

    const/16 v5, 0xb

    packed-switch v1, :pswitch_data_0

    const/16 v2, 0x8

    packed-switch v1, :pswitch_data_1

    .line 1321
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_2

    .line 1313
    :pswitch_0
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v2, :cond_1

    .line 1314
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI32()I

    move-result v0

    iput v0, p0, Lcom/evernote/edam/type/NoteAttributes;->lastEditorId:I

    .line 1315
    invoke-virtual {p0, v3}, Lcom/evernote/edam/type/NoteAttributes;->setLastEditorIdIsSet(Z)V

    goto/16 :goto_2

    .line 1317
    :cond_1
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_2

    .line 1305
    :pswitch_1
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v2, :cond_2

    .line 1306
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI32()I

    move-result v0

    iput v0, p0, Lcom/evernote/edam/type/NoteAttributes;->creatorId:I

    .line 1307
    invoke-virtual {p0, v3}, Lcom/evernote/edam/type/NoteAttributes;->setCreatorIdIsSet(Z)V

    goto/16 :goto_2

    .line 1309
    :cond_2
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_2

    .line 1286
    :pswitch_2
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    const/16 v2, 0xd

    if-ne v1, v2, :cond_4

    .line 1288
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readMapBegin()Lcom/evernote/thrift/protocol/TMap;

    move-result-object v0

    .line 1289
    new-instance v1, Ljava/util/HashMap;

    iget v2, v0, Lcom/evernote/thrift/protocol/TMap;->size:I

    mul-int/lit8 v2, v2, 0x2

    invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(I)V

    iput-object v1, p0, Lcom/evernote/edam/type/NoteAttributes;->classifications:Ljava/util/Map;

    const/4 v1, 0x0

    .line 1290
    :goto_1
    iget v2, v0, Lcom/evernote/thrift/protocol/TMap;->size:I

    if-ge v1, v2, :cond_3

    .line 1294
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v2

    .line 1295
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v3

    .line 1296
    iget-object v4, p0, Lcom/evernote/edam/type/NoteAttributes;->classifications:Ljava/util/Map;

    invoke-interface {v4, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1298
    :cond_3
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readMapEnd()V

    goto/16 :goto_2

    .line 1301
    :cond_4
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_2

    .line 1279
    :pswitch_3
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v5, :cond_5

    .line 1280
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->lastEditedBy:Ljava/lang/String;

    goto/16 :goto_2

    .line 1282
    :cond_5
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_2

    .line 1271
    :pswitch_4
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    const/16 v2, 0xc

    if-ne v1, v2, :cond_6

    .line 1272
    new-instance v0, Lcom/evernote/edam/type/LazyMap;

    invoke-direct {v0}, Lcom/evernote/edam/type/LazyMap;-><init>()V

    iput-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->applicationData:Lcom/evernote/edam/type/LazyMap;

    .line 1273
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->applicationData:Lcom/evernote/edam/type/LazyMap;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/type/LazyMap;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    goto/16 :goto_2

    .line 1275
    :cond_6
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_2

    .line 1264
    :pswitch_5
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v5, :cond_7

    .line 1265
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->contentClass:Ljava/lang/String;

    goto/16 :goto_2

    .line 1267
    :cond_7
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_2

    .line 1257
    :pswitch_6
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v5, :cond_8

    .line 1258
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->placeName:Ljava/lang/String;

    goto/16 :goto_2

    .line 1260
    :cond_8
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_2

    .line 1249
    :pswitch_7
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v2, :cond_9

    .line 1250
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/evernote/edam/type/NoteAttributes;->reminderTime:J

    .line 1251
    invoke-virtual {p0, v3}, Lcom/evernote/edam/type/NoteAttributes;->setReminderTimeIsSet(Z)V

    goto/16 :goto_2

    .line 1253
    :cond_9
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_2

    .line 1241
    :pswitch_8
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v2, :cond_a

    .line 1242
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/evernote/edam/type/NoteAttributes;->reminderDoneTime:J

    .line 1243
    invoke-virtual {p0, v3}, Lcom/evernote/edam/type/NoteAttributes;->setReminderDoneTimeIsSet(Z)V

    goto/16 :goto_2

    .line 1245
    :cond_a
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_2

    .line 1233
    :pswitch_9
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v2, :cond_b

    .line 1234
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/evernote/edam/type/NoteAttributes;->reminderOrder:J

    .line 1235
    invoke-virtual {p0, v3}, Lcom/evernote/edam/type/NoteAttributes;->setReminderOrderIsSet(Z)V

    goto/16 :goto_2

    .line 1237
    :cond_b
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_2

    .line 1225
    :pswitch_a
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v2, :cond_c

    .line 1226
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/evernote/edam/type/NoteAttributes;->shareDate:J

    .line 1227
    invoke-virtual {p0, v3}, Lcom/evernote/edam/type/NoteAttributes;->setShareDateIsSet(Z)V

    goto/16 :goto_2

    .line 1229
    :cond_c
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_2

    .line 1218
    :pswitch_b
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v5, :cond_d

    .line 1219
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->sourceApplication:Ljava/lang/String;

    goto/16 :goto_2

    .line 1221
    :cond_d
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_2

    .line 1211
    :pswitch_c
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v5, :cond_e

    .line 1212
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->sourceURL:Ljava/lang/String;

    goto/16 :goto_2

    .line 1214
    :cond_e
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_2

    .line 1204
    :pswitch_d
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v5, :cond_f

    .line 1205
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->source:Ljava/lang/String;

    goto/16 :goto_2

    .line 1207
    :cond_f
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_2

    .line 1197
    :pswitch_e
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v5, :cond_10

    .line 1198
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->author:Ljava/lang/String;

    goto :goto_2

    .line 1200
    :cond_10
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_2

    .line 1189
    :pswitch_f
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_11

    .line 1190
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/evernote/edam/type/NoteAttributes;->altitude:D

    .line 1191
    invoke-virtual {p0, v3}, Lcom/evernote/edam/type/NoteAttributes;->setAltitudeIsSet(Z)V

    goto :goto_2

    .line 1193
    :cond_11
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_2

    .line 1181
    :pswitch_10
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_12

    .line 1182
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/evernote/edam/type/NoteAttributes;->longitude:D

    .line 1183
    invoke-virtual {p0, v3}, Lcom/evernote/edam/type/NoteAttributes;->setLongitudeIsSet(Z)V

    goto :goto_2

    .line 1185
    :cond_12
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_2

    .line 1173
    :pswitch_11
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_13

    .line 1174
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/evernote/edam/type/NoteAttributes;->latitude:D

    .line 1175
    invoke-virtual {p0, v3}, Lcom/evernote/edam/type/NoteAttributes;->setLatitudeIsSet(Z)V

    goto :goto_2

    .line 1177
    :cond_13
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_2

    .line 1165
    :cond_14
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v2, :cond_15

    .line 1166
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/evernote/edam/type/NoteAttributes;->subjectDate:J

    .line 1167
    invoke-virtual {p0, v3}, Lcom/evernote/edam/type/NoteAttributes;->setSubjectDateIsSet(Z)V

    goto :goto_2

    .line 1169
    :cond_15
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    .line 1323
    :goto_2
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readFieldEnd()V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1a
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setAltitude(D)V
    .locals 0

    .line 413
    iput-wide p1, p0, Lcom/evernote/edam/type/NoteAttributes;->altitude:D

    const/4 p1, 0x1

    .line 414
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/NoteAttributes;->setAltitudeIsSet(Z)V

    return-void
.end method

.method public setAltitudeIsSet(Z)V
    .locals 2

    .line 427
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->__isset_vector:[Z

    const/4 v1, 0x3

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setApplicationData(Lcom/evernote/edam/type/LazyMap;)V
    .locals 0

    .line 661
    iput-object p1, p0, Lcom/evernote/edam/type/NoteAttributes;->applicationData:Lcom/evernote/edam/type/LazyMap;

    return-void
.end method

.method public setApplicationDataIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 675
    iput-object p1, p0, Lcom/evernote/edam/type/NoteAttributes;->applicationData:Lcom/evernote/edam/type/LazyMap;

    :cond_0
    return-void
.end method

.method public setAuthor(Ljava/lang/String;)V
    .locals 0

    .line 435
    iput-object p1, p0, Lcom/evernote/edam/type/NoteAttributes;->author:Ljava/lang/String;

    return-void
.end method

.method public setAuthorIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 449
    iput-object p1, p0, Lcom/evernote/edam/type/NoteAttributes;->author:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setClassifications(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 718
    iput-object p1, p0, Lcom/evernote/edam/type/NoteAttributes;->classifications:Ljava/util/Map;

    return-void
.end method

.method public setClassificationsIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 732
    iput-object p1, p0, Lcom/evernote/edam/type/NoteAttributes;->classifications:Ljava/util/Map;

    :cond_0
    return-void
.end method

.method public setContentClass(Ljava/lang/String;)V
    .locals 0

    .line 638
    iput-object p1, p0, Lcom/evernote/edam/type/NoteAttributes;->contentClass:Ljava/lang/String;

    return-void
.end method

.method public setContentClassIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 652
    iput-object p1, p0, Lcom/evernote/edam/type/NoteAttributes;->contentClass:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setCreatorId(I)V
    .locals 0

    .line 741
    iput p1, p0, Lcom/evernote/edam/type/NoteAttributes;->creatorId:I

    const/4 p1, 0x1

    .line 742
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/NoteAttributes;->setCreatorIdIsSet(Z)V

    return-void
.end method

.method public setCreatorIdIsSet(Z)V
    .locals 2

    .line 755
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->__isset_vector:[Z

    const/16 v1, 0x8

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setLastEditedBy(Ljava/lang/String;)V
    .locals 0

    .line 684
    iput-object p1, p0, Lcom/evernote/edam/type/NoteAttributes;->lastEditedBy:Ljava/lang/String;

    return-void
.end method

.method public setLastEditedByIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 698
    iput-object p1, p0, Lcom/evernote/edam/type/NoteAttributes;->lastEditedBy:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setLastEditorId(I)V
    .locals 0

    .line 763
    iput p1, p0, Lcom/evernote/edam/type/NoteAttributes;->lastEditorId:I

    const/4 p1, 0x1

    .line 764
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/NoteAttributes;->setLastEditorIdIsSet(Z)V

    return-void
.end method

.method public setLastEditorIdIsSet(Z)V
    .locals 2

    .line 777
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->__isset_vector:[Z

    const/16 v1, 0x9

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setLatitude(D)V
    .locals 0

    .line 369
    iput-wide p1, p0, Lcom/evernote/edam/type/NoteAttributes;->latitude:D

    const/4 p1, 0x1

    .line 370
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/NoteAttributes;->setLatitudeIsSet(Z)V

    return-void
.end method

.method public setLatitudeIsSet(Z)V
    .locals 2

    .line 383
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->__isset_vector:[Z

    const/4 v1, 0x1

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setLongitude(D)V
    .locals 0

    .line 391
    iput-wide p1, p0, Lcom/evernote/edam/type/NoteAttributes;->longitude:D

    const/4 p1, 0x1

    .line 392
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/NoteAttributes;->setLongitudeIsSet(Z)V

    return-void
.end method

.method public setLongitudeIsSet(Z)V
    .locals 2

    .line 405
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->__isset_vector:[Z

    const/4 v1, 0x2

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setPlaceName(Ljava/lang/String;)V
    .locals 0

    .line 615
    iput-object p1, p0, Lcom/evernote/edam/type/NoteAttributes;->placeName:Ljava/lang/String;

    return-void
.end method

.method public setPlaceNameIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 629
    iput-object p1, p0, Lcom/evernote/edam/type/NoteAttributes;->placeName:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setReminderDoneTime(J)V
    .locals 0

    .line 571
    iput-wide p1, p0, Lcom/evernote/edam/type/NoteAttributes;->reminderDoneTime:J

    const/4 p1, 0x1

    .line 572
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/NoteAttributes;->setReminderDoneTimeIsSet(Z)V

    return-void
.end method

.method public setReminderDoneTimeIsSet(Z)V
    .locals 2

    .line 585
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->__isset_vector:[Z

    const/4 v1, 0x6

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setReminderOrder(J)V
    .locals 0

    .line 549
    iput-wide p1, p0, Lcom/evernote/edam/type/NoteAttributes;->reminderOrder:J

    const/4 p1, 0x1

    .line 550
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/NoteAttributes;->setReminderOrderIsSet(Z)V

    return-void
.end method

.method public setReminderOrderIsSet(Z)V
    .locals 2

    .line 563
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->__isset_vector:[Z

    const/4 v1, 0x5

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setReminderTime(J)V
    .locals 0

    .line 593
    iput-wide p1, p0, Lcom/evernote/edam/type/NoteAttributes;->reminderTime:J

    const/4 p1, 0x1

    .line 594
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/NoteAttributes;->setReminderTimeIsSet(Z)V

    return-void
.end method

.method public setReminderTimeIsSet(Z)V
    .locals 2

    .line 607
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->__isset_vector:[Z

    const/4 v1, 0x7

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setShareDate(J)V
    .locals 0

    .line 527
    iput-wide p1, p0, Lcom/evernote/edam/type/NoteAttributes;->shareDate:J

    const/4 p1, 0x1

    .line 528
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/NoteAttributes;->setShareDateIsSet(Z)V

    return-void
.end method

.method public setShareDateIsSet(Z)V
    .locals 2

    .line 541
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->__isset_vector:[Z

    const/4 v1, 0x4

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setSource(Ljava/lang/String;)V
    .locals 0

    .line 458
    iput-object p1, p0, Lcom/evernote/edam/type/NoteAttributes;->source:Ljava/lang/String;

    return-void
.end method

.method public setSourceApplication(Ljava/lang/String;)V
    .locals 0

    .line 504
    iput-object p1, p0, Lcom/evernote/edam/type/NoteAttributes;->sourceApplication:Ljava/lang/String;

    return-void
.end method

.method public setSourceApplicationIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 518
    iput-object p1, p0, Lcom/evernote/edam/type/NoteAttributes;->sourceApplication:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setSourceIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 472
    iput-object p1, p0, Lcom/evernote/edam/type/NoteAttributes;->source:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setSourceURL(Ljava/lang/String;)V
    .locals 0

    .line 481
    iput-object p1, p0, Lcom/evernote/edam/type/NoteAttributes;->sourceURL:Ljava/lang/String;

    return-void
.end method

.method public setSourceURLIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 495
    iput-object p1, p0, Lcom/evernote/edam/type/NoteAttributes;->sourceURL:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setSubjectDate(J)V
    .locals 0

    .line 347
    iput-wide p1, p0, Lcom/evernote/edam/type/NoteAttributes;->subjectDate:J

    const/4 p1, 0x1

    .line 348
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/NoteAttributes;->setSubjectDateIsSet(Z)V

    return-void
.end method

.method public setSubjectDateIsSet(Z)V
    .locals 2

    .line 361
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->__isset_vector:[Z

    const/4 v1, 0x0

    aput-boolean p1, v0, v1

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .line 1460
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NoteAttributes("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1463
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetSubjectDate()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    const-string v1, "subjectDate:"

    .line 1464
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1465
    iget-wide v3, p0, Lcom/evernote/edam/type/NoteAttributes;->subjectDate:J

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    const/4 v1, 0x1

    .line 1468
    :goto_0
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetLatitude()Z

    move-result v3

    if-eqz v3, :cond_2

    if-nez v1, :cond_1

    const-string v1, ", "

    .line 1469
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const-string v1, "latitude:"

    .line 1470
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1471
    iget-wide v3, p0, Lcom/evernote/edam/type/NoteAttributes;->latitude:D

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1474
    :cond_2
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetLongitude()Z

    move-result v3

    if-eqz v3, :cond_4

    if-nez v1, :cond_3

    const-string v1, ", "

    .line 1475
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    const-string v1, "longitude:"

    .line 1476
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1477
    iget-wide v3, p0, Lcom/evernote/edam/type/NoteAttributes;->longitude:D

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1480
    :cond_4
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetAltitude()Z

    move-result v3

    if-eqz v3, :cond_6

    if-nez v1, :cond_5

    const-string v1, ", "

    .line 1481
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    const-string v1, "altitude:"

    .line 1482
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1483
    iget-wide v3, p0, Lcom/evernote/edam/type/NoteAttributes;->altitude:D

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1486
    :cond_6
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetAuthor()Z

    move-result v3

    if-eqz v3, :cond_9

    if-nez v1, :cond_7

    const-string v1, ", "

    .line 1487
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_7
    const-string v1, "author:"

    .line 1488
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1489
    iget-object v1, p0, Lcom/evernote/edam/type/NoteAttributes;->author:Ljava/lang/String;

    if-nez v1, :cond_8

    const-string v1, "null"

    .line 1490
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1492
    :cond_8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_1
    const/4 v1, 0x0

    .line 1496
    :cond_9
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetSource()Z

    move-result v3

    if-eqz v3, :cond_c

    if-nez v1, :cond_a

    const-string v1, ", "

    .line 1497
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_a
    const-string v1, "source:"

    .line 1498
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1499
    iget-object v1, p0, Lcom/evernote/edam/type/NoteAttributes;->source:Ljava/lang/String;

    if-nez v1, :cond_b

    const-string v1, "null"

    .line 1500
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 1502
    :cond_b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_2
    const/4 v1, 0x0

    .line 1506
    :cond_c
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetSourceURL()Z

    move-result v3

    if-eqz v3, :cond_f

    if-nez v1, :cond_d

    const-string v1, ", "

    .line 1507
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_d
    const-string v1, "sourceURL:"

    .line 1508
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1509
    iget-object v1, p0, Lcom/evernote/edam/type/NoteAttributes;->sourceURL:Ljava/lang/String;

    if-nez v1, :cond_e

    const-string v1, "null"

    .line 1510
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 1512
    :cond_e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    const/4 v1, 0x0

    .line 1516
    :cond_f
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetSourceApplication()Z

    move-result v3

    if-eqz v3, :cond_12

    if-nez v1, :cond_10

    const-string v1, ", "

    .line 1517
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_10
    const-string v1, "sourceApplication:"

    .line 1518
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1519
    iget-object v1, p0, Lcom/evernote/edam/type/NoteAttributes;->sourceApplication:Ljava/lang/String;

    if-nez v1, :cond_11

    const-string v1, "null"

    .line 1520
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 1522
    :cond_11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_4
    const/4 v1, 0x0

    .line 1526
    :cond_12
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetShareDate()Z

    move-result v3

    if-eqz v3, :cond_14

    if-nez v1, :cond_13

    const-string v1, ", "

    .line 1527
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_13
    const-string v1, "shareDate:"

    .line 1528
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1529
    iget-wide v3, p0, Lcom/evernote/edam/type/NoteAttributes;->shareDate:J

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1532
    :cond_14
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetReminderOrder()Z

    move-result v3

    if-eqz v3, :cond_16

    if-nez v1, :cond_15

    const-string v1, ", "

    .line 1533
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_15
    const-string v1, "reminderOrder:"

    .line 1534
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1535
    iget-wide v3, p0, Lcom/evernote/edam/type/NoteAttributes;->reminderOrder:J

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1538
    :cond_16
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetReminderDoneTime()Z

    move-result v3

    if-eqz v3, :cond_18

    if-nez v1, :cond_17

    const-string v1, ", "

    .line 1539
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_17
    const-string v1, "reminderDoneTime:"

    .line 1540
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1541
    iget-wide v3, p0, Lcom/evernote/edam/type/NoteAttributes;->reminderDoneTime:J

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1544
    :cond_18
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetReminderTime()Z

    move-result v3

    if-eqz v3, :cond_1a

    if-nez v1, :cond_19

    const-string v1, ", "

    .line 1545
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_19
    const-string v1, "reminderTime:"

    .line 1546
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1547
    iget-wide v3, p0, Lcom/evernote/edam/type/NoteAttributes;->reminderTime:J

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1550
    :cond_1a
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetPlaceName()Z

    move-result v3

    if-eqz v3, :cond_1d

    if-nez v1, :cond_1b

    const-string v1, ", "

    .line 1551
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1b
    const-string v1, "placeName:"

    .line 1552
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1553
    iget-object v1, p0, Lcom/evernote/edam/type/NoteAttributes;->placeName:Ljava/lang/String;

    if-nez v1, :cond_1c

    const-string v1, "null"

    .line 1554
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5

    .line 1556
    :cond_1c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_5
    const/4 v1, 0x0

    .line 1560
    :cond_1d
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetContentClass()Z

    move-result v3

    if-eqz v3, :cond_20

    if-nez v1, :cond_1e

    const-string v1, ", "

    .line 1561
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1e
    const-string v1, "contentClass:"

    .line 1562
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1563
    iget-object v1, p0, Lcom/evernote/edam/type/NoteAttributes;->contentClass:Ljava/lang/String;

    if-nez v1, :cond_1f

    const-string v1, "null"

    .line 1564
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6

    .line 1566
    :cond_1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_6
    const/4 v1, 0x0

    .line 1570
    :cond_20
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetApplicationData()Z

    move-result v3

    if-eqz v3, :cond_23

    if-nez v1, :cond_21

    const-string v1, ", "

    .line 1571
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_21
    const-string v1, "applicationData:"

    .line 1572
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1573
    iget-object v1, p0, Lcom/evernote/edam/type/NoteAttributes;->applicationData:Lcom/evernote/edam/type/LazyMap;

    if-nez v1, :cond_22

    const-string v1, "null"

    .line 1574
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_7

    .line 1576
    :cond_22
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :goto_7
    const/4 v1, 0x0

    .line 1580
    :cond_23
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetLastEditedBy()Z

    move-result v3

    if-eqz v3, :cond_26

    if-nez v1, :cond_24

    const-string v1, ", "

    .line 1581
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_24
    const-string v1, "lastEditedBy:"

    .line 1582
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1583
    iget-object v1, p0, Lcom/evernote/edam/type/NoteAttributes;->lastEditedBy:Ljava/lang/String;

    if-nez v1, :cond_25

    const-string v1, "null"

    .line 1584
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_8

    .line 1586
    :cond_25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_8
    const/4 v1, 0x0

    .line 1590
    :cond_26
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetClassifications()Z

    move-result v3

    if-eqz v3, :cond_29

    if-nez v1, :cond_27

    const-string v1, ", "

    .line 1591
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_27
    const-string v1, "classifications:"

    .line 1592
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1593
    iget-object v1, p0, Lcom/evernote/edam/type/NoteAttributes;->classifications:Ljava/util/Map;

    if-nez v1, :cond_28

    const-string v1, "null"

    .line 1594
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_9

    .line 1596
    :cond_28
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :goto_9
    const/4 v1, 0x0

    .line 1600
    :cond_29
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetCreatorId()Z

    move-result v3

    if-eqz v3, :cond_2b

    if-nez v1, :cond_2a

    const-string v1, ", "

    .line 1601
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2a
    const-string v1, "creatorId:"

    .line 1602
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1603
    iget v1, p0, Lcom/evernote/edam/type/NoteAttributes;->creatorId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 1606
    :cond_2b
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetLastEditorId()Z

    move-result v2

    if-eqz v2, :cond_2d

    if-nez v1, :cond_2c

    const-string v1, ", "

    .line 1607
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2c
    const-string v1, "lastEditorId:"

    .line 1608
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1609
    iget v1, p0, Lcom/evernote/edam/type/NoteAttributes;->lastEditorId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_2d
    const-string v1, ")"

    .line 1612
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1613
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public unsetAltitude()V
    .locals 3

    .line 418
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->__isset_vector:[Z

    const/4 v1, 0x3

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetApplicationData()V
    .locals 1

    const/4 v0, 0x0

    .line 665
    iput-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->applicationData:Lcom/evernote/edam/type/LazyMap;

    return-void
.end method

.method public unsetAuthor()V
    .locals 1

    const/4 v0, 0x0

    .line 439
    iput-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->author:Ljava/lang/String;

    return-void
.end method

.method public unsetClassifications()V
    .locals 1

    const/4 v0, 0x0

    .line 722
    iput-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->classifications:Ljava/util/Map;

    return-void
.end method

.method public unsetContentClass()V
    .locals 1

    const/4 v0, 0x0

    .line 642
    iput-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->contentClass:Ljava/lang/String;

    return-void
.end method

.method public unsetCreatorId()V
    .locals 3

    .line 746
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->__isset_vector:[Z

    const/16 v1, 0x8

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetLastEditedBy()V
    .locals 1

    const/4 v0, 0x0

    .line 688
    iput-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->lastEditedBy:Ljava/lang/String;

    return-void
.end method

.method public unsetLastEditorId()V
    .locals 3

    .line 768
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->__isset_vector:[Z

    const/16 v1, 0x9

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetLatitude()V
    .locals 3

    .line 374
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->__isset_vector:[Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetLongitude()V
    .locals 3

    .line 396
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->__isset_vector:[Z

    const/4 v1, 0x2

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetPlaceName()V
    .locals 1

    const/4 v0, 0x0

    .line 619
    iput-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->placeName:Ljava/lang/String;

    return-void
.end method

.method public unsetReminderDoneTime()V
    .locals 3

    .line 576
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->__isset_vector:[Z

    const/4 v1, 0x6

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetReminderOrder()V
    .locals 3

    .line 554
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->__isset_vector:[Z

    const/4 v1, 0x5

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetReminderTime()V
    .locals 3

    .line 598
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->__isset_vector:[Z

    const/4 v1, 0x7

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetShareDate()V
    .locals 3

    .line 532
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->__isset_vector:[Z

    const/4 v1, 0x4

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetSource()V
    .locals 1

    const/4 v0, 0x0

    .line 462
    iput-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->source:Ljava/lang/String;

    return-void
.end method

.method public unsetSourceApplication()V
    .locals 1

    const/4 v0, 0x0

    .line 508
    iput-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->sourceApplication:Ljava/lang/String;

    return-void
.end method

.method public unsetSourceURL()V
    .locals 1

    const/4 v0, 0x0

    .line 485
    iput-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->sourceURL:Ljava/lang/String;

    return-void
.end method

.method public unsetSubjectDate()V
    .locals 2

    .line 352
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->__isset_vector:[Z

    const/4 v1, 0x0

    aput-boolean v1, v0, v1

    return-void
.end method

.method public validate()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    return-void
.end method

.method public write(Lcom/evernote/thrift/protocol/TProtocol;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1330
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->validate()V

    .line 1332
    sget-object v0, Lcom/evernote/edam/type/NoteAttributes;->STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeStructBegin(Lcom/evernote/thrift/protocol/TStruct;)V

    .line 1333
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetSubjectDate()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1334
    sget-object v0, Lcom/evernote/edam/type/NoteAttributes;->SUBJECT_DATE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1335
    iget-wide v0, p0, Lcom/evernote/edam/type/NoteAttributes;->subjectDate:J

    invoke-virtual {p1, v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeI64(J)V

    .line 1336
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1338
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetLatitude()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1339
    sget-object v0, Lcom/evernote/edam/type/NoteAttributes;->LATITUDE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1340
    iget-wide v0, p0, Lcom/evernote/edam/type/NoteAttributes;->latitude:D

    invoke-virtual {p1, v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeDouble(D)V

    .line 1341
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1343
    :cond_1
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetLongitude()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1344
    sget-object v0, Lcom/evernote/edam/type/NoteAttributes;->LONGITUDE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1345
    iget-wide v0, p0, Lcom/evernote/edam/type/NoteAttributes;->longitude:D

    invoke-virtual {p1, v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeDouble(D)V

    .line 1346
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1348
    :cond_2
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetAltitude()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1349
    sget-object v0, Lcom/evernote/edam/type/NoteAttributes;->ALTITUDE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1350
    iget-wide v0, p0, Lcom/evernote/edam/type/NoteAttributes;->altitude:D

    invoke-virtual {p1, v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeDouble(D)V

    .line 1351
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1353
    :cond_3
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->author:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 1354
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetAuthor()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1355
    sget-object v0, Lcom/evernote/edam/type/NoteAttributes;->AUTHOR_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1356
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->author:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 1357
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1360
    :cond_4
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->source:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 1361
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetSource()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1362
    sget-object v0, Lcom/evernote/edam/type/NoteAttributes;->SOURCE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1363
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->source:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 1364
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1367
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->sourceURL:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 1368
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetSourceURL()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1369
    sget-object v0, Lcom/evernote/edam/type/NoteAttributes;->SOURCE_URL_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1370
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->sourceURL:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 1371
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1374
    :cond_6
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->sourceApplication:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 1375
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetSourceApplication()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1376
    sget-object v0, Lcom/evernote/edam/type/NoteAttributes;->SOURCE_APPLICATION_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1377
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->sourceApplication:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 1378
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1381
    :cond_7
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetShareDate()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1382
    sget-object v0, Lcom/evernote/edam/type/NoteAttributes;->SHARE_DATE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1383
    iget-wide v0, p0, Lcom/evernote/edam/type/NoteAttributes;->shareDate:J

    invoke-virtual {p1, v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeI64(J)V

    .line 1384
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1386
    :cond_8
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetReminderOrder()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1387
    sget-object v0, Lcom/evernote/edam/type/NoteAttributes;->REMINDER_ORDER_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1388
    iget-wide v0, p0, Lcom/evernote/edam/type/NoteAttributes;->reminderOrder:J

    invoke-virtual {p1, v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeI64(J)V

    .line 1389
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1391
    :cond_9
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetReminderDoneTime()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1392
    sget-object v0, Lcom/evernote/edam/type/NoteAttributes;->REMINDER_DONE_TIME_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1393
    iget-wide v0, p0, Lcom/evernote/edam/type/NoteAttributes;->reminderDoneTime:J

    invoke-virtual {p1, v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeI64(J)V

    .line 1394
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1396
    :cond_a
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetReminderTime()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1397
    sget-object v0, Lcom/evernote/edam/type/NoteAttributes;->REMINDER_TIME_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1398
    iget-wide v0, p0, Lcom/evernote/edam/type/NoteAttributes;->reminderTime:J

    invoke-virtual {p1, v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeI64(J)V

    .line 1399
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1401
    :cond_b
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->placeName:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 1402
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetPlaceName()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1403
    sget-object v0, Lcom/evernote/edam/type/NoteAttributes;->PLACE_NAME_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1404
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->placeName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 1405
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1408
    :cond_c
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->contentClass:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 1409
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetContentClass()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1410
    sget-object v0, Lcom/evernote/edam/type/NoteAttributes;->CONTENT_CLASS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1411
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->contentClass:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 1412
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1415
    :cond_d
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->applicationData:Lcom/evernote/edam/type/LazyMap;

    if-eqz v0, :cond_e

    .line 1416
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetApplicationData()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1417
    sget-object v0, Lcom/evernote/edam/type/NoteAttributes;->APPLICATION_DATA_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1418
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->applicationData:Lcom/evernote/edam/type/LazyMap;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/type/LazyMap;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1419
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1422
    :cond_e
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->lastEditedBy:Ljava/lang/String;

    if-eqz v0, :cond_f

    .line 1423
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetLastEditedBy()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1424
    sget-object v0, Lcom/evernote/edam/type/NoteAttributes;->LAST_EDITED_BY_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1425
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->lastEditedBy:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 1426
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1429
    :cond_f
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->classifications:Ljava/util/Map;

    if-eqz v0, :cond_11

    .line 1430
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetClassifications()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 1431
    sget-object v0, Lcom/evernote/edam/type/NoteAttributes;->CLASSIFICATIONS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1433
    new-instance v0, Lcom/evernote/thrift/protocol/TMap;

    iget-object v1, p0, Lcom/evernote/edam/type/NoteAttributes;->classifications:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    const/16 v2, 0xb

    invoke-direct {v0, v2, v2, v1}, Lcom/evernote/thrift/protocol/TMap;-><init>(BBI)V

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeMapBegin(Lcom/evernote/thrift/protocol/TMap;)V

    .line 1434
    iget-object v0, p0, Lcom/evernote/edam/type/NoteAttributes;->classifications:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_10

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 1436
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {p1, v2}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 1437
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 1439
    :cond_10
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMapEnd()V

    .line 1441
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1444
    :cond_11
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetCreatorId()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 1445
    sget-object v0, Lcom/evernote/edam/type/NoteAttributes;->CREATOR_ID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1446
    iget v0, p0, Lcom/evernote/edam/type/NoteAttributes;->creatorId:I

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeI32(I)V

    .line 1447
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1449
    :cond_12
    invoke-virtual {p0}, Lcom/evernote/edam/type/NoteAttributes;->isSetLastEditorId()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 1450
    sget-object v0, Lcom/evernote/edam/type/NoteAttributes;->LAST_EDITOR_ID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1451
    iget v0, p0, Lcom/evernote/edam/type/NoteAttributes;->lastEditorId:I

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeI32(I)V

    .line 1452
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1454
    :cond_13
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldStop()V

    .line 1455
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeStructEnd()V

    return-void
.end method

.class public Lcom/evernote/edam/type/SharedNotebookRecipientSettings;
.super Ljava/lang/Object;
.source "SharedNotebookRecipientSettings.java"

# interfaces
.implements Lcom/evernote/thrift/TBase;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/evernote/thrift/TBase<",
        "Lcom/evernote/edam/type/SharedNotebookRecipientSettings;",
        ">;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# static fields
.field private static final REMINDER_NOTIFY_EMAIL_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final REMINDER_NOTIFY_IN_APP_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

.field private static final __REMINDERNOTIFYEMAIL_ISSET_ID:I = 0x0

.field private static final __REMINDERNOTIFYINAPP_ISSET_ID:I = 0x1


# instance fields
.field private __isset_vector:[Z

.field private reminderNotifyEmail:Z

.field private reminderNotifyInApp:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 47
    new-instance v0, Lcom/evernote/thrift/protocol/TStruct;

    const-string v1, "SharedNotebookRecipientSettings"

    invoke-direct {v0, v1}, Lcom/evernote/thrift/protocol/TStruct;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

    .line 49
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "reminderNotifyEmail"

    const/4 v2, 0x2

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->REMINDER_NOTIFY_EMAIL_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 50
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "reminderNotifyInApp"

    invoke-direct {v0, v1, v2, v2}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->REMINDER_NOTIFY_IN_APP_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    .line 59
    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->__isset_vector:[Z

    return-void
.end method

.method public constructor <init>(Lcom/evernote/edam/type/SharedNotebookRecipientSettings;)V
    .locals 4

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    .line 59
    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->__isset_vector:[Z

    .line 68
    iget-object v0, p1, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->__isset_vector:[Z

    iget-object v1, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->__isset_vector:[Z

    array-length v2, v0

    const/4 v3, 0x0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 69
    iget-boolean v0, p1, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->reminderNotifyEmail:Z

    iput-boolean v0, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->reminderNotifyEmail:Z

    .line 70
    iget-boolean p1, p1, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->reminderNotifyInApp:Z

    iput-boolean p1, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->reminderNotifyInApp:Z

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    const/4 v0, 0x0

    .line 78
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->setReminderNotifyEmailIsSet(Z)V

    .line 79
    iput-boolean v0, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->reminderNotifyEmail:Z

    .line 80
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->setReminderNotifyInAppIsSet(Z)V

    .line 81
    iput-boolean v0, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->reminderNotifyInApp:Z

    return-void
.end method

.method public compareTo(Lcom/evernote/edam/type/SharedNotebookRecipientSettings;)I
    .locals 2

    .line 168
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 169
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    return p1

    .line 175
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->isSetReminderNotifyEmail()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->isSetReminderNotifyEmail()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_1

    return v0

    .line 179
    :cond_1
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->isSetReminderNotifyEmail()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->reminderNotifyEmail:Z

    iget-boolean v1, p1, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->reminderNotifyEmail:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_2

    return v0

    .line 184
    :cond_2
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->isSetReminderNotifyInApp()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->isSetReminderNotifyInApp()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_3

    return v0

    .line 188
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->isSetReminderNotifyInApp()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->reminderNotifyInApp:Z

    iget-boolean p1, p1, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->reminderNotifyInApp:Z

    invoke-static {v0, p1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result p1

    if-eqz p1, :cond_4

    return p1

    :cond_4
    const/4 p1, 0x0

    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 46
    check-cast p1, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;

    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->compareTo(Lcom/evernote/edam/type/SharedNotebookRecipientSettings;)I

    move-result p1

    return p1
.end method

.method public deepCopy()Lcom/evernote/edam/type/SharedNotebookRecipientSettings;
    .locals 1

    .line 74
    new-instance v0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;

    invoke-direct {v0, p0}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;-><init>(Lcom/evernote/edam/type/SharedNotebookRecipientSettings;)V

    return-object v0
.end method

.method public bridge synthetic deepCopy()Lcom/evernote/thrift/TBase;
    .locals 1

    .line 46
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->deepCopy()Lcom/evernote/edam/type/SharedNotebookRecipientSettings;

    move-result-object v0

    return-object v0
.end method

.method public equals(Lcom/evernote/edam/type/SharedNotebookRecipientSettings;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 141
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->isSetReminderNotifyEmail()Z

    move-result v1

    .line 142
    invoke-virtual {p1}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->isSetReminderNotifyEmail()Z

    move-result v2

    if-nez v1, :cond_1

    if-eqz v2, :cond_3

    :cond_1
    if-eqz v1, :cond_8

    if-nez v2, :cond_2

    goto :goto_1

    .line 146
    :cond_2
    iget-boolean v1, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->reminderNotifyEmail:Z

    iget-boolean v2, p1, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->reminderNotifyEmail:Z

    if-eq v1, v2, :cond_3

    return v0

    .line 150
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->isSetReminderNotifyInApp()Z

    move-result v1

    .line 151
    invoke-virtual {p1}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->isSetReminderNotifyInApp()Z

    move-result v2

    if-nez v1, :cond_4

    if-eqz v2, :cond_6

    :cond_4
    if-eqz v1, :cond_7

    if-nez v2, :cond_5

    goto :goto_0

    .line 155
    :cond_5
    iget-boolean v1, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->reminderNotifyInApp:Z

    iget-boolean p1, p1, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->reminderNotifyInApp:Z

    if-eq v1, p1, :cond_6

    return v0

    :cond_6
    const/4 p1, 0x1

    return p1

    :cond_7
    :goto_0
    return v0

    :cond_8
    :goto_1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 132
    :cond_0
    instance-of v1, p1, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;

    if-eqz v1, :cond_1

    .line 133
    check-cast p1, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;

    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->equals(Lcom/evernote/edam/type/SharedNotebookRecipientSettings;)Z

    move-result p1

    return p1

    :cond_1
    return v0
.end method

.method public hashCode()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isReminderNotifyEmail()Z
    .locals 1

    .line 85
    iget-boolean v0, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->reminderNotifyEmail:Z

    return v0
.end method

.method public isReminderNotifyInApp()Z
    .locals 1

    .line 107
    iget-boolean v0, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->reminderNotifyInApp:Z

    return v0
.end method

.method public isSetReminderNotifyEmail()Z
    .locals 2

    .line 99
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->__isset_vector:[Z

    const/4 v1, 0x0

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetReminderNotifyInApp()Z
    .locals 2

    .line 121
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->__isset_vector:[Z

    const/4 v1, 0x1

    aget-boolean v0, v0, v1

    return v0
.end method

.method public read(Lcom/evernote/thrift/protocol/TProtocol;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 198
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readStructBegin()Lcom/evernote/thrift/protocol/TStruct;

    .line 201
    :goto_0
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readFieldBegin()Lcom/evernote/thrift/protocol/TField;

    move-result-object v0

    .line 202
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-nez v1, :cond_0

    .line 227
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readStructEnd()V

    .line 228
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->validate()V

    return-void

    .line 205
    :cond_0
    iget-short v1, v0, Lcom/evernote/thrift/protocol/TField;->id:S

    const/4 v2, 0x1

    const/4 v3, 0x2

    packed-switch v1, :pswitch_data_0

    .line 223
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 215
    :pswitch_0
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v3, :cond_1

    .line 216
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->reminderNotifyInApp:Z

    .line 217
    invoke-virtual {p0, v2}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->setReminderNotifyInAppIsSet(Z)V

    goto :goto_1

    .line 219
    :cond_1
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 207
    :pswitch_1
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v3, :cond_2

    .line 208
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->reminderNotifyEmail:Z

    .line 209
    invoke-virtual {p0, v2}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->setReminderNotifyEmailIsSet(Z)V

    goto :goto_1

    .line 211
    :cond_2
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    .line 225
    :goto_1
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readFieldEnd()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setReminderNotifyEmail(Z)V
    .locals 0

    .line 89
    iput-boolean p1, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->reminderNotifyEmail:Z

    const/4 p1, 0x1

    .line 90
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->setReminderNotifyEmailIsSet(Z)V

    return-void
.end method

.method public setReminderNotifyEmailIsSet(Z)V
    .locals 2

    .line 103
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->__isset_vector:[Z

    const/4 v1, 0x0

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setReminderNotifyInApp(Z)V
    .locals 0

    .line 111
    iput-boolean p1, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->reminderNotifyInApp:Z

    const/4 p1, 0x1

    .line 112
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->setReminderNotifyInAppIsSet(Z)V

    return-void
.end method

.method public setReminderNotifyInAppIsSet(Z)V
    .locals 2

    .line 125
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->__isset_vector:[Z

    const/4 v1, 0x1

    aput-boolean p1, v0, v1

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 251
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SharedNotebookRecipientSettings("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 254
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->isSetReminderNotifyEmail()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "reminderNotifyEmail:"

    .line 255
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 256
    iget-boolean v1, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->reminderNotifyEmail:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    const/4 v1, 0x1

    .line 259
    :goto_0
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->isSetReminderNotifyInApp()Z

    move-result v2

    if-eqz v2, :cond_2

    if-nez v1, :cond_1

    const-string v1, ", "

    .line 260
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const-string v1, "reminderNotifyInApp:"

    .line 261
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 262
    iget-boolean v1, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->reminderNotifyInApp:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    :cond_2
    const-string v1, ")"

    .line 265
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 266
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public unsetReminderNotifyEmail()V
    .locals 2

    .line 94
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->__isset_vector:[Z

    const/4 v1, 0x0

    aput-boolean v1, v0, v1

    return-void
.end method

.method public unsetReminderNotifyInApp()V
    .locals 3

    .line 116
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->__isset_vector:[Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public validate()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    return-void
.end method

.method public write(Lcom/evernote/thrift/protocol/TProtocol;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 232
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->validate()V

    .line 234
    sget-object v0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeStructBegin(Lcom/evernote/thrift/protocol/TStruct;)V

    .line 235
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->isSetReminderNotifyEmail()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 236
    sget-object v0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->REMINDER_NOTIFY_EMAIL_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 237
    iget-boolean v0, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->reminderNotifyEmail:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 238
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 240
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->isSetReminderNotifyInApp()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 241
    sget-object v0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->REMINDER_NOTIFY_IN_APP_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 242
    iget-boolean v0, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->reminderNotifyInApp:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 243
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 245
    :cond_1
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldStop()V

    .line 246
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeStructEnd()V

    return-void
.end method

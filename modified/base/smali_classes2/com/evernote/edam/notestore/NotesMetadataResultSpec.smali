.class public Lcom/evernote/edam/notestore/NotesMetadataResultSpec;
.super Ljava/lang/Object;
.source "NotesMetadataResultSpec.java"

# interfaces
.implements Lcom/evernote/thrift/TBase;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/evernote/thrift/TBase<",
        "Lcom/evernote/edam/notestore/NotesMetadataResultSpec;",
        ">;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# static fields
.field private static final INCLUDE_ATTRIBUTES_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final INCLUDE_CONTENT_LENGTH_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final INCLUDE_CREATED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final INCLUDE_DELETED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final INCLUDE_LARGEST_RESOURCE_MIME_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final INCLUDE_LARGEST_RESOURCE_SIZE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final INCLUDE_NOTEBOOK_GUID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final INCLUDE_TAG_GUIDS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final INCLUDE_TITLE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final INCLUDE_UPDATED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final INCLUDE_UPDATE_SEQUENCE_NUM_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

.field private static final __INCLUDEATTRIBUTES_ISSET_ID:I = 0x8

.field private static final __INCLUDECONTENTLENGTH_ISSET_ID:I = 0x1

.field private static final __INCLUDECREATED_ISSET_ID:I = 0x2

.field private static final __INCLUDEDELETED_ISSET_ID:I = 0x4

.field private static final __INCLUDELARGESTRESOURCEMIME_ISSET_ID:I = 0x9

.field private static final __INCLUDELARGESTRESOURCESIZE_ISSET_ID:I = 0xa

.field private static final __INCLUDENOTEBOOKGUID_ISSET_ID:I = 0x6

.field private static final __INCLUDETAGGUIDS_ISSET_ID:I = 0x7

.field private static final __INCLUDETITLE_ISSET_ID:I = 0x0

.field private static final __INCLUDEUPDATED_ISSET_ID:I = 0x3

.field private static final __INCLUDEUPDATESEQUENCENUM_ISSET_ID:I = 0x5


# instance fields
.field private __isset_vector:[Z

.field private includeAttributes:Z

.field private includeContentLength:Z

.field private includeCreated:Z

.field private includeDeleted:Z

.field private includeLargestResourceMime:Z

.field private includeLargestResourceSize:Z

.field private includeNotebookGuid:Z

.field private includeTagGuids:Z

.field private includeTitle:Z

.field private includeUpdateSequenceNum:Z

.field private includeUpdated:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 32
    new-instance v0, Lcom/evernote/thrift/protocol/TStruct;

    const-string v1, "NotesMetadataResultSpec"

    invoke-direct {v0, v1}, Lcom/evernote/thrift/protocol/TStruct;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

    .line 34
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "includeTitle"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2, v2}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->INCLUDE_TITLE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 35
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "includeContentLength"

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->INCLUDE_CONTENT_LENGTH_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 36
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "includeCreated"

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->INCLUDE_CREATED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 37
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "includeUpdated"

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->INCLUDE_UPDATED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 38
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "includeDeleted"

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->INCLUDE_DELETED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 39
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "includeUpdateSequenceNum"

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->INCLUDE_UPDATE_SEQUENCE_NUM_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 40
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "includeNotebookGuid"

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->INCLUDE_NOTEBOOK_GUID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 41
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "includeTagGuids"

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->INCLUDE_TAG_GUIDS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 42
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "includeAttributes"

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->INCLUDE_ATTRIBUTES_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 43
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "includeLargestResourceMime"

    const/16 v3, 0x14

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->INCLUDE_LARGEST_RESOURCE_MIME_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 44
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "includeLargestResourceSize"

    const/16 v3, 0x15

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->INCLUDE_LARGEST_RESOURCE_SIZE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0xb

    .line 71
    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->__isset_vector:[Z

    return-void
.end method

.method public constructor <init>(Lcom/evernote/edam/notestore/NotesMetadataResultSpec;)V
    .locals 4

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0xb

    .line 71
    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->__isset_vector:[Z

    .line 80
    iget-object v0, p1, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->__isset_vector:[Z

    iget-object v1, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->__isset_vector:[Z

    array-length v2, v0

    const/4 v3, 0x0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 81
    iget-boolean v0, p1, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeTitle:Z

    iput-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeTitle:Z

    .line 82
    iget-boolean v0, p1, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeContentLength:Z

    iput-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeContentLength:Z

    .line 83
    iget-boolean v0, p1, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeCreated:Z

    iput-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeCreated:Z

    .line 84
    iget-boolean v0, p1, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeUpdated:Z

    iput-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeUpdated:Z

    .line 85
    iget-boolean v0, p1, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeDeleted:Z

    iput-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeDeleted:Z

    .line 86
    iget-boolean v0, p1, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeUpdateSequenceNum:Z

    iput-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeUpdateSequenceNum:Z

    .line 87
    iget-boolean v0, p1, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeNotebookGuid:Z

    iput-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeNotebookGuid:Z

    .line 88
    iget-boolean v0, p1, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeTagGuids:Z

    iput-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeTagGuids:Z

    .line 89
    iget-boolean v0, p1, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeAttributes:Z

    iput-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeAttributes:Z

    .line 90
    iget-boolean v0, p1, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeLargestResourceMime:Z

    iput-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeLargestResourceMime:Z

    .line 91
    iget-boolean p1, p1, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeLargestResourceSize:Z

    iput-boolean p1, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeLargestResourceSize:Z

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    const/4 v0, 0x0

    .line 99
    invoke-virtual {p0, v0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->setIncludeTitleIsSet(Z)V

    .line 100
    iput-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeTitle:Z

    .line 101
    invoke-virtual {p0, v0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->setIncludeContentLengthIsSet(Z)V

    .line 102
    iput-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeContentLength:Z

    .line 103
    invoke-virtual {p0, v0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->setIncludeCreatedIsSet(Z)V

    .line 104
    iput-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeCreated:Z

    .line 105
    invoke-virtual {p0, v0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->setIncludeUpdatedIsSet(Z)V

    .line 106
    iput-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeUpdated:Z

    .line 107
    invoke-virtual {p0, v0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->setIncludeDeletedIsSet(Z)V

    .line 108
    iput-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeDeleted:Z

    .line 109
    invoke-virtual {p0, v0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->setIncludeUpdateSequenceNumIsSet(Z)V

    .line 110
    iput-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeUpdateSequenceNum:Z

    .line 111
    invoke-virtual {p0, v0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->setIncludeNotebookGuidIsSet(Z)V

    .line 112
    iput-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeNotebookGuid:Z

    .line 113
    invoke-virtual {p0, v0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->setIncludeTagGuidsIsSet(Z)V

    .line 114
    iput-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeTagGuids:Z

    .line 115
    invoke-virtual {p0, v0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->setIncludeAttributesIsSet(Z)V

    .line 116
    iput-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeAttributes:Z

    .line 117
    invoke-virtual {p0, v0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->setIncludeLargestResourceMimeIsSet(Z)V

    .line 118
    iput-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeLargestResourceMime:Z

    .line 119
    invoke-virtual {p0, v0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->setIncludeLargestResourceSizeIsSet(Z)V

    .line 120
    iput-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeLargestResourceSize:Z

    return-void
.end method

.method public compareTo(Lcom/evernote/edam/notestore/NotesMetadataResultSpec;)I
    .locals 2

    .line 486
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 487
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    return p1

    .line 493
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeTitle()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeTitle()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_1

    return v0

    .line 497
    :cond_1
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeTitle()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeTitle:Z

    iget-boolean v1, p1, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeTitle:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_2

    return v0

    .line 502
    :cond_2
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeContentLength()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeContentLength()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_3

    return v0

    .line 506
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeContentLength()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeContentLength:Z

    iget-boolean v1, p1, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeContentLength:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_4

    return v0

    .line 511
    :cond_4
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeCreated()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeCreated()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_5

    return v0

    .line 515
    :cond_5
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeCreated()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeCreated:Z

    iget-boolean v1, p1, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeCreated:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_6

    return v0

    .line 520
    :cond_6
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeUpdated()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeUpdated()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_7

    return v0

    .line 524
    :cond_7
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeUpdated()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeUpdated:Z

    iget-boolean v1, p1, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeUpdated:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_8

    return v0

    .line 529
    :cond_8
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeDeleted()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeDeleted()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_9

    return v0

    .line 533
    :cond_9
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeDeleted()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeDeleted:Z

    iget-boolean v1, p1, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeDeleted:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_a

    return v0

    .line 538
    :cond_a
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeUpdateSequenceNum()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeUpdateSequenceNum()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_b

    return v0

    .line 542
    :cond_b
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeUpdateSequenceNum()Z

    move-result v0

    if-eqz v0, :cond_c

    iget-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeUpdateSequenceNum:Z

    iget-boolean v1, p1, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeUpdateSequenceNum:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_c

    return v0

    .line 547
    :cond_c
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeNotebookGuid()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeNotebookGuid()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_d

    return v0

    .line 551
    :cond_d
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeNotebookGuid()Z

    move-result v0

    if-eqz v0, :cond_e

    iget-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeNotebookGuid:Z

    iget-boolean v1, p1, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeNotebookGuid:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_e

    return v0

    .line 556
    :cond_e
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeTagGuids()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeTagGuids()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_f

    return v0

    .line 560
    :cond_f
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeTagGuids()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeTagGuids:Z

    iget-boolean v1, p1, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeTagGuids:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_10

    return v0

    .line 565
    :cond_10
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeAttributes()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeAttributes()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_11

    return v0

    .line 569
    :cond_11
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeAttributes()Z

    move-result v0

    if-eqz v0, :cond_12

    iget-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeAttributes:Z

    iget-boolean v1, p1, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeAttributes:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_12

    return v0

    .line 574
    :cond_12
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeLargestResourceMime()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeLargestResourceMime()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_13

    return v0

    .line 578
    :cond_13
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeLargestResourceMime()Z

    move-result v0

    if-eqz v0, :cond_14

    iget-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeLargestResourceMime:Z

    iget-boolean v1, p1, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeLargestResourceMime:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_14

    return v0

    .line 583
    :cond_14
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeLargestResourceSize()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeLargestResourceSize()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_15

    return v0

    .line 587
    :cond_15
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeLargestResourceSize()Z

    move-result v0

    if-eqz v0, :cond_16

    iget-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeLargestResourceSize:Z

    iget-boolean p1, p1, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeLargestResourceSize:Z

    invoke-static {v0, p1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result p1

    if-eqz p1, :cond_16

    return p1

    :cond_16
    const/4 p1, 0x0

    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 31
    check-cast p1, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;

    invoke-virtual {p0, p1}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->compareTo(Lcom/evernote/edam/notestore/NotesMetadataResultSpec;)I

    move-result p1

    return p1
.end method

.method public deepCopy()Lcom/evernote/edam/notestore/NotesMetadataResultSpec;
    .locals 1

    .line 95
    new-instance v0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;

    invoke-direct {v0, p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;-><init>(Lcom/evernote/edam/notestore/NotesMetadataResultSpec;)V

    return-object v0
.end method

.method public bridge synthetic deepCopy()Lcom/evernote/thrift/TBase;
    .locals 1

    .line 31
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->deepCopy()Lcom/evernote/edam/notestore/NotesMetadataResultSpec;

    move-result-object v0

    return-object v0
.end method

.method public equals(Lcom/evernote/edam/notestore/NotesMetadataResultSpec;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 378
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeTitle()Z

    move-result v1

    .line 379
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeTitle()Z

    move-result v2

    if-nez v1, :cond_1

    if-eqz v2, :cond_3

    :cond_1
    if-eqz v1, :cond_2c

    if-nez v2, :cond_2

    goto/16 :goto_a

    .line 383
    :cond_2
    iget-boolean v1, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeTitle:Z

    iget-boolean v2, p1, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeTitle:Z

    if-eq v1, v2, :cond_3

    return v0

    .line 387
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeContentLength()Z

    move-result v1

    .line 388
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeContentLength()Z

    move-result v2

    if-nez v1, :cond_4

    if-eqz v2, :cond_6

    :cond_4
    if-eqz v1, :cond_2b

    if-nez v2, :cond_5

    goto/16 :goto_9

    .line 392
    :cond_5
    iget-boolean v1, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeContentLength:Z

    iget-boolean v2, p1, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeContentLength:Z

    if-eq v1, v2, :cond_6

    return v0

    .line 396
    :cond_6
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeCreated()Z

    move-result v1

    .line 397
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeCreated()Z

    move-result v2

    if-nez v1, :cond_7

    if-eqz v2, :cond_9

    :cond_7
    if-eqz v1, :cond_2a

    if-nez v2, :cond_8

    goto/16 :goto_8

    .line 401
    :cond_8
    iget-boolean v1, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeCreated:Z

    iget-boolean v2, p1, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeCreated:Z

    if-eq v1, v2, :cond_9

    return v0

    .line 405
    :cond_9
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeUpdated()Z

    move-result v1

    .line 406
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeUpdated()Z

    move-result v2

    if-nez v1, :cond_a

    if-eqz v2, :cond_c

    :cond_a
    if-eqz v1, :cond_29

    if-nez v2, :cond_b

    goto/16 :goto_7

    .line 410
    :cond_b
    iget-boolean v1, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeUpdated:Z

    iget-boolean v2, p1, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeUpdated:Z

    if-eq v1, v2, :cond_c

    return v0

    .line 414
    :cond_c
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeDeleted()Z

    move-result v1

    .line 415
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeDeleted()Z

    move-result v2

    if-nez v1, :cond_d

    if-eqz v2, :cond_f

    :cond_d
    if-eqz v1, :cond_28

    if-nez v2, :cond_e

    goto/16 :goto_6

    .line 419
    :cond_e
    iget-boolean v1, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeDeleted:Z

    iget-boolean v2, p1, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeDeleted:Z

    if-eq v1, v2, :cond_f

    return v0

    .line 423
    :cond_f
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeUpdateSequenceNum()Z

    move-result v1

    .line 424
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeUpdateSequenceNum()Z

    move-result v2

    if-nez v1, :cond_10

    if-eqz v2, :cond_12

    :cond_10
    if-eqz v1, :cond_27

    if-nez v2, :cond_11

    goto/16 :goto_5

    .line 428
    :cond_11
    iget-boolean v1, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeUpdateSequenceNum:Z

    iget-boolean v2, p1, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeUpdateSequenceNum:Z

    if-eq v1, v2, :cond_12

    return v0

    .line 432
    :cond_12
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeNotebookGuid()Z

    move-result v1

    .line 433
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeNotebookGuid()Z

    move-result v2

    if-nez v1, :cond_13

    if-eqz v2, :cond_15

    :cond_13
    if-eqz v1, :cond_26

    if-nez v2, :cond_14

    goto/16 :goto_4

    .line 437
    :cond_14
    iget-boolean v1, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeNotebookGuid:Z

    iget-boolean v2, p1, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeNotebookGuid:Z

    if-eq v1, v2, :cond_15

    return v0

    .line 441
    :cond_15
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeTagGuids()Z

    move-result v1

    .line 442
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeTagGuids()Z

    move-result v2

    if-nez v1, :cond_16

    if-eqz v2, :cond_18

    :cond_16
    if-eqz v1, :cond_25

    if-nez v2, :cond_17

    goto :goto_3

    .line 446
    :cond_17
    iget-boolean v1, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeTagGuids:Z

    iget-boolean v2, p1, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeTagGuids:Z

    if-eq v1, v2, :cond_18

    return v0

    .line 450
    :cond_18
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeAttributes()Z

    move-result v1

    .line 451
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeAttributes()Z

    move-result v2

    if-nez v1, :cond_19

    if-eqz v2, :cond_1b

    :cond_19
    if-eqz v1, :cond_24

    if-nez v2, :cond_1a

    goto :goto_2

    .line 455
    :cond_1a
    iget-boolean v1, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeAttributes:Z

    iget-boolean v2, p1, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeAttributes:Z

    if-eq v1, v2, :cond_1b

    return v0

    .line 459
    :cond_1b
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeLargestResourceMime()Z

    move-result v1

    .line 460
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeLargestResourceMime()Z

    move-result v2

    if-nez v1, :cond_1c

    if-eqz v2, :cond_1e

    :cond_1c
    if-eqz v1, :cond_23

    if-nez v2, :cond_1d

    goto :goto_1

    .line 464
    :cond_1d
    iget-boolean v1, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeLargestResourceMime:Z

    iget-boolean v2, p1, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeLargestResourceMime:Z

    if-eq v1, v2, :cond_1e

    return v0

    .line 468
    :cond_1e
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeLargestResourceSize()Z

    move-result v1

    .line 469
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeLargestResourceSize()Z

    move-result v2

    if-nez v1, :cond_1f

    if-eqz v2, :cond_21

    :cond_1f
    if-eqz v1, :cond_22

    if-nez v2, :cond_20

    goto :goto_0

    .line 473
    :cond_20
    iget-boolean v1, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeLargestResourceSize:Z

    iget-boolean p1, p1, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeLargestResourceSize:Z

    if-eq v1, p1, :cond_21

    return v0

    :cond_21
    const/4 p1, 0x1

    return p1

    :cond_22
    :goto_0
    return v0

    :cond_23
    :goto_1
    return v0

    :cond_24
    :goto_2
    return v0

    :cond_25
    :goto_3
    return v0

    :cond_26
    :goto_4
    return v0

    :cond_27
    :goto_5
    return v0

    :cond_28
    :goto_6
    return v0

    :cond_29
    :goto_7
    return v0

    :cond_2a
    :goto_8
    return v0

    :cond_2b
    :goto_9
    return v0

    :cond_2c
    :goto_a
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 369
    :cond_0
    instance-of v1, p1, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;

    if-eqz v1, :cond_1

    .line 370
    check-cast p1, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;

    invoke-virtual {p0, p1}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->equals(Lcom/evernote/edam/notestore/NotesMetadataResultSpec;)Z

    move-result p1

    return p1

    :cond_1
    return v0
.end method

.method public hashCode()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isIncludeAttributes()Z
    .locals 1

    .line 300
    iget-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeAttributes:Z

    return v0
.end method

.method public isIncludeContentLength()Z
    .locals 1

    .line 146
    iget-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeContentLength:Z

    return v0
.end method

.method public isIncludeCreated()Z
    .locals 1

    .line 168
    iget-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeCreated:Z

    return v0
.end method

.method public isIncludeDeleted()Z
    .locals 1

    .line 212
    iget-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeDeleted:Z

    return v0
.end method

.method public isIncludeLargestResourceMime()Z
    .locals 1

    .line 322
    iget-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeLargestResourceMime:Z

    return v0
.end method

.method public isIncludeLargestResourceSize()Z
    .locals 1

    .line 344
    iget-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeLargestResourceSize:Z

    return v0
.end method

.method public isIncludeNotebookGuid()Z
    .locals 1

    .line 256
    iget-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeNotebookGuid:Z

    return v0
.end method

.method public isIncludeTagGuids()Z
    .locals 1

    .line 278
    iget-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeTagGuids:Z

    return v0
.end method

.method public isIncludeTitle()Z
    .locals 1

    .line 124
    iget-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeTitle:Z

    return v0
.end method

.method public isIncludeUpdateSequenceNum()Z
    .locals 1

    .line 234
    iget-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeUpdateSequenceNum:Z

    return v0
.end method

.method public isIncludeUpdated()Z
    .locals 1

    .line 190
    iget-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeUpdated:Z

    return v0
.end method

.method public isSetIncludeAttributes()Z
    .locals 2

    .line 314
    iget-object v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->__isset_vector:[Z

    const/16 v1, 0x8

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetIncludeContentLength()Z
    .locals 2

    .line 160
    iget-object v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->__isset_vector:[Z

    const/4 v1, 0x1

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetIncludeCreated()Z
    .locals 2

    .line 182
    iget-object v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->__isset_vector:[Z

    const/4 v1, 0x2

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetIncludeDeleted()Z
    .locals 2

    .line 226
    iget-object v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->__isset_vector:[Z

    const/4 v1, 0x4

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetIncludeLargestResourceMime()Z
    .locals 2

    .line 336
    iget-object v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->__isset_vector:[Z

    const/16 v1, 0x9

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetIncludeLargestResourceSize()Z
    .locals 2

    .line 358
    iget-object v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->__isset_vector:[Z

    const/16 v1, 0xa

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetIncludeNotebookGuid()Z
    .locals 2

    .line 270
    iget-object v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->__isset_vector:[Z

    const/4 v1, 0x6

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetIncludeTagGuids()Z
    .locals 2

    .line 292
    iget-object v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->__isset_vector:[Z

    const/4 v1, 0x7

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetIncludeTitle()Z
    .locals 2

    .line 138
    iget-object v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->__isset_vector:[Z

    const/4 v1, 0x0

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetIncludeUpdateSequenceNum()Z
    .locals 2

    .line 248
    iget-object v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->__isset_vector:[Z

    const/4 v1, 0x5

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetIncludeUpdated()Z
    .locals 2

    .line 204
    iget-object v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->__isset_vector:[Z

    const/4 v1, 0x3

    aget-boolean v0, v0, v1

    return v0
.end method

.method public read(Lcom/evernote/thrift/protocol/TProtocol;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 597
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readStructBegin()Lcom/evernote/thrift/protocol/TStruct;

    .line 600
    :goto_0
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readFieldBegin()Lcom/evernote/thrift/protocol/TField;

    move-result-object v0

    .line 601
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-nez v1, :cond_0

    .line 698
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readStructEnd()V

    .line 699
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->validate()V

    return-void

    .line 604
    :cond_0
    iget-short v1, v0, Lcom/evernote/thrift/protocol/TField;->id:S

    const/4 v2, 0x1

    const/4 v3, 0x2

    if-eq v1, v3, :cond_c

    const/16 v4, 0xe

    if-eq v1, v4, :cond_a

    packed-switch v1, :pswitch_data_0

    packed-switch v1, :pswitch_data_1

    packed-switch v1, :pswitch_data_2

    .line 694
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 686
    :pswitch_0
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v3, :cond_1

    .line 687
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeLargestResourceSize:Z

    .line 688
    invoke-virtual {p0, v2}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->setIncludeLargestResourceSizeIsSet(Z)V

    goto/16 :goto_1

    .line 690
    :cond_1
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 678
    :pswitch_1
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v3, :cond_2

    .line 679
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeLargestResourceMime:Z

    .line 680
    invoke-virtual {p0, v2}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->setIncludeLargestResourceMimeIsSet(Z)V

    goto/16 :goto_1

    .line 682
    :cond_2
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 662
    :pswitch_2
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v3, :cond_3

    .line 663
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeTagGuids:Z

    .line 664
    invoke-virtual {p0, v2}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->setIncludeTagGuidsIsSet(Z)V

    goto/16 :goto_1

    .line 666
    :cond_3
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 654
    :pswitch_3
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v3, :cond_4

    .line 655
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeNotebookGuid:Z

    .line 656
    invoke-virtual {p0, v2}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->setIncludeNotebookGuidIsSet(Z)V

    goto/16 :goto_1

    .line 658
    :cond_4
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 646
    :pswitch_4
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v3, :cond_5

    .line 647
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeUpdateSequenceNum:Z

    .line 648
    invoke-virtual {p0, v2}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->setIncludeUpdateSequenceNumIsSet(Z)V

    goto/16 :goto_1

    .line 650
    :cond_5
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 638
    :pswitch_5
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v3, :cond_6

    .line 639
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeDeleted:Z

    .line 640
    invoke-virtual {p0, v2}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->setIncludeDeletedIsSet(Z)V

    goto/16 :goto_1

    .line 642
    :cond_6
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 630
    :pswitch_6
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v3, :cond_7

    .line 631
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeUpdated:Z

    .line 632
    invoke-virtual {p0, v2}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->setIncludeUpdatedIsSet(Z)V

    goto :goto_1

    .line 634
    :cond_7
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 622
    :pswitch_7
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v3, :cond_8

    .line 623
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeCreated:Z

    .line 624
    invoke-virtual {p0, v2}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->setIncludeCreatedIsSet(Z)V

    goto :goto_1

    .line 626
    :cond_8
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 614
    :pswitch_8
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v3, :cond_9

    .line 615
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeContentLength:Z

    .line 616
    invoke-virtual {p0, v2}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->setIncludeContentLengthIsSet(Z)V

    goto :goto_1

    .line 618
    :cond_9
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 670
    :cond_a
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v3, :cond_b

    .line 671
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeAttributes:Z

    .line 672
    invoke-virtual {p0, v2}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->setIncludeAttributesIsSet(Z)V

    goto :goto_1

    .line 674
    :cond_b
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 606
    :cond_c
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v3, :cond_d

    .line 607
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeTitle:Z

    .line 608
    invoke-virtual {p0, v2}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->setIncludeTitleIsSet(Z)V

    goto :goto_1

    .line 610
    :cond_d
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    .line 696
    :goto_1
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readFieldEnd()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0xa
        :pswitch_4
        :pswitch_3
        :pswitch_2
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x14
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setIncludeAttributes(Z)V
    .locals 0

    .line 304
    iput-boolean p1, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeAttributes:Z

    const/4 p1, 0x1

    .line 305
    invoke-virtual {p0, p1}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->setIncludeAttributesIsSet(Z)V

    return-void
.end method

.method public setIncludeAttributesIsSet(Z)V
    .locals 2

    .line 318
    iget-object v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->__isset_vector:[Z

    const/16 v1, 0x8

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setIncludeContentLength(Z)V
    .locals 0

    .line 150
    iput-boolean p1, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeContentLength:Z

    const/4 p1, 0x1

    .line 151
    invoke-virtual {p0, p1}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->setIncludeContentLengthIsSet(Z)V

    return-void
.end method

.method public setIncludeContentLengthIsSet(Z)V
    .locals 2

    .line 164
    iget-object v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->__isset_vector:[Z

    const/4 v1, 0x1

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setIncludeCreated(Z)V
    .locals 0

    .line 172
    iput-boolean p1, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeCreated:Z

    const/4 p1, 0x1

    .line 173
    invoke-virtual {p0, p1}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->setIncludeCreatedIsSet(Z)V

    return-void
.end method

.method public setIncludeCreatedIsSet(Z)V
    .locals 2

    .line 186
    iget-object v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->__isset_vector:[Z

    const/4 v1, 0x2

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setIncludeDeleted(Z)V
    .locals 0

    .line 216
    iput-boolean p1, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeDeleted:Z

    const/4 p1, 0x1

    .line 217
    invoke-virtual {p0, p1}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->setIncludeDeletedIsSet(Z)V

    return-void
.end method

.method public setIncludeDeletedIsSet(Z)V
    .locals 2

    .line 230
    iget-object v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->__isset_vector:[Z

    const/4 v1, 0x4

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setIncludeLargestResourceMime(Z)V
    .locals 0

    .line 326
    iput-boolean p1, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeLargestResourceMime:Z

    const/4 p1, 0x1

    .line 327
    invoke-virtual {p0, p1}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->setIncludeLargestResourceMimeIsSet(Z)V

    return-void
.end method

.method public setIncludeLargestResourceMimeIsSet(Z)V
    .locals 2

    .line 340
    iget-object v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->__isset_vector:[Z

    const/16 v1, 0x9

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setIncludeLargestResourceSize(Z)V
    .locals 0

    .line 348
    iput-boolean p1, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeLargestResourceSize:Z

    const/4 p1, 0x1

    .line 349
    invoke-virtual {p0, p1}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->setIncludeLargestResourceSizeIsSet(Z)V

    return-void
.end method

.method public setIncludeLargestResourceSizeIsSet(Z)V
    .locals 2

    .line 362
    iget-object v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->__isset_vector:[Z

    const/16 v1, 0xa

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setIncludeNotebookGuid(Z)V
    .locals 0

    .line 260
    iput-boolean p1, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeNotebookGuid:Z

    const/4 p1, 0x1

    .line 261
    invoke-virtual {p0, p1}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->setIncludeNotebookGuidIsSet(Z)V

    return-void
.end method

.method public setIncludeNotebookGuidIsSet(Z)V
    .locals 2

    .line 274
    iget-object v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->__isset_vector:[Z

    const/4 v1, 0x6

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setIncludeTagGuids(Z)V
    .locals 0

    .line 282
    iput-boolean p1, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeTagGuids:Z

    const/4 p1, 0x1

    .line 283
    invoke-virtual {p0, p1}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->setIncludeTagGuidsIsSet(Z)V

    return-void
.end method

.method public setIncludeTagGuidsIsSet(Z)V
    .locals 2

    .line 296
    iget-object v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->__isset_vector:[Z

    const/4 v1, 0x7

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setIncludeTitle(Z)V
    .locals 0

    .line 128
    iput-boolean p1, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeTitle:Z

    const/4 p1, 0x1

    .line 129
    invoke-virtual {p0, p1}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->setIncludeTitleIsSet(Z)V

    return-void
.end method

.method public setIncludeTitleIsSet(Z)V
    .locals 2

    .line 142
    iget-object v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->__isset_vector:[Z

    const/4 v1, 0x0

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setIncludeUpdateSequenceNum(Z)V
    .locals 0

    .line 238
    iput-boolean p1, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeUpdateSequenceNum:Z

    const/4 p1, 0x1

    .line 239
    invoke-virtual {p0, p1}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->setIncludeUpdateSequenceNumIsSet(Z)V

    return-void
.end method

.method public setIncludeUpdateSequenceNumIsSet(Z)V
    .locals 2

    .line 252
    iget-object v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->__isset_vector:[Z

    const/4 v1, 0x5

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setIncludeUpdated(Z)V
    .locals 0

    .line 194
    iput-boolean p1, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeUpdated:Z

    const/4 p1, 0x1

    .line 195
    invoke-virtual {p0, p1}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->setIncludeUpdatedIsSet(Z)V

    return-void
.end method

.method public setIncludeUpdatedIsSet(Z)V
    .locals 2

    .line 208
    iget-object v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->__isset_vector:[Z

    const/4 v1, 0x3

    aput-boolean p1, v0, v1

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 767
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NotesMetadataResultSpec("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 770
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeTitle()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    const-string v1, "includeTitle:"

    .line 771
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 772
    iget-boolean v1, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeTitle:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    const/4 v1, 0x1

    .line 775
    :goto_0
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeContentLength()Z

    move-result v3

    if-eqz v3, :cond_2

    if-nez v1, :cond_1

    const-string v1, ", "

    .line 776
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const-string v1, "includeContentLength:"

    .line 777
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 778
    iget-boolean v1, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeContentLength:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 781
    :cond_2
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeCreated()Z

    move-result v3

    if-eqz v3, :cond_4

    if-nez v1, :cond_3

    const-string v1, ", "

    .line 782
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    const-string v1, "includeCreated:"

    .line 783
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 784
    iget-boolean v1, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeCreated:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 787
    :cond_4
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeUpdated()Z

    move-result v3

    if-eqz v3, :cond_6

    if-nez v1, :cond_5

    const-string v1, ", "

    .line 788
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    const-string v1, "includeUpdated:"

    .line 789
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 790
    iget-boolean v1, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeUpdated:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 793
    :cond_6
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeDeleted()Z

    move-result v3

    if-eqz v3, :cond_8

    if-nez v1, :cond_7

    const-string v1, ", "

    .line 794
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_7
    const-string v1, "includeDeleted:"

    .line 795
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 796
    iget-boolean v1, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeDeleted:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 799
    :cond_8
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeUpdateSequenceNum()Z

    move-result v3

    if-eqz v3, :cond_a

    if-nez v1, :cond_9

    const-string v1, ", "

    .line 800
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_9
    const-string v1, "includeUpdateSequenceNum:"

    .line 801
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 802
    iget-boolean v1, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeUpdateSequenceNum:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 805
    :cond_a
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeNotebookGuid()Z

    move-result v3

    if-eqz v3, :cond_c

    if-nez v1, :cond_b

    const-string v1, ", "

    .line 806
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_b
    const-string v1, "includeNotebookGuid:"

    .line 807
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 808
    iget-boolean v1, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeNotebookGuid:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 811
    :cond_c
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeTagGuids()Z

    move-result v3

    if-eqz v3, :cond_e

    if-nez v1, :cond_d

    const-string v1, ", "

    .line 812
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_d
    const-string v1, "includeTagGuids:"

    .line 813
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 814
    iget-boolean v1, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeTagGuids:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 817
    :cond_e
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeAttributes()Z

    move-result v3

    if-eqz v3, :cond_10

    if-nez v1, :cond_f

    const-string v1, ", "

    .line 818
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_f
    const-string v1, "includeAttributes:"

    .line 819
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 820
    iget-boolean v1, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeAttributes:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 823
    :cond_10
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeLargestResourceMime()Z

    move-result v3

    if-eqz v3, :cond_12

    if-nez v1, :cond_11

    const-string v1, ", "

    .line 824
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_11
    const-string v1, "includeLargestResourceMime:"

    .line 825
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 826
    iget-boolean v1, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeLargestResourceMime:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 829
    :cond_12
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeLargestResourceSize()Z

    move-result v2

    if-eqz v2, :cond_14

    if-nez v1, :cond_13

    const-string v1, ", "

    .line 830
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_13
    const-string v1, "includeLargestResourceSize:"

    .line 831
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 832
    iget-boolean v1, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeLargestResourceSize:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    :cond_14
    const-string v1, ")"

    .line 835
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 836
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public unsetIncludeAttributes()V
    .locals 3

    .line 309
    iget-object v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->__isset_vector:[Z

    const/16 v1, 0x8

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetIncludeContentLength()V
    .locals 3

    .line 155
    iget-object v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->__isset_vector:[Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetIncludeCreated()V
    .locals 3

    .line 177
    iget-object v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->__isset_vector:[Z

    const/4 v1, 0x2

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetIncludeDeleted()V
    .locals 3

    .line 221
    iget-object v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->__isset_vector:[Z

    const/4 v1, 0x4

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetIncludeLargestResourceMime()V
    .locals 3

    .line 331
    iget-object v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->__isset_vector:[Z

    const/16 v1, 0x9

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetIncludeLargestResourceSize()V
    .locals 3

    .line 353
    iget-object v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->__isset_vector:[Z

    const/16 v1, 0xa

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetIncludeNotebookGuid()V
    .locals 3

    .line 265
    iget-object v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->__isset_vector:[Z

    const/4 v1, 0x6

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetIncludeTagGuids()V
    .locals 3

    .line 287
    iget-object v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->__isset_vector:[Z

    const/4 v1, 0x7

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetIncludeTitle()V
    .locals 2

    .line 133
    iget-object v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->__isset_vector:[Z

    const/4 v1, 0x0

    aput-boolean v1, v0, v1

    return-void
.end method

.method public unsetIncludeUpdateSequenceNum()V
    .locals 3

    .line 243
    iget-object v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->__isset_vector:[Z

    const/4 v1, 0x5

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetIncludeUpdated()V
    .locals 3

    .line 199
    iget-object v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->__isset_vector:[Z

    const/4 v1, 0x3

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public validate()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    return-void
.end method

.method public write(Lcom/evernote/thrift/protocol/TProtocol;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 703
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->validate()V

    .line 705
    sget-object v0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeStructBegin(Lcom/evernote/thrift/protocol/TStruct;)V

    .line 706
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeTitle()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 707
    sget-object v0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->INCLUDE_TITLE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 708
    iget-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeTitle:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 709
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 711
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeContentLength()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 712
    sget-object v0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->INCLUDE_CONTENT_LENGTH_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 713
    iget-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeContentLength:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 714
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 716
    :cond_1
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeCreated()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 717
    sget-object v0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->INCLUDE_CREATED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 718
    iget-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeCreated:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 719
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 721
    :cond_2
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeUpdated()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 722
    sget-object v0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->INCLUDE_UPDATED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 723
    iget-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeUpdated:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 724
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 726
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeDeleted()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 727
    sget-object v0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->INCLUDE_DELETED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 728
    iget-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeDeleted:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 729
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 731
    :cond_4
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeUpdateSequenceNum()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 732
    sget-object v0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->INCLUDE_UPDATE_SEQUENCE_NUM_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 733
    iget-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeUpdateSequenceNum:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 734
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 736
    :cond_5
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeNotebookGuid()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 737
    sget-object v0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->INCLUDE_NOTEBOOK_GUID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 738
    iget-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeNotebookGuid:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 739
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 741
    :cond_6
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeTagGuids()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 742
    sget-object v0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->INCLUDE_TAG_GUIDS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 743
    iget-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeTagGuids:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 744
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 746
    :cond_7
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeAttributes()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 747
    sget-object v0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->INCLUDE_ATTRIBUTES_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 748
    iget-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeAttributes:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 749
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 751
    :cond_8
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeLargestResourceMime()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 752
    sget-object v0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->INCLUDE_LARGEST_RESOURCE_MIME_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 753
    iget-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeLargestResourceMime:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 754
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 756
    :cond_9
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->isSetIncludeLargestResourceSize()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 757
    sget-object v0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->INCLUDE_LARGEST_RESOURCE_SIZE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 758
    iget-boolean v0, p0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->includeLargestResourceSize:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 759
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 761
    :cond_a
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldStop()V

    .line 762
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeStructEnd()V

    return-void
.end method

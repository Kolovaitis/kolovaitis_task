.class public Lcom/evernote/edam/notestore/NoteFilter;
.super Ljava/lang/Object;
.source "NoteFilter.java"

# interfaces
.implements Lcom/evernote/thrift/TBase;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/evernote/thrift/TBase<",
        "Lcom/evernote/edam/notestore/NoteFilter;",
        ">;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# static fields
.field private static final ASCENDING_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final EMPHASIZED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final INACTIVE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final NOTEBOOK_GUID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final ORDER_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

.field private static final TAG_GUIDS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final TIME_ZONE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final WORDS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final __ASCENDING_ISSET_ID:I = 0x1

.field private static final __INACTIVE_ISSET_ID:I = 0x2

.field private static final __ORDER_ISSET_ID:I


# instance fields
.field private __isset_vector:[Z

.field private ascending:Z

.field private emphasized:Ljava/lang/String;

.field private inactive:Z

.field private notebookGuid:Ljava/lang/String;

.field private order:I

.field private tagGuids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private timeZone:Ljava/lang/String;

.field private words:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 82
    new-instance v0, Lcom/evernote/thrift/protocol/TStruct;

    const-string v1, "NoteFilter"

    invoke-direct {v0, v1}, Lcom/evernote/thrift/protocol/TStruct;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/evernote/edam/notestore/NoteFilter;->STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

    .line 84
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "order"

    const/16 v2, 0x8

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/notestore/NoteFilter;->ORDER_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 85
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "ascending"

    const/4 v3, 0x2

    invoke-direct {v0, v1, v3, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/notestore/NoteFilter;->ASCENDING_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 86
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "words"

    const/16 v4, 0xb

    const/4 v5, 0x3

    invoke-direct {v0, v1, v4, v5}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/notestore/NoteFilter;->WORDS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 87
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "notebookGuid"

    const/4 v5, 0x4

    invoke-direct {v0, v1, v4, v5}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/notestore/NoteFilter;->NOTEBOOK_GUID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 88
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "tagGuids"

    const/16 v5, 0xf

    const/4 v6, 0x5

    invoke-direct {v0, v1, v5, v6}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/notestore/NoteFilter;->TAG_GUIDS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 89
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "timeZone"

    const/4 v5, 0x6

    invoke-direct {v0, v1, v4, v5}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/notestore/NoteFilter;->TIME_ZONE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 90
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "inactive"

    const/4 v5, 0x7

    invoke-direct {v0, v1, v3, v5}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/notestore/NoteFilter;->INACTIVE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 91
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "emphasized"

    invoke-direct {v0, v1, v4, v2}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/notestore/NoteFilter;->EMPHASIZED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x3

    .line 107
    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->__isset_vector:[Z

    return-void
.end method

.method public constructor <init>(Lcom/evernote/edam/notestore/NoteFilter;)V
    .locals 4

    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x3

    .line 107
    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->__isset_vector:[Z

    .line 116
    iget-object v0, p1, Lcom/evernote/edam/notestore/NoteFilter;->__isset_vector:[Z

    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteFilter;->__isset_vector:[Z

    array-length v2, v0

    const/4 v3, 0x0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 117
    iget v0, p1, Lcom/evernote/edam/notestore/NoteFilter;->order:I

    iput v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->order:I

    .line 118
    iget-boolean v0, p1, Lcom/evernote/edam/notestore/NoteFilter;->ascending:Z

    iput-boolean v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->ascending:Z

    .line 119
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteFilter;->isSetWords()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p1, Lcom/evernote/edam/notestore/NoteFilter;->words:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->words:Ljava/lang/String;

    .line 122
    :cond_0
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteFilter;->isSetNotebookGuid()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 123
    iget-object v0, p1, Lcom/evernote/edam/notestore/NoteFilter;->notebookGuid:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->notebookGuid:Ljava/lang/String;

    .line 125
    :cond_1
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteFilter;->isSetTagGuids()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 126
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 127
    iget-object v1, p1, Lcom/evernote/edam/notestore/NoteFilter;->tagGuids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 128
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 130
    :cond_2
    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->tagGuids:Ljava/util/List;

    .line 132
    :cond_3
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteFilter;->isSetTimeZone()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 133
    iget-object v0, p1, Lcom/evernote/edam/notestore/NoteFilter;->timeZone:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->timeZone:Ljava/lang/String;

    .line 135
    :cond_4
    iget-boolean v0, p1, Lcom/evernote/edam/notestore/NoteFilter;->inactive:Z

    iput-boolean v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->inactive:Z

    .line 136
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteFilter;->isSetEmphasized()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 137
    iget-object p1, p1, Lcom/evernote/edam/notestore/NoteFilter;->emphasized:Ljava/lang/String;

    iput-object p1, p0, Lcom/evernote/edam/notestore/NoteFilter;->emphasized:Ljava/lang/String;

    :cond_5
    return-void
.end method


# virtual methods
.method public addToTagGuids(Ljava/lang/String;)V
    .locals 1

    .line 258
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->tagGuids:Ljava/util/List;

    if-nez v0, :cond_0

    .line 259
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->tagGuids:Ljava/util/List;

    .line 261
    :cond_0
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->tagGuids:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public clear()V
    .locals 2

    const/4 v0, 0x0

    .line 146
    invoke-virtual {p0, v0}, Lcom/evernote/edam/notestore/NoteFilter;->setOrderIsSet(Z)V

    .line 147
    iput v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->order:I

    .line 148
    invoke-virtual {p0, v0}, Lcom/evernote/edam/notestore/NoteFilter;->setAscendingIsSet(Z)V

    .line 149
    iput-boolean v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->ascending:Z

    const/4 v1, 0x0

    .line 150
    iput-object v1, p0, Lcom/evernote/edam/notestore/NoteFilter;->words:Ljava/lang/String;

    .line 151
    iput-object v1, p0, Lcom/evernote/edam/notestore/NoteFilter;->notebookGuid:Ljava/lang/String;

    .line 152
    iput-object v1, p0, Lcom/evernote/edam/notestore/NoteFilter;->tagGuids:Ljava/util/List;

    .line 153
    iput-object v1, p0, Lcom/evernote/edam/notestore/NoteFilter;->timeZone:Ljava/lang/String;

    .line 154
    invoke-virtual {p0, v0}, Lcom/evernote/edam/notestore/NoteFilter;->setInactiveIsSet(Z)V

    .line 155
    iput-boolean v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->inactive:Z

    .line 156
    iput-object v1, p0, Lcom/evernote/edam/notestore/NoteFilter;->emphasized:Ljava/lang/String;

    return-void
.end method

.method public compareTo(Lcom/evernote/edam/notestore/NoteFilter;)I
    .locals 2

    .line 449
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 450
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    return p1

    .line 456
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteFilter;->isSetOrder()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteFilter;->isSetOrder()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_1

    return v0

    .line 460
    :cond_1
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteFilter;->isSetOrder()Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->order:I

    iget v1, p1, Lcom/evernote/edam/notestore/NoteFilter;->order:I

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(II)I

    move-result v0

    if-eqz v0, :cond_2

    return v0

    .line 465
    :cond_2
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteFilter;->isSetAscending()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteFilter;->isSetAscending()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_3

    return v0

    .line 469
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteFilter;->isSetAscending()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->ascending:Z

    iget-boolean v1, p1, Lcom/evernote/edam/notestore/NoteFilter;->ascending:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_4

    return v0

    .line 474
    :cond_4
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteFilter;->isSetWords()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteFilter;->isSetWords()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_5

    return v0

    .line 478
    :cond_5
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteFilter;->isSetWords()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->words:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/notestore/NoteFilter;->words:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_6

    return v0

    .line 483
    :cond_6
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteFilter;->isSetNotebookGuid()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteFilter;->isSetNotebookGuid()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_7

    return v0

    .line 487
    :cond_7
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteFilter;->isSetNotebookGuid()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->notebookGuid:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/notestore/NoteFilter;->notebookGuid:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_8

    return v0

    .line 492
    :cond_8
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteFilter;->isSetTagGuids()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteFilter;->isSetTagGuids()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_9

    return v0

    .line 496
    :cond_9
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteFilter;->isSetTagGuids()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->tagGuids:Ljava/util/List;

    iget-object v1, p1, Lcom/evernote/edam/notestore/NoteFilter;->tagGuids:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/util/List;Ljava/util/List;)I

    move-result v0

    if-eqz v0, :cond_a

    return v0

    .line 501
    :cond_a
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteFilter;->isSetTimeZone()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteFilter;->isSetTimeZone()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_b

    return v0

    .line 505
    :cond_b
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteFilter;->isSetTimeZone()Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->timeZone:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/notestore/NoteFilter;->timeZone:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_c

    return v0

    .line 510
    :cond_c
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteFilter;->isSetInactive()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteFilter;->isSetInactive()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_d

    return v0

    .line 514
    :cond_d
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteFilter;->isSetInactive()Z

    move-result v0

    if-eqz v0, :cond_e

    iget-boolean v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->inactive:Z

    iget-boolean v1, p1, Lcom/evernote/edam/notestore/NoteFilter;->inactive:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_e

    return v0

    .line 519
    :cond_e
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteFilter;->isSetEmphasized()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteFilter;->isSetEmphasized()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_f

    return v0

    .line 523
    :cond_f
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteFilter;->isSetEmphasized()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->emphasized:Ljava/lang/String;

    iget-object p1, p1, Lcom/evernote/edam/notestore/NoteFilter;->emphasized:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    if-eqz p1, :cond_10

    return p1

    :cond_10
    const/4 p1, 0x0

    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 81
    check-cast p1, Lcom/evernote/edam/notestore/NoteFilter;

    invoke-virtual {p0, p1}, Lcom/evernote/edam/notestore/NoteFilter;->compareTo(Lcom/evernote/edam/notestore/NoteFilter;)I

    move-result p1

    return p1
.end method

.method public deepCopy()Lcom/evernote/edam/notestore/NoteFilter;
    .locals 1

    .line 142
    new-instance v0, Lcom/evernote/edam/notestore/NoteFilter;

    invoke-direct {v0, p0}, Lcom/evernote/edam/notestore/NoteFilter;-><init>(Lcom/evernote/edam/notestore/NoteFilter;)V

    return-object v0
.end method

.method public bridge synthetic deepCopy()Lcom/evernote/thrift/TBase;
    .locals 1

    .line 81
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteFilter;->deepCopy()Lcom/evernote/edam/notestore/NoteFilter;

    move-result-object v0

    return-object v0
.end method

.method public equals(Lcom/evernote/edam/notestore/NoteFilter;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 368
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteFilter;->isSetOrder()Z

    move-result v1

    .line 369
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteFilter;->isSetOrder()Z

    move-result v2

    if-nez v1, :cond_1

    if-eqz v2, :cond_3

    :cond_1
    if-eqz v1, :cond_20

    if-nez v2, :cond_2

    goto/16 :goto_7

    .line 373
    :cond_2
    iget v1, p0, Lcom/evernote/edam/notestore/NoteFilter;->order:I

    iget v2, p1, Lcom/evernote/edam/notestore/NoteFilter;->order:I

    if-eq v1, v2, :cond_3

    return v0

    .line 377
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteFilter;->isSetAscending()Z

    move-result v1

    .line 378
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteFilter;->isSetAscending()Z

    move-result v2

    if-nez v1, :cond_4

    if-eqz v2, :cond_6

    :cond_4
    if-eqz v1, :cond_1f

    if-nez v2, :cond_5

    goto/16 :goto_6

    .line 382
    :cond_5
    iget-boolean v1, p0, Lcom/evernote/edam/notestore/NoteFilter;->ascending:Z

    iget-boolean v2, p1, Lcom/evernote/edam/notestore/NoteFilter;->ascending:Z

    if-eq v1, v2, :cond_6

    return v0

    .line 386
    :cond_6
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteFilter;->isSetWords()Z

    move-result v1

    .line 387
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteFilter;->isSetWords()Z

    move-result v2

    if-nez v1, :cond_7

    if-eqz v2, :cond_9

    :cond_7
    if-eqz v1, :cond_1e

    if-nez v2, :cond_8

    goto/16 :goto_5

    .line 391
    :cond_8
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteFilter;->words:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/notestore/NoteFilter;->words:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    return v0

    .line 395
    :cond_9
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteFilter;->isSetNotebookGuid()Z

    move-result v1

    .line 396
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteFilter;->isSetNotebookGuid()Z

    move-result v2

    if-nez v1, :cond_a

    if-eqz v2, :cond_c

    :cond_a
    if-eqz v1, :cond_1d

    if-nez v2, :cond_b

    goto/16 :goto_4

    .line 400
    :cond_b
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteFilter;->notebookGuid:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/notestore/NoteFilter;->notebookGuid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    return v0

    .line 404
    :cond_c
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteFilter;->isSetTagGuids()Z

    move-result v1

    .line 405
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteFilter;->isSetTagGuids()Z

    move-result v2

    if-nez v1, :cond_d

    if-eqz v2, :cond_f

    :cond_d
    if-eqz v1, :cond_1c

    if-nez v2, :cond_e

    goto :goto_3

    .line 409
    :cond_e
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteFilter;->tagGuids:Ljava/util/List;

    iget-object v2, p1, Lcom/evernote/edam/notestore/NoteFilter;->tagGuids:Ljava/util/List;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    return v0

    .line 413
    :cond_f
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteFilter;->isSetTimeZone()Z

    move-result v1

    .line 414
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteFilter;->isSetTimeZone()Z

    move-result v2

    if-nez v1, :cond_10

    if-eqz v2, :cond_12

    :cond_10
    if-eqz v1, :cond_1b

    if-nez v2, :cond_11

    goto :goto_2

    .line 418
    :cond_11
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteFilter;->timeZone:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/notestore/NoteFilter;->timeZone:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_12

    return v0

    .line 422
    :cond_12
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteFilter;->isSetInactive()Z

    move-result v1

    .line 423
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteFilter;->isSetInactive()Z

    move-result v2

    if-nez v1, :cond_13

    if-eqz v2, :cond_15

    :cond_13
    if-eqz v1, :cond_1a

    if-nez v2, :cond_14

    goto :goto_1

    .line 427
    :cond_14
    iget-boolean v1, p0, Lcom/evernote/edam/notestore/NoteFilter;->inactive:Z

    iget-boolean v2, p1, Lcom/evernote/edam/notestore/NoteFilter;->inactive:Z

    if-eq v1, v2, :cond_15

    return v0

    .line 431
    :cond_15
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteFilter;->isSetEmphasized()Z

    move-result v1

    .line 432
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteFilter;->isSetEmphasized()Z

    move-result v2

    if-nez v1, :cond_16

    if-eqz v2, :cond_18

    :cond_16
    if-eqz v1, :cond_19

    if-nez v2, :cond_17

    goto :goto_0

    .line 436
    :cond_17
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteFilter;->emphasized:Ljava/lang/String;

    iget-object p1, p1, Lcom/evernote/edam/notestore/NoteFilter;->emphasized:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_18

    return v0

    :cond_18
    const/4 p1, 0x1

    return p1

    :cond_19
    :goto_0
    return v0

    :cond_1a
    :goto_1
    return v0

    :cond_1b
    :goto_2
    return v0

    :cond_1c
    :goto_3
    return v0

    :cond_1d
    :goto_4
    return v0

    :cond_1e
    :goto_5
    return v0

    :cond_1f
    :goto_6
    return v0

    :cond_20
    :goto_7
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 359
    :cond_0
    instance-of v1, p1, Lcom/evernote/edam/notestore/NoteFilter;

    if-eqz v1, :cond_1

    .line 360
    check-cast p1, Lcom/evernote/edam/notestore/NoteFilter;

    invoke-virtual {p0, p1}, Lcom/evernote/edam/notestore/NoteFilter;->equals(Lcom/evernote/edam/notestore/NoteFilter;)Z

    move-result p1

    return p1

    :cond_1
    return v0
.end method

.method public getEmphasized()Ljava/lang/String;
    .locals 1

    .line 333
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->emphasized:Ljava/lang/String;

    return-object v0
.end method

.method public getNotebookGuid()Ljava/lang/String;
    .locals 1

    .line 227
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->notebookGuid:Ljava/lang/String;

    return-object v0
.end method

.method public getOrder()I
    .locals 1

    .line 160
    iget v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->order:I

    return v0
.end method

.method public getTagGuids()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 265
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->tagGuids:Ljava/util/List;

    return-object v0
.end method

.method public getTagGuidsIterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 254
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->tagGuids:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public getTagGuidsSize()I
    .locals 1

    .line 250
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->tagGuids:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    return v0
.end method

.method public getTimeZone()Ljava/lang/String;
    .locals 1

    .line 288
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->timeZone:Ljava/lang/String;

    return-object v0
.end method

.method public getWords()Ljava/lang/String;
    .locals 1

    .line 204
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->words:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isAscending()Z
    .locals 1

    .line 182
    iget-boolean v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->ascending:Z

    return v0
.end method

.method public isInactive()Z
    .locals 1

    .line 311
    iget-boolean v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->inactive:Z

    return v0
.end method

.method public isSetAscending()Z
    .locals 2

    .line 196
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->__isset_vector:[Z

    const/4 v1, 0x1

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetEmphasized()Z
    .locals 1

    .line 346
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->emphasized:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetInactive()Z
    .locals 2

    .line 325
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->__isset_vector:[Z

    const/4 v1, 0x2

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetNotebookGuid()Z
    .locals 1

    .line 240
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->notebookGuid:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetOrder()Z
    .locals 2

    .line 174
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->__isset_vector:[Z

    const/4 v1, 0x0

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetTagGuids()Z
    .locals 1

    .line 278
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->tagGuids:Ljava/util/List;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetTimeZone()Z
    .locals 1

    .line 301
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->timeZone:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetWords()Z
    .locals 1

    .line 217
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->words:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public read(Lcom/evernote/thrift/protocol/TProtocol;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 533
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readStructBegin()Lcom/evernote/thrift/protocol/TStruct;

    .line 536
    :goto_0
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readFieldBegin()Lcom/evernote/thrift/protocol/TField;

    move-result-object v0

    .line 537
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-nez v1, :cond_0

    .line 615
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readStructEnd()V

    .line 616
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteFilter;->validate()V

    return-void

    .line 540
    :cond_0
    iget-short v1, v0, Lcom/evernote/thrift/protocol/TField;->id:S

    const/4 v2, 0x2

    const/4 v3, 0x1

    const/16 v4, 0xb

    packed-switch v1, :pswitch_data_0

    .line 611
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_2

    .line 604
    :pswitch_0
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_1

    .line 605
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->emphasized:Ljava/lang/String;

    goto/16 :goto_2

    .line 607
    :cond_1
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_2

    .line 596
    :pswitch_1
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v2, :cond_2

    .line 597
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->inactive:Z

    .line 598
    invoke-virtual {p0, v3}, Lcom/evernote/edam/notestore/NoteFilter;->setInactiveIsSet(Z)V

    goto/16 :goto_2

    .line 600
    :cond_2
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_2

    .line 589
    :pswitch_2
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_3

    .line 590
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->timeZone:Ljava/lang/String;

    goto/16 :goto_2

    .line 592
    :cond_3
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_2

    .line 572
    :pswitch_3
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    const/16 v2, 0xf

    if-ne v1, v2, :cond_5

    .line 574
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readListBegin()Lcom/evernote/thrift/protocol/TList;

    move-result-object v0

    .line 575
    new-instance v1, Ljava/util/ArrayList;

    iget v2, v0, Lcom/evernote/thrift/protocol/TList;->size:I

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/evernote/edam/notestore/NoteFilter;->tagGuids:Ljava/util/List;

    const/4 v1, 0x0

    .line 576
    :goto_1
    iget v2, v0, Lcom/evernote/thrift/protocol/TList;->size:I

    if-ge v1, v2, :cond_4

    .line 579
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v2

    .line 580
    iget-object v3, p0, Lcom/evernote/edam/notestore/NoteFilter;->tagGuids:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 582
    :cond_4
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readListEnd()V

    goto :goto_2

    .line 585
    :cond_5
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_2

    .line 565
    :pswitch_4
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_6

    .line 566
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->notebookGuid:Ljava/lang/String;

    goto :goto_2

    .line 568
    :cond_6
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_2

    .line 558
    :pswitch_5
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_7

    .line 559
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->words:Ljava/lang/String;

    goto :goto_2

    .line 561
    :cond_7
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_2

    .line 550
    :pswitch_6
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v2, :cond_8

    .line 551
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->ascending:Z

    .line 552
    invoke-virtual {p0, v3}, Lcom/evernote/edam/notestore/NoteFilter;->setAscendingIsSet(Z)V

    goto :goto_2

    .line 554
    :cond_8
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_2

    .line 542
    :pswitch_7
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    const/16 v2, 0x8

    if-ne v1, v2, :cond_9

    .line 543
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI32()I

    move-result v0

    iput v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->order:I

    .line 544
    invoke-virtual {p0, v3}, Lcom/evernote/edam/notestore/NoteFilter;->setOrderIsSet(Z)V

    goto :goto_2

    .line 546
    :cond_9
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    .line 613
    :goto_2
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readFieldEnd()V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setAscending(Z)V
    .locals 0

    .line 186
    iput-boolean p1, p0, Lcom/evernote/edam/notestore/NoteFilter;->ascending:Z

    const/4 p1, 0x1

    .line 187
    invoke-virtual {p0, p1}, Lcom/evernote/edam/notestore/NoteFilter;->setAscendingIsSet(Z)V

    return-void
.end method

.method public setAscendingIsSet(Z)V
    .locals 2

    .line 200
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->__isset_vector:[Z

    const/4 v1, 0x1

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setEmphasized(Ljava/lang/String;)V
    .locals 0

    .line 337
    iput-object p1, p0, Lcom/evernote/edam/notestore/NoteFilter;->emphasized:Ljava/lang/String;

    return-void
.end method

.method public setEmphasizedIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 351
    iput-object p1, p0, Lcom/evernote/edam/notestore/NoteFilter;->emphasized:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setInactive(Z)V
    .locals 0

    .line 315
    iput-boolean p1, p0, Lcom/evernote/edam/notestore/NoteFilter;->inactive:Z

    const/4 p1, 0x1

    .line 316
    invoke-virtual {p0, p1}, Lcom/evernote/edam/notestore/NoteFilter;->setInactiveIsSet(Z)V

    return-void
.end method

.method public setInactiveIsSet(Z)V
    .locals 2

    .line 329
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->__isset_vector:[Z

    const/4 v1, 0x2

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setNotebookGuid(Ljava/lang/String;)V
    .locals 0

    .line 231
    iput-object p1, p0, Lcom/evernote/edam/notestore/NoteFilter;->notebookGuid:Ljava/lang/String;

    return-void
.end method

.method public setNotebookGuidIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 245
    iput-object p1, p0, Lcom/evernote/edam/notestore/NoteFilter;->notebookGuid:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setOrder(I)V
    .locals 0

    .line 164
    iput p1, p0, Lcom/evernote/edam/notestore/NoteFilter;->order:I

    const/4 p1, 0x1

    .line 165
    invoke-virtual {p0, p1}, Lcom/evernote/edam/notestore/NoteFilter;->setOrderIsSet(Z)V

    return-void
.end method

.method public setOrderIsSet(Z)V
    .locals 2

    .line 178
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->__isset_vector:[Z

    const/4 v1, 0x0

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setTagGuids(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 269
    iput-object p1, p0, Lcom/evernote/edam/notestore/NoteFilter;->tagGuids:Ljava/util/List;

    return-void
.end method

.method public setTagGuidsIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 283
    iput-object p1, p0, Lcom/evernote/edam/notestore/NoteFilter;->tagGuids:Ljava/util/List;

    :cond_0
    return-void
.end method

.method public setTimeZone(Ljava/lang/String;)V
    .locals 0

    .line 292
    iput-object p1, p0, Lcom/evernote/edam/notestore/NoteFilter;->timeZone:Ljava/lang/String;

    return-void
.end method

.method public setTimeZoneIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 306
    iput-object p1, p0, Lcom/evernote/edam/notestore/NoteFilter;->timeZone:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setWords(Ljava/lang/String;)V
    .locals 0

    .line 208
    iput-object p1, p0, Lcom/evernote/edam/notestore/NoteFilter;->words:Ljava/lang/String;

    return-void
.end method

.method public setWordsIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 222
    iput-object p1, p0, Lcom/evernote/edam/notestore/NoteFilter;->words:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 686
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NoteFilter("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 689
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteFilter;->isSetOrder()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    const-string v1, "order:"

    .line 690
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 691
    iget v1, p0, Lcom/evernote/edam/notestore/NoteFilter;->order:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    const/4 v1, 0x1

    .line 694
    :goto_0
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteFilter;->isSetAscending()Z

    move-result v3

    if-eqz v3, :cond_2

    if-nez v1, :cond_1

    const-string v1, ", "

    .line 695
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const-string v1, "ascending:"

    .line 696
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 697
    iget-boolean v1, p0, Lcom/evernote/edam/notestore/NoteFilter;->ascending:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 700
    :cond_2
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteFilter;->isSetWords()Z

    move-result v3

    if-eqz v3, :cond_5

    if-nez v1, :cond_3

    const-string v1, ", "

    .line 701
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    const-string v1, "words:"

    .line 702
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 703
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteFilter;->words:Ljava/lang/String;

    if-nez v1, :cond_4

    const-string v1, "null"

    .line 704
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 706
    :cond_4
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_1
    const/4 v1, 0x0

    .line 710
    :cond_5
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteFilter;->isSetNotebookGuid()Z

    move-result v3

    if-eqz v3, :cond_8

    if-nez v1, :cond_6

    const-string v1, ", "

    .line 711
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_6
    const-string v1, "notebookGuid:"

    .line 712
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 713
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteFilter;->notebookGuid:Ljava/lang/String;

    if-nez v1, :cond_7

    const-string v1, "null"

    .line 714
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 716
    :cond_7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_2
    const/4 v1, 0x0

    .line 720
    :cond_8
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteFilter;->isSetTagGuids()Z

    move-result v3

    if-eqz v3, :cond_b

    if-nez v1, :cond_9

    const-string v1, ", "

    .line 721
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_9
    const-string v1, "tagGuids:"

    .line 722
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 723
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteFilter;->tagGuids:Ljava/util/List;

    if-nez v1, :cond_a

    const-string v1, "null"

    .line 724
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 726
    :cond_a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :goto_3
    const/4 v1, 0x0

    .line 730
    :cond_b
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteFilter;->isSetTimeZone()Z

    move-result v3

    if-eqz v3, :cond_e

    if-nez v1, :cond_c

    const-string v1, ", "

    .line 731
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_c
    const-string v1, "timeZone:"

    .line 732
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 733
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteFilter;->timeZone:Ljava/lang/String;

    if-nez v1, :cond_d

    const-string v1, "null"

    .line 734
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 736
    :cond_d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_4
    const/4 v1, 0x0

    .line 740
    :cond_e
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteFilter;->isSetInactive()Z

    move-result v3

    if-eqz v3, :cond_10

    if-nez v1, :cond_f

    const-string v1, ", "

    .line 741
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_f
    const-string v1, "inactive:"

    .line 742
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 743
    iget-boolean v1, p0, Lcom/evernote/edam/notestore/NoteFilter;->inactive:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 746
    :cond_10
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteFilter;->isSetEmphasized()Z

    move-result v2

    if-eqz v2, :cond_13

    if-nez v1, :cond_11

    const-string v1, ", "

    .line 747
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_11
    const-string v1, "emphasized:"

    .line 748
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 749
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteFilter;->emphasized:Ljava/lang/String;

    if-nez v1, :cond_12

    const-string v1, "null"

    .line 750
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5

    .line 752
    :cond_12
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_13
    :goto_5
    const-string v1, ")"

    .line 756
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 757
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public unsetAscending()V
    .locals 3

    .line 191
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->__isset_vector:[Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetEmphasized()V
    .locals 1

    const/4 v0, 0x0

    .line 341
    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->emphasized:Ljava/lang/String;

    return-void
.end method

.method public unsetInactive()V
    .locals 3

    .line 320
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->__isset_vector:[Z

    const/4 v1, 0x2

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetNotebookGuid()V
    .locals 1

    const/4 v0, 0x0

    .line 235
    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->notebookGuid:Ljava/lang/String;

    return-void
.end method

.method public unsetOrder()V
    .locals 2

    .line 169
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->__isset_vector:[Z

    const/4 v1, 0x0

    aput-boolean v1, v0, v1

    return-void
.end method

.method public unsetTagGuids()V
    .locals 1

    const/4 v0, 0x0

    .line 273
    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->tagGuids:Ljava/util/List;

    return-void
.end method

.method public unsetTimeZone()V
    .locals 1

    const/4 v0, 0x0

    .line 296
    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->timeZone:Ljava/lang/String;

    return-void
.end method

.method public unsetWords()V
    .locals 1

    const/4 v0, 0x0

    .line 212
    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->words:Ljava/lang/String;

    return-void
.end method

.method public validate()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    return-void
.end method

.method public write(Lcom/evernote/thrift/protocol/TProtocol;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 620
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteFilter;->validate()V

    .line 622
    sget-object v0, Lcom/evernote/edam/notestore/NoteFilter;->STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeStructBegin(Lcom/evernote/thrift/protocol/TStruct;)V

    .line 623
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteFilter;->isSetOrder()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 624
    sget-object v0, Lcom/evernote/edam/notestore/NoteFilter;->ORDER_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 625
    iget v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->order:I

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeI32(I)V

    .line 626
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 628
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteFilter;->isSetAscending()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 629
    sget-object v0, Lcom/evernote/edam/notestore/NoteFilter;->ASCENDING_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 630
    iget-boolean v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->ascending:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 631
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 633
    :cond_1
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->words:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 634
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteFilter;->isSetWords()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 635
    sget-object v0, Lcom/evernote/edam/notestore/NoteFilter;->WORDS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 636
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->words:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 637
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 640
    :cond_2
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->notebookGuid:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 641
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteFilter;->isSetNotebookGuid()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 642
    sget-object v0, Lcom/evernote/edam/notestore/NoteFilter;->NOTEBOOK_GUID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 643
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->notebookGuid:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 644
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 647
    :cond_3
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->tagGuids:Ljava/util/List;

    if-eqz v0, :cond_5

    .line 648
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteFilter;->isSetTagGuids()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 649
    sget-object v0, Lcom/evernote/edam/notestore/NoteFilter;->TAG_GUIDS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 651
    new-instance v0, Lcom/evernote/thrift/protocol/TList;

    const/16 v1, 0xb

    iget-object v2, p0, Lcom/evernote/edam/notestore/NoteFilter;->tagGuids:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/protocol/TList;-><init>(BI)V

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeListBegin(Lcom/evernote/thrift/protocol/TList;)V

    .line 652
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->tagGuids:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 654
    invoke-virtual {p1, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 656
    :cond_4
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeListEnd()V

    .line 658
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 661
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->timeZone:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 662
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteFilter;->isSetTimeZone()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 663
    sget-object v0, Lcom/evernote/edam/notestore/NoteFilter;->TIME_ZONE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 664
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->timeZone:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 665
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 668
    :cond_6
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteFilter;->isSetInactive()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 669
    sget-object v0, Lcom/evernote/edam/notestore/NoteFilter;->INACTIVE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 670
    iget-boolean v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->inactive:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 671
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 673
    :cond_7
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->emphasized:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 674
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteFilter;->isSetEmphasized()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 675
    sget-object v0, Lcom/evernote/edam/notestore/NoteFilter;->EMPHASIZED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 676
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteFilter;->emphasized:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 677
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 680
    :cond_8
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldStop()V

    .line 681
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeStructEnd()V

    return-void
.end method

.class public Lcom/evernote/edam/notestore/RelatedQuery;
.super Ljava/lang/Object;
.source "RelatedQuery.java"

# interfaces
.implements Lcom/evernote/thrift/TBase;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/evernote/thrift/TBase<",
        "Lcom/evernote/edam/notestore/RelatedQuery;",
        ">;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# static fields
.field private static final FILTER_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final NOTE_GUID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final PLAIN_TEXT_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final REFERENCE_URI_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;


# instance fields
.field private filter:Lcom/evernote/edam/notestore/NoteFilter;

.field private noteGuid:Ljava/lang/String;

.field private plainText:Ljava/lang/String;

.field private referenceUri:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 50
    new-instance v0, Lcom/evernote/thrift/protocol/TStruct;

    const-string v1, "RelatedQuery"

    invoke-direct {v0, v1}, Lcom/evernote/thrift/protocol/TStruct;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/evernote/edam/notestore/RelatedQuery;->STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

    .line 52
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "noteGuid"

    const/16 v2, 0xb

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/notestore/RelatedQuery;->NOTE_GUID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 53
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "plainText"

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/notestore/RelatedQuery;->PLAIN_TEXT_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 54
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "filter"

    const/16 v3, 0xc

    const/4 v4, 0x3

    invoke-direct {v0, v1, v3, v4}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/notestore/RelatedQuery;->FILTER_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 55
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "referenceUri"

    const/4 v3, 0x4

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/notestore/RelatedQuery;->REFERENCE_URI_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/evernote/edam/notestore/RelatedQuery;)V
    .locals 2

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/RelatedQuery;->isSetNoteGuid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p1, Lcom/evernote/edam/notestore/RelatedQuery;->noteGuid:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/notestore/RelatedQuery;->noteGuid:Ljava/lang/String;

    .line 75
    :cond_0
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/RelatedQuery;->isSetPlainText()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 76
    iget-object v0, p1, Lcom/evernote/edam/notestore/RelatedQuery;->plainText:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/notestore/RelatedQuery;->plainText:Ljava/lang/String;

    .line 78
    :cond_1
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/RelatedQuery;->isSetFilter()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 79
    new-instance v0, Lcom/evernote/edam/notestore/NoteFilter;

    iget-object v1, p1, Lcom/evernote/edam/notestore/RelatedQuery;->filter:Lcom/evernote/edam/notestore/NoteFilter;

    invoke-direct {v0, v1}, Lcom/evernote/edam/notestore/NoteFilter;-><init>(Lcom/evernote/edam/notestore/NoteFilter;)V

    iput-object v0, p0, Lcom/evernote/edam/notestore/RelatedQuery;->filter:Lcom/evernote/edam/notestore/NoteFilter;

    .line 81
    :cond_2
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/RelatedQuery;->isSetReferenceUri()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 82
    iget-object p1, p1, Lcom/evernote/edam/notestore/RelatedQuery;->referenceUri:Ljava/lang/String;

    iput-object p1, p0, Lcom/evernote/edam/notestore/RelatedQuery;->referenceUri:Ljava/lang/String;

    :cond_3
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    const/4 v0, 0x0

    .line 91
    iput-object v0, p0, Lcom/evernote/edam/notestore/RelatedQuery;->noteGuid:Ljava/lang/String;

    .line 92
    iput-object v0, p0, Lcom/evernote/edam/notestore/RelatedQuery;->plainText:Ljava/lang/String;

    .line 93
    iput-object v0, p0, Lcom/evernote/edam/notestore/RelatedQuery;->filter:Lcom/evernote/edam/notestore/NoteFilter;

    .line 94
    iput-object v0, p0, Lcom/evernote/edam/notestore/RelatedQuery;->referenceUri:Ljava/lang/String;

    return-void
.end method

.method public compareTo(Lcom/evernote/edam/notestore/RelatedQuery;)I
    .locals 2

    .line 247
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 248
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    return p1

    .line 254
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/RelatedQuery;->isSetNoteGuid()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/notestore/RelatedQuery;->isSetNoteGuid()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_1

    return v0

    .line 258
    :cond_1
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/RelatedQuery;->isSetNoteGuid()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/evernote/edam/notestore/RelatedQuery;->noteGuid:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/notestore/RelatedQuery;->noteGuid:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_2

    return v0

    .line 263
    :cond_2
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/RelatedQuery;->isSetPlainText()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/notestore/RelatedQuery;->isSetPlainText()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_3

    return v0

    .line 267
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/RelatedQuery;->isSetPlainText()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/evernote/edam/notestore/RelatedQuery;->plainText:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/notestore/RelatedQuery;->plainText:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_4

    return v0

    .line 272
    :cond_4
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/RelatedQuery;->isSetFilter()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/notestore/RelatedQuery;->isSetFilter()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_5

    return v0

    .line 276
    :cond_5
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/RelatedQuery;->isSetFilter()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/evernote/edam/notestore/RelatedQuery;->filter:Lcom/evernote/edam/notestore/NoteFilter;

    iget-object v1, p1, Lcom/evernote/edam/notestore/RelatedQuery;->filter:Lcom/evernote/edam/notestore/NoteFilter;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result v0

    if-eqz v0, :cond_6

    return v0

    .line 281
    :cond_6
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/RelatedQuery;->isSetReferenceUri()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/notestore/RelatedQuery;->isSetReferenceUri()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_7

    return v0

    .line 285
    :cond_7
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/RelatedQuery;->isSetReferenceUri()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/evernote/edam/notestore/RelatedQuery;->referenceUri:Ljava/lang/String;

    iget-object p1, p1, Lcom/evernote/edam/notestore/RelatedQuery;->referenceUri:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    if-eqz p1, :cond_8

    return p1

    :cond_8
    const/4 p1, 0x0

    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 49
    check-cast p1, Lcom/evernote/edam/notestore/RelatedQuery;

    invoke-virtual {p0, p1}, Lcom/evernote/edam/notestore/RelatedQuery;->compareTo(Lcom/evernote/edam/notestore/RelatedQuery;)I

    move-result p1

    return p1
.end method

.method public deepCopy()Lcom/evernote/edam/notestore/RelatedQuery;
    .locals 1

    .line 87
    new-instance v0, Lcom/evernote/edam/notestore/RelatedQuery;

    invoke-direct {v0, p0}, Lcom/evernote/edam/notestore/RelatedQuery;-><init>(Lcom/evernote/edam/notestore/RelatedQuery;)V

    return-object v0
.end method

.method public bridge synthetic deepCopy()Lcom/evernote/thrift/TBase;
    .locals 1

    .line 49
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/RelatedQuery;->deepCopy()Lcom/evernote/edam/notestore/RelatedQuery;

    move-result-object v0

    return-object v0
.end method

.method public equals(Lcom/evernote/edam/notestore/RelatedQuery;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 202
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/RelatedQuery;->isSetNoteGuid()Z

    move-result v1

    .line 203
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/RelatedQuery;->isSetNoteGuid()Z

    move-result v2

    if-nez v1, :cond_1

    if-eqz v2, :cond_3

    :cond_1
    if-eqz v1, :cond_10

    if-nez v2, :cond_2

    goto :goto_3

    .line 207
    :cond_2
    iget-object v1, p0, Lcom/evernote/edam/notestore/RelatedQuery;->noteGuid:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/notestore/RelatedQuery;->noteGuid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    return v0

    .line 211
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/RelatedQuery;->isSetPlainText()Z

    move-result v1

    .line 212
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/RelatedQuery;->isSetPlainText()Z

    move-result v2

    if-nez v1, :cond_4

    if-eqz v2, :cond_6

    :cond_4
    if-eqz v1, :cond_f

    if-nez v2, :cond_5

    goto :goto_2

    .line 216
    :cond_5
    iget-object v1, p0, Lcom/evernote/edam/notestore/RelatedQuery;->plainText:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/notestore/RelatedQuery;->plainText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    return v0

    .line 220
    :cond_6
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/RelatedQuery;->isSetFilter()Z

    move-result v1

    .line 221
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/RelatedQuery;->isSetFilter()Z

    move-result v2

    if-nez v1, :cond_7

    if-eqz v2, :cond_9

    :cond_7
    if-eqz v1, :cond_e

    if-nez v2, :cond_8

    goto :goto_1

    .line 225
    :cond_8
    iget-object v1, p0, Lcom/evernote/edam/notestore/RelatedQuery;->filter:Lcom/evernote/edam/notestore/NoteFilter;

    iget-object v2, p1, Lcom/evernote/edam/notestore/RelatedQuery;->filter:Lcom/evernote/edam/notestore/NoteFilter;

    invoke-virtual {v1, v2}, Lcom/evernote/edam/notestore/NoteFilter;->equals(Lcom/evernote/edam/notestore/NoteFilter;)Z

    move-result v1

    if-nez v1, :cond_9

    return v0

    .line 229
    :cond_9
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/RelatedQuery;->isSetReferenceUri()Z

    move-result v1

    .line 230
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/RelatedQuery;->isSetReferenceUri()Z

    move-result v2

    if-nez v1, :cond_a

    if-eqz v2, :cond_c

    :cond_a
    if-eqz v1, :cond_d

    if-nez v2, :cond_b

    goto :goto_0

    .line 234
    :cond_b
    iget-object v1, p0, Lcom/evernote/edam/notestore/RelatedQuery;->referenceUri:Ljava/lang/String;

    iget-object p1, p1, Lcom/evernote/edam/notestore/RelatedQuery;->referenceUri:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_c

    return v0

    :cond_c
    const/4 p1, 0x1

    return p1

    :cond_d
    :goto_0
    return v0

    :cond_e
    :goto_1
    return v0

    :cond_f
    :goto_2
    return v0

    :cond_10
    :goto_3
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 193
    :cond_0
    instance-of v1, p1, Lcom/evernote/edam/notestore/RelatedQuery;

    if-eqz v1, :cond_1

    .line 194
    check-cast p1, Lcom/evernote/edam/notestore/RelatedQuery;

    invoke-virtual {p0, p1}, Lcom/evernote/edam/notestore/RelatedQuery;->equals(Lcom/evernote/edam/notestore/RelatedQuery;)Z

    move-result p1

    return p1

    :cond_1
    return v0
.end method

.method public getFilter()Lcom/evernote/edam/notestore/NoteFilter;
    .locals 1

    .line 144
    iget-object v0, p0, Lcom/evernote/edam/notestore/RelatedQuery;->filter:Lcom/evernote/edam/notestore/NoteFilter;

    return-object v0
.end method

.method public getNoteGuid()Ljava/lang/String;
    .locals 1

    .line 98
    iget-object v0, p0, Lcom/evernote/edam/notestore/RelatedQuery;->noteGuid:Ljava/lang/String;

    return-object v0
.end method

.method public getPlainText()Ljava/lang/String;
    .locals 1

    .line 121
    iget-object v0, p0, Lcom/evernote/edam/notestore/RelatedQuery;->plainText:Ljava/lang/String;

    return-object v0
.end method

.method public getReferenceUri()Ljava/lang/String;
    .locals 1

    .line 167
    iget-object v0, p0, Lcom/evernote/edam/notestore/RelatedQuery;->referenceUri:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isSetFilter()Z
    .locals 1

    .line 157
    iget-object v0, p0, Lcom/evernote/edam/notestore/RelatedQuery;->filter:Lcom/evernote/edam/notestore/NoteFilter;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetNoteGuid()Z
    .locals 1

    .line 111
    iget-object v0, p0, Lcom/evernote/edam/notestore/RelatedQuery;->noteGuid:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetPlainText()Z
    .locals 1

    .line 134
    iget-object v0, p0, Lcom/evernote/edam/notestore/RelatedQuery;->plainText:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetReferenceUri()Z
    .locals 1

    .line 180
    iget-object v0, p0, Lcom/evernote/edam/notestore/RelatedQuery;->referenceUri:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public read(Lcom/evernote/thrift/protocol/TProtocol;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 295
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readStructBegin()Lcom/evernote/thrift/protocol/TStruct;

    .line 298
    :goto_0
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readFieldBegin()Lcom/evernote/thrift/protocol/TField;

    move-result-object v0

    .line 299
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-nez v1, :cond_0

    .line 337
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readStructEnd()V

    .line 338
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/RelatedQuery;->validate()V

    return-void

    .line 302
    :cond_0
    iget-short v1, v0, Lcom/evernote/thrift/protocol/TField;->id:S

    const/16 v2, 0xb

    packed-switch v1, :pswitch_data_0

    .line 333
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 326
    :pswitch_0
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v2, :cond_1

    .line 327
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/notestore/RelatedQuery;->referenceUri:Ljava/lang/String;

    goto :goto_1

    .line 329
    :cond_1
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 318
    :pswitch_1
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    const/16 v2, 0xc

    if-ne v1, v2, :cond_2

    .line 319
    new-instance v0, Lcom/evernote/edam/notestore/NoteFilter;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteFilter;-><init>()V

    iput-object v0, p0, Lcom/evernote/edam/notestore/RelatedQuery;->filter:Lcom/evernote/edam/notestore/NoteFilter;

    .line 320
    iget-object v0, p0, Lcom/evernote/edam/notestore/RelatedQuery;->filter:Lcom/evernote/edam/notestore/NoteFilter;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteFilter;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    goto :goto_1

    .line 322
    :cond_2
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 311
    :pswitch_2
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v2, :cond_3

    .line 312
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/notestore/RelatedQuery;->plainText:Ljava/lang/String;

    goto :goto_1

    .line 314
    :cond_3
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 304
    :pswitch_3
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v2, :cond_4

    .line 305
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/notestore/RelatedQuery;->noteGuid:Ljava/lang/String;

    goto :goto_1

    .line 307
    :cond_4
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    .line 335
    :goto_1
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readFieldEnd()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setFilter(Lcom/evernote/edam/notestore/NoteFilter;)V
    .locals 0

    .line 148
    iput-object p1, p0, Lcom/evernote/edam/notestore/RelatedQuery;->filter:Lcom/evernote/edam/notestore/NoteFilter;

    return-void
.end method

.method public setFilterIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 162
    iput-object p1, p0, Lcom/evernote/edam/notestore/RelatedQuery;->filter:Lcom/evernote/edam/notestore/NoteFilter;

    :cond_0
    return-void
.end method

.method public setNoteGuid(Ljava/lang/String;)V
    .locals 0

    .line 102
    iput-object p1, p0, Lcom/evernote/edam/notestore/RelatedQuery;->noteGuid:Ljava/lang/String;

    return-void
.end method

.method public setNoteGuidIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 116
    iput-object p1, p0, Lcom/evernote/edam/notestore/RelatedQuery;->noteGuid:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setPlainText(Ljava/lang/String;)V
    .locals 0

    .line 125
    iput-object p1, p0, Lcom/evernote/edam/notestore/RelatedQuery;->plainText:Ljava/lang/String;

    return-void
.end method

.method public setPlainTextIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 139
    iput-object p1, p0, Lcom/evernote/edam/notestore/RelatedQuery;->plainText:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setReferenceUri(Ljava/lang/String;)V
    .locals 0

    .line 171
    iput-object p1, p0, Lcom/evernote/edam/notestore/RelatedQuery;->referenceUri:Ljava/lang/String;

    return-void
.end method

.method public setReferenceUriIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 185
    iput-object p1, p0, Lcom/evernote/edam/notestore/RelatedQuery;->referenceUri:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 379
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "RelatedQuery("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 382
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/RelatedQuery;->isSetNoteGuid()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    const-string v1, "noteGuid:"

    .line 383
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 384
    iget-object v1, p0, Lcom/evernote/edam/notestore/RelatedQuery;->noteGuid:Ljava/lang/String;

    if-nez v1, :cond_0

    const-string v1, "null"

    .line 385
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 387
    :cond_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    const/4 v1, 0x1

    .line 391
    :goto_1
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/RelatedQuery;->isSetPlainText()Z

    move-result v3

    if-eqz v3, :cond_4

    if-nez v1, :cond_2

    const-string v1, ", "

    .line 392
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const-string v1, "plainText:"

    .line 393
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 394
    iget-object v1, p0, Lcom/evernote/edam/notestore/RelatedQuery;->plainText:Ljava/lang/String;

    if-nez v1, :cond_3

    const-string v1, "null"

    .line 395
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 397
    :cond_3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_2
    const/4 v1, 0x0

    .line 401
    :cond_4
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/RelatedQuery;->isSetFilter()Z

    move-result v3

    if-eqz v3, :cond_7

    if-nez v1, :cond_5

    const-string v1, ", "

    .line 402
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    const-string v1, "filter:"

    .line 403
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 404
    iget-object v1, p0, Lcom/evernote/edam/notestore/RelatedQuery;->filter:Lcom/evernote/edam/notestore/NoteFilter;

    if-nez v1, :cond_6

    const-string v1, "null"

    .line 405
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 407
    :cond_6
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :goto_3
    const/4 v1, 0x0

    .line 411
    :cond_7
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/RelatedQuery;->isSetReferenceUri()Z

    move-result v2

    if-eqz v2, :cond_a

    if-nez v1, :cond_8

    const-string v1, ", "

    .line 412
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_8
    const-string v1, "referenceUri:"

    .line 413
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 414
    iget-object v1, p0, Lcom/evernote/edam/notestore/RelatedQuery;->referenceUri:Ljava/lang/String;

    if-nez v1, :cond_9

    const-string v1, "null"

    .line 415
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 417
    :cond_9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_a
    :goto_4
    const-string v1, ")"

    .line 421
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public unsetFilter()V
    .locals 1

    const/4 v0, 0x0

    .line 152
    iput-object v0, p0, Lcom/evernote/edam/notestore/RelatedQuery;->filter:Lcom/evernote/edam/notestore/NoteFilter;

    return-void
.end method

.method public unsetNoteGuid()V
    .locals 1

    const/4 v0, 0x0

    .line 106
    iput-object v0, p0, Lcom/evernote/edam/notestore/RelatedQuery;->noteGuid:Ljava/lang/String;

    return-void
.end method

.method public unsetPlainText()V
    .locals 1

    const/4 v0, 0x0

    .line 129
    iput-object v0, p0, Lcom/evernote/edam/notestore/RelatedQuery;->plainText:Ljava/lang/String;

    return-void
.end method

.method public unsetReferenceUri()V
    .locals 1

    const/4 v0, 0x0

    .line 175
    iput-object v0, p0, Lcom/evernote/edam/notestore/RelatedQuery;->referenceUri:Ljava/lang/String;

    return-void
.end method

.method public validate()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    return-void
.end method

.method public write(Lcom/evernote/thrift/protocol/TProtocol;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 342
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/RelatedQuery;->validate()V

    .line 344
    sget-object v0, Lcom/evernote/edam/notestore/RelatedQuery;->STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeStructBegin(Lcom/evernote/thrift/protocol/TStruct;)V

    .line 345
    iget-object v0, p0, Lcom/evernote/edam/notestore/RelatedQuery;->noteGuid:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 346
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/RelatedQuery;->isSetNoteGuid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 347
    sget-object v0, Lcom/evernote/edam/notestore/RelatedQuery;->NOTE_GUID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 348
    iget-object v0, p0, Lcom/evernote/edam/notestore/RelatedQuery;->noteGuid:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 349
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 352
    :cond_0
    iget-object v0, p0, Lcom/evernote/edam/notestore/RelatedQuery;->plainText:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 353
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/RelatedQuery;->isSetPlainText()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 354
    sget-object v0, Lcom/evernote/edam/notestore/RelatedQuery;->PLAIN_TEXT_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 355
    iget-object v0, p0, Lcom/evernote/edam/notestore/RelatedQuery;->plainText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 356
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 359
    :cond_1
    iget-object v0, p0, Lcom/evernote/edam/notestore/RelatedQuery;->filter:Lcom/evernote/edam/notestore/NoteFilter;

    if-eqz v0, :cond_2

    .line 360
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/RelatedQuery;->isSetFilter()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 361
    sget-object v0, Lcom/evernote/edam/notestore/RelatedQuery;->FILTER_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 362
    iget-object v0, p0, Lcom/evernote/edam/notestore/RelatedQuery;->filter:Lcom/evernote/edam/notestore/NoteFilter;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteFilter;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 363
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 366
    :cond_2
    iget-object v0, p0, Lcom/evernote/edam/notestore/RelatedQuery;->referenceUri:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 367
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/RelatedQuery;->isSetReferenceUri()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 368
    sget-object v0, Lcom/evernote/edam/notestore/RelatedQuery;->REFERENCE_URI_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 369
    iget-object v0, p0, Lcom/evernote/edam/notestore/RelatedQuery;->referenceUri:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 370
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 373
    :cond_3
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldStop()V

    .line 374
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeStructEnd()V

    return-void
.end method

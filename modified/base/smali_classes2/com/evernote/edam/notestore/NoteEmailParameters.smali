.class public Lcom/evernote/edam/notestore/NoteEmailParameters;
.super Ljava/lang/Object;
.source "NoteEmailParameters.java"

# interfaces
.implements Lcom/evernote/thrift/TBase;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/evernote/thrift/TBase<",
        "Lcom/evernote/edam/notestore/NoteEmailParameters;",
        ">;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# static fields
.field private static final CC_ADDRESSES_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final GUID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final MESSAGE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final NOTE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

.field private static final SUBJECT_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final TO_ADDRESSES_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;


# instance fields
.field private ccAddresses:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private guid:Ljava/lang/String;

.field private message:Ljava/lang/String;

.field private note:Lcom/evernote/edam/type/Note;

.field private subject:Ljava/lang/String;

.field private toAddresses:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 67
    new-instance v0, Lcom/evernote/thrift/protocol/TStruct;

    const-string v1, "NoteEmailParameters"

    invoke-direct {v0, v1}, Lcom/evernote/thrift/protocol/TStruct;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/evernote/edam/notestore/NoteEmailParameters;->STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

    .line 69
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "guid"

    const/16 v2, 0xb

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/notestore/NoteEmailParameters;->GUID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 70
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "note"

    const/16 v3, 0xc

    const/4 v4, 0x2

    invoke-direct {v0, v1, v3, v4}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/notestore/NoteEmailParameters;->NOTE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 71
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "toAddresses"

    const/16 v3, 0xf

    const/4 v4, 0x3

    invoke-direct {v0, v1, v3, v4}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/notestore/NoteEmailParameters;->TO_ADDRESSES_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 72
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "ccAddresses"

    const/4 v4, 0x4

    invoke-direct {v0, v1, v3, v4}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/notestore/NoteEmailParameters;->CC_ADDRESSES_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 73
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "subject"

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/notestore/NoteEmailParameters;->SUBJECT_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 74
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "message"

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/notestore/NoteEmailParameters;->MESSAGE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/evernote/edam/notestore/NoteEmailParameters;)V
    .locals 3

    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteEmailParameters;->isSetGuid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p1, Lcom/evernote/edam/notestore/NoteEmailParameters;->guid:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->guid:Ljava/lang/String;

    .line 96
    :cond_0
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteEmailParameters;->isSetNote()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 97
    new-instance v0, Lcom/evernote/edam/type/Note;

    iget-object v1, p1, Lcom/evernote/edam/notestore/NoteEmailParameters;->note:Lcom/evernote/edam/type/Note;

    invoke-direct {v0, v1}, Lcom/evernote/edam/type/Note;-><init>(Lcom/evernote/edam/type/Note;)V

    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->note:Lcom/evernote/edam/type/Note;

    .line 99
    :cond_1
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteEmailParameters;->isSetToAddresses()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 100
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 101
    iget-object v1, p1, Lcom/evernote/edam/notestore/NoteEmailParameters;->toAddresses:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 102
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 104
    :cond_2
    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->toAddresses:Ljava/util/List;

    .line 106
    :cond_3
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteEmailParameters;->isSetCcAddresses()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 107
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 108
    iget-object v1, p1, Lcom/evernote/edam/notestore/NoteEmailParameters;->ccAddresses:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 109
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 111
    :cond_4
    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->ccAddresses:Ljava/util/List;

    .line 113
    :cond_5
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteEmailParameters;->isSetSubject()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 114
    iget-object v0, p1, Lcom/evernote/edam/notestore/NoteEmailParameters;->subject:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->subject:Ljava/lang/String;

    .line 116
    :cond_6
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteEmailParameters;->isSetMessage()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 117
    iget-object p1, p1, Lcom/evernote/edam/notestore/NoteEmailParameters;->message:Ljava/lang/String;

    iput-object p1, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->message:Ljava/lang/String;

    :cond_7
    return-void
.end method


# virtual methods
.method public addToCcAddresses(Ljava/lang/String;)V
    .locals 1

    .line 227
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->ccAddresses:Ljava/util/List;

    if-nez v0, :cond_0

    .line 228
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->ccAddresses:Ljava/util/List;

    .line 230
    :cond_0
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->ccAddresses:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public addToToAddresses(Ljava/lang/String;)V
    .locals 1

    .line 189
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->toAddresses:Ljava/util/List;

    if-nez v0, :cond_0

    .line 190
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->toAddresses:Ljava/util/List;

    .line 192
    :cond_0
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->toAddresses:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public clear()V
    .locals 1

    const/4 v0, 0x0

    .line 126
    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->guid:Ljava/lang/String;

    .line 127
    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->note:Lcom/evernote/edam/type/Note;

    .line 128
    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->toAddresses:Ljava/util/List;

    .line 129
    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->ccAddresses:Ljava/util/List;

    .line 130
    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->subject:Ljava/lang/String;

    .line 131
    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->message:Ljava/lang/String;

    return-void
.end method

.method public compareTo(Lcom/evernote/edam/notestore/NoteEmailParameters;)I
    .locals 2

    .line 378
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 379
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    return p1

    .line 385
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteEmailParameters;->isSetGuid()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteEmailParameters;->isSetGuid()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_1

    return v0

    .line 389
    :cond_1
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteEmailParameters;->isSetGuid()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->guid:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/notestore/NoteEmailParameters;->guid:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_2

    return v0

    .line 394
    :cond_2
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteEmailParameters;->isSetNote()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteEmailParameters;->isSetNote()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_3

    return v0

    .line 398
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteEmailParameters;->isSetNote()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->note:Lcom/evernote/edam/type/Note;

    iget-object v1, p1, Lcom/evernote/edam/notestore/NoteEmailParameters;->note:Lcom/evernote/edam/type/Note;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result v0

    if-eqz v0, :cond_4

    return v0

    .line 403
    :cond_4
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteEmailParameters;->isSetToAddresses()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteEmailParameters;->isSetToAddresses()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_5

    return v0

    .line 407
    :cond_5
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteEmailParameters;->isSetToAddresses()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->toAddresses:Ljava/util/List;

    iget-object v1, p1, Lcom/evernote/edam/notestore/NoteEmailParameters;->toAddresses:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/util/List;Ljava/util/List;)I

    move-result v0

    if-eqz v0, :cond_6

    return v0

    .line 412
    :cond_6
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteEmailParameters;->isSetCcAddresses()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteEmailParameters;->isSetCcAddresses()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_7

    return v0

    .line 416
    :cond_7
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteEmailParameters;->isSetCcAddresses()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->ccAddresses:Ljava/util/List;

    iget-object v1, p1, Lcom/evernote/edam/notestore/NoteEmailParameters;->ccAddresses:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/util/List;Ljava/util/List;)I

    move-result v0

    if-eqz v0, :cond_8

    return v0

    .line 421
    :cond_8
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteEmailParameters;->isSetSubject()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteEmailParameters;->isSetSubject()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_9

    return v0

    .line 425
    :cond_9
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteEmailParameters;->isSetSubject()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->subject:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/notestore/NoteEmailParameters;->subject:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_a

    return v0

    .line 430
    :cond_a
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteEmailParameters;->isSetMessage()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteEmailParameters;->isSetMessage()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_b

    return v0

    .line 434
    :cond_b
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteEmailParameters;->isSetMessage()Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->message:Ljava/lang/String;

    iget-object p1, p1, Lcom/evernote/edam/notestore/NoteEmailParameters;->message:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    if-eqz p1, :cond_c

    return p1

    :cond_c
    const/4 p1, 0x0

    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 66
    check-cast p1, Lcom/evernote/edam/notestore/NoteEmailParameters;

    invoke-virtual {p0, p1}, Lcom/evernote/edam/notestore/NoteEmailParameters;->compareTo(Lcom/evernote/edam/notestore/NoteEmailParameters;)I

    move-result p1

    return p1
.end method

.method public deepCopy()Lcom/evernote/edam/notestore/NoteEmailParameters;
    .locals 1

    .line 122
    new-instance v0, Lcom/evernote/edam/notestore/NoteEmailParameters;

    invoke-direct {v0, p0}, Lcom/evernote/edam/notestore/NoteEmailParameters;-><init>(Lcom/evernote/edam/notestore/NoteEmailParameters;)V

    return-object v0
.end method

.method public bridge synthetic deepCopy()Lcom/evernote/thrift/TBase;
    .locals 1

    .line 66
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteEmailParameters;->deepCopy()Lcom/evernote/edam/notestore/NoteEmailParameters;

    move-result-object v0

    return-object v0
.end method

.method public equals(Lcom/evernote/edam/notestore/NoteEmailParameters;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 315
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteEmailParameters;->isSetGuid()Z

    move-result v1

    .line 316
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteEmailParameters;->isSetGuid()Z

    move-result v2

    if-nez v1, :cond_1

    if-eqz v2, :cond_3

    :cond_1
    if-eqz v1, :cond_18

    if-nez v2, :cond_2

    goto/16 :goto_5

    .line 320
    :cond_2
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->guid:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/notestore/NoteEmailParameters;->guid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    return v0

    .line 324
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteEmailParameters;->isSetNote()Z

    move-result v1

    .line 325
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteEmailParameters;->isSetNote()Z

    move-result v2

    if-nez v1, :cond_4

    if-eqz v2, :cond_6

    :cond_4
    if-eqz v1, :cond_17

    if-nez v2, :cond_5

    goto/16 :goto_4

    .line 329
    :cond_5
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->note:Lcom/evernote/edam/type/Note;

    iget-object v2, p1, Lcom/evernote/edam/notestore/NoteEmailParameters;->note:Lcom/evernote/edam/type/Note;

    invoke-virtual {v1, v2}, Lcom/evernote/edam/type/Note;->equals(Lcom/evernote/edam/type/Note;)Z

    move-result v1

    if-nez v1, :cond_6

    return v0

    .line 333
    :cond_6
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteEmailParameters;->isSetToAddresses()Z

    move-result v1

    .line 334
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteEmailParameters;->isSetToAddresses()Z

    move-result v2

    if-nez v1, :cond_7

    if-eqz v2, :cond_9

    :cond_7
    if-eqz v1, :cond_16

    if-nez v2, :cond_8

    goto :goto_3

    .line 338
    :cond_8
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->toAddresses:Ljava/util/List;

    iget-object v2, p1, Lcom/evernote/edam/notestore/NoteEmailParameters;->toAddresses:Ljava/util/List;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    return v0

    .line 342
    :cond_9
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteEmailParameters;->isSetCcAddresses()Z

    move-result v1

    .line 343
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteEmailParameters;->isSetCcAddresses()Z

    move-result v2

    if-nez v1, :cond_a

    if-eqz v2, :cond_c

    :cond_a
    if-eqz v1, :cond_15

    if-nez v2, :cond_b

    goto :goto_2

    .line 347
    :cond_b
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->ccAddresses:Ljava/util/List;

    iget-object v2, p1, Lcom/evernote/edam/notestore/NoteEmailParameters;->ccAddresses:Ljava/util/List;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    return v0

    .line 351
    :cond_c
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteEmailParameters;->isSetSubject()Z

    move-result v1

    .line 352
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteEmailParameters;->isSetSubject()Z

    move-result v2

    if-nez v1, :cond_d

    if-eqz v2, :cond_f

    :cond_d
    if-eqz v1, :cond_14

    if-nez v2, :cond_e

    goto :goto_1

    .line 356
    :cond_e
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->subject:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/notestore/NoteEmailParameters;->subject:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    return v0

    .line 360
    :cond_f
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteEmailParameters;->isSetMessage()Z

    move-result v1

    .line 361
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteEmailParameters;->isSetMessage()Z

    move-result v2

    if-nez v1, :cond_10

    if-eqz v2, :cond_12

    :cond_10
    if-eqz v1, :cond_13

    if-nez v2, :cond_11

    goto :goto_0

    .line 365
    :cond_11
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->message:Ljava/lang/String;

    iget-object p1, p1, Lcom/evernote/edam/notestore/NoteEmailParameters;->message:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_12

    return v0

    :cond_12
    const/4 p1, 0x1

    return p1

    :cond_13
    :goto_0
    return v0

    :cond_14
    :goto_1
    return v0

    :cond_15
    :goto_2
    return v0

    :cond_16
    :goto_3
    return v0

    :cond_17
    :goto_4
    return v0

    :cond_18
    :goto_5
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 306
    :cond_0
    instance-of v1, p1, Lcom/evernote/edam/notestore/NoteEmailParameters;

    if-eqz v1, :cond_1

    .line 307
    check-cast p1, Lcom/evernote/edam/notestore/NoteEmailParameters;

    invoke-virtual {p0, p1}, Lcom/evernote/edam/notestore/NoteEmailParameters;->equals(Lcom/evernote/edam/notestore/NoteEmailParameters;)Z

    move-result p1

    return p1

    :cond_1
    return v0
.end method

.method public getCcAddresses()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 234
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->ccAddresses:Ljava/util/List;

    return-object v0
.end method

.method public getCcAddressesIterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 223
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->ccAddresses:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public getCcAddressesSize()I
    .locals 1

    .line 219
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->ccAddresses:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    return v0
.end method

.method public getGuid()Ljava/lang/String;
    .locals 1

    .line 135
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->guid:Ljava/lang/String;

    return-object v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    .line 280
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->message:Ljava/lang/String;

    return-object v0
.end method

.method public getNote()Lcom/evernote/edam/type/Note;
    .locals 1

    .line 158
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->note:Lcom/evernote/edam/type/Note;

    return-object v0
.end method

.method public getSubject()Ljava/lang/String;
    .locals 1

    .line 257
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->subject:Ljava/lang/String;

    return-object v0
.end method

.method public getToAddresses()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 196
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->toAddresses:Ljava/util/List;

    return-object v0
.end method

.method public getToAddressesIterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 185
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->toAddresses:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public getToAddressesSize()I
    .locals 1

    .line 181
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->toAddresses:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isSetCcAddresses()Z
    .locals 1

    .line 247
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->ccAddresses:Ljava/util/List;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetGuid()Z
    .locals 1

    .line 148
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->guid:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetMessage()Z
    .locals 1

    .line 293
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->message:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetNote()Z
    .locals 1

    .line 171
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->note:Lcom/evernote/edam/type/Note;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetSubject()Z
    .locals 1

    .line 270
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->subject:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetToAddresses()Z
    .locals 1

    .line 209
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->toAddresses:Ljava/util/List;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public read(Lcom/evernote/thrift/protocol/TProtocol;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 444
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readStructBegin()Lcom/evernote/thrift/protocol/TStruct;

    .line 447
    :goto_0
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readFieldBegin()Lcom/evernote/thrift/protocol/TField;

    move-result-object v0

    .line 448
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-nez v1, :cond_0

    .line 520
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readStructEnd()V

    .line 521
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteEmailParameters;->validate()V

    return-void

    .line 451
    :cond_0
    iget-short v1, v0, Lcom/evernote/thrift/protocol/TField;->id:S

    const/4 v2, 0x0

    const/16 v3, 0xf

    const/16 v4, 0xb

    packed-switch v1, :pswitch_data_0

    .line 516
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_3

    .line 509
    :pswitch_0
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_1

    .line 510
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->message:Ljava/lang/String;

    goto/16 :goto_3

    .line 512
    :cond_1
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_3

    .line 502
    :pswitch_1
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_2

    .line 503
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->subject:Ljava/lang/String;

    goto/16 :goto_3

    .line 505
    :cond_2
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_3

    .line 485
    :pswitch_2
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v3, :cond_4

    .line 487
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readListBegin()Lcom/evernote/thrift/protocol/TList;

    move-result-object v0

    .line 488
    new-instance v1, Ljava/util/ArrayList;

    iget v3, v0, Lcom/evernote/thrift/protocol/TList;->size:I

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->ccAddresses:Ljava/util/List;

    .line 489
    :goto_1
    iget v1, v0, Lcom/evernote/thrift/protocol/TList;->size:I

    if-ge v2, v1, :cond_3

    .line 492
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v1

    .line 493
    iget-object v3, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->ccAddresses:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 495
    :cond_3
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readListEnd()V

    goto :goto_3

    .line 498
    :cond_4
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_3

    .line 468
    :pswitch_3
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v3, :cond_6

    .line 470
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readListBegin()Lcom/evernote/thrift/protocol/TList;

    move-result-object v0

    .line 471
    new-instance v1, Ljava/util/ArrayList;

    iget v3, v0, Lcom/evernote/thrift/protocol/TList;->size:I

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->toAddresses:Ljava/util/List;

    .line 472
    :goto_2
    iget v1, v0, Lcom/evernote/thrift/protocol/TList;->size:I

    if-ge v2, v1, :cond_5

    .line 475
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v1

    .line 476
    iget-object v3, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->toAddresses:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 478
    :cond_5
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readListEnd()V

    goto :goto_3

    .line 481
    :cond_6
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_3

    .line 460
    :pswitch_4
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    const/16 v2, 0xc

    if-ne v1, v2, :cond_7

    .line 461
    new-instance v0, Lcom/evernote/edam/type/Note;

    invoke-direct {v0}, Lcom/evernote/edam/type/Note;-><init>()V

    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->note:Lcom/evernote/edam/type/Note;

    .line 462
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->note:Lcom/evernote/edam/type/Note;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/type/Note;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    goto :goto_3

    .line 464
    :cond_7
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_3

    .line 453
    :pswitch_5
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_8

    .line 454
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->guid:Ljava/lang/String;

    goto :goto_3

    .line 456
    :cond_8
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    .line 518
    :goto_3
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readFieldEnd()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setCcAddresses(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 238
    iput-object p1, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->ccAddresses:Ljava/util/List;

    return-void
.end method

.method public setCcAddressesIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 252
    iput-object p1, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->ccAddresses:Ljava/util/List;

    :cond_0
    return-void
.end method

.method public setGuid(Ljava/lang/String;)V
    .locals 0

    .line 139
    iput-object p1, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->guid:Ljava/lang/String;

    return-void
.end method

.method public setGuidIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 153
    iput-object p1, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->guid:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setMessage(Ljava/lang/String;)V
    .locals 0

    .line 284
    iput-object p1, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->message:Ljava/lang/String;

    return-void
.end method

.method public setMessageIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 298
    iput-object p1, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->message:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setNote(Lcom/evernote/edam/type/Note;)V
    .locals 0

    .line 162
    iput-object p1, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->note:Lcom/evernote/edam/type/Note;

    return-void
.end method

.method public setNoteIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 176
    iput-object p1, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->note:Lcom/evernote/edam/type/Note;

    :cond_0
    return-void
.end method

.method public setSubject(Ljava/lang/String;)V
    .locals 0

    .line 261
    iput-object p1, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->subject:Ljava/lang/String;

    return-void
.end method

.method public setSubjectIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 275
    iput-object p1, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->subject:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setToAddresses(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 200
    iput-object p1, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->toAddresses:Ljava/util/List;

    return-void
.end method

.method public setToAddressesIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 214
    iput-object p1, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->toAddresses:Ljava/util/List;

    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 590
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NoteEmailParameters("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 593
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteEmailParameters;->isSetGuid()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    const-string v1, "guid:"

    .line 594
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 595
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->guid:Ljava/lang/String;

    if-nez v1, :cond_0

    const-string v1, "null"

    .line 596
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 598
    :cond_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    const/4 v1, 0x1

    .line 602
    :goto_1
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteEmailParameters;->isSetNote()Z

    move-result v3

    if-eqz v3, :cond_4

    if-nez v1, :cond_2

    const-string v1, ", "

    .line 603
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const-string v1, "note:"

    .line 604
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 605
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->note:Lcom/evernote/edam/type/Note;

    if-nez v1, :cond_3

    const-string v1, "null"

    .line 606
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 608
    :cond_3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :goto_2
    const/4 v1, 0x0

    .line 612
    :cond_4
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteEmailParameters;->isSetToAddresses()Z

    move-result v3

    if-eqz v3, :cond_7

    if-nez v1, :cond_5

    const-string v1, ", "

    .line 613
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    const-string v1, "toAddresses:"

    .line 614
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 615
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->toAddresses:Ljava/util/List;

    if-nez v1, :cond_6

    const-string v1, "null"

    .line 616
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 618
    :cond_6
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :goto_3
    const/4 v1, 0x0

    .line 622
    :cond_7
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteEmailParameters;->isSetCcAddresses()Z

    move-result v3

    if-eqz v3, :cond_a

    if-nez v1, :cond_8

    const-string v1, ", "

    .line 623
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_8
    const-string v1, "ccAddresses:"

    .line 624
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 625
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->ccAddresses:Ljava/util/List;

    if-nez v1, :cond_9

    const-string v1, "null"

    .line 626
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 628
    :cond_9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :goto_4
    const/4 v1, 0x0

    .line 632
    :cond_a
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteEmailParameters;->isSetSubject()Z

    move-result v3

    if-eqz v3, :cond_d

    if-nez v1, :cond_b

    const-string v1, ", "

    .line 633
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_b
    const-string v1, "subject:"

    .line 634
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 635
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->subject:Ljava/lang/String;

    if-nez v1, :cond_c

    const-string v1, "null"

    .line 636
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5

    .line 638
    :cond_c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_5
    const/4 v1, 0x0

    .line 642
    :cond_d
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteEmailParameters;->isSetMessage()Z

    move-result v2

    if-eqz v2, :cond_10

    if-nez v1, :cond_e

    const-string v1, ", "

    .line 643
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_e
    const-string v1, "message:"

    .line 644
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 645
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->message:Ljava/lang/String;

    if-nez v1, :cond_f

    const-string v1, "null"

    .line 646
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6

    .line 648
    :cond_f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_10
    :goto_6
    const-string v1, ")"

    .line 652
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 653
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public unsetCcAddresses()V
    .locals 1

    const/4 v0, 0x0

    .line 242
    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->ccAddresses:Ljava/util/List;

    return-void
.end method

.method public unsetGuid()V
    .locals 1

    const/4 v0, 0x0

    .line 143
    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->guid:Ljava/lang/String;

    return-void
.end method

.method public unsetMessage()V
    .locals 1

    const/4 v0, 0x0

    .line 288
    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->message:Ljava/lang/String;

    return-void
.end method

.method public unsetNote()V
    .locals 1

    const/4 v0, 0x0

    .line 166
    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->note:Lcom/evernote/edam/type/Note;

    return-void
.end method

.method public unsetSubject()V
    .locals 1

    const/4 v0, 0x0

    .line 265
    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->subject:Ljava/lang/String;

    return-void
.end method

.method public unsetToAddresses()V
    .locals 1

    const/4 v0, 0x0

    .line 204
    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->toAddresses:Ljava/util/List;

    return-void
.end method

.method public validate()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    return-void
.end method

.method public write(Lcom/evernote/thrift/protocol/TProtocol;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 525
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteEmailParameters;->validate()V

    .line 527
    sget-object v0, Lcom/evernote/edam/notestore/NoteEmailParameters;->STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeStructBegin(Lcom/evernote/thrift/protocol/TStruct;)V

    .line 528
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->guid:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 529
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteEmailParameters;->isSetGuid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 530
    sget-object v0, Lcom/evernote/edam/notestore/NoteEmailParameters;->GUID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 531
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->guid:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 532
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 535
    :cond_0
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->note:Lcom/evernote/edam/type/Note;

    if-eqz v0, :cond_1

    .line 536
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteEmailParameters;->isSetNote()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 537
    sget-object v0, Lcom/evernote/edam/notestore/NoteEmailParameters;->NOTE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 538
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->note:Lcom/evernote/edam/type/Note;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/type/Note;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 539
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 542
    :cond_1
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->toAddresses:Ljava/util/List;

    const/16 v1, 0xb

    if-eqz v0, :cond_3

    .line 543
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteEmailParameters;->isSetToAddresses()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 544
    sget-object v0, Lcom/evernote/edam/notestore/NoteEmailParameters;->TO_ADDRESSES_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 546
    new-instance v0, Lcom/evernote/thrift/protocol/TList;

    iget-object v2, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->toAddresses:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/protocol/TList;-><init>(BI)V

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeListBegin(Lcom/evernote/thrift/protocol/TList;)V

    .line 547
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->toAddresses:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 549
    invoke-virtual {p1, v2}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 551
    :cond_2
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeListEnd()V

    .line 553
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 556
    :cond_3
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->ccAddresses:Ljava/util/List;

    if-eqz v0, :cond_5

    .line 557
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteEmailParameters;->isSetCcAddresses()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 558
    sget-object v0, Lcom/evernote/edam/notestore/NoteEmailParameters;->CC_ADDRESSES_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 560
    new-instance v0, Lcom/evernote/thrift/protocol/TList;

    iget-object v2, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->ccAddresses:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/protocol/TList;-><init>(BI)V

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeListBegin(Lcom/evernote/thrift/protocol/TList;)V

    .line 561
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->ccAddresses:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 563
    invoke-virtual {p1, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    goto :goto_1

    .line 565
    :cond_4
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeListEnd()V

    .line 567
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 570
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->subject:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 571
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteEmailParameters;->isSetSubject()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 572
    sget-object v0, Lcom/evernote/edam/notestore/NoteEmailParameters;->SUBJECT_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 573
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->subject:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 574
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 577
    :cond_6
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->message:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 578
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteEmailParameters;->isSetMessage()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 579
    sget-object v0, Lcom/evernote/edam/notestore/NoteEmailParameters;->MESSAGE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 580
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteEmailParameters;->message:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 581
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 584
    :cond_7
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldStop()V

    .line 585
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeStructEnd()V

    return-void
.end method

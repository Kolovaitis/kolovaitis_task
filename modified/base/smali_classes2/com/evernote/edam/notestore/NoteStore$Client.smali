.class public Lcom/evernote/edam/notestore/NoteStore$Client;
.super Ljava/lang/Object;
.source "NoteStore.java"

# interfaces
.implements Lcom/evernote/thrift/TServiceClient;
.implements Lcom/evernote/edam/notestore/NoteStore$Iface;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/evernote/edam/notestore/NoteStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Client"
.end annotation


# instance fields
.field protected iprot_:Lcom/evernote/thrift/protocol/TProtocol;

.field protected oprot_:Lcom/evernote/thrift/protocol/TProtocol;

.field protected seqid_:I


# direct methods
.method public constructor <init>(Lcom/evernote/thrift/protocol/TProtocol;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;-><init>(Lcom/evernote/thrift/protocol/TProtocol;Lcom/evernote/thrift/protocol/TProtocol;)V

    return-void
.end method

.method public constructor <init>(Lcom/evernote/thrift/protocol/TProtocol;Lcom/evernote/thrift/protocol/TProtocol;)V
    .locals 0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    .line 31
    iput-object p2, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    return-void
.end method


# virtual methods
.method public authenticateToSharedNote(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/edam/userstore/AuthenticationResult;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 3399
    invoke-virtual {p0, p1, p2, p3}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_authenticateToSharedNote(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3400
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_authenticateToSharedNote()Lcom/evernote/edam/userstore/AuthenticationResult;

    move-result-object p1

    return-object p1
.end method

.method public authenticateToSharedNotebook(Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/edam/userstore/AuthenticationResult;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 3176
    invoke-virtual {p0, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_authenticateToSharedNotebook(Ljava/lang/String;Ljava/lang/String;)V

    .line 3177
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_authenticateToSharedNotebook()Lcom/evernote/edam/userstore/AuthenticationResult;

    move-result-object p1

    return-object p1
.end method

.method public copyNote(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/edam/type/Note;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2011
    invoke-virtual {p0, p1, p2, p3}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_copyNote(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2012
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_copyNote()Lcom/evernote/edam/type/Note;

    move-result-object p1

    return-object p1
.end method

.method public createLinkedNotebook(Ljava/lang/String;Lcom/evernote/edam/type/LinkedNotebook;)Lcom/evernote/edam/type/LinkedNotebook;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2993
    invoke-virtual {p0, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_createLinkedNotebook(Ljava/lang/String;Lcom/evernote/edam/type/LinkedNotebook;)V

    .line 2994
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_createLinkedNotebook()Lcom/evernote/edam/type/LinkedNotebook;

    move-result-object p1

    return-object p1
.end method

.method public createNote(Ljava/lang/String;Lcom/evernote/edam/type/Note;)Lcom/evernote/edam/type/Note;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1739
    invoke-virtual {p0, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_createNote(Ljava/lang/String;Lcom/evernote/edam/type/Note;)V

    .line 1740
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_createNote()Lcom/evernote/edam/type/Note;

    move-result-object p1

    return-object p1
.end method

.method public createNotebook(Ljava/lang/String;Lcom/evernote/edam/type/Notebook;)Lcom/evernote/edam/type/Notebook;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 451
    invoke-virtual {p0, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_createNotebook(Ljava/lang/String;Lcom/evernote/edam/type/Notebook;)V

    .line 452
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_createNotebook()Lcom/evernote/edam/type/Notebook;

    move-result-object p1

    return-object p1
.end method

.method public createSearch(Ljava/lang/String;Lcom/evernote/edam/type/SavedSearch;)Lcom/evernote/edam/type/SavedSearch;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 989
    invoke-virtual {p0, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_createSearch(Ljava/lang/String;Lcom/evernote/edam/type/SavedSearch;)V

    .line 990
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_createSearch()Lcom/evernote/edam/type/SavedSearch;

    move-result-object p1

    return-object p1
.end method

.method public createSharedNotebook(Ljava/lang/String;Lcom/evernote/edam/type/SharedNotebook;)Lcom/evernote/edam/type/SharedNotebook;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2715
    invoke-virtual {p0, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_createSharedNotebook(Ljava/lang/String;Lcom/evernote/edam/type/SharedNotebook;)V

    .line 2716
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_createSharedNotebook()Lcom/evernote/edam/type/SharedNotebook;

    move-result-object p1

    return-object p1
.end method

.method public createTag(Ljava/lang/String;Lcom/evernote/edam/type/Tag;)Lcom/evernote/edam/type/Tag;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 720
    invoke-virtual {p0, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_createTag(Ljava/lang/String;Lcom/evernote/edam/type/Tag;)V

    .line 721
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_createTag()Lcom/evernote/edam/type/Tag;

    move-result-object p1

    return-object p1
.end method

.method public deleteNote(Ljava/lang/String;Ljava/lang/String;)I
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1831
    invoke-virtual {p0, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_deleteNote(Ljava/lang/String;Ljava/lang/String;)V

    .line 1832
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_deleteNote()I

    move-result p1

    return p1
.end method

.method public emailNote(Ljava/lang/String;Lcom/evernote/edam/notestore/NoteEmailParameters;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 3267
    invoke-virtual {p0, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_emailNote(Ljava/lang/String;Lcom/evernote/edam/notestore/NoteEmailParameters;)V

    .line 3268
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_emailNote()V

    return-void
.end method

.method public expungeInactiveNotes(Ljava/lang/String;)I
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1969
    invoke-virtual {p0, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_expungeInactiveNotes(Ljava/lang/String;)V

    .line 1970
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_expungeInactiveNotes()I

    move-result p1

    return p1
.end method

.method public expungeLinkedNotebook(Ljava/lang/String;Ljava/lang/String;)I
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 3130
    invoke-virtual {p0, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_expungeLinkedNotebook(Ljava/lang/String;Ljava/lang/String;)V

    .line 3131
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_expungeLinkedNotebook()I

    move-result p1

    return p1
.end method

.method public expungeNote(Ljava/lang/String;Ljava/lang/String;)I
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1877
    invoke-virtual {p0, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_expungeNote(Ljava/lang/String;Ljava/lang/String;)V

    .line 1878
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_expungeNote()I

    move-result p1

    return p1
.end method

.method public expungeNotebook(Ljava/lang/String;Ljava/lang/String;)I
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 540
    invoke-virtual {p0, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_expungeNotebook(Ljava/lang/String;Ljava/lang/String;)V

    .line 541
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_expungeNotebook()I

    move-result p1

    return p1
.end method

.method public expungeNotes(Ljava/lang/String;Ljava/util/List;)I
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1923
    invoke-virtual {p0, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_expungeNotes(Ljava/lang/String;Ljava/util/List;)V

    .line 1924
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_expungeNotes()I

    move-result p1

    return p1
.end method

.method public expungeSearch(Ljava/lang/String;Ljava/lang/String;)I
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1078
    invoke-virtual {p0, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_expungeSearch(Ljava/lang/String;Ljava/lang/String;)V

    .line 1079
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_expungeSearch()I

    move-result p1

    return p1
.end method

.method public expungeSharedNotebooks(Ljava/lang/String;Ljava/util/List;)I
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;)I"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2947
    invoke-virtual {p0, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_expungeSharedNotebooks(Ljava/lang/String;Ljava/util/List;)V

    .line 2948
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_expungeSharedNotebooks()I

    move-result p1

    return p1
.end method

.method public expungeTag(Ljava/lang/String;Ljava/lang/String;)I
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 855
    invoke-virtual {p0, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_expungeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 856
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_expungeTag()I

    move-result p1

    return p1
.end method

.method public findNoteCounts(Ljava/lang/String;Lcom/evernote/edam/notestore/NoteFilter;Z)Lcom/evernote/edam/notestore/NoteCollectionCounts;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1268
    invoke-virtual {p0, p1, p2, p3}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_findNoteCounts(Ljava/lang/String;Lcom/evernote/edam/notestore/NoteFilter;Z)V

    .line 1269
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_findNoteCounts()Lcom/evernote/edam/notestore/NoteCollectionCounts;

    move-result-object p1

    return-object p1
.end method

.method public findNoteOffset(Ljava/lang/String;Lcom/evernote/edam/notestore/NoteFilter;Ljava/lang/String;)I
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1172
    invoke-virtual {p0, p1, p2, p3}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_findNoteOffset(Ljava/lang/String;Lcom/evernote/edam/notestore/NoteFilter;Ljava/lang/String;)V

    .line 1173
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_findNoteOffset()I

    move-result p1

    return p1
.end method

.method public findNotes(Ljava/lang/String;Lcom/evernote/edam/notestore/NoteFilter;II)Lcom/evernote/edam/notestore/NoteList;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1124
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_findNotes(Ljava/lang/String;Lcom/evernote/edam/notestore/NoteFilter;II)V

    .line 1125
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_findNotes()Lcom/evernote/edam/notestore/NoteList;

    move-result-object p1

    return-object p1
.end method

.method public findNotesMetadata(Ljava/lang/String;Lcom/evernote/edam/notestore/NoteFilter;IILcom/evernote/edam/notestore/NotesMetadataResultSpec;)Lcom/evernote/edam/notestore/NotesMetadataList;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1219
    invoke-virtual/range {p0 .. p5}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_findNotesMetadata(Ljava/lang/String;Lcom/evernote/edam/notestore/NoteFilter;IILcom/evernote/edam/notestore/NotesMetadataResultSpec;)V

    .line 1220
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_findNotesMetadata()Lcom/evernote/edam/notestore/NotesMetadataList;

    move-result-object p1

    return-object p1
.end method

.method public findRelated(Ljava/lang/String;Lcom/evernote/edam/notestore/RelatedQuery;Lcom/evernote/edam/notestore/RelatedResultSpec;)Lcom/evernote/edam/notestore/RelatedResult;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 3446
    invoke-virtual {p0, p1, p2, p3}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_findRelated(Ljava/lang/String;Lcom/evernote/edam/notestore/RelatedQuery;Lcom/evernote/edam/notestore/RelatedResultSpec;)V

    .line 3447
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_findRelated()Lcom/evernote/edam/notestore/RelatedResult;

    move-result-object p1

    return-object p1
.end method

.method public getDefaultNotebook(Ljava/lang/String;)Lcom/evernote/edam/type/Notebook;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 409
    invoke-virtual {p0, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_getDefaultNotebook(Ljava/lang/String;)V

    .line 410
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_getDefaultNotebook()Lcom/evernote/edam/type/Notebook;

    move-result-object p1

    return-object p1
.end method

.method public getFilteredSyncChunk(Ljava/lang/String;IILcom/evernote/edam/notestore/SyncChunkFilter;)Lcom/evernote/edam/notestore/SyncChunk;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 181
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_getFilteredSyncChunk(Ljava/lang/String;IILcom/evernote/edam/notestore/SyncChunkFilter;)V

    .line 182
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_getFilteredSyncChunk()Lcom/evernote/edam/notestore/SyncChunk;

    move-result-object p1

    return-object p1
.end method

.method public getInputProtocol()Lcom/evernote/thrift/protocol/TProtocol;
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    return-object v0
.end method

.method public getLinkedNotebookSyncChunk(Ljava/lang/String;Lcom/evernote/edam/type/LinkedNotebook;IIZ)Lcom/evernote/edam/notestore/SyncChunk;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 272
    invoke-virtual/range {p0 .. p5}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_getLinkedNotebookSyncChunk(Ljava/lang/String;Lcom/evernote/edam/type/LinkedNotebook;IIZ)V

    .line 273
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_getLinkedNotebookSyncChunk()Lcom/evernote/edam/notestore/SyncChunk;

    move-result-object p1

    return-object p1
.end method

.method public getLinkedNotebookSyncState(Ljava/lang/String;Lcom/evernote/edam/type/LinkedNotebook;)Lcom/evernote/edam/notestore/SyncState;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 226
    invoke-virtual {p0, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_getLinkedNotebookSyncState(Ljava/lang/String;Lcom/evernote/edam/type/LinkedNotebook;)V

    .line 227
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_getLinkedNotebookSyncState()Lcom/evernote/edam/notestore/SyncState;

    move-result-object p1

    return-object p1
.end method

.method public getNote(Ljava/lang/String;Ljava/lang/String;ZZZZ)Lcom/evernote/edam/type/Note;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1315
    invoke-virtual/range {p0 .. p6}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_getNote(Ljava/lang/String;Ljava/lang/String;ZZZZ)V

    .line 1316
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_getNote()Lcom/evernote/edam/type/Note;

    move-result-object p1

    return-object p1
.end method

.method public getNoteApplicationData(Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/edam/type/LazyMap;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1365
    invoke-virtual {p0, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_getNoteApplicationData(Ljava/lang/String;Ljava/lang/String;)V

    .line 1366
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_getNoteApplicationData()Lcom/evernote/edam/type/LazyMap;

    move-result-object p1

    return-object p1
.end method

.method public getNoteApplicationDataEntry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1411
    invoke-virtual {p0, p1, p2, p3}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_getNoteApplicationDataEntry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1412
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_getNoteApplicationDataEntry()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getNoteContent(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1553
    invoke-virtual {p0, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_getNoteContent(Ljava/lang/String;Ljava/lang/String;)V

    .line 1554
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_getNoteContent()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getNoteSearchText(Ljava/lang/String;Ljava/lang/String;ZZ)Ljava/lang/String;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1599
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_getNoteSearchText(Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 1600
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_getNoteSearchText()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getNoteTagNames(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1693
    invoke-virtual {p0, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_getNoteTagNames(Ljava/lang/String;Ljava/lang/String;)V

    .line 1694
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_getNoteTagNames()Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public getNoteVersion(Ljava/lang/String;Ljava/lang/String;IZZZ)Lcom/evernote/edam/type/Note;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2104
    invoke-virtual/range {p0 .. p6}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_getNoteVersion(Ljava/lang/String;Ljava/lang/String;IZZZ)V

    .line 2105
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_getNoteVersion()Lcom/evernote/edam/type/Note;

    move-result-object p1

    return-object p1
.end method

.method public getNotebook(Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/edam/type/Notebook;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 363
    invoke-virtual {p0, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_getNotebook(Ljava/lang/String;Ljava/lang/String;)V

    .line 364
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_getNotebook()Lcom/evernote/edam/type/Notebook;

    move-result-object p1

    return-object p1
.end method

.method public getOutputProtocol()Lcom/evernote/thrift/protocol/TProtocol;
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    return-object v0
.end method

.method public getPublicNotebook(ILjava/lang/String;)Lcom/evernote/edam/type/Notebook;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2672
    invoke-virtual {p0, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_getPublicNotebook(ILjava/lang/String;)V

    .line 2673
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_getPublicNotebook()Lcom/evernote/edam/type/Notebook;

    move-result-object p1

    return-object p1
.end method

.method public getResource(Ljava/lang/String;Ljava/lang/String;ZZZZ)Lcom/evernote/edam/type/Resource;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2154
    invoke-virtual/range {p0 .. p6}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_getResource(Ljava/lang/String;Ljava/lang/String;ZZZZ)V

    .line 2155
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_getResource()Lcom/evernote/edam/type/Resource;

    move-result-object p1

    return-object p1
.end method

.method public getResourceAlternateData(Ljava/lang/String;Ljava/lang/String;)[B
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2580
    invoke-virtual {p0, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_getResourceAlternateData(Ljava/lang/String;Ljava/lang/String;)V

    .line 2581
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_getResourceAlternateData()[B

    move-result-object p1

    return-object p1
.end method

.method public getResourceApplicationData(Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/edam/type/LazyMap;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2204
    invoke-virtual {p0, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_getResourceApplicationData(Ljava/lang/String;Ljava/lang/String;)V

    .line 2205
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_getResourceApplicationData()Lcom/evernote/edam/type/LazyMap;

    move-result-object p1

    return-object p1
.end method

.method public getResourceApplicationDataEntry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2250
    invoke-virtual {p0, p1, p2, p3}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_getResourceApplicationDataEntry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2251
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_getResourceApplicationDataEntry()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getResourceAttributes(Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/edam/type/ResourceAttributes;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2626
    invoke-virtual {p0, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_getResourceAttributes(Ljava/lang/String;Ljava/lang/String;)V

    .line 2627
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_getResourceAttributes()Lcom/evernote/edam/type/ResourceAttributes;

    move-result-object p1

    return-object p1
.end method

.method public getResourceByHash(Ljava/lang/String;Ljava/lang/String;[BZZZ)Lcom/evernote/edam/type/Resource;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2484
    invoke-virtual/range {p0 .. p6}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_getResourceByHash(Ljava/lang/String;Ljava/lang/String;[BZZZ)V

    .line 2485
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_getResourceByHash()Lcom/evernote/edam/type/Resource;

    move-result-object p1

    return-object p1
.end method

.method public getResourceData(Ljava/lang/String;Ljava/lang/String;)[B
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2438
    invoke-virtual {p0, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_getResourceData(Ljava/lang/String;Ljava/lang/String;)V

    .line 2439
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_getResourceData()[B

    move-result-object p1

    return-object p1
.end method

.method public getResourceRecognition(Ljava/lang/String;Ljava/lang/String;)[B
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2534
    invoke-virtual {p0, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_getResourceRecognition(Ljava/lang/String;Ljava/lang/String;)V

    .line 2535
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_getResourceRecognition()[B

    move-result-object p1

    return-object p1
.end method

.method public getResourceSearchText(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1647
    invoke-virtual {p0, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_getResourceSearchText(Ljava/lang/String;Ljava/lang/String;)V

    .line 1648
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_getResourceSearchText()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getSearch(Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/edam/type/SavedSearch;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 943
    invoke-virtual {p0, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_getSearch(Ljava/lang/String;Ljava/lang/String;)V

    .line 944
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_getSearch()Lcom/evernote/edam/type/SavedSearch;

    move-result-object p1

    return-object p1
.end method

.method public getSharedNotebookByAuth(Ljava/lang/String;)Lcom/evernote/edam/type/SharedNotebook;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 3222
    invoke-virtual {p0, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_getSharedNotebookByAuth(Ljava/lang/String;)V

    .line 3223
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_getSharedNotebookByAuth()Lcom/evernote/edam/type/SharedNotebook;

    move-result-object p1

    return-object p1
.end method

.method public getSyncChunk(Ljava/lang/String;IIZ)Lcom/evernote/edam/notestore/SyncChunk;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 136
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_getSyncChunk(Ljava/lang/String;IIZ)V

    .line 137
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_getSyncChunk()Lcom/evernote/edam/notestore/SyncChunk;

    move-result-object p1

    return-object p1
.end method

.method public getSyncState(Ljava/lang/String;)Lcom/evernote/edam/notestore/SyncState;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 51
    invoke-virtual {p0, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_getSyncState(Ljava/lang/String;)V

    .line 52
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_getSyncState()Lcom/evernote/edam/notestore/SyncState;

    move-result-object p1

    return-object p1
.end method

.method public getSyncStateWithMetrics(Ljava/lang/String;Lcom/evernote/edam/notestore/ClientUsageMetrics;)Lcom/evernote/edam/notestore/SyncState;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 93
    invoke-virtual {p0, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_getSyncStateWithMetrics(Ljava/lang/String;Lcom/evernote/edam/notestore/ClientUsageMetrics;)V

    .line 94
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_getSyncStateWithMetrics()Lcom/evernote/edam/notestore/SyncState;

    move-result-object p1

    return-object p1
.end method

.method public getTag(Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/edam/type/Tag;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 674
    invoke-virtual {p0, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_getTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 675
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_getTag()Lcom/evernote/edam/type/Tag;

    move-result-object p1

    return-object p1
.end method

.method public listLinkedNotebooks(Ljava/lang/String;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/evernote/edam/type/LinkedNotebook;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 3085
    invoke-virtual {p0, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_listLinkedNotebooks(Ljava/lang/String;)V

    .line 3086
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_listLinkedNotebooks()Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public listNoteVersions(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/evernote/edam/notestore/NoteVersionId;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2058
    invoke-virtual {p0, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_listNoteVersions(Ljava/lang/String;Ljava/lang/String;)V

    .line 2059
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_listNoteVersions()Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public listNotebooks(Ljava/lang/String;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/evernote/edam/type/Notebook;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 321
    invoke-virtual {p0, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_listNotebooks(Ljava/lang/String;)V

    .line 322
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_listNotebooks()Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public listSearches(Ljava/lang/String;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/evernote/edam/type/SavedSearch;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 901
    invoke-virtual {p0, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_listSearches(Ljava/lang/String;)V

    .line 902
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_listSearches()Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public listSharedNotebooks(Ljava/lang/String;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/evernote/edam/type/SharedNotebook;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2902
    invoke-virtual {p0, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_listSharedNotebooks(Ljava/lang/String;)V

    .line 2903
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_listSharedNotebooks()Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public listTags(Ljava/lang/String;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/evernote/edam/type/Tag;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 586
    invoke-virtual {p0, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_listTags(Ljava/lang/String;)V

    .line 587
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_listTags()Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public listTagsByNotebook(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/evernote/edam/type/Tag;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 628
    invoke-virtual {p0, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_listTagsByNotebook(Ljava/lang/String;Ljava/lang/String;)V

    .line 629
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_listTagsByNotebook()Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public recv_authenticateToSharedNote()Lcom/evernote/edam/userstore/AuthenticationResult;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 3417
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 3418
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 3423
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 3426
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$authenticateToSharedNote_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$authenticateToSharedNote_result;-><init>()V

    .line 3427
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$authenticateToSharedNote_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 3428
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 3429
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$authenticateToSharedNote_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3430
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$authenticateToSharedNote_result;->access$27700(Lcom/evernote/edam/notestore/NoteStore$authenticateToSharedNote_result;)Lcom/evernote/edam/userstore/AuthenticationResult;

    move-result-object v0

    return-object v0

    .line 3432
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$authenticateToSharedNote_result;->access$27800(Lcom/evernote/edam/notestore/NoteStore$authenticateToSharedNote_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 3435
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$authenticateToSharedNote_result;->access$27900(Lcom/evernote/edam/notestore/NoteStore$authenticateToSharedNote_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 3438
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$authenticateToSharedNote_result;->access$28000(Lcom/evernote/edam/notestore/NoteStore$authenticateToSharedNote_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 3439
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$authenticateToSharedNote_result;->access$28000(Lcom/evernote/edam/notestore/NoteStore$authenticateToSharedNote_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 3441
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "authenticateToSharedNote failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 3436
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$authenticateToSharedNote_result;->access$27900(Lcom/evernote/edam/notestore/NoteStore$authenticateToSharedNote_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 3433
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$authenticateToSharedNote_result;->access$27800(Lcom/evernote/edam/notestore/NoteStore$authenticateToSharedNote_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 3424
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "authenticateToSharedNote failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 3419
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 3420
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 3421
    throw v0
.end method

.method public recv_authenticateToSharedNotebook()Lcom/evernote/edam/userstore/AuthenticationResult;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 3193
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 3194
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 3199
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 3202
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$authenticateToSharedNotebook_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$authenticateToSharedNotebook_result;-><init>()V

    .line 3203
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$authenticateToSharedNotebook_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 3204
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 3205
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$authenticateToSharedNotebook_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3206
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$authenticateToSharedNotebook_result;->access$25900(Lcom/evernote/edam/notestore/NoteStore$authenticateToSharedNotebook_result;)Lcom/evernote/edam/userstore/AuthenticationResult;

    move-result-object v0

    return-object v0

    .line 3208
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$authenticateToSharedNotebook_result;->access$26000(Lcom/evernote/edam/notestore/NoteStore$authenticateToSharedNotebook_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 3211
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$authenticateToSharedNotebook_result;->access$26100(Lcom/evernote/edam/notestore/NoteStore$authenticateToSharedNotebook_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 3214
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$authenticateToSharedNotebook_result;->access$26200(Lcom/evernote/edam/notestore/NoteStore$authenticateToSharedNotebook_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 3215
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$authenticateToSharedNotebook_result;->access$26200(Lcom/evernote/edam/notestore/NoteStore$authenticateToSharedNotebook_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 3217
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "authenticateToSharedNotebook failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 3212
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$authenticateToSharedNotebook_result;->access$26100(Lcom/evernote/edam/notestore/NoteStore$authenticateToSharedNotebook_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 3209
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$authenticateToSharedNotebook_result;->access$26000(Lcom/evernote/edam/notestore/NoteStore$authenticateToSharedNotebook_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 3200
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "authenticateToSharedNotebook failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 3195
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 3196
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 3197
    throw v0
.end method

.method public recv_copyNote()Lcom/evernote/edam/type/Note;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2029
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 2030
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 2035
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 2038
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$copyNote_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$copyNote_result;-><init>()V

    .line 2039
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$copyNote_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 2040
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 2041
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$copyNote_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2042
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$copyNote_result;->access$16000(Lcom/evernote/edam/notestore/NoteStore$copyNote_result;)Lcom/evernote/edam/type/Note;

    move-result-object v0

    return-object v0

    .line 2044
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$copyNote_result;->access$16100(Lcom/evernote/edam/notestore/NoteStore$copyNote_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 2047
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$copyNote_result;->access$16200(Lcom/evernote/edam/notestore/NoteStore$copyNote_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 2050
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$copyNote_result;->access$16300(Lcom/evernote/edam/notestore/NoteStore$copyNote_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2051
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$copyNote_result;->access$16300(Lcom/evernote/edam/notestore/NoteStore$copyNote_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 2053
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "copyNote failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2048
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$copyNote_result;->access$16200(Lcom/evernote/edam/notestore/NoteStore$copyNote_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 2045
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$copyNote_result;->access$16100(Lcom/evernote/edam/notestore/NoteStore$copyNote_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 2036
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "copyNote failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2031
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 2032
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 2033
    throw v0
.end method

.method public recv_createLinkedNotebook()Lcom/evernote/edam/type/LinkedNotebook;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 3010
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 3011
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 3016
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 3019
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$createLinkedNotebook_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$createLinkedNotebook_result;-><init>()V

    .line 3020
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$createLinkedNotebook_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 3021
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 3022
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$createLinkedNotebook_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3023
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$createLinkedNotebook_result;->access$24300(Lcom/evernote/edam/notestore/NoteStore$createLinkedNotebook_result;)Lcom/evernote/edam/type/LinkedNotebook;

    move-result-object v0

    return-object v0

    .line 3025
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$createLinkedNotebook_result;->access$24400(Lcom/evernote/edam/notestore/NoteStore$createLinkedNotebook_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 3028
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$createLinkedNotebook_result;->access$24500(Lcom/evernote/edam/notestore/NoteStore$createLinkedNotebook_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 3031
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$createLinkedNotebook_result;->access$24600(Lcom/evernote/edam/notestore/NoteStore$createLinkedNotebook_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 3032
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$createLinkedNotebook_result;->access$24600(Lcom/evernote/edam/notestore/NoteStore$createLinkedNotebook_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 3034
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "createLinkedNotebook failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 3029
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$createLinkedNotebook_result;->access$24500(Lcom/evernote/edam/notestore/NoteStore$createLinkedNotebook_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 3026
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$createLinkedNotebook_result;->access$24400(Lcom/evernote/edam/notestore/NoteStore$createLinkedNotebook_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 3017
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "createLinkedNotebook failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 3012
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 3013
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 3014
    throw v0
.end method

.method public recv_createNote()Lcom/evernote/edam/type/Note;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1756
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 1757
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 1762
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 1765
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$createNote_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$createNote_result;-><init>()V

    .line 1766
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$createNote_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1767
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 1768
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$createNote_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1769
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$createNote_result;->access$13700(Lcom/evernote/edam/notestore/NoteStore$createNote_result;)Lcom/evernote/edam/type/Note;

    move-result-object v0

    return-object v0

    .line 1771
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$createNote_result;->access$13800(Lcom/evernote/edam/notestore/NoteStore$createNote_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 1774
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$createNote_result;->access$13900(Lcom/evernote/edam/notestore/NoteStore$createNote_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 1777
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$createNote_result;->access$14000(Lcom/evernote/edam/notestore/NoteStore$createNote_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1778
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$createNote_result;->access$14000(Lcom/evernote/edam/notestore/NoteStore$createNote_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 1780
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "createNote failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1775
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$createNote_result;->access$13900(Lcom/evernote/edam/notestore/NoteStore$createNote_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 1772
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$createNote_result;->access$13800(Lcom/evernote/edam/notestore/NoteStore$createNote_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 1763
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "createNote failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1758
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 1759
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 1760
    throw v0
.end method

.method public recv_createNotebook()Lcom/evernote/edam/type/Notebook;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 468
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 469
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_4

    .line 474
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_3

    .line 477
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$createNotebook_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$createNotebook_result;-><init>()V

    .line 478
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$createNotebook_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 479
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 480
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$createNotebook_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 481
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$createNotebook_result;->access$3000(Lcom/evernote/edam/notestore/NoteStore$createNotebook_result;)Lcom/evernote/edam/type/Notebook;

    move-result-object v0

    return-object v0

    .line 483
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$createNotebook_result;->access$3100(Lcom/evernote/edam/notestore/NoteStore$createNotebook_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 486
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$createNotebook_result;->access$3200(Lcom/evernote/edam/notestore/NoteStore$createNotebook_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 487
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$createNotebook_result;->access$3200(Lcom/evernote/edam/notestore/NoteStore$createNotebook_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 489
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "createNotebook failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 484
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$createNotebook_result;->access$3100(Lcom/evernote/edam/notestore/NoteStore$createNotebook_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 475
    :cond_3
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "createNotebook failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 470
    :cond_4
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 471
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 472
    throw v0
.end method

.method public recv_createSearch()Lcom/evernote/edam/type/SavedSearch;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1006
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 1007
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_4

    .line 1012
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_3

    .line 1015
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$createSearch_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$createSearch_result;-><init>()V

    .line 1016
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$createSearch_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1017
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 1018
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$createSearch_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1019
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$createSearch_result;->access$7400(Lcom/evernote/edam/notestore/NoteStore$createSearch_result;)Lcom/evernote/edam/type/SavedSearch;

    move-result-object v0

    return-object v0

    .line 1021
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$createSearch_result;->access$7500(Lcom/evernote/edam/notestore/NoteStore$createSearch_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 1024
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$createSearch_result;->access$7600(Lcom/evernote/edam/notestore/NoteStore$createSearch_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1025
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$createSearch_result;->access$7600(Lcom/evernote/edam/notestore/NoteStore$createSearch_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 1027
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "createSearch failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1022
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$createSearch_result;->access$7500(Lcom/evernote/edam/notestore/NoteStore$createSearch_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 1013
    :cond_3
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "createSearch failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1008
    :cond_4
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 1009
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 1010
    throw v0
.end method

.method public recv_createSharedNotebook()Lcom/evernote/edam/type/SharedNotebook;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2732
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 2733
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 2738
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 2741
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$createSharedNotebook_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$createSharedNotebook_result;-><init>()V

    .line 2742
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$createSharedNotebook_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 2743
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 2744
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$createSharedNotebook_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2745
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$createSharedNotebook_result;->access$21900(Lcom/evernote/edam/notestore/NoteStore$createSharedNotebook_result;)Lcom/evernote/edam/type/SharedNotebook;

    move-result-object v0

    return-object v0

    .line 2747
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$createSharedNotebook_result;->access$22000(Lcom/evernote/edam/notestore/NoteStore$createSharedNotebook_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 2750
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$createSharedNotebook_result;->access$22100(Lcom/evernote/edam/notestore/NoteStore$createSharedNotebook_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 2753
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$createSharedNotebook_result;->access$22200(Lcom/evernote/edam/notestore/NoteStore$createSharedNotebook_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2754
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$createSharedNotebook_result;->access$22200(Lcom/evernote/edam/notestore/NoteStore$createSharedNotebook_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 2756
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "createSharedNotebook failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2751
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$createSharedNotebook_result;->access$22100(Lcom/evernote/edam/notestore/NoteStore$createSharedNotebook_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 2748
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$createSharedNotebook_result;->access$22000(Lcom/evernote/edam/notestore/NoteStore$createSharedNotebook_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 2739
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "createSharedNotebook failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2734
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 2735
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 2736
    throw v0
.end method

.method public recv_createTag()Lcom/evernote/edam/type/Tag;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 737
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 738
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 743
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 746
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$createTag_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$createTag_result;-><init>()V

    .line 747
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$createTag_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 748
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 749
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$createTag_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 750
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$createTag_result;->access$5200(Lcom/evernote/edam/notestore/NoteStore$createTag_result;)Lcom/evernote/edam/type/Tag;

    move-result-object v0

    return-object v0

    .line 752
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$createTag_result;->access$5300(Lcom/evernote/edam/notestore/NoteStore$createTag_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 755
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$createTag_result;->access$5400(Lcom/evernote/edam/notestore/NoteStore$createTag_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 758
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$createTag_result;->access$5500(Lcom/evernote/edam/notestore/NoteStore$createTag_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 759
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$createTag_result;->access$5500(Lcom/evernote/edam/notestore/NoteStore$createTag_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 761
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "createTag failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 756
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$createTag_result;->access$5400(Lcom/evernote/edam/notestore/NoteStore$createTag_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 753
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$createTag_result;->access$5300(Lcom/evernote/edam/notestore/NoteStore$createTag_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 744
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "createTag failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 739
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 740
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 741
    throw v0
.end method

.method public recv_deleteNote()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1848
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 1849
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 1854
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 1857
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$deleteNote_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$deleteNote_result;-><init>()V

    .line 1858
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$deleteNote_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1859
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 1860
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$deleteNote_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1861
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$deleteNote_result;->access$14500(Lcom/evernote/edam/notestore/NoteStore$deleteNote_result;)I

    move-result v0

    return v0

    .line 1863
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$deleteNote_result;->access$14600(Lcom/evernote/edam/notestore/NoteStore$deleteNote_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 1866
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$deleteNote_result;->access$14700(Lcom/evernote/edam/notestore/NoteStore$deleteNote_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 1869
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$deleteNote_result;->access$14800(Lcom/evernote/edam/notestore/NoteStore$deleteNote_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1870
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$deleteNote_result;->access$14800(Lcom/evernote/edam/notestore/NoteStore$deleteNote_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 1872
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "deleteNote failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1867
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$deleteNote_result;->access$14700(Lcom/evernote/edam/notestore/NoteStore$deleteNote_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 1864
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$deleteNote_result;->access$14600(Lcom/evernote/edam/notestore/NoteStore$deleteNote_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 1855
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "deleteNote failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1850
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 1851
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 1852
    throw v0
.end method

.method public recv_emailNote()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 3284
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 3285
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_4

    .line 3290
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_3

    .line 3293
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$emailNote_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$emailNote_result;-><init>()V

    .line 3294
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$emailNote_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 3295
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 3296
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$emailNote_result;->access$26700(Lcom/evernote/edam/notestore/NoteStore$emailNote_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 3299
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$emailNote_result;->access$26800(Lcom/evernote/edam/notestore/NoteStore$emailNote_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-nez v1, :cond_1

    .line 3302
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$emailNote_result;->access$26900(Lcom/evernote/edam/notestore/NoteStore$emailNote_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_0

    return-void

    .line 3303
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$emailNote_result;->access$26900(Lcom/evernote/edam/notestore/NoteStore$emailNote_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 3300
    :cond_1
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$emailNote_result;->access$26800(Lcom/evernote/edam/notestore/NoteStore$emailNote_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 3297
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$emailNote_result;->access$26700(Lcom/evernote/edam/notestore/NoteStore$emailNote_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 3291
    :cond_3
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "emailNote failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 3286
    :cond_4
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 3287
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 3288
    throw v0
.end method

.method public recv_expungeInactiveNotes()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1985
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 1986
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_4

    .line 1991
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_3

    .line 1994
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$expungeInactiveNotes_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeInactiveNotes_result;-><init>()V

    .line 1995
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$expungeInactiveNotes_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1996
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 1997
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeInactiveNotes_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1998
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeInactiveNotes_result;->access$15700(Lcom/evernote/edam/notestore/NoteStore$expungeInactiveNotes_result;)I

    move-result v0

    return v0

    .line 2000
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeInactiveNotes_result;->access$15800(Lcom/evernote/edam/notestore/NoteStore$expungeInactiveNotes_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 2003
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeInactiveNotes_result;->access$15900(Lcom/evernote/edam/notestore/NoteStore$expungeInactiveNotes_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2004
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeInactiveNotes_result;->access$15900(Lcom/evernote/edam/notestore/NoteStore$expungeInactiveNotes_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 2006
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "expungeInactiveNotes failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2001
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeInactiveNotes_result;->access$15800(Lcom/evernote/edam/notestore/NoteStore$expungeInactiveNotes_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 1992
    :cond_3
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "expungeInactiveNotes failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1987
    :cond_4
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 1988
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 1989
    throw v0
.end method

.method public recv_expungeLinkedNotebook()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 3147
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 3148
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 3153
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 3156
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$expungeLinkedNotebook_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeLinkedNotebook_result;-><init>()V

    .line 3157
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$expungeLinkedNotebook_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 3158
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 3159
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeLinkedNotebook_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3160
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeLinkedNotebook_result;->access$25500(Lcom/evernote/edam/notestore/NoteStore$expungeLinkedNotebook_result;)I

    move-result v0

    return v0

    .line 3162
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeLinkedNotebook_result;->access$25600(Lcom/evernote/edam/notestore/NoteStore$expungeLinkedNotebook_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 3165
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeLinkedNotebook_result;->access$25700(Lcom/evernote/edam/notestore/NoteStore$expungeLinkedNotebook_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 3168
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeLinkedNotebook_result;->access$25800(Lcom/evernote/edam/notestore/NoteStore$expungeLinkedNotebook_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 3169
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeLinkedNotebook_result;->access$25800(Lcom/evernote/edam/notestore/NoteStore$expungeLinkedNotebook_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 3171
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "expungeLinkedNotebook failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 3166
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeLinkedNotebook_result;->access$25700(Lcom/evernote/edam/notestore/NoteStore$expungeLinkedNotebook_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 3163
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeLinkedNotebook_result;->access$25600(Lcom/evernote/edam/notestore/NoteStore$expungeLinkedNotebook_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 3154
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "expungeLinkedNotebook failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 3149
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 3150
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 3151
    throw v0
.end method

.method public recv_expungeNote()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1894
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 1895
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 1900
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 1903
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$expungeNote_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeNote_result;-><init>()V

    .line 1904
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$expungeNote_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1905
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 1906
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeNote_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1907
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeNote_result;->access$14900(Lcom/evernote/edam/notestore/NoteStore$expungeNote_result;)I

    move-result v0

    return v0

    .line 1909
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeNote_result;->access$15000(Lcom/evernote/edam/notestore/NoteStore$expungeNote_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 1912
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeNote_result;->access$15100(Lcom/evernote/edam/notestore/NoteStore$expungeNote_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 1915
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeNote_result;->access$15200(Lcom/evernote/edam/notestore/NoteStore$expungeNote_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1916
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeNote_result;->access$15200(Lcom/evernote/edam/notestore/NoteStore$expungeNote_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 1918
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "expungeNote failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1913
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeNote_result;->access$15100(Lcom/evernote/edam/notestore/NoteStore$expungeNote_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 1910
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeNote_result;->access$15000(Lcom/evernote/edam/notestore/NoteStore$expungeNote_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 1901
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "expungeNote failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1896
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 1897
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 1898
    throw v0
.end method

.method public recv_expungeNotebook()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 557
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 558
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 563
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 566
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$expungeNotebook_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeNotebook_result;-><init>()V

    .line 567
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$expungeNotebook_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 568
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 569
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeNotebook_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 570
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeNotebook_result;->access$3700(Lcom/evernote/edam/notestore/NoteStore$expungeNotebook_result;)I

    move-result v0

    return v0

    .line 572
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeNotebook_result;->access$3800(Lcom/evernote/edam/notestore/NoteStore$expungeNotebook_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 575
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeNotebook_result;->access$3900(Lcom/evernote/edam/notestore/NoteStore$expungeNotebook_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 578
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeNotebook_result;->access$4000(Lcom/evernote/edam/notestore/NoteStore$expungeNotebook_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 579
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeNotebook_result;->access$4000(Lcom/evernote/edam/notestore/NoteStore$expungeNotebook_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 581
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "expungeNotebook failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 576
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeNotebook_result;->access$3900(Lcom/evernote/edam/notestore/NoteStore$expungeNotebook_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 573
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeNotebook_result;->access$3800(Lcom/evernote/edam/notestore/NoteStore$expungeNotebook_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 564
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "expungeNotebook failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 559
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 560
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 561
    throw v0
.end method

.method public recv_expungeNotes()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1940
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 1941
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 1946
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 1949
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$expungeNotes_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeNotes_result;-><init>()V

    .line 1950
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$expungeNotes_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1951
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 1952
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeNotes_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1953
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeNotes_result;->access$15300(Lcom/evernote/edam/notestore/NoteStore$expungeNotes_result;)I

    move-result v0

    return v0

    .line 1955
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeNotes_result;->access$15400(Lcom/evernote/edam/notestore/NoteStore$expungeNotes_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 1958
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeNotes_result;->access$15500(Lcom/evernote/edam/notestore/NoteStore$expungeNotes_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 1961
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeNotes_result;->access$15600(Lcom/evernote/edam/notestore/NoteStore$expungeNotes_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1962
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeNotes_result;->access$15600(Lcom/evernote/edam/notestore/NoteStore$expungeNotes_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 1964
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "expungeNotes failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1959
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeNotes_result;->access$15500(Lcom/evernote/edam/notestore/NoteStore$expungeNotes_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 1956
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeNotes_result;->access$15400(Lcom/evernote/edam/notestore/NoteStore$expungeNotes_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 1947
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "expungeNotes failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1942
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 1943
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 1944
    throw v0
.end method

.method public recv_expungeSearch()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1095
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 1096
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 1101
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 1104
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$expungeSearch_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeSearch_result;-><init>()V

    .line 1105
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$expungeSearch_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1106
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 1107
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeSearch_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1108
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeSearch_result;->access$8100(Lcom/evernote/edam/notestore/NoteStore$expungeSearch_result;)I

    move-result v0

    return v0

    .line 1110
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeSearch_result;->access$8200(Lcom/evernote/edam/notestore/NoteStore$expungeSearch_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 1113
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeSearch_result;->access$8300(Lcom/evernote/edam/notestore/NoteStore$expungeSearch_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 1116
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeSearch_result;->access$8400(Lcom/evernote/edam/notestore/NoteStore$expungeSearch_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1117
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeSearch_result;->access$8400(Lcom/evernote/edam/notestore/NoteStore$expungeSearch_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 1119
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "expungeSearch failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1114
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeSearch_result;->access$8300(Lcom/evernote/edam/notestore/NoteStore$expungeSearch_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 1111
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeSearch_result;->access$8200(Lcom/evernote/edam/notestore/NoteStore$expungeSearch_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 1102
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "expungeSearch failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1097
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 1098
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 1099
    throw v0
.end method

.method public recv_expungeSharedNotebooks()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2964
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 2965
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 2970
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 2973
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$expungeSharedNotebooks_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeSharedNotebooks_result;-><init>()V

    .line 2974
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$expungeSharedNotebooks_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 2975
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 2976
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeSharedNotebooks_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2977
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeSharedNotebooks_result;->access$23900(Lcom/evernote/edam/notestore/NoteStore$expungeSharedNotebooks_result;)I

    move-result v0

    return v0

    .line 2979
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeSharedNotebooks_result;->access$24000(Lcom/evernote/edam/notestore/NoteStore$expungeSharedNotebooks_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 2982
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeSharedNotebooks_result;->access$24100(Lcom/evernote/edam/notestore/NoteStore$expungeSharedNotebooks_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 2985
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeSharedNotebooks_result;->access$24200(Lcom/evernote/edam/notestore/NoteStore$expungeSharedNotebooks_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2986
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeSharedNotebooks_result;->access$24200(Lcom/evernote/edam/notestore/NoteStore$expungeSharedNotebooks_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 2988
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "expungeSharedNotebooks failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2983
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeSharedNotebooks_result;->access$24100(Lcom/evernote/edam/notestore/NoteStore$expungeSharedNotebooks_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 2980
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeSharedNotebooks_result;->access$24000(Lcom/evernote/edam/notestore/NoteStore$expungeSharedNotebooks_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 2971
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "expungeSharedNotebooks failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2966
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 2967
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 2968
    throw v0
.end method

.method public recv_expungeTag()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 872
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 873
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 878
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 881
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$expungeTag_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeTag_result;-><init>()V

    .line 882
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$expungeTag_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 883
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 884
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeTag_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 885
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeTag_result;->access$6300(Lcom/evernote/edam/notestore/NoteStore$expungeTag_result;)I

    move-result v0

    return v0

    .line 887
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeTag_result;->access$6400(Lcom/evernote/edam/notestore/NoteStore$expungeTag_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 890
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeTag_result;->access$6500(Lcom/evernote/edam/notestore/NoteStore$expungeTag_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 893
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeTag_result;->access$6600(Lcom/evernote/edam/notestore/NoteStore$expungeTag_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 894
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeTag_result;->access$6600(Lcom/evernote/edam/notestore/NoteStore$expungeTag_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 896
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "expungeTag failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 891
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeTag_result;->access$6500(Lcom/evernote/edam/notestore/NoteStore$expungeTag_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 888
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeTag_result;->access$6400(Lcom/evernote/edam/notestore/NoteStore$expungeTag_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 879
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "expungeTag failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 874
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 875
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 876
    throw v0
.end method

.method public recv_findNoteCounts()Lcom/evernote/edam/notestore/NoteCollectionCounts;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1286
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 1287
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 1292
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 1295
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$findNoteCounts_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$findNoteCounts_result;-><init>()V

    .line 1296
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$findNoteCounts_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1297
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 1298
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$findNoteCounts_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1299
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$findNoteCounts_result;->access$9700(Lcom/evernote/edam/notestore/NoteStore$findNoteCounts_result;)Lcom/evernote/edam/notestore/NoteCollectionCounts;

    move-result-object v0

    return-object v0

    .line 1301
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$findNoteCounts_result;->access$9800(Lcom/evernote/edam/notestore/NoteStore$findNoteCounts_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 1304
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$findNoteCounts_result;->access$9900(Lcom/evernote/edam/notestore/NoteStore$findNoteCounts_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 1307
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$findNoteCounts_result;->access$10000(Lcom/evernote/edam/notestore/NoteStore$findNoteCounts_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1308
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$findNoteCounts_result;->access$10000(Lcom/evernote/edam/notestore/NoteStore$findNoteCounts_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 1310
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "findNoteCounts failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1305
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$findNoteCounts_result;->access$9900(Lcom/evernote/edam/notestore/NoteStore$findNoteCounts_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 1302
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$findNoteCounts_result;->access$9800(Lcom/evernote/edam/notestore/NoteStore$findNoteCounts_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 1293
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "findNoteCounts failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1288
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 1289
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 1290
    throw v0
.end method

.method public recv_findNoteOffset()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1190
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 1191
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 1196
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 1199
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$findNoteOffset_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$findNoteOffset_result;-><init>()V

    .line 1200
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$findNoteOffset_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1201
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 1202
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$findNoteOffset_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1203
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$findNoteOffset_result;->access$8900(Lcom/evernote/edam/notestore/NoteStore$findNoteOffset_result;)I

    move-result v0

    return v0

    .line 1205
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$findNoteOffset_result;->access$9000(Lcom/evernote/edam/notestore/NoteStore$findNoteOffset_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 1208
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$findNoteOffset_result;->access$9100(Lcom/evernote/edam/notestore/NoteStore$findNoteOffset_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 1211
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$findNoteOffset_result;->access$9200(Lcom/evernote/edam/notestore/NoteStore$findNoteOffset_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1212
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$findNoteOffset_result;->access$9200(Lcom/evernote/edam/notestore/NoteStore$findNoteOffset_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 1214
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "findNoteOffset failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1209
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$findNoteOffset_result;->access$9100(Lcom/evernote/edam/notestore/NoteStore$findNoteOffset_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 1206
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$findNoteOffset_result;->access$9000(Lcom/evernote/edam/notestore/NoteStore$findNoteOffset_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 1197
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "findNoteOffset failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1192
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 1193
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 1194
    throw v0
.end method

.method public recv_findNotes()Lcom/evernote/edam/notestore/NoteList;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1143
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 1144
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 1149
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 1152
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$findNotes_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$findNotes_result;-><init>()V

    .line 1153
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$findNotes_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1154
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 1155
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$findNotes_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1156
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$findNotes_result;->access$8500(Lcom/evernote/edam/notestore/NoteStore$findNotes_result;)Lcom/evernote/edam/notestore/NoteList;

    move-result-object v0

    return-object v0

    .line 1158
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$findNotes_result;->access$8600(Lcom/evernote/edam/notestore/NoteStore$findNotes_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 1161
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$findNotes_result;->access$8700(Lcom/evernote/edam/notestore/NoteStore$findNotes_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 1164
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$findNotes_result;->access$8800(Lcom/evernote/edam/notestore/NoteStore$findNotes_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1165
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$findNotes_result;->access$8800(Lcom/evernote/edam/notestore/NoteStore$findNotes_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 1167
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "findNotes failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1162
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$findNotes_result;->access$8700(Lcom/evernote/edam/notestore/NoteStore$findNotes_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 1159
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$findNotes_result;->access$8600(Lcom/evernote/edam/notestore/NoteStore$findNotes_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 1150
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "findNotes failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1145
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 1146
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 1147
    throw v0
.end method

.method public recv_findNotesMetadata()Lcom/evernote/edam/notestore/NotesMetadataList;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1239
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 1240
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 1245
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 1248
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$findNotesMetadata_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$findNotesMetadata_result;-><init>()V

    .line 1249
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$findNotesMetadata_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1250
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 1251
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$findNotesMetadata_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1252
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$findNotesMetadata_result;->access$9300(Lcom/evernote/edam/notestore/NoteStore$findNotesMetadata_result;)Lcom/evernote/edam/notestore/NotesMetadataList;

    move-result-object v0

    return-object v0

    .line 1254
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$findNotesMetadata_result;->access$9400(Lcom/evernote/edam/notestore/NoteStore$findNotesMetadata_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 1257
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$findNotesMetadata_result;->access$9500(Lcom/evernote/edam/notestore/NoteStore$findNotesMetadata_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 1260
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$findNotesMetadata_result;->access$9600(Lcom/evernote/edam/notestore/NoteStore$findNotesMetadata_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1261
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$findNotesMetadata_result;->access$9600(Lcom/evernote/edam/notestore/NoteStore$findNotesMetadata_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 1263
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "findNotesMetadata failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1258
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$findNotesMetadata_result;->access$9500(Lcom/evernote/edam/notestore/NoteStore$findNotesMetadata_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 1255
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$findNotesMetadata_result;->access$9400(Lcom/evernote/edam/notestore/NoteStore$findNotesMetadata_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 1246
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "findNotesMetadata failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1241
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 1242
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 1243
    throw v0
.end method

.method public recv_findRelated()Lcom/evernote/edam/notestore/RelatedResult;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 3464
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 3465
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 3470
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 3473
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$findRelated_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$findRelated_result;-><init>()V

    .line 3474
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$findRelated_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 3475
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 3476
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$findRelated_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3477
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$findRelated_result;->access$28100(Lcom/evernote/edam/notestore/NoteStore$findRelated_result;)Lcom/evernote/edam/notestore/RelatedResult;

    move-result-object v0

    return-object v0

    .line 3479
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$findRelated_result;->access$28200(Lcom/evernote/edam/notestore/NoteStore$findRelated_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 3482
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$findRelated_result;->access$28300(Lcom/evernote/edam/notestore/NoteStore$findRelated_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 3485
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$findRelated_result;->access$28400(Lcom/evernote/edam/notestore/NoteStore$findRelated_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 3486
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$findRelated_result;->access$28400(Lcom/evernote/edam/notestore/NoteStore$findRelated_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 3488
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "findRelated failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 3483
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$findRelated_result;->access$28300(Lcom/evernote/edam/notestore/NoteStore$findRelated_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 3480
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$findRelated_result;->access$28200(Lcom/evernote/edam/notestore/NoteStore$findRelated_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 3471
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "findRelated failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 3466
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 3467
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 3468
    throw v0
.end method

.method public recv_getDefaultNotebook()Lcom/evernote/edam/type/Notebook;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 425
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 426
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_4

    .line 431
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_3

    .line 434
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getDefaultNotebook_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getDefaultNotebook_result;-><init>()V

    .line 435
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$getDefaultNotebook_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 436
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 437
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$getDefaultNotebook_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 438
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getDefaultNotebook_result;->access$2700(Lcom/evernote/edam/notestore/NoteStore$getDefaultNotebook_result;)Lcom/evernote/edam/type/Notebook;

    move-result-object v0

    return-object v0

    .line 440
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getDefaultNotebook_result;->access$2800(Lcom/evernote/edam/notestore/NoteStore$getDefaultNotebook_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 443
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getDefaultNotebook_result;->access$2900(Lcom/evernote/edam/notestore/NoteStore$getDefaultNotebook_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 444
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getDefaultNotebook_result;->access$2900(Lcom/evernote/edam/notestore/NoteStore$getDefaultNotebook_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 446
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "getDefaultNotebook failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 441
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getDefaultNotebook_result;->access$2800(Lcom/evernote/edam/notestore/NoteStore$getDefaultNotebook_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 432
    :cond_3
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "getDefaultNotebook failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 427
    :cond_4
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 428
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 429
    throw v0
.end method

.method public recv_getFilteredSyncChunk()Lcom/evernote/edam/notestore/SyncChunk;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 200
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 201
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_4

    .line 206
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_3

    .line 209
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getFilteredSyncChunk_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getFilteredSyncChunk_result;-><init>()V

    .line 210
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$getFilteredSyncChunk_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 211
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 212
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$getFilteredSyncChunk_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 213
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getFilteredSyncChunk_result;->access$900(Lcom/evernote/edam/notestore/NoteStore$getFilteredSyncChunk_result;)Lcom/evernote/edam/notestore/SyncChunk;

    move-result-object v0

    return-object v0

    .line 215
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getFilteredSyncChunk_result;->access$1000(Lcom/evernote/edam/notestore/NoteStore$getFilteredSyncChunk_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 218
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getFilteredSyncChunk_result;->access$1100(Lcom/evernote/edam/notestore/NoteStore$getFilteredSyncChunk_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 219
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getFilteredSyncChunk_result;->access$1100(Lcom/evernote/edam/notestore/NoteStore$getFilteredSyncChunk_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 221
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "getFilteredSyncChunk failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 216
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getFilteredSyncChunk_result;->access$1000(Lcom/evernote/edam/notestore/NoteStore$getFilteredSyncChunk_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 207
    :cond_3
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "getFilteredSyncChunk failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 202
    :cond_4
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 203
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 204
    throw v0
.end method

.method public recv_getLinkedNotebookSyncChunk()Lcom/evernote/edam/notestore/SyncChunk;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 292
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 293
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 298
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 301
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getLinkedNotebookSyncChunk_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getLinkedNotebookSyncChunk_result;-><init>()V

    .line 302
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$getLinkedNotebookSyncChunk_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 303
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 304
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$getLinkedNotebookSyncChunk_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 305
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getLinkedNotebookSyncChunk_result;->access$1600(Lcom/evernote/edam/notestore/NoteStore$getLinkedNotebookSyncChunk_result;)Lcom/evernote/edam/notestore/SyncChunk;

    move-result-object v0

    return-object v0

    .line 307
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getLinkedNotebookSyncChunk_result;->access$1700(Lcom/evernote/edam/notestore/NoteStore$getLinkedNotebookSyncChunk_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 310
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getLinkedNotebookSyncChunk_result;->access$1800(Lcom/evernote/edam/notestore/NoteStore$getLinkedNotebookSyncChunk_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 313
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getLinkedNotebookSyncChunk_result;->access$1900(Lcom/evernote/edam/notestore/NoteStore$getLinkedNotebookSyncChunk_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 314
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getLinkedNotebookSyncChunk_result;->access$1900(Lcom/evernote/edam/notestore/NoteStore$getLinkedNotebookSyncChunk_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 316
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "getLinkedNotebookSyncChunk failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 311
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getLinkedNotebookSyncChunk_result;->access$1800(Lcom/evernote/edam/notestore/NoteStore$getLinkedNotebookSyncChunk_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 308
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getLinkedNotebookSyncChunk_result;->access$1700(Lcom/evernote/edam/notestore/NoteStore$getLinkedNotebookSyncChunk_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 299
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "getLinkedNotebookSyncChunk failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 294
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 295
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 296
    throw v0
.end method

.method public recv_getLinkedNotebookSyncState()Lcom/evernote/edam/notestore/SyncState;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 243
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 244
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 249
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 252
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getLinkedNotebookSyncState_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getLinkedNotebookSyncState_result;-><init>()V

    .line 253
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$getLinkedNotebookSyncState_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 254
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 255
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$getLinkedNotebookSyncState_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 256
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getLinkedNotebookSyncState_result;->access$1200(Lcom/evernote/edam/notestore/NoteStore$getLinkedNotebookSyncState_result;)Lcom/evernote/edam/notestore/SyncState;

    move-result-object v0

    return-object v0

    .line 258
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getLinkedNotebookSyncState_result;->access$1300(Lcom/evernote/edam/notestore/NoteStore$getLinkedNotebookSyncState_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 261
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getLinkedNotebookSyncState_result;->access$1400(Lcom/evernote/edam/notestore/NoteStore$getLinkedNotebookSyncState_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 264
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getLinkedNotebookSyncState_result;->access$1500(Lcom/evernote/edam/notestore/NoteStore$getLinkedNotebookSyncState_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 265
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getLinkedNotebookSyncState_result;->access$1500(Lcom/evernote/edam/notestore/NoteStore$getLinkedNotebookSyncState_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 267
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "getLinkedNotebookSyncState failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 262
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getLinkedNotebookSyncState_result;->access$1400(Lcom/evernote/edam/notestore/NoteStore$getLinkedNotebookSyncState_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 259
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getLinkedNotebookSyncState_result;->access$1300(Lcom/evernote/edam/notestore/NoteStore$getLinkedNotebookSyncState_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 250
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "getLinkedNotebookSyncState failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 245
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 246
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 247
    throw v0
.end method

.method public recv_getNote()Lcom/evernote/edam/type/Note;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1336
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 1337
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 1342
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 1345
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getNote_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getNote_result;-><init>()V

    .line 1346
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$getNote_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1347
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 1348
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$getNote_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1349
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNote_result;->access$10100(Lcom/evernote/edam/notestore/NoteStore$getNote_result;)Lcom/evernote/edam/type/Note;

    move-result-object v0

    return-object v0

    .line 1351
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNote_result;->access$10200(Lcom/evernote/edam/notestore/NoteStore$getNote_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 1354
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNote_result;->access$10300(Lcom/evernote/edam/notestore/NoteStore$getNote_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 1357
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNote_result;->access$10400(Lcom/evernote/edam/notestore/NoteStore$getNote_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1358
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNote_result;->access$10400(Lcom/evernote/edam/notestore/NoteStore$getNote_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 1360
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "getNote failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1355
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNote_result;->access$10300(Lcom/evernote/edam/notestore/NoteStore$getNote_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 1352
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNote_result;->access$10200(Lcom/evernote/edam/notestore/NoteStore$getNote_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 1343
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "getNote failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1338
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 1339
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 1340
    throw v0
.end method

.method public recv_getNoteApplicationData()Lcom/evernote/edam/type/LazyMap;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1382
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 1383
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 1388
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 1391
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getNoteApplicationData_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteApplicationData_result;-><init>()V

    .line 1392
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$getNoteApplicationData_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1393
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 1394
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteApplicationData_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1395
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteApplicationData_result;->access$10500(Lcom/evernote/edam/notestore/NoteStore$getNoteApplicationData_result;)Lcom/evernote/edam/type/LazyMap;

    move-result-object v0

    return-object v0

    .line 1397
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteApplicationData_result;->access$10600(Lcom/evernote/edam/notestore/NoteStore$getNoteApplicationData_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 1400
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteApplicationData_result;->access$10700(Lcom/evernote/edam/notestore/NoteStore$getNoteApplicationData_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 1403
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteApplicationData_result;->access$10800(Lcom/evernote/edam/notestore/NoteStore$getNoteApplicationData_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1404
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteApplicationData_result;->access$10800(Lcom/evernote/edam/notestore/NoteStore$getNoteApplicationData_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 1406
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "getNoteApplicationData failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1401
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteApplicationData_result;->access$10700(Lcom/evernote/edam/notestore/NoteStore$getNoteApplicationData_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 1398
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteApplicationData_result;->access$10600(Lcom/evernote/edam/notestore/NoteStore$getNoteApplicationData_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 1389
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "getNoteApplicationData failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1384
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 1385
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 1386
    throw v0
.end method

.method public recv_getNoteApplicationDataEntry()Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1429
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 1430
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 1435
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 1438
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getNoteApplicationDataEntry_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteApplicationDataEntry_result;-><init>()V

    .line 1439
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$getNoteApplicationDataEntry_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1440
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 1441
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteApplicationDataEntry_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1442
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteApplicationDataEntry_result;->access$10900(Lcom/evernote/edam/notestore/NoteStore$getNoteApplicationDataEntry_result;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1444
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteApplicationDataEntry_result;->access$11000(Lcom/evernote/edam/notestore/NoteStore$getNoteApplicationDataEntry_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 1447
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteApplicationDataEntry_result;->access$11100(Lcom/evernote/edam/notestore/NoteStore$getNoteApplicationDataEntry_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 1450
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteApplicationDataEntry_result;->access$11200(Lcom/evernote/edam/notestore/NoteStore$getNoteApplicationDataEntry_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1451
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteApplicationDataEntry_result;->access$11200(Lcom/evernote/edam/notestore/NoteStore$getNoteApplicationDataEntry_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 1453
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "getNoteApplicationDataEntry failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1448
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteApplicationDataEntry_result;->access$11100(Lcom/evernote/edam/notestore/NoteStore$getNoteApplicationDataEntry_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 1445
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteApplicationDataEntry_result;->access$11000(Lcom/evernote/edam/notestore/NoteStore$getNoteApplicationDataEntry_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 1436
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "getNoteApplicationDataEntry failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1431
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 1432
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 1433
    throw v0
.end method

.method public recv_getNoteContent()Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1570
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 1571
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 1576
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 1579
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getNoteContent_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteContent_result;-><init>()V

    .line 1580
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$getNoteContent_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1581
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 1582
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteContent_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1583
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteContent_result;->access$12100(Lcom/evernote/edam/notestore/NoteStore$getNoteContent_result;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1585
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteContent_result;->access$12200(Lcom/evernote/edam/notestore/NoteStore$getNoteContent_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 1588
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteContent_result;->access$12300(Lcom/evernote/edam/notestore/NoteStore$getNoteContent_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 1591
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteContent_result;->access$12400(Lcom/evernote/edam/notestore/NoteStore$getNoteContent_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1592
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteContent_result;->access$12400(Lcom/evernote/edam/notestore/NoteStore$getNoteContent_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 1594
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "getNoteContent failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1589
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteContent_result;->access$12300(Lcom/evernote/edam/notestore/NoteStore$getNoteContent_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 1586
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteContent_result;->access$12200(Lcom/evernote/edam/notestore/NoteStore$getNoteContent_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 1577
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "getNoteContent failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1572
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 1573
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 1574
    throw v0
.end method

.method public recv_getNoteSearchText()Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1618
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 1619
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 1624
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 1627
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getNoteSearchText_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteSearchText_result;-><init>()V

    .line 1628
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$getNoteSearchText_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1629
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 1630
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteSearchText_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1631
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteSearchText_result;->access$12500(Lcom/evernote/edam/notestore/NoteStore$getNoteSearchText_result;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1633
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteSearchText_result;->access$12600(Lcom/evernote/edam/notestore/NoteStore$getNoteSearchText_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 1636
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteSearchText_result;->access$12700(Lcom/evernote/edam/notestore/NoteStore$getNoteSearchText_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 1639
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteSearchText_result;->access$12800(Lcom/evernote/edam/notestore/NoteStore$getNoteSearchText_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1640
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteSearchText_result;->access$12800(Lcom/evernote/edam/notestore/NoteStore$getNoteSearchText_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 1642
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "getNoteSearchText failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1637
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteSearchText_result;->access$12700(Lcom/evernote/edam/notestore/NoteStore$getNoteSearchText_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 1634
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteSearchText_result;->access$12600(Lcom/evernote/edam/notestore/NoteStore$getNoteSearchText_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 1625
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "getNoteSearchText failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1620
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 1621
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 1622
    throw v0
.end method

.method public recv_getNoteTagNames()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1710
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 1711
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 1716
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 1719
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getNoteTagNames_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteTagNames_result;-><init>()V

    .line 1720
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$getNoteTagNames_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1721
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 1722
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteTagNames_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1723
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteTagNames_result;->access$13300(Lcom/evernote/edam/notestore/NoteStore$getNoteTagNames_result;)Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 1725
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteTagNames_result;->access$13400(Lcom/evernote/edam/notestore/NoteStore$getNoteTagNames_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 1728
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteTagNames_result;->access$13500(Lcom/evernote/edam/notestore/NoteStore$getNoteTagNames_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 1731
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteTagNames_result;->access$13600(Lcom/evernote/edam/notestore/NoteStore$getNoteTagNames_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1732
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteTagNames_result;->access$13600(Lcom/evernote/edam/notestore/NoteStore$getNoteTagNames_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 1734
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "getNoteTagNames failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1729
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteTagNames_result;->access$13500(Lcom/evernote/edam/notestore/NoteStore$getNoteTagNames_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 1726
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteTagNames_result;->access$13400(Lcom/evernote/edam/notestore/NoteStore$getNoteTagNames_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 1717
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "getNoteTagNames failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1712
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 1713
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 1714
    throw v0
.end method

.method public recv_getNoteVersion()Lcom/evernote/edam/type/Note;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2125
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 2126
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 2131
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 2134
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getNoteVersion_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteVersion_result;-><init>()V

    .line 2135
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$getNoteVersion_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 2136
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 2137
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteVersion_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2138
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteVersion_result;->access$16800(Lcom/evernote/edam/notestore/NoteStore$getNoteVersion_result;)Lcom/evernote/edam/type/Note;

    move-result-object v0

    return-object v0

    .line 2140
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteVersion_result;->access$16900(Lcom/evernote/edam/notestore/NoteStore$getNoteVersion_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 2143
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteVersion_result;->access$17000(Lcom/evernote/edam/notestore/NoteStore$getNoteVersion_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 2146
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteVersion_result;->access$17100(Lcom/evernote/edam/notestore/NoteStore$getNoteVersion_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2147
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteVersion_result;->access$17100(Lcom/evernote/edam/notestore/NoteStore$getNoteVersion_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 2149
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "getNoteVersion failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2144
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteVersion_result;->access$17000(Lcom/evernote/edam/notestore/NoteStore$getNoteVersion_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 2141
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteVersion_result;->access$16900(Lcom/evernote/edam/notestore/NoteStore$getNoteVersion_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 2132
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "getNoteVersion failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2127
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 2128
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 2129
    throw v0
.end method

.method public recv_getNotebook()Lcom/evernote/edam/type/Notebook;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 380
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 381
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 386
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 389
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getNotebook_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getNotebook_result;-><init>()V

    .line 390
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$getNotebook_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 391
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 392
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$getNotebook_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 393
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNotebook_result;->access$2300(Lcom/evernote/edam/notestore/NoteStore$getNotebook_result;)Lcom/evernote/edam/type/Notebook;

    move-result-object v0

    return-object v0

    .line 395
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNotebook_result;->access$2400(Lcom/evernote/edam/notestore/NoteStore$getNotebook_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 398
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNotebook_result;->access$2500(Lcom/evernote/edam/notestore/NoteStore$getNotebook_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 401
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNotebook_result;->access$2600(Lcom/evernote/edam/notestore/NoteStore$getNotebook_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 402
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNotebook_result;->access$2600(Lcom/evernote/edam/notestore/NoteStore$getNotebook_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 404
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "getNotebook failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 399
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNotebook_result;->access$2500(Lcom/evernote/edam/notestore/NoteStore$getNotebook_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 396
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getNotebook_result;->access$2400(Lcom/evernote/edam/notestore/NoteStore$getNotebook_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 387
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "getNotebook failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 382
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 383
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 384
    throw v0
.end method

.method public recv_getPublicNotebook()Lcom/evernote/edam/type/Notebook;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2689
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 2690
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_4

    .line 2695
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_3

    .line 2698
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getPublicNotebook_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getPublicNotebook_result;-><init>()V

    .line 2699
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$getPublicNotebook_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 2700
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 2701
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$getPublicNotebook_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2702
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getPublicNotebook_result;->access$21600(Lcom/evernote/edam/notestore/NoteStore$getPublicNotebook_result;)Lcom/evernote/edam/type/Notebook;

    move-result-object v0

    return-object v0

    .line 2704
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getPublicNotebook_result;->access$21700(Lcom/evernote/edam/notestore/NoteStore$getPublicNotebook_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 2707
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getPublicNotebook_result;->access$21800(Lcom/evernote/edam/notestore/NoteStore$getPublicNotebook_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2708
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getPublicNotebook_result;->access$21800(Lcom/evernote/edam/notestore/NoteStore$getPublicNotebook_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 2710
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "getPublicNotebook failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2705
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getPublicNotebook_result;->access$21700(Lcom/evernote/edam/notestore/NoteStore$getPublicNotebook_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 2696
    :cond_3
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "getPublicNotebook failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2691
    :cond_4
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 2692
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 2693
    throw v0
.end method

.method public recv_getResource()Lcom/evernote/edam/type/Resource;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2175
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 2176
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 2181
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 2184
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getResource_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getResource_result;-><init>()V

    .line 2185
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$getResource_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 2186
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 2187
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$getResource_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2188
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResource_result;->access$17200(Lcom/evernote/edam/notestore/NoteStore$getResource_result;)Lcom/evernote/edam/type/Resource;

    move-result-object v0

    return-object v0

    .line 2190
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResource_result;->access$17300(Lcom/evernote/edam/notestore/NoteStore$getResource_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 2193
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResource_result;->access$17400(Lcom/evernote/edam/notestore/NoteStore$getResource_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 2196
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResource_result;->access$17500(Lcom/evernote/edam/notestore/NoteStore$getResource_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2197
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResource_result;->access$17500(Lcom/evernote/edam/notestore/NoteStore$getResource_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 2199
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "getResource failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2194
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResource_result;->access$17400(Lcom/evernote/edam/notestore/NoteStore$getResource_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 2191
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResource_result;->access$17300(Lcom/evernote/edam/notestore/NoteStore$getResource_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 2182
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "getResource failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2177
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 2178
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 2179
    throw v0
.end method

.method public recv_getResourceAlternateData()[B
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2597
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 2598
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 2603
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 2606
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getResourceAlternateData_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceAlternateData_result;-><init>()V

    .line 2607
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$getResourceAlternateData_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 2608
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 2609
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceAlternateData_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2610
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceAlternateData_result;->access$20800(Lcom/evernote/edam/notestore/NoteStore$getResourceAlternateData_result;)[B

    move-result-object v0

    return-object v0

    .line 2612
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceAlternateData_result;->access$20900(Lcom/evernote/edam/notestore/NoteStore$getResourceAlternateData_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 2615
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceAlternateData_result;->access$21000(Lcom/evernote/edam/notestore/NoteStore$getResourceAlternateData_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 2618
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceAlternateData_result;->access$21100(Lcom/evernote/edam/notestore/NoteStore$getResourceAlternateData_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2619
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceAlternateData_result;->access$21100(Lcom/evernote/edam/notestore/NoteStore$getResourceAlternateData_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 2621
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "getResourceAlternateData failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2616
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceAlternateData_result;->access$21000(Lcom/evernote/edam/notestore/NoteStore$getResourceAlternateData_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 2613
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceAlternateData_result;->access$20900(Lcom/evernote/edam/notestore/NoteStore$getResourceAlternateData_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 2604
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "getResourceAlternateData failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2599
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 2600
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 2601
    throw v0
.end method

.method public recv_getResourceApplicationData()Lcom/evernote/edam/type/LazyMap;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2221
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 2222
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 2227
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 2230
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getResourceApplicationData_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceApplicationData_result;-><init>()V

    .line 2231
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$getResourceApplicationData_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 2232
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 2233
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceApplicationData_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2234
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceApplicationData_result;->access$17600(Lcom/evernote/edam/notestore/NoteStore$getResourceApplicationData_result;)Lcom/evernote/edam/type/LazyMap;

    move-result-object v0

    return-object v0

    .line 2236
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceApplicationData_result;->access$17700(Lcom/evernote/edam/notestore/NoteStore$getResourceApplicationData_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 2239
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceApplicationData_result;->access$17800(Lcom/evernote/edam/notestore/NoteStore$getResourceApplicationData_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 2242
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceApplicationData_result;->access$17900(Lcom/evernote/edam/notestore/NoteStore$getResourceApplicationData_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2243
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceApplicationData_result;->access$17900(Lcom/evernote/edam/notestore/NoteStore$getResourceApplicationData_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 2245
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "getResourceApplicationData failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2240
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceApplicationData_result;->access$17800(Lcom/evernote/edam/notestore/NoteStore$getResourceApplicationData_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 2237
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceApplicationData_result;->access$17700(Lcom/evernote/edam/notestore/NoteStore$getResourceApplicationData_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 2228
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "getResourceApplicationData failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2223
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 2224
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 2225
    throw v0
.end method

.method public recv_getResourceApplicationDataEntry()Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2268
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 2269
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 2274
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 2277
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getResourceApplicationDataEntry_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceApplicationDataEntry_result;-><init>()V

    .line 2278
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$getResourceApplicationDataEntry_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 2279
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 2280
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceApplicationDataEntry_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2281
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceApplicationDataEntry_result;->access$18000(Lcom/evernote/edam/notestore/NoteStore$getResourceApplicationDataEntry_result;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2283
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceApplicationDataEntry_result;->access$18100(Lcom/evernote/edam/notestore/NoteStore$getResourceApplicationDataEntry_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 2286
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceApplicationDataEntry_result;->access$18200(Lcom/evernote/edam/notestore/NoteStore$getResourceApplicationDataEntry_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 2289
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceApplicationDataEntry_result;->access$18300(Lcom/evernote/edam/notestore/NoteStore$getResourceApplicationDataEntry_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2290
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceApplicationDataEntry_result;->access$18300(Lcom/evernote/edam/notestore/NoteStore$getResourceApplicationDataEntry_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 2292
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "getResourceApplicationDataEntry failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2287
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceApplicationDataEntry_result;->access$18200(Lcom/evernote/edam/notestore/NoteStore$getResourceApplicationDataEntry_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 2284
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceApplicationDataEntry_result;->access$18100(Lcom/evernote/edam/notestore/NoteStore$getResourceApplicationDataEntry_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 2275
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "getResourceApplicationDataEntry failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2270
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 2271
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 2272
    throw v0
.end method

.method public recv_getResourceAttributes()Lcom/evernote/edam/type/ResourceAttributes;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2643
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 2644
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 2649
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 2652
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getResourceAttributes_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceAttributes_result;-><init>()V

    .line 2653
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$getResourceAttributes_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 2654
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 2655
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceAttributes_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2656
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceAttributes_result;->access$21200(Lcom/evernote/edam/notestore/NoteStore$getResourceAttributes_result;)Lcom/evernote/edam/type/ResourceAttributes;

    move-result-object v0

    return-object v0

    .line 2658
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceAttributes_result;->access$21300(Lcom/evernote/edam/notestore/NoteStore$getResourceAttributes_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 2661
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceAttributes_result;->access$21400(Lcom/evernote/edam/notestore/NoteStore$getResourceAttributes_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 2664
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceAttributes_result;->access$21500(Lcom/evernote/edam/notestore/NoteStore$getResourceAttributes_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2665
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceAttributes_result;->access$21500(Lcom/evernote/edam/notestore/NoteStore$getResourceAttributes_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 2667
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "getResourceAttributes failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2662
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceAttributes_result;->access$21400(Lcom/evernote/edam/notestore/NoteStore$getResourceAttributes_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 2659
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceAttributes_result;->access$21300(Lcom/evernote/edam/notestore/NoteStore$getResourceAttributes_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 2650
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "getResourceAttributes failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2645
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 2646
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 2647
    throw v0
.end method

.method public recv_getResourceByHash()Lcom/evernote/edam/type/Resource;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2505
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 2506
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 2511
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 2514
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getResourceByHash_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceByHash_result;-><init>()V

    .line 2515
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$getResourceByHash_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 2516
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 2517
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceByHash_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2518
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceByHash_result;->access$20000(Lcom/evernote/edam/notestore/NoteStore$getResourceByHash_result;)Lcom/evernote/edam/type/Resource;

    move-result-object v0

    return-object v0

    .line 2520
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceByHash_result;->access$20100(Lcom/evernote/edam/notestore/NoteStore$getResourceByHash_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 2523
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceByHash_result;->access$20200(Lcom/evernote/edam/notestore/NoteStore$getResourceByHash_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 2526
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceByHash_result;->access$20300(Lcom/evernote/edam/notestore/NoteStore$getResourceByHash_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2527
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceByHash_result;->access$20300(Lcom/evernote/edam/notestore/NoteStore$getResourceByHash_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 2529
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "getResourceByHash failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2524
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceByHash_result;->access$20200(Lcom/evernote/edam/notestore/NoteStore$getResourceByHash_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 2521
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceByHash_result;->access$20100(Lcom/evernote/edam/notestore/NoteStore$getResourceByHash_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 2512
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "getResourceByHash failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2507
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 2508
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 2509
    throw v0
.end method

.method public recv_getResourceData()[B
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2455
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 2456
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 2461
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 2464
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getResourceData_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceData_result;-><init>()V

    .line 2465
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$getResourceData_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 2466
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 2467
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceData_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2468
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceData_result;->access$19600(Lcom/evernote/edam/notestore/NoteStore$getResourceData_result;)[B

    move-result-object v0

    return-object v0

    .line 2470
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceData_result;->access$19700(Lcom/evernote/edam/notestore/NoteStore$getResourceData_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 2473
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceData_result;->access$19800(Lcom/evernote/edam/notestore/NoteStore$getResourceData_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 2476
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceData_result;->access$19900(Lcom/evernote/edam/notestore/NoteStore$getResourceData_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2477
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceData_result;->access$19900(Lcom/evernote/edam/notestore/NoteStore$getResourceData_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 2479
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "getResourceData failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2474
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceData_result;->access$19800(Lcom/evernote/edam/notestore/NoteStore$getResourceData_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 2471
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceData_result;->access$19700(Lcom/evernote/edam/notestore/NoteStore$getResourceData_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 2462
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "getResourceData failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2457
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 2458
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 2459
    throw v0
.end method

.method public recv_getResourceRecognition()[B
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2551
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 2552
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 2557
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 2560
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getResourceRecognition_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceRecognition_result;-><init>()V

    .line 2561
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$getResourceRecognition_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 2562
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 2563
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceRecognition_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2564
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceRecognition_result;->access$20400(Lcom/evernote/edam/notestore/NoteStore$getResourceRecognition_result;)[B

    move-result-object v0

    return-object v0

    .line 2566
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceRecognition_result;->access$20500(Lcom/evernote/edam/notestore/NoteStore$getResourceRecognition_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 2569
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceRecognition_result;->access$20600(Lcom/evernote/edam/notestore/NoteStore$getResourceRecognition_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 2572
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceRecognition_result;->access$20700(Lcom/evernote/edam/notestore/NoteStore$getResourceRecognition_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2573
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceRecognition_result;->access$20700(Lcom/evernote/edam/notestore/NoteStore$getResourceRecognition_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 2575
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "getResourceRecognition failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2570
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceRecognition_result;->access$20600(Lcom/evernote/edam/notestore/NoteStore$getResourceRecognition_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 2567
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceRecognition_result;->access$20500(Lcom/evernote/edam/notestore/NoteStore$getResourceRecognition_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 2558
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "getResourceRecognition failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2553
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 2554
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 2555
    throw v0
.end method

.method public recv_getResourceSearchText()Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1664
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 1665
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 1670
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 1673
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getResourceSearchText_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceSearchText_result;-><init>()V

    .line 1674
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$getResourceSearchText_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1675
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 1676
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceSearchText_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1677
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceSearchText_result;->access$12900(Lcom/evernote/edam/notestore/NoteStore$getResourceSearchText_result;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1679
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceSearchText_result;->access$13000(Lcom/evernote/edam/notestore/NoteStore$getResourceSearchText_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 1682
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceSearchText_result;->access$13100(Lcom/evernote/edam/notestore/NoteStore$getResourceSearchText_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 1685
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceSearchText_result;->access$13200(Lcom/evernote/edam/notestore/NoteStore$getResourceSearchText_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1686
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceSearchText_result;->access$13200(Lcom/evernote/edam/notestore/NoteStore$getResourceSearchText_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 1688
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "getResourceSearchText failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1683
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceSearchText_result;->access$13100(Lcom/evernote/edam/notestore/NoteStore$getResourceSearchText_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 1680
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceSearchText_result;->access$13000(Lcom/evernote/edam/notestore/NoteStore$getResourceSearchText_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 1671
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "getResourceSearchText failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1666
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 1667
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 1668
    throw v0
.end method

.method public recv_getSearch()Lcom/evernote/edam/type/SavedSearch;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 960
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 961
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 966
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 969
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getSearch_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getSearch_result;-><init>()V

    .line 970
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$getSearch_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 971
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 972
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$getSearch_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 973
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getSearch_result;->access$7000(Lcom/evernote/edam/notestore/NoteStore$getSearch_result;)Lcom/evernote/edam/type/SavedSearch;

    move-result-object v0

    return-object v0

    .line 975
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getSearch_result;->access$7100(Lcom/evernote/edam/notestore/NoteStore$getSearch_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 978
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getSearch_result;->access$7200(Lcom/evernote/edam/notestore/NoteStore$getSearch_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 981
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getSearch_result;->access$7300(Lcom/evernote/edam/notestore/NoteStore$getSearch_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 982
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getSearch_result;->access$7300(Lcom/evernote/edam/notestore/NoteStore$getSearch_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 984
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "getSearch failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 979
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getSearch_result;->access$7200(Lcom/evernote/edam/notestore/NoteStore$getSearch_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 976
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getSearch_result;->access$7100(Lcom/evernote/edam/notestore/NoteStore$getSearch_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 967
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "getSearch failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 962
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 963
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 964
    throw v0
.end method

.method public recv_getSharedNotebookByAuth()Lcom/evernote/edam/type/SharedNotebook;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 3238
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 3239
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 3244
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 3247
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getSharedNotebookByAuth_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getSharedNotebookByAuth_result;-><init>()V

    .line 3248
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$getSharedNotebookByAuth_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 3249
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 3250
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$getSharedNotebookByAuth_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3251
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getSharedNotebookByAuth_result;->access$26300(Lcom/evernote/edam/notestore/NoteStore$getSharedNotebookByAuth_result;)Lcom/evernote/edam/type/SharedNotebook;

    move-result-object v0

    return-object v0

    .line 3253
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getSharedNotebookByAuth_result;->access$26400(Lcom/evernote/edam/notestore/NoteStore$getSharedNotebookByAuth_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 3256
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getSharedNotebookByAuth_result;->access$26500(Lcom/evernote/edam/notestore/NoteStore$getSharedNotebookByAuth_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 3259
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getSharedNotebookByAuth_result;->access$26600(Lcom/evernote/edam/notestore/NoteStore$getSharedNotebookByAuth_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 3260
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getSharedNotebookByAuth_result;->access$26600(Lcom/evernote/edam/notestore/NoteStore$getSharedNotebookByAuth_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 3262
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "getSharedNotebookByAuth failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 3257
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getSharedNotebookByAuth_result;->access$26500(Lcom/evernote/edam/notestore/NoteStore$getSharedNotebookByAuth_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 3254
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getSharedNotebookByAuth_result;->access$26400(Lcom/evernote/edam/notestore/NoteStore$getSharedNotebookByAuth_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 3245
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "getSharedNotebookByAuth failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 3240
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 3241
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 3242
    throw v0
.end method

.method public recv_getSyncChunk()Lcom/evernote/edam/notestore/SyncChunk;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 155
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 156
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_4

    .line 161
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_3

    .line 164
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getSyncChunk_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getSyncChunk_result;-><init>()V

    .line 165
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$getSyncChunk_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 166
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 167
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$getSyncChunk_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 168
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getSyncChunk_result;->access$600(Lcom/evernote/edam/notestore/NoteStore$getSyncChunk_result;)Lcom/evernote/edam/notestore/SyncChunk;

    move-result-object v0

    return-object v0

    .line 170
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getSyncChunk_result;->access$700(Lcom/evernote/edam/notestore/NoteStore$getSyncChunk_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 173
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getSyncChunk_result;->access$800(Lcom/evernote/edam/notestore/NoteStore$getSyncChunk_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 174
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getSyncChunk_result;->access$800(Lcom/evernote/edam/notestore/NoteStore$getSyncChunk_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 176
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "getSyncChunk failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 171
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getSyncChunk_result;->access$700(Lcom/evernote/edam/notestore/NoteStore$getSyncChunk_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 162
    :cond_3
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "getSyncChunk failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 157
    :cond_4
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 158
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 159
    throw v0
.end method

.method public recv_getSyncState()Lcom/evernote/edam/notestore/SyncState;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 67
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 68
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_4

    .line 73
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_3

    .line 76
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getSyncState_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getSyncState_result;-><init>()V

    .line 77
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$getSyncState_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 78
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 79
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$getSyncState_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 80
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getSyncState_result;->access$000(Lcom/evernote/edam/notestore/NoteStore$getSyncState_result;)Lcom/evernote/edam/notestore/SyncState;

    move-result-object v0

    return-object v0

    .line 82
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getSyncState_result;->access$100(Lcom/evernote/edam/notestore/NoteStore$getSyncState_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 85
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getSyncState_result;->access$200(Lcom/evernote/edam/notestore/NoteStore$getSyncState_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 86
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getSyncState_result;->access$200(Lcom/evernote/edam/notestore/NoteStore$getSyncState_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 88
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "getSyncState failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 83
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getSyncState_result;->access$100(Lcom/evernote/edam/notestore/NoteStore$getSyncState_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 74
    :cond_3
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "getSyncState failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 69
    :cond_4
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 70
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 71
    throw v0
.end method

.method public recv_getSyncStateWithMetrics()Lcom/evernote/edam/notestore/SyncState;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 110
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 111
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_4

    .line 116
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_3

    .line 119
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getSyncStateWithMetrics_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getSyncStateWithMetrics_result;-><init>()V

    .line 120
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$getSyncStateWithMetrics_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 121
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 122
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$getSyncStateWithMetrics_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 123
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getSyncStateWithMetrics_result;->access$300(Lcom/evernote/edam/notestore/NoteStore$getSyncStateWithMetrics_result;)Lcom/evernote/edam/notestore/SyncState;

    move-result-object v0

    return-object v0

    .line 125
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getSyncStateWithMetrics_result;->access$400(Lcom/evernote/edam/notestore/NoteStore$getSyncStateWithMetrics_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 128
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getSyncStateWithMetrics_result;->access$500(Lcom/evernote/edam/notestore/NoteStore$getSyncStateWithMetrics_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 129
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getSyncStateWithMetrics_result;->access$500(Lcom/evernote/edam/notestore/NoteStore$getSyncStateWithMetrics_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 131
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "getSyncStateWithMetrics failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 126
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getSyncStateWithMetrics_result;->access$400(Lcom/evernote/edam/notestore/NoteStore$getSyncStateWithMetrics_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 117
    :cond_3
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "getSyncStateWithMetrics failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 112
    :cond_4
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 113
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 114
    throw v0
.end method

.method public recv_getTag()Lcom/evernote/edam/type/Tag;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 691
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 692
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 697
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 700
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getTag_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getTag_result;-><init>()V

    .line 701
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$getTag_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 702
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 703
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$getTag_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 704
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getTag_result;->access$4800(Lcom/evernote/edam/notestore/NoteStore$getTag_result;)Lcom/evernote/edam/type/Tag;

    move-result-object v0

    return-object v0

    .line 706
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getTag_result;->access$4900(Lcom/evernote/edam/notestore/NoteStore$getTag_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 709
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getTag_result;->access$5000(Lcom/evernote/edam/notestore/NoteStore$getTag_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 712
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getTag_result;->access$5100(Lcom/evernote/edam/notestore/NoteStore$getTag_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 713
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getTag_result;->access$5100(Lcom/evernote/edam/notestore/NoteStore$getTag_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 715
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "getTag failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 710
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getTag_result;->access$5000(Lcom/evernote/edam/notestore/NoteStore$getTag_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 707
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$getTag_result;->access$4900(Lcom/evernote/edam/notestore/NoteStore$getTag_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 698
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "getTag failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 693
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 694
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 695
    throw v0
.end method

.method public recv_listLinkedNotebooks()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/evernote/edam/type/LinkedNotebook;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 3101
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 3102
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 3107
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 3110
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$listLinkedNotebooks_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$listLinkedNotebooks_result;-><init>()V

    .line 3111
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$listLinkedNotebooks_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 3112
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 3113
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$listLinkedNotebooks_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3114
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$listLinkedNotebooks_result;->access$25100(Lcom/evernote/edam/notestore/NoteStore$listLinkedNotebooks_result;)Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 3116
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$listLinkedNotebooks_result;->access$25200(Lcom/evernote/edam/notestore/NoteStore$listLinkedNotebooks_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 3119
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$listLinkedNotebooks_result;->access$25300(Lcom/evernote/edam/notestore/NoteStore$listLinkedNotebooks_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 3122
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$listLinkedNotebooks_result;->access$25400(Lcom/evernote/edam/notestore/NoteStore$listLinkedNotebooks_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 3123
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$listLinkedNotebooks_result;->access$25400(Lcom/evernote/edam/notestore/NoteStore$listLinkedNotebooks_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 3125
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "listLinkedNotebooks failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 3120
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$listLinkedNotebooks_result;->access$25300(Lcom/evernote/edam/notestore/NoteStore$listLinkedNotebooks_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 3117
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$listLinkedNotebooks_result;->access$25200(Lcom/evernote/edam/notestore/NoteStore$listLinkedNotebooks_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 3108
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "listLinkedNotebooks failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 3103
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 3104
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 3105
    throw v0
.end method

.method public recv_listNoteVersions()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/evernote/edam/notestore/NoteVersionId;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2075
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 2076
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 2081
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 2084
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$listNoteVersions_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$listNoteVersions_result;-><init>()V

    .line 2085
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$listNoteVersions_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 2086
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 2087
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$listNoteVersions_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2088
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$listNoteVersions_result;->access$16400(Lcom/evernote/edam/notestore/NoteStore$listNoteVersions_result;)Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 2090
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$listNoteVersions_result;->access$16500(Lcom/evernote/edam/notestore/NoteStore$listNoteVersions_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 2093
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$listNoteVersions_result;->access$16600(Lcom/evernote/edam/notestore/NoteStore$listNoteVersions_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 2096
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$listNoteVersions_result;->access$16700(Lcom/evernote/edam/notestore/NoteStore$listNoteVersions_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2097
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$listNoteVersions_result;->access$16700(Lcom/evernote/edam/notestore/NoteStore$listNoteVersions_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 2099
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "listNoteVersions failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2094
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$listNoteVersions_result;->access$16600(Lcom/evernote/edam/notestore/NoteStore$listNoteVersions_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 2091
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$listNoteVersions_result;->access$16500(Lcom/evernote/edam/notestore/NoteStore$listNoteVersions_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 2082
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "listNoteVersions failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2077
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 2078
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 2079
    throw v0
.end method

.method public recv_listNotebooks()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/evernote/edam/type/Notebook;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 337
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 338
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_4

    .line 343
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_3

    .line 346
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$listNotebooks_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$listNotebooks_result;-><init>()V

    .line 347
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$listNotebooks_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 348
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 349
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$listNotebooks_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 350
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$listNotebooks_result;->access$2000(Lcom/evernote/edam/notestore/NoteStore$listNotebooks_result;)Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 352
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$listNotebooks_result;->access$2100(Lcom/evernote/edam/notestore/NoteStore$listNotebooks_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 355
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$listNotebooks_result;->access$2200(Lcom/evernote/edam/notestore/NoteStore$listNotebooks_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 356
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$listNotebooks_result;->access$2200(Lcom/evernote/edam/notestore/NoteStore$listNotebooks_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 358
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "listNotebooks failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 353
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$listNotebooks_result;->access$2100(Lcom/evernote/edam/notestore/NoteStore$listNotebooks_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 344
    :cond_3
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "listNotebooks failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 339
    :cond_4
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 340
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 341
    throw v0
.end method

.method public recv_listSearches()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/evernote/edam/type/SavedSearch;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 917
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 918
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_4

    .line 923
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_3

    .line 926
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$listSearches_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$listSearches_result;-><init>()V

    .line 927
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$listSearches_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 928
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 929
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$listSearches_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 930
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$listSearches_result;->access$6700(Lcom/evernote/edam/notestore/NoteStore$listSearches_result;)Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 932
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$listSearches_result;->access$6800(Lcom/evernote/edam/notestore/NoteStore$listSearches_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 935
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$listSearches_result;->access$6900(Lcom/evernote/edam/notestore/NoteStore$listSearches_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 936
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$listSearches_result;->access$6900(Lcom/evernote/edam/notestore/NoteStore$listSearches_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 938
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "listSearches failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 933
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$listSearches_result;->access$6800(Lcom/evernote/edam/notestore/NoteStore$listSearches_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 924
    :cond_3
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "listSearches failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 919
    :cond_4
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 920
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 921
    throw v0
.end method

.method public recv_listSharedNotebooks()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/evernote/edam/type/SharedNotebook;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2918
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 2919
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 2924
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 2927
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$listSharedNotebooks_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$listSharedNotebooks_result;-><init>()V

    .line 2928
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$listSharedNotebooks_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 2929
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 2930
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$listSharedNotebooks_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2931
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$listSharedNotebooks_result;->access$23500(Lcom/evernote/edam/notestore/NoteStore$listSharedNotebooks_result;)Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 2933
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$listSharedNotebooks_result;->access$23600(Lcom/evernote/edam/notestore/NoteStore$listSharedNotebooks_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 2936
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$listSharedNotebooks_result;->access$23700(Lcom/evernote/edam/notestore/NoteStore$listSharedNotebooks_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 2939
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$listSharedNotebooks_result;->access$23800(Lcom/evernote/edam/notestore/NoteStore$listSharedNotebooks_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2940
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$listSharedNotebooks_result;->access$23800(Lcom/evernote/edam/notestore/NoteStore$listSharedNotebooks_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 2942
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "listSharedNotebooks failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2937
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$listSharedNotebooks_result;->access$23700(Lcom/evernote/edam/notestore/NoteStore$listSharedNotebooks_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 2934
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$listSharedNotebooks_result;->access$23600(Lcom/evernote/edam/notestore/NoteStore$listSharedNotebooks_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 2925
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "listSharedNotebooks failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2920
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 2921
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 2922
    throw v0
.end method

.method public recv_listTags()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/evernote/edam/type/Tag;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 602
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 603
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_4

    .line 608
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_3

    .line 611
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$listTags_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$listTags_result;-><init>()V

    .line 612
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$listTags_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 613
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 614
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$listTags_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 615
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$listTags_result;->access$4100(Lcom/evernote/edam/notestore/NoteStore$listTags_result;)Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 617
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$listTags_result;->access$4200(Lcom/evernote/edam/notestore/NoteStore$listTags_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 620
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$listTags_result;->access$4300(Lcom/evernote/edam/notestore/NoteStore$listTags_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 621
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$listTags_result;->access$4300(Lcom/evernote/edam/notestore/NoteStore$listTags_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 623
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "listTags failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 618
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$listTags_result;->access$4200(Lcom/evernote/edam/notestore/NoteStore$listTags_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 609
    :cond_3
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "listTags failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 604
    :cond_4
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 605
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 606
    throw v0
.end method

.method public recv_listTagsByNotebook()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/evernote/edam/type/Tag;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 645
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 646
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 651
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 654
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$listTagsByNotebook_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$listTagsByNotebook_result;-><init>()V

    .line 655
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$listTagsByNotebook_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 656
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 657
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$listTagsByNotebook_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 658
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$listTagsByNotebook_result;->access$4400(Lcom/evernote/edam/notestore/NoteStore$listTagsByNotebook_result;)Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 660
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$listTagsByNotebook_result;->access$4500(Lcom/evernote/edam/notestore/NoteStore$listTagsByNotebook_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 663
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$listTagsByNotebook_result;->access$4600(Lcom/evernote/edam/notestore/NoteStore$listTagsByNotebook_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 666
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$listTagsByNotebook_result;->access$4700(Lcom/evernote/edam/notestore/NoteStore$listTagsByNotebook_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 667
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$listTagsByNotebook_result;->access$4700(Lcom/evernote/edam/notestore/NoteStore$listTagsByNotebook_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 669
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "listTagsByNotebook failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 664
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$listTagsByNotebook_result;->access$4600(Lcom/evernote/edam/notestore/NoteStore$listTagsByNotebook_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 661
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$listTagsByNotebook_result;->access$4500(Lcom/evernote/edam/notestore/NoteStore$listTagsByNotebook_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 652
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "listTagsByNotebook failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 647
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 648
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 649
    throw v0
.end method

.method public recv_sendMessageToSharedNotebookMembers()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2873
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 2874
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 2879
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 2882
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$sendMessageToSharedNotebookMembers_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$sendMessageToSharedNotebookMembers_result;-><init>()V

    .line 2883
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$sendMessageToSharedNotebookMembers_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 2884
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 2885
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$sendMessageToSharedNotebookMembers_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2886
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$sendMessageToSharedNotebookMembers_result;->access$23100(Lcom/evernote/edam/notestore/NoteStore$sendMessageToSharedNotebookMembers_result;)I

    move-result v0

    return v0

    .line 2888
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$sendMessageToSharedNotebookMembers_result;->access$23200(Lcom/evernote/edam/notestore/NoteStore$sendMessageToSharedNotebookMembers_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 2891
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$sendMessageToSharedNotebookMembers_result;->access$23300(Lcom/evernote/edam/notestore/NoteStore$sendMessageToSharedNotebookMembers_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 2894
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$sendMessageToSharedNotebookMembers_result;->access$23400(Lcom/evernote/edam/notestore/NoteStore$sendMessageToSharedNotebookMembers_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2895
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$sendMessageToSharedNotebookMembers_result;->access$23400(Lcom/evernote/edam/notestore/NoteStore$sendMessageToSharedNotebookMembers_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 2897
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "sendMessageToSharedNotebookMembers failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2892
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$sendMessageToSharedNotebookMembers_result;->access$23300(Lcom/evernote/edam/notestore/NoteStore$sendMessageToSharedNotebookMembers_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 2889
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$sendMessageToSharedNotebookMembers_result;->access$23200(Lcom/evernote/edam/notestore/NoteStore$sendMessageToSharedNotebookMembers_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 2880
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "sendMessageToSharedNotebookMembers failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2875
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 2876
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 2877
    throw v0
.end method

.method public recv_setNoteApplicationDataEntry()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1477
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 1478
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 1483
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 1486
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$setNoteApplicationDataEntry_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$setNoteApplicationDataEntry_result;-><init>()V

    .line 1487
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$setNoteApplicationDataEntry_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1488
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 1489
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$setNoteApplicationDataEntry_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1490
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$setNoteApplicationDataEntry_result;->access$11300(Lcom/evernote/edam/notestore/NoteStore$setNoteApplicationDataEntry_result;)I

    move-result v0

    return v0

    .line 1492
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$setNoteApplicationDataEntry_result;->access$11400(Lcom/evernote/edam/notestore/NoteStore$setNoteApplicationDataEntry_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 1495
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$setNoteApplicationDataEntry_result;->access$11500(Lcom/evernote/edam/notestore/NoteStore$setNoteApplicationDataEntry_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 1498
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$setNoteApplicationDataEntry_result;->access$11600(Lcom/evernote/edam/notestore/NoteStore$setNoteApplicationDataEntry_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1499
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$setNoteApplicationDataEntry_result;->access$11600(Lcom/evernote/edam/notestore/NoteStore$setNoteApplicationDataEntry_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 1501
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "setNoteApplicationDataEntry failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1496
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$setNoteApplicationDataEntry_result;->access$11500(Lcom/evernote/edam/notestore/NoteStore$setNoteApplicationDataEntry_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 1493
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$setNoteApplicationDataEntry_result;->access$11400(Lcom/evernote/edam/notestore/NoteStore$setNoteApplicationDataEntry_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 1484
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "setNoteApplicationDataEntry failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1479
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 1480
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 1481
    throw v0
.end method

.method public recv_setResourceApplicationDataEntry()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2316
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 2317
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 2322
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 2325
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$setResourceApplicationDataEntry_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$setResourceApplicationDataEntry_result;-><init>()V

    .line 2326
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$setResourceApplicationDataEntry_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 2327
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 2328
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$setResourceApplicationDataEntry_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2329
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$setResourceApplicationDataEntry_result;->access$18400(Lcom/evernote/edam/notestore/NoteStore$setResourceApplicationDataEntry_result;)I

    move-result v0

    return v0

    .line 2331
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$setResourceApplicationDataEntry_result;->access$18500(Lcom/evernote/edam/notestore/NoteStore$setResourceApplicationDataEntry_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 2334
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$setResourceApplicationDataEntry_result;->access$18600(Lcom/evernote/edam/notestore/NoteStore$setResourceApplicationDataEntry_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 2337
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$setResourceApplicationDataEntry_result;->access$18700(Lcom/evernote/edam/notestore/NoteStore$setResourceApplicationDataEntry_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2338
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$setResourceApplicationDataEntry_result;->access$18700(Lcom/evernote/edam/notestore/NoteStore$setResourceApplicationDataEntry_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 2340
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "setResourceApplicationDataEntry failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2335
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$setResourceApplicationDataEntry_result;->access$18600(Lcom/evernote/edam/notestore/NoteStore$setResourceApplicationDataEntry_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 2332
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$setResourceApplicationDataEntry_result;->access$18500(Lcom/evernote/edam/notestore/NoteStore$setResourceApplicationDataEntry_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 2323
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "setResourceApplicationDataEntry failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2318
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 2319
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 2320
    throw v0
.end method

.method public recv_setSharedNotebookRecipientSettings()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2825
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 2826
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 2831
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 2834
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$setSharedNotebookRecipientSettings_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$setSharedNotebookRecipientSettings_result;-><init>()V

    .line 2835
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$setSharedNotebookRecipientSettings_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 2836
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 2837
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$setSharedNotebookRecipientSettings_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2838
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$setSharedNotebookRecipientSettings_result;->access$22700(Lcom/evernote/edam/notestore/NoteStore$setSharedNotebookRecipientSettings_result;)I

    move-result v0

    return v0

    .line 2840
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$setSharedNotebookRecipientSettings_result;->access$22800(Lcom/evernote/edam/notestore/NoteStore$setSharedNotebookRecipientSettings_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 2843
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$setSharedNotebookRecipientSettings_result;->access$22900(Lcom/evernote/edam/notestore/NoteStore$setSharedNotebookRecipientSettings_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 2846
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$setSharedNotebookRecipientSettings_result;->access$23000(Lcom/evernote/edam/notestore/NoteStore$setSharedNotebookRecipientSettings_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2847
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$setSharedNotebookRecipientSettings_result;->access$23000(Lcom/evernote/edam/notestore/NoteStore$setSharedNotebookRecipientSettings_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 2849
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "setSharedNotebookRecipientSettings failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2844
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$setSharedNotebookRecipientSettings_result;->access$22900(Lcom/evernote/edam/notestore/NoteStore$setSharedNotebookRecipientSettings_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 2841
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$setSharedNotebookRecipientSettings_result;->access$22800(Lcom/evernote/edam/notestore/NoteStore$setSharedNotebookRecipientSettings_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 2832
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "setSharedNotebookRecipientSettings failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2827
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 2828
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 2829
    throw v0
.end method

.method public recv_shareNote()Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 3327
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 3328
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 3333
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 3336
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$shareNote_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$shareNote_result;-><init>()V

    .line 3337
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$shareNote_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 3338
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 3339
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$shareNote_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3340
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$shareNote_result;->access$27000(Lcom/evernote/edam/notestore/NoteStore$shareNote_result;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 3342
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$shareNote_result;->access$27100(Lcom/evernote/edam/notestore/NoteStore$shareNote_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 3345
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$shareNote_result;->access$27200(Lcom/evernote/edam/notestore/NoteStore$shareNote_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 3348
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$shareNote_result;->access$27300(Lcom/evernote/edam/notestore/NoteStore$shareNote_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 3349
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$shareNote_result;->access$27300(Lcom/evernote/edam/notestore/NoteStore$shareNote_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 3351
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "shareNote failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 3346
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$shareNote_result;->access$27200(Lcom/evernote/edam/notestore/NoteStore$shareNote_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 3343
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$shareNote_result;->access$27100(Lcom/evernote/edam/notestore/NoteStore$shareNote_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 3334
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "shareNote failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 3329
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 3330
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 3331
    throw v0
.end method

.method public recv_stopSharingNote()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 3373
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 3374
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_4

    .line 3379
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_3

    .line 3382
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$stopSharingNote_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$stopSharingNote_result;-><init>()V

    .line 3383
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$stopSharingNote_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 3384
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 3385
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$stopSharingNote_result;->access$27400(Lcom/evernote/edam/notestore/NoteStore$stopSharingNote_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 3388
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$stopSharingNote_result;->access$27500(Lcom/evernote/edam/notestore/NoteStore$stopSharingNote_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-nez v1, :cond_1

    .line 3391
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$stopSharingNote_result;->access$27600(Lcom/evernote/edam/notestore/NoteStore$stopSharingNote_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_0

    return-void

    .line 3392
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$stopSharingNote_result;->access$27600(Lcom/evernote/edam/notestore/NoteStore$stopSharingNote_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 3389
    :cond_1
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$stopSharingNote_result;->access$27500(Lcom/evernote/edam/notestore/NoteStore$stopSharingNote_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 3386
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$stopSharingNote_result;->access$27400(Lcom/evernote/edam/notestore/NoteStore$stopSharingNote_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 3380
    :cond_3
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "stopSharingNote failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 3375
    :cond_4
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 3376
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 3377
    throw v0
.end method

.method public recv_unsetNoteApplicationDataEntry()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1524
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 1525
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 1530
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 1533
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$unsetNoteApplicationDataEntry_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$unsetNoteApplicationDataEntry_result;-><init>()V

    .line 1534
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$unsetNoteApplicationDataEntry_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1535
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 1536
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$unsetNoteApplicationDataEntry_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1537
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$unsetNoteApplicationDataEntry_result;->access$11700(Lcom/evernote/edam/notestore/NoteStore$unsetNoteApplicationDataEntry_result;)I

    move-result v0

    return v0

    .line 1539
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$unsetNoteApplicationDataEntry_result;->access$11800(Lcom/evernote/edam/notestore/NoteStore$unsetNoteApplicationDataEntry_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 1542
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$unsetNoteApplicationDataEntry_result;->access$11900(Lcom/evernote/edam/notestore/NoteStore$unsetNoteApplicationDataEntry_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 1545
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$unsetNoteApplicationDataEntry_result;->access$12000(Lcom/evernote/edam/notestore/NoteStore$unsetNoteApplicationDataEntry_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1546
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$unsetNoteApplicationDataEntry_result;->access$12000(Lcom/evernote/edam/notestore/NoteStore$unsetNoteApplicationDataEntry_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 1548
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "unsetNoteApplicationDataEntry failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1543
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$unsetNoteApplicationDataEntry_result;->access$11900(Lcom/evernote/edam/notestore/NoteStore$unsetNoteApplicationDataEntry_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 1540
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$unsetNoteApplicationDataEntry_result;->access$11800(Lcom/evernote/edam/notestore/NoteStore$unsetNoteApplicationDataEntry_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 1531
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "unsetNoteApplicationDataEntry failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1526
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 1527
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 1528
    throw v0
.end method

.method public recv_unsetResourceApplicationDataEntry()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2363
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 2364
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 2369
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 2372
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$unsetResourceApplicationDataEntry_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$unsetResourceApplicationDataEntry_result;-><init>()V

    .line 2373
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$unsetResourceApplicationDataEntry_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 2374
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 2375
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$unsetResourceApplicationDataEntry_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2376
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$unsetResourceApplicationDataEntry_result;->access$18800(Lcom/evernote/edam/notestore/NoteStore$unsetResourceApplicationDataEntry_result;)I

    move-result v0

    return v0

    .line 2378
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$unsetResourceApplicationDataEntry_result;->access$18900(Lcom/evernote/edam/notestore/NoteStore$unsetResourceApplicationDataEntry_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 2381
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$unsetResourceApplicationDataEntry_result;->access$19000(Lcom/evernote/edam/notestore/NoteStore$unsetResourceApplicationDataEntry_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 2384
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$unsetResourceApplicationDataEntry_result;->access$19100(Lcom/evernote/edam/notestore/NoteStore$unsetResourceApplicationDataEntry_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2385
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$unsetResourceApplicationDataEntry_result;->access$19100(Lcom/evernote/edam/notestore/NoteStore$unsetResourceApplicationDataEntry_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 2387
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "unsetResourceApplicationDataEntry failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2382
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$unsetResourceApplicationDataEntry_result;->access$19000(Lcom/evernote/edam/notestore/NoteStore$unsetResourceApplicationDataEntry_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 2379
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$unsetResourceApplicationDataEntry_result;->access$18900(Lcom/evernote/edam/notestore/NoteStore$unsetResourceApplicationDataEntry_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 2370
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "unsetResourceApplicationDataEntry failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2365
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 2366
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 2367
    throw v0
.end method

.method public recv_untagAll()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 829
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 830
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_4

    .line 835
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_3

    .line 838
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$untagAll_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$untagAll_result;-><init>()V

    .line 839
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$untagAll_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 840
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 841
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$untagAll_result;->access$6000(Lcom/evernote/edam/notestore/NoteStore$untagAll_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 844
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$untagAll_result;->access$6100(Lcom/evernote/edam/notestore/NoteStore$untagAll_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_1

    .line 847
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$untagAll_result;->access$6200(Lcom/evernote/edam/notestore/NoteStore$untagAll_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-nez v1, :cond_0

    return-void

    .line 848
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$untagAll_result;->access$6200(Lcom/evernote/edam/notestore/NoteStore$untagAll_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 845
    :cond_1
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$untagAll_result;->access$6100(Lcom/evernote/edam/notestore/NoteStore$untagAll_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 842
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$untagAll_result;->access$6000(Lcom/evernote/edam/notestore/NoteStore$untagAll_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 836
    :cond_3
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "untagAll failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 831
    :cond_4
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 832
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 833
    throw v0
.end method

.method public recv_updateLinkedNotebook()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 3056
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 3057
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 3062
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 3065
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$updateLinkedNotebook_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$updateLinkedNotebook_result;-><init>()V

    .line 3066
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$updateLinkedNotebook_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 3067
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 3068
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$updateLinkedNotebook_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3069
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$updateLinkedNotebook_result;->access$24700(Lcom/evernote/edam/notestore/NoteStore$updateLinkedNotebook_result;)I

    move-result v0

    return v0

    .line 3071
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$updateLinkedNotebook_result;->access$24800(Lcom/evernote/edam/notestore/NoteStore$updateLinkedNotebook_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 3074
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$updateLinkedNotebook_result;->access$24900(Lcom/evernote/edam/notestore/NoteStore$updateLinkedNotebook_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 3077
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$updateLinkedNotebook_result;->access$25000(Lcom/evernote/edam/notestore/NoteStore$updateLinkedNotebook_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 3078
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$updateLinkedNotebook_result;->access$25000(Lcom/evernote/edam/notestore/NoteStore$updateLinkedNotebook_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 3080
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "updateLinkedNotebook failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 3075
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$updateLinkedNotebook_result;->access$24900(Lcom/evernote/edam/notestore/NoteStore$updateLinkedNotebook_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 3072
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$updateLinkedNotebook_result;->access$24800(Lcom/evernote/edam/notestore/NoteStore$updateLinkedNotebook_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 3063
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "updateLinkedNotebook failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 3058
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 3059
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 3060
    throw v0
.end method

.method public recv_updateNote()Lcom/evernote/edam/type/Note;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1802
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 1803
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 1808
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 1811
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$updateNote_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$updateNote_result;-><init>()V

    .line 1812
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$updateNote_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1813
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 1814
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$updateNote_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1815
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$updateNote_result;->access$14100(Lcom/evernote/edam/notestore/NoteStore$updateNote_result;)Lcom/evernote/edam/type/Note;

    move-result-object v0

    return-object v0

    .line 1817
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$updateNote_result;->access$14200(Lcom/evernote/edam/notestore/NoteStore$updateNote_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 1820
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$updateNote_result;->access$14300(Lcom/evernote/edam/notestore/NoteStore$updateNote_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 1823
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$updateNote_result;->access$14400(Lcom/evernote/edam/notestore/NoteStore$updateNote_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1824
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$updateNote_result;->access$14400(Lcom/evernote/edam/notestore/NoteStore$updateNote_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 1826
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "updateNote failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1821
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$updateNote_result;->access$14300(Lcom/evernote/edam/notestore/NoteStore$updateNote_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 1818
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$updateNote_result;->access$14200(Lcom/evernote/edam/notestore/NoteStore$updateNote_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 1809
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "updateNote failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1804
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 1805
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 1806
    throw v0
.end method

.method public recv_updateNotebook()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 511
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 512
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 517
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 520
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$updateNotebook_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$updateNotebook_result;-><init>()V

    .line 521
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$updateNotebook_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 522
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 523
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$updateNotebook_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 524
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$updateNotebook_result;->access$3300(Lcom/evernote/edam/notestore/NoteStore$updateNotebook_result;)I

    move-result v0

    return v0

    .line 526
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$updateNotebook_result;->access$3400(Lcom/evernote/edam/notestore/NoteStore$updateNotebook_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 529
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$updateNotebook_result;->access$3500(Lcom/evernote/edam/notestore/NoteStore$updateNotebook_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 532
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$updateNotebook_result;->access$3600(Lcom/evernote/edam/notestore/NoteStore$updateNotebook_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 533
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$updateNotebook_result;->access$3600(Lcom/evernote/edam/notestore/NoteStore$updateNotebook_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 535
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "updateNotebook failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 530
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$updateNotebook_result;->access$3500(Lcom/evernote/edam/notestore/NoteStore$updateNotebook_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 527
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$updateNotebook_result;->access$3400(Lcom/evernote/edam/notestore/NoteStore$updateNotebook_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 518
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "updateNotebook failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 513
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 514
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 515
    throw v0
.end method

.method public recv_updateResource()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2409
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 2410
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 2415
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 2418
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$updateResource_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$updateResource_result;-><init>()V

    .line 2419
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$updateResource_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 2420
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 2421
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$updateResource_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2422
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$updateResource_result;->access$19200(Lcom/evernote/edam/notestore/NoteStore$updateResource_result;)I

    move-result v0

    return v0

    .line 2424
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$updateResource_result;->access$19300(Lcom/evernote/edam/notestore/NoteStore$updateResource_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 2427
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$updateResource_result;->access$19400(Lcom/evernote/edam/notestore/NoteStore$updateResource_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 2430
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$updateResource_result;->access$19500(Lcom/evernote/edam/notestore/NoteStore$updateResource_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2431
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$updateResource_result;->access$19500(Lcom/evernote/edam/notestore/NoteStore$updateResource_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 2433
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "updateResource failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2428
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$updateResource_result;->access$19400(Lcom/evernote/edam/notestore/NoteStore$updateResource_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 2425
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$updateResource_result;->access$19300(Lcom/evernote/edam/notestore/NoteStore$updateResource_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 2416
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "updateResource failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2411
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 2412
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 2413
    throw v0
.end method

.method public recv_updateSearch()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1049
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 1050
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 1055
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 1058
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$updateSearch_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$updateSearch_result;-><init>()V

    .line 1059
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$updateSearch_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1060
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 1061
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$updateSearch_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1062
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$updateSearch_result;->access$7700(Lcom/evernote/edam/notestore/NoteStore$updateSearch_result;)I

    move-result v0

    return v0

    .line 1064
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$updateSearch_result;->access$7800(Lcom/evernote/edam/notestore/NoteStore$updateSearch_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 1067
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$updateSearch_result;->access$7900(Lcom/evernote/edam/notestore/NoteStore$updateSearch_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 1070
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$updateSearch_result;->access$8000(Lcom/evernote/edam/notestore/NoteStore$updateSearch_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1071
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$updateSearch_result;->access$8000(Lcom/evernote/edam/notestore/NoteStore$updateSearch_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 1073
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "updateSearch failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1068
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$updateSearch_result;->access$7900(Lcom/evernote/edam/notestore/NoteStore$updateSearch_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 1065
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$updateSearch_result;->access$7800(Lcom/evernote/edam/notestore/NoteStore$updateSearch_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 1056
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "updateSearch failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1051
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 1052
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 1053
    throw v0
.end method

.method public recv_updateSharedNotebook()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2778
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 2779
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 2784
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 2787
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$updateSharedNotebook_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$updateSharedNotebook_result;-><init>()V

    .line 2788
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$updateSharedNotebook_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 2789
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 2790
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$updateSharedNotebook_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2791
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$updateSharedNotebook_result;->access$22300(Lcom/evernote/edam/notestore/NoteStore$updateSharedNotebook_result;)I

    move-result v0

    return v0

    .line 2793
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$updateSharedNotebook_result;->access$22400(Lcom/evernote/edam/notestore/NoteStore$updateSharedNotebook_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 2796
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$updateSharedNotebook_result;->access$22500(Lcom/evernote/edam/notestore/NoteStore$updateSharedNotebook_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 2799
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$updateSharedNotebook_result;->access$22600(Lcom/evernote/edam/notestore/NoteStore$updateSharedNotebook_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2800
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$updateSharedNotebook_result;->access$22600(Lcom/evernote/edam/notestore/NoteStore$updateSharedNotebook_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 2802
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "updateSharedNotebook failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2797
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$updateSharedNotebook_result;->access$22500(Lcom/evernote/edam/notestore/NoteStore$updateSharedNotebook_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 2794
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$updateSharedNotebook_result;->access$22400(Lcom/evernote/edam/notestore/NoteStore$updateSharedNotebook_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 2785
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "updateSharedNotebook failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2780
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 2781
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 2782
    throw v0
.end method

.method public recv_updateTag()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 783
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 784
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 789
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 792
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$updateTag_result;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$updateTag_result;-><init>()V

    .line 793
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$updateTag_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 794
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 795
    invoke-virtual {v0}, Lcom/evernote/edam/notestore/NoteStore$updateTag_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 796
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$updateTag_result;->access$5600(Lcom/evernote/edam/notestore/NoteStore$updateTag_result;)I

    move-result v0

    return v0

    .line 798
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$updateTag_result;->access$5700(Lcom/evernote/edam/notestore/NoteStore$updateTag_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 801
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$updateTag_result;->access$5800(Lcom/evernote/edam/notestore/NoteStore$updateTag_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 804
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$updateTag_result;->access$5900(Lcom/evernote/edam/notestore/NoteStore$updateTag_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 805
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$updateTag_result;->access$5900(Lcom/evernote/edam/notestore/NoteStore$updateTag_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 807
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "updateTag failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 802
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$updateTag_result;->access$5800(Lcom/evernote/edam/notestore/NoteStore$updateTag_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 799
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/notestore/NoteStore$updateTag_result;->access$5700(Lcom/evernote/edam/notestore/NoteStore$updateTag_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 790
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "updateTag failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 785
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 786
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 787
    throw v0
.end method

.method public sendMessageToSharedNotebookMembers(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)I
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2854
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_sendMessageToSharedNotebookMembers(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    .line 2855
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_sendMessageToSharedNotebookMembers()I

    move-result p1

    return p1
.end method

.method public send_authenticateToSharedNote(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 3405
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "authenticateToSharedNote"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 3406
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$authenticateToSharedNote_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$authenticateToSharedNote_args;-><init>()V

    .line 3407
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$authenticateToSharedNote_args;->setGuid(Ljava/lang/String;)V

    .line 3408
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$authenticateToSharedNote_args;->setNoteKey(Ljava/lang/String;)V

    .line 3409
    invoke-virtual {v0, p3}, Lcom/evernote/edam/notestore/NoteStore$authenticateToSharedNote_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 3410
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$authenticateToSharedNote_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 3411
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 3412
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_authenticateToSharedNotebook(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 3182
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "authenticateToSharedNotebook"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 3183
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$authenticateToSharedNotebook_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$authenticateToSharedNotebook_args;-><init>()V

    .line 3184
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$authenticateToSharedNotebook_args;->setShareKey(Ljava/lang/String;)V

    .line 3185
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$authenticateToSharedNotebook_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 3186
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$authenticateToSharedNotebook_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 3187
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 3188
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_copyNote(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2017
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "copyNote"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 2018
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$copyNote_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$copyNote_args;-><init>()V

    .line 2019
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$copyNote_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 2020
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$copyNote_args;->setNoteGuid(Ljava/lang/String;)V

    .line 2021
    invoke-virtual {v0, p3}, Lcom/evernote/edam/notestore/NoteStore$copyNote_args;->setToNotebookGuid(Ljava/lang/String;)V

    .line 2022
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$copyNote_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 2023
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 2024
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_createLinkedNotebook(Ljava/lang/String;Lcom/evernote/edam/type/LinkedNotebook;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2999
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "createLinkedNotebook"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 3000
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$createLinkedNotebook_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$createLinkedNotebook_args;-><init>()V

    .line 3001
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$createLinkedNotebook_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 3002
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$createLinkedNotebook_args;->setLinkedNotebook(Lcom/evernote/edam/type/LinkedNotebook;)V

    .line 3003
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$createLinkedNotebook_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 3004
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 3005
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_createNote(Ljava/lang/String;Lcom/evernote/edam/type/Note;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1745
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "createNote"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 1746
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$createNote_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$createNote_args;-><init>()V

    .line 1747
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$createNote_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 1748
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$createNote_args;->setNote(Lcom/evernote/edam/type/Note;)V

    .line 1749
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$createNote_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1750
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 1751
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_createNotebook(Ljava/lang/String;Lcom/evernote/edam/type/Notebook;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 457
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "createNotebook"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 458
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$createNotebook_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$createNotebook_args;-><init>()V

    .line 459
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$createNotebook_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 460
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$createNotebook_args;->setNotebook(Lcom/evernote/edam/type/Notebook;)V

    .line 461
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$createNotebook_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 462
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 463
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_createSearch(Ljava/lang/String;Lcom/evernote/edam/type/SavedSearch;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 995
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "createSearch"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 996
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$createSearch_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$createSearch_args;-><init>()V

    .line 997
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$createSearch_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 998
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$createSearch_args;->setSearch(Lcom/evernote/edam/type/SavedSearch;)V

    .line 999
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$createSearch_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1000
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 1001
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_createSharedNotebook(Ljava/lang/String;Lcom/evernote/edam/type/SharedNotebook;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2721
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "createSharedNotebook"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 2722
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$createSharedNotebook_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$createSharedNotebook_args;-><init>()V

    .line 2723
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$createSharedNotebook_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 2724
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$createSharedNotebook_args;->setSharedNotebook(Lcom/evernote/edam/type/SharedNotebook;)V

    .line 2725
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$createSharedNotebook_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 2726
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 2727
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_createTag(Ljava/lang/String;Lcom/evernote/edam/type/Tag;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 726
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "createTag"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 727
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$createTag_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$createTag_args;-><init>()V

    .line 728
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$createTag_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 729
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$createTag_args;->setTag(Lcom/evernote/edam/type/Tag;)V

    .line 730
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$createTag_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 731
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 732
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_deleteNote(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1837
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "deleteNote"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 1838
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$deleteNote_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$deleteNote_args;-><init>()V

    .line 1839
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$deleteNote_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 1840
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$deleteNote_args;->setGuid(Ljava/lang/String;)V

    .line 1841
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$deleteNote_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1842
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 1843
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_emailNote(Ljava/lang/String;Lcom/evernote/edam/notestore/NoteEmailParameters;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 3273
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "emailNote"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 3274
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$emailNote_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$emailNote_args;-><init>()V

    .line 3275
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$emailNote_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 3276
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$emailNote_args;->setParameters(Lcom/evernote/edam/notestore/NoteEmailParameters;)V

    .line 3277
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$emailNote_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 3278
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 3279
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_expungeInactiveNotes(Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1975
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "expungeInactiveNotes"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 1976
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$expungeInactiveNotes_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeInactiveNotes_args;-><init>()V

    .line 1977
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$expungeInactiveNotes_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 1978
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$expungeInactiveNotes_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1979
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 1980
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_expungeLinkedNotebook(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 3136
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "expungeLinkedNotebook"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 3137
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$expungeLinkedNotebook_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeLinkedNotebook_args;-><init>()V

    .line 3138
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$expungeLinkedNotebook_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 3139
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$expungeLinkedNotebook_args;->setGuid(Ljava/lang/String;)V

    .line 3140
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$expungeLinkedNotebook_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 3141
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 3142
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_expungeNote(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1883
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "expungeNote"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 1884
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$expungeNote_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeNote_args;-><init>()V

    .line 1885
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$expungeNote_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 1886
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$expungeNote_args;->setGuid(Ljava/lang/String;)V

    .line 1887
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$expungeNote_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1888
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 1889
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_expungeNotebook(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 546
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "expungeNotebook"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 547
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$expungeNotebook_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeNotebook_args;-><init>()V

    .line 548
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$expungeNotebook_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 549
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$expungeNotebook_args;->setGuid(Ljava/lang/String;)V

    .line 550
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$expungeNotebook_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 551
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 552
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_expungeNotes(Ljava/lang/String;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1929
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "expungeNotes"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 1930
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$expungeNotes_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeNotes_args;-><init>()V

    .line 1931
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$expungeNotes_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 1932
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$expungeNotes_args;->setNoteGuids(Ljava/util/List;)V

    .line 1933
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$expungeNotes_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1934
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 1935
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_expungeSearch(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1084
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "expungeSearch"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 1085
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$expungeSearch_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeSearch_args;-><init>()V

    .line 1086
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$expungeSearch_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 1087
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$expungeSearch_args;->setGuid(Ljava/lang/String;)V

    .line 1088
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$expungeSearch_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1089
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 1090
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_expungeSharedNotebooks(Ljava/lang/String;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2953
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "expungeSharedNotebooks"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 2954
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$expungeSharedNotebooks_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeSharedNotebooks_args;-><init>()V

    .line 2955
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$expungeSharedNotebooks_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 2956
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$expungeSharedNotebooks_args;->setSharedNotebookIds(Ljava/util/List;)V

    .line 2957
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$expungeSharedNotebooks_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 2958
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 2959
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_expungeTag(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 861
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "expungeTag"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 862
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$expungeTag_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$expungeTag_args;-><init>()V

    .line 863
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$expungeTag_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 864
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$expungeTag_args;->setGuid(Ljava/lang/String;)V

    .line 865
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$expungeTag_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 866
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 867
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_findNoteCounts(Ljava/lang/String;Lcom/evernote/edam/notestore/NoteFilter;Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1274
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "findNoteCounts"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 1275
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$findNoteCounts_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$findNoteCounts_args;-><init>()V

    .line 1276
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$findNoteCounts_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 1277
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$findNoteCounts_args;->setFilter(Lcom/evernote/edam/notestore/NoteFilter;)V

    .line 1278
    invoke-virtual {v0, p3}, Lcom/evernote/edam/notestore/NoteStore$findNoteCounts_args;->setWithTrash(Z)V

    .line 1279
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$findNoteCounts_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1280
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 1281
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_findNoteOffset(Ljava/lang/String;Lcom/evernote/edam/notestore/NoteFilter;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1178
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "findNoteOffset"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 1179
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$findNoteOffset_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$findNoteOffset_args;-><init>()V

    .line 1180
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$findNoteOffset_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 1181
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$findNoteOffset_args;->setFilter(Lcom/evernote/edam/notestore/NoteFilter;)V

    .line 1182
    invoke-virtual {v0, p3}, Lcom/evernote/edam/notestore/NoteStore$findNoteOffset_args;->setGuid(Ljava/lang/String;)V

    .line 1183
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$findNoteOffset_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1184
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 1185
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_findNotes(Ljava/lang/String;Lcom/evernote/edam/notestore/NoteFilter;II)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1130
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "findNotes"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 1131
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$findNotes_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$findNotes_args;-><init>()V

    .line 1132
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$findNotes_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 1133
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$findNotes_args;->setFilter(Lcom/evernote/edam/notestore/NoteFilter;)V

    .line 1134
    invoke-virtual {v0, p3}, Lcom/evernote/edam/notestore/NoteStore$findNotes_args;->setOffset(I)V

    .line 1135
    invoke-virtual {v0, p4}, Lcom/evernote/edam/notestore/NoteStore$findNotes_args;->setMaxNotes(I)V

    .line 1136
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$findNotes_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1137
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 1138
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_findNotesMetadata(Ljava/lang/String;Lcom/evernote/edam/notestore/NoteFilter;IILcom/evernote/edam/notestore/NotesMetadataResultSpec;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1225
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "findNotesMetadata"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 1226
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$findNotesMetadata_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$findNotesMetadata_args;-><init>()V

    .line 1227
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$findNotesMetadata_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 1228
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$findNotesMetadata_args;->setFilter(Lcom/evernote/edam/notestore/NoteFilter;)V

    .line 1229
    invoke-virtual {v0, p3}, Lcom/evernote/edam/notestore/NoteStore$findNotesMetadata_args;->setOffset(I)V

    .line 1230
    invoke-virtual {v0, p4}, Lcom/evernote/edam/notestore/NoteStore$findNotesMetadata_args;->setMaxNotes(I)V

    .line 1231
    invoke-virtual {v0, p5}, Lcom/evernote/edam/notestore/NoteStore$findNotesMetadata_args;->setResultSpec(Lcom/evernote/edam/notestore/NotesMetadataResultSpec;)V

    .line 1232
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$findNotesMetadata_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1233
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 1234
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_findRelated(Ljava/lang/String;Lcom/evernote/edam/notestore/RelatedQuery;Lcom/evernote/edam/notestore/RelatedResultSpec;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 3452
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "findRelated"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 3453
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$findRelated_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$findRelated_args;-><init>()V

    .line 3454
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$findRelated_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 3455
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$findRelated_args;->setQuery(Lcom/evernote/edam/notestore/RelatedQuery;)V

    .line 3456
    invoke-virtual {v0, p3}, Lcom/evernote/edam/notestore/NoteStore$findRelated_args;->setResultSpec(Lcom/evernote/edam/notestore/RelatedResultSpec;)V

    .line 3457
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$findRelated_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 3458
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 3459
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_getDefaultNotebook(Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 415
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "getDefaultNotebook"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 416
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getDefaultNotebook_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getDefaultNotebook_args;-><init>()V

    .line 417
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getDefaultNotebook_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 418
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getDefaultNotebook_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 419
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 420
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_getFilteredSyncChunk(Ljava/lang/String;IILcom/evernote/edam/notestore/SyncChunkFilter;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 187
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "getFilteredSyncChunk"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 188
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getFilteredSyncChunk_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getFilteredSyncChunk_args;-><init>()V

    .line 189
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getFilteredSyncChunk_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 190
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$getFilteredSyncChunk_args;->setAfterUSN(I)V

    .line 191
    invoke-virtual {v0, p3}, Lcom/evernote/edam/notestore/NoteStore$getFilteredSyncChunk_args;->setMaxEntries(I)V

    .line 192
    invoke-virtual {v0, p4}, Lcom/evernote/edam/notestore/NoteStore$getFilteredSyncChunk_args;->setFilter(Lcom/evernote/edam/notestore/SyncChunkFilter;)V

    .line 193
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getFilteredSyncChunk_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 194
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 195
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_getLinkedNotebookSyncChunk(Ljava/lang/String;Lcom/evernote/edam/type/LinkedNotebook;IIZ)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 278
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "getLinkedNotebookSyncChunk"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 279
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getLinkedNotebookSyncChunk_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getLinkedNotebookSyncChunk_args;-><init>()V

    .line 280
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getLinkedNotebookSyncChunk_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 281
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$getLinkedNotebookSyncChunk_args;->setLinkedNotebook(Lcom/evernote/edam/type/LinkedNotebook;)V

    .line 282
    invoke-virtual {v0, p3}, Lcom/evernote/edam/notestore/NoteStore$getLinkedNotebookSyncChunk_args;->setAfterUSN(I)V

    .line 283
    invoke-virtual {v0, p4}, Lcom/evernote/edam/notestore/NoteStore$getLinkedNotebookSyncChunk_args;->setMaxEntries(I)V

    .line 284
    invoke-virtual {v0, p5}, Lcom/evernote/edam/notestore/NoteStore$getLinkedNotebookSyncChunk_args;->setFullSyncOnly(Z)V

    .line 285
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getLinkedNotebookSyncChunk_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 286
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 287
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_getLinkedNotebookSyncState(Ljava/lang/String;Lcom/evernote/edam/type/LinkedNotebook;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 232
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "getLinkedNotebookSyncState"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 233
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getLinkedNotebookSyncState_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getLinkedNotebookSyncState_args;-><init>()V

    .line 234
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getLinkedNotebookSyncState_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 235
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$getLinkedNotebookSyncState_args;->setLinkedNotebook(Lcom/evernote/edam/type/LinkedNotebook;)V

    .line 236
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getLinkedNotebookSyncState_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 237
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 238
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_getNote(Ljava/lang/String;Ljava/lang/String;ZZZZ)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1321
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "getNote"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 1322
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getNote_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getNote_args;-><init>()V

    .line 1323
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getNote_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 1324
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$getNote_args;->setGuid(Ljava/lang/String;)V

    .line 1325
    invoke-virtual {v0, p3}, Lcom/evernote/edam/notestore/NoteStore$getNote_args;->setWithContent(Z)V

    .line 1326
    invoke-virtual {v0, p4}, Lcom/evernote/edam/notestore/NoteStore$getNote_args;->setWithResourcesData(Z)V

    .line 1327
    invoke-virtual {v0, p5}, Lcom/evernote/edam/notestore/NoteStore$getNote_args;->setWithResourcesRecognition(Z)V

    .line 1328
    invoke-virtual {v0, p6}, Lcom/evernote/edam/notestore/NoteStore$getNote_args;->setWithResourcesAlternateData(Z)V

    .line 1329
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getNote_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1330
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 1331
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_getNoteApplicationData(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1371
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "getNoteApplicationData"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 1372
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getNoteApplicationData_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteApplicationData_args;-><init>()V

    .line 1373
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getNoteApplicationData_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 1374
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$getNoteApplicationData_args;->setGuid(Ljava/lang/String;)V

    .line 1375
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getNoteApplicationData_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1376
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 1377
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_getNoteApplicationDataEntry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1417
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "getNoteApplicationDataEntry"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 1418
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getNoteApplicationDataEntry_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteApplicationDataEntry_args;-><init>()V

    .line 1419
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getNoteApplicationDataEntry_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 1420
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$getNoteApplicationDataEntry_args;->setGuid(Ljava/lang/String;)V

    .line 1421
    invoke-virtual {v0, p3}, Lcom/evernote/edam/notestore/NoteStore$getNoteApplicationDataEntry_args;->setKey(Ljava/lang/String;)V

    .line 1422
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getNoteApplicationDataEntry_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1423
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 1424
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_getNoteContent(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1559
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "getNoteContent"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 1560
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getNoteContent_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteContent_args;-><init>()V

    .line 1561
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getNoteContent_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 1562
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$getNoteContent_args;->setGuid(Ljava/lang/String;)V

    .line 1563
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getNoteContent_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1564
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 1565
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_getNoteSearchText(Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1605
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "getNoteSearchText"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 1606
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getNoteSearchText_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteSearchText_args;-><init>()V

    .line 1607
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getNoteSearchText_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 1608
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$getNoteSearchText_args;->setGuid(Ljava/lang/String;)V

    .line 1609
    invoke-virtual {v0, p3}, Lcom/evernote/edam/notestore/NoteStore$getNoteSearchText_args;->setNoteOnly(Z)V

    .line 1610
    invoke-virtual {v0, p4}, Lcom/evernote/edam/notestore/NoteStore$getNoteSearchText_args;->setTokenizeForIndexing(Z)V

    .line 1611
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getNoteSearchText_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1612
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 1613
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_getNoteTagNames(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1699
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "getNoteTagNames"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 1700
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getNoteTagNames_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteTagNames_args;-><init>()V

    .line 1701
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getNoteTagNames_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 1702
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$getNoteTagNames_args;->setGuid(Ljava/lang/String;)V

    .line 1703
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getNoteTagNames_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1704
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 1705
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_getNoteVersion(Ljava/lang/String;Ljava/lang/String;IZZZ)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2110
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "getNoteVersion"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 2111
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getNoteVersion_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getNoteVersion_args;-><init>()V

    .line 2112
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getNoteVersion_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 2113
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$getNoteVersion_args;->setNoteGuid(Ljava/lang/String;)V

    .line 2114
    invoke-virtual {v0, p3}, Lcom/evernote/edam/notestore/NoteStore$getNoteVersion_args;->setUpdateSequenceNum(I)V

    .line 2115
    invoke-virtual {v0, p4}, Lcom/evernote/edam/notestore/NoteStore$getNoteVersion_args;->setWithResourcesData(Z)V

    .line 2116
    invoke-virtual {v0, p5}, Lcom/evernote/edam/notestore/NoteStore$getNoteVersion_args;->setWithResourcesRecognition(Z)V

    .line 2117
    invoke-virtual {v0, p6}, Lcom/evernote/edam/notestore/NoteStore$getNoteVersion_args;->setWithResourcesAlternateData(Z)V

    .line 2118
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getNoteVersion_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 2119
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 2120
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_getNotebook(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 369
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "getNotebook"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 370
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getNotebook_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getNotebook_args;-><init>()V

    .line 371
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getNotebook_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 372
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$getNotebook_args;->setGuid(Ljava/lang/String;)V

    .line 373
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getNotebook_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 374
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 375
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_getPublicNotebook(ILjava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2678
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "getPublicNotebook"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 2679
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getPublicNotebook_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getPublicNotebook_args;-><init>()V

    .line 2680
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getPublicNotebook_args;->setUserId(I)V

    .line 2681
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$getPublicNotebook_args;->setPublicUri(Ljava/lang/String;)V

    .line 2682
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getPublicNotebook_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 2683
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 2684
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_getResource(Ljava/lang/String;Ljava/lang/String;ZZZZ)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2160
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "getResource"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 2161
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getResource_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getResource_args;-><init>()V

    .line 2162
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getResource_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 2163
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$getResource_args;->setGuid(Ljava/lang/String;)V

    .line 2164
    invoke-virtual {v0, p3}, Lcom/evernote/edam/notestore/NoteStore$getResource_args;->setWithData(Z)V

    .line 2165
    invoke-virtual {v0, p4}, Lcom/evernote/edam/notestore/NoteStore$getResource_args;->setWithRecognition(Z)V

    .line 2166
    invoke-virtual {v0, p5}, Lcom/evernote/edam/notestore/NoteStore$getResource_args;->setWithAttributes(Z)V

    .line 2167
    invoke-virtual {v0, p6}, Lcom/evernote/edam/notestore/NoteStore$getResource_args;->setWithAlternateData(Z)V

    .line 2168
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getResource_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 2169
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 2170
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_getResourceAlternateData(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2586
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "getResourceAlternateData"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 2587
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getResourceAlternateData_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceAlternateData_args;-><init>()V

    .line 2588
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getResourceAlternateData_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 2589
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$getResourceAlternateData_args;->setGuid(Ljava/lang/String;)V

    .line 2590
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getResourceAlternateData_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 2591
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 2592
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_getResourceApplicationData(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2210
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "getResourceApplicationData"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 2211
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getResourceApplicationData_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceApplicationData_args;-><init>()V

    .line 2212
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getResourceApplicationData_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 2213
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$getResourceApplicationData_args;->setGuid(Ljava/lang/String;)V

    .line 2214
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getResourceApplicationData_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 2215
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 2216
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_getResourceApplicationDataEntry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2256
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "getResourceApplicationDataEntry"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 2257
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getResourceApplicationDataEntry_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceApplicationDataEntry_args;-><init>()V

    .line 2258
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getResourceApplicationDataEntry_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 2259
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$getResourceApplicationDataEntry_args;->setGuid(Ljava/lang/String;)V

    .line 2260
    invoke-virtual {v0, p3}, Lcom/evernote/edam/notestore/NoteStore$getResourceApplicationDataEntry_args;->setKey(Ljava/lang/String;)V

    .line 2261
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getResourceApplicationDataEntry_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 2262
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 2263
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_getResourceAttributes(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2632
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "getResourceAttributes"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 2633
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getResourceAttributes_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceAttributes_args;-><init>()V

    .line 2634
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getResourceAttributes_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 2635
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$getResourceAttributes_args;->setGuid(Ljava/lang/String;)V

    .line 2636
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getResourceAttributes_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 2637
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 2638
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_getResourceByHash(Ljava/lang/String;Ljava/lang/String;[BZZZ)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2490
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "getResourceByHash"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 2491
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getResourceByHash_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceByHash_args;-><init>()V

    .line 2492
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getResourceByHash_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 2493
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$getResourceByHash_args;->setNoteGuid(Ljava/lang/String;)V

    .line 2494
    invoke-virtual {v0, p3}, Lcom/evernote/edam/notestore/NoteStore$getResourceByHash_args;->setContentHash([B)V

    .line 2495
    invoke-virtual {v0, p4}, Lcom/evernote/edam/notestore/NoteStore$getResourceByHash_args;->setWithData(Z)V

    .line 2496
    invoke-virtual {v0, p5}, Lcom/evernote/edam/notestore/NoteStore$getResourceByHash_args;->setWithRecognition(Z)V

    .line 2497
    invoke-virtual {v0, p6}, Lcom/evernote/edam/notestore/NoteStore$getResourceByHash_args;->setWithAlternateData(Z)V

    .line 2498
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getResourceByHash_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 2499
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 2500
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_getResourceData(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2444
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "getResourceData"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 2445
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getResourceData_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceData_args;-><init>()V

    .line 2446
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getResourceData_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 2447
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$getResourceData_args;->setGuid(Ljava/lang/String;)V

    .line 2448
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getResourceData_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 2449
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 2450
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_getResourceRecognition(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2540
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "getResourceRecognition"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 2541
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getResourceRecognition_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceRecognition_args;-><init>()V

    .line 2542
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getResourceRecognition_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 2543
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$getResourceRecognition_args;->setGuid(Ljava/lang/String;)V

    .line 2544
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getResourceRecognition_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 2545
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 2546
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_getResourceSearchText(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1653
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "getResourceSearchText"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 1654
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getResourceSearchText_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getResourceSearchText_args;-><init>()V

    .line 1655
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getResourceSearchText_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 1656
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$getResourceSearchText_args;->setGuid(Ljava/lang/String;)V

    .line 1657
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getResourceSearchText_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1658
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 1659
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_getSearch(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 949
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "getSearch"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 950
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getSearch_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getSearch_args;-><init>()V

    .line 951
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getSearch_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 952
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$getSearch_args;->setGuid(Ljava/lang/String;)V

    .line 953
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getSearch_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 954
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 955
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_getSharedNotebookByAuth(Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 3228
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "getSharedNotebookByAuth"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 3229
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getSharedNotebookByAuth_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getSharedNotebookByAuth_args;-><init>()V

    .line 3230
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getSharedNotebookByAuth_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 3231
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getSharedNotebookByAuth_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 3232
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 3233
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_getSyncChunk(Ljava/lang/String;IIZ)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 142
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "getSyncChunk"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 143
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getSyncChunk_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getSyncChunk_args;-><init>()V

    .line 144
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getSyncChunk_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 145
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$getSyncChunk_args;->setAfterUSN(I)V

    .line 146
    invoke-virtual {v0, p3}, Lcom/evernote/edam/notestore/NoteStore$getSyncChunk_args;->setMaxEntries(I)V

    .line 147
    invoke-virtual {v0, p4}, Lcom/evernote/edam/notestore/NoteStore$getSyncChunk_args;->setFullSyncOnly(Z)V

    .line 148
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getSyncChunk_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 149
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 150
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_getSyncState(Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 57
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "getSyncState"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 58
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getSyncState_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getSyncState_args;-><init>()V

    .line 59
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getSyncState_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 60
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getSyncState_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 61
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 62
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_getSyncStateWithMetrics(Ljava/lang/String;Lcom/evernote/edam/notestore/ClientUsageMetrics;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 99
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "getSyncStateWithMetrics"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 100
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getSyncStateWithMetrics_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getSyncStateWithMetrics_args;-><init>()V

    .line 101
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getSyncStateWithMetrics_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 102
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$getSyncStateWithMetrics_args;->setClientMetrics(Lcom/evernote/edam/notestore/ClientUsageMetrics;)V

    .line 103
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getSyncStateWithMetrics_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 104
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 105
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_getTag(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 680
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "getTag"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 681
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$getTag_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$getTag_args;-><init>()V

    .line 682
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getTag_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 683
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$getTag_args;->setGuid(Ljava/lang/String;)V

    .line 684
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$getTag_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 685
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 686
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_listLinkedNotebooks(Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 3091
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "listLinkedNotebooks"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 3092
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$listLinkedNotebooks_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$listLinkedNotebooks_args;-><init>()V

    .line 3093
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$listLinkedNotebooks_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 3094
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$listLinkedNotebooks_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 3095
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 3096
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_listNoteVersions(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2064
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "listNoteVersions"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 2065
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$listNoteVersions_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$listNoteVersions_args;-><init>()V

    .line 2066
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$listNoteVersions_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 2067
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$listNoteVersions_args;->setNoteGuid(Ljava/lang/String;)V

    .line 2068
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$listNoteVersions_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 2069
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 2070
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_listNotebooks(Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 327
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "listNotebooks"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 328
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$listNotebooks_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$listNotebooks_args;-><init>()V

    .line 329
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$listNotebooks_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 330
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$listNotebooks_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 331
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 332
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_listSearches(Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 907
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "listSearches"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 908
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$listSearches_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$listSearches_args;-><init>()V

    .line 909
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$listSearches_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 910
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$listSearches_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 911
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 912
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_listSharedNotebooks(Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2908
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "listSharedNotebooks"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 2909
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$listSharedNotebooks_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$listSharedNotebooks_args;-><init>()V

    .line 2910
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$listSharedNotebooks_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 2911
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$listSharedNotebooks_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 2912
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 2913
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_listTags(Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 592
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "listTags"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 593
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$listTags_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$listTags_args;-><init>()V

    .line 594
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$listTags_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 595
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$listTags_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 596
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 597
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_listTagsByNotebook(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 634
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "listTagsByNotebook"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 635
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$listTagsByNotebook_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$listTagsByNotebook_args;-><init>()V

    .line 636
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$listTagsByNotebook_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 637
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$listTagsByNotebook_args;->setNotebookGuid(Ljava/lang/String;)V

    .line 638
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$listTagsByNotebook_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 639
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 640
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_sendMessageToSharedNotebookMembers(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2860
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "sendMessageToSharedNotebookMembers"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 2861
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$sendMessageToSharedNotebookMembers_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$sendMessageToSharedNotebookMembers_args;-><init>()V

    .line 2862
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$sendMessageToSharedNotebookMembers_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 2863
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$sendMessageToSharedNotebookMembers_args;->setNotebookGuid(Ljava/lang/String;)V

    .line 2864
    invoke-virtual {v0, p3}, Lcom/evernote/edam/notestore/NoteStore$sendMessageToSharedNotebookMembers_args;->setMessageText(Ljava/lang/String;)V

    .line 2865
    invoke-virtual {v0, p4}, Lcom/evernote/edam/notestore/NoteStore$sendMessageToSharedNotebookMembers_args;->setRecipients(Ljava/util/List;)V

    .line 2866
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$sendMessageToSharedNotebookMembers_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 2867
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 2868
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_setNoteApplicationDataEntry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1464
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "setNoteApplicationDataEntry"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 1465
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$setNoteApplicationDataEntry_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$setNoteApplicationDataEntry_args;-><init>()V

    .line 1466
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$setNoteApplicationDataEntry_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 1467
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$setNoteApplicationDataEntry_args;->setGuid(Ljava/lang/String;)V

    .line 1468
    invoke-virtual {v0, p3}, Lcom/evernote/edam/notestore/NoteStore$setNoteApplicationDataEntry_args;->setKey(Ljava/lang/String;)V

    .line 1469
    invoke-virtual {v0, p4}, Lcom/evernote/edam/notestore/NoteStore$setNoteApplicationDataEntry_args;->setValue(Ljava/lang/String;)V

    .line 1470
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$setNoteApplicationDataEntry_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1471
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 1472
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_setResourceApplicationDataEntry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2303
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "setResourceApplicationDataEntry"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 2304
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$setResourceApplicationDataEntry_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$setResourceApplicationDataEntry_args;-><init>()V

    .line 2305
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$setResourceApplicationDataEntry_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 2306
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$setResourceApplicationDataEntry_args;->setGuid(Ljava/lang/String;)V

    .line 2307
    invoke-virtual {v0, p3}, Lcom/evernote/edam/notestore/NoteStore$setResourceApplicationDataEntry_args;->setKey(Ljava/lang/String;)V

    .line 2308
    invoke-virtual {v0, p4}, Lcom/evernote/edam/notestore/NoteStore$setResourceApplicationDataEntry_args;->setValue(Ljava/lang/String;)V

    .line 2309
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$setResourceApplicationDataEntry_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 2310
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 2311
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_setSharedNotebookRecipientSettings(Ljava/lang/String;JLcom/evernote/edam/type/SharedNotebookRecipientSettings;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2813
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "setSharedNotebookRecipientSettings"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 2814
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$setSharedNotebookRecipientSettings_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$setSharedNotebookRecipientSettings_args;-><init>()V

    .line 2815
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$setSharedNotebookRecipientSettings_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 2816
    invoke-virtual {v0, p2, p3}, Lcom/evernote/edam/notestore/NoteStore$setSharedNotebookRecipientSettings_args;->setSharedNotebookId(J)V

    .line 2817
    invoke-virtual {v0, p4}, Lcom/evernote/edam/notestore/NoteStore$setSharedNotebookRecipientSettings_args;->setRecipientSettings(Lcom/evernote/edam/type/SharedNotebookRecipientSettings;)V

    .line 2818
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$setSharedNotebookRecipientSettings_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 2819
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 2820
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_shareNote(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 3316
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "shareNote"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 3317
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$shareNote_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$shareNote_args;-><init>()V

    .line 3318
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$shareNote_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 3319
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$shareNote_args;->setGuid(Ljava/lang/String;)V

    .line 3320
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$shareNote_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 3321
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 3322
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_stopSharingNote(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 3362
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "stopSharingNote"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 3363
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$stopSharingNote_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$stopSharingNote_args;-><init>()V

    .line 3364
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$stopSharingNote_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 3365
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$stopSharingNote_args;->setGuid(Ljava/lang/String;)V

    .line 3366
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$stopSharingNote_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 3367
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 3368
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_unsetNoteApplicationDataEntry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1512
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "unsetNoteApplicationDataEntry"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 1513
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$unsetNoteApplicationDataEntry_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$unsetNoteApplicationDataEntry_args;-><init>()V

    .line 1514
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$unsetNoteApplicationDataEntry_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 1515
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$unsetNoteApplicationDataEntry_args;->setGuid(Ljava/lang/String;)V

    .line 1516
    invoke-virtual {v0, p3}, Lcom/evernote/edam/notestore/NoteStore$unsetNoteApplicationDataEntry_args;->setKey(Ljava/lang/String;)V

    .line 1517
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$unsetNoteApplicationDataEntry_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1518
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 1519
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_unsetResourceApplicationDataEntry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2351
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "unsetResourceApplicationDataEntry"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 2352
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$unsetResourceApplicationDataEntry_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$unsetResourceApplicationDataEntry_args;-><init>()V

    .line 2353
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$unsetResourceApplicationDataEntry_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 2354
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$unsetResourceApplicationDataEntry_args;->setGuid(Ljava/lang/String;)V

    .line 2355
    invoke-virtual {v0, p3}, Lcom/evernote/edam/notestore/NoteStore$unsetResourceApplicationDataEntry_args;->setKey(Ljava/lang/String;)V

    .line 2356
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$unsetResourceApplicationDataEntry_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 2357
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 2358
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_untagAll(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 818
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "untagAll"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 819
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$untagAll_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$untagAll_args;-><init>()V

    .line 820
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$untagAll_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 821
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$untagAll_args;->setGuid(Ljava/lang/String;)V

    .line 822
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$untagAll_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 823
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 824
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_updateLinkedNotebook(Ljava/lang/String;Lcom/evernote/edam/type/LinkedNotebook;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 3045
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "updateLinkedNotebook"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 3046
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$updateLinkedNotebook_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$updateLinkedNotebook_args;-><init>()V

    .line 3047
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$updateLinkedNotebook_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 3048
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$updateLinkedNotebook_args;->setLinkedNotebook(Lcom/evernote/edam/type/LinkedNotebook;)V

    .line 3049
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$updateLinkedNotebook_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 3050
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 3051
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_updateNote(Ljava/lang/String;Lcom/evernote/edam/type/Note;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1791
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "updateNote"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 1792
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$updateNote_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$updateNote_args;-><init>()V

    .line 1793
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$updateNote_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 1794
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$updateNote_args;->setNote(Lcom/evernote/edam/type/Note;)V

    .line 1795
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$updateNote_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1796
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 1797
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_updateNotebook(Ljava/lang/String;Lcom/evernote/edam/type/Notebook;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 500
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "updateNotebook"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 501
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$updateNotebook_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$updateNotebook_args;-><init>()V

    .line 502
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$updateNotebook_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 503
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$updateNotebook_args;->setNotebook(Lcom/evernote/edam/type/Notebook;)V

    .line 504
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$updateNotebook_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 505
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 506
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_updateResource(Ljava/lang/String;Lcom/evernote/edam/type/Resource;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2398
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "updateResource"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 2399
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$updateResource_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$updateResource_args;-><init>()V

    .line 2400
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$updateResource_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 2401
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$updateResource_args;->setResource(Lcom/evernote/edam/type/Resource;)V

    .line 2402
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$updateResource_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 2403
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 2404
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_updateSearch(Ljava/lang/String;Lcom/evernote/edam/type/SavedSearch;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1038
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "updateSearch"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 1039
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$updateSearch_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$updateSearch_args;-><init>()V

    .line 1040
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$updateSearch_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 1041
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$updateSearch_args;->setSearch(Lcom/evernote/edam/type/SavedSearch;)V

    .line 1042
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$updateSearch_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 1043
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 1044
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_updateSharedNotebook(Ljava/lang/String;Lcom/evernote/edam/type/SharedNotebook;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2767
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "updateSharedNotebook"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 2768
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$updateSharedNotebook_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$updateSharedNotebook_args;-><init>()V

    .line 2769
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$updateSharedNotebook_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 2770
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$updateSharedNotebook_args;->setSharedNotebook(Lcom/evernote/edam/type/SharedNotebook;)V

    .line 2771
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$updateSharedNotebook_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 2772
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 2773
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_updateTag(Ljava/lang/String;Lcom/evernote/edam/type/Tag;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 772
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "updateTag"

    iget v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 773
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$updateTag_args;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteStore$updateTag_args;-><init>()V

    .line 774
    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$updateTag_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 775
    invoke-virtual {v0, p2}, Lcom/evernote/edam/notestore/NoteStore$updateTag_args;->setTag(Lcom/evernote/edam/type/Tag;)V

    .line 776
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$updateTag_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 777
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 778
    iget-object p1, p0, Lcom/evernote/edam/notestore/NoteStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public setNoteApplicationDataEntry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1458
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_setNoteApplicationDataEntry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1459
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_setNoteApplicationDataEntry()I

    move-result p1

    return p1
.end method

.method public setResourceApplicationDataEntry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2297
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_setResourceApplicationDataEntry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2298
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_setResourceApplicationDataEntry()I

    move-result p1

    return p1
.end method

.method public setSharedNotebookRecipientSettings(Ljava/lang/String;JLcom/evernote/edam/type/SharedNotebookRecipientSettings;)I
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2807
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_setSharedNotebookRecipientSettings(Ljava/lang/String;JLcom/evernote/edam/type/SharedNotebookRecipientSettings;)V

    .line 2808
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_setSharedNotebookRecipientSettings()I

    move-result p1

    return p1
.end method

.method public shareNote(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 3310
    invoke-virtual {p0, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_shareNote(Ljava/lang/String;Ljava/lang/String;)V

    .line 3311
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_shareNote()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public stopSharingNote(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 3356
    invoke-virtual {p0, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_stopSharingNote(Ljava/lang/String;Ljava/lang/String;)V

    .line 3357
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_stopSharingNote()V

    return-void
.end method

.method public unsetNoteApplicationDataEntry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1506
    invoke-virtual {p0, p1, p2, p3}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_unsetNoteApplicationDataEntry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1507
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_unsetNoteApplicationDataEntry()I

    move-result p1

    return p1
.end method

.method public unsetResourceApplicationDataEntry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2345
    invoke-virtual {p0, p1, p2, p3}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_unsetResourceApplicationDataEntry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2346
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_unsetResourceApplicationDataEntry()I

    move-result p1

    return p1
.end method

.method public untagAll(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 812
    invoke-virtual {p0, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_untagAll(Ljava/lang/String;Ljava/lang/String;)V

    .line 813
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_untagAll()V

    return-void
.end method

.method public updateLinkedNotebook(Ljava/lang/String;Lcom/evernote/edam/type/LinkedNotebook;)I
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 3039
    invoke-virtual {p0, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_updateLinkedNotebook(Ljava/lang/String;Lcom/evernote/edam/type/LinkedNotebook;)V

    .line 3040
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_updateLinkedNotebook()I

    move-result p1

    return p1
.end method

.method public updateNote(Ljava/lang/String;Lcom/evernote/edam/type/Note;)Lcom/evernote/edam/type/Note;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1785
    invoke-virtual {p0, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_updateNote(Ljava/lang/String;Lcom/evernote/edam/type/Note;)V

    .line 1786
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_updateNote()Lcom/evernote/edam/type/Note;

    move-result-object p1

    return-object p1
.end method

.method public updateNotebook(Ljava/lang/String;Lcom/evernote/edam/type/Notebook;)I
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 494
    invoke-virtual {p0, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_updateNotebook(Ljava/lang/String;Lcom/evernote/edam/type/Notebook;)V

    .line 495
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_updateNotebook()I

    move-result p1

    return p1
.end method

.method public updateResource(Ljava/lang/String;Lcom/evernote/edam/type/Resource;)I
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2392
    invoke-virtual {p0, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_updateResource(Ljava/lang/String;Lcom/evernote/edam/type/Resource;)V

    .line 2393
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_updateResource()I

    move-result p1

    return p1
.end method

.method public updateSearch(Ljava/lang/String;Lcom/evernote/edam/type/SavedSearch;)I
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1032
    invoke-virtual {p0, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_updateSearch(Ljava/lang/String;Lcom/evernote/edam/type/SavedSearch;)V

    .line 1033
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_updateSearch()I

    move-result p1

    return p1
.end method

.method public updateSharedNotebook(Ljava/lang/String;Lcom/evernote/edam/type/SharedNotebook;)I
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 2761
    invoke-virtual {p0, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_updateSharedNotebook(Ljava/lang/String;Lcom/evernote/edam/type/SharedNotebook;)V

    .line 2762
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_updateSharedNotebook()I

    move-result p1

    return p1
.end method

.method public updateTag(Ljava/lang/String;Lcom/evernote/edam/type/Tag;)I
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 766
    invoke-virtual {p0, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->send_updateTag(Ljava/lang/String;Lcom/evernote/edam/type/Tag;)V

    .line 767
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteStore$Client;->recv_updateTag()I

    move-result p1

    return p1
.end method

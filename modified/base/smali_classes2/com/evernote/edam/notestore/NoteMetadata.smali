.class public Lcom/evernote/edam/notestore/NoteMetadata;
.super Ljava/lang/Object;
.source "NoteMetadata.java"

# interfaces
.implements Lcom/evernote/thrift/TBase;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/evernote/thrift/TBase<",
        "Lcom/evernote/edam/notestore/NoteMetadata;",
        ">;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# static fields
.field private static final ATTRIBUTES_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final CONTENT_LENGTH_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final CREATED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final DELETED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final GUID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final LARGEST_RESOURCE_MIME_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final LARGEST_RESOURCE_SIZE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final NOTEBOOK_GUID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

.field private static final TAG_GUIDS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final TITLE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final UPDATED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final UPDATE_SEQUENCE_NUM_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final __CONTENTLENGTH_ISSET_ID:I = 0x0

.field private static final __CREATED_ISSET_ID:I = 0x1

.field private static final __DELETED_ISSET_ID:I = 0x3

.field private static final __LARGESTRESOURCESIZE_ISSET_ID:I = 0x5

.field private static final __UPDATED_ISSET_ID:I = 0x2

.field private static final __UPDATESEQUENCENUM_ISSET_ID:I = 0x4


# instance fields
.field private __isset_vector:[Z

.field private attributes:Lcom/evernote/edam/type/NoteAttributes;

.field private contentLength:I

.field private created:J

.field private deleted:J

.field private guid:Ljava/lang/String;

.field private largestResourceMime:Ljava/lang/String;

.field private largestResourceSize:I

.field private notebookGuid:Ljava/lang/String;

.field private tagGuids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private title:Ljava/lang/String;

.field private updateSequenceNum:I

.field private updated:J


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 42
    new-instance v0, Lcom/evernote/thrift/protocol/TStruct;

    const-string v1, "NoteMetadata"

    invoke-direct {v0, v1}, Lcom/evernote/thrift/protocol/TStruct;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/evernote/edam/notestore/NoteMetadata;->STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

    .line 44
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "guid"

    const/16 v2, 0xb

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/notestore/NoteMetadata;->GUID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 45
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "title"

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/notestore/NoteMetadata;->TITLE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 46
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "contentLength"

    const/16 v3, 0x8

    const/4 v4, 0x5

    invoke-direct {v0, v1, v3, v4}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/notestore/NoteMetadata;->CONTENT_LENGTH_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 47
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "created"

    const/16 v4, 0xa

    const/4 v5, 0x6

    invoke-direct {v0, v1, v4, v5}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/notestore/NoteMetadata;->CREATED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 48
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "updated"

    const/4 v5, 0x7

    invoke-direct {v0, v1, v4, v5}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/notestore/NoteMetadata;->UPDATED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 49
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "deleted"

    invoke-direct {v0, v1, v4, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/notestore/NoteMetadata;->DELETED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 50
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "updateSequenceNum"

    invoke-direct {v0, v1, v3, v4}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/notestore/NoteMetadata;->UPDATE_SEQUENCE_NUM_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 51
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "notebookGuid"

    invoke-direct {v0, v1, v2, v2}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/notestore/NoteMetadata;->NOTEBOOK_GUID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 52
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "tagGuids"

    const/16 v4, 0xc

    const/16 v5, 0xf

    invoke-direct {v0, v1, v5, v4}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/notestore/NoteMetadata;->TAG_GUIDS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 53
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "attributes"

    const/16 v5, 0xe

    invoke-direct {v0, v1, v4, v5}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/notestore/NoteMetadata;->ATTRIBUTES_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 54
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "largestResourceMime"

    const/16 v4, 0x14

    invoke-direct {v0, v1, v2, v4}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/notestore/NoteMetadata;->LARGEST_RESOURCE_MIME_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 55
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "largestResourceSize"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v3, v2}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/notestore/NoteMetadata;->LARGEST_RESOURCE_SIZE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x6

    .line 78
    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->__isset_vector:[Z

    return-void
.end method

.method public constructor <init>(Lcom/evernote/edam/notestore/NoteMetadata;)V
    .locals 4

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x6

    .line 78
    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->__isset_vector:[Z

    .line 94
    iget-object v0, p1, Lcom/evernote/edam/notestore/NoteMetadata;->__isset_vector:[Z

    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteMetadata;->__isset_vector:[Z

    array-length v2, v0

    const/4 v3, 0x0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 95
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetGuid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p1, Lcom/evernote/edam/notestore/NoteMetadata;->guid:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->guid:Ljava/lang/String;

    .line 98
    :cond_0
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetTitle()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 99
    iget-object v0, p1, Lcom/evernote/edam/notestore/NoteMetadata;->title:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->title:Ljava/lang/String;

    .line 101
    :cond_1
    iget v0, p1, Lcom/evernote/edam/notestore/NoteMetadata;->contentLength:I

    iput v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->contentLength:I

    .line 102
    iget-wide v0, p1, Lcom/evernote/edam/notestore/NoteMetadata;->created:J

    iput-wide v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->created:J

    .line 103
    iget-wide v0, p1, Lcom/evernote/edam/notestore/NoteMetadata;->updated:J

    iput-wide v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->updated:J

    .line 104
    iget-wide v0, p1, Lcom/evernote/edam/notestore/NoteMetadata;->deleted:J

    iput-wide v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->deleted:J

    .line 105
    iget v0, p1, Lcom/evernote/edam/notestore/NoteMetadata;->updateSequenceNum:I

    iput v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->updateSequenceNum:I

    .line 106
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetNotebookGuid()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 107
    iget-object v0, p1, Lcom/evernote/edam/notestore/NoteMetadata;->notebookGuid:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->notebookGuid:Ljava/lang/String;

    .line 109
    :cond_2
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetTagGuids()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 110
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 111
    iget-object v1, p1, Lcom/evernote/edam/notestore/NoteMetadata;->tagGuids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 112
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 114
    :cond_3
    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->tagGuids:Ljava/util/List;

    .line 116
    :cond_4
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetAttributes()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 117
    new-instance v0, Lcom/evernote/edam/type/NoteAttributes;

    iget-object v1, p1, Lcom/evernote/edam/notestore/NoteMetadata;->attributes:Lcom/evernote/edam/type/NoteAttributes;

    invoke-direct {v0, v1}, Lcom/evernote/edam/type/NoteAttributes;-><init>(Lcom/evernote/edam/type/NoteAttributes;)V

    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->attributes:Lcom/evernote/edam/type/NoteAttributes;

    .line 119
    :cond_5
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetLargestResourceMime()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 120
    iget-object v0, p1, Lcom/evernote/edam/notestore/NoteMetadata;->largestResourceMime:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->largestResourceMime:Ljava/lang/String;

    .line 122
    :cond_6
    iget p1, p1, Lcom/evernote/edam/notestore/NoteMetadata;->largestResourceSize:I

    iput p1, p0, Lcom/evernote/edam/notestore/NoteMetadata;->largestResourceSize:I

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 86
    invoke-direct {p0}, Lcom/evernote/edam/notestore/NoteMetadata;-><init>()V

    .line 87
    iput-object p1, p0, Lcom/evernote/edam/notestore/NoteMetadata;->guid:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public addToTagGuids(Ljava/lang/String;)V
    .locals 1

    .line 338
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->tagGuids:Ljava/util/List;

    if-nez v0, :cond_0

    .line 339
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->tagGuids:Ljava/util/List;

    .line 341
    :cond_0
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->tagGuids:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public clear()V
    .locals 4

    const/4 v0, 0x0

    .line 130
    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->guid:Ljava/lang/String;

    .line 131
    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->title:Ljava/lang/String;

    const/4 v1, 0x0

    .line 132
    invoke-virtual {p0, v1}, Lcom/evernote/edam/notestore/NoteMetadata;->setContentLengthIsSet(Z)V

    .line 133
    iput v1, p0, Lcom/evernote/edam/notestore/NoteMetadata;->contentLength:I

    .line 134
    invoke-virtual {p0, v1}, Lcom/evernote/edam/notestore/NoteMetadata;->setCreatedIsSet(Z)V

    const-wide/16 v2, 0x0

    .line 135
    iput-wide v2, p0, Lcom/evernote/edam/notestore/NoteMetadata;->created:J

    .line 136
    invoke-virtual {p0, v1}, Lcom/evernote/edam/notestore/NoteMetadata;->setUpdatedIsSet(Z)V

    .line 137
    iput-wide v2, p0, Lcom/evernote/edam/notestore/NoteMetadata;->updated:J

    .line 138
    invoke-virtual {p0, v1}, Lcom/evernote/edam/notestore/NoteMetadata;->setDeletedIsSet(Z)V

    .line 139
    iput-wide v2, p0, Lcom/evernote/edam/notestore/NoteMetadata;->deleted:J

    .line 140
    invoke-virtual {p0, v1}, Lcom/evernote/edam/notestore/NoteMetadata;->setUpdateSequenceNumIsSet(Z)V

    .line 141
    iput v1, p0, Lcom/evernote/edam/notestore/NoteMetadata;->updateSequenceNum:I

    .line 142
    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->notebookGuid:Ljava/lang/String;

    .line 143
    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->tagGuids:Ljava/util/List;

    .line 144
    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->attributes:Lcom/evernote/edam/type/NoteAttributes;

    .line 145
    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->largestResourceMime:Ljava/lang/String;

    .line 146
    invoke-virtual {p0, v1}, Lcom/evernote/edam/notestore/NoteMetadata;->setLargestResourceSizeIsSet(Z)V

    .line 147
    iput v1, p0, Lcom/evernote/edam/notestore/NoteMetadata;->largestResourceSize:I

    return-void
.end method

.method public compareTo(Lcom/evernote/edam/notestore/NoteMetadata;)I
    .locals 4

    .line 565
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 566
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    return p1

    .line 572
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetGuid()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetGuid()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_1

    return v0

    .line 576
    :cond_1
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetGuid()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->guid:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/notestore/NoteMetadata;->guid:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_2

    return v0

    .line 581
    :cond_2
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetTitle()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetTitle()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_3

    return v0

    .line 585
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetTitle()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->title:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/notestore/NoteMetadata;->title:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_4

    return v0

    .line 590
    :cond_4
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetContentLength()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetContentLength()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_5

    return v0

    .line 594
    :cond_5
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetContentLength()Z

    move-result v0

    if-eqz v0, :cond_6

    iget v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->contentLength:I

    iget v1, p1, Lcom/evernote/edam/notestore/NoteMetadata;->contentLength:I

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(II)I

    move-result v0

    if-eqz v0, :cond_6

    return v0

    .line 599
    :cond_6
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetCreated()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetCreated()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_7

    return v0

    .line 603
    :cond_7
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetCreated()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-wide v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->created:J

    iget-wide v2, p1, Lcom/evernote/edam/notestore/NoteMetadata;->created:J

    invoke-static {v0, v1, v2, v3}, Lcom/evernote/thrift/TBaseHelper;->compareTo(JJ)I

    move-result v0

    if-eqz v0, :cond_8

    return v0

    .line 608
    :cond_8
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetUpdated()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetUpdated()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_9

    return v0

    .line 612
    :cond_9
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetUpdated()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-wide v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->updated:J

    iget-wide v2, p1, Lcom/evernote/edam/notestore/NoteMetadata;->updated:J

    invoke-static {v0, v1, v2, v3}, Lcom/evernote/thrift/TBaseHelper;->compareTo(JJ)I

    move-result v0

    if-eqz v0, :cond_a

    return v0

    .line 617
    :cond_a
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetDeleted()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetDeleted()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_b

    return v0

    .line 621
    :cond_b
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetDeleted()Z

    move-result v0

    if-eqz v0, :cond_c

    iget-wide v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->deleted:J

    iget-wide v2, p1, Lcom/evernote/edam/notestore/NoteMetadata;->deleted:J

    invoke-static {v0, v1, v2, v3}, Lcom/evernote/thrift/TBaseHelper;->compareTo(JJ)I

    move-result v0

    if-eqz v0, :cond_c

    return v0

    .line 626
    :cond_c
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetUpdateSequenceNum()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetUpdateSequenceNum()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_d

    return v0

    .line 630
    :cond_d
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetUpdateSequenceNum()Z

    move-result v0

    if-eqz v0, :cond_e

    iget v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->updateSequenceNum:I

    iget v1, p1, Lcom/evernote/edam/notestore/NoteMetadata;->updateSequenceNum:I

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(II)I

    move-result v0

    if-eqz v0, :cond_e

    return v0

    .line 635
    :cond_e
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetNotebookGuid()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetNotebookGuid()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_f

    return v0

    .line 639
    :cond_f
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetNotebookGuid()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->notebookGuid:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/notestore/NoteMetadata;->notebookGuid:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_10

    return v0

    .line 644
    :cond_10
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetTagGuids()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetTagGuids()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_11

    return v0

    .line 648
    :cond_11
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetTagGuids()Z

    move-result v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->tagGuids:Ljava/util/List;

    iget-object v1, p1, Lcom/evernote/edam/notestore/NoteMetadata;->tagGuids:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/util/List;Ljava/util/List;)I

    move-result v0

    if-eqz v0, :cond_12

    return v0

    .line 653
    :cond_12
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetAttributes()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetAttributes()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_13

    return v0

    .line 657
    :cond_13
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetAttributes()Z

    move-result v0

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->attributes:Lcom/evernote/edam/type/NoteAttributes;

    iget-object v1, p1, Lcom/evernote/edam/notestore/NoteMetadata;->attributes:Lcom/evernote/edam/type/NoteAttributes;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result v0

    if-eqz v0, :cond_14

    return v0

    .line 662
    :cond_14
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetLargestResourceMime()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetLargestResourceMime()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_15

    return v0

    .line 666
    :cond_15
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetLargestResourceMime()Z

    move-result v0

    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->largestResourceMime:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/notestore/NoteMetadata;->largestResourceMime:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_16

    return v0

    .line 671
    :cond_16
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetLargestResourceSize()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetLargestResourceSize()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_17

    return v0

    .line 675
    :cond_17
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetLargestResourceSize()Z

    move-result v0

    if-eqz v0, :cond_18

    iget v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->largestResourceSize:I

    iget p1, p1, Lcom/evernote/edam/notestore/NoteMetadata;->largestResourceSize:I

    invoke-static {v0, p1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(II)I

    move-result p1

    if-eqz p1, :cond_18

    return p1

    :cond_18
    const/4 p1, 0x0

    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 41
    check-cast p1, Lcom/evernote/edam/notestore/NoteMetadata;

    invoke-virtual {p0, p1}, Lcom/evernote/edam/notestore/NoteMetadata;->compareTo(Lcom/evernote/edam/notestore/NoteMetadata;)I

    move-result p1

    return p1
.end method

.method public deepCopy()Lcom/evernote/edam/notestore/NoteMetadata;
    .locals 1

    .line 126
    new-instance v0, Lcom/evernote/edam/notestore/NoteMetadata;

    invoke-direct {v0, p0}, Lcom/evernote/edam/notestore/NoteMetadata;-><init>(Lcom/evernote/edam/notestore/NoteMetadata;)V

    return-object v0
.end method

.method public bridge synthetic deepCopy()Lcom/evernote/thrift/TBase;
    .locals 1

    .line 41
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->deepCopy()Lcom/evernote/edam/notestore/NoteMetadata;

    move-result-object v0

    return-object v0
.end method

.method public equals(Lcom/evernote/edam/notestore/NoteMetadata;)Z
    .locals 6

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 448
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetGuid()Z

    move-result v1

    .line 449
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetGuid()Z

    move-result v2

    if-nez v1, :cond_1

    if-eqz v2, :cond_3

    :cond_1
    if-eqz v1, :cond_30

    if-nez v2, :cond_2

    goto/16 :goto_b

    .line 453
    :cond_2
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteMetadata;->guid:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/notestore/NoteMetadata;->guid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    return v0

    .line 457
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetTitle()Z

    move-result v1

    .line 458
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetTitle()Z

    move-result v2

    if-nez v1, :cond_4

    if-eqz v2, :cond_6

    :cond_4
    if-eqz v1, :cond_2f

    if-nez v2, :cond_5

    goto/16 :goto_a

    .line 462
    :cond_5
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteMetadata;->title:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/notestore/NoteMetadata;->title:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    return v0

    .line 466
    :cond_6
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetContentLength()Z

    move-result v1

    .line 467
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetContentLength()Z

    move-result v2

    if-nez v1, :cond_7

    if-eqz v2, :cond_9

    :cond_7
    if-eqz v1, :cond_2e

    if-nez v2, :cond_8

    goto/16 :goto_9

    .line 471
    :cond_8
    iget v1, p0, Lcom/evernote/edam/notestore/NoteMetadata;->contentLength:I

    iget v2, p1, Lcom/evernote/edam/notestore/NoteMetadata;->contentLength:I

    if-eq v1, v2, :cond_9

    return v0

    .line 475
    :cond_9
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetCreated()Z

    move-result v1

    .line 476
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetCreated()Z

    move-result v2

    if-nez v1, :cond_a

    if-eqz v2, :cond_c

    :cond_a
    if-eqz v1, :cond_2d

    if-nez v2, :cond_b

    goto/16 :goto_8

    .line 480
    :cond_b
    iget-wide v1, p0, Lcom/evernote/edam/notestore/NoteMetadata;->created:J

    iget-wide v3, p1, Lcom/evernote/edam/notestore/NoteMetadata;->created:J

    cmp-long v5, v1, v3

    if-eqz v5, :cond_c

    return v0

    .line 484
    :cond_c
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetUpdated()Z

    move-result v1

    .line 485
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetUpdated()Z

    move-result v2

    if-nez v1, :cond_d

    if-eqz v2, :cond_f

    :cond_d
    if-eqz v1, :cond_2c

    if-nez v2, :cond_e

    goto/16 :goto_7

    .line 489
    :cond_e
    iget-wide v1, p0, Lcom/evernote/edam/notestore/NoteMetadata;->updated:J

    iget-wide v3, p1, Lcom/evernote/edam/notestore/NoteMetadata;->updated:J

    cmp-long v5, v1, v3

    if-eqz v5, :cond_f

    return v0

    .line 493
    :cond_f
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetDeleted()Z

    move-result v1

    .line 494
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetDeleted()Z

    move-result v2

    if-nez v1, :cond_10

    if-eqz v2, :cond_12

    :cond_10
    if-eqz v1, :cond_2b

    if-nez v2, :cond_11

    goto/16 :goto_6

    .line 498
    :cond_11
    iget-wide v1, p0, Lcom/evernote/edam/notestore/NoteMetadata;->deleted:J

    iget-wide v3, p1, Lcom/evernote/edam/notestore/NoteMetadata;->deleted:J

    cmp-long v5, v1, v3

    if-eqz v5, :cond_12

    return v0

    .line 502
    :cond_12
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetUpdateSequenceNum()Z

    move-result v1

    .line 503
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetUpdateSequenceNum()Z

    move-result v2

    if-nez v1, :cond_13

    if-eqz v2, :cond_15

    :cond_13
    if-eqz v1, :cond_2a

    if-nez v2, :cond_14

    goto/16 :goto_5

    .line 507
    :cond_14
    iget v1, p0, Lcom/evernote/edam/notestore/NoteMetadata;->updateSequenceNum:I

    iget v2, p1, Lcom/evernote/edam/notestore/NoteMetadata;->updateSequenceNum:I

    if-eq v1, v2, :cond_15

    return v0

    .line 511
    :cond_15
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetNotebookGuid()Z

    move-result v1

    .line 512
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetNotebookGuid()Z

    move-result v2

    if-nez v1, :cond_16

    if-eqz v2, :cond_18

    :cond_16
    if-eqz v1, :cond_29

    if-nez v2, :cond_17

    goto/16 :goto_4

    .line 516
    :cond_17
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteMetadata;->notebookGuid:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/notestore/NoteMetadata;->notebookGuid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_18

    return v0

    .line 520
    :cond_18
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetTagGuids()Z

    move-result v1

    .line 521
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetTagGuids()Z

    move-result v2

    if-nez v1, :cond_19

    if-eqz v2, :cond_1b

    :cond_19
    if-eqz v1, :cond_28

    if-nez v2, :cond_1a

    goto :goto_3

    .line 525
    :cond_1a
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteMetadata;->tagGuids:Ljava/util/List;

    iget-object v2, p1, Lcom/evernote/edam/notestore/NoteMetadata;->tagGuids:Ljava/util/List;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1b

    return v0

    .line 529
    :cond_1b
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetAttributes()Z

    move-result v1

    .line 530
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetAttributes()Z

    move-result v2

    if-nez v1, :cond_1c

    if-eqz v2, :cond_1e

    :cond_1c
    if-eqz v1, :cond_27

    if-nez v2, :cond_1d

    goto :goto_2

    .line 534
    :cond_1d
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteMetadata;->attributes:Lcom/evernote/edam/type/NoteAttributes;

    iget-object v2, p1, Lcom/evernote/edam/notestore/NoteMetadata;->attributes:Lcom/evernote/edam/type/NoteAttributes;

    invoke-virtual {v1, v2}, Lcom/evernote/edam/type/NoteAttributes;->equals(Lcom/evernote/edam/type/NoteAttributes;)Z

    move-result v1

    if-nez v1, :cond_1e

    return v0

    .line 538
    :cond_1e
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetLargestResourceMime()Z

    move-result v1

    .line 539
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetLargestResourceMime()Z

    move-result v2

    if-nez v1, :cond_1f

    if-eqz v2, :cond_21

    :cond_1f
    if-eqz v1, :cond_26

    if-nez v2, :cond_20

    goto :goto_1

    .line 543
    :cond_20
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteMetadata;->largestResourceMime:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/notestore/NoteMetadata;->largestResourceMime:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_21

    return v0

    .line 547
    :cond_21
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetLargestResourceSize()Z

    move-result v1

    .line 548
    invoke-virtual {p1}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetLargestResourceSize()Z

    move-result v2

    if-nez v1, :cond_22

    if-eqz v2, :cond_24

    :cond_22
    if-eqz v1, :cond_25

    if-nez v2, :cond_23

    goto :goto_0

    .line 552
    :cond_23
    iget v1, p0, Lcom/evernote/edam/notestore/NoteMetadata;->largestResourceSize:I

    iget p1, p1, Lcom/evernote/edam/notestore/NoteMetadata;->largestResourceSize:I

    if-eq v1, p1, :cond_24

    return v0

    :cond_24
    const/4 p1, 0x1

    return p1

    :cond_25
    :goto_0
    return v0

    :cond_26
    :goto_1
    return v0

    :cond_27
    :goto_2
    return v0

    :cond_28
    :goto_3
    return v0

    :cond_29
    :goto_4
    return v0

    :cond_2a
    :goto_5
    return v0

    :cond_2b
    :goto_6
    return v0

    :cond_2c
    :goto_7
    return v0

    :cond_2d
    :goto_8
    return v0

    :cond_2e
    :goto_9
    return v0

    :cond_2f
    :goto_a
    return v0

    :cond_30
    :goto_b
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 439
    :cond_0
    instance-of v1, p1, Lcom/evernote/edam/notestore/NoteMetadata;

    if-eqz v1, :cond_1

    .line 440
    check-cast p1, Lcom/evernote/edam/notestore/NoteMetadata;

    invoke-virtual {p0, p1}, Lcom/evernote/edam/notestore/NoteMetadata;->equals(Lcom/evernote/edam/notestore/NoteMetadata;)Z

    move-result p1

    return p1

    :cond_1
    return v0
.end method

.method public getAttributes()Lcom/evernote/edam/type/NoteAttributes;
    .locals 1

    .line 368
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->attributes:Lcom/evernote/edam/type/NoteAttributes;

    return-object v0
.end method

.method public getContentLength()I
    .locals 1

    .line 197
    iget v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->contentLength:I

    return v0
.end method

.method public getCreated()J
    .locals 2

    .line 219
    iget-wide v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->created:J

    return-wide v0
.end method

.method public getDeleted()J
    .locals 2

    .line 263
    iget-wide v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->deleted:J

    return-wide v0
.end method

.method public getGuid()Ljava/lang/String;
    .locals 1

    .line 151
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->guid:Ljava/lang/String;

    return-object v0
.end method

.method public getLargestResourceMime()Ljava/lang/String;
    .locals 1

    .line 391
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->largestResourceMime:Ljava/lang/String;

    return-object v0
.end method

.method public getLargestResourceSize()I
    .locals 1

    .line 414
    iget v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->largestResourceSize:I

    return v0
.end method

.method public getNotebookGuid()Ljava/lang/String;
    .locals 1

    .line 307
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->notebookGuid:Ljava/lang/String;

    return-object v0
.end method

.method public getTagGuids()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 345
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->tagGuids:Ljava/util/List;

    return-object v0
.end method

.method public getTagGuidsIterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 334
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->tagGuids:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public getTagGuidsSize()I
    .locals 1

    .line 330
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->tagGuids:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .line 174
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->title:Ljava/lang/String;

    return-object v0
.end method

.method public getUpdateSequenceNum()I
    .locals 1

    .line 285
    iget v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->updateSequenceNum:I

    return v0
.end method

.method public getUpdated()J
    .locals 2

    .line 241
    iget-wide v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->updated:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isSetAttributes()Z
    .locals 1

    .line 381
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->attributes:Lcom/evernote/edam/type/NoteAttributes;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetContentLength()Z
    .locals 2

    .line 211
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->__isset_vector:[Z

    const/4 v1, 0x0

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetCreated()Z
    .locals 2

    .line 233
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->__isset_vector:[Z

    const/4 v1, 0x1

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetDeleted()Z
    .locals 2

    .line 277
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->__isset_vector:[Z

    const/4 v1, 0x3

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetGuid()Z
    .locals 1

    .line 164
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->guid:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetLargestResourceMime()Z
    .locals 1

    .line 404
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->largestResourceMime:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetLargestResourceSize()Z
    .locals 2

    .line 428
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->__isset_vector:[Z

    const/4 v1, 0x5

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetNotebookGuid()Z
    .locals 1

    .line 320
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->notebookGuid:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetTagGuids()Z
    .locals 1

    .line 358
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->tagGuids:Ljava/util/List;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetTitle()Z
    .locals 1

    .line 187
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->title:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetUpdateSequenceNum()Z
    .locals 2

    .line 299
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->__isset_vector:[Z

    const/4 v1, 0x4

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetUpdated()Z
    .locals 2

    .line 255
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->__isset_vector:[Z

    const/4 v1, 0x2

    aget-boolean v0, v0, v1

    return v0
.end method

.method public read(Lcom/evernote/thrift/protocol/TProtocol;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 685
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readStructBegin()Lcom/evernote/thrift/protocol/TStruct;

    .line 688
    :goto_0
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readFieldBegin()Lcom/evernote/thrift/protocol/TField;

    move-result-object v0

    .line 689
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-nez v1, :cond_0

    .line 799
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readStructEnd()V

    .line 800
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->validate()V

    return-void

    .line 692
    :cond_0
    iget-short v1, v0, Lcom/evernote/thrift/protocol/TField;->id:S

    const/16 v2, 0xa

    const/16 v3, 0x8

    const/16 v4, 0xb

    const/4 v5, 0x1

    packed-switch v1, :pswitch_data_0

    .line 795
    :pswitch_0
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_2

    .line 787
    :pswitch_1
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v3, :cond_1

    .line 788
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI32()I

    move-result v0

    iput v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->largestResourceSize:I

    .line 789
    invoke-virtual {p0, v5}, Lcom/evernote/edam/notestore/NoteMetadata;->setLargestResourceSizeIsSet(Z)V

    goto/16 :goto_2

    .line 791
    :cond_1
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_2

    .line 780
    :pswitch_2
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_2

    .line 781
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->largestResourceMime:Ljava/lang/String;

    goto/16 :goto_2

    .line 783
    :cond_2
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_2

    .line 772
    :pswitch_3
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    const/16 v2, 0xc

    if-ne v1, v2, :cond_3

    .line 773
    new-instance v0, Lcom/evernote/edam/type/NoteAttributes;

    invoke-direct {v0}, Lcom/evernote/edam/type/NoteAttributes;-><init>()V

    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->attributes:Lcom/evernote/edam/type/NoteAttributes;

    .line 774
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->attributes:Lcom/evernote/edam/type/NoteAttributes;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/type/NoteAttributes;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    goto/16 :goto_2

    .line 776
    :cond_3
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_2

    .line 755
    :pswitch_4
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    const/16 v2, 0xf

    if-ne v1, v2, :cond_5

    .line 757
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readListBegin()Lcom/evernote/thrift/protocol/TList;

    move-result-object v0

    .line 758
    new-instance v1, Ljava/util/ArrayList;

    iget v2, v0, Lcom/evernote/thrift/protocol/TList;->size:I

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/evernote/edam/notestore/NoteMetadata;->tagGuids:Ljava/util/List;

    const/4 v1, 0x0

    .line 759
    :goto_1
    iget v2, v0, Lcom/evernote/thrift/protocol/TList;->size:I

    if-ge v1, v2, :cond_4

    .line 762
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v2

    .line 763
    iget-object v3, p0, Lcom/evernote/edam/notestore/NoteMetadata;->tagGuids:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 765
    :cond_4
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readListEnd()V

    goto/16 :goto_2

    .line 768
    :cond_5
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_2

    .line 748
    :pswitch_5
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_6

    .line 749
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->notebookGuid:Ljava/lang/String;

    goto/16 :goto_2

    .line 751
    :cond_6
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_2

    .line 740
    :pswitch_6
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v3, :cond_7

    .line 741
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI32()I

    move-result v0

    iput v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->updateSequenceNum:I

    .line 742
    invoke-virtual {p0, v5}, Lcom/evernote/edam/notestore/NoteMetadata;->setUpdateSequenceNumIsSet(Z)V

    goto/16 :goto_2

    .line 744
    :cond_7
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_2

    .line 732
    :pswitch_7
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v2, :cond_8

    .line 733
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->deleted:J

    .line 734
    invoke-virtual {p0, v5}, Lcom/evernote/edam/notestore/NoteMetadata;->setDeletedIsSet(Z)V

    goto :goto_2

    .line 736
    :cond_8
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_2

    .line 724
    :pswitch_8
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v2, :cond_9

    .line 725
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->updated:J

    .line 726
    invoke-virtual {p0, v5}, Lcom/evernote/edam/notestore/NoteMetadata;->setUpdatedIsSet(Z)V

    goto :goto_2

    .line 728
    :cond_9
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_2

    .line 716
    :pswitch_9
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v2, :cond_a

    .line 717
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->created:J

    .line 718
    invoke-virtual {p0, v5}, Lcom/evernote/edam/notestore/NoteMetadata;->setCreatedIsSet(Z)V

    goto :goto_2

    .line 720
    :cond_a
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_2

    .line 708
    :pswitch_a
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v3, :cond_b

    .line 709
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI32()I

    move-result v0

    iput v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->contentLength:I

    .line 710
    invoke-virtual {p0, v5}, Lcom/evernote/edam/notestore/NoteMetadata;->setContentLengthIsSet(Z)V

    goto :goto_2

    .line 712
    :cond_b
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_2

    .line 701
    :pswitch_b
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_c

    .line 702
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->title:Ljava/lang/String;

    goto :goto_2

    .line 704
    :cond_c
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_2

    .line 694
    :pswitch_c
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_d

    .line 695
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->guid:Ljava/lang/String;

    goto :goto_2

    .line 697
    :cond_d
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    .line 797
    :goto_2
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readFieldEnd()V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_c
        :pswitch_b
        :pswitch_0
        :pswitch_0
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public setAttributes(Lcom/evernote/edam/type/NoteAttributes;)V
    .locals 0

    .line 372
    iput-object p1, p0, Lcom/evernote/edam/notestore/NoteMetadata;->attributes:Lcom/evernote/edam/type/NoteAttributes;

    return-void
.end method

.method public setAttributesIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 386
    iput-object p1, p0, Lcom/evernote/edam/notestore/NoteMetadata;->attributes:Lcom/evernote/edam/type/NoteAttributes;

    :cond_0
    return-void
.end method

.method public setContentLength(I)V
    .locals 0

    .line 201
    iput p1, p0, Lcom/evernote/edam/notestore/NoteMetadata;->contentLength:I

    const/4 p1, 0x1

    .line 202
    invoke-virtual {p0, p1}, Lcom/evernote/edam/notestore/NoteMetadata;->setContentLengthIsSet(Z)V

    return-void
.end method

.method public setContentLengthIsSet(Z)V
    .locals 2

    .line 215
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->__isset_vector:[Z

    const/4 v1, 0x0

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setCreated(J)V
    .locals 0

    .line 223
    iput-wide p1, p0, Lcom/evernote/edam/notestore/NoteMetadata;->created:J

    const/4 p1, 0x1

    .line 224
    invoke-virtual {p0, p1}, Lcom/evernote/edam/notestore/NoteMetadata;->setCreatedIsSet(Z)V

    return-void
.end method

.method public setCreatedIsSet(Z)V
    .locals 2

    .line 237
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->__isset_vector:[Z

    const/4 v1, 0x1

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setDeleted(J)V
    .locals 0

    .line 267
    iput-wide p1, p0, Lcom/evernote/edam/notestore/NoteMetadata;->deleted:J

    const/4 p1, 0x1

    .line 268
    invoke-virtual {p0, p1}, Lcom/evernote/edam/notestore/NoteMetadata;->setDeletedIsSet(Z)V

    return-void
.end method

.method public setDeletedIsSet(Z)V
    .locals 2

    .line 281
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->__isset_vector:[Z

    const/4 v1, 0x3

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setGuid(Ljava/lang/String;)V
    .locals 0

    .line 155
    iput-object p1, p0, Lcom/evernote/edam/notestore/NoteMetadata;->guid:Ljava/lang/String;

    return-void
.end method

.method public setGuidIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 169
    iput-object p1, p0, Lcom/evernote/edam/notestore/NoteMetadata;->guid:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setLargestResourceMime(Ljava/lang/String;)V
    .locals 0

    .line 395
    iput-object p1, p0, Lcom/evernote/edam/notestore/NoteMetadata;->largestResourceMime:Ljava/lang/String;

    return-void
.end method

.method public setLargestResourceMimeIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 409
    iput-object p1, p0, Lcom/evernote/edam/notestore/NoteMetadata;->largestResourceMime:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setLargestResourceSize(I)V
    .locals 0

    .line 418
    iput p1, p0, Lcom/evernote/edam/notestore/NoteMetadata;->largestResourceSize:I

    const/4 p1, 0x1

    .line 419
    invoke-virtual {p0, p1}, Lcom/evernote/edam/notestore/NoteMetadata;->setLargestResourceSizeIsSet(Z)V

    return-void
.end method

.method public setLargestResourceSizeIsSet(Z)V
    .locals 2

    .line 432
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->__isset_vector:[Z

    const/4 v1, 0x5

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setNotebookGuid(Ljava/lang/String;)V
    .locals 0

    .line 311
    iput-object p1, p0, Lcom/evernote/edam/notestore/NoteMetadata;->notebookGuid:Ljava/lang/String;

    return-void
.end method

.method public setNotebookGuidIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 325
    iput-object p1, p0, Lcom/evernote/edam/notestore/NoteMetadata;->notebookGuid:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setTagGuids(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 349
    iput-object p1, p0, Lcom/evernote/edam/notestore/NoteMetadata;->tagGuids:Ljava/util/List;

    return-void
.end method

.method public setTagGuidsIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 363
    iput-object p1, p0, Lcom/evernote/edam/notestore/NoteMetadata;->tagGuids:Ljava/util/List;

    :cond_0
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0

    .line 178
    iput-object p1, p0, Lcom/evernote/edam/notestore/NoteMetadata;->title:Ljava/lang/String;

    return-void
.end method

.method public setTitleIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 192
    iput-object p1, p0, Lcom/evernote/edam/notestore/NoteMetadata;->title:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setUpdateSequenceNum(I)V
    .locals 0

    .line 289
    iput p1, p0, Lcom/evernote/edam/notestore/NoteMetadata;->updateSequenceNum:I

    const/4 p1, 0x1

    .line 290
    invoke-virtual {p0, p1}, Lcom/evernote/edam/notestore/NoteMetadata;->setUpdateSequenceNumIsSet(Z)V

    return-void
.end method

.method public setUpdateSequenceNumIsSet(Z)V
    .locals 2

    .line 303
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->__isset_vector:[Z

    const/4 v1, 0x4

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setUpdated(J)V
    .locals 0

    .line 245
    iput-wide p1, p0, Lcom/evernote/edam/notestore/NoteMetadata;->updated:J

    const/4 p1, 0x1

    .line 246
    invoke-virtual {p0, p1}, Lcom/evernote/edam/notestore/NoteMetadata;->setUpdatedIsSet(Z)V

    return-void
.end method

.method public setUpdatedIsSet(Z)V
    .locals 2

    .line 259
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->__isset_vector:[Z

    const/4 v1, 0x2

    aput-boolean p1, v0, v1

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 890
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NoteMetadata("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "guid:"

    .line 893
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 894
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteMetadata;->guid:Ljava/lang/String;

    if-nez v1, :cond_0

    const-string v1, "null"

    .line 895
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 897
    :cond_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 900
    :goto_0
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetTitle()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, ", "

    .line 901
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "title:"

    .line 902
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 903
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteMetadata;->title:Ljava/lang/String;

    if-nez v1, :cond_1

    const-string v1, "null"

    .line 904
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 906
    :cond_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 910
    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetContentLength()Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, ", "

    .line 911
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "contentLength:"

    .line 912
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 913
    iget v1, p0, Lcom/evernote/edam/notestore/NoteMetadata;->contentLength:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 916
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetCreated()Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, ", "

    .line 917
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "created:"

    .line 918
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 919
    iget-wide v1, p0, Lcom/evernote/edam/notestore/NoteMetadata;->created:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 922
    :cond_4
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetUpdated()Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, ", "

    .line 923
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "updated:"

    .line 924
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 925
    iget-wide v1, p0, Lcom/evernote/edam/notestore/NoteMetadata;->updated:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 928
    :cond_5
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetDeleted()Z

    move-result v1

    if-eqz v1, :cond_6

    const-string v1, ", "

    .line 929
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "deleted:"

    .line 930
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 931
    iget-wide v1, p0, Lcom/evernote/edam/notestore/NoteMetadata;->deleted:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 934
    :cond_6
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetUpdateSequenceNum()Z

    move-result v1

    if-eqz v1, :cond_7

    const-string v1, ", "

    .line 935
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "updateSequenceNum:"

    .line 936
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 937
    iget v1, p0, Lcom/evernote/edam/notestore/NoteMetadata;->updateSequenceNum:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 940
    :cond_7
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetNotebookGuid()Z

    move-result v1

    if-eqz v1, :cond_9

    const-string v1, ", "

    .line 941
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "notebookGuid:"

    .line 942
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 943
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteMetadata;->notebookGuid:Ljava/lang/String;

    if-nez v1, :cond_8

    const-string v1, "null"

    .line 944
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 946
    :cond_8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 950
    :cond_9
    :goto_2
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetTagGuids()Z

    move-result v1

    if-eqz v1, :cond_b

    const-string v1, ", "

    .line 951
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "tagGuids:"

    .line 952
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 953
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteMetadata;->tagGuids:Ljava/util/List;

    if-nez v1, :cond_a

    const-string v1, "null"

    .line 954
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 956
    :cond_a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 960
    :cond_b
    :goto_3
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetAttributes()Z

    move-result v1

    if-eqz v1, :cond_d

    const-string v1, ", "

    .line 961
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "attributes:"

    .line 962
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 963
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteMetadata;->attributes:Lcom/evernote/edam/type/NoteAttributes;

    if-nez v1, :cond_c

    const-string v1, "null"

    .line 964
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 966
    :cond_c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 970
    :cond_d
    :goto_4
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetLargestResourceMime()Z

    move-result v1

    if-eqz v1, :cond_f

    const-string v1, ", "

    .line 971
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "largestResourceMime:"

    .line 972
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 973
    iget-object v1, p0, Lcom/evernote/edam/notestore/NoteMetadata;->largestResourceMime:Ljava/lang/String;

    if-nez v1, :cond_e

    const-string v1, "null"

    .line 974
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5

    .line 976
    :cond_e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 980
    :cond_f
    :goto_5
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetLargestResourceSize()Z

    move-result v1

    if-eqz v1, :cond_10

    const-string v1, ", "

    .line 981
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "largestResourceSize:"

    .line 982
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 983
    iget v1, p0, Lcom/evernote/edam/notestore/NoteMetadata;->largestResourceSize:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_10
    const-string v1, ")"

    .line 986
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 987
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public unsetAttributes()V
    .locals 1

    const/4 v0, 0x0

    .line 376
    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->attributes:Lcom/evernote/edam/type/NoteAttributes;

    return-void
.end method

.method public unsetContentLength()V
    .locals 2

    .line 206
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->__isset_vector:[Z

    const/4 v1, 0x0

    aput-boolean v1, v0, v1

    return-void
.end method

.method public unsetCreated()V
    .locals 3

    .line 228
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->__isset_vector:[Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetDeleted()V
    .locals 3

    .line 272
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->__isset_vector:[Z

    const/4 v1, 0x3

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetGuid()V
    .locals 1

    const/4 v0, 0x0

    .line 159
    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->guid:Ljava/lang/String;

    return-void
.end method

.method public unsetLargestResourceMime()V
    .locals 1

    const/4 v0, 0x0

    .line 399
    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->largestResourceMime:Ljava/lang/String;

    return-void
.end method

.method public unsetLargestResourceSize()V
    .locals 3

    .line 423
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->__isset_vector:[Z

    const/4 v1, 0x5

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetNotebookGuid()V
    .locals 1

    const/4 v0, 0x0

    .line 315
    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->notebookGuid:Ljava/lang/String;

    return-void
.end method

.method public unsetTagGuids()V
    .locals 1

    const/4 v0, 0x0

    .line 353
    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->tagGuids:Ljava/util/List;

    return-void
.end method

.method public unsetTitle()V
    .locals 1

    const/4 v0, 0x0

    .line 182
    iput-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->title:Ljava/lang/String;

    return-void
.end method

.method public unsetUpdateSequenceNum()V
    .locals 3

    .line 294
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->__isset_vector:[Z

    const/4 v1, 0x4

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetUpdated()V
    .locals 3

    .line 250
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->__isset_vector:[Z

    const/4 v1, 0x2

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public validate()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 992
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetGuid()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 993
    :cond_0
    new-instance v0, Lcom/evernote/thrift/protocol/TProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Required field \'guid\' is unset! Struct:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/evernote/thrift/protocol/TProtocolException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public write(Lcom/evernote/thrift/protocol/TProtocol;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 804
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->validate()V

    .line 806
    sget-object v0, Lcom/evernote/edam/notestore/NoteMetadata;->STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeStructBegin(Lcom/evernote/thrift/protocol/TStruct;)V

    .line 807
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->guid:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 808
    sget-object v0, Lcom/evernote/edam/notestore/NoteMetadata;->GUID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 809
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->guid:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 810
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 812
    :cond_0
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->title:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 813
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetTitle()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 814
    sget-object v0, Lcom/evernote/edam/notestore/NoteMetadata;->TITLE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 815
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->title:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 816
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 819
    :cond_1
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetContentLength()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 820
    sget-object v0, Lcom/evernote/edam/notestore/NoteMetadata;->CONTENT_LENGTH_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 821
    iget v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->contentLength:I

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeI32(I)V

    .line 822
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 824
    :cond_2
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetCreated()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 825
    sget-object v0, Lcom/evernote/edam/notestore/NoteMetadata;->CREATED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 826
    iget-wide v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->created:J

    invoke-virtual {p1, v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeI64(J)V

    .line 827
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 829
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetUpdated()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 830
    sget-object v0, Lcom/evernote/edam/notestore/NoteMetadata;->UPDATED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 831
    iget-wide v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->updated:J

    invoke-virtual {p1, v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeI64(J)V

    .line 832
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 834
    :cond_4
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetDeleted()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 835
    sget-object v0, Lcom/evernote/edam/notestore/NoteMetadata;->DELETED_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 836
    iget-wide v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->deleted:J

    invoke-virtual {p1, v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeI64(J)V

    .line 837
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 839
    :cond_5
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetUpdateSequenceNum()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 840
    sget-object v0, Lcom/evernote/edam/notestore/NoteMetadata;->UPDATE_SEQUENCE_NUM_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 841
    iget v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->updateSequenceNum:I

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeI32(I)V

    .line 842
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 844
    :cond_6
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->notebookGuid:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 845
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetNotebookGuid()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 846
    sget-object v0, Lcom/evernote/edam/notestore/NoteMetadata;->NOTEBOOK_GUID_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 847
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->notebookGuid:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 848
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 851
    :cond_7
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->tagGuids:Ljava/util/List;

    if-eqz v0, :cond_9

    .line 852
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetTagGuids()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 853
    sget-object v0, Lcom/evernote/edam/notestore/NoteMetadata;->TAG_GUIDS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 855
    new-instance v0, Lcom/evernote/thrift/protocol/TList;

    const/16 v1, 0xb

    iget-object v2, p0, Lcom/evernote/edam/notestore/NoteMetadata;->tagGuids:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/protocol/TList;-><init>(BI)V

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeListBegin(Lcom/evernote/thrift/protocol/TList;)V

    .line 856
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->tagGuids:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 858
    invoke-virtual {p1, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 860
    :cond_8
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeListEnd()V

    .line 862
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 865
    :cond_9
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->attributes:Lcom/evernote/edam/type/NoteAttributes;

    if-eqz v0, :cond_a

    .line 866
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetAttributes()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 867
    sget-object v0, Lcom/evernote/edam/notestore/NoteMetadata;->ATTRIBUTES_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 868
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->attributes:Lcom/evernote/edam/type/NoteAttributes;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/type/NoteAttributes;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 869
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 872
    :cond_a
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->largestResourceMime:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 873
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetLargestResourceMime()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 874
    sget-object v0, Lcom/evernote/edam/notestore/NoteMetadata;->LARGEST_RESOURCE_MIME_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 875
    iget-object v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->largestResourceMime:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 876
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 879
    :cond_b
    invoke-virtual {p0}, Lcom/evernote/edam/notestore/NoteMetadata;->isSetLargestResourceSize()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 880
    sget-object v0, Lcom/evernote/edam/notestore/NoteMetadata;->LARGEST_RESOURCE_SIZE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 881
    iget v0, p0, Lcom/evernote/edam/notestore/NoteMetadata;->largestResourceSize:I

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeI32(I)V

    .line 882
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 884
    :cond_c
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldStop()V

    .line 885
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeStructEnd()V

    return-void
.end method

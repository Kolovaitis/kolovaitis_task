.class public Lcom/evernote/edam/userstore/UserStore$Client;
.super Ljava/lang/Object;
.source "UserStore.java"

# interfaces
.implements Lcom/evernote/thrift/TServiceClient;
.implements Lcom/evernote/edam/userstore/UserStore$Iface;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/evernote/edam/userstore/UserStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Client"
.end annotation


# instance fields
.field protected iprot_:Lcom/evernote/thrift/protocol/TProtocol;

.field protected oprot_:Lcom/evernote/thrift/protocol/TProtocol;

.field protected seqid_:I


# direct methods
.method public constructor <init>(Lcom/evernote/thrift/protocol/TProtocol;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1, p1}, Lcom/evernote/edam/userstore/UserStore$Client;-><init>(Lcom/evernote/thrift/protocol/TProtocol;Lcom/evernote/thrift/protocol/TProtocol;)V

    return-void
.end method

.method public constructor <init>(Lcom/evernote/thrift/protocol/TProtocol;Lcom/evernote/thrift/protocol/TProtocol;)V
    .locals 0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    .line 31
    iput-object p2, p0, Lcom/evernote/edam/userstore/UserStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    return-void
.end method


# virtual methods
.method public authenticate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/evernote/edam/userstore/AuthenticationResult;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 125
    invoke-virtual/range {p0 .. p5}, Lcom/evernote/edam/userstore/UserStore$Client;->send_authenticate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 126
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/UserStore$Client;->recv_authenticate()Lcom/evernote/edam/userstore/AuthenticationResult;

    move-result-object p1

    return-object p1
.end method

.method public authenticateLongSession(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/evernote/edam/userstore/AuthenticationResult;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 171
    invoke-virtual/range {p0 .. p7}, Lcom/evernote/edam/userstore/UserStore$Client;->send_authenticateLongSession(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 172
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/UserStore$Client;->recv_authenticateLongSession()Lcom/evernote/edam/userstore/AuthenticationResult;

    move-result-object p1

    return-object p1
.end method

.method public authenticateToBusiness(Ljava/lang/String;)Lcom/evernote/edam/userstore/AuthenticationResult;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 303
    invoke-virtual {p0, p1}, Lcom/evernote/edam/userstore/UserStore$Client;->send_authenticateToBusiness(Ljava/lang/String;)V

    .line 304
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/UserStore$Client;->recv_authenticateToBusiness()Lcom/evernote/edam/userstore/AuthenticationResult;

    move-result-object p1

    return-object p1
.end method

.method public checkVersion(Ljava/lang/String;SS)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 51
    invoke-virtual {p0, p1, p2, p3}, Lcom/evernote/edam/userstore/UserStore$Client;->send_checkVersion(Ljava/lang/String;SS)V

    .line 52
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/UserStore$Client;->recv_checkVersion()Z

    move-result p1

    return p1
.end method

.method public completeTwoFactorAuthentication(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/edam/userstore/AuthenticationResult;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 219
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/evernote/edam/userstore/UserStore$Client;->send_completeTwoFactorAuthentication(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/UserStore$Client;->recv_completeTwoFactorAuthentication()Lcom/evernote/edam/userstore/AuthenticationResult;

    move-result-object p1

    return-object p1
.end method

.method public getBootstrapInfo(Ljava/lang/String;)Lcom/evernote/edam/userstore/BootstrapInfo;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 89
    invoke-virtual {p0, p1}, Lcom/evernote/edam/userstore/UserStore$Client;->send_getBootstrapInfo(Ljava/lang/String;)V

    .line 90
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/UserStore$Client;->recv_getBootstrapInfo()Lcom/evernote/edam/userstore/BootstrapInfo;

    move-result-object p1

    return-object p1
.end method

.method public getInputProtocol()Lcom/evernote/thrift/protocol/TProtocol;
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    return-object v0
.end method

.method public getNoteStoreUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 516
    invoke-virtual {p0, p1}, Lcom/evernote/edam/userstore/UserStore$Client;->send_getNoteStoreUrl(Ljava/lang/String;)V

    .line 517
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/UserStore$Client;->recv_getNoteStoreUrl()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getOutputProtocol()Lcom/evernote/thrift/protocol/TProtocol;
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    return-object v0
.end method

.method public getPremiumInfo(Ljava/lang/String;)Lcom/evernote/edam/type/PremiumInfo;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 474
    invoke-virtual {p0, p1}, Lcom/evernote/edam/userstore/UserStore$Client;->send_getPremiumInfo(Ljava/lang/String;)V

    .line 475
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/UserStore$Client;->recv_getPremiumInfo()Lcom/evernote/edam/type/PremiumInfo;

    move-result-object p1

    return-object p1
.end method

.method public getPublicUserInfo(Ljava/lang/String;)Lcom/evernote/edam/userstore/PublicUserInfo;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 429
    invoke-virtual {p0, p1}, Lcom/evernote/edam/userstore/UserStore$Client;->send_getPublicUserInfo(Ljava/lang/String;)V

    .line 430
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/UserStore$Client;->recv_getPublicUserInfo()Lcom/evernote/edam/userstore/PublicUserInfo;

    move-result-object p1

    return-object p1
.end method

.method public getUser(Ljava/lang/String;)Lcom/evernote/edam/type/User;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 387
    invoke-virtual {p0, p1}, Lcom/evernote/edam/userstore/UserStore$Client;->send_getUser(Ljava/lang/String;)V

    .line 388
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/UserStore$Client;->recv_getUser()Lcom/evernote/edam/type/User;

    move-result-object p1

    return-object p1
.end method

.method public recv_authenticate()Lcom/evernote/edam/userstore/AuthenticationResult;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 145
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 146
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_4

    .line 151
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->seqid_:I

    if-ne v0, v1, :cond_3

    .line 154
    new-instance v0, Lcom/evernote/edam/userstore/UserStore$authenticate_result;

    invoke-direct {v0}, Lcom/evernote/edam/userstore/UserStore$authenticate_result;-><init>()V

    .line 155
    iget-object v1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/userstore/UserStore$authenticate_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 156
    iget-object v1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 157
    invoke-virtual {v0}, Lcom/evernote/edam/userstore/UserStore$authenticate_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 158
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$authenticate_result;->access$200(Lcom/evernote/edam/userstore/UserStore$authenticate_result;)Lcom/evernote/edam/userstore/AuthenticationResult;

    move-result-object v0

    return-object v0

    .line 160
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$authenticate_result;->access$300(Lcom/evernote/edam/userstore/UserStore$authenticate_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 163
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$authenticate_result;->access$400(Lcom/evernote/edam/userstore/UserStore$authenticate_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 164
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$authenticate_result;->access$400(Lcom/evernote/edam/userstore/UserStore$authenticate_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 166
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "authenticate failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 161
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$authenticate_result;->access$300(Lcom/evernote/edam/userstore/UserStore$authenticate_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 152
    :cond_3
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "authenticate failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 147
    :cond_4
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 148
    iget-object v1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 149
    throw v0
.end method

.method public recv_authenticateLongSession()Lcom/evernote/edam/userstore/AuthenticationResult;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 193
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 194
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_4

    .line 199
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->seqid_:I

    if-ne v0, v1, :cond_3

    .line 202
    new-instance v0, Lcom/evernote/edam/userstore/UserStore$authenticateLongSession_result;

    invoke-direct {v0}, Lcom/evernote/edam/userstore/UserStore$authenticateLongSession_result;-><init>()V

    .line 203
    iget-object v1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/userstore/UserStore$authenticateLongSession_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 204
    iget-object v1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 205
    invoke-virtual {v0}, Lcom/evernote/edam/userstore/UserStore$authenticateLongSession_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 206
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$authenticateLongSession_result;->access$500(Lcom/evernote/edam/userstore/UserStore$authenticateLongSession_result;)Lcom/evernote/edam/userstore/AuthenticationResult;

    move-result-object v0

    return-object v0

    .line 208
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$authenticateLongSession_result;->access$600(Lcom/evernote/edam/userstore/UserStore$authenticateLongSession_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 211
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$authenticateLongSession_result;->access$700(Lcom/evernote/edam/userstore/UserStore$authenticateLongSession_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 212
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$authenticateLongSession_result;->access$700(Lcom/evernote/edam/userstore/UserStore$authenticateLongSession_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 214
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "authenticateLongSession failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 209
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$authenticateLongSession_result;->access$600(Lcom/evernote/edam/userstore/UserStore$authenticateLongSession_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 200
    :cond_3
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "authenticateLongSession failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 195
    :cond_4
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 196
    iget-object v1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 197
    throw v0
.end method

.method public recv_authenticateToBusiness()Lcom/evernote/edam/userstore/AuthenticationResult;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 319
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 320
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_4

    .line 325
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->seqid_:I

    if-ne v0, v1, :cond_3

    .line 328
    new-instance v0, Lcom/evernote/edam/userstore/UserStore$authenticateToBusiness_result;

    invoke-direct {v0}, Lcom/evernote/edam/userstore/UserStore$authenticateToBusiness_result;-><init>()V

    .line 329
    iget-object v1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/userstore/UserStore$authenticateToBusiness_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 330
    iget-object v1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 331
    invoke-virtual {v0}, Lcom/evernote/edam/userstore/UserStore$authenticateToBusiness_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 332
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$authenticateToBusiness_result;->access$1300(Lcom/evernote/edam/userstore/UserStore$authenticateToBusiness_result;)Lcom/evernote/edam/userstore/AuthenticationResult;

    move-result-object v0

    return-object v0

    .line 334
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$authenticateToBusiness_result;->access$1400(Lcom/evernote/edam/userstore/UserStore$authenticateToBusiness_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 337
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$authenticateToBusiness_result;->access$1500(Lcom/evernote/edam/userstore/UserStore$authenticateToBusiness_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 338
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$authenticateToBusiness_result;->access$1500(Lcom/evernote/edam/userstore/UserStore$authenticateToBusiness_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 340
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "authenticateToBusiness failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 335
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$authenticateToBusiness_result;->access$1400(Lcom/evernote/edam/userstore/UserStore$authenticateToBusiness_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 326
    :cond_3
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "authenticateToBusiness failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 321
    :cond_4
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 322
    iget-object v1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 323
    throw v0
.end method

.method public recv_checkVersion()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 69
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 70
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_2

    .line 75
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->seqid_:I

    if-ne v0, v1, :cond_1

    .line 78
    new-instance v0, Lcom/evernote/edam/userstore/UserStore$checkVersion_result;

    invoke-direct {v0}, Lcom/evernote/edam/userstore/UserStore$checkVersion_result;-><init>()V

    .line 79
    iget-object v1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/userstore/UserStore$checkVersion_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 80
    iget-object v1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 81
    invoke-virtual {v0}, Lcom/evernote/edam/userstore/UserStore$checkVersion_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 82
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$checkVersion_result;->access$000(Lcom/evernote/edam/userstore/UserStore$checkVersion_result;)Z

    move-result v0

    return v0

    .line 84
    :cond_0
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "checkVersion failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 76
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "checkVersion failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 71
    :cond_2
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 72
    iget-object v1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 73
    throw v0
.end method

.method public recv_completeTwoFactorAuthentication()Lcom/evernote/edam/userstore/AuthenticationResult;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 238
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 239
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_4

    .line 244
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->seqid_:I

    if-ne v0, v1, :cond_3

    .line 247
    new-instance v0, Lcom/evernote/edam/userstore/UserStore$completeTwoFactorAuthentication_result;

    invoke-direct {v0}, Lcom/evernote/edam/userstore/UserStore$completeTwoFactorAuthentication_result;-><init>()V

    .line 248
    iget-object v1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/userstore/UserStore$completeTwoFactorAuthentication_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 249
    iget-object v1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 250
    invoke-virtual {v0}, Lcom/evernote/edam/userstore/UserStore$completeTwoFactorAuthentication_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 251
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$completeTwoFactorAuthentication_result;->access$800(Lcom/evernote/edam/userstore/UserStore$completeTwoFactorAuthentication_result;)Lcom/evernote/edam/userstore/AuthenticationResult;

    move-result-object v0

    return-object v0

    .line 253
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$completeTwoFactorAuthentication_result;->access$900(Lcom/evernote/edam/userstore/UserStore$completeTwoFactorAuthentication_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 256
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$completeTwoFactorAuthentication_result;->access$1000(Lcom/evernote/edam/userstore/UserStore$completeTwoFactorAuthentication_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 257
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$completeTwoFactorAuthentication_result;->access$1000(Lcom/evernote/edam/userstore/UserStore$completeTwoFactorAuthentication_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 259
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "completeTwoFactorAuthentication failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 254
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$completeTwoFactorAuthentication_result;->access$900(Lcom/evernote/edam/userstore/UserStore$completeTwoFactorAuthentication_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 245
    :cond_3
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "completeTwoFactorAuthentication failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 240
    :cond_4
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 241
    iget-object v1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 242
    throw v0
.end method

.method public recv_getBootstrapInfo()Lcom/evernote/edam/userstore/BootstrapInfo;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 105
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 106
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_2

    .line 111
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->seqid_:I

    if-ne v0, v1, :cond_1

    .line 114
    new-instance v0, Lcom/evernote/edam/userstore/UserStore$getBootstrapInfo_result;

    invoke-direct {v0}, Lcom/evernote/edam/userstore/UserStore$getBootstrapInfo_result;-><init>()V

    .line 115
    iget-object v1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/userstore/UserStore$getBootstrapInfo_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 116
    iget-object v1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 117
    invoke-virtual {v0}, Lcom/evernote/edam/userstore/UserStore$getBootstrapInfo_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 118
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$getBootstrapInfo_result;->access$100(Lcom/evernote/edam/userstore/UserStore$getBootstrapInfo_result;)Lcom/evernote/edam/userstore/BootstrapInfo;

    move-result-object v0

    return-object v0

    .line 120
    :cond_0
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "getBootstrapInfo failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 112
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "getBootstrapInfo failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 107
    :cond_2
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 108
    iget-object v1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 109
    throw v0
.end method

.method public recv_getNoteStoreUrl()Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 532
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 533
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_4

    .line 538
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->seqid_:I

    if-ne v0, v1, :cond_3

    .line 541
    new-instance v0, Lcom/evernote/edam/userstore/UserStore$getNoteStoreUrl_result;

    invoke-direct {v0}, Lcom/evernote/edam/userstore/UserStore$getNoteStoreUrl_result;-><init>()V

    .line 542
    iget-object v1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/userstore/UserStore$getNoteStoreUrl_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 543
    iget-object v1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 544
    invoke-virtual {v0}, Lcom/evernote/edam/userstore/UserStore$getNoteStoreUrl_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 545
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$getNoteStoreUrl_result;->access$2900(Lcom/evernote/edam/userstore/UserStore$getNoteStoreUrl_result;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 547
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$getNoteStoreUrl_result;->access$3000(Lcom/evernote/edam/userstore/UserStore$getNoteStoreUrl_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 550
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$getNoteStoreUrl_result;->access$3100(Lcom/evernote/edam/userstore/UserStore$getNoteStoreUrl_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 551
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$getNoteStoreUrl_result;->access$3100(Lcom/evernote/edam/userstore/UserStore$getNoteStoreUrl_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 553
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "getNoteStoreUrl failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 548
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$getNoteStoreUrl_result;->access$3000(Lcom/evernote/edam/userstore/UserStore$getNoteStoreUrl_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 539
    :cond_3
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "getNoteStoreUrl failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 534
    :cond_4
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 535
    iget-object v1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 536
    throw v0
.end method

.method public recv_getPremiumInfo()Lcom/evernote/edam/type/PremiumInfo;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 490
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 491
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_4

    .line 496
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->seqid_:I

    if-ne v0, v1, :cond_3

    .line 499
    new-instance v0, Lcom/evernote/edam/userstore/UserStore$getPremiumInfo_result;

    invoke-direct {v0}, Lcom/evernote/edam/userstore/UserStore$getPremiumInfo_result;-><init>()V

    .line 500
    iget-object v1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/userstore/UserStore$getPremiumInfo_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 501
    iget-object v1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 502
    invoke-virtual {v0}, Lcom/evernote/edam/userstore/UserStore$getPremiumInfo_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 503
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$getPremiumInfo_result;->access$2600(Lcom/evernote/edam/userstore/UserStore$getPremiumInfo_result;)Lcom/evernote/edam/type/PremiumInfo;

    move-result-object v0

    return-object v0

    .line 505
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$getPremiumInfo_result;->access$2700(Lcom/evernote/edam/userstore/UserStore$getPremiumInfo_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 508
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$getPremiumInfo_result;->access$2800(Lcom/evernote/edam/userstore/UserStore$getPremiumInfo_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 509
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$getPremiumInfo_result;->access$2800(Lcom/evernote/edam/userstore/UserStore$getPremiumInfo_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 511
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "getPremiumInfo failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 506
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$getPremiumInfo_result;->access$2700(Lcom/evernote/edam/userstore/UserStore$getPremiumInfo_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 497
    :cond_3
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "getPremiumInfo failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 492
    :cond_4
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 493
    iget-object v1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 494
    throw v0
.end method

.method public recv_getPublicUserInfo()Lcom/evernote/edam/userstore/PublicUserInfo;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 445
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 446
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    .line 451
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->seqid_:I

    if-ne v0, v1, :cond_4

    .line 454
    new-instance v0, Lcom/evernote/edam/userstore/UserStore$getPublicUserInfo_result;

    invoke-direct {v0}, Lcom/evernote/edam/userstore/UserStore$getPublicUserInfo_result;-><init>()V

    .line 455
    iget-object v1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/userstore/UserStore$getPublicUserInfo_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 456
    iget-object v1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 457
    invoke-virtual {v0}, Lcom/evernote/edam/userstore/UserStore$getPublicUserInfo_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 458
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$getPublicUserInfo_result;->access$2200(Lcom/evernote/edam/userstore/UserStore$getPublicUserInfo_result;)Lcom/evernote/edam/userstore/PublicUserInfo;

    move-result-object v0

    return-object v0

    .line 460
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$getPublicUserInfo_result;->access$2300(Lcom/evernote/edam/userstore/UserStore$getPublicUserInfo_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v1

    if-nez v1, :cond_3

    .line 463
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$getPublicUserInfo_result;->access$2400(Lcom/evernote/edam/userstore/UserStore$getPublicUserInfo_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 466
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$getPublicUserInfo_result;->access$2500(Lcom/evernote/edam/userstore/UserStore$getPublicUserInfo_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 467
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$getPublicUserInfo_result;->access$2500(Lcom/evernote/edam/userstore/UserStore$getPublicUserInfo_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 469
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "getPublicUserInfo failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 464
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$getPublicUserInfo_result;->access$2400(Lcom/evernote/edam/userstore/UserStore$getPublicUserInfo_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 461
    :cond_3
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$getPublicUserInfo_result;->access$2300(Lcom/evernote/edam/userstore/UserStore$getPublicUserInfo_result;)Lcom/evernote/edam/error/EDAMNotFoundException;

    move-result-object v0

    throw v0

    .line 452
    :cond_4
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "getPublicUserInfo failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 447
    :cond_5
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 448
    iget-object v1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 449
    throw v0
.end method

.method public recv_getUser()Lcom/evernote/edam/type/User;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 403
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 404
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_4

    .line 409
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->seqid_:I

    if-ne v0, v1, :cond_3

    .line 412
    new-instance v0, Lcom/evernote/edam/userstore/UserStore$getUser_result;

    invoke-direct {v0}, Lcom/evernote/edam/userstore/UserStore$getUser_result;-><init>()V

    .line 413
    iget-object v1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/userstore/UserStore$getUser_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 414
    iget-object v1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 415
    invoke-virtual {v0}, Lcom/evernote/edam/userstore/UserStore$getUser_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 416
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$getUser_result;->access$1900(Lcom/evernote/edam/userstore/UserStore$getUser_result;)Lcom/evernote/edam/type/User;

    move-result-object v0

    return-object v0

    .line 418
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$getUser_result;->access$2000(Lcom/evernote/edam/userstore/UserStore$getUser_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 421
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$getUser_result;->access$2100(Lcom/evernote/edam/userstore/UserStore$getUser_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 422
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$getUser_result;->access$2100(Lcom/evernote/edam/userstore/UserStore$getUser_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 424
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "getUser failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 419
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$getUser_result;->access$2000(Lcom/evernote/edam/userstore/UserStore$getUser_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 410
    :cond_3
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "getUser failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 405
    :cond_4
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 406
    iget-object v1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 407
    throw v0
.end method

.method public recv_refreshAuthentication()Lcom/evernote/edam/userstore/AuthenticationResult;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 361
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 362
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_4

    .line 367
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->seqid_:I

    if-ne v0, v1, :cond_3

    .line 370
    new-instance v0, Lcom/evernote/edam/userstore/UserStore$refreshAuthentication_result;

    invoke-direct {v0}, Lcom/evernote/edam/userstore/UserStore$refreshAuthentication_result;-><init>()V

    .line 371
    iget-object v1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/userstore/UserStore$refreshAuthentication_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 372
    iget-object v1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 373
    invoke-virtual {v0}, Lcom/evernote/edam/userstore/UserStore$refreshAuthentication_result;->isSetSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 374
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$refreshAuthentication_result;->access$1600(Lcom/evernote/edam/userstore/UserStore$refreshAuthentication_result;)Lcom/evernote/edam/userstore/AuthenticationResult;

    move-result-object v0

    return-object v0

    .line 376
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$refreshAuthentication_result;->access$1700(Lcom/evernote/edam/userstore/UserStore$refreshAuthentication_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_2

    .line 379
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$refreshAuthentication_result;->access$1800(Lcom/evernote/edam/userstore/UserStore$refreshAuthentication_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 380
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$refreshAuthentication_result;->access$1800(Lcom/evernote/edam/userstore/UserStore$refreshAuthentication_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 382
    :cond_1
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x5

    const-string v2, "refreshAuthentication failed: unknown result"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 377
    :cond_2
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$refreshAuthentication_result;->access$1700(Lcom/evernote/edam/userstore/UserStore$refreshAuthentication_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 368
    :cond_3
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "refreshAuthentication failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 363
    :cond_4
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 364
    iget-object v1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 365
    throw v0
.end method

.method public recv_revokeLongSession()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 280
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;

    move-result-object v0

    .line 281
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_3

    .line 286
    iget v0, v0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget v1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->seqid_:I

    if-ne v0, v1, :cond_2

    .line 289
    new-instance v0, Lcom/evernote/edam/userstore/UserStore$revokeLongSession_result;

    invoke-direct {v0}, Lcom/evernote/edam/userstore/UserStore$revokeLongSession_result;-><init>()V

    .line 290
    iget-object v1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/userstore/UserStore$revokeLongSession_result;->read(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 291
    iget-object v1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 292
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$revokeLongSession_result;->access$1100(Lcom/evernote/edam/userstore/UserStore$revokeLongSession_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v1

    if-nez v1, :cond_1

    .line 295
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$revokeLongSession_result;->access$1200(Lcom/evernote/edam/userstore/UserStore$revokeLongSession_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v1

    if-nez v1, :cond_0

    return-void

    .line 296
    :cond_0
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$revokeLongSession_result;->access$1200(Lcom/evernote/edam/userstore/UserStore$revokeLongSession_result;)Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    throw v0

    .line 293
    :cond_1
    invoke-static {v0}, Lcom/evernote/edam/userstore/UserStore$revokeLongSession_result;->access$1100(Lcom/evernote/edam/userstore/UserStore$revokeLongSession_result;)Lcom/evernote/edam/error/EDAMUserException;

    move-result-object v0

    throw v0

    .line 287
    :cond_2
    new-instance v0, Lcom/evernote/thrift/TApplicationException;

    const/4 v1, 0x4

    const-string v2, "revokeLongSession failed: out of sequence response"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/TApplicationException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 282
    :cond_3
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-static {v0}, Lcom/evernote/thrift/TApplicationException;->read(Lcom/evernote/thrift/protocol/TProtocol;)Lcom/evernote/thrift/TApplicationException;

    move-result-object v0

    .line 283
    iget-object v1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->iprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v1}, Lcom/evernote/thrift/protocol/TProtocol;->readMessageEnd()V

    .line 284
    throw v0
.end method

.method public refreshAuthentication(Ljava/lang/String;)Lcom/evernote/edam/userstore/AuthenticationResult;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 345
    invoke-virtual {p0, p1}, Lcom/evernote/edam/userstore/UserStore$Client;->send_refreshAuthentication(Ljava/lang/String;)V

    .line 346
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/UserStore$Client;->recv_refreshAuthentication()Lcom/evernote/edam/userstore/AuthenticationResult;

    move-result-object p1

    return-object p1
.end method

.method public revokeLongSession(Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 264
    invoke-virtual {p0, p1}, Lcom/evernote/edam/userstore/UserStore$Client;->send_revokeLongSession(Ljava/lang/String;)V

    .line 265
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/UserStore$Client;->recv_revokeLongSession()V

    return-void
.end method

.method public send_authenticate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 131
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "authenticate"

    iget v3, p0, Lcom/evernote/edam/userstore/UserStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/userstore/UserStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 132
    new-instance v0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;

    invoke-direct {v0}, Lcom/evernote/edam/userstore/UserStore$authenticate_args;-><init>()V

    .line 133
    invoke-virtual {v0, p1}, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->setUsername(Ljava/lang/String;)V

    .line 134
    invoke-virtual {v0, p2}, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->setPassword(Ljava/lang/String;)V

    .line 135
    invoke-virtual {v0, p3}, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->setConsumerKey(Ljava/lang/String;)V

    .line 136
    invoke-virtual {v0, p4}, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->setConsumerSecret(Ljava/lang/String;)V

    .line 137
    invoke-virtual {v0, p5}, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->setSupportsTwoFactor(Z)V

    .line 138
    iget-object p1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 139
    iget-object p1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 140
    iget-object p1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_authenticateLongSession(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 177
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "authenticateLongSession"

    iget v3, p0, Lcom/evernote/edam/userstore/UserStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/userstore/UserStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 178
    new-instance v0, Lcom/evernote/edam/userstore/UserStore$authenticateLongSession_args;

    invoke-direct {v0}, Lcom/evernote/edam/userstore/UserStore$authenticateLongSession_args;-><init>()V

    .line 179
    invoke-virtual {v0, p1}, Lcom/evernote/edam/userstore/UserStore$authenticateLongSession_args;->setUsername(Ljava/lang/String;)V

    .line 180
    invoke-virtual {v0, p2}, Lcom/evernote/edam/userstore/UserStore$authenticateLongSession_args;->setPassword(Ljava/lang/String;)V

    .line 181
    invoke-virtual {v0, p3}, Lcom/evernote/edam/userstore/UserStore$authenticateLongSession_args;->setConsumerKey(Ljava/lang/String;)V

    .line 182
    invoke-virtual {v0, p4}, Lcom/evernote/edam/userstore/UserStore$authenticateLongSession_args;->setConsumerSecret(Ljava/lang/String;)V

    .line 183
    invoke-virtual {v0, p5}, Lcom/evernote/edam/userstore/UserStore$authenticateLongSession_args;->setDeviceIdentifier(Ljava/lang/String;)V

    .line 184
    invoke-virtual {v0, p6}, Lcom/evernote/edam/userstore/UserStore$authenticateLongSession_args;->setDeviceDescription(Ljava/lang/String;)V

    .line 185
    invoke-virtual {v0, p7}, Lcom/evernote/edam/userstore/UserStore$authenticateLongSession_args;->setSupportsTwoFactor(Z)V

    .line 186
    iget-object p1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/userstore/UserStore$authenticateLongSession_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 187
    iget-object p1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 188
    iget-object p1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_authenticateToBusiness(Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 309
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "authenticateToBusiness"

    iget v3, p0, Lcom/evernote/edam/userstore/UserStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/userstore/UserStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 310
    new-instance v0, Lcom/evernote/edam/userstore/UserStore$authenticateToBusiness_args;

    invoke-direct {v0}, Lcom/evernote/edam/userstore/UserStore$authenticateToBusiness_args;-><init>()V

    .line 311
    invoke-virtual {v0, p1}, Lcom/evernote/edam/userstore/UserStore$authenticateToBusiness_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 312
    iget-object p1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/userstore/UserStore$authenticateToBusiness_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 313
    iget-object p1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 314
    iget-object p1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_checkVersion(Ljava/lang/String;SS)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 57
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "checkVersion"

    iget v3, p0, Lcom/evernote/edam/userstore/UserStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/userstore/UserStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 58
    new-instance v0, Lcom/evernote/edam/userstore/UserStore$checkVersion_args;

    invoke-direct {v0}, Lcom/evernote/edam/userstore/UserStore$checkVersion_args;-><init>()V

    .line 59
    invoke-virtual {v0, p1}, Lcom/evernote/edam/userstore/UserStore$checkVersion_args;->setClientName(Ljava/lang/String;)V

    .line 60
    invoke-virtual {v0, p2}, Lcom/evernote/edam/userstore/UserStore$checkVersion_args;->setEdamVersionMajor(S)V

    .line 61
    invoke-virtual {v0, p3}, Lcom/evernote/edam/userstore/UserStore$checkVersion_args;->setEdamVersionMinor(S)V

    .line 62
    iget-object p1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/userstore/UserStore$checkVersion_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 63
    iget-object p1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 64
    iget-object p1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_completeTwoFactorAuthentication(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 225
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "completeTwoFactorAuthentication"

    iget v3, p0, Lcom/evernote/edam/userstore/UserStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/userstore/UserStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 226
    new-instance v0, Lcom/evernote/edam/userstore/UserStore$completeTwoFactorAuthentication_args;

    invoke-direct {v0}, Lcom/evernote/edam/userstore/UserStore$completeTwoFactorAuthentication_args;-><init>()V

    .line 227
    invoke-virtual {v0, p1}, Lcom/evernote/edam/userstore/UserStore$completeTwoFactorAuthentication_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 228
    invoke-virtual {v0, p2}, Lcom/evernote/edam/userstore/UserStore$completeTwoFactorAuthentication_args;->setOneTimeCode(Ljava/lang/String;)V

    .line 229
    invoke-virtual {v0, p3}, Lcom/evernote/edam/userstore/UserStore$completeTwoFactorAuthentication_args;->setDeviceIdentifier(Ljava/lang/String;)V

    .line 230
    invoke-virtual {v0, p4}, Lcom/evernote/edam/userstore/UserStore$completeTwoFactorAuthentication_args;->setDeviceDescription(Ljava/lang/String;)V

    .line 231
    iget-object p1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/userstore/UserStore$completeTwoFactorAuthentication_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 232
    iget-object p1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 233
    iget-object p1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_getBootstrapInfo(Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 95
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "getBootstrapInfo"

    iget v3, p0, Lcom/evernote/edam/userstore/UserStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/userstore/UserStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 96
    new-instance v0, Lcom/evernote/edam/userstore/UserStore$getBootstrapInfo_args;

    invoke-direct {v0}, Lcom/evernote/edam/userstore/UserStore$getBootstrapInfo_args;-><init>()V

    .line 97
    invoke-virtual {v0, p1}, Lcom/evernote/edam/userstore/UserStore$getBootstrapInfo_args;->setLocale(Ljava/lang/String;)V

    .line 98
    iget-object p1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/userstore/UserStore$getBootstrapInfo_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 99
    iget-object p1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 100
    iget-object p1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_getNoteStoreUrl(Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 522
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "getNoteStoreUrl"

    iget v3, p0, Lcom/evernote/edam/userstore/UserStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/userstore/UserStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 523
    new-instance v0, Lcom/evernote/edam/userstore/UserStore$getNoteStoreUrl_args;

    invoke-direct {v0}, Lcom/evernote/edam/userstore/UserStore$getNoteStoreUrl_args;-><init>()V

    .line 524
    invoke-virtual {v0, p1}, Lcom/evernote/edam/userstore/UserStore$getNoteStoreUrl_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 525
    iget-object p1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/userstore/UserStore$getNoteStoreUrl_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 526
    iget-object p1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 527
    iget-object p1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_getPremiumInfo(Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 480
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "getPremiumInfo"

    iget v3, p0, Lcom/evernote/edam/userstore/UserStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/userstore/UserStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 481
    new-instance v0, Lcom/evernote/edam/userstore/UserStore$getPremiumInfo_args;

    invoke-direct {v0}, Lcom/evernote/edam/userstore/UserStore$getPremiumInfo_args;-><init>()V

    .line 482
    invoke-virtual {v0, p1}, Lcom/evernote/edam/userstore/UserStore$getPremiumInfo_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 483
    iget-object p1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/userstore/UserStore$getPremiumInfo_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 484
    iget-object p1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 485
    iget-object p1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_getPublicUserInfo(Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 435
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "getPublicUserInfo"

    iget v3, p0, Lcom/evernote/edam/userstore/UserStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/userstore/UserStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 436
    new-instance v0, Lcom/evernote/edam/userstore/UserStore$getPublicUserInfo_args;

    invoke-direct {v0}, Lcom/evernote/edam/userstore/UserStore$getPublicUserInfo_args;-><init>()V

    .line 437
    invoke-virtual {v0, p1}, Lcom/evernote/edam/userstore/UserStore$getPublicUserInfo_args;->setUsername(Ljava/lang/String;)V

    .line 438
    iget-object p1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/userstore/UserStore$getPublicUserInfo_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 439
    iget-object p1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 440
    iget-object p1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_getUser(Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 393
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "getUser"

    iget v3, p0, Lcom/evernote/edam/userstore/UserStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/userstore/UserStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 394
    new-instance v0, Lcom/evernote/edam/userstore/UserStore$getUser_args;

    invoke-direct {v0}, Lcom/evernote/edam/userstore/UserStore$getUser_args;-><init>()V

    .line 395
    invoke-virtual {v0, p1}, Lcom/evernote/edam/userstore/UserStore$getUser_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 396
    iget-object p1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/userstore/UserStore$getUser_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 397
    iget-object p1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 398
    iget-object p1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_refreshAuthentication(Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 351
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "refreshAuthentication"

    iget v3, p0, Lcom/evernote/edam/userstore/UserStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/userstore/UserStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 352
    new-instance v0, Lcom/evernote/edam/userstore/UserStore$refreshAuthentication_args;

    invoke-direct {v0}, Lcom/evernote/edam/userstore/UserStore$refreshAuthentication_args;-><init>()V

    .line 353
    invoke-virtual {v0, p1}, Lcom/evernote/edam/userstore/UserStore$refreshAuthentication_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 354
    iget-object p1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/userstore/UserStore$refreshAuthentication_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 355
    iget-object p1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 356
    iget-object p1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.method public send_revokeLongSession(Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 270
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    const-string v2, "revokeLongSession"

    iget v3, p0, Lcom/evernote/edam/userstore/UserStore$Client;->seqid_:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/evernote/edam/userstore/UserStore$Client;->seqid_:I

    invoke-direct {v1, v2, v4, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    invoke-virtual {v0, v1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V

    .line 271
    new-instance v0, Lcom/evernote/edam/userstore/UserStore$revokeLongSession_args;

    invoke-direct {v0}, Lcom/evernote/edam/userstore/UserStore$revokeLongSession_args;-><init>()V

    .line 272
    invoke-virtual {v0, p1}, Lcom/evernote/edam/userstore/UserStore$revokeLongSession_args;->setAuthenticationToken(Ljava/lang/String;)V

    .line 273
    iget-object p1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {v0, p1}, Lcom/evernote/edam/userstore/UserStore$revokeLongSession_args;->write(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 274
    iget-object p1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeMessageEnd()V

    .line 275
    iget-object p1, p0, Lcom/evernote/edam/userstore/UserStore$Client;->oprot_:Lcom/evernote/thrift/protocol/TProtocol;

    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->getTransport()Lcom/evernote/thrift/transport/TTransport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/thrift/transport/TTransport;->flush()V

    return-void
.end method

.class Lcom/evernote/edam/userstore/UserStore$authenticate_args;
.super Ljava/lang/Object;
.source "UserStore.java"

# interfaces
.implements Lcom/evernote/thrift/TBase;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/evernote/edam/userstore/UserStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "authenticate_args"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/evernote/thrift/TBase<",
        "Lcom/evernote/edam/userstore/UserStore$authenticate_args;",
        ">;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# static fields
.field private static final CONSUMER_KEY_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final CONSUMER_SECRET_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final PASSWORD_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

.field private static final SUPPORTS_TWO_FACTOR_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final USERNAME_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final __SUPPORTSTWOFACTOR_ISSET_ID:I


# instance fields
.field private __isset_vector:[Z

.field private consumerKey:Ljava/lang/String;

.field private consumerSecret:Ljava/lang/String;

.field private password:Ljava/lang/String;

.field private supportsTwoFactor:Z

.field private username:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 1058
    new-instance v0, Lcom/evernote/thrift/protocol/TStruct;

    const-string v1, "authenticate_args"

    invoke-direct {v0, v1}, Lcom/evernote/thrift/protocol/TStruct;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

    .line 1060
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "username"

    const/16 v2, 0xb

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->USERNAME_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 1061
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "password"

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->PASSWORD_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 1062
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "consumerKey"

    const/4 v4, 0x3

    invoke-direct {v0, v1, v2, v4}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->CONSUMER_KEY_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 1063
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "consumerSecret"

    const/4 v4, 0x4

    invoke-direct {v0, v1, v2, v4}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->CONSUMER_SECRET_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 1064
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "supportsTwoFactor"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v3, v2}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->SUPPORTS_TWO_FACTOR_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 1077
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 1075
    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->__isset_vector:[Z

    return-void
.end method

.method public constructor <init>(Lcom/evernote/edam/userstore/UserStore$authenticate_args;)V
    .locals 4

    .line 1083
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 1075
    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->__isset_vector:[Z

    .line 1084
    iget-object v0, p1, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->__isset_vector:[Z

    iget-object v1, p0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->__isset_vector:[Z

    array-length v2, v0

    const/4 v3, 0x0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1085
    invoke-virtual {p1}, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->isSetUsername()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1086
    iget-object v0, p1, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->username:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->username:Ljava/lang/String;

    .line 1088
    :cond_0
    invoke-virtual {p1}, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->isSetPassword()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1089
    iget-object v0, p1, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->password:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->password:Ljava/lang/String;

    .line 1091
    :cond_1
    invoke-virtual {p1}, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->isSetConsumerKey()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1092
    iget-object v0, p1, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->consumerKey:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->consumerKey:Ljava/lang/String;

    .line 1094
    :cond_2
    invoke-virtual {p1}, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->isSetConsumerSecret()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1095
    iget-object v0, p1, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->consumerSecret:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->consumerSecret:Ljava/lang/String;

    .line 1097
    :cond_3
    iget-boolean p1, p1, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->supportsTwoFactor:Z

    iput-boolean p1, p0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->supportsTwoFactor:Z

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    const/4 v0, 0x0

    .line 1105
    iput-object v0, p0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->username:Ljava/lang/String;

    .line 1106
    iput-object v0, p0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->password:Ljava/lang/String;

    .line 1107
    iput-object v0, p0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->consumerKey:Ljava/lang/String;

    .line 1108
    iput-object v0, p0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->consumerSecret:Ljava/lang/String;

    const/4 v0, 0x0

    .line 1109
    invoke-virtual {p0, v0}, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->setSupportsTwoFactorIsSet(Z)V

    .line 1110
    iput-boolean v0, p0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->supportsTwoFactor:Z

    return-void
.end method

.method public compareTo(Lcom/evernote/edam/userstore/UserStore$authenticate_args;)I
    .locals 2

    .line 1164
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1165
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    return p1

    .line 1171
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->isSetUsername()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->isSetUsername()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_1

    return v0

    .line 1175
    :cond_1
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->isSetUsername()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->username:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->username:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_2

    return v0

    .line 1180
    :cond_2
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->isSetPassword()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->isSetPassword()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_3

    return v0

    .line 1184
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->isSetPassword()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->password:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->password:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_4

    return v0

    .line 1189
    :cond_4
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->isSetConsumerKey()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->isSetConsumerKey()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_5

    return v0

    .line 1193
    :cond_5
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->isSetConsumerKey()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->consumerKey:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->consumerKey:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_6

    return v0

    .line 1198
    :cond_6
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->isSetConsumerSecret()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->isSetConsumerSecret()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_7

    return v0

    .line 1202
    :cond_7
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->isSetConsumerSecret()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->consumerSecret:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->consumerSecret:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_8

    return v0

    .line 1207
    :cond_8
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->isSetSupportsTwoFactor()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->isSetSupportsTwoFactor()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_9

    return v0

    .line 1211
    :cond_9
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->isSetSupportsTwoFactor()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-boolean v0, p0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->supportsTwoFactor:Z

    iget-boolean p1, p1, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->supportsTwoFactor:Z

    invoke-static {v0, p1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result p1

    if-eqz p1, :cond_a

    return p1

    :cond_a
    const/4 p1, 0x0

    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 1057
    check-cast p1, Lcom/evernote/edam/userstore/UserStore$authenticate_args;

    invoke-virtual {p0, p1}, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->compareTo(Lcom/evernote/edam/userstore/UserStore$authenticate_args;)I

    move-result p1

    return p1
.end method

.method public deepCopy()Lcom/evernote/edam/userstore/UserStore$authenticate_args;
    .locals 1

    .line 1101
    new-instance v0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;

    invoke-direct {v0, p0}, Lcom/evernote/edam/userstore/UserStore$authenticate_args;-><init>(Lcom/evernote/edam/userstore/UserStore$authenticate_args;)V

    return-object v0
.end method

.method public bridge synthetic deepCopy()Lcom/evernote/thrift/TBase;
    .locals 1

    .line 1057
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->deepCopy()Lcom/evernote/edam/userstore/UserStore$authenticate_args;

    move-result-object v0

    return-object v0
.end method

.method public isSetConsumerKey()Z
    .locals 1

    .line 1137
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->consumerKey:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetConsumerSecret()Z
    .locals 1

    .line 1146
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->consumerSecret:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetPassword()Z
    .locals 1

    .line 1128
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->password:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetSupportsTwoFactor()Z
    .locals 2

    .line 1156
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->__isset_vector:[Z

    const/4 v1, 0x0

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetUsername()Z
    .locals 1

    .line 1119
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->username:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public read(Lcom/evernote/thrift/protocol/TProtocol;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1221
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readStructBegin()Lcom/evernote/thrift/protocol/TStruct;

    .line 1224
    :goto_0
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readFieldBegin()Lcom/evernote/thrift/protocol/TField;

    move-result-object v0

    .line 1225
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-nez v1, :cond_0

    .line 1270
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readStructEnd()V

    .line 1271
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->validate()V

    return-void

    .line 1228
    :cond_0
    iget-short v1, v0, Lcom/evernote/thrift/protocol/TField;->id:S

    const/16 v2, 0xb

    packed-switch v1, :pswitch_data_0

    .line 1266
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 1258
    :pswitch_0
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 1259
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->supportsTwoFactor:Z

    const/4 v0, 0x1

    .line 1260
    invoke-virtual {p0, v0}, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->setSupportsTwoFactorIsSet(Z)V

    goto :goto_1

    .line 1262
    :cond_1
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 1251
    :pswitch_1
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v2, :cond_2

    .line 1252
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->consumerSecret:Ljava/lang/String;

    goto :goto_1

    .line 1254
    :cond_2
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 1244
    :pswitch_2
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v2, :cond_3

    .line 1245
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->consumerKey:Ljava/lang/String;

    goto :goto_1

    .line 1247
    :cond_3
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 1237
    :pswitch_3
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v2, :cond_4

    .line 1238
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->password:Ljava/lang/String;

    goto :goto_1

    .line 1240
    :cond_4
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 1230
    :pswitch_4
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v2, :cond_5

    .line 1231
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->username:Ljava/lang/String;

    goto :goto_1

    .line 1233
    :cond_5
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    .line 1268
    :goto_1
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readFieldEnd()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setConsumerKey(Ljava/lang/String;)V
    .locals 0

    .line 1132
    iput-object p1, p0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->consumerKey:Ljava/lang/String;

    return-void
.end method

.method public setConsumerSecret(Ljava/lang/String;)V
    .locals 0

    .line 1141
    iput-object p1, p0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->consumerSecret:Ljava/lang/String;

    return-void
.end method

.method public setPassword(Ljava/lang/String;)V
    .locals 0

    .line 1123
    iput-object p1, p0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->password:Ljava/lang/String;

    return-void
.end method

.method public setSupportsTwoFactor(Z)V
    .locals 0

    .line 1150
    iput-boolean p1, p0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->supportsTwoFactor:Z

    const/4 p1, 0x1

    .line 1151
    invoke-virtual {p0, p1}, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->setSupportsTwoFactorIsSet(Z)V

    return-void
.end method

.method public setSupportsTwoFactorIsSet(Z)V
    .locals 2

    .line 1160
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->__isset_vector:[Z

    const/4 v1, 0x0

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setUsername(Ljava/lang/String;)V
    .locals 0

    .line 1114
    iput-object p1, p0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->username:Ljava/lang/String;

    return-void
.end method

.method public validate()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    return-void
.end method

.method public write(Lcom/evernote/thrift/protocol/TProtocol;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1275
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->validate()V

    .line 1277
    sget-object v0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeStructBegin(Lcom/evernote/thrift/protocol/TStruct;)V

    .line 1278
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->username:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1279
    sget-object v0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->USERNAME_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1280
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->username:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 1281
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1283
    :cond_0
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->password:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1284
    sget-object v0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->PASSWORD_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1285
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->password:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 1286
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1288
    :cond_1
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->consumerKey:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1289
    sget-object v0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->CONSUMER_KEY_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1290
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->consumerKey:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 1291
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1293
    :cond_2
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->consumerSecret:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1294
    sget-object v0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->CONSUMER_SECRET_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1295
    iget-object v0, p0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->consumerSecret:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 1296
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1298
    :cond_3
    sget-object v0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->SUPPORTS_TWO_FACTOR_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 1299
    iget-boolean v0, p0, Lcom/evernote/edam/userstore/UserStore$authenticate_args;->supportsTwoFactor:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 1300
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 1301
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldStop()V

    .line 1302
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeStructEnd()V

    return-void
.end method

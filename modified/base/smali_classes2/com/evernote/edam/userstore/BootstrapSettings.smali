.class public Lcom/evernote/edam/userstore/BootstrapSettings;
.super Ljava/lang/Object;
.source "BootstrapSettings.java"

# interfaces
.implements Lcom/evernote/thrift/TBase;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/evernote/thrift/TBase<",
        "Lcom/evernote/edam/userstore/BootstrapSettings;",
        ">;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# static fields
.field private static final ACCOUNT_EMAIL_DOMAIN_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final ENABLE_FACEBOOK_SHARING_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final ENABLE_GIFT_SUBSCRIPTIONS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final ENABLE_LINKED_IN_SHARING_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final ENABLE_PUBLIC_NOTEBOOKS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final ENABLE_SHARED_NOTEBOOKS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final ENABLE_SINGLE_NOTE_SHARING_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final ENABLE_SPONSORED_ACCOUNTS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final ENABLE_SUPPORT_TICKETS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final ENABLE_TWITTER_SHARING_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final MARKETING_URL_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final SERVICE_HOST_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

.field private static final SUPPORT_URL_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final __ENABLEFACEBOOKSHARING_ISSET_ID:I = 0x0

.field private static final __ENABLEGIFTSUBSCRIPTIONS_ISSET_ID:I = 0x1

.field private static final __ENABLELINKEDINSHARING_ISSET_ID:I = 0x7

.field private static final __ENABLEPUBLICNOTEBOOKS_ISSET_ID:I = 0x8

.field private static final __ENABLESHAREDNOTEBOOKS_ISSET_ID:I = 0x3

.field private static final __ENABLESINGLENOTESHARING_ISSET_ID:I = 0x4

.field private static final __ENABLESPONSOREDACCOUNTS_ISSET_ID:I = 0x5

.field private static final __ENABLESUPPORTTICKETS_ISSET_ID:I = 0x2

.field private static final __ENABLETWITTERSHARING_ISSET_ID:I = 0x6


# instance fields
.field private __isset_vector:[Z

.field private accountEmailDomain:Ljava/lang/String;

.field private enableFacebookSharing:Z

.field private enableGiftSubscriptions:Z

.field private enableLinkedInSharing:Z

.field private enablePublicNotebooks:Z

.field private enableSharedNotebooks:Z

.field private enableSingleNoteSharing:Z

.field private enableSponsoredAccounts:Z

.field private enableSupportTickets:Z

.field private enableTwitterSharing:Z

.field private marketingUrl:Ljava/lang/String;

.field private serviceHost:Ljava/lang/String;

.field private supportUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 78
    new-instance v0, Lcom/evernote/thrift/protocol/TStruct;

    const-string v1, "BootstrapSettings"

    invoke-direct {v0, v1}, Lcom/evernote/thrift/protocol/TStruct;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/evernote/edam/userstore/BootstrapSettings;->STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

    .line 80
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "serviceHost"

    const/16 v2, 0xb

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/userstore/BootstrapSettings;->SERVICE_HOST_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 81
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "marketingUrl"

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/userstore/BootstrapSettings;->MARKETING_URL_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 82
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "supportUrl"

    const/4 v4, 0x3

    invoke-direct {v0, v1, v2, v4}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/userstore/BootstrapSettings;->SUPPORT_URL_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 83
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "accountEmailDomain"

    const/4 v4, 0x4

    invoke-direct {v0, v1, v2, v4}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/userstore/BootstrapSettings;->ACCOUNT_EMAIL_DOMAIN_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 84
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "enableFacebookSharing"

    const/4 v4, 0x5

    invoke-direct {v0, v1, v3, v4}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/userstore/BootstrapSettings;->ENABLE_FACEBOOK_SHARING_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 85
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "enableGiftSubscriptions"

    const/4 v4, 0x6

    invoke-direct {v0, v1, v3, v4}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/userstore/BootstrapSettings;->ENABLE_GIFT_SUBSCRIPTIONS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 86
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "enableSupportTickets"

    const/4 v4, 0x7

    invoke-direct {v0, v1, v3, v4}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/userstore/BootstrapSettings;->ENABLE_SUPPORT_TICKETS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 87
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "enableSharedNotebooks"

    const/16 v4, 0x8

    invoke-direct {v0, v1, v3, v4}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/userstore/BootstrapSettings;->ENABLE_SHARED_NOTEBOOKS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 88
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "enableSingleNoteSharing"

    const/16 v4, 0x9

    invoke-direct {v0, v1, v3, v4}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/userstore/BootstrapSettings;->ENABLE_SINGLE_NOTE_SHARING_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 89
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "enableSponsoredAccounts"

    const/16 v4, 0xa

    invoke-direct {v0, v1, v3, v4}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/userstore/BootstrapSettings;->ENABLE_SPONSORED_ACCOUNTS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 90
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "enableTwitterSharing"

    invoke-direct {v0, v1, v3, v2}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/userstore/BootstrapSettings;->ENABLE_TWITTER_SHARING_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 91
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "enableLinkedInSharing"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v3, v2}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/userstore/BootstrapSettings;->ENABLE_LINKED_IN_SHARING_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 92
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "enablePublicNotebooks"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v3, v2}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/userstore/BootstrapSettings;->ENABLE_PUBLIC_NOTEBOOKS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x9

    .line 119
    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->__isset_vector:[Z

    return-void
.end method

.method public constructor <init>(Lcom/evernote/edam/userstore/BootstrapSettings;)V
    .locals 4

    .line 140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x9

    .line 119
    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->__isset_vector:[Z

    .line 141
    iget-object v0, p1, Lcom/evernote/edam/userstore/BootstrapSettings;->__isset_vector:[Z

    iget-object v1, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->__isset_vector:[Z

    array-length v2, v0

    const/4 v3, 0x0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 142
    invoke-virtual {p1}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetServiceHost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p1, Lcom/evernote/edam/userstore/BootstrapSettings;->serviceHost:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->serviceHost:Ljava/lang/String;

    .line 145
    :cond_0
    invoke-virtual {p1}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetMarketingUrl()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 146
    iget-object v0, p1, Lcom/evernote/edam/userstore/BootstrapSettings;->marketingUrl:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->marketingUrl:Ljava/lang/String;

    .line 148
    :cond_1
    invoke-virtual {p1}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetSupportUrl()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 149
    iget-object v0, p1, Lcom/evernote/edam/userstore/BootstrapSettings;->supportUrl:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->supportUrl:Ljava/lang/String;

    .line 151
    :cond_2
    invoke-virtual {p1}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetAccountEmailDomain()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 152
    iget-object v0, p1, Lcom/evernote/edam/userstore/BootstrapSettings;->accountEmailDomain:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->accountEmailDomain:Ljava/lang/String;

    .line 154
    :cond_3
    iget-boolean v0, p1, Lcom/evernote/edam/userstore/BootstrapSettings;->enableFacebookSharing:Z

    iput-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableFacebookSharing:Z

    .line 155
    iget-boolean v0, p1, Lcom/evernote/edam/userstore/BootstrapSettings;->enableGiftSubscriptions:Z

    iput-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableGiftSubscriptions:Z

    .line 156
    iget-boolean v0, p1, Lcom/evernote/edam/userstore/BootstrapSettings;->enableSupportTickets:Z

    iput-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableSupportTickets:Z

    .line 157
    iget-boolean v0, p1, Lcom/evernote/edam/userstore/BootstrapSettings;->enableSharedNotebooks:Z

    iput-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableSharedNotebooks:Z

    .line 158
    iget-boolean v0, p1, Lcom/evernote/edam/userstore/BootstrapSettings;->enableSingleNoteSharing:Z

    iput-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableSingleNoteSharing:Z

    .line 159
    iget-boolean v0, p1, Lcom/evernote/edam/userstore/BootstrapSettings;->enableSponsoredAccounts:Z

    iput-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableSponsoredAccounts:Z

    .line 160
    iget-boolean v0, p1, Lcom/evernote/edam/userstore/BootstrapSettings;->enableTwitterSharing:Z

    iput-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableTwitterSharing:Z

    .line 161
    iget-boolean v0, p1, Lcom/evernote/edam/userstore/BootstrapSettings;->enableLinkedInSharing:Z

    iput-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableLinkedInSharing:Z

    .line 162
    iget-boolean p1, p1, Lcom/evernote/edam/userstore/BootstrapSettings;->enablePublicNotebooks:Z

    iput-boolean p1, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enablePublicNotebooks:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 130
    invoke-direct {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;-><init>()V

    .line 131
    iput-object p1, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->serviceHost:Ljava/lang/String;

    .line 132
    iput-object p2, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->marketingUrl:Ljava/lang/String;

    .line 133
    iput-object p3, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->supportUrl:Ljava/lang/String;

    .line 134
    iput-object p4, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->accountEmailDomain:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    const/4 v0, 0x0

    .line 170
    iput-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->serviceHost:Ljava/lang/String;

    .line 171
    iput-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->marketingUrl:Ljava/lang/String;

    .line 172
    iput-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->supportUrl:Ljava/lang/String;

    .line 173
    iput-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->accountEmailDomain:Ljava/lang/String;

    const/4 v0, 0x0

    .line 174
    invoke-virtual {p0, v0}, Lcom/evernote/edam/userstore/BootstrapSettings;->setEnableFacebookSharingIsSet(Z)V

    .line 175
    iput-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableFacebookSharing:Z

    .line 176
    invoke-virtual {p0, v0}, Lcom/evernote/edam/userstore/BootstrapSettings;->setEnableGiftSubscriptionsIsSet(Z)V

    .line 177
    iput-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableGiftSubscriptions:Z

    .line 178
    invoke-virtual {p0, v0}, Lcom/evernote/edam/userstore/BootstrapSettings;->setEnableSupportTicketsIsSet(Z)V

    .line 179
    iput-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableSupportTickets:Z

    .line 180
    invoke-virtual {p0, v0}, Lcom/evernote/edam/userstore/BootstrapSettings;->setEnableSharedNotebooksIsSet(Z)V

    .line 181
    iput-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableSharedNotebooks:Z

    .line 182
    invoke-virtual {p0, v0}, Lcom/evernote/edam/userstore/BootstrapSettings;->setEnableSingleNoteSharingIsSet(Z)V

    .line 183
    iput-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableSingleNoteSharing:Z

    .line 184
    invoke-virtual {p0, v0}, Lcom/evernote/edam/userstore/BootstrapSettings;->setEnableSponsoredAccountsIsSet(Z)V

    .line 185
    iput-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableSponsoredAccounts:Z

    .line 186
    invoke-virtual {p0, v0}, Lcom/evernote/edam/userstore/BootstrapSettings;->setEnableTwitterSharingIsSet(Z)V

    .line 187
    iput-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableTwitterSharing:Z

    .line 188
    invoke-virtual {p0, v0}, Lcom/evernote/edam/userstore/BootstrapSettings;->setEnableLinkedInSharingIsSet(Z)V

    .line 189
    iput-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableLinkedInSharing:Z

    .line 190
    invoke-virtual {p0, v0}, Lcom/evernote/edam/userstore/BootstrapSettings;->setEnablePublicNotebooksIsSet(Z)V

    .line 191
    iput-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enablePublicNotebooks:Z

    return-void
.end method

.method public compareTo(Lcom/evernote/edam/userstore/BootstrapSettings;)I
    .locals 2

    .line 623
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 624
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    return p1

    .line 630
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetServiceHost()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetServiceHost()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_1

    return v0

    .line 634
    :cond_1
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetServiceHost()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->serviceHost:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/userstore/BootstrapSettings;->serviceHost:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_2

    return v0

    .line 639
    :cond_2
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetMarketingUrl()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetMarketingUrl()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_3

    return v0

    .line 643
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetMarketingUrl()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->marketingUrl:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/userstore/BootstrapSettings;->marketingUrl:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_4

    return v0

    .line 648
    :cond_4
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetSupportUrl()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetSupportUrl()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_5

    return v0

    .line 652
    :cond_5
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetSupportUrl()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->supportUrl:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/userstore/BootstrapSettings;->supportUrl:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_6

    return v0

    .line 657
    :cond_6
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetAccountEmailDomain()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetAccountEmailDomain()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_7

    return v0

    .line 661
    :cond_7
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetAccountEmailDomain()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->accountEmailDomain:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/userstore/BootstrapSettings;->accountEmailDomain:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_8

    return v0

    .line 666
    :cond_8
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableFacebookSharing()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableFacebookSharing()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_9

    return v0

    .line 670
    :cond_9
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableFacebookSharing()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableFacebookSharing:Z

    iget-boolean v1, p1, Lcom/evernote/edam/userstore/BootstrapSettings;->enableFacebookSharing:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_a

    return v0

    .line 675
    :cond_a
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableGiftSubscriptions()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableGiftSubscriptions()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_b

    return v0

    .line 679
    :cond_b
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableGiftSubscriptions()Z

    move-result v0

    if-eqz v0, :cond_c

    iget-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableGiftSubscriptions:Z

    iget-boolean v1, p1, Lcom/evernote/edam/userstore/BootstrapSettings;->enableGiftSubscriptions:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_c

    return v0

    .line 684
    :cond_c
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableSupportTickets()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableSupportTickets()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_d

    return v0

    .line 688
    :cond_d
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableSupportTickets()Z

    move-result v0

    if-eqz v0, :cond_e

    iget-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableSupportTickets:Z

    iget-boolean v1, p1, Lcom/evernote/edam/userstore/BootstrapSettings;->enableSupportTickets:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_e

    return v0

    .line 693
    :cond_e
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableSharedNotebooks()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableSharedNotebooks()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_f

    return v0

    .line 697
    :cond_f
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableSharedNotebooks()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableSharedNotebooks:Z

    iget-boolean v1, p1, Lcom/evernote/edam/userstore/BootstrapSettings;->enableSharedNotebooks:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_10

    return v0

    .line 702
    :cond_10
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableSingleNoteSharing()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableSingleNoteSharing()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_11

    return v0

    .line 706
    :cond_11
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableSingleNoteSharing()Z

    move-result v0

    if-eqz v0, :cond_12

    iget-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableSingleNoteSharing:Z

    iget-boolean v1, p1, Lcom/evernote/edam/userstore/BootstrapSettings;->enableSingleNoteSharing:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_12

    return v0

    .line 711
    :cond_12
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableSponsoredAccounts()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableSponsoredAccounts()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_13

    return v0

    .line 715
    :cond_13
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableSponsoredAccounts()Z

    move-result v0

    if-eqz v0, :cond_14

    iget-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableSponsoredAccounts:Z

    iget-boolean v1, p1, Lcom/evernote/edam/userstore/BootstrapSettings;->enableSponsoredAccounts:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_14

    return v0

    .line 720
    :cond_14
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableTwitterSharing()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableTwitterSharing()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_15

    return v0

    .line 724
    :cond_15
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableTwitterSharing()Z

    move-result v0

    if-eqz v0, :cond_16

    iget-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableTwitterSharing:Z

    iget-boolean v1, p1, Lcom/evernote/edam/userstore/BootstrapSettings;->enableTwitterSharing:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_16

    return v0

    .line 729
    :cond_16
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableLinkedInSharing()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableLinkedInSharing()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_17

    return v0

    .line 733
    :cond_17
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableLinkedInSharing()Z

    move-result v0

    if-eqz v0, :cond_18

    iget-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableLinkedInSharing:Z

    iget-boolean v1, p1, Lcom/evernote/edam/userstore/BootstrapSettings;->enableLinkedInSharing:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_18

    return v0

    .line 738
    :cond_18
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnablePublicNotebooks()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnablePublicNotebooks()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_19

    return v0

    .line 742
    :cond_19
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnablePublicNotebooks()Z

    move-result v0

    if-eqz v0, :cond_1a

    iget-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enablePublicNotebooks:Z

    iget-boolean p1, p1, Lcom/evernote/edam/userstore/BootstrapSettings;->enablePublicNotebooks:Z

    invoke-static {v0, p1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result p1

    if-eqz p1, :cond_1a

    return p1

    :cond_1a
    const/4 p1, 0x0

    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 77
    check-cast p1, Lcom/evernote/edam/userstore/BootstrapSettings;

    invoke-virtual {p0, p1}, Lcom/evernote/edam/userstore/BootstrapSettings;->compareTo(Lcom/evernote/edam/userstore/BootstrapSettings;)I

    move-result p1

    return p1
.end method

.method public deepCopy()Lcom/evernote/edam/userstore/BootstrapSettings;
    .locals 1

    .line 166
    new-instance v0, Lcom/evernote/edam/userstore/BootstrapSettings;

    invoke-direct {v0, p0}, Lcom/evernote/edam/userstore/BootstrapSettings;-><init>(Lcom/evernote/edam/userstore/BootstrapSettings;)V

    return-object v0
.end method

.method public bridge synthetic deepCopy()Lcom/evernote/thrift/TBase;
    .locals 1

    .line 77
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->deepCopy()Lcom/evernote/edam/userstore/BootstrapSettings;

    move-result-object v0

    return-object v0
.end method

.method public equals(Lcom/evernote/edam/userstore/BootstrapSettings;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 497
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetServiceHost()Z

    move-result v1

    .line 498
    invoke-virtual {p1}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetServiceHost()Z

    move-result v2

    if-nez v1, :cond_1

    if-eqz v2, :cond_3

    :cond_1
    if-eqz v1, :cond_34

    if-nez v2, :cond_2

    goto/16 :goto_c

    .line 502
    :cond_2
    iget-object v1, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->serviceHost:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/userstore/BootstrapSettings;->serviceHost:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    return v0

    .line 506
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetMarketingUrl()Z

    move-result v1

    .line 507
    invoke-virtual {p1}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetMarketingUrl()Z

    move-result v2

    if-nez v1, :cond_4

    if-eqz v2, :cond_6

    :cond_4
    if-eqz v1, :cond_33

    if-nez v2, :cond_5

    goto/16 :goto_b

    .line 511
    :cond_5
    iget-object v1, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->marketingUrl:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/userstore/BootstrapSettings;->marketingUrl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    return v0

    .line 515
    :cond_6
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetSupportUrl()Z

    move-result v1

    .line 516
    invoke-virtual {p1}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetSupportUrl()Z

    move-result v2

    if-nez v1, :cond_7

    if-eqz v2, :cond_9

    :cond_7
    if-eqz v1, :cond_32

    if-nez v2, :cond_8

    goto/16 :goto_a

    .line 520
    :cond_8
    iget-object v1, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->supportUrl:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/userstore/BootstrapSettings;->supportUrl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    return v0

    .line 524
    :cond_9
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetAccountEmailDomain()Z

    move-result v1

    .line 525
    invoke-virtual {p1}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetAccountEmailDomain()Z

    move-result v2

    if-nez v1, :cond_a

    if-eqz v2, :cond_c

    :cond_a
    if-eqz v1, :cond_31

    if-nez v2, :cond_b

    goto/16 :goto_9

    .line 529
    :cond_b
    iget-object v1, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->accountEmailDomain:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/userstore/BootstrapSettings;->accountEmailDomain:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    return v0

    .line 533
    :cond_c
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableFacebookSharing()Z

    move-result v1

    .line 534
    invoke-virtual {p1}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableFacebookSharing()Z

    move-result v2

    if-nez v1, :cond_d

    if-eqz v2, :cond_f

    :cond_d
    if-eqz v1, :cond_30

    if-nez v2, :cond_e

    goto/16 :goto_8

    .line 538
    :cond_e
    iget-boolean v1, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableFacebookSharing:Z

    iget-boolean v2, p1, Lcom/evernote/edam/userstore/BootstrapSettings;->enableFacebookSharing:Z

    if-eq v1, v2, :cond_f

    return v0

    .line 542
    :cond_f
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableGiftSubscriptions()Z

    move-result v1

    .line 543
    invoke-virtual {p1}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableGiftSubscriptions()Z

    move-result v2

    if-nez v1, :cond_10

    if-eqz v2, :cond_12

    :cond_10
    if-eqz v1, :cond_2f

    if-nez v2, :cond_11

    goto/16 :goto_7

    .line 547
    :cond_11
    iget-boolean v1, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableGiftSubscriptions:Z

    iget-boolean v2, p1, Lcom/evernote/edam/userstore/BootstrapSettings;->enableGiftSubscriptions:Z

    if-eq v1, v2, :cond_12

    return v0

    .line 551
    :cond_12
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableSupportTickets()Z

    move-result v1

    .line 552
    invoke-virtual {p1}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableSupportTickets()Z

    move-result v2

    if-nez v1, :cond_13

    if-eqz v2, :cond_15

    :cond_13
    if-eqz v1, :cond_2e

    if-nez v2, :cond_14

    goto/16 :goto_6

    .line 556
    :cond_14
    iget-boolean v1, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableSupportTickets:Z

    iget-boolean v2, p1, Lcom/evernote/edam/userstore/BootstrapSettings;->enableSupportTickets:Z

    if-eq v1, v2, :cond_15

    return v0

    .line 560
    :cond_15
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableSharedNotebooks()Z

    move-result v1

    .line 561
    invoke-virtual {p1}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableSharedNotebooks()Z

    move-result v2

    if-nez v1, :cond_16

    if-eqz v2, :cond_18

    :cond_16
    if-eqz v1, :cond_2d

    if-nez v2, :cond_17

    goto/16 :goto_5

    .line 565
    :cond_17
    iget-boolean v1, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableSharedNotebooks:Z

    iget-boolean v2, p1, Lcom/evernote/edam/userstore/BootstrapSettings;->enableSharedNotebooks:Z

    if-eq v1, v2, :cond_18

    return v0

    .line 569
    :cond_18
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableSingleNoteSharing()Z

    move-result v1

    .line 570
    invoke-virtual {p1}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableSingleNoteSharing()Z

    move-result v2

    if-nez v1, :cond_19

    if-eqz v2, :cond_1b

    :cond_19
    if-eqz v1, :cond_2c

    if-nez v2, :cond_1a

    goto/16 :goto_4

    .line 574
    :cond_1a
    iget-boolean v1, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableSingleNoteSharing:Z

    iget-boolean v2, p1, Lcom/evernote/edam/userstore/BootstrapSettings;->enableSingleNoteSharing:Z

    if-eq v1, v2, :cond_1b

    return v0

    .line 578
    :cond_1b
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableSponsoredAccounts()Z

    move-result v1

    .line 579
    invoke-virtual {p1}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableSponsoredAccounts()Z

    move-result v2

    if-nez v1, :cond_1c

    if-eqz v2, :cond_1e

    :cond_1c
    if-eqz v1, :cond_2b

    if-nez v2, :cond_1d

    goto :goto_3

    .line 583
    :cond_1d
    iget-boolean v1, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableSponsoredAccounts:Z

    iget-boolean v2, p1, Lcom/evernote/edam/userstore/BootstrapSettings;->enableSponsoredAccounts:Z

    if-eq v1, v2, :cond_1e

    return v0

    .line 587
    :cond_1e
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableTwitterSharing()Z

    move-result v1

    .line 588
    invoke-virtual {p1}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableTwitterSharing()Z

    move-result v2

    if-nez v1, :cond_1f

    if-eqz v2, :cond_21

    :cond_1f
    if-eqz v1, :cond_2a

    if-nez v2, :cond_20

    goto :goto_2

    .line 592
    :cond_20
    iget-boolean v1, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableTwitterSharing:Z

    iget-boolean v2, p1, Lcom/evernote/edam/userstore/BootstrapSettings;->enableTwitterSharing:Z

    if-eq v1, v2, :cond_21

    return v0

    .line 596
    :cond_21
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableLinkedInSharing()Z

    move-result v1

    .line 597
    invoke-virtual {p1}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableLinkedInSharing()Z

    move-result v2

    if-nez v1, :cond_22

    if-eqz v2, :cond_24

    :cond_22
    if-eqz v1, :cond_29

    if-nez v2, :cond_23

    goto :goto_1

    .line 601
    :cond_23
    iget-boolean v1, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableLinkedInSharing:Z

    iget-boolean v2, p1, Lcom/evernote/edam/userstore/BootstrapSettings;->enableLinkedInSharing:Z

    if-eq v1, v2, :cond_24

    return v0

    .line 605
    :cond_24
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnablePublicNotebooks()Z

    move-result v1

    .line 606
    invoke-virtual {p1}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnablePublicNotebooks()Z

    move-result v2

    if-nez v1, :cond_25

    if-eqz v2, :cond_27

    :cond_25
    if-eqz v1, :cond_28

    if-nez v2, :cond_26

    goto :goto_0

    .line 610
    :cond_26
    iget-boolean v1, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enablePublicNotebooks:Z

    iget-boolean p1, p1, Lcom/evernote/edam/userstore/BootstrapSettings;->enablePublicNotebooks:Z

    if-eq v1, p1, :cond_27

    return v0

    :cond_27
    const/4 p1, 0x1

    return p1

    :cond_28
    :goto_0
    return v0

    :cond_29
    :goto_1
    return v0

    :cond_2a
    :goto_2
    return v0

    :cond_2b
    :goto_3
    return v0

    :cond_2c
    :goto_4
    return v0

    :cond_2d
    :goto_5
    return v0

    :cond_2e
    :goto_6
    return v0

    :cond_2f
    :goto_7
    return v0

    :cond_30
    :goto_8
    return v0

    :cond_31
    :goto_9
    return v0

    :cond_32
    :goto_a
    return v0

    :cond_33
    :goto_b
    return v0

    :cond_34
    :goto_c
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 488
    :cond_0
    instance-of v1, p1, Lcom/evernote/edam/userstore/BootstrapSettings;

    if-eqz v1, :cond_1

    .line 489
    check-cast p1, Lcom/evernote/edam/userstore/BootstrapSettings;

    invoke-virtual {p0, p1}, Lcom/evernote/edam/userstore/BootstrapSettings;->equals(Lcom/evernote/edam/userstore/BootstrapSettings;)Z

    move-result p1

    return p1

    :cond_1
    return v0
.end method

.method public getAccountEmailDomain()Ljava/lang/String;
    .locals 1

    .line 264
    iget-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->accountEmailDomain:Ljava/lang/String;

    return-object v0
.end method

.method public getMarketingUrl()Ljava/lang/String;
    .locals 1

    .line 218
    iget-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->marketingUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getServiceHost()Ljava/lang/String;
    .locals 1

    .line 195
    iget-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->serviceHost:Ljava/lang/String;

    return-object v0
.end method

.method public getSupportUrl()Ljava/lang/String;
    .locals 1

    .line 241
    iget-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->supportUrl:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isEnableFacebookSharing()Z
    .locals 1

    .line 287
    iget-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableFacebookSharing:Z

    return v0
.end method

.method public isEnableGiftSubscriptions()Z
    .locals 1

    .line 309
    iget-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableGiftSubscriptions:Z

    return v0
.end method

.method public isEnableLinkedInSharing()Z
    .locals 1

    .line 441
    iget-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableLinkedInSharing:Z

    return v0
.end method

.method public isEnablePublicNotebooks()Z
    .locals 1

    .line 463
    iget-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enablePublicNotebooks:Z

    return v0
.end method

.method public isEnableSharedNotebooks()Z
    .locals 1

    .line 353
    iget-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableSharedNotebooks:Z

    return v0
.end method

.method public isEnableSingleNoteSharing()Z
    .locals 1

    .line 375
    iget-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableSingleNoteSharing:Z

    return v0
.end method

.method public isEnableSponsoredAccounts()Z
    .locals 1

    .line 397
    iget-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableSponsoredAccounts:Z

    return v0
.end method

.method public isEnableSupportTickets()Z
    .locals 1

    .line 331
    iget-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableSupportTickets:Z

    return v0
.end method

.method public isEnableTwitterSharing()Z
    .locals 1

    .line 419
    iget-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableTwitterSharing:Z

    return v0
.end method

.method public isSetAccountEmailDomain()Z
    .locals 1

    .line 277
    iget-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->accountEmailDomain:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetEnableFacebookSharing()Z
    .locals 2

    .line 301
    iget-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->__isset_vector:[Z

    const/4 v1, 0x0

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetEnableGiftSubscriptions()Z
    .locals 2

    .line 323
    iget-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->__isset_vector:[Z

    const/4 v1, 0x1

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetEnableLinkedInSharing()Z
    .locals 2

    .line 455
    iget-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->__isset_vector:[Z

    const/4 v1, 0x7

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetEnablePublicNotebooks()Z
    .locals 2

    .line 477
    iget-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->__isset_vector:[Z

    const/16 v1, 0x8

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetEnableSharedNotebooks()Z
    .locals 2

    .line 367
    iget-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->__isset_vector:[Z

    const/4 v1, 0x3

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetEnableSingleNoteSharing()Z
    .locals 2

    .line 389
    iget-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->__isset_vector:[Z

    const/4 v1, 0x4

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetEnableSponsoredAccounts()Z
    .locals 2

    .line 411
    iget-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->__isset_vector:[Z

    const/4 v1, 0x5

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetEnableSupportTickets()Z
    .locals 2

    .line 345
    iget-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->__isset_vector:[Z

    const/4 v1, 0x2

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetEnableTwitterSharing()Z
    .locals 2

    .line 433
    iget-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->__isset_vector:[Z

    const/4 v1, 0x6

    aget-boolean v0, v0, v1

    return v0
.end method

.method public isSetMarketingUrl()Z
    .locals 1

    .line 231
    iget-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->marketingUrl:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetServiceHost()Z
    .locals 1

    .line 208
    iget-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->serviceHost:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetSupportUrl()Z
    .locals 1

    .line 254
    iget-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->supportUrl:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public read(Lcom/evernote/thrift/protocol/TProtocol;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 752
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readStructBegin()Lcom/evernote/thrift/protocol/TStruct;

    .line 755
    :goto_0
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readFieldBegin()Lcom/evernote/thrift/protocol/TField;

    move-result-object v0

    .line 756
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-nez v1, :cond_0

    .line 865
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readStructEnd()V

    .line 866
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->validate()V

    return-void

    .line 759
    :cond_0
    iget-short v1, v0, Lcom/evernote/thrift/protocol/TField;->id:S

    const/16 v2, 0xb

    const/4 v3, 0x1

    const/4 v4, 0x2

    packed-switch v1, :pswitch_data_0

    .line 861
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 853
    :pswitch_0
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_1

    .line 854
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enablePublicNotebooks:Z

    .line 855
    invoke-virtual {p0, v3}, Lcom/evernote/edam/userstore/BootstrapSettings;->setEnablePublicNotebooksIsSet(Z)V

    goto/16 :goto_1

    .line 857
    :cond_1
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 845
    :pswitch_1
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_2

    .line 846
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableLinkedInSharing:Z

    .line 847
    invoke-virtual {p0, v3}, Lcom/evernote/edam/userstore/BootstrapSettings;->setEnableLinkedInSharingIsSet(Z)V

    goto/16 :goto_1

    .line 849
    :cond_2
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 837
    :pswitch_2
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_3

    .line 838
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableTwitterSharing:Z

    .line 839
    invoke-virtual {p0, v3}, Lcom/evernote/edam/userstore/BootstrapSettings;->setEnableTwitterSharingIsSet(Z)V

    goto/16 :goto_1

    .line 841
    :cond_3
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 829
    :pswitch_3
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_4

    .line 830
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableSponsoredAccounts:Z

    .line 831
    invoke-virtual {p0, v3}, Lcom/evernote/edam/userstore/BootstrapSettings;->setEnableSponsoredAccountsIsSet(Z)V

    goto/16 :goto_1

    .line 833
    :cond_4
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 821
    :pswitch_4
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_5

    .line 822
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableSingleNoteSharing:Z

    .line 823
    invoke-virtual {p0, v3}, Lcom/evernote/edam/userstore/BootstrapSettings;->setEnableSingleNoteSharingIsSet(Z)V

    goto/16 :goto_1

    .line 825
    :cond_5
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 813
    :pswitch_5
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_6

    .line 814
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableSharedNotebooks:Z

    .line 815
    invoke-virtual {p0, v3}, Lcom/evernote/edam/userstore/BootstrapSettings;->setEnableSharedNotebooksIsSet(Z)V

    goto/16 :goto_1

    .line 817
    :cond_6
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 805
    :pswitch_6
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_7

    .line 806
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableSupportTickets:Z

    .line 807
    invoke-virtual {p0, v3}, Lcom/evernote/edam/userstore/BootstrapSettings;->setEnableSupportTicketsIsSet(Z)V

    goto/16 :goto_1

    .line 809
    :cond_7
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto/16 :goto_1

    .line 797
    :pswitch_7
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_8

    .line 798
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableGiftSubscriptions:Z

    .line 799
    invoke-virtual {p0, v3}, Lcom/evernote/edam/userstore/BootstrapSettings;->setEnableGiftSubscriptionsIsSet(Z)V

    goto :goto_1

    .line 801
    :cond_8
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 789
    :pswitch_8
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v4, :cond_9

    .line 790
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableFacebookSharing:Z

    .line 791
    invoke-virtual {p0, v3}, Lcom/evernote/edam/userstore/BootstrapSettings;->setEnableFacebookSharingIsSet(Z)V

    goto :goto_1

    .line 793
    :cond_9
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 782
    :pswitch_9
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v2, :cond_a

    .line 783
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->accountEmailDomain:Ljava/lang/String;

    goto :goto_1

    .line 785
    :cond_a
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 775
    :pswitch_a
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v2, :cond_b

    .line 776
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->supportUrl:Ljava/lang/String;

    goto :goto_1

    .line 778
    :cond_b
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 768
    :pswitch_b
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v2, :cond_c

    .line 769
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->marketingUrl:Ljava/lang/String;

    goto :goto_1

    .line 771
    :cond_c
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 761
    :pswitch_c
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v2, :cond_d

    .line 762
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->serviceHost:Ljava/lang/String;

    goto :goto_1

    .line 764
    :cond_d
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    .line 863
    :goto_1
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readFieldEnd()V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setAccountEmailDomain(Ljava/lang/String;)V
    .locals 0

    .line 268
    iput-object p1, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->accountEmailDomain:Ljava/lang/String;

    return-void
.end method

.method public setAccountEmailDomainIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 282
    iput-object p1, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->accountEmailDomain:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setEnableFacebookSharing(Z)V
    .locals 0

    .line 291
    iput-boolean p1, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableFacebookSharing:Z

    const/4 p1, 0x1

    .line 292
    invoke-virtual {p0, p1}, Lcom/evernote/edam/userstore/BootstrapSettings;->setEnableFacebookSharingIsSet(Z)V

    return-void
.end method

.method public setEnableFacebookSharingIsSet(Z)V
    .locals 2

    .line 305
    iget-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->__isset_vector:[Z

    const/4 v1, 0x0

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setEnableGiftSubscriptions(Z)V
    .locals 0

    .line 313
    iput-boolean p1, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableGiftSubscriptions:Z

    const/4 p1, 0x1

    .line 314
    invoke-virtual {p0, p1}, Lcom/evernote/edam/userstore/BootstrapSettings;->setEnableGiftSubscriptionsIsSet(Z)V

    return-void
.end method

.method public setEnableGiftSubscriptionsIsSet(Z)V
    .locals 2

    .line 327
    iget-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->__isset_vector:[Z

    const/4 v1, 0x1

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setEnableLinkedInSharing(Z)V
    .locals 0

    .line 445
    iput-boolean p1, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableLinkedInSharing:Z

    const/4 p1, 0x1

    .line 446
    invoke-virtual {p0, p1}, Lcom/evernote/edam/userstore/BootstrapSettings;->setEnableLinkedInSharingIsSet(Z)V

    return-void
.end method

.method public setEnableLinkedInSharingIsSet(Z)V
    .locals 2

    .line 459
    iget-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->__isset_vector:[Z

    const/4 v1, 0x7

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setEnablePublicNotebooks(Z)V
    .locals 0

    .line 467
    iput-boolean p1, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enablePublicNotebooks:Z

    const/4 p1, 0x1

    .line 468
    invoke-virtual {p0, p1}, Lcom/evernote/edam/userstore/BootstrapSettings;->setEnablePublicNotebooksIsSet(Z)V

    return-void
.end method

.method public setEnablePublicNotebooksIsSet(Z)V
    .locals 2

    .line 481
    iget-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->__isset_vector:[Z

    const/16 v1, 0x8

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setEnableSharedNotebooks(Z)V
    .locals 0

    .line 357
    iput-boolean p1, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableSharedNotebooks:Z

    const/4 p1, 0x1

    .line 358
    invoke-virtual {p0, p1}, Lcom/evernote/edam/userstore/BootstrapSettings;->setEnableSharedNotebooksIsSet(Z)V

    return-void
.end method

.method public setEnableSharedNotebooksIsSet(Z)V
    .locals 2

    .line 371
    iget-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->__isset_vector:[Z

    const/4 v1, 0x3

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setEnableSingleNoteSharing(Z)V
    .locals 0

    .line 379
    iput-boolean p1, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableSingleNoteSharing:Z

    const/4 p1, 0x1

    .line 380
    invoke-virtual {p0, p1}, Lcom/evernote/edam/userstore/BootstrapSettings;->setEnableSingleNoteSharingIsSet(Z)V

    return-void
.end method

.method public setEnableSingleNoteSharingIsSet(Z)V
    .locals 2

    .line 393
    iget-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->__isset_vector:[Z

    const/4 v1, 0x4

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setEnableSponsoredAccounts(Z)V
    .locals 0

    .line 401
    iput-boolean p1, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableSponsoredAccounts:Z

    const/4 p1, 0x1

    .line 402
    invoke-virtual {p0, p1}, Lcom/evernote/edam/userstore/BootstrapSettings;->setEnableSponsoredAccountsIsSet(Z)V

    return-void
.end method

.method public setEnableSponsoredAccountsIsSet(Z)V
    .locals 2

    .line 415
    iget-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->__isset_vector:[Z

    const/4 v1, 0x5

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setEnableSupportTickets(Z)V
    .locals 0

    .line 335
    iput-boolean p1, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableSupportTickets:Z

    const/4 p1, 0x1

    .line 336
    invoke-virtual {p0, p1}, Lcom/evernote/edam/userstore/BootstrapSettings;->setEnableSupportTicketsIsSet(Z)V

    return-void
.end method

.method public setEnableSupportTicketsIsSet(Z)V
    .locals 2

    .line 349
    iget-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->__isset_vector:[Z

    const/4 v1, 0x2

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setEnableTwitterSharing(Z)V
    .locals 0

    .line 423
    iput-boolean p1, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableTwitterSharing:Z

    const/4 p1, 0x1

    .line 424
    invoke-virtual {p0, p1}, Lcom/evernote/edam/userstore/BootstrapSettings;->setEnableTwitterSharingIsSet(Z)V

    return-void
.end method

.method public setEnableTwitterSharingIsSet(Z)V
    .locals 2

    .line 437
    iget-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->__isset_vector:[Z

    const/4 v1, 0x6

    aput-boolean p1, v0, v1

    return-void
.end method

.method public setMarketingUrl(Ljava/lang/String;)V
    .locals 0

    .line 222
    iput-object p1, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->marketingUrl:Ljava/lang/String;

    return-void
.end method

.method public setMarketingUrlIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 236
    iput-object p1, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->marketingUrl:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setServiceHost(Ljava/lang/String;)V
    .locals 0

    .line 199
    iput-object p1, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->serviceHost:Ljava/lang/String;

    return-void
.end method

.method public setServiceHostIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 213
    iput-object p1, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->serviceHost:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setSupportUrl(Ljava/lang/String;)V
    .locals 0

    .line 245
    iput-object p1, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->supportUrl:Ljava/lang/String;

    return-void
.end method

.method public setSupportUrlIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 259
    iput-object p1, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->supportUrl:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 944
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "BootstrapSettings("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "serviceHost:"

    .line 947
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 948
    iget-object v1, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->serviceHost:Ljava/lang/String;

    if-nez v1, :cond_0

    const-string v1, "null"

    .line 949
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 951
    :cond_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    const-string v1, ", "

    .line 954
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "marketingUrl:"

    .line 955
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 956
    iget-object v1, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->marketingUrl:Ljava/lang/String;

    if-nez v1, :cond_1

    const-string v1, "null"

    .line 957
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 959
    :cond_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_1
    const-string v1, ", "

    .line 962
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "supportUrl:"

    .line 963
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 964
    iget-object v1, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->supportUrl:Ljava/lang/String;

    if-nez v1, :cond_2

    const-string v1, "null"

    .line 965
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 967
    :cond_2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_2
    const-string v1, ", "

    .line 970
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "accountEmailDomain:"

    .line 971
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 972
    iget-object v1, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->accountEmailDomain:Ljava/lang/String;

    if-nez v1, :cond_3

    const-string v1, "null"

    .line 973
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 975
    :cond_3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 978
    :goto_3
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableFacebookSharing()Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, ", "

    .line 979
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "enableFacebookSharing:"

    .line 980
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 981
    iget-boolean v1, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableFacebookSharing:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 984
    :cond_4
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableGiftSubscriptions()Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, ", "

    .line 985
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "enableGiftSubscriptions:"

    .line 986
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 987
    iget-boolean v1, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableGiftSubscriptions:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 990
    :cond_5
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableSupportTickets()Z

    move-result v1

    if-eqz v1, :cond_6

    const-string v1, ", "

    .line 991
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "enableSupportTickets:"

    .line 992
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 993
    iget-boolean v1, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableSupportTickets:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 996
    :cond_6
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableSharedNotebooks()Z

    move-result v1

    if-eqz v1, :cond_7

    const-string v1, ", "

    .line 997
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "enableSharedNotebooks:"

    .line 998
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 999
    iget-boolean v1, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableSharedNotebooks:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 1002
    :cond_7
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableSingleNoteSharing()Z

    move-result v1

    if-eqz v1, :cond_8

    const-string v1, ", "

    .line 1003
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "enableSingleNoteSharing:"

    .line 1004
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1005
    iget-boolean v1, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableSingleNoteSharing:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 1008
    :cond_8
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableSponsoredAccounts()Z

    move-result v1

    if-eqz v1, :cond_9

    const-string v1, ", "

    .line 1009
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "enableSponsoredAccounts:"

    .line 1010
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1011
    iget-boolean v1, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableSponsoredAccounts:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 1014
    :cond_9
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableTwitterSharing()Z

    move-result v1

    if-eqz v1, :cond_a

    const-string v1, ", "

    .line 1015
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "enableTwitterSharing:"

    .line 1016
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1017
    iget-boolean v1, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableTwitterSharing:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 1020
    :cond_a
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableLinkedInSharing()Z

    move-result v1

    if-eqz v1, :cond_b

    const-string v1, ", "

    .line 1021
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "enableLinkedInSharing:"

    .line 1022
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1023
    iget-boolean v1, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableLinkedInSharing:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 1026
    :cond_b
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnablePublicNotebooks()Z

    move-result v1

    if-eqz v1, :cond_c

    const-string v1, ", "

    .line 1027
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "enablePublicNotebooks:"

    .line 1028
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1029
    iget-boolean v1, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enablePublicNotebooks:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    :cond_c
    const-string v1, ")"

    .line 1032
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1033
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public unsetAccountEmailDomain()V
    .locals 1

    const/4 v0, 0x0

    .line 272
    iput-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->accountEmailDomain:Ljava/lang/String;

    return-void
.end method

.method public unsetEnableFacebookSharing()V
    .locals 2

    .line 296
    iget-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->__isset_vector:[Z

    const/4 v1, 0x0

    aput-boolean v1, v0, v1

    return-void
.end method

.method public unsetEnableGiftSubscriptions()V
    .locals 3

    .line 318
    iget-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->__isset_vector:[Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetEnableLinkedInSharing()V
    .locals 3

    .line 450
    iget-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->__isset_vector:[Z

    const/4 v1, 0x7

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetEnablePublicNotebooks()V
    .locals 3

    .line 472
    iget-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->__isset_vector:[Z

    const/16 v1, 0x8

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetEnableSharedNotebooks()V
    .locals 3

    .line 362
    iget-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->__isset_vector:[Z

    const/4 v1, 0x3

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetEnableSingleNoteSharing()V
    .locals 3

    .line 384
    iget-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->__isset_vector:[Z

    const/4 v1, 0x4

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetEnableSponsoredAccounts()V
    .locals 3

    .line 406
    iget-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->__isset_vector:[Z

    const/4 v1, 0x5

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetEnableSupportTickets()V
    .locals 3

    .line 340
    iget-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->__isset_vector:[Z

    const/4 v1, 0x2

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetEnableTwitterSharing()V
    .locals 3

    .line 428
    iget-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->__isset_vector:[Z

    const/4 v1, 0x6

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public unsetMarketingUrl()V
    .locals 1

    const/4 v0, 0x0

    .line 226
    iput-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->marketingUrl:Ljava/lang/String;

    return-void
.end method

.method public unsetServiceHost()V
    .locals 1

    const/4 v0, 0x0

    .line 203
    iput-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->serviceHost:Ljava/lang/String;

    return-void
.end method

.method public unsetSupportUrl()V
    .locals 1

    const/4 v0, 0x0

    .line 249
    iput-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->supportUrl:Ljava/lang/String;

    return-void
.end method

.method public validate()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1038
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetServiceHost()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1042
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetMarketingUrl()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1046
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetSupportUrl()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1050
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetAccountEmailDomain()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 1051
    :cond_0
    new-instance v0, Lcom/evernote/thrift/protocol/TProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Required field \'accountEmailDomain\' is unset! Struct:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/evernote/thrift/protocol/TProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1047
    :cond_1
    new-instance v0, Lcom/evernote/thrift/protocol/TProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Required field \'supportUrl\' is unset! Struct:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/evernote/thrift/protocol/TProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1043
    :cond_2
    new-instance v0, Lcom/evernote/thrift/protocol/TProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Required field \'marketingUrl\' is unset! Struct:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/evernote/thrift/protocol/TProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1039
    :cond_3
    new-instance v0, Lcom/evernote/thrift/protocol/TProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Required field \'serviceHost\' is unset! Struct:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/evernote/thrift/protocol/TProtocolException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public write(Lcom/evernote/thrift/protocol/TProtocol;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 870
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->validate()V

    .line 872
    sget-object v0, Lcom/evernote/edam/userstore/BootstrapSettings;->STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeStructBegin(Lcom/evernote/thrift/protocol/TStruct;)V

    .line 873
    iget-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->serviceHost:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 874
    sget-object v0, Lcom/evernote/edam/userstore/BootstrapSettings;->SERVICE_HOST_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 875
    iget-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->serviceHost:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 876
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 878
    :cond_0
    iget-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->marketingUrl:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 879
    sget-object v0, Lcom/evernote/edam/userstore/BootstrapSettings;->MARKETING_URL_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 880
    iget-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->marketingUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 881
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 883
    :cond_1
    iget-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->supportUrl:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 884
    sget-object v0, Lcom/evernote/edam/userstore/BootstrapSettings;->SUPPORT_URL_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 885
    iget-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->supportUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 886
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 888
    :cond_2
    iget-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->accountEmailDomain:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 889
    sget-object v0, Lcom/evernote/edam/userstore/BootstrapSettings;->ACCOUNT_EMAIL_DOMAIN_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 890
    iget-object v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->accountEmailDomain:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 891
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 893
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableFacebookSharing()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 894
    sget-object v0, Lcom/evernote/edam/userstore/BootstrapSettings;->ENABLE_FACEBOOK_SHARING_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 895
    iget-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableFacebookSharing:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 896
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 898
    :cond_4
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableGiftSubscriptions()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 899
    sget-object v0, Lcom/evernote/edam/userstore/BootstrapSettings;->ENABLE_GIFT_SUBSCRIPTIONS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 900
    iget-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableGiftSubscriptions:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 901
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 903
    :cond_5
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableSupportTickets()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 904
    sget-object v0, Lcom/evernote/edam/userstore/BootstrapSettings;->ENABLE_SUPPORT_TICKETS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 905
    iget-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableSupportTickets:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 906
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 908
    :cond_6
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableSharedNotebooks()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 909
    sget-object v0, Lcom/evernote/edam/userstore/BootstrapSettings;->ENABLE_SHARED_NOTEBOOKS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 910
    iget-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableSharedNotebooks:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 911
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 913
    :cond_7
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableSingleNoteSharing()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 914
    sget-object v0, Lcom/evernote/edam/userstore/BootstrapSettings;->ENABLE_SINGLE_NOTE_SHARING_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 915
    iget-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableSingleNoteSharing:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 916
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 918
    :cond_8
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableSponsoredAccounts()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 919
    sget-object v0, Lcom/evernote/edam/userstore/BootstrapSettings;->ENABLE_SPONSORED_ACCOUNTS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 920
    iget-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableSponsoredAccounts:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 921
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 923
    :cond_9
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableTwitterSharing()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 924
    sget-object v0, Lcom/evernote/edam/userstore/BootstrapSettings;->ENABLE_TWITTER_SHARING_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 925
    iget-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableTwitterSharing:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 926
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 928
    :cond_a
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnableLinkedInSharing()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 929
    sget-object v0, Lcom/evernote/edam/userstore/BootstrapSettings;->ENABLE_LINKED_IN_SHARING_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 930
    iget-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enableLinkedInSharing:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 931
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 933
    :cond_b
    invoke-virtual {p0}, Lcom/evernote/edam/userstore/BootstrapSettings;->isSetEnablePublicNotebooks()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 934
    sget-object v0, Lcom/evernote/edam/userstore/BootstrapSettings;->ENABLE_PUBLIC_NOTEBOOKS_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 935
    iget-boolean v0, p0, Lcom/evernote/edam/userstore/BootstrapSettings;->enablePublicNotebooks:Z

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 936
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 938
    :cond_c
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldStop()V

    .line 939
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeStructEnd()V

    return-void
.end method

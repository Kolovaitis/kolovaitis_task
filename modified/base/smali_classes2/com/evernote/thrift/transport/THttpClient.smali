.class public Lcom/evernote/thrift/transport/THttpClient;
.super Lcom/evernote/thrift/transport/TTransport;
.source "THttpClient.java"


# instance fields
.field private connectTimeout_:I

.field private customHeaders_:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private inputStream_:Ljava/io/InputStream;

.field private readTimeout_:I

.field private final requestBuffer_:Ljava/io/ByteArrayOutputStream;

.field private url_:Ljava/net/URL;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/transport/TTransportException;
        }
    .end annotation

    .line 51
    invoke-direct {p0}, Lcom/evernote/thrift/transport/TTransport;-><init>()V

    const/4 v0, 0x0

    .line 38
    iput-object v0, p0, Lcom/evernote/thrift/transport/THttpClient;->url_:Ljava/net/URL;

    .line 40
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v1, p0, Lcom/evernote/thrift/transport/THttpClient;->requestBuffer_:Ljava/io/ByteArrayOutputStream;

    .line 43
    iput-object v0, p0, Lcom/evernote/thrift/transport/THttpClient;->inputStream_:Ljava/io/InputStream;

    const/4 v1, 0x0

    .line 45
    iput v1, p0, Lcom/evernote/thrift/transport/THttpClient;->connectTimeout_:I

    .line 47
    iput v1, p0, Lcom/evernote/thrift/transport/THttpClient;->readTimeout_:I

    .line 49
    iput-object v0, p0, Lcom/evernote/thrift/transport/THttpClient;->customHeaders_:Ljava/util/Map;

    .line 53
    :try_start_0
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/evernote/thrift/transport/THttpClient;->url_:Ljava/net/URL;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    .line 55
    new-instance v0, Lcom/evernote/thrift/transport/TTransportException;

    invoke-direct {v0, p1}, Lcom/evernote/thrift/transport/TTransportException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method


# virtual methods
.method public close()V
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/evernote/thrift/transport/THttpClient;->inputStream_:Ljava/io/InputStream;

    if-eqz v0, :cond_0

    .line 83
    :try_start_0
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v0, 0x0

    .line 87
    iput-object v0, p0, Lcom/evernote/thrift/transport/THttpClient;->inputStream_:Ljava/io/InputStream;

    :cond_0
    return-void
.end method

.method public flush()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/transport/TTransportException;
        }
    .end annotation

    .line 116
    iget-object v0, p0, Lcom/evernote/thrift/transport/THttpClient;->requestBuffer_:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 117
    iget-object v1, p0, Lcom/evernote/thrift/transport/THttpClient;->requestBuffer_:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 121
    :try_start_0
    iget-object v1, p0, Lcom/evernote/thrift/transport/THttpClient;->url_:Ljava/net/URL;

    invoke-virtual {v1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v1

    check-cast v1, Ljava/net/HttpURLConnection;

    .line 124
    iget v2, p0, Lcom/evernote/thrift/transport/THttpClient;->connectTimeout_:I

    if-lez v2, :cond_0

    .line 125
    iget v2, p0, Lcom/evernote/thrift/transport/THttpClient;->connectTimeout_:I

    invoke-virtual {v1, v2}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 127
    :cond_0
    iget v2, p0, Lcom/evernote/thrift/transport/THttpClient;->readTimeout_:I

    if-lez v2, :cond_1

    .line 128
    iget v2, p0, Lcom/evernote/thrift/transport/THttpClient;->readTimeout_:I

    invoke-virtual {v1, v2}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    :cond_1
    const-string v2, "POST"

    .line 132
    invoke-virtual {v1, v2}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    const-string v2, "Content-Type"

    const-string v3, "application/x-thrift"

    .line 133
    invoke-virtual {v1, v2, v3}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "Accept"

    const-string v3, "application/x-thrift"

    .line 134
    invoke-virtual {v1, v2, v3}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "User-Agent"

    const-string v3, "Java/THttpClient"

    .line 135
    invoke-virtual {v1, v2, v3}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    iget-object v2, p0, Lcom/evernote/thrift/transport/THttpClient;->customHeaders_:Ljava/util/Map;

    if-eqz v2, :cond_2

    .line 137
    iget-object v2, p0, Lcom/evernote/thrift/transport/THttpClient;->customHeaders_:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 138
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v1, v4, v3}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const/4 v2, 0x1

    .line 141
    invoke-virtual {v1, v2}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 142
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->connect()V

    .line 143
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/io/OutputStream;->write([B)V

    .line 145
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v0

    const/16 v2, 0xc8

    if-ne v0, v2, :cond_3

    .line 151
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/thrift/transport/THttpClient;->inputStream_:Ljava/io/InputStream;

    return-void

    .line 147
    :cond_3
    new-instance v1, Lcom/evernote/thrift/transport/TTransportException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "HTTP Response code: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/evernote/thrift/transport/TTransportException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    .line 154
    new-instance v1, Lcom/evernote/thrift/transport/TTransportException;

    invoke-direct {v1, v0}, Lcom/evernote/thrift/transport/TTransportException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public isOpen()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public open()V
    .locals 0

    return-void
.end method

.method public read([BII)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/transport/TTransportException;
        }
    .end annotation

    .line 96
    iget-object v0, p0, Lcom/evernote/thrift/transport/THttpClient;->inputStream_:Ljava/io/InputStream;

    if-eqz v0, :cond_1

    .line 100
    :try_start_0
    invoke-virtual {v0, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I

    move-result p1

    const/4 p2, -0x1

    if-eq p1, p2, :cond_0

    return p1

    .line 102
    :cond_0
    new-instance p1, Lcom/evernote/thrift/transport/TTransportException;

    const-string p2, "No more data available."

    invoke-direct {p1, p2}, Lcom/evernote/thrift/transport/TTransportException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    .line 106
    new-instance p2, Lcom/evernote/thrift/transport/TTransportException;

    invoke-direct {p2, p1}, Lcom/evernote/thrift/transport/TTransportException;-><init>(Ljava/lang/Throwable;)V

    throw p2

    .line 97
    :cond_1
    new-instance p1, Lcom/evernote/thrift/transport/TTransportException;

    const-string p2, "Response buffer is empty, no request."

    invoke-direct {p1, p2}, Lcom/evernote/thrift/transport/TTransportException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setConnectTimeout(I)V
    .locals 0

    .line 60
    iput p1, p0, Lcom/evernote/thrift/transport/THttpClient;->connectTimeout_:I

    return-void
.end method

.method public setCustomHeader(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/evernote/thrift/transport/THttpClient;->customHeaders_:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 73
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/evernote/thrift/transport/THttpClient;->customHeaders_:Ljava/util/Map;

    .line 75
    :cond_0
    iget-object v0, p0, Lcom/evernote/thrift/transport/THttpClient;->customHeaders_:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public setCustomHeaders(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 68
    iput-object p1, p0, Lcom/evernote/thrift/transport/THttpClient;->customHeaders_:Ljava/util/Map;

    return-void
.end method

.method public setReadTimeout(I)V
    .locals 0

    .line 64
    iput p1, p0, Lcom/evernote/thrift/transport/THttpClient;->readTimeout_:I

    return-void
.end method

.method public write([BII)V
    .locals 1

    .line 111
    iget-object v0, p0, Lcom/evernote/thrift/transport/THttpClient;->requestBuffer_:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    return-void
.end method

.class public Lcom/evernote/thrift/protocol/TProtocolUtil;
.super Ljava/lang/Object;
.source "TProtocolUtil.java"


# static fields
.field private static maxSkipDepth:I = 0x7fffffff


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static setMaxSkipDepth(I)V
    .locals 0

    .line 49
    sput p0, Lcom/evernote/thrift/protocol/TProtocolUtil;->maxSkipDepth:I

    return-void
.end method

.method public static skip(Lcom/evernote/thrift/protocol/TProtocol;B)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 60
    sget v0, Lcom/evernote/thrift/protocol/TProtocolUtil;->maxSkipDepth:I

    invoke-static {p0, p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;BI)V

    return-void
.end method

.method public static skip(Lcom/evernote/thrift/protocol/TProtocol;BI)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    if-lez p2, :cond_4

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    goto/16 :goto_4

    .line 147
    :pswitch_1
    invoke-virtual {p0}, Lcom/evernote/thrift/protocol/TProtocol;->readListBegin()Lcom/evernote/thrift/protocol/TList;

    move-result-object p1

    .line 148
    :goto_0
    iget v1, p1, Lcom/evernote/thrift/protocol/TList;->size:I

    if-ge v0, v1, :cond_0

    .line 149
    iget-byte v1, p1, Lcom/evernote/thrift/protocol/TList;->elemType:B

    add-int/lit8 v2, p2, -0x1

    invoke-static {p0, v1, v2}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;BI)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 151
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/thrift/protocol/TProtocol;->readListEnd()V

    goto/16 :goto_4

    .line 138
    :pswitch_2
    invoke-virtual {p0}, Lcom/evernote/thrift/protocol/TProtocol;->readSetBegin()Lcom/evernote/thrift/protocol/TSet;

    move-result-object p1

    .line 139
    :goto_1
    iget v1, p1, Lcom/evernote/thrift/protocol/TSet;->size:I

    if-ge v0, v1, :cond_1

    .line 140
    iget-byte v1, p1, Lcom/evernote/thrift/protocol/TSet;->elemType:B

    add-int/lit8 v2, p2, -0x1

    invoke-static {p0, v1, v2}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;BI)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 142
    :cond_1
    invoke-virtual {p0}, Lcom/evernote/thrift/protocol/TProtocol;->readSetEnd()V

    goto :goto_4

    .line 128
    :pswitch_3
    invoke-virtual {p0}, Lcom/evernote/thrift/protocol/TProtocol;->readMapBegin()Lcom/evernote/thrift/protocol/TMap;

    move-result-object p1

    .line 129
    :goto_2
    iget v1, p1, Lcom/evernote/thrift/protocol/TMap;->size:I

    if-ge v0, v1, :cond_2

    .line 130
    iget-byte v1, p1, Lcom/evernote/thrift/protocol/TMap;->keyType:B

    add-int/lit8 v2, p2, -0x1

    invoke-static {p0, v1, v2}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;BI)V

    .line 131
    iget-byte v1, p1, Lcom/evernote/thrift/protocol/TMap;->valueType:B

    invoke-static {p0, v1, v2}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;BI)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 133
    :cond_2
    invoke-virtual {p0}, Lcom/evernote/thrift/protocol/TProtocol;->readMapEnd()V

    goto :goto_4

    .line 114
    :pswitch_4
    invoke-virtual {p0}, Lcom/evernote/thrift/protocol/TProtocol;->readStructBegin()Lcom/evernote/thrift/protocol/TStruct;

    .line 116
    :goto_3
    invoke-virtual {p0}, Lcom/evernote/thrift/protocol/TProtocol;->readFieldBegin()Lcom/evernote/thrift/protocol/TField;

    move-result-object p1

    .line 117
    iget-byte v0, p1, Lcom/evernote/thrift/protocol/TField;->type:B

    if-nez v0, :cond_3

    .line 123
    invoke-virtual {p0}, Lcom/evernote/thrift/protocol/TProtocol;->readStructEnd()V

    goto :goto_4

    .line 120
    :cond_3
    iget-byte p1, p1, Lcom/evernote/thrift/protocol/TField;->type:B

    add-int/lit8 v0, p2, -0x1

    :try_start_0
    invoke-static {p0, p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;BI)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 121
    invoke-virtual {p0}, Lcom/evernote/thrift/protocol/TProtocol;->readFieldEnd()V

    goto :goto_3

    .line 109
    :pswitch_5
    invoke-virtual {p0}, Lcom/evernote/thrift/protocol/TProtocol;->readBinary()Ljava/nio/ByteBuffer;

    goto :goto_4

    .line 99
    :pswitch_6
    invoke-virtual {p0}, Lcom/evernote/thrift/protocol/TProtocol;->readI64()J

    goto :goto_4

    .line 94
    :pswitch_7
    invoke-virtual {p0}, Lcom/evernote/thrift/protocol/TProtocol;->readI32()I

    goto :goto_4

    .line 89
    :pswitch_8
    invoke-virtual {p0}, Lcom/evernote/thrift/protocol/TProtocol;->readI16()S

    goto :goto_4

    .line 104
    :pswitch_9
    invoke-virtual {p0}, Lcom/evernote/thrift/protocol/TProtocol;->readDouble()D

    goto :goto_4

    .line 84
    :pswitch_a
    invoke-virtual {p0}, Lcom/evernote/thrift/protocol/TProtocol;->readByte()B

    goto :goto_4

    .line 79
    :pswitch_b
    invoke-virtual {p0}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    :goto_4
    return-void

    .line 74
    :cond_4
    new-instance p0, Lcom/evernote/thrift/TException;

    const-string p1, "Maximum skip depth exceeded"

    invoke-direct {p0, p1}, Lcom/evernote/thrift/TException;-><init>(Ljava/lang/String;)V

    throw p0

    :catch_0
    move-exception p0

    .line 120
    throw p0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_0
        :pswitch_8
        :pswitch_0
        :pswitch_7
        :pswitch_0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.class public Lcom/evernote/thrift/protocol/TBinaryProtocol;
.super Lcom/evernote/thrift/protocol/TProtocol;
.source "TBinaryProtocol.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/evernote/thrift/protocol/TBinaryProtocol$Factory;
    }
.end annotation


# static fields
.field private static final ANONYMOUS_STRUCT:Lcom/evernote/thrift/protocol/TStruct;

.field private static final UTF8:Ljava/nio/charset/Charset;

.field protected static final VERSION_1:I = -0x7fff0000

.field protected static final VERSION_MASK:I = -0x10000


# instance fields
.field private bin:[B

.field private bout:[B

.field protected checkReadLength_:Z

.field private i16out:[B

.field private i16rd:[B

.field private i32out:[B

.field private i32rd:[B

.field private i64out:[B

.field private i64rd:[B

.field protected readLength_:I

.field protected strictRead_:Z

.field protected strictWrite_:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 33
    new-instance v0, Lcom/evernote/thrift/protocol/TStruct;

    invoke-direct {v0}, Lcom/evernote/thrift/protocol/TStruct;-><init>()V

    sput-object v0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->ANONYMOUS_STRUCT:Lcom/evernote/thrift/protocol/TStruct;

    const-string v0, "UTF-8"

    .line 35
    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->UTF8:Ljava/nio/charset/Charset;

    return-void
.end method

.method public constructor <init>(Lcom/evernote/thrift/transport/TTransport;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 81
    invoke-direct {p0, p1, v0, v1}, Lcom/evernote/thrift/protocol/TBinaryProtocol;-><init>(Lcom/evernote/thrift/transport/TTransport;ZZ)V

    return-void
.end method

.method public constructor <init>(Lcom/evernote/thrift/transport/TTransport;ZZ)V
    .locals 4

    .line 85
    invoke-direct {p0, p1}, Lcom/evernote/thrift/protocol/TProtocol;-><init>(Lcom/evernote/thrift/transport/TTransport;)V

    const/4 p1, 0x0

    .line 40
    iput-boolean p1, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->strictRead_:Z

    const/4 v0, 0x1

    .line 41
    iput-boolean v0, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->strictWrite_:Z

    .line 44
    iput-boolean p1, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->checkReadLength_:Z

    .line 160
    new-array p1, v0, [B

    iput-object p1, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->bout:[B

    const/4 p1, 0x2

    .line 167
    new-array v1, p1, [B

    iput-object v1, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->i16out:[B

    const/4 v1, 0x4

    .line 175
    new-array v2, v1, [B

    iput-object v2, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->i32out:[B

    const/16 v2, 0x8

    .line 185
    new-array v3, v2, [B

    iput-object v3, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->i64out:[B

    .line 290
    new-array v0, v0, [B

    iput-object v0, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->bin:[B

    .line 302
    new-array p1, p1, [B

    iput-object p1, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->i16rd:[B

    .line 322
    new-array p1, v1, [B

    iput-object p1, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->i32rd:[B

    .line 342
    new-array p1, v2, [B

    iput-object p1, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->i64rd:[B

    .line 86
    iput-boolean p2, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->strictRead_:Z

    .line 87
    iput-boolean p3, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->strictWrite_:Z

    return-void
.end method

.method private readAll([BII)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 418
    invoke-virtual {p0, p3}, Lcom/evernote/thrift/protocol/TBinaryProtocol;->checkReadLength(I)V

    .line 419
    iget-object v0, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->trans_:Lcom/evernote/thrift/transport/TTransport;

    invoke-virtual {v0, p1, p2, p3}, Lcom/evernote/thrift/transport/TTransport;->readAll([BII)I

    move-result p1

    return p1
.end method


# virtual methods
.method protected checkReadLength(I)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    if-ltz p1, :cond_2

    .line 431
    iget-boolean v0, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->checkReadLength_:Z

    if-eqz v0, :cond_1

    .line 432
    iget v0, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->readLength_:I

    sub-int/2addr v0, p1

    iput v0, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->readLength_:I

    .line 433
    iget v0, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->readLength_:I

    if-ltz v0, :cond_0

    goto :goto_0

    .line 434
    :cond_0
    new-instance v0, Lcom/evernote/thrift/TException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Message length exceeded: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/evernote/thrift/TException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    return-void

    .line 429
    :cond_2
    new-instance v0, Lcom/evernote/thrift/TException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Negative length: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/evernote/thrift/TException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public readBinary()Ljava/nio/ByteBuffer;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 395
    invoke-virtual {p0}, Lcom/evernote/thrift/protocol/TBinaryProtocol;->readI32()I

    move-result v0

    .line 396
    invoke-virtual {p0, v0}, Lcom/evernote/thrift/protocol/TBinaryProtocol;->checkReadLength(I)V

    .line 398
    iget-object v1, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->trans_:Lcom/evernote/thrift/transport/TTransport;

    invoke-virtual {v1}, Lcom/evernote/thrift/transport/TTransport;->getBytesRemainingInBuffer()I

    move-result v1

    if-lt v1, v0, :cond_0

    .line 399
    iget-object v1, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->trans_:Lcom/evernote/thrift/transport/TTransport;

    invoke-virtual {v1}, Lcom/evernote/thrift/transport/TTransport;->getBuffer()[B

    move-result-object v1

    iget-object v2, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->trans_:Lcom/evernote/thrift/transport/TTransport;

    invoke-virtual {v2}, Lcom/evernote/thrift/transport/TTransport;->getBufferPosition()I

    move-result v2

    invoke-static {v1, v2, v0}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 400
    iget-object v2, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->trans_:Lcom/evernote/thrift/transport/TTransport;

    invoke-virtual {v2, v0}, Lcom/evernote/thrift/transport/TTransport;->consumeBuffer(I)V

    return-object v1

    .line 404
    :cond_0
    new-array v1, v0, [B

    .line 405
    iget-object v2, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->trans_:Lcom/evernote/thrift/transport/TTransport;

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3, v0}, Lcom/evernote/thrift/transport/TTransport;->readAll([BII)I

    .line 406
    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    return-object v0
.end method

.method public readBool()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 287
    invoke-virtual {p0}, Lcom/evernote/thrift/protocol/TBinaryProtocol;->readByte()B

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public readByte()B
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 293
    iget-object v0, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->trans_:Lcom/evernote/thrift/transport/TTransport;

    invoke-virtual {v0}, Lcom/evernote/thrift/transport/TTransport;->getBytesRemainingInBuffer()I

    move-result v0

    const/4 v1, 0x1

    if-lt v0, v1, :cond_0

    .line 294
    iget-object v0, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->trans_:Lcom/evernote/thrift/transport/TTransport;

    invoke-virtual {v0}, Lcom/evernote/thrift/transport/TTransport;->getBuffer()[B

    move-result-object v0

    iget-object v2, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->trans_:Lcom/evernote/thrift/transport/TTransport;

    invoke-virtual {v2}, Lcom/evernote/thrift/transport/TTransport;->getBufferPosition()I

    move-result v2

    aget-byte v0, v0, v2

    .line 295
    iget-object v2, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->trans_:Lcom/evernote/thrift/transport/TTransport;

    invoke-virtual {v2, v1}, Lcom/evernote/thrift/transport/TTransport;->consumeBuffer(I)V

    return v0

    .line 298
    :cond_0
    iget-object v0, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->bin:[B

    const/4 v2, 0x0

    invoke-direct {p0, v0, v2, v1}, Lcom/evernote/thrift/protocol/TBinaryProtocol;->readAll([BII)I

    .line 299
    iget-object v0, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->bin:[B

    aget-byte v0, v0, v2

    return v0
.end method

.method public readBytes()[B
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 411
    invoke-virtual {p0}, Lcom/evernote/thrift/protocol/TBinaryProtocol;->readI32()I

    move-result v0

    .line 412
    new-array v1, v0, [B

    .line 413
    iget-object v2, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->trans_:Lcom/evernote/thrift/transport/TTransport;

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3, v0}, Lcom/evernote/thrift/transport/TTransport;->readAll([BII)I

    return-object v1
.end method

.method public readDouble()D
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 369
    invoke-virtual {p0}, Lcom/evernote/thrift/protocol/TBinaryProtocol;->readI64()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    return-wide v0
.end method

.method public readFieldBegin()Lcom/evernote/thrift/protocol/TField;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 253
    invoke-virtual {p0}, Lcom/evernote/thrift/protocol/TBinaryProtocol;->readByte()B

    move-result v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    .line 254
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/thrift/protocol/TBinaryProtocol;->readI16()S

    move-result v1

    .line 255
    :goto_0
    new-instance v2, Lcom/evernote/thrift/protocol/TField;

    const-string v3, ""

    invoke-direct {v2, v3, v0, v1}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    return-object v2
.end method

.method public readFieldEnd()V
    .locals 0

    return-void
.end method

.method public readI16()S
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 305
    iget-object v0, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->i16rd:[B

    .line 308
    iget-object v1, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->trans_:Lcom/evernote/thrift/transport/TTransport;

    invoke-virtual {v1}, Lcom/evernote/thrift/transport/TTransport;->getBytesRemainingInBuffer()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x2

    if-lt v1, v3, :cond_0

    .line 309
    iget-object v0, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->trans_:Lcom/evernote/thrift/transport/TTransport;

    invoke-virtual {v0}, Lcom/evernote/thrift/transport/TTransport;->getBuffer()[B

    move-result-object v0

    .line 310
    iget-object v1, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->trans_:Lcom/evernote/thrift/transport/TTransport;

    invoke-virtual {v1}, Lcom/evernote/thrift/transport/TTransport;->getBufferPosition()I

    move-result v2

    .line 311
    iget-object v1, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->trans_:Lcom/evernote/thrift/transport/TTransport;

    invoke-virtual {v1, v3}, Lcom/evernote/thrift/transport/TTransport;->consumeBuffer(I)V

    goto :goto_0

    .line 313
    :cond_0
    iget-object v1, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->i16rd:[B

    invoke-direct {p0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TBinaryProtocol;->readAll([BII)I

    .line 316
    :goto_0
    aget-byte v1, v0, v2

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    add-int/lit8 v2, v2, 0x1

    aget-byte v0, v0, v2

    and-int/lit16 v0, v0, 0xff

    or-int/2addr v0, v1

    int-to-short v0, v0

    return v0
.end method

.method public readI32()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 325
    iget-object v0, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->i32rd:[B

    .line 328
    iget-object v1, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->trans_:Lcom/evernote/thrift/transport/TTransport;

    invoke-virtual {v1}, Lcom/evernote/thrift/transport/TTransport;->getBytesRemainingInBuffer()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x4

    if-lt v1, v3, :cond_0

    .line 329
    iget-object v0, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->trans_:Lcom/evernote/thrift/transport/TTransport;

    invoke-virtual {v0}, Lcom/evernote/thrift/transport/TTransport;->getBuffer()[B

    move-result-object v0

    .line 330
    iget-object v1, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->trans_:Lcom/evernote/thrift/transport/TTransport;

    invoke-virtual {v1}, Lcom/evernote/thrift/transport/TTransport;->getBufferPosition()I

    move-result v2

    .line 331
    iget-object v1, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->trans_:Lcom/evernote/thrift/transport/TTransport;

    invoke-virtual {v1, v3}, Lcom/evernote/thrift/transport/TTransport;->consumeBuffer(I)V

    goto :goto_0

    .line 333
    :cond_0
    iget-object v1, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->i32rd:[B

    invoke-direct {p0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TBinaryProtocol;->readAll([BII)I

    .line 335
    :goto_0
    aget-byte v1, v0, v2

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x18

    add-int/lit8 v3, v2, 0x1

    aget-byte v3, v0, v3

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x10

    or-int/2addr v1, v3

    add-int/lit8 v3, v2, 0x2

    aget-byte v3, v0, v3

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x8

    or-int/2addr v1, v3

    add-int/lit8 v2, v2, 0x3

    aget-byte v0, v0, v2

    and-int/lit16 v0, v0, 0xff

    or-int/2addr v0, v1

    return v0
.end method

.method public readI64()J
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 345
    iget-object v0, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->i64rd:[B

    .line 348
    iget-object v1, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->trans_:Lcom/evernote/thrift/transport/TTransport;

    invoke-virtual {v1}, Lcom/evernote/thrift/transport/TTransport;->getBytesRemainingInBuffer()I

    move-result v1

    const/4 v2, 0x0

    const/16 v3, 0x8

    if-lt v1, v3, :cond_0

    .line 349
    iget-object v0, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->trans_:Lcom/evernote/thrift/transport/TTransport;

    invoke-virtual {v0}, Lcom/evernote/thrift/transport/TTransport;->getBuffer()[B

    move-result-object v0

    .line 350
    iget-object v1, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->trans_:Lcom/evernote/thrift/transport/TTransport;

    invoke-virtual {v1}, Lcom/evernote/thrift/transport/TTransport;->getBufferPosition()I

    move-result v2

    .line 351
    iget-object v1, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->trans_:Lcom/evernote/thrift/transport/TTransport;

    invoke-virtual {v1, v3}, Lcom/evernote/thrift/transport/TTransport;->consumeBuffer(I)V

    goto :goto_0

    .line 353
    :cond_0
    iget-object v1, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->i64rd:[B

    invoke-direct {p0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TBinaryProtocol;->readAll([BII)I

    .line 356
    :goto_0
    aget-byte v1, v0, v2

    and-int/lit16 v1, v1, 0xff

    int-to-long v4, v1

    const/16 v1, 0x38

    shl-long/2addr v4, v1

    add-int/lit8 v1, v2, 0x1

    aget-byte v1, v0, v1

    and-int/lit16 v1, v1, 0xff

    int-to-long v6, v1

    const/16 v1, 0x30

    shl-long/2addr v6, v1

    or-long/2addr v4, v6

    add-int/lit8 v1, v2, 0x2

    aget-byte v1, v0, v1

    and-int/lit16 v1, v1, 0xff

    int-to-long v6, v1

    const/16 v1, 0x28

    shl-long/2addr v6, v1

    or-long/2addr v4, v6

    add-int/lit8 v1, v2, 0x3

    aget-byte v1, v0, v1

    and-int/lit16 v1, v1, 0xff

    int-to-long v6, v1

    const/16 v1, 0x20

    shl-long/2addr v6, v1

    or-long/2addr v4, v6

    add-int/lit8 v1, v2, 0x4

    aget-byte v1, v0, v1

    and-int/lit16 v1, v1, 0xff

    int-to-long v6, v1

    const/16 v1, 0x18

    shl-long/2addr v6, v1

    or-long/2addr v4, v6

    add-int/lit8 v1, v2, 0x5

    aget-byte v1, v0, v1

    and-int/lit16 v1, v1, 0xff

    int-to-long v6, v1

    const/16 v1, 0x10

    shl-long/2addr v6, v1

    or-long/2addr v4, v6

    add-int/lit8 v1, v2, 0x6

    aget-byte v1, v0, v1

    and-int/lit16 v1, v1, 0xff

    int-to-long v6, v1

    shl-long/2addr v6, v3

    or-long v3, v4, v6

    add-int/lit8 v2, v2, 0x7

    aget-byte v0, v0, v2

    and-int/lit16 v0, v0, 0xff

    int-to-long v0, v0

    or-long/2addr v0, v3

    return-wide v0
.end method

.method public readListBegin()Lcom/evernote/thrift/protocol/TList;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 271
    new-instance v0, Lcom/evernote/thrift/protocol/TList;

    invoke-virtual {p0}, Lcom/evernote/thrift/protocol/TBinaryProtocol;->readByte()B

    move-result v1

    invoke-virtual {p0}, Lcom/evernote/thrift/protocol/TBinaryProtocol;->readI32()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/protocol/TList;-><init>(BI)V

    return-object v0
.end method

.method public readListEnd()V
    .locals 0

    return-void
.end method

.method public readMapBegin()Lcom/evernote/thrift/protocol/TMap;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 263
    new-instance v0, Lcom/evernote/thrift/protocol/TMap;

    invoke-virtual {p0}, Lcom/evernote/thrift/protocol/TBinaryProtocol;->readByte()B

    move-result v1

    invoke-virtual {p0}, Lcom/evernote/thrift/protocol/TBinaryProtocol;->readByte()B

    move-result v2

    invoke-virtual {p0}, Lcom/evernote/thrift/protocol/TBinaryProtocol;->readI32()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TMap;-><init>(BBI)V

    return-object v0
.end method

.method public readMapEnd()V
    .locals 0

    return-void
.end method

.method public readMessageBegin()Lcom/evernote/thrift/protocol/TMessage;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 225
    invoke-virtual {p0}, Lcom/evernote/thrift/protocol/TBinaryProtocol;->readI32()I

    move-result v0

    const/4 v1, 0x4

    if-gez v0, :cond_1

    const/high16 v2, -0x10000

    and-int/2addr v2, v0

    const/high16 v3, -0x7fff0000

    if-ne v2, v3, :cond_0

    .line 231
    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    invoke-virtual {p0}, Lcom/evernote/thrift/protocol/TBinaryProtocol;->readString()Ljava/lang/String;

    move-result-object v2

    and-int/lit16 v0, v0, 0xff

    int-to-byte v0, v0

    invoke-virtual {p0}, Lcom/evernote/thrift/protocol/TBinaryProtocol;->readI32()I

    move-result v3

    invoke-direct {v1, v2, v0, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    return-object v1

    .line 229
    :cond_0
    new-instance v0, Lcom/evernote/thrift/protocol/TProtocolException;

    const-string v2, "Bad version in readMessageBegin"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/protocol/TProtocolException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 233
    :cond_1
    iget-boolean v2, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->strictRead_:Z

    if-nez v2, :cond_2

    .line 236
    new-instance v1, Lcom/evernote/thrift/protocol/TMessage;

    invoke-virtual {p0, v0}, Lcom/evernote/thrift/protocol/TBinaryProtocol;->readStringBody(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/evernote/thrift/protocol/TBinaryProtocol;->readByte()B

    move-result v2

    invoke-virtual {p0}, Lcom/evernote/thrift/protocol/TBinaryProtocol;->readI32()I

    move-result v3

    invoke-direct {v1, v0, v2, v3}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    return-object v1

    .line 234
    :cond_2
    new-instance v0, Lcom/evernote/thrift/protocol/TProtocolException;

    const-string v2, "Missing version in readMessageBegin, old client?"

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/protocol/TProtocolException;-><init>(ILjava/lang/String;)V

    throw v0
.end method

.method public readMessageEnd()V
    .locals 0

    return-void
.end method

.method public readSetBegin()Lcom/evernote/thrift/protocol/TSet;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 279
    new-instance v0, Lcom/evernote/thrift/protocol/TSet;

    invoke-virtual {p0}, Lcom/evernote/thrift/protocol/TBinaryProtocol;->readByte()B

    move-result v1

    invoke-virtual {p0}, Lcom/evernote/thrift/protocol/TBinaryProtocol;->readI32()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/evernote/thrift/protocol/TSet;-><init>(BI)V

    return-object v0
.end method

.method public readSetEnd()V
    .locals 0

    return-void
.end method

.method public readString()Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 374
    invoke-virtual {p0}, Lcom/evernote/thrift/protocol/TBinaryProtocol;->readI32()I

    move-result v0

    .line 376
    iget-object v1, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->trans_:Lcom/evernote/thrift/transport/TTransport;

    invoke-virtual {v1}, Lcom/evernote/thrift/transport/TTransport;->getBytesRemainingInBuffer()I

    move-result v1

    if-lt v1, v0, :cond_0

    .line 377
    sget-object v1, Lcom/evernote/thrift/protocol/TBinaryProtocol;->UTF8:Ljava/nio/charset/Charset;

    iget-object v2, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->trans_:Lcom/evernote/thrift/transport/TTransport;

    invoke-virtual {v2}, Lcom/evernote/thrift/transport/TTransport;->getBuffer()[B

    move-result-object v2

    iget-object v3, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->trans_:Lcom/evernote/thrift/transport/TTransport;

    invoke-virtual {v3}, Lcom/evernote/thrift/transport/TTransport;->getBufferPosition()I

    move-result v3

    invoke-static {v2, v3, v0}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/nio/charset/Charset;->decode(Ljava/nio/ByteBuffer;)Ljava/nio/CharBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/CharBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    .line 379
    iget-object v2, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->trans_:Lcom/evernote/thrift/transport/TTransport;

    invoke-virtual {v2, v0}, Lcom/evernote/thrift/transport/TTransport;->consumeBuffer(I)V

    return-object v1

    .line 383
    :cond_0
    invoke-virtual {p0, v0}, Lcom/evernote/thrift/protocol/TBinaryProtocol;->readStringBody(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public readStringBody(I)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 387
    invoke-virtual {p0, p1}, Lcom/evernote/thrift/protocol/TBinaryProtocol;->checkReadLength(I)V

    .line 388
    new-array v0, p1, [B

    .line 389
    iget-object v1, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->trans_:Lcom/evernote/thrift/transport/TTransport;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2, p1}, Lcom/evernote/thrift/transport/TTransport;->readAll([BII)I

    .line 390
    sget-object p1, Lcom/evernote/thrift/protocol/TBinaryProtocol;->UTF8:Ljava/nio/charset/Charset;

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/nio/charset/Charset;->decode(Ljava/nio/ByteBuffer;)Ljava/nio/CharBuffer;

    move-result-object p1

    invoke-virtual {p1}, Ljava/nio/CharBuffer;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public readStructBegin()Lcom/evernote/thrift/protocol/TStruct;
    .locals 1

    .line 245
    sget-object v0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->ANONYMOUS_STRUCT:Lcom/evernote/thrift/protocol/TStruct;

    return-object v0
.end method

.method public readStructEnd()V
    .locals 0

    return-void
.end method

.method public setReadLength(I)V
    .locals 0

    .line 423
    iput p1, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->readLength_:I

    const/4 p1, 0x1

    .line 424
    iput-boolean p1, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->checkReadLength_:Z

    return-void
.end method

.method public writeBinary([BII)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 215
    invoke-virtual {p0, p3}, Lcom/evernote/thrift/protocol/TBinaryProtocol;->writeI32(I)V

    .line 216
    iget-object v0, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->trans_:Lcom/evernote/thrift/transport/TTransport;

    invoke-virtual {v0, p1, p2, p3}, Lcom/evernote/thrift/transport/TTransport;->write([BII)V

    return-void
.end method

.method public writeBool(Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 157
    invoke-virtual {p0, p1}, Lcom/evernote/thrift/protocol/TBinaryProtocol;->writeByte(B)V

    return-void
.end method

.method public writeByte(B)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 163
    iget-object v0, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->bout:[B

    const/4 v1, 0x0

    aput-byte p1, v0, v1

    .line 164
    iget-object p1, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->trans_:Lcom/evernote/thrift/transport/TTransport;

    iget-object v0, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->bout:[B

    const/4 v2, 0x1

    invoke-virtual {p1, v0, v1, v2}, Lcom/evernote/thrift/transport/TTransport;->write([BII)V

    return-void
.end method

.method public writeDouble(D)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 201
    invoke-static {p1, p2}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide p1

    invoke-virtual {p0, p1, p2}, Lcom/evernote/thrift/protocol/TBinaryProtocol;->writeI64(J)V

    return-void
.end method

.method public writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 115
    iget-byte v0, p1, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-virtual {p0, v0}, Lcom/evernote/thrift/protocol/TBinaryProtocol;->writeByte(B)V

    .line 116
    iget-short p1, p1, Lcom/evernote/thrift/protocol/TField;->id:S

    invoke-virtual {p0, p1}, Lcom/evernote/thrift/protocol/TBinaryProtocol;->writeI16(S)V

    return-void
.end method

.method public writeFieldEnd()V
    .locals 0

    return-void
.end method

.method public writeFieldStop()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    const/4 v0, 0x0

    .line 124
    invoke-virtual {p0, v0}, Lcom/evernote/thrift/protocol/TBinaryProtocol;->writeByte(B)V

    return-void
.end method

.method public writeI16(S)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 170
    iget-object v0, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->i16out:[B

    shr-int/lit8 v1, p1, 0x8

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    and-int/lit16 p1, p1, 0xff

    int-to-byte p1, p1

    const/4 v1, 0x1

    .line 171
    aput-byte p1, v0, v1

    .line 172
    iget-object p1, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->trans_:Lcom/evernote/thrift/transport/TTransport;

    iget-object v0, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->i16out:[B

    const/4 v1, 0x2

    invoke-virtual {p1, v0, v2, v1}, Lcom/evernote/thrift/transport/TTransport;->write([BII)V

    return-void
.end method

.method public writeI32(I)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 178
    iget-object v0, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->i32out:[B

    shr-int/lit8 v1, p1, 0x18

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    shr-int/lit8 v1, p1, 0x10

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    const/4 v3, 0x1

    .line 179
    aput-byte v1, v0, v3

    shr-int/lit8 v1, p1, 0x8

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    const/4 v3, 0x2

    .line 180
    aput-byte v1, v0, v3

    and-int/lit16 p1, p1, 0xff

    int-to-byte p1, p1

    const/4 v1, 0x3

    .line 181
    aput-byte p1, v0, v1

    .line 182
    iget-object p1, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->trans_:Lcom/evernote/thrift/transport/TTransport;

    iget-object v0, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->i32out:[B

    const/4 v1, 0x4

    invoke-virtual {p1, v0, v2, v1}, Lcom/evernote/thrift/transport/TTransport;->write([BII)V

    return-void
.end method

.method public writeI64(J)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 188
    iget-object v0, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->i64out:[B

    const/16 v1, 0x38

    shr-long v1, p1, v1

    const-wide/16 v3, 0xff

    and-long/2addr v1, v3

    long-to-int v2, v1

    int-to-byte v1, v2

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    const/16 v1, 0x30

    shr-long v5, p1, v1

    and-long/2addr v5, v3

    long-to-int v1, v5

    int-to-byte v1, v1

    const/4 v5, 0x1

    .line 189
    aput-byte v1, v0, v5

    const/16 v1, 0x28

    shr-long v5, p1, v1

    and-long/2addr v5, v3

    long-to-int v1, v5

    int-to-byte v1, v1

    const/4 v5, 0x2

    .line 190
    aput-byte v1, v0, v5

    const/16 v1, 0x20

    shr-long v5, p1, v1

    and-long/2addr v5, v3

    long-to-int v1, v5

    int-to-byte v1, v1

    const/4 v5, 0x3

    .line 191
    aput-byte v1, v0, v5

    const/16 v1, 0x18

    shr-long v5, p1, v1

    and-long/2addr v5, v3

    long-to-int v1, v5

    int-to-byte v1, v1

    const/4 v5, 0x4

    .line 192
    aput-byte v1, v0, v5

    const/16 v1, 0x10

    shr-long v5, p1, v1

    and-long/2addr v5, v3

    long-to-int v1, v5

    int-to-byte v1, v1

    const/4 v5, 0x5

    .line 193
    aput-byte v1, v0, v5

    const/16 v1, 0x8

    shr-long v5, p1, v1

    and-long/2addr v5, v3

    long-to-int v6, v5

    int-to-byte v5, v6

    const/4 v6, 0x6

    .line 194
    aput-byte v5, v0, v6

    and-long/2addr p1, v3

    long-to-int p2, p1

    int-to-byte p1, p2

    const/4 p2, 0x7

    .line 195
    aput-byte p1, v0, p2

    .line 196
    iget-object p1, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->trans_:Lcom/evernote/thrift/transport/TTransport;

    iget-object p2, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->i64out:[B

    invoke-virtual {p1, p2, v2, v1}, Lcom/evernote/thrift/transport/TTransport;->write([BII)V

    return-void
.end method

.method public writeListBegin(Lcom/evernote/thrift/protocol/TList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 139
    iget-byte v0, p1, Lcom/evernote/thrift/protocol/TList;->elemType:B

    invoke-virtual {p0, v0}, Lcom/evernote/thrift/protocol/TBinaryProtocol;->writeByte(B)V

    .line 140
    iget p1, p1, Lcom/evernote/thrift/protocol/TList;->size:I

    invoke-virtual {p0, p1}, Lcom/evernote/thrift/protocol/TBinaryProtocol;->writeI32(I)V

    return-void
.end method

.method public writeListEnd()V
    .locals 0

    return-void
.end method

.method public writeMapBegin(Lcom/evernote/thrift/protocol/TMap;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 129
    iget-byte v0, p1, Lcom/evernote/thrift/protocol/TMap;->keyType:B

    invoke-virtual {p0, v0}, Lcom/evernote/thrift/protocol/TBinaryProtocol;->writeByte(B)V

    .line 130
    iget-byte v0, p1, Lcom/evernote/thrift/protocol/TMap;->valueType:B

    invoke-virtual {p0, v0}, Lcom/evernote/thrift/protocol/TBinaryProtocol;->writeByte(B)V

    .line 131
    iget p1, p1, Lcom/evernote/thrift/protocol/TMap;->size:I

    invoke-virtual {p0, p1}, Lcom/evernote/thrift/protocol/TBinaryProtocol;->writeI32(I)V

    return-void
.end method

.method public writeMapEnd()V
    .locals 0

    return-void
.end method

.method public writeMessageBegin(Lcom/evernote/thrift/protocol/TMessage;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 92
    iget-boolean v0, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->strictWrite_:Z

    if-eqz v0, :cond_0

    const/high16 v0, -0x7fff0000

    .line 93
    iget-byte v1, p1, Lcom/evernote/thrift/protocol/TMessage;->type:B

    or-int/2addr v0, v1

    .line 94
    invoke-virtual {p0, v0}, Lcom/evernote/thrift/protocol/TBinaryProtocol;->writeI32(I)V

    .line 95
    iget-object v0, p1, Lcom/evernote/thrift/protocol/TMessage;->name:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/evernote/thrift/protocol/TBinaryProtocol;->writeString(Ljava/lang/String;)V

    .line 96
    iget p1, p1, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    invoke-virtual {p0, p1}, Lcom/evernote/thrift/protocol/TBinaryProtocol;->writeI32(I)V

    goto :goto_0

    .line 98
    :cond_0
    iget-object v0, p1, Lcom/evernote/thrift/protocol/TMessage;->name:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/evernote/thrift/protocol/TBinaryProtocol;->writeString(Ljava/lang/String;)V

    .line 99
    iget-byte v0, p1, Lcom/evernote/thrift/protocol/TMessage;->type:B

    invoke-virtual {p0, v0}, Lcom/evernote/thrift/protocol/TBinaryProtocol;->writeByte(B)V

    .line 100
    iget p1, p1, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    invoke-virtual {p0, p1}, Lcom/evernote/thrift/protocol/TBinaryProtocol;->writeI32(I)V

    :goto_0
    return-void
.end method

.method public writeMessageEnd()V
    .locals 0

    return-void
.end method

.method public writeSetBegin(Lcom/evernote/thrift/protocol/TSet;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 148
    iget-byte v0, p1, Lcom/evernote/thrift/protocol/TSet;->elemType:B

    invoke-virtual {p0, v0}, Lcom/evernote/thrift/protocol/TBinaryProtocol;->writeByte(B)V

    .line 149
    iget p1, p1, Lcom/evernote/thrift/protocol/TSet;->size:I

    invoke-virtual {p0, p1}, Lcom/evernote/thrift/protocol/TBinaryProtocol;->writeI32(I)V

    return-void
.end method

.method public writeSetEnd()V
    .locals 0

    return-void
.end method

.method public writeString(Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 206
    sget-object v0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->UTF8:Ljava/nio/charset/Charset;

    invoke-virtual {v0, p1}, Ljava/nio/charset/Charset;->encode(Ljava/lang/String;)Ljava/nio/ByteBuffer;

    move-result-object p1

    .line 207
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    new-array v0, v0, [B

    .line 208
    array-length v1, v0

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2, v1}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 209
    array-length p1, v0

    invoke-virtual {p0, p1}, Lcom/evernote/thrift/protocol/TBinaryProtocol;->writeI32(I)V

    .line 210
    iget-object p1, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol;->trans_:Lcom/evernote/thrift/transport/TTransport;

    array-length v1, v0

    invoke-virtual {p1, v0, v2, v1}, Lcom/evernote/thrift/transport/TTransport;->write([BII)V

    return-void
.end method

.method public writeStructBegin(Lcom/evernote/thrift/protocol/TStruct;)V
    .locals 0

    return-void
.end method

.method public writeStructEnd()V
    .locals 0

    return-void
.end method

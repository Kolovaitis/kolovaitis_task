.class Lepson/server/screens/UploadScreen$8;
.super Ljava/lang/Object;
.source "UploadScreen.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/server/screens/UploadScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/server/screens/UploadScreen;


# direct methods
.method constructor <init>(Lepson/server/screens/UploadScreen;)V
    .locals 0

    .line 578
    iput-object p1, p0, Lepson/server/screens/UploadScreen$8;->this$0:Lepson/server/screens/UploadScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 9

    .line 581
    iget p1, p1, Landroid/os/Message;->what:I

    const/4 v0, 0x0

    if-eqz p1, :cond_2

    const/16 v1, 0x10

    if-eq p1, v1, :cond_0

    goto/16 :goto_0

    :cond_0
    const-string p1, "createUploadThread"

    .line 586
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "5:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lepson/server/screens/UploadScreen$8;->this$0:Lepson/server/screens/UploadScreen;

    iget-boolean v2, v2, Lepson/server/screens/UploadScreen;->isSaved:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 587
    iget-object p1, p0, Lepson/server/screens/UploadScreen$8;->this$0:Lepson/server/screens/UploadScreen;

    iget-boolean p1, p1, Lepson/server/screens/UploadScreen;->isSaved:Z

    const/4 v1, 0x1

    if-ne p1, v1, :cond_1

    .line 589
    iget-object p1, p0, Lepson/server/screens/UploadScreen$8;->this$0:Lepson/server/screens/UploadScreen;

    invoke-virtual {p1}, Lepson/server/screens/UploadScreen;->getIntent()Landroid/content/Intent;

    move-result-object p1

    .line 590
    iget-object v1, p0, Lepson/server/screens/UploadScreen$8;->this$0:Lepson/server/screens/UploadScreen;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, p1}, Lepson/server/screens/UploadScreen;->setResult(ILandroid/content/Intent;)V

    .line 591
    iget-object p1, p0, Lepson/server/screens/UploadScreen$8;->this$0:Lepson/server/screens/UploadScreen;

    invoke-virtual {p1}, Lepson/server/screens/UploadScreen;->finish()V

    goto :goto_0

    :cond_1
    const-string p1, "createUploadThread"

    .line 593
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "6:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lepson/server/screens/UploadScreen$8;->this$0:Lepson/server/screens/UploadScreen;

    iget-boolean v2, v2, Lepson/server/screens/UploadScreen;->isSaved:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 594
    iget-object p1, p0, Lepson/server/screens/UploadScreen$8;->this$0:Lepson/server/screens/UploadScreen;

    new-instance v1, Lepson/server/screens/UploadScreen$ErrorDialog;

    const v5, 0x7f0f0009

    const/4 v6, 0x4

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v2, v1

    move-object v3, p1

    move-object v4, p1

    invoke-direct/range {v2 .. v8}, Lepson/server/screens/UploadScreen$ErrorDialog;-><init>(Lepson/server/screens/UploadScreen;Landroid/content/Context;IIIZ)V

    iput-object v1, p1, Lepson/server/screens/UploadScreen;->errorDialog:Landroid/app/Dialog;

    .line 595
    iget-object p1, p0, Lepson/server/screens/UploadScreen$8;->this$0:Lepson/server/screens/UploadScreen;

    iget-object p1, p1, Lepson/server/screens/UploadScreen;->errorDialog:Landroid/app/Dialog;

    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    .line 596
    iget-object p1, p0, Lepson/server/screens/UploadScreen$8;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v1}, Lepson/server/screens/UploadScreen;->access$700(Lepson/server/screens/UploadScreen;Ljava/lang/Boolean;)V

    goto :goto_0

    .line 583
    :cond_2
    iget-object p1, p0, Lepson/server/screens/UploadScreen$8;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v1}, Lepson/server/screens/UploadScreen;->access$700(Lepson/server/screens/UploadScreen;Ljava/lang/Boolean;)V

    :goto_0
    return v0
.end method

.class public Lepson/server/screens/StorageServer;
.super Lepson/print/ActivityIACommon;
.source "StorageServer.java"

# interfaces
.implements Lepson/print/CommonDefine;


# static fields
.field public static final FOR_PRINT_LOGGER_CALL_FROM:Ljava/lang/String; = "forLoggerCallFrom"

.field public static final FOR_PRINT_LOGGER_CALL_FROM_NO_INFO:I = 0x0

.field public static final FOR_PRINT_LOGGER_CALL_FROM_SCAN:I = 0x1

.field private static final REQEST_RUNTIMEPERMMISSION:I = 0x1


# instance fields
.field mNumOfFiles:I

.field mScanSaveAction:Z

.field private mScanedFilePath:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 39
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lepson/server/screens/StorageServer;->mScanedFilePath:Ljava/util/ArrayList;

    const/4 v0, 0x0

    .line 52
    iput-boolean v0, p0, Lepson/server/screens/StorageServer;->mScanSaveAction:Z

    return-void
.end method

.method static synthetic access$000(Lepson/server/screens/StorageServer;)Ljava/util/ArrayList;
    .locals 0

    .line 39
    iget-object p0, p0, Lepson/server/screens/StorageServer;->mScanedFilePath:Ljava/util/ArrayList;

    return-object p0
.end method

.method static synthetic access$100(Lepson/server/screens/StorageServer;)V
    .locals 0

    .line 39
    invoke-direct {p0}, Lepson/server/screens/StorageServer;->showCannotUseDialog()V

    return-void
.end method

.method private showCannotUseDialog()V
    .locals 3

    const v0, 0x7f0e02b6

    .line 388
    invoke-static {v0}, Lepson/print/SimpleMessageDialogFragment;->newInstance(I)Lepson/print/SimpleMessageDialogFragment;

    move-result-object v0

    .line 389
    invoke-virtual {p0}, Lepson/server/screens/StorageServer;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "can-not-use-dialog"

    invoke-virtual {v0, v1, v2}, Lepson/print/SimpleMessageDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method initStorageItem(Z)V
    .locals 9

    .line 126
    invoke-virtual {p0}, Lepson/server/screens/StorageServer;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "forLoggerCallFrom"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 129
    iput-boolean v1, p0, Lepson/server/screens/StorageServer;->mScanSaveAction:Z

    goto :goto_0

    .line 131
    :cond_0
    iput-boolean v2, p0, Lepson/server/screens/StorageServer;->mScanSaveAction:Z

    .line 133
    :goto_0
    invoke-virtual {p0}, Lepson/server/screens/StorageServer;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "PICK_SERVER_FOR"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    const v1, 0x7f080237

    const v3, 0x7f08007b

    const v4, 0x7f08011b

    const v5, 0x7f080164

    const v6, 0x7f080136

    if-nez p1, :cond_1

    .line 136
    invoke-virtual {p0, v6}, Lepson/server/screens/StorageServer;->findViewById(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 137
    invoke-virtual {p0, v5}, Lepson/server/screens/StorageServer;->findViewById(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 138
    invoke-virtual {p0, v4}, Lepson/server/screens/StorageServer;->findViewById(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 139
    invoke-virtual {p0, v3}, Lepson/server/screens/StorageServer;->findViewById(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 140
    invoke-virtual {p0, v1}, Lepson/server/screens/StorageServer;->findViewById(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v2}, Landroid/view/View;->setEnabled(Z)V

    const p1, 0x7f080137

    .line 142
    invoke-virtual {p0, p1}, Lepson/server/screens/StorageServer;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const/16 v7, 0x8

    invoke-virtual {p1, v7}, Landroid/view/View;->setVisibility(I)V

    const p1, 0x7f080165

    .line 143
    invoke-virtual {p0, p1}, Lepson/server/screens/StorageServer;->findViewById(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v7}, Landroid/view/View;->setVisibility(I)V

    const p1, 0x7f08011c

    .line 144
    invoke-virtual {p0, p1}, Lepson/server/screens/StorageServer;->findViewById(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v7}, Landroid/view/View;->setVisibility(I)V

    const p1, 0x7f08007c

    .line 145
    invoke-virtual {p0, p1}, Lepson/server/screens/StorageServer;->findViewById(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v7}, Landroid/view/View;->setVisibility(I)V

    const p1, 0x7f080238

    .line 146
    invoke-virtual {p0, p1}, Lepson/server/screens/StorageServer;->findViewById(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v7}, Landroid/view/View;->setVisibility(I)V

    .line 152
    new-instance p1, Landroid/app/AlertDialog$Builder;

    invoke-direct {p1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 153
    invoke-virtual {p1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const v7, 0x7f0e04e8

    .line 154
    invoke-virtual {p0, v7}, Lepson/server/screens/StorageServer;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const v7, 0x7f0e04f2

    .line 155
    invoke-virtual {p0, v7}, Lepson/server/screens/StorageServer;->getString(I)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Lepson/server/screens/StorageServer$2;

    invoke-direct {v8, p0}, Lepson/server/screens/StorageServer$2;-><init>(Lepson/server/screens/StorageServer;)V

    invoke-virtual {p1, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 160
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    :cond_1
    const/4 p1, 0x2

    if-eq v0, p1, :cond_2

    goto :goto_1

    .line 165
    :cond_2
    invoke-virtual {p0}, Lepson/server/screens/StorageServer;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v7, "SAVING_FILE_PATH"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object p1

    iput-object p1, p0, Lepson/server/screens/StorageServer;->mScanedFilePath:Ljava/util/ArrayList;

    const p1, 0x7f0802cf

    .line 166
    invoke-virtual {p0, p1}, Lepson/server/screens/StorageServer;->findViewById(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    const p1, 0x7f0e0454

    .line 167
    invoke-virtual {p0, p1}, Lepson/server/screens/StorageServer;->setTitle(I)V

    .line 176
    :goto_1
    invoke-virtual {p0, v6}, Lepson/server/screens/StorageServer;->findViewById(I)Landroid/view/View;

    move-result-object p1

    new-instance v2, Lepson/server/screens/StorageServer$3;

    invoke-direct {v2, p0, v0}, Lepson/server/screens/StorageServer$3;-><init>(Lepson/server/screens/StorageServer;I)V

    invoke-virtual {p1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 211
    invoke-virtual {p0, v5}, Lepson/server/screens/StorageServer;->findViewById(I)Landroid/view/View;

    move-result-object p1

    new-instance v2, Lepson/server/screens/StorageServer$4;

    invoke-direct {v2, p0, v0}, Lepson/server/screens/StorageServer$4;-><init>(Lepson/server/screens/StorageServer;I)V

    invoke-virtual {p1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 240
    invoke-virtual {p0, v4}, Lepson/server/screens/StorageServer;->findViewById(I)Landroid/view/View;

    move-result-object p1

    new-instance v2, Lepson/server/screens/StorageServer$5;

    invoke-direct {v2, p0, v0}, Lepson/server/screens/StorageServer$5;-><init>(Lepson/server/screens/StorageServer;I)V

    invoke-virtual {p1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 277
    invoke-virtual {p0, v3}, Lepson/server/screens/StorageServer;->findViewById(I)Landroid/view/View;

    move-result-object p1

    new-instance v2, Lepson/server/screens/StorageServer$6;

    invoke-direct {v2, p0, v0}, Lepson/server/screens/StorageServer$6;-><init>(Lepson/server/screens/StorageServer;I)V

    invoke-virtual {p1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 308
    invoke-virtual {p0, v1}, Lepson/server/screens/StorageServer;->findViewById(I)Landroid/view/View;

    move-result-object p1

    new-instance v1, Lepson/server/screens/StorageServer$7;

    invoke-direct {v1, p0, v0}, Lepson/server/screens/StorageServer$7;-><init>(Lepson/server/screens/StorageServer;I)V

    invoke-virtual {p1, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f0801f9

    .line 338
    invoke-virtual {p0, p1}, Lepson/server/screens/StorageServer;->findViewById(I)Landroid/view/View;

    move-result-object p1

    new-instance v0, Lepson/server/screens/StorageServer$8;

    invoke-direct {v0, p0}, Lepson/server/screens/StorageServer$8;-><init>(Lepson/server/screens/StorageServer;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f08023c

    .line 368
    invoke-virtual {p0, p1}, Lepson/server/screens/StorageServer;->findViewById(I)Landroid/view/View;

    move-result-object p1

    new-instance v0, Lepson/server/screens/StorageServer$9;

    invoke-direct {v0, p0}, Lepson/server/screens/StorageServer$9;-><init>(Lepson/server/screens/StorageServer;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    .line 109
    invoke-super {p0, p1, p2, p3}, Lepson/print/ActivityIACommon;->onActivityResult(IILandroid/content/Intent;)V

    const/4 p3, 0x1

    if-eq p1, p3, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, -0x1

    if-eq p2, p1, :cond_1

    goto :goto_0

    .line 115
    :cond_1
    invoke-virtual {p0}, Lepson/server/screens/StorageServer;->saveLocalMemory()V

    :goto_0
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .line 418
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onBackPressed()V

    const-string v0, "StorageServer "

    const-string v1, "(onBackPressed)"

    .line 419
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    invoke-static {p0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v0

    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->initDownloadDir()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 57
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0a004a

    .line 59
    invoke-virtual {p0, p1}, Lepson/server/screens/StorageServer;->setContentView(I)V

    const p1, 0x7f0e03d9

    const/4 v0, 0x1

    .line 62
    invoke-virtual {p0, p1, v0}, Lepson/server/screens/StorageServer;->setActionBar(IZ)V

    .line 65
    new-instance p1, Lepson/server/screens/StorageServer$1;

    invoke-direct {p1, p0}, Lepson/server/screens/StorageServer$1;-><init>(Lepson/server/screens/StorageServer;)V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    .line 104
    invoke-virtual {p1, v0}, Lepson/server/screens/StorageServer$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .line 413
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onDestroy()V

    return-void
.end method

.method protected onResume()V
    .locals 0

    .line 408
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onResume()V

    return-void
.end method

.method saveLocalMemory()V
    .locals 3

    .line 396
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "SAVING_FILE_PATH"

    .line 398
    iget-object v2, p0, Lepson/server/screens/StorageServer;->mScanedFilePath:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    const-string v1, "for"

    const/4 v2, 0x7

    .line 399
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 401
    invoke-virtual {p0}, Lepson/server/screens/StorageServer;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lepson/server/screens/UploadScreen;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 403
    invoke-virtual {p0, v0}, Lepson/server/screens/StorageServer;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

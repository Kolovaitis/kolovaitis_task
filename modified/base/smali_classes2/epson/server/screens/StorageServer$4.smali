.class Lepson/server/screens/StorageServer$4;
.super Ljava/lang/Object;
.source "StorageServer.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/server/screens/StorageServer;->initStorageItem(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/server/screens/StorageServer;

.field final synthetic val$goal:I


# direct methods
.method constructor <init>(Lepson/server/screens/StorageServer;I)V
    .locals 0

    .line 211
    iput-object p1, p0, Lepson/server/screens/StorageServer$4;->this$0:Lepson/server/screens/StorageServer;

    iput p2, p0, Lepson/server/screens/StorageServer$4;->val$goal:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .line 214
    iget p1, p0, Lepson/server/screens/StorageServer$4;->val$goal:I

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 223
    :pswitch_0
    sget-object p1, Lcom/epson/iprint/storage/StorageServiceClient;->STORAGE_GOOGLEDRIVE:Ljava/lang/String;

    .line 224
    sget-object v0, Lcom/epson/iprint/storage/StorageProcessActivity$ProcessType;->UPLOAD:Lcom/epson/iprint/storage/StorageProcessActivity$ProcessType;

    .line 225
    iget-object v1, p0, Lepson/server/screens/StorageServer$4;->this$0:Lepson/server/screens/StorageServer;

    invoke-virtual {v1}, Lepson/server/screens/StorageServer;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p1, v0}, Lcom/epson/iprint/storage/StorageProcessActivity;->getProcessIntent(Landroid/content/Context;Ljava/lang/String;Lcom/epson/iprint/storage/StorageProcessActivity$ProcessType;)Landroid/content/Intent;

    move-result-object p1

    const-string v0, "Extra.Uploadfile.List"

    .line 226
    iget-object v1, p0, Lepson/server/screens/StorageServer$4;->this$0:Lepson/server/screens/StorageServer;

    invoke-static {v1}, Lepson/server/screens/StorageServer;->access$000(Lepson/server/screens/StorageServer;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 227
    iget-object v0, p0, Lepson/server/screens/StorageServer$4;->this$0:Lepson/server/screens/StorageServer;

    invoke-virtual {v0, p1}, Lepson/server/screens/StorageServer;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 217
    :pswitch_1
    new-instance p1, Landroid/content/Intent;

    iget-object v0, p0, Lepson/server/screens/StorageServer$4;->this$0:Lepson/server/screens/StorageServer;

    invoke-virtual {v0}, Lepson/server/screens/StorageServer;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/epson/iprint/storage/gdrivev3/ListAndDownloadActivity;

    invoke-direct {p1, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 218
    iget-object v0, p0, Lepson/server/screens/StorageServer$4;->this$0:Lepson/server/screens/StorageServer;

    invoke-virtual {v0, p1}, Lepson/server/screens/StorageServer;->startActivity(Landroid/content/Intent;)V

    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

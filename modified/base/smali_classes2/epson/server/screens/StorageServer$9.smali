.class Lepson/server/screens/StorageServer$9;
.super Ljava/lang/Object;
.source "StorageServer.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/server/screens/StorageServer;->initStorageItem(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/server/screens/StorageServer;


# direct methods
.method constructor <init>(Lepson/server/screens/StorageServer;)V
    .locals 0

    .line 368
    iput-object p1, p0, Lepson/server/screens/StorageServer$9;->this$0:Lepson/server/screens/StorageServer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .line 371
    iget-object p1, p0, Lepson/server/screens/StorageServer$9;->this$0:Lepson/server/screens/StorageServer;

    iget-boolean p1, p1, Lepson/server/screens/StorageServer;->mScanSaveAction:Z

    .line 375
    new-instance p1, Landroid/content/Intent;

    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    const-string v0, "SAVING_FILE_PATH"

    .line 377
    iget-object v1, p0, Lepson/server/screens/StorageServer$9;->this$0:Lepson/server/screens/StorageServer;

    invoke-static {v1}, Lepson/server/screens/StorageServer;->access$000(Lepson/server/screens/StorageServer;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    const-string v0, "for"

    const/4 v1, 0x6

    .line 378
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 379
    iget-object v0, p0, Lepson/server/screens/StorageServer$9;->this$0:Lepson/server/screens/StorageServer;

    invoke-virtual {v0}, Lepson/server/screens/StorageServer;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lepson/server/screens/UploadScreen;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 381
    iget-object v0, p0, Lepson/server/screens/StorageServer$9;->this$0:Lepson/server/screens/StorageServer;

    invoke-virtual {v0, p1}, Lepson/server/screens/StorageServer;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

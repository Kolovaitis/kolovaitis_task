.class Lepson/server/screens/UploadScreen$4;
.super Ljava/lang/Object;
.source "UploadScreen.java"

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/server/screens/UploadScreen;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/server/screens/UploadScreen;


# direct methods
.method constructor <init>(Lepson/server/screens/UploadScreen;)V
    .locals 0

    .line 136
    iput-object p1, p0, Lepson/server/screens/UploadScreen$4;->this$0:Lepson/server/screens/UploadScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 1

    const p1, 0x7f0801b5

    if-eq p2, p1, :cond_1

    const p1, 0x7f08025a

    if-eq p2, p1, :cond_0

    goto :goto_0

    .line 140
    :cond_0
    iget-object p1, p0, Lepson/server/screens/UploadScreen$4;->this$0:Lepson/server/screens/UploadScreen;

    const-string p2, "PDF"

    invoke-static {p1, p2}, Lepson/server/screens/UploadScreen;->access$302(Lepson/server/screens/UploadScreen;Ljava/lang/String;)Ljava/lang/String;

    const-string p1, "checkedId"

    .line 141
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "....."

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lepson/server/screens/UploadScreen$4;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {v0}, Lepson/server/screens/UploadScreen;->access$300(Lepson/server/screens/UploadScreen;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 144
    :cond_1
    iget-object p1, p0, Lepson/server/screens/UploadScreen$4;->this$0:Lepson/server/screens/UploadScreen;

    const-string p2, "JPEG"

    invoke-static {p1, p2}, Lepson/server/screens/UploadScreen;->access$302(Lepson/server/screens/UploadScreen;Ljava/lang/String;)Ljava/lang/String;

    const-string p1, "checkedId"

    .line 145
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "....."

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lepson/server/screens/UploadScreen$4;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {v0}, Lepson/server/screens/UploadScreen;->access$300(Lepson/server/screens/UploadScreen;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

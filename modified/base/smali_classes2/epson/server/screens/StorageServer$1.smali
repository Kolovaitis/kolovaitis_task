.class Lepson/server/screens/StorageServer$1;
.super Landroid/os/AsyncTask;
.source "StorageServer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/server/screens/StorageServer;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lepson/server/screens/StorageServer;

.field workingDialog:Lepson/print/screen/WorkingDialog;


# direct methods
.method constructor <init>(Lepson/server/screens/StorageServer;)V
    .locals 1

    .line 65
    iput-object p1, p0, Lepson/server/screens/StorageServer$1;->this$0:Lepson/server/screens/StorageServer;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 66
    new-instance p1, Lepson/print/screen/WorkingDialog;

    iget-object v0, p0, Lepson/server/screens/StorageServer$1;->this$0:Lepson/server/screens/StorageServer;

    invoke-direct {p1, v0}, Lepson/print/screen/WorkingDialog;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lepson/server/screens/StorageServer$1;->workingDialog:Lepson/print/screen/WorkingDialog;

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 2

    .line 78
    invoke-static {}, Lepson/print/Util/Utils;->getInstance()Lepson/print/Util/Utils;

    move-result-object p1

    .line 81
    iget-object v0, p0, Lepson/server/screens/StorageServer$1;->this$0:Lepson/server/screens/StorageServer;

    invoke-virtual {p1, v0}, Lepson/print/Util/Utils;->isNetworkAvailable(Landroid/content/Context;)Z

    move-result p1

    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 82
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    .line 86
    :cond_0
    new-instance p1, Lepson/server/utils/MyUtility;

    invoke-direct {p1}, Lepson/server/utils/MyUtility;-><init>()V

    .line 87
    iget-object v1, p0, Lepson/server/screens/StorageServer$1;->this$0:Lepson/server/screens/StorageServer;

    invoke-virtual {p1, v1}, Lepson/server/utils/MyUtility;->doNet(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    const-string v1, ""

    if-ne p1, v1, :cond_1

    .line 88
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    :cond_1
    const/4 p1, 0x1

    .line 91
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 65
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/server/screens/StorageServer$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 1

    .line 96
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 99
    iget-object v0, p0, Lepson/server/screens/StorageServer$1;->workingDialog:Lepson/print/screen/WorkingDialog;

    invoke-virtual {v0}, Lepson/print/screen/WorkingDialog;->dismiss()V

    .line 102
    iget-object v0, p0, Lepson/server/screens/StorageServer$1;->this$0:Lepson/server/screens/StorageServer;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {v0, p1}, Lepson/server/screens/StorageServer;->initStorageItem(Z)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 65
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lepson/server/screens/StorageServer$1;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .line 70
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 73
    iget-object v0, p0, Lepson/server/screens/StorageServer$1;->workingDialog:Lepson/print/screen/WorkingDialog;

    invoke-virtual {v0}, Lepson/print/screen/WorkingDialog;->show()V

    return-void
.end method

.class Lepson/server/screens/UploadScreen$7;
.super Ljava/lang/Thread;
.source "UploadScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/server/screens/UploadScreen;->createUploadThread()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/server/screens/UploadScreen;


# direct methods
.method constructor <init>(Lepson/server/screens/UploadScreen;)V
    .locals 0

    .line 342
    iput-object p1, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .line 344
    iget-object v0, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lepson/server/screens/UploadScreen;->isSaved:Z

    .line 346
    :try_start_0
    sget-object v0, Lepson/print/CommonDefine;->EPSONIPRINT_FOLDER:Ljava/lang/String;

    invoke-static {v0}, Lepson/server/utils/MyUtility;->createFolder(Ljava/lang/String;)V

    .line 347
    iget-object v0, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {v0}, Lepson/server/screens/UploadScreen;->access$500(Lepson/server/screens/UploadScreen;)I

    move-result v0

    .line 349
    iget-object v2, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {v2}, Lepson/server/screens/UploadScreen;->access$800(Lepson/server/screens/UploadScreen;)Ljava/util/Vector;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    const/16 v3, 0x63

    const/4 v4, 0x1

    if-ne v2, v4, :cond_9

    .line 351
    iget-object v0, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {v0}, Lepson/server/screens/UploadScreen;->access$300(Lepson/server/screens/UploadScreen;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "PDF"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 352
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 354
    iget-object v1, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {v1}, Lepson/server/screens/UploadScreen;->access$800(Lepson/server/screens/UploadScreen;)Ljava/util/Vector;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/io/File;

    .line 355
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 358
    :cond_0
    new-instance v1, Lepson/scan/lib/libHaru;

    invoke-direct {v1}, Lepson/scan/lib/libHaru;-><init>()V

    .line 359
    new-instance v2, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lepson/print/CommonDefine;->EPSONIPRINT_FOLDER:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    iget-object v6, v6, Lepson/server/screens/UploadScreen;->currentFileName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, ".pdf"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 362
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v5, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {v5}, Lepson/server/screens/UploadScreen;->access$900(Lepson/server/screens/UploadScreen;)Z

    move-result v5

    if-nez v5, :cond_3

    const/4 v2, 0x1

    .line 366
    :goto_1
    new-instance v5, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lepson/print/CommonDefine;->EPSONIPRINT_FOLDER:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v7, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    iget-object v7, v7, Lepson/server/screens/UploadScreen;->currentFileName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, " ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, ".pdf"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 369
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_1

    move-object v2, v5

    goto :goto_2

    :cond_1
    if-lt v2, v3, :cond_2

    .line 374
    new-instance v2, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lepson/print/CommonDefine;->EPSONIPRINT_FOLDER:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    iget-object v6, v6, Lepson/server/screens/UploadScreen;->currentFileName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, " ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, ")"

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ".pdf"

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    goto :goto_2

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 383
    :cond_3
    :goto_2
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Lepson/scan/lib/libHaru;->createPDF(Ljava/util/ArrayList;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_16

    .line 384
    iget-object v0, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    iput-boolean v4, v0, Lepson/server/screens/UploadScreen;->isSaved:Z

    .line 386
    iget-object v0, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lepson/server/screens/UploadScreen;->addMediaStorage(Ljava/lang/String;)V

    goto/16 :goto_c

    :cond_4
    const-string v0, "createUploadThread"

    const-string v2, "1"

    .line 392
    invoke-static {v0, v2}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    iget-object v0, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    iget-object v2, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    .line 402
    invoke-static {v2}, Lepson/server/screens/UploadScreen;->access$800(Lepson/server/screens/UploadScreen;)Ljava/util/Vector;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    iget-object v5, v5, Lepson/server/screens/UploadScreen;->currentFileName:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, ".jpg"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 401
    invoke-static {v0, v1, v2}, Lepson/server/utils/MyUtility;->createTempFile(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    const-string v1, "createUploadThread"

    .line 403
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "2"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v0, :cond_16

    .line 406
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lepson/print/CommonDefine;->EPSONIPRINT_FOLDER:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    iget-object v5, v5, Lepson/server/screens/UploadScreen;->currentFileName:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, ".jpg"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-string v2, "createUploadThread"

    .line 407
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "3"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 409
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v2, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {v2}, Lepson/server/screens/UploadScreen;->access$900(Lepson/server/screens/UploadScreen;)Z

    move-result v2

    if-nez v2, :cond_7

    const/4 v1, 0x1

    .line 413
    :goto_3
    new-instance v2, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lepson/print/CommonDefine;->EPSONIPRINT_FOLDER:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    iget-object v6, v6, Lepson/server/screens/UploadScreen;->currentFileName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, " ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, ".jpg"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 416
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_5

    move-object v1, v2

    goto :goto_4

    :cond_5
    if-lt v1, v3, :cond_6

    .line 421
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lepson/print/CommonDefine;->EPSONIPRINT_FOLDER:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    iget-object v5, v5, Lepson/server/screens/UploadScreen;->currentFileName:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " ("

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ".jpg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    goto :goto_4

    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 430
    :cond_7
    :goto_4
    invoke-static {v0, v1}, Lepson/server/utils/MyUtility;->copy(Ljava/io/File;Ljava/io/File;)V

    .line 432
    iget-object v2, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lepson/server/screens/UploadScreen;->addMediaStorage(Ljava/lang/String;)V

    .line 434
    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 435
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 437
    :cond_8
    iget-object v0, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    iput-boolean v4, v0, Lepson/server/screens/UploadScreen;->isSaved:Z

    const-string v0, "createUploadThread"

    .line 438
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "4:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    iget-boolean v2, v2, Lepson/server/screens/UploadScreen;->isSaved:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_c

    .line 442
    :cond_9
    iget-object v2, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {v2}, Lepson/server/screens/UploadScreen;->access$900(Lepson/server/screens/UploadScreen;)Z

    move-result v2

    if-ne v2, v4, :cond_a

    const/4 v0, 0x1

    .line 447
    :cond_a
    iget-object v2, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {v2}, Lepson/server/screens/UploadScreen;->access$300(Lepson/server/screens/UploadScreen;)Ljava/lang/String;

    move-result-object v2

    const-string v5, "PDF"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 448
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 450
    iget-object v2, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {v2}, Lepson/server/screens/UploadScreen;->access$800(Lepson/server/screens/UploadScreen;)Ljava/util/Vector;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/io/File;

    .line 451
    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 454
    :cond_b
    new-instance v2, Lepson/scan/lib/libHaru;

    invoke-direct {v2}, Lepson/scan/lib/libHaru;-><init>()V

    .line 456
    iget-object v5, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {v5}, Lepson/server/screens/UploadScreen;->access$900(Lepson/server/screens/UploadScreen;)Z

    move-result v5

    if-ne v5, v4, :cond_c

    .line 457
    new-instance v5, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lepson/print/CommonDefine;->EPSONIPRINT_FOLDER:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v7, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    iget-object v7, v7, Lepson/server/screens/UploadScreen;->currentFileName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, ".pdf"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    goto :goto_6

    .line 460
    :cond_c
    new-instance v5, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lepson/print/CommonDefine;->EPSONIPRINT_FOLDER:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v7, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    iget-object v7, v7, Lepson/server/screens/UploadScreen;->currentFileName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v7, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    .line 461
    invoke-static {v7}, Lepson/server/screens/UploadScreen;->access$600(Lepson/server/screens/UploadScreen;)Lepson/print/Util/Utils;

    move-result-object v7

    invoke-virtual {v7, v0}, Lepson/print/Util/Utils;->editNumber(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, ".pdf"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 464
    :goto_6
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_f

    iget-object v6, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {v6}, Lepson/server/screens/UploadScreen;->access$900(Lepson/server/screens/UploadScreen;)Z

    move-result v6

    if-nez v6, :cond_f

    const/4 v5, 0x1

    .line 468
    :goto_7
    new-instance v6, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lepson/print/CommonDefine;->EPSONIPRINT_FOLDER:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v8, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    iget-object v8, v8, Lepson/server/screens/UploadScreen;->currentFileName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v8, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    .line 469
    invoke-static {v8}, Lepson/server/screens/UploadScreen;->access$600(Lepson/server/screens/UploadScreen;)Lepson/print/Util/Utils;

    move-result-object v8

    invoke-virtual {v8, v0}, Lepson/print/Util/Utils;->editNumber(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, " ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, ".pdf"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 471
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_d

    move-object v5, v6

    goto :goto_8

    :cond_d
    if-lt v5, v3, :cond_e

    .line 476
    new-instance v5, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lepson/print/CommonDefine;->EPSONIPRINT_FOLDER:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v7, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    iget-object v7, v7, Lepson/server/screens/UploadScreen;->currentFileName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v7, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    .line 477
    invoke-static {v7}, Lepson/server/screens/UploadScreen;->access$600(Lepson/server/screens/UploadScreen;)Lepson/print/Util/Utils;

    move-result-object v7

    invoke-virtual {v7, v0}, Lepson/print/Util/Utils;->editNumber(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " ("

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ")"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ".pdf"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    goto :goto_8

    :cond_e
    add-int/lit8 v5, v5, 0x1

    goto :goto_7

    .line 484
    :cond_f
    :goto_8
    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Lepson/scan/lib/libHaru;->createPDF(Ljava/util/ArrayList;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_16

    .line 485
    iget-object v0, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    iput-boolean v4, v0, Lepson/server/screens/UploadScreen;->isSaved:Z

    .line 487
    iget-object v0, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lepson/server/screens/UploadScreen;->addMediaStorage(Ljava/lang/String;)V

    goto/16 :goto_c

    .line 492
    :cond_10
    iget-object v2, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {v2}, Lepson/server/screens/UploadScreen;->access$800(Lepson/server/screens/UploadScreen;)Ljava/util/Vector;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_11
    :goto_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_16

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/io/File;

    .line 494
    iget-object v6, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    iput-boolean v4, v6, Lepson/server/screens/UploadScreen;->isSaved:Z

    .line 496
    iget-object v6, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    .line 497
    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    iget-object v8, v8, Lepson/server/screens/UploadScreen;->currentFileName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v8, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {v8}, Lepson/server/screens/UploadScreen;->access$600(Lepson/server/screens/UploadScreen;)Lepson/print/Util/Utils;

    move-result-object v8

    invoke-virtual {v8, v0}, Lepson/print/Util/Utils;->editNumber(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, ".jpg"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 496
    invoke-static {v6, v5, v7}, Lepson/server/utils/MyUtility;->createTempFile(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v5

    if-nez v5, :cond_12

    .line 499
    iget-object v0, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    iput-boolean v1, v0, Lepson/server/screens/UploadScreen;->isSaved:Z

    goto/16 :goto_c

    .line 503
    :cond_12
    new-instance v6, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lepson/print/CommonDefine;->EPSONIPRINT_FOLDER:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v8, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    iget-object v8, v8, Lepson/server/screens/UploadScreen;->currentFileName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v8, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    .line 504
    invoke-static {v8}, Lepson/server/screens/UploadScreen;->access$600(Lepson/server/screens/UploadScreen;)Lepson/print/Util/Utils;

    move-result-object v8

    invoke-virtual {v8, v0}, Lepson/print/Util/Utils;->editNumber(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, ".jpg"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 506
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_15

    iget-object v7, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {v7}, Lepson/server/screens/UploadScreen;->access$900(Lepson/server/screens/UploadScreen;)Z

    move-result v7

    if-nez v7, :cond_15

    const/4 v6, 0x1

    .line 510
    :goto_a
    new-instance v7, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lepson/print/CommonDefine;->EPSONIPRINT_FOLDER:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v9, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    iget-object v9, v9, Lepson/server/screens/UploadScreen;->currentFileName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v9, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    .line 511
    invoke-static {v9}, Lepson/server/screens/UploadScreen;->access$600(Lepson/server/screens/UploadScreen;)Lepson/print/Util/Utils;

    move-result-object v9

    invoke-virtual {v9, v0}, Lepson/print/Util/Utils;->editNumber(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, " ("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v9, ")"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, ".jpg"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 513
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_13

    move-object v6, v7

    goto :goto_b

    :cond_13
    if-lt v6, v3, :cond_14

    .line 518
    new-instance v6, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lepson/print/CommonDefine;->EPSONIPRINT_FOLDER:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v8, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    iget-object v8, v8, Lepson/server/screens/UploadScreen;->currentFileName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v8, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    .line 519
    invoke-static {v8}, Lepson/server/screens/UploadScreen;->access$600(Lepson/server/screens/UploadScreen;)Lepson/print/Util/Utils;

    move-result-object v8

    invoke-virtual {v8, v0}, Lepson/print/Util/Utils;->editNumber(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, " ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, ".jpg"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    goto :goto_b

    :cond_14
    add-int/lit8 v6, v6, 0x1

    goto :goto_a

    :cond_15
    :goto_b
    add-int/lit8 v0, v0, 0x1

    .line 527
    invoke-static {v5, v6}, Lepson/server/utils/MyUtility;->copy(Ljava/io/File;Ljava/io/File;)V

    .line 529
    iget-object v7, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Lepson/server/screens/UploadScreen;->addMediaStorage(Ljava/lang/String;)V

    .line 531
    invoke-virtual {v5}, Ljava/io/File;->isFile()Z

    move-result v6

    if-eqz v6, :cond_11

    .line 532
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    goto/16 :goto_9

    .line 538
    :cond_16
    :goto_c
    iget-object v0, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {v0}, Lepson/server/screens/UploadScreen;->access$800(Lepson/server/screens/UploadScreen;)Ljava/util/Vector;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_d

    :catch_0
    move-exception v0

    .line 552
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 556
    :goto_d
    iget-object v0, p0, Lepson/server/screens/UploadScreen$7;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {v0}, Lepson/server/screens/UploadScreen;->access$1100(Lepson/server/screens/UploadScreen;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x10

    const-wide/16 v2, 0x5dc

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method

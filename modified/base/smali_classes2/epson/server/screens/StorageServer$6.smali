.class Lepson/server/screens/StorageServer$6;
.super Ljava/lang/Object;
.source "StorageServer.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/server/screens/StorageServer;->initStorageItem(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/server/screens/StorageServer;

.field final synthetic val$goal:I


# direct methods
.method constructor <init>(Lepson/server/screens/StorageServer;I)V
    .locals 0

    .line 277
    iput-object p1, p0, Lepson/server/screens/StorageServer$6;->this$0:Lepson/server/screens/StorageServer;

    iput p2, p0, Lepson/server/screens/StorageServer$6;->val$goal:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .line 280
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v0, 0x14

    if-gt p1, v0, :cond_0

    .line 281
    iget-object p1, p0, Lepson/server/screens/StorageServer$6;->this$0:Lepson/server/screens/StorageServer;

    invoke-static {p1}, Lepson/server/screens/StorageServer;->access$100(Lepson/server/screens/StorageServer;)V

    return-void

    .line 285
    :cond_0
    iget p1, p0, Lepson/server/screens/StorageServer$6;->val$goal:I

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 294
    :pswitch_0
    sget-object p1, Lcom/epson/iprint/storage/StorageServiceClient;->STORAGE_BOX:Ljava/lang/String;

    .line 295
    sget-object v0, Lcom/epson/iprint/storage/StorageProcessActivity$ProcessType;->UPLOAD:Lcom/epson/iprint/storage/StorageProcessActivity$ProcessType;

    .line 296
    iget-object v1, p0, Lepson/server/screens/StorageServer$6;->this$0:Lepson/server/screens/StorageServer;

    invoke-virtual {v1}, Lepson/server/screens/StorageServer;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p1, v0}, Lcom/epson/iprint/storage/StorageProcessActivity;->getProcessIntent(Landroid/content/Context;Ljava/lang/String;Lcom/epson/iprint/storage/StorageProcessActivity$ProcessType;)Landroid/content/Intent;

    move-result-object p1

    const-string v0, "Extra.Uploadfile.List"

    .line 297
    iget-object v1, p0, Lepson/server/screens/StorageServer$6;->this$0:Lepson/server/screens/StorageServer;

    invoke-static {v1}, Lepson/server/screens/StorageServer;->access$000(Lepson/server/screens/StorageServer;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 298
    iget-object v0, p0, Lepson/server/screens/StorageServer$6;->this$0:Lepson/server/screens/StorageServer;

    invoke-virtual {v0, p1}, Lepson/server/screens/StorageServer;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 288
    :pswitch_1
    iget-object p1, p0, Lepson/server/screens/StorageServer$6;->this$0:Lepson/server/screens/StorageServer;

    invoke-virtual {p1}, Lepson/server/screens/StorageServer;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    sget-object v0, Lcom/epson/iprint/storage/StorageServiceClient;->STORAGE_BOX:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/epson/iprint/storage/StorageProcessDownloadActivity;->getStartIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p1

    .line 289
    iget-object v0, p0, Lepson/server/screens/StorageServer$6;->this$0:Lepson/server/screens/StorageServer;

    invoke-virtual {v0, p1}, Lepson/server/screens/StorageServer;->startActivity(Landroid/content/Intent;)V

    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

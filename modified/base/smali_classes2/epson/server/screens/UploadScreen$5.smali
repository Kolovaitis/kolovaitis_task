.class Lepson/server/screens/UploadScreen$5;
.super Ljava/lang/Object;
.source "UploadScreen.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/server/screens/UploadScreen;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/server/screens/UploadScreen;


# direct methods
.method constructor <init>(Lepson/server/screens/UploadScreen;)V
    .locals 0

    .line 178
    iput-object p1, p0, Lepson/server/screens/UploadScreen$5;->this$0:Lepson/server/screens/UploadScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .line 181
    new-instance p1, Landroid/content/Intent;

    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    .line 185
    :try_start_0
    iget-object v0, p0, Lepson/server/screens/UploadScreen$5;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {v0}, Lepson/server/screens/UploadScreen;->access$000(Lepson/server/screens/UploadScreen;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lepson/print/fileBrower;->isAvailableFileName(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    .line 186
    iget-object p1, p0, Lepson/server/screens/UploadScreen$5;->this$0:Lepson/server/screens/UploadScreen;

    invoke-virtual {p1}, Lepson/server/screens/UploadScreen;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const v0, 0x7f0e04b4

    invoke-static {p1, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    return-void

    :cond_0
    const-string v0, "android.intent.action.SEND"

    .line 193
    invoke-virtual {p1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 196
    iget-object v0, p0, Lepson/server/screens/UploadScreen$5;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {v0}, Lepson/server/screens/UploadScreen;->access$300(Lepson/server/screens/UploadScreen;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "PDF"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 198
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 200
    iget-object v1, p0, Lepson/server/screens/UploadScreen$5;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {v1}, Lepson/server/screens/UploadScreen;->access$400(Lepson/server/screens/UploadScreen;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 201
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 204
    :cond_1
    new-instance v1, Lepson/scan/lib/libHaru;

    invoke-direct {v1}, Lepson/scan/lib/libHaru;-><init>()V

    .line 205
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lepson/server/screens/UploadScreen$5;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {v3}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v3

    invoke-virtual {v3}, Lepson/common/ExternalFileUtils;->getScannedImageDir()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lepson/server/screens/UploadScreen$5;->this$0:Lepson/server/screens/UploadScreen;

    .line 206
    invoke-static {v5}, Lepson/server/screens/UploadScreen;->access$000(Lepson/server/screens/UploadScreen;)Landroid/widget/EditText;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, ".pdf"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Lepson/scan/lib/libHaru;->createPDF(Ljava/util/ArrayList;Ljava/lang/String;)I

    const-string v0, "android.intent.extra.STREAM"

    .line 209
    iget-object v1, p0, Lepson/server/screens/UploadScreen$5;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {v1, v2}, Lepson/provider/ScannedFileProvider;->getUriForFile(Landroid/content/Context;Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v0, "application/pdf"

    .line 211
    invoke-virtual {p1, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_5

    .line 222
    :cond_2
    iget-object v0, p0, Lepson/server/screens/UploadScreen$5;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {v0}, Lepson/server/screens/UploadScreen;->access$000(Lepson/server/screens/UploadScreen;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "EPSON"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lepson/server/screens/UploadScreen$5;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {v3}, Lepson/server/screens/UploadScreen;->access$600(Lepson/server/screens/UploadScreen;)Lepson/print/Util/Utils;

    move-result-object v3

    iget-object v4, p0, Lepson/server/screens/UploadScreen$5;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {v4}, Lepson/server/screens/UploadScreen;->access$500(Lepson/server/screens/UploadScreen;)I

    move-result v4

    invoke-virtual {v3, v4}, Lepson/print/Util/Utils;->editNumber(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v2, 0x0

    if-nez v0, :cond_3

    .line 225
    iget-object v0, p0, Lepson/server/screens/UploadScreen$5;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {v0}, Lepson/server/screens/UploadScreen;->access$000(Lepson/server/screens/UploadScreen;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    const/4 v0, 0x1

    goto :goto_1

    .line 226
    :cond_3
    iget-object v0, p0, Lepson/server/screens/UploadScreen$5;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {v0}, Lepson/server/screens/UploadScreen;->access$400(Lepson/server/screens/UploadScreen;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v1, :cond_4

    .line 229
    iget-object v0, p0, Lepson/server/screens/UploadScreen$5;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {v0}, Lepson/server/screens/UploadScreen;->access$000(Lepson/server/screens/UploadScreen;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    const-string v0, "EPSON"

    move-object v3, v0

    const/4 v0, 0x0

    .line 235
    :goto_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 238
    iget-object v5, p0, Lepson/server/screens/UploadScreen$5;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {v5}, Lepson/server/screens/UploadScreen;->access$500(Lepson/server/screens/UploadScreen;)I

    move-result v5

    .line 240
    iget-object v6, p0, Lepson/server/screens/UploadScreen$5;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {v6}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v6

    iget-object v7, p0, Lepson/server/screens/UploadScreen$5;->this$0:Lepson/server/screens/UploadScreen;

    .line 241
    invoke-static {v7}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v7

    invoke-virtual {v7}, Lepson/common/ExternalFileUtils;->getScannedImageDir()Ljava/lang/String;

    move-result-object v7

    .line 240
    invoke-virtual {v6, v7}, Lepson/common/ExternalFileUtils;->createTempFolder(Ljava/lang/String;)Z

    .line 243
    iget-object v6, p0, Lepson/server/screens/UploadScreen$5;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {v6}, Lepson/server/screens/UploadScreen;->access$400(Lepson/server/screens/UploadScreen;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ne v6, v1, :cond_5

    .line 244
    iget-object v0, p0, Lepson/server/screens/UploadScreen$5;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {v0}, Lepson/server/screens/UploadScreen;->access$400(Lepson/server/screens/UploadScreen;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 245
    iget-object v5, p0, Lepson/server/screens/UploadScreen$5;->this$0:Lepson/server/screens/UploadScreen;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ".jpg"

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v0, v3}, Lepson/server/utils/MyUtility;->createTempFile(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 247
    iget-object v3, p0, Lepson/server/screens/UploadScreen$5;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {v3, v0}, Lepson/provider/ScannedFileProvider;->getUriForFile(Landroid/content/Context;Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 248
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_5
    if-ne v0, v1, :cond_6

    const/4 v5, 0x1

    :cond_6
    const/4 v0, 0x0

    .line 254
    :goto_2
    iget-object v6, p0, Lepson/server/screens/UploadScreen$5;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {v6}, Lepson/server/screens/UploadScreen;->access$400(Lepson/server/screens/UploadScreen;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v0, v6, :cond_7

    .line 255
    iget-object v6, p0, Lepson/server/screens/UploadScreen$5;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {v6}, Lepson/server/screens/UploadScreen;->access$400(Lepson/server/screens/UploadScreen;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 257
    iget-object v7, p0, Lepson/server/screens/UploadScreen$5;->this$0:Lepson/server/screens/UploadScreen;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v9, p0, Lepson/server/screens/UploadScreen$5;->this$0:Lepson/server/screens/UploadScreen;

    .line 258
    invoke-static {v9}, Lepson/server/screens/UploadScreen;->access$600(Lepson/server/screens/UploadScreen;)Lepson/print/Util/Utils;

    move-result-object v9

    invoke-virtual {v9, v5}, Lepson/print/Util/Utils;->editNumber(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, ".jpg"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 257
    invoke-static {v7, v6, v8}, Lepson/server/utils/MyUtility;->createTempFile(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v6

    add-int/lit8 v5, v5, 0x1

    .line 260
    iget-object v7, p0, Lepson/server/screens/UploadScreen$5;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {v7, v6}, Lepson/provider/ScannedFileProvider;->getUriForFile(Landroid/content/Context;Ljava/io/File;)Landroid/net/Uri;

    move-result-object v6

    .line 261
    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 265
    :cond_7
    :goto_3
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gt v0, v1, :cond_8

    const-string v0, "android.intent.extra.STREAM"

    .line 266
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto :goto_4

    :cond_8
    const-string v0, "android.intent.action.SEND_MULTIPLE"

    .line 268
    invoke-virtual {p1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "android.intent.extra.STREAM"

    .line 269
    invoke-virtual {p1, v0, v4}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    :goto_4
    const-string v0, "image/jpg"

    .line 271
    invoke-virtual {p1, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 273
    :goto_5
    iget-object v0, p0, Lepson/server/screens/UploadScreen$5;->this$0:Lepson/server/screens/UploadScreen;

    iget-object v1, p0, Lepson/server/screens/UploadScreen$5;->this$0:Lepson/server/screens/UploadScreen;

    const v2, 0x7f0e0401

    invoke-virtual {v1, v2}, Lepson/server/screens/UploadScreen;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object p1

    invoke-virtual {v0, p1}, Lepson/server/screens/UploadScreen;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_6

    :catch_0
    move-exception p1

    .line 276
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_6
    return-void
.end method

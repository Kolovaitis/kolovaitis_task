.class Lepson/server/screens/StorageServer$8;
.super Ljava/lang/Object;
.source "StorageServer.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/server/screens/StorageServer;->initStorageItem(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/server/screens/StorageServer;


# direct methods
.method constructor <init>(Lepson/server/screens/StorageServer;)V
    .locals 0

    .line 338
    iput-object p1, p0, Lepson/server/screens/StorageServer$8;->this$0:Lepson/server/screens/StorageServer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .line 342
    invoke-static {}, Lepson/print/ActivityRequestPermissions;->isRuntimePermissionSupported()Z

    move-result p1

    if-eqz p1, :cond_0

    const-string p1, "android.permission.WRITE_EXTERNAL_STORAGE"

    .line 343
    filled-new-array {p1}, [Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x2

    .line 344
    new-array v1, v0, [Ljava/lang/String;

    iget-object v2, p0, Lepson/server/screens/StorageServer$8;->this$0:Lepson/server/screens/StorageServer;

    const v3, 0x7f0e0422

    invoke-virtual {v2, v3}, Lepson/server/screens/StorageServer;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    aput-object v2, v1, v4

    iget-object v2, p0, Lepson/server/screens/StorageServer$8;->this$0:Lepson/server/screens/StorageServer;

    invoke-virtual {v2, v3}, Lepson/server/screens/StorageServer;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    .line 345
    new-array v0, v0, [Ljava/lang/String;

    iget-object v2, p0, Lepson/server/screens/StorageServer$8;->this$0:Lepson/server/screens/StorageServer;

    const v5, 0x7f0e0420

    .line 346
    invoke-virtual {v2, v5}, Lepson/server/screens/StorageServer;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Lepson/print/ActivityRequestPermissions$DialogParameter;->setMessage2(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v4

    iget-object v2, p0, Lepson/server/screens/StorageServer$8;->this$0:Lepson/server/screens/StorageServer;

    .line 347
    invoke-virtual {v2, v5}, Lepson/server/screens/StorageServer;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lepson/server/screens/StorageServer$8;->this$0:Lepson/server/screens/StorageServer;

    const v7, 0x7f0e0424

    invoke-virtual {v6, v7}, Lepson/server/screens/StorageServer;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v5, v6}, Lepson/print/ActivityRequestPermissions$DialogParameter;->setMessage3A(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v3

    .line 353
    new-instance v2, Lepson/print/ActivityRequestPermissions$Permission;

    aget-object v4, p1, v4

    invoke-direct {v2, v4, v1, v0}, Lepson/print/ActivityRequestPermissions$Permission;-><init>(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V

    .line 355
    iget-object v0, p0, Lepson/server/screens/StorageServer$8;->this$0:Lepson/server/screens/StorageServer;

    invoke-static {v0, p1}, Lepson/print/ActivityRequestPermissions;->checkPermission(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 356
    iget-object p1, p0, Lepson/server/screens/StorageServer$8;->this$0:Lepson/server/screens/StorageServer;

    invoke-static {p1, v2, v3}, Lepson/print/ActivityRequestPermissions;->requestPermission(Landroid/app/Activity;Lepson/print/ActivityRequestPermissions$Permission;I)V

    return-void

    .line 361
    :cond_0
    iget-object p1, p0, Lepson/server/screens/StorageServer$8;->this$0:Lepson/server/screens/StorageServer;

    invoke-virtual {p1}, Lepson/server/screens/StorageServer;->saveLocalMemory()V

    return-void
.end method

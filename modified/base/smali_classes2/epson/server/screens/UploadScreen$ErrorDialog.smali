.class Lepson/server/screens/UploadScreen$ErrorDialog;
.super Landroid/app/Dialog;
.source "UploadScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/server/screens/UploadScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ErrorDialog"
.end annotation


# instance fields
.field private mErrorCode:I

.field private mExtraCode:I

.field private mIsCloseDialog:Z

.field final synthetic this$0:Lepson/server/screens/UploadScreen;


# direct methods
.method public constructor <init>(Lepson/server/screens/UploadScreen;Landroid/content/Context;IIIZ)V
    .locals 0

    .line 615
    iput-object p1, p0, Lepson/server/screens/UploadScreen$ErrorDialog;->this$0:Lepson/server/screens/UploadScreen;

    .line 616
    invoke-direct {p0, p2, p3}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 617
    iput p4, p0, Lepson/server/screens/UploadScreen$ErrorDialog;->mErrorCode:I

    .line 618
    iput-boolean p6, p0, Lepson/server/screens/UploadScreen$ErrorDialog;->mIsCloseDialog:Z

    .line 619
    iput p5, p0, Lepson/server/screens/UploadScreen$ErrorDialog;->mExtraCode:I

    return-void
.end method

.method static synthetic access$1200(Lepson/server/screens/UploadScreen$ErrorDialog;)Z
    .locals 0

    .line 610
    iget-boolean p0, p0, Lepson/server/screens/UploadScreen$ErrorDialog;->mIsCloseDialog:Z

    return p0
.end method

.method static synthetic access$1300(Lepson/server/screens/UploadScreen$ErrorDialog;)I
    .locals 0

    .line 610
    iget p0, p0, Lepson/server/screens/UploadScreen$ErrorDialog;->mExtraCode:I

    return p0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    const/4 v0, 0x1

    .line 624
    invoke-virtual {p0, v0}, Lepson/server/screens/UploadScreen$ErrorDialog;->requestWindowFeature(I)Z

    .line 625
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 626
    invoke-virtual {p0}, Lepson/server/screens/UploadScreen$ErrorDialog;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object p1

    const v0, 0x7f0a0093

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    .line 627
    iget v0, p0, Lepson/server/screens/UploadScreen$ErrorDialog;->mErrorCode:I

    const/16 v1, 0x10

    const v2, 0x7f08010a

    const v3, 0x7f08010e

    if-eq v0, v1, :cond_0

    const v1, 0x7f0e053f

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    .line 645
    :pswitch_0
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v3, p0, Lepson/server/screens/UploadScreen$ErrorDialog;->this$0:Lepson/server/screens/UploadScreen;

    invoke-virtual {v3, v1}, Lepson/server/screens/UploadScreen;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 646
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lepson/server/screens/UploadScreen$ErrorDialog;->this$0:Lepson/server/screens/UploadScreen;

    const v2, 0x7f0e04b4

    invoke-virtual {v1, v2}, Lepson/server/screens/UploadScreen;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 629
    :pswitch_1
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lepson/server/screens/UploadScreen$ErrorDialog;->this$0:Lepson/server/screens/UploadScreen;

    const v3, 0x7f0e036b

    invoke-virtual {v1, v3}, Lepson/server/screens/UploadScreen;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 630
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lepson/server/screens/UploadScreen$ErrorDialog;->this$0:Lepson/server/screens/UploadScreen;

    const v2, 0x7f0e03d8

    invoke-virtual {v1, v2}, Lepson/server/screens/UploadScreen;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 637
    :pswitch_2
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v3, p0, Lepson/server/screens/UploadScreen$ErrorDialog;->this$0:Lepson/server/screens/UploadScreen;

    invoke-virtual {v3, v1}, Lepson/server/screens/UploadScreen;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 638
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lepson/server/screens/UploadScreen$ErrorDialog;->this$0:Lepson/server/screens/UploadScreen;

    const v2, 0x7f0e053e

    invoke-virtual {v1, v2}, Lepson/server/screens/UploadScreen;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 633
    :pswitch_3
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lepson/server/screens/UploadScreen$ErrorDialog;->this$0:Lepson/server/screens/UploadScreen;

    const v3, 0x7f0e0324

    invoke-virtual {v1, v3}, Lepson/server/screens/UploadScreen;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 634
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lepson/server/screens/UploadScreen$ErrorDialog;->this$0:Lepson/server/screens/UploadScreen;

    const v2, 0x7f0e0323

    invoke-virtual {v1, v2}, Lepson/server/screens/UploadScreen;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 641
    :pswitch_4
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lepson/server/screens/UploadScreen$ErrorDialog;->this$0:Lepson/server/screens/UploadScreen;

    const v3, 0x7f0e02ae

    invoke-virtual {v1, v3}, Lepson/server/screens/UploadScreen;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 642
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lepson/server/screens/UploadScreen$ErrorDialog;->this$0:Lepson/server/screens/UploadScreen;

    const v2, 0x7f0e02ad

    invoke-virtual {v1, v2}, Lepson/server/screens/UploadScreen;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 649
    :cond_0
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lepson/server/screens/UploadScreen$ErrorDialog;->this$0:Lepson/server/screens/UploadScreen;

    const v3, 0x7f0e053c

    invoke-virtual {v1, v3}, Lepson/server/screens/UploadScreen;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 650
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lepson/server/screens/UploadScreen$ErrorDialog;->this$0:Lepson/server/screens/UploadScreen;

    const v2, 0x7f0e053b

    invoke-virtual {v1, v2}, Lepson/server/screens/UploadScreen;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    const v0, 0x7f080233

    .line 656
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lepson/server/screens/UploadScreen$ErrorDialog$1;

    invoke-direct {v1, p0}, Lepson/server/screens/UploadScreen$ErrorDialog$1;-><init>(Lepson/server/screens/UploadScreen$ErrorDialog;)V

    .line 657
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 674
    invoke-virtual {p0, p1}, Lepson/server/screens/UploadScreen$ErrorDialog;->setContentView(Landroid/view/View;)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 0

    .line 679
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result p2

    if-nez p2, :cond_0

    const/16 p2, 0x54

    if-ne p1, p2, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

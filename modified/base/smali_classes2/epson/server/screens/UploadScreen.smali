.class public Lepson/server/screens/UploadScreen;
.super Lepson/print/ActivityIACommon;
.source "UploadScreen.java"

# interfaces
.implements Lepson/print/CommonDefine;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/server/screens/UploadScreen$ErrorDialog;
    }
.end annotation


# static fields
.field private static final LOCAL_SAVED:I = 0x10

.field private static final SET_SCREENSTATE:I = 0x0

.field private static final TAG:Ljava/lang/String; = "UploadScreen"


# instance fields
.field private final MAX_COPY_COUNT:I

.field private chooseFileType:Landroid/widget/RadioGroup;

.field private count:I

.field currentFileName:Ljava/lang/String;

.field errorDialog:Landroid/app/Dialog;

.field isSaved:Z

.field private mClear:Landroid/widget/Button;

.field private mFileName:Landroid/widget/EditText;

.field private mFileType:Ljava/lang/String;

.field private mHandler:Landroid/os/Handler;

.field private mIsRename:Z

.field private mListFilePath:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSaveBtn:Landroid/widget/Button;

.field private mSaveFiles:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private utils:Lepson/print/Util/Utils;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 55
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    const-string v0, "PDF"

    .line 58
    iput-object v0, p0, Lepson/server/screens/UploadScreen;->mFileType:Ljava/lang/String;

    .line 59
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lepson/server/screens/UploadScreen;->mSaveFiles:Ljava/util/Vector;

    .line 64
    new-instance v0, Lepson/print/Util/Utils;

    invoke-direct {v0}, Lepson/print/Util/Utils;-><init>()V

    iput-object v0, p0, Lepson/server/screens/UploadScreen;->utils:Lepson/print/Util/Utils;

    const/4 v0, 0x0

    .line 68
    iput-boolean v0, p0, Lepson/server/screens/UploadScreen;->isSaved:Z

    const/16 v0, 0x63

    .line 70
    iput v0, p0, Lepson/server/screens/UploadScreen;->MAX_COPY_COUNT:I

    .line 578
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lepson/server/screens/UploadScreen$8;

    invoke-direct {v1, p0}, Lepson/server/screens/UploadScreen$8;-><init>(Lepson/server/screens/UploadScreen;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lepson/server/screens/UploadScreen;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lepson/server/screens/UploadScreen;)Landroid/widget/EditText;
    .locals 0

    .line 55
    iget-object p0, p0, Lepson/server/screens/UploadScreen;->mFileName:Landroid/widget/EditText;

    return-object p0
.end method

.method static synthetic access$100(Lepson/server/screens/UploadScreen;)Landroid/widget/Button;
    .locals 0

    .line 55
    iget-object p0, p0, Lepson/server/screens/UploadScreen;->mClear:Landroid/widget/Button;

    return-object p0
.end method

.method static synthetic access$1000(Lepson/server/screens/UploadScreen;)V
    .locals 0

    .line 55
    invoke-direct {p0}, Lepson/server/screens/UploadScreen;->createUploadThread()V

    return-void
.end method

.method static synthetic access$1100(Lepson/server/screens/UploadScreen;)Landroid/os/Handler;
    .locals 0

    .line 55
    iget-object p0, p0, Lepson/server/screens/UploadScreen;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static synthetic access$200(Lepson/server/screens/UploadScreen;)Landroid/widget/Button;
    .locals 0

    .line 55
    iget-object p0, p0, Lepson/server/screens/UploadScreen;->mSaveBtn:Landroid/widget/Button;

    return-object p0
.end method

.method static synthetic access$300(Lepson/server/screens/UploadScreen;)Ljava/lang/String;
    .locals 0

    .line 55
    iget-object p0, p0, Lepson/server/screens/UploadScreen;->mFileType:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$302(Lepson/server/screens/UploadScreen;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 55
    iput-object p1, p0, Lepson/server/screens/UploadScreen;->mFileType:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$400(Lepson/server/screens/UploadScreen;)Ljava/util/ArrayList;
    .locals 0

    .line 55
    iget-object p0, p0, Lepson/server/screens/UploadScreen;->mListFilePath:Ljava/util/ArrayList;

    return-object p0
.end method

.method static synthetic access$500(Lepson/server/screens/UploadScreen;)I
    .locals 0

    .line 55
    iget p0, p0, Lepson/server/screens/UploadScreen;->count:I

    return p0
.end method

.method static synthetic access$502(Lepson/server/screens/UploadScreen;I)I
    .locals 0

    .line 55
    iput p1, p0, Lepson/server/screens/UploadScreen;->count:I

    return p1
.end method

.method static synthetic access$600(Lepson/server/screens/UploadScreen;)Lepson/print/Util/Utils;
    .locals 0

    .line 55
    iget-object p0, p0, Lepson/server/screens/UploadScreen;->utils:Lepson/print/Util/Utils;

    return-object p0
.end method

.method static synthetic access$700(Lepson/server/screens/UploadScreen;Ljava/lang/Boolean;)V
    .locals 0

    .line 55
    invoke-direct {p0, p1}, Lepson/server/screens/UploadScreen;->setScreenState(Ljava/lang/Boolean;)V

    return-void
.end method

.method static synthetic access$800(Lepson/server/screens/UploadScreen;)Ljava/util/Vector;
    .locals 0

    .line 55
    iget-object p0, p0, Lepson/server/screens/UploadScreen;->mSaveFiles:Ljava/util/Vector;

    return-object p0
.end method

.method static synthetic access$900(Lepson/server/screens/UploadScreen;)Z
    .locals 0

    .line 55
    iget-boolean p0, p0, Lepson/server/screens/UploadScreen;->mIsRename:Z

    return p0
.end method

.method static synthetic access$902(Lepson/server/screens/UploadScreen;Z)Z
    .locals 0

    .line 55
    iput-boolean p1, p0, Lepson/server/screens/UploadScreen;->mIsRename:Z

    return p1
.end method

.method private copyFile(Ljava/io/File;Ljava/io/File;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 703
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v0}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object p1

    .line 704
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object p2

    .line 706
    invoke-virtual {p1}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v4

    const-wide/16 v2, 0x0

    move-object v1, p1

    move-object v6, p2

    invoke-virtual/range {v1 .. v6}, Ljava/nio/channels/FileChannel;->transferTo(JJLjava/nio/channels/WritableByteChannel;)J

    .line 708
    invoke-virtual {p1}, Ljava/nio/channels/FileChannel;->close()V

    .line 709
    invoke-virtual {p2}, Ljava/nio/channels/FileChannel;->close()V

    return-void
.end method

.method private createUploadThread()V
    .locals 1

    .line 342
    new-instance v0, Lepson/server/screens/UploadScreen$7;

    invoke-direct {v0, p0}, Lepson/server/screens/UploadScreen$7;-><init>(Lepson/server/screens/UploadScreen;)V

    .line 560
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method private setScreenState(Ljava/lang/Boolean;)V
    .locals 2

    .line 690
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    const v0, 0x7f08019f

    if-nez p1, :cond_0

    .line 691
    invoke-virtual {p0, v0}, Lepson/server/screens/UploadScreen;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 692
    iget-object p1, p0, Lepson/server/screens/UploadScreen;->mSaveBtn:Landroid/widget/Button;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 693
    iget-object p1, p0, Lepson/server/screens/UploadScreen;->chooseFileType:Landroid/widget/RadioGroup;

    invoke-virtual {p1, v0}, Landroid/widget/RadioGroup;->setEnabled(Z)V

    goto :goto_0

    .line 695
    :cond_0
    iget-object p1, p0, Lepson/server/screens/UploadScreen;->mSaveBtn:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 696
    iget-object p1, p0, Lepson/server/screens/UploadScreen;->chooseFileType:Landroid/widget/RadioGroup;

    invoke-virtual {p1, v1}, Landroid/widget/RadioGroup;->setEnabled(Z)V

    .line 697
    invoke-virtual {p0, v0}, Lepson/server/screens/UploadScreen;->findViewById(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void
.end method


# virtual methods
.method addMediaStorage(Ljava/lang/String;)V
    .locals 3

    const-string v0, "UploadScreen"

    .line 572
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MediaScannerConnection.scanFile path="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 573
    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 p1, 0x0

    invoke-static {p0, v0, p1, p1}, Landroid/media/MediaScannerConnection;->scanFile(Landroid/content/Context;[Ljava/lang/String;[Ljava/lang/String;Landroid/media/MediaScannerConnection$OnScanCompletedListener;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .line 76
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0a00c6

    .line 78
    invoke-virtual {p0, p1}, Lepson/server/screens/UploadScreen;->setContentView(I)V

    const-string p1, ""

    const/4 v0, 0x1

    .line 81
    invoke-virtual {p0, p1, v0}, Lepson/server/screens/UploadScreen;->setActionBar(Ljava/lang/String;Z)V

    const p1, 0x7f0802cc

    .line 83
    invoke-virtual {p0, p1}, Lepson/server/screens/UploadScreen;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lepson/server/screens/UploadScreen;->mSaveBtn:Landroid/widget/Button;

    const p1, 0x7f08037a

    .line 85
    invoke-virtual {p0, p1}, Lepson/server/screens/UploadScreen;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/EditText;

    iput-object p1, p0, Lepson/server/screens/UploadScreen;->mFileName:Landroid/widget/EditText;

    .line 86
    new-array p1, v0, [Landroid/text/InputFilter;

    .line 87
    new-instance v0, Landroid/text/InputFilter$LengthFilter;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    const/4 v1, 0x0

    aput-object v0, p1, v1

    .line 88
    iget-object v0, p0, Lepson/server/screens/UploadScreen;->mFileName:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 90
    iget-object p1, p0, Lepson/server/screens/UploadScreen;->mFileName:Landroid/widget/EditText;

    new-instance v0, Lepson/server/screens/UploadScreen$1;

    invoke-direct {v0, p0}, Lepson/server/screens/UploadScreen$1;-><init>(Lepson/server/screens/UploadScreen;)V

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    const p1, 0x7f0800c5

    .line 99
    invoke-virtual {p0, p1}, Lepson/server/screens/UploadScreen;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lepson/server/screens/UploadScreen;->mClear:Landroid/widget/Button;

    .line 101
    invoke-static {p0}, Lepson/print/ScanFileNumber;->getCount(Landroid/content/Context;)I

    move-result p1

    iput p1, p0, Lepson/server/screens/UploadScreen;->count:I

    .line 102
    iget-object p1, p0, Lepson/server/screens/UploadScreen;->mFileName:Landroid/widget/EditText;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "EPSON"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lepson/server/screens/UploadScreen;->utils:Lepson/print/Util/Utils;

    iget v3, p0, Lepson/server/screens/UploadScreen;->count:I

    invoke-virtual {v2, v3}, Lepson/print/Util/Utils;->editNumber(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 103
    iget-object p1, p0, Lepson/server/screens/UploadScreen;->mFileName:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    if-lez p1, :cond_0

    .line 104
    iget-object p1, p0, Lepson/server/screens/UploadScreen;->mClear:Landroid/widget/Button;

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 107
    :cond_0
    iget-object p1, p0, Lepson/server/screens/UploadScreen;->mFileName:Landroid/widget/EditText;

    new-instance v0, Lepson/server/screens/UploadScreen$2;

    invoke-direct {v0, p0}, Lepson/server/screens/UploadScreen$2;-><init>(Lepson/server/screens/UploadScreen;)V

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 126
    iget-object p1, p0, Lepson/server/screens/UploadScreen;->mClear:Landroid/widget/Button;

    new-instance v0, Lepson/server/screens/UploadScreen$3;

    invoke-direct {v0, p0}, Lepson/server/screens/UploadScreen$3;-><init>(Lepson/server/screens/UploadScreen;)V

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f080148

    .line 132
    invoke-virtual {p0, p1}, Lepson/server/screens/UploadScreen;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/RadioGroup;

    iput-object p1, p0, Lepson/server/screens/UploadScreen;->chooseFileType:Landroid/widget/RadioGroup;

    .line 133
    iget-object p1, p0, Lepson/server/screens/UploadScreen;->chooseFileType:Landroid/widget/RadioGroup;

    const v0, 0x7f08025a

    invoke-virtual {p1, v0}, Landroid/widget/RadioGroup;->check(I)V

    const-string p1, "PDF"

    .line 134
    iput-object p1, p0, Lepson/server/screens/UploadScreen;->mFileType:Ljava/lang/String;

    .line 136
    iget-object p1, p0, Lepson/server/screens/UploadScreen;->chooseFileType:Landroid/widget/RadioGroup;

    new-instance v0, Lepson/server/screens/UploadScreen$4;

    invoke-direct {v0, p0}, Lepson/server/screens/UploadScreen$4;-><init>(Lepson/server/screens/UploadScreen;)V

    invoke-virtual {p1, v0}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 153
    invoke-virtual {p0}, Lepson/server/screens/UploadScreen;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "SAVING_FILE_PATH"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object p1

    iput-object p1, p0, Lepson/server/screens/UploadScreen;->mListFilePath:Ljava/util/ArrayList;

    const-wide/16 v2, 0x0

    .line 155
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    .line 157
    iget-object v0, p0, Lepson/server/screens/UploadScreen;->mListFilePath:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 158
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 159
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v2

    add-long/2addr v4, v2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    goto :goto_0

    .line 162
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x400

    div-long/2addr v2, v4

    long-to-float p1, v2

    const/high16 v0, 0x43fa0000    # 500.0f

    const/4 v2, 0x2

    const v3, 0x7f080145

    cmpl-float v0, p1, v0

    if-lez v0, :cond_2

    .line 165
    invoke-virtual {p0, v3}, Lepson/server/screens/UploadScreen;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/high16 v4, 0x44800000    # 1024.0f

    div-float/2addr p1, v4

    float-to-double v4, p1

    invoke-static {v4, v5, v2}, Lepson/server/utils/MyUtility;->mathRound(DI)D

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string p1, "MB"

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 167
    :cond_2
    invoke-virtual {p0, v3}, Lepson/server/screens/UploadScreen;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    float-to-double v4, p1

    invoke-static {v4, v5, v2}, Lepson/server/utils/MyUtility;->mathRound(DI)D

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string p1, "KB"

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 171
    :goto_1
    invoke-virtual {p0}, Lepson/server/screens/UploadScreen;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "for"

    const/4 v2, 0x7

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p1

    packed-switch p1, :pswitch_data_0

    goto :goto_2

    :pswitch_0
    const p1, 0x7f0e03a2

    .line 284
    invoke-virtual {p0, p1}, Lepson/server/screens/UploadScreen;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lepson/server/screens/UploadScreen;->setTitle(Ljava/lang/CharSequence;)V

    const p1, 0x7f08037d

    .line 285
    invoke-virtual {p0, p1}, Lepson/server/screens/UploadScreen;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget-object v2, Lepson/server/screens/UploadScreen;->EPSONIPRINT_FOLDER:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 286
    invoke-virtual {p0, p1}, Lepson/server/screens/UploadScreen;->findViewById(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 287
    iget-object p1, p0, Lepson/server/screens/UploadScreen;->mSaveBtn:Landroid/widget/Button;

    const v0, 0x7f0e04fc

    invoke-virtual {p0, v0}, Lepson/server/screens/UploadScreen;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 289
    iget-object p1, p0, Lepson/server/screens/UploadScreen;->mSaveBtn:Landroid/widget/Button;

    new-instance v0, Lepson/server/screens/UploadScreen$6;

    invoke-direct {v0, p0}, Lepson/server/screens/UploadScreen$6;-><init>(Lepson/server/screens/UploadScreen;)V

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2

    :pswitch_1
    const p1, 0x7f0e0401

    .line 176
    invoke-virtual {p0, p1}, Lepson/server/screens/UploadScreen;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lepson/server/screens/UploadScreen;->setTitle(Ljava/lang/CharSequence;)V

    .line 177
    iget-object v0, p0, Lepson/server/screens/UploadScreen;->mSaveBtn:Landroid/widget/Button;

    invoke-virtual {p0, p1}, Lepson/server/screens/UploadScreen;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 178
    iget-object p1, p0, Lepson/server/screens/UploadScreen;->mSaveBtn:Landroid/widget/Button;

    new-instance v0, Lepson/server/screens/UploadScreen$5;

    invoke-direct {v0, p0}, Lepson/server/screens/UploadScreen$5;-><init>(Lepson/server/screens/UploadScreen;)V

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_2
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.class Lepson/server/screens/UploadScreen$6;
.super Ljava/lang/Object;
.source "UploadScreen.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/server/screens/UploadScreen;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/server/screens/UploadScreen;


# direct methods
.method constructor <init>(Lepson/server/screens/UploadScreen;)V
    .locals 0

    .line 289
    iput-object p1, p0, Lepson/server/screens/UploadScreen$6;->this$0:Lepson/server/screens/UploadScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .line 294
    iget-object p1, p0, Lepson/server/screens/UploadScreen$6;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {p1}, Lepson/server/screens/UploadScreen;->access$000(Lepson/server/screens/UploadScreen;)Landroid/widget/EditText;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lepson/print/fileBrower;->isAvailableFileName(Ljava/lang/String;)Z

    move-result p1

    const/4 v0, 0x1

    if-nez p1, :cond_0

    .line 295
    iget-object p1, p0, Lepson/server/screens/UploadScreen$6;->this$0:Lepson/server/screens/UploadScreen;

    invoke-virtual {p1}, Lepson/server/screens/UploadScreen;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const v1, 0x7f0e04b4

    invoke-static {p1, v1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    return-void

    .line 299
    :cond_0
    iget-object p1, p0, Lepson/server/screens/UploadScreen$6;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {p1}, Lepson/server/screens/UploadScreen;->access$000(Lepson/server/screens/UploadScreen;)Landroid/widget/EditText;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    if-lez p1, :cond_4

    .line 300
    iget-object p1, p0, Lepson/server/screens/UploadScreen$6;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v1}, Lepson/server/screens/UploadScreen;->access$700(Lepson/server/screens/UploadScreen;Ljava/lang/Boolean;)V

    .line 304
    iget-object p1, p0, Lepson/server/screens/UploadScreen$6;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {p1}, Lepson/server/screens/UploadScreen;->access$400(Lepson/server/screens/UploadScreen;)Ljava/util/ArrayList;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 305
    iget-object v2, p0, Lepson/server/screens/UploadScreen$6;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {v2}, Lepson/server/screens/UploadScreen;->access$800(Lepson/server/screens/UploadScreen;)Ljava/util/Vector;

    move-result-object v2

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 309
    :cond_1
    iget-object p1, p0, Lepson/server/screens/UploadScreen$6;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {p1}, Lepson/server/screens/UploadScreen;->access$000(Lepson/server/screens/UploadScreen;)Landroid/widget/EditText;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "EPSON"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lepson/server/screens/UploadScreen$6;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {v2}, Lepson/server/screens/UploadScreen;->access$600(Lepson/server/screens/UploadScreen;)Lepson/print/Util/Utils;

    move-result-object v2

    iget-object v3, p0, Lepson/server/screens/UploadScreen$6;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {v3}, Lepson/server/screens/UploadScreen;->access$500(Lepson/server/screens/UploadScreen;)I

    move-result v3

    invoke-virtual {v2, v3}, Lepson/print/Util/Utils;->editNumber(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_2

    .line 311
    iget-object p1, p0, Lepson/server/screens/UploadScreen$6;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {p1, v0}, Lepson/server/screens/UploadScreen;->access$902(Lepson/server/screens/UploadScreen;Z)Z

    .line 312
    iget-object p1, p0, Lepson/server/screens/UploadScreen$6;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {p1}, Lepson/server/screens/UploadScreen;->access$000(Lepson/server/screens/UploadScreen;)Landroid/widget/EditText;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 313
    :cond_2
    iget-object p1, p0, Lepson/server/screens/UploadScreen$6;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {p1}, Lepson/server/screens/UploadScreen;->access$800(Lepson/server/screens/UploadScreen;)Ljava/util/Vector;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result p1

    const/4 v1, 0x0

    if-ne p1, v0, :cond_3

    .line 315
    iget-object p1, p0, Lepson/server/screens/UploadScreen$6;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {p1, v1}, Lepson/server/screens/UploadScreen;->access$902(Lepson/server/screens/UploadScreen;Z)Z

    .line 316
    iget-object p1, p0, Lepson/server/screens/UploadScreen$6;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {p1}, Lepson/server/screens/UploadScreen;->access$000(Lepson/server/screens/UploadScreen;)Landroid/widget/EditText;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 318
    :cond_3
    iget-object p1, p0, Lepson/server/screens/UploadScreen$6;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {p1, v1}, Lepson/server/screens/UploadScreen;->access$902(Lepson/server/screens/UploadScreen;Z)Z

    const-string p1, "EPSON"

    .line 322
    :goto_1
    iget-object v0, p0, Lepson/server/screens/UploadScreen$6;->this$0:Lepson/server/screens/UploadScreen;

    iput-object p1, v0, Lepson/server/screens/UploadScreen;->currentFileName:Ljava/lang/String;

    .line 323
    invoke-static {v0}, Lepson/server/screens/UploadScreen;->access$1000(Lepson/server/screens/UploadScreen;)V

    :cond_4
    return-void
.end method

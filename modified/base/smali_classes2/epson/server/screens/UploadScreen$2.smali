.class Lepson/server/screens/UploadScreen$2;
.super Ljava/lang/Object;
.source "UploadScreen.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/server/screens/UploadScreen;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/server/screens/UploadScreen;


# direct methods
.method constructor <init>(Lepson/server/screens/UploadScreen;)V
    .locals 0

    .line 107
    iput-object p1, p0, Lepson/server/screens/UploadScreen$2;->this$0:Lepson/server/screens/UploadScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .line 116
    iget-object p1, p0, Lepson/server/screens/UploadScreen$2;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {p1}, Lepson/server/screens/UploadScreen;->access$000(Lepson/server/screens/UploadScreen;)Landroid/widget/EditText;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    const/4 v0, 0x0

    if-lez p1, :cond_0

    .line 117
    iget-object p1, p0, Lepson/server/screens/UploadScreen$2;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {p1}, Lepson/server/screens/UploadScreen;->access$100(Lepson/server/screens/UploadScreen;)Landroid/widget/Button;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 118
    iget-object p1, p0, Lepson/server/screens/UploadScreen$2;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {p1}, Lepson/server/screens/UploadScreen;->access$200(Lepson/server/screens/UploadScreen;)Landroid/widget/Button;

    move-result-object p1

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 120
    :cond_0
    iget-object p1, p0, Lepson/server/screens/UploadScreen$2;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {p1}, Lepson/server/screens/UploadScreen;->access$100(Lepson/server/screens/UploadScreen;)Landroid/widget/Button;

    move-result-object p1

    const/16 v1, 0x8

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 121
    iget-object p1, p0, Lepson/server/screens/UploadScreen$2;->this$0:Lepson/server/screens/UploadScreen;

    invoke-static {p1}, Lepson/server/screens/UploadScreen;->access$200(Lepson/server/screens/UploadScreen;)Landroid/widget/Button;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    :goto_0
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

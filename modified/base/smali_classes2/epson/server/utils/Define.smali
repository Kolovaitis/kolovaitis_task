.class public Lepson/server/utils/Define;
.super Ljava/lang/Object;
.source "Define.java"


# static fields
.field public static final ACCESS_KEY_NAME:Ljava/lang/String; = "FolderViewer.ACCESS_KEY"

.field public static final ACCESS_SECRET_NAME:Ljava/lang/String; = "FolderViewer.ACCESS_SECRET"

.field public static final AUTHENTICATE_ERROR:I = 0x0

.field public static final BOXNET_ACCOUNT_REMEMBERED:Ljava/lang/String; = "BOXNET_ACCOUNT_REMBERED"

.field public static final BOXNET_AUTH_INFO_V4:Ljava/lang/String; = "FolderViewer.BOXNET_AUTO_INFO_V4"

.field public static final BOXNET_LAST_AUTHENTICATED_USER_ID:Ljava/lang/String; = "FolderViewer.BOXNET_LAST_USER_ID"

.field public static final BOXNET_PASSOWRD:Ljava/lang/String; = "FolderViewer.BOXNET_PASSWORD"

.field public static final BOXNET_TOKEN:Ljava/lang/String; = "FolderViewer.BOXNET_TOKEN"

.field public static final BOXNET_USERNAME:Ljava/lang/String; = "FolderViewer.BOXNET_USERNAME"

.field public static final BROWSE_FOLDER_FOR:Ljava/lang/String; = "BROWSE_FOLDER_FOR"

.field public static final BROWSE_FOLDER_FOR_GET:I = 0x0

.field public static final BROWSE_FOLDER_FOR_SAVE:I = 0x1

.field public static final DEFAULT_NAME_FOR_SAVING:Ljava/lang/String; = "EPSON"

.field public static final DOWNLOAD_ERROR:I = 0x1

.field public static final DROPBOX:I = 0x4

.field public static final DROPBOX_ACCESS_KEY:Ljava/lang/String; = "FolderViewer.DROPBOX_ACCESS_KEY"

.field public static final DROPBOX_ACCESS_SECRET:Ljava/lang/String; = "FolderViewer.DROPBOX_ACCESS_SECRET"

.field public static final DROPBOX_ACCOUNT_REMEMBERED:Ljava/lang/String; = "DROPBOX_ACCOUNT_REMBERED"

.field public static final DROPBOX_PASSWORD:Ljava/lang/String; = "FolderViewer.ROPBOX_PASSWORD"

.field public static final DROPBOX_USERNAME:Ljava/lang/String; = "FolderViewer.DROPBOX_USERNAME"

.field public static final DROPBOX_V2_TOKEN:Ljava/lang/String; = "FolderViewer.DROPBOX_V2_TOKEN"

.field public static final EVERNOTE:I = 0x1

.field public static final EVERNOTE_ACCOUNT_REMEMBERED:Ljava/lang/String; = "EVERNOTE_ACCOUNT_REMBERED"

.field public static final EVERNOTE_HOST:Ljava/lang/String; = "FolderViewer.EVERNOTE_HOST"

.field public static final EVERNOTE_ISAPPLINKEDNOTEBOOK:Ljava/lang/String; = "FolderViewer.EVERNOTE_ISAPPLINKEDNOTEBOOK"

.field public static final EVERNOTE_NOTESTOREURL:Ljava/lang/String; = "FolderViewer.EVERNOTE_NOTESTOREURL"

.field public static final EVERNOTE_PASSWORD:Ljava/lang/String; = "FolderViewer.EVERNOTE_PASSWORD"

.field public static final EVERNOTE_TOKEN:Ljava/lang/String; = "FolderViewer.EVERNOTE_TOKEN"

.field public static final EVERNOTE_USERID:Ljava/lang/String; = "FolderViewer.EVERNOTE_USERID"

.field public static final EVERNOTE_USERNAME:Ljava/lang/String; = "FolderViewer.EVERNOTE_USERNAME"

.field public static final EVERNOTE_WEBAPIURLPREFIX:Ljava/lang/String; = "FolderViewer.EVERNOTE_WEBAPIURLPREFIX"

.field public static final FILENAME_ERROR:I = 0x4

.field public static final FOR_GETTING_FILE:I = 0x1

.field public static final FOR_SAVING_FILE:I = 0x2

.field public static final FOR_SETTING:I = 0x0

.field public static final GOOGLEDOCS:I = 0x3

.field public static final GOOGLE_ACCOUNT_REMEMBERED:Ljava/lang/String; = "GOOGLE_ACCOUNT_REMBERED"

.field public static final GOOGLE_PASSWORD:Ljava/lang/String; = "FolderViewer.GOOGLE_PASSWORD"

.field public static final GOOGLE_USERNAME:Ljava/lang/String; = "FolderViewer.GOOGLE_USERNAME"

.field public static final LOCAL:I = 0x7

.field public static final NETWORK_ERROR:I = 0x3

.field public static final ONEDRIVE_TOKEN:Ljava/lang/String; = "FolderViewer.ONEDRIVE_V2_TOKEN"

.field public static final OPEN_IN:I = 0x6

.field public static final PICK_SERVER_FOR:Ljava/lang/String; = "PICK_SERVER_FOR"

.field public static final SAVING_FILE_PATH:Ljava/lang/String; = "SAVING_FILE_PATH"

.field public static final SHARED_PREFERENCES_NAME:Ljava/lang/String; = "login_server"

.field public static final SHARED_PREFERENCES_NAME2:Ljava/lang/String; = "LOGIN_PREFERENCES"

.field public static final UNKNOW_ERROR:I = 0x10

.field public static final UPLOAD_ERROR:I = 0x2


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

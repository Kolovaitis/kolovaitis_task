.class Lepson/server/utils/NetworkConnectivityListener$ConnectivityBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "NetworkConnectivityListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/server/utils/NetworkConnectivityListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ConnectivityBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lepson/server/utils/NetworkConnectivityListener;


# direct methods
.method private constructor <init>(Lepson/server/utils/NetworkConnectivityListener;)V
    .locals 0

    .line 54
    iput-object p1, p0, Lepson/server/utils/NetworkConnectivityListener$ConnectivityBroadcastReceiver;->this$0:Lepson/server/utils/NetworkConnectivityListener;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lepson/server/utils/NetworkConnectivityListener;Lepson/server/utils/NetworkConnectivityListener$1;)V
    .locals 0

    .line 54
    invoke-direct {p0, p1}, Lepson/server/utils/NetworkConnectivityListener$ConnectivityBroadcastReceiver;-><init>(Lepson/server/utils/NetworkConnectivityListener;)V

    return-void
.end method

.method private cancelDownload()V
    .locals 2

    .line 72
    iget-object v0, p0, Lepson/server/utils/NetworkConnectivityListener$ConnectivityBroadcastReceiver;->this$0:Lepson/server/utils/NetworkConnectivityListener;

    invoke-static {v0}, Lepson/server/utils/NetworkConnectivityListener;->access$200(Lepson/server/utils/NetworkConnectivityListener;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 73
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    const/4 v1, 0x1

    .line 74
    iput v1, v0, Landroid/os/Message;->what:I

    .line 75
    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 76
    iget-object v1, p0, Lepson/server/utils/NetworkConnectivityListener$ConnectivityBroadcastReceiver;->this$0:Lepson/server/utils/NetworkConnectivityListener;

    invoke-static {v1}, Lepson/server/utils/NetworkConnectivityListener;->access$200(Lepson/server/utils/NetworkConnectivityListener;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 77
    iget-object v0, p0, Lepson/server/utils/NetworkConnectivityListener$ConnectivityBroadcastReceiver;->this$0:Lepson/server/utils/NetworkConnectivityListener;

    invoke-virtual {v0}, Lepson/server/utils/NetworkConnectivityListener;->stopListening()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0

    .line 57
    invoke-static {}, Lepson/print/Util/Utils;->getInstance()Lepson/print/Util/Utils;

    move-result-object p2

    invoke-virtual {p2, p1}, Lepson/print/Util/Utils;->isNetworkAvailable(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 58
    iget-object p1, p0, Lepson/server/utils/NetworkConnectivityListener$ConnectivityBroadcastReceiver;->this$0:Lepson/server/utils/NetworkConnectivityListener;

    const/4 p2, 0x1

    invoke-static {p1, p2}, Lepson/server/utils/NetworkConnectivityListener;->access$102(Lepson/server/utils/NetworkConnectivityListener;Z)Z

    const-string p1, "ConnectivityBroadcastReceiver:  "

    const-string p2, "===========OKKKKKKKKKKKKKKKKKKKK"

    .line 59
    invoke-static {p1, p2}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 61
    :cond_0
    iget-object p1, p0, Lepson/server/utils/NetworkConnectivityListener$ConnectivityBroadcastReceiver;->this$0:Lepson/server/utils/NetworkConnectivityListener;

    const/4 p2, 0x0

    invoke-static {p1, p2}, Lepson/server/utils/NetworkConnectivityListener;->access$102(Lepson/server/utils/NetworkConnectivityListener;Z)Z

    .line 62
    invoke-direct {p0}, Lepson/server/utils/NetworkConnectivityListener$ConnectivityBroadcastReceiver;->cancelDownload()V

    const-string p1, "ConnectivityBroadcastReceiver:  "

    const-string p2, "============FALSEEEEEEEEEEEEEEEEE"

    .line 63
    invoke-static {p1, p2}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.class public Lepson/server/utils/NetworkConnectivityListener;
.super Ljava/lang/Object;
.source "NetworkConnectivityListener.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/server/utils/NetworkConnectivityListener$ConnectivityBroadcastReceiver;
    }
.end annotation


# static fields
.field public static final CREATE_DIALOG:I = 0x1

.field private static final DOWNLOAD_FINISH:I = 0x4


# instance fields
.field private isOnline:Z

.field private mActivityHandler:Landroid/os/Handler;

.field private mContext:Landroid/content/Context;

.field private mListening:Z

.field private mReceiver:Lepson/server/utils/NetworkConnectivityListener$ConnectivityBroadcastReceiver;


# direct methods
.method public constructor <init>(Landroid/os/Handler;)V
    .locals 1

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lepson/server/utils/NetworkConnectivityListener;->mActivityHandler:Landroid/os/Handler;

    .line 51
    new-instance p1, Lepson/server/utils/NetworkConnectivityListener$ConnectivityBroadcastReceiver;

    const/4 v0, 0x0

    invoke-direct {p1, p0, v0}, Lepson/server/utils/NetworkConnectivityListener$ConnectivityBroadcastReceiver;-><init>(Lepson/server/utils/NetworkConnectivityListener;Lepson/server/utils/NetworkConnectivityListener$1;)V

    iput-object p1, p0, Lepson/server/utils/NetworkConnectivityListener;->mReceiver:Lepson/server/utils/NetworkConnectivityListener$ConnectivityBroadcastReceiver;

    return-void
.end method

.method static synthetic access$102(Lepson/server/utils/NetworkConnectivityListener;Z)Z
    .locals 0

    .line 34
    iput-boolean p1, p0, Lepson/server/utils/NetworkConnectivityListener;->isOnline:Z

    return p1
.end method

.method static synthetic access$200(Lepson/server/utils/NetworkConnectivityListener;)Landroid/os/Handler;
    .locals 0

    .line 34
    iget-object p0, p0, Lepson/server/utils/NetworkConnectivityListener;->mActivityHandler:Landroid/os/Handler;

    return-object p0
.end method


# virtual methods
.method public getIsOnline()Z
    .locals 1

    .line 112
    iget-boolean v0, p0, Lepson/server/utils/NetworkConnectivityListener;->isOnline:Z

    return v0
.end method

.method public declared-synchronized startListening(Landroid/content/Context;)V
    .locals 2

    monitor-enter p0

    .line 87
    :try_start_0
    iget-boolean v0, p0, Lepson/server/utils/NetworkConnectivityListener;->mListening:Z

    if-nez v0, :cond_0

    .line 88
    iput-object p1, p0, Lepson/server/utils/NetworkConnectivityListener;->mContext:Landroid/content/Context;

    .line 89
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    .line 90
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 91
    iget-object v1, p0, Lepson/server/utils/NetworkConnectivityListener;->mReceiver:Lepson/server/utils/NetworkConnectivityListener$ConnectivityBroadcastReceiver;

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 p1, 0x1

    .line 92
    iput-boolean p1, p0, Lepson/server/utils/NetworkConnectivityListener;->mListening:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 94
    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized stopListening()V
    .locals 2

    monitor-enter p0

    .line 100
    :try_start_0
    iget-boolean v0, p0, Lepson/server/utils/NetworkConnectivityListener;->mListening:Z

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lepson/server/utils/NetworkConnectivityListener;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lepson/server/utils/NetworkConnectivityListener;->mReceiver:Lepson/server/utils/NetworkConnectivityListener$ConnectivityBroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    .line 102
    iput-object v0, p0, Lepson/server/utils/NetworkConnectivityListener;->mContext:Landroid/content/Context;

    const/4 v0, 0x0

    .line 103
    iput-boolean v0, p0, Lepson/server/utils/NetworkConnectivityListener;->mListening:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 105
    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

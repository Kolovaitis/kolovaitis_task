.class public Lepson/server/service/EvernoteClient;
.super Ljava/lang/Object;
.source "EvernoteClient.java"


# static fields
.field static session:Lcom/evernote/client/android/EvernoteSession;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getEvernoteSession(Landroid/content/Context;)Lcom/evernote/client/android/EvernoteSession;
    .locals 3

    .line 23
    sget-object v0, Lepson/server/service/EvernoteClient;->session:Lcom/evernote/client/android/EvernoteSession;

    if-eqz v0, :cond_0

    return-object v0

    .line 27
    :cond_0
    new-instance v0, Lcom/evernote/client/android/EvernoteSession$Builder;

    invoke-direct {v0, p0}, Lcom/evernote/client/android/EvernoteSession$Builder;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/evernote/client/android/EvernoteSession$EvernoteService;->PRODUCTION:Lcom/evernote/client/android/EvernoteSession$EvernoteService;

    .line 28
    invoke-virtual {v0, v1}, Lcom/evernote/client/android/EvernoteSession$Builder;->setEvernoteService(Lcom/evernote/client/android/EvernoteSession$EvernoteService;)Lcom/evernote/client/android/EvernoteSession$Builder;

    move-result-object v0

    const/4 v1, 0x0

    .line 29
    invoke-virtual {v0, v1}, Lcom/evernote/client/android/EvernoteSession$Builder;->setSupportAppLinkedNotebooks(Z)Lcom/evernote/client/android/EvernoteSession$Builder;

    move-result-object v0

    new-instance v1, Lcom/epson/iprint/storage/SecureKeyStore;

    invoke-direct {v1}, Lcom/epson/iprint/storage/SecureKeyStore;-><init>()V

    .line 30
    invoke-virtual {v1, p0}, Lcom/epson/iprint/storage/SecureKeyStore;->getApiKeyB(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/epson/iprint/storage/SecureKeyStore;

    invoke-direct {v2}, Lcom/epson/iprint/storage/SecureKeyStore;-><init>()V

    invoke-virtual {v2, p0}, Lcom/epson/iprint/storage/SecureKeyStore;->getSecKeyB(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, v1, p0}, Lcom/evernote/client/android/EvernoteSession$Builder;->build(Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/client/android/EvernoteSession;

    move-result-object p0

    .line 31
    invoke-virtual {p0}, Lcom/evernote/client/android/EvernoteSession;->asSingleton()Lcom/evernote/client/android/EvernoteSession;

    move-result-object p0

    sput-object p0, Lepson/server/service/EvernoteClient;->session:Lcom/evernote/client/android/EvernoteSession;

    .line 33
    sget-object p0, Lepson/server/service/EvernoteClient;->session:Lcom/evernote/client/android/EvernoteSession;

    return-object p0
.end method

.method public static logout(Landroid/content/Context;)V
    .locals 0

    .line 43
    :try_start_0
    invoke-static {p0}, Lepson/server/service/EvernoteClient;->getEvernoteSession(Landroid/content/Context;)Lcom/evernote/client/android/EvernoteSession;

    move-result-object p0

    invoke-virtual {p0}, Lcom/evernote/client/android/EvernoteSession;->logOut()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

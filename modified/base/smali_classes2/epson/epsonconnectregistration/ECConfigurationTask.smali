.class public Lepson/epsonconnectregistration/ECConfigurationTask;
.super Lepson/epsonconnectregistration/ECBaseTask;
.source "ECConfigurationTask.java"


# static fields
.field private static final CHECK_INTERVAL:I = 0x1388

.field private static final MAX_RETRY:I = 0x6

.field private static final TAG:Ljava/lang/String; = "ECConfigurationTask"


# instance fields
.field private registrationUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 21
    invoke-direct {p0}, Lepson/epsonconnectregistration/ECBaseTask;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lepson/epsonconnectregistration/ECConfigurationTask;)Landroid/net/Uri;
    .locals 0

    .line 21
    iget-object p0, p0, Lepson/epsonconnectregistration/ECConfigurationTask;->registrationUri:Landroid/net/Uri;

    return-object p0
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lepson/epsonconnectregistration/ECStatus;
    .locals 9

    .line 57
    new-instance p1, Lepson/common/httpclient/IAHttpClient;

    invoke-direct {p1}, Lepson/common/httpclient/IAHttpClient;-><init>()V

    .line 59
    invoke-virtual {p0}, Lepson/epsonconnectregistration/ECConfigurationTask;->checkParam()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    .line 61
    sget-object p1, Lepson/epsonconnectregistration/ECStatus;->ERROR_UNEXPECTED:Lepson/epsonconnectregistration/ECStatus;

    return-object p1

    .line 65
    :cond_0
    invoke-virtual {p0}, Lepson/epsonconnectregistration/ECConfigurationTask;->getEndpoint()Landroid/net/Uri;

    move-result-object v1

    .line 66
    new-instance v2, Lepson/common/httpclient/IAHttpClient$HttpPost;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lepson/common/httpclient/IAHttpClient$HttpPost;-><init>(Ljava/lang/String;)V

    .line 69
    new-instance v3, Lepson/epsonconnectregistration/SoapWEBSETUPDataInfo;

    invoke-direct {v3}, Lepson/epsonconnectregistration/SoapWEBSETUPDataInfo;-><init>()V

    .line 73
    :try_start_0
    invoke-virtual {p0, v0}, Lepson/epsonconnectregistration/ECConfigurationTask;->getLang(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v1, v4}, Lepson/epsonconnectregistration/SoapRequestFactory;->createSetRequestWEBSETUP(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "application/soap+xml"

    .line 74
    invoke-virtual {v2, v5}, Lepson/common/httpclient/IAHttpClient$HttpPost;->setContentType(Ljava/lang/String;)V

    .line 76
    sget-object v5, Landroid/util/Xml$Encoding;->UTF_8:Landroid/util/Xml$Encoding;

    invoke-virtual {v5}, Landroid/util/Xml$Encoding;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v4

    .line 77
    invoke-virtual {v2, v4}, Lepson/common/httpclient/IAHttpClient$HttpPost;->setEntity([B)V

    .line 78
    array-length v4, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Lepson/common/httpclient/IAHttpClient$HttpPost;->setContentLength(Ljava/lang/Integer;)V

    .line 81
    invoke-virtual {p1, v2}, Lepson/common/httpclient/IAHttpClient;->execute(Lepson/common/httpclient/IAHttpClient$HttpPost;)Lepson/common/httpclient/IAHttpClient$HttpResponse;

    move-result-object v4

    .line 84
    invoke-virtual {v4}, Lepson/common/httpclient/IAHttpClient$HttpResponse;->getResponseCode()I

    move-result v5

    const/16 v6, 0xc8

    if-ne v5, v6, :cond_6

    .line 85
    new-instance v5, Ljava/io/ByteArrayInputStream;

    invoke-virtual {v4}, Lepson/common/httpclient/IAHttpClient$HttpResponse;->getEntity()Ljava/io/ByteArrayOutputStream;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    invoke-direct {v5, v4}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 86
    invoke-virtual {v3, v5}, Lepson/epsonconnectregistration/SoapWEBSETUPDataInfo;->parseXml(Ljava/io/InputStream;)V

    .line 88
    invoke-virtual {v3}, Lepson/epsonconnectregistration/SoapWEBSETUPDataInfo;->getRetVal()Z

    move-result v4

    if-nez v4, :cond_1

    const-string p1, "ECConfigurationTask"

    const-string v0, "Failed enable EC 1"

    .line 90
    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/EpLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    sget-object p1, Lepson/epsonconnectregistration/ECStatus;->ERROR_CANNOT_ENABLE:Lepson/epsonconnectregistration/ECStatus;

    return-object p1

    :cond_1
    const/4 v4, 0x0

    .line 103
    :cond_2
    invoke-static {v0, v1}, Lepson/epsonconnectregistration/SoapRequestFactory;->createGetRequestWEBSETUP(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v5

    const-string v7, "application/soap+xml"

    .line 104
    invoke-virtual {v2, v7}, Lepson/common/httpclient/IAHttpClient$HttpPost;->setContentType(Ljava/lang/String;)V

    .line 106
    sget-object v7, Landroid/util/Xml$Encoding;->UTF_8:Landroid/util/Xml$Encoding;

    invoke-virtual {v7}, Landroid/util/Xml$Encoding;->name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v5

    .line 107
    invoke-virtual {v2, v5}, Lepson/common/httpclient/IAHttpClient$HttpPost;->setEntity([B)V

    .line 108
    array-length v5, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v5}, Lepson/common/httpclient/IAHttpClient$HttpPost;->setContentLength(Ljava/lang/Integer;)V

    .line 111
    invoke-virtual {p1, v2}, Lepson/common/httpclient/IAHttpClient;->execute(Lepson/common/httpclient/IAHttpClient$HttpPost;)Lepson/common/httpclient/IAHttpClient$HttpResponse;

    move-result-object v5

    .line 114
    invoke-virtual {v5}, Lepson/common/httpclient/IAHttpClient$HttpResponse;->getResponseCode()I

    move-result v7

    if-ne v7, v6, :cond_5

    .line 115
    new-instance v7, Ljava/io/ByteArrayInputStream;

    invoke-virtual {v5}, Lepson/common/httpclient/IAHttpClient$HttpResponse;->getEntity()Ljava/io/ByteArrayOutputStream;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    invoke-direct {v7, v5}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 116
    invoke-virtual {v3, v7}, Lepson/epsonconnectregistration/SoapWEBSETUPDataInfo;->parseXml(Ljava/io/InputStream;)V

    .line 122
    invoke-virtual {v3}, Lepson/epsonconnectregistration/SoapWEBSETUPDataInfo;->inProgress()Z

    move-result v5
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v5, :cond_3

    const-wide/16 v7, 0x1388

    .line 124
    :try_start_1
    invoke-static {v7, v8}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_0
    move-exception v5

    .line 126
    :try_start_2
    invoke-virtual {v5}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_1

    :goto_0
    add-int/lit8 v4, v4, 0x1

    const/4 v5, 0x6

    if-lt v4, v5, :cond_2

    .line 148
    :cond_3
    invoke-virtual {v3}, Lepson/epsonconnectregistration/SoapWEBSETUPDataInfo;->isSucceeded()Z

    move-result p1

    if-nez p1, :cond_4

    const-string p1, "ECConfigurationTask"

    const-string v0, "Failed enable EC 2"

    .line 150
    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/EpLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    sget-object p1, Lepson/epsonconnectregistration/ECStatus;->ERROR_CANNOT_ENABLE:Lepson/epsonconnectregistration/ECStatus;

    return-object p1

    .line 154
    :cond_4
    invoke-virtual {v3}, Lepson/epsonconnectregistration/SoapWEBSETUPDataInfo;->getRegistrationUri()Landroid/net/Uri;

    move-result-object p1

    iput-object p1, p0, Lepson/epsonconnectregistration/ECConfigurationTask;->registrationUri:Landroid/net/Uri;

    .line 157
    sget-object p1, Lepson/epsonconnectregistration/ECStatus;->ERROR_NONE:Lepson/epsonconnectregistration/ECStatus;

    return-object p1

    .line 118
    :cond_5
    :try_start_3
    new-instance p1, Ljava/io/IOException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "soapRequest ResponseCode =  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Lepson/common/httpclient/IAHttpClient$HttpResponse;->getResponseCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 96
    :cond_6
    new-instance p1, Ljava/io/IOException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "soapRequest ResponseCode =  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Lepson/common/httpclient/IAHttpClient$HttpResponse;->getResponseCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_3 .. :try_end_3} :catch_1

    :catch_1
    move-exception p1

    .line 142
    invoke-virtual {p1}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    .line 143
    sget-object p1, Lepson/epsonconnectregistration/ECStatus;->ERROR_UNEXPECTED:Lepson/epsonconnectregistration/ECStatus;

    return-object p1

    :catch_2
    move-exception p1

    .line 139
    invoke-virtual {p1}, Ljava/io/IOException;->printStackTrace()V

    .line 140
    sget-object p1, Lepson/epsonconnectregistration/ECStatus;->ERROR_COMMUNICATION:Lepson/epsonconnectregistration/ECStatus;

    return-object p1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/epsonconnectregistration/ECConfigurationTask;->doInBackground([Ljava/lang/Void;)Lepson/epsonconnectregistration/ECStatus;

    move-result-object p1

    return-object p1
.end method

.method enableProgressText(Z)V
    .locals 3

    .line 41
    iget-object v0, p0, Lepson/epsonconnectregistration/ECConfigurationTask;->activityWeakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    if-eqz v0, :cond_1

    const v1, 0x7f08030d

    .line 43
    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v1, "ECConfigurationTask"

    const-string v2, "enableProgressText called"

    .line 45
    invoke-static {v1, v2}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/4 p1, 0x4

    .line 46
    :goto_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_1
    const-string p1, "ECConfigurationTask"

    const-string v0, "enableProgressText Failed"

    .line 51
    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/EpLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Lepson/epsonconnectregistration/ECStatus;)V
    .locals 4

    .line 162
    invoke-super {p0, p1}, Lepson/epsonconnectregistration/ECBaseTask;->onPostExecute(Ljava/lang/Object;)V

    const/4 v0, 0x0

    .line 164
    invoke-virtual {p0, v0}, Lepson/epsonconnectregistration/ECConfigurationTask;->enableProgressText(Z)V

    .line 166
    iget-object v1, p0, Lepson/epsonconnectregistration/ECConfigurationTask;->activityWeakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    if-eqz v1, :cond_1

    .line 168
    sget-object v2, Lepson/epsonconnectregistration/ECStatus;->ERROR_NONE:Lepson/epsonconnectregistration/ECStatus;

    invoke-virtual {v2, p1}, Lepson/epsonconnectregistration/ECStatus;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const-string p1, "ECConfigurationTask"

    const-string v2, "Succeed Configuration"

    .line 169
    invoke-static {p1, v2}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    new-instance p1, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {p1, v1}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0e032c

    .line 171
    invoke-virtual {p1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object p1

    const v2, 0x7f0e032b

    .line 172
    invoke-virtual {p1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object p1

    const v2, 0x7f0e04e5

    new-instance v3, Lepson/epsonconnectregistration/ECConfigurationTask$1;

    invoke-direct {v3, p0, v1}, Lepson/epsonconnectregistration/ECConfigurationTask$1;-><init>(Lepson/epsonconnectregistration/ECConfigurationTask;Landroid/app/Activity;)V

    .line 173
    invoke-virtual {p1, v2, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object p1

    .line 180
    invoke-virtual {p1}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object p1

    .line 181
    invoke-virtual {p1, v0}, Landroid/support/v7/app/AlertDialog;->setCancelable(Z)V

    .line 188
    invoke-virtual {p1}, Landroid/support/v7/app/AlertDialog;->show()V

    goto :goto_0

    :cond_0
    const-string p1, "ECConfigurationTask"

    const-string v2, "Failed Configuration"

    .line 191
    invoke-static {p1, v2}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    new-instance p1, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {p1, v1}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0e032f

    .line 193
    invoke-virtual {p1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object p1

    const v2, 0x7f0e032e

    .line 194
    invoke-virtual {p1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object p1

    const v2, 0x7f0e04f2

    new-instance v3, Lepson/epsonconnectregistration/ECConfigurationTask$2;

    invoke-direct {v3, p0, v1}, Lepson/epsonconnectregistration/ECConfigurationTask$2;-><init>(Lepson/epsonconnectregistration/ECConfigurationTask;Landroid/app/Activity;)V

    .line 195
    invoke-virtual {p1, v2, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object p1

    .line 202
    invoke-virtual {p1}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object p1

    .line 203
    invoke-virtual {p1, v0}, Landroid/support/v7/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 204
    new-instance v0, Lepson/epsonconnectregistration/ECConfigurationTask$3;

    invoke-direct {v0, p0, v1}, Lepson/epsonconnectregistration/ECConfigurationTask$3;-><init>(Lepson/epsonconnectregistration/ECConfigurationTask;Landroid/app/Activity;)V

    invoke-virtual {p1, v0}, Landroid/support/v7/app/AlertDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 210
    invoke-virtual {p1}, Landroid/support/v7/app/AlertDialog;->show()V

    goto :goto_0

    :cond_1
    const-string p1, "ECConfigurationTask"

    const-string v0, "activity is null. Maybe activity died"

    .line 213
    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/EpLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 21
    check-cast p1, Lepson/epsonconnectregistration/ECStatus;

    invoke-virtual {p0, p1}, Lepson/epsonconnectregistration/ECConfigurationTask;->onPostExecute(Lepson/epsonconnectregistration/ECStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .line 33
    invoke-super {p0}, Lepson/epsonconnectregistration/ECBaseTask;->onPreExecute()V

    const/4 v0, 0x1

    .line 35
    invoke-virtual {p0, v0}, Lepson/epsonconnectregistration/ECConfigurationTask;->enableProgressText(Z)V

    return-void
.end method

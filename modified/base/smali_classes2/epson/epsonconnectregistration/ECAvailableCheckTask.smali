.class public Lepson/epsonconnectregistration/ECAvailableCheckTask;
.super Lepson/epsonconnectregistration/ECBaseTask;
.source "ECAvailableCheckTask.java"


# static fields
.field private static final CONTENT_TYPE_FORM:Ljava/lang/String; = "application/x-www-form-urlencoded"

.field private static final HEALTHCHECK_BODY:Ljava/lang/String; = "c=1"

.field private static final HEALTHCHECK_ENCODE:Ljava/lang/String; = "aHR0cHM6Ly93d3cuZXBzb25jb25uZWN0LmNvbS9kOGQzYjkwNGJkYzk2YzZjMjc1NTAwYjVhZWZk\nM2U0YS92MS9zZXR1cA==\n"

.field private static final HEALTHCHECK_ENCODE2:Ljava/lang/String; = "aHR0cHM6Ly9zdGcxLWhyby13MDEuZXBzb25jb25uZWN0LmNvbS9kOGQzYjkwNGJkYzk2YzZjMjc1\nNTAwYjVhZWZkM2U0YS92MS9zZXR1cA==\n"

.field private static final HEALTHCHECK_OK:Ljava/lang/String; = "Open"

.field private static final TAG:Ljava/lang/String; = "ECAvailableCheckTask"


# instance fields
.field private mIsBLEContent:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 0

    .line 44
    invoke-direct {p0}, Lepson/epsonconnectregistration/ECBaseTask;-><init>()V

    .line 45
    iput-boolean p1, p0, Lepson/epsonconnectregistration/ECAvailableCheckTask;->mIsBLEContent:Z

    return-void
.end method

.method static synthetic lambda$createECDialog$0(Landroid/content/DialogInterface;)V
    .locals 2

    .line 307
    check-cast p0, Landroid/app/AlertDialog;

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setAllCaps(Z)V

    const/4 v0, -0x3

    .line 308
    invoke-virtual {p0, v0}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object p0

    invoke-virtual {p0, v1}, Landroid/widget/Button;->setAllCaps(Z)V

    return-void
.end method


# virtual methods
.method createECDialog(Lepson/epsonconnectregistration/ECStatus;)V
    .locals 6

    .line 227
    iget-object v0, p0, Lepson/epsonconnectregistration/ECAvailableCheckTask;->activityWeakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    if-eqz v0, :cond_3

    .line 229
    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0a0052

    const/4 v3, 0x0

    .line 230
    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f08012f

    .line 231
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f08012b

    .line 232
    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 235
    iget-boolean v5, p0, Lepson/epsonconnectregistration/ECAvailableCheckTask;->mIsBLEContent:Z

    if-eqz v5, :cond_0

    const v5, 0x7f0e032a

    .line 237
    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :cond_0
    const v5, 0x7f0e0339

    .line 240
    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(I)V

    .line 243
    :goto_0
    sget-object v3, Lepson/epsonconnectregistration/ECStatus;->ERROR_NONE:Lepson/epsonconnectregistration/ECStatus;

    if-eq p1, v3, :cond_1

    .line 245
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    const v2, 0x7f08012e

    .line 246
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 247
    sget-object v2, Lepson/epsonconnectregistration/ECAvailableCheckTask$5;->$SwitchMap$epson$epsonconnectregistration$ECStatus:[I

    invoke-virtual {p1}, Lepson/epsonconnectregistration/ECStatus;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    const v2, 0x7f0e0335

    .line 257
    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    :pswitch_0
    const v2, 0x7f0e0334

    .line 252
    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    :pswitch_1
    const v2, 0x7f0e0337

    .line 249
    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(I)V

    .line 263
    :cond_1
    :goto_1
    new-instance v2, Lepson/epsonconnectregistration/ECAvailableCheckTask$1;

    invoke-direct {v2, p0, v0}, Lepson/epsonconnectregistration/ECAvailableCheckTask$1;-><init>(Lepson/epsonconnectregistration/ECAvailableCheckTask;Landroid/app/Activity;)V

    .line 271
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 273
    sget-object v3, Lepson/epsonconnectregistration/ECAvailableCheckTask$5;->$SwitchMap$epson$epsonconnectregistration$ECStatus:[I

    invoke-virtual {p1}, Lepson/epsonconnectregistration/ECStatus;->ordinal()I

    move-result p1

    aget p1, v3, p1

    const/4 v3, 0x2

    if-eq p1, v3, :cond_2

    const p1, 0x7f0e0331

    .line 293
    new-instance v0, Lepson/epsonconnectregistration/ECAvailableCheckTask$4;

    invoke-direct {v0, p0, v2}, Lepson/epsonconnectregistration/ECAvailableCheckTask$4;-><init>(Lepson/epsonconnectregistration/ECAvailableCheckTask;Landroid/content/DialogInterface$OnCancelListener;)V

    invoke-virtual {v1, p1, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_2

    :cond_2
    const p1, 0x7f0e0332

    .line 275
    new-instance v3, Lepson/epsonconnectregistration/ECAvailableCheckTask$3;

    invoke-direct {v3, p0, v0}, Lepson/epsonconnectregistration/ECAvailableCheckTask$3;-><init>(Lepson/epsonconnectregistration/ECAvailableCheckTask;Landroid/app/Activity;)V

    invoke-virtual {v1, p1, v3}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const v0, 0x7f0e0333

    new-instance v3, Lepson/epsonconnectregistration/ECAvailableCheckTask$2;

    invoke-direct {v3, p0, v2}, Lepson/epsonconnectregistration/ECAvailableCheckTask$2;-><init>(Lepson/epsonconnectregistration/ECAvailableCheckTask;Landroid/content/DialogInterface$OnCancelListener;)V

    .line 281
    invoke-virtual {p1, v0, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 303
    :goto_2
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    const/4 v0, 0x0

    .line 304
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 305
    invoke-virtual {p1, v2}, Landroid/app/AlertDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 306
    sget-object v0, Lepson/epsonconnectregistration/-$$Lambda$ECAvailableCheckTask$gioJU6TFsLI9iDXVbpchuMiYbOo;->INSTANCE:Lepson/epsonconnectregistration/-$$Lambda$ECAvailableCheckTask$gioJU6TFsLI9iDXVbpchuMiYbOo;

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 310
    invoke-virtual {p1}, Landroid/app/AlertDialog;->show()V

    :cond_3
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Lepson/epsonconnectregistration/ECStatus;
    .locals 6

    .line 51
    new-instance p1, Lepson/common/httpclient/IAHttpClient;

    invoke-direct {p1}, Lepson/common/httpclient/IAHttpClient;-><init>()V

    .line 53
    invoke-virtual {p0}, Lepson/epsonconnectregistration/ECAvailableCheckTask;->checkParam()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    .line 55
    sget-object p1, Lepson/epsonconnectregistration/ECStatus;->ERROR_UNEXPECTED:Lepson/epsonconnectregistration/ECStatus;

    return-object p1

    .line 59
    :cond_0
    invoke-static {v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->getCurSSID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string v2, "DIRECT-"

    .line 60
    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string p1, "ECAvailableCheckTask"

    const-string v0, "Connected SimpleAP !!"

    .line 61
    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/EpLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    sget-object p1, Lepson/epsonconnectregistration/ECStatus;->ERROR_CANNOT_ENABLE:Lepson/epsonconnectregistration/ECStatus;

    return-object p1

    :cond_1
    :try_start_0
    const-string v1, "Beta"

    const-string v2, "FC"

    .line 68
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    .line 69
    new-instance v1, Ljava/lang/String;

    const-string v3, "aHR0cHM6Ly9zdGcxLWhyby13MDEuZXBzb25jb25uZWN0LmNvbS9kOGQzYjkwNGJkYzk2YzZjMjc1\nNTAwYjVhZWZkM2U0YS92MS9zZXR1cA==\n"

    invoke-static {v3, v2}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v2

    const-string v3, "UTF-8"

    invoke-direct {v1, v2, v3}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    goto :goto_0

    .line 71
    :cond_2
    new-instance v1, Ljava/lang/String;

    const-string v3, "aHR0cHM6Ly93d3cuZXBzb25jb25uZWN0LmNvbS9kOGQzYjkwNGJkYzk2YzZjMjc1NTAwYjVhZWZk\nM2U0YS92MS9zZXR1cA==\n"

    invoke-static {v3, v2}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v2

    const-string v3, "UTF-8"

    invoke-direct {v1, v2, v3}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_3

    .line 78
    :goto_0
    new-instance v2, Lepson/common/httpclient/IAHttpClient$HttpPost;

    invoke-direct {v2, v1}, Lepson/common/httpclient/IAHttpClient$HttpPost;-><init>(Ljava/lang/String;)V

    const-string v1, "application/x-www-form-urlencoded"

    .line 79
    invoke-virtual {v2, v1}, Lepson/common/httpclient/IAHttpClient$HttpPost;->setContentType(Ljava/lang/String;)V

    const-string v1, "c=1"

    .line 80
    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v2, v1}, Lepson/common/httpclient/IAHttpClient$HttpPost;->setEntity([B)V

    const-string v1, "c=1"

    .line 81
    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    array-length v1, v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v1}, Lepson/common/httpclient/IAHttpClient$HttpPost;->setContentLength(Ljava/lang/Integer;)V

    .line 85
    :try_start_1
    invoke-virtual {p1, v2}, Lepson/common/httpclient/IAHttpClient;->execute(Lepson/common/httpclient/IAHttpClient$HttpPost;)Lepson/common/httpclient/IAHttpClient$HttpResponse;

    move-result-object v1

    .line 88
    invoke-virtual {v1}, Lepson/common/httpclient/IAHttpClient$HttpResponse;->getResponseCode()I

    move-result v2

    const/16 v3, 0xc8

    if-ne v2, v3, :cond_8

    const-string v2, "Open"

    .line 89
    invoke-virtual {v1}, Lepson/common/httpclient/IAHttpClient$HttpResponse;->getEntity()Ljava/io/ByteArrayOutputStream;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string p1, "ECAvailableCheckTask"

    const-string v0, "EC Service Down"

    .line 90
    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/EpLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    sget-object p1, Lepson/epsonconnectregistration/ECStatus;->ERROR_CANNOT_ENABLE:Lepson/epsonconnectregistration/ECStatus;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    return-object p1

    .line 103
    :cond_3
    invoke-virtual {p0}, Lepson/epsonconnectregistration/ECAvailableCheckTask;->getEndpoint()Landroid/net/Uri;

    move-result-object v1

    .line 104
    new-instance v2, Lepson/common/httpclient/IAHttpClient$HttpPost;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Lepson/common/httpclient/IAHttpClient$HttpPost;-><init>(Ljava/lang/String;)V

    .line 107
    new-instance v4, Lepson/epsonconnectregistration/SoapWEBSETUPDataInfo;

    invoke-direct {v4}, Lepson/epsonconnectregistration/SoapWEBSETUPDataInfo;-><init>()V

    .line 108
    new-instance v5, Lepson/epsonconnectregistration/SoapAUTHENTICATIONDataInfo;

    invoke-direct {v5}, Lepson/epsonconnectregistration/SoapAUTHENTICATIONDataInfo;-><init>()V

    .line 112
    :try_start_2
    invoke-static {v0, v1}, Lepson/epsonconnectregistration/SoapRequestFactory;->createGetRequest(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "application/soap+xml"

    .line 113
    invoke-virtual {v2, v1}, Lepson/common/httpclient/IAHttpClient$HttpPost;->setContentType(Ljava/lang/String;)V

    .line 115
    sget-object v1, Landroid/util/Xml$Encoding;->UTF_8:Landroid/util/Xml$Encoding;

    invoke-virtual {v1}, Landroid/util/Xml$Encoding;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    .line 116
    invoke-virtual {v2, v0}, Lepson/common/httpclient/IAHttpClient$HttpPost;->setEntity([B)V

    .line 117
    array-length v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Lepson/common/httpclient/IAHttpClient$HttpPost;->setContentLength(Ljava/lang/Integer;)V

    .line 120
    invoke-virtual {p1, v2}, Lepson/common/httpclient/IAHttpClient;->execute(Lepson/common/httpclient/IAHttpClient$HttpPost;)Lepson/common/httpclient/IAHttpClient$HttpResponse;

    move-result-object p1

    .line 123
    invoke-virtual {p1}, Lepson/common/httpclient/IAHttpClient$HttpResponse;->getResponseCode()I

    move-result v0

    if-ne v0, v3, :cond_6

    .line 124
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-virtual {p1}, Lepson/common/httpclient/IAHttpClient$HttpResponse;->getEntity()Ljava/io/ByteArrayOutputStream;

    move-result-object p1

    invoke-virtual {p1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 127
    invoke-virtual {v4, v0}, Lepson/epsonconnectregistration/SoapWEBSETUPDataInfo;->parseXml(Ljava/io/InputStream;)V

    .line 128
    invoke-virtual {v4}, Lepson/epsonconnectregistration/SoapWEBSETUPDataInfo;->checkECPrinterStatus()Lepson/epsonconnectregistration/ECStatus;

    move-result-object p1

    .line 129
    sget-object v1, Lepson/epsonconnectregistration/ECStatus;->ERROR_NONE:Lepson/epsonconnectregistration/ECStatus;

    if-eq p1, v1, :cond_4

    return-object p1

    .line 136
    :cond_4
    invoke-virtual {v5, v0}, Lepson/epsonconnectregistration/SoapAUTHENTICATIONDataInfo;->parseXml(Ljava/io/InputStream;)V

    .line 137
    invoke-virtual {v5}, Lepson/epsonconnectregistration/SoapAUTHENTICATIONDataInfo;->isAdminMode()Z

    move-result p1

    if-eqz p1, :cond_5

    .line 138
    sget-object p1, Lepson/epsonconnectregistration/ECStatus;->ERROR_PRINTER_ADMIN_MODE:Lepson/epsonconnectregistration/ECStatus;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_0

    return-object p1

    .line 158
    :cond_5
    sget-object p1, Lepson/epsonconnectregistration/ECStatus;->ERROR_NONE:Lepson/epsonconnectregistration/ECStatus;

    return-object p1

    .line 143
    :cond_6
    :try_start_3
    invoke-virtual {p1}, Lepson/common/httpclient/IAHttpClient$HttpResponse;->getResponseCode()I

    move-result v0

    const/16 v1, 0x1f4

    if-ne v0, v1, :cond_7

    .line 145
    sget-object p1, Lepson/epsonconnectregistration/ECStatus;->ERROR_PRINTER_NOT_SUPPORT_EC:Lepson/epsonconnectregistration/ECStatus;

    return-object p1

    .line 147
    :cond_7
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "soapRequest ResponseCode =  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lepson/common/httpclient/IAHttpClient$HttpResponse;->getResponseCode()I

    move-result p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_3 .. :try_end_3} :catch_0

    :catch_0
    move-exception p1

    .line 154
    invoke-virtual {p1}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    .line 155
    sget-object p1, Lepson/epsonconnectregistration/ECStatus;->ERROR_UNEXPECTED:Lepson/epsonconnectregistration/ECStatus;

    return-object p1

    :catch_1
    move-exception p1

    .line 151
    invoke-virtual {p1}, Ljava/io/IOException;->printStackTrace()V

    .line 152
    sget-object p1, Lepson/epsonconnectregistration/ECStatus;->ERROR_COMMUNICATION:Lepson/epsonconnectregistration/ECStatus;

    return-object p1

    .line 94
    :cond_8
    :try_start_4
    new-instance p1, Ljava/io/IOException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "health_check ResponseCode =  "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lepson/common/httpclient/IAHttpClient$HttpResponse;->getResponseCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    :catch_2
    move-exception p1

    .line 98
    invoke-virtual {p1}, Ljava/io/IOException;->printStackTrace()V

    .line 99
    sget-object p1, Lepson/epsonconnectregistration/ECStatus;->ERROR_COMMUNICATION:Lepson/epsonconnectregistration/ECStatus;

    return-object p1

    :catch_3
    move-exception p1

    .line 74
    invoke-virtual {p1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 75
    sget-object p1, Lepson/epsonconnectregistration/ECStatus;->ERROR_UNEXPECTED:Lepson/epsonconnectregistration/ECStatus;

    return-object p1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 33
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/epsonconnectregistration/ECAvailableCheckTask;->doInBackground([Ljava/lang/Void;)Lepson/epsonconnectregistration/ECStatus;

    move-result-object p1

    return-object p1
.end method

.method protected onPostExecute(Lepson/epsonconnectregistration/ECStatus;)V
    .locals 3

    .line 163
    invoke-super {p0, p1}, Lepson/epsonconnectregistration/ECBaseTask;->onPostExecute(Ljava/lang/Object;)V

    .line 165
    iget-object v0, p0, Lepson/epsonconnectregistration/ECAvailableCheckTask;->activityWeakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    if-eqz v0, :cond_3

    .line 167
    iget-boolean v1, p0, Lepson/epsonconnectregistration/ECAvailableCheckTask;->mIsBLEContent:Z

    if-nez v1, :cond_1

    .line 168
    sget-object v1, Lepson/epsonconnectregistration/ECAvailableCheckTask$5;->$SwitchMap$epson$epsonconnectregistration$ECStatus:[I

    invoke-virtual {p1}, Lepson/epsonconnectregistration/ECStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    .line 184
    invoke-virtual {p0, p1}, Lepson/epsonconnectregistration/ECAvailableCheckTask;->createECDialog(Lepson/epsonconnectregistration/ECStatus;)V

    goto :goto_0

    :cond_0
    const-string p1, "https://www.epsonconnect.com/user/"

    .line 171
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    .line 172
    invoke-virtual {p0, v0, p1}, Lepson/epsonconnectregistration/ECAvailableCheckTask;->openWebPage(Landroid/app/Activity;Landroid/net/Uri;)V

    .line 173
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 188
    :cond_1
    sget-object v1, Lepson/epsonconnectregistration/ECAvailableCheckTask$5;->$SwitchMap$epson$epsonconnectregistration$ECStatus:[I

    invoke-virtual {p1}, Lepson/epsonconnectregistration/ECStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x2

    if-eq v1, v2, :cond_2

    .line 202
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 191
    :cond_2
    invoke-virtual {p0, p1}, Lepson/epsonconnectregistration/ECAvailableCheckTask;->createECDialog(Lepson/epsonconnectregistration/ECStatus;)V

    goto :goto_0

    :cond_3
    const-string p1, "ECAvailableCheckTask"

    const-string v0, "activity is null. Maybe activity died"

    .line 207
    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/EpLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 33
    check-cast p1, Lepson/epsonconnectregistration/ECStatus;

    invoke-virtual {p0, p1}, Lepson/epsonconnectregistration/ECAvailableCheckTask;->onPostExecute(Lepson/epsonconnectregistration/ECStatus;)V

    return-void
.end method

.method startECConfigure(Landroid/app/Activity;Landroid/net/Uri;)V
    .locals 1

    .line 216
    new-instance v0, Lepson/epsonconnectregistration/ECConfigurationTask;

    invoke-direct {v0}, Lepson/epsonconnectregistration/ECConfigurationTask;-><init>()V

    .line 217
    invoke-virtual {v0, p1}, Lepson/epsonconnectregistration/ECConfigurationTask;->setContext(Landroid/app/Activity;)V

    .line 218
    invoke-virtual {v0, p2}, Lepson/epsonconnectregistration/ECConfigurationTask;->setRootUri(Landroid/net/Uri;)V

    .line 219
    sget-object p1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 p2, 0x0

    new-array p2, p2, [Ljava/lang/Void;

    invoke-virtual {v0, p1, p2}, Lepson/epsonconnectregistration/ECConfigurationTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

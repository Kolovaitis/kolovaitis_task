.class public Lepson/epsonconnectregistration/SoapAUTHENTICATIONDataInfo;
.super Lepson/epsonconnectregistration/SoapConfigDataParser;
.source "SoapAUTHENTICATIONDataInfo.java"


# static fields
.field private static final GROUP_ID:Ljava/lang/String; = "AUTHENTICATIONDataInfo"

.field static final ID_ADMINLOCK:Ljava/lang/String; = "AdminLock"

.field static final ID_MACADDRESS:Ljava/lang/String; = "MACAddress"

.field static final ID_VERSION:Ljava/lang/String; = "Version"

.field static final STATUS_DISABLE:Ljava/lang/String; = "0"

.field static final STATUS_ENABLE:Ljava/lang/String; = "1"

.field private static final TAG:Ljava/lang/String; = "SoapAUTHENTICATIONDataInfo"


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "AUTHENTICATIONDataInfo"

    .line 18
    invoke-direct {p0, v0}, Lepson/epsonconnectregistration/SoapConfigDataParser;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public isAdminMode()Z
    .locals 4

    const-string v0, "SoapAUTHENTICATIONDataInfo"

    const-string v1, "Enter isAdminMode"

    .line 26
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    invoke-virtual {p0}, Lepson/epsonconnectregistration/SoapAUTHENTICATIONDataInfo;->getRetVal()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string v0, "SoapAUTHENTICATIONDataInfo"

    .line 29
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "return = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lepson/epsonconnectregistration/SoapAUTHENTICATIONDataInfo;->getRetVal()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_0
    const-string v0, "AdminLock"

    .line 33
    invoke-virtual {p0, v0}, Lepson/epsonconnectregistration/SoapAUTHENTICATIONDataInfo;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "1"

    .line 34
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    return v0

    :cond_1
    return v1
.end method

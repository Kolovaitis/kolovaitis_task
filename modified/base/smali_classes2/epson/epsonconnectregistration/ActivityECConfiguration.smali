.class public Lepson/epsonconnectregistration/ActivityECConfiguration;
.super Landroid/support/v7/app/AppCompatActivity;
.source "ActivityECConfiguration.java"


# static fields
.field public static final INTENT_EC_BLE_CONTENT:Ljava/lang/String; = "Epson-Connect-BLE-Content"

.field private static final TAG:Ljava/lang/String; = "ActivityECConfiguration"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public finish()V
    .locals 1

    .line 45
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->finish()V

    const/4 v0, 0x0

    .line 46
    invoke-virtual {p0, v0, v0}, Lepson/epsonconnectregistration/ActivityECConfiguration;->overridePendingTransition(II)V

    return-void
.end method

.method public onBackPressed()V
    .locals 0

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .line 22
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0a0020

    .line 23
    invoke-virtual {p0, p1}, Lepson/epsonconnectregistration/ActivityECConfiguration;->setContentView(I)V

    .line 24
    invoke-virtual {p0}, Lepson/epsonconnectregistration/ActivityECConfiguration;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "Epson-Connect-BLE-Content"

    const/4 v1, 0x0

    .line 25
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p1

    .line 27
    invoke-static {p0}, Lepson/print/MyPrinter;->getCurPrinter(Landroid/content/Context;)Lepson/print/MyPrinter;

    move-result-object v0

    .line 28
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    .line 29
    invoke-virtual {p0}, Lepson/epsonconnectregistration/ActivityECConfiguration;->finish()V

    goto :goto_0

    :cond_0
    const-string v2, "http"

    .line 31
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object v0

    const-string v3, ""

    invoke-static {v2, v0, v3}, Lepson/common/IPAddressUtils;->buildURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 32
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 35
    new-instance v2, Lepson/epsonconnectregistration/ECAvailableCheckTask;

    invoke-direct {v2, p1}, Lepson/epsonconnectregistration/ECAvailableCheckTask;-><init>(Z)V

    .line 36
    invoke-virtual {v2, p0}, Lepson/epsonconnectregistration/ECAvailableCheckTask;->setContext(Landroid/app/Activity;)V

    .line 37
    invoke-virtual {v2, v0}, Lepson/epsonconnectregistration/ECAvailableCheckTask;->setRootUri(Landroid/net/Uri;)V

    .line 38
    sget-object p1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v0, v1, [Ljava/lang/Void;

    invoke-virtual {v2, p1, v0}, Lepson/epsonconnectregistration/ECAvailableCheckTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_1
    :goto_0
    return-void
.end method

.class public Lepson/epsonconnectregistration/SoapWEBSETUPDataInfo;
.super Lepson/epsonconnectregistration/SoapConfigDataParser;
.source "SoapWEBSETUPDataInfo.java"


# static fields
.field static final AVAILABLEWEBMODE_DISABLE:I = 0x1

.field static final AVAILABLEWEBMODE_ENABLE:I = 0x10

.field static final AVAILABLEWEBMODE_ENABLE_PRINT:I = 0x8

.field static final AVAILABLEWEBMODE_ENABLE_REMOVEACCOUNT:I = 0x4

.field static final AVAILABLEWEBMODE_ENABLE_REMOVEACCOUNT_PRINT:I = 0x2

.field static final GROUP_ID:Ljava/lang/String; = "WEBSETUPDataInfo"

.field static final ID_ADMIN_PASSWORD:Ljava/lang/String; = "AdminPassword"

.field static final ID_ADMIN_URL:Ljava/lang/String; = "AdminURL"

.field static final ID_AVAILABLE_MODE:Ljava/lang/String; = "AvailableWebMode"

.field static final ID_MODE:Ljava/lang/String; = "WebMode"

.field static final ID_PRINTER_MAILADDRESS:Ljava/lang/String; = "PrinterMailAddress"

.field static final ID_SERVER_URL:Ljava/lang/String; = "WebServerURL"

.field static final ID_SETUP_LANGUAGE:Ljava/lang/String; = "WebSetupLanguage"

.field static final ID_SETUP_RESULT:Ljava/lang/String; = "WebSetupResult"

.field static final ID_VERSION:Ljava/lang/String; = "Version"

.field static final ID_WEBSERVER_STATUS:Ljava/lang/String; = "WebServerStatus"

.field static final ID_XMPPSERVER_STATUS:Ljava/lang/String; = "XMPPServerStatus"

.field static final RESULT_NOT_WORKING:Ljava/lang/String; = "0"

.field static final RESULT_SUCCESS:Ljava/lang/String; = "2"

.field static final RESULT_WORKING:Ljava/lang/String; = "1"

.field static final SERVER_STATUS_DISABLE:Ljava/lang/String; = "0"

.field static final SERVER_STATUS_ENABLE:Ljava/lang/String; = "2"

.field private static final TAG:Ljava/lang/String; = "SoapWEBSETUPDataInfo"


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "WEBSETUPDataInfo"

    .line 39
    invoke-direct {p0, v0}, Lepson/epsonconnectregistration/SoapConfigDataParser;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public checkECPrinterStatus()Lepson/epsonconnectregistration/ECStatus;
    .locals 7

    const-string v0, "SoapWEBSETUPDataInfo"

    const-string v1, "Enter isUnregisteredECPrinter"

    .line 91
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    invoke-virtual {p0}, Lepson/epsonconnectregistration/SoapWEBSETUPDataInfo;->getRetVal()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "SoapWEBSETUPDataInfo"

    .line 94
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "return = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lepson/epsonconnectregistration/SoapWEBSETUPDataInfo;->getRetVal()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    sget-object v0, Lepson/epsonconnectregistration/ECStatus;->ERROR_CANNOT_ENABLE:Lepson/epsonconnectregistration/ECStatus;

    return-object v0

    :cond_0
    :try_start_0
    const-string v0, "AvailableWebMode"

    .line 99
    invoke-virtual {p0, v0}, Lepson/epsonconnectregistration/SoapWEBSETUPDataInfo;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x10

    invoke-static {v0, v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v2, "2"

    const-string v3, "WebServerStatus"

    .line 100
    invoke-virtual {p0, v3}, Lepson/epsonconnectregistration/SoapWEBSETUPDataInfo;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "2"

    const-string v3, "XMPPServerStatus"

    invoke-virtual {p0, v3}, Lepson/epsonconnectregistration/SoapWEBSETUPDataInfo;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 101
    sget-object v0, Lepson/epsonconnectregistration/ECStatus;->ERROR_PRINTER_EC_REGISTERED:Lepson/epsonconnectregistration/ECStatus;

    return-object v0

    .line 102
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    and-int/lit8 v2, v2, 0x8

    if-nez v2, :cond_2

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    and-int/2addr v0, v1

    if-nez v0, :cond_2

    const-string v0, "SoapWEBSETUPDataInfo"

    const-string v1, "Cannot enable EC without delete account -> unregistered"

    .line 104
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    sget-object v0, Lepson/epsonconnectregistration/ECStatus;->ERROR_NONE:Lepson/epsonconnectregistration/ECStatus;

    return-object v0

    :cond_2
    const-string v0, "0"

    const-string v1, "WebServerStatus"

    .line 106
    invoke-virtual {p0, v1}, Lepson/epsonconnectregistration/SoapWEBSETUPDataInfo;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "0"

    const-string v1, "XMPPServerStatus"

    invoke-virtual {p0, v1}, Lepson/epsonconnectregistration/SoapWEBSETUPDataInfo;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_3
    const-string v0, "Version"

    .line 109
    invoke-virtual {p0, v0}, Lepson/epsonconnectregistration/SoapWEBSETUPDataInfo;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    .line 110
    invoke-virtual {v0}, Ljava/lang/Double;->intValue()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 111
    new-instance v2, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    invoke-virtual {v0}, Ljava/lang/Double;->intValue()I

    move-result v0

    int-to-double v5, v0

    sub-double/2addr v3, v5

    const-wide/high16 v5, 0x4059000000000000L    # 100.0

    mul-double v3, v3, v5

    invoke-direct {v2, v3, v4}, Ljava/lang/Double;-><init>(D)V

    invoke-virtual {v2}, Ljava/lang/Double;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 112
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_4

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "SoapWEBSETUPDataInfo"

    const-string v1, "WebServerStatus:disable or XMPPServerStatus:disable -> unregistered"

    .line 113
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    sget-object v0, Lepson/epsonconnectregistration/ECStatus;->ERROR_NONE:Lepson/epsonconnectregistration/ECStatus;

    return-object v0

    .line 116
    :cond_4
    sget-object v0, Lepson/epsonconnectregistration/ECStatus;->ERROR_PRINTER_NOT_SUPPORT_EC:Lepson/epsonconnectregistration/ECStatus;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    .line 124
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    .line 122
    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V

    .line 127
    :cond_5
    :goto_0
    sget-object v0, Lepson/epsonconnectregistration/ECStatus;->ERROR_CANNOT_ENABLE:Lepson/epsonconnectregistration/ECStatus;

    return-object v0
.end method

.method public getRegistrationUri()Landroid/net/Uri;
    .locals 7

    const-string v0, "SoapWEBSETUPDataInfo"

    const-string v1, "Enter isSucceeded"

    .line 179
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    invoke-virtual {p0}, Lepson/epsonconnectregistration/SoapWEBSETUPDataInfo;->getRetVal()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string v0, "SoapWEBSETUPDataInfo"

    .line 182
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "return = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lepson/epsonconnectregistration/SoapWEBSETUPDataInfo;->getRetVal()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-object v1

    :cond_0
    :try_start_0
    const-string v0, "AdminURL"

    .line 187
    invoke-virtual {p0, v0}, Lepson/epsonconnectregistration/SoapWEBSETUPDataInfo;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_1

    return-object v1

    :cond_1
    const-string v2, "Alpha"

    const-string v3, "FC"

    .line 204
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "setuptool"

    goto :goto_0

    :cond_2
    const-string v2, "printerfinder"

    .line 211
    :goto_0
    new-instance v3, Landroid/net/Uri$Builder;

    invoke-direct {v3}, Landroid/net/Uri$Builder;-><init>()V

    .line 212
    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    .line 213
    invoke-virtual {v0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    .line 214
    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    const-string v5, "/setup/"

    const-string v6, "/nsetup/"

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    .line 215
    invoke-virtual {v0}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/net/Uri$Builder;->encodedQuery(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v3, "from"

    .line 216
    invoke-virtual {v0, v3, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 217
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    const-string v2, "SoapWEBSETUPDataInfo"

    .line 219
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "rebuildUri = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    .line 223
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    return-object v1
.end method

.method public inProgress()Z
    .locals 4

    const-string v0, "SoapWEBSETUPDataInfo"

    const-string v1, "Enter inProgress"

    .line 135
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    invoke-virtual {p0}, Lepson/epsonconnectregistration/SoapWEBSETUPDataInfo;->getRetVal()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string v0, "SoapWEBSETUPDataInfo"

    .line 138
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "return = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lepson/epsonconnectregistration/SoapWEBSETUPDataInfo;->getRetVal()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_0
    const-string v0, "1"

    const-string v2, "WebSetupResult"

    .line 143
    invoke-virtual {p0, v2}, Lepson/epsonconnectregistration/SoapWEBSETUPDataInfo;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "SoapWEBSETUPDataInfo"

    const-string v1, "Working"

    .line 144
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    return v0

    :cond_1
    return v1
.end method

.method public isSucceeded()Z
    .locals 4

    const-string v0, "SoapWEBSETUPDataInfo"

    const-string v1, "Enter isSucceeded"

    .line 156
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    invoke-virtual {p0}, Lepson/epsonconnectregistration/SoapWEBSETUPDataInfo;->getRetVal()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string v0, "SoapWEBSETUPDataInfo"

    .line 159
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "return = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lepson/epsonconnectregistration/SoapWEBSETUPDataInfo;->getRetVal()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_0
    const-string v0, "2"

    const-string v2, "WebSetupResult"

    .line 163
    invoke-virtual {p0, v2}, Lepson/epsonconnectregistration/SoapWEBSETUPDataInfo;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "AdminURL"

    .line 164
    invoke-virtual {p0, v0}, Lepson/epsonconnectregistration/SoapWEBSETUPDataInfo;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 165
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "SoapWEBSETUPDataInfo"

    const-string v1, "Succeeded"

    .line 166
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    return v0

    :cond_1
    return v1
.end method

.method public isUnregisteredECPrinter()Z
    .locals 9

    const-string v0, "SoapWEBSETUPDataInfo"

    const-string v1, "Enter isUnregisteredECPrinter"

    .line 47
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    invoke-virtual {p0}, Lepson/epsonconnectregistration/SoapWEBSETUPDataInfo;->getRetVal()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string v0, "SoapWEBSETUPDataInfo"

    .line 50
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "return = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lepson/epsonconnectregistration/SoapWEBSETUPDataInfo;->getRetVal()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_0
    :try_start_0
    const-string v0, "AvailableWebMode"

    .line 55
    invoke-virtual {p0, v0}, Lepson/epsonconnectregistration/SoapWEBSETUPDataInfo;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/16 v2, 0x10

    invoke-static {v0, v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v3, "2"

    const-string v4, "WebServerStatus"

    .line 56
    invoke-virtual {p0, v4}, Lepson/epsonconnectregistration/SoapWEBSETUPDataInfo;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "2"

    const-string v4, "XMPPServerStatus"

    invoke-virtual {p0, v4}, Lepson/epsonconnectregistration/SoapWEBSETUPDataInfo;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto/16 :goto_0

    .line 58
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    and-int/lit8 v3, v3, 0x8

    const/4 v4, 0x1

    if-nez v3, :cond_2

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    and-int/2addr v0, v2

    if-nez v0, :cond_2

    const-string v0, "SoapWEBSETUPDataInfo"

    const-string v2, "Cannot enable EC without delete account -> unregistered"

    .line 60
    invoke-static {v0, v2}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    return v4

    :cond_2
    const-string v0, "0"

    const-string v2, "WebServerStatus"

    .line 62
    invoke-virtual {p0, v2}, Lepson/epsonconnectregistration/SoapWEBSETUPDataInfo;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "0"

    const-string v2, "XMPPServerStatus"

    invoke-virtual {p0, v2}, Lepson/epsonconnectregistration/SoapWEBSETUPDataInfo;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    const-string v0, "Version"

    .line 65
    invoke-virtual {p0, v0}, Lepson/epsonconnectregistration/SoapWEBSETUPDataInfo;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    .line 66
    invoke-virtual {v0}, Ljava/lang/Double;->intValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 67
    new-instance v3, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v5

    invoke-virtual {v0}, Ljava/lang/Double;->intValue()I

    move-result v0

    int-to-double v7, v0

    sub-double/2addr v5, v7

    const-wide/high16 v7, 0x4059000000000000L    # 100.0

    mul-double v5, v5, v7

    invoke-direct {v3, v5, v6}, Ljava/lang/Double;-><init>(D)V

    invoke-virtual {v3}, Ljava/lang/Double;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 68
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, v4, :cond_4

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "SoapWEBSETUPDataInfo"

    const-string v2, "WebServerStatus:disable or XMPPServerStatus:disable -> unregistered"

    .line 69
    invoke-static {v0, v2}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v4

    :catch_0
    move-exception v0

    .line 80
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    .line 78
    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V

    :cond_4
    :goto_0
    return v1
.end method

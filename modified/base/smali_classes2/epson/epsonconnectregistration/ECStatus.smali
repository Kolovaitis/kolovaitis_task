.class public final enum Lepson/epsonconnectregistration/ECStatus;
.super Ljava/lang/Enum;
.source "ECStatus.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lepson/epsonconnectregistration/ECStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lepson/epsonconnectregistration/ECStatus;

.field public static final enum ERROR_CANNOT_ENABLE:Lepson/epsonconnectregistration/ECStatus;

.field public static final enum ERROR_COMMUNICATION:Lepson/epsonconnectregistration/ECStatus;

.field public static final enum ERROR_NONE:Lepson/epsonconnectregistration/ECStatus;

.field public static final enum ERROR_PRINTER_ADMIN_MODE:Lepson/epsonconnectregistration/ECStatus;

.field public static final enum ERROR_PRINTER_EC_REGISTERED:Lepson/epsonconnectregistration/ECStatus;

.field public static final enum ERROR_PRINTER_NOT_SUPPORT_EC:Lepson/epsonconnectregistration/ECStatus;

.field public static final enum ERROR_SERVICE_UNAVAILABLE:Lepson/epsonconnectregistration/ECStatus;

.field public static final enum ERROR_UNEXPECTED:Lepson/epsonconnectregistration/ECStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .line 4
    new-instance v0, Lepson/epsonconnectregistration/ECStatus;

    const-string v1, "ERROR_NONE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lepson/epsonconnectregistration/ECStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/epsonconnectregistration/ECStatus;->ERROR_NONE:Lepson/epsonconnectregistration/ECStatus;

    .line 5
    new-instance v0, Lepson/epsonconnectregistration/ECStatus;

    const-string v1, "ERROR_COMMUNICATION"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lepson/epsonconnectregistration/ECStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/epsonconnectregistration/ECStatus;->ERROR_COMMUNICATION:Lepson/epsonconnectregistration/ECStatus;

    .line 6
    new-instance v0, Lepson/epsonconnectregistration/ECStatus;

    const-string v1, "ERROR_UNEXPECTED"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lepson/epsonconnectregistration/ECStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/epsonconnectregistration/ECStatus;->ERROR_UNEXPECTED:Lepson/epsonconnectregistration/ECStatus;

    .line 7
    new-instance v0, Lepson/epsonconnectregistration/ECStatus;

    const-string v1, "ERROR_SERVICE_UNAVAILABLE"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lepson/epsonconnectregistration/ECStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/epsonconnectregistration/ECStatus;->ERROR_SERVICE_UNAVAILABLE:Lepson/epsonconnectregistration/ECStatus;

    .line 8
    new-instance v0, Lepson/epsonconnectregistration/ECStatus;

    const-string v1, "ERROR_CANNOT_ENABLE"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Lepson/epsonconnectregistration/ECStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/epsonconnectregistration/ECStatus;->ERROR_CANNOT_ENABLE:Lepson/epsonconnectregistration/ECStatus;

    .line 9
    new-instance v0, Lepson/epsonconnectregistration/ECStatus;

    const-string v1, "ERROR_PRINTER_NOT_SUPPORT_EC"

    const/4 v7, 0x5

    invoke-direct {v0, v1, v7}, Lepson/epsonconnectregistration/ECStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/epsonconnectregistration/ECStatus;->ERROR_PRINTER_NOT_SUPPORT_EC:Lepson/epsonconnectregistration/ECStatus;

    .line 10
    new-instance v0, Lepson/epsonconnectregistration/ECStatus;

    const-string v1, "ERROR_PRINTER_ADMIN_MODE"

    const/4 v8, 0x6

    invoke-direct {v0, v1, v8}, Lepson/epsonconnectregistration/ECStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/epsonconnectregistration/ECStatus;->ERROR_PRINTER_ADMIN_MODE:Lepson/epsonconnectregistration/ECStatus;

    .line 11
    new-instance v0, Lepson/epsonconnectregistration/ECStatus;

    const-string v1, "ERROR_PRINTER_EC_REGISTERED"

    const/4 v9, 0x7

    invoke-direct {v0, v1, v9}, Lepson/epsonconnectregistration/ECStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/epsonconnectregistration/ECStatus;->ERROR_PRINTER_EC_REGISTERED:Lepson/epsonconnectregistration/ECStatus;

    const/16 v0, 0x8

    .line 3
    new-array v0, v0, [Lepson/epsonconnectregistration/ECStatus;

    sget-object v1, Lepson/epsonconnectregistration/ECStatus;->ERROR_NONE:Lepson/epsonconnectregistration/ECStatus;

    aput-object v1, v0, v2

    sget-object v1, Lepson/epsonconnectregistration/ECStatus;->ERROR_COMMUNICATION:Lepson/epsonconnectregistration/ECStatus;

    aput-object v1, v0, v3

    sget-object v1, Lepson/epsonconnectregistration/ECStatus;->ERROR_UNEXPECTED:Lepson/epsonconnectregistration/ECStatus;

    aput-object v1, v0, v4

    sget-object v1, Lepson/epsonconnectregistration/ECStatus;->ERROR_SERVICE_UNAVAILABLE:Lepson/epsonconnectregistration/ECStatus;

    aput-object v1, v0, v5

    sget-object v1, Lepson/epsonconnectregistration/ECStatus;->ERROR_CANNOT_ENABLE:Lepson/epsonconnectregistration/ECStatus;

    aput-object v1, v0, v6

    sget-object v1, Lepson/epsonconnectregistration/ECStatus;->ERROR_PRINTER_NOT_SUPPORT_EC:Lepson/epsonconnectregistration/ECStatus;

    aput-object v1, v0, v7

    sget-object v1, Lepson/epsonconnectregistration/ECStatus;->ERROR_PRINTER_ADMIN_MODE:Lepson/epsonconnectregistration/ECStatus;

    aput-object v1, v0, v8

    sget-object v1, Lepson/epsonconnectregistration/ECStatus;->ERROR_PRINTER_EC_REGISTERED:Lepson/epsonconnectregistration/ECStatus;

    aput-object v1, v0, v9

    sput-object v0, Lepson/epsonconnectregistration/ECStatus;->$VALUES:[Lepson/epsonconnectregistration/ECStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lepson/epsonconnectregistration/ECStatus;
    .locals 1

    .line 3
    const-class v0, Lepson/epsonconnectregistration/ECStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lepson/epsonconnectregistration/ECStatus;

    return-object p0
.end method

.method public static values()[Lepson/epsonconnectregistration/ECStatus;
    .locals 1

    .line 3
    sget-object v0, Lepson/epsonconnectregistration/ECStatus;->$VALUES:[Lepson/epsonconnectregistration/ECStatus;

    invoke-virtual {v0}, [Lepson/epsonconnectregistration/ECStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lepson/epsonconnectregistration/ECStatus;

    return-object v0
.end method

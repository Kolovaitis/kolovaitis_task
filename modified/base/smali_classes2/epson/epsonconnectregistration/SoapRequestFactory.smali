.class public Lepson/epsonconnectregistration/SoapRequestFactory;
.super Ljava/lang/Object;
.source "SoapRequestFactory.java"


# static fields
.field private static final PRINER_KEY:Ljava/lang/String; = "Y2OFL0wVYau52Rlrt7vNpt/8T4F4IjTMvqP1qn4Id0ToRYTlJLxG1CnD9C49Bl205RGyK23HeJzW+TX6HX7m6HwKuiJJKl+K5WQ6fLBY5JU"

.field private static final PRINER_KEY2:Ljava/lang/String; = "w4Wv3krd2XpBQbvkSX/ZvnbBvIPBW2Msq0E5VwlZX9SkiGHgynhJzOO+flksH6SEieMYlxNjeIBWdXCTu+QuXwO5zDZ89RjQmn7Qm/qYTNQ="

.field private static final TAG_DESTINATION:Ljava/lang/String; = "%DESTINATION%"

.field private static final TAG_GROUPID:Ljava/lang/String; = "%GROUPID%"

.field private static final TAG_LANG:Ljava/lang/String; = "%LANG%"

.field private static final TAG_PRINER_KEY:Ljava/lang/String; = "%PRINER_KEY%"

.field private static final TAG_UUID:Ljava/lang/String; = "%UUID%"

.field private static final XML_GETREQUEST:Ljava/lang/String; = "soap/getrequest.xml"

.field private static final XML_GETREQUEST_WEBSETUP:Ljava/lang/String; = "soap/getrequest_WEBSETUP.xml"

.field private static final XML_SETREQUEST_WEBSETUP:Ljava/lang/String; = "soap/setrequest_WEBSETUP.xml"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createGetRequest(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    const-string v0, "soap/getrequest.xml"

    .line 34
    invoke-static {p0, v0}, Lepson/epsonconnectregistration/SoapRequestFactory;->getXmlTemplate(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    const-string v0, "%DESTINATION%"

    .line 36
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, v0, p1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    const-string p1, "%UUID%"

    .line 37
    invoke-static {}, Lepson/epsonconnectregistration/SoapRequestFactory;->getUUID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static createGetRequestWEBSETUP(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    const-string v0, "soap/getrequest_WEBSETUP.xml"

    .line 44
    invoke-static {p0, v0}, Lepson/epsonconnectregistration/SoapRequestFactory;->getXmlTemplate(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    const-string v0, "%DESTINATION%"

    .line 46
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, v0, p1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    const-string p1, "%UUID%"

    .line 47
    invoke-static {}, Lepson/epsonconnectregistration/SoapRequestFactory;->getUUID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static createSetRequestWEBSETUP(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    const-string v0, "soap/setrequest_WEBSETUP.xml"

    .line 54
    invoke-static {p0, v0}, Lepson/epsonconnectregistration/SoapRequestFactory;->getXmlTemplate(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    const-string v0, "%DESTINATION%"

    .line 56
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, v0, p1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    const-string p1, "%UUID%"

    .line 57
    invoke-static {}, Lepson/epsonconnectregistration/SoapRequestFactory;->getUUID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    const-string p1, "%LANG%"

    .line 58
    invoke-virtual {p0, p1, p2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    const-string p1, "Beta"

    const-string p2, "FC"

    .line 60
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const-string p1, "%PRINER_KEY%"

    const-string p2, "w4Wv3krd2XpBQbvkSX/ZvnbBvIPBW2Msq0E5VwlZX9SkiGHgynhJzOO+flksH6SEieMYlxNjeIBWdXCTu+QuXwO5zDZ89RjQmn7Qm/qYTNQ="

    .line 61
    invoke-virtual {p0, p1, p2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_0
    const-string p1, "%PRINER_KEY%"

    const-string p2, "Y2OFL0wVYau52Rlrt7vNpt/8T4F4IjTMvqP1qn4Id0ToRYTlJLxG1CnD9C49Bl205RGyK23HeJzW+TX6HX7m6HwKuiJJKl+K5WQ6fLBY5JU"

    .line 63
    invoke-virtual {p0, p1, p2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method private static getUUID()Ljava/lang/String;
    .locals 1

    .line 108
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getXmlTemplate(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .line 78
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v1, 0x0

    .line 84
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object p0

    invoke-virtual {p0, p1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 85
    :try_start_1
    new-instance p1, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/InputStreamReader;

    invoke-direct {v2, p0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {p1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 88
    :goto_0
    :try_start_2
    invoke-virtual {p1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 89
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :cond_0
    if-eqz p0, :cond_1

    .line 92
    :try_start_3
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    .line 93
    :cond_1
    invoke-virtual {p1}, Ljava/io/BufferedReader;->close()V

    goto :goto_4

    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, p1

    move-object p1, v3

    goto :goto_1

    :catchall_1
    move-exception p1

    goto :goto_1

    :catchall_2
    move-exception p0

    move-object p1, p0

    move-object p0, v1

    :goto_1
    if-eqz p0, :cond_2

    .line 92
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    goto :goto_2

    :catch_0
    move-exception p0

    goto :goto_3

    :cond_2
    :goto_2
    if-eqz v1, :cond_3

    .line 93
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    :cond_3
    throw p1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    .line 97
    :goto_3
    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V

    .line 100
    :goto_4
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

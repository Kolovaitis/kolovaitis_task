.class public Lepson/colorcorrection/SDIC_PWC;
.super Lepson/colorcorrection/SDIC_Exception;
.source "SDIC_PWC.java"

# interfaces
.implements Lepson/colorcorrection/SDIC_CommonDefine;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "opencv_java3"

    .line 20
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Lepson/colorcorrection/SDIC_Exception;-><init>()V

    return-void
.end method

.method private static ColorDodge(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lepson/colorcorrection/SDIC_MAT_PTR;)I
    .locals 15

    .line 288
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->channels()I

    move-result v0

    .line 289
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->total()J

    move-result-wide v1

    int-to-long v3, v0

    mul-long v1, v1, v3

    long-to-int v2, v1

    .line 295
    invoke-static {p0}, Lepson/colorcorrection/SDIC_BCSC;->GetPixcelData(Lorg/opencv/core/Mat;)[B

    move-result-object v1

    .line 296
    invoke-static/range {p1 .. p1}, Lepson/colorcorrection/SDIC_BCSC;->GetPixcelData(Lorg/opencv/core/Mat;)[B

    move-result-object v3

    .line 298
    invoke-static {p0}, Lepson/colorcorrection/SDIC_BCSC;->MakePixcelData(Lorg/opencv/core/Mat;)[B

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    :goto_0
    if-ge v6, v2, :cond_4

    const/4 v7, 0x0

    :goto_1
    const/4 v8, 0x3

    if-ge v7, v8, :cond_3

    add-int v8, v6, v7

    .line 303
    aget-byte v9, v3, v8

    const/16 v10, 0xff

    and-int/2addr v9, v10

    if-gtz v9, :cond_0

    const/4 v9, 0x1

    .line 309
    :cond_0
    aget-byte v11, v1, v8

    and-int/2addr v11, v10

    mul-int/lit16 v11, v11, 0xff

    int-to-double v11, v11

    int-to-double v13, v9

    div-double/2addr v11, v13

    double-to-int v9, v11

    if-le v9, v10, :cond_1

    const/16 v9, 0xff

    :cond_1
    if-gez v9, :cond_2

    const/4 v9, 0x0

    :cond_2
    int-to-byte v9, v9

    .line 312
    aput-byte v9, v4, v8

    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_3
    add-int/2addr v6, v0

    goto :goto_0

    .line 315
    :cond_4
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->rows()I

    move-result v0

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->cols()I

    move-result v1

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->type()I

    move-result v2

    invoke-static {v0, v1, v2, v4}, Lepson/colorcorrection/SDIC_BCSC;->SetPixcelData(III[B)Lorg/opencv/core/Mat;

    move-result-object v0

    move-object/from16 v1, p2

    .line 316
    invoke-virtual {v1, v0}, Lepson/colorcorrection/SDIC_MAT_PTR;->setMat(Lorg/opencv/core/Mat;)V

    return v5
.end method

.method private static LogMemory(Ljava/lang/String;)V
    .locals 9

    .line 574
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    .line 575
    invoke-virtual {v0}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v1

    long-to-float v1, v1

    .line 576
    invoke-virtual {v0}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v2

    long-to-float v2, v2

    .line 577
    invoke-virtual {v0}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v3

    long-to-float v0, v3

    sub-float v3, v2, v0

    const-string v4, "LogMemory"

    const-string v5, "max:%.2fMB total:%.2fMB free:%.2fMB used:%.2fMB %s"

    const/4 v6, 0x5

    .line 579
    new-array v6, v6, [Ljava/lang/Object;

    const/high16 v7, 0x49800000    # 1048576.0f

    div-float/2addr v1, v7

    .line 581
    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    const/4 v8, 0x0

    aput-object v1, v6, v8

    div-float/2addr v2, v7

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v6, v2

    div-float/2addr v0, v7

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    const/4 v1, 0x2

    aput-object v0, v6, v1

    div-float/2addr v3, v7

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    const/4 v1, 0x3

    aput-object v0, v6, v1

    const/4 v0, 0x4

    aput-object p0, v6, v0

    .line 580
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    .line 579
    invoke-static {v4, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private static MakeCleaningFilter(Lorg/opencv/core/Mat;Lepson/colorcorrection/SDIC_MAT_PTR;DD)I
    .locals 15

    const/4 v1, 0x0

    .line 514
    :try_start_0
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->size()Lorg/opencv/core/Size;

    move-result-object v0

    iget-wide v2, v0, Lorg/opencv/core/Size;->width:D

    div-double v2, p4, v2

    .line 515
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->size()Lorg/opencv/core/Size;

    move-result-object v0

    iget-wide v4, v0, Lorg/opencv/core/Size;->height:D

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->size()Lorg/opencv/core/Size;

    move-result-object v0

    iget-wide v6, v0, Lorg/opencv/core/Size;->width:D

    cmpg-double v0, v4, v6

    if-gez v0, :cond_0

    .line 516
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->size()Lorg/opencv/core/Size;

    move-result-object v0

    iget-wide v2, v0, Lorg/opencv/core/Size;->height:D

    div-double v2, p4, v2

    .line 520
    :cond_0
    new-instance v0, Lorg/opencv/core/Size;

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->size()Lorg/opencv/core/Size;

    move-result-object v4

    iget-wide v4, v4, Lorg/opencv/core/Size;->width:D

    mul-double v4, v4, v2

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->size()Lorg/opencv/core/Size;

    move-result-object v6

    iget-wide v6, v6, Lorg/opencv/core/Size;->height:D

    mul-double v6, v6, v2

    invoke-direct {v0, v4, v5, v6, v7}, Lorg/opencv/core/Size;-><init>(DD)V

    .line 521
    new-instance v2, Lorg/opencv/core/Mat;

    invoke-direct {v2}, Lorg/opencv/core/Mat;-><init>()V
    :try_end_0
    .catch Lepson/colorcorrection/SDIC_Exception; {:try_start_0 .. :try_end_0} :catch_e
    .catch Lorg/opencv/core/CvException; {:try_start_0 .. :try_end_0} :catch_d
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_c
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-object v3, p0

    .line 522
    :try_start_1
    invoke-static {p0, v2, v0}, Lorg/opencv/imgproc/Imgproc;->resize(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Size;)V

    const-wide/16 v4, 0x0

    cmpl-double v0, p2, v4

    if-lez v0, :cond_3

    .line 527
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->size()Lorg/opencv/core/Size;

    move-result-object v0

    iget-wide v4, v0, Lorg/opencv/core/Size;->height:D

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->size()Lorg/opencv/core/Size;

    move-result-object v0

    iget-wide v6, v0, Lorg/opencv/core/Size;->width:D

    cmpg-double v0, v4, v6

    if-gez v0, :cond_1

    .line 528
    invoke-virtual {v2}, Lorg/opencv/core/Mat;->size()Lorg/opencv/core/Size;

    move-result-object v0

    iget-wide v4, v0, Lorg/opencv/core/Size;->height:D

    mul-double v4, v4, p2

    double-to-int v0, v4

    goto :goto_0

    .line 530
    :cond_1
    invoke-virtual {v2}, Lorg/opencv/core/Mat;->size()Lorg/opencv/core/Size;

    move-result-object v0

    iget-wide v4, v0, Lorg/opencv/core/Size;->width:D

    mul-double v4, v4, p2

    double-to-int v0, v4

    .line 537
    :goto_0
    rem-int/lit8 v4, v0, 0x2

    if-nez v4, :cond_2

    add-int/lit8 v0, v0, 0x1

    .line 542
    :cond_2
    new-instance v4, Lorg/opencv/core/Mat;

    invoke-direct {v4}, Lorg/opencv/core/Mat;-><init>()V
    :try_end_1
    .catch Lepson/colorcorrection/SDIC_Exception; {:try_start_1 .. :try_end_1} :catch_b
    .catch Lorg/opencv/core/CvException; {:try_start_1 .. :try_end_1} :catch_a
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_9
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 543
    :try_start_2
    new-instance v10, Lorg/opencv/core/Size;

    int-to-double v0, v0

    invoke-direct {v10, v0, v1, v0, v1}, Lorg/opencv/core/Size;-><init>(DD)V

    const-wide/16 v11, 0x0

    const-wide/16 v13, 0x0

    move-object v8, v2

    move-object v9, v4

    invoke-static/range {v8 .. v14}, Lorg/opencv/imgproc/Imgproc;->GaussianBlur(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Size;DD)V

    .line 546
    invoke-virtual/range {p1 .. p1}, Lepson/colorcorrection/SDIC_MAT_PTR;->getMat()Lorg/opencv/core/Mat;

    move-result-object v0

    .line 547
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->size()Lorg/opencv/core/Size;

    move-result-object v1

    invoke-static {v4, v0, v1}, Lorg/opencv/imgproc/Imgproc;->resize(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Size;)V
    :try_end_2
    .catch Lepson/colorcorrection/SDIC_Exception; {:try_start_2 .. :try_end_2} :catch_5
    .catch Lorg/opencv/core/CvException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object/from16 v3, p1

    .line 548
    :try_start_3
    invoke-virtual {v3, v0}, Lepson/colorcorrection/SDIC_MAT_PTR;->setMat(Lorg/opencv/core/Mat;)V
    :try_end_3
    .catch Lepson/colorcorrection/SDIC_Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lorg/opencv/core/CvException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 558
    invoke-virtual {v2}, Lorg/opencv/core/Mat;->release()V

    .line 562
    invoke-virtual {v4}, Lorg/opencv/core/Mat;->release()V

    const/4 v0, 0x0

    goto/16 :goto_8

    :catch_0
    move-exception v0

    goto :goto_2

    :catchall_0
    move-exception v0

    goto/16 :goto_9

    :catch_1
    move-object/from16 v3, p1

    :catch_2
    move-object v1, v4

    goto :goto_3

    :catch_3
    move-object/from16 v3, p1

    :catch_4
    move-object v1, v4

    goto :goto_6

    :catch_5
    move-exception v0

    move-object/from16 v3, p1

    goto :goto_2

    :cond_3
    move-object/from16 v3, p1

    .line 533
    :try_start_4
    new-instance v0, Lepson/colorcorrection/SDIC_Exception;

    const/4 v4, -0x7

    invoke-direct {v0, v4}, Lepson/colorcorrection/SDIC_Exception;-><init>(I)V

    throw v0
    :try_end_4
    .catch Lepson/colorcorrection/SDIC_Exception; {:try_start_4 .. :try_end_4} :catch_8
    .catch Lorg/opencv/core/CvException; {:try_start_4 .. :try_end_4} :catch_7
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_6
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catch_6
    nop

    goto :goto_3

    :catch_7
    nop

    goto :goto_6

    :catch_8
    move-exception v0

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_a

    :catch_9
    move-object/from16 v3, p1

    goto :goto_3

    :catch_a
    move-object/from16 v3, p1

    goto :goto_6

    :catch_b
    move-exception v0

    move-object/from16 v3, p1

    :goto_1
    move-object v4, v1

    :goto_2
    move-object v1, v2

    goto :goto_7

    :catchall_2
    move-exception v0

    move-object v2, v1

    goto :goto_a

    :catch_c
    move-object/from16 v3, p1

    move-object v2, v1

    :goto_3
    const/16 v0, -0x190

    if-eqz v2, :cond_4

    .line 558
    invoke-virtual {v2}, Lorg/opencv/core/Mat;->release()V

    :cond_4
    if-eqz v1, :cond_5

    .line 562
    :goto_4
    invoke-virtual {v1}, Lorg/opencv/core/Mat;->release()V

    .line 567
    :cond_5
    :goto_5
    invoke-virtual/range {p1 .. p1}, Lepson/colorcorrection/SDIC_MAT_PTR;->releaseMat()V

    goto :goto_8

    :catch_d
    move-object/from16 v3, p1

    move-object v2, v1

    :goto_6
    const/16 v0, -0x64

    if-eqz v2, :cond_6

    .line 558
    invoke-virtual {v2}, Lorg/opencv/core/Mat;->release()V

    :cond_6
    if-eqz v1, :cond_5

    goto :goto_4

    :catch_e
    move-exception v0

    move-object/from16 v3, p1

    move-object v4, v1

    .line 551
    :goto_7
    :try_start_5
    iget v0, v0, Lepson/colorcorrection/SDIC_Exception;->mCode:I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    if-eqz v1, :cond_7

    .line 558
    invoke-virtual {v1}, Lorg/opencv/core/Mat;->release()V

    :cond_7
    if-eqz v4, :cond_8

    .line 562
    invoke-virtual {v4}, Lorg/opencv/core/Mat;->release()V

    :cond_8
    if-eqz v0, :cond_9

    goto :goto_5

    :cond_9
    :goto_8
    return v0

    :catchall_3
    move-exception v0

    move-object v2, v1

    :goto_9
    move-object v1, v4

    :goto_a
    if-eqz v2, :cond_a

    .line 558
    invoke-virtual {v2}, Lorg/opencv/core/Mat;->release()V

    :cond_a
    if-eqz v1, :cond_b

    .line 562
    invoke-virtual {v1}, Lorg/opencv/core/Mat;->release()V

    .line 567
    :cond_b
    throw v0
.end method

.method private static Multiply(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)I
    .locals 12

    .line 221
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->channels()I

    move-result v0

    .line 222
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->total()J

    move-result-wide v1

    int-to-long v3, v0

    mul-long v1, v1, v3

    long-to-int v2, v1

    .line 224
    invoke-static {p0}, Lepson/colorcorrection/SDIC_BCSC;->GetPixcelData(Lorg/opencv/core/Mat;)[B

    move-result-object v1

    .line 225
    invoke-static {p1}, Lepson/colorcorrection/SDIC_BCSC;->GetPixcelData(Lorg/opencv/core/Mat;)[B

    move-result-object p1

    .line 226
    invoke-static {p0}, Lepson/colorcorrection/SDIC_BCSC;->GetPixcelData(Lorg/opencv/core/Mat;)[B

    move-result-object p0

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v2, :cond_3

    const/4 v5, 0x0

    :goto_1
    const/4 v6, 0x3

    if-ge v5, v6, :cond_2

    add-int v6, v4, v5

    .line 232
    aget-byte v7, v1, v6

    and-int/lit16 v7, v7, 0xff

    aget-byte v8, p1, v6

    and-int/lit16 v8, v8, 0xff

    mul-int v7, v7, v8

    int-to-double v7, v7

    const-wide v9, 0x3f70101010101010L    # 0.00392156862745098

    mul-double v7, v7, v9

    const-wide v9, 0x406fe00000000000L    # 255.0

    cmpl-double v11, v7, v9

    if-lez v11, :cond_0

    move-wide v7, v9

    :cond_0
    const-wide/16 v9, 0x0

    cmpg-double v11, v7, v9

    if-gez v11, :cond_1

    move-wide v7, v9

    :cond_1
    double-to-int v7, v7

    int-to-byte v7, v7

    .line 235
    aput-byte v7, p0, v6

    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_2
    add-int/2addr v4, v0

    goto :goto_0

    .line 239
    :cond_3
    invoke-virtual {p2, v3, v3, p0}, Lorg/opencv/core/Mat;->put(II[B)I

    return v3
.end method

.method public static PaperWhiteCorrection(Lorg/opencv/core/Mat;Lepson/colorcorrection/SDIC_MAT_PTR;)I
    .locals 2

    .line 25
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->getNativeObjAddr()J

    move-result-wide v0

    .line 26
    invoke-virtual {p1}, Lepson/colorcorrection/SDIC_MAT_PTR;->getMat()Lorg/opencv/core/Mat;

    move-result-object p0

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->getNativeObjAddr()J

    move-result-wide p0

    .line 25
    invoke-static {v0, v1, p0, p1}, Lcom/epson/cameracopy/device/RectangleDetector;->paperWhiteCorrection(JJ)I

    move-result p0

    return p0
.end method

.method public static PaperWhiteCorrection2(Lorg/opencv/core/Mat;Lepson/colorcorrection/SDIC_MAT_PTR;)I
    .locals 3

    .line 32
    invoke-static {}, Lcom/epson/cameracopy/device/RectangleDetector;->getDefaultPaperWhiteCorrection()Landroid/graphics/Point;

    move-result-object v0

    .line 33
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->getNativeObjAddr()J

    move-result-wide v1

    .line 34
    invoke-virtual {p1}, Lepson/colorcorrection/SDIC_MAT_PTR;->getMat()Lorg/opencv/core/Mat;

    move-result-object p0

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->getNativeObjAddr()J

    move-result-wide p0

    .line 33
    invoke-static {v1, v2, p0, p1, v0}, Lcom/epson/cameracopy/device/RectangleDetector;->paperWhiteCorrection(JJLandroid/graphics/Point;)I

    move-result p0

    return p0
.end method

.method public static PaperWhiteCorrectionJAVA(Lorg/opencv/core/Mat;Lepson/colorcorrection/SDIC_MAT_PTR;)I
    .locals 16

    .line 42
    invoke-virtual/range {p0 .. p0}, Lorg/opencv/core/Mat;->cols()I

    move-result v0

    .line 43
    invoke-virtual/range {p0 .. p0}, Lorg/opencv/core/Mat;->rows()I

    move-result v1

    .line 44
    invoke-virtual/range {p0 .. p0}, Lorg/opencv/core/Mat;->channels()I

    move-result v2

    .line 45
    invoke-virtual/range {p0 .. p0}, Lorg/opencv/core/Mat;->type()I

    move-result v3

    .line 46
    new-instance v4, Lorg/opencv/core/Mat;

    invoke-direct {v4, v1, v0, v3}, Lorg/opencv/core/Mat;-><init>(III)V

    .line 49
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v3

    .line 50
    invoke-virtual {v3}, Ljava/lang/Runtime;->maxMemory()J

    .line 51
    invoke-virtual {v3}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v5

    long-to-float v5, v5

    const/high16 v6, 0x49800000    # 1048576.0f

    div-float/2addr v5, v6

    .line 52
    invoke-virtual {v3}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v7

    long-to-float v3, v7

    div-float/2addr v3, v6

    sub-float v3, v5, v3

    sub-float/2addr v5, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v5, v3

    const/high16 v7, 0x40400000    # 3.0f

    div-float/2addr v5, v7

    cmpl-float v7, v5, v3

    if-lez v7, :cond_0

    move v3, v5

    :cond_0
    mul-float v5, v3, v6

    mul-int v7, v0, v2

    int-to-float v7, v7

    div-float/2addr v5, v7

    float-to-int v5, v5

    .line 61
    invoke-virtual/range {p0 .. p0}, Lorg/opencv/core/Mat;->rows()I

    move-result v7

    if-ge v5, v7, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual/range {p0 .. p0}, Lorg/opencv/core/Mat;->rows()I

    move-result v5

    .line 63
    :goto_0
    new-instance v7, Lorg/opencv/core/Rect;

    const/4 v8, 0x0

    invoke-direct {v7, v8, v8, v0, v5}, Lorg/opencv/core/Rect;-><init>(IIII)V

    int-to-double v9, v1

    int-to-double v11, v5

    div-double/2addr v9, v11

    .line 65
    invoke-static {v9, v10}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v9

    double-to-int v9, v9

    const-string v10, "PWC S - [%d] w:%d h:%d c:%d m:%.2fMB use:%.2fMB"

    const/4 v11, 0x6

    .line 67
    new-array v11, v11, [Ljava/lang/Object;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v11, v8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    const/4 v13, 0x1

    aput-object v12, v11, v13

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    const/4 v14, 0x2

    aput-object v12, v11, v14

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    const/4 v15, 0x3

    aput-object v12, v11, v15

    const/4 v12, 0x4

    mul-int v0, v0, v1

    mul-int v0, v0, v2

    int-to-float v0, v0

    div-float/2addr v0, v6

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    aput-object v0, v11, v12

    const/4 v0, 0x5

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v11, v0

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lepson/colorcorrection/SDIC_PWC;->LogMemory(Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v2, 0x0

    :goto_1
    if-lez v1, :cond_4

    if-ge v1, v5, :cond_2

    move v2, v1

    goto :goto_2

    :cond_2
    move v2, v5

    .line 71
    :goto_2
    iput v2, v7, Lorg/opencv/core/Rect;->height:I

    move-object/from16 v6, p0

    .line 72
    invoke-virtual {v6, v7}, Lorg/opencv/core/Mat;->submat(Lorg/opencv/core/Rect;)Lorg/opencv/core/Mat;

    move-result-object v2

    .line 73
    invoke-virtual {v4, v7}, Lorg/opencv/core/Mat;->submat(Lorg/opencv/core/Rect;)Lorg/opencv/core/Mat;

    move-result-object v10

    .line 75
    invoke-static {v2, v10}, Lepson/colorcorrection/SDIC_PWC;->PaperWhiteCorrection_ROI(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)I

    move-result v11

    .line 76
    invoke-virtual {v2}, Lorg/opencv/core/Mat;->release()V

    .line 77
    invoke-virtual {v10}, Lorg/opencv/core/Mat;->release()V

    const-string v2, "PWC R - [%d/%d] %d"

    .line 79
    new-array v10, v15, [Ljava/lang/Object;

    add-int/2addr v0, v13

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v8

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v13

    iget v12, v7, Lorg/opencv/core/Rect;->height:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v14

    invoke-static {v2, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lepson/colorcorrection/SDIC_PWC;->LogMemory(Ljava/lang/String;)V

    if-eqz v11, :cond_3

    move v2, v11

    goto :goto_3

    :cond_3
    sub-int/2addr v1, v5

    .line 69
    iget v2, v7, Lorg/opencv/core/Rect;->y:I

    add-int/2addr v2, v5

    iput v2, v7, Lorg/opencv/core/Rect;->y:I

    move v2, v11

    goto :goto_1

    :cond_4
    :goto_3
    if-nez v2, :cond_5

    move-object/from16 v0, p1

    .line 87
    invoke-virtual {v0, v4}, Lepson/colorcorrection/SDIC_MAT_PTR;->setMat(Lorg/opencv/core/Mat;)V

    :cond_5
    const-string v0, "PWC E use:%.2fMB"

    .line 90
    new-array v1, v13, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v8

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lepson/colorcorrection/SDIC_PWC;->LogMemory(Ljava/lang/String;)V

    return v2
.end method

.method public static PaperWhiteCorrection_ROI(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)I
    .locals 10

    const/4 v0, -0x1

    .line 100
    :try_start_0
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->empty()Z

    move-result v1

    if-nez v1, :cond_7

    .line 102
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->dataAddr()J

    move-result-wide v1

    invoke-virtual {p1}, Lorg/opencv/core/Mat;->dataAddr()J

    move-result-wide v3

    cmp-long v5, v1, v3

    if-eqz v5, :cond_6

    .line 104
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->isContinuous()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 113
    new-instance v1, Lepson/colorcorrection/SDIC_MAT_PTR;

    invoke-direct {v1}, Lepson/colorcorrection/SDIC_MAT_PTR;-><init>()V

    .line 115
    new-instance v8, Lepson/colorcorrection/SDIC_MAT_PTR;

    invoke-direct {v8}, Lepson/colorcorrection/SDIC_MAT_PTR;-><init>()V

    .line 116
    new-instance v9, Lepson/colorcorrection/SDIC_MAT_PTR;

    invoke-direct {v9}, Lepson/colorcorrection/SDIC_MAT_PTR;-><init>()V

    const-wide v4, 0x3fa999999999999aL    # 0.05

    const-wide/high16 v6, 0x4054000000000000L    # 80.0

    move-object v2, p0

    move-object v3, v8

    .line 118
    invoke-static/range {v2 .. v7}, Lepson/colorcorrection/SDIC_PWC;->MakeCleaningFilter(Lorg/opencv/core/Mat;Lepson/colorcorrection/SDIC_MAT_PTR;DD)I

    move-result v2

    if-nez v2, :cond_4

    const/high16 v2, 0x40000000    # 2.0f

    .line 123
    invoke-static {p0, v9, v2}, Lepson/colorcorrection/SDIC_PWC;->Sharpness(Lorg/opencv/core/Mat;Lepson/colorcorrection/SDIC_MAT_PTR;F)I

    move-result v2

    if-nez v2, :cond_3

    .line 128
    invoke-virtual {v9}, Lepson/colorcorrection/SDIC_MAT_PTR;->getMat()Lorg/opencv/core/Mat;

    move-result-object v2

    invoke-virtual {v8}, Lepson/colorcorrection/SDIC_MAT_PTR;->getMat()Lorg/opencv/core/Mat;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lepson/colorcorrection/SDIC_PWC;->ColorDodge(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lepson/colorcorrection/SDIC_MAT_PTR;)I

    move-result v2

    if-nez v2, :cond_2

    .line 133
    invoke-virtual {v8}, Lepson/colorcorrection/SDIC_MAT_PTR;->releaseMat()V

    .line 134
    invoke-virtual {v9}, Lepson/colorcorrection/SDIC_MAT_PTR;->releaseMat()V

    .line 138
    new-instance v2, Lepson/colorcorrection/SDIC_MAT_PTR;

    invoke-direct {v2}, Lepson/colorcorrection/SDIC_MAT_PTR;-><init>()V

    .line 140
    new-instance v9, Lepson/colorcorrection/SDIC_MAT_PTR;

    invoke-direct {v9}, Lepson/colorcorrection/SDIC_MAT_PTR;-><init>()V

    const-wide/high16 v5, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v7, 0x4054000000000000L    # 80.0

    move-object v3, p0

    move-object v4, v9

    .line 141
    invoke-static/range {v3 .. v8}, Lepson/colorcorrection/SDIC_PWC;->MakeCleaningFilter(Lorg/opencv/core/Mat;Lepson/colorcorrection/SDIC_MAT_PTR;DD)I

    move-result v3

    if-nez v3, :cond_1

    .line 146
    invoke-virtual {v9}, Lepson/colorcorrection/SDIC_MAT_PTR;->getMat()Lorg/opencv/core/Mat;

    move-result-object v3

    invoke-static {p0, v3, v2}, Lepson/colorcorrection/SDIC_PWC;->ColorDodge(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lepson/colorcorrection/SDIC_MAT_PTR;)I

    move-result p0

    if-nez p0, :cond_0

    .line 151
    invoke-virtual {v9}, Lepson/colorcorrection/SDIC_MAT_PTR;->releaseMat()V

    .line 155
    invoke-virtual {v2}, Lepson/colorcorrection/SDIC_MAT_PTR;->getMat()Lorg/opencv/core/Mat;

    move-result-object p0

    invoke-virtual {v1}, Lepson/colorcorrection/SDIC_MAT_PTR;->getMat()Lorg/opencv/core/Mat;

    move-result-object v3

    invoke-static {p0, v3, p1}, Lepson/colorcorrection/SDIC_PWC;->Multiply(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)I

    .line 157
    invoke-virtual {v2}, Lepson/colorcorrection/SDIC_MAT_PTR;->releaseMat()V

    .line 158
    invoke-virtual {v1}, Lepson/colorcorrection/SDIC_MAT_PTR;->releaseMat()V

    const/4 p0, 0x0

    goto :goto_0

    .line 148
    :cond_0
    new-instance v1, Lepson/colorcorrection/SDIC_Exception;

    invoke-direct {v1, p0}, Lepson/colorcorrection/SDIC_Exception;-><init>(I)V

    throw v1

    .line 143
    :cond_1
    new-instance p0, Lepson/colorcorrection/SDIC_Exception;

    invoke-direct {p0, v3}, Lepson/colorcorrection/SDIC_Exception;-><init>(I)V

    throw p0

    .line 130
    :cond_2
    new-instance p0, Lepson/colorcorrection/SDIC_Exception;

    invoke-direct {p0, v2}, Lepson/colorcorrection/SDIC_Exception;-><init>(I)V

    throw p0

    .line 125
    :cond_3
    new-instance p0, Lepson/colorcorrection/SDIC_Exception;

    invoke-direct {p0, v2}, Lepson/colorcorrection/SDIC_Exception;-><init>(I)V

    throw p0

    .line 120
    :cond_4
    new-instance p0, Lepson/colorcorrection/SDIC_Exception;

    invoke-direct {p0, v2}, Lepson/colorcorrection/SDIC_Exception;-><init>(I)V

    throw p0

    .line 105
    :cond_5
    new-instance p0, Lepson/colorcorrection/SDIC_Exception;

    const/4 v1, -0x3

    invoke-direct {p0, v1}, Lepson/colorcorrection/SDIC_Exception;-><init>(I)V

    throw p0

    .line 103
    :cond_6
    new-instance p0, Lepson/colorcorrection/SDIC_Exception;

    invoke-direct {p0, v0}, Lepson/colorcorrection/SDIC_Exception;-><init>(I)V

    throw p0

    .line 101
    :cond_7
    new-instance p0, Lepson/colorcorrection/SDIC_Exception;

    const/4 v1, -0x2

    invoke-direct {p0, v1}, Lepson/colorcorrection/SDIC_Exception;-><init>(I)V

    throw p0
    :try_end_0
    .catch Lepson/colorcorrection/SDIC_Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lorg/opencv/core/CvException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception p0

    goto :goto_1

    :catch_0
    const/16 p0, -0x190

    goto :goto_0

    :catch_1
    const/16 p0, -0x64

    goto :goto_0

    :catch_2
    move-exception p0

    .line 161
    :try_start_1
    iget p0, p0, Lepson/colorcorrection/SDIC_Exception;->mCode:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    if-eqz p0, :cond_8

    .line 171
    invoke-virtual {p1}, Lorg/opencv/core/Mat;->empty()Z

    move-result v1

    if-nez v1, :cond_8

    if-eq p0, v0, :cond_8

    .line 172
    invoke-virtual {p1}, Lorg/opencv/core/Mat;->release()V

    :cond_8
    return p0

    .line 166
    :goto_1
    throw p0
.end method

.method private static Sharpness(Lorg/opencv/core/Mat;Lepson/colorcorrection/SDIC_MAT_PTR;F)I
    .locals 6

    .line 180
    invoke-virtual {p1}, Lepson/colorcorrection/SDIC_MAT_PTR;->getMat()Lorg/opencv/core/Mat;

    move-result-object v0

    .line 185
    new-instance v1, Lorg/opencv/core/Mat;

    const/4 v2, 0x3

    const/4 v3, 0x5

    invoke-direct {v1, v2, v2, v3}, Lorg/opencv/core/Mat;-><init>(III)V

    const/high16 v2, -0x40800000    # -1.0f

    mul-float v2, v2, p2

    const/high16 v3, 0x41100000    # 9.0f

    div-float/2addr v2, v3

    float-to-double v4, v2

    .line 189
    invoke-static {v4, v5}, Lorg/opencv/core/Scalar;->all(D)Lorg/opencv/core/Scalar;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/opencv/core/Mat;->setTo(Lorg/opencv/core/Scalar;)Lorg/opencv/core/Mat;

    const/high16 v2, 0x41000000    # 8.0f

    mul-float p2, p2, v2

    div-float/2addr p2, v3

    const/high16 v2, 0x3f800000    # 1.0f

    add-float/2addr p2, v2

    .line 192
    invoke-virtual {v1}, Lorg/opencv/core/Mat;->channels()I

    move-result v2

    .line 193
    new-array v3, v2, [F

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v2, :cond_0

    .line 195
    aput p2, v3, v5

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x1

    .line 197
    invoke-virtual {v1, p2, p2, v3}, Lorg/opencv/core/Mat;->put(II[F)I

    .line 200
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->depth()I

    move-result p2

    invoke-static {p0, v0, p2, v1}, Lorg/opencv/imgproc/Imgproc;->filter2D(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;ILorg/opencv/core/Mat;)V

    .line 201
    invoke-virtual {v1}, Lorg/opencv/core/Mat;->release()V

    .line 204
    invoke-virtual {v0}, Lorg/opencv/core/Mat;->empty()Z

    move-result p0

    if-eqz p0, :cond_1

    const/16 v4, -0x64

    .line 209
    :cond_1
    invoke-virtual {p1, v0}, Lepson/colorcorrection/SDIC_MAT_PTR;->setMat(Lorg/opencv/core/Mat;)V

    return v4
.end method

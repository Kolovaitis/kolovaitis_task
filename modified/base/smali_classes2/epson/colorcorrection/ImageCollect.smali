.class public Lepson/colorcorrection/ImageCollect;
.super Ljava/lang/Object;
.source "ImageCollect.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/colorcorrection/ImageCollect$EPxLog;,
        Lepson/colorcorrection/ImageCollect$SortPoints;,
        Lepson/colorcorrection/ImageCollect$CacheFileFilter;
    }
.end annotation


# static fields
.field private static final BLUR_RATE:F = 0.6f

.field private static final DETECT_AREA_RATIO:F = 0.8f

.field private static final ICPD_FILE_NAME_FORMAT:Ljava/lang/String; = "yyyyMMddkkmm_"

.field private static final ICPD_PREFIX_PRINT_DATA:Ljava/lang/String; = "icpd_"

.field private static final ICPD_PREFIX_SEPARATOR:Ljava/lang/String; = "/"

.field private static final ICPD_USER_FOLDER:Ljava/lang/String; = "/EpsonCameraCapture"

.field public static final IMAGE_TYPE_PREVIEW_BCSC:I = 0x4

.field public static final IMAGE_TYPE_PREVIEW_BCSC_DST:I = 0x6

.field public static final IMAGE_TYPE_PREVIEW_BCSC_MSK_ALL:I = 0x7

.field public static final IMAGE_TYPE_PREVIEW_BCSC_MSK_PRT:I = 0x8

.field public static final IMAGE_TYPE_PREVIEW_BCSC_MSK_PSM:I = 0x9

.field public static final IMAGE_TYPE_PREVIEW_BCSC_SRC:I = 0x5

.field public static final IMAGE_TYPE_PREVIEW_COUNT:I = 0xa

.field public static final IMAGE_TYPE_PREVIEW_CROP:I = 0x1

.field public static final IMAGE_TYPE_PREVIEW_PWC1:I = 0x2

.field public static final IMAGE_TYPE_PREVIEW_PWC2:I = 0x3

.field public static final IMAGE_TYPE_PREVIEW_RAW:I = 0x0

.field public static final IMAGE_TYPE_PRINT_BCSC:I = 0x4

.field public static final IMAGE_TYPE_PRINT_COUNT:I = 0x5

.field public static final IMAGE_TYPE_PRINT_CROP:I = 0x1

.field public static final IMAGE_TYPE_PRINT_PWC1:I = 0x2

.field public static final IMAGE_TYPE_PRINT_PWC2:I = 0x3

.field public static final IMAGE_TYPE_PRINT_RAW:I = 0x0

.field public static final MASK_LINE_WIDTH:F = 20.0f

.field private static final MASK_RADIUS:F = 20.0f

.field public static final PWCTYPE_0:I = 0x0

.field public static final PWCTYPE_1:I = 0x1

.field public static final PWCTYPE_2:I = 0x2

.field public static final PWCTYPE_w:I = -0x1

.field private static final RESOLUTION_THRESHOLD:D = 240.0

.field private static final SAVE_JPEG_QUALITY:I = 0x5a

.field private static final SMALL_PREVIEW_RATE:F = 2.0f


# instance fields
.field private mCacheFolder:Ljava/io/File;

.field private mCacheSaveName:Ljava/lang/String;

.field private mCurrentPaperSize:Lorg/opencv/core/Size;

.field private mDensity:F

.field private mLoadPath:Ljava/lang/String;

.field private mMatPreview:[Lorg/opencv/core/Mat;

.field private mMatPrint:[Lorg/opencv/core/Mat;

.field private mPointEditPts:[Landroid/graphics/PointF;

.field private mRectEditCrop:Landroid/graphics/RectF;

.field private mResolutionThreshold:D

.field private mSaveDisplayPath:Ljava/lang/String;

.field private mSaveFullPath:Ljava/lang/String;

.field private mSmallPreviewRate:F

.field private mUserFolder:Ljava/io/File;

.field private mUserSaveName:Ljava/lang/String;

.field private mUserSelectedFolder:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "opencv_java3"

    .line 87
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;FLorg/opencv/core/Size;)V
    .locals 5

    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 90
    iput-object v0, p0, Lepson/colorcorrection/ImageCollect;->mLoadPath:Ljava/lang/String;

    .line 93
    iput-object v0, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    .line 94
    iput-object v0, p0, Lepson/colorcorrection/ImageCollect;->mMatPrint:[Lorg/opencv/core/Mat;

    const/4 v1, 0x4

    .line 96
    new-array v1, v1, [Landroid/graphics/PointF;

    iput-object v1, p0, Lepson/colorcorrection/ImageCollect;->mPointEditPts:[Landroid/graphics/PointF;

    .line 97
    new-instance v1, Landroid/graphics/RectF;

    const/4 v2, 0x0

    invoke-direct {v1, v2, v2, v2, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v1, p0, Lepson/colorcorrection/ImageCollect;->mRectEditCrop:Landroid/graphics/RectF;

    .line 99
    iput v2, p0, Lepson/colorcorrection/ImageCollect;->mDensity:F

    .line 102
    iput-object v0, p0, Lepson/colorcorrection/ImageCollect;->mCacheFolder:Ljava/io/File;

    .line 103
    iput-object v0, p0, Lepson/colorcorrection/ImageCollect;->mUserFolder:Ljava/io/File;

    .line 104
    iput-object v0, p0, Lepson/colorcorrection/ImageCollect;->mSaveDisplayPath:Ljava/lang/String;

    .line 105
    iput-object v0, p0, Lepson/colorcorrection/ImageCollect;->mSaveFullPath:Ljava/lang/String;

    .line 106
    iput-object v0, p0, Lepson/colorcorrection/ImageCollect;->mCacheSaveName:Ljava/lang/String;

    .line 107
    iput-object v0, p0, Lepson/colorcorrection/ImageCollect;->mUserSaveName:Ljava/lang/String;

    .line 108
    iput-object v0, p0, Lepson/colorcorrection/ImageCollect;->mUserSelectedFolder:Ljava/lang/String;

    const-wide/high16 v3, 0x406e000000000000L    # 240.0

    .line 111
    iput-wide v3, p0, Lepson/colorcorrection/ImageCollect;->mResolutionThreshold:D

    const/high16 v1, 0x40000000    # 2.0f

    .line 114
    iput v1, p0, Lepson/colorcorrection/ImageCollect;->mSmallPreviewRate:F

    .line 117
    iput-object v0, p0, Lepson/colorcorrection/ImageCollect;->mCurrentPaperSize:Lorg/opencv/core/Size;

    .line 120
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lepson/colorcorrection/ImageCollect;->mLoadPath:Ljava/lang/String;

    const/16 p1, 0xa

    .line 121
    new-array v1, p1, [Lorg/opencv/core/Mat;

    iput-object v1, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    const/4 v1, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, p1, :cond_0

    .line 123
    iget-object v4, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    aput-object v0, v4, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x5

    .line 125
    new-array v3, p1, [Lorg/opencv/core/Mat;

    iput-object v3, p0, Lepson/colorcorrection/ImageCollect;->mMatPrint:[Lorg/opencv/core/Mat;

    const/4 v3, 0x0

    :goto_1
    if-ge v3, p1, :cond_1

    .line 127
    iget-object v4, p0, Lepson/colorcorrection/ImageCollect;->mMatPrint:[Lorg/opencv/core/Mat;

    aput-object v0, v4, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 129
    :cond_1
    iput p2, p0, Lepson/colorcorrection/ImageCollect;->mDensity:F

    .line 130
    iget-object p1, p0, Lepson/colorcorrection/ImageCollect;->mPointEditPts:[Landroid/graphics/PointF;

    new-instance p2, Landroid/graphics/PointF;

    invoke-direct {p2, v2, v2}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object p2, p1, v1

    .line 131
    iget-object p1, p0, Lepson/colorcorrection/ImageCollect;->mPointEditPts:[Landroid/graphics/PointF;

    new-instance p2, Landroid/graphics/PointF;

    invoke-direct {p2, v2, v2}, Landroid/graphics/PointF;-><init>(FF)V

    const/4 v0, 0x1

    aput-object p2, p1, v0

    .line 132
    iget-object p1, p0, Lepson/colorcorrection/ImageCollect;->mPointEditPts:[Landroid/graphics/PointF;

    const/4 p2, 0x2

    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, v2, v2}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v0, p1, p2

    .line 133
    iget-object p1, p0, Lepson/colorcorrection/ImageCollect;->mPointEditPts:[Landroid/graphics/PointF;

    const/4 p2, 0x3

    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, v2, v2}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v0, p1, p2

    .line 134
    invoke-virtual {p3}, Lorg/opencv/core/Size;->clone()Lorg/opencv/core/Size;

    move-result-object p1

    iput-object p1, p0, Lepson/colorcorrection/ImageCollect;->mCurrentPaperSize:Lorg/opencv/core/Size;

    return-void
.end method

.method private CalcResizeSize(DDDDZ)Lorg/opencv/core/Size;
    .locals 0

    div-double/2addr p5, p1

    div-double/2addr p7, p3

    if-eqz p9, :cond_1

    cmpg-double p9, p5, p7

    if-gez p9, :cond_0

    goto :goto_0

    :cond_0
    move-wide p5, p7

    goto :goto_0

    :cond_1
    cmpl-double p9, p5, p7

    if-lez p9, :cond_2

    goto :goto_0

    :cond_2
    move-wide p5, p7

    :goto_0
    mul-double p1, p1, p5

    mul-double p3, p3, p5

    .line 1571
    new-instance p5, Lorg/opencv/core/Size;

    invoke-direct {p5, p1, p2, p3, p4}, Lorg/opencv/core/Size;-><init>(DD)V

    return-object p5
.end method

.method private ConvertMatToBitmap(Lorg/opencv/core/Mat;)Landroid/graphics/Bitmap;
    .locals 4

    if-eqz p1, :cond_0

    .line 1578
    invoke-virtual {p1}, Lorg/opencv/core/Mat;->empty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1581
    new-instance v0, Lorg/opencv/core/Mat;

    invoke-virtual {p1}, Lorg/opencv/core/Mat;->rows()I

    move-result v1

    invoke-virtual {p1}, Lorg/opencv/core/Mat;->cols()I

    move-result v2

    invoke-virtual {p1}, Lorg/opencv/core/Mat;->type()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lorg/opencv/core/Mat;-><init>(III)V

    const/4 v1, 0x4

    .line 1582
    invoke-static {p1, v0, v1}, Lorg/opencv/imgproc/Imgproc;->cvtColor(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V

    .line 1585
    invoke-virtual {v0}, Lorg/opencv/core/Mat;->cols()I

    move-result p1

    .line 1586
    invoke-virtual {v0}, Lorg/opencv/core/Mat;->rows()I

    move-result v1

    .line 1587
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object p1

    .line 1590
    invoke-static {v0, p1}, Lorg/opencv/android/Utils;->matToBitmap(Lorg/opencv/core/Mat;Landroid/graphics/Bitmap;)V

    .line 1592
    invoke-virtual {v0}, Lorg/opencv/core/Mat;->release()V

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method private DEBUG_SAVE(Lorg/opencv/core/Mat;Ljava/lang/String;)V
    .locals 5

    if-eqz p1, :cond_0

    .line 1602
    invoke-virtual {p1}, Lorg/opencv/core/Mat;->width()I

    move-result v0

    .line 1603
    invoke-virtual {p1}, Lorg/opencv/core/Mat;->height()I

    move-result v1

    if-lez v0, :cond_0

    if-lez v1, :cond_0

    .line 1605
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    .line 1606
    new-instance v1, Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    const-string v2, "ic_prn_%s.png"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-direct {v1, v0, p2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1607
    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object p2

    .line 1608
    invoke-static {p2, p1}, Lorg/opencv/imgcodecs/Imgcodecs;->imwrite(Ljava/lang/String;Lorg/opencv/core/Mat;)Z

    :cond_0
    return-void
.end method

.method private DeleteCacheFileAll(Ljava/io/File;Ljava/lang/String;)I
    .locals 3

    .line 181
    new-instance v0, Lepson/colorcorrection/ImageCollect$CacheFileFilter;

    invoke-direct {v0, p0, p2}, Lepson/colorcorrection/ImageCollect$CacheFileFilter;-><init>(Lepson/colorcorrection/ImageCollect;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;

    move-result-object p1

    const/4 p2, 0x0

    if-eqz p1, :cond_1

    .line 183
    array-length v0, p1

    .line 184
    array-length v1, p1

    :goto_0
    if-ge p2, v1, :cond_0

    aget-object v2, p1, p2

    .line 185
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_0
    move p2, v0

    :cond_1
    return p2
.end method

.method private DetectEdge(Lorg/opencv/core/Mat;I)Lorg/opencv/core/MatOfPoint2f;
    .locals 8

    .line 1327
    new-instance v6, Lorg/opencv/core/Mat;

    invoke-virtual {p1}, Lorg/opencv/core/Mat;->rows()I

    move-result v0

    invoke-virtual {p1}, Lorg/opencv/core/Mat;->cols()I

    move-result v1

    invoke-virtual {p1}, Lorg/opencv/core/Mat;->type()I

    move-result v2

    invoke-direct {v6, v0, v1, v2}, Lorg/opencv/core/Mat;-><init>(III)V

    int-to-double v4, p2

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    move-object v0, p1

    move-object v1, v6

    .line 1330
    invoke-static/range {v0 .. v5}, Lorg/opencv/imgproc/Imgproc;->Canny(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;DD)V

    .line 1333
    new-instance p1, Lorg/opencv/core/Mat;

    const-wide v0, 0x406fe00000000000L    # 255.0

    invoke-static {v0, v1}, Lorg/opencv/core/Scalar;->all(D)Lorg/opencv/core/Scalar;

    move-result-object p2

    const/4 v7, 0x3

    const/4 v0, 0x0

    invoke-direct {p1, v7, v7, v0, p2}, Lorg/opencv/core/Mat;-><init>(IIILorg/opencv/core/Scalar;)V

    .line 1334
    new-instance v4, Lorg/opencv/core/Point;

    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    invoke-direct {v4, v0, v1, v0, v1}, Lorg/opencv/core/Point;-><init>(DD)V

    const/4 v2, 0x4

    const/4 v5, 0x1

    move-object v0, v6

    move-object v1, v6

    move-object v3, p1

    invoke-static/range {v0 .. v5}, Lorg/opencv/imgproc/Imgproc;->morphologyEx(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;ILorg/opencv/core/Mat;Lorg/opencv/core/Point;I)V

    .line 1337
    new-instance p2, Lorg/opencv/core/Mat;

    invoke-direct {p2}, Lorg/opencv/core/Mat;-><init>()V

    .line 1338
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x1

    .line 1339
    invoke-static {v6, v0, p2, v1, v7}, Lorg/opencv/imgproc/Imgproc;->findContours(Lorg/opencv/core/Mat;Ljava/util/List;Lorg/opencv/core/Mat;II)V

    .line 1342
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 1343
    invoke-direct {p0, v0, p2}, Lepson/colorcorrection/ImageCollect;->DetectPoints(Ljava/util/List;Lorg/opencv/core/Mat;)Lorg/opencv/core/MatOfPoint2f;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 1347
    :goto_0
    invoke-virtual {p2}, Lorg/opencv/core/Mat;->release()V

    .line 1349
    invoke-virtual {p1}, Lorg/opencv/core/Mat;->release()V

    .line 1351
    invoke-virtual {v6}, Lorg/opencv/core/Mat;->release()V

    return-object v0
.end method

.method private DetectPoints(Ljava/util/List;Lorg/opencv/core/Mat;)Lorg/opencv/core/MatOfPoint2f;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/opencv/core/MatOfPoint;",
            ">;",
            "Lorg/opencv/core/Mat;",
            ")",
            "Lorg/opencv/core/MatOfPoint2f;"
        }
    .end annotation

    .line 1359
    invoke-virtual {p2}, Lorg/opencv/core/Mat;->cols()I

    move-result v0

    const/4 v1, 0x4

    filled-new-array {v0, v1}, [I

    move-result-object v0

    const-class v2, I

    invoke-static {v2, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[I

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 1360
    :goto_0
    invoke-virtual {p2}, Lorg/opencv/core/Mat;->cols()I

    move-result v4

    if-ge v3, v4, :cond_0

    .line 1361
    aget-object v4, v0, v3

    invoke-virtual {p2, v2, v3, v4}, Lorg/opencv/core/Mat;->get(II[I)I

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    const-wide/16 v3, 0x0

    move-wide v4, v3

    const/4 p2, 0x0

    const/4 v3, 0x0

    :goto_1
    if-ltz p2, :cond_2

    .line 1369
    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/opencv/core/MatOfPoint;

    .line 1370
    invoke-virtual {v6}, Lorg/opencv/core/MatOfPoint;->rows()I

    move-result v7

    if-lt v7, v1, :cond_1

    .line 1371
    invoke-static {v6}, Lorg/opencv/imgproc/Imgproc;->contourArea(Lorg/opencv/core/Mat;)D

    move-result-wide v6

    cmpg-double v8, v4, v6

    if-gez v8, :cond_1

    move v3, p2

    move-wide v4, v6

    .line 1368
    :cond_1
    aget-object p2, v0, p2

    aget p2, p2, v2

    goto :goto_1

    .line 1380
    :cond_2
    new-instance p2, Lorg/opencv/core/MatOfPoint2f;

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/opencv/core/MatOfPoint;

    invoke-virtual {p1}, Lorg/opencv/core/MatOfPoint;->toArray()[Lorg/opencv/core/Point;

    move-result-object p1

    invoke-direct {p2, p1}, Lorg/opencv/core/MatOfPoint2f;-><init>([Lorg/opencv/core/Point;)V

    .line 1381
    new-instance p1, Lorg/opencv/core/MatOfPoint2f;

    invoke-direct {p1}, Lorg/opencv/core/MatOfPoint2f;-><init>()V

    const/16 v0, 0x190

    :goto_2
    const/16 v2, 0x32

    if-lt v0, v2, :cond_4

    int-to-double v2, v0

    const/4 v4, 0x1

    .line 1383
    invoke-static {p2, p1, v2, v3, v4}, Lorg/opencv/imgproc/Imgproc;->approxPolyDP(Lorg/opencv/core/MatOfPoint2f;Lorg/opencv/core/MatOfPoint2f;DZ)V

    .line 1384
    invoke-virtual {p1}, Lorg/opencv/core/MatOfPoint2f;->rows()I

    move-result v2

    if-lt v2, v1, :cond_3

    goto :goto_3

    :cond_3
    add-int/lit8 v0, v0, -0x32

    goto :goto_2

    :cond_4
    :goto_3
    return-object p1
.end method

.method private ExecPwc(III)V
    .locals 2

    .line 477
    new-instance v0, Lepson/colorcorrection/SDIC_MAT_PTR;

    invoke-direct {v0}, Lepson/colorcorrection/SDIC_MAT_PTR;-><init>()V

    const/4 v1, 0x1

    if-ne p3, v1, :cond_0

    .line 480
    iget-object p3, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    aget-object p1, p3, p1

    invoke-static {p1, v0}, Lepson/colorcorrection/SDIC_PWC;->PaperWhiteCorrection(Lorg/opencv/core/Mat;Lepson/colorcorrection/SDIC_MAT_PTR;)I

    move-result p1

    goto :goto_0

    .line 482
    :cond_0
    iget-object p3, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    aget-object p1, p3, p1

    invoke-static {p1, v0}, Lepson/colorcorrection/SDIC_PWC;->PaperWhiteCorrection2(Lorg/opencv/core/Mat;Lepson/colorcorrection/SDIC_MAT_PTR;)I

    move-result p1

    :goto_0
    if-nez p1, :cond_2

    .line 486
    iget-object p1, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    aget-object p3, p1, p2

    if-eqz p3, :cond_1

    .line 487
    aget-object p1, p1, p2

    invoke-virtual {p1}, Lorg/opencv/core/Mat;->release()V

    .line 488
    iget-object p1, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    const/4 p3, 0x0

    aput-object p3, p1, p2

    .line 490
    :cond_1
    iget-object p1, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    invoke-virtual {v0}, Lepson/colorcorrection/SDIC_MAT_PTR;->getMat()Lorg/opencv/core/Mat;

    move-result-object p3

    aput-object p3, p1, p2

    :cond_2
    return-void
.end method

.method private GetCropBoundRect([Landroid/graphics/PointF;)Landroid/graphics/RectF;
    .locals 7

    if-eqz p1, :cond_0

    .line 521
    array-length v0, p1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x3

    .line 538
    aget-object v1, p1, v0

    iget v1, v1, Landroid/graphics/PointF;->x:F

    const/4 v2, 0x0

    aget-object v3, p1, v2

    iget v3, v3, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v3

    const/4 v3, 0x2

    aget-object v4, p1, v3

    iget v4, v4, Landroid/graphics/PointF;->x:F

    const/4 v5, 0x1

    aget-object v6, p1, v5

    iget v6, v6, Landroid/graphics/PointF;->x:F

    sub-float/2addr v4, v6

    add-float/2addr v1, v4

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v1, v4

    .line 539
    aget-object v5, p1, v5

    iget v5, v5, Landroid/graphics/PointF;->y:F

    aget-object v2, p1, v2

    iget v2, v2, Landroid/graphics/PointF;->y:F

    sub-float/2addr v5, v2

    aget-object v2, p1, v3

    iget v2, v2, Landroid/graphics/PointF;->y:F

    aget-object p1, p1, v0

    iget p1, p1, Landroid/graphics/PointF;->y:F

    sub-float/2addr v2, p1

    add-float/2addr v5, v2

    div-float/2addr v5, v4

    .line 540
    new-instance p1, Landroid/graphics/RectF;

    const/4 v0, 0x0

    invoke-direct {p1, v0, v0, v1, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method private GetCropImage(Lorg/opencv/core/Mat;[Landroid/graphics/PointF;Landroid/graphics/RectF;Lorg/opencv/core/Size;Lorg/opencv/core/Size;)Lorg/opencv/core/Mat;
    .locals 25

    move-object/from16 v10, p0

    move-object/from16 v0, p2

    move-object/from16 v11, p4

    move-object/from16 v1, p5

    if-eqz v0, :cond_e

    .line 552
    array-length v2, v0

    const/4 v12, 0x4

    if-ne v2, v12, :cond_e

    .line 555
    new-instance v13, Lorg/opencv/core/Mat;

    const/4 v14, 0x5

    const/4 v15, 0x2

    invoke-direct {v13, v12, v15, v14}, Lorg/opencv/core/Mat;-><init>(III)V

    .line 556
    new-instance v9, Lorg/opencv/core/Mat;

    invoke-direct {v9, v12, v15, v14}, Lorg/opencv/core/Mat;-><init>(III)V

    const/16 v2, 0x8

    .line 557
    new-array v3, v2, [F

    .line 558
    new-array v7, v2, [F

    const-wide/16 v4, 0x0

    const/16 v16, 0x1

    const/4 v8, 0x0

    if-eqz v11, :cond_4

    move-object/from16 v18, v13

    .line 570
    iget-wide v12, v11, Lorg/opencv/core/Size;->width:D

    cmpl-double v2, v12, v4

    if-lez v2, :cond_5

    iget-wide v12, v11, Lorg/opencv/core/Size;->height:D

    cmpl-double v2, v12, v4

    if-lez v2, :cond_5

    .line 572
    iget-wide v12, v11, Lorg/opencv/core/Size;->width:D

    iget-wide v4, v11, Lorg/opencv/core/Size;->height:D

    cmpl-double v2, v12, v4

    if-lez v2, :cond_1

    .line 574
    invoke-virtual/range {p3 .. p3}, Landroid/graphics/RectF;->width()F

    move-result v2

    invoke-virtual/range {p3 .. p3}, Landroid/graphics/RectF;->height()F

    move-result v4

    cmpg-float v2, v2, v4

    if-gez v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 581
    :cond_1
    iget-wide v4, v11, Lorg/opencv/core/Size;->width:D

    iget-wide v12, v11, Lorg/opencv/core/Size;->height:D

    cmpg-double v2, v4, v12

    if-gez v2, :cond_3

    .line 583
    invoke-virtual/range {p3 .. p3}, Landroid/graphics/RectF;->width()F

    move-result v2

    invoke-virtual/range {p3 .. p3}, Landroid/graphics/RectF;->height()F

    move-result v4

    cmpg-float v2, v2, v4

    if-gez v2, :cond_2

    const/4 v2, 0x0

    goto :goto_0

    :cond_2
    const/4 v2, 0x1

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    goto :goto_0

    :cond_4
    move-object/from16 v18, v13

    :cond_5
    const/4 v2, 0x0

    :goto_0
    const/4 v12, 0x7

    const/4 v13, 0x6

    const/16 v20, 0x3

    if-nez v2, :cond_6

    .line 603
    aget-object v2, v0, v8

    iget v2, v2, Landroid/graphics/PointF;->x:F

    aput v2, v3, v8

    .line 604
    aget-object v2, v0, v8

    iget v2, v2, Landroid/graphics/PointF;->y:F

    aput v2, v3, v16

    .line 605
    aget-object v2, v0, v16

    iget v2, v2, Landroid/graphics/PointF;->x:F

    aput v2, v3, v15

    .line 606
    aget-object v2, v0, v16

    iget v2, v2, Landroid/graphics/PointF;->y:F

    aput v2, v3, v20

    .line 607
    aget-object v2, v0, v15

    iget v2, v2, Landroid/graphics/PointF;->x:F

    const/4 v4, 0x4

    aput v2, v3, v4

    .line 608
    aget-object v2, v0, v15

    iget v2, v2, Landroid/graphics/PointF;->y:F

    aput v2, v3, v14

    .line 609
    aget-object v2, v0, v20

    iget v2, v2, Landroid/graphics/PointF;->x:F

    aput v2, v3, v13

    .line 610
    aget-object v0, v0, v20

    iget v0, v0, Landroid/graphics/PointF;->y:F

    aput v0, v3, v12

    move-object/from16 v5, v18

    goto :goto_1

    .line 613
    :cond_6
    aget-object v2, v0, v16

    iget v2, v2, Landroid/graphics/PointF;->x:F

    aput v2, v3, v8

    .line 614
    aget-object v2, v0, v16

    iget v2, v2, Landroid/graphics/PointF;->y:F

    aput v2, v3, v16

    .line 615
    aget-object v2, v0, v15

    iget v2, v2, Landroid/graphics/PointF;->x:F

    aput v2, v3, v15

    .line 616
    aget-object v2, v0, v15

    iget v2, v2, Landroid/graphics/PointF;->y:F

    aput v2, v3, v20

    .line 617
    aget-object v2, v0, v20

    iget v2, v2, Landroid/graphics/PointF;->x:F

    const/4 v4, 0x4

    aput v2, v3, v4

    .line 618
    aget-object v2, v0, v20

    iget v2, v2, Landroid/graphics/PointF;->y:F

    aput v2, v3, v14

    .line 619
    aget-object v2, v0, v8

    iget v2, v2, Landroid/graphics/PointF;->x:F

    aput v2, v3, v13

    .line 620
    aget-object v0, v0, v8

    iget v0, v0, Landroid/graphics/PointF;->y:F

    aput v0, v3, v12

    move-object/from16 v5, v18

    .line 622
    :goto_1
    invoke-virtual {v5, v8, v8, v3}, Lorg/opencv/core/Mat;->put(II[F)I

    if-eqz v1, :cond_8

    if-eqz v11, :cond_7

    .line 627
    iget-wide v2, v11, Lorg/opencv/core/Size;->width:D

    const-wide/16 v18, 0x0

    cmpl-double v0, v2, v18

    if-lez v0, :cond_7

    iget-wide v2, v11, Lorg/opencv/core/Size;->height:D

    cmpl-double v0, v2, v18

    if-lez v0, :cond_7

    .line 628
    iget-wide v2, v11, Lorg/opencv/core/Size;->width:D

    move-object/from16 v18, v7

    iget-wide v6, v11, Lorg/opencv/core/Size;->height:D

    move-object/from16 v19, v9

    iget-wide v8, v1, Lorg/opencv/core/Size;->width:D

    iget-wide v0, v1, Lorg/opencv/core/Size;->height:D

    const/4 v11, 0x1

    move-wide/from16 v21, v0

    move-object/from16 v0, p0

    move-wide v1, v2

    move-wide v3, v6

    move-object v7, v5

    const/4 v12, 0x0

    move-wide v5, v8

    move-object v9, v7

    move-object/from16 v13, v18

    const/4 v14, 0x0

    move-wide/from16 v7, v21

    move-object/from16 v23, v9

    move-object/from16 v24, v19

    move v9, v11

    invoke-direct/range {v0 .. v9}, Lepson/colorcorrection/ImageCollect;->CalcResizeSize(DDDDZ)Lorg/opencv/core/Size;

    move-result-object v0

    .line 629
    new-instance v1, Landroid/graphics/RectF;

    iget-wide v2, v0, Lorg/opencv/core/Size;->width:D

    double-to-float v2, v2

    iget-wide v3, v0, Lorg/opencv/core/Size;->height:D

    double-to-float v0, v3

    invoke-direct {v1, v12, v12, v2, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    goto/16 :goto_5

    :cond_7
    move-object/from16 v23, v5

    move-object v13, v7

    move-object/from16 v24, v9

    const/4 v12, 0x0

    const/4 v14, 0x0

    goto/16 :goto_4

    :cond_8
    move-object/from16 v23, v5

    move-object v13, v7

    move-object/from16 v24, v9

    const/4 v12, 0x0

    const/4 v14, 0x0

    .line 632
    invoke-virtual/range {p3 .. p3}, Landroid/graphics/RectF;->width()F

    move-result v0

    float-to-double v5, v0

    .line 633
    invoke-virtual/range {p3 .. p3}, Landroid/graphics/RectF;->height()F

    move-result v0

    float-to-double v7, v0

    const-wide v21, 0x4076800000000000L    # 360.0

    if-eqz v11, :cond_b

    .line 634
    iget-wide v0, v11, Lorg/opencv/core/Size;->width:D

    const-wide/16 v2, 0x0

    cmpl-double v4, v0, v2

    if-lez v4, :cond_b

    iget-wide v0, v11, Lorg/opencv/core/Size;->height:D

    cmpl-double v4, v0, v2

    if-lez v4, :cond_b

    .line 642
    iget-wide v1, v11, Lorg/opencv/core/Size;->width:D

    iget-wide v3, v11, Lorg/opencv/core/Size;->height:D

    const/4 v9, 0x0

    move-object/from16 v0, p0

    invoke-direct/range {v0 .. v9}, Lepson/colorcorrection/ImageCollect;->CalcResizeSize(DDDDZ)Lorg/opencv/core/Size;

    move-result-object v0

    .line 645
    iget-wide v1, v11, Lorg/opencv/core/Size;->width:D

    div-double v1, v1, v21

    .line 646
    iget-wide v3, v11, Lorg/opencv/core/Size;->height:D

    div-double v3, v3, v21

    .line 647
    iget-wide v5, v0, Lorg/opencv/core/Size;->width:D

    div-double/2addr v5, v1

    .line 648
    iget-wide v7, v0, Lorg/opencv/core/Size;->height:D

    div-double/2addr v7, v3

    cmpl-double v9, v5, v7

    if-lez v9, :cond_9

    goto :goto_2

    :cond_9
    move-wide v5, v7

    .line 650
    :goto_2
    iget-wide v7, v10, Lepson/colorcorrection/ImageCollect;->mResolutionThreshold:D

    cmpl-double v9, v5, v7

    if-lez v9, :cond_a

    mul-double v5, v1, v7

    mul-double v7, v7, v3

    .line 653
    iget-wide v1, v11, Lorg/opencv/core/Size;->width:D

    iget-wide v3, v11, Lorg/opencv/core/Size;->height:D

    const/4 v9, 0x0

    move-object/from16 v0, p0

    invoke-direct/range {v0 .. v9}, Lepson/colorcorrection/ImageCollect;->CalcResizeSize(DDDDZ)Lorg/opencv/core/Size;

    move-result-object v0

    .line 656
    :cond_a
    new-instance v1, Landroid/graphics/RectF;

    iget-wide v2, v0, Lorg/opencv/core/Size;->width:D

    double-to-float v2, v2

    iget-wide v3, v0, Lorg/opencv/core/Size;->height:D

    double-to-float v0, v3

    invoke-direct {v1, v12, v12, v2, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    goto :goto_5

    .line 661
    :cond_b
    iget-object v0, v10, Lepson/colorcorrection/ImageCollect;->mCurrentPaperSize:Lorg/opencv/core/Size;

    iget-wide v0, v0, Lorg/opencv/core/Size;->width:D

    div-double v0, v0, v21

    .line 662
    iget-object v2, v10, Lepson/colorcorrection/ImageCollect;->mCurrentPaperSize:Lorg/opencv/core/Size;

    iget-wide v2, v2, Lorg/opencv/core/Size;->height:D

    div-double v2, v2, v21

    div-double v0, v5, v0

    div-double v2, v7, v2

    cmpl-double v4, v0, v2

    if-lez v4, :cond_c

    goto :goto_3

    :cond_c
    move-wide v0, v2

    .line 666
    :goto_3
    iget-wide v2, v10, Lepson/colorcorrection/ImageCollect;->mResolutionThreshold:D

    cmpl-double v4, v0, v2

    if-lez v4, :cond_d

    div-double/2addr v2, v0

    mul-double v5, v5, v2

    mul-double v7, v7, v2

    .line 670
    new-instance v0, Landroid/graphics/RectF;

    double-to-float v1, v5

    double-to-float v2, v7

    invoke-direct {v0, v12, v12, v1, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    move-object v1, v0

    goto :goto_5

    :cond_d
    :goto_4
    move-object/from16 v1, p3

    :goto_5
    aput v12, v13, v14

    aput v12, v13, v16

    aput v12, v13, v15

    .line 679
    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v0

    aput v0, v13, v20

    .line 680
    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v0

    const/4 v2, 0x4

    aput v0, v13, v2

    .line 681
    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v0

    const/4 v2, 0x5

    aput v0, v13, v2

    .line 682
    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v0

    const/4 v2, 0x6

    aput v0, v13, v2

    const/4 v0, 0x7

    aput v12, v13, v0

    move-object/from16 v0, v24

    .line 684
    invoke-virtual {v0, v14, v14, v13}, Lorg/opencv/core/Mat;->put(II[F)I

    move-object/from16 v2, v23

    .line 687
    invoke-static {v2, v0}, Lorg/opencv/imgproc/Imgproc;->getPerspectiveTransform(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)Lorg/opencv/core/Mat;

    move-result-object v11

    .line 690
    new-instance v12, Lorg/opencv/core/Mat;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual/range {p1 .. p1}, Lorg/opencv/core/Mat;->type()I

    move-result v4

    invoke-direct {v12, v3, v1, v4}, Lorg/opencv/core/Mat;-><init>(III)V

    .line 692
    invoke-virtual {v12}, Lorg/opencv/core/Mat;->size()Lorg/opencv/core/Size;

    move-result-object v6

    const/4 v7, 0x1

    const/4 v8, 0x0

    new-instance v1, Lorg/opencv/core/Scalar;

    const-wide v14, 0x406fe00000000000L    # 255.0

    const-wide v16, 0x406fe00000000000L    # 255.0

    const-wide v18, 0x406fe00000000000L    # 255.0

    move-object v13, v1

    invoke-direct/range {v13 .. v19}, Lorg/opencv/core/Scalar;-><init>(DDD)V

    move-object/from16 v3, p1

    move-object v4, v12

    move-object v5, v11

    move-object v9, v1

    invoke-static/range {v3 .. v9}, Lorg/opencv/imgproc/Imgproc;->warpPerspective(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Size;IILorg/opencv/core/Scalar;)V

    .line 706
    invoke-virtual {v2}, Lorg/opencv/core/Mat;->release()V

    .line 708
    invoke-virtual {v0}, Lorg/opencv/core/Mat;->release()V

    .line 710
    invoke-virtual {v11}, Lorg/opencv/core/Mat;->release()V

    goto :goto_6

    :cond_e
    const/4 v12, 0x0

    :goto_6
    return-object v12
.end method

.method private GetCropPointsAdjustment([Landroid/graphics/PointF;F)[Landroid/graphics/PointF;
    .locals 5

    if-eqz p1, :cond_0

    .line 503
    array-length v0, p1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 506
    new-array v0, v1, [Landroid/graphics/PointF;

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    .line 508
    new-instance v3, Landroid/graphics/PointF;

    invoke-direct {v3}, Landroid/graphics/PointF;-><init>()V

    aput-object v3, v0, v2

    .line 509
    aget-object v3, v0, v2

    aget-object v4, p1, v2

    iget v4, v4, Landroid/graphics/PointF;->x:F

    mul-float v4, v4, p2

    iput v4, v3, Landroid/graphics/PointF;->x:F

    .line 510
    aget-object v3, v0, v2

    aget-object v4, p1, v2

    iget v4, v4, Landroid/graphics/PointF;->y:F

    mul-float v4, v4, p2

    iput v4, v3, Landroid/graphics/PointF;->y:F

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :cond_1
    return-object v0
.end method

.method private GetMat()Lorg/opencv/core/Mat;
    .locals 15

    .line 1408
    iget-object v0, p0, Lepson/colorcorrection/ImageCollect;->mMatPrint:[Lorg/opencv/core/Mat;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    if-nez v0, :cond_8

    .line 1409
    iget-object v0, p0, Lepson/colorcorrection/ImageCollect;->mLoadPath:Ljava/lang/String;

    const/4 v2, -0x1

    invoke-static {v0, v2}, Lorg/opencv/imgcodecs/Imgcodecs;->imread(Ljava/lang/String;I)Lorg/opencv/core/Mat;

    move-result-object v0

    .line 1410
    invoke-virtual {v0}, Lorg/opencv/core/Mat;->channels()I

    move-result v3

    const/4 v4, 0x4

    const/16 v5, 0x8

    const/4 v6, 0x1

    const/4 v7, 0x3

    if-ne v3, v4, :cond_1

    .line 1415
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1416
    invoke-static {v0, v3}, Lorg/opencv/core/Core;->split(Lorg/opencv/core/Mat;Ljava/util/List;)V

    .line 1417
    invoke-virtual {v0}, Lorg/opencv/core/Mat;->release()V

    .line 1427
    new-instance v0, Lorg/opencv/core/Mat;

    invoke-direct {v0}, Lorg/opencv/core/Mat;-><init>()V

    .line 1428
    invoke-interface {v3, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/opencv/core/Mat;

    sget v8, Lorg/opencv/core/CvType;->CV_32FC1:I

    invoke-virtual {v4, v0, v8}, Lorg/opencv/core/Mat;->convertTo(Lorg/opencv/core/Mat;I)V

    .line 1430
    invoke-interface {v3, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/opencv/core/Mat;

    invoke-virtual {v4}, Lorg/opencv/core/Mat;->release()V

    .line 1431
    invoke-interface {v3, v7}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    const-wide/16 v10, 0x0

    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    const/16 v14, 0x20

    move-object v8, v0

    move-object v9, v0

    .line 1433
    invoke-static/range {v8 .. v14}, Lorg/opencv/core/Core;->normalize(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;DDI)V

    .line 1435
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    const/4 v8, 0x0

    :goto_0
    if-ge v8, v7, :cond_0

    .line 1436
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 1438
    :cond_0
    new-instance v8, Lorg/opencv/core/Mat;

    invoke-direct {v8}, Lorg/opencv/core/Mat;-><init>()V

    .line 1439
    invoke-static {v4, v8}, Lorg/opencv/core/Core;->merge(Ljava/util/List;Lorg/opencv/core/Mat;)V

    .line 1441
    invoke-virtual {v0}, Lorg/opencv/core/Mat;->release()V

    .line 1451
    new-instance v0, Lorg/opencv/core/Mat;

    invoke-direct {v0}, Lorg/opencv/core/Mat;-><init>()V

    .line 1452
    new-instance v4, Lorg/opencv/core/Mat;

    invoke-direct {v4}, Lorg/opencv/core/Mat;-><init>()V

    .line 1454
    invoke-static {v3, v0}, Lorg/opencv/core/Core;->merge(Ljava/util/List;Lorg/opencv/core/Mat;)V

    .line 1456
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/opencv/core/Mat;

    invoke-virtual {v9}, Lorg/opencv/core/Mat;->release()V

    .line 1457
    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/opencv/core/Mat;

    invoke-virtual {v9}, Lorg/opencv/core/Mat;->release()V

    const/4 v9, 0x2

    .line 1458
    invoke-interface {v3, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/opencv/core/Mat;

    invoke-virtual {v3}, Lorg/opencv/core/Mat;->release()V

    .line 1461
    sget v3, Lorg/opencv/core/CvType;->CV_32FC1:I

    invoke-virtual {v0, v4, v3}, Lorg/opencv/core/Mat;->convertTo(Lorg/opencv/core/Mat;I)V

    .line 1462
    invoke-virtual {v0}, Lorg/opencv/core/Mat;->release()V

    .line 1467
    invoke-virtual {v4, v8}, Lorg/opencv/core/Mat;->mul(Lorg/opencv/core/Mat;)Lorg/opencv/core/Mat;

    move-result-object v0

    .line 1468
    invoke-virtual {v4}, Lorg/opencv/core/Mat;->release()V

    .line 1473
    iget-object v3, p0, Lepson/colorcorrection/ImageCollect;->mMatPrint:[Lorg/opencv/core/Mat;

    new-instance v4, Lorg/opencv/core/Mat;

    invoke-direct {v4}, Lorg/opencv/core/Mat;-><init>()V

    aput-object v4, v3, v1

    .line 1475
    iget-object v3, p0, Lepson/colorcorrection/ImageCollect;->mMatPrint:[Lorg/opencv/core/Mat;

    aget-object v3, v3, v1

    sget v4, Lorg/opencv/core/CvType;->CV_8UC3:I

    invoke-virtual {v0, v3, v4}, Lorg/opencv/core/Mat;->convertTo(Lorg/opencv/core/Mat;I)V

    .line 1476
    invoke-virtual {v0}, Lorg/opencv/core/Mat;->release()V

    goto :goto_1

    :cond_1
    if-ne v3, v6, :cond_2

    .line 1481
    new-instance v3, Lorg/opencv/core/Mat;

    invoke-virtual {v0}, Lorg/opencv/core/Mat;->rows()I

    move-result v4

    invoke-virtual {v0}, Lorg/opencv/core/Mat;->cols()I

    move-result v8

    sget v9, Lorg/opencv/core/CvType;->CV_8UC3:I

    invoke-direct {v3, v4, v8, v9}, Lorg/opencv/core/Mat;-><init>(III)V

    .line 1482
    invoke-static {v0, v3, v5}, Lorg/opencv/imgproc/Imgproc;->cvtColor(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V

    .line 1483
    invoke-virtual {v0}, Lorg/opencv/core/Mat;->release()V

    .line 1485
    iget-object v0, p0, Lepson/colorcorrection/ImageCollect;->mMatPrint:[Lorg/opencv/core/Mat;

    aput-object v3, v0, v1

    goto :goto_1

    .line 1487
    :cond_2
    iget-object v3, p0, Lepson/colorcorrection/ImageCollect;->mMatPrint:[Lorg/opencv/core/Mat;

    aput-object v0, v3, v1

    .line 1494
    :goto_1
    iget-object v0, p0, Lepson/colorcorrection/ImageCollect;->mMatPrint:[Lorg/opencv/core/Mat;

    aget-object v0, v0, v1

    if-eqz v0, :cond_8

    const/4 v0, 0x0

    .line 1498
    invoke-static {}, Lepson/print/Util/OpenCvHelper;->doseImreadRotateWithExifTag()Z

    move-result v3

    if-nez v3, :cond_5

    .line 1500
    :try_start_0
    new-instance v3, Landroid/media/ExifInterface;

    iget-object v4, p0, Lepson/colorcorrection/ImageCollect;->mLoadPath:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    const-string v4, "Orientation"

    .line 1501
    invoke-virtual {v3, v4, v1}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v3
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v4, 0x6

    if-ne v3, v4, :cond_3

    const/high16 v0, 0x42b40000    # 90.0f

    goto :goto_2

    :cond_3
    if-ne v3, v7, :cond_4

    const/high16 v0, 0x43340000    # 180.0f

    goto :goto_2

    :cond_4
    if-ne v3, v5, :cond_5

    const/high16 v0, 0x43870000    # 270.0f

    goto :goto_2

    :catch_0
    move-exception v3

    .line 1510
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    :cond_5
    :goto_2
    float-to-double v3, v0

    const-wide v7, 0x4056800000000000L    # 90.0

    const/4 v0, 0x0

    cmpl-double v5, v3, v7

    if-nez v5, :cond_6

    .line 1516
    new-instance v2, Lorg/opencv/core/Size;

    iget-object v3, p0, Lepson/colorcorrection/ImageCollect;->mMatPrint:[Lorg/opencv/core/Mat;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Lorg/opencv/core/Mat;->rows()I

    move-result v3

    int-to-double v3, v3

    iget-object v5, p0, Lepson/colorcorrection/ImageCollect;->mMatPrint:[Lorg/opencv/core/Mat;

    aget-object v5, v5, v1

    invoke-virtual {v5}, Lorg/opencv/core/Mat;->cols()I

    move-result v5

    int-to-double v7, v5

    invoke-direct {v2, v3, v4, v7, v8}, Lorg/opencv/core/Size;-><init>(DD)V

    .line 1518
    new-instance v3, Lorg/opencv/core/Mat;

    sget v4, Lorg/opencv/core/CvType;->CV_8UC3:I

    invoke-direct {v3, v2, v4}, Lorg/opencv/core/Mat;-><init>(Lorg/opencv/core/Size;I)V

    .line 1519
    iget-object v4, p0, Lepson/colorcorrection/ImageCollect;->mMatPrint:[Lorg/opencv/core/Mat;

    aget-object v4, v4, v1

    invoke-static {v4, v3}, Lorg/opencv/core/Core;->transpose(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V

    .line 1520
    iget-object v4, p0, Lepson/colorcorrection/ImageCollect;->mMatPrint:[Lorg/opencv/core/Mat;

    aget-object v4, v4, v1

    invoke-virtual {v4}, Lorg/opencv/core/Mat;->release()V

    .line 1521
    iget-object v4, p0, Lepson/colorcorrection/ImageCollect;->mMatPrint:[Lorg/opencv/core/Mat;

    aput-object v0, v4, v1

    .line 1523
    new-instance v0, Lorg/opencv/core/Mat;

    sget v5, Lorg/opencv/core/CvType;->CV_8UC3:I

    invoke-direct {v0, v2, v5}, Lorg/opencv/core/Mat;-><init>(Lorg/opencv/core/Size;I)V

    aput-object v0, v4, v1

    .line 1524
    iget-object v0, p0, Lepson/colorcorrection/ImageCollect;->mMatPrint:[Lorg/opencv/core/Mat;

    aget-object v0, v0, v1

    invoke-static {v3, v0, v6}, Lorg/opencv/core/Core;->flip(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V

    .line 1525
    invoke-virtual {v3}, Lorg/opencv/core/Mat;->release()V

    goto :goto_3

    :cond_6
    const-wide v5, 0x4066800000000000L    # 180.0

    cmpl-double v7, v3, v5

    if-nez v7, :cond_7

    .line 1530
    iget-object v0, p0, Lepson/colorcorrection/ImageCollect;->mMatPrint:[Lorg/opencv/core/Mat;

    aget-object v3, v0, v1

    aget-object v0, v0, v1

    invoke-static {v3, v0, v2}, Lorg/opencv/core/Core;->flip(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V

    goto :goto_3

    :cond_7
    const-wide v5, 0x4070e00000000000L    # 270.0

    cmpl-double v2, v3, v5

    if-nez v2, :cond_8

    .line 1533
    new-instance v2, Lorg/opencv/core/Size;

    iget-object v3, p0, Lepson/colorcorrection/ImageCollect;->mMatPrint:[Lorg/opencv/core/Mat;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Lorg/opencv/core/Mat;->rows()I

    move-result v3

    int-to-double v3, v3

    iget-object v5, p0, Lepson/colorcorrection/ImageCollect;->mMatPrint:[Lorg/opencv/core/Mat;

    aget-object v5, v5, v1

    invoke-virtual {v5}, Lorg/opencv/core/Mat;->cols()I

    move-result v5

    int-to-double v5, v5

    invoke-direct {v2, v3, v4, v5, v6}, Lorg/opencv/core/Size;-><init>(DD)V

    .line 1535
    new-instance v3, Lorg/opencv/core/Mat;

    sget v4, Lorg/opencv/core/CvType;->CV_8UC3:I

    invoke-direct {v3, v2, v4}, Lorg/opencv/core/Mat;-><init>(Lorg/opencv/core/Size;I)V

    .line 1536
    iget-object v4, p0, Lepson/colorcorrection/ImageCollect;->mMatPrint:[Lorg/opencv/core/Mat;

    aget-object v4, v4, v1

    invoke-static {v4, v3}, Lorg/opencv/core/Core;->transpose(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V

    .line 1537
    iget-object v4, p0, Lepson/colorcorrection/ImageCollect;->mMatPrint:[Lorg/opencv/core/Mat;

    aget-object v4, v4, v1

    invoke-virtual {v4}, Lorg/opencv/core/Mat;->release()V

    .line 1538
    iget-object v4, p0, Lepson/colorcorrection/ImageCollect;->mMatPrint:[Lorg/opencv/core/Mat;

    aput-object v0, v4, v1

    .line 1540
    new-instance v0, Lorg/opencv/core/Mat;

    sget v5, Lorg/opencv/core/CvType;->CV_8UC3:I

    invoke-direct {v0, v2, v5}, Lorg/opencv/core/Mat;-><init>(Lorg/opencv/core/Size;I)V

    aput-object v0, v4, v1

    .line 1541
    iget-object v0, p0, Lepson/colorcorrection/ImageCollect;->mMatPrint:[Lorg/opencv/core/Mat;

    aget-object v0, v0, v1

    invoke-static {v3, v0, v1}, Lorg/opencv/core/Core;->flip(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V

    .line 1542
    invoke-virtual {v3}, Lorg/opencv/core/Mat;->release()V

    .line 1547
    :cond_8
    :goto_3
    iget-object v0, p0, Lepson/colorcorrection/ImageCollect;->mMatPrint:[Lorg/opencv/core/Mat;

    aget-object v0, v0, v1

    return-object v0
.end method

.method private static LogMemory(Ljava/lang/String;)V
    .locals 9

    .line 1652
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    .line 1653
    invoke-virtual {v0}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v1

    long-to-float v1, v1

    .line 1654
    invoke-virtual {v0}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v2

    long-to-float v2, v2

    .line 1655
    invoke-virtual {v0}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v3

    long-to-float v0, v3

    sub-float v3, v2, v0

    const-string v4, "LogMemory"

    const-string v5, "max:%.2fMB total:%.2fMB free:%.2fMB used:%.2fMB %s"

    const/4 v6, 0x5

    .line 1657
    new-array v6, v6, [Ljava/lang/Object;

    const/high16 v7, 0x49800000    # 1048576.0f

    div-float/2addr v1, v7

    .line 1659
    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    const/4 v8, 0x0

    aput-object v1, v6, v8

    div-float/2addr v2, v7

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v6, v2

    div-float/2addr v0, v7

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    const/4 v1, 0x2

    aput-object v0, v6, v1

    div-float/2addr v3, v7

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    const/4 v1, 0x3

    aput-object v0, v6, v1

    const/4 v0, 0x4

    aput-object p0, v6, v0

    .line 1658
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    .line 1657
    invoke-static {v4, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private MakeCropImage(Lorg/opencv/core/Mat;[Landroid/graphics/PointF;FLorg/opencv/core/Size;Lorg/opencv/core/Size;)Lorg/opencv/core/Mat;
    .locals 6

    .line 495
    invoke-direct {p0, p2, p3}, Lepson/colorcorrection/ImageCollect;->GetCropPointsAdjustment([Landroid/graphics/PointF;F)[Landroid/graphics/PointF;

    move-result-object v2

    .line 496
    invoke-direct {p0, v2}, Lepson/colorcorrection/ImageCollect;->GetCropBoundRect([Landroid/graphics/PointF;)Landroid/graphics/RectF;

    move-result-object v3

    move-object v0, p0

    move-object v1, p1

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lepson/colorcorrection/ImageCollect;->GetCropImage(Lorg/opencv/core/Mat;[Landroid/graphics/PointF;Landroid/graphics/RectF;Lorg/opencv/core/Size;Lorg/opencv/core/Size;)Lorg/opencv/core/Mat;

    move-result-object p1

    return-object p1
.end method

.method private MakeImageBCSC(Lorg/opencv/core/Mat;Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;)Lorg/opencv/core/Mat;
    .locals 24

    move-object/from16 v0, p2

    if-eqz v0, :cond_8

    .line 838
    invoke-virtual/range {p2 .. p2}, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;->Size()I

    move-result v1

    if-lez v1, :cond_8

    const/4 v2, 0x0

    move-object/from16 v13, p1

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 840
    :goto_0
    invoke-virtual/range {p2 .. p2}, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;->Size()I

    move-result v5

    if-ge v3, v5, :cond_9

    .line 843
    invoke-virtual {v0, v3}, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;->GetParam(I)Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;

    move-result-object v5

    .line 845
    invoke-virtual {v5}, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->GetMaskType()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 849
    new-instance v4, Lorg/opencv/core/Mat;

    invoke-virtual {v13}, Lorg/opencv/core/Mat;->rows()I

    move-result v6

    invoke-virtual {v13}, Lorg/opencv/core/Mat;->cols()I

    move-result v7

    invoke-direct {v4, v6, v7, v2}, Lorg/opencv/core/Mat;-><init>(III)V

    const-wide v6, 0x406fe00000000000L    # 255.0

    .line 850
    invoke-static {v6, v7}, Lorg/opencv/core/Scalar;->all(D)Lorg/opencv/core/Scalar;

    move-result-object v6

    invoke-virtual {v4, v6}, Lorg/opencv/core/Mat;->setTo(Lorg/opencv/core/Scalar;)Lorg/opencv/core/Mat;

    move-object v1, v4

    goto/16 :goto_3

    .line 853
    :cond_0
    invoke-virtual {v5}, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->GetPointArray()Ljava/util/List;

    move-result-object v6

    if-eqz v6, :cond_5

    .line 854
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    if-lez v7, :cond_5

    .line 861
    invoke-virtual {v5}, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->GetPreviewImageSize()Landroid/graphics/Point;

    move-result-object v7

    .line 862
    iget v8, v7, Landroid/graphics/Point;->x:I

    .line 863
    iget v7, v7, Landroid/graphics/Point;->y:I

    .line 865
    sget-object v9, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v8, v7, v9}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v9

    if-eqz v9, :cond_5

    .line 867
    new-instance v10, Landroid/graphics/Canvas;

    invoke-direct {v10, v9}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 869
    new-instance v11, Landroid/graphics/Path;

    invoke-direct {v11}, Landroid/graphics/Path;-><init>()V

    .line 872
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/util/List;

    .line 873
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v14

    if-lez v14, :cond_1

    .line 874
    invoke-interface {v12, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/graphics/PointF;

    .line 875
    invoke-virtual {v5}, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->GetScale()F

    move-result v15

    .line 876
    iget v1, v14, Landroid/graphics/PointF;->x:F

    div-float/2addr v1, v15

    iget v14, v14, Landroid/graphics/PointF;->y:F

    div-float/2addr v14, v15

    invoke-virtual {v11, v1, v14}, Landroid/graphics/Path;->moveTo(FF)V

    const/4 v1, 0x1

    .line 877
    :goto_2
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v14

    if-ge v1, v14, :cond_1

    .line 878
    invoke-interface {v12, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/graphics/PointF;

    .line 879
    iget v2, v14, Landroid/graphics/PointF;->x:F

    div-float/2addr v2, v15

    iget v14, v14, Landroid/graphics/PointF;->y:F

    div-float/2addr v14, v15

    invoke-virtual {v11, v2, v14}, Landroid/graphics/Path;->lineTo(FF)V

    add-int/lit8 v1, v1, 0x1

    const/4 v2, 0x0

    goto :goto_2

    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 883
    :cond_2
    invoke-virtual {v11}, Landroid/graphics/Path;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 884
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 885
    invoke-virtual {v5}, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->GetStrokeWidth()F

    move-result v2

    .line 886
    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    const/4 v4, -0x1

    .line 887
    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 888
    sget-object v4, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 889
    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 891
    invoke-virtual {v10, v11, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 894
    new-instance v1, Lorg/opencv/core/Mat;

    invoke-direct {v1}, Lorg/opencv/core/Mat;-><init>()V

    .line 895
    invoke-static {v9, v1}, Lorg/opencv/android/Utils;->bitmapToMat(Landroid/graphics/Bitmap;Lorg/opencv/core/Mat;)V

    const/4 v2, 0x6

    .line 898
    invoke-static {v1, v1, v2}, Lorg/opencv/imgproc/Imgproc;->cvtColor(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V

    const/high16 v2, 0x41400000    # 12.0f

    .line 903
    new-instance v4, Lepson/colorcorrection/SDIC_MAT_PTR;

    invoke-direct {v4}, Lepson/colorcorrection/SDIC_MAT_PTR;-><init>()V

    float-to-int v2, v2

    .line 904
    invoke-static {v1, v4, v2}, Lepson/colorcorrection/SDIC_BCSC;->MakeMask(Lorg/opencv/core/Mat;Lepson/colorcorrection/SDIC_MAT_PTR;I)I

    .line 905
    invoke-virtual {v1}, Lorg/opencv/core/Mat;->release()V

    .line 907
    invoke-virtual {v4}, Lepson/colorcorrection/SDIC_MAT_PTR;->getMat()Lorg/opencv/core/Mat;

    move-result-object v4

    .line 910
    :cond_3
    invoke-virtual {v9}, Landroid/graphics/Bitmap;->recycle()V

    .line 915
    invoke-virtual {v13}, Lorg/opencv/core/Mat;->cols()I

    move-result v1

    .line 916
    invoke-virtual {v13}, Lorg/opencv/core/Mat;->rows()I

    move-result v2

    if-eqz v4, :cond_5

    if-ne v1, v8, :cond_4

    if-eq v2, v7, :cond_5

    .line 919
    :cond_4
    new-instance v6, Lorg/opencv/core/Size;

    int-to-double v7, v1

    int-to-double v1, v2

    invoke-direct {v6, v7, v8, v1, v2}, Lorg/opencv/core/Size;-><init>(DD)V

    const-wide/16 v19, 0x0

    const-wide/16 v21, 0x0

    const/16 v23, 0x2

    move-object/from16 v16, v4

    move-object/from16 v17, v4

    move-object/from16 v18, v6

    invoke-static/range {v16 .. v23}, Lorg/opencv/imgproc/Imgproc;->resize(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Size;DDI)V

    :cond_5
    move-object v1, v4

    :goto_3
    if-eqz v1, :cond_7

    .line 927
    new-instance v2, Lepson/colorcorrection/SDIC_MAT_PTR;

    invoke-direct {v2}, Lepson/colorcorrection/SDIC_MAT_PTR;-><init>()V

    .line 929
    invoke-virtual {v5}, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->GetBcscBValue()F

    move-result v4

    float-to-double v7, v4

    invoke-virtual {v5}, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->GetBcscCValue()F

    move-result v4

    float-to-double v9, v4

    invoke-virtual {v5}, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParam;->GetBcscSValue()F

    move-result v4

    float-to-double v11, v4

    move-object v4, v13

    move-object v5, v1

    move-object v6, v2

    .line 928
    invoke-static/range {v4 .. v12}, Lepson/colorcorrection/SDIC_BCSC;->BCSCorrection(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lepson/colorcorrection/SDIC_MAT_PTR;DDD)I

    move-result v4

    if-nez v4, :cond_6

    .line 930
    invoke-virtual {v13}, Lorg/opencv/core/Mat;->release()V

    .line 931
    invoke-virtual {v2}, Lepson/colorcorrection/SDIC_MAT_PTR;->getMat()Lorg/opencv/core/Mat;

    move-result-object v2

    move-object v13, v2

    .line 939
    :cond_6
    invoke-virtual {v1}, Lorg/opencv/core/Mat;->release()V

    const/4 v4, 0x0

    goto :goto_4

    :cond_7
    move-object v4, v1

    :goto_4
    add-int/lit8 v3, v3, 0x1

    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_8
    move-object/from16 v13, p1

    :cond_9
    return-object v13
.end method

.method private MakePreviewImageBCSC_InitMSK(I)V
    .locals 2

    .line 1096
    iget-object v0, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    aget-object v1, v0, p1

    if-eqz v1, :cond_0

    .line 1097
    aget-object p1, v0, p1

    const-wide v0, 0x406fe00000000000L    # 255.0

    invoke-static {v0, v1}, Lorg/opencv/core/Scalar;->all(D)Lorg/opencv/core/Scalar;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/opencv/core/Mat;->setTo(Lorg/opencv/core/Scalar;)Lorg/opencv/core/Mat;

    :cond_0
    return-void
.end method

.method private MakePreviewImageBCSC_MakeMSK(I)V
    .locals 5

    .line 1081
    iget-object v0, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    aget-object v1, v0, p1

    if-eqz v1, :cond_0

    .line 1082
    aget-object v0, v0, p1

    invoke-virtual {v0}, Lorg/opencv/core/Mat;->release()V

    .line 1086
    :cond_0
    iget-object v0, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    new-instance v1, Lorg/opencv/core/Mat;

    const/4 v2, 0x5

    aget-object v3, v0, v2

    .line 1087
    invoke-virtual {v3}, Lorg/opencv/core/Mat;->rows()I

    move-result v3

    iget-object v4, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    aget-object v2, v4, v2

    .line 1088
    invoke-virtual {v2}, Lorg/opencv/core/Mat;->cols()I

    move-result v2

    const/4 v4, 0x0

    invoke-direct {v1, v3, v2, v4}, Lorg/opencv/core/Mat;-><init>(III)V

    aput-object v1, v0, p1

    .line 1092
    invoke-direct {p0, p1}, Lepson/colorcorrection/ImageCollect;->MakePreviewImageBCSC_InitMSK(I)V

    return-void
.end method

.method private MakePreviewImageBCSC_MakeSRC(IZ)V
    .locals 3

    .line 1038
    iget-object v0, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    const/4 v1, 0x5

    aget-object v2, v0, v1

    if-eqz v2, :cond_0

    .line 1039
    aget-object v0, v0, v1

    invoke-virtual {v0}, Lorg/opencv/core/Mat;->release()V

    :cond_0
    const/4 v0, 0x7

    if-eqz p2, :cond_1

    .line 1044
    iget-object p2, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    invoke-direct {p0, p1}, Lepson/colorcorrection/ImageCollect;->ResizeMatSmallPreviewSize(I)Lorg/opencv/core/Mat;

    move-result-object p1

    aput-object p1, p2, v1

    .line 1047
    invoke-direct {p0, v0}, Lepson/colorcorrection/ImageCollect;->MakePreviewImageBCSC_MakeMSK(I)V

    .line 1048
    iget-object p1, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    const/16 p2, 0x8

    aget-object v0, p1, p2

    if-eqz v0, :cond_2

    .line 1049
    aget-object p1, p1, p2

    invoke-virtual {p1}, Lorg/opencv/core/Mat;->release()V

    .line 1050
    iget-object p1, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    const/4 v0, 0x0

    aput-object v0, p1, p2

    goto :goto_0

    .line 1055
    :cond_1
    iget-object p2, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    aget-object p1, p2, p1

    invoke-virtual {p1}, Lorg/opencv/core/Mat;->clone()Lorg/opencv/core/Mat;

    move-result-object p1

    aput-object p1, p2, v1

    .line 1058
    invoke-direct {p0, v0}, Lepson/colorcorrection/ImageCollect;->MakePreviewImageBCSC_MakeMSK(I)V

    .line 1059
    invoke-direct {p0}, Lepson/colorcorrection/ImageCollect;->MakePreviewImageBCSC_PartMSK()V

    :cond_2
    :goto_0
    return-void
.end method

.method private MakePreviewImageBCSC_PartMSK()V
    .locals 12

    .line 1102
    iget-object v0, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    const/16 v1, 0x8

    aget-object v2, v0, v1

    if-eqz v2, :cond_1

    const/4 v2, 0x5

    .line 1103
    aget-object v0, v0, v2

    invoke-virtual {v0}, Lorg/opencv/core/Mat;->width()I

    move-result v0

    iget-object v3, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Lorg/opencv/core/Mat;->width()I

    move-result v3

    if-ne v0, v3, :cond_0

    iget-object v0, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    aget-object v0, v0, v2

    .line 1104
    invoke-virtual {v0}, Lorg/opencv/core/Mat;->height()I

    move-result v0

    iget-object v3, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Lorg/opencv/core/Mat;->height()I

    move-result v3

    if-eq v0, v3, :cond_1

    .line 1106
    :cond_0
    new-instance v0, Lorg/opencv/core/Mat;

    invoke-direct {v0}, Lorg/opencv/core/Mat;-><init>()V

    .line 1107
    iget-object v3, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    aget-object v4, v3, v1

    new-instance v6, Lorg/opencv/core/Size;

    aget-object v3, v3, v2

    .line 1108
    invoke-virtual {v3}, Lorg/opencv/core/Mat;->width()I

    move-result v3

    int-to-double v7, v3

    iget-object v3, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    aget-object v2, v3, v2

    invoke-virtual {v2}, Lorg/opencv/core/Mat;->height()I

    move-result v2

    int-to-double v2, v2

    invoke-direct {v6, v7, v8, v2, v3}, Lorg/opencv/core/Size;-><init>(DD)V

    const-wide/16 v7, 0x0

    const-wide/16 v9, 0x0

    const/4 v11, 0x2

    move-object v5, v0

    .line 1107
    invoke-static/range {v4 .. v11}, Lorg/opencv/imgproc/Imgproc;->resize(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Size;DDI)V

    .line 1111
    iget-object v2, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Lorg/opencv/core/Mat;->release()V

    .line 1112
    iget-object v2, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    aput-object v0, v2, v1

    :cond_1
    return-void
.end method

.method private ResizeMat(Lorg/opencv/core/Mat;DD)Lorg/opencv/core/Mat;
    .locals 19

    .line 1551
    invoke-virtual/range {p1 .. p1}, Lorg/opencv/core/Mat;->cols()I

    move-result v0

    int-to-double v2, v0

    invoke-virtual/range {p1 .. p1}, Lorg/opencv/core/Mat;->rows()I

    move-result v0

    int-to-double v4, v0

    const/4 v10, 0x1

    move-object/from16 v1, p0

    move-wide/from16 v6, p2

    move-wide/from16 v8, p4

    invoke-direct/range {v1 .. v10}, Lepson/colorcorrection/ImageCollect;->CalcResizeSize(DDDDZ)Lorg/opencv/core/Size;

    move-result-object v13

    .line 1552
    new-instance v0, Lorg/opencv/core/Mat;

    invoke-direct {v0}, Lorg/opencv/core/Mat;-><init>()V

    const-wide/16 v14, 0x0

    const-wide/16 v16, 0x0

    const/16 v18, 0x3

    move-object/from16 v11, p1

    move-object v12, v0

    .line 1553
    invoke-static/range {v11 .. v18}, Lorg/opencv/imgproc/Imgproc;->resize(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Size;DDI)V

    return-object v0
.end method

.method private ResizeMatSmallPreviewSize(I)Lorg/opencv/core/Mat;
    .locals 9

    .line 1065
    new-instance v8, Lorg/opencv/core/Mat;

    invoke-direct {v8}, Lorg/opencv/core/Mat;-><init>()V

    .line 1066
    iget-object v0, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    aget-object v1, v0, p1

    new-instance v2, Lorg/opencv/core/Size;

    aget-object v0, v0, p1

    .line 1070
    invoke-virtual {v0}, Lorg/opencv/core/Mat;->width()I

    move-result v0

    int-to-float v0, v0

    iget v3, p0, Lepson/colorcorrection/ImageCollect;->mSmallPreviewRate:F

    div-float/2addr v0, v3

    float-to-double v3, v0

    iget-object v0, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    aget-object p1, v0, p1

    .line 1071
    invoke-virtual {p1}, Lorg/opencv/core/Mat;->height()I

    move-result p1

    int-to-float p1, p1

    iget v0, p0, Lepson/colorcorrection/ImageCollect;->mSmallPreviewRate:F

    div-float/2addr p1, v0

    float-to-double v5, p1

    invoke-direct {v2, v3, v4, v5, v6}, Lorg/opencv/core/Size;-><init>(DD)V

    const-wide/16 v3, 0x0

    const-wide/16 v5, 0x0

    const/4 v7, 0x3

    move-object v0, v1

    move-object v1, v8

    .line 1066
    invoke-static/range {v0 .. v7}, Lorg/opencv/imgproc/Imgproc;->resize(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Size;DDI)V

    return-object v8
.end method

.method private ensureDirectory(Ljava/io/File;)Z
    .locals 1
    .param p1    # Ljava/io/File;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 465
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 466
    invoke-virtual {p1}, Ljava/io/File;->mkdirs()Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method


# virtual methods
.method public DeleteCacheFile()V
    .locals 2

    .line 176
    iget-object v0, p0, Lepson/colorcorrection/ImageCollect;->mCacheFolder:Ljava/io/File;

    const-string v1, "icpd_"

    invoke-direct {p0, v0, v1}, Lepson/colorcorrection/ImageCollect;->DeleteCacheFileAll(Ljava/io/File;Ljava/lang/String;)I

    return-void
.end method

.method public DetectEdge(Landroid/graphics/Bitmap;)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            ")",
            "Ljava/util/List<",
            "Ljava/util/List<",
            "Landroid/graphics/PointF;",
            ">;>;"
        }
    .end annotation

    .line 1143
    iget-object v0, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    const/16 v1, 0x8

    aget-object v2, v0, v1

    if-eqz v2, :cond_0

    .line 1144
    aget-object v0, v0, v1

    invoke-virtual {v0}, Lorg/opencv/core/Mat;->release()V

    .line 1147
    :cond_0
    new-instance v0, Lorg/opencv/core/Mat;

    invoke-direct {v0}, Lorg/opencv/core/Mat;-><init>()V

    .line 1148
    invoke-static {p1, v0}, Lorg/opencv/android/Utils;->bitmapToMat(Landroid/graphics/Bitmap;Lorg/opencv/core/Mat;)V

    .line 1155
    new-instance p1, Lorg/opencv/core/Mat;

    invoke-direct {p1}, Lorg/opencv/core/Mat;-><init>()V

    const/4 v2, 0x6

    .line 1156
    invoke-static {v0, p1, v2}, Lorg/opencv/imgproc/Imgproc;->cvtColor(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V

    .line 1163
    new-instance v2, Lorg/opencv/core/Mat;

    invoke-direct {v2}, Lorg/opencv/core/Mat;-><init>()V

    .line 1164
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1167
    new-instance v4, Lorg/opencv/core/Mat;

    invoke-direct {v4}, Lorg/opencv/core/Mat;-><init>()V

    .line 1168
    invoke-virtual {p1}, Lorg/opencv/core/Mat;->clone()Lorg/opencv/core/Mat;

    move-result-object v4

    const/4 v5, 0x3

    const/4 v6, 0x1

    .line 1169
    invoke-static {v4, v3, v2, v6, v5}, Lorg/opencv/imgproc/Imgproc;->findContours(Lorg/opencv/core/Mat;Ljava/util/List;Lorg/opencv/core/Mat;II)V

    .line 1170
    invoke-virtual {v4}, Lorg/opencv/core/Mat;->release()V

    .line 1173
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v4, 0x0

    .line 1175
    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    if-ge v4, v5, :cond_2

    .line 1176
    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/opencv/core/MatOfPoint;

    .line 1177
    invoke-virtual {v5}, Lorg/opencv/core/MatOfPoint;->toList()Ljava/util/List;

    move-result-object v5

    .line 1178
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1179
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/opencv/core/Point;

    .line 1180
    new-instance v8, Landroid/graphics/PointF;

    iget-wide v9, v7, Lorg/opencv/core/Point;->x:D

    double-to-float v9, v9

    iget-wide v10, v7, Lorg/opencv/core/Point;->y:D

    double-to-float v7, v10

    invoke-direct {v8, v9, v7}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1182
    :cond_1
    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1185
    :cond_2
    iget-object v3, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    aput-object p1, v3, v1

    const/16 p1, 0x9

    .line 1188
    aget-object v1, v3, p1

    if-eqz v1, :cond_3

    .line 1189
    aget-object v1, v3, p1

    invoke-virtual {v1}, Lorg/opencv/core/Mat;->release()V

    .line 1190
    iget-object v1, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    const/4 v3, 0x0

    aput-object v3, v1, p1

    .line 1193
    :cond_3
    invoke-virtual {v0}, Lorg/opencv/core/Mat;->release()V

    return-object v2
.end method

.method public GetCornerPoints()[Landroid/graphics/PointF;
    .locals 17

    move-object/from16 v6, p0

    .line 1202
    iget-object v0, v6, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    const/4 v7, 0x0

    aget-object v1, v0, v7

    if-eqz v1, :cond_15

    .line 1205
    aget-object v1, v0, v7

    const-wide/high16 v2, 0x4084000000000000L    # 640.0

    const-wide/high16 v4, 0x4084000000000000L    # 640.0

    move-object/from16 v0, p0

    invoke-direct/range {v0 .. v5}, Lepson/colorcorrection/ImageCollect;->ResizeMat(Lorg/opencv/core/Mat;DD)Lorg/opencv/core/Mat;

    move-result-object v0

    if-eqz v0, :cond_14

    const/16 v1, 0xc8

    const/4 v2, 0x0

    :goto_0
    const/16 v3, 0x32

    const/4 v4, 0x4

    const/4 v5, 0x3

    if-lt v1, v3, :cond_e

    .line 1210
    invoke-direct {v6, v0, v1}, Lepson/colorcorrection/ImageCollect;->DetectEdge(Lorg/opencv/core/Mat;I)Lorg/opencv/core/MatOfPoint2f;

    move-result-object v2

    if-eqz v2, :cond_d

    .line 1213
    invoke-virtual {v0}, Lorg/opencv/core/Mat;->rows()I

    move-result v3

    .line 1215
    invoke-virtual {v2}, Lorg/opencv/core/MatOfPoint2f;->rows()I

    move-result v10

    if-lt v5, v10, :cond_0

    goto/16 :goto_c

    .line 1217
    :cond_0
    invoke-virtual {v2}, Lorg/opencv/core/MatOfPoint2f;->rows()I

    move-result v10

    if-ne v4, v10, :cond_4

    .line 1218
    invoke-virtual {v2}, Lorg/opencv/core/MatOfPoint2f;->toArray()[Lorg/opencv/core/Point;

    move-result-object v10

    move v11, v3

    const/4 v3, 0x0

    const/4 v12, 0x0

    :goto_1
    if-ge v3, v4, :cond_3

    int-to-double v13, v11

    .line 1220
    aget-object v11, v10, v3

    iget-wide v5, v11, Lorg/opencv/core/Point;->y:D

    cmpg-double v11, v13, v5

    if-gez v11, :cond_1

    goto :goto_2

    :cond_1
    aget-object v5, v10, v3

    iget-wide v13, v5, Lorg/opencv/core/Point;->y:D

    :goto_2
    double-to-int v11, v13

    int-to-double v5, v12

    .line 1221
    aget-object v12, v10, v3

    iget-wide v12, v12, Lorg/opencv/core/Point;->y:D

    cmpl-double v14, v5, v12

    if-lez v14, :cond_2

    goto :goto_3

    :cond_2
    aget-object v5, v10, v3

    iget-wide v5, v5, Lorg/opencv/core/Point;->y:D

    :goto_3
    double-to-int v12, v5

    add-int/lit8 v3, v3, 0x1

    const/4 v5, 0x3

    move-object/from16 v6, p0

    goto :goto_1

    :cond_3
    const/4 v6, 0x0

    goto/16 :goto_a

    .line 1225
    :cond_4
    invoke-static {v2}, Lorg/opencv/imgproc/Imgproc;->minAreaRect(Lorg/opencv/core/MatOfPoint2f;)Lorg/opencv/core/RotatedRect;

    move-result-object v5

    .line 1226
    new-array v6, v4, [Lorg/opencv/core/Point;

    .line 1227
    invoke-virtual {v5, v6}, Lorg/opencv/core/RotatedRect;->points([Lorg/opencv/core/Point;)V

    .line 1228
    invoke-virtual {v2}, Lorg/opencv/core/MatOfPoint2f;->toList()Ljava/util/List;

    move-result-object v5

    move v11, v3

    const/4 v3, 0x0

    const/4 v12, 0x0

    :goto_4
    if-ge v3, v4, :cond_9

    .line 1233
    aget-object v10, v6, v3

    iget-wide v13, v10, Lorg/opencv/core/Point;->x:D

    const-wide/16 v15, 0x0

    cmpg-double v10, v13, v15

    if-lez v10, :cond_8

    aget-object v10, v6, v3

    iget-wide v13, v10, Lorg/opencv/core/Point;->y:D

    cmpg-double v10, v13, v15

    if-lez v10, :cond_8

    aget-object v10, v6, v3

    iget-wide v13, v10, Lorg/opencv/core/Point;->x:D

    .line 1234
    invoke-virtual {v0}, Lorg/opencv/core/Mat;->cols()I

    move-result v10

    int-to-double v8, v10

    cmpl-double v10, v13, v8

    if-gez v10, :cond_8

    aget-object v8, v6, v3

    iget-wide v8, v8, Lorg/opencv/core/Point;->y:D

    invoke-virtual {v0}, Lorg/opencv/core/Mat;->rows()I

    move-result v10

    int-to-double v13, v10

    cmpl-double v10, v8, v13

    if-ltz v10, :cond_5

    goto :goto_7

    .line 1239
    :cond_5
    aget-object v8, v6, v3

    invoke-interface {v5, v3, v8}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    int-to-double v8, v11

    .line 1240
    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/opencv/core/Point;

    iget-wide v10, v10, Lorg/opencv/core/Point;->y:D

    cmpg-double v13, v8, v10

    if-gez v13, :cond_6

    goto :goto_5

    :cond_6
    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/opencv/core/Point;

    iget-wide v8, v8, Lorg/opencv/core/Point;->y:D

    :goto_5
    double-to-int v11, v8

    int-to-double v8, v12

    .line 1241
    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/opencv/core/Point;

    iget-wide v12, v10, Lorg/opencv/core/Point;->y:D

    cmpl-double v10, v8, v12

    if-lez v10, :cond_7

    goto :goto_6

    :cond_7
    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/opencv/core/Point;

    iget-wide v8, v8, Lorg/opencv/core/Point;->y:D

    :goto_6
    double-to-int v12, v8

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_8
    :goto_7
    const/4 v6, 0x1

    goto :goto_8

    :cond_9
    const/4 v6, 0x0

    .line 1243
    :goto_8
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 1244
    invoke-virtual {v2}, Lorg/opencv/core/MatOfPoint2f;->release()V

    .line 1245
    new-instance v2, Lorg/opencv/core/MatOfPoint2f;

    invoke-direct {v2}, Lorg/opencv/core/MatOfPoint2f;-><init>()V

    const/4 v9, 0x0

    :goto_9
    if-ge v9, v3, :cond_a

    .line 1247
    invoke-interface {v5, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    invoke-interface {v8, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v9, v9, 0x1

    goto :goto_9

    .line 1249
    :cond_a
    invoke-virtual {v2, v8}, Lorg/opencv/core/MatOfPoint2f;->fromList(Ljava/util/List;)V

    :goto_a
    if-nez v6, :cond_c

    const v3, 0x3f4ccccd    # 0.8f

    sub-int/2addr v12, v11

    int-to-float v5, v12

    .line 1254
    invoke-virtual {v0}, Lorg/opencv/core/Mat;->rows()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v5, v6

    cmpg-float v3, v3, v5

    if-ltz v3, :cond_b

    const/4 v6, 0x1

    goto :goto_b

    :cond_b
    const/4 v6, 0x0

    :cond_c
    :goto_b
    if-nez v6, :cond_d

    goto :goto_d

    :cond_d
    :goto_c
    add-int/lit8 v1, v1, -0xa

    move-object/from16 v6, p0

    goto/16 :goto_0

    :cond_e
    :goto_d
    if-eqz v2, :cond_13

    .line 1264
    invoke-virtual {v2}, Lorg/opencv/core/MatOfPoint2f;->rows()I

    move-result v1

    if-lt v1, v4, :cond_11

    .line 1265
    new-array v8, v4, [Landroid/graphics/PointF;

    .line 1266
    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1}, Landroid/graphics/PointF;-><init>()V

    aput-object v1, v8, v7

    .line 1267
    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1}, Landroid/graphics/PointF;-><init>()V

    const/4 v3, 0x1

    aput-object v1, v8, v3

    .line 1268
    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1}, Landroid/graphics/PointF;-><init>()V

    const/4 v3, 0x2

    aput-object v1, v8, v3

    .line 1269
    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1}, Landroid/graphics/PointF;-><init>()V

    const/4 v5, 0x3

    aput-object v1, v8, v5

    .line 1272
    invoke-virtual {v2}, Lorg/opencv/core/MatOfPoint2f;->toList()Ljava/util/List;

    move-result-object v1

    .line 1273
    new-instance v5, Lepson/colorcorrection/ImageCollect$SortPoints;

    move-object/from16 v6, p0

    const/4 v9, 0x0

    invoke-direct {v5, v6, v9}, Lepson/colorcorrection/ImageCollect$SortPoints;-><init>(Lepson/colorcorrection/ImageCollect;Lepson/colorcorrection/ImageCollect$1;)V

    invoke-static {v1, v5}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1275
    invoke-interface {v1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/opencv/core/Point;

    iget-wide v9, v5, Lorg/opencv/core/Point;->x:D

    const/4 v5, 0x1

    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lorg/opencv/core/Point;

    iget-wide v11, v11, Lorg/opencv/core/Point;->x:D

    cmpg-double v5, v9, v11

    if-gez v5, :cond_f

    .line 1276
    aget-object v5, v8, v7

    invoke-interface {v1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/opencv/core/Point;

    iget-wide v9, v9, Lorg/opencv/core/Point;->x:D

    double-to-float v9, v9

    iput v9, v5, Landroid/graphics/PointF;->x:F

    .line 1277
    aget-object v5, v8, v7

    invoke-interface {v1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/opencv/core/Point;

    iget-wide v9, v9, Lorg/opencv/core/Point;->y:D

    double-to-float v9, v9

    iput v9, v5, Landroid/graphics/PointF;->y:F

    const/4 v5, 0x3

    .line 1278
    aget-object v9, v8, v5

    const/4 v10, 0x1

    invoke-interface {v1, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lorg/opencv/core/Point;

    iget-wide v11, v11, Lorg/opencv/core/Point;->x:D

    double-to-float v11, v11

    iput v11, v9, Landroid/graphics/PointF;->x:F

    .line 1279
    aget-object v9, v8, v5

    invoke-interface {v1, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/opencv/core/Point;

    iget-wide v11, v5, Lorg/opencv/core/Point;->y:D

    double-to-float v5, v11

    iput v5, v9, Landroid/graphics/PointF;->y:F

    const/4 v5, 0x3

    goto :goto_e

    :cond_f
    const/4 v10, 0x1

    .line 1281
    aget-object v5, v8, v7

    invoke-interface {v1, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/opencv/core/Point;

    iget-wide v11, v9, Lorg/opencv/core/Point;->x:D

    double-to-float v9, v11

    iput v9, v5, Landroid/graphics/PointF;->x:F

    .line 1282
    aget-object v5, v8, v7

    invoke-interface {v1, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/opencv/core/Point;

    iget-wide v9, v9, Lorg/opencv/core/Point;->y:D

    double-to-float v9, v9

    iput v9, v5, Landroid/graphics/PointF;->y:F

    const/4 v5, 0x3

    .line 1283
    aget-object v9, v8, v5

    invoke-interface {v1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/opencv/core/Point;

    iget-wide v10, v10, Lorg/opencv/core/Point;->x:D

    double-to-float v10, v10

    iput v10, v9, Landroid/graphics/PointF;->x:F

    .line 1284
    aget-object v9, v8, v5

    invoke-interface {v1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/opencv/core/Point;

    iget-wide v10, v10, Lorg/opencv/core/Point;->y:D

    double-to-float v10, v10

    iput v10, v9, Landroid/graphics/PointF;->y:F

    .line 1286
    :goto_e
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/opencv/core/Point;

    iget-wide v9, v9, Lorg/opencv/core/Point;->x:D

    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lorg/opencv/core/Point;

    iget-wide v11, v11, Lorg/opencv/core/Point;->x:D

    cmpg-double v5, v9, v11

    if-gez v5, :cond_10

    const/4 v5, 0x1

    .line 1287
    aget-object v9, v8, v5

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/opencv/core/Point;

    iget-wide v10, v10, Lorg/opencv/core/Point;->x:D

    double-to-float v10, v10

    iput v10, v9, Landroid/graphics/PointF;->x:F

    .line 1288
    aget-object v5, v8, v5

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/opencv/core/Point;

    iget-wide v9, v9, Lorg/opencv/core/Point;->y:D

    double-to-float v9, v9

    iput v9, v5, Landroid/graphics/PointF;->y:F

    .line 1289
    aget-object v5, v8, v3

    const/4 v9, 0x3

    invoke-interface {v1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/opencv/core/Point;

    iget-wide v10, v10, Lorg/opencv/core/Point;->x:D

    double-to-float v10, v10

    iput v10, v5, Landroid/graphics/PointF;->x:F

    .line 1290
    aget-object v3, v8, v3

    invoke-interface {v1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/opencv/core/Point;

    iget-wide v9, v1, Lorg/opencv/core/Point;->y:D

    double-to-float v1, v9

    iput v1, v3, Landroid/graphics/PointF;->y:F

    goto :goto_f

    :cond_10
    const/4 v5, 0x1

    const/4 v9, 0x3

    .line 1292
    aget-object v10, v8, v5

    invoke-interface {v1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lorg/opencv/core/Point;

    iget-wide v11, v11, Lorg/opencv/core/Point;->x:D

    double-to-float v11, v11

    iput v11, v10, Landroid/graphics/PointF;->x:F

    .line 1293
    aget-object v5, v8, v5

    invoke-interface {v1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/opencv/core/Point;

    iget-wide v9, v9, Lorg/opencv/core/Point;->y:D

    double-to-float v9, v9

    iput v9, v5, Landroid/graphics/PointF;->y:F

    .line 1294
    aget-object v5, v8, v3

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/opencv/core/Point;

    iget-wide v9, v9, Lorg/opencv/core/Point;->x:D

    double-to-float v9, v9

    iput v9, v5, Landroid/graphics/PointF;->x:F

    .line 1295
    aget-object v5, v8, v3

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/opencv/core/Point;

    iget-wide v9, v1, Lorg/opencv/core/Point;->y:D

    double-to-float v1, v9

    iput v1, v5, Landroid/graphics/PointF;->y:F

    .line 1299
    :goto_f
    iget-object v1, v6, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    aget-object v1, v1, v7

    invoke-virtual {v1}, Lorg/opencv/core/Mat;->rows()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0}, Lorg/opencv/core/Mat;->rows()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v1, v3

    :goto_10
    if-ge v7, v4, :cond_12

    .line 1301
    aget-object v3, v8, v7

    iget v5, v3, Landroid/graphics/PointF;->x:F

    mul-float v5, v5, v1

    iput v5, v3, Landroid/graphics/PointF;->x:F

    .line 1302
    aget-object v3, v8, v7

    iget v5, v3, Landroid/graphics/PointF;->y:F

    mul-float v5, v5, v1

    iput v5, v3, Landroid/graphics/PointF;->y:F

    add-int/lit8 v7, v7, 0x1

    goto :goto_10

    :cond_11
    move-object/from16 v6, p0

    const/4 v9, 0x0

    move-object v8, v9

    .line 1306
    :cond_12
    invoke-virtual {v2}, Lorg/opencv/core/MatOfPoint2f;->release()V

    goto :goto_11

    :cond_13
    move-object/from16 v6, p0

    const/4 v9, 0x0

    move-object v8, v9

    .line 1310
    :goto_11
    invoke-virtual {v0}, Lorg/opencv/core/Mat;->release()V

    goto :goto_13

    :cond_14
    const/4 v9, 0x0

    goto :goto_12

    :cond_15
    const/4 v9, 0x0

    :goto_12
    move-object v8, v9

    :goto_13
    return-object v8
.end method

.method public GetMakedPreviewImageSize()Landroid/graphics/Point;
    .locals 4

    .line 220
    new-instance v0, Landroid/graphics/Point;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v1}, Landroid/graphics/Point;-><init>(II)V

    .line 221
    iget-object v1, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    const/4 v2, 0x1

    aget-object v3, v1, v2

    if-eqz v3, :cond_0

    .line 222
    aget-object v1, v1, v2

    invoke-virtual {v1}, Lorg/opencv/core/Mat;->cols()I

    move-result v1

    iput v1, v0, Landroid/graphics/Point;->x:I

    .line 223
    iget-object v1, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    aget-object v1, v1, v2

    invoke-virtual {v1}, Lorg/opencv/core/Mat;->rows()I

    move-result v1

    iput v1, v0, Landroid/graphics/Point;->y:I

    :cond_0
    return-object v0
.end method

.method public GetPreviewImage(I)Landroid/graphics/Bitmap;
    .locals 12

    .line 1121
    iget-object v0, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    aget-object v1, v0, p1

    if-eqz v1, :cond_2

    const/4 v1, 0x6

    if-ne p1, v1, :cond_1

    .line 1124
    aget-object v0, v0, v1

    invoke-virtual {v0}, Lorg/opencv/core/Mat;->width()I

    move-result v0

    iget-object v2, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lorg/opencv/core/Mat;->width()I

    move-result v2

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    aget-object v0, v0, v1

    .line 1125
    invoke-virtual {v0}, Lorg/opencv/core/Mat;->height()I

    move-result v0

    iget-object v2, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lorg/opencv/core/Mat;->height()I

    move-result v2

    if-eq v0, v2, :cond_1

    .line 1127
    :cond_0
    new-instance v0, Lorg/opencv/core/Mat;

    invoke-direct {v0}, Lorg/opencv/core/Mat;-><init>()V

    .line 1128
    iget-object v2, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    aget-object v4, v2, v1

    new-instance v6, Lorg/opencv/core/Size;

    aget-object v2, v2, v3

    .line 1129
    invoke-virtual {v2}, Lorg/opencv/core/Mat;->width()I

    move-result v2

    int-to-double v7, v2

    iget-object v2, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lorg/opencv/core/Mat;->height()I

    move-result v2

    int-to-double v2, v2

    invoke-direct {v6, v7, v8, v2, v3}, Lorg/opencv/core/Size;-><init>(DD)V

    const-wide/16 v7, 0x0

    const-wide/16 v9, 0x0

    const/4 v11, 0x2

    move-object v5, v0

    .line 1128
    invoke-static/range {v4 .. v11}, Lorg/opencv/imgproc/Imgproc;->resize(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Size;DDI)V

    .line 1131
    iget-object v2, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Lorg/opencv/core/Mat;->release()V

    .line 1132
    iget-object v2, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    aput-object v0, v2, v1

    .line 1135
    :cond_1
    iget-object v0, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    aget-object p1, v0, p1

    invoke-direct {p0, p1}, Lepson/colorcorrection/ImageCollect;->ConvertMatToBitmap(Lorg/opencv/core/Mat;)Landroid/graphics/Bitmap;

    move-result-object p1

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public GetSavePath(Z)Ljava/lang/String;
    .locals 0

    if-eqz p1, :cond_0

    .line 212
    iget-object p1, p0, Lepson/colorcorrection/ImageCollect;->mSaveFullPath:Ljava/lang/String;

    return-object p1

    .line 215
    :cond_0
    iget-object p1, p0, Lepson/colorcorrection/ImageCollect;->mSaveDisplayPath:Ljava/lang/String;

    return-object p1
.end method

.method public MakeCropPrintSizeImage([Landroid/graphics/PointF;Lorg/opencv/core/Size;ILjava/util/List;)Lorg/opencv/core/Mat;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Landroid/graphics/PointF;",
            "Lorg/opencv/core/Size;",
            "I",
            "Ljava/util/List<",
            "Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;",
            ">;)",
            "Lorg/opencv/core/Mat;"
        }
    .end annotation

    move-object/from16 v6, p0

    move-object/from16 v7, p4

    .line 231
    iget-object v0, v6, Lepson/colorcorrection/ImageCollect;->mMatPrint:[Lorg/opencv/core/Mat;

    const/4 v8, 0x0

    aget-object v1, v0, v8

    const/4 v9, 0x0

    if-eqz v1, :cond_12

    iget-object v1, v6, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    aget-object v1, v1, v8

    if-eqz v1, :cond_12

    .line 234
    aget-object v0, v0, v8

    invoke-virtual {v0}, Lorg/opencv/core/Mat;->width()I

    move-result v0

    int-to-float v0, v0

    .line 235
    iget-object v1, v6, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    aget-object v1, v1, v8

    invoke-virtual {v1}, Lorg/opencv/core/Mat;->width()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    move-object/from16 v1, p1

    .line 238
    invoke-direct {v6, v1, v0}, Lepson/colorcorrection/ImageCollect;->GetCropPointsAdjustment([Landroid/graphics/PointF;F)[Landroid/graphics/PointF;

    move-result-object v10

    .line 239
    invoke-direct {v6, v10}, Lepson/colorcorrection/ImageCollect;->GetCropBoundRect([Landroid/graphics/PointF;)Landroid/graphics/RectF;

    move-result-object v11

    .line 242
    iget-object v0, v6, Lepson/colorcorrection/ImageCollect;->mPointEditPts:[Landroid/graphics/PointF;

    aget-object v0, v0, v8

    iget v0, v0, Landroid/graphics/PointF;->x:F

    aget-object v1, v10, v8

    iget v1, v1, Landroid/graphics/PointF;->x:F

    const/4 v12, 0x3

    const/4 v13, 0x2

    const/4 v14, 0x1

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    iget-object v0, v6, Lepson/colorcorrection/ImageCollect;->mPointEditPts:[Landroid/graphics/PointF;

    aget-object v0, v0, v8

    iget v0, v0, Landroid/graphics/PointF;->y:F

    aget-object v1, v10, v8

    iget v1, v1, Landroid/graphics/PointF;->y:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    iget-object v0, v6, Lepson/colorcorrection/ImageCollect;->mPointEditPts:[Landroid/graphics/PointF;

    aget-object v0, v0, v14

    iget v0, v0, Landroid/graphics/PointF;->x:F

    aget-object v1, v10, v14

    iget v1, v1, Landroid/graphics/PointF;->x:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    iget-object v0, v6, Lepson/colorcorrection/ImageCollect;->mPointEditPts:[Landroid/graphics/PointF;

    aget-object v0, v0, v14

    iget v0, v0, Landroid/graphics/PointF;->y:F

    aget-object v1, v10, v14

    iget v1, v1, Landroid/graphics/PointF;->y:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    iget-object v0, v6, Lepson/colorcorrection/ImageCollect;->mPointEditPts:[Landroid/graphics/PointF;

    aget-object v0, v0, v13

    iget v0, v0, Landroid/graphics/PointF;->x:F

    aget-object v1, v10, v13

    iget v1, v1, Landroid/graphics/PointF;->x:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    iget-object v0, v6, Lepson/colorcorrection/ImageCollect;->mPointEditPts:[Landroid/graphics/PointF;

    aget-object v0, v0, v13

    iget v0, v0, Landroid/graphics/PointF;->y:F

    aget-object v1, v10, v13

    iget v1, v1, Landroid/graphics/PointF;->y:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    iget-object v0, v6, Lepson/colorcorrection/ImageCollect;->mPointEditPts:[Landroid/graphics/PointF;

    aget-object v0, v0, v12

    iget v0, v0, Landroid/graphics/PointF;->x:F

    aget-object v1, v10, v12

    iget v1, v1, Landroid/graphics/PointF;->x:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    iget-object v0, v6, Lepson/colorcorrection/ImageCollect;->mPointEditPts:[Landroid/graphics/PointF;

    aget-object v0, v0, v12

    iget v0, v0, Landroid/graphics/PointF;->y:F

    aget-object v1, v10, v12

    iget v1, v1, Landroid/graphics/PointF;->y:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    iget-object v0, v6, Lepson/colorcorrection/ImageCollect;->mRectEditCrop:Landroid/graphics/RectF;

    .line 246
    invoke-virtual {v0, v11}, Landroid/graphics/RectF;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_3

    .line 250
    iget-object v0, v6, Lepson/colorcorrection/ImageCollect;->mMatPrint:[Lorg/opencv/core/Mat;

    aget-object v0, v0, v14

    if-nez v0, :cond_2

    goto :goto_2

    :cond_2
    move/from16 v0, p3

    goto :goto_3

    .line 251
    :cond_3
    :goto_2
    iget-object v0, v6, Lepson/colorcorrection/ImageCollect;->mMatPrint:[Lorg/opencv/core/Mat;

    aget-object v1, v0, v14

    if-eqz v1, :cond_4

    .line 252
    aget-object v0, v0, v14

    invoke-virtual {v0}, Lorg/opencv/core/Mat;->release()V

    .line 253
    iget-object v0, v6, Lepson/colorcorrection/ImageCollect;->mMatPrint:[Lorg/opencv/core/Mat;

    aput-object v9, v0, v14

    .line 256
    :cond_4
    iget-object v15, v6, Lepson/colorcorrection/ImageCollect;->mMatPrint:[Lorg/opencv/core/Mat;

    aget-object v1, v15, v8

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object v2, v10

    move-object v3, v11

    move-object/from16 v4, p2

    invoke-direct/range {v0 .. v5}, Lepson/colorcorrection/ImageCollect;->GetCropImage(Lorg/opencv/core/Mat;[Landroid/graphics/PointF;Landroid/graphics/RectF;Lorg/opencv/core/Size;Lorg/opencv/core/Size;)Lorg/opencv/core/Mat;

    move-result-object v0

    aput-object v0, v15, v14

    .line 259
    iget-object v0, v6, Lepson/colorcorrection/ImageCollect;->mRectEditCrop:Landroid/graphics/RectF;

    invoke-virtual {v0, v11}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 260
    iget-object v0, v6, Lepson/colorcorrection/ImageCollect;->mPointEditPts:[Landroid/graphics/PointF;

    aget-object v0, v0, v8

    aget-object v1, v10, v8

    invoke-virtual {v0, v1}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    .line 261
    iget-object v0, v6, Lepson/colorcorrection/ImageCollect;->mPointEditPts:[Landroid/graphics/PointF;

    aget-object v0, v0, v14

    aget-object v1, v10, v14

    invoke-virtual {v0, v1}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    .line 262
    iget-object v0, v6, Lepson/colorcorrection/ImageCollect;->mPointEditPts:[Landroid/graphics/PointF;

    aget-object v0, v0, v13

    aget-object v1, v10, v13

    invoke-virtual {v0, v1}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    .line 263
    iget-object v0, v6, Lepson/colorcorrection/ImageCollect;->mPointEditPts:[Landroid/graphics/PointF;

    aget-object v0, v0, v12

    aget-object v1, v10, v12

    invoke-virtual {v0, v1}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    move/from16 v0, p3

    :goto_3
    if-ne v0, v14, :cond_5

    .line 268
    iget-object v0, v6, Lepson/colorcorrection/ImageCollect;->mMatPrint:[Lorg/opencv/core/Mat;

    aget-object v0, v0, v14

    return-object v0

    .line 270
    :cond_5
    iget-object v1, v6, Lepson/colorcorrection/ImageCollect;->mMatPrint:[Lorg/opencv/core/Mat;

    aget-object v1, v1, v14

    .line 276
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_6

    .line 280
    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;

    .line 281
    invoke-virtual {v0}, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;->GetImageType()I

    move-result v0

    :cond_6
    if-ne v0, v13, :cond_8

    .line 290
    iget-object v2, v6, Lepson/colorcorrection/ImageCollect;->mMatPrint:[Lorg/opencv/core/Mat;

    aget-object v3, v2, v13

    if-eqz v3, :cond_7

    .line 291
    aget-object v2, v2, v13

    invoke-virtual {v2}, Lorg/opencv/core/Mat;->release()V

    .line 292
    iget-object v2, v6, Lepson/colorcorrection/ImageCollect;->mMatPrint:[Lorg/opencv/core/Mat;

    aput-object v9, v2, v13

    .line 294
    :cond_7
    new-instance v2, Lepson/colorcorrection/SDIC_MAT_PTR;

    invoke-direct {v2}, Lepson/colorcorrection/SDIC_MAT_PTR;-><init>()V

    .line 295
    iget-object v3, v6, Lepson/colorcorrection/ImageCollect;->mMatPrint:[Lorg/opencv/core/Mat;

    aget-object v3, v3, v14

    invoke-static {v3, v2}, Lepson/colorcorrection/SDIC_PWC;->PaperWhiteCorrection(Lorg/opencv/core/Mat;Lepson/colorcorrection/SDIC_MAT_PTR;)I

    move-result v3

    if-nez v3, :cond_a

    .line 296
    iget-object v3, v6, Lepson/colorcorrection/ImageCollect;->mMatPrint:[Lorg/opencv/core/Mat;

    invoke-virtual {v2}, Lepson/colorcorrection/SDIC_MAT_PTR;->getMat()Lorg/opencv/core/Mat;

    move-result-object v9

    aput-object v9, v3, v13

    goto :goto_4

    :cond_8
    if-ne v0, v12, :cond_a

    .line 299
    iget-object v2, v6, Lepson/colorcorrection/ImageCollect;->mMatPrint:[Lorg/opencv/core/Mat;

    aget-object v3, v2, v12

    if-eqz v3, :cond_9

    .line 300
    aget-object v2, v2, v12

    invoke-virtual {v2}, Lorg/opencv/core/Mat;->release()V

    .line 301
    iget-object v2, v6, Lepson/colorcorrection/ImageCollect;->mMatPrint:[Lorg/opencv/core/Mat;

    aput-object v9, v2, v12

    .line 303
    :cond_9
    new-instance v2, Lepson/colorcorrection/SDIC_MAT_PTR;

    invoke-direct {v2}, Lepson/colorcorrection/SDIC_MAT_PTR;-><init>()V

    .line 304
    iget-object v3, v6, Lepson/colorcorrection/ImageCollect;->mMatPrint:[Lorg/opencv/core/Mat;

    aget-object v3, v3, v14

    invoke-static {v3, v2}, Lepson/colorcorrection/SDIC_PWC;->PaperWhiteCorrection2(Lorg/opencv/core/Mat;Lepson/colorcorrection/SDIC_MAT_PTR;)I

    move-result v3

    if-nez v3, :cond_a

    .line 305
    iget-object v3, v6, Lepson/colorcorrection/ImageCollect;->mMatPrint:[Lorg/opencv/core/Mat;

    invoke-virtual {v2}, Lepson/colorcorrection/SDIC_MAT_PTR;->getMat()Lorg/opencv/core/Mat;

    move-result-object v9

    aput-object v9, v3, v12

    :cond_a
    :goto_4
    if-lez v1, :cond_11

    if-ne v0, v13, :cond_b

    .line 312
    iget-object v0, v6, Lepson/colorcorrection/ImageCollect;->mMatPrint:[Lorg/opencv/core/Mat;

    aget-object v0, v0, v13

    goto :goto_5

    :cond_b
    if-ne v0, v12, :cond_c

    .line 314
    iget-object v0, v6, Lepson/colorcorrection/ImageCollect;->mMatPrint:[Lorg/opencv/core/Mat;

    aget-object v0, v0, v12

    goto :goto_5

    .line 316
    :cond_c
    iget-object v0, v6, Lepson/colorcorrection/ImageCollect;->mMatPrint:[Lorg/opencv/core/Mat;

    aget-object v0, v0, v14

    .line 324
    :goto_5
    invoke-virtual {v0}, Lorg/opencv/core/Mat;->clone()Lorg/opencv/core/Mat;

    move-result-object v0

    .line 326
    :goto_6
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v1

    if-ge v8, v1, :cond_10

    .line 327
    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;

    .line 329
    invoke-virtual {v1}, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;->Size()I

    move-result v2

    if-lez v2, :cond_f

    .line 335
    invoke-direct {v6, v0, v1}, Lepson/colorcorrection/ImageCollect;->MakeImageBCSC(Lorg/opencv/core/Mat;Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;)Lorg/opencv/core/Mat;

    move-result-object v0

    .line 342
    invoke-virtual {v1}, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;->GetAfterPWC()I

    move-result v1

    if-lez v1, :cond_f

    .line 347
    new-instance v2, Lepson/colorcorrection/SDIC_MAT_PTR;

    invoke-direct {v2}, Lepson/colorcorrection/SDIC_MAT_PTR;-><init>()V

    if-ne v1, v14, :cond_d

    .line 350
    invoke-static {v0, v2}, Lepson/colorcorrection/SDIC_PWC;->PaperWhiteCorrection(Lorg/opencv/core/Mat;Lepson/colorcorrection/SDIC_MAT_PTR;)I

    move-result v1

    goto :goto_7

    .line 352
    :cond_d
    invoke-static {v0, v2}, Lepson/colorcorrection/SDIC_PWC;->PaperWhiteCorrection2(Lorg/opencv/core/Mat;Lepson/colorcorrection/SDIC_MAT_PTR;)I

    move-result v1

    :goto_7
    if-nez v1, :cond_f

    if-eqz v0, :cond_e

    .line 357
    invoke-virtual {v0}, Lorg/opencv/core/Mat;->release()V

    .line 360
    :cond_e
    invoke-virtual {v2}, Lepson/colorcorrection/SDIC_MAT_PTR;->getMat()Lorg/opencv/core/Mat;

    move-result-object v0

    :cond_f
    add-int/lit8 v8, v8, 0x1

    goto :goto_6

    .line 366
    :cond_10
    iget-object v1, v6, Lepson/colorcorrection/ImageCollect;->mMatPrint:[Lorg/opencv/core/Mat;

    const/4 v2, 0x4

    aput-object v0, v1, v2

    .line 368
    aget-object v0, v1, v2

    return-object v0

    :cond_11
    return-object v9

    :cond_12
    return-object v9
.end method

.method public MakePreviewBCSC(Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;)V
    .locals 4

    .line 824
    iget-object v0, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    invoke-virtual {p1}, Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;->GetImageType()I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lorg/opencv/core/Mat;->clone()Lorg/opencv/core/Mat;

    move-result-object v0

    .line 825
    iget-object v1, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    const/4 v2, 0x4

    aget-object v3, v1, v2

    if-eqz v3, :cond_0

    .line 826
    aget-object v1, v1, v2

    invoke-virtual {v1}, Lorg/opencv/core/Mat;->release()V

    .line 827
    iget-object v1, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    const/4 v3, 0x0

    aput-object v3, v1, v2

    .line 831
    :cond_0
    iget-object v1, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    invoke-direct {p0, v0, p1}, Lepson/colorcorrection/ImageCollect;->MakeImageBCSC(Lorg/opencv/core/Mat;Lcom/epson/cameracopy/ui/CropImageActivity$CorrectionParamSet;)Lorg/opencv/core/Mat;

    move-result-object p1

    aput-object p1, v1, v2

    return-void
.end method

.method public MakePreviewImageBCSC_Exec(IZFFFF)V
    .locals 20

    move-object/from16 v0, p0

    move/from16 v1, p3

    move/from16 v2, p4

    move/from16 v3, p5

    .line 965
    iget-object v4, v0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    const/4 v5, 0x5

    aget-object v6, v4, v5

    if-eqz v6, :cond_8

    const/4 v6, 0x6

    .line 967
    aget-object v7, v4, v6

    const/4 v8, 0x0

    if-eqz v7, :cond_0

    .line 968
    aget-object v4, v4, v6

    invoke-virtual {v4}, Lorg/opencv/core/Mat;->release()V

    .line 969
    iget-object v4, v0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    aput-object v8, v4, v6

    :cond_0
    const/4 v4, 0x4

    const/16 v7, 0x8

    move/from16 v9, p1

    if-ne v9, v7, :cond_6

    .line 974
    iget-object v9, v0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    aget-object v10, v9, v7

    if-eqz v10, :cond_8

    const/16 v10, 0x9

    if-eqz p2, :cond_1

    .line 982
    aget-object v9, v9, v10

    if-nez v9, :cond_3

    .line 983
    invoke-direct {v0, v7}, Lepson/colorcorrection/ImageCollect;->ResizeMatSmallPreviewSize(I)Lorg/opencv/core/Mat;

    move-result-object v8

    goto :goto_0

    .line 987
    :cond_1
    aget-object v11, v9, v10

    if-eqz v11, :cond_2

    .line 988
    aget-object v9, v9, v10

    invoke-virtual {v9}, Lorg/opencv/core/Mat;->release()V

    .line 989
    iget-object v9, v0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    aput-object v8, v9, v10

    .line 991
    :cond_2
    iget-object v8, v0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    aget-object v7, v8, v7

    invoke-virtual {v7}, Lorg/opencv/core/Mat;->clone()Lorg/opencv/core/Mat;

    move-result-object v8

    :cond_3
    :goto_0
    if-eqz v8, :cond_4

    const/high16 v7, 0x41400000    # 12.0f

    .line 1001
    iget v9, v0, Lepson/colorcorrection/ImageCollect;->mDensity:F

    div-float/2addr v7, v9

    .line 1003
    new-instance v9, Lepson/colorcorrection/SDIC_MAT_PTR;

    invoke-direct {v9}, Lepson/colorcorrection/SDIC_MAT_PTR;-><init>()V

    float-to-int v7, v7

    .line 1004
    invoke-static {v8, v9, v7}, Lepson/colorcorrection/SDIC_BCSC;->MakeMask(Lorg/opencv/core/Mat;Lepson/colorcorrection/SDIC_MAT_PTR;I)I

    .line 1005
    invoke-virtual {v8}, Lorg/opencv/core/Mat;->release()V

    .line 1007
    iget-object v7, v0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    invoke-virtual {v9}, Lepson/colorcorrection/SDIC_MAT_PTR;->getMat()Lorg/opencv/core/Mat;

    move-result-object v8

    aput-object v8, v7, v10

    .line 1014
    :cond_4
    new-instance v7, Lepson/colorcorrection/SDIC_MAT_PTR;

    invoke-direct {v7}, Lepson/colorcorrection/SDIC_MAT_PTR;-><init>()V

    .line 1015
    iget-object v8, v0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    aget-object v11, v8, v5

    aget-object v12, v8, v10

    float-to-double v14, v1

    float-to-double v1, v2

    float-to-double v8, v3

    move-object v13, v7

    move-wide/from16 v16, v1

    move-wide/from16 v18, v8

    invoke-static/range {v11 .. v19}, Lepson/colorcorrection/SDIC_BCSC;->BCSCorrection(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lepson/colorcorrection/SDIC_MAT_PTR;DDD)I

    move-result v1

    if-nez v1, :cond_8

    .line 1017
    iget-object v1, v0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    if-eqz p2, :cond_5

    const/4 v4, 0x6

    :cond_5
    invoke-virtual {v7}, Lepson/colorcorrection/SDIC_MAT_PTR;->getMat()Lorg/opencv/core/Mat;

    move-result-object v2

    aput-object v2, v1, v4

    goto :goto_1

    .line 1026
    :cond_6
    new-instance v16, Lepson/colorcorrection/SDIC_MAT_PTR;

    invoke-direct/range {v16 .. v16}, Lepson/colorcorrection/SDIC_MAT_PTR;-><init>()V

    .line 1027
    iget-object v7, v0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    aget-object v5, v7, v5

    const/4 v8, 0x7

    aget-object v8, v7, v8

    float-to-double v10, v1

    float-to-double v12, v2

    float-to-double v14, v3

    move-object v7, v5

    move-object/from16 v9, v16

    invoke-static/range {v7 .. v15}, Lepson/colorcorrection/SDIC_BCSC;->BCSCorrection(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lepson/colorcorrection/SDIC_MAT_PTR;DDD)I

    move-result v1

    if-nez v1, :cond_8

    .line 1029
    iget-object v1, v0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    if-eqz p2, :cond_7

    const/4 v4, 0x6

    :cond_7
    invoke-virtual/range {v16 .. v16}, Lepson/colorcorrection/SDIC_MAT_PTR;->getMat()Lorg/opencv/core/Mat;

    move-result-object v2

    aput-object v2, v1, v4

    :cond_8
    :goto_1
    return-void
.end method

.method public MakePreviewImageBCSC_Init(IZ)V
    .locals 2

    if-eqz p2, :cond_0

    .line 954
    iget-object v0, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    const/4 v1, 0x5

    aget-object v0, v0, v1

    if-nez v0, :cond_1

    .line 955
    invoke-direct {p0, p1, p2}, Lepson/colorcorrection/ImageCollect;->MakePreviewImageBCSC_MakeSRC(IZ)V

    goto :goto_0

    .line 959
    :cond_0
    invoke-direct {p0, p1, p2}, Lepson/colorcorrection/ImageCollect;->MakePreviewImageBCSC_MakeSRC(IZ)V

    :cond_1
    :goto_0
    return-void
.end method

.method public MakePreviewImageCROP([Landroid/graphics/PointF;Lorg/opencv/core/Size;Lorg/opencv/core/Size;)V
    .locals 9

    .line 731
    iget-object v0, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    const/4 v1, 0x0

    aget-object v2, v0, v1

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    .line 732
    aget-object v3, v0, v2

    if-eqz v3, :cond_0

    .line 733
    aget-object v0, v0, v2

    invoke-virtual {v0}, Lorg/opencv/core/Mat;->release()V

    .line 734
    iget-object v0, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    const/4 v3, 0x0

    aput-object v3, v0, v2

    .line 736
    :cond_0
    iget-object v0, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    aget-object v4, v0, v1

    const/high16 v6, 0x3f800000    # 1.0f

    move-object v3, p0

    move-object v5, p1

    move-object v7, p2

    move-object v8, p3

    invoke-direct/range {v3 .. v8}, Lepson/colorcorrection/ImageCollect;->MakeCropImage(Lorg/opencv/core/Mat;[Landroid/graphics/PointF;FLorg/opencv/core/Size;Lorg/opencv/core/Size;)Lorg/opencv/core/Mat;

    move-result-object p1

    aput-object p1, v0, v2

    :cond_1
    return-void
.end method

.method public MakePreviewImagePWC(I)V
    .locals 4

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v1, 0x2

    if-ne p1, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, -0x1

    if-ne p1, v2, :cond_3

    .line 747
    iget-object p1, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    aget-object v2, p1, v0

    const/4 v3, 0x3

    if-eqz v2, :cond_1

    .line 749
    invoke-direct {p0, v0, v1, v0}, Lepson/colorcorrection/ImageCollect;->ExecPwc(III)V

    .line 750
    invoke-direct {p0, v0, v3, v1}, Lepson/colorcorrection/ImageCollect;->ExecPwc(III)V

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    .line 751
    aget-object p1, p1, v2

    if-eqz p1, :cond_3

    .line 753
    invoke-direct {p0, v2, v1, v0}, Lepson/colorcorrection/ImageCollect;->ExecPwc(III)V

    .line 754
    invoke-direct {p0, v2, v3, v1}, Lepson/colorcorrection/ImageCollect;->ExecPwc(III)V

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v0, 0x4

    .line 744
    invoke-direct {p0, v0, v0, p1}, Lepson/colorcorrection/ImageCollect;->ExecPwc(III)V

    :cond_3
    :goto_1
    return-void
.end method

.method public MakePreviewImageRAW(DD)V
    .locals 9

    .line 719
    iget-object v0, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    if-nez v0, :cond_0

    .line 721
    invoke-direct {p0}, Lepson/colorcorrection/ImageCollect;->GetMat()Lorg/opencv/core/Mat;

    .line 723
    iget-object v0, p0, Lepson/colorcorrection/ImageCollect;->mMatPrint:[Lorg/opencv/core/Mat;

    aget-object v2, v0, v1

    if-eqz v2, :cond_0

    .line 724
    iget-object v2, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    aget-object v4, v0, v1

    move-object v3, p0

    move-wide v5, p1

    move-wide v7, p3

    invoke-direct/range {v3 .. v8}, Lepson/colorcorrection/ImageCollect;->ResizeMat(Lorg/opencv/core/Mat;DD)Lorg/opencv/core/Mat;

    move-result-object p1

    aput-object p1, v2, v1

    :cond_0
    return-void
.end method

.method public ReleaseImageCollect()V
    .locals 5

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    const/16 v2, 0xa

    const/4 v3, 0x0

    if-ge v1, v2, :cond_1

    .line 1394
    iget-object v2, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    aget-object v4, v2, v1

    if-eqz v4, :cond_0

    .line 1395
    aget-object v2, v2, v1

    invoke-virtual {v2}, Lorg/opencv/core/Mat;->release()V

    .line 1396
    iget-object v2, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    aput-object v3, v2, v1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    const/4 v1, 0x5

    if-ge v0, v1, :cond_3

    .line 1400
    iget-object v1, p0, Lepson/colorcorrection/ImageCollect;->mMatPrint:[Lorg/opencv/core/Mat;

    aget-object v2, v1, v0

    if-eqz v2, :cond_2

    .line 1401
    aget-object v1, v1, v0

    invoke-virtual {v1}, Lorg/opencv/core/Mat;->release()V

    .line 1402
    iget-object v1, p0, Lepson/colorcorrection/ImageCollect;->mMatPrint:[Lorg/opencv/core/Mat;

    aput-object v3, v1, v0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    return-void
.end method

.method public ResetPreviewImageBCSC()V
    .locals 4

    .line 788
    iget-object v0, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    const/4 v1, 0x5

    aget-object v2, v0, v1

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    .line 789
    aget-object v0, v0, v1

    invoke-virtual {v0}, Lorg/opencv/core/Mat;->release()V

    .line 790
    iget-object v0, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    aput-object v3, v0, v1

    .line 792
    :cond_0
    iget-object v0, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    const/4 v1, 0x6

    aget-object v2, v0, v1

    if-eqz v2, :cond_1

    .line 793
    aget-object v0, v0, v1

    invoke-virtual {v0}, Lorg/opencv/core/Mat;->release()V

    .line 794
    iget-object v0, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    aput-object v3, v0, v1

    .line 796
    :cond_1
    iget-object v0, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    const/16 v1, 0x8

    aget-object v2, v0, v1

    if-eqz v2, :cond_2

    .line 797
    aget-object v0, v0, v1

    invoke-virtual {v0}, Lorg/opencv/core/Mat;->release()V

    .line 798
    iget-object v0, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    aput-object v3, v0, v1

    :cond_2
    return-void
.end method

.method public SaveExecute(ZILorg/opencv/core/Size;)Z
    .locals 9

    const/4 v0, 0x4

    const/4 v1, 0x3

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x0

    if-ne p2, v0, :cond_0

    .line 379
    iget-object p2, p0, Lepson/colorcorrection/ImageCollect;->mMatPrint:[Lorg/opencv/core/Mat;

    aget-object p2, p2, v0

    goto :goto_0

    :cond_0
    if-ne p2, v3, :cond_1

    .line 381
    iget-object p2, p0, Lepson/colorcorrection/ImageCollect;->mMatPrint:[Lorg/opencv/core/Mat;

    aget-object p2, p2, v3

    goto :goto_0

    :cond_1
    if-ne p2, v1, :cond_2

    .line 383
    iget-object p2, p0, Lepson/colorcorrection/ImageCollect;->mMatPrint:[Lorg/opencv/core/Mat;

    aget-object p2, p2, v1

    goto :goto_0

    :cond_2
    if-ne p2, v2, :cond_3

    .line 385
    iget-object p2, p0, Lepson/colorcorrection/ImageCollect;->mMatPrint:[Lorg/opencv/core/Mat;

    aget-object p2, p2, v2

    goto :goto_0

    .line 387
    :cond_3
    iget-object p2, p0, Lepson/colorcorrection/ImageCollect;->mMatPrint:[Lorg/opencv/core/Mat;

    aget-object p2, p2, v4

    :goto_0
    const/4 v0, 0x0

    .line 390
    iput-object v0, p0, Lepson/colorcorrection/ImageCollect;->mSaveDisplayPath:Ljava/lang/String;

    iput-object v0, p0, Lepson/colorcorrection/ImageCollect;->mSaveFullPath:Ljava/lang/String;

    .line 392
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    const-string v5, "yyyyMMddkkmm_"

    .line 393
    invoke-static {v5, v0}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz p1, :cond_4

    .line 396
    new-instance v5, Ljava/io/File;

    iget-object v6, p0, Lepson/colorcorrection/ImageCollect;->mCacheSaveName:Ljava/lang/String;

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    new-instance v5, Ljava/io/File;

    iget-object v6, p0, Lepson/colorcorrection/ImageCollect;->mUserSaveName:Ljava/lang/String;

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :goto_1
    invoke-direct {p0, v5}, Lepson/colorcorrection/ImageCollect;->ensureDirectory(Ljava/io/File;)Z

    move-result v5

    if-eqz v5, :cond_5

    return v4

    :cond_5
    const/4 v5, 0x0

    :goto_2
    const/16 v6, 0x64

    if-ge v5, v6, :cond_9

    const-string v6, "%s%02d.jpg"

    .line 403
    new-array v7, v3, [Ljava/lang/Object;

    aput-object v0, v7, v4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v2

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    if-eqz p1, :cond_6

    .line 407
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lepson/colorcorrection/ImageCollect;->mCacheSaveName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "icpd_"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_3

    .line 409
    :cond_6
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lepson/colorcorrection/ImageCollect;->mUserSaveName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 414
    :goto_3
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 415
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_8

    .line 417
    new-array v0, v3, [I

    fill-array-data v0, :array_0

    .line 418
    new-instance v2, Lorg/opencv/core/MatOfInt;

    new-array v1, v1, [I

    fill-array-data v1, :array_1

    invoke-direct {v2, v1}, Lorg/opencv/core/MatOfInt;-><init>([I)V

    .line 419
    invoke-virtual {v2, v4, v4, v0}, Lorg/opencv/core/MatOfInt;->put(II[I)I

    if-eqz p3, :cond_7

    .line 422
    iget-wide v0, p3, Lorg/opencv/core/Size;->width:D

    const-wide/16 v3, 0x0

    cmpl-double v5, v0, v3

    if-lez v5, :cond_7

    iget-wide v0, p3, Lorg/opencv/core/Size;->height:D

    cmpl-double p3, v0, v3

    if-lez p3, :cond_7

    .line 434
    invoke-static {v6, p2, v2}, Lorg/opencv/imgcodecs/Imgcodecs;->imwrite(Ljava/lang/String;Lorg/opencv/core/Mat;Lorg/opencv/core/MatOfInt;)Z

    move-result p2

    move v4, p2

    goto :goto_4

    .line 437
    :cond_7
    invoke-static {v6, p2, v2}, Lorg/opencv/imgcodecs/Imgcodecs;->imwrite(Ljava/lang/String;Lorg/opencv/core/Mat;Lorg/opencv/core/MatOfInt;)Z

    move-result p2

    move v4, p2

    .line 440
    :goto_4
    invoke-virtual {v2}, Lorg/opencv/core/MatOfInt;->release()V

    if-eqz v4, :cond_9

    .line 444
    iput-object v6, p0, Lepson/colorcorrection/ImageCollect;->mSaveFullPath:Ljava/lang/String;

    if-nez p1, :cond_9

    .line 448
    invoke-virtual {v7}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object p1

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/colorcorrection/ImageCollect;->mSaveDisplayPath:Ljava/lang/String;

    goto :goto_5

    :cond_8
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_2

    :cond_9
    :goto_5
    return v4

    nop

    :array_0
    .array-data 4
        0x1
        0x5a
    .end array-data

    :array_1
    .array-data 4
        0x2
        0x1
        0x4
    .end array-data
.end method

.method public SetPath(Ljava/io/File;Ljava/io/File;Ljava/lang/String;)V
    .locals 2

    if-eqz p3, :cond_1

    .line 147
    invoke-virtual {p3}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 148
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "/EpsonCameraCapture"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 149
    invoke-virtual {p3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 150
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 151
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    const-string p3, "/"

    .line 152
    invoke-virtual {v1, v0, p3}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    iput-object p3, p0, Lepson/colorcorrection/ImageCollect;->mUserSelectedFolder:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const-string p3, "/EpsonCameraCapture"

    .line 154
    iput-object p3, p0, Lepson/colorcorrection/ImageCollect;->mUserSelectedFolder:Ljava/lang/String;

    goto :goto_0

    :cond_1
    const-string p3, "/EpsonCameraCapture"

    .line 157
    iput-object p3, p0, Lepson/colorcorrection/ImageCollect;->mUserSelectedFolder:Ljava/lang/String;

    .line 160
    :goto_0
    iput-object p1, p0, Lepson/colorcorrection/ImageCollect;->mCacheFolder:Ljava/io/File;

    .line 161
    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/colorcorrection/ImageCollect;->mCacheSaveName:Ljava/lang/String;

    .line 162
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p2, p0, Lepson/colorcorrection/ImageCollect;->mUserSelectedFolder:Ljava/lang/String;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/colorcorrection/ImageCollect;->mUserSaveName:Ljava/lang/String;

    .line 163
    new-instance p1, Ljava/io/File;

    iget-object p2, p0, Lepson/colorcorrection/ImageCollect;->mUserSaveName:Ljava/lang/String;

    invoke-direct {p1, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lepson/colorcorrection/ImageCollect;->mUserFolder:Ljava/io/File;

    .line 166
    iget-object p1, p0, Lepson/colorcorrection/ImageCollect;->mCacheFolder:Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result p1

    if-nez p1, :cond_2

    .line 167
    iget-object p1, p0, Lepson/colorcorrection/ImageCollect;->mCacheFolder:Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->mkdir()Z

    .line 169
    :cond_2
    iget-object p1, p0, Lepson/colorcorrection/ImageCollect;->mUserFolder:Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result p1

    if-nez p1, :cond_3

    .line 170
    iget-object p1, p0, Lepson/colorcorrection/ImageCollect;->mUserFolder:Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->mkdir()Z

    :cond_3
    return-void
.end method

.method public SetResolutionThreshold(D)V
    .locals 0

    .line 138
    iput-wide p1, p0, Lepson/colorcorrection/ImageCollect;->mResolutionThreshold:D

    return-void
.end method

.method public SetSmallPreviewRate(F)V
    .locals 0

    .line 142
    iput p1, p0, Lepson/colorcorrection/ImageCollect;->mSmallPreviewRate:F

    return-void
.end method

.method public SiwtchPreviewImageBCSC(I)V
    .locals 5

    .line 805
    iget-object v0, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    const/4 v1, 0x6

    aget-object v2, v0, v1

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz v2, :cond_1

    const/4 p1, 0x5

    .line 806
    aget-object v2, v0, p1

    if-eqz v2, :cond_0

    .line 807
    aget-object v0, v0, p1

    invoke-virtual {v0}, Lorg/opencv/core/Mat;->release()V

    .line 808
    iget-object v0, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    aput-object v3, v0, p1

    .line 810
    :cond_0
    invoke-virtual {p0, v1, v4}, Lepson/colorcorrection/ImageCollect;->MakePreviewImageBCSC_Init(IZ)V

    goto :goto_0

    .line 812
    :cond_1
    invoke-virtual {p0, p1, v4}, Lepson/colorcorrection/ImageCollect;->MakePreviewImageBCSC_Init(IZ)V

    .line 816
    :goto_0
    iget-object p1, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    const/16 v0, 0x8

    aget-object v1, p1, v0

    if-eqz v1, :cond_2

    .line 817
    aget-object p1, p1, v0

    invoke-virtual {p1}, Lorg/opencv/core/Mat;->release()V

    .line 818
    iget-object p1, p0, Lepson/colorcorrection/ImageCollect;->mMatPreview:[Lorg/opencv/core/Mat;

    aput-object v3, p1, v0

    :cond_2
    return-void
.end method

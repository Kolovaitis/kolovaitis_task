.class public Lepson/colorcorrection/SDIC_BCSC;
.super Lepson/colorcorrection/SDIC_Exception;
.source "SDIC_BCSC.java"

# interfaces
.implements Lepson/colorcorrection/SDIC_CommonDefine;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "opencv_java3"

    .line 24
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 21
    invoke-direct {p0}, Lepson/colorcorrection/SDIC_Exception;-><init>()V

    return-void
.end method

.method public static BCSCorrection(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lepson/colorcorrection/SDIC_MAT_PTR;DDD)I
    .locals 12

    .line 31
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->getNativeObjAddr()J

    move-result-wide v0

    .line 32
    invoke-virtual {p1}, Lorg/opencv/core/Mat;->getNativeObjAddr()J

    move-result-wide v2

    .line 33
    invoke-virtual {p2}, Lepson/colorcorrection/SDIC_MAT_PTR;->getMat()Lorg/opencv/core/Mat;

    move-result-object v4

    invoke-virtual {v4}, Lorg/opencv/core/Mat;->getNativeObjAddr()J

    move-result-wide v4

    move-wide v6, p3

    move-wide/from16 v8, p5

    move-wide/from16 v10, p7

    .line 31
    invoke-static/range {v0 .. v11}, Lcom/epson/cameracopy/device/RectangleDetector;->bcsCorrection(JJJDDD)I

    move-result v0

    return v0
.end method

.method public static BCSCorrectionJAVA(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lepson/colorcorrection/SDIC_MAT_PTR;DDD)I
    .locals 25

    .line 43
    invoke-virtual/range {p0 .. p0}, Lorg/opencv/core/Mat;->cols()I

    move-result v0

    .line 44
    invoke-virtual/range {p0 .. p0}, Lorg/opencv/core/Mat;->rows()I

    move-result v1

    .line 45
    invoke-virtual/range {p0 .. p0}, Lorg/opencv/core/Mat;->channels()I

    move-result v2

    .line 46
    invoke-virtual/range {p0 .. p0}, Lorg/opencv/core/Mat;->type()I

    move-result v3

    .line 47
    new-instance v4, Lorg/opencv/core/Mat;

    invoke-direct {v4, v1, v0, v3}, Lorg/opencv/core/Mat;-><init>(III)V

    .line 50
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v3

    .line 51
    invoke-virtual {v3}, Ljava/lang/Runtime;->maxMemory()J

    .line 52
    invoke-virtual {v3}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v5

    long-to-float v5, v5

    const/high16 v6, 0x49800000    # 1048576.0f

    div-float/2addr v5, v6

    .line 53
    invoke-virtual {v3}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v7

    long-to-float v3, v7

    div-float/2addr v3, v6

    sub-float v3, v5, v3

    sub-float/2addr v5, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v5, v3

    const/high16 v7, 0x40600000    # 3.5f

    div-float/2addr v5, v7

    cmpl-float v7, v5, v3

    if-lez v7, :cond_0

    move v3, v5

    :cond_0
    mul-float v5, v3, v6

    mul-int v7, v0, v2

    int-to-float v7, v7

    div-float/2addr v5, v7

    float-to-int v5, v5

    .line 62
    invoke-virtual/range {p0 .. p0}, Lorg/opencv/core/Mat;->rows()I

    move-result v7

    if-ge v5, v7, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual/range {p0 .. p0}, Lorg/opencv/core/Mat;->rows()I

    move-result v5

    .line 64
    :goto_0
    new-instance v7, Lorg/opencv/core/Rect;

    const/4 v8, 0x0

    invoke-direct {v7, v8, v8, v0, v5}, Lorg/opencv/core/Rect;-><init>(IIII)V

    int-to-double v9, v1

    int-to-double v11, v5

    div-double/2addr v9, v11

    .line 66
    invoke-static {v9, v10}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v9

    double-to-int v9, v9

    const-string v10, "BCSC S - [%d] w:%d h:%d c:%d m:%.2fMB use:%.2fMB"

    const/4 v11, 0x6

    .line 68
    new-array v11, v11, [Ljava/lang/Object;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v11, v8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    const/4 v13, 0x1

    aput-object v12, v11, v13

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    const/4 v14, 0x2

    aput-object v12, v11, v14

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    const/4 v15, 0x3

    aput-object v12, v11, v15

    const/4 v12, 0x4

    mul-int v0, v0, v1

    mul-int v0, v0, v2

    int-to-float v0, v0

    div-float/2addr v0, v6

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    aput-object v0, v11, v12

    const/4 v0, 0x5

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v11, v0

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lepson/colorcorrection/SDIC_BCSC;->LogMemory(Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v2, 0x0

    :goto_1
    if-lez v1, :cond_4

    if-ge v1, v5, :cond_2

    move v2, v1

    goto :goto_2

    :cond_2
    move v2, v5

    .line 72
    :goto_2
    iput v2, v7, Lorg/opencv/core/Rect;->height:I

    move-object/from16 v6, p0

    .line 73
    invoke-virtual {v6, v7}, Lorg/opencv/core/Mat;->submat(Lorg/opencv/core/Rect;)Lorg/opencv/core/Mat;

    move-result-object v2

    move-object/from16 v10, p1

    .line 74
    invoke-virtual {v10, v7}, Lorg/opencv/core/Mat;->submat(Lorg/opencv/core/Rect;)Lorg/opencv/core/Mat;

    move-result-object v11

    .line 75
    invoke-virtual {v4, v7}, Lorg/opencv/core/Mat;->submat(Lorg/opencv/core/Rect;)Lorg/opencv/core/Mat;

    move-result-object v12

    move-object/from16 v16, v2

    move-object/from16 v17, v11

    move-object/from16 v18, v12

    move-wide/from16 v19, p3

    move-wide/from16 v21, p5

    move-wide/from16 v23, p7

    .line 77
    invoke-static/range {v16 .. v24}, Lepson/colorcorrection/SDIC_BCSC;->BCSCorrection_ROI(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;DDD)I

    move-result v16

    .line 78
    invoke-virtual {v2}, Lorg/opencv/core/Mat;->release()V

    .line 79
    invoke-virtual {v11}, Lorg/opencv/core/Mat;->release()V

    .line 80
    invoke-virtual {v12}, Lorg/opencv/core/Mat;->release()V

    const-string v2, "BCSC R - [%d/%d] %d"

    .line 82
    new-array v11, v15, [Ljava/lang/Object;

    add-int/2addr v0, v13

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v11, v8

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v11, v13

    iget v12, v7, Lorg/opencv/core/Rect;->height:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v11, v14

    invoke-static {v2, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lepson/colorcorrection/SDIC_BCSC;->LogMemory(Ljava/lang/String;)V

    if-eqz v16, :cond_3

    goto :goto_3

    :cond_3
    sub-int/2addr v1, v5

    .line 70
    iget v2, v7, Lorg/opencv/core/Rect;->y:I

    add-int/2addr v2, v5

    iput v2, v7, Lorg/opencv/core/Rect;->y:I

    move/from16 v2, v16

    goto :goto_1

    :cond_4
    move/from16 v16, v2

    :goto_3
    if-nez v16, :cond_5

    move-object/from16 v0, p2

    .line 90
    invoke-virtual {v0, v4}, Lepson/colorcorrection/SDIC_MAT_PTR;->setMat(Lorg/opencv/core/Mat;)V

    :cond_5
    const-string v0, "BCSC E use:%.2fMB"

    .line 93
    new-array v1, v13, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v1, v8

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lepson/colorcorrection/SDIC_BCSC;->LogMemory(Ljava/lang/String;)V

    return v16
.end method

.method public static BCSCorrection_ROI(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;DDD)I
    .locals 7

    const/4 v0, -0x1

    const/4 v1, 0x0

    .line 107
    :try_start_0
    invoke-static {p0, p1}, Lepson/colorcorrection/SDIC_BCSC;->CheckImage(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)I

    move-result v1

    if-nez v1, :cond_4

    .line 111
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->dataAddr()J

    move-result-wide v2

    invoke-virtual {p2}, Lorg/opencv/core/Mat;->dataAddr()J

    move-result-wide v4

    cmp-long v6, v2, v4

    if-eqz v6, :cond_3

    .line 114
    invoke-virtual {p2}, Lorg/opencv/core/Mat;->empty()Z

    .line 121
    invoke-static {p0, p1, p2, p5, p6}, Lepson/colorcorrection/SDIC_BCSC;->Contrast(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;D)I

    move-result v1

    if-nez v1, :cond_2

    .line 127
    invoke-virtual {p2}, Lorg/opencv/core/Mat;->clone()Lorg/opencv/core/Mat;

    move-result-object p0

    invoke-static {p0, p1, p2, p7, p8}, Lepson/colorcorrection/SDIC_BCSC;->Saturation(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;D)I

    move-result v1

    if-nez v1, :cond_1

    .line 133
    invoke-virtual {p2}, Lorg/opencv/core/Mat;->clone()Lorg/opencv/core/Mat;

    move-result-object p0

    invoke-static {p0, p1, p2, p3, p4}, Lepson/colorcorrection/SDIC_BCSC;->Brightness(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;D)I

    move-result v1
    :try_end_0
    .catch Lepson/colorcorrection/SDIC_Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lorg/opencv/core/CvException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    if-eqz v1, :cond_5

    .line 147
    invoke-virtual {p2}, Lorg/opencv/core/Mat;->empty()Z

    move-result p0

    if-nez p0, :cond_5

    if-eq v1, v0, :cond_5

    .line 148
    :goto_0
    invoke-virtual {p2}, Lorg/opencv/core/Mat;->release()V

    goto :goto_1

    .line 135
    :cond_0
    :try_start_1
    new-instance p0, Lepson/colorcorrection/SDIC_Exception;

    invoke-direct {p0, v1}, Lepson/colorcorrection/SDIC_Exception;-><init>(I)V

    throw p0

    .line 129
    :cond_1
    new-instance p0, Lepson/colorcorrection/SDIC_Exception;

    invoke-direct {p0, v1}, Lepson/colorcorrection/SDIC_Exception;-><init>(I)V

    throw p0

    .line 123
    :cond_2
    new-instance p0, Lepson/colorcorrection/SDIC_Exception;

    invoke-direct {p0, v1}, Lepson/colorcorrection/SDIC_Exception;-><init>(I)V

    throw p0

    .line 112
    :cond_3
    new-instance p0, Lepson/colorcorrection/SDIC_Exception;

    invoke-direct {p0, v0}, Lepson/colorcorrection/SDIC_Exception;-><init>(I)V

    throw p0

    .line 109
    :cond_4
    new-instance p0, Lepson/colorcorrection/SDIC_Exception;

    invoke-direct {p0, v1}, Lepson/colorcorrection/SDIC_Exception;-><init>(I)V

    throw p0
    :try_end_1
    .catch Lepson/colorcorrection/SDIC_Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lorg/opencv/core/CvException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p0

    goto :goto_2

    :catch_0
    const/16 v1, -0x190

    .line 147
    invoke-virtual {p2}, Lorg/opencv/core/Mat;->empty()Z

    move-result p0

    if-nez p0, :cond_5

    goto :goto_0

    :catch_1
    const/16 v1, -0x64

    invoke-virtual {p2}, Lorg/opencv/core/Mat;->empty()Z

    move-result p0

    if-nez p0, :cond_5

    goto :goto_0

    :catch_2
    move-exception p0

    .line 139
    :try_start_2
    iget v1, p0, Lepson/colorcorrection/SDIC_Exception;->mCode:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v1, :cond_5

    .line 147
    invoke-virtual {p2}, Lorg/opencv/core/Mat;->empty()Z

    move-result p0

    if-nez p0, :cond_5

    if-eq v1, v0, :cond_5

    goto :goto_0

    :cond_5
    :goto_1
    return v1

    :goto_2
    if-eqz v1, :cond_6

    invoke-virtual {p2}, Lorg/opencv/core/Mat;->empty()Z

    move-result p1

    if-nez p1, :cond_6

    if-eq v1, v0, :cond_6

    .line 148
    invoke-virtual {p2}, Lorg/opencv/core/Mat;->release()V

    :cond_6
    throw p0
.end method

.method private static Brightness(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;D)I
    .locals 11

    const/4 v0, 0x0

    const-wide v1, 0x3f50624de0000000L    # 0.0010000000474974513

    cmpg-double v3, p3, v1

    if-gez v3, :cond_0

    const-wide v1, -0x40af9db220000000L    # -0.0010000000474974513

    cmpl-double v3, p3, v1

    if-lez v3, :cond_0

    return v0

    :cond_0
    const-wide/high16 v1, 0x3ff0000000000000L    # 1.0

    cmpl-double v3, p3, v1

    if-gtz v3, :cond_6

    const-wide/high16 v1, -0x4010000000000000L    # -1.0

    cmpg-double v3, p3, v1

    if-gez v3, :cond_1

    goto :goto_2

    .line 279
    :cond_1
    invoke-static {p1}, Lepson/colorcorrection/SDIC_BCSC;->GetPixcelData(Lorg/opencv/core/Mat;)[B

    move-result-object p1

    .line 280
    invoke-static {p0}, Lepson/colorcorrection/SDIC_BCSC;->GetPixcelData(Lorg/opencv/core/Mat;)[B

    move-result-object v1

    .line 281
    invoke-static {p0}, Lepson/colorcorrection/SDIC_BCSC;->GetPixcelData(Lorg/opencv/core/Mat;)[B

    move-result-object v2

    .line 283
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->channels()I

    move-result v3

    .line 284
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->total()J

    move-result-wide v4

    long-to-int p0, v4

    mul-int p0, p0, v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_0
    if-ge v4, p0, :cond_5

    .line 287
    aget-byte v6, p1, v5

    const/16 v7, 0xff

    and-int/2addr v6, v7

    if-lez v6, :cond_4

    int-to-double v8, v6

    mul-double v8, v8, p3

    double-to-int v6, v8

    const/4 v8, 0x0

    :goto_1
    const/4 v9, 0x3

    if-ge v8, v9, :cond_4

    add-int v9, v4, v8

    .line 292
    aget-byte v10, v1, v9

    and-int/2addr v10, v7

    add-int/2addr v10, v6

    if-le v10, v7, :cond_2

    const/16 v10, 0xff

    :cond_2
    if-gez v10, :cond_3

    const/4 v10, 0x0

    :cond_3
    int-to-byte v10, v10

    .line 295
    aput-byte v10, v2, v9

    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    :cond_4
    add-int/2addr v4, v3

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 305
    :cond_5
    invoke-virtual {p2, v0, v0, v2}, Lorg/opencv/core/Mat;->put(II[B)I

    return v0

    :cond_6
    :goto_2
    const/4 p0, -0x7

    return p0
.end method

.method private static CheckImage(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)I
    .locals 5

    .line 537
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->empty()Z

    move-result v0

    if-nez v0, :cond_7

    invoke-virtual {p1}, Lorg/opencv/core/Mat;->empty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_3

    .line 542
    :cond_0
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->isContinuous()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lorg/opencv/core/Mat;->isContinuous()Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_2

    .line 547
    :cond_1
    invoke-virtual {p1}, Lorg/opencv/core/Mat;->size()Lorg/opencv/core/Size;

    move-result-object v0

    iget-wide v0, v0, Lorg/opencv/core/Size;->width:D

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->size()Lorg/opencv/core/Size;

    move-result-object v2

    iget-wide v2, v2, Lorg/opencv/core/Size;->width:D

    cmpl-double v4, v0, v2

    if-nez v4, :cond_5

    invoke-virtual {p1}, Lorg/opencv/core/Mat;->size()Lorg/opencv/core/Size;

    move-result-object v0

    iget-wide v0, v0, Lorg/opencv/core/Size;->height:D

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->size()Lorg/opencv/core/Size;

    move-result-object v2

    iget-wide v2, v2, Lorg/opencv/core/Size;->height:D

    cmpl-double v4, v0, v2

    if-eqz v4, :cond_2

    goto :goto_1

    .line 552
    :cond_2
    invoke-virtual {p1}, Lorg/opencv/core/Mat;->channels()I

    move-result p1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_4

    .line 553
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->channels()I

    move-result p1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_3

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->channels()I

    move-result p0

    const/4 p1, 0x4

    if-eq p0, p1, :cond_3

    goto :goto_0

    :cond_3
    const/4 p0, 0x0

    return p0

    :cond_4
    :goto_0
    const/4 p0, -0x4

    return p0

    :cond_5
    :goto_1
    const/4 p0, -0x5

    return p0

    :cond_6
    :goto_2
    const/4 p0, -0x3

    return p0

    :cond_7
    :goto_3
    const/4 p0, -0x2

    return p0
.end method

.method private static Contrast(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;D)I
    .locals 19

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    cmpl-double v4, p3, v2

    if-gtz v4, :cond_b

    const-wide/16 v4, 0x0

    cmpg-double v6, p3, v4

    if-gez v6, :cond_0

    goto/16 :goto_7

    :cond_0
    const-wide v4, 0x3f50624de0000000L    # 0.0010000000474974513

    const/4 v6, 0x0

    cmpg-double v7, p3, v4

    if-gez v7, :cond_1

    .line 322
    invoke-virtual {v0, v1}, Lorg/opencv/core/Mat;->copyTo(Lorg/opencv/core/Mat;)V

    return v6

    .line 331
    :cond_1
    new-instance v4, Lorg/opencv/core/Mat;

    invoke-direct {v4}, Lorg/opencv/core/Mat;-><init>()V

    .line 332
    invoke-virtual/range {p0 .. p0}, Lorg/opencv/core/Mat;->channels()I

    move-result v5

    const/4 v7, 0x3

    if-ne v5, v7, :cond_2

    const/4 v5, 0x6

    .line 333
    invoke-static {v0, v4, v5}, Lorg/opencv/imgproc/Imgproc;->cvtColor(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V

    goto :goto_0

    .line 334
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lorg/opencv/core/Mat;->channels()I

    move-result v5

    const/4 v8, 0x4

    if-ne v5, v8, :cond_a

    const/16 v5, 0xa

    .line 335
    invoke-static {v0, v4, v5}, Lorg/opencv/imgproc/Imgproc;->cvtColor(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V

    .line 340
    :goto_0
    new-instance v5, Lorg/opencv/core/Mat;

    invoke-direct {v5}, Lorg/opencv/core/Mat;-><init>()V

    .line 341
    new-instance v8, Lorg/opencv/core/Mat;

    invoke-direct {v8}, Lorg/opencv/core/Mat;-><init>()V

    const/4 v9, 0x1

    .line 342
    invoke-static {v4, v5, v9, v9}, Lorg/opencv/core/Core;->reduce(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;II)V

    .line 343
    invoke-virtual {v4}, Lorg/opencv/core/Mat;->release()V

    .line 346
    invoke-static {v5, v8, v6, v9}, Lorg/opencv/core/Core;->reduce(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;II)V

    .line 347
    invoke-virtual {v5}, Lorg/opencv/core/Mat;->release()V

    .line 350
    new-instance v4, Lorg/opencv/core/MatOfByte;

    invoke-direct {v4, v8}, Lorg/opencv/core/MatOfByte;-><init>(Lorg/opencv/core/Mat;)V

    .line 351
    invoke-virtual {v4}, Lorg/opencv/core/MatOfByte;->toArray()[B

    move-result-object v5

    .line 352
    aget-byte v5, v5, v6

    const/16 v9, 0xff

    and-int/2addr v5, v9

    .line 353
    invoke-virtual {v8}, Lorg/opencv/core/Mat;->release()V

    .line 355
    invoke-virtual {v4}, Lorg/opencv/core/MatOfByte;->release()V

    const/16 v4, 0x100

    .line 359
    new-array v8, v4, [D

    .line 360
    new-array v10, v4, [I

    const-wide/high16 v11, 0x4034000000000000L    # 20.0

    mul-double v11, v11, p3

    const/4 v13, 0x0

    :goto_1
    const-wide v14, 0x406fe00000000000L    # 255.0

    if-ge v13, v4, :cond_3

    sub-int v7, v5, v13

    move/from16 v16, v5

    int-to-double v4, v7

    mul-double v4, v4, v11

    div-double/2addr v4, v14

    .line 366
    invoke-static {v4, v5}, Ljava/lang/Math;->exp(D)D

    move-result-wide v4

    add-double/2addr v4, v2

    div-double v4, v2, v4

    aput-wide v4, v8, v13

    add-int/lit8 v13, v13, 0x1

    move/from16 v5, v16

    const/16 v4, 0x100

    const/4 v7, 0x3

    goto :goto_1

    .line 369
    :cond_3
    aget-wide v2, v8, v6

    .line 370
    aget-wide v4, v8, v9

    sub-double/2addr v4, v2

    const/4 v7, 0x0

    const/16 v11, 0x100

    :goto_2
    if-ge v7, v11, :cond_4

    .line 373
    aget-wide v12, v8, v7

    sub-double/2addr v12, v2

    div-double/2addr v12, v4

    mul-double v12, v12, v14

    const-wide/high16 v16, 0x3fe0000000000000L    # 0.5

    add-double v12, v12, v16

    double-to-int v12, v12

    and-int/2addr v12, v9

    aput v12, v10, v7

    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 379
    :cond_4
    invoke-static/range {p1 .. p1}, Lepson/colorcorrection/SDIC_BCSC;->GetPixcelData(Lorg/opencv/core/Mat;)[B

    move-result-object v2

    .line 380
    invoke-static/range {p0 .. p0}, Lepson/colorcorrection/SDIC_BCSC;->GetPixcelData(Lorg/opencv/core/Mat;)[B

    move-result-object v3

    .line 381
    invoke-static/range {p0 .. p0}, Lepson/colorcorrection/SDIC_BCSC;->GetPixcelData(Lorg/opencv/core/Mat;)[B

    move-result-object v4

    .line 383
    invoke-virtual/range {p0 .. p0}, Lorg/opencv/core/Mat;->channels()I

    move-result v5

    .line 384
    invoke-virtual/range {p0 .. p0}, Lorg/opencv/core/Mat;->total()J

    move-result-wide v7

    long-to-int v0, v7

    mul-int v0, v0, v5

    const/4 v7, 0x0

    const/4 v8, 0x0

    :goto_3
    if-ge v7, v0, :cond_9

    .line 386
    aget-byte v11, v2, v8

    and-int/2addr v11, v9

    if-lez v11, :cond_8

    int-to-double v11, v11

    div-double/2addr v11, v14

    const/4 v13, 0x0

    :goto_4
    const/4 v14, 0x3

    if-ge v13, v14, :cond_7

    add-int v15, v7, v13

    .line 391
    aget-byte v14, v3, v15

    and-int/2addr v14, v9

    .line 395
    aget v16, v10, v14

    sub-int v6, v16, v14

    move-object/from16 v18, v10

    int-to-double v9, v14

    move-object/from16 p1, v2

    move-object v14, v3

    int-to-double v2, v6

    mul-double v2, v2, v11

    add-double/2addr v9, v2

    double-to-int v2, v9

    const/16 v3, 0xff

    if-le v2, v3, :cond_5

    const/16 v6, 0xff

    goto :goto_5

    :cond_5
    move v6, v2

    :goto_5
    if-gez v6, :cond_6

    const/4 v6, 0x0

    :cond_6
    int-to-byte v2, v6

    .line 399
    aput-byte v2, v4, v15

    add-int/lit8 v13, v13, 0x1

    move-object/from16 v2, p1

    move-object v3, v14

    move-object/from16 v10, v18

    const/4 v6, 0x0

    const/16 v9, 0xff

    goto :goto_4

    :cond_7
    move-object/from16 p1, v2

    move-object v14, v3

    move-object/from16 v18, v10

    const/16 v3, 0xff

    goto :goto_6

    :cond_8
    move-object/from16 p1, v2

    move-object v14, v3

    move-object/from16 v18, v10

    const/16 v3, 0xff

    :goto_6
    add-int/2addr v7, v5

    add-int/lit8 v8, v8, 0x1

    move-object/from16 v2, p1

    move-object v3, v14

    move-object/from16 v10, v18

    const/4 v6, 0x0

    const/16 v9, 0xff

    const-wide v14, 0x406fe00000000000L    # 255.0

    goto :goto_3

    :cond_9
    const/4 v2, 0x0

    .line 404
    invoke-virtual {v1, v2, v2, v4}, Lorg/opencv/core/Mat;->put(II[B)I

    return v2

    :cond_a
    const/4 v0, -0x4

    return v0

    :cond_b
    :goto_7
    const/4 v0, -0x7

    return v0
.end method

.method public static GetPixcelData(Lorg/opencv/core/Mat;)[B
    .locals 3

    .line 625
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->rows()I

    move-result v0

    .line 626
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->cols()I

    move-result v1

    .line 627
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->channels()I

    move-result v2

    mul-int v0, v0, v1

    mul-int v0, v0, v2

    .line 628
    new-array v0, v0, [B

    const/4 v1, 0x0

    .line 629
    invoke-virtual {p0, v1, v1, v0}, Lorg/opencv/core/Mat;->get(II[B)I

    return-object v0
.end method

.method private static LengthBw2Point(Lorg/opencv/core/Point;Lorg/opencv/core/Point;)D
    .locals 4

    .line 562
    iget-wide v0, p1, Lorg/opencv/core/Point;->x:D

    iget-wide v2, p0, Lorg/opencv/core/Point;->x:D

    sub-double/2addr v0, v2

    .line 563
    iget-wide v2, p1, Lorg/opencv/core/Point;->y:D

    iget-wide p0, p0, Lorg/opencv/core/Point;->y:D

    sub-double/2addr v2, p0

    mul-double v0, v0, v0

    mul-double v2, v2, v2

    add-double/2addr v0, v2

    .line 564
    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide p0

    return-wide p0
.end method

.method private static LogMemory(Ljava/lang/String;)V
    .locals 9

    .line 649
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    .line 650
    invoke-virtual {v0}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v1

    long-to-float v1, v1

    .line 651
    invoke-virtual {v0}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v2

    long-to-float v2, v2

    .line 652
    invoke-virtual {v0}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v3

    long-to-float v0, v3

    sub-float v3, v2, v0

    const-string v4, "LogMemory"

    const-string v5, "max:%.2fMB total:%.2fMB free:%.2fMB used:%.2fMB %s"

    const/4 v6, 0x5

    .line 654
    new-array v6, v6, [Ljava/lang/Object;

    const/high16 v7, 0x49800000    # 1048576.0f

    div-float/2addr v1, v7

    .line 656
    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    const/4 v8, 0x0

    aput-object v1, v6, v8

    div-float/2addr v2, v7

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v6, v2

    div-float/2addr v0, v7

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    const/4 v1, 0x2

    aput-object v0, v6, v1

    div-float/2addr v3, v7

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    const/4 v1, 0x3

    aput-object v0, v6, v1

    const/4 v0, 0x4

    aput-object p0, v6, v0

    .line 655
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    .line 654
    invoke-static {v4, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private static MakeBlurFilter(II)[B
    .locals 9

    mul-int v0, p1, p1

    .line 569
    new-array v0, v0, [B

    .line 570
    new-instance v1, Lorg/opencv/core/Point;

    int-to-double v2, p0

    invoke-direct {v1, v2, v3, v2, v3}, Lorg/opencv/core/Point;-><init>(DD)V

    const/4 p0, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, p1, :cond_2

    const/4 v3, 0x0

    :goto_1
    if-ge v3, p1, :cond_1

    .line 576
    new-instance v4, Lorg/opencv/core/Point;

    int-to-double v5, v3

    int-to-double v7, v2

    invoke-direct {v4, v5, v6, v7, v8}, Lorg/opencv/core/Point;-><init>(DD)V

    invoke-static {v4, v1}, Lepson/colorcorrection/SDIC_BCSC;->LengthBw2Point(Lorg/opencv/core/Point;Lorg/opencv/core/Point;)D

    move-result-wide v4

    int-to-double v6, p1

    cmpl-double v8, v4, v6

    if-lez v8, :cond_0

    move-wide v4, v6

    :cond_0
    mul-int v8, v2, p1

    add-int/2addr v8, v3

    div-double/2addr v4, v6

    const-wide v6, 0x406fe00000000000L    # 255.0

    mul-double v4, v4, v6

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    add-double/2addr v4, v6

    double-to-int v4, v4

    and-int/lit16 v4, v4, 0xff

    int-to-byte v4, v4

    .line 583
    aput-byte v4, v0, v8

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method public static MakeMask(Lorg/opencv/core/Mat;Lepson/colorcorrection/SDIC_MAT_PTR;I)I
    .locals 19

    move-object/from16 v1, p1

    move/from16 v0, p2

    .line 157
    new-instance v2, Lorg/opencv/core/Mat;

    invoke-direct {v2}, Lorg/opencv/core/Mat;-><init>()V

    .line 163
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lorg/opencv/core/Mat;->empty()Z

    move-result v5

    if-nez v5, :cond_b

    .line 165
    invoke-virtual/range {p0 .. p0}, Lorg/opencv/core/Mat;->dataAddr()J

    move-result-wide v5

    invoke-virtual {v2}, Lorg/opencv/core/Mat;->dataAddr()J

    move-result-wide v7

    cmp-long v9, v5, v7

    if-eqz v9, :cond_a

    .line 167
    invoke-virtual/range {p0 .. p0}, Lorg/opencv/core/Mat;->isContinuous()Z

    move-result v5

    if-eqz v5, :cond_9

    .line 169
    invoke-virtual/range {p0 .. p0}, Lorg/opencv/core/Mat;->channels()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_8

    .line 171
    invoke-virtual {v2}, Lorg/opencv/core/Mat;->empty()Z

    move-result v5

    if-eqz v5, :cond_7

    if-eqz v0, :cond_6

    mul-int/lit8 v5, v0, 0x2

    add-int/lit8 v7, v5, 0x1

    .line 180
    invoke-static {v0, v7}, Lepson/colorcorrection/SDIC_BCSC;->MakeBlurFilter(II)[B

    move-result-object v8

    .line 183
    invoke-virtual/range {p0 .. p0}, Lorg/opencv/core/Mat;->rows()I

    move-result v9

    add-int/2addr v9, v5

    invoke-virtual/range {p0 .. p0}, Lorg/opencv/core/Mat;->cols()I

    move-result v10

    add-int/2addr v10, v5

    sget v5, Lorg/opencv/core/CvType;->CV_8UC1:I

    invoke-static {v9, v10, v5}, Lorg/opencv/core/Mat;->zeros(III)Lorg/opencv/core/Mat;

    move-result-object v5

    .line 184
    new-instance v9, Lorg/opencv/core/Rect;

    invoke-virtual/range {p0 .. p0}, Lorg/opencv/core/Mat;->cols()I

    move-result v10

    invoke-virtual/range {p0 .. p0}, Lorg/opencv/core/Mat;->rows()I

    move-result v11

    invoke-direct {v9, v0, v0, v10, v11}, Lorg/opencv/core/Rect;-><init>(IIII)V

    .line 185
    invoke-virtual {v5, v9}, Lorg/opencv/core/Mat;->submat(Lorg/opencv/core/Rect;)Lorg/opencv/core/Mat;

    move-result-object v9

    move-object/from16 v10, p0

    .line 186
    invoke-virtual {v10, v9}, Lorg/opencv/core/Mat;->copyTo(Lorg/opencv/core/Mat;)V

    .line 191
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 192
    new-instance v12, Lorg/opencv/core/Mat;

    invoke-direct {v12}, Lorg/opencv/core/Mat;-><init>()V

    .line 193
    invoke-virtual {v5}, Lorg/opencv/core/Mat;->clone()Lorg/opencv/core/Mat;

    move-result-object v13

    .line 194
    invoke-static {v13, v11, v12, v6, v6}, Lorg/opencv/imgproc/Imgproc;->findContours(Lorg/opencv/core/Mat;Ljava/util/List;Lorg/opencv/core/Mat;II)V

    .line 195
    invoke-virtual {v13}, Lorg/opencv/core/Mat;->release()V

    .line 196
    invoke-virtual {v12}, Lorg/opencv/core/Mat;->release()V

    .line 199
    invoke-virtual/range {p0 .. p0}, Lorg/opencv/core/Mat;->size()Lorg/opencv/core/Size;

    move-result-object v12

    iget-wide v12, v12, Lorg/opencv/core/Size;->width:D

    double-to-int v12, v12

    add-int/2addr v12, v0

    sub-int/2addr v12, v6

    .line 200
    invoke-virtual/range {p0 .. p0}, Lorg/opencv/core/Mat;->size()Lorg/opencv/core/Size;

    move-result-object v10

    iget-wide v13, v10, Lorg/opencv/core/Size;->height:D

    double-to-int v10, v13

    add-int/2addr v10, v0

    sub-int/2addr v10, v6

    .line 202
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v6
    :try_end_0
    .catch Lepson/colorcorrection/SDIC_Exception; {:try_start_0 .. :try_end_0} :catch_9
    .catch Lorg/opencv/core/CvException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_5

    const/4 v13, 0x0

    :goto_0
    if-ge v13, v6, :cond_5

    .line 204
    :try_start_1
    invoke-interface {v11, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lorg/opencv/core/MatOfPoint;

    .line 205
    invoke-virtual {v14}, Lorg/opencv/core/MatOfPoint;->toList()Ljava/util/List;

    move-result-object v14

    .line 206
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v15

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v15, :cond_4

    .line 209
    invoke-interface {v14, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    move-object/from16 v4, v16

    check-cast v4, Lorg/opencv/core/Point;

    move-object/from16 p0, v14

    move/from16 v16, v15

    .line 212
    iget-wide v14, v4, Lorg/opencv/core/Point;->x:D
    :try_end_1
    .catch Lepson/colorcorrection/SDIC_Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lorg/opencv/core/CvException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object/from16 v17, v2

    int-to-double v1, v0

    cmpl-double v18, v14, v1

    if-eqz v18, :cond_3

    :try_start_2
    iget-wide v14, v4, Lorg/opencv/core/Point;->y:D

    cmpl-double v18, v14, v1

    if-eqz v18, :cond_3

    iget-wide v1, v4, Lorg/opencv/core/Point;->x:D

    int-to-double v14, v12

    cmpl-double v18, v1, v14

    if-eqz v18, :cond_3

    iget-wide v1, v4, Lorg/opencv/core/Point;->y:D

    int-to-double v14, v10

    cmpl-double v18, v1, v14

    if-eqz v18, :cond_3

    .line 214
    new-instance v1, Lorg/opencv/core/Rect;

    iget-wide v14, v4, Lorg/opencv/core/Point;->x:D

    double-to-int v2, v14

    sub-int/2addr v2, v0

    iget-wide v14, v4, Lorg/opencv/core/Point;->y:D

    double-to-int v4, v14

    sub-int/2addr v4, v0

    invoke-direct {v1, v2, v4, v7, v7}, Lorg/opencv/core/Rect;-><init>(IIII)V

    .line 215
    invoke-virtual {v5, v1}, Lorg/opencv/core/Mat;->submat(Lorg/opencv/core/Rect;)Lorg/opencv/core/Mat;

    move-result-object v1

    .line 216
    invoke-static {v1}, Lepson/colorcorrection/SDIC_BCSC;->GetPixcelData(Lorg/opencv/core/Mat;)[B

    move-result-object v2

    const/4 v4, 0x0

    :goto_2
    if-ge v4, v7, :cond_2

    const/4 v14, 0x0

    :goto_3
    if-ge v14, v7, :cond_1

    mul-int v15, v7, v4

    add-int/2addr v15, v14

    .line 226
    aget-byte v0, v8, v15

    and-int/lit16 v0, v0, 0xff

    move/from16 v18, v6

    .line 227
    aget-byte v6, v2, v15

    and-int/lit16 v6, v6, 0xff

    if-le v6, v0, :cond_0

    int-to-byte v0, v0

    .line 229
    aput-byte v0, v2, v15

    :cond_0
    add-int/lit8 v14, v14, 0x1

    move/from16 v6, v18

    move/from16 v0, p2

    goto :goto_3

    :cond_1
    move/from16 v18, v6

    add-int/lit8 v4, v4, 0x1

    move/from16 v0, p2

    goto :goto_2

    :cond_2
    move/from16 v18, v6

    const/4 v0, 0x0

    .line 235
    invoke-static {v1, v0, v0, v2}, Lepson/colorcorrection/SDIC_BCSC;->PutPixcelData(Lorg/opencv/core/Mat;II[B)V
    :try_end_2
    .catch Lepson/colorcorrection/SDIC_Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lorg/opencv/core/CvException; {:try_start_2 .. :try_end_2} :catch_8
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_6
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_4

    :catchall_0
    move-exception v0

    move-object/from16 v2, v17

    goto :goto_5

    :catch_0
    move-exception v0

    move-object/from16 v2, v17

    goto :goto_6

    :cond_3
    move/from16 v18, v6

    const/4 v0, 0x0

    :goto_4
    add-int/lit8 v3, v3, 0x1

    move-object/from16 v14, p0

    move/from16 v15, v16

    move-object/from16 v2, v17

    move/from16 v6, v18

    move/from16 v0, p2

    move-object/from16 v1, p1

    goto/16 :goto_1

    :cond_4
    move-object/from16 v17, v2

    move/from16 v18, v6

    const/4 v0, 0x0

    add-int/lit8 v13, v13, 0x1

    move/from16 v0, p2

    move-object/from16 v1, p1

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    move-object/from16 v17, v2

    :goto_5
    move-object/from16 v1, p1

    goto/16 :goto_c

    :catch_1
    move-exception v0

    move-object/from16 v17, v2

    :goto_6
    move-object/from16 v1, p1

    goto/16 :goto_a

    :cond_5
    move-object/from16 v17, v2

    const/4 v0, 0x0

    .line 240
    :try_start_3
    invoke-virtual {v9}, Lorg/opencv/core/Mat;->clone()Lorg/opencv/core/Mat;

    move-result-object v2
    :try_end_3
    .catch Lepson/colorcorrection/SDIC_Exception; {:try_start_3 .. :try_end_3} :catch_3
    .catch Lorg/opencv/core/CvException; {:try_start_3 .. :try_end_3} :catch_8
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 242
    :try_start_4
    invoke-virtual {v5}, Lorg/opencv/core/Mat;->release()V
    :try_end_4
    .catch Lepson/colorcorrection/SDIC_Exception; {:try_start_4 .. :try_end_4} :catch_2
    .catch Lorg/opencv/core/CvException; {:try_start_4 .. :try_end_4} :catch_7
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_5
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    move-object/from16 v1, p1

    .line 259
    invoke-virtual {v1, v2}, Lepson/colorcorrection/SDIC_MAT_PTR;->setMat(Lorg/opencv/core/Mat;)V

    const/4 v4, 0x0

    goto/16 :goto_b

    :catchall_2
    move-exception v0

    goto :goto_5

    :catch_2
    move-exception v0

    goto :goto_6

    :catchall_3
    move-exception v0

    move-object/from16 v1, p1

    goto :goto_7

    :catch_3
    move-exception v0

    move-object/from16 v1, p1

    goto :goto_8

    :cond_6
    move-object/from16 v17, v2

    .line 174
    :try_start_5
    new-instance v0, Lepson/colorcorrection/SDIC_Exception;

    const/4 v2, -0x7

    invoke-direct {v0, v2}, Lepson/colorcorrection/SDIC_Exception;-><init>(I)V

    throw v0

    :cond_7
    move-object/from16 v17, v2

    .line 172
    new-instance v0, Lepson/colorcorrection/SDIC_Exception;

    const/4 v2, -0x6

    invoke-direct {v0, v2}, Lepson/colorcorrection/SDIC_Exception;-><init>(I)V

    throw v0

    :cond_8
    move-object/from16 v17, v2

    .line 170
    new-instance v0, Lepson/colorcorrection/SDIC_Exception;

    const/4 v2, -0x4

    invoke-direct {v0, v2}, Lepson/colorcorrection/SDIC_Exception;-><init>(I)V

    throw v0

    :cond_9
    move-object/from16 v17, v2

    .line 168
    new-instance v0, Lepson/colorcorrection/SDIC_Exception;

    const/4 v2, -0x3

    invoke-direct {v0, v2}, Lepson/colorcorrection/SDIC_Exception;-><init>(I)V

    throw v0

    :cond_a
    move-object/from16 v17, v2

    .line 166
    new-instance v0, Lepson/colorcorrection/SDIC_Exception;

    const/4 v2, -0x1

    invoke-direct {v0, v2}, Lepson/colorcorrection/SDIC_Exception;-><init>(I)V

    throw v0

    :cond_b
    move-object/from16 v17, v2

    .line 164
    new-instance v0, Lepson/colorcorrection/SDIC_Exception;

    const/4 v2, -0x2

    invoke-direct {v0, v2}, Lepson/colorcorrection/SDIC_Exception;-><init>(I)V

    throw v0
    :try_end_5
    .catch Lepson/colorcorrection/SDIC_Exception; {:try_start_5 .. :try_end_5} :catch_4
    .catch Lorg/opencv/core/CvException; {:try_start_5 .. :try_end_5} :catch_8
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_6
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    :catchall_4
    move-exception v0

    :goto_7
    move-object/from16 v2, v17

    goto :goto_c

    :catch_4
    move-exception v0

    :goto_8
    move-object/from16 v2, v17

    goto :goto_a

    :catchall_5
    move-exception v0

    move-object/from16 v17, v2

    goto :goto_c

    :catch_5
    move-object/from16 v17, v2

    :catch_6
    const/16 v4, -0x190

    .line 254
    invoke-virtual/range {v17 .. v17}, Lorg/opencv/core/Mat;->empty()Z

    move-result v0

    if-nez v0, :cond_d

    .line 255
    :goto_9
    invoke-virtual/range {v17 .. v17}, Lorg/opencv/core/Mat;->release()V

    goto :goto_b

    :catch_7
    move-object/from16 v17, v2

    :catch_8
    const/16 v4, -0x64

    .line 254
    invoke-virtual/range {v17 .. v17}, Lorg/opencv/core/Mat;->empty()Z

    move-result v0

    if-nez v0, :cond_d

    goto :goto_9

    :catch_9
    move-exception v0

    move-object/from16 v17, v2

    .line 246
    :goto_a
    :try_start_6
    iget v4, v0, Lepson/colorcorrection/SDIC_Exception;->mCode:I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_6

    if-eqz v4, :cond_c

    .line 254
    invoke-virtual {v2}, Lorg/opencv/core/Mat;->empty()Z

    move-result v0

    if-nez v0, :cond_d

    const/4 v1, -0x1

    if-eq v4, v1, :cond_d

    .line 255
    invoke-virtual {v2}, Lorg/opencv/core/Mat;->release()V

    goto :goto_b

    .line 259
    :cond_c
    invoke-virtual {v1, v2}, Lepson/colorcorrection/SDIC_MAT_PTR;->setMat(Lorg/opencv/core/Mat;)V

    :cond_d
    :goto_b
    return v4

    :catchall_6
    move-exception v0

    :goto_c
    invoke-virtual {v1, v2}, Lepson/colorcorrection/SDIC_MAT_PTR;->setMat(Lorg/opencv/core/Mat;)V

    .line 261
    throw v0
.end method

.method public static MakePixcelData(Lorg/opencv/core/Mat;)[B
    .locals 2

    .line 634
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->rows()I

    move-result v0

    .line 635
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->cols()I

    move-result v1

    .line 636
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->channels()I

    move-result p0

    mul-int v0, v0, v1

    mul-int v0, v0, p0

    .line 637
    new-array p0, v0, [B

    return-object p0
.end method

.method private static PutPixcelData(Lorg/opencv/core/Mat;II[B)V
    .locals 0

    .line 615
    invoke-virtual {p0, p1, p2, p3}, Lorg/opencv/core/Mat;->put(II[B)I

    return-void
.end method

.method private static Saturation(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;D)I
    .locals 21

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    .line 414
    new-instance v2, Lorg/opencv/core/Mat;

    invoke-direct {v2}, Lorg/opencv/core/Mat;-><init>()V

    .line 415
    new-instance v3, Lorg/opencv/core/Mat;

    invoke-direct {v3}, Lorg/opencv/core/Mat;-><init>()V

    const-wide v4, 0x3f50624de0000000L    # 0.0010000000474974513

    const/4 v6, 0x0

    cmpg-double v7, p3, v4

    if-gez v7, :cond_0

    const-wide v4, -0x40af9db220000000L    # -0.0010000000474974513

    cmpl-double v7, p3, v4

    if-lez v7, :cond_0

    .line 422
    :try_start_0
    invoke-virtual {v0, v1}, Lorg/opencv/core/Mat;->copyTo(Lorg/opencv/core/Mat;)V

    return v6

    :catch_0
    move-object v1, v3

    goto/16 :goto_a

    :catch_1
    move-object v1, v3

    goto/16 :goto_b

    :catch_2
    move-exception v0

    move-object v1, v3

    goto/16 :goto_c

    :cond_0
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    cmpl-double v7, p3, v4

    if-gtz v7, :cond_10

    const-wide/high16 v4, -0x4010000000000000L    # -1.0

    cmpg-double v7, p3, v4

    if-ltz v7, :cond_10

    .line 429
    invoke-virtual/range {p0 .. p0}, Lorg/opencv/core/Mat;->channels()I

    move-result v4

    const/4 v5, 0x4

    const/16 v7, 0x28

    const/4 v8, 0x3

    const/4 v9, 0x1

    if-ne v4, v8, :cond_1

    .line 430
    invoke-static {v0, v2, v7}, Lorg/opencv/imgproc/Imgproc;->cvtColor(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V

    goto :goto_0

    .line 431
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lorg/opencv/core/Mat;->channels()I

    move-result v4

    if-ne v4, v5, :cond_f

    .line 432
    new-instance v4, Lorg/opencv/core/Mat;

    invoke-direct {v4}, Lorg/opencv/core/Mat;-><init>()V

    .line 433
    invoke-static {v0, v4, v9}, Lorg/opencv/imgproc/Imgproc;->cvtColor(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V

    .line 434
    invoke-static {v4, v2, v7}, Lorg/opencv/imgproc/Imgproc;->cvtColor(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V

    .line 435
    invoke-virtual {v4}, Lorg/opencv/core/Mat;->release()V

    .line 443
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lorg/opencv/core/Mat;->total()J

    move-result-wide v10

    const-wide/16 v12, 0x3

    mul-long v10, v10, v12

    long-to-int v4, v10

    const-wide v10, 0x406fe00000000000L    # 255.0

    div-double v12, p3, v10

    .line 447
    invoke-static/range {p1 .. p1}, Lepson/colorcorrection/SDIC_BCSC;->GetPixcelData(Lorg/opencv/core/Mat;)[B

    move-result-object v7

    .line 448
    invoke-static {v2}, Lepson/colorcorrection/SDIC_BCSC;->GetPixcelData(Lorg/opencv/core/Mat;)[B

    move-result-object v14

    .line 449
    invoke-virtual {v2}, Lorg/opencv/core/Mat;->release()V
    :try_end_0
    .catch Lepson/colorcorrection/SDIC_Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lorg/opencv/core/CvException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-wide/16 v15, 0x0

    const/16 v2, 0xff

    cmpl-double v17, p3, v15

    if-lez v17, :cond_6

    const/4 v15, 0x0

    :goto_1
    if-ge v9, v4, :cond_5

    .line 455
    :try_start_1
    aget-byte v6, v7, v15

    and-int/2addr v6, v2

    if-lez v6, :cond_4

    .line 457
    aget-byte v5, v14, v9

    and-int/2addr v5, v2

    add-int/lit8 v18, v9, 0x1

    .line 458
    aget-byte v8, v14, v18

    and-int/2addr v8, v2

    int-to-double v10, v6

    mul-double v10, v10, v12

    int-to-double v0, v5

    rsub-int v5, v5, 0xff

    rsub-int v6, v8, 0xff

    mul-int v5, v5, v6

    int-to-double v5, v5

    mul-double v5, v5, v10

    const-wide v10, 0x406fe00000000000L    # 255.0

    div-double/2addr v5, v10

    add-double/2addr v0, v5

    double-to-int v0, v0

    if-le v0, v2, :cond_2

    const/16 v6, 0xff

    goto :goto_2

    :cond_2
    move v6, v0

    :goto_2
    if-gez v6, :cond_3

    const/4 v6, 0x0

    :cond_3
    int-to-byte v0, v6

    .line 465
    aput-byte v0, v14, v9

    :cond_4
    add-int/lit8 v9, v9, 0x3

    add-int/lit8 v15, v15, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    const/4 v5, 0x4

    const/4 v6, 0x0

    const/4 v8, 0x3

    goto :goto_1

    :cond_5
    move-object v1, v3

    goto :goto_5

    :cond_6
    const/4 v0, 0x0

    :goto_3
    if-ge v9, v4, :cond_c

    .line 471
    aget-byte v1, v7, v0

    and-int/2addr v1, v2

    if-lez v1, :cond_b

    .line 473
    aget-byte v5, v14, v9

    and-int/2addr v5, v2

    add-int/lit8 v6, v9, 0x1

    .line 474
    aget-byte v8, v14, v6
    :try_end_1
    .catch Lepson/colorcorrection/SDIC_Exception; {:try_start_1 .. :try_end_1} :catch_5
    .catch Lorg/opencv/core/CvException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    and-int/2addr v8, v2

    int-to-double v10, v1

    mul-double v10, v10, v12

    move-object v1, v3

    int-to-double v2, v5

    mul-double v19, v2, v10

    add-double v2, v2, v19

    double-to-int v2, v2

    const/16 v3, 0xff

    if-le v2, v3, :cond_7

    const/16 v2, 0xff

    :cond_7
    if-gez v2, :cond_8

    const/4 v2, 0x0

    :cond_8
    int-to-byte v2, v2

    .line 481
    :try_start_2
    aput-byte v2, v14, v9

    int-to-double v2, v8

    mul-int v5, v5, v8

    move v8, v4

    int-to-double v4, v5

    const-wide v19, 0x407fe00000000000L    # 510.0

    div-double v4, v4, v19

    mul-double v4, v4, v10

    add-double/2addr v2, v4

    double-to-int v2, v2

    const/16 v3, 0xff

    if-le v2, v3, :cond_9

    const/16 v2, 0xff

    :cond_9
    if-gez v2, :cond_a

    const/4 v2, 0x0

    :cond_a
    int-to-byte v2, v2

    .line 487
    aput-byte v2, v14, v6

    goto :goto_4

    :cond_b
    move-object v1, v3

    move v8, v4

    const/16 v3, 0xff

    :goto_4
    add-int/lit8 v9, v9, 0x3

    add-int/lit8 v0, v0, 0x1

    move-object v3, v1

    move v4, v8

    const/16 v2, 0xff

    goto :goto_3

    :catch_3
    move-object v1, v3

    goto :goto_7

    :catch_4
    move-object v1, v3

    goto :goto_8

    :catch_5
    move-exception v0

    move-object v1, v3

    goto :goto_9

    :cond_c
    move-object v1, v3

    .line 492
    :goto_5
    invoke-virtual/range {p0 .. p0}, Lorg/opencv/core/Mat;->rows()I

    move-result v0

    invoke-virtual/range {p0 .. p0}, Lorg/opencv/core/Mat;->cols()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lorg/opencv/core/Mat;->type()I

    move-result v3

    invoke-static {v0, v2, v3, v14}, Lepson/colorcorrection/SDIC_BCSC;->SetPixcelData(III[B)Lorg/opencv/core/Mat;

    move-result-object v2
    :try_end_2
    .catch Lepson/colorcorrection/SDIC_Exception; {:try_start_2 .. :try_end_2} :catch_8
    .catch Lorg/opencv/core/CvException; {:try_start_2 .. :try_end_2} :catch_7
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_6

    const/16 v0, 0x36

    .line 496
    :try_start_3
    invoke-static {v2, v1, v0}, Lorg/opencv/imgproc/Imgproc;->cvtColor(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V

    .line 497
    invoke-virtual/range {p0 .. p0}, Lorg/opencv/core/Mat;->channels()I

    move-result v0

    const/4 v3, 0x3

    if-ne v0, v3, :cond_d

    .line 498
    invoke-virtual {v2}, Lorg/opencv/core/Mat;->release()V

    .line 499
    invoke-virtual {v1}, Lorg/opencv/core/Mat;->clone()Lorg/opencv/core/Mat;

    move-result-object v2

    const/4 v0, 0x0

    goto :goto_6

    .line 500
    :cond_d
    invoke-virtual/range {p0 .. p0}, Lorg/opencv/core/Mat;->channels()I

    move-result v0

    const/4 v3, 0x4

    if-ne v0, v3, :cond_e

    .line 501
    invoke-virtual {v2}, Lorg/opencv/core/Mat;->release()V

    const/4 v0, 0x0

    .line 502
    invoke-static {v1, v2, v0}, Lorg/opencv/imgproc/Imgproc;->cvtColor(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V

    goto :goto_6

    :cond_e
    const/4 v0, 0x0

    :goto_6
    const/4 v6, 0x0

    goto :goto_d

    :catch_6
    :goto_7
    const/4 v2, 0x0

    goto :goto_a

    :catch_7
    :goto_8
    const/4 v2, 0x0

    goto :goto_b

    :catch_8
    move-exception v0

    :goto_9
    const/4 v2, 0x0

    goto :goto_c

    :cond_f
    move-object v1, v3

    .line 438
    new-instance v0, Lepson/colorcorrection/SDIC_Exception;

    const/4 v3, -0x4

    invoke-direct {v0, v3}, Lepson/colorcorrection/SDIC_Exception;-><init>(I)V

    throw v0

    :cond_10
    move-object v1, v3

    .line 425
    new-instance v0, Lepson/colorcorrection/SDIC_Exception;

    const/4 v3, -0x7

    invoke-direct {v0, v3}, Lepson/colorcorrection/SDIC_Exception;-><init>(I)V

    throw v0
    :try_end_3
    .catch Lepson/colorcorrection/SDIC_Exception; {:try_start_3 .. :try_end_3} :catch_b
    .catch Lorg/opencv/core/CvException; {:try_start_3 .. :try_end_3} :catch_a
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_9

    :catch_9
    :goto_a
    const/16 v6, -0x190

    goto :goto_d

    :catch_a
    :goto_b
    const/16 v6, -0x64

    goto :goto_d

    :catch_b
    move-exception v0

    .line 506
    :goto_c
    iget v6, v0, Lepson/colorcorrection/SDIC_Exception;->mCode:I

    .line 513
    :goto_d
    invoke-virtual {v1}, Lorg/opencv/core/Mat;->empty()Z

    move-result v0

    if-nez v0, :cond_11

    .line 514
    invoke-virtual {v1}, Lorg/opencv/core/Mat;->release()V

    :cond_11
    if-eqz v6, :cond_12

    .line 520
    invoke-virtual {v2}, Lorg/opencv/core/Mat;->empty()Z

    move-result v0

    if-nez v0, :cond_13

    .line 521
    invoke-virtual {v2}, Lorg/opencv/core/Mat;->release()V

    goto :goto_e

    :cond_12
    move-object/from16 v1, p2

    .line 525
    invoke-virtual {v2, v1}, Lorg/opencv/core/Mat;->copyTo(Lorg/opencv/core/Mat;)V

    .line 526
    invoke-virtual {v2}, Lorg/opencv/core/Mat;->release()V

    :cond_13
    :goto_e
    return v6
.end method

.method public static SetPixcelData(III[B)Lorg/opencv/core/Mat;
    .locals 1

    .line 619
    new-instance v0, Lorg/opencv/core/Mat;

    invoke-direct {v0, p0, p1, p2}, Lorg/opencv/core/Mat;-><init>(III)V

    const/4 p0, 0x0

    .line 620
    invoke-virtual {v0, p0, p0, p3}, Lorg/opencv/core/Mat;->put(II[B)I

    return-object v0
.end method

.method private static saturate_cast(I)I
    .locals 1

    const/16 v0, 0xff

    if-le p0, v0, :cond_0

    const/16 p0, 0xff

    :cond_0
    if-gez p0, :cond_1

    const/4 p0, 0x0

    :cond_1
    return p0
.end method

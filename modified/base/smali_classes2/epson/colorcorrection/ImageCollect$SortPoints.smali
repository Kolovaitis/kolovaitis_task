.class Lepson/colorcorrection/ImageCollect$SortPoints;
.super Ljava/lang/Object;
.source "ImageCollect.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/colorcorrection/ImageCollect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SortPoints"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "Lorg/opencv/core/Point;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lepson/colorcorrection/ImageCollect;


# direct methods
.method private constructor <init>(Lepson/colorcorrection/ImageCollect;)V
    .locals 0

    .line 1317
    iput-object p1, p0, Lepson/colorcorrection/ImageCollect$SortPoints;->this$0:Lepson/colorcorrection/ImageCollect;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lepson/colorcorrection/ImageCollect;Lepson/colorcorrection/ImageCollect$1;)V
    .locals 0

    .line 1317
    invoke-direct {p0, p1}, Lepson/colorcorrection/ImageCollect$SortPoints;-><init>(Lepson/colorcorrection/ImageCollect;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    .line 1317
    check-cast p1, Lorg/opencv/core/Point;

    check-cast p2, Lorg/opencv/core/Point;

    invoke-virtual {p0, p1, p2}, Lepson/colorcorrection/ImageCollect$SortPoints;->compare(Lorg/opencv/core/Point;Lorg/opencv/core/Point;)I

    move-result p1

    return p1
.end method

.method public compare(Lorg/opencv/core/Point;Lorg/opencv/core/Point;)I
    .locals 3

    .line 1321
    iget-wide v0, p1, Lorg/opencv/core/Point;->y:D

    iget-wide p1, p2, Lorg/opencv/core/Point;->y:D

    cmpg-double v2, v0, p1

    if-gez v2, :cond_0

    const/4 p1, -0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    :goto_0
    return p1
.end method

.class public interface abstract Lepson/colorcorrection/SDIC_CommonDefine;
.super Ljava/lang/Object;
.source "SDIC_CommonDefine.java"


# static fields
.field public static final SDIC_BCSC_CONTRAST_GAIN:D = 20.0

.field public static final SDIC_BCSC_CONTRAST_LUT_LENGTH:I = 0x100

.field public static final SDIC_BCSC_CONTRAST_PARAMETER_ERR_RANGE_MINUS:F = -0.001f

.field public static final SDIC_BCSC_CONTRAST_PARAMETER_ERR_RANGE_PLUS:F = 0.001f

.field public static final SDIC_ERR_MAT_CHANNEL:I = -0x4

.field public static final SDIC_ERR_MAT_DISCONTINUOUS:I = -0x3

.field public static final SDIC_ERR_MAT_EMPTY:I = -0x2

.field public static final SDIC_ERR_MAT_OUT:I = -0x6

.field public static final SDIC_ERR_MAT_PROHIBITED_ACTS:I = -0x1

.field public static final SDIC_ERR_MAT_SIZE:I = -0x5

.field public static final SDIC_ERR_OPENCV_METHOD:I = -0x64

.field public static final SDIC_ERR_PARAMETER:I = -0x7

.field public static final SDIC_ERR_UNKNOWN:I = -0x190

.field public static final SDIC_IMAGE_CHANNEL_BGR:I = 0x3

.field public static final SDIC_IMAGE_CHANNEL_BGRA:I = 0x4

.field public static final SDIC_IMAGE_CHANNEL_HSV:I = 0x3

.field public static final SDIC_IMAGE_CHANNEL_HSV_S:I = 0x1

.field public static final SDIC_IMAGE_CHANNEL_MASK:I = 0x1

.field public static final SDIC_IMAGE_MAX_VALUE:I = 0xff

.field public static final SDIC_IMAGE_MIN_VALUE:I = 0x0

.field public static final SDIC_OK:I = 0x0

.field public static final SDIC_PARAMETER_CENTER:F = 0.0f

.field public static final SDIC_PARAMETER_MAX:F = 1.0f

.field public static final SDIC_PARAMETER_MIN:F = -1.0f

.field public static final SDIC_PWC_FILTER_HIGHPASS:D = 0.05

.field public static final SDIC_PWC_FILTER_KERNELSIZE:I = 0x3

.field public static final SDIC_PWC_FILTER_LOWPASS:D = 1.0

.field public static final SDIC_PWC_FILTER_MAKINGSIZE:D = 80.0

.field public static final SDIC_PWC_FILTER_SHARPNESS:F = 2.0f

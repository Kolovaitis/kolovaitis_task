.class public Lepson/colorcorrection/SDIC_API;
.super Ljava/lang/Object;
.source "SDIC_API.java"

# interfaces
.implements Lepson/colorcorrection/SDIC_CommonDefine;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "opencv_java3"

    .line 69
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static SDIC_API_BCSCorrection(Ljava/lang/String;FLjava/lang/String;Ljava/lang/String;DDD)I
    .locals 13

    .line 124
    new-instance v1, Lepson/colorcorrection/SDIC_MAT_PTR;

    invoke-direct {v1}, Lepson/colorcorrection/SDIC_MAT_PTR;-><init>()V

    .line 125
    new-instance v2, Lepson/colorcorrection/SDIC_MAT_PTR;

    invoke-direct {v2}, Lepson/colorcorrection/SDIC_MAT_PTR;-><init>()V

    .line 126
    new-instance v12, Lepson/colorcorrection/SDIC_MAT_PTR;

    invoke-direct {v12}, Lepson/colorcorrection/SDIC_MAT_PTR;-><init>()V

    const/16 v0, -0x190

    .line 132
    :try_start_0
    invoke-static {p0}, Lorg/opencv/imgcodecs/Imgcodecs;->imread(Ljava/lang/String;)Lorg/opencv/core/Mat;

    move-result-object v3

    invoke-virtual {v1, v3}, Lepson/colorcorrection/SDIC_MAT_PTR;->setMat(Lorg/opencv/core/Mat;)V

    .line 133
    invoke-virtual {v1}, Lepson/colorcorrection/SDIC_MAT_PTR;->getMat()Lorg/opencv/core/Mat;

    move-result-object v3

    move v4, p1

    .line 136
    invoke-virtual {v1, p1}, Lepson/colorcorrection/SDIC_MAT_PTR;->rotateMat(F)V

    const/4 v4, 0x0

    move-object v5, p2

    .line 139
    invoke-static {p2, v4}, Lorg/opencv/imgcodecs/Imgcodecs;->imread(Ljava/lang/String;I)Lorg/opencv/core/Mat;

    move-result-object v4

    invoke-virtual {v2, v4}, Lepson/colorcorrection/SDIC_MAT_PTR;->setMat(Lorg/opencv/core/Mat;)V

    .line 140
    invoke-virtual {v2}, Lepson/colorcorrection/SDIC_MAT_PTR;->getMat()Lorg/opencv/core/Mat;

    move-result-object v4

    move-object v5, v12

    move-wide/from16 v6, p4

    move-wide/from16 v8, p6

    move-wide/from16 v10, p8

    .line 142
    invoke-static/range {v3 .. v11}, Lepson/colorcorrection/SDIC_BCSC;->BCSCorrection(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lepson/colorcorrection/SDIC_MAT_PTR;DDD)I

    move-result v3

    if-nez v3, :cond_0

    .line 146
    invoke-virtual {v12}, Lepson/colorcorrection/SDIC_MAT_PTR;->getMat()Lorg/opencv/core/Mat;

    move-result-object v4

    move-object/from16 v5, p3

    invoke-static {v5, v4}, Lorg/opencv/imgcodecs/Imgcodecs;->imwrite(Ljava/lang/String;Lorg/opencv/core/Mat;)Z

    move-result v4
    :try_end_0
    .catch Lorg/opencv/core/CvException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v4, :cond_0

    goto :goto_0

    :cond_0
    move v0, v3

    .line 157
    :goto_0
    invoke-virtual {v1}, Lepson/colorcorrection/SDIC_MAT_PTR;->releaseMat()V

    .line 160
    :goto_1
    invoke-virtual {v2}, Lepson/colorcorrection/SDIC_MAT_PTR;->releaseMat()V

    .line 163
    invoke-virtual {v12}, Lepson/colorcorrection/SDIC_MAT_PTR;->releaseMat()V

    goto :goto_2

    :catchall_0
    move-exception v0

    .line 157
    invoke-virtual {v1}, Lepson/colorcorrection/SDIC_MAT_PTR;->releaseMat()V

    .line 160
    invoke-virtual {v2}, Lepson/colorcorrection/SDIC_MAT_PTR;->releaseMat()V

    .line 163
    invoke-virtual {v12}, Lepson/colorcorrection/SDIC_MAT_PTR;->releaseMat()V

    throw v0

    .line 157
    :catch_0
    invoke-virtual {v1}, Lepson/colorcorrection/SDIC_MAT_PTR;->releaseMat()V

    goto :goto_1

    :catch_1
    const/16 v0, -0x64

    invoke-virtual {v1}, Lepson/colorcorrection/SDIC_MAT_PTR;->releaseMat()V

    goto :goto_1

    :goto_2
    return v0
.end method

.method public static SDIC_API_BCSCorrection(Ljava/lang/String;FLjava/lang/String;Ljava/lang/String;IDDD)I
    .locals 13

    move-object/from16 v0, p3

    .line 180
    new-instance v1, Lepson/colorcorrection/SDIC_MAT_PTR;

    invoke-direct {v1}, Lepson/colorcorrection/SDIC_MAT_PTR;-><init>()V

    .line 181
    new-instance v2, Lepson/colorcorrection/SDIC_MAT_PTR;

    invoke-direct {v2}, Lepson/colorcorrection/SDIC_MAT_PTR;-><init>()V

    .line 182
    new-instance v12, Lepson/colorcorrection/SDIC_MAT_PTR;

    invoke-direct {v12}, Lepson/colorcorrection/SDIC_MAT_PTR;-><init>()V

    .line 187
    :try_start_0
    invoke-static {p0}, Lorg/opencv/imgcodecs/Imgcodecs;->imread(Ljava/lang/String;)Lorg/opencv/core/Mat;

    move-result-object v3

    invoke-virtual {v1, v3}, Lepson/colorcorrection/SDIC_MAT_PTR;->setMat(Lorg/opencv/core/Mat;)V

    .line 188
    invoke-virtual {v1}, Lepson/colorcorrection/SDIC_MAT_PTR;->getMat()Lorg/opencv/core/Mat;

    move-result-object v3

    move v4, p1

    .line 191
    invoke-virtual {v1, p1}, Lepson/colorcorrection/SDIC_MAT_PTR;->rotateMat(F)V

    .line 194
    new-instance v4, Lorg/opencv/core/Mat;

    invoke-direct {v4}, Lorg/opencv/core/Mat;-><init>()V

    .line 195
    invoke-virtual {v1}, Lepson/colorcorrection/SDIC_MAT_PTR;->getMat()Lorg/opencv/core/Mat;

    move-result-object v5

    invoke-virtual {v5}, Lorg/opencv/core/Mat;->channels()I

    move-result v5

    const/4 v6, 0x3

    if-ne v5, v6, :cond_0

    .line 196
    invoke-virtual {v1}, Lepson/colorcorrection/SDIC_MAT_PTR;->getMat()Lorg/opencv/core/Mat;

    move-result-object v5

    const/4 v6, 0x6

    invoke-static {v5, v4, v6}, Lorg/opencv/imgproc/Imgproc;->cvtColor(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V

    move/from16 v5, p4

    goto :goto_0

    .line 197
    :cond_0
    invoke-virtual {v1}, Lepson/colorcorrection/SDIC_MAT_PTR;->getMat()Lorg/opencv/core/Mat;

    move-result-object v5

    invoke-virtual {v5}, Lorg/opencv/core/Mat;->channels()I

    move-result v5

    const/4 v6, 0x4

    if-ne v5, v6, :cond_3

    .line 198
    invoke-virtual {v1}, Lepson/colorcorrection/SDIC_MAT_PTR;->getMat()Lorg/opencv/core/Mat;

    move-result-object v5

    const/16 v6, 0xa

    invoke-static {v5, v4, v6}, Lorg/opencv/imgproc/Imgproc;->cvtColor(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V

    move/from16 v5, p4

    .line 204
    :goto_0
    invoke-static {v4, v2, v5}, Lepson/colorcorrection/SDIC_BCSC;->MakeMask(Lorg/opencv/core/Mat;Lepson/colorcorrection/SDIC_MAT_PTR;I)I

    move-result v5

    .line 205
    invoke-virtual {v4}, Lorg/opencv/core/Mat;->release()V

    if-nez v5, :cond_2

    .line 210
    invoke-virtual {v2}, Lepson/colorcorrection/SDIC_MAT_PTR;->getMat()Lorg/opencv/core/Mat;

    move-result-object v4

    move-object v5, v12

    move-wide/from16 v6, p5

    move-wide/from16 v8, p7

    move-wide/from16 v10, p9

    invoke-static/range {v3 .. v11}, Lepson/colorcorrection/SDIC_BCSC;->BCSCorrection(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lepson/colorcorrection/SDIC_MAT_PTR;DDD)I

    move-result v5

    if-nez v5, :cond_1

    .line 214
    invoke-virtual {v12}, Lepson/colorcorrection/SDIC_MAT_PTR;->getMat()Lorg/opencv/core/Mat;

    move-result-object v3

    move-object v4, p2

    invoke-static {p2, v3}, Lorg/opencv/imgcodecs/Imgcodecs;->imwrite(Ljava/lang/String;Lorg/opencv/core/Mat;)Z

    :cond_1
    if-eqz v0, :cond_2

    .line 217
    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_2

    .line 218
    invoke-virtual {v2}, Lepson/colorcorrection/SDIC_MAT_PTR;->getMat()Lorg/opencv/core/Mat;

    move-result-object v3

    invoke-static {v0, v3}, Lorg/opencv/imgcodecs/Imgcodecs;->imwrite(Ljava/lang/String;Lorg/opencv/core/Mat;)Z
    :try_end_0
    .catch Lorg/opencv/core/CvException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 228
    :cond_2
    invoke-virtual {v1}, Lepson/colorcorrection/SDIC_MAT_PTR;->releaseMat()V

    .line 231
    :goto_1
    invoke-virtual {v2}, Lepson/colorcorrection/SDIC_MAT_PTR;->releaseMat()V

    .line 234
    invoke-virtual {v12}, Lepson/colorcorrection/SDIC_MAT_PTR;->releaseMat()V

    goto :goto_2

    :cond_3
    const/4 v0, -0x4

    .line 228
    invoke-virtual {v1}, Lepson/colorcorrection/SDIC_MAT_PTR;->releaseMat()V

    .line 231
    invoke-virtual {v2}, Lepson/colorcorrection/SDIC_MAT_PTR;->releaseMat()V

    .line 234
    invoke-virtual {v12}, Lepson/colorcorrection/SDIC_MAT_PTR;->releaseMat()V

    return v0

    :catchall_0
    move-exception v0

    .line 228
    invoke-virtual {v1}, Lepson/colorcorrection/SDIC_MAT_PTR;->releaseMat()V

    .line 231
    invoke-virtual {v2}, Lepson/colorcorrection/SDIC_MAT_PTR;->releaseMat()V

    .line 234
    invoke-virtual {v12}, Lepson/colorcorrection/SDIC_MAT_PTR;->releaseMat()V

    throw v0

    :catch_0
    const/16 v5, -0x190

    .line 228
    invoke-virtual {v1}, Lepson/colorcorrection/SDIC_MAT_PTR;->releaseMat()V

    goto :goto_1

    :catch_1
    const/16 v5, -0x64

    invoke-virtual {v1}, Lepson/colorcorrection/SDIC_MAT_PTR;->releaseMat()V

    goto :goto_1

    :goto_2
    return v5
.end method

.method public static SDIC_API_MakeMask(Ljava/lang/String;FLjava/lang/String;I)I
    .locals 4

    .line 78
    new-instance v0, Lepson/colorcorrection/SDIC_MAT_PTR;

    invoke-direct {v0}, Lepson/colorcorrection/SDIC_MAT_PTR;-><init>()V

    .line 79
    new-instance v1, Lepson/colorcorrection/SDIC_MAT_PTR;

    invoke-direct {v1}, Lepson/colorcorrection/SDIC_MAT_PTR;-><init>()V

    const/4 v2, 0x0

    const/16 v3, -0x190

    .line 84
    :try_start_0
    invoke-static {p0, v2}, Lorg/opencv/imgcodecs/Imgcodecs;->imread(Ljava/lang/String;I)Lorg/opencv/core/Mat;

    move-result-object p0

    invoke-virtual {v0, p0}, Lepson/colorcorrection/SDIC_MAT_PTR;->setMat(Lorg/opencv/core/Mat;)V

    .line 85
    invoke-virtual {v0}, Lepson/colorcorrection/SDIC_MAT_PTR;->getMat()Lorg/opencv/core/Mat;

    move-result-object p0

    .line 88
    invoke-virtual {v0, p1}, Lepson/colorcorrection/SDIC_MAT_PTR;->rotateMat(F)V

    .line 91
    invoke-static {p0, v1, p3}, Lepson/colorcorrection/SDIC_BCSC;->MakeMask(Lorg/opencv/core/Mat;Lepson/colorcorrection/SDIC_MAT_PTR;I)I

    move-result p0

    if-nez p0, :cond_0

    .line 94
    invoke-virtual {v1}, Lepson/colorcorrection/SDIC_MAT_PTR;->getMat()Lorg/opencv/core/Mat;

    move-result-object p1

    invoke-static {p2, p1}, Lorg/opencv/imgcodecs/Imgcodecs;->imwrite(Ljava/lang/String;Lorg/opencv/core/Mat;)Z

    move-result p1
    :try_end_0
    .catch Lorg/opencv/core/CvException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    move v3, p0

    .line 105
    :catch_0
    :goto_0
    invoke-virtual {v0}, Lepson/colorcorrection/SDIC_MAT_PTR;->releaseMat()V

    .line 108
    invoke-virtual {v1}, Lepson/colorcorrection/SDIC_MAT_PTR;->releaseMat()V

    goto :goto_1

    :catchall_0
    move-exception p0

    .line 105
    invoke-virtual {v0}, Lepson/colorcorrection/SDIC_MAT_PTR;->releaseMat()V

    .line 108
    invoke-virtual {v1}, Lepson/colorcorrection/SDIC_MAT_PTR;->releaseMat()V

    throw p0

    :catch_1
    const/16 v3, -0x64

    goto :goto_0

    :goto_1
    return v3
.end method

.method public static SDIC_API_PaperWhiteCorrection(Ljava/lang/String;FLjava/lang/String;)I
    .locals 2

    .line 246
    new-instance v0, Lepson/colorcorrection/SDIC_MAT_PTR;

    invoke-direct {v0}, Lepson/colorcorrection/SDIC_MAT_PTR;-><init>()V

    .line 247
    new-instance v1, Lepson/colorcorrection/SDIC_MAT_PTR;

    invoke-direct {v1}, Lepson/colorcorrection/SDIC_MAT_PTR;-><init>()V

    .line 252
    :try_start_0
    invoke-static {p0}, Lorg/opencv/imgcodecs/Imgcodecs;->imread(Ljava/lang/String;)Lorg/opencv/core/Mat;

    move-result-object p0

    invoke-virtual {v0, p0}, Lepson/colorcorrection/SDIC_MAT_PTR;->setMat(Lorg/opencv/core/Mat;)V

    .line 253
    invoke-virtual {v0}, Lepson/colorcorrection/SDIC_MAT_PTR;->getMat()Lorg/opencv/core/Mat;

    move-result-object p0

    .line 256
    invoke-virtual {v0, p1}, Lepson/colorcorrection/SDIC_MAT_PTR;->rotateMat(F)V

    .line 258
    invoke-static {p0, v1}, Lepson/colorcorrection/SDIC_PWC;->PaperWhiteCorrection(Lorg/opencv/core/Mat;Lepson/colorcorrection/SDIC_MAT_PTR;)I

    move-result p0

    if-nez p0, :cond_0

    .line 261
    invoke-virtual {v1}, Lepson/colorcorrection/SDIC_MAT_PTR;->getMat()Lorg/opencv/core/Mat;

    move-result-object p1

    invoke-static {p2, p1}, Lorg/opencv/imgcodecs/Imgcodecs;->imwrite(Ljava/lang/String;Lorg/opencv/core/Mat;)Z
    :try_end_0
    .catch Lorg/opencv/core/CvException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p0

    .line 268
    throw p0

    :catch_0
    const/16 p0, -0x190

    goto :goto_0

    :catch_1
    const/16 p0, -0x64

    :cond_0
    :goto_0
    return p0
.end method

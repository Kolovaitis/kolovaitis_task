.class public Lepson/colorcorrection/SDIC_MAT_PTR;
.super Ljava/lang/Object;
.source "SDIC_MAT_PTR.java"


# instance fields
.field private mMat:Lorg/opencv/core/Mat;


# direct methods
.method constructor <init>()V
    .locals 1

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 7
    iput-object v0, p0, Lepson/colorcorrection/SDIC_MAT_PTR;->mMat:Lorg/opencv/core/Mat;

    .line 10
    new-instance v0, Lorg/opencv/core/Mat;

    invoke-direct {v0}, Lorg/opencv/core/Mat;-><init>()V

    iput-object v0, p0, Lepson/colorcorrection/SDIC_MAT_PTR;->mMat:Lorg/opencv/core/Mat;

    return-void
.end method

.method constructor <init>(Lorg/opencv/core/Mat;)V
    .locals 1

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 7
    iput-object v0, p0, Lepson/colorcorrection/SDIC_MAT_PTR;->mMat:Lorg/opencv/core/Mat;

    .line 14
    invoke-virtual {p0, p1}, Lepson/colorcorrection/SDIC_MAT_PTR;->setMat(Lorg/opencv/core/Mat;)V

    return-void
.end method


# virtual methods
.method public getMat()Lorg/opencv/core/Mat;
    .locals 1

    .line 18
    iget-object v0, p0, Lepson/colorcorrection/SDIC_MAT_PTR;->mMat:Lorg/opencv/core/Mat;

    return-object v0
.end method

.method public releaseMat()V
    .locals 1

    .line 41
    iget-object v0, p0, Lepson/colorcorrection/SDIC_MAT_PTR;->mMat:Lorg/opencv/core/Mat;

    if-eqz v0, :cond_0

    .line 42
    invoke-virtual {v0}, Lorg/opencv/core/Mat;->release()V

    const/4 v0, 0x0

    .line 43
    iput-object v0, p0, Lepson/colorcorrection/SDIC_MAT_PTR;->mMat:Lorg/opencv/core/Mat;

    :cond_0
    return-void
.end method

.method public rotateMat(F)V
    .locals 5

    .line 27
    iget-object v0, p0, Lepson/colorcorrection/SDIC_MAT_PTR;->mMat:Lorg/opencv/core/Mat;

    if-eqz v0, :cond_2

    const/high16 v1, 0x42b40000    # 90.0f

    cmpl-float v1, p1, v1

    if-nez v1, :cond_0

    .line 29
    invoke-static {v0, v0}, Lorg/opencv/core/Core;->transpose(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V

    .line 30
    iget-object p1, p0, Lepson/colorcorrection/SDIC_MAT_PTR;->mMat:Lorg/opencv/core/Mat;

    const/4 v0, 0x1

    invoke-static {p1, p1, v0}, Lorg/opencv/core/Core;->flip(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V

    goto :goto_0

    :cond_0
    float-to-double v1, p1

    const-wide v3, 0x4066800000000000L    # 180.0

    cmpl-double p1, v1, v3

    if-nez p1, :cond_1

    const/4 p1, -0x1

    .line 32
    invoke-static {v0, v0, p1}, Lorg/opencv/core/Core;->flip(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V

    goto :goto_0

    :cond_1
    const-wide v3, 0x4070e00000000000L    # 270.0

    cmpl-double p1, v1, v3

    if-nez p1, :cond_2

    .line 34
    invoke-static {v0, v0}, Lorg/opencv/core/Core;->transpose(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V

    .line 35
    iget-object p1, p0, Lepson/colorcorrection/SDIC_MAT_PTR;->mMat:Lorg/opencv/core/Mat;

    const/4 v0, 0x0

    invoke-static {p1, p1, v0}, Lorg/opencv/core/Core;->flip(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V

    :cond_2
    :goto_0
    return-void
.end method

.method public setMat(Lorg/opencv/core/Mat;)V
    .locals 0

    .line 22
    iput-object p1, p0, Lepson/colorcorrection/SDIC_MAT_PTR;->mMat:Lorg/opencv/core/Mat;

    return-void
.end method

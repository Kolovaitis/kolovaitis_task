.class Lepson/print/MenudataAdapter$MenuDataCapability;
.super Ljava/lang/Object;
.source "MenudataAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/MenudataAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MenuDataCapability"
.end annotation


# instance fields
.field appIDs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field deviceIDs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field functionIDs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field isPrinter:Ljava/lang/Boolean;

.field isScanner:Ljava/lang/Boolean;

.field final synthetic this$0:Lepson/print/MenudataAdapter;


# direct methods
.method constructor <init>(Lepson/print/MenudataAdapter;)V
    .locals 1

    .line 705
    iput-object p1, p0, Lepson/print/MenudataAdapter$MenuDataCapability;->this$0:Lepson/print/MenudataAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 706
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lepson/print/MenudataAdapter$MenuDataCapability;->deviceIDs:Ljava/util/ArrayList;

    const/4 p1, 0x0

    .line 707
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lepson/print/MenudataAdapter$MenuDataCapability;->isPrinter:Ljava/lang/Boolean;

    .line 708
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lepson/print/MenudataAdapter$MenuDataCapability;->isScanner:Ljava/lang/Boolean;

    .line 709
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lepson/print/MenudataAdapter$MenuDataCapability;->appIDs:Ljava/util/ArrayList;

    .line 710
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lepson/print/MenudataAdapter$MenuDataCapability;->functionIDs:Ljava/util/ArrayList;

    return-void
.end method

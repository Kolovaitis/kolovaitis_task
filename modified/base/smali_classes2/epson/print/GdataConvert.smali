.class public Lepson/print/GdataConvert;
.super Ljava/lang/Object;
.source "GdataConvert.java"


# static fields
.field private static final BASE:I = 0x64

.field public static final CONVERT_CANCEL:I = 0x67

.field public static final CONVERT_FAIL:I = 0x65

.field public static final CONVERT_OK:I = 0x64

.field public static final FAIL_SIZEOVER_DOC:I = 0x68

.field public static final FAIL_SIZEOVER_PPT:I = 0x6a

.field public static final FAIL_SIZEOVER_SPR:I = 0x69

.field public static final GD_MAXSIZE_DOCUMENT:I = 0x2

.field public static final GD_MAXSIZE_PRESENTATION:I = 0xa

.field public static final GD_MAXSIZE_SPREDSHEET:I = 0x14

.field public static final INTERNET_PROBLEM:I = 0x66


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static checkFileSize(Ljava/lang/String;)I
    .locals 4

    .line 39
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 40
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    long-to-double v0, v0

    const-wide/high16 v2, 0x4130000000000000L    # 1048576.0

    div-double/2addr v0, v2

    double-to-float v0, v0

    .line 41
    invoke-static {p0}, Lepson/common/Utils;->getExtention(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_7

    .line 42
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    goto :goto_2

    :cond_0
    const-string v1, ".doc"

    .line 46
    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, ".docx"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, ".rtf"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_1

    :cond_1
    const-string v1, ".xls"

    .line 50
    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, ".xlsx"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, ".csv"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    goto :goto_0

    :cond_2
    const-string v1, ".ppt"

    .line 54
    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ".pptx"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_6

    :cond_3
    const/high16 p0, 0x41200000    # 10.0f

    cmpl-float p0, v0, p0

    if-lez p0, :cond_6

    const/16 p0, 0x6a

    return p0

    :cond_4
    :goto_0
    const/high16 p0, 0x41a00000    # 20.0f

    cmpl-float p0, v0, p0

    if-lez p0, :cond_6

    const/16 p0, 0x69

    return p0

    :cond_5
    :goto_1
    const/high16 p0, 0x40000000    # 2.0f

    cmpl-float p0, v0, p0

    if-lez p0, :cond_6

    const/16 p0, 0x68

    return p0

    :cond_6
    const/16 p0, 0x64

    return p0

    :cond_7
    :goto_2
    const/16 p0, 0x65

    return p0
.end method

.class public Lepson/print/ActivityNfcPrinter;
.super Landroid/app/Activity;
.source "ActivityNfcPrinter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/ActivityNfcPrinter$NfcSimpleApRetryStatus;,
        Lepson/print/ActivityNfcPrinter$NfcStatus;
    }
.end annotation


# static fields
.field public static final CHANGEMODE:Ljava/lang/String; = "changeMode"

.field public static final CHANGE_HOME:I = 0x0

.field public static final CHANGE_PRINTER_ONLY:I = 0x1

.field public static final CHANGE_SCANNER_ONLY:I = 0x2

.field private static final CHECK_WIFIACCESSS:I = 0x8

.field private static final CONNECTED_SIMPLEAP:I = 0x2

.field public static final CONNECTINFO:Ljava/lang/String; = "connectInfo"

.field private static final CONNECT_PRINTER_VIA_INFRA:I = 0x9

.field private static final CONNECT_SIMPLEAP:I = 0xa

.field private static final CONNECT_SIMPLEAP_PRINTER:I = 0xb

.field private static final ENABLED_WIFI:I = 0x1

.field private static final EPS_COMM_BID:I = 0x2

.field private static final FINISH:I = 0x14

.field private static final FOUND_PRINTER:I = 0x0

.field private static final IS_NEW_SAVE:Ljava/lang/String; = "IS_NEW_SAVE"

.field private static final NO_CLEAR_RESULT:Ljava/lang/String; = "NO_CLEAR_RESULT"

.field private static final PRINTER_ID:Ljava/lang/String; = "id"

.field private static final PRINTER_IP:Ljava/lang/String; = "ip"

.field private static final PRINTER_NAME:Ljava/lang/String; = "name"

.field private static final PRINTER_SERIAL_NO:Ljava/lang/String; = "serial_no"

.field private static final PROBE_PRINTER:I = 0xc

.field private static final PROBE_SCANNER:I = 0xd

.field private static final REQUEST_LOCATION_PERMISSION:I = 0x3

.field public static final SEARCH_TIME_INFRA:I = 0x5

.field private static final TAG:Ljava/lang/String; = "ActivityChangeWifiPrinter"

.field private static mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

.field private static mScanner:Lepson/scan/lib/escanLib;


# instance fields
.field private connectInfo:Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;

.field private foundPrinter:Lepson/print/MyPrinter;

.field final mHandler:Landroid/os/Handler;

.field private nChangeMode:I

.field private printerStatus:I

.field progress:Lepson/print/screen/WorkingDialog;

.field retrystatus:Lepson/print/ActivityNfcPrinter$NfcSimpleApRetryStatus;

.field status:Lepson/print/ActivityNfcPrinter$NfcStatus;

.field private strInitPass:Ljava/lang/String;

.field private strIpAddressV4:Ljava/lang/String;

.field private strIpAddressV4SimpleAP:Ljava/lang/String;

.field private strMacAdress:Ljava/lang/String;

.field private strPass:Ljava/lang/String;

.field private strSSID:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 129
    new-instance v0, Lepson/scan/lib/escanLib;

    invoke-direct {v0}, Lepson/scan/lib/escanLib;-><init>()V

    sput-object v0, Lepson/print/ActivityNfcPrinter;->mScanner:Lepson/scan/lib/escanLib;

    .line 130
    invoke-static {}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->getInstance()Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object v0

    sput-object v0, Lepson/print/ActivityNfcPrinter;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 51
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 104
    sget-object v0, Lepson/print/ActivityNfcPrinter$NfcStatus;->INIT:Lepson/print/ActivityNfcPrinter$NfcStatus;

    iput-object v0, p0, Lepson/print/ActivityNfcPrinter;->status:Lepson/print/ActivityNfcPrinter$NfcStatus;

    .line 105
    sget-object v0, Lepson/print/ActivityNfcPrinter$NfcSimpleApRetryStatus;->FIRST_CONNECTION:Lepson/print/ActivityNfcPrinter$NfcSimpleApRetryStatus;

    iput-object v0, p0, Lepson/print/ActivityNfcPrinter;->retrystatus:Lepson/print/ActivityNfcPrinter$NfcSimpleApRetryStatus;

    const/4 v0, 0x0

    .line 132
    iput v0, p0, Lepson/print/ActivityNfcPrinter;->nChangeMode:I

    const/4 v0, 0x0

    .line 145
    iput-object v0, p0, Lepson/print/ActivityNfcPrinter;->foundPrinter:Lepson/print/MyPrinter;

    .line 330
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lepson/print/ActivityNfcPrinter$1;

    invoke-direct {v1, p0}, Lepson/print/ActivityNfcPrinter$1;-><init>(Lepson/print/ActivityNfcPrinter;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lepson/print/ActivityNfcPrinter;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lepson/print/ActivityNfcPrinter;)Ljava/lang/String;
    .locals 0

    .line 51
    iget-object p0, p0, Lepson/print/ActivityNfcPrinter;->strSSID:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$100(Lepson/print/ActivityNfcPrinter;)Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;
    .locals 0

    .line 51
    iget-object p0, p0, Lepson/print/ActivityNfcPrinter;->connectInfo:Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;

    return-object p0
.end method

.method static synthetic access$1000(Lepson/print/ActivityNfcPrinter;)Ljava/lang/String;
    .locals 0

    .line 51
    iget-object p0, p0, Lepson/print/ActivityNfcPrinter;->strInitPass:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1100()Lepson/scan/lib/escanLib;
    .locals 1

    .line 51
    sget-object v0, Lepson/print/ActivityNfcPrinter;->mScanner:Lepson/scan/lib/escanLib;

    return-object v0
.end method

.method static synthetic access$200(Lepson/print/ActivityNfcPrinter;)I
    .locals 0

    .line 51
    iget p0, p0, Lepson/print/ActivityNfcPrinter;->printerStatus:I

    return p0
.end method

.method static synthetic access$300(Lepson/print/ActivityNfcPrinter;)Ljava/lang/String;
    .locals 0

    .line 51
    iget-object p0, p0, Lepson/print/ActivityNfcPrinter;->strIpAddressV4:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$400()Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;
    .locals 1

    .line 51
    sget-object v0, Lepson/print/ActivityNfcPrinter;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    return-object v0
.end method

.method static synthetic access$500(Lepson/print/ActivityNfcPrinter;)Lepson/print/MyPrinter;
    .locals 0

    .line 51
    iget-object p0, p0, Lepson/print/ActivityNfcPrinter;->foundPrinter:Lepson/print/MyPrinter;

    return-object p0
.end method

.method static synthetic access$502(Lepson/print/ActivityNfcPrinter;Lepson/print/MyPrinter;)Lepson/print/MyPrinter;
    .locals 0

    .line 51
    iput-object p1, p0, Lepson/print/ActivityNfcPrinter;->foundPrinter:Lepson/print/MyPrinter;

    return-object p1
.end method

.method static synthetic access$600(Lepson/print/ActivityNfcPrinter;)Ljava/lang/String;
    .locals 0

    .line 51
    iget-object p0, p0, Lepson/print/ActivityNfcPrinter;->strMacAdress:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$700(Lepson/print/ActivityNfcPrinter;)I
    .locals 0

    .line 51
    iget p0, p0, Lepson/print/ActivityNfcPrinter;->nChangeMode:I

    return p0
.end method

.method static synthetic access$800(Lepson/print/ActivityNfcPrinter;)Ljava/lang/String;
    .locals 0

    .line 51
    iget-object p0, p0, Lepson/print/ActivityNfcPrinter;->strIpAddressV4SimpleAP:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$900(Lepson/print/ActivityNfcPrinter;)Ljava/lang/String;
    .locals 0

    .line 51
    iget-object p0, p0, Lepson/print/ActivityNfcPrinter;->strPass:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .line 220
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    const/16 p3, 0x8

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    goto/16 :goto_0

    :pswitch_0
    packed-switch p2, :pswitch_data_1

    goto/16 :goto_0

    .line 251
    :pswitch_1
    invoke-virtual {p0, v0}, Lepson/print/ActivityNfcPrinter;->setResult(I)V

    .line 252
    invoke-virtual {p0}, Lepson/print/ActivityNfcPrinter;->finish()V

    goto/16 :goto_0

    .line 246
    :pswitch_2
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, p3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :pswitch_3
    packed-switch p2, :pswitch_data_2

    goto :goto_0

    .line 273
    :pswitch_4
    invoke-static {}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->getLastDetailError()I

    move-result p1

    const/4 p2, 0x1

    if-ne p1, p2, :cond_0

    .line 274
    invoke-virtual {p0, v0}, Lepson/print/ActivityNfcPrinter;->setResult(I)V

    .line 275
    invoke-virtual {p0}, Lepson/print/ActivityNfcPrinter;->finish()V

    goto :goto_0

    .line 281
    :cond_0
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter;->retrystatus:Lepson/print/ActivityNfcPrinter$NfcSimpleApRetryStatus;

    sget-object p2, Lepson/print/ActivityNfcPrinter$NfcSimpleApRetryStatus;->FIRST_CONNECTION:Lepson/print/ActivityNfcPrinter$NfcSimpleApRetryStatus;

    if-ne p1, p2, :cond_1

    iget-object p1, p0, Lepson/print/ActivityNfcPrinter;->strInitPass:Ljava/lang/String;

    if-eqz p1, :cond_1

    .line 282
    sget-object p1, Lepson/print/ActivityNfcPrinter$NfcSimpleApRetryStatus;->RETRY_CONNECTION:Lepson/print/ActivityNfcPrinter$NfcSimpleApRetryStatus;

    iput-object p1, p0, Lepson/print/ActivityNfcPrinter;->retrystatus:Lepson/print/ActivityNfcPrinter$NfcSimpleApRetryStatus;

    .line 283
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter;->mHandler:Landroid/os/Handler;

    const/16 p2, 0xa

    invoke-virtual {p1, p2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 288
    :cond_1
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter;->connectInfo:Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;->isTagWithInterface()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 290
    iget p1, p0, Lepson/print/ActivityNfcPrinter;->printerStatus:I

    and-int/lit16 p1, p1, 0x100

    if-eqz p1, :cond_2

    const p1, 0x7f0e03e7

    const p2, 0x7f0e03e6

    .line 292
    invoke-virtual {p0, p1, p2}, Lepson/print/ActivityNfcPrinter;->showErrorMessage(II)V

    goto :goto_0

    :cond_2
    const p1, 0x7f0e03ed

    const p2, 0x7f0e03de

    .line 299
    invoke-virtual {p0, p1, p2}, Lepson/print/ActivityNfcPrinter;->showErrorMessage(II)V

    goto :goto_0

    .line 263
    :pswitch_5
    invoke-static {}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->setFlagSimpleAPCreated()V

    .line 266
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter;->mHandler:Landroid/os/Handler;

    const/16 p2, 0xb

    invoke-virtual {p1, p2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :pswitch_6
    packed-switch p2, :pswitch_data_3

    goto :goto_0

    .line 234
    :pswitch_7
    invoke-virtual {p0, v0}, Lepson/print/ActivityNfcPrinter;->setResult(I)V

    .line 235
    invoke-virtual {p0}, Lepson/print/ActivityNfcPrinter;->finish()V

    goto :goto_0

    .line 229
    :pswitch_8
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, p3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_3
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch -0x1
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch -0x1
        :pswitch_5
        :pswitch_4
    .end packed-switch

    :pswitch_data_3
    .packed-switch -0x1
        :pswitch_8
        :pswitch_7
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .line 152
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 155
    sget-object p1, Lepson/print/ActivityNfcPrinter;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    const/4 v0, 0x2

    invoke-virtual {p1, p0, v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doInitDriver(Landroid/content/Context;I)I

    .line 158
    new-instance p1, Lepson/print/screen/WorkingDialog;

    invoke-direct {p1, p0}, Lepson/print/screen/WorkingDialog;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lepson/print/ActivityNfcPrinter;->progress:Lepson/print/screen/WorkingDialog;

    .line 161
    invoke-virtual {p0}, Lepson/print/ActivityNfcPrinter;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "connectInfo"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;

    iput-object p1, p0, Lepson/print/ActivityNfcPrinter;->connectInfo:Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;

    .line 162
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter;->connectInfo:Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;->macAdress:Ljava/lang/String;

    iput-object p1, p0, Lepson/print/ActivityNfcPrinter;->strMacAdress:Ljava/lang/String;

    .line 164
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter;->connectInfo:Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;->ssid:Ljava/lang/String;

    iput-object p1, p0, Lepson/print/ActivityNfcPrinter;->strSSID:Ljava/lang/String;

    .line 165
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter;->connectInfo:Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;->password:Ljava/lang/String;

    iput-object p1, p0, Lepson/print/ActivityNfcPrinter;->strPass:Ljava/lang/String;

    .line 166
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter;->connectInfo:Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;->initialpassword:Ljava/lang/String;

    iput-object p1, p0, Lepson/print/ActivityNfcPrinter;->strInitPass:Ljava/lang/String;

    .line 167
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter;->connectInfo:Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;->ipAddressV4:Ljava/lang/String;

    iput-object p1, p0, Lepson/print/ActivityNfcPrinter;->strIpAddressV4:Ljava/lang/String;

    .line 168
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter;->connectInfo:Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;

    iget-object p1, p1, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;->ipAddressV4SimpleAP:Ljava/lang/String;

    iput-object p1, p0, Lepson/print/ActivityNfcPrinter;->strIpAddressV4SimpleAP:Ljava/lang/String;

    .line 169
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter;->connectInfo:Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;

    iget p1, p1, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;->printerStatus:I

    iput p1, p0, Lepson/print/ActivityNfcPrinter;->printerStatus:I

    .line 172
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter;->connectInfo:Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;

    invoke-virtual {p1, p0}, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;->hasOwnAAR(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_0

    const p1, 0x7f0e03e0

    const v0, 0x7f0e03df

    .line 173
    invoke-virtual {p0, p1, v0}, Lepson/print/ActivityNfcPrinter;->showErrorMessage(II)V

    return-void

    .line 179
    :cond_0
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter;->connectInfo:Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;->isTagWithInterface()Z

    move-result p1

    const/4 v0, 0x1

    if-eqz p1, :cond_1

    .line 180
    iget p1, p0, Lepson/print/ActivityNfcPrinter;->printerStatus:I

    and-int/2addr p1, v0

    if-nez p1, :cond_1

    const p1, 0x7f0e03eb

    const v0, 0x7f0e03ea

    .line 182
    invoke-virtual {p0, p1, v0}, Lepson/print/ActivityNfcPrinter;->showErrorMessage(II)V

    return-void

    .line 191
    :cond_1
    invoke-virtual {p0}, Lepson/print/ActivityNfcPrinter;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v1, "changeMode"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p1

    iput p1, p0, Lepson/print/ActivityNfcPrinter;->nChangeMode:I

    .line 193
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->isWifiEnabled(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_2

    .line 195
    sget-object p1, Lepson/print/ActivityNfcPrinter$NfcStatus;->ENABLING_WIFI:Lepson/print/ActivityNfcPrinter$NfcStatus;

    iput-object p1, p0, Lepson/print/ActivityNfcPrinter;->status:Lepson/print/ActivityNfcPrinter$NfcStatus;

    .line 196
    invoke-static {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->enableWiFi(Landroid/app/Activity;I)V

    goto :goto_0

    .line 199
    :cond_2
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter;->mHandler:Landroid/os/Handler;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_0
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .line 206
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 209
    iget-object v0, p0, Lepson/print/ActivityNfcPrinter;->progress:Lepson/print/screen/WorkingDialog;

    invoke-virtual {v0}, Lepson/print/screen/WorkingDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 210
    iget-object v0, p0, Lepson/print/ActivityNfcPrinter;->progress:Lepson/print/screen/WorkingDialog;

    invoke-virtual {v0}, Lepson/print/screen/WorkingDialog;->dismiss()V

    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 2

    const-string v0, "ActivityChangeWifiPrinter"

    const-string v1, "onPause"

    .line 324
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 327
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils;->disableForegroundDispatch(Landroid/app/Activity;)V

    return-void
.end method

.method protected onResume()V
    .locals 3

    const-string v0, "ActivityChangeWifiPrinter"

    .line 314
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onResume status="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lepson/print/ActivityNfcPrinter;->status:Lepson/print/ActivityNfcPrinter$NfcStatus;

    invoke-virtual {v2}, Lepson/print/ActivityNfcPrinter$NfcStatus;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 315
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const/4 v0, 0x0

    .line 318
    move-object v1, v0

    check-cast v1, [[Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils;->enableForegroundDispatch(Landroid/app/Activity;[Landroid/content/IntentFilter;[[Ljava/lang/String;)V

    return-void
.end method

.method showErrorMessage(II)V
    .locals 2

    .line 1005
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->isSimpleAP(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1007
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->getCurSSID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    .line 1006
    invoke-static {p0, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->disconnectSimpleAP(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    .line 1011
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 1012
    invoke-virtual {p0, p1}, Lepson/print/ActivityNfcPrinter;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 1013
    invoke-virtual {p0, p2}, Lepson/print/ActivityNfcPrinter;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    const/4 p1, 0x0

    .line 1014
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog;->setCancelable(Z)V

    const/4 p1, -0x1

    const p2, 0x7f0e04f2

    .line 1015
    invoke-virtual {p0, p2}, Lepson/print/ActivityNfcPrinter;->getString(I)Ljava/lang/String;

    move-result-object p2

    new-instance v1, Lepson/print/ActivityNfcPrinter$2;

    invoke-direct {v1, p0, v0}, Lepson/print/ActivityNfcPrinter$2;-><init>(Lepson/print/ActivityNfcPrinter;Landroid/app/AlertDialog;)V

    invoke-virtual {v0, p1, p2, v1}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 1023
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

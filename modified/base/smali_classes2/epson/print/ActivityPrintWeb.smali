.class public Lepson/print/ActivityPrintWeb;
.super Lepson/print/ActivityIACommon;
.source "ActivityPrintWeb.java"

# interfaces
.implements Lepson/print/CommonDefine;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/epson/mobilephone/common/ReviewInvitationDialog$OnClickListener;


# static fields
.field private static final DIALOG_KEY_STORE_DIALOG:Ljava/lang/String; = "store-dialog"

.field public static final PARAMS_KEY_WEB_PRINT_LOG:Ljava/lang/String; = "print_log"

.field public static final PREFS_NAME:Ljava/lang/String; = "PrintSetting"

.field public static final REFRESH:Z = false

.field private static final TAG:Ljava/lang/String; = "ActivityPrintWeb"


# instance fields
.field private final ERROR_IMAGE_CODE:I

.field private final ERROR_PRINT_SETTING:I

.field private final INIT:I

.field private final REDRAW_PREVIEW:I

.field private Relative_para:Landroid/widget/RelativeLayout$LayoutParams;

.field private final START_PRINT:I

.field private aPaperSourceSetting:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lepson/print/screen/PaperSourceSetting;",
            ">;"
        }
    .end annotation
.end field

.field private addImageView:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field bAutoStartPrint:Z

.field private bm:Landroid/graphics/Bitmap;

.field private enableShowWarning:Z

.field private fileName2User:Ljava/lang/String;

.field private fromActivity:I

.field private imageList:Lepson/print/EPImageList;

.field private isCustomAction:Z

.field private isRemotePrinter:Z

.field private mCallback:Lepson/print/service/IEpsonServiceCallback;

.field private mColorMode:I

.field private mContext:Landroid/content/Context;

.field private mCurrentPage:I

.field private mDisplay:Landroid/view/Display;

.field private mEndPage:I

.field private mEpsonConnection:Landroid/content/ServiceConnection;

.field private mEpsonService:Lepson/print/service/IEpsonService;

.field private mFileTotalPages:I

.field private mImageView:Landroid/widget/ImageView;

.field private mIsPortrait:Z

.field private mLayout:I

.field private mLayoutMulti:I

.field private mLn_zoomview:Landroid/widget/LinearLayout;

.field private mMoveX:F

.field private mNext:Landroid/widget/Button;

.field private mPageNum:Landroid/widget/TextView;

.field private mPaperSize:Lepson/common/Info_paper;

.field private mPre:Landroid/widget/Button;

.field private mPreviewPaperAreaLandscape:Z

.field private mPrint:Landroid/widget/Button;

.field private mPrintImageList:Lepson/print/EPImageList;

.field private mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;

.field private mReviewInvitationViewModel:Lcom/epson/mobilephone/common/ReviewInvitationViewModel;

.field private mRl_zoomview:Landroid/widget/RelativeLayout;

.field private mSizeInfo:Landroid/widget/TextView;

.field private mStartPage:I

.field private mStartX:F

.field mUiHandler:Landroid/os/Handler;

.field private paperMissmath:Landroid/widget/ImageView;

.field private paperSize:I

.field paperSourceInfo:Lepson/print/screen/PaperSourceInfo;

.field printWeb:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private printerId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 4

    .line 66
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    .line 92
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lepson/print/ActivityPrintWeb;->addImageView:Ljava/util/ArrayList;

    const/4 v0, 0x0

    .line 95
    iput-object v0, p0, Lepson/print/ActivityPrintWeb;->paperMissmath:Landroid/widget/ImageView;

    const/4 v1, 0x1

    .line 100
    iput v1, p0, Lepson/print/ActivityPrintWeb;->ERROR_IMAGE_CODE:I

    const/4 v2, 0x2

    .line 101
    iput v2, p0, Lepson/print/ActivityPrintWeb;->ERROR_PRINT_SETTING:I

    const/4 v3, 0x4

    .line 102
    iput v3, p0, Lepson/print/ActivityPrintWeb;->START_PRINT:I

    const/4 v3, 0x5

    .line 103
    iput v3, p0, Lepson/print/ActivityPrintWeb;->INIT:I

    const/4 v3, 0x6

    .line 104
    iput v3, p0, Lepson/print/ActivityPrintWeb;->REDRAW_PREVIEW:I

    .line 106
    iput-object v0, p0, Lepson/print/ActivityPrintWeb;->bm:Landroid/graphics/Bitmap;

    .line 107
    new-instance v3, Lepson/print/EPImageList;

    invoke-direct {v3}, Lepson/print/EPImageList;-><init>()V

    iput-object v3, p0, Lepson/print/ActivityPrintWeb;->imageList:Lepson/print/EPImageList;

    .line 110
    iput-object v0, p0, Lepson/print/ActivityPrintWeb;->mContext:Landroid/content/Context;

    .line 113
    iput-object v0, p0, Lepson/print/ActivityPrintWeb;->paperSourceInfo:Lepson/print/screen/PaperSourceInfo;

    const/4 v3, 0x0

    .line 121
    iput-boolean v3, p0, Lepson/print/ActivityPrintWeb;->isRemotePrinter:Z

    .line 122
    iput-boolean v1, p0, Lepson/print/ActivityPrintWeb;->enableShowWarning:Z

    .line 123
    iput v2, p0, Lepson/print/ActivityPrintWeb;->fromActivity:I

    .line 124
    iput-boolean v3, p0, Lepson/print/ActivityPrintWeb;->isCustomAction:Z

    .line 129
    iput-boolean v3, p0, Lepson/print/ActivityPrintWeb;->bAutoStartPrint:Z

    .line 331
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lepson/print/ActivityPrintWeb;->printWeb:Ljava/util/ArrayList;

    .line 480
    new-instance v1, Lepson/print/ActivityPrintWeb$8;

    invoke-direct {v1, p0}, Lepson/print/ActivityPrintWeb$8;-><init>(Lepson/print/ActivityPrintWeb;)V

    iput-object v1, p0, Lepson/print/ActivityPrintWeb;->mUiHandler:Landroid/os/Handler;

    .line 1086
    iput-object v0, p0, Lepson/print/ActivityPrintWeb;->mEpsonService:Lepson/print/service/IEpsonService;

    .line 1087
    new-instance v0, Lepson/print/ActivityPrintWeb$9;

    invoke-direct {v0, p0}, Lepson/print/ActivityPrintWeb$9;-><init>(Lepson/print/ActivityPrintWeb;)V

    iput-object v0, p0, Lepson/print/ActivityPrintWeb;->mEpsonConnection:Landroid/content/ServiceConnection;

    .line 1112
    new-instance v0, Lepson/print/ActivityPrintWeb$10;

    invoke-direct {v0, p0}, Lepson/print/ActivityPrintWeb$10;-><init>(Lepson/print/ActivityPrintWeb;)V

    iput-object v0, p0, Lepson/print/ActivityPrintWeb;->mCallback:Lepson/print/service/IEpsonServiceCallback;

    return-void
.end method

.method static synthetic access$000(Lepson/print/ActivityPrintWeb;)V
    .locals 0

    .line 66
    invoke-direct {p0}, Lepson/print/ActivityPrintWeb;->showStoreDialog()V

    return-void
.end method

.method static synthetic access$100(Lepson/print/ActivityPrintWeb;)Landroid/widget/Button;
    .locals 0

    .line 66
    iget-object p0, p0, Lepson/print/ActivityPrintWeb;->mPre:Landroid/widget/Button;

    return-object p0
.end method

.method static synthetic access$1000(Lepson/print/ActivityPrintWeb;)Landroid/widget/LinearLayout;
    .locals 0

    .line 66
    iget-object p0, p0, Lepson/print/ActivityPrintWeb;->mLn_zoomview:Landroid/widget/LinearLayout;

    return-object p0
.end method

.method static synthetic access$1100(Lepson/print/ActivityPrintWeb;)Landroid/widget/Button;
    .locals 0

    .line 66
    iget-object p0, p0, Lepson/print/ActivityPrintWeb;->mPrint:Landroid/widget/Button;

    return-object p0
.end method

.method static synthetic access$1200(Lepson/print/ActivityPrintWeb;)Landroid/widget/RelativeLayout;
    .locals 0

    .line 66
    iget-object p0, p0, Lepson/print/ActivityPrintWeb;->mRl_zoomview:Landroid/widget/RelativeLayout;

    return-object p0
.end method

.method static synthetic access$1300(Lepson/print/ActivityPrintWeb;)Landroid/content/Context;
    .locals 0

    .line 66
    iget-object p0, p0, Lepson/print/ActivityPrintWeb;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic access$1400(Lepson/print/ActivityPrintWeb;)I
    .locals 0

    .line 66
    iget p0, p0, Lepson/print/ActivityPrintWeb;->mStartPage:I

    return p0
.end method

.method static synthetic access$1500(Lepson/print/ActivityPrintWeb;)I
    .locals 0

    .line 66
    iget p0, p0, Lepson/print/ActivityPrintWeb;->mEndPage:I

    return p0
.end method

.method static synthetic access$1600(Lepson/print/ActivityPrintWeb;)Lepson/print/EPImageList;
    .locals 0

    .line 66
    iget-object p0, p0, Lepson/print/ActivityPrintWeb;->imageList:Lepson/print/EPImageList;

    return-object p0
.end method

.method static synthetic access$1700(Lepson/print/ActivityPrintWeb;)Landroid/widget/ImageView;
    .locals 0

    .line 66
    iget-object p0, p0, Lepson/print/ActivityPrintWeb;->mImageView:Landroid/widget/ImageView;

    return-object p0
.end method

.method static synthetic access$1800(Lepson/print/ActivityPrintWeb;)Z
    .locals 0

    .line 66
    iget-boolean p0, p0, Lepson/print/ActivityPrintWeb;->mPreviewPaperAreaLandscape:Z

    return p0
.end method

.method static synthetic access$1900(Lepson/print/ActivityPrintWeb;)I
    .locals 0

    .line 66
    iget p0, p0, Lepson/print/ActivityPrintWeb;->fromActivity:I

    return p0
.end method

.method static synthetic access$200(Lepson/print/ActivityPrintWeb;)Landroid/widget/Button;
    .locals 0

    .line 66
    iget-object p0, p0, Lepson/print/ActivityPrintWeb;->mNext:Landroid/widget/Button;

    return-object p0
.end method

.method static synthetic access$2002(Lepson/print/ActivityPrintWeb;Lepson/print/EPImageList;)Lepson/print/EPImageList;
    .locals 0

    .line 66
    iput-object p1, p0, Lepson/print/ActivityPrintWeb;->mPrintImageList:Lepson/print/EPImageList;

    return-object p1
.end method

.method static synthetic access$2100(Lepson/print/ActivityPrintWeb;)Ljava/util/ArrayList;
    .locals 0

    .line 66
    iget-object p0, p0, Lepson/print/ActivityPrintWeb;->aPaperSourceSetting:Ljava/util/ArrayList;

    return-object p0
.end method

.method static synthetic access$2102(Lepson/print/ActivityPrintWeb;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .line 66
    iput-object p1, p0, Lepson/print/ActivityPrintWeb;->aPaperSourceSetting:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic access$2200(Lepson/print/ActivityPrintWeb;)Landroid/widget/TextView;
    .locals 0

    .line 66
    iget-object p0, p0, Lepson/print/ActivityPrintWeb;->mSizeInfo:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$2300(Lepson/print/ActivityPrintWeb;)Landroid/widget/ImageView;
    .locals 0

    .line 66
    iget-object p0, p0, Lepson/print/ActivityPrintWeb;->paperMissmath:Landroid/widget/ImageView;

    return-object p0
.end method

.method static synthetic access$2400(Lepson/print/ActivityPrintWeb;)Lepson/print/service/IEpsonServiceCallback;
    .locals 0

    .line 66
    iget-object p0, p0, Lepson/print/ActivityPrintWeb;->mCallback:Lepson/print/service/IEpsonServiceCallback;

    return-object p0
.end method

.method static synthetic access$300(Lepson/print/ActivityPrintWeb;)F
    .locals 0

    .line 66
    iget p0, p0, Lepson/print/ActivityPrintWeb;->mStartX:F

    return p0
.end method

.method static synthetic access$302(Lepson/print/ActivityPrintWeb;F)F
    .locals 0

    .line 66
    iput p1, p0, Lepson/print/ActivityPrintWeb;->mStartX:F

    return p1
.end method

.method static synthetic access$400(Lepson/print/ActivityPrintWeb;)F
    .locals 0

    .line 66
    iget p0, p0, Lepson/print/ActivityPrintWeb;->mMoveX:F

    return p0
.end method

.method static synthetic access$402(Lepson/print/ActivityPrintWeb;F)F
    .locals 0

    .line 66
    iput p1, p0, Lepson/print/ActivityPrintWeb;->mMoveX:F

    return p1
.end method

.method static synthetic access$500(Lepson/print/ActivityPrintWeb;)V
    .locals 0

    .line 66
    invoke-direct {p0}, Lepson/print/ActivityPrintWeb;->nextPage()V

    return-void
.end method

.method static synthetic access$600(Lepson/print/ActivityPrintWeb;)V
    .locals 0

    .line 66
    invoke-direct {p0}, Lepson/print/ActivityPrintWeb;->prevPage()V

    return-void
.end method

.method static synthetic access$700(Lepson/print/ActivityPrintWeb;)Lepson/print/service/IEpsonService;
    .locals 0

    .line 66
    iget-object p0, p0, Lepson/print/ActivityPrintWeb;->mEpsonService:Lepson/print/service/IEpsonService;

    return-object p0
.end method

.method static synthetic access$702(Lepson/print/ActivityPrintWeb;Lepson/print/service/IEpsonService;)Lepson/print/service/IEpsonService;
    .locals 0

    .line 66
    iput-object p1, p0, Lepson/print/ActivityPrintWeb;->mEpsonService:Lepson/print/service/IEpsonService;

    return-object p1
.end method

.method static synthetic access$800(Lepson/print/ActivityPrintWeb;)Z
    .locals 0

    .line 66
    iget-boolean p0, p0, Lepson/print/ActivityPrintWeb;->isRemotePrinter:Z

    return p0
.end method

.method static synthetic access$900(Lepson/print/ActivityPrintWeb;)Z
    .locals 0

    .line 66
    iget-boolean p0, p0, Lepson/print/ActivityPrintWeb;->mIsPortrait:Z

    return p0
.end method

.method private loadConfig()V
    .locals 5

    const-string v0, "PrintSetting"

    const/4 v1, 0x0

    .line 1026
    invoke-virtual {p0, v0, v1}, Lepson/print/ActivityPrintWeb;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1032
    new-instance v1, Lepson/print/screen/PrintSetting;

    sget-object v2, Lepson/print/screen/PrintSetting$Kind;->document:Lepson/print/screen/PrintSetting$Kind;

    invoke-direct {v1, p0, v2}, Lepson/print/screen/PrintSetting;-><init>(Landroid/content/Context;Lepson/print/screen/PrintSetting$Kind;)V

    .line 1033
    invoke-virtual {v1}, Lepson/print/screen/PrintSetting;->loadSettings()V

    .line 1035
    iget v2, v1, Lepson/print/screen/PrintSetting;->paperSizeValue:I

    iput v2, p0, Lepson/print/ActivityPrintWeb;->paperSize:I

    .line 1037
    iget v2, v1, Lepson/print/screen/PrintSetting;->colorValue:I

    iput v2, p0, Lepson/print/ActivityPrintWeb;->mColorMode:I

    .line 1038
    iget v2, v1, Lepson/print/screen/PrintSetting;->layoutValue:I

    iput v2, p0, Lepson/print/ActivityPrintWeb;->mLayout:I

    .line 1039
    iget v2, v1, Lepson/print/screen/PrintSetting;->layoutMultiPageValue:I

    iput v2, p0, Lepson/print/ActivityPrintWeb;->mLayoutMulti:I

    .line 1041
    iget v2, p0, Lepson/print/ActivityPrintWeb;->paperSize:I

    invoke-static {p0, v2}, Lepson/common/Info_paper;->getInfoPaper(Landroid/content/Context;I)Lepson/common/Info_paper;

    move-result-object v2

    iput-object v2, p0, Lepson/print/ActivityPrintWeb;->mPaperSize:Lepson/common/Info_paper;

    .line 1043
    new-instance v2, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize;

    invoke-direct {v2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize;-><init>()V

    .line 1044
    iget-object v3, p0, Lepson/print/ActivityPrintWeb;->mSizeInfo:Landroid/widget/TextView;

    iget v4, p0, Lepson/print/ActivityPrintWeb;->paperSize:I

    invoke-virtual {v2, v4}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result v4

    invoke-virtual {p0, v4}, Lepson/print/ActivityPrintWeb;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1045
    invoke-virtual {v2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    const-string v2, "PRINTER_NAME"

    const v3, 0x7f0e04d6

    .line 1047
    invoke-virtual {p0, v3}, Lepson/print/ActivityPrintWeb;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1046
    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lepson/print/ActivityPrintWeb;->fileName2User:Ljava/lang/String;

    const-string v2, "PRINTER_ID"

    const/4 v3, 0x0

    .line 1048
    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepson/print/ActivityPrintWeb;->printerId:Ljava/lang/String;

    .line 1049
    iget v0, v1, Lepson/print/screen/PrintSetting;->startValue:I

    iput v0, p0, Lepson/print/ActivityPrintWeb;->mStartPage:I

    .line 1050
    iget v0, v1, Lepson/print/screen/PrintSetting;->endValue:I

    iput v0, p0, Lepson/print/ActivityPrintWeb;->mEndPage:I

    return-void
.end method

.method private nextPage()V
    .locals 2

    .line 400
    iget v0, p0, Lepson/print/ActivityPrintWeb;->mCurrentPage:I

    iget v1, p0, Lepson/print/ActivityPrintWeb;->mFileTotalPages:I

    if-ge v0, v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    .line 401
    iput v0, p0, Lepson/print/ActivityPrintWeb;->mCurrentPage:I

    .line 402
    iget v0, p0, Lepson/print/ActivityPrintWeb;->mCurrentPage:I

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, Lepson/print/ActivityPrintWeb;->setNewImageView(I)V

    .line 403
    invoke-virtual {p0}, Lepson/print/ActivityPrintWeb;->updateTextView()V

    .line 404
    invoke-direct {p0}, Lepson/print/ActivityPrintWeb;->updateButton()V

    :cond_0
    return-void
.end method

.method private prevPage()V
    .locals 2

    .line 409
    iget v0, p0, Lepson/print/ActivityPrintWeb;->mCurrentPage:I

    const/4 v1, 0x1

    if-ge v1, v0, :cond_0

    sub-int/2addr v0, v1

    .line 410
    iput v0, p0, Lepson/print/ActivityPrintWeb;->mCurrentPage:I

    .line 411
    iget v0, p0, Lepson/print/ActivityPrintWeb;->mCurrentPage:I

    sub-int/2addr v0, v1

    invoke-direct {p0, v0}, Lepson/print/ActivityPrintWeb;->setNewImageView(I)V

    .line 412
    invoke-virtual {p0}, Lepson/print/ActivityPrintWeb;->updateTextView()V

    .line 413
    invoke-direct {p0}, Lepson/print/ActivityPrintWeb;->updateButton()V

    :cond_0
    return-void
.end method

.method private setNewImageView(I)V
    .locals 5

    const-string v0, "Current page :"

    .line 827
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 829
    :try_start_0
    iget-object v1, p0, Lepson/print/ActivityPrintWeb;->addImageView:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x320

    const/16 v4, 0x400

    invoke-static {v1, v4, v3, v2}, Lepson/print/Util/Photo;->createBitmapWithUri(Ljava/lang/String;IIZ)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lepson/print/ActivityPrintWeb;->bm:Landroid/graphics/Bitmap;

    .line 831
    iget-object v1, p0, Lepson/print/ActivityPrintWeb;->bm:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lepson/print/ActivityPrintWeb;->mImageView:Landroid/widget/ImageView;

    if-eqz v1, :cond_1

    .line 832
    iget v1, p0, Lepson/print/ActivityPrintWeb;->mColorMode:I

    if-ne v1, v0, :cond_0

    .line 833
    iget-object v1, p0, Lepson/print/ActivityPrintWeb;->addImageView:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    invoke-static {p1, v4, v3, v0}, Lepson/print/Util/Photo;->createBitmapWithUri(Ljava/lang/String;IIZ)Landroid/graphics/Bitmap;

    move-result-object p1

    iput-object p1, p0, Lepson/print/ActivityPrintWeb;->bm:Landroid/graphics/Bitmap;

    .line 837
    :cond_0
    iget-object p1, p0, Lepson/print/ActivityPrintWeb;->mImageView:Landroid/widget/ImageView;

    iget-object v1, p0, Lepson/print/ActivityPrintWeb;->bm:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 839
    invoke-static {}, Ljava/lang/System;->gc()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 842
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 843
    iget-object p1, p0, Lepson/print/ActivityPrintWeb;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_1
    :goto_0
    return-void
.end method

.method private showStoreDialog()V
    .locals 3

    .line 327
    invoke-static {}, Lcom/epson/mobilephone/common/ReviewInvitationDialog;->newInstance()Lcom/epson/mobilephone/common/ReviewInvitationDialog;

    move-result-object v0

    .line 328
    invoke-virtual {p0}, Lepson/print/ActivityPrintWeb;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "store-dialog"

    invoke-virtual {v0, v1, v2}, Lcom/epson/mobilephone/common/ReviewInvitationDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method private startPrint()V
    .locals 4

    .line 468
    iget-object v0, p0, Lepson/print/ActivityPrintWeb;->mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;

    if-nez v0, :cond_0

    .line 470
    new-instance v0, Lcom/epson/iprint/prtlogger/PrintLog;

    invoke-direct {v0}, Lcom/epson/iprint/prtlogger/PrintLog;-><init>()V

    iput-object v0, p0, Lepson/print/ActivityPrintWeb;->mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;

    .line 473
    :cond_0
    iget-object v0, p0, Lepson/print/ActivityPrintWeb;->mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;

    const/4 v1, 0x3

    iput v1, v0, Lcom/epson/iprint/prtlogger/PrintLog;->previewType:I

    .line 474
    iget-object v1, p0, Lepson/print/ActivityPrintWeb;->mPrintImageList:Lepson/print/EPImageList;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {p0, v1, v2, v3, v0}, Lepson/print/screen/PrintProgress;->getPrintIntent(Landroid/content/Context;Lepson/print/EPImageList;ZZLcom/epson/iprint/prtlogger/PrintLog;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0xff

    .line 476
    invoke-virtual {p0, v0, v1}, Lepson/print/ActivityPrintWeb;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private updateButton()V
    .locals 5

    .line 418
    iget v0, p0, Lepson/print/ActivityPrintWeb;->mFileTotalPages:I

    const/4 v1, 0x1

    const/16 v2, 0x8

    if-le v0, v1, :cond_3

    .line 419
    iget v3, p0, Lepson/print/ActivityPrintWeb;->mCurrentPage:I

    const/4 v4, 0x0

    if-ne v3, v0, :cond_0

    .line 420
    iget-object v0, p0, Lepson/print/ActivityPrintWeb;->mNext:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 422
    :cond_0
    iget-object v0, p0, Lepson/print/ActivityPrintWeb;->mNext:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 425
    :goto_0
    iget v0, p0, Lepson/print/ActivityPrintWeb;->mCurrentPage:I

    if-eq v0, v1, :cond_2

    if-nez v0, :cond_1

    goto :goto_1

    .line 428
    :cond_1
    iget-object v0, p0, Lepson/print/ActivityPrintWeb;->mPre:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_2

    .line 426
    :cond_2
    :goto_1
    iget-object v0, p0, Lepson/print/ActivityPrintWeb;->mPre:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_2

    .line 431
    :cond_3
    iget-object v0, p0, Lepson/print/ActivityPrintWeb;->mNext:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 432
    iget-object v0, p0, Lepson/print/ActivityPrintWeb;->mPre:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    :goto_2
    return-void
.end method


# virtual methods
.method public callPrintSetting()V
    .locals 5

    :try_start_0
    const-string v0, "PrintSetting"

    const/4 v1, 0x0

    .line 850
    invoke-virtual {p0, v0, v1}, Lepson/print/ActivityPrintWeb;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 851
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "SOURCE_TYPE"

    const/4 v3, 0x3

    .line 852
    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 853
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 855
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lepson/print/screen/SettingScr;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "PRINT_DOCUMENT"

    const/4 v3, 0x1

    .line 857
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v2, "SHEETS"

    .line 858
    iget v4, p0, Lepson/print/ActivityPrintWeb;->mFileTotalPages:I

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    .line 859
    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "PrintSetting"

    .line 861
    invoke-virtual {p0, v2, v1}, Lepson/print/ActivityPrintWeb;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 862
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "SOURCE_TYPE"

    .line 864
    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 865
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    const/4 v1, 0x6

    .line 866
    invoke-virtual {p0, v0, v1}, Lepson/print/ActivityPrintWeb;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 868
    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->printStackTrace()V

    .line 869
    iget-object v0, p0, Lepson/print/ActivityPrintWeb;->mUiHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_0
    return-void
.end method

.method check3GAndStartPrint()V
    .locals 0

    .line 459
    invoke-direct {p0}, Lepson/print/ActivityPrintWeb;->startPrint()V

    return-void
.end method

.method public invitationDialogClicked(Z)V
    .locals 0

    .line 1083
    iget-object p1, p0, Lepson/print/ActivityPrintWeb;->mReviewInvitationViewModel:Lcom/epson/mobilephone/common/ReviewInvitationViewModel;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/ReviewInvitationViewModel;->setStartStoreEnd()V

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    const-string p3, "ActivityPrintWeb"

    .line 951
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onActivityResult requestCode = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p3, v0}, Lepson/print/Util/EPLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    const/16 p3, 0xff

    const/4 v0, 0x1

    const/4 v1, -0x1

    if-eq p1, p3, :cond_0

    packed-switch p1, :pswitch_data_0

    goto/16 :goto_1

    :pswitch_0
    const/4 p1, 0x3

    if-ne p2, p1, :cond_5

    .line 997
    invoke-static {p0}, Lepson/print/MyPrinter;->isRemotePrinter(Landroid/content/Context;)Z

    move-result p1

    iput-boolean p1, p0, Lepson/print/ActivityPrintWeb;->isRemotePrinter:Z

    .line 1000
    iget-boolean p1, p0, Lepson/print/ActivityPrintWeb;->mIsPortrait:Z

    invoke-virtual {p0, p1}, Lepson/print/ActivityPrintWeb;->setOrentationView(Z)V

    goto/16 :goto_1

    :pswitch_1
    if-ne p2, v1, :cond_5

    .line 1010
    invoke-static {p0}, Lepson/print/MyPrinter;->isRemotePrinter(Landroid/content/Context;)Z

    move-result p1

    iput-boolean p1, p0, Lepson/print/ActivityPrintWeb;->isRemotePrinter:Z

    .line 1015
    iput-boolean v0, p0, Lepson/print/ActivityPrintWeb;->bAutoStartPrint:Z

    .line 1018
    iget-object p1, p0, Lepson/print/ActivityPrintWeb;->mUiHandler:Landroid/os/Handler;

    const/4 p2, 0x5

    invoke-virtual {p1, p2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    :cond_0
    const-string p1, "ActivityPrintWeb"

    .line 955
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onActivityResult resultCode = "

    invoke-virtual {p3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-static {p1, p3}, Lepson/print/Util/EPLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p2, :cond_2

    const/4 p1, 0x4

    if-eq p2, p1, :cond_3

    const/16 p1, 0x3e8

    if-eq p2, p1, :cond_1

    goto :goto_0

    :cond_1
    const-string p1, "ActivityPrintWeb"

    const-string p2, "finish print = RESULT_ERROR"

    .line 978
    invoke-static {p1, p2}, Lepson/print/Util/EPLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const-string p1, "ActivityPrintWeb"

    const-string p3, "finish print = RESULT_CANCELED"

    .line 958
    invoke-static {p1, p3}, Lepson/print/Util/EPLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 959
    iget-boolean p1, p0, Lepson/print/ActivityPrintWeb;->isCustomAction:Z

    if-eqz p1, :cond_3

    const/4 p1, 0x0

    .line 960
    invoke-virtual {p0, p1}, Lepson/print/ActivityPrintWeb;->setResult(I)V

    .line 961
    invoke-virtual {p0}, Lepson/print/ActivityPrintWeb;->finish()V

    return-void

    :cond_3
    const-string p1, "ActivityPrintWeb"

    const-string p3, "finish print = END_PRINT"

    .line 965
    invoke-static {p1, p3}, Lepson/print/Util/EPLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 966
    iget-object p1, p0, Lepson/print/ActivityPrintWeb;->mReviewInvitationViewModel:Lcom/epson/mobilephone/common/ReviewInvitationViewModel;

    invoke-static {p2}, Lepson/print/screen/PrintProgress;->isPrintSuccess(I)Z

    move-result p2

    invoke-virtual {p1, p2}, Lcom/epson/mobilephone/common/ReviewInvitationViewModel;->onPrintEnd(Z)V

    .line 968
    iget-boolean p1, p0, Lepson/print/ActivityPrintWeb;->isCustomAction:Z

    if-eqz p1, :cond_4

    const-string p1, "ActivityPrintWeb"

    const-string p2, "*****isCustomAction"

    .line 969
    invoke-static {p1, p2}, Lepson/print/Util/EPLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 970
    invoke-virtual {p0, v1}, Lepson/print/ActivityPrintWeb;->setResult(I)V

    .line 971
    invoke-virtual {p0}, Lepson/print/ActivityPrintWeb;->finish()V

    return-void

    .line 990
    :cond_4
    :goto_0
    iget-object p1, p0, Lepson/print/ActivityPrintWeb;->mPrint:Landroid/widget/Button;

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    :cond_5
    :goto_1
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 2

    .line 1071
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onBackPressed()V

    .line 1073
    invoke-static {p0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v0

    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->initTempViewDir()V

    .line 1075
    iget-boolean v0, p0, Lepson/print/ActivityPrintWeb;->isCustomAction:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    .line 1077
    invoke-virtual {p0, v0}, Lepson/print/ActivityPrintWeb;->setResult(I)V

    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .line 333
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    const v0, 0x7f080375

    if-eq p1, v0, :cond_2

    packed-switch p1, :pswitch_data_0

    goto/16 :goto_0

    .line 344
    :pswitch_0
    invoke-static {p0}, Lepson/print/MyPrinter;->isRemotePrinter(Landroid/content/Context;)Z

    move-result p1

    iput-boolean p1, p0, Lepson/print/ActivityPrintWeb;->isRemotePrinter:Z

    .line 345
    iget-boolean p1, p0, Lepson/print/ActivityPrintWeb;->isRemotePrinter:Z

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    iget-boolean p1, p0, Lepson/print/ActivityPrintWeb;->enableShowWarning:Z

    if-ne p1, v1, :cond_0

    .line 346
    new-instance p1, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lepson/print/ActivityPrintWeb;->mContext:Landroid/content/Context;

    invoke-direct {p1, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 347
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const v1, 0x7f0e0362

    .line 348
    invoke-virtual {p0, v1}, Lepson/print/ActivityPrintWeb;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const v1, 0x7f0e0363

    .line 349
    invoke-virtual {p0, v1}, Lepson/print/ActivityPrintWeb;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 348
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const v0, 0x7f0e052b

    .line 350
    invoke-virtual {p0, v0}, Lepson/print/ActivityPrintWeb;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lepson/print/ActivityPrintWeb$6;

    invoke-direct {v1, p0}, Lepson/print/ActivityPrintWeb$6;-><init>(Lepson/print/ActivityPrintWeb;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const v0, 0x7f0e04e6

    .line 356
    invoke-virtual {p0, v0}, Lepson/print/ActivityPrintWeb;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lepson/print/ActivityPrintWeb$5;

    invoke-direct {v1, p0}, Lepson/print/ActivityPrintWeb$5;-><init>(Lepson/print/ActivityPrintWeb;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 361
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_0

    .line 362
    :cond_0
    iget-object p1, p0, Lepson/print/ActivityPrintWeb;->fileName2User:Ljava/lang/String;

    const v1, 0x7f0e04d6

    invoke-virtual {p0, v1}, Lepson/print/ActivityPrintWeb;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 364
    new-instance p1, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lepson/print/ActivityPrintWeb;->mContext:Landroid/content/Context;

    invoke-direct {p1, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 365
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const v0, 0x7f0e0439

    .line 366
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const v0, 0x7f0e043b

    .line 367
    invoke-virtual {p0, v0}, Lepson/print/ActivityPrintWeb;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const v0, 0x7f0e04f2

    .line 368
    invoke-virtual {p0, v0}, Lepson/print/ActivityPrintWeb;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lepson/print/ActivityPrintWeb$7;

    invoke-direct {v1, p0}, Lepson/print/ActivityPrintWeb$7;-><init>(Lepson/print/ActivityPrintWeb;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 373
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0

    .line 375
    :cond_1
    iget-object p1, p0, Lepson/print/ActivityPrintWeb;->mUiHandler:Landroid/os/Handler;

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 335
    :pswitch_1
    invoke-direct {p0}, Lepson/print/ActivityPrintWeb;->prevPage()V

    goto :goto_0

    .line 339
    :pswitch_2
    invoke-direct {p0}, Lepson/print/ActivityPrintWeb;->nextPage()V

    goto :goto_0

    .line 381
    :cond_2
    iget-object p1, p0, Lepson/print/ActivityPrintWeb;->aPaperSourceSetting:Ljava/util/ArrayList;

    if-eqz p1, :cond_3

    .line 383
    new-instance p1, Landroid/content/Intent;

    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    .line 384
    const-class v0, Lepson/print/screen/PaperSourceSettingScr;

    invoke-virtual {p1, p0, v0}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v0, "print-setting-type"

    .line 386
    sget-object v1, Lepson/print/screen/PrintSetting$Kind;->document:Lepson/print/screen/PrintSetting$Kind;

    .line 387
    invoke-virtual {v1}, Lepson/print/screen/PrintSetting$Kind;->name()Ljava/lang/String;

    move-result-object v1

    .line 386
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "PAPERSOURCEINFO"

    .line 388
    iget-object v1, p0, Lepson/print/ActivityPrintWeb;->aPaperSourceSetting:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    const/4 v0, 0x6

    .line 389
    invoke-virtual {p0, p1, v0}, Lepson/print/ActivityPrintWeb;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 394
    :cond_3
    invoke-virtual {p0}, Lepson/print/ActivityPrintWeb;->callPrintSetting()V

    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x7f0800a0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .line 666
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 667
    iget-boolean p1, p0, Lepson/print/ActivityPrintWeb;->mIsPortrait:Z

    invoke-virtual {p0, p1}, Lepson/print/ActivityPrintWeb;->setOrentationView(Z)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .line 138
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0a00ab

    .line 139
    invoke-virtual {p0, p1}, Lepson/print/ActivityPrintWeb;->setContentView(I)V

    .line 140
    iput-object p0, p0, Lepson/print/ActivityPrintWeb;->mContext:Landroid/content/Context;

    const p1, 0x7f0e0546

    const/4 v0, 0x1

    .line 143
    invoke-virtual {p0, p1, v0}, Lepson/print/ActivityPrintWeb;->setActionBar(IZ)V

    .line 145
    invoke-static {p0}, Landroid/arch/lifecycle/ViewModelProviders;->of(Landroid/support/v4/app/FragmentActivity;)Landroid/arch/lifecycle/ViewModelProvider;

    move-result-object v1

    const-class v2, Lcom/epson/mobilephone/common/ReviewInvitationViewModel;

    invoke-virtual {v1, v2}, Landroid/arch/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object v1

    check-cast v1, Lcom/epson/mobilephone/common/ReviewInvitationViewModel;

    iput-object v1, p0, Lepson/print/ActivityPrintWeb;->mReviewInvitationViewModel:Lcom/epson/mobilephone/common/ReviewInvitationViewModel;

    .line 146
    iget-object v1, p0, Lepson/print/ActivityPrintWeb;->mReviewInvitationViewModel:Lcom/epson/mobilephone/common/ReviewInvitationViewModel;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/ReviewInvitationViewModel;->getShowInvitationLiveData()Landroid/arch/lifecycle/MutableLiveData;

    move-result-object v1

    new-instance v2, Lepson/print/ActivityPrintWeb$1;

    invoke-direct {v2, p0}, Lepson/print/ActivityPrintWeb$1;-><init>(Lepson/print/ActivityPrintWeb;)V

    invoke-virtual {v1, p0, v2}, Landroid/arch/lifecycle/MutableLiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    .line 155
    invoke-virtual {p0}, Lepson/print/ActivityPrintWeb;->getIntent()Landroid/content/Intent;

    move-result-object v1

    if-nez v1, :cond_0

    const-string p1, "ActivityPrintWeb"

    const-string v0, "intent == null"

    .line 157
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    invoke-virtual {p0}, Lepson/print/ActivityPrintWeb;->finish()V

    return-void

    :cond_0
    const-string v2, "from"

    const/4 v3, 0x2

    .line 162
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lepson/print/ActivityPrintWeb;->fromActivity:I

    const-string v2, "print_web"

    .line 163
    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lepson/print/ActivityPrintWeb;->addImageView:Ljava/util/ArrayList;

    .line 164
    iget-object v2, p0, Lepson/print/ActivityPrintWeb;->addImageView:Ljava/util/ArrayList;

    if-nez v2, :cond_1

    const-string p1, "ActivityPrintWeb"

    .line 166
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "intent.getStringArrayListExtra(\"print_web\") returns null. action "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    invoke-virtual {p0}, Lepson/print/ActivityPrintWeb;->finish()V

    return-void

    :cond_1
    const-string v2, "print_url"

    .line 171
    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "from_customaction"

    const/4 v4, 0x0

    .line 173
    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lepson/print/ActivityPrintWeb;->isCustomAction:Z

    :try_start_0
    const-string v3, "print_log"

    .line 175
    invoke-virtual {v1, v3}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/epson/iprint/prtlogger/PrintLog;

    iput-object v1, p0, Lepson/print/ActivityPrintWeb;->mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v1, 0x0

    .line 180
    :goto_0
    iget-object v3, p0, Lepson/print/ActivityPrintWeb;->addImageView:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 182
    new-instance v3, Lepson/print/EPImage;

    iget-object v5, p0, Lepson/print/ActivityPrintWeb;->addImageView:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-direct {v3, v5, v1}, Lepson/print/EPImage;-><init>(Ljava/lang/String;I)V

    .line 183
    iput-object v2, v3, Lepson/print/EPImage;->webUrl:Ljava/lang/String;

    .line 184
    iget-object v5, p0, Lepson/print/ActivityPrintWeb;->imageList:Lepson/print/EPImageList;

    invoke-virtual {v5, v3}, Lepson/print/EPImageList;->add(Lepson/print/EPImage;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    const v1, 0x7f0801f6

    .line 187
    invoke-virtual {p0, v1}, Lepson/print/ActivityPrintWeb;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lepson/print/ActivityPrintWeb;->mLn_zoomview:Landroid/widget/LinearLayout;

    const v1, 0x7f0802ba

    .line 188
    invoke-virtual {p0, v1}, Lepson/print/ActivityPrintWeb;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lepson/print/ActivityPrintWeb;->mRl_zoomview:Landroid/widget/RelativeLayout;

    const v1, 0x7f0800a2

    .line 190
    invoke-virtual {p0, v1}, Lepson/print/ActivityPrintWeb;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lepson/print/ActivityPrintWeb;->mPrint:Landroid/widget/Button;

    const v1, 0x7f08038c

    .line 191
    invoke-virtual {p0, v1}, Lepson/print/ActivityPrintWeb;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lepson/print/ActivityPrintWeb;->mImageView:Landroid/widget/ImageView;

    .line 192
    iput-boolean v0, p0, Lepson/print/ActivityPrintWeb;->mIsPortrait:Z

    const v1, 0x7f080375

    .line 193
    invoke-virtual {p0, v1}, Lepson/print/ActivityPrintWeb;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lepson/print/ActivityPrintWeb;->mSizeInfo:Landroid/widget/TextView;

    .line 195
    iget-object v1, p0, Lepson/print/ActivityPrintWeb;->mPrint:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 196
    iget-object v1, p0, Lepson/print/ActivityPrintWeb;->mSizeInfo:Landroid/widget/TextView;

    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f08018d

    .line 198
    invoke-virtual {p0, v1}, Lepson/print/ActivityPrintWeb;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lepson/print/ActivityPrintWeb;->paperMissmath:Landroid/widget/ImageView;

    const v1, 0x7f080370

    .line 200
    invoke-virtual {p0, v1}, Lepson/print/ActivityPrintWeb;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lepson/print/ActivityPrintWeb;->mPageNum:Landroid/widget/TextView;

    const v1, 0x7f0800a0

    .line 201
    invoke-virtual {p0, v1}, Lepson/print/ActivityPrintWeb;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lepson/print/ActivityPrintWeb;->mNext:Landroid/widget/Button;

    const v1, 0x7f0800a1

    .line 202
    invoke-virtual {p0, v1}, Lepson/print/ActivityPrintWeb;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lepson/print/ActivityPrintWeb;->mPre:Landroid/widget/Button;

    .line 203
    iget-object v1, p0, Lepson/print/ActivityPrintWeb;->addImageView:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iput v1, p0, Lepson/print/ActivityPrintWeb;->mFileTotalPages:I

    .line 205
    invoke-direct {p0}, Lepson/print/ActivityPrintWeb;->updateButton()V

    .line 207
    iget-object v1, p0, Lepson/print/ActivityPrintWeb;->mPre:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 208
    iget-object v1, p0, Lepson/print/ActivityPrintWeb;->mPre:Landroid/widget/Button;

    new-instance v2, Lepson/print/ActivityPrintWeb$2;

    invoke-direct {v2, p0}, Lepson/print/ActivityPrintWeb$2;-><init>(Lepson/print/ActivityPrintWeb;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 225
    iget-object v1, p0, Lepson/print/ActivityPrintWeb;->mNext:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 226
    iget-object v1, p0, Lepson/print/ActivityPrintWeb;->mNext:Landroid/widget/Button;

    new-instance v2, Lepson/print/ActivityPrintWeb$3;

    invoke-direct {v2, p0}, Lepson/print/ActivityPrintWeb$3;-><init>(Lepson/print/ActivityPrintWeb;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 243
    invoke-virtual {p0}, Lepson/print/ActivityPrintWeb;->savePageRange2Pre()V

    .line 244
    invoke-direct {p0}, Lepson/print/ActivityPrintWeb;->loadConfig()V

    .line 246
    iget-object v1, p0, Lepson/print/ActivityPrintWeb;->mImageView:Landroid/widget/ImageView;

    new-instance v2, Lepson/print/ActivityPrintWeb$4;

    invoke-direct {v2, p0}, Lepson/print/ActivityPrintWeb$4;-><init>(Lepson/print/ActivityPrintWeb;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 287
    iget v1, p0, Lepson/print/ActivityPrintWeb;->fromActivity:I

    if-eq v1, v0, :cond_4

    const/4 v2, 0x4

    if-eq v1, v2, :cond_3

    .line 298
    invoke-virtual {p0, p1}, Lepson/print/ActivityPrintWeb;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lepson/print/ActivityPrintWeb;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_3
    const p1, 0x7f0e03d9

    .line 289
    invoke-virtual {p0, p1}, Lepson/print/ActivityPrintWeb;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lepson/print/ActivityPrintWeb;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_4
    const p1, 0x7f0e04fd

    .line 293
    invoke-virtual {p0, p1}, Lepson/print/ActivityPrintWeb;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lepson/print/ActivityPrintWeb;->setTitle(Ljava/lang/CharSequence;)V

    .line 302
    :goto_1
    iget-object p1, p0, Lepson/print/ActivityPrintWeb;->mPrint:Landroid/widget/Button;

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 304
    iput v0, p0, Lepson/print/ActivityPrintWeb;->mCurrentPage:I

    .line 306
    iget-object p1, p0, Lepson/print/ActivityPrintWeb;->mLn_zoomview:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {p1, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 307
    invoke-virtual {p0}, Lepson/print/ActivityPrintWeb;->updateTextView()V

    const-string p1, "PREFS_EPSON_CONNECT"

    .line 310
    invoke-virtual {p0, p1, v4}, Lepson/print/ActivityPrintWeb;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    const-string v1, "ENABLE_SHOW_WARNING"

    .line 311
    invoke-interface {p1, v1, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result p1

    iput-boolean p1, p0, Lepson/print/ActivityPrintWeb;->enableShowWarning:Z

    .line 314
    invoke-static {p0}, Lepson/print/MyPrinter;->isRemotePrinter(Landroid/content/Context;)Z

    move-result p1

    iput-boolean p1, p0, Lepson/print/ActivityPrintWeb;->isRemotePrinter:Z

    .line 317
    invoke-static {p0}, Lepson/print/screen/PaperSourceInfo;->getInstance(Landroid/content/Context;)Lepson/print/screen/PaperSourceInfo;

    move-result-object p1

    iput-object p1, p0, Lepson/print/ActivityPrintWeb;->paperSourceInfo:Lepson/print/screen/PaperSourceInfo;

    .line 319
    iget-object p1, p0, Lepson/print/ActivityPrintWeb;->mEpsonService:Lepson/print/service/IEpsonService;

    if-nez p1, :cond_5

    .line 320
    new-instance p1, Landroid/content/Intent;

    const-class v1, Lepson/print/service/EpsonService;

    invoke-direct {p1, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lepson/print/ActivityPrintWeb;->mEpsonConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, p1, v1, v0}, Lepson/print/ActivityPrintWeb;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 322
    iget-object p1, p0, Lepson/print/ActivityPrintWeb;->mUiHandler:Landroid/os/Handler;

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_5
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .line 437
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 438
    invoke-virtual {p0}, Lepson/print/ActivityPrintWeb;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0b0008

    .line 439
    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const/4 p1, 0x1

    return p1
.end method

.method protected onDestroy()V
    .locals 2

    .line 932
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onDestroy()V

    .line 935
    iget-object v0, p0, Lepson/print/ActivityPrintWeb;->mEpsonService:Lepson/print/service/IEpsonService;

    if-eqz v0, :cond_0

    .line 937
    :try_start_0
    invoke-interface {v0}, Lepson/print/service/IEpsonService;->cancelSearchPrinter()I

    .line 939
    iget-object v0, p0, Lepson/print/ActivityPrintWeb;->mEpsonService:Lepson/print/service/IEpsonService;

    iget-object v1, p0, Lepson/print/ActivityPrintWeb;->mCallback:Lepson/print/service/IEpsonServiceCallback;

    invoke-interface {v0, v1}, Lepson/print/service/IEpsonService;->unregisterCallback(Lepson/print/service/IEpsonServiceCallback;)V

    .line 940
    iget-object v0, p0, Lepson/print/ActivityPrintWeb;->mEpsonConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lepson/print/ActivityPrintWeb;->unbindService(Landroid/content/ServiceConnection;)V

    const/4 v0, 0x0

    .line 941
    iput-object v0, p0, Lepson/print/ActivityPrintWeb;->mEpsonService:Lepson/print/service/IEpsonService;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 943
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_0
    :goto_0
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2

    .line 901
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onNewIntent(Landroid/content/Intent;)V

    .line 903
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.nfc.action.NDEF_DISCOVERED"

    .line 905
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 906
    invoke-static {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils;->parseNECTag(Landroid/content/Context;Landroid/content/Intent;)Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 914
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 915
    const-class v1, Lepson/print/ActivityNfcPrinter;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v1, "connectInfo"

    .line 916
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string p1, "changeMode"

    const/4 v1, 0x1

    .line 917
    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 p1, 0x5

    .line 918
    invoke-virtual {p0, v0, p1}, Lepson/print/ActivityPrintWeb;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .line 445
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f080021

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 447
    :cond_0
    invoke-virtual {p0}, Lepson/print/ActivityPrintWeb;->callPrintSetting()V

    .line 450
    :goto_0
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1
.end method

.method protected onPause()V
    .locals 1

    .line 890
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onPause()V

    .line 892
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils;->disableForegroundDispatch(Landroid/app/Activity;)V

    .line 895
    iget-object v0, p0, Lepson/print/ActivityPrintWeb;->paperSourceInfo:Lepson/print/screen/PaperSourceInfo;

    invoke-virtual {v0}, Lepson/print/screen/PaperSourceInfo;->stop()V

    return-void
.end method

.method protected onResume()V
    .locals 2

    const-string v0, "ActivityPrintWeb"

    const-string v1, "onResume()"

    .line 876
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 877
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onResume()V

    const/4 v0, 0x0

    .line 880
    move-object v1, v0

    check-cast v1, [[Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils;->enableForegroundDispatch(Landroid/app/Activity;[Landroid/content/IntentFilter;[[Ljava/lang/String;)V

    .line 883
    iget-object v0, p0, Lepson/print/ActivityPrintWeb;->paperMissmath:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 884
    iget-object v0, p0, Lepson/print/ActivityPrintWeb;->paperSourceInfo:Lepson/print/screen/PaperSourceInfo;

    iget-object v1, p0, Lepson/print/ActivityPrintWeb;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {v0, p0, v1}, Lepson/print/screen/PaperSourceInfo;->start(Landroid/content/Context;Landroid/os/Handler;)V

    return-void
.end method

.method public savePageRange2Pre()V
    .locals 3

    .line 1060
    new-instance v0, Lepson/print/screen/PrintSetting;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lepson/print/screen/PrintSetting;-><init>(Landroid/content/Context;Lepson/print/screen/PrintSetting$Kind;)V

    iget v1, p0, Lepson/print/ActivityPrintWeb;->mFileTotalPages:I

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lepson/print/screen/PrintSetting;->resetPageRange(II)V

    .line 1062
    iput v2, p0, Lepson/print/ActivityPrintWeb;->mStartPage:I

    .line 1063
    iget v0, p0, Lepson/print/ActivityPrintWeb;->mFileTotalPages:I

    iput v0, p0, Lepson/print/ActivityPrintWeb;->mEndPage:I

    const-string v0, "OldStartPage"

    .line 1064
    iget v1, p0, Lepson/print/ActivityPrintWeb;->mStartPage:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "OldEndPage"

    .line 1065
    iget v1, p0, Lepson/print/ActivityPrintWeb;->mEndPage:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setOrentationView(Z)V
    .locals 18

    move-object/from16 v1, p0

    .line 672
    :try_start_0
    invoke-direct/range {p0 .. p0}, Lepson/print/ActivityPrintWeb;->loadConfig()V

    .line 674
    invoke-virtual/range {p0 .. p0}, Lepson/print/ActivityPrintWeb;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iput-object v0, v1, Lepson/print/ActivityPrintWeb;->mDisplay:Landroid/view/Display;

    .line 677
    iget-object v0, v1, Lepson/print/ActivityPrintWeb;->mPaperSize:Lepson/common/Info_paper;

    invoke-virtual {v0}, Lepson/common/Info_paper;->getPaper_width()I

    move-result v0

    .line 678
    iget-object v2, v1, Lepson/print/ActivityPrintWeb;->mPaperSize:Lepson/common/Info_paper;

    invoke-virtual {v2}, Lepson/common/Info_paper;->getPaper_height()I

    move-result v2

    .line 682
    iget v3, v1, Lepson/print/ActivityPrintWeb;->mLayoutMulti:I

    const/4 v4, 0x0

    if-eqz v3, :cond_2

    .line 685
    iget v3, v1, Lepson/print/ActivityPrintWeb;->mLayoutMulti:I

    const/high16 v5, 0x10000

    if-eq v3, v5, :cond_1

    const/high16 v5, 0x20000

    if-eq v3, v5, :cond_0

    const/high16 v5, 0x40000

    if-eq v3, v5, :cond_0

    const/4 v3, 0x0

    const/4 v5, 0x0

    move/from16 v16, v2

    move v2, v0

    move/from16 v0, v16

    goto :goto_0

    .line 698
    :cond_0
    iget-object v0, v1, Lepson/print/ActivityPrintWeb;->mPaperSize:Lepson/common/Info_paper;

    invoke-virtual {v0}, Lepson/common/Info_paper;->getPaperSize_4in1()Landroid/graphics/Rect;

    move-result-object v0

    .line 699
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v2

    .line 700
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    .line 702
    iget-object v3, v1, Lepson/print/ActivityPrintWeb;->mPaperSize:Lepson/common/Info_paper;

    invoke-virtual {v3}, Lepson/common/Info_paper;->getPrintingArea_4in1()Landroid/graphics/Rect;

    move-result-object v3

    .line 703
    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v5

    .line 704
    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    goto :goto_0

    .line 687
    :cond_1
    iget-object v0, v1, Lepson/print/ActivityPrintWeb;->mPaperSize:Lepson/common/Info_paper;

    invoke-virtual {v0}, Lepson/common/Info_paper;->getPaperSize_2in1()Landroid/graphics/Rect;

    move-result-object v0

    .line 688
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v2

    .line 689
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    .line 691
    iget-object v3, v1, Lepson/print/ActivityPrintWeb;->mPaperSize:Lepson/common/Info_paper;

    invoke-virtual {v3}, Lepson/common/Info_paper;->getPrintingArea_2in1()Landroid/graphics/Rect;

    move-result-object v3

    .line 692
    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v5

    .line 693
    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    :goto_0
    if-le v2, v0, :cond_4

    move/from16 v16, v2

    move v2, v0

    move/from16 v0, v16

    move/from16 v17, v5

    move v5, v3

    move/from16 v3, v17

    goto :goto_1

    :cond_2
    if-le v0, v2, :cond_3

    const/4 v3, 0x0

    const/4 v5, 0x0

    goto :goto_1

    :cond_3
    const/4 v3, 0x0

    const/4 v5, 0x0

    move/from16 v16, v2

    move v2, v0

    move/from16 v0, v16

    :cond_4
    :goto_1
    const/4 v6, 0x1

    if-nez p1, :cond_5

    const/4 v7, 0x1

    goto :goto_2

    :cond_5
    const/4 v7, 0x0

    .line 727
    :goto_2
    iput-boolean v7, v1, Lepson/print/ActivityPrintWeb;->mPreviewPaperAreaLandscape:Z

    .line 729
    new-instance v7, Landroid/graphics/Point;

    invoke-direct {v7}, Landroid/graphics/Point;-><init>()V

    .line 730
    iget-object v8, v1, Lepson/print/ActivityPrintWeb;->mDisplay:Landroid/view/Display;

    invoke-virtual {v8, v7}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    if-eqz p1, :cond_7

    .line 741
    iget v8, v7, Landroid/graphics/Point;->y:I

    iget-object v9, v1, Lepson/print/ActivityPrintWeb;->mPrint:Landroid/widget/Button;

    invoke-virtual {v9}, Landroid/widget/Button;->getHeight()I

    move-result v9

    mul-int/lit8 v9, v9, 0x4

    sub-int/2addr v8, v9

    int-to-double v9, v8

    int-to-double v11, v0

    div-double/2addr v9, v11

    int-to-double v13, v2

    mul-double v9, v9, v13

    double-to-int v9, v9

    .line 751
    iget v10, v7, Landroid/graphics/Point;->x:I

    mul-int/lit8 v10, v10, 0x4

    div-int/lit8 v10, v10, 0x5

    if-le v9, v10, :cond_6

    .line 752
    iget v7, v7, Landroid/graphics/Point;->x:I

    mul-int/lit8 v7, v7, 0x4

    div-int/lit8 v9, v7, 0x5

    int-to-double v7, v9

    div-double/2addr v7, v13

    mul-double v7, v7, v11

    double-to-int v8, v7

    .line 756
    :cond_6
    new-instance v7, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v7, v9, v8}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v7, v1, Lepson/print/ActivityPrintWeb;->Relative_para:Landroid/widget/RelativeLayout$LayoutParams;

    goto :goto_3

    .line 758
    :cond_7
    iget v8, v7, Landroid/graphics/Point;->x:I

    mul-int/lit8 v8, v8, 0x4

    div-int/lit8 v8, v8, 0x5

    int-to-double v9, v8

    int-to-double v11, v0

    div-double/2addr v9, v11

    int-to-double v13, v2

    mul-double v9, v9, v13

    double-to-int v9, v9

    .line 766
    iget v10, v7, Landroid/graphics/Point;->y:I

    iget-object v15, v1, Lepson/print/ActivityPrintWeb;->mPrint:Landroid/widget/Button;

    invoke-virtual {v15}, Landroid/widget/Button;->getHeight()I

    move-result v15

    mul-int/lit8 v15, v15, 0x4

    sub-int/2addr v10, v15

    if-le v9, v10, :cond_8

    .line 767
    iget v7, v7, Landroid/graphics/Point;->y:I

    iget-object v8, v1, Lepson/print/ActivityPrintWeb;->mPrint:Landroid/widget/Button;

    invoke-virtual {v8}, Landroid/widget/Button;->getHeight()I

    move-result v8

    mul-int/lit8 v8, v8, 0x4

    sub-int v9, v7, v8

    int-to-double v7, v9

    div-double/2addr v7, v13

    mul-double v7, v7, v11

    double-to-int v8, v7

    .line 771
    :cond_8
    new-instance v7, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v7, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v7, v1, Lepson/print/ActivityPrintWeb;->Relative_para:Landroid/widget/RelativeLayout$LayoutParams;

    .line 773
    :goto_3
    iget-object v7, v1, Lepson/print/ActivityPrintWeb;->Relative_para:Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v8, 0xd

    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 775
    iget-object v7, v1, Lepson/print/ActivityPrintWeb;->mLn_zoomview:Landroid/widget/LinearLayout;

    iget-object v8, v1, Lepson/print/ActivityPrintWeb;->Relative_para:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v7, v8}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 777
    invoke-direct {v1, v4}, Lepson/print/ActivityPrintWeb;->setNewImageView(I)V

    .line 778
    iput v6, v1, Lepson/print/ActivityPrintWeb;->mCurrentPage:I

    .line 779
    invoke-virtual/range {p0 .. p0}, Lepson/print/ActivityPrintWeb;->updateTextView()V

    .line 780
    invoke-direct/range {p0 .. p0}, Lepson/print/ActivityPrintWeb;->updateButton()V

    .line 783
    iget v4, v1, Lepson/print/ActivityPrintWeb;->mLayout:I

    const/4 v6, 0x2

    if-ne v4, v6, :cond_c

    .line 784
    iget v4, v1, Lepson/print/ActivityPrintWeb;->mLayoutMulti:I

    if-nez v4, :cond_a

    if-eqz p1, :cond_9

    .line 786
    iget-object v0, v1, Lepson/print/ActivityPrintWeb;->Relative_para:Landroid/widget/RelativeLayout$LayoutParams;

    iget v0, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    int-to-float v0, v0

    int-to-float v2, v2

    div-float/2addr v0, v2

    .line 787
    iget-object v2, v1, Lepson/print/ActivityPrintWeb;->mLn_zoomview:Landroid/widget/LinearLayout;

    iget-object v3, v1, Lepson/print/ActivityPrintWeb;->mPaperSize:Lepson/common/Info_paper;

    invoke-virtual {v3}, Lepson/common/Info_paper;->getLeftMargin_border()I

    move-result v3

    int-to-float v3, v3

    mul-float v3, v3, v0

    float-to-int v3, v3

    iget-object v4, v1, Lepson/print/ActivityPrintWeb;->mPaperSize:Lepson/common/Info_paper;

    .line 788
    invoke-virtual {v4}, Lepson/common/Info_paper;->getTopMargin_border()I

    move-result v4

    int-to-float v4, v4

    mul-float v4, v4, v0

    float-to-int v4, v4

    iget-object v5, v1, Lepson/print/ActivityPrintWeb;->mPaperSize:Lepson/common/Info_paper;

    .line 789
    invoke-virtual {v5}, Lepson/common/Info_paper;->getRightMargin_border()I

    move-result v5

    int-to-float v5, v5

    mul-float v5, v5, v0

    float-to-int v5, v5

    iget-object v6, v1, Lepson/print/ActivityPrintWeb;->mPaperSize:Lepson/common/Info_paper;

    .line 790
    invoke-virtual {v6}, Lepson/common/Info_paper;->getBottomMargin_border()I

    move-result v6

    int-to-float v6, v6

    mul-float v6, v6, v0

    float-to-int v0, v6

    .line 787
    invoke-virtual {v2, v3, v4, v5, v0}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    goto/16 :goto_4

    .line 793
    :cond_9
    iget-object v2, v1, Lepson/print/ActivityPrintWeb;->Relative_para:Landroid/widget/RelativeLayout$LayoutParams;

    iget v2, v2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    int-to-float v2, v2

    int-to-float v0, v0

    div-float/2addr v2, v0

    .line 794
    iget-object v0, v1, Lepson/print/ActivityPrintWeb;->mLn_zoomview:Landroid/widget/LinearLayout;

    iget-object v3, v1, Lepson/print/ActivityPrintWeb;->mPaperSize:Lepson/common/Info_paper;

    invoke-virtual {v3}, Lepson/common/Info_paper;->getTopMargin_border()I

    move-result v3

    int-to-float v3, v3

    mul-float v3, v3, v2

    float-to-int v3, v3

    iget-object v4, v1, Lepson/print/ActivityPrintWeb;->mPaperSize:Lepson/common/Info_paper;

    .line 795
    invoke-virtual {v4}, Lepson/common/Info_paper;->getRightMargin_border()I

    move-result v4

    int-to-float v4, v4

    mul-float v4, v4, v2

    float-to-int v4, v4

    iget-object v5, v1, Lepson/print/ActivityPrintWeb;->mPaperSize:Lepson/common/Info_paper;

    .line 796
    invoke-virtual {v5}, Lepson/common/Info_paper;->getBottomMargin_border()I

    move-result v5

    int-to-float v5, v5

    mul-float v5, v5, v2

    float-to-int v5, v5

    iget-object v6, v1, Lepson/print/ActivityPrintWeb;->mPaperSize:Lepson/common/Info_paper;

    .line 797
    invoke-virtual {v6}, Lepson/common/Info_paper;->getLeftMargin_border()I

    move-result v6

    int-to-float v6, v6

    mul-float v6, v6, v2

    float-to-int v2, v6

    .line 794
    invoke-virtual {v0, v3, v4, v5, v2}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    goto :goto_4

    :cond_a
    if-eqz p1, :cond_b

    .line 802
    iget-object v4, v1, Lepson/print/ActivityPrintWeb;->Relative_para:Landroid/widget/RelativeLayout$LayoutParams;

    iget v4, v4, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    int-to-float v4, v4

    int-to-float v7, v2

    div-float/2addr v4, v7

    .line 803
    iget-object v7, v1, Lepson/print/ActivityPrintWeb;->mLn_zoomview:Landroid/widget/LinearLayout;

    sub-int/2addr v2, v5

    div-int/lit8 v5, v2, 0x2

    int-to-float v5, v5

    mul-float v5, v5, v4

    float-to-int v5, v5

    sub-int/2addr v0, v3

    div-int/lit8 v3, v0, 0x2

    int-to-float v3, v3

    mul-float v3, v3, v4

    float-to-int v3, v3

    div-int/2addr v2, v6

    int-to-float v2, v2

    mul-float v2, v2, v4

    float-to-int v2, v2

    div-int/2addr v0, v6

    int-to-float v0, v0

    mul-float v0, v0, v4

    float-to-int v0, v0

    invoke-virtual {v7, v5, v3, v2, v0}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    goto :goto_4

    .line 808
    :cond_b
    iget-object v4, v1, Lepson/print/ActivityPrintWeb;->Relative_para:Landroid/widget/RelativeLayout$LayoutParams;

    iget v4, v4, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    int-to-float v4, v4

    int-to-float v7, v0

    div-float/2addr v4, v7

    .line 809
    iget-object v7, v1, Lepson/print/ActivityPrintWeb;->mLn_zoomview:Landroid/widget/LinearLayout;

    sub-int/2addr v0, v3

    div-int/lit8 v3, v0, 0x2

    int-to-float v3, v3

    mul-float v3, v3, v4

    float-to-int v3, v3

    sub-int/2addr v2, v5

    div-int/lit8 v5, v2, 0x2

    int-to-float v5, v5

    mul-float v5, v5, v4

    float-to-int v5, v5

    div-int/2addr v0, v6

    int-to-float v0, v0

    mul-float v0, v0, v4

    float-to-int v0, v0

    div-int/2addr v2, v6

    int-to-float v2, v2

    mul-float v2, v2, v4

    float-to-int v2, v2

    invoke-virtual {v7, v3, v5, v0, v2}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 817
    :cond_c
    :goto_4
    iget-object v0, v1, Lepson/print/ActivityPrintWeb;->mUiHandler:Landroid/os/Handler;

    const/4 v2, 0x6

    const-wide/16 v3, 0xc8

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_5

    :catch_0
    move-exception v0

    .line 820
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 821
    invoke-virtual/range {p0 .. p0}, Lepson/print/ActivityPrintWeb;->finish()V

    :goto_5
    return-void
.end method

.method public updateTextView()V
    .locals 2

    .line 1054
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lepson/print/ActivityPrintWeb;->mCurrentPage:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lepson/print/ActivityPrintWeb;->mFileTotalPages:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1055
    iget-object v1, p0, Lepson/print/ActivityPrintWeb;->mPageNum:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

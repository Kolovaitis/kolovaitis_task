.class Lepson/print/fileBrower$4;
.super Ljava/lang/Object;
.source "fileBrower.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/fileBrower;->onContextItemSelected(Landroid/view/MenuItem;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/fileBrower;

.field final synthetic val$clickedFile11:Ljava/io/File;

.field final synthetic val$fileCurrent:Ljava/io/File;


# direct methods
.method constructor <init>(Lepson/print/fileBrower;Ljava/io/File;Ljava/io/File;)V
    .locals 0

    .line 206
    iput-object p1, p0, Lepson/print/fileBrower$4;->this$0:Lepson/print/fileBrower;

    iput-object p2, p0, Lepson/print/fileBrower$4;->val$clickedFile11:Ljava/io/File;

    iput-object p3, p0, Lepson/print/fileBrower$4;->val$fileCurrent:Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 209
    :try_start_0
    iget-object p1, p0, Lepson/print/fileBrower$4;->val$clickedFile11:Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lepson/print/fileBrower$4;->val$clickedFile11:Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object p1

    array-length p1, p1

    if-lez p1, :cond_0

    .line 210
    iget-object p1, p0, Lepson/print/fileBrower$4;->this$0:Lepson/print/fileBrower;

    iget-object p2, p0, Lepson/print/fileBrower$4;->val$clickedFile11:Ljava/io/File;

    invoke-static {p1, p2}, Lepson/print/fileBrower;->access$200(Lepson/print/fileBrower;Ljava/io/File;)V

    goto :goto_0

    .line 212
    :cond_0
    iget-object p1, p0, Lepson/print/fileBrower$4;->val$clickedFile11:Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    .line 214
    :goto_0
    iget-object p1, p0, Lepson/print/fileBrower$4;->this$0:Lepson/print/fileBrower;

    iget-object p2, p0, Lepson/print/fileBrower$4;->val$fileCurrent:Ljava/io/File;

    invoke-static {p1, p2}, Lepson/print/fileBrower;->access$300(Lepson/print/fileBrower;Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    .line 218
    invoke-virtual {p1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception p1

    .line 216
    invoke-virtual {p1}, Ljava/lang/SecurityException;->printStackTrace()V

    :goto_1
    return-void
.end method

.class Lepson/print/WebviewActivity$ImageFileCreator$1;
.super Ljava/lang/Object;
.source "WebviewActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/WebviewActivity$ImageFileCreator;->createImageFile(Landroid/webkit/WebView;Ljava/util/ArrayList;Lepson/print/WebviewActivity$InteruptChecker;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lepson/print/WebviewActivity$ImageFileCreator;

.field final synthetic val$canvas:Landroid/graphics/Canvas;

.field final synthetic val$lock:Ljava/lang/Object;

.field final synthetic val$webView:Landroid/webkit/WebView;


# direct methods
.method constructor <init>(Lepson/print/WebviewActivity$ImageFileCreator;Ljava/lang/Object;Landroid/webkit/WebView;Landroid/graphics/Canvas;)V
    .locals 0

    .line 1004
    iput-object p1, p0, Lepson/print/WebviewActivity$ImageFileCreator$1;->this$1:Lepson/print/WebviewActivity$ImageFileCreator;

    iput-object p2, p0, Lepson/print/WebviewActivity$ImageFileCreator$1;->val$lock:Ljava/lang/Object;

    iput-object p3, p0, Lepson/print/WebviewActivity$ImageFileCreator$1;->val$webView:Landroid/webkit/WebView;

    iput-object p4, p0, Lepson/print/WebviewActivity$ImageFileCreator$1;->val$canvas:Landroid/graphics/Canvas;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 1007
    iget-object v0, p0, Lepson/print/WebviewActivity$ImageFileCreator$1;->val$lock:Ljava/lang/Object;

    monitor-enter v0

    .line 1008
    :try_start_0
    iget-object v1, p0, Lepson/print/WebviewActivity$ImageFileCreator$1;->val$webView:Landroid/webkit/WebView;

    iget-object v2, p0, Lepson/print/WebviewActivity$ImageFileCreator$1;->val$canvas:Landroid/graphics/Canvas;

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->draw(Landroid/graphics/Canvas;)V

    .line 1009
    iget-object v1, p0, Lepson/print/WebviewActivity$ImageFileCreator$1;->val$lock:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    const-string v1, "WebviewActivity"

    const-string v2, "webView.draw() finish"

    .line 1010
    invoke-static {v1, v2}, Lepson/print/Util/EPLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1011
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

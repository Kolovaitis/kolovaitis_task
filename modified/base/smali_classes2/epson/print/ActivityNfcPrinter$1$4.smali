.class Lepson/print/ActivityNfcPrinter$1$4;
.super Landroid/os/AsyncTask;
.source "ActivityNfcPrinter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/ActivityNfcPrinter$1;->handleMessage(Landroid/os/Message;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lepson/scan/lib/ScannerInfo;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lepson/print/ActivityNfcPrinter$1;

.field final synthetic val$printer:Lepson/print/MyPrinter;


# direct methods
.method constructor <init>(Lepson/print/ActivityNfcPrinter$1;Lepson/print/MyPrinter;)V
    .locals 0

    .line 828
    iput-object p1, p0, Lepson/print/ActivityNfcPrinter$1$4;->this$1:Lepson/print/ActivityNfcPrinter$1;

    iput-object p2, p0, Lepson/print/ActivityNfcPrinter$1$4;->val$printer:Lepson/print/MyPrinter;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lepson/scan/lib/ScannerInfo;
    .locals 5

    .line 838
    invoke-static {}, Lepson/print/ActivityNfcPrinter;->access$1100()Lepson/scan/lib/escanLib;

    move-result-object p1

    iget-object v0, p0, Lepson/print/ActivityNfcPrinter$1$4;->this$1:Lepson/print/ActivityNfcPrinter$1;

    iget-object v0, v0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    invoke-virtual {p1, v0}, Lepson/scan/lib/escanLib;->escanWrapperInitDriver(Landroid/content/Context;)I

    move-result p1

    const/16 v0, -0x41a

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eq p1, v0, :cond_1

    if-eqz p1, :cond_0

    return-object v2

    :cond_0
    const/4 p1, 0x0

    goto :goto_0

    :cond_1
    const/4 p1, 0x1

    .line 856
    :goto_0
    :try_start_0
    invoke-static {}, Lepson/print/ActivityNfcPrinter;->access$1100()Lepson/scan/lib/escanLib;

    move-result-object v0

    iget-object v3, p0, Lepson/print/ActivityNfcPrinter$1$4;->this$1:Lepson/print/ActivityNfcPrinter$1;

    iget-object v3, v3, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    invoke-virtual {v3}, Lepson/print/ActivityNfcPrinter;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lepson/print/ActivityNfcPrinter$1$4;->val$printer:Lepson/print/MyPrinter;

    .line 855
    invoke-static {v0, v3, v4}, Lepson/scan/lib/ScanSettingHelper;->recodeScannerInfo(Lepson/scan/lib/escanLib;Landroid/content/Context;Lepson/print/MyPrinter;)Lepson/scan/activity/ScannerPropertyWrapper;

    move-result-object v0

    .line 857
    invoke-virtual {v0}, Lepson/scan/activity/ScannerPropertyWrapper;->getI1ScannerInfo()Lepson/scan/lib/ScannerInfo;

    move-result-object v0
    :try_end_0
    .catch Lepson/scan/lib/EscanLibException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-eq p1, v1, :cond_2

    .line 865
    invoke-static {}, Lepson/print/ActivityNfcPrinter;->access$1100()Lepson/scan/lib/escanLib;

    move-result-object p1

    invoke-virtual {p1}, Lepson/scan/lib/escanLib;->escanWrapperReleaseDriver()I

    .line 869
    :cond_2
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1$4;->val$printer:Lepson/print/MyPrinter;

    invoke-virtual {p1}, Lepson/print/MyPrinter;->getLocation()I

    move-result p1

    const/4 v1, 0x3

    if-ne p1, v1, :cond_4

    .line 871
    new-instance p1, Lepson/print/EPPrinterManager;

    iget-object v1, p0, Lepson/print/ActivityNfcPrinter$1$4;->this$1:Lepson/print/ActivityNfcPrinter$1;

    iget-object v1, v1, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    invoke-direct {p1, v1}, Lepson/print/EPPrinterManager;-><init>(Landroid/content/Context;)V

    .line 872
    iget-object v1, p0, Lepson/print/ActivityNfcPrinter$1$4;->val$printer:Lepson/print/MyPrinter;

    invoke-virtual {v1}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lepson/print/EPPrinterManager;->loadIpPrinterInfo(Ljava/lang/String;)Lepson/print/EPPrinterInfo;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 874
    invoke-virtual {v0}, Lepson/scan/lib/ScannerInfo;->getScannerId()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lepson/print/EPPrinterInfo;->scannerID:Ljava/lang/String;

    .line 875
    iget-object v2, p0, Lepson/print/ActivityNfcPrinter$1$4;->val$printer:Lepson/print/MyPrinter;

    invoke-virtual {v2}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lepson/print/EPPrinterInfo;->printerIP:Ljava/lang/String;

    goto :goto_1

    .line 877
    :cond_3
    new-instance v1, Lepson/print/EPPrinterInfo;

    invoke-direct {v1}, Lepson/print/EPPrinterInfo;-><init>()V

    .line 878
    iget-object v2, p0, Lepson/print/ActivityNfcPrinter$1$4;->val$printer:Lepson/print/MyPrinter;

    invoke-virtual {v2}, Lepson/print/MyPrinter;->getName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lepson/print/EPPrinterInfo;->printerName:Ljava/lang/String;

    .line 879
    iget-object v2, p0, Lepson/print/ActivityNfcPrinter$1$4;->val$printer:Lepson/print/MyPrinter;

    invoke-virtual {v2}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lepson/print/EPPrinterInfo;->printerID:Ljava/lang/String;

    .line 880
    invoke-virtual {v0}, Lepson/scan/lib/ScannerInfo;->getScannerId()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lepson/print/EPPrinterInfo;->scannerID:Ljava/lang/String;

    .line 881
    iget-object v2, p0, Lepson/print/ActivityNfcPrinter$1$4;->val$printer:Lepson/print/MyPrinter;

    invoke-virtual {v2}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lepson/print/EPPrinterInfo;->printerIP:Ljava/lang/String;

    .line 882
    iget-object v2, p0, Lepson/print/ActivityNfcPrinter$1$4;->val$printer:Lepson/print/MyPrinter;

    invoke-virtual {v2}, Lepson/print/MyPrinter;->getName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lepson/print/EPPrinterInfo;->userDefName:Ljava/lang/String;

    .line 883
    iget-object v2, p0, Lepson/print/ActivityNfcPrinter$1$4;->val$printer:Lepson/print/MyPrinter;

    invoke-virtual {v2}, Lepson/print/MyPrinter;->getLocation()I

    move-result v2

    iput v2, v1, Lepson/print/EPPrinterInfo;->printerLocation:I

    .line 884
    iget-object v2, p0, Lepson/print/ActivityNfcPrinter$1$4;->val$printer:Lepson/print/MyPrinter;

    invoke-virtual {v2}, Lepson/print/MyPrinter;->getSerialNo()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lepson/print/EPPrinterInfo;->printerSerialNo:Ljava/lang/String;

    .line 887
    :goto_1
    iget-object v2, p0, Lepson/print/ActivityNfcPrinter$1$4;->val$printer:Lepson/print/MyPrinter;

    invoke-virtual {v2}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2, v1}, Lepson/print/EPPrinterManager;->saveIPPrinterInfo(Ljava/lang/String;Lepson/print/EPPrinterInfo;)V

    .line 888
    invoke-virtual {p1}, Lepson/print/EPPrinterManager;->commitIPPrinterInfo()V

    :cond_4
    return-object v0

    :catch_0
    return-object v2
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 828
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/print/ActivityNfcPrinter$1$4;->doInBackground([Ljava/lang/Void;)Lepson/scan/lib/ScannerInfo;

    move-result-object p1

    return-object p1
.end method

.method protected onPostExecute(Lepson/scan/lib/ScannerInfo;)V
    .locals 2

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const-string p1, "ActivityChangeWifiPrinter"

    .line 928
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lepson/print/ActivityNfcPrinter$1$4;->val$printer:Lepson/print/MyPrinter;

    invoke-virtual {v1}, Lepson/print/MyPrinter;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "is not scanner"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 930
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1$4;->this$1:Lepson/print/ActivityNfcPrinter$1;

    iget-object p1, p1, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    invoke-static {p1}, Lepson/print/ActivityNfcPrinter;->access$700(Lepson/print/ActivityNfcPrinter;)I

    move-result p1

    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 932
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1$4;->this$1:Lepson/print/ActivityNfcPrinter$1;

    iget-object p1, p1, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    const v0, 0x7f0e03ed

    const v1, 0x7f0e03e3

    invoke-virtual {p1, v0, v1}, Lepson/print/ActivityNfcPrinter;->showErrorMessage(II)V

    return-void

    .line 940
    :cond_1
    :goto_0
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1$4;->this$1:Lepson/print/ActivityNfcPrinter$1;

    iget-object p1, p1, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    iget-object p1, p1, Lepson/print/ActivityNfcPrinter;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object p1

    const/16 v0, 0x14

    .line 941
    iput v0, p1, Landroid/os/Message;->what:I

    .line 942
    iget-object v0, p0, Lepson/print/ActivityNfcPrinter$1$4;->val$printer:Lepson/print/MyPrinter;

    iput-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 943
    iget-object v0, p0, Lepson/print/ActivityNfcPrinter$1$4;->this$1:Lepson/print/ActivityNfcPrinter$1;

    iget-object v0, v0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    iget-object v0, v0, Lepson/print/ActivityNfcPrinter;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 828
    check-cast p1, Lepson/scan/lib/ScannerInfo;

    invoke-virtual {p0, p1}, Lepson/print/ActivityNfcPrinter$1$4;->onPostExecute(Lepson/scan/lib/ScannerInfo;)V

    return-void
.end method

.class final enum Lepson/print/rpcopy/ActivityBase$ClickButton;
.super Ljava/lang/Enum;
.source "ActivityBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/rpcopy/ActivityBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "ClickButton"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lepson/print/rpcopy/ActivityBase$ClickButton;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lepson/print/rpcopy/ActivityBase$ClickButton;

.field public static final enum Cancel:Lepson/print/rpcopy/ActivityBase$ClickButton;

.field public static final enum ClearError:Lepson/print/rpcopy/ActivityBase$ClickButton;

.field public static final enum Ok:Lepson/print/rpcopy/ActivityBase$ClickButton;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 155
    new-instance v0, Lepson/print/rpcopy/ActivityBase$ClickButton;

    const-string v1, "Ok"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lepson/print/rpcopy/ActivityBase$ClickButton;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/rpcopy/ActivityBase$ClickButton;->Ok:Lepson/print/rpcopy/ActivityBase$ClickButton;

    .line 156
    new-instance v0, Lepson/print/rpcopy/ActivityBase$ClickButton;

    const-string v1, "Cancel"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lepson/print/rpcopy/ActivityBase$ClickButton;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/rpcopy/ActivityBase$ClickButton;->Cancel:Lepson/print/rpcopy/ActivityBase$ClickButton;

    .line 157
    new-instance v0, Lepson/print/rpcopy/ActivityBase$ClickButton;

    const-string v1, "ClearError"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lepson/print/rpcopy/ActivityBase$ClickButton;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/rpcopy/ActivityBase$ClickButton;->ClearError:Lepson/print/rpcopy/ActivityBase$ClickButton;

    const/4 v0, 0x3

    .line 154
    new-array v0, v0, [Lepson/print/rpcopy/ActivityBase$ClickButton;

    sget-object v1, Lepson/print/rpcopy/ActivityBase$ClickButton;->Ok:Lepson/print/rpcopy/ActivityBase$ClickButton;

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/ActivityBase$ClickButton;->Cancel:Lepson/print/rpcopy/ActivityBase$ClickButton;

    aput-object v1, v0, v3

    sget-object v1, Lepson/print/rpcopy/ActivityBase$ClickButton;->ClearError:Lepson/print/rpcopy/ActivityBase$ClickButton;

    aput-object v1, v0, v4

    sput-object v0, Lepson/print/rpcopy/ActivityBase$ClickButton;->$VALUES:[Lepson/print/rpcopy/ActivityBase$ClickButton;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 154
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lepson/print/rpcopy/ActivityBase$ClickButton;
    .locals 1

    .line 154
    const-class v0, Lepson/print/rpcopy/ActivityBase$ClickButton;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lepson/print/rpcopy/ActivityBase$ClickButton;

    return-object p0
.end method

.method public static values()[Lepson/print/rpcopy/ActivityBase$ClickButton;
    .locals 1

    .line 154
    sget-object v0, Lepson/print/rpcopy/ActivityBase$ClickButton;->$VALUES:[Lepson/print/rpcopy/ActivityBase$ClickButton;

    invoke-virtual {v0}, [Lepson/print/rpcopy/ActivityBase$ClickButton;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lepson/print/rpcopy/ActivityBase$ClickButton;

    return-object v0
.end method

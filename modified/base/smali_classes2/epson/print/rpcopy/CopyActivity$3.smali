.class Lepson/print/rpcopy/CopyActivity$3;
.super Ljava/lang/Object;
.source "CopyActivity.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/rpcopy/CopyActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/rpcopy/CopyActivity;


# direct methods
.method constructor <init>(Lepson/print/rpcopy/CopyActivity;)V
    .locals 0

    .line 235
    iput-object p1, p0, Lepson/print/rpcopy/CopyActivity$3;->this$0:Lepson/print/rpcopy/CopyActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 4

    .line 238
    iget-object v0, p0, Lepson/print/rpcopy/CopyActivity$3;->this$0:Lepson/print/rpcopy/CopyActivity;

    invoke-static {v0}, Lepson/print/rpcopy/CopyActivity;->access$200(Lepson/print/rpcopy/CopyActivity;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "HandlerCallback"

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    iget p1, p1, Landroid/os/Message;->what:I

    packed-switch p1, :pswitch_data_0

    goto/16 :goto_0

    .line 259
    :pswitch_0
    iget-object p1, p0, Lepson/print/rpcopy/CopyActivity$3;->this$0:Lepson/print/rpcopy/CopyActivity;

    invoke-static {p1}, Lepson/print/rpcopy/CopyActivity;->access$200(Lepson/print/rpcopy/CopyActivity;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "UPDATE_SETTING"

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    iget-object p1, p0, Lepson/print/rpcopy/CopyActivity$3;->this$0:Lepson/print/rpcopy/CopyActivity;

    iget-object p1, p1, Lepson/print/rpcopy/CopyActivity;->mECopyOptionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    if-nez p1, :cond_0

    .line 262
    iget-object p1, p0, Lepson/print/rpcopy/CopyActivity$3;->this$0:Lepson/print/rpcopy/CopyActivity;

    invoke-virtual {p1}, Lepson/print/rpcopy/CopyActivity;->fetchCopyOptionContext()V

    goto/16 :goto_0

    .line 265
    :cond_0
    iget-object p1, p0, Lepson/print/rpcopy/CopyActivity$3;->this$0:Lepson/print/rpcopy/CopyActivity;

    iget-object v0, p1, Lepson/print/rpcopy/CopyActivity;->copyComponent:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;

    invoke-virtual {v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;->getBindedCopyOptionContext()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    move-result-object v0

    iput-object v0, p1, Lepson/print/rpcopy/CopyActivity;->mECopyOptionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    .line 266
    iget-object p1, p0, Lepson/print/rpcopy/CopyActivity$3;->this$0:Lepson/print/rpcopy/CopyActivity;

    iget-object p1, p1, Lepson/print/rpcopy/CopyActivity;->copyComponent:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;

    iget-object v0, p0, Lepson/print/rpcopy/CopyActivity$3;->this$0:Lepson/print/rpcopy/CopyActivity;

    iget-object v0, v0, Lepson/print/rpcopy/CopyActivity;->mECopyOptionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    iget-object v1, p0, Lepson/print/rpcopy/CopyActivity$3;->this$0:Lepson/print/rpcopy/CopyActivity;

    iget-object v1, v1, Lepson/print/rpcopy/CopyActivity;->optionListener:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;

    invoke-virtual {p1, v0, v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;->bindCopyOptionContext(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;)V

    .line 267
    iget-object p1, p0, Lepson/print/rpcopy/CopyActivity$3;->this$0:Lepson/print/rpcopy/CopyActivity;

    iget-object v0, p1, Lepson/print/rpcopy/CopyActivity;->copyComponent:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;

    invoke-virtual {v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;->getCopyOptionItems()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p1, v0}, Lepson/print/rpcopy/CopyActivity;->buildCopyOptions(Ljava/util/ArrayList;)V

    goto :goto_0

    .line 248
    :pswitch_1
    iget-object p1, p0, Lepson/print/rpcopy/CopyActivity$3;->this$0:Lepson/print/rpcopy/CopyActivity;

    invoke-static {p1}, Lepson/print/rpcopy/CopyActivity;->access$200(Lepson/print/rpcopy/CopyActivity;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "COMM_ERROR"

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    iget-object p1, p0, Lepson/print/rpcopy/CopyActivity$3;->this$0:Lepson/print/rpcopy/CopyActivity;

    new-instance v0, Lepson/print/rpcopy/ActivityBase$errorDialog;

    invoke-direct {v0, p1, p1}, Lepson/print/rpcopy/ActivityBase$errorDialog;-><init>(Lepson/print/rpcopy/ActivityBase;Landroid/content/Context;)V

    invoke-static {p1, v0}, Lepson/print/rpcopy/CopyActivity;->access$502(Lepson/print/rpcopy/CopyActivity;Lepson/print/rpcopy/ActivityBase$errorDialog;)Lepson/print/rpcopy/ActivityBase$errorDialog;

    .line 250
    iget-object p1, p0, Lepson/print/rpcopy/CopyActivity$3;->this$0:Lepson/print/rpcopy/CopyActivity;

    invoke-static {p1}, Lepson/print/rpcopy/CopyActivity;->access$500(Lepson/print/rpcopy/CopyActivity;)Lepson/print/rpcopy/ActivityBase$errorDialog;

    move-result-object p1

    iget-object v0, p0, Lepson/print/rpcopy/CopyActivity$3;->this$0:Lepson/print/rpcopy/CopyActivity;

    const v1, 0x7f0e01b1

    invoke-virtual {v0, v1}, Lepson/print/rpcopy/CopyActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lepson/print/rpcopy/CopyActivity$3;->this$0:Lepson/print/rpcopy/CopyActivity;

    const v2, 0x7f0e01af

    invoke-virtual {v1, v2}, Lepson/print/rpcopy/CopyActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lepson/print/rpcopy/ActivityBase$DialogButtons;->Ok:Lepson/print/rpcopy/ActivityBase$DialogButtons;

    new-instance v3, Lepson/print/rpcopy/CopyActivity$3$1;

    invoke-direct {v3, p0}, Lepson/print/rpcopy/CopyActivity$3$1;-><init>(Lepson/print/rpcopy/CopyActivity$3;)V

    invoke-virtual {p1, v0, v1, v2, v3}, Lepson/print/rpcopy/ActivityBase$errorDialog;->showErrorDialog(Ljava/lang/String;Ljava/lang/String;Lepson/print/rpcopy/ActivityBase$DialogButtons;Lepson/print/rpcopy/ActivityBase$IClose;)V

    goto :goto_0

    .line 242
    :pswitch_2
    iget-object p1, p0, Lepson/print/rpcopy/CopyActivity$3;->this$0:Lepson/print/rpcopy/CopyActivity;

    invoke-static {p1}, Lepson/print/rpcopy/CopyActivity;->access$200(Lepson/print/rpcopy/CopyActivity;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "PROBE_PRINTER"

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    iget-object p1, p0, Lepson/print/rpcopy/CopyActivity$3;->this$0:Lepson/print/rpcopy/CopyActivity;

    new-instance v0, Lepson/print/rpcopy/CopyActivity$ProbePrinter;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lepson/print/rpcopy/CopyActivity$ProbePrinter;-><init>(Lepson/print/rpcopy/CopyActivity;Lepson/print/rpcopy/CopyActivity$1;)V

    invoke-static {p1, v0}, Lepson/print/rpcopy/CopyActivity;->access$302(Lepson/print/rpcopy/CopyActivity;Lepson/print/rpcopy/CopyActivity$ProbePrinter;)Lepson/print/rpcopy/CopyActivity$ProbePrinter;

    .line 244
    iget-object p1, p0, Lepson/print/rpcopy/CopyActivity$3;->this$0:Lepson/print/rpcopy/CopyActivity;

    invoke-static {p1}, Lepson/print/rpcopy/CopyActivity;->access$300(Lepson/print/rpcopy/CopyActivity;)Lepson/print/rpcopy/CopyActivity$ProbePrinter;

    move-result-object p1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {p1, v0}, Lepson/print/rpcopy/CopyActivity$ProbePrinter;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :goto_0
    const/4 p1, 0x1

    return p1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.class Lepson/print/rpcopy/ActivityBase$WheelDialog;
.super Landroid/app/Dialog;
.source "ActivityBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/rpcopy/ActivityBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "WheelDialog"
.end annotation


# instance fields
.field cancelButton:Landroid/widget/Button;

.field cancelCallback:Lepson/print/rpcopy/ActivityBase$CancelRequestCallback;

.field cancelDialog:Landroid/app/Dialog;

.field messageText:Landroid/widget/TextView;

.field final synthetic this$0:Lepson/print/rpcopy/ActivityBase;


# direct methods
.method public constructor <init>(Lepson/print/rpcopy/ActivityBase;Landroid/content/Context;)V
    .locals 0

    .line 97
    iput-object p1, p0, Lepson/print/rpcopy/ActivityBase$WheelDialog;->this$0:Lepson/print/rpcopy/ActivityBase;

    const p1, 0x7f0f000a

    .line 98
    invoke-direct {p0, p2, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    const p1, 0x7f0a00c8

    .line 99
    invoke-virtual {p0, p1}, Lepson/print/rpcopy/ActivityBase$WheelDialog;->setContentView(I)V

    const/4 p1, 0x0

    .line 100
    invoke-virtual {p0, p1}, Lepson/print/rpcopy/ActivityBase$WheelDialog;->setCancelable(Z)V

    const p1, 0x7f080219

    .line 101
    invoke-virtual {p0, p1}, Lepson/print/rpcopy/ActivityBase$WheelDialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lepson/print/rpcopy/ActivityBase$WheelDialog;->messageText:Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method createCancelDialog()Landroid/app/Dialog;
    .locals 3

    .line 78
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lepson/print/rpcopy/ActivityBase$WheelDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 79
    iget-object v1, p0, Lepson/print/rpcopy/ActivityBase$WheelDialog;->this$0:Lepson/print/rpcopy/ActivityBase;

    const v2, 0x7f0e04de

    invoke-virtual {v1, v2}, Lepson/print/rpcopy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const/4 v1, 0x0

    .line 80
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 81
    iget-object v1, p0, Lepson/print/rpcopy/ActivityBase$WheelDialog;->this$0:Lepson/print/rpcopy/ActivityBase;

    const v2, 0x7f0e052b

    invoke-virtual {v1, v2}, Lepson/print/rpcopy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lepson/print/rpcopy/ActivityBase$WheelDialog$1;

    invoke-direct {v2, p0}, Lepson/print/rpcopy/ActivityBase$WheelDialog$1;-><init>(Lepson/print/rpcopy/ActivityBase$WheelDialog;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 88
    iget-object v1, p0, Lepson/print/rpcopy/ActivityBase$WheelDialog;->this$0:Lepson/print/rpcopy/ActivityBase;

    const v2, 0x7f0e04e6

    invoke-virtual {v1, v2}, Lepson/print/rpcopy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lepson/print/rpcopy/ActivityBase$WheelDialog$2;

    invoke-direct {v2, p0}, Lepson/print/rpcopy/ActivityBase$WheelDialog$2;-><init>(Lepson/print/rpcopy/ActivityBase$WheelDialog;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 94
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method dissmiss()V
    .locals 1

    .line 131
    invoke-super {p0}, Landroid/app/Dialog;->dismiss()V

    .line 133
    iget-object v0, p0, Lepson/print/rpcopy/ActivityBase$WheelDialog;->cancelDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 134
    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    :cond_0
    return-void
.end method

.method hideCancelButton()V
    .locals 2

    .line 127
    iget-object v0, p0, Lepson/print/rpcopy/ActivityBase$WheelDialog;->cancelButton:Landroid/widget/Button;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    return-void
.end method

.method setText(Ljava/lang/String;)V
    .locals 1

    .line 123
    iget-object v0, p0, Lepson/print/rpcopy/ActivityBase$WheelDialog;->messageText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method show(Ljava/lang/String;Lepson/print/rpcopy/ActivityBase$CancelRequestCallback;)V
    .locals 1

    .line 105
    iput-object p2, p0, Lepson/print/rpcopy/ActivityBase$WheelDialog;->cancelCallback:Lepson/print/rpcopy/ActivityBase$CancelRequestCallback;

    .line 107
    iget-object p2, p0, Lepson/print/rpcopy/ActivityBase$WheelDialog;->messageText:Landroid/widget/TextView;

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 108
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$WheelDialog;->messageText:Landroid/widget/TextView;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setVisibility(I)V

    const p1, 0x7f0800b0

    .line 110
    invoke-virtual {p0, p1}, Lepson/print/rpcopy/ActivityBase$WheelDialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lepson/print/rpcopy/ActivityBase$WheelDialog;->cancelButton:Landroid/widget/Button;

    .line 111
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$WheelDialog;->cancelButton:Landroid/widget/Button;

    new-instance v0, Lepson/print/rpcopy/ActivityBase$WheelDialog$3;

    invoke-direct {v0, p0}, Lepson/print/rpcopy/ActivityBase$WheelDialog$3;-><init>(Lepson/print/rpcopy/ActivityBase$WheelDialog;)V

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 118
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$WheelDialog;->cancelButton:Landroid/widget/Button;

    invoke-virtual {p1, p2}, Landroid/widget/Button;->setVisibility(I)V

    .line 119
    invoke-virtual {p0}, Lepson/print/rpcopy/ActivityBase$WheelDialog;->show()V

    return-void
.end method

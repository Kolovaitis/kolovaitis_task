.class Lepson/print/rpcopy/ActivityBase$ListOptionValue;
.super Lepson/print/rpcopy/ActivityBase$OptionValue;
.source "ActivityBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/rpcopy/ActivityBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ListOptionValue"
.end annotation


# instance fields
.field choices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;",
            ">;"
        }
    .end annotation
.end field

.field selected:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field final synthetic this$0:Lepson/print/rpcopy/ActivityBase;


# direct methods
.method constructor <init>(Lepson/print/rpcopy/ActivityBase;)V
    .locals 0

    .line 438
    iput-object p1, p0, Lepson/print/rpcopy/ActivityBase$ListOptionValue;->this$0:Lepson/print/rpcopy/ActivityBase;

    invoke-direct {p0, p1}, Lepson/print/rpcopy/ActivityBase$OptionValue;-><init>(Lepson/print/rpcopy/ActivityBase;)V

    return-void
.end method


# virtual methods
.method bindOption(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)V
    .locals 2

    .line 445
    sget-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XCutLineStyle:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {p1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getKey()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    move-result-object v1

    invoke-virtual {v0, v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XCutLineWeight:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    .line 446
    invoke-virtual {p1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getKey()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    move-result-object v1

    invoke-virtual {v0, v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 448
    :cond_0
    invoke-virtual {p1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->updateChoicesXCutLine()V

    .line 453
    :cond_1
    iput-object p1, p0, Lepson/print/rpcopy/ActivityBase$ListOptionValue;->optionItem:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    .line 454
    invoke-virtual {p1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getSelectableChoices()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lepson/print/rpcopy/ActivityBase$ListOptionValue;->choices:Ljava/util/List;

    .line 455
    invoke-virtual {p1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object p1

    iput-object p1, p0, Lepson/print/rpcopy/ActivityBase$ListOptionValue;->selected:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-void
.end method

.method getKeyArray()[Ljava/lang/String;
    .locals 4

    .line 463
    iget-object v0, p0, Lepson/print/rpcopy/ActivityBase$ListOptionValue;->choices:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 464
    :goto_0
    iget-object v2, p0, Lepson/print/rpcopy/ActivityBase$ListOptionValue;->choices:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 465
    iget-object v2, p0, Lepson/print/rpcopy/ActivityBase$ListOptionValue;->choices:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    invoke-virtual {v2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->name()Ljava/lang/String;

    move-result-object v2

    .line 466
    iget-object v3, p0, Lepson/print/rpcopy/ActivityBase$ListOptionValue;->this$0:Lepson/print/rpcopy/ActivityBase;

    invoke-virtual {v3, v2}, Lepson/print/rpcopy/ActivityBase;->getDisplayString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.class Lepson/print/rpcopy/CopyActivity$2;
.super Ljava/lang/Object;
.source "CopyActivity.java"

# interfaces
.implements Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/rpcopy/CopyActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/rpcopy/CopyActivity;


# direct methods
.method constructor <init>(Lepson/print/rpcopy/CopyActivity;)V
    .locals 0

    .line 114
    iput-object p1, p0, Lepson/print/rpcopy/CopyActivity$2;->this$0:Lepson/print/rpcopy/CopyActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCopyOptionChanged(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;Ljava/util/ArrayList;Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;",
            "Ljava/util/ArrayList<",
            "Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;",
            ">;",
            "Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p3, :cond_0

    .line 119
    iget-object p1, p0, Lepson/print/rpcopy/CopyActivity$2;->this$0:Lepson/print/rpcopy/CopyActivity;

    iget-object p1, p1, Lepson/print/rpcopy/CopyActivity;->loading:Lepson/print/screen/WorkingDialog;

    invoke-virtual {p1}, Lepson/print/screen/WorkingDialog;->dismiss()V

    .line 120
    new-instance p1, Lepson/print/rpcopy/ActivityBase$errorDialog;

    iget-object p2, p0, Lepson/print/rpcopy/CopyActivity$2;->this$0:Lepson/print/rpcopy/CopyActivity;

    invoke-direct {p1, p2, p2}, Lepson/print/rpcopy/ActivityBase$errorDialog;-><init>(Lepson/print/rpcopy/ActivityBase;Landroid/content/Context;)V

    .line 121
    invoke-virtual {p1, p3}, Lepson/print/rpcopy/ActivityBase$errorDialog;->getReasonText(Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;)[Ljava/lang/String;

    move-result-object p2

    .line 122
    aget-object p3, p2, v1

    .line 123
    aget-object p2, p2, v0

    .line 124
    invoke-virtual {p1, p3, p2}, Lepson/print/rpcopy/ActivityBase$errorDialog;->showErrorDialog(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 127
    :cond_0
    new-instance p3, Ljava/util/ArrayList;

    invoke-direct {p3}, Ljava/util/ArrayList;-><init>()V

    .line 128
    iget-object v2, p0, Lepson/print/rpcopy/CopyActivity$2;->this$0:Lepson/print/rpcopy/CopyActivity;

    invoke-static {v2}, Lepson/print/rpcopy/CopyActivity;->access$000(Lepson/print/rpcopy/CopyActivity;)Z

    move-result v2

    if-ne v2, v0, :cond_2

    .line 130
    iget-object v0, p0, Lepson/print/rpcopy/CopyActivity$2;->this$0:Lepson/print/rpcopy/CopyActivity;

    invoke-static {v0}, Lepson/print/rpcopy/CopyActivity;->access$100(Lepson/print/rpcopy/CopyActivity;)I

    move-result v0

    iget-object v2, p0, Lepson/print/rpcopy/CopyActivity$2;->this$0:Lepson/print/rpcopy/CopyActivity;

    iget-object v2, v2, Lepson/print/rpcopy/CopyActivity;->copyComponent:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;

    invoke-virtual {v2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;->getCopyOptionItems()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-eq v0, v2, :cond_1

    .line 132
    iget-object p1, p0, Lepson/print/rpcopy/CopyActivity$2;->this$0:Lepson/print/rpcopy/CopyActivity;

    invoke-virtual {p1}, Lepson/print/rpcopy/CopyActivity;->allClear()V

    return-void

    .line 136
    :cond_1
    iget-object v0, p0, Lepson/print/rpcopy/CopyActivity$2;->this$0:Lepson/print/rpcopy/CopyActivity;

    invoke-static {v0, v1}, Lepson/print/rpcopy/CopyActivity;->access$002(Lepson/print/rpcopy/CopyActivity;Z)Z

    .line 137
    iget-object v0, p0, Lepson/print/rpcopy/CopyActivity$2;->this$0:Lepson/print/rpcopy/CopyActivity;

    invoke-static {v0, v1}, Lepson/print/rpcopy/CopyActivity;->access$102(Lepson/print/rpcopy/CopyActivity;I)I

    .line 138
    iget-object v0, p0, Lepson/print/rpcopy/CopyActivity$2;->this$0:Lepson/print/rpcopy/CopyActivity;

    iget-object v0, v0, Lepson/print/rpcopy/CopyActivity;->copyComponent:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;

    invoke-virtual {v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;->getCopyOptionItems()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 141
    :cond_2
    invoke-virtual {p3, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz p2, :cond_3

    .line 143
    invoke-virtual {p3, p2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 145
    :cond_3
    iget-object p1, p0, Lepson/print/rpcopy/CopyActivity$2;->this$0:Lepson/print/rpcopy/CopyActivity;

    invoke-virtual {p1, p3}, Lepson/print/rpcopy/CopyActivity;->buildCopyOptions(Ljava/util/ArrayList;)V

    return-void
.end method

.class public Lepson/print/rpcopy/CopyActivity;
.super Lepson/print/rpcopy/ActivityBase;
.source "CopyActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/rpcopy/CopyActivity$ProbePrinter;,
        Lepson/print/rpcopy/CopyActivity$XDensityValue;,
        Lepson/print/rpcopy/CopyActivity$CopiesValue;
    }
.end annotation


# static fields
.field private static final EPS_COMM_BID:I = 0x2

.field public static final RESULT_BACK_TO_HOME:I = -0x1

.field public static final RESULT_CHANGE_TO_NORMAL_COPY:I


# instance fields
.field private final COMM_ERROR:I

.field private final PROBE_PRINTER:I

.field private TAG:Ljava/lang/String;

.field private final UPDATE_SETTING:I

.field bProbedPrinter:Z

.field private clearindex:I

.field copyProcess:Lepson/print/rpcopy/CopyProcess;

.field private doClear:Z

.field private errordialog:Lepson/print/rpcopy/ActivityBase$errorDialog;

.field private ipAdress:Ljava/lang/String;

.field mHandler:Landroid/os/Handler;

.field private mLayout:Landroid/view/ViewGroup;

.field private mNormalCopyButton:Landroid/widget/RadioButton;

.field private mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

.field optionListener:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;

.field optionValueChangedListener:Lepson/print/rpcopy/ActivityBase$OptionItemChangedListener;

.field private presettings:Lepson/print/rpcopy/ActivityBase$settingPreference;

.field private printerLocation:I

.field systemSettings:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;

.field private task:Lepson/print/rpcopy/CopyActivity$ProbePrinter;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 52
    invoke-direct {p0}, Lepson/print/rpcopy/ActivityBase;-><init>()V

    const-string v0, "CopyActivty"

    .line 57
    iput-object v0, p0, Lepson/print/rpcopy/CopyActivity;->TAG:Ljava/lang/String;

    .line 60
    new-instance v0, Lepson/print/rpcopy/ActivityBase$settingPreference;

    invoke-direct {v0, p0}, Lepson/print/rpcopy/ActivityBase$settingPreference;-><init>(Lepson/print/rpcopy/ActivityBase;)V

    iput-object v0, p0, Lepson/print/rpcopy/CopyActivity;->presettings:Lepson/print/rpcopy/ActivityBase$settingPreference;

    const/4 v0, 0x0

    .line 66
    iput v0, p0, Lepson/print/rpcopy/CopyActivity;->clearindex:I

    .line 67
    iput-boolean v0, p0, Lepson/print/rpcopy/CopyActivity;->doClear:Z

    .line 77
    iput-boolean v0, p0, Lepson/print/rpcopy/CopyActivity;->bProbedPrinter:Z

    .line 231
    iput v0, p0, Lepson/print/rpcopy/CopyActivity;->PROBE_PRINTER:I

    const/4 v0, 0x1

    .line 232
    iput v0, p0, Lepson/print/rpcopy/CopyActivity;->COMM_ERROR:I

    const/4 v0, 0x2

    .line 233
    iput v0, p0, Lepson/print/rpcopy/CopyActivity;->UPDATE_SETTING:I

    .line 235
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lepson/print/rpcopy/CopyActivity$3;

    invoke-direct {v1, p0}, Lepson/print/rpcopy/CopyActivity$3;-><init>(Lepson/print/rpcopy/CopyActivity;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lepson/print/rpcopy/CopyActivity;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lepson/print/rpcopy/CopyActivity;)Z
    .locals 0

    .line 52
    iget-boolean p0, p0, Lepson/print/rpcopy/CopyActivity;->doClear:Z

    return p0
.end method

.method static synthetic access$002(Lepson/print/rpcopy/CopyActivity;Z)Z
    .locals 0

    .line 52
    iput-boolean p1, p0, Lepson/print/rpcopy/CopyActivity;->doClear:Z

    return p1
.end method

.method static synthetic access$100(Lepson/print/rpcopy/CopyActivity;)I
    .locals 0

    .line 52
    iget p0, p0, Lepson/print/rpcopy/CopyActivity;->clearindex:I

    return p0
.end method

.method static synthetic access$1000(Lepson/print/rpcopy/CopyActivity;)Lepson/print/rpcopy/ActivityBase$settingPreference;
    .locals 0

    .line 52
    iget-object p0, p0, Lepson/print/rpcopy/CopyActivity;->presettings:Lepson/print/rpcopy/ActivityBase$settingPreference;

    return-object p0
.end method

.method static synthetic access$102(Lepson/print/rpcopy/CopyActivity;I)I
    .locals 0

    .line 52
    iput p1, p0, Lepson/print/rpcopy/CopyActivity;->clearindex:I

    return p1
.end method

.method static synthetic access$1100(Lepson/print/rpcopy/CopyActivity;)I
    .locals 0

    .line 52
    iget p0, p0, Lepson/print/rpcopy/CopyActivity;->printerLocation:I

    return p0
.end method

.method static synthetic access$1200(Lepson/print/rpcopy/CopyActivity;)Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;
    .locals 0

    .line 52
    iget-object p0, p0, Lepson/print/rpcopy/CopyActivity;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    return-object p0
.end method

.method static synthetic access$1300(Lepson/print/rpcopy/CopyActivity;)Ljava/lang/String;
    .locals 0

    .line 52
    iget-object p0, p0, Lepson/print/rpcopy/CopyActivity;->ipAdress:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1302(Lepson/print/rpcopy/CopyActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 52
    iput-object p1, p0, Lepson/print/rpcopy/CopyActivity;->ipAdress:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$200(Lepson/print/rpcopy/CopyActivity;)Ljava/lang/String;
    .locals 0

    .line 52
    iget-object p0, p0, Lepson/print/rpcopy/CopyActivity;->TAG:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$300(Lepson/print/rpcopy/CopyActivity;)Lepson/print/rpcopy/CopyActivity$ProbePrinter;
    .locals 0

    .line 52
    iget-object p0, p0, Lepson/print/rpcopy/CopyActivity;->task:Lepson/print/rpcopy/CopyActivity$ProbePrinter;

    return-object p0
.end method

.method static synthetic access$302(Lepson/print/rpcopy/CopyActivity;Lepson/print/rpcopy/CopyActivity$ProbePrinter;)Lepson/print/rpcopy/CopyActivity$ProbePrinter;
    .locals 0

    .line 52
    iput-object p1, p0, Lepson/print/rpcopy/CopyActivity;->task:Lepson/print/rpcopy/CopyActivity$ProbePrinter;

    return-object p1
.end method

.method static synthetic access$500(Lepson/print/rpcopy/CopyActivity;)Lepson/print/rpcopy/ActivityBase$errorDialog;
    .locals 0

    .line 52
    iget-object p0, p0, Lepson/print/rpcopy/CopyActivity;->errordialog:Lepson/print/rpcopy/ActivityBase$errorDialog;

    return-object p0
.end method

.method static synthetic access$502(Lepson/print/rpcopy/CopyActivity;Lepson/print/rpcopy/ActivityBase$errorDialog;)Lepson/print/rpcopy/ActivityBase$errorDialog;
    .locals 0

    .line 52
    iput-object p1, p0, Lepson/print/rpcopy/CopyActivity;->errordialog:Lepson/print/rpcopy/ActivityBase$errorDialog;

    return-object p1
.end method

.method static synthetic access$600(Lepson/print/rpcopy/CopyActivity;)V
    .locals 0

    .line 52
    invoke-direct {p0}, Lepson/print/rpcopy/CopyActivity;->returnToHome()V

    return-void
.end method

.method static synthetic access$700(Lepson/print/rpcopy/CopyActivity;)V
    .locals 0

    .line 52
    invoke-direct {p0}, Lepson/print/rpcopy/CopyActivity;->switchToNormalCopy()V

    return-void
.end method

.method static synthetic access$800(Lepson/print/rpcopy/CopyActivity;)V
    .locals 0

    .line 52
    invoke-direct {p0}, Lepson/print/rpcopy/CopyActivity;->setupUiPostCommunication()V

    return-void
.end method

.method static synthetic access$900(Lepson/print/rpcopy/CopyActivity;)V
    .locals 0

    .line 52
    invoke-direct {p0}, Lepson/print/rpcopy/CopyActivity;->setClickListenerPostCommunication()V

    return-void
.end method

.method private getPrinterInfo()V
    .locals 2

    .line 753
    invoke-static {p0}, Lepson/print/MyPrinter;->getCurPrinter(Landroid/content/Context;)Lepson/print/MyPrinter;

    move-result-object v0

    .line 754
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lepson/print/rpcopy/CopyActivity;->printerId:Ljava/lang/String;

    .line 755
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lepson/print/rpcopy/CopyActivity;->printerIp:Ljava/lang/String;

    .line 756
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getLocation()I

    move-result v0

    iput v0, p0, Lepson/print/rpcopy/CopyActivity;->printerLocation:I

    return-void
.end method

.method private returnToHome()V
    .locals 1

    const/4 v0, -0x1

    .line 325
    invoke-virtual {p0, v0}, Lepson/print/rpcopy/CopyActivity;->setResult(I)V

    .line 326
    invoke-virtual {p0}, Lepson/print/rpcopy/CopyActivity;->finish()V

    return-void
.end method

.method private setClickListener()V
    .locals 1

    const v0, 0x7f0800d2

    .line 333
    invoke-virtual {p0, v0}, Lepson/print/rpcopy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0802d1

    .line 334
    invoke-virtual {p0, v0}, Lepson/print/rpcopy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f08024a

    .line 335
    invoke-virtual {p0, v0}, Lepson/print/rpcopy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f080252

    .line 336
    invoke-virtual {p0, v0}, Lepson/print/rpcopy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f08024f

    .line 337
    invoke-virtual {p0, v0}, Lepson/print/rpcopy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f080118

    .line 338
    invoke-virtual {p0, v0}, Lepson/print/rpcopy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0802a0

    .line 339
    invoke-virtual {p0, v0}, Lepson/print/rpcopy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0800f7

    .line 340
    invoke-virtual {p0, v0}, Lepson/print/rpcopy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0800e4

    .line 341
    invoke-virtual {p0, v0}, Lepson/print/rpcopy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0802a8

    .line 343
    invoke-virtual {p0, v0}, Lepson/print/rpcopy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0802a4

    .line 344
    invoke-virtual {p0, v0}, Lepson/print/rpcopy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f080027

    .line 347
    invoke-virtual {p0, v0}, Lepson/print/rpcopy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lepson/print/widgets/LongTapRepeatAdapter;->bless(Landroid/view/View;)V

    const v0, 0x7f080026

    .line 348
    invoke-virtual {p0, v0}, Lepson/print/rpcopy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lepson/print/widgets/LongTapRepeatAdapter;->bless(Landroid/view/View;)V

    const v0, 0x7f08002b

    .line 349
    invoke-virtual {p0, v0}, Lepson/print/rpcopy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lepson/print/widgets/LongTapRepeatAdapter;->bless(Landroid/view/View;)V

    const v0, 0x7f08002a

    .line 350
    invoke-virtual {p0, v0}, Lepson/print/rpcopy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lepson/print/widgets/LongTapRepeatAdapter;->bless(Landroid/view/View;)V

    return-void
.end method

.method private setClickListenerPostCommunication()V
    .locals 1

    const v0, 0x7f080269

    .line 359
    invoke-virtual {p0, v0}, Lepson/print/rpcopy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f08026b

    .line 360
    invoke-virtual {p0, v0}, Lepson/print/rpcopy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f080270

    .line 361
    invoke-virtual {p0, v0}, Lepson/print/rpcopy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private setupUi()V
    .locals 2

    const v0, 0x7f0802d1

    .line 281
    invoke-virtual {p0, v0}, Lepson/print/rpcopy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f080119

    .line 282
    invoke-virtual {p0, v0}, Lepson/print/rpcopy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f080118

    .line 283
    invoke-virtual {p0, v0}, Lepson/print/rpcopy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0802a8

    .line 285
    invoke-virtual {p0, v0}, Lepson/print/rpcopy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0802a6

    .line 286
    invoke-virtual {p0, v0}, Lepson/print/rpcopy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0802a4

    .line 287
    invoke-virtual {p0, v0}, Lepson/print/rpcopy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0800f9

    .line 289
    invoke-virtual {p0, v0}, Lepson/print/rpcopy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    .line 290
    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setVisibility(I)V

    const v0, 0x7f08022a

    .line 292
    invoke-virtual {p0, v0}, Lepson/print/rpcopy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lepson/print/rpcopy/CopyActivity;->mNormalCopyButton:Landroid/widget/RadioButton;

    .line 293
    iget-object v0, p0, Lepson/print/rpcopy/CopyActivity;->mNormalCopyButton:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 294
    iget-object v0, p0, Lepson/print/rpcopy/CopyActivity;->mNormalCopyButton:Landroid/widget/RadioButton;

    new-instance v1, Lepson/print/rpcopy/CopyActivity$4;

    invoke-direct {v1, p0}, Lepson/print/rpcopy/CopyActivity$4;-><init>(Lepson/print/rpcopy/CopyActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    const v0, 0x7f0802aa

    .line 303
    invoke-virtual {p0, v0}, Lepson/print/rpcopy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    return-void
.end method

.method private setupUiPostCommunication()V
    .locals 2

    const v0, 0x7f080269

    .line 312
    invoke-virtual {p0, v0}, Lepson/print/rpcopy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f08026d

    .line 313
    invoke-virtual {p0, v0}, Lepson/print/rpcopy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f08026b

    .line 314
    invoke-virtual {p0, v0}, Lepson/print/rpcopy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f080272

    .line 315
    invoke-virtual {p0, v0}, Lepson/print/rpcopy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f080270

    .line 316
    invoke-virtual {p0, v0}, Lepson/print/rpcopy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private switchToNormalCopy()V
    .locals 1

    const/4 v0, 0x0

    .line 320
    invoke-virtual {p0, v0}, Lepson/print/rpcopy/CopyActivity;->setResult(I)V

    .line 321
    invoke-virtual {p0}, Lepson/print/rpcopy/CopyActivity;->finish()V

    return-void
.end method


# virtual methods
.method allClear()V
    .locals 3

    const/4 v0, 0x1

    .line 485
    iput-boolean v0, p0, Lepson/print/rpcopy/CopyActivity;->doClear:Z

    .line 486
    iget-object v0, p0, Lepson/print/rpcopy/CopyActivity;->loading:Lepson/print/screen/WorkingDialog;

    invoke-virtual {v0}, Lepson/print/screen/WorkingDialog;->show()V

    .line 488
    iget-object v0, p0, Lepson/print/rpcopy/CopyActivity;->copyComponent:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;

    invoke-virtual {v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;->getCopyOptionItems()Ljava/util/ArrayList;

    move-result-object v0

    .line 489
    iget v1, p0, Lepson/print/rpcopy/CopyActivity;->clearindex:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lepson/print/rpcopy/CopyActivity;->clearindex:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    .line 490
    invoke-virtual {v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getKey()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    move-result-object v1

    .line 491
    sget-object v2, Lepson/print/rpcopy/CopyActivity$6;->$SwitchMap$epson$print$rpcopy$Component$ecopycomponent$ECopyOptionItem$ECopyOptionItemKey:[I

    invoke-virtual {v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    goto/16 :goto_0

    .line 534
    :pswitch_0
    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XCutLineWidth_Dash:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    invoke-virtual {v0, v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->selectChoice(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;)V

    goto :goto_0

    .line 531
    :pswitch_1
    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XCutLineStyle_Dash:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    invoke-virtual {v0, v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->selectChoice(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;)V

    goto :goto_0

    .line 528
    :pswitch_2
    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XCutLine_Off:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    invoke-virtual {v0, v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->selectChoice(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;)V

    goto :goto_0

    .line 523
    :pswitch_3
    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XRemoveBackground_Off:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    invoke-virtual {v0, v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->selectChoice(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;)V

    goto :goto_0

    .line 520
    :pswitch_4
    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->RepeatLayout_twoRepeat:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    invoke-virtual {v0, v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->selectChoice(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;)V

    goto :goto_0

    .line 515
    :pswitch_5
    invoke-virtual {v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getDefaultValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->selectValue(I)V

    goto :goto_0

    .line 511
    :pswitch_6
    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintQuality_Normal:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    invoke-virtual {v0, v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->selectChoice(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;)V

    goto :goto_0

    .line 508
    :pswitch_7
    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSource_Bottom:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    invoke-virtual {v0, v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->selectChoice(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;)V

    goto :goto_0

    .line 500
    :pswitch_8
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 501
    invoke-virtual {v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getSelectableChoices()Ljava/util/ArrayList;

    move-result-object v1

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_Letter:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 502
    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_Letter:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    invoke-virtual {v0, v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->selectChoice(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;)V

    goto :goto_0

    .line 504
    :cond_0
    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_A4:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    invoke-virtual {v0, v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->selectChoice(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;)V

    goto :goto_0

    .line 497
    :pswitch_9
    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_Stationery:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    invoke-virtual {v0, v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->selectChoice(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;)V

    goto :goto_0

    .line 494
    :pswitch_a
    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->ColorEffectsType_Color:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    invoke-virtual {v0, v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->selectChoice(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;)V

    .line 542
    :goto_0
    iget-object v1, p0, Lepson/print/rpcopy/CopyActivity;->copyComponent:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;

    invoke-virtual {v1, v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;->setCopyOptionItem(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)V

    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method buildCopyOptions(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;",
            ">;)V"
        }
    .end annotation

    .line 598
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    .line 599
    invoke-virtual {v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getKey()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    move-result-object v1

    .line 600
    sget-object v2, Lepson/print/rpcopy/CopyActivity$6;->$SwitchMap$epson$print$rpcopy$Component$ecopycomponent$ECopyOptionItem$ECopyOptionItemKey:[I

    invoke-virtual {v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto/16 :goto_1

    :pswitch_0
    const v1, 0x7f080273

    .line 644
    invoke-virtual {p0, v1, v0}, Lepson/print/rpcopy/CopyActivity;->updateSetting(ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)V

    goto/16 :goto_1

    :pswitch_1
    const v1, 0x7f08026e

    .line 641
    invoke-virtual {p0, v1, v0}, Lepson/print/rpcopy/CopyActivity;->updateSetting(ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)V

    goto/16 :goto_1

    :pswitch_2
    const v1, 0x7f08026f

    .line 638
    invoke-virtual {p0, v1, v0}, Lepson/print/rpcopy/CopyActivity;->updateSetting(ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)V

    goto/16 :goto_1

    :pswitch_3
    const v1, 0x7f0802a7

    .line 633
    invoke-virtual {p0, v1, v0}, Lepson/print/rpcopy/CopyActivity;->updateSetting(ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)V

    goto/16 :goto_1

    :pswitch_4
    const v1, 0x7f0802a9

    .line 630
    invoke-virtual {p0, v1, v0}, Lepson/print/rpcopy/CopyActivity;->updateSetting(ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)V

    goto/16 :goto_1

    .line 625
    :pswitch_5
    iget-object v2, p0, Lepson/print/rpcopy/CopyActivity;->optionValueMap:Ljava/util/HashMap;

    new-instance v3, Lepson/print/rpcopy/CopyActivity$CopiesValue;

    invoke-direct {v3, p0, v0}, Lepson/print/rpcopy/CopyActivity$CopiesValue;-><init>(Lepson/print/rpcopy/CopyActivity;Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)V

    invoke-virtual {v2, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 622
    :pswitch_6
    iget-object v2, p0, Lepson/print/rpcopy/CopyActivity;->optionValueMap:Ljava/util/HashMap;

    new-instance v3, Lepson/print/rpcopy/CopyActivity$XDensityValue;

    invoke-direct {v3, p0, v0}, Lepson/print/rpcopy/CopyActivity$XDensityValue;-><init>(Lepson/print/rpcopy/CopyActivity;Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)V

    invoke-virtual {v2, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 618
    :pswitch_7
    iget-object v2, p0, Lepson/print/rpcopy/CopyActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "show "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ":"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " setting"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const v1, 0x7f0800f1

    .line 619
    invoke-virtual {p0, v1, v0}, Lepson/print/rpcopy/CopyActivity;->updateSetting(ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)V

    goto/16 :goto_1

    .line 614
    :pswitch_8
    iget-object v2, p0, Lepson/print/rpcopy/CopyActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "show "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ":"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " setting"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const v1, 0x7f0800ef

    .line 615
    invoke-virtual {p0, v1, v0}, Lepson/print/rpcopy/CopyActivity;->updateSetting(ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)V

    goto/16 :goto_1

    .line 610
    :pswitch_9
    iget-object v2, p0, Lepson/print/rpcopy/CopyActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "show "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ":"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " setting"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const v1, 0x7f0800ee

    .line 611
    invoke-virtual {p0, v1, v0}, Lepson/print/rpcopy/CopyActivity;->updateSetting(ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)V

    goto :goto_1

    .line 606
    :pswitch_a
    iget-object v2, p0, Lepson/print/rpcopy/CopyActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "show "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ":"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " setting"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const v1, 0x7f0800f0

    .line 607
    invoke-virtual {p0, v1, v0}, Lepson/print/rpcopy/CopyActivity;->updateSetting(ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)V

    goto :goto_1

    .line 602
    :pswitch_b
    iget-object v2, p0, Lepson/print/rpcopy/CopyActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "show "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ":"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " setting"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const v1, 0x7f0800e5

    .line 603
    invoke-virtual {p0, v1, v0}, Lepson/print/rpcopy/CopyActivity;->updateSetting(ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)V

    .line 650
    :goto_1
    iget-object v0, p0, Lepson/print/rpcopy/CopyActivity;->loading:Lepson/print/screen/WorkingDialog;

    invoke-virtual {v0}, Lepson/print/screen/WorkingDialog;->dismiss()V

    goto/16 :goto_0

    :cond_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected deleteLongTapMessage()V
    .locals 8

    .line 844
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0xa

    add-long/2addr v2, v4

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    const v1, 0x7f080027

    .line 847
    invoke-virtual {p0, v1}, Lepson/print/rpcopy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    const v1, 0x7f080026

    .line 848
    invoke-virtual {p0, v1}, Lepson/print/rpcopy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    const v1, 0x7f08002b

    .line 849
    invoke-virtual {p0, v1}, Lepson/print/rpcopy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    const v1, 0x7f08002a

    .line 850
    invoke-virtual {p0, v1}, Lepson/print/rpcopy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    return-void
.end method

.method fetchCopyOptionContext()V
    .locals 3

    .line 549
    iget-object v0, p0, Lepson/print/rpcopy/CopyActivity;->copyComponent:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;

    iget-object v1, p0, Lepson/print/rpcopy/CopyActivity;->copyType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ECopyType;

    new-instance v2, Lepson/print/rpcopy/CopyActivity$5;

    invoke-direct {v2, p0}, Lepson/print/rpcopy/CopyActivity$5;-><init>(Lepson/print/rpcopy/CopyActivity;)V

    invoke-static {v0, v1, v2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;->createCopyOptionContext(Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ECopyType;Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener;)V

    return-void
.end method

.method public onBackPressed()V
    .locals 0

    .line 228
    invoke-direct {p0}, Lepson/print/rpcopy/CopyActivity;->returnToHome()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .line 366
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 368
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    sparse-switch p1, :sswitch_data_0

    goto/16 :goto_0

    .line 432
    :sswitch_0
    invoke-virtual {p0}, Lepson/print/rpcopy/CopyActivity;->getBaseContext()Landroid/content/Context;

    move-result-object p1

    const-class v1, Lepson/print/rpcopy/CopySettingActivity;

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string p1, "Key"

    .line 433
    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->RepeatLayout:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 434
    invoke-virtual {p0, v0}, Lepson/print/rpcopy/CopyActivity;->startActivityAndKeepSimpleAp(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 439
    :sswitch_1
    invoke-virtual {p0}, Lepson/print/rpcopy/CopyActivity;->getBaseContext()Landroid/content/Context;

    move-result-object p1

    const-class v1, Lepson/print/rpcopy/CopySettingActivity;

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string p1, "Key"

    .line 440
    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->RemoveBackground:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 441
    invoke-virtual {p0, v0}, Lepson/print/rpcopy/CopyActivity;->startActivityAndKeepSimpleAp(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 425
    :sswitch_2
    invoke-virtual {p0}, Lepson/print/rpcopy/CopyActivity;->getBaseContext()Landroid/content/Context;

    move-result-object p1

    const-class v1, Lepson/print/rpcopy/CopySettingActivity;

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string p1, "Key"

    .line 426
    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintQuality:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 427
    invoke-virtual {p0, v0}, Lepson/print/rpcopy/CopyActivity;->startActivityAndKeepSimpleAp(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 460
    :sswitch_3
    invoke-virtual {p0}, Lepson/print/rpcopy/CopyActivity;->getBaseContext()Landroid/content/Context;

    move-result-object p1

    const-class v1, Lepson/print/rpcopy/CopySettingActivity;

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string p1, "Key"

    .line 461
    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XCutLineWeight:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 462
    invoke-virtual {p0, v0}, Lepson/print/rpcopy/CopyActivity;->startActivityAndKeepSimpleAp(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 453
    :sswitch_4
    invoke-virtual {p0}, Lepson/print/rpcopy/CopyActivity;->getBaseContext()Landroid/content/Context;

    move-result-object p1

    const-class v1, Lepson/print/rpcopy/CopySettingActivity;

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string p1, "Key"

    .line 454
    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XCutLineStyle:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 455
    invoke-virtual {p0, v0}, Lepson/print/rpcopy/CopyActivity;->startActivityAndKeepSimpleAp(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 446
    :sswitch_5
    invoke-virtual {p0}, Lepson/print/rpcopy/CopyActivity;->getBaseContext()Landroid/content/Context;

    move-result-object p1

    const-class v1, Lepson/print/rpcopy/CopySettingActivity;

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string p1, "Key"

    .line 447
    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XCutLine:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 448
    invoke-virtual {p0, v0}, Lepson/print/rpcopy/CopyActivity;->startActivityAndKeepSimpleAp(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 404
    :sswitch_6
    invoke-virtual {p0}, Lepson/print/rpcopy/CopyActivity;->getBaseContext()Landroid/content/Context;

    move-result-object p1

    const-class v1, Lepson/print/rpcopy/CopySettingActivity;

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string p1, "Key"

    .line 405
    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 406
    invoke-virtual {p0, v0}, Lepson/print/rpcopy/CopyActivity;->startActivityAndKeepSimpleAp(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 410
    :sswitch_7
    invoke-virtual {p0}, Lepson/print/rpcopy/CopyActivity;->getBaseContext()Landroid/content/Context;

    move-result-object p1

    const-class v1, Lepson/print/rpcopy/CopySettingActivity;

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string p1, "Key"

    .line 411
    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSource:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 412
    invoke-virtual {p0, v0}, Lepson/print/rpcopy/CopyActivity;->startActivityAndKeepSimpleAp(Landroid/content/Intent;)V

    goto :goto_0

    .line 398
    :sswitch_8
    invoke-virtual {p0}, Lepson/print/rpcopy/CopyActivity;->getBaseContext()Landroid/content/Context;

    move-result-object p1

    const-class v1, Lepson/print/rpcopy/CopySettingActivity;

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string p1, "Key"

    .line 399
    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 400
    invoke-virtual {p0, v0}, Lepson/print/rpcopy/CopyActivity;->startActivityAndKeepSimpleAp(Landroid/content/Intent;)V

    goto :goto_0

    .line 379
    :sswitch_9
    invoke-virtual {p0}, Lepson/print/rpcopy/CopyActivity;->allClear()V

    goto :goto_0

    .line 371
    :sswitch_a
    iget-object p1, p0, Lepson/print/rpcopy/CopyActivity;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "NOW_PRINTER:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Lepson/print/rpcopy/CopyActivity;->printerId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 372
    iget-object p1, p0, Lepson/print/rpcopy/CopyActivity;->presettings:Lepson/print/rpcopy/ActivityBase$settingPreference;

    iget-object v0, p0, Lepson/print/rpcopy/CopyActivity;->copyComponent:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;

    invoke-virtual {v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;->getCopyOptionItems()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p1, v0}, Lepson/print/rpcopy/ActivityBase$settingPreference;->saveCopyOptions(Ljava/util/ArrayList;)V

    .line 373
    new-instance p1, Lepson/print/rpcopy/CopyProcess;

    invoke-direct {p1, p0}, Lepson/print/rpcopy/CopyProcess;-><init>(Lepson/print/rpcopy/ActivityBase;)V

    iput-object p1, p0, Lepson/print/rpcopy/CopyActivity;->copyProcess:Lepson/print/rpcopy/CopyProcess;

    .line 374
    iget-object p1, p0, Lepson/print/rpcopy/CopyActivity;->copyProcess:Lepson/print/rpcopy/CopyProcess;

    invoke-virtual {p0}, Lepson/print/rpcopy/CopyActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1, v0}, Lepson/print/rpcopy/CopyProcess;->startCopy(Landroid/content/Context;)V

    goto :goto_0

    .line 383
    :sswitch_b
    invoke-virtual {p0}, Lepson/print/rpcopy/CopyActivity;->getBaseContext()Landroid/content/Context;

    move-result-object p1

    const-class v1, Lepson/print/rpcopy/CopySettingActivity;

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string p1, "Key"

    .line 384
    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->ColorEffectsType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 385
    invoke-virtual {p0, v0}, Lepson/print/rpcopy/CopyActivity;->startActivityAndKeepSimpleAp(Landroid/content/Intent;)V

    :goto_0
    :sswitch_c
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0800d2 -> :sswitch_b
        0x7f0800e4 -> :sswitch_a
        0x7f0800f7 -> :sswitch_9
        0x7f080118 -> :sswitch_c
        0x7f08024a -> :sswitch_8
        0x7f08024f -> :sswitch_7
        0x7f080252 -> :sswitch_6
        0x7f080269 -> :sswitch_5
        0x7f08026b -> :sswitch_4
        0x7f080270 -> :sswitch_3
        0x7f0802a0 -> :sswitch_2
        0x7f0802a4 -> :sswitch_1
        0x7f0802a8 -> :sswitch_0
        0x7f0802d1 -> :sswitch_c
    .end sparse-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .line 856
    invoke-super {p0, p1}, Lepson/print/rpcopy/ActivityBase;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 857
    invoke-virtual {p0}, Lepson/print/rpcopy/CopyActivity;->deleteLongTapMessage()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 84
    invoke-super {p0, p1}, Lepson/print/rpcopy/ActivityBase;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0a004c

    .line 85
    invoke-virtual {p0, p1}, Lepson/print/rpcopy/CopyActivity;->setContentView(I)V

    .line 88
    iget-object p1, p0, Lepson/print/rpcopy/CopyActivity;->copyComponent:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;

    invoke-virtual {p1, p0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;->getRemoteOperationUUID(Landroid/content/Context;)V

    const/4 p1, 0x0

    .line 89
    iput-object p1, p0, Lepson/print/rpcopy/CopyActivity;->mECopyOptionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    const p1, 0x7f0800e3

    .line 92
    invoke-virtual {p0, p1}, Lepson/print/rpcopy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, Lepson/print/rpcopy/CopyActivity;->mLayout:Landroid/view/ViewGroup;

    .line 94
    invoke-direct {p0}, Lepson/print/rpcopy/CopyActivity;->setupUi()V

    .line 95
    invoke-direct {p0}, Lepson/print/rpcopy/CopyActivity;->setClickListener()V

    const p1, 0x7f0e022a

    const/4 v0, 0x1

    .line 98
    invoke-virtual {p0, p1, v0}, Lepson/print/rpcopy/CopyActivity;->setActionBar(IZ)V

    .line 100
    iget-object p1, p0, Lepson/print/rpcopy/CopyActivity;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    if-nez p1, :cond_0

    .line 101
    invoke-static {}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->getInstance()Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object p1

    iput-object p1, p0, Lepson/print/rpcopy/CopyActivity;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    .line 104
    :cond_0
    new-instance p1, Lepson/print/rpcopy/CopyActivity$1;

    invoke-direct {p1, p0}, Lepson/print/rpcopy/CopyActivity$1;-><init>(Lepson/print/rpcopy/CopyActivity;)V

    iput-object p1, p0, Lepson/print/rpcopy/CopyActivity;->optionValueChangedListener:Lepson/print/rpcopy/ActivityBase$OptionItemChangedListener;

    .line 114
    new-instance p1, Lepson/print/rpcopy/CopyActivity$2;

    invoke-direct {p1, p0}, Lepson/print/rpcopy/CopyActivity$2;-><init>(Lepson/print/rpcopy/CopyActivity;)V

    iput-object p1, p0, Lepson/print/rpcopy/CopyActivity;->optionListener:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;

    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .line 223
    invoke-super {p0}, Lepson/print/rpcopy/ActivityBase;->onDestroy()V

    return-void
.end method

.method public onPause()V
    .locals 3

    .line 194
    iget-object v0, p0, Lepson/print/rpcopy/CopyActivity;->TAG:Ljava/lang/String;

    const-string v1, "onPause()"

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    invoke-super {p0}, Lepson/print/rpcopy/ActivityBase;->onPause()V

    .line 196
    iget v0, p0, Lepson/print/rpcopy/CopyActivity;->printerLocation:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    const/4 v2, 0x3

    if-ne v0, v2, :cond_3

    .line 197
    :cond_0
    iget-object v0, p0, Lepson/print/rpcopy/CopyActivity;->task:Lepson/print/rpcopy/CopyActivity$ProbePrinter;

    if-eqz v0, :cond_1

    .line 198
    invoke-virtual {v0, v1}, Lepson/print/rpcopy/CopyActivity$ProbePrinter;->cancel(Z)Z

    .line 201
    :cond_1
    iget-object v0, p0, Lepson/print/rpcopy/CopyActivity;->copyProcess:Lepson/print/rpcopy/CopyProcess;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lepson/print/rpcopy/CopyProcess;->isProccessing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 203
    iget-object v0, p0, Lepson/print/rpcopy/CopyActivity;->copyProcess:Lepson/print/rpcopy/CopyProcess;

    invoke-virtual {v0, v1}, Lepson/print/rpcopy/CopyProcess;->setDisconnectWifi(Z)V

    goto :goto_0

    .line 206
    :cond_2
    iget-boolean v0, p0, Lepson/print/rpcopy/CopyActivity;->isKeepSimpleAPConnection:Z

    if-nez v0, :cond_3

    const-string v0, "printer"

    .line 207
    sget-object v1, Lepson/print/rpcopy/ActivityBase;->printerIp:Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->disconnect(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    .line 215
    :cond_3
    :goto_0
    invoke-virtual {p0}, Lepson/print/rpcopy/CopyActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 216
    invoke-virtual {p0}, Lepson/print/rpcopy/CopyActivity;->deleteLongTapMessage()V

    :cond_4
    return-void
.end method

.method public onResume()V
    .locals 4

    .line 154
    iget-object v0, p0, Lepson/print/rpcopy/CopyActivity;->TAG:Ljava/lang/String;

    const-string v1, "onResume()"

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    invoke-super {p0}, Lepson/print/rpcopy/ActivityBase;->onResume()V

    const/4 v0, 0x0

    .line 158
    iput-boolean v0, p0, Lepson/print/rpcopy/CopyActivity;->isKeepSimpleAPConnection:Z

    .line 161
    invoke-direct {p0}, Lepson/print/rpcopy/CopyActivity;->getPrinterInfo()V

    .line 164
    iget-object v1, p0, Lepson/print/rpcopy/CopyActivity;->copyProcess:Lepson/print/rpcopy/CopyProcess;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lepson/print/rpcopy/CopyProcess;->isProccessing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 166
    iget-object v1, p0, Lepson/print/rpcopy/CopyActivity;->copyProcess:Lepson/print/rpcopy/CopyProcess;

    invoke-virtual {v1, v0}, Lepson/print/rpcopy/CopyProcess;->setDisconnectWifi(Z)V

    goto :goto_1

    :cond_0
    const-string v1, "printer"

    .line 169
    invoke-static {p0, v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->isNeedConnect(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 171
    iput-boolean v0, p0, Lepson/print/rpcopy/CopyActivity;->isTryConnectSimpleAp:Z

    goto :goto_0

    .line 172
    :cond_1
    iget-boolean v1, p0, Lepson/print/rpcopy/CopyActivity;->isTryConnectSimpleAp:Z

    if-nez v1, :cond_2

    const/4 v1, 0x1

    .line 174
    iput-boolean v1, p0, Lepson/print/rpcopy/CopyActivity;->isTryConnectSimpleAp:Z

    const-string v2, "printer"

    const/4 v3, -0x1

    .line 175
    invoke-static {p0, v2, v3}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->reconnect(Landroid/app/Activity;Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 176
    iput-boolean v1, p0, Lepson/print/rpcopy/CopyActivity;->isKeepSimpleAPConnection:Z

    .line 177
    iput-boolean v0, p0, Lepson/print/rpcopy/CopyActivity;->bProbedPrinter:Z

    return-void

    .line 181
    :cond_2
    :goto_0
    iget-object v1, p0, Lepson/print/rpcopy/CopyActivity;->loading:Lepson/print/screen/WorkingDialog;

    invoke-virtual {v1}, Lepson/print/screen/WorkingDialog;->show()V

    .line 182
    iget-boolean v1, p0, Lepson/print/rpcopy/CopyActivity;->bProbedPrinter:Z

    if-nez v1, :cond_3

    .line 184
    iget-object v1, p0, Lepson/print/rpcopy/CopyActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    .line 187
    :cond_3
    iget-object v0, p0, Lepson/print/rpcopy/CopyActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_1
    return-void
.end method

.method startActivityAndKeepSimpleAp(Landroid/content/Intent;)V
    .locals 1

    const/4 v0, 0x1

    .line 476
    iput-boolean v0, p0, Lepson/print/rpcopy/CopyActivity;->isKeepSimpleAPConnection:Z

    .line 477
    invoke-virtual {p0, p1}, Lepson/print/rpcopy/CopyActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method updateSetting(ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)V
    .locals 1

    .line 660
    iget-object v0, p0, Lepson/print/rpcopy/CopyActivity;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    .line 662
    invoke-virtual {p2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v0

    if-nez v0, :cond_0

    const-string p1, "CopyActivity"

    const-string p2, "updateSetting() choice == null"

    .line 664
    invoke-static {p1, p2}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 668
    :cond_0
    invoke-virtual {p2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object p2

    invoke-virtual {p2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p2}, Lepson/print/rpcopy/CopyActivity;->getDisplayString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

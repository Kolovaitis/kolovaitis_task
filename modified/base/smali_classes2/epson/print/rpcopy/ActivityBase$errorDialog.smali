.class public Lepson/print/rpcopy/ActivityBase$errorDialog;
.super Ljava/lang/Object;
.source "ActivityBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/rpcopy/ActivityBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "errorDialog"
.end annotation


# static fields
.field public static final MESSAGE:I = 0x1

.field public static final TITLE:I


# instance fields
.field context:Landroid/content/Context;

.field dialog:Landroid/app/Dialog;

.field final synthetic this$0:Lepson/print/rpcopy/ActivityBase;


# direct methods
.method public constructor <init>(Lepson/print/rpcopy/ActivityBase;Landroid/content/Context;)V
    .locals 0

    .line 177
    iput-object p1, p0, Lepson/print/rpcopy/ActivityBase$errorDialog;->this$0:Lepson/print/rpcopy/ActivityBase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 178
    iput-object p2, p0, Lepson/print/rpcopy/ActivityBase$errorDialog;->context:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method Cancel()V
    .locals 1

    .line 251
    iget-object v0, p0, Lepson/print/rpcopy/ActivityBase$errorDialog;->dialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 252
    invoke-virtual {v0}, Landroid/app/Dialog;->cancel()V

    :cond_0
    return-void
.end method

.method public getReasonText(Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;)[Ljava/lang/String;
    .locals 4

    const/4 v0, 0x2

    .line 293
    new-array v0, v0, [Ljava/lang/String;

    .line 294
    sget-object v1, Lepson/print/rpcopy/ActivityBase$2;->$SwitchMap$epson$print$rpcopy$Component$ecopycomponent$ECopyComponent$ICopyOptionContextListener$ContextCreationError:[I

    invoke-virtual {p1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;->ordinal()I

    move-result p1

    aget p1, v1, p1

    const/4 v1, 0x1

    const/4 v2, 0x0

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 301
    :pswitch_0
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$errorDialog;->this$0:Lepson/print/rpcopy/ActivityBase;

    const v3, 0x7f0e01c4

    invoke-virtual {p1, v3}, Lepson/print/rpcopy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v2

    .line 302
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$errorDialog;->this$0:Lepson/print/rpcopy/ActivityBase;

    const v2, 0x7f0e0041

    invoke-virtual {p1, v2}, Lepson/print/rpcopy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v1

    goto :goto_0

    .line 296
    :pswitch_1
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$errorDialog;->this$0:Lepson/print/rpcopy/ActivityBase;

    const v3, 0x7f0e01b1

    invoke-virtual {p1, v3}, Lepson/print/rpcopy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v2

    .line 297
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$errorDialog;->this$0:Lepson/print/rpcopy/ActivityBase;

    const v2, 0x7f0e01af

    invoke-virtual {p1, v2}, Lepson/print/rpcopy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v1

    :goto_0
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public getReasonText(Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;)[Ljava/lang/String;
    .locals 4

    const/4 v0, 0x2

    .line 316
    new-array v0, v0, [Ljava/lang/String;

    .line 317
    sget-object v1, Lepson/print/rpcopy/ActivityBase$2;->$SwitchMap$epson$print$rpcopy$Component$ecopycomponent$ECopyComponent$ICopyOptionListener$CopyOptionChangedError:[I

    invoke-virtual {p1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;->ordinal()I

    move-result p1

    aget p1, v1, p1

    const/4 v1, 0x1

    const/4 v2, 0x0

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 324
    :pswitch_0
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$errorDialog;->this$0:Lepson/print/rpcopy/ActivityBase;

    const v3, 0x7f0e01c4

    invoke-virtual {p1, v3}, Lepson/print/rpcopy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v2

    .line 325
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$errorDialog;->this$0:Lepson/print/rpcopy/ActivityBase;

    const v2, 0x7f0e0041

    invoke-virtual {p1, v2}, Lepson/print/rpcopy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v1

    goto :goto_0

    .line 319
    :pswitch_1
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$errorDialog;->this$0:Lepson/print/rpcopy/ActivityBase;

    const v3, 0x7f0e01b1

    invoke-virtual {p1, v3}, Lepson/print/rpcopy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v2

    .line 320
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$errorDialog;->this$0:Lepson/print/rpcopy/ActivityBase;

    const v2, 0x7f0e01af

    invoke-virtual {p1, v2}, Lepson/print/rpcopy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v1

    :goto_0
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public getReasonText(Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;)[Ljava/lang/String;
    .locals 4

    const/4 v0, 0x2

    .line 339
    new-array v0, v0, [Ljava/lang/String;

    .line 340
    sget-object v1, Lepson/print/rpcopy/ActivityBase$2;->$SwitchMap$epson$print$rpcopy$Component$ecopycomponent$ECopyComponent$ICopyResumeRequest$StopReason:[I

    invoke-virtual {p1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->ordinal()I

    move-result p1

    aget p1, v1, p1

    const/4 v1, 0x1

    const/4 v2, 0x0

    packed-switch p1, :pswitch_data_0

    goto/16 :goto_0

    .line 374
    :pswitch_0
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$errorDialog;->this$0:Lepson/print/rpcopy/ActivityBase;

    const v3, 0x7f0e04ac

    invoke-virtual {p1, v3}, Lepson/print/rpcopy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v2

    .line 375
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$errorDialog;->this$0:Lepson/print/rpcopy/ActivityBase;

    const v2, 0x7f0e04ab

    invoke-virtual {p1, v2}, Lepson/print/rpcopy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v1

    goto/16 :goto_0

    .line 370
    :pswitch_1
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$errorDialog;->this$0:Lepson/print/rpcopy/ActivityBase;

    const v3, 0x7f0e01c4

    invoke-virtual {p1, v3}, Lepson/print/rpcopy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v2

    .line 371
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$errorDialog;->this$0:Lepson/print/rpcopy/ActivityBase;

    const v2, 0x7f0e0041

    invoke-virtual {p1, v2}, Lepson/print/rpcopy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v1

    goto/16 :goto_0

    .line 366
    :pswitch_2
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$errorDialog;->this$0:Lepson/print/rpcopy/ActivityBase;

    const v3, 0x7f0e01a0

    invoke-virtual {p1, v3}, Lepson/print/rpcopy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v2

    .line 367
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$errorDialog;->this$0:Lepson/print/rpcopy/ActivityBase;

    const v2, 0x7f0e0033

    invoke-virtual {p1, v2}, Lepson/print/rpcopy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v1

    goto/16 :goto_0

    .line 362
    :pswitch_3
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$errorDialog;->this$0:Lepson/print/rpcopy/ActivityBase;

    const v3, 0x7f0e01b7

    invoke-virtual {p1, v3}, Lepson/print/rpcopy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v2

    .line 363
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$errorDialog;->this$0:Lepson/print/rpcopy/ActivityBase;

    const v2, 0x7f0e01b6

    invoke-virtual {p1, v2}, Lepson/print/rpcopy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v1

    goto :goto_0

    .line 358
    :pswitch_4
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$errorDialog;->this$0:Lepson/print/rpcopy/ActivityBase;

    const v3, 0x7f0e01e0

    invoke-virtual {p1, v3}, Lepson/print/rpcopy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v2

    .line 359
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$errorDialog;->this$0:Lepson/print/rpcopy/ActivityBase;

    const v2, 0x7f0e01dd

    invoke-virtual {p1, v2}, Lepson/print/rpcopy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v1

    goto :goto_0

    .line 354
    :pswitch_5
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$errorDialog;->this$0:Lepson/print/rpcopy/ActivityBase;

    const v3, 0x7f0e01f4

    invoke-virtual {p1, v3}, Lepson/print/rpcopy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v2

    .line 355
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$errorDialog;->this$0:Lepson/print/rpcopy/ActivityBase;

    const v2, 0x7f0e01f3

    invoke-virtual {p1, v2}, Lepson/print/rpcopy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v1

    goto :goto_0

    .line 350
    :pswitch_6
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$errorDialog;->this$0:Lepson/print/rpcopy/ActivityBase;

    const v3, 0x7f0e01f0

    invoke-virtual {p1, v3}, Lepson/print/rpcopy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v2

    .line 351
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$errorDialog;->this$0:Lepson/print/rpcopy/ActivityBase;

    const v2, 0x7f0e0037

    invoke-virtual {p1, v2}, Lepson/print/rpcopy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v1

    goto :goto_0

    .line 346
    :pswitch_7
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$errorDialog;->this$0:Lepson/print/rpcopy/ActivityBase;

    const v3, 0x7f0e0214

    invoke-virtual {p1, v3}, Lepson/print/rpcopy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v2

    .line 347
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$errorDialog;->this$0:Lepson/print/rpcopy/ActivityBase;

    const v2, 0x7f0e0213

    invoke-virtual {p1, v2}, Lepson/print/rpcopy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v1

    goto :goto_0

    .line 342
    :pswitch_8
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$errorDialog;->this$0:Lepson/print/rpcopy/ActivityBase;

    const v3, 0x7f0e01cc

    invoke-virtual {p1, v3}, Lepson/print/rpcopy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v2

    .line 343
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$errorDialog;->this$0:Lepson/print/rpcopy/ActivityBase;

    const v2, 0x7f0e01cb

    invoke-virtual {p1, v2}, Lepson/print/rpcopy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v1

    :goto_0
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getReasonText(Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;)[Ljava/lang/String;
    .locals 4

    const/4 v0, 0x2

    .line 263
    new-array v0, v0, [Ljava/lang/String;

    .line 264
    sget-object v1, Lepson/print/rpcopy/ActivityBase$2;->$SwitchMap$epson$print$rpcopy$Component$ecopycomponent$ECopyComponent$ICopyStatusListener$CopyTaskResult:[I

    invoke-virtual {p1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;->ordinal()I

    move-result p1

    aget p1, v1, p1

    const/4 v1, 0x1

    const/4 v2, 0x0

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 278
    :pswitch_0
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$errorDialog;->this$0:Lepson/print/rpcopy/ActivityBase;

    const v3, 0x7f0e01c4

    invoke-virtual {p1, v3}, Lepson/print/rpcopy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v2

    .line 279
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$errorDialog;->this$0:Lepson/print/rpcopy/ActivityBase;

    const v2, 0x7f0e0041

    invoke-virtual {p1, v2}, Lepson/print/rpcopy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v1

    goto :goto_0

    .line 274
    :pswitch_1
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$errorDialog;->this$0:Lepson/print/rpcopy/ActivityBase;

    const v3, 0x7f0e0043

    invoke-virtual {p1, v3}, Lepson/print/rpcopy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v2

    .line 275
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$errorDialog;->this$0:Lepson/print/rpcopy/ActivityBase;

    const v2, 0x7f0e0042

    invoke-virtual {p1, v2}, Lepson/print/rpcopy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v1

    goto :goto_0

    .line 270
    :pswitch_2
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$errorDialog;->this$0:Lepson/print/rpcopy/ActivityBase;

    const v3, 0x7f0e0218

    invoke-virtual {p1, v3}, Lepson/print/rpcopy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v2

    .line 271
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$errorDialog;->this$0:Lepson/print/rpcopy/ActivityBase;

    const v2, 0x7f0e0217

    invoke-virtual {p1, v2}, Lepson/print/rpcopy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v1

    goto :goto_0

    .line 266
    :pswitch_3
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$errorDialog;->this$0:Lepson/print/rpcopy/ActivityBase;

    const v3, 0x7f0e01b1

    invoke-virtual {p1, v3}, Lepson/print/rpcopy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v2

    .line 267
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$errorDialog;->this$0:Lepson/print/rpcopy/ActivityBase;

    const v2, 0x7f0e0035

    invoke-virtual {p1, v2}, Lepson/print/rpcopy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v1

    :goto_0
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method showErrorDialog(I)V
    .locals 1

    .line 186
    iget-object v0, p0, Lepson/print/rpcopy/ActivityBase$errorDialog;->this$0:Lepson/print/rpcopy/ActivityBase;

    invoke-virtual {v0, p1}, Lepson/print/rpcopy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lepson/print/rpcopy/ActivityBase$errorDialog;->showErrorDialog(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method showErrorDialog(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 182
    sget-object v0, Lepson/print/rpcopy/ActivityBase$DialogButtons;->Ok:Lepson/print/rpcopy/ActivityBase$DialogButtons;

    const/4 v1, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, Lepson/print/rpcopy/ActivityBase$errorDialog;->showErrorDialog(Ljava/lang/String;Ljava/lang/String;Lepson/print/rpcopy/ActivityBase$DialogButtons;Lepson/print/rpcopy/ActivityBase$IClose;)V

    return-void
.end method

.method showErrorDialog(Ljava/lang/String;Ljava/lang/String;Lepson/print/rpcopy/ActivityBase$DialogButtons;Lepson/print/rpcopy/ActivityBase$IClose;)V
    .locals 5

    .line 197
    new-instance v0, Lepson/print/widgets/CustomTitleAlertDialogBuilder;

    iget-object v1, p0, Lepson/print/rpcopy/ActivityBase$errorDialog;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lepson/print/widgets/CustomTitleAlertDialogBuilder;-><init>(Landroid/content/Context;)V

    .line 201
    sget-object v1, Lepson/print/rpcopy/ActivityBase$2;->$SwitchMap$epson$print$rpcopy$ActivityBase$DialogButtons:[I

    invoke-virtual {p3}, Lepson/print/rpcopy/ActivityBase$DialogButtons;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const v2, 0x7f0e0476

    const/4 v3, 0x0

    packed-switch v1, :pswitch_data_0

    move-object v1, v3

    goto :goto_0

    .line 211
    :pswitch_0
    iget-object v1, p0, Lepson/print/rpcopy/ActivityBase$errorDialog;->this$0:Lepson/print/rpcopy/ActivityBase;

    const v3, 0x7f0e0482

    invoke-virtual {v1, v3}, Lepson/print/rpcopy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 212
    iget-object v1, p0, Lepson/print/rpcopy/ActivityBase$errorDialog;->this$0:Lepson/print/rpcopy/ActivityBase;

    invoke-virtual {v1, v2}, Lepson/print/rpcopy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 207
    :pswitch_1
    iget-object v1, p0, Lepson/print/rpcopy/ActivityBase$errorDialog;->this$0:Lepson/print/rpcopy/ActivityBase;

    invoke-virtual {v1, v2}, Lepson/print/rpcopy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 203
    :pswitch_2
    iget-object v1, p0, Lepson/print/rpcopy/ActivityBase$errorDialog;->this$0:Lepson/print/rpcopy/ActivityBase;

    const v2, 0x7f0e04f2

    invoke-virtual {v1, v2}, Lepson/print/rpcopy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object v4, v3

    move-object v3, v1

    move-object v1, v4

    :goto_0
    const/4 v2, 0x0

    if-eqz p1, :cond_0

    .line 218
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 220
    :cond_0
    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 221
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    if-eqz v3, :cond_1

    .line 223
    new-instance p1, Lepson/print/rpcopy/ActivityBase$errorDialog$1;

    invoke-direct {p1, p0, p4, p3}, Lepson/print/rpcopy/ActivityBase$errorDialog$1;-><init>(Lepson/print/rpcopy/ActivityBase$errorDialog;Lepson/print/rpcopy/ActivityBase$IClose;Lepson/print/rpcopy/ActivityBase$DialogButtons;)V

    invoke-virtual {v0, v3, p1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    :cond_1
    if-eqz v1, :cond_2

    .line 234
    new-instance p1, Lepson/print/rpcopy/ActivityBase$errorDialog$2;

    invoke-direct {p1, p0, p4}, Lepson/print/rpcopy/ActivityBase$errorDialog$2;-><init>(Lepson/print/rpcopy/ActivityBase$errorDialog;Lepson/print/rpcopy/ActivityBase$IClose;)V

    invoke-virtual {v0, v1, p1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 243
    :cond_2
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    iput-object p1, p0, Lepson/print/rpcopy/ActivityBase$errorDialog;->dialog:Landroid/app/Dialog;

    .line 244
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$errorDialog;->dialog:Landroid/app/Dialog;

    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

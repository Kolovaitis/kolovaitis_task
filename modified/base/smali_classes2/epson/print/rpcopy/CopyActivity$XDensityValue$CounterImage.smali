.class Lepson/print/rpcopy/CopyActivity$XDensityValue$CounterImage;
.super Lepson/print/rpcopy/ActivityBase$NumberOptionValue$Counter;
.source "CopyActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/rpcopy/CopyActivity$XDensityValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CounterImage"
.end annotation


# instance fields
.field final synthetic this$1:Lepson/print/rpcopy/CopyActivity$XDensityValue;


# direct methods
.method public constructor <init>(Lepson/print/rpcopy/CopyActivity$XDensityValue;I)V
    .locals 0

    .line 729
    iput-object p1, p0, Lepson/print/rpcopy/CopyActivity$XDensityValue$CounterImage;->this$1:Lepson/print/rpcopy/CopyActivity$XDensityValue;

    .line 730
    invoke-direct {p0, p1, p2}, Lepson/print/rpcopy/ActivityBase$NumberOptionValue$Counter;-><init>(Lepson/print/rpcopy/ActivityBase$NumberOptionValue;I)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 0

    .line 743
    iget p1, p0, Lepson/print/rpcopy/CopyActivity$XDensityValue$CounterImage;->amount:I

    invoke-virtual {p0, p1}, Lepson/print/rpcopy/CopyActivity$XDensityValue$CounterImage;->updateImage(I)V

    return-void
.end method

.method updateImage(I)V
    .locals 2

    .line 734
    iget-object v0, p0, Lepson/print/rpcopy/CopyActivity$XDensityValue$CounterImage;->this$1:Lepson/print/rpcopy/CopyActivity$XDensityValue;

    iget v1, v0, Lepson/print/rpcopy/CopyActivity$XDensityValue;->value:I

    add-int/2addr v1, p1

    iput v1, v0, Lepson/print/rpcopy/CopyActivity$XDensityValue;->value:I

    .line 735
    iget-object p1, p0, Lepson/print/rpcopy/CopyActivity$XDensityValue$CounterImage;->this$1:Lepson/print/rpcopy/CopyActivity$XDensityValue;

    iget-object p1, p1, Lepson/print/rpcopy/CopyActivity$XDensityValue;->optionItem:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    iget-object v0, p0, Lepson/print/rpcopy/CopyActivity$XDensityValue$CounterImage;->this$1:Lepson/print/rpcopy/CopyActivity$XDensityValue;

    iget v0, v0, Lepson/print/rpcopy/CopyActivity$XDensityValue;->value:I

    invoke-virtual {p1, v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->selectValue(I)V

    .line 736
    iget-object p1, p0, Lepson/print/rpcopy/CopyActivity$XDensityValue$CounterImage;->this$1:Lepson/print/rpcopy/CopyActivity$XDensityValue;

    iget-object p1, p1, Lepson/print/rpcopy/CopyActivity$XDensityValue;->changedListener:Lepson/print/rpcopy/ActivityBase$OptionItemChangedListener;

    if-eqz p1, :cond_0

    .line 737
    iget-object p1, p0, Lepson/print/rpcopy/CopyActivity$XDensityValue$CounterImage;->this$1:Lepson/print/rpcopy/CopyActivity$XDensityValue;

    iget-object p1, p1, Lepson/print/rpcopy/CopyActivity$XDensityValue;->changedListener:Lepson/print/rpcopy/ActivityBase$OptionItemChangedListener;

    iget-object v0, p0, Lepson/print/rpcopy/CopyActivity$XDensityValue$CounterImage;->this$1:Lepson/print/rpcopy/CopyActivity$XDensityValue;

    iget-object v0, v0, Lepson/print/rpcopy/CopyActivity$XDensityValue;->optionItem:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    invoke-interface {p1, v0}, Lepson/print/rpcopy/ActivityBase$OptionItemChangedListener;->onOptionItemChanged(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)V

    :cond_0
    return-void
.end method

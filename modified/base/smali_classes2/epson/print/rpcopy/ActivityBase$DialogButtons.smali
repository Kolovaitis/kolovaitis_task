.class final enum Lepson/print/rpcopy/ActivityBase$DialogButtons;
.super Ljava/lang/Enum;
.source "ActivityBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/rpcopy/ActivityBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "DialogButtons"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lepson/print/rpcopy/ActivityBase$DialogButtons;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lepson/print/rpcopy/ActivityBase$DialogButtons;

.field public static final enum Cancel:Lepson/print/rpcopy/ActivityBase$DialogButtons;

.field public static final enum ClearErrorCancel:Lepson/print/rpcopy/ActivityBase$DialogButtons;

.field public static final enum Ok:Lepson/print/rpcopy/ActivityBase$DialogButtons;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 149
    new-instance v0, Lepson/print/rpcopy/ActivityBase$DialogButtons;

    const-string v1, "Ok"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lepson/print/rpcopy/ActivityBase$DialogButtons;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/rpcopy/ActivityBase$DialogButtons;->Ok:Lepson/print/rpcopy/ActivityBase$DialogButtons;

    .line 150
    new-instance v0, Lepson/print/rpcopy/ActivityBase$DialogButtons;

    const-string v1, "Cancel"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lepson/print/rpcopy/ActivityBase$DialogButtons;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/rpcopy/ActivityBase$DialogButtons;->Cancel:Lepson/print/rpcopy/ActivityBase$DialogButtons;

    .line 151
    new-instance v0, Lepson/print/rpcopy/ActivityBase$DialogButtons;

    const-string v1, "ClearErrorCancel"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lepson/print/rpcopy/ActivityBase$DialogButtons;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/rpcopy/ActivityBase$DialogButtons;->ClearErrorCancel:Lepson/print/rpcopy/ActivityBase$DialogButtons;

    const/4 v0, 0x3

    .line 148
    new-array v0, v0, [Lepson/print/rpcopy/ActivityBase$DialogButtons;

    sget-object v1, Lepson/print/rpcopy/ActivityBase$DialogButtons;->Ok:Lepson/print/rpcopy/ActivityBase$DialogButtons;

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/ActivityBase$DialogButtons;->Cancel:Lepson/print/rpcopy/ActivityBase$DialogButtons;

    aput-object v1, v0, v3

    sget-object v1, Lepson/print/rpcopy/ActivityBase$DialogButtons;->ClearErrorCancel:Lepson/print/rpcopy/ActivityBase$DialogButtons;

    aput-object v1, v0, v4

    sput-object v0, Lepson/print/rpcopy/ActivityBase$DialogButtons;->$VALUES:[Lepson/print/rpcopy/ActivityBase$DialogButtons;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 148
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lepson/print/rpcopy/ActivityBase$DialogButtons;
    .locals 1

    .line 148
    const-class v0, Lepson/print/rpcopy/ActivityBase$DialogButtons;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lepson/print/rpcopy/ActivityBase$DialogButtons;

    return-object p0
.end method

.method public static values()[Lepson/print/rpcopy/ActivityBase$DialogButtons;
    .locals 1

    .line 148
    sget-object v0, Lepson/print/rpcopy/ActivityBase$DialogButtons;->$VALUES:[Lepson/print/rpcopy/ActivityBase$DialogButtons;

    invoke-virtual {v0}, [Lepson/print/rpcopy/ActivityBase$DialogButtons;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lepson/print/rpcopy/ActivityBase$DialogButtons;

    return-object v0
.end method

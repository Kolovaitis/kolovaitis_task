.class Lepson/print/rpcopy/ActivityBase$settingPreference;
.super Ljava/lang/Object;
.source "ActivityBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/rpcopy/ActivityBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "settingPreference"
.end annotation


# instance fields
.field private editer:Landroid/content/SharedPreferences$Editor;

.field private presettings:Landroid/content/SharedPreferences;

.field final synthetic this$0:Lepson/print/rpcopy/ActivityBase;


# direct methods
.method constructor <init>(Lepson/print/rpcopy/ActivityBase;)V
    .locals 0

    .line 568
    iput-object p1, p0, Lepson/print/rpcopy/ActivityBase$settingPreference;->this$0:Lepson/print/rpcopy/ActivityBase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getPreferences()Landroid/content/SharedPreferences;
    .locals 3

    .line 573
    iget-object v0, p0, Lepson/print/rpcopy/ActivityBase$settingPreference;->this$0:Lepson/print/rpcopy/ActivityBase;

    const-string v1, "RepeatCopySetting"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lepson/print/rpcopy/ActivityBase;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method private saveOption(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)V
    .locals 3

    .line 666
    invoke-direct {p0}, Lepson/print/rpcopy/ActivityBase$settingPreference;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lepson/print/rpcopy/ActivityBase$settingPreference;->presettings:Landroid/content/SharedPreferences;

    .line 667
    invoke-virtual {p2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getChoiceType()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;

    move-result-object v0

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;->ChoiceArray:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;

    if-ne v0, v1, :cond_0

    .line 669
    iget-object v0, p0, Lepson/print/rpcopy/ActivityBase$settingPreference;->this$0:Lepson/print/rpcopy/ActivityBase;

    invoke-static {v0}, Lepson/print/rpcopy/ActivityBase;->access$000(Lepson/print/rpcopy/ActivityBase;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cursetting:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v2

    invoke-virtual {v2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 670
    iget-object v0, p0, Lepson/print/rpcopy/ActivityBase$settingPreference;->presettings:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iput-object v0, p0, Lepson/print/rpcopy/ActivityBase$settingPreference;->editer:Landroid/content/SharedPreferences$Editor;

    .line 671
    iget-object v0, p0, Lepson/print/rpcopy/ActivityBase$settingPreference;->editer:Landroid/content/SharedPreferences$Editor;

    invoke-virtual {p1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->name()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object p2

    invoke-virtual {p2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->getParam()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object p2

    invoke-virtual {p2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 672
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$settingPreference;->editer:Landroid/content/SharedPreferences$Editor;

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0

    .line 675
    :cond_0
    iget-object v0, p0, Lepson/print/rpcopy/ActivityBase$settingPreference;->this$0:Lepson/print/rpcopy/ActivityBase;

    invoke-static {v0}, Lepson/print/rpcopy/ActivityBase;->access$000(Lepson/print/rpcopy/ActivityBase;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cursetting:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getSelectedValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 676
    iget-object v0, p0, Lepson/print/rpcopy/ActivityBase$settingPreference;->presettings:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iput-object v0, p0, Lepson/print/rpcopy/ActivityBase$settingPreference;->editer:Landroid/content/SharedPreferences$Editor;

    .line 677
    iget-object v0, p0, Lepson/print/rpcopy/ActivityBase$settingPreference;->editer:Landroid/content/SharedPreferences$Editor;

    invoke-virtual {p1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->name()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getSelectedValue()I

    move-result p2

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 678
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$settingPreference;->editer:Landroid/content/SharedPreferences$Editor;

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :goto_0
    return-void
.end method

.method private savePrePrinter()V
    .locals 3

    .line 589
    invoke-direct {p0}, Lepson/print/rpcopy/ActivityBase$settingPreference;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lepson/print/rpcopy/ActivityBase$settingPreference;->presettings:Landroid/content/SharedPreferences;

    .line 591
    iget-object v0, p0, Lepson/print/rpcopy/ActivityBase$settingPreference;->presettings:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iput-object v0, p0, Lepson/print/rpcopy/ActivityBase$settingPreference;->editer:Landroid/content/SharedPreferences$Editor;

    .line 592
    iget-object v0, p0, Lepson/print/rpcopy/ActivityBase$settingPreference;->editer:Landroid/content/SharedPreferences$Editor;

    const-string v1, "PRINTER_ID"

    sget-object v2, Lepson/print/rpcopy/ActivityBase;->printerId:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 593
    iget-object v0, p0, Lepson/print/rpcopy/ActivityBase$settingPreference;->editer:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method private setOption(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)V
    .locals 3

    .line 628
    invoke-direct {p0}, Lepson/print/rpcopy/ActivityBase$settingPreference;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lepson/print/rpcopy/ActivityBase$settingPreference;->presettings:Landroid/content/SharedPreferences;

    .line 629
    invoke-virtual {p2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getChoiceType()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;

    move-result-object v0

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;->ChoiceArray:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;

    if-ne v0, v1, :cond_1

    .line 631
    invoke-virtual {p2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getDefaultChoice()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getDefaultChoice()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v0

    invoke-virtual {v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->getParam()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 634
    iget-object v0, p0, Lepson/print/rpcopy/ActivityBase$settingPreference;->presettings:Landroid/content/SharedPreferences;

    invoke-virtual {p1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getDefaultChoice()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v2

    invoke-virtual {v2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->getParam()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v2

    invoke-virtual {v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 635
    invoke-static {v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->stringOf(Ljava/lang/String;)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v0

    .line 636
    invoke-static {p1, v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->valueOf(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object p1

    invoke-virtual {p2, p1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->selectChoice(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;)V

    goto :goto_0

    .line 632
    :cond_0
    new-instance p1, Ljava/lang/RuntimeException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "key: <"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getKey()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p2, "> error"

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 639
    :cond_1
    iget-object v0, p0, Lepson/print/rpcopy/ActivityBase$settingPreference;->presettings:Landroid/content/SharedPreferences;

    invoke-virtual {p1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->name()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getDefaultValue()I

    move-result v1

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result p1

    .line 640
    invoke-virtual {p2, p1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->selectValue(I)V

    :goto_0
    return-void
.end method


# virtual methods
.method loadPrePrinter()Ljava/lang/String;
    .locals 3

    .line 580
    invoke-direct {p0}, Lepson/print/rpcopy/ActivityBase$settingPreference;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lepson/print/rpcopy/ActivityBase$settingPreference;->presettings:Landroid/content/SharedPreferences;

    .line 581
    iget-object v0, p0, Lepson/print/rpcopy/ActivityBase$settingPreference;->presettings:Landroid/content/SharedPreferences;

    const-string v1, "PRINTER_ID"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method saveCopyOptions(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;",
            ">;)V"
        }
    .end annotation

    .line 649
    invoke-direct {p0}, Lepson/print/rpcopy/ActivityBase$settingPreference;->savePrePrinter()V

    .line 651
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    .line 652
    invoke-virtual {v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getKey()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    move-result-object v1

    .line 653
    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->Copies:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v1, v2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 655
    invoke-direct {p0, v1, v0}, Lepson/print/rpcopy/ActivityBase$settingPreference;->saveOption(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method setCopyOptions(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;",
            ">;)V"
        }
    .end annotation

    .line 604
    iget-object v0, p0, Lepson/print/rpcopy/ActivityBase$settingPreference;->this$0:Lepson/print/rpcopy/ActivityBase;

    iget-object v0, v0, Lepson/print/rpcopy/ActivityBase;->copyComponent:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;

    invoke-virtual {v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;->getBindedCopyOptionContext()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    move-result-object v0

    .line 606
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    .line 607
    invoke-virtual {v2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getKey()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    move-result-object v3

    .line 608
    sget-object v4, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->Copies:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v3, v4}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 610
    invoke-direct {p0, v3, v2}, Lepson/print/rpcopy/ActivityBase$settingPreference;->setOption(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)V

    .line 611
    invoke-virtual {v0, v2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->replace(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    goto :goto_0

    .line 615
    :cond_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    .line 618
    iget-object v1, p0, Lepson/print/rpcopy/ActivityBase$settingPreference;->this$0:Lepson/print/rpcopy/ActivityBase;

    iget-object v1, v1, Lepson/print/rpcopy/ActivityBase;->copyComponent:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;

    invoke-virtual {v1, v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;->setCopyOptionItem(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)V

    goto :goto_1

    :cond_2
    return-void
.end method

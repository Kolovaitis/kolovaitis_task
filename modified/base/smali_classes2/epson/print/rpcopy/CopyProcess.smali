.class public Lepson/print/rpcopy/CopyProcess;
.super Ljava/lang/Object;
.source "CopyProcess.java"

# interfaces
.implements Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;


# instance fields
.field activityBase:Lepson/print/rpcopy/ActivityBase;

.field cancelRequest:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyCancelRequest;

.field copyComponent:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;

.field disconnectWifi:Z

.field jobToken:Ljava/lang/String;

.field printererror:Lepson/print/rpcopy/ActivityBase$errorDialog;

.field wheel:Lepson/print/rpcopy/ActivityBase$WheelDialog;


# direct methods
.method public constructor <init>(Lepson/print/rpcopy/ActivityBase;)V
    .locals 1

    const/4 v0, 0x0

    .line 39
    invoke-direct {p0, p1, v0}, Lepson/print/rpcopy/CopyProcess;-><init>(Lepson/print/rpcopy/ActivityBase;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Lepson/print/rpcopy/ActivityBase;Ljava/lang/String;)V
    .locals 1

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    invoke-static {}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;->sharedComponent()Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;

    move-result-object v0

    iput-object v0, p0, Lepson/print/rpcopy/CopyProcess;->copyComponent:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;

    const/4 v0, 0x0

    .line 36
    iput-boolean v0, p0, Lepson/print/rpcopy/CopyProcess;->disconnectWifi:Z

    .line 43
    iput-object p1, p0, Lepson/print/rpcopy/CopyProcess;->activityBase:Lepson/print/rpcopy/ActivityBase;

    .line 44
    iput-object p2, p0, Lepson/print/rpcopy/CopyProcess;->jobToken:Ljava/lang/String;

    .line 45
    new-instance p1, Lepson/print/rpcopy/ActivityBase$WheelDialog;

    iget-object p2, p0, Lepson/print/rpcopy/CopyProcess;->activityBase:Lepson/print/rpcopy/ActivityBase;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lepson/print/rpcopy/CopyProcess;->activityBase:Lepson/print/rpcopy/ActivityBase;

    invoke-direct {p1, p2, v0}, Lepson/print/rpcopy/ActivityBase$WheelDialog;-><init>(Lepson/print/rpcopy/ActivityBase;Landroid/content/Context;)V

    iput-object p1, p0, Lepson/print/rpcopy/CopyProcess;->wheel:Lepson/print/rpcopy/ActivityBase$WheelDialog;

    return-void
.end method


# virtual methods
.method public isProccessing()Z
    .locals 1

    .line 176
    iget-object v0, p0, Lepson/print/rpcopy/CopyProcess;->wheel:Lepson/print/rpcopy/ActivityBase$WheelDialog;

    invoke-virtual {v0}, Lepson/print/rpcopy/ActivityBase$WheelDialog;->isShowing()Z

    move-result v0

    return v0
.end method

.method public onFinished(Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;)V
    .locals 3

    .line 128
    iget-object p1, p0, Lepson/print/rpcopy/CopyProcess;->wheel:Lepson/print/rpcopy/ActivityBase$WheelDialog;

    invoke-virtual {p1}, Lepson/print/rpcopy/ActivityBase$WheelDialog;->dismiss()V

    .line 130
    sget-object p1, Lepson/print/rpcopy/CopyProcess$5;->$SwitchMap$epson$print$rpcopy$Component$ecopycomponent$ECopyComponent$ICopyStatusListener$CopyTaskResult:[I

    invoke-virtual {p2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;->ordinal()I

    move-result v0

    aget p1, p1, v0

    packed-switch p1, :pswitch_data_0

    goto :goto_1

    .line 149
    :pswitch_0
    new-instance p1, Lepson/print/rpcopy/ActivityBase$errorDialog;

    iget-object v0, p0, Lepson/print/rpcopy/CopyProcess;->activityBase:Lepson/print/rpcopy/ActivityBase;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v1, p0, Lepson/print/rpcopy/CopyProcess;->activityBase:Lepson/print/rpcopy/ActivityBase;

    invoke-direct {p1, v0, v1}, Lepson/print/rpcopy/ActivityBase$errorDialog;-><init>(Lepson/print/rpcopy/ActivityBase;Landroid/content/Context;)V

    iput-object p1, p0, Lepson/print/rpcopy/CopyProcess;->printererror:Lepson/print/rpcopy/ActivityBase$errorDialog;

    .line 150
    iget-object p1, p0, Lepson/print/rpcopy/CopyProcess;->printererror:Lepson/print/rpcopy/ActivityBase$errorDialog;

    invoke-virtual {p1, p2}, Lepson/print/rpcopy/ActivityBase$errorDialog;->getReasonText(Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;)[Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x0

    .line 151
    aget-object p2, p1, p2

    const/4 v0, 0x1

    .line 152
    aget-object p1, p1, v0

    .line 153
    sget-object v0, Lepson/print/rpcopy/ActivityBase$DialogButtons;->Ok:Lepson/print/rpcopy/ActivityBase$DialogButtons;

    .line 154
    iget-object v1, p0, Lepson/print/rpcopy/CopyProcess;->printererror:Lepson/print/rpcopy/ActivityBase$errorDialog;

    new-instance v2, Lepson/print/rpcopy/CopyProcess$4;

    invoke-direct {v2, p0}, Lepson/print/rpcopy/CopyProcess$4;-><init>(Lepson/print/rpcopy/CopyProcess;)V

    invoke-virtual {v1, p2, p1, v0, v2}, Lepson/print/rpcopy/ActivityBase$errorDialog;->showErrorDialog(Ljava/lang/String;Ljava/lang/String;Lepson/print/rpcopy/ActivityBase$DialogButtons;Lepson/print/rpcopy/ActivityBase$IClose;)V

    goto :goto_1

    .line 133
    :pswitch_1
    iget-object p1, p0, Lepson/print/rpcopy/CopyProcess;->copyComponent:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;

    invoke-virtual {p1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;->getCopyOptionItems()Ljava/util/ArrayList;

    move-result-object p1

    .line 134
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    .line 135
    invoke-virtual {p2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getKey()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    move-result-object v0

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->Copies:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v0, v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    invoke-virtual {p2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getDefaultValue()I

    move-result v0

    invoke-virtual {p2, v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->selectValue(I)V

    .line 139
    iget-object v0, p0, Lepson/print/rpcopy/CopyProcess;->copyComponent:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;

    invoke-virtual {v0, p2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;->setCopyOptionItem(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)V

    goto :goto_0

    .line 164
    :cond_1
    :goto_1
    :pswitch_2
    iget-boolean p1, p0, Lepson/print/rpcopy/CopyProcess;->disconnectWifi:Z

    if-eqz p1, :cond_2

    .line 166
    iget-object p1, p0, Lepson/print/rpcopy/CopyProcess;->activityBase:Lepson/print/rpcopy/ActivityBase;

    const-string p2, "printer"

    sget-object v0, Lepson/print/rpcopy/ActivityBase;->printerIp:Ljava/lang/String;

    invoke-static {p1, p2, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->disconnect(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    :cond_2
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onProcessed(Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;IILepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest;)V
    .locals 3

    .line 68
    iget-object p1, p0, Lepson/print/rpcopy/CopyProcess;->printererror:Lepson/print/rpcopy/ActivityBase$errorDialog;

    if-eqz p1, :cond_0

    .line 70
    invoke-virtual {p1}, Lepson/print/rpcopy/ActivityBase$errorDialog;->Cancel()V

    .line 72
    :cond_0
    sget-object p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;->Copying:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    invoke-virtual {p4, p1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_2

    sget-object p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;->Processing:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    invoke-virtual {p4, p1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_0

    .line 76
    :cond_1
    iget-object p1, p0, Lepson/print/rpcopy/CopyProcess;->wheel:Lepson/print/rpcopy/ActivityBase$WheelDialog;

    const-string p2, ""

    invoke-virtual {p1, p2}, Lepson/print/rpcopy/ActivityBase$WheelDialog;->setText(Ljava/lang/String;)V

    goto :goto_1

    .line 74
    :cond_2
    :goto_0
    iget-object p1, p0, Lepson/print/rpcopy/CopyProcess;->wheel:Lepson/print/rpcopy/ActivityBase$WheelDialog;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lepson/print/rpcopy/CopyProcess;->activityBase:Lepson/print/rpcopy/ActivityBase;

    const v2, 0x7f0e0040

    invoke-virtual {v1, v2}, Lepson/print/rpcopy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p3, "/"

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lepson/print/rpcopy/ActivityBase$WheelDialog;->setText(Ljava/lang/String;)V

    .line 79
    :goto_1
    sget-object p1, Lepson/print/rpcopy/CopyProcess$5;->$SwitchMap$epson$print$rpcopy$Component$ecopycomponent$ECopyComponent$ICopyStatusListener$CopyTaskProgress:[I

    invoke-virtual {p4}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;->ordinal()I

    move-result p2

    aget p1, p1, p2

    packed-switch p1, :pswitch_data_0

    goto/16 :goto_4

    .line 114
    :pswitch_0
    iget-object p1, p0, Lepson/print/rpcopy/CopyProcess;->wheel:Lepson/print/rpcopy/ActivityBase$WheelDialog;

    invoke-virtual {p1}, Lepson/print/rpcopy/ActivityBase$WheelDialog;->hideCancelButton()V

    goto/16 :goto_4

    .line 106
    :pswitch_1
    iget-object p1, p0, Lepson/print/rpcopy/CopyProcess;->activityBase:Lepson/print/rpcopy/ActivityBase;

    new-instance p2, Lepson/print/rpcopy/CopyProcess$3;

    invoke-direct {p2, p0, p5}, Lepson/print/rpcopy/CopyProcess$3;-><init>(Lepson/print/rpcopy/CopyProcess;Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest;)V

    invoke-virtual {p1, p2}, Lepson/print/rpcopy/ActivityBase;->showNextPageDialog(Lepson/print/rpcopy/ActivityBase$INextPageClose;)V

    goto :goto_4

    .line 81
    :pswitch_2
    new-instance p1, Lepson/print/rpcopy/ActivityBase$errorDialog;

    iget-object p2, p0, Lepson/print/rpcopy/CopyProcess;->activityBase:Lepson/print/rpcopy/ActivityBase;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object p3, p0, Lepson/print/rpcopy/CopyProcess;->activityBase:Lepson/print/rpcopy/ActivityBase;

    invoke-direct {p1, p2, p3}, Lepson/print/rpcopy/ActivityBase$errorDialog;-><init>(Lepson/print/rpcopy/ActivityBase;Landroid/content/Context;)V

    iput-object p1, p0, Lepson/print/rpcopy/CopyProcess;->printererror:Lepson/print/rpcopy/ActivityBase$errorDialog;

    .line 82
    invoke-interface {p5}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest;->getStopReason()Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    move-result-object p1

    .line 83
    sget-object p2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->PrinterMediaEmptyError:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    invoke-virtual {p1, p2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_4

    .line 84
    iget-object p2, p0, Lepson/print/rpcopy/CopyProcess;->copyComponent:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;

    invoke-virtual {p2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;->getCopyOptionItems()Ljava/util/ArrayList;

    move-result-object p2

    .line 85
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_3
    :goto_2
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_4

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    .line 86
    invoke-virtual {p3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getKey()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    move-result-object p4

    sget-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSource:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {p4, v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->equals(Ljava/lang/Object;)Z

    move-result p4

    if-eqz p4, :cond_3

    .line 87
    invoke-virtual {p3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object p3

    sget-object p4, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSource_Manual:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    invoke-virtual {p3, p4}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_3

    .line 89
    sget-object p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->ManualfeedGuide:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    goto :goto_2

    .line 94
    :cond_4
    iget-object p2, p0, Lepson/print/rpcopy/CopyProcess;->printererror:Lepson/print/rpcopy/ActivityBase$errorDialog;

    invoke-virtual {p2, p1}, Lepson/print/rpcopy/ActivityBase$errorDialog;->getReasonText(Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;)[Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x0

    .line 95
    aget-object p2, p1, p2

    const/4 p3, 0x1

    .line 96
    aget-object p1, p1, p3

    .line 97
    invoke-interface {p5}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest;->isPossibleClearError()Z

    move-result p3

    if-eqz p3, :cond_5

    sget-object p3, Lepson/print/rpcopy/ActivityBase$DialogButtons;->ClearErrorCancel:Lepson/print/rpcopy/ActivityBase$DialogButtons;

    goto :goto_3

    :cond_5
    sget-object p3, Lepson/print/rpcopy/ActivityBase$DialogButtons;->Cancel:Lepson/print/rpcopy/ActivityBase$DialogButtons;

    .line 98
    :goto_3
    iget-object p4, p0, Lepson/print/rpcopy/CopyProcess;->printererror:Lepson/print/rpcopy/ActivityBase$errorDialog;

    new-instance v0, Lepson/print/rpcopy/CopyProcess$2;

    invoke-direct {v0, p0, p5}, Lepson/print/rpcopy/CopyProcess$2;-><init>(Lepson/print/rpcopy/CopyProcess;Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest;)V

    invoke-virtual {p4, p2, p1, p3, v0}, Lepson/print/rpcopy/ActivityBase$errorDialog;->showErrorDialog(Ljava/lang/String;Ljava/lang/String;Lepson/print/rpcopy/ActivityBase$DialogButtons;Lepson/print/rpcopy/ActivityBase$IClose;)V

    :goto_4
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onStarted(Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;)V
    .locals 2

    .line 59
    iget-object p1, p0, Lepson/print/rpcopy/CopyProcess;->wheel:Lepson/print/rpcopy/ActivityBase$WheelDialog;

    iget-object v0, p0, Lepson/print/rpcopy/CopyProcess;->activityBase:Lepson/print/rpcopy/ActivityBase;

    const v1, 0x7f0e0040

    invoke-virtual {v0, v1}, Lepson/print/rpcopy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lepson/print/rpcopy/CopyProcess$1;

    invoke-direct {v1, p0}, Lepson/print/rpcopy/CopyProcess$1;-><init>(Lepson/print/rpcopy/CopyProcess;)V

    invoke-virtual {p1, v0, v1}, Lepson/print/rpcopy/ActivityBase$WheelDialog;->show(Ljava/lang/String;Lepson/print/rpcopy/ActivityBase$CancelRequestCallback;)V

    return-void
.end method

.method public setDisconnectWifi(Z)V
    .locals 0

    .line 185
    iput-boolean p1, p0, Lepson/print/rpcopy/CopyProcess;->disconnectWifi:Z

    return-void
.end method

.method public startCopy(Landroid/content/Context;)V
    .locals 1

    .line 54
    iget-object v0, p0, Lepson/print/rpcopy/CopyProcess;->jobToken:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lepson/print/rpcopy/CopyProcess;->copyComponent:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;

    invoke-virtual {v0, p0, p1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;->startCopy(Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;Landroid/content/Context;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyCancelRequest;

    move-result-object p1

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lepson/print/rpcopy/CopyProcess;->copyComponent:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;

    invoke-virtual {p1, v0, p0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;->recoverCopy(Ljava/lang/String;Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyCancelRequest;

    move-result-object p1

    :goto_0
    iput-object p1, p0, Lepson/print/rpcopy/CopyProcess;->cancelRequest:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyCancelRequest;

    return-void
.end method

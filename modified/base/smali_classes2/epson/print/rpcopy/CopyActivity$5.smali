.class Lepson/print/rpcopy/CopyActivity$5;
.super Ljava/lang/Object;
.source "CopyActivity.java"

# interfaces
.implements Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/rpcopy/CopyActivity;->fetchCopyOptionContext()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/rpcopy/CopyActivity;


# direct methods
.method constructor <init>(Lepson/print/rpcopy/CopyActivity;)V
    .locals 0

    .line 549
    iput-object p1, p0, Lepson/print/rpcopy/CopyActivity$5;->this$0:Lepson/print/rpcopy/CopyActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCopyOptionContextCreated(Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ECopyType;Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;)V
    .locals 2

    .line 553
    iget-object p1, p0, Lepson/print/rpcopy/CopyActivity$5;->this$0:Lepson/print/rpcopy/CopyActivity;

    iput-object p2, p1, Lepson/print/rpcopy/CopyActivity;->mECopyOptionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    if-nez p3, :cond_2

    .line 555
    iget-object p1, p1, Lepson/print/rpcopy/CopyActivity;->copyComponent:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;

    iget-object p2, p0, Lepson/print/rpcopy/CopyActivity$5;->this$0:Lepson/print/rpcopy/CopyActivity;

    iget-object p2, p2, Lepson/print/rpcopy/CopyActivity;->mECopyOptionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    iget-object p3, p0, Lepson/print/rpcopy/CopyActivity$5;->this$0:Lepson/print/rpcopy/CopyActivity;

    iget-object p3, p3, Lepson/print/rpcopy/CopyActivity;->optionListener:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;

    invoke-virtual {p1, p2, p3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;->bindCopyOptionContext(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;)V

    .line 556
    iget-object p1, p0, Lepson/print/rpcopy/CopyActivity$5;->this$0:Lepson/print/rpcopy/CopyActivity;

    iget-object p1, p1, Lepson/print/rpcopy/CopyActivity;->copyComponent:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;

    invoke-virtual {p1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;->getCopyOptionItems()Ljava/util/ArrayList;

    move-result-object p1

    .line 559
    iget-object p2, p0, Lepson/print/rpcopy/CopyActivity$5;->this$0:Lepson/print/rpcopy/CopyActivity;

    iget-object p2, p2, Lepson/print/rpcopy/CopyActivity;->copyComponent:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;

    invoke-virtual {p2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;->getBindedCopyOptionContext()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    move-result-object p2

    if-eqz p2, :cond_0

    .line 560
    sget-object p3, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XCutLine:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {p2, p3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->hasCopyOptionItemOf(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 561
    iget-object p2, p0, Lepson/print/rpcopy/CopyActivity$5;->this$0:Lepson/print/rpcopy/CopyActivity;

    invoke-static {p2}, Lepson/print/rpcopy/CopyActivity;->access$800(Lepson/print/rpcopy/CopyActivity;)V

    .line 562
    iget-object p2, p0, Lepson/print/rpcopy/CopyActivity$5;->this$0:Lepson/print/rpcopy/CopyActivity;

    invoke-static {p2}, Lepson/print/rpcopy/CopyActivity;->access$900(Lepson/print/rpcopy/CopyActivity;)V

    .line 565
    :cond_0
    sget-object p2, Lepson/print/rpcopy/ActivityBase;->printerId:Ljava/lang/String;

    iget-object p3, p0, Lepson/print/rpcopy/CopyActivity$5;->this$0:Lepson/print/rpcopy/CopyActivity;

    invoke-static {p3}, Lepson/print/rpcopy/CopyActivity;->access$1000(Lepson/print/rpcopy/CopyActivity;)Lepson/print/rpcopy/ActivityBase$settingPreference;

    move-result-object p3

    invoke-virtual {p3}, Lepson/print/rpcopy/ActivityBase$settingPreference;->loadPrePrinter()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_1

    .line 569
    iget-object p2, p0, Lepson/print/rpcopy/CopyActivity$5;->this$0:Lepson/print/rpcopy/CopyActivity;

    invoke-static {p2}, Lepson/print/rpcopy/CopyActivity;->access$1000(Lepson/print/rpcopy/CopyActivity;)Lepson/print/rpcopy/ActivityBase$settingPreference;

    move-result-object p2

    invoke-virtual {p2, p1}, Lepson/print/rpcopy/ActivityBase$settingPreference;->setCopyOptions(Ljava/util/ArrayList;)V

    goto :goto_0

    .line 572
    :cond_1
    iget-object p2, p0, Lepson/print/rpcopy/CopyActivity$5;->this$0:Lepson/print/rpcopy/CopyActivity;

    invoke-virtual {p2, p1}, Lepson/print/rpcopy/CopyActivity;->buildCopyOptions(Ljava/util/ArrayList;)V

    goto :goto_0

    .line 575
    :cond_2
    new-instance p2, Lepson/print/rpcopy/ActivityBase$errorDialog;

    invoke-direct {p2, p1, p1}, Lepson/print/rpcopy/ActivityBase$errorDialog;-><init>(Lepson/print/rpcopy/ActivityBase;Landroid/content/Context;)V

    invoke-static {p1, p2}, Lepson/print/rpcopy/CopyActivity;->access$502(Lepson/print/rpcopy/CopyActivity;Lepson/print/rpcopy/ActivityBase$errorDialog;)Lepson/print/rpcopy/ActivityBase$errorDialog;

    .line 576
    iget-object p1, p0, Lepson/print/rpcopy/CopyActivity$5;->this$0:Lepson/print/rpcopy/CopyActivity;

    invoke-static {p1}, Lepson/print/rpcopy/CopyActivity;->access$500(Lepson/print/rpcopy/CopyActivity;)Lepson/print/rpcopy/ActivityBase$errorDialog;

    move-result-object p1

    invoke-virtual {p1, p3}, Lepson/print/rpcopy/ActivityBase$errorDialog;->getReasonText(Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;)[Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x0

    .line 577
    aget-object p2, p1, p2

    const/4 p3, 0x1

    .line 578
    aget-object p1, p1, p3

    .line 579
    iget-object p3, p0, Lepson/print/rpcopy/CopyActivity$5;->this$0:Lepson/print/rpcopy/CopyActivity;

    invoke-static {p3}, Lepson/print/rpcopy/CopyActivity;->access$500(Lepson/print/rpcopy/CopyActivity;)Lepson/print/rpcopy/ActivityBase$errorDialog;

    move-result-object p3

    sget-object v0, Lepson/print/rpcopy/ActivityBase$DialogButtons;->Ok:Lepson/print/rpcopy/ActivityBase$DialogButtons;

    new-instance v1, Lepson/print/rpcopy/CopyActivity$5$1;

    invoke-direct {v1, p0}, Lepson/print/rpcopy/CopyActivity$5$1;-><init>(Lepson/print/rpcopy/CopyActivity$5;)V

    invoke-virtual {p3, p2, p1, v0, v1}, Lepson/print/rpcopy/ActivityBase$errorDialog;->showErrorDialog(Ljava/lang/String;Ljava/lang/String;Lepson/print/rpcopy/ActivityBase$DialogButtons;Lepson/print/rpcopy/ActivityBase$IClose;)V

    :goto_0
    return-void
.end method

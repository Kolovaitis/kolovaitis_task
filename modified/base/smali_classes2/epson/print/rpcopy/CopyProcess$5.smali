.class synthetic Lepson/print/rpcopy/CopyProcess$5;
.super Ljava/lang/Object;
.source "CopyProcess.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/rpcopy/CopyProcess;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$epson$print$rpcopy$Component$ecopycomponent$ECopyComponent$ICopyStatusListener$CopyTaskProgress:[I

.field static final synthetic $SwitchMap$epson$print$rpcopy$Component$ecopycomponent$ECopyComponent$ICopyStatusListener$CopyTaskResult:[I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 130
    invoke-static {}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;->values()[Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lepson/print/rpcopy/CopyProcess$5;->$SwitchMap$epson$print$rpcopy$Component$ecopycomponent$ECopyComponent$ICopyStatusListener$CopyTaskResult:[I

    const/4 v0, 0x1

    :try_start_0
    sget-object v1, Lepson/print/rpcopy/CopyProcess$5;->$SwitchMap$epson$print$rpcopy$Component$ecopycomponent$ECopyComponent$ICopyStatusListener$CopyTaskResult:[I

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;->Succeed:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    invoke-virtual {v2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;->ordinal()I

    move-result v2

    aput v0, v1, v2
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v1, 0x2

    :try_start_1
    sget-object v2, Lepson/print/rpcopy/CopyProcess$5;->$SwitchMap$epson$print$rpcopy$Component$ecopycomponent$ECopyComponent$ICopyStatusListener$CopyTaskResult:[I

    sget-object v3, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;->Canceled:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    invoke-virtual {v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;->ordinal()I

    move-result v3

    aput v1, v2, v3
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    const/4 v2, 0x3

    :try_start_2
    sget-object v3, Lepson/print/rpcopy/CopyProcess$5;->$SwitchMap$epson$print$rpcopy$Component$ecopycomponent$ECopyComponent$ICopyStatusListener$CopyTaskResult:[I

    sget-object v4, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;->Busy:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    invoke-virtual {v4}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;->ordinal()I

    move-result v4

    aput v2, v3, v4
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    :try_start_3
    sget-object v3, Lepson/print/rpcopy/CopyProcess$5;->$SwitchMap$epson$print$rpcopy$Component$ecopycomponent$ECopyComponent$ICopyStatusListener$CopyTaskResult:[I

    sget-object v4, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;->ErrorCommunication:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    invoke-virtual {v4}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;->ordinal()I

    move-result v4

    const/4 v5, 0x4

    aput v5, v3, v4
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    :try_start_4
    sget-object v3, Lepson/print/rpcopy/CopyProcess$5;->$SwitchMap$epson$print$rpcopy$Component$ecopycomponent$ECopyComponent$ICopyStatusListener$CopyTaskResult:[I

    sget-object v4, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;->RemoveAdfPaper:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    invoke-virtual {v4}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;->ordinal()I

    move-result v4

    const/4 v5, 0x5

    aput v5, v3, v4
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :catch_4
    :try_start_5
    sget-object v3, Lepson/print/rpcopy/CopyProcess$5;->$SwitchMap$epson$print$rpcopy$Component$ecopycomponent$ECopyComponent$ICopyStatusListener$CopyTaskResult:[I

    sget-object v4, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;->ErrorOther:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    invoke-virtual {v4}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;->ordinal()I

    move-result v4

    const/4 v5, 0x6

    aput v5, v3, v4
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    .line 79
    :catch_5
    invoke-static {}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;->values()[Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    move-result-object v3

    array-length v3, v3

    new-array v3, v3, [I

    sput-object v3, Lepson/print/rpcopy/CopyProcess$5;->$SwitchMap$epson$print$rpcopy$Component$ecopycomponent$ECopyComponent$ICopyStatusListener$CopyTaskProgress:[I

    :try_start_6
    sget-object v3, Lepson/print/rpcopy/CopyProcess$5;->$SwitchMap$epson$print$rpcopy$Component$ecopycomponent$ECopyComponent$ICopyStatusListener$CopyTaskProgress:[I

    sget-object v4, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;->Stopped:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    invoke-virtual {v4}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;->ordinal()I

    move-result v4

    aput v0, v3, v4
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_6

    :catch_6
    :try_start_7
    sget-object v0, Lepson/print/rpcopy/CopyProcess$5;->$SwitchMap$epson$print$rpcopy$Component$ecopycomponent$ECopyComponent$ICopyStatusListener$CopyTaskProgress:[I

    sget-object v3, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;->Waiting2ndPage:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    invoke-virtual {v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;->ordinal()I

    move-result v3

    aput v1, v0, v3
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_7

    :catch_7
    :try_start_8
    sget-object v0, Lepson/print/rpcopy/CopyProcess$5;->$SwitchMap$epson$print$rpcopy$Component$ecopycomponent$ECopyComponent$ICopyStatusListener$CopyTaskProgress:[I

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;->Canceling:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    invoke-virtual {v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;->ordinal()I

    move-result v1

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_8

    :catch_8
    return-void
.end method

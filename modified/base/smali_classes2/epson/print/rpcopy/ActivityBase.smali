.class Lepson/print/rpcopy/ActivityBase;
.super Lepson/print/ActivityIACommon;
.source "ActivityBase.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "Registered"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/rpcopy/ActivityBase$settingPreference;,
        Lepson/print/rpcopy/ActivityBase$CopyMagnificationValue;,
        Lepson/print/rpcopy/ActivityBase$NumberOptionValue;,
        Lepson/print/rpcopy/ActivityBase$ListOptionValue;,
        Lepson/print/rpcopy/ActivityBase$OptionValue;,
        Lepson/print/rpcopy/ActivityBase$OptionItemChangedListener;,
        Lepson/print/rpcopy/ActivityBase$INextPageClose;,
        Lepson/print/rpcopy/ActivityBase$errorDialog;,
        Lepson/print/rpcopy/ActivityBase$IClose;,
        Lepson/print/rpcopy/ActivityBase$ClickButton;,
        Lepson/print/rpcopy/ActivityBase$DialogButtons;,
        Lepson/print/rpcopy/ActivityBase$WheelDialog;,
        Lepson/print/rpcopy/ActivityBase$CancelRequestCallback;
    }
.end annotation


# static fields
.field static final Title:Ljava/lang/String; = "Title"

.field private static final mParamStringToDisplayStringHash:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field static printerId:Ljava/lang/String;

.field static printerIp:Ljava/lang/String;


# instance fields
.field private TAG:Ljava/lang/String;

.field copyComponent:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;

.field copyType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ECopyType;

.field isKeepSimpleAPConnection:Z

.field isTryConnectSimpleAp:Z

.field loading:Lepson/print/screen/WorkingDialog;

.field mECopyOptionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

.field optionValueMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;",
            "Lepson/print/rpcopy/ActivityBase$OptionValue;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 49
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lepson/print/rpcopy/ActivityBase;->mParamStringToDisplayStringHash:Ljava/util/HashMap;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .line 42
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    const-string v0, "ActivityBase"

    .line 43
    iput-object v0, p0, Lepson/print/rpcopy/ActivityBase;->TAG:Ljava/lang/String;

    .line 53
    invoke-static {}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;->sharedComponent()Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;

    move-result-object v0

    iput-object v0, p0, Lepson/print/rpcopy/ActivityBase;->copyComponent:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;

    .line 54
    sget-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ECopyType;->Standard:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ECopyType;

    iput-object v0, p0, Lepson/print/rpcopy/ActivityBase;->copyType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ECopyType;

    .line 57
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lepson/print/rpcopy/ActivityBase;->optionValueMap:Ljava/util/HashMap;

    const/4 v0, 0x0

    .line 60
    iput-boolean v0, p0, Lepson/print/rpcopy/ActivityBase;->isKeepSimpleAPConnection:Z

    .line 61
    iput-boolean v0, p0, Lepson/print/rpcopy/ActivityBase;->isTryConnectSimpleAp:Z

    return-void
.end method

.method static synthetic access$000(Lepson/print/rpcopy/ActivityBase;)Ljava/lang/String;
    .locals 0

    .line 42
    iget-object p0, p0, Lepson/print/rpcopy/ActivityBase;->TAG:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method buildCopyOptions(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method getDisplayString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 555
    sget-object v0, Lepson/print/rpcopy/ActivityBase;->mParamStringToDisplayStringHash:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lepson/print/rpcopy/ActivityBase;->mParamStringToDisplayStringHash:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_0

    :cond_0
    sget-object p1, Lepson/print/rpcopy/ActivityBase;->mParamStringToDisplayStringHash:Ljava/util/HashMap;

    const-string v0, "Unknown"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    :goto_0
    check-cast p1, Ljava/lang/String;

    return-object p1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .line 141
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    .line 143
    new-instance p1, Lepson/print/screen/WorkingDialog;

    invoke-direct {p1, p0}, Lepson/print/screen/WorkingDialog;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lepson/print/rpcopy/ActivityBase;->loading:Lepson/print/screen/WorkingDialog;

    .line 145
    sget-object p1, Lepson/print/rpcopy/ActivityBase;->mParamStringToDisplayStringHash:Ljava/util/HashMap;

    invoke-static {p0, p1}, Lepson/print/copy/DisplayUtil;->setMap(Landroid/content/Context;Ljava/util/HashMap;)V

    return-void
.end method

.method showNextPageDialog(Lepson/print/rpcopy/ActivityBase$INextPageClose;)V
    .locals 2

    .line 389
    new-instance v0, Lepson/print/rpcopy/ActivityBase$1;

    invoke-direct {v0, p0, p1}, Lepson/print/rpcopy/ActivityBase$1;-><init>(Lepson/print/rpcopy/ActivityBase;Lepson/print/rpcopy/ActivityBase$INextPageClose;)V

    .line 404
    new-instance p1, Landroid/app/AlertDialog$Builder;

    invoke-direct {p1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0e0545

    .line 405
    invoke-virtual {p1, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    const/4 v1, 0x1

    .line 406
    invoke-virtual {p1, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f0e03dd

    .line 407
    invoke-virtual {p0, v1}, Lepson/print/rpcopy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f0e03dc

    .line 408
    invoke-virtual {p1, v1, v0}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f0e0476

    .line 409
    invoke-virtual {p0, v1}, Lepson/print/rpcopy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 411
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 412
    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    return-void
.end method

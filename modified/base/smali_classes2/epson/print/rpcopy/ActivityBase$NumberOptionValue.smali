.class Lepson/print/rpcopy/ActivityBase$NumberOptionValue;
.super Lepson/print/rpcopy/ActivityBase$OptionValue;
.source "ActivityBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/rpcopy/ActivityBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "NumberOptionValue"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/rpcopy/ActivityBase$NumberOptionValue$Counter;
    }
.end annotation


# instance fields
.field countDown:Landroid/widget/Button;

.field countUp:Landroid/widget/Button;

.field editText:Landroid/widget/TextView;

.field final synthetic this$0:Lepson/print/rpcopy/ActivityBase;

.field value:I


# direct methods
.method constructor <init>(Lepson/print/rpcopy/ActivityBase;)V
    .locals 0

    .line 475
    iput-object p1, p0, Lepson/print/rpcopy/ActivityBase$NumberOptionValue;->this$0:Lepson/print/rpcopy/ActivityBase;

    invoke-direct {p0, p1}, Lepson/print/rpcopy/ActivityBase$OptionValue;-><init>(Lepson/print/rpcopy/ActivityBase;)V

    return-void
.end method


# virtual methods
.method bindCountDown(I)V
    .locals 2

    .line 530
    iget-object v0, p0, Lepson/print/rpcopy/ActivityBase$NumberOptionValue;->this$0:Lepson/print/rpcopy/ActivityBase;

    invoke-virtual {v0, p1}, Lepson/print/rpcopy/ActivityBase;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lepson/print/rpcopy/ActivityBase$NumberOptionValue;->countDown:Landroid/widget/Button;

    .line 531
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$NumberOptionValue;->countDown:Landroid/widget/Button;

    new-instance v0, Lepson/print/rpcopy/ActivityBase$NumberOptionValue$Counter;

    const/4 v1, -0x1

    invoke-direct {v0, p0, v1}, Lepson/print/rpcopy/ActivityBase$NumberOptionValue$Counter;-><init>(Lepson/print/rpcopy/ActivityBase$NumberOptionValue;I)V

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 532
    iget p1, p0, Lepson/print/rpcopy/ActivityBase$NumberOptionValue;->value:I

    iget-object v0, p0, Lepson/print/rpcopy/ActivityBase$NumberOptionValue;->optionItem:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    invoke-virtual {v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getMinimumValue()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 533
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$NumberOptionValue;->countDown:Landroid/widget/Button;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 535
    :cond_0
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$NumberOptionValue;->countDown:Landroid/widget/Button;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    :goto_0
    return-void
.end method

.method bindCountUp(I)V
    .locals 2

    .line 520
    iget-object v0, p0, Lepson/print/rpcopy/ActivityBase$NumberOptionValue;->this$0:Lepson/print/rpcopy/ActivityBase;

    invoke-virtual {v0, p1}, Lepson/print/rpcopy/ActivityBase;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lepson/print/rpcopy/ActivityBase$NumberOptionValue;->countUp:Landroid/widget/Button;

    .line 521
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$NumberOptionValue;->countUp:Landroid/widget/Button;

    new-instance v0, Lepson/print/rpcopy/ActivityBase$NumberOptionValue$Counter;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Lepson/print/rpcopy/ActivityBase$NumberOptionValue$Counter;-><init>(Lepson/print/rpcopy/ActivityBase$NumberOptionValue;I)V

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 522
    iget p1, p0, Lepson/print/rpcopy/ActivityBase$NumberOptionValue;->value:I

    iget-object v0, p0, Lepson/print/rpcopy/ActivityBase$NumberOptionValue;->optionItem:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    invoke-virtual {v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getMaximumValue()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 523
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$NumberOptionValue;->countUp:Landroid/widget/Button;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 525
    :cond_0
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$NumberOptionValue;->countUp:Landroid/widget/Button;

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setEnabled(Z)V

    :goto_0
    return-void
.end method

.method bindOption(ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)V
    .locals 1

    .line 512
    iput-object p2, p0, Lepson/print/rpcopy/ActivityBase$NumberOptionValue;->optionItem:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    .line 513
    iget-object v0, p0, Lepson/print/rpcopy/ActivityBase$NumberOptionValue;->this$0:Lepson/print/rpcopy/ActivityBase;

    invoke-virtual {v0, p1}, Lepson/print/rpcopy/ActivityBase;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lepson/print/rpcopy/ActivityBase$NumberOptionValue;->editText:Landroid/widget/TextView;

    .line 514
    invoke-virtual {p2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getSelectedValue()I

    move-result p1

    iput p1, p0, Lepson/print/rpcopy/ActivityBase$NumberOptionValue;->value:I

    .line 515
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$NumberOptionValue;->editText:Landroid/widget/TextView;

    iget v0, p0, Lepson/print/rpcopy/ActivityBase$NumberOptionValue;->value:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 516
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$NumberOptionValue;->editText:Landroid/widget/TextView;

    invoke-virtual {p2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->isEnabled()Z

    move-result p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void
.end method

.method bindOption(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)V
    .locals 0

    .line 507
    iput-object p1, p0, Lepson/print/rpcopy/ActivityBase$NumberOptionValue;->optionItem:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    .line 508
    invoke-virtual {p1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getSelectedValue()I

    move-result p1

    iput p1, p0, Lepson/print/rpcopy/ActivityBase$NumberOptionValue;->value:I

    return-void
.end method

.method updateWithAddition(I)V
    .locals 2

    .line 483
    :try_start_0
    iget-object v0, p0, Lepson/print/rpcopy/ActivityBase$NumberOptionValue;->editText:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lepson/print/rpcopy/ActivityBase$NumberOptionValue;->value:I

    .line 484
    iget-object v0, p0, Lepson/print/rpcopy/ActivityBase$NumberOptionValue;->optionItem:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    invoke-virtual {v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getMinimumValue()I

    move-result v0

    iget v1, p0, Lepson/print/rpcopy/ActivityBase$NumberOptionValue;->value:I

    add-int/2addr v1, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result p1

    iget-object v0, p0, Lepson/print/rpcopy/ActivityBase$NumberOptionValue;->optionItem:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    invoke-virtual {v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getMaximumValue()I

    move-result v0

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result p1

    iput p1, p0, Lepson/print/rpcopy/ActivityBase$NumberOptionValue;->value:I

    .line 485
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$NumberOptionValue;->optionItem:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    iget v0, p0, Lepson/print/rpcopy/ActivityBase$NumberOptionValue;->value:I

    invoke-virtual {p1, v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->selectValue(I)V

    .line 486
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$NumberOptionValue;->changedListener:Lepson/print/rpcopy/ActivityBase$OptionItemChangedListener;

    if-eqz p1, :cond_0

    .line 487
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$NumberOptionValue;->changedListener:Lepson/print/rpcopy/ActivityBase$OptionItemChangedListener;

    iget-object v0, p0, Lepson/print/rpcopy/ActivityBase$NumberOptionValue;->optionItem:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    invoke-interface {p1, v0}, Lepson/print/rpcopy/ActivityBase$OptionItemChangedListener;->onOptionItemChanged(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method

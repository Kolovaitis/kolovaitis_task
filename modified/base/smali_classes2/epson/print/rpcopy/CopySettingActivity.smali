.class public Lepson/print/rpcopy/CopySettingActivity;
.super Lepson/print/rpcopy/ActivityBase;
.source "CopySettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/rpcopy/CopySettingActivity$OthersettingValue;
    }
.end annotation


# instance fields
.field private TAG:Ljava/lang/String;

.field optionListener:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;

.field optionValueChangedListener:Lepson/print/rpcopy/ActivityBase$OptionItemChangedListener;

.field settingItemKey:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 30
    invoke-direct {p0}, Lepson/print/rpcopy/ActivityBase;-><init>()V

    const-string v0, "CopySettingActivity"

    .line 31
    iput-object v0, p0, Lepson/print/rpcopy/CopySettingActivity;->TAG:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method buildCopyOptions(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;",
            ">;)V"
        }
    .end annotation

    .line 88
    iget-object v0, p0, Lepson/print/rpcopy/CopySettingActivity;->TAG:Ljava/lang/String;

    const-string v1, "buildCopyOptions"

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    .line 90
    invoke-virtual {v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getKey()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    move-result-object v1

    .line 91
    invoke-virtual {v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->name()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lepson/print/rpcopy/CopySettingActivity;->settingItemKey:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 92
    iget-object v2, p0, Lepson/print/rpcopy/CopySettingActivity;->optionValueMap:Ljava/util/HashMap;

    new-instance v3, Lepson/print/rpcopy/CopySettingActivity$OthersettingValue;

    invoke-direct {v3, p0, v0}, Lepson/print/rpcopy/CopySettingActivity$OthersettingValue;-><init>(Lepson/print/rpcopy/CopySettingActivity;Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)V

    invoke-virtual {v2, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    const/4 v0, 0x1

    .line 183
    iput-boolean v0, p0, Lepson/print/rpcopy/CopySettingActivity;->isKeepSimpleAPConnection:Z

    .line 184
    invoke-virtual {p0}, Lepson/print/rpcopy/CopySettingActivity;->finish()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .line 39
    invoke-super {p0, p1}, Lepson/print/rpcopy/ActivityBase;->onCreate(Landroid/os/Bundle;)V

    .line 40
    iget-object p1, p0, Lepson/print/rpcopy/CopySettingActivity;->TAG:Ljava/lang/String;

    const-string v0, "onCreate"

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const p1, 0x7f0a004f

    .line 41
    invoke-virtual {p0, p1}, Lepson/print/rpcopy/CopySettingActivity;->setContentView(I)V

    .line 43
    invoke-virtual {p0}, Lepson/print/rpcopy/CopySettingActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    if-eqz p1, :cond_0

    const-string v0, "Key"

    .line 45
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/print/rpcopy/CopySettingActivity;->settingItemKey:Ljava/lang/String;

    .line 48
    iget-object p1, p0, Lepson/print/rpcopy/CopySettingActivity;->settingItemKey:Ljava/lang/String;

    invoke-virtual {p0, p1}, Lepson/print/rpcopy/CopySettingActivity;->getDisplayString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lepson/print/rpcopy/CopySettingActivity;->setActionBar(Ljava/lang/String;Z)V

    .line 51
    :cond_0
    new-instance p1, Lepson/print/rpcopy/CopySettingActivity$1;

    invoke-direct {p1, p0}, Lepson/print/rpcopy/CopySettingActivity$1;-><init>(Lepson/print/rpcopy/CopySettingActivity;)V

    iput-object p1, p0, Lepson/print/rpcopy/CopySettingActivity;->optionValueChangedListener:Lepson/print/rpcopy/ActivityBase$OptionItemChangedListener;

    .line 60
    new-instance p1, Lepson/print/rpcopy/CopySettingActivity$2;

    invoke-direct {p1, p0}, Lepson/print/rpcopy/CopySettingActivity$2;-><init>(Lepson/print/rpcopy/CopySettingActivity;)V

    iput-object p1, p0, Lepson/print/rpcopy/CopySettingActivity;->optionListener:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;

    .line 76
    iget-object p1, p0, Lepson/print/rpcopy/CopySettingActivity;->copyComponent:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;

    invoke-virtual {p1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;->getBindedCopyOptionContext()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    move-result-object p1

    iput-object p1, p0, Lepson/print/rpcopy/CopySettingActivity;->mECopyOptionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    .line 77
    iget-object p1, p0, Lepson/print/rpcopy/CopySettingActivity;->copyComponent:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;

    iget-object v0, p0, Lepson/print/rpcopy/CopySettingActivity;->mECopyOptionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    iget-object v1, p0, Lepson/print/rpcopy/CopySettingActivity;->optionListener:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;

    invoke-virtual {p1, v0, v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;->bindCopyOptionContext(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;)V

    .line 79
    iget-object p1, p0, Lepson/print/rpcopy/CopySettingActivity;->copyComponent:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;

    invoke-virtual {p1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;->getCopyOptionItems()Ljava/util/ArrayList;

    move-result-object p1

    invoke-virtual {p0, p1}, Lepson/print/rpcopy/CopySettingActivity;->buildCopyOptions(Ljava/util/ArrayList;)V

    return-void
.end method

.method protected onPause()V
    .locals 2

    .line 209
    invoke-super {p0}, Lepson/print/rpcopy/ActivityBase;->onPause()V

    .line 211
    iget-boolean v0, p0, Lepson/print/rpcopy/CopySettingActivity;->isKeepSimpleAPConnection:Z

    if-nez v0, :cond_0

    const-string v0, "printer"

    .line 213
    sget-object v1, Lepson/print/rpcopy/ActivityBase;->printerIp:Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->disconnect(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 3

    .line 189
    invoke-super {p0}, Lepson/print/rpcopy/ActivityBase;->onResume()V

    const-string v0, "printer"

    .line 192
    invoke-static {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->isNeedConnect(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 194
    iput-boolean v0, p0, Lepson/print/rpcopy/CopySettingActivity;->isTryConnectSimpleAp:Z

    goto :goto_0

    .line 195
    :cond_0
    iget-boolean v0, p0, Lepson/print/rpcopy/CopySettingActivity;->isTryConnectSimpleAp:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 197
    iput-boolean v0, p0, Lepson/print/rpcopy/CopySettingActivity;->isTryConnectSimpleAp:Z

    const-string v1, "printer"

    const/4 v2, -0x1

    .line 198
    invoke-static {p0, v1, v2}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->reconnect(Landroid/app/Activity;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 199
    iput-boolean v0, p0, Lepson/print/rpcopy/CopySettingActivity;->isKeepSimpleAPConnection:Z

    return-void

    :cond_1
    :goto_0
    return-void
.end method

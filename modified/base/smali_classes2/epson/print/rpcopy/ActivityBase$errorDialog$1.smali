.class Lepson/print/rpcopy/ActivityBase$errorDialog$1;
.super Ljava/lang/Object;
.source "ActivityBase.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/rpcopy/ActivityBase$errorDialog;->showErrorDialog(Ljava/lang/String;Ljava/lang/String;Lepson/print/rpcopy/ActivityBase$DialogButtons;Lepson/print/rpcopy/ActivityBase$IClose;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lepson/print/rpcopy/ActivityBase$errorDialog;

.field final synthetic val$buttons:Lepson/print/rpcopy/ActivityBase$DialogButtons;

.field final synthetic val$closeListener:Lepson/print/rpcopy/ActivityBase$IClose;


# direct methods
.method constructor <init>(Lepson/print/rpcopy/ActivityBase$errorDialog;Lepson/print/rpcopy/ActivityBase$IClose;Lepson/print/rpcopy/ActivityBase$DialogButtons;)V
    .locals 0

    .line 223
    iput-object p1, p0, Lepson/print/rpcopy/ActivityBase$errorDialog$1;->this$1:Lepson/print/rpcopy/ActivityBase$errorDialog;

    iput-object p2, p0, Lepson/print/rpcopy/ActivityBase$errorDialog$1;->val$closeListener:Lepson/print/rpcopy/ActivityBase$IClose;

    iput-object p3, p0, Lepson/print/rpcopy/ActivityBase$errorDialog$1;->val$buttons:Lepson/print/rpcopy/ActivityBase$DialogButtons;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    .line 225
    invoke-interface {p1}, Landroid/content/DialogInterface;->cancel()V

    .line 226
    iget-object p1, p0, Lepson/print/rpcopy/ActivityBase$errorDialog$1;->val$closeListener:Lepson/print/rpcopy/ActivityBase$IClose;

    if-eqz p1, :cond_1

    .line 227
    iget-object p2, p0, Lepson/print/rpcopy/ActivityBase$errorDialog$1;->val$buttons:Lepson/print/rpcopy/ActivityBase$DialogButtons;

    sget-object v0, Lepson/print/rpcopy/ActivityBase$DialogButtons;->Ok:Lepson/print/rpcopy/ActivityBase$DialogButtons;

    if-ne p2, v0, :cond_0

    sget-object p2, Lepson/print/rpcopy/ActivityBase$ClickButton;->Ok:Lepson/print/rpcopy/ActivityBase$ClickButton;

    goto :goto_0

    :cond_0
    sget-object p2, Lepson/print/rpcopy/ActivityBase$ClickButton;->ClearError:Lepson/print/rpcopy/ActivityBase$ClickButton;

    :goto_0
    invoke-interface {p1, p2}, Lepson/print/rpcopy/ActivityBase$IClose;->onClose(Lepson/print/rpcopy/ActivityBase$ClickButton;)V

    :cond_1
    return-void
.end method

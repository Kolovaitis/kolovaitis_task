.class Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask;
.super Landroid/os/AsyncTask;
.source "RemoteCopyGetOptionTask.java"

# interfaces
.implements Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ITask;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;",
        ">;",
        "Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ITask;"
    }
.end annotation


# instance fields
.field clientID:Ljava/lang/String;

.field contextListener:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener;

.field copyType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ECopyType;

.field operation:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;

.field systemSettings:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;


# direct methods
.method public constructor <init>(Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ECopyType;Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener;)V
    .locals 1

    .line 40
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 41
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;

    invoke-direct {v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;-><init>()V

    iput-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask;->operation:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;

    .line 42
    iput-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask;->copyType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ECopyType;

    .line 43
    iput-object p2, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask;->contextListener:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener;

    return-void
.end method


# virtual methods
.method checkDeviceError()Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;
    .locals 5

    .line 82
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;

    invoke-direct {v0, p0}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;-><init>(Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask;)V

    .line 83
    iget-object v1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask;->copyType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ECopyType;

    iput-object v1, v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;->copyType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ECopyType;

    .line 85
    new-instance v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteDevice;

    invoke-direct {v1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteDevice;-><init>()V

    .line 86
    iget-object v2, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask;->operation:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;

    invoke-virtual {v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;->getHostIP()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteDevice;->setHostIP(Ljava/lang/String;)V

    .line 87
    iget-object v2, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask;->operation:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;

    invoke-virtual {v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;->getRequestConnectionTimeout()I

    move-result v2

    invoke-virtual {v1, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteDevice;->setRequestConnectionTimeout(I)V

    .line 90
    new-instance v2, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask$1;

    invoke-direct {v2, p0}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask$1;-><init>(Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask;)V

    invoke-virtual {v1, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteDevice;->getFunctions(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$IRemoteOperationParameter;)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteDevice$ERemoteDeviceFunctionsResult;

    move-result-object v1

    .line 97
    invoke-virtual {v1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteDevice$ERemoteDeviceFunctionsResult;->success()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x0

    .line 99
    invoke-virtual {v1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteDevice$ERemoteDeviceFunctionsResult;->functions()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 100
    sget-object v4, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->repeat_copy:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    if-ne v3, v4, :cond_0

    const/4 v2, 0x1

    :cond_1
    if-nez v2, :cond_2

    .line 107
    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;->ErrorNotCopySupported:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;

    iput-object v1, v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;->error:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;

    return-object v0

    .line 114
    :cond_2
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemotePrinter;

    invoke-direct {v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemotePrinter;-><init>()V

    .line 115
    iget-object v1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask;->operation:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;

    invoke-virtual {v1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;->getHostIP()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemotePrinter;->setHostIP(Ljava/lang/String;)V

    .line 116
    iget-object v1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask;->operation:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;

    invoke-virtual {v1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;->getRequestConnectionTimeout()I

    move-result v1

    invoke-virtual {v0, v1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemotePrinter;->setRequestConnectionTimeout(I)V

    .line 117
    new-instance v1, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask$2;

    invoke-direct {v1, p0}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask$2;-><init>(Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask;)V

    invoke-virtual {v0, v1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemotePrinter;->getComponents(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$IRemoteOperationParameter;)Lepson/print/rpcopy/Component/eremoteoperation/ERemotePrinter$ERemotePrinterComponentsResult;

    move-result-object v0

    .line 123
    invoke-virtual {v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemotePrinter$ERemotePrinterComponentsResult;->success()Z

    move-result v1

    if-nez v1, :cond_3

    .line 124
    invoke-virtual {p0, v0}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask;->getErrorResult(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;)Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;

    move-result-object v0

    return-object v0

    .line 127
    :cond_3
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteScanner;

    invoke-direct {v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteScanner;-><init>()V

    .line 128
    iget-object v1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask;->operation:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;

    invoke-virtual {v1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;->getHostIP()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteScanner;->setHostIP(Ljava/lang/String;)V

    .line 129
    iget-object v1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask;->operation:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;

    invoke-virtual {v1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;->getRequestConnectionTimeout()I

    move-result v1

    invoke-virtual {v0, v1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteScanner;->setRequestConnectionTimeout(I)V

    .line 130
    new-instance v1, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask$3;

    invoke-direct {v1, p0}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask$3;-><init>(Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask;)V

    invoke-virtual {v0, v1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteScanner;->getComponents(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$IRemoteOperationParameter;)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteScanner$ERemoteScannerComponentsResult;

    move-result-object v0

    .line 136
    invoke-virtual {v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteScanner$ERemoteScannerComponentsResult;->success()Z

    move-result v1

    if-nez v1, :cond_4

    .line 137
    invoke-virtual {p0, v0}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask;->getErrorResult(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;)Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;

    move-result-object v0

    return-object v0

    :cond_4
    const/4 v0, 0x0

    return-object v0

    .line 111
    :cond_5
    invoke-virtual {p0, v1}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask;->getErrorResult(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;)Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;
    .locals 5

    .line 146
    iget-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask;->operation:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;

    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask;->systemSettings:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;

    invoke-interface {v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;->getPrinterIPAddress()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;->setHostIP(Ljava/lang/String;)V

    .line 148
    invoke-virtual {p0}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask;->checkDeviceError()Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;

    move-result-object p1

    if-eqz p1, :cond_0

    return-object p1

    .line 153
    :cond_0
    new-instance p1, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;

    invoke-direct {p1, p0}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;-><init>(Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask;)V

    .line 154
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask;->copyType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ECopyType;

    iput-object v0, p1, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;->copyType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ECopyType;

    .line 157
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask;->operation:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;

    invoke-virtual {v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;->getSelectableOptions()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;

    move-result-object v0

    .line 160
    invoke-virtual {v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;->success()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 161
    new-instance v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    iget-object v2, p1, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;->copyType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ECopyType;

    invoke-direct {v1, v2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;-><init>(Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ECopyType;)V

    iput-object v1, p1, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;->optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    .line 162
    iget-object v1, p1, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;->optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    invoke-virtual {v1, v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->setCopyOptionsResult(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;)V

    .line 164
    iget-object v1, p1, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;->optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->color_effects_type:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-static {v2, v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->createOptionItem(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->add(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)V

    .line 165
    iget-object v1, p1, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;->optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->layout:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-static {v2, v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->createOptionItem(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->add(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)V

    .line 167
    iget-object v1, p1, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;->optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_size:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-static {v2, v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->createOptionItem(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->add(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)V

    .line 168
    iget-object v1, p1, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;->optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_type:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-static {v2, v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->createOptionItem(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->add(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)V

    .line 169
    iget-object v1, p1, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;->optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_source:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-static {v2, v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->createOptionItem(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->add(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)V

    .line 170
    iget-object v1, p1, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;->optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_quality:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-static {v2, v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->createOptionItem(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->add(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)V

    .line 173
    iget-object v1, p1, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;->optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_remove_background:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-static {v2, v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->createOptionItem(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->add(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)V

    .line 176
    iget-object v1, p1, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;->optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_cut_line:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-static {v2, v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->createOptionItem(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->add(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)V

    .line 177
    iget-object v1, p1, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;->optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_cut_line_style:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-static {v2, v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->createOptionItem(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->add(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)V

    .line 178
    iget-object v1, p1, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;->optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_cut_line_weight:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-static {v2, v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->createOptionItem(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v0

    invoke-virtual {v1, v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->add(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)V

    .line 181
    iget-object v0, p1, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;->optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->copies:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x63

    const/4 v3, 0x1

    invoke-static {v1, v3, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->createLocalOptionItem(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;III)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->add(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)V

    .line 182
    iget-object v0, p1, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;->optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_density:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/4 v2, -0x4

    const/4 v3, 0x4

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->createLocalOptionItem(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;III)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->add(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)V

    goto :goto_0

    .line 185
    :cond_1
    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->success:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;->isNull(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 186
    sget-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;->ErrorCommunication:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;

    iput-object v0, p1, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;->error:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;

    goto :goto_0

    .line 188
    :cond_2
    sget-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;->Error:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;

    iput-object v0, p1, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;->error:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;

    :goto_0
    return-object p1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 27
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask;->doInBackground([Ljava/lang/Void;)Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;

    move-result-object p1

    return-object p1
.end method

.method getErrorResult(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;)Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;
    .locals 2

    .line 72
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;

    invoke-direct {v0, p0}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;-><init>(Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask;)V

    .line 73
    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->success:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p1, v1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;->isNull(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 74
    sget-object p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;->ErrorCommunication:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;

    iput-object p1, v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;->error:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;

    goto :goto_0

    .line 76
    :cond_0
    sget-object p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;->Error:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;

    iput-object p1, v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;->error:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;

    :goto_0
    return-object v0
.end method

.method protected onPostExecute(Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;)V
    .locals 3

    .line 196
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask;->contextListener:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener;

    iget-object v1, p1, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;->copyType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ECopyType;

    iget-object v2, p1, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;->optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    iget-object p1, p1, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;->error:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;

    invoke-interface {v0, v1, v2, p1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener;->onCopyOptionContextCreated(Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ECopyType;Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 27
    check-cast p1, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;

    invoke-virtual {p0, p1}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask;->onPostExecute(Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;)V

    return-void
.end method

.method public setClientID(Ljava/lang/String;)V
    .locals 0

    .line 68
    iput-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask;->clientID:Ljava/lang/String;

    return-void
.end method

.method public setOptionContext(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;)V
    .locals 0

    return-void
.end method

.method public setRequestConnectionTimeout(I)V
    .locals 1

    .line 63
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask;->operation:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;

    invoke-virtual {v0, p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;->setRequestConnectionTimeout(I)V

    return-void
.end method

.method public setSystemSettings(Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;)V
    .locals 0

    .line 58
    iput-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask;->systemSettings:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;

    return-void
.end method

.method public start()Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyCancelRequest;
    .locals 1

    const/4 v0, 0x0

    .line 48
    new-array v0, v0, [Ljava/lang/Void;

    invoke-super {p0, v0}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    const/4 v0, 0x0

    return-object v0
.end method

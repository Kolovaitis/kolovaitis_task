.class public final enum Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;
.super Ljava/lang/Enum;
.source "ECopyOptionItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ECopyOptionItemKey"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

.field public static final enum ColorEffectsType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

.field public static final enum Copies:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

.field public static final enum InvalidKey:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

.field public static final enum PrintMediaSize:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

.field public static final enum PrintMediaSource:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

.field public static final enum PrintMediaType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

.field public static final enum PrintQuality:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

.field public static final enum RemoveBackground:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

.field public static final enum RepeatLayout:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

.field public static final enum XCutLine:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

.field public static final enum XCutLineStyle:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

.field public static final enum XCutLineWeight:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

.field public static final enum XDensity:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;


# instance fields
.field param:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 273
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    const-string v1, "ColorEffectsType"

    sget-object v2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->color_effects_type:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3, v2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->ColorEffectsType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    .line 281
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    const-string v1, "PrintMediaType"

    sget-object v2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_type:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/4 v4, 0x1

    invoke-direct {v0, v1, v4, v2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    .line 285
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    const-string v1, "PrintMediaSize"

    sget-object v2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_size:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/4 v5, 0x2

    invoke-direct {v0, v1, v5, v2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    .line 289
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    const-string v1, "PrintMediaSource"

    sget-object v2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_source:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/4 v6, 0x3

    invoke-direct {v0, v1, v6, v2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSource:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    .line 293
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    const-string v1, "PrintQuality"

    sget-object v2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_quality:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/4 v7, 0x4

    invoke-direct {v0, v1, v7, v2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintQuality:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    .line 301
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    const-string v1, "XDensity"

    sget-object v2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_density:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/4 v8, 0x5

    invoke-direct {v0, v1, v8, v2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XDensity:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    .line 324
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    const-string v1, "Copies"

    sget-object v2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->copies:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/4 v9, 0x6

    invoke-direct {v0, v1, v9, v2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->Copies:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    .line 334
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    const-string v1, "RepeatLayout"

    sget-object v2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->layout:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/4 v10, 0x7

    invoke-direct {v0, v1, v10, v2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->RepeatLayout:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    .line 339
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    const-string v1, "RemoveBackground"

    sget-object v2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_remove_background:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v11, 0x8

    invoke-direct {v0, v1, v11, v2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->RemoveBackground:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    .line 344
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    const-string v1, "XCutLine"

    sget-object v2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_cut_line:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v12, 0x9

    invoke-direct {v0, v1, v12, v2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XCutLine:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    .line 345
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    const-string v1, "XCutLineStyle"

    sget-object v2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_cut_line_style:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v13, 0xa

    invoke-direct {v0, v1, v13, v2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XCutLineStyle:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    .line 346
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    const-string v1, "XCutLineWeight"

    sget-object v2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_cut_line_weight:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v14, 0xb

    invoke-direct {v0, v1, v14, v2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XCutLineWeight:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    .line 351
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    const-string v1, "InvalidKey"

    sget-object v2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_null:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0xc

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->InvalidKey:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    const/16 v0, 0xd

    .line 269
    new-array v0, v0, [Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->ColorEffectsType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    aput-object v1, v0, v3

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    aput-object v1, v0, v4

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    aput-object v1, v0, v5

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSource:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    aput-object v1, v0, v6

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintQuality:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    aput-object v1, v0, v7

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XDensity:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    aput-object v1, v0, v8

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->Copies:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    aput-object v1, v0, v9

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->RepeatLayout:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    aput-object v1, v0, v10

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->RemoveBackground:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    aput-object v1, v0, v11

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XCutLine:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    aput-object v1, v0, v12

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XCutLineStyle:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    aput-object v1, v0, v13

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XCutLineWeight:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    aput-object v1, v0, v14

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->InvalidKey:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    aput-object v1, v0, v15

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->$VALUES:[Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ")V"
        }
    .end annotation

    .line 354
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 355
    iput-object p3, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->param:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;
    .locals 1

    .line 269
    const-class v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    return-object p0
.end method

.method public static values()[Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;
    .locals 1

    .line 269
    sget-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->$VALUES:[Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v0}, [Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    return-object v0
.end method

.class public final enum Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;
.super Ljava/lang/Enum;
.source "ECopyComponent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CopyOptionChangedError"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;

.field public static final enum Error:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;

.field public static final enum ErrorCommunication:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;

.field public static final enum ErrorInvalidOption:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 437
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;

    const-string v1, "ErrorCommunication"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;->ErrorCommunication:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;

    .line 442
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;

    const-string v1, "ErrorInvalidOption"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;->ErrorInvalidOption:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;

    .line 446
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;

    const-string v1, "Error"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;->Error:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;

    const/4 v0, 0x3

    .line 433
    new-array v0, v0, [Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;->ErrorCommunication:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;->ErrorInvalidOption:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;

    aput-object v1, v0, v3

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;->Error:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;

    aput-object v1, v0, v4

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;->$VALUES:[Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 433
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;
    .locals 1

    .line 433
    const-class v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;

    return-object p0
.end method

.method public static values()[Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;
    .locals 1

    .line 433
    sget-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;->$VALUES:[Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;

    invoke-virtual {v0}, [Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;

    return-object v0
.end method

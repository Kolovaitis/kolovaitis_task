.class public interface abstract Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest;
.super Ljava/lang/Object;
.source "ECopyComponent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ICopyResumeRequest"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;,
        Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$ResumeState;
    }
.end annotation


# virtual methods
.method public abstract getStopReason()Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;
.end method

.method public abstract isPossibleClearError()Z
.end method

.method public abstract resume(Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$ResumeState;)V
.end method

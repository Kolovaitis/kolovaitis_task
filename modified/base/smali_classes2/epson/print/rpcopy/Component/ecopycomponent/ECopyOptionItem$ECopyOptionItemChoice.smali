.class public final enum Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;
.super Ljava/lang/Enum;
.source "ECopyOptionItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ECopyOptionItemChoice"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum ColorEffectsType_Color:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum ColorEffectsType_MonochromeGrayscale:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_11x14in:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_16K:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_254x305mm:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_2L:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_8K:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_8d5x13in:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_8x10in:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_A3:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_A4:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_A5:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_A6:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_B4:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_B5:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_B6:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_CARD:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_CHOU3:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_CHOU4:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_EnvelopeDL:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_EnvelopeNumber10:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_Executive:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_HalfLetter:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_Hivision:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_IndianLegal215x345mm:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_KAKU2:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_KAKU20:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_KG:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_L:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_Legal:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_Letter:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_MEISHI:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_MexicoOficio8d5x13d4in:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_Oficio9_8d46x12d4in:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_Postcard:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_US_B:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_YOU1:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_YOU2:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_YOU3:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_YOU4:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSource_Bottom:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSource_Manual:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSource_Rear:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSource_Top:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaType_BrightColorPlain:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaType_BussnessPlain:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaType_CustomMediaTypeEpson02:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaType_CustomMediaTypeEpson19:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaType_CustomMediaTypeEpson1B:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaType_CustomMediaTypeEpson2A:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaType_CustomMediaTypeEpson44:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaType_DBLMEISHI_HALFGROSSY:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaType_Envelope:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaType_HagakiAtena:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaType_Lebals:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaType_Photographic:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaType_PhotographicGlossy:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaType_PhotographicHighGloss:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaType_PhotographicMatte:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaType_PhotographicSemiGloss:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaType_Stationery:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaType_StationeryCoated:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaType_StationeryHeavyweight:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaType_StationeryInkjet:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaType_StationeryLetterhead:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintQuality_Best:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintQuality_Economy:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintQuality_High:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintQuality_Normal:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum RepeatLayout_autoRepeat:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum RepeatLayout_fourRepeat:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum RepeatLayout_twoRepeat:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum XCutLineStyle_Continuous:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum XCutLineStyle_Dash:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum XCutLineStyle_Dot:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum XCutLineWidth_Dash:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum XCutLineWidth_Medium:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum XCutLineWidth_Thick:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum XCutLineWidth_Thin:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum XCutLine_Dash:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum XCutLine_Off:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum XCutLine_On:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum XRemoveBackground_Off:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum XRemoveBackground_On:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;


# instance fields
.field key:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

.field param:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 382
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "ColorEffectsType_Color"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->ColorEffectsType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->color:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/4 v4, 0x0

    invoke-direct {v0, v1, v4, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->ColorEffectsType_Color:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 386
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "ColorEffectsType_MonochromeGrayscale"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->ColorEffectsType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->monochrome_grayscale:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/4 v5, 0x1

    invoke-direct {v0, v1, v5, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->ColorEffectsType_MonochromeGrayscale:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 404
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSource_Top"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSource:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->top:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/4 v6, 0x2

    invoke-direct {v0, v1, v6, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSource_Top:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 408
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSource_Bottom"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSource:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->bottom:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/4 v7, 0x3

    invoke-direct {v0, v1, v7, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSource_Bottom:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 412
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSource_Rear"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSource:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->rear:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/4 v8, 0x4

    invoke-direct {v0, v1, v8, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSource_Rear:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 416
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSource_Manual"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSource:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->manual:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/4 v9, 0x5

    invoke-direct {v0, v1, v9, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSource_Manual:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 421
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaType_Stationery"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->stationery:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/4 v10, 0x6

    invoke-direct {v0, v1, v10, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_Stationery:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 425
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaType_PhotographicHighGloss"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->photographic_high_gloss:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/4 v11, 0x7

    invoke-direct {v0, v1, v11, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_PhotographicHighGloss:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 429
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaType_Photographic"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->photographic:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v12, 0x8

    invoke-direct {v0, v1, v12, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_Photographic:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 433
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaType_PhotographicSemiGloss"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->photographic_semi_gloss:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v13, 0x9

    invoke-direct {v0, v1, v13, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_PhotographicSemiGloss:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 437
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaType_PhotographicGlossy"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->photographic_glossy:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v14, 0xa

    invoke-direct {v0, v1, v14, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_PhotographicGlossy:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 441
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaType_CustomMediaTypeEpson44"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_media_type_epson_44:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0xb

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_CustomMediaTypeEpson44:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 445
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaType_PhotographicMatte"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->photographic_matte:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0xc

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_PhotographicMatte:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 449
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaType_StationeryCoated"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->stationery_coated:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0xd

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_StationeryCoated:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 453
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaType_CustomMediaTypeEpson2A"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_media_type_epson_2a:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0xe

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_CustomMediaTypeEpson2A:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 457
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaType_StationeryInkjet"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->stationery_inkjet:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0xf

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_StationeryInkjet:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 461
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaType_CustomMediaTypeEpson1B"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_media_type_epson_1b:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x10

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_CustomMediaTypeEpson1B:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 465
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaType_CustomMediaTypeEpson02"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_media_type_epson_02:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x11

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_CustomMediaTypeEpson02:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 469
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaType_CustomMediaTypeEpson19"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_media_type_epson_19:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x12

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_CustomMediaTypeEpson19:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 473
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaType_Lebals"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->labels:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x13

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_Lebals:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 477
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaType_Envelope"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->envelope:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x14

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_Envelope:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 481
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaType_DBLMEISHI_HALFGROSSY"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_media_type_epson_47:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x15

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_DBLMEISHI_HALFGROSSY:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 485
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaType_HagakiAtena"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_media_type_epson_20:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x16

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_HagakiAtena:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 490
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaType_BussnessPlain"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_media_type_epson_39:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x17

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_BussnessPlain:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 496
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaType_StationeryHeavyweight"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->stationery_heavyweight:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x18

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_StationeryHeavyweight:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 501
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaType_StationeryLetterhead"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->stationery_letterhead:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x19

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_StationeryLetterhead:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 506
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaType_BrightColorPlain"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_media_type_epson_46:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x1a

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_BrightColorPlain:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 512
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_A4"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->iso_a4_210x297mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x1b

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_A4:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 516
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_B4"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->jis_b4_257x364mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x1c

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_B4:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 520
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_B5"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->jis_b5_182x257mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x1d

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_B5:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 524
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_L"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->oe_photo_l_3_5x5in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x1e

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_L:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 528
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_2L"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_5x7_5x7in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x1f

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_2L:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 532
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_Postcard"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->jpn_hagaki_100x148mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x20

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_Postcard:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 536
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_KG"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_index_4x6_4x6in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x21

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_KG:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 540
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_8x10in"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_govt_letter_8x10in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x22

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_8x10in:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 544
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_Letter"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_letter_8_5x11in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x23

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_Letter:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 548
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_Legal"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_legal_8_5x14in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x24

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_Legal:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 552
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_A5"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->iso_a5_148x210mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x25

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_A5:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 556
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_254x305mm"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_4psize_254x305mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x26

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_254x305mm:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 560
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_A3"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->iso_a3_297x420mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x27

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_A3:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 564
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_US_B"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_ledger_11x17in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x28

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_US_B:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 568
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_A6"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->iso_a6_105x148mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x29

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_A6:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 572
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_CHOU3"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->jpn_chou3_120x235mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x2a

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_CHOU3:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 576
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_CHOU4"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->jpn_chou4_90x205mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x2b

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_CHOU4:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 580
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_YOU1"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_epson_18_120x176mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x2c

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_YOU1:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 584
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_YOU3"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_epson_1A_98x148mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x2d

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_YOU3:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 588
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_YOU4"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->jpn_you4_105x235mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x2e

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_YOU4:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 592
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_YOU2"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->iso_c6_114x162mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x2f

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_YOU2:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 596
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_KAKU2"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->jpn_kaku2_240x332mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x30

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_KAKU2:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 600
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_KAKU20"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->iso_c4_229x324mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x31

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_KAKU20:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 604
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_MEISHI"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_cardsize_55x91mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x32

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_MEISHI:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 608
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_CARD"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_creditcardsize_54x86mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x33

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_CARD:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 612
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_Hivision"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_hivision_101_6x180_6mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x34

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_Hivision:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 617
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_EnvelopeDL"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->iso_dl_110x220mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x35

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_EnvelopeDL:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 622
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_B6"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->jis_b6_128x182mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x36

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_B6:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 627
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_Executive"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_executive_7_25x10_5in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x37

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_Executive:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 632
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_8d5x13in"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_foolscap_8_5x13in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x38

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_8d5x13in:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 637
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_11x14in"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_edp_11x14in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x39

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_11x14in:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 642
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_EnvelopeNumber10"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_number_10_4_125x9_5in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x3a

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_EnvelopeNumber10:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 647
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_8K"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->om_8k_270x390mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x3b

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_8K:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 652
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_16K"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->om_16k_195x270mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x3c

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_16K:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 657
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_HalfLetter"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_invoice_5_5x8_5in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x3d

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_HalfLetter:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 663
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_IndianLegal215x345mm"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_epson_indianlegal_215x345mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x3e

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_IndianLegal215x345mm:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 669
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_MexicoOficio8d5x13d4in"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_oficio_8_5x13_4in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x3f

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_MexicoOficio8d5x13d4in:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 675
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_Oficio9_8d46x12d4in"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->om_folio_sp_215x315mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x40

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_Oficio9_8d46x12d4in:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 696
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintQuality_Economy"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintQuality:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->draft:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x41

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintQuality_Economy:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 700
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintQuality_Normal"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintQuality:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->normal:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x42

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintQuality_Normal:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 704
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintQuality_High"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintQuality:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->high:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x43

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintQuality_High:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 708
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintQuality_Best"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintQuality:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->best:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x44

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintQuality_Best:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 855
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "RepeatLayout_twoRepeat"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->RepeatLayout:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->two_repeat:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x45

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->RepeatLayout_twoRepeat:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 856
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "RepeatLayout_fourRepeat"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->RepeatLayout:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->four_repeat:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x46

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->RepeatLayout_fourRepeat:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 857
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "RepeatLayout_autoRepeat"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->RepeatLayout:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->auto_repeat:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x47

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->RepeatLayout_autoRepeat:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 859
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "XRemoveBackground_On"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->RemoveBackground:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->on:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x48

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XRemoveBackground_On:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 860
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "XRemoveBackground_Off"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->RemoveBackground:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->off:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x49

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XRemoveBackground_Off:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 864
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "XCutLine_Dash"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XCutLine:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->dash:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x4a

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XCutLine_Dash:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 865
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "XCutLine_On"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XCutLine:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->on:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x4b

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XCutLine_On:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 866
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "XCutLine_Off"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XCutLine:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->off:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x4c

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XCutLine_Off:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 868
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "XCutLineStyle_Dash"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XCutLineStyle:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->dash:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x4d

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XCutLineStyle_Dash:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 869
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "XCutLineStyle_Dot"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XCutLineStyle:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->dot:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x4e

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XCutLineStyle_Dot:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 870
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "XCutLineStyle_Continuous"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XCutLineStyle:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->continuous:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x4f

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XCutLineStyle_Continuous:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 872
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "XCutLineWidth_Dash"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XCutLineWeight:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->dash:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x50

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XCutLineWidth_Dash:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 873
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "XCutLineWidth_Thin"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XCutLineWeight:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->thin:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x51

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XCutLineWidth_Thin:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 874
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "XCutLineWidth_Medium"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XCutLineWeight:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->medium:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x52

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XCutLineWidth_Medium:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 875
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "XCutLineWidth_Thick"

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XCutLineWeight:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->thick:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x53

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XCutLineWidth_Thick:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v0, 0x54

    .line 378
    new-array v0, v0, [Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->ColorEffectsType_Color:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    aput-object v1, v0, v4

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->ColorEffectsType_MonochromeGrayscale:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    aput-object v1, v0, v5

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSource_Top:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    aput-object v1, v0, v6

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSource_Bottom:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    aput-object v1, v0, v7

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSource_Rear:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    aput-object v1, v0, v8

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSource_Manual:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    aput-object v1, v0, v9

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_Stationery:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    aput-object v1, v0, v10

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_PhotographicHighGloss:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    aput-object v1, v0, v11

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_Photographic:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    aput-object v1, v0, v12

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_PhotographicSemiGloss:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    aput-object v1, v0, v13

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_PhotographicGlossy:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    aput-object v1, v0, v14

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_CustomMediaTypeEpson44:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_PhotographicMatte:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_StationeryCoated:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_CustomMediaTypeEpson2A:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_StationeryInkjet:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_CustomMediaTypeEpson1B:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_CustomMediaTypeEpson02:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_CustomMediaTypeEpson19:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_Lebals:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_Envelope:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_DBLMEISHI_HALFGROSSY:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_HagakiAtena:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_BussnessPlain:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_StationeryHeavyweight:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_StationeryLetterhead:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_BrightColorPlain:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_A4:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_B4:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_B5:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_L:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_2L:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_Postcard:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_KG:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_8x10in:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_Letter:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x23

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_Legal:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x24

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_A5:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x25

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_254x305mm:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x26

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_A3:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x27

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_US_B:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x28

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_A6:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x29

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_CHOU3:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x2a

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_CHOU4:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x2b

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_YOU1:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x2c

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_YOU3:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x2d

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_YOU4:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x2e

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_YOU2:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x2f

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_KAKU2:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x30

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_KAKU20:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x31

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_MEISHI:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x32

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_CARD:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x33

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_Hivision:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x34

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_EnvelopeDL:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x35

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_B6:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x36

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_Executive:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x37

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_8d5x13in:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x38

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_11x14in:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x39

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_EnvelopeNumber10:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x3a

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_8K:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x3b

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_16K:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x3c

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_HalfLetter:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x3d

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_IndianLegal215x345mm:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x3e

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_MexicoOficio8d5x13d4in:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x3f

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_Oficio9_8d46x12d4in:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x40

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintQuality_Economy:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x41

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintQuality_Normal:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x42

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintQuality_High:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x43

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintQuality_Best:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x44

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->RepeatLayout_twoRepeat:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x45

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->RepeatLayout_fourRepeat:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x46

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->RepeatLayout_autoRepeat:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x47

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XRemoveBackground_On:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x48

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XRemoveBackground_Off:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x49

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XCutLine_Dash:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x4a

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XCutLine_On:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x4b

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XCutLine_Off:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x4c

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XCutLineStyle_Dash:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x4d

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XCutLineStyle_Dot:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x4e

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XCutLineStyle_Continuous:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x4f

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XCutLineWidth_Dash:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x50

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XCutLineWidth_Thin:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x51

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XCutLineWidth_Medium:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x52

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XCutLineWidth_Thick:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x53

    aput-object v1, v0, v2

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->$VALUES:[Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;",
            "Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ")V"
        }
    .end annotation

    .line 880
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 881
    iput-object p4, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->param:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    return-void
.end method

.method public static valueOf(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;
    .locals 1

    .line 892
    sget-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$2;->$SwitchMap$epson$print$rpcopy$Component$ecopycomponent$ECopyOptionItem$ECopyOptionItemKey:[I

    invoke-virtual {p0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->ordinal()I

    move-result p0

    aget p0, v0, p0

    packed-switch p0, :pswitch_data_0

    goto/16 :goto_9

    .line 894
    :pswitch_0
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$2;->$SwitchMap$epson$print$rpcopy$Component$eremoteoperation$ERemoteOperation$ERemoteParam:[I

    invoke-virtual {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->ordinal()I

    move-result v0

    aget p0, p0, v0

    packed-switch p0, :pswitch_data_1

    goto :goto_0

    .line 896
    :pswitch_1
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->ColorEffectsType_MonochromeGrayscale:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 895
    :pswitch_2
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->ColorEffectsType_Color:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 901
    :goto_0
    :pswitch_3
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$2;->$SwitchMap$epson$print$rpcopy$Component$eremoteoperation$ERemoteOperation$ERemoteParam:[I

    invoke-virtual {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->ordinal()I

    move-result v0

    aget p0, p0, v0

    packed-switch p0, :pswitch_data_2

    goto :goto_1

    .line 922
    :pswitch_4
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_BrightColorPlain:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 921
    :pswitch_5
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_StationeryLetterhead:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 920
    :pswitch_6
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_StationeryHeavyweight:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 919
    :pswitch_7
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_BussnessPlain:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 918
    :pswitch_8
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_HagakiAtena:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 917
    :pswitch_9
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_DBLMEISHI_HALFGROSSY:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 916
    :pswitch_a
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_Envelope:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 915
    :pswitch_b
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_Lebals:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 914
    :pswitch_c
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_CustomMediaTypeEpson19:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 913
    :pswitch_d
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_CustomMediaTypeEpson02:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 912
    :pswitch_e
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_CustomMediaTypeEpson1B:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 911
    :pswitch_f
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_StationeryInkjet:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 910
    :pswitch_10
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_CustomMediaTypeEpson2A:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 909
    :pswitch_11
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_StationeryCoated:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 908
    :pswitch_12
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_PhotographicMatte:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 907
    :pswitch_13
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_CustomMediaTypeEpson44:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 906
    :pswitch_14
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_PhotographicGlossy:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 905
    :pswitch_15
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_PhotographicSemiGloss:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 904
    :pswitch_16
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_Photographic:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 903
    :pswitch_17
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_PhotographicHighGloss:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 902
    :pswitch_18
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_Stationery:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 926
    :goto_1
    :pswitch_19
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$2;->$SwitchMap$epson$print$rpcopy$Component$eremoteoperation$ERemoteOperation$ERemoteParam:[I

    invoke-virtual {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->ordinal()I

    move-result v0

    aget p0, p0, v0

    packed-switch p0, :pswitch_data_3

    goto :goto_2

    .line 964
    :pswitch_1a
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_Oficio9_8d46x12d4in:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 963
    :pswitch_1b
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_MexicoOficio8d5x13d4in:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 962
    :pswitch_1c
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_IndianLegal215x345mm:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 961
    :pswitch_1d
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_HalfLetter:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 960
    :pswitch_1e
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_16K:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 959
    :pswitch_1f
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_8K:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 958
    :pswitch_20
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_EnvelopeNumber10:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 957
    :pswitch_21
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_11x14in:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 956
    :pswitch_22
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_8d5x13in:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 955
    :pswitch_23
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_Executive:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 954
    :pswitch_24
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_B6:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 953
    :pswitch_25
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_EnvelopeDL:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 952
    :pswitch_26
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_Hivision:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 951
    :pswitch_27
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_CARD:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 950
    :pswitch_28
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_MEISHI:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 949
    :pswitch_29
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_KAKU20:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 948
    :pswitch_2a
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_KAKU2:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 947
    :pswitch_2b
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_YOU2:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 946
    :pswitch_2c
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_YOU4:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 945
    :pswitch_2d
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_YOU3:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 944
    :pswitch_2e
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_YOU1:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 943
    :pswitch_2f
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_CHOU4:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 942
    :pswitch_30
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_CHOU3:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 941
    :pswitch_31
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_A6:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 940
    :pswitch_32
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_US_B:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 939
    :pswitch_33
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_A3:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 938
    :pswitch_34
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_254x305mm:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 937
    :pswitch_35
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_A5:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 936
    :pswitch_36
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_Legal:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 935
    :pswitch_37
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_Letter:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 934
    :pswitch_38
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_8x10in:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 933
    :pswitch_39
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_KG:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 932
    :pswitch_3a
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_Postcard:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 931
    :pswitch_3b
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_2L:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 930
    :pswitch_3c
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_L:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 929
    :pswitch_3d
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_B5:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 928
    :pswitch_3e
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_B4:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 927
    :pswitch_3f
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_A4:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 968
    :goto_2
    :pswitch_40
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$2;->$SwitchMap$epson$print$rpcopy$Component$eremoteoperation$ERemoteOperation$ERemoteParam:[I

    invoke-virtual {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->ordinal()I

    move-result v0

    aget p0, p0, v0

    packed-switch p0, :pswitch_data_4

    goto :goto_3

    .line 972
    :pswitch_41
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSource_Manual:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 971
    :pswitch_42
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSource_Rear:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 970
    :pswitch_43
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSource_Bottom:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 969
    :pswitch_44
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSource_Top:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 976
    :goto_3
    :pswitch_45
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$2;->$SwitchMap$epson$print$rpcopy$Component$eremoteoperation$ERemoteOperation$ERemoteParam:[I

    invoke-virtual {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->ordinal()I

    move-result v0

    aget p0, p0, v0

    packed-switch p0, :pswitch_data_5

    goto :goto_4

    .line 980
    :pswitch_46
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintQuality_Best:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 979
    :pswitch_47
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintQuality_High:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 978
    :pswitch_48
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintQuality_Normal:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 977
    :pswitch_49
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintQuality_Economy:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 985
    :goto_4
    :pswitch_4a
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$2;->$SwitchMap$epson$print$rpcopy$Component$eremoteoperation$ERemoteOperation$ERemoteParam:[I

    invoke-virtual {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->ordinal()I

    move-result v0

    aget p0, p0, v0

    packed-switch p0, :pswitch_data_6

    goto :goto_5

    .line 988
    :pswitch_4b
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->RepeatLayout_autoRepeat:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 987
    :pswitch_4c
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->RepeatLayout_fourRepeat:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 986
    :pswitch_4d
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->RepeatLayout_twoRepeat:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 996
    :goto_5
    :pswitch_4e
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$2;->$SwitchMap$epson$print$rpcopy$Component$eremoteoperation$ERemoteOperation$ERemoteParam:[I

    invoke-virtual {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->ordinal()I

    move-result v0

    aget p0, p0, v0

    packed-switch p0, :pswitch_data_7

    goto :goto_6

    .line 998
    :pswitch_4f
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XRemoveBackground_Off:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 997
    :pswitch_50
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XRemoveBackground_On:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 1006
    :goto_6
    :pswitch_51
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$2;->$SwitchMap$epson$print$rpcopy$Component$eremoteoperation$ERemoteOperation$ERemoteParam:[I

    invoke-virtual {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->ordinal()I

    move-result v0

    aget p0, p0, v0

    packed-switch p0, :pswitch_data_8

    goto :goto_7

    .line 1008
    :pswitch_52
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XCutLine_Dash:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 1010
    :pswitch_53
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XCutLine_Off:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 1009
    :pswitch_54
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XCutLine_On:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 1018
    :goto_7
    :pswitch_55
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$2;->$SwitchMap$epson$print$rpcopy$Component$eremoteoperation$ERemoteOperation$ERemoteParam:[I

    invoke-virtual {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->ordinal()I

    move-result v0

    aget p0, p0, v0

    packed-switch p0, :pswitch_data_9

    goto :goto_8

    .line 1022
    :pswitch_56
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XCutLineStyle_Continuous:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 1021
    :pswitch_57
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XCutLineStyle_Dot:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 1020
    :pswitch_58
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XCutLineStyle_Dash:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 1030
    :goto_8
    :pswitch_59
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$2;->$SwitchMap$epson$print$rpcopy$Component$eremoteoperation$ERemoteOperation$ERemoteParam:[I

    invoke-virtual {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->ordinal()I

    move-result p1

    aget p0, p0, p1

    packed-switch p0, :pswitch_data_a

    :pswitch_5a
    goto :goto_9

    .line 1035
    :pswitch_5b
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XCutLineWidth_Thick:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 1034
    :pswitch_5c
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XCutLineWidth_Medium:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 1033
    :pswitch_5d
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XCutLineWidth_Thin:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 1032
    :pswitch_5e
    sget-object p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XCutLineWidth_Dash:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    :goto_9
    const/4 p0, 0x0

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_19
        :pswitch_40
        :pswitch_45
        :pswitch_4a
        :pswitch_4e
        :pswitch_51
        :pswitch_55
        :pswitch_59
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x3
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x18
        :pswitch_3f
        :pswitch_3e
        :pswitch_3d
        :pswitch_3c
        :pswitch_3b
        :pswitch_3a
        :pswitch_39
        :pswitch_38
        :pswitch_37
        :pswitch_36
        :pswitch_35
        :pswitch_34
        :pswitch_33
        :pswitch_32
        :pswitch_31
        :pswitch_30
        :pswitch_2f
        :pswitch_2e
        :pswitch_2d
        :pswitch_2c
        :pswitch_2b
        :pswitch_2a
        :pswitch_29
        :pswitch_28
        :pswitch_27
        :pswitch_26
        :pswitch_25
        :pswitch_24
        :pswitch_23
        :pswitch_22
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x3e
        :pswitch_44
        :pswitch_43
        :pswitch_42
        :pswitch_41
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x42
        :pswitch_49
        :pswitch_48
        :pswitch_47
        :pswitch_46
    .end packed-switch

    :pswitch_data_6
    .packed-switch 0x46
        :pswitch_4d
        :pswitch_4c
        :pswitch_4b
    .end packed-switch

    :pswitch_data_7
    .packed-switch 0x49
        :pswitch_50
        :pswitch_4f
    .end packed-switch

    :pswitch_data_8
    .packed-switch 0x49
        :pswitch_54
        :pswitch_53
        :pswitch_52
        :pswitch_52
    .end packed-switch

    :pswitch_data_9
    .packed-switch 0x4b
        :pswitch_58
        :pswitch_58
        :pswitch_57
        :pswitch_56
    .end packed-switch

    :pswitch_data_a
    .packed-switch 0x4b
        :pswitch_5e
        :pswitch_5e
        :pswitch_5a
        :pswitch_5a
        :pswitch_5d
        :pswitch_5c
        :pswitch_5b
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;
    .locals 1

    .line 378
    const-class v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0
.end method

.method public static values()[Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;
    .locals 1

    .line 378
    sget-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->$VALUES:[Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    invoke-virtual {v0}, [Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object v0
.end method


# virtual methods
.method public getParam()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 1

    .line 885
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->param:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    return-object v0
.end method

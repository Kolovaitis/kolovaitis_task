.class Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;
.super Ljava/lang/Object;
.source "ECopyOptionItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "NumberRange"
.end annotation


# instance fields
.field defaultValue:I

.field exceptionValue:I

.field max:I

.field min:I

.field selectedValue:I

.field final synthetic this$0:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;


# direct methods
.method public constructor <init>(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)V
    .locals 0

    .line 82
    iput-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->this$0:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;)V
    .locals 0

    .line 85
    iput-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->this$0:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    iget p1, p2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->min:I

    iput p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->min:I

    .line 87
    iget p1, p2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->max:I

    iput p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->max:I

    .line 88
    iget p1, p2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->selectedValue:I

    iput p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->selectedValue:I

    .line 89
    iget p1, p2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->defaultValue:I

    iput p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->defaultValue:I

    .line 90
    iget p1, p2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->exceptionValue:I

    iput p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->exceptionValue:I

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    .line 99
    :cond_0
    instance-of v1, p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 103
    :cond_1
    check-cast p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    .line 105
    iget v1, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->min:I

    iget v3, p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->min:I

    if-ne v1, v3, :cond_2

    iget v1, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->max:I

    iget v3, p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->max:I

    if-ne v1, v3, :cond_2

    iget v1, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->selectedValue:I

    iget v3, p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->selectedValue:I

    if-ne v1, v3, :cond_2

    iget v1, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->defaultValue:I

    iget v3, p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->defaultValue:I

    if-ne v1, v3, :cond_2

    iget v1, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->exceptionValue:I

    iget p1, p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->exceptionValue:I

    if-ne v1, p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

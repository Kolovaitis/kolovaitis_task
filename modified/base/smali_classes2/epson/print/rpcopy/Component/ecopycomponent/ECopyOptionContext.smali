.class public Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;
.super Ljava/lang/Object;
.source "ECopyOptionContext.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field copyOptionsResult:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;

.field copyType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ECopyType;

.field optionItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 26
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext$1;

    invoke-direct {v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext$1;-><init>()V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->optionItems:Ljava/util/ArrayList;

    const/4 v0, 0x0

    .line 71
    iput-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->copyType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ECopyType;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->optionItems:Ljava/util/ArrayList;

    .line 47
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ECopyType;->valueOf(Ljava/lang/String;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ECopyType;

    move-result-object v0

    iput-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->copyType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ECopyType;

    .line 48
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    .line 49
    const-class v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    .line 50
    iget-object v3, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->optionItems:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 54
    :cond_0
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 55
    new-instance p1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;

    new-instance v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;

    invoke-direct {v1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;-><init>()V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {p1, v1, v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;-><init>(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;Lorg/json/JSONObject;)V

    iput-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->copyOptionsResult:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    .line 57
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    :goto_1
    return-void
.end method

.method constructor <init>(Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ECopyType;)V
    .locals 1

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->optionItems:Ljava/util/ArrayList;

    .line 75
    iput-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->copyType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ECopyType;

    return-void
.end method

.method constructor <init>(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;)V
    .locals 4

    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->optionItems:Ljava/util/ArrayList;

    const/4 v0, 0x0

    .line 79
    iput-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->copyType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ECopyType;

    if-eqz p1, :cond_1

    .line 81
    iget-object v0, p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->copyType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ECopyType;

    iput-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->copyType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ECopyType;

    .line 82
    iget-object v0, p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->optionItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    .line 83
    iget-object v2, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->optionItems:Ljava/util/ArrayList;

    new-instance v3, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    invoke-direct {v3, v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;-><init>(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 85
    :cond_0
    iget-object p1, p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->copyOptionsResult:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;

    iput-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->copyOptionsResult:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;

    :cond_1
    return-void
.end method


# virtual methods
.method add(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)V
    .locals 1

    .line 109
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->optionItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method getCopyOptionItemOf(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;
    .locals 3

    .line 99
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->optionItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    .line 100
    iget-object v2, v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->key:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    if-ne v2, p1, :cond_0

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    return-object v1
.end method

.method getCopyOptionItems()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;",
            ">;"
        }
    .end annotation

    .line 94
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->optionItems:Ljava/util/ArrayList;

    return-object v0
.end method

.method getCopyOptionsResult()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;
    .locals 1

    .line 175
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->copyOptionsResult:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;

    return-object v0
.end method

.method getCopyType()Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ECopyType;
    .locals 1

    .line 90
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->copyType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ECopyType;

    return-object v0
.end method

.method public hasCopyOptionItemOf(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Z
    .locals 2

    .line 138
    invoke-virtual {p0, p1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionItemOf(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object p1

    const/4 v0, 0x0

    if-eqz p1, :cond_2

    .line 140
    invoke-virtual {p1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object p1

    iget-object p1, p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->param:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 143
    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_null:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v1, p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    return v0

    .line 146
    :cond_0
    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->dash:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v1, p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    return v0

    :cond_1
    const/4 p1, 0x1

    return p1

    :cond_2
    return v0
.end method

.method isChanged(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)Z
    .locals 4

    .line 159
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->optionItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    .line 160
    iget-object v2, v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->key:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    iget-object v3, p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->key:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    if-ne v2, v3, :cond_0

    .line 161
    invoke-virtual {v1, p1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method isNull()Z
    .locals 1

    .line 67
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->copyType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ECopyType;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public replace(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;
    .locals 4

    .line 120
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->optionItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    .line 121
    iget-object v2, v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->key:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    iget-object v3, p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->key:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    if-ne v2, v3, :cond_0

    .line 122
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->optionItems:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    goto :goto_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_1
    return-object p1
.end method

.method setCopyOptionsResult(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;)V
    .locals 0

    .line 171
    iput-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->copyOptionsResult:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .line 38
    iget-object p2, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->copyType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ECopyType;

    invoke-virtual {p2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ECopyType;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 39
    iget-object p2, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->optionItems:Ljava/util/ArrayList;

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 40
    iget-object p2, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->optionItems:Ljava/util/ArrayList;

    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    const/4 v1, 0x0

    .line 41
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    goto :goto_0

    .line 43
    :cond_0
    iget-object p2, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->copyOptionsResult:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;

    invoke-virtual {p2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method

.class Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$5;
.super Ljava/lang/Object;
.source "RemoteCopyTask.java"

# interfaces
.implements Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyDocumentChangedParameter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->resumeExecute()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;


# direct methods
.method constructor <init>(Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;)V
    .locals 0

    .line 351
    iput-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$5;->this$0:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public client_id()Ljava/lang/String;
    .locals 1

    .line 354
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$5;->this$0:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;

    iget-object v0, v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->clientID:Ljava/lang/String;

    return-object v0
.end method

.method public job_token()Ljava/lang/String;
    .locals 1

    .line 359
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$5;->this$0:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;

    iget-object v0, v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->jobToken:Ljava/lang/String;

    return-object v0
.end method

.method public next_document()Z
    .locals 2

    .line 364
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$5;->this$0:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;

    iget-object v0, v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;

    iget-object v0, v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;->resumeState:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$ResumeState;

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$ResumeState;->NextPageReady:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$ResumeState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

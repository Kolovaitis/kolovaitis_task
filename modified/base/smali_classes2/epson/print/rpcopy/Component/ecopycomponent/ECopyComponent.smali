.class public final Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;
.super Ljava/lang/Object;
.source "ECopyComponent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ITask;,
        Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener;,
        Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;,
        Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;,
        Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyInvalidateResumeRequest;,
        Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest;,
        Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyCancelRequest;,
        Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ECopyType;,
        Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ECopyComponentHolder;,
        Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;,
        Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$Property;
    }
.end annotation


# instance fields
.field clientID:Ljava/lang/String;

.field optionChangedListener:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;

.field optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

.field properties:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$Property;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field systemSettings:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;


# direct methods
.method constructor <init>()V
    .locals 3

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    invoke-direct {v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;-><init>()V

    iput-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;->optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    .line 28
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;->properties:Ljava/util/HashMap;

    .line 29
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;->properties:Ljava/util/HashMap;

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$Property;->RequestConnectionTimeout:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$Property;

    const/16 v2, 0x2710

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$1;

    invoke-direct {v0, p0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$1;-><init>(Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;)V

    iput-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;->systemSettings:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;

    return-void
.end method

.method public static createCopyOptionContext(Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ECopyType;Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener;)V
    .locals 1

    .line 538
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask;

    invoke-direct {v0, p1, p2}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyGetOptionTask;-><init>(Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ECopyType;Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener;)V

    .line 540
    invoke-virtual {p0, v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;->execute(Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ITask;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyCancelRequest;

    return-void
.end method

.method public static sharedComponent()Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;
    .locals 1

    .line 111
    sget-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ECopyComponentHolder;->instance:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;

    return-object v0
.end method


# virtual methods
.method public bindCopyOptionContext(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;)V
    .locals 1

    .line 503
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    invoke-direct {v0, p1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;-><init>(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;)V

    iput-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;->optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    .line 504
    iput-object p2, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;->optionChangedListener:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;

    return-void
.end method

.method protected execute(Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ITask;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyCancelRequest;
    .locals 2

    .line 592
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;->properties:Ljava/util/HashMap;

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$Property;->RequestConnectionTimeout:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$Property;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 593
    iget-object v1, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;->systemSettings:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;

    invoke-interface {p1, v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ITask;->setSystemSettings(Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;)V

    .line 594
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {p1, v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ITask;->setRequestConnectionTimeout(I)V

    .line 595
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;->clientID:Ljava/lang/String;

    invoke-interface {p1, v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ITask;->setClientID(Ljava/lang/String;)V

    .line 596
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;->optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    invoke-interface {p1, v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ITask;->setOptionContext(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;)V

    .line 597
    invoke-interface {p1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ITask;->start()Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyCancelRequest;

    move-result-object p1

    return-object p1
.end method

.method public getBindedCopyOptionContext()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;
    .locals 1

    .line 512
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;->optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    invoke-virtual {v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->isNull()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;->optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    :goto_0
    return-object v0
.end method

.method public getCopyOptionItems()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;",
            ">;"
        }
    .end annotation

    .line 520
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 521
    iget-object v1, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;->optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    iget-object v1, v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->optionItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    .line 522
    new-instance v3, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    invoke-direct {v3, v2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;-><init>(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)V

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public getRemoteOperationUUID(Landroid/content/Context;)V
    .locals 0

    .line 119
    invoke-static {p1}, Lepson/common/Utils;->getRemoteOperationUUID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;->clientID:Ljava/lang/String;

    return-void
.end method

.method public recoverCopy(Ljava/lang/String;Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyCancelRequest;
    .locals 3

    .line 580
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$CopyMode;->Recover:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$CopyMode;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, p2, v2}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;-><init>(Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$CopyMode;Ljava/lang/String;Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;->execute(Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ITask;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyCancelRequest;

    move-result-object p1

    return-object p1
.end method

.method public setCopyOptionItem(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)V
    .locals 2

    .line 558
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask;

    iget-object v1, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;->optionChangedListener:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;

    invoke-direct {v0, p1, v1}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask;-><init>(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;)V

    .line 560
    invoke-virtual {p0, v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;->execute(Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ITask;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyCancelRequest;

    return-void
.end method

.method public setProperty(Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$Property;I)V
    .locals 2

    .line 60
    sget-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$2;->$SwitchMap$epson$print$rpcopy$Component$ecopycomponent$ECopyComponent$Property:[I

    invoke-virtual {p1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$Property;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 62
    :cond_0
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;->properties:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-void
.end method

.method public setSystemSettings(Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;)V
    .locals 0

    .line 99
    iput-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;->systemSettings:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;

    return-void
.end method

.method public startCopy(Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;Landroid/content/Context;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyCancelRequest;
    .locals 2

    .line 570
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$CopyMode;->Copy:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$CopyMode;

    invoke-direct {v0, v1, p1, p2}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;-><init>(Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$CopyMode;Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent;->execute(Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ITask;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyCancelRequest;

    move-result-object p1

    return-object p1
.end method

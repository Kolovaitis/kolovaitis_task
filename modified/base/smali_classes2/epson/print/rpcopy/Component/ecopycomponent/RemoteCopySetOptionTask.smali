.class Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask;
.super Landroid/os/AsyncTask;
.source "RemoteCopySetOptionTask.java"

# interfaces
.implements Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ITask;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;",
        ">;",
        "Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ITask;"
    }
.end annotation


# static fields
.field public static XScale_2L_to_A4_Value:I = 0xa5

.field public static XScale_2L_to_A4_ValueEx:I = 0xad

.field public static XScale_2L_to_KG_Value:I = 0x4d

.field public static XScale_2L_to_Letter_Value:I = 0x9d

.field public static XScale_2L_to_Postcard_Value:I = 0x4c

.field public static XScale_2L_to_Postcard_ValueEx:I = 0x59

.field public static XScale_8x10_to_2L_Value:I = 0x3d

.field public static XScale_A4_to_2L_Value:I = 0x3b

.field public static XScale_A4_to_A3_Value:I = 0x8d

.field public static XScale_A4_to_A5_Value:I = 0x45

.field public static XScale_A4_to_B5_Value:I = 0x55

.field public static XScale_A4_to_KG_Value:I = 0x2f

.field public static XScale_A4_to_Postcard_Value:I = 0x2e

.field public static XScale_A4_to_Postcard_ValueEx:I = 0x35

.field public static XScale_A5_to_A4_Value:I = 0x8d

.field public static XScale_Autofit_Value:I = 0x0

.field public static XScale_B5_to_A4_Value:I = 0x72

.field public static XScale_B5_to_A4_ValueEx:I = 0x78

.field public static XScale_Custom_Value:I = 0x0

.field public static XScale_FullSize_Value:I = 0x64

.field public static XScale_KG_to_2L_Value:I = 0x73

.field public static XScale_KG_to_8x10_Value:I = 0xa6

.field public static XScale_KG_to_A4_Value:I = 0xc3

.field public static XScale_KG_to_Letter_Value:I = 0xb7

.field public static XScale_L_to_2L_Value:I = 0x8b

.field public static XScale_L_to_2L_ValueEx:I = 0x97

.field public static XScale_L_to_A4_Value:I = 0xeb

.field public static XScale_L_to_A4_ValueEx:I = 0xf6

.field public static XScale_L_to_Postcard_Value:I = 0x6e

.field public static XScale_L_to_Postcard_ValueEx:I = 0x7d

.field public static XScale_Legal_to_Letter_Value:I = 0x4e

.field public static XScale_Letter_to_11x17_Value:I = 0x81

.field public static XScale_Letter_to_2L_Value:I = 0x39

.field public static XScale_Letter_to_KG_Value:I = 0x2d

.field public static XScale_Postcard_to_A4_Value:I = 0xc9

.field public static XScale_Postcard_to_A4_ValueEx:I = 0xdb


# instance fields
.field private changableParams:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ">;"
        }
    .end annotation
.end field

.field clientID:Ljava/lang/String;

.field operation:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;

.field optionChangedListener:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;

.field optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

.field replacedItem:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

.field selectedItem:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

.field systemSettings:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 107
    sget v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->CopyMagnificationAutofitValue:I

    sput v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_Autofit_Value:I

    return-void
.end method

.method public constructor <init>(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;)V
    .locals 1

    .line 124
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 125
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;

    invoke-direct {v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;-><init>()V

    iput-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask;->operation:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;

    .line 126
    iput-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask;->selectedItem:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    .line 127
    iput-object p2, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask;->optionChangedListener:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;

    return-void
.end method

.method static getCopyMagnificationMap(Ljava/util/HashMap;)Ljava/util/HashMap;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/HashMap<",
            "Ljava/lang/Integer;",
            "Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;",
            ">;"
        }
    .end annotation

    .line 33
    new-instance p0, Ljava/util/HashMap;

    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    return-object p0
.end method

.method static getXScaleMap(Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ECopyType;)Ljava/util/HashMap;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ECopyType;",
            ")",
            "Ljava/util/HashMap<",
            "Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/4 p0, 0x0

    return-object p0
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;
    .locals 7

    .line 187
    iget-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask;->operation:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;

    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask;->systemSettings:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;

    invoke-interface {v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;->getPrinterIPAddress()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;->setHostIP(Ljava/lang/String;)V

    .line 189
    iget-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask;->selectedItem:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    iget-boolean p1, p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->isLocalOption:Z

    if-eqz p1, :cond_0

    .line 190
    invoke-virtual {p0}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask;->getResultLocalOptions()Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;

    move-result-object p1

    return-object p1

    .line 193
    :cond_0
    iget-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask;->optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->RemoveBackground:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    .line 194
    invoke-virtual {p1, v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionItemOf(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object p1

    .line 195
    invoke-virtual {p1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object p1

    iget-object v2, p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->param:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 197
    iget-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask;->optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->ColorEffectsType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    .line 198
    invoke-virtual {p1, v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionItemOf(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object p1

    .line 199
    invoke-virtual {p1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object p1

    iget-object v3, p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->param:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 202
    iget-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask;->optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XCutLine:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    .line 203
    invoke-virtual {p1, v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionItemOf(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object p1

    .line 204
    invoke-virtual {p1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object p1

    iget-object v4, p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->param:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 206
    iget-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask;->optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XCutLineStyle:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    .line 207
    invoke-virtual {p1, v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionItemOf(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object p1

    .line 208
    invoke-virtual {p1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object p1

    iget-object v5, p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->param:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 210
    iget-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask;->optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XCutLineWeight:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    .line 211
    invoke-virtual {p1, v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionItemOf(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object p1

    .line 212
    invoke-virtual {p1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object p1

    iget-object v6, p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->param:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 214
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask;->operation:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;

    new-instance v1, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask$1;

    invoke-direct {v1, p0}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask$1;-><init>(Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask;)V

    invoke-virtual/range {v0 .. v6}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;->getOptions(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyOptionsParameter;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;

    move-result-object p1

    .line 341
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;

    invoke-direct {v0, p0}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;-><init>(Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask;)V

    .line 342
    iget-object v1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask;->selectedItem:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    iput-object v1, v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;->selectedItem:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    .line 343
    invoke-virtual {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;->success()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 344
    iget-object v1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask;->optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    invoke-virtual {v1, p1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->setCopyOptionsResult(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;)V

    .line 346
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;->changedItems:Ljava/util/ArrayList;

    .line 347
    iget-object v1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask;->changableParams:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 348
    invoke-static {v2, p1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->createOptionItem(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v2

    .line 349
    iget-object v3, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask;->optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    invoke-virtual {v3, v2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->isChanged(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 350
    iget-object v3, v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;->changedItems:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 351
    iget-object v3, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask;->optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    invoke-virtual {v3, v2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->replace(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    goto :goto_0

    .line 355
    :cond_2
    iget-object p1, v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;->changedItems:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    if-nez p1, :cond_5

    const/4 p1, 0x0

    .line 356
    iput-object p1, v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;->changedItems:Ljava/util/ArrayList;

    goto :goto_2

    .line 359
    :cond_3
    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->success:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p1, v1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;->isNull(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 360
    sget-object p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;->ErrorCommunication:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;

    iput-object p1, v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;->error:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;

    goto :goto_1

    .line 362
    :cond_4
    sget-object p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;->Error:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;

    iput-object p1, v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;->error:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;

    .line 372
    :goto_1
    new-instance p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    iget-object v1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask;->replacedItem:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    invoke-direct {p1, v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;-><init>(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)V

    iput-object p1, v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;->selectedItem:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    .line 373
    iget-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask;->optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    iget-object v1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask;->replacedItem:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    invoke-virtual {p1, v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->replace(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    :cond_5
    :goto_2
    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 22
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask;->doInBackground([Ljava/lang/Void;)Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;

    move-result-object p1

    return-object p1
.end method

.method public getResultLocalOptions()Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;
    .locals 2

    .line 177
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;

    invoke-direct {v0, p0}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;-><init>(Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask;)V

    .line 178
    iget-object v1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask;->selectedItem:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    iput-object v1, v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;->selectedItem:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    .line 179
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;->changedItems:Ljava/util/ArrayList;

    const/4 v1, 0x0

    .line 181
    iput-object v1, v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;->changedItems:Ljava/util/ArrayList;

    return-object v0
.end method

.method protected onPostExecute(Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;)V
    .locals 3

    .line 380
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask;->optionChangedListener:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;

    iget-object v1, p1, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;->selectedItem:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    iget-object v2, p1, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;->changedItems:Ljava/util/ArrayList;

    iget-object p1, p1, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;->error:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;

    invoke-interface {v0, v1, v2, p1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;->onCopyOptionChanged(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;Ljava/util/ArrayList;Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 22
    check-cast p1, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;

    invoke-virtual {p0, p1}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask;->onPostExecute(Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;)V

    return-void
.end method

.method public setClientID(Ljava/lang/String;)V
    .locals 0

    .line 148
    iput-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask;->clientID:Ljava/lang/String;

    return-void
.end method

.method public setOptionContext(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;)V
    .locals 2

    .line 153
    iput-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask;->optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    .line 160
    iget-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask;->optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    iget-object v1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask;->selectedItem:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    invoke-direct {v0, v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;-><init>(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)V

    invoke-virtual {p1, v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->replace(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object p1

    iput-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask;->replacedItem:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    .line 163
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask;->changableParams:Ljava/util/ArrayList;

    .line 164
    iget-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask;->changableParams:Ljava/util/ArrayList;

    sget-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->color_effects_type:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 165
    iget-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask;->changableParams:Ljava/util/ArrayList;

    sget-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->layout:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 166
    iget-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask;->changableParams:Ljava/util/ArrayList;

    sget-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_size:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 167
    iget-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask;->changableParams:Ljava/util/ArrayList;

    sget-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_type:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 168
    iget-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask;->changableParams:Ljava/util/ArrayList;

    sget-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_source:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 169
    iget-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask;->changableParams:Ljava/util/ArrayList;

    sget-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_quality:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 170
    iget-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask;->changableParams:Ljava/util/ArrayList;

    sget-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_remove_background:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 171
    iget-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask;->changableParams:Ljava/util/ArrayList;

    sget-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_cut_line:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 172
    iget-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask;->changableParams:Ljava/util/ArrayList;

    sget-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_cut_line_style:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 173
    iget-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask;->changableParams:Ljava/util/ArrayList;

    sget-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_cut_line_weight:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public setRequestConnectionTimeout(I)V
    .locals 1

    .line 143
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask;->operation:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;

    invoke-virtual {v0, p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;->setRequestConnectionTimeout(I)V

    return-void
.end method

.method public setSystemSettings(Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;)V
    .locals 0

    .line 138
    iput-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopySetOptionTask;->systemSettings:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;

    return-void
.end method

.method public start()Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyCancelRequest;
    .locals 1

    const/4 v0, 0x0

    .line 132
    new-array v0, v0, [Ljava/lang/Void;

    invoke-super {p0, v0}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    const/4 v0, 0x0

    return-object v0
.end method

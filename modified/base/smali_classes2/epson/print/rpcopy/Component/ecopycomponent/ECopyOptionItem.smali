.class public Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;
.super Ljava/lang/Object;
.source "ECopyOptionItem.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;,
        Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;,
        Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;,
        Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;,
        Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;",
            ">;"
        }
    .end annotation
.end field

.field public static CopyMagnificationAutofitValue:I

.field static DefaultExceptionValue:I


# instance fields
.field choiceArray:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

.field choiceType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;

.field enabled:Z

.field isLocalOption:Z

.field key:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

.field numberRange:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 113
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$1;

    invoke-direct {v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$1;-><init>()V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->CREATOR:Landroid/os/Parcelable$Creator;

    const/16 v0, -0x3e7

    .line 261
    sput v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->CopyMagnificationAutofitValue:I

    const/16 v0, -0x2710

    .line 262
    sput v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->DefaultExceptionValue:I

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 5

    .line 146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 147
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->valueOf(Ljava/lang/String;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    move-result-object v0

    iput-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->key:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    .line 148
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;->valueOf(Ljava/lang/String;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;

    move-result-object v0

    iput-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->choiceType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;

    .line 149
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->enabled:Z

    .line 150
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v2, :cond_1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    iput-boolean v2, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->isLocalOption:Z

    .line 152
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->choiceType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;->ChoiceArray:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;

    if-ne v0, v2, :cond_3

    .line 153
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    invoke-direct {v0, p0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;-><init>(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)V

    iput-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    .line 154
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    :goto_2
    if-ge v1, v0, :cond_2

    .line 161
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->valueOf(Ljava/lang/String;)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v2

    .line 162
    iget-object v3, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    iget-object v3, v3, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->choices:Ljava/util/ArrayList;

    iget-object v4, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->key:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-static {v4, v2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->valueOf(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 164
    :cond_2
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->selectedChoice:I

    .line 165
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result p1

    iput p1, v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->defaultChoice:I

    goto :goto_3

    .line 167
    :cond_3
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    invoke-direct {v0, p0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;-><init>(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)V

    iput-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->numberRange:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    .line 168
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->numberRange:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->min:I

    .line 169
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->numberRange:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->max:I

    .line 170
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->numberRange:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->selectedValue:I

    .line 171
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->numberRange:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->defaultValue:I

    .line 172
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->numberRange:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result p1

    iput p1, v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->exceptionValue:I

    :goto_3
    return-void
.end method

.method constructor <init>(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;IIII)V
    .locals 0

    .line 176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 177
    iput-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->key:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    .line 178
    sget-object p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;->NumberRange:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;

    iput-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->choiceType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;

    const/4 p1, 0x1

    .line 179
    iput-boolean p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->enabled:Z

    .line 181
    new-instance p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    invoke-direct {p1, p0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;-><init>(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)V

    iput-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->numberRange:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    .line 182
    iget-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->numberRange:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    iput p2, p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->min:I

    .line 183
    iput p3, p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->max:I

    .line 184
    iput p4, p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->selectedValue:I

    .line 185
    iput p4, p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->defaultValue:I

    .line 186
    iput p5, p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->exceptionValue:I

    return-void
.end method

.method constructor <init>(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Ljava/util/ArrayList;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;",
            "Ljava/util/ArrayList<",
            "Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ">;",
            "Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ")V"
        }
    .end annotation

    .line 189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 190
    iput-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->key:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    .line 191
    sget-object p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;->ChoiceArray:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;

    iput-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->choiceType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;

    .line 192
    new-instance p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    invoke-direct {p1, p0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;-><init>(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)V

    iput-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    .line 193
    iget-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    const/4 v0, 0x0

    iput v0, p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->defaultChoice:I

    .line 194
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 195
    iget-object v3, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    iget-object v3, v3, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->choices:Ljava/util/ArrayList;

    iget-object v4, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->key:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-static {v4, v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->valueOf(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-ne v1, p3, :cond_0

    .line 197
    iget-object v1, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    iget-object v3, v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->choices:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    sub-int/2addr v3, v2

    iput v3, v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->defaultChoice:I

    goto :goto_0

    .line 200
    :cond_1
    iget-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    iget p3, p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->defaultChoice:I

    iput p3, p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->selectedChoice:I

    .line 202
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result p1

    if-le p1, v2, :cond_2

    .line 203
    iput-boolean v2, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->enabled:Z

    goto :goto_1

    .line 205
    :cond_2
    iput-boolean v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->enabled:Z

    :goto_1
    return-void
.end method

.method constructor <init>(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;)V
    .locals 2

    .line 209
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 210
    iget-object v0, p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->key:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    iput-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->key:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    .line 211
    iget-object v0, p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->choiceType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;

    iput-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->choiceType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;

    .line 212
    iget-boolean v0, p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->enabled:Z

    iput-boolean v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->enabled:Z

    .line 213
    iget-boolean v0, p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->isLocalOption:Z

    iput-boolean v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->isLocalOption:Z

    .line 214
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->choiceType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;->ChoiceArray:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;

    if-ne v0, v1, :cond_0

    .line 215
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    iget-object p1, p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    invoke-direct {v0, p0, p1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;-><init>(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;)V

    iput-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    goto :goto_0

    .line 217
    :cond_0
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    iget-object p1, p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->numberRange:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    invoke-direct {v0, p0, p1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;-><init>(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;)V

    iput-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->numberRange:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    :goto_0
    return-void
.end method

.method static createLocalOptionItem(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;III)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;
    .locals 7

    .line 1326
    new-instance v6, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    invoke-static {p0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->key(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    move-result-object v1

    sget v5, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->DefaultExceptionValue:I

    move-object v0, v6

    move v2, p1

    move v3, p2

    move v4, p3

    invoke-direct/range {v0 .. v5}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;-><init>(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;IIII)V

    const/4 p0, 0x1

    .line 1327
    iput-boolean p0, v6, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->isLocalOption:Z

    return-object v6
.end method

.method static createLocalOptionItem(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;
    .locals 2

    .line 1308
    invoke-virtual {p1, p0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;->local_options(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1309
    invoke-virtual {p1, p0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;->local_default(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object p1

    .line 1310
    new-instance v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    invoke-static {p0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->key(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    move-result-object p0

    invoke-direct {v1, p0, v0, p1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;-><init>(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Ljava/util/ArrayList;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    const/4 p0, 0x1

    .line 1311
    iput-boolean p0, v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->isLocalOption:Z

    return-object v1
.end method

.method static createOptionItem(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;
    .locals 3

    .line 1284
    invoke-virtual {p1, p0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;->parameter_options(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1285
    invoke-virtual {p1, p0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;->parameter_default(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object p1

    .line 1292
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 1293
    invoke-virtual {p1, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    if-nez v1, :cond_2

    .line 1299
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1304
    :cond_2
    new-instance v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    invoke-static {p0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->key(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    move-result-object p0

    invoke-direct {v1, p0, v0, p1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;-><init>(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Ljava/util/ArrayList;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    return-object v1
.end method

.method static createScaleOptionItem(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;
    .locals 2

    .line 1316
    sget-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_size:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p1, v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;->parameter_options(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1317
    new-instance v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult$ScaleOptions;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v1, p1, v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult$ScaleOptions;-><init>(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;Ljava/util/ArrayList;)V

    .line 1318
    invoke-virtual {v1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult$ScaleOptions;->scale_options()Ljava/util/ArrayList;

    move-result-object v0

    .line 1319
    invoke-virtual {p1, p0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;->local_default(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object p1

    .line 1320
    new-instance v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    invoke-static {p0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->key(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    move-result-object p0

    invoke-direct {v1, p0, v0, p1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;-><init>(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Ljava/util/ArrayList;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    const/4 p0, 0x1

    .line 1321
    iput-boolean p0, v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->isLocalOption:Z

    return-object v1
.end method

.method static key(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;
    .locals 2

    .line 1249
    sget-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->InvalidKey:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    .line 1250
    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$2;->$SwitchMap$epson$print$rpcopy$Component$eremoteoperation$ERemoteOperation$ERemoteParam:[I

    invoke-virtual {p0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->ordinal()I

    move-result p0

    aget p0, v1, p0

    packed-switch p0, :pswitch_data_0

    goto :goto_0

    .line 1269
    :pswitch_0
    sget-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XCutLineWeight:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    goto :goto_0

    .line 1268
    :pswitch_1
    sget-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XCutLineStyle:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    goto :goto_0

    .line 1267
    :pswitch_2
    sget-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XCutLine:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    goto :goto_0

    .line 1266
    :pswitch_3
    sget-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->RemoveBackground:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    goto :goto_0

    .line 1265
    :pswitch_4
    sget-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->RepeatLayout:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    goto :goto_0

    .line 1264
    :pswitch_5
    sget-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XDensity:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    goto :goto_0

    .line 1257
    :pswitch_6
    sget-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintQuality:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    goto :goto_0

    .line 1256
    :pswitch_7
    sget-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    goto :goto_0

    .line 1255
    :pswitch_8
    sget-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSource:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    goto :goto_0

    .line 1254
    :pswitch_9
    sget-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    goto :goto_0

    .line 1252
    :pswitch_a
    sget-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->Copies:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    goto :goto_0

    .line 1251
    :pswitch_b
    sget-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->ColorEffectsType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    :goto_0
    return-object v0

    :pswitch_data_0
    .packed-switch 0x52
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static layoutOf(Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ECopyType;)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 2

    .line 1227
    sget-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->standard:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 1228
    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$2;->$SwitchMap$epson$print$rpcopy$Component$ecopycomponent$ECopyComponent$ECopyType:[I

    invoke-virtual {p0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ECopyType;->ordinal()I

    move-result p0

    aget p0, v1, p0

    packed-switch p0, :pswitch_data_0

    goto :goto_0

    .line 1236
    :pswitch_0
    sget-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->standard:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    goto :goto_0

    .line 1235
    :pswitch_1
    sget-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->mirror:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    goto :goto_0

    .line 1234
    :pswitch_2
    sget-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->two_up_book:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    goto :goto_0

    .line 1233
    :pswitch_3
    sget-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->two_up_book:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    goto :goto_0

    .line 1232
    :pswitch_4
    sget-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->two_up:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    goto :goto_0

    .line 1231
    :pswitch_5
    sget-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->two_up:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    goto :goto_0

    .line 1230
    :pswitch_6
    sget-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->borderless:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    goto :goto_0

    .line 1229
    :pswitch_7
    sget-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->standard:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    :goto_0
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    .line 227
    :cond_0
    instance-of v0, p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    const/4 v1, 0x0

    if-nez v0, :cond_1

    return v1

    .line 231
    :cond_1
    check-cast p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    .line 232
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->key:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    iget-object v2, p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->key:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    if-eq v0, v2, :cond_2

    return v1

    .line 236
    :cond_2
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->choiceType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;

    iget-object v2, p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->choiceType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;

    if-eq v0, v2, :cond_3

    return v1

    .line 240
    :cond_3
    iget-boolean v2, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->isLocalOption:Z

    iget-boolean v3, p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->isLocalOption:Z

    if-eq v2, v3, :cond_4

    return v1

    .line 244
    :cond_4
    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;->ChoiceArray:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;

    if-ne v0, v1, :cond_5

    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    iget-object p1, p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    .line 245
    invoke-virtual {v0, p1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->equals(Ljava/lang/Object;)Z

    move-result p1

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->numberRange:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    iget-object p1, p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->numberRange:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    .line 246
    invoke-virtual {v0, p1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->equals(Ljava/lang/Object;)Z

    move-result p1

    :goto_0
    return p1
.end method

.method public getChoiceType()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;
    .locals 1

    .line 1078
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->choiceType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;

    return-object v0
.end method

.method public getDefaultChoice()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;
    .locals 2

    .line 1153
    :try_start_0
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    iget-object v0, v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->choices:Ljava/util/ArrayList;

    iget-object v1, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    iget v1, v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->defaultChoice:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getDefaultValue()I
    .locals 1

    .line 1108
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->numberRange:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    iget v0, v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->defaultValue:I

    return v0
.end method

.method public getKey()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;
    .locals 1

    .line 1062
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->key:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    return-object v0
.end method

.method public getMaximumValue()I
    .locals 1

    .line 1098
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->numberRange:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    iget v0, v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->max:I

    return v0
.end method

.method public getMinimumValue()I
    .locals 1

    .line 1088
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->numberRange:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    iget v0, v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->min:I

    return v0
.end method

.method public getSelectableChoices()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;",
            ">;"
        }
    .end annotation

    .line 1139
    :try_start_0
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    iget-object v0, v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->choices:Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public declared-synchronized getSelectedChoice()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;
    .locals 2

    monitor-enter p0

    .line 1167
    :try_start_0
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    iget-object v0, v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->choices:Ljava/util/ArrayList;

    iget-object v1, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    iget v1, v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->selectedChoice:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_0
    const/4 v0, 0x0

    .line 1170
    monitor-exit p0

    return-object v0
.end method

.method public getSelectedValue()I
    .locals 1

    .line 1118
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->numberRange:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    iget v0, v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->selectedValue:I

    return v0
.end method

.method public isEnabled()Z
    .locals 1

    .line 1070
    iget-boolean v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->enabled:Z

    return v0
.end method

.method public declared-synchronized selectChoice(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;)V
    .locals 3

    monitor-enter p0

    const/4 v0, 0x0

    .line 1181
    :try_start_0
    iget-object v1, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    iget-object v1, v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->choices:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    if-ne v2, p1, :cond_0

    .line 1183
    iget-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    iput v0, p1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->selectedChoice:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1188
    :cond_1
    :goto_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public selectValue(I)V
    .locals 2

    .line 1127
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->numberRange:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    iget v1, v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->exceptionValue:I

    if-ne p1, v1, :cond_0

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->numberRange:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    iget v1, v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->min:I

    .line 1128
    invoke-static {v1, p1}, Ljava/lang/Math;->max(II)I

    move-result p1

    iget-object v1, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->numberRange:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    iget v1, v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->max:I

    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result p1

    :goto_0
    iput p1, v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->selectedValue:I

    return-void
.end method

.method public declared-synchronized updateChoicesXCutLine()V
    .locals 6

    monitor-enter p0

    .line 1195
    :try_start_0
    sget-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->dash:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v1

    invoke-virtual {v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->getParam()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v1

    invoke-virtual {v0, v1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    .line 1196
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    iget-object v0, v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->choices:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, v2, :cond_1

    .line 1199
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    iget-object v0, v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->choices:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1200
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    iget-object v0, v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->choices:Ljava/util/ArrayList;

    sget-object v3, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XCutLine_Dash:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1201
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    iput v1, v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->selectedChoice:I

    .line 1202
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    iput v1, v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->defaultChoice:I

    goto :goto_0

    .line 1206
    :cond_0
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    iget-object v0, v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->choices:Ljava/util/ArrayList;

    sget-object v3, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XCutLine_Dash:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    const/4 v3, -0x1

    if-eq v0, v3, :cond_1

    .line 1209
    invoke-virtual {p0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v0

    .line 1210
    invoke-virtual {p0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getDefaultChoice()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v3

    .line 1211
    iget-object v4, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    iget-object v4, v4, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->choices:Ljava/util/ArrayList;

    sget-object v5, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XCutLine_Dash:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1212
    iget-object v4, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    iget-object v5, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    iget-object v5, v5, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->choices:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    iput v0, v4, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->selectedChoice:I

    .line 1213
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    iget-object v4, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    iget-object v4, v4, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->choices:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v3

    iput v3, v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->defaultChoice:I

    .line 1218
    :cond_1
    :goto_0
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    iget-object v0, v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->choices:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, v2, :cond_2

    .line 1219
    iput-boolean v2, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->enabled:Z

    goto :goto_1

    .line 1221
    :cond_2
    iput-boolean v1, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->enabled:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1224
    :goto_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 125
    iget-object p2, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->key:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {p2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 126
    iget-object p2, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->choiceType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;

    invoke-virtual {p2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 127
    iget-boolean p2, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->enabled:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 128
    iget-boolean p2, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->isLocalOption:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 130
    iget-object p2, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->choiceType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;

    sget-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;->ChoiceArray:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;

    if-ne p2, v0, :cond_1

    .line 131
    iget-object p2, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    iget-object p2, p2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->choices:Ljava/util/ArrayList;

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 132
    iget-object p2, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    iget-object p2, p2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->choices:Ljava/util/ArrayList;

    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 133
    iget-object v0, v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->param:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 135
    :cond_0
    iget-object p2, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    iget p2, p2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->selectedChoice:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 136
    iget-object p2, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    iget p2, p2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->defaultChoice:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_1

    .line 138
    :cond_1
    iget-object p2, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->numberRange:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    iget p2, p2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->min:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 139
    iget-object p2, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->numberRange:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    iget p2, p2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->max:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 140
    iget-object p2, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->numberRange:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    iget p2, p2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->selectedValue:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 141
    iget-object p2, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->numberRange:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    iget p2, p2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->defaultValue:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 142
    iget-object p2, p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->numberRange:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    iget p2, p2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->exceptionValue:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    :goto_1
    return-void
.end method

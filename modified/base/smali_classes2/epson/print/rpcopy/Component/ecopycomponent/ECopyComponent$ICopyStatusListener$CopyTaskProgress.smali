.class public final enum Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;
.super Ljava/lang/Enum;
.source "ECopyComponent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CopyTaskProgress"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

.field public static final enum Canceling:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

.field public static final enum Copying:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

.field public static final enum Processing:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

.field public static final enum Scanning:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

.field public static final enum Stopped:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

.field public static final enum Waiting2ndPage:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 334
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    const-string v1, "Copying"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;->Copying:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    .line 338
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    const-string v1, "Waiting2ndPage"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;->Waiting2ndPage:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    .line 342
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    const-string v1, "Canceling"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;->Canceling:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    .line 346
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    const-string v1, "Scanning"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;->Scanning:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    .line 350
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    const-string v1, "Stopped"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;->Stopped:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    .line 354
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    const-string v1, "Processing"

    const/4 v7, 0x5

    invoke-direct {v0, v1, v7}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;->Processing:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    const/4 v0, 0x6

    .line 330
    new-array v0, v0, [Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;->Copying:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;->Waiting2ndPage:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    aput-object v1, v0, v3

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;->Canceling:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    aput-object v1, v0, v4

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;->Scanning:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    aput-object v1, v0, v5

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;->Stopped:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    aput-object v1, v0, v6

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;->Processing:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    aput-object v1, v0, v7

    sput-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;->$VALUES:[Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 330
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;
    .locals 1

    .line 330
    const-class v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    return-object p0
.end method

.method public static values()[Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;
    .locals 1

    .line 330
    sget-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;->$VALUES:[Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    invoke-virtual {v0}, [Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    return-object v0
.end method

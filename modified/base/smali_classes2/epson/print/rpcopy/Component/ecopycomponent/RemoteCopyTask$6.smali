.class Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$6;
.super Ljava/lang/Object;
.source "RemoteCopyTask.java"

# interfaces
.implements Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyParameter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->startCopy()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;


# direct methods
.method constructor <init>(Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;)V
    .locals 0

    .line 386
    iput-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$6;->this$0:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public client_id()Ljava/lang/String;
    .locals 1

    .line 389
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$6;->this$0:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;

    iget-object v0, v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->clientID:Ljava/lang/String;

    return-object v0
.end method

.method public color_effects_type()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 2

    .line 456
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$6;->this$0:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;

    iget-object v0, v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->ColorEffectsType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v0, v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionItemOf(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v0

    .line 457
    invoke-virtual {v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v0

    iget-object v0, v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->param:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    return-object v0
.end method

.method public copies()I
    .locals 2

    .line 450
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$6;->this$0:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;

    iget-object v0, v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->Copies:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v0, v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionItemOf(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v0

    .line 451
    invoke-virtual {v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getSelectedValue()I

    move-result v0

    return v0
.end method

.method public copy_magnification()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public layout()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 1

    .line 394
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$6;->this$0:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;

    iget-object v0, v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    invoke-virtual {v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->getCopyType()Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ECopyType;

    move-result-object v0

    invoke-static {v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->layoutOf(Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ECopyType;)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v0

    return-object v0
.end method

.method public orientation()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 1

    .line 480
    sget-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->portrait:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    return-object v0
.end method

.method public print_media_size()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 2

    .line 427
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$6;->this$0:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;

    iget-object v0, v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v0, v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionItemOf(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v0

    .line 428
    invoke-virtual {v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v0

    iget-object v0, v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->param:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    return-object v0
.end method

.method public print_media_source()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 2

    .line 433
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$6;->this$0:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;

    iget-object v0, v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSource:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v0, v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionItemOf(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v0

    .line 434
    invoke-virtual {v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v0

    iget-object v0, v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->param:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    return-object v0
.end method

.method public print_media_type()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 2

    .line 421
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$6;->this$0:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;

    iget-object v0, v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v0, v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionItemOf(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v0

    .line 422
    invoke-virtual {v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v0

    iget-object v0, v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->param:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    return-object v0
.end method

.method public print_quality()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 2

    .line 439
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$6;->this$0:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;

    iget-object v0, v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintQuality:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v0, v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionItemOf(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v0

    .line 440
    invoke-virtual {v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v0

    iget-object v0, v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->param:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    return-object v0
.end method

.method public print_sheet_collate()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 2

    .line 445
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$6;->this$0:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;

    iget-object v0, v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    invoke-virtual {v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionsResult()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;

    move-result-object v0

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_sheet_collate:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;->parameter_default(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v0

    return-object v0
.end method

.method public print_sides()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 2

    .line 404
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$6;->this$0:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;

    iget-object v0, v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    invoke-virtual {v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionsResult()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;

    move-result-object v0

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_sides:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;->parameter_default(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v0

    return-object v0
.end method

.method public print_x_auto_pg()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 2

    .line 490
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$6;->this$0:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;

    iget-object v0, v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->systemSettings:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;

    invoke-interface {v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;->getThickPaper()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 491
    sget-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->on:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    goto :goto_0

    :cond_0
    sget-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->off:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    :goto_0
    return-object v0
.end method

.method public print_x_bleed()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public print_x_dry_time()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 1

    .line 485
    sget-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->normal:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    return-object v0
.end method

.method public scan_content_type()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public scan_media_size()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 2

    .line 409
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$6;->this$0:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;

    iget-object v0, v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    invoke-virtual {v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionsResult()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;

    move-result-object v0

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scan_media_size:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;->parameter_default(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v0

    return-object v0
.end method

.method public scan_sides()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 2

    .line 399
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$6;->this$0:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;

    iget-object v0, v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    invoke-virtual {v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionsResult()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;

    move-result-object v0

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scan_sides:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;->parameter_default(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v0

    return-object v0
.end method

.method public x_density()I
    .locals 2

    .line 467
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$6;->this$0:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;

    iget-object v0, v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XDensity:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v0, v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionItemOf(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v0

    .line 468
    invoke-virtual {v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getSelectedValue()I

    move-result v0

    return v0
.end method

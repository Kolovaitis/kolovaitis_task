.class Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;
.super Landroid/os/AsyncTask;
.source "RemoteCopyTask.java"

# interfaces
.implements Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ITask;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Result;,
        Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;,
        Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$CopyMode;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;",
        "Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Result;",
        ">;",
        "Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ITask;"
    }
.end annotation


# instance fields
.field cancelParameter:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCancelParameter;

.field cancelRequested:Z

.field clientID:Ljava/lang/String;

.field copyMode:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$CopyMode;

.field jobToken:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mCopyParams:Lepson/print/rpcopy/Component/ecopycomponent/RepeatCopyAnalyticsParams;

.field operation:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;

.field optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

.field progress:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;

.field recoverJobToken:Ljava/lang/String;

.field statusListener:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;

.field statusParameter:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyStatusParameter;

.field systemSettings:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;


# direct methods
.method public constructor <init>(Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$CopyMode;Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;Landroid/content/Context;)V
    .locals 1

    const-string v0, ""

    .line 175
    invoke-direct {p0, p1, v0, p2, p3}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;-><init>(Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$CopyMode;Ljava/lang/String;Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$CopyMode;Ljava/lang/String;Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;Landroid/content/Context;)V
    .locals 1

    .line 164
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 165
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;

    invoke-direct {v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;-><init>()V

    iput-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->operation:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;

    .line 166
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;

    invoke-direct {v0, p0}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;-><init>(Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;)V

    iput-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;

    .line 167
    iput-object p3, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->statusListener:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;

    .line 168
    iput-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->copyMode:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$CopyMode;

    .line 169
    iput-object p2, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->recoverJobToken:Ljava/lang/String;

    .line 171
    iput-object p4, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->mContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$000(Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;[Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;)V
    .locals 0

    .line 38
    invoke-direct {p0, p1}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->localPublishProgress([Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;)V

    return-void
.end method

.method private getCopyParams(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyParameter;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/rpcopy/Component/ecopycomponent/RepeatCopyAnalyticsParams;
    .locals 2
    .param p1    # Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyParameter;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p4    # Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p5    # Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p6    # Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 559
    :try_start_0
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/RepeatCopyAnalyticsParams;

    invoke-direct {v0}, Lepson/print/rpcopy/Component/ecopycomponent/RepeatCopyAnalyticsParams;-><init>()V

    .line 561
    invoke-interface {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyParameter;->color_effects_type()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v1

    invoke-virtual {v1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lepson/print/rpcopy/Component/ecopycomponent/RepeatCopyAnalyticsParams;->colorMode:Ljava/lang/String;

    .line 562
    invoke-interface {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyParameter;->x_density()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lepson/print/rpcopy/Component/ecopycomponent/RepeatCopyAnalyticsParams;->density:Ljava/lang/String;

    .line 563
    invoke-virtual {p2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->toString()Ljava/lang/String;

    move-result-object p2

    iput-object p2, v0, Lepson/print/rpcopy/Component/ecopycomponent/RepeatCopyAnalyticsParams;->repeatLayout:Ljava/lang/String;

    .line 564
    invoke-interface {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyParameter;->print_media_size()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object p2

    invoke-virtual {p2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->toString()Ljava/lang/String;

    move-result-object p2

    iput-object p2, v0, Lepson/print/rpcopy/Component/ecopycomponent/RepeatCopyAnalyticsParams;->paperSize:Ljava/lang/String;

    .line 565
    invoke-interface {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyParameter;->print_media_type()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object p2

    invoke-virtual {p2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->toString()Ljava/lang/String;

    move-result-object p2

    iput-object p2, v0, Lepson/print/rpcopy/Component/ecopycomponent/RepeatCopyAnalyticsParams;->paperType:Ljava/lang/String;

    .line 566
    invoke-interface {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyParameter;->print_media_source()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object p2

    invoke-virtual {p2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->toString()Ljava/lang/String;

    move-result-object p2

    iput-object p2, v0, Lepson/print/rpcopy/Component/ecopycomponent/RepeatCopyAnalyticsParams;->printDevice:Ljava/lang/String;

    .line 567
    invoke-interface {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyParameter;->print_quality()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object p1

    invoke-virtual {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, v0, Lepson/print/rpcopy/Component/ecopycomponent/RepeatCopyAnalyticsParams;->copyQuality:Ljava/lang/String;

    .line 568
    invoke-virtual {p3}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, v0, Lepson/print/rpcopy/Component/ecopycomponent/RepeatCopyAnalyticsParams;->removeBackground:Ljava/lang/String;

    if-eqz p4, :cond_0

    .line 570
    sget-object p1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->dash:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    if-eq p4, p1, :cond_0

    .line 572
    invoke-virtual {p4}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, v0, Lepson/print/rpcopy/Component/ecopycomponent/RepeatCopyAnalyticsParams;->XCutLine:Ljava/lang/String;

    .line 573
    sget-object p1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->on:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    if-ne p4, p1, :cond_0

    .line 575
    invoke-virtual {p5}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, v0, Lepson/print/rpcopy/Component/ecopycomponent/RepeatCopyAnalyticsParams;->XCutLineStyle:Ljava/lang/String;

    .line 576
    invoke-virtual {p6}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, v0, Lepson/print/rpcopy/Component/ecopycomponent/RepeatCopyAnalyticsParams;->XCutLineWeight:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-object v0

    :catch_0
    const/4 p1, 0x0

    return-object p1
.end method

.method static getPrinterStopReason(Ljava/util/ArrayList;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ">;)",
            "Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;"
        }
    .end annotation

    .line 107
    sget-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->None:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    .line 108
    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->none:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    iget-object v1, v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->string:Ljava/lang/String;

    iput-object v1, v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->string:Ljava/lang/String;

    .line 109
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/4 v2, 0x1

    .line 111
    sget-object v3, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$10;->$SwitchMap$epson$print$rpcopy$Component$eremoteoperation$ERemoteOperation$ERemoteParam:[I

    invoke-virtual {v1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    const/4 v2, 0x0

    goto :goto_1

    .line 119
    :pswitch_0
    sget-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->PrinterOtherError:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    goto :goto_1

    .line 118
    :pswitch_1
    sget-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->PrinterOutputAreaFullError:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    goto :goto_1

    .line 117
    :pswitch_2
    sget-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->PrinterCoverOpenError:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    goto :goto_1

    .line 116
    :pswitch_3
    sget-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->PrinterOtherError:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    goto :goto_1

    .line 115
    :pswitch_4
    sget-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->PrinterMediaEmptyError:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    goto :goto_1

    .line 114
    :pswitch_5
    sget-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->PrinterMediaJamError:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    goto :goto_1

    .line 113
    :pswitch_6
    sget-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->PrinterMarkerWasteFullError:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    goto :goto_1

    .line 112
    :pswitch_7
    sget-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->PrinterMarkerSupplyEmptyError:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    :goto_1
    if-eqz v2, :cond_0

    .line 127
    iget-object v1, v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->string:Ljava/lang/String;

    iput-object v1, v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->string:Ljava/lang/String;

    goto :goto_0

    :cond_1
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0xc
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static getScannerStopReason(Ljava/util/ArrayList;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ">;)",
            "Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;"
        }
    .end annotation

    .line 134
    sget-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->None:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    .line 135
    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->none:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    iget-object v1, v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->string:Ljava/lang/String;

    iput-object v1, v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->string:Ljava/lang/String;

    .line 136
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 138
    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$10;->$SwitchMap$epson$print$rpcopy$Component$eremoteoperation$ERemoteOperation$ERemoteParam:[I

    invoke-virtual {v1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    goto :goto_1

    .line 141
    :pswitch_1
    sget-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->ScannerOtherError:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    goto :goto_1

    .line 142
    :pswitch_2
    sget-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->ScannerOtherError:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    goto :goto_1

    .line 139
    :pswitch_3
    sget-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->ScannerOtherError:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    goto :goto_1

    .line 140
    :pswitch_4
    sget-object v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->ScannerOtherError:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    .line 150
    :goto_1
    iget-object v1, v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->string:Ljava/lang/String;

    iput-object v1, v0, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->string:Ljava/lang/String;

    goto :goto_0

    :cond_0
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0xe
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private varargs localPublishProgress([Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;)V
    .locals 1

    const/4 v0, 0x0

    .line 778
    aget-object v0, p1, v0

    .line 779
    iget-object v0, v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;->taskProgress:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    if-nez v0, :cond_0

    const-string p1, "RemoteCOpyTask"

    const-string v0, "Error: Progress.taskProgress == null"

    .line 780
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 783
    :cond_0
    invoke-virtual {p0, p1}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->publishProgress([Ljava/lang/Object;)V

    return-void
.end method

.method private sendLog(I)V
    .locals 4

    .line 761
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->mCopyParams:Lepson/print/rpcopy/Component/ecopycomponent/RepeatCopyAnalyticsParams;

    .line 762
    iget-object v1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_1

    if-nez v0, :cond_0

    goto :goto_0

    .line 768
    :cond_0
    new-instance v2, Lcom/epson/iprint/prtlogger/CommonLog;

    invoke-direct {v2}, Lcom/epson/iprint/prtlogger/CommonLog;-><init>()V

    .line 769
    invoke-virtual {v2, v1}, Lcom/epson/iprint/prtlogger/CommonLog;->setConnectionType(Landroid/content/Context;)V

    .line 770
    invoke-virtual {v2, v1}, Lcom/epson/iprint/prtlogger/CommonLog;->setPrinterName(Landroid/content/Context;)V

    const/16 v3, 0x2006

    .line 771
    iput v3, v2, Lcom/epson/iprint/prtlogger/CommonLog;->action:I

    .line 772
    iput p1, v2, Lcom/epson/iprint/prtlogger/CommonLog;->numberOfSheet:I

    .line 774
    invoke-static {v1, v0, v2}, Lcom/epson/iprint/prtlogger/Analytics;->sendRepeatCopyLog(Landroid/content/Context;Lepson/print/rpcopy/Component/ecopycomponent/RepeatCopyAnalyticsParams;Lcom/epson/iprint/prtlogger/CommonLog;)V

    return-void

    :cond_1
    :goto_0
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Result;
    .locals 9

    .line 590
    iget-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->operation:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;

    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->systemSettings:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;

    invoke-interface {v0}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;->getPrinterIPAddress()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;->setHostIP(Ljava/lang/String;)V

    .line 597
    invoke-virtual {p0}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->startCopy()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;

    move-result-object p1

    .line 598
    invoke-virtual {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;->success()Z

    move-result v0

    if-nez v0, :cond_0

    .line 599
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Result;

    invoke-direct {v0, p0, p1}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Result;-><init>(Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;)V

    return-object v0

    :cond_0
    const/4 p1, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x1

    move-object v3, v0

    const/4 v2, 0x0

    const/4 v4, 0x0

    :goto_0
    if-nez v2, :cond_7

    .line 603
    iget-boolean v3, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->cancelRequested:Z

    if-eqz v3, :cond_2

    if-nez v4, :cond_2

    .line 604
    iget-object v3, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->operation:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;

    iget-object v4, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->cancelParameter:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCancelParameter;

    invoke-virtual {v3, v4}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;->cancel(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCancelParameter;)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;

    move-result-object v3

    .line 608
    invoke-virtual {v3}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;->success()Z

    move-result v3

    if-nez v3, :cond_1

    .line 609
    new-instance p1, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Result;

    sget-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->canceled:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-direct {p1, p0, v0}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Result;-><init>(Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    return-object p1

    :cond_1
    const/4 v4, 0x1

    .line 614
    :cond_2
    iget-object v3, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->operation:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;

    iget-object v5, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->statusParameter:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyStatusParameter;

    invoke-virtual {v3, v5}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;->getStatus(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyStatusParameter;)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;

    move-result-object v3

    .line 615
    invoke-virtual {v3}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->success()Z

    move-result v5

    if-nez v5, :cond_3

    .line 616
    iget-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;

    iget p1, p1, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;->currentPages:I

    invoke-direct {p0, p1}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->sendLog(I)V

    .line 617
    new-instance p1, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Result;

    invoke-direct {p1, p0, v3}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Result;-><init>(Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;)V

    return-object p1

    .line 620
    :cond_3
    new-instance v5, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Result;

    invoke-virtual {v3}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->job_result()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v6

    invoke-direct {v5, p0, v6}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Result;-><init>(Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 621
    iget-object v6, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;

    invoke-virtual {v3}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->job_print_total_pages()I

    move-result v7

    iput v7, v6, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;->totalPages:I

    .line 622
    iget-object v6, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;

    invoke-virtual {v3}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->job_print_current_pages()I

    move-result v7

    iput v7, v6, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;->currentPages:I

    .line 623
    iget-object v6, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;

    iget v6, v6, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;->currentPages:I

    if-ge v6, v1, :cond_4

    .line 624
    iget-object v6, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;

    iput v1, v6, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;->currentPages:I

    .line 627
    :cond_4
    invoke-virtual {v3}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->job_state()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v6

    .line 628
    sget-object v7, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$10;->$SwitchMap$epson$print$rpcopy$Component$eremoteoperation$ERemoteOperation$ERemoteParam:[I

    invoke-virtual {v6}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_0

    goto :goto_1

    .line 661
    :pswitch_0
    iget-object v2, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;

    iget v2, v2, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;->currentPages:I

    invoke-direct {p0, v2}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->sendLog(I)V

    move-object v3, v5

    const/4 v2, 0x1

    goto :goto_0

    .line 633
    :pswitch_1
    iget-object v7, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;

    iput-object v0, v7, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;->resumeState:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$ResumeState;

    .line 634
    sget-object v8, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;->Waiting2ndPage:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    iput-object v8, v7, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;->taskProgress:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    .line 635
    iget-object v7, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;

    new-instance v8, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$8;

    invoke-direct {v8, p0}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$8;-><init>(Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;)V

    iput-object v8, v7, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;->resumeRequest:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyInvalidateResumeRequest;

    goto :goto_1

    .line 631
    :pswitch_2
    iget-object v7, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;

    sget-object v8, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;->Canceling:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    iput-object v8, v7, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;->taskProgress:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    goto :goto_1

    .line 630
    :pswitch_3
    iget-object v7, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;

    sget-object v8, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;->Copying:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    iput-object v8, v7, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;->taskProgress:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    goto :goto_1

    .line 629
    :pswitch_4
    iget-object v7, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;

    sget-object v8, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;->Scanning:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    iput-object v8, v7, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;->taskProgress:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    .line 674
    :goto_1
    sget-object v7, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$10;->$SwitchMap$epson$print$rpcopy$Component$eremoteoperation$ERemoteOperation$ERemoteParam:[I

    invoke-virtual {v3}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->printer_state()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v8

    invoke-virtual {v8}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_1

    goto :goto_2

    .line 678
    :pswitch_5
    sget-object v7, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->copying:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    if-eq v6, v7, :cond_5

    sget-object v7, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scanning:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    if-ne v6, v7, :cond_6

    .line 679
    :cond_5
    iget-object v6, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;

    iput-object v0, v6, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;->resumeState:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$ResumeState;

    .line 680
    sget-object v7, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;->Stopped:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    iput-object v7, v6, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;->taskProgress:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    .line 681
    iget-object v6, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;

    new-instance v7, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$9;

    invoke-direct {v7, p0, v3}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$9;-><init>(Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;)V

    iput-object v7, v6, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;->resumeRequest:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyInvalidateResumeRequest;

    .line 741
    :cond_6
    :goto_2
    :pswitch_6
    new-array v3, v1, [Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;

    iget-object v6, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;

    aput-object v6, v3, p1

    invoke-direct {p0, v3}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->localPublishProgress([Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;)V

    .line 745
    invoke-virtual {p0}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->resumeExecute()V

    const/16 v3, 0x3e8

    int-to-long v6, v3

    .line 747
    :try_start_0
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    move-exception v3

    .line 749
    invoke-virtual {v3}, Ljava/lang/InterruptedException;->printStackTrace()V

    :goto_3
    move-object v3, v5

    goto/16 :goto_0

    :cond_7
    return-object v3

    :pswitch_data_0
    .packed-switch 0x18
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x15
        :pswitch_6
        :pswitch_6
        :pswitch_5
    .end packed-switch
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 38
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->doInBackground([Ljava/lang/Void;)Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Result;

    move-result-object p1

    return-object p1
.end method

.method protected onPostExecute(Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Result;)V
    .locals 2

    .line 788
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->statusListener:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;->Copy:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;

    iget-object p1, p1, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Result;->taskResult:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    invoke-interface {v0, v1, p1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;->onFinished(Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 38
    check-cast p1, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Result;

    invoke-virtual {p0, p1}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->onPostExecute(Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Result;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .line 213
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$2;

    invoke-direct {v0, p0}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$2;-><init>(Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;)V

    iput-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->cancelParameter:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCancelParameter;

    .line 225
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$3;

    invoke-direct {v0, p0}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$3;-><init>(Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;)V

    iput-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->statusParameter:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyStatusParameter;

    .line 251
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->statusListener:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;->Copy:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;

    invoke-interface {v0, v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;->onStarted(Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;)V

    return-void
.end method

.method protected varargs onProgressUpdate([Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;)V
    .locals 6

    const/4 v0, 0x0

    .line 256
    aget-object p1, p1, v0

    .line 257
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->statusListener:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;->Copy:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;

    iget v2, p1, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;->totalPages:I

    iget v3, p1, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;->currentPages:I

    iget-object v4, p1, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;->taskProgress:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    iget-object v5, p1, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;->resumeRequest:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyInvalidateResumeRequest;

    invoke-interface/range {v0 .. v5}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;->onProcessed(Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;IILepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest;)V

    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .line 38
    check-cast p1, [Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;

    invoke-virtual {p0, p1}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->onProgressUpdate([Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;)V

    return-void
.end method

.method resumeExecute()V
    .locals 6

    .line 268
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;

    iget-object v0, v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;->resumeRequest:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyInvalidateResumeRequest;

    if-nez v0, :cond_0

    return-void

    .line 272
    :cond_0
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;

    monitor-enter v0

    :cond_1
    :goto_0
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 274
    :try_start_0
    iget-object v3, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;

    iget-object v3, v3, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;->resumeState:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$ResumeState;

    if-nez v3, :cond_2

    .line 275
    iget-object v3, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;

    const-wide/16 v4, 0x3e8

    invoke-virtual {v3, v4, v5}, Ljava/lang/Object;->wait(J)V

    .line 278
    iget-object v3, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->operation:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;

    iget-object v4, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->statusParameter:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyStatusParameter;

    invoke-virtual {v3, v4}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;->getStatus(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyStatusParameter;)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;

    move-result-object v3

    .line 279
    sget-object v4, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$10;->$SwitchMap$epson$print$rpcopy$Component$eremoteoperation$ERemoteOperation$ERemoteParam:[I

    invoke-virtual {v3}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->printer_state()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v5

    invoke-virtual {v5}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 291
    :pswitch_0
    iget-object v4, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;

    iget-object v4, v4, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;->resumeRequest:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyInvalidateResumeRequest;

    invoke-interface {v4}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyInvalidateResumeRequest;->getStopReason()Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    move-result-object v4

    invoke-virtual {v3}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->printer_state_reasons()Ljava/util/ArrayList;

    move-result-object v3

    invoke-static {v3}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->getPrinterStopReason(Ljava/util/ArrayList;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    move-result-object v3

    invoke-virtual {v4, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->equals(Ljava/lang/Object;)Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_1

    .line 295
    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void

    .line 286
    :pswitch_1
    :try_start_2
    iget-object v3, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;

    sget-object v4, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;->Processing:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    iput-object v4, v3, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;->taskProgress:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    .line 287
    new-array v3, v2, [Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;

    iget-object v4, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;

    aput-object v4, v3, v1

    invoke-direct {p0, v3}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->localPublishProgress([Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;)V

    .line 288
    sget-object v3, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$ResumeState;->ClearError:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$ResumeState;

    invoke-virtual {p0, v3}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->resumeNotify(Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$ResumeState;)V

    goto :goto_0

    .line 281
    :pswitch_2
    iget-object v3, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;

    sget-object v4, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;->Processing:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    iput-object v4, v3, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;->taskProgress:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    .line 282
    new-array v3, v2, [Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;

    iget-object v4, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;

    aput-object v4, v3, v1

    invoke-direct {p0, v3}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->localPublishProgress([Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;)V

    .line 283
    sget-object v3, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$ResumeState;->Cancel:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$ResumeState;

    invoke-virtual {p0, v3}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->resumeNotify(Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$ResumeState;)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    goto/16 :goto_4

    :catch_0
    move-exception v3

    .line 303
    :try_start_3
    invoke-virtual {v3}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 305
    :cond_2
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 307
    sget-object v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$10;->$SwitchMap$epson$print$rpcopy$Component$ecopycomponent$ECopyComponent$ICopyResumeRequest$ResumeState:[I

    iget-object v3, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;

    iget-object v3, v3, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;->resumeState:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$ResumeState;

    invoke-virtual {v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$ResumeState;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_1

    goto :goto_3

    .line 351
    :pswitch_3
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->operation:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;

    new-instance v1, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$5;

    invoke-direct {v1, p0}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$5;-><init>(Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;)V

    invoke-virtual {v0, v1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;->documentChanged(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyDocumentChangedParameter;)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;

    move-result-object v0

    .line 368
    invoke-virtual {v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;->success()Z

    move-result v0

    goto :goto_3

    .line 346
    :pswitch_4
    iput-boolean v2, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->cancelRequested:Z

    goto :goto_3

    .line 314
    :pswitch_5
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemotePrinter;

    invoke-direct {v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemotePrinter;-><init>()V

    .line 315
    iget-object v2, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->operation:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;

    invoke-virtual {v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;->getHostIP()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemotePrinter;->setHostIP(Ljava/lang/String;)V

    .line 316
    iget-object v2, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->operation:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;

    invoke-virtual {v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;->getRequestConnectionTimeout()I

    move-result v2

    invoke-virtual {v0, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemotePrinter;->setRequestConnectionTimeout(I)V

    .line 317
    new-instance v2, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$4;

    invoke-direct {v2, p0}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$4;-><init>(Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;)V

    invoke-virtual {v0, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemotePrinter;->clearError(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$IRemoteOperationParameter;)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;

    :goto_1
    const/4 v0, 0x6

    if-ge v1, v0, :cond_4

    .line 333
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->operation:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;

    iget-object v2, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->statusParameter:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyStatusParameter;

    invoke-virtual {v0, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;->getStatus(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyStatusParameter;)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;

    move-result-object v0

    .line 334
    invoke-virtual {v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->printer_state()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v0

    sget-object v2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->processing:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    goto :goto_3

    :cond_3
    const-wide/16 v2, 0x1388

    .line 338
    :try_start_4
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_2

    :catch_1
    move-exception v0

    .line 340
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 379
    :cond_4
    :goto_3
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;

    const/4 v1, 0x0

    iput-object v1, v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;->resumeState:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$ResumeState;

    .line 380
    iput-object v1, v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;->resumeRequest:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyInvalidateResumeRequest;

    return-void

    .line 305
    :goto_4
    :try_start_5
    monitor-exit v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    throw v1

    :pswitch_data_0
    .packed-switch 0x15
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method resumeNotify(Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$ResumeState;)V
    .locals 2

    .line 261
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;

    monitor-enter v0

    .line 262
    :try_start_0
    iget-object v1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;

    iput-object p1, v1, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;->resumeState:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$ResumeState;

    .line 263
    iget-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$Progress;

    invoke-virtual {p1}, Ljava/lang/Object;->notify()V

    .line 264
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public setClientID(Ljava/lang/String;)V
    .locals 0

    .line 203
    iput-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->clientID:Ljava/lang/String;

    return-void
.end method

.method public setOptionContext(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;)V
    .locals 0

    .line 208
    iput-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    return-void
.end method

.method public setRequestConnectionTimeout(I)V
    .locals 1

    .line 198
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->operation:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;

    invoke-virtual {v0, p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;->setRequestConnectionTimeout(I)V

    return-void
.end method

.method public setSystemSettings(Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;)V
    .locals 0

    .line 193
    iput-object p1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->systemSettings:Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;

    return-void
.end method

.method public start()Lepson/print/rpcopy/Component/ecopycomponent/ECopyComponent$ICopyCancelRequest;
    .locals 1

    const/4 v0, 0x0

    .line 180
    new-array v0, v0, [Ljava/lang/Void;

    invoke-super {p0, v0}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 181
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$1;

    invoke-direct {v0, p0}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$1;-><init>(Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;)V

    return-object v0
.end method

.method startCopy()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;
    .locals 13

    .line 385
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->copyMode:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$CopyMode;

    sget-object v1, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$CopyMode;->Copy:Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$CopyMode;

    if-ne v0, v1, :cond_0

    .line 386
    new-instance v0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$6;

    invoke-direct {v0, p0}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$6;-><init>(Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;)V

    .line 495
    iget-object v1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->RepeatLayout:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v1, v2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionItemOf(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v1

    .line 496
    invoke-virtual {v1}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v1

    iget-object v1, v1, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->param:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 497
    iget-object v2, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v3, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->RemoveBackground:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionItemOf(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v2

    .line 498
    invoke-virtual {v2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v2

    iget-object v9, v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->param:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 501
    iget-object v2, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v3, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XCutLine:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionItemOf(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v2

    .line 502
    invoke-virtual {v2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v2

    iget-object v10, v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->param:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 503
    iget-object v2, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v3, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XCutLineStyle:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionItemOf(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v2

    .line 504
    invoke-virtual {v2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v2

    iget-object v11, v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->param:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 505
    iget-object v2, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->optionContext:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v3, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XCutLineWeight:Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v2, v3}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionItemOf(Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v2

    .line 506
    invoke-virtual {v2}, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v2

    iget-object v12, v2, Lepson/print/rpcopy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->param:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-object v2, p0

    move-object v3, v0

    move-object v4, v1

    move-object v5, v9

    move-object v6, v10

    move-object v7, v11

    move-object v8, v12

    .line 508
    invoke-direct/range {v2 .. v8}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->getCopyParams(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyParameter;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/rpcopy/Component/ecopycomponent/RepeatCopyAnalyticsParams;

    move-result-object v2

    iput-object v2, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->mCopyParams:Lepson/print/rpcopy/Component/ecopycomponent/RepeatCopyAnalyticsParams;

    .line 512
    iget-object v2, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->operation:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;

    invoke-virtual/range {v2 .. v8}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;->copy(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyParameter;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyResult;

    move-result-object v0

    .line 514
    invoke-virtual {v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyResult;->success()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 515
    invoke-virtual {v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyResult;->job_token()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->jobToken:Ljava/lang/String;

    goto :goto_1

    .line 522
    :cond_0
    iget-object v0, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->operation:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;

    new-instance v1, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$7;

    invoke-direct {v1, p0}, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask$7;-><init>(Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;)V

    invoke-virtual {v0, v1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;->getStatus(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyStatusParameter;)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;

    move-result-object v0

    .line 540
    invoke-virtual {v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->success()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 541
    invoke-virtual {v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->job_tokens()Ljava/util/ArrayList;

    move-result-object v1

    .line 542
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 543
    iget-object v3, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->recoverJobToken:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 544
    iput-object v2, p0, Lepson/print/rpcopy/Component/ecopycomponent/RemoteCopyTask;->jobToken:Ljava/lang/String;

    goto :goto_0

    :cond_2
    :goto_1
    return-object v0
.end method

.class public Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;
.super Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;
.source "ERemoteCopy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ERemoteCopyOptionsResult"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult$ScaleOptions;
    }
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;


# direct methods
.method public constructor <init>(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;Lorg/json/JSONObject;)V
    .locals 0

    .line 69
    iput-object p1, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;->this$0:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;

    .line 70
    invoke-direct {p0, p1, p2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;-><init>(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation;Lorg/json/JSONObject;)V

    return-void
.end method


# virtual methods
.method public local_default(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 1

    .line 100
    sget-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->color_effects_type:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    if-ne p1, v0, :cond_0

    .line 101
    sget-object p1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->color:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    return-object p1

    .line 103
    :cond_0
    sget-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_x_bleed:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    if-ne p1, v0, :cond_1

    .line 104
    sget-object p1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->standard:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    return-object p1

    .line 106
    :cond_1
    sget-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    if-ne p1, v0, :cond_2

    .line 107
    sget-object p1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_autofit:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    return-object p1

    .line 109
    :cond_2
    sget-object p1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_null:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    return-object p1
.end method

.method public local_options(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ")",
            "Ljava/util/ArrayList<",
            "Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ">;"
        }
    .end annotation

    .line 82
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 83
    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->color_effects_type:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    if-ne p1, v1, :cond_0

    .line 84
    sget-object p1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->color:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 85
    sget-object p1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->monochrome_grayscale:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0

    .line 88
    :cond_0
    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_x_bleed:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    if-ne p1, v1, :cond_1

    .line 89
    iget-object p1, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;->request:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;

    iget-object p1, p1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->remoteParam:Ljava/util/HashMap;

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_size:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p1, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 90
    sget-object p1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->standard:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 91
    sget-object p1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->midium:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 92
    sget-object p1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->minimum:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0

    :cond_1
    return-object v0
.end method

.method public parameter_default(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 0

    .line 78
    invoke-virtual {p0, p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;->getDefaultValueOf(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object p1

    return-object p1
.end method

.method public parameter_options(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Ljava/util/ArrayList;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ")",
            "Ljava/util/ArrayList<",
            "Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ">;"
        }
    .end annotation

    .line 74
    invoke-virtual {p0, p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;->getOptionsValueOf(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Ljava/util/ArrayList;

    move-result-object p1

    return-object p1
.end method

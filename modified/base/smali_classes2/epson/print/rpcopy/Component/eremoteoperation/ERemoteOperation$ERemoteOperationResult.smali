.class public Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;
.super Ljava/lang/Object;
.source "ERemoteOperation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ERemoteOperationResult"
.end annotation


# instance fields
.field json:Lorg/json/JSONObject;

.field request:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;


# direct methods
.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 0

    .line 438
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 439
    iput-object p1, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;->json:Lorg/json/JSONObject;

    return-void
.end method

.method static parseResponse(Lepson/common/httpclient/IAHttpClient$HttpResponse;)Lorg/json/JSONObject;
    .locals 3

    .line 424
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 426
    :try_start_0
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 427
    invoke-virtual {p0}, Lepson/common/httpclient/IAHttpClient$HttpResponse;->getEntity()Ljava/io/ByteArrayOutputStream;

    move-result-object p0

    invoke-virtual {p0, v1}, Ljava/io/ByteArrayOutputStream;->writeTo(Ljava/io/OutputStream;)V

    const-string p0, "ERequest C"

    .line 429
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 431
    new-instance p0, Lorg/json/JSONObject;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    .line 433
    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V

    move-object p0, v0

    :goto_0
    return-object p0
.end method


# virtual methods
.method getBooleanValueOf(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Z
    .locals 1

    .line 467
    :try_start_0
    iget-object v0, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;->json:Lorg/json/JSONObject;

    iget-object p1, p1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->string:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 469
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method getDefaultValueOf(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 3

    .line 531
    sget-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_null:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 533
    :try_start_0
    iget-object v1, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;->json:Lorg/json/JSONObject;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object p1, p1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->string:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "-default"

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->stringOf(Ljava/lang/String;)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 535
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-object v0
.end method

.method getIntValueOf(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)I
    .locals 1

    .line 477
    :try_start_0
    iget-object v0, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;->json:Lorg/json/JSONObject;

    iget-object p1, p1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->string:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method getOptionsValueOf(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ")",
            "Ljava/util/ArrayList<",
            "Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ">;"
        }
    .end annotation

    .line 541
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 543
    :try_start_0
    iget-object v1, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;->json:Lorg/json/JSONObject;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object p1, p1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->string:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "-options"

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v1, ","

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    .line 544
    array-length v1, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, p1, v2

    .line 545
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->stringOf(Ljava/lang/String;)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v3

    .line 546
    sget-object v4, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_null:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    if-eq v3, v4, :cond_0

    .line 547
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :catch_0
    move-exception p1

    const-string v1, "OperationResult"

    .line 551
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getOptionsValueOf() Exception <"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ">"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 552
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :cond_1
    return-object v0
.end method

.method getParamValueOf(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 5

    .line 511
    sget-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_null:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 513
    :try_start_0
    iget-object v1, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;->json:Lorg/json/JSONObject;

    iget-object v2, p1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->string:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->stringOf(Ljava/lang/String;)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    if-nez p1, :cond_0

    const-string p1, "ERemoteOpe"

    const-string v2, "param null"

    .line 516
    invoke-static {p1, v2}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 518
    :cond_0
    iget-object p1, p1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->string:Ljava/lang/String;

    if-nez p1, :cond_1

    const-string p1, "param.string[==null]"

    :cond_1
    const-string v2, "ERemoteOpe"

    .line 522
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getString("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ")"

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v2, p1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 525
    :goto_0
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_1
    return-object v0
.end method

.method getParamsValueOf(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ")",
            "Ljava/util/ArrayList<",
            "Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ">;"
        }
    .end annotation

    .line 558
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 560
    :try_start_0
    invoke-virtual {p0, p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;->getStringValueOf(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Ljava/lang/String;

    move-result-object p1

    const-string v1, ","

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    .line 561
    array-length v1, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, p1, v2

    .line 562
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->stringOf(Ljava/lang/String;)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :catch_0
    move-exception p1

    .line 565
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :cond_0
    return-object v0
.end method

.method getStringValueOf(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Ljava/lang/String;
    .locals 2

    const-string v0, ""

    .line 490
    :try_start_0
    iget-object v1, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;->json:Lorg/json/JSONObject;

    iget-object p1, p1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->string:Ljava/lang/String;

    invoke-virtual {v1, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 492
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-object v0
.end method

.method getStringsValueOf(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ")",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 498
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 500
    :try_start_0
    iget-object v1, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;->json:Lorg/json/JSONObject;

    iget-object p1, p1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->string:Ljava/lang/String;

    invoke-virtual {v1, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v1, ","

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    .line 501
    array-length v1, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, p1, v2

    .line 502
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :catch_0
    move-exception p1

    .line 505
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :cond_0
    return-object v0
.end method

.method public isNull(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Z
    .locals 1

    .line 447
    iget-object v0, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;->json:Lorg/json/JSONObject;

    iget-object p1, p1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->string:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method public setRemoteRequestBuilder(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;)V
    .locals 0

    .line 451
    iput-object p1, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;->request:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;

    return-void
.end method

.method public success()Z
    .locals 2

    .line 457
    :try_start_0
    iget-object v0, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;->json:Lorg/json/JSONObject;

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->success:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    iget-object v1, v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->string:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 459
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 443
    iget-object v0, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;->json:Lorg/json/JSONObject;

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

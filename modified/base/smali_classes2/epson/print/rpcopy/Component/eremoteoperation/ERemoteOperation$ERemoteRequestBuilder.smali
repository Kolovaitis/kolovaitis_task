.class Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;
.super Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation;
.source "ERemoteOperation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ERemoteRequestBuilder"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder$VariantValueConverter;
    }
.end annotation


# instance fields
.field command:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;

.field messageBody:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lepson/common/httpclient/BasicNameValuePair;",
            ">;"
        }
    .end annotation
.end field

.field operation:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;

.field remoteParam:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field requestURI:Ljava/lang/StringBuffer;

.field timeout:I

.field version:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;


# direct methods
.method constructor <init>(Ljava/lang/String;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;I)V
    .locals 1

    .line 641
    invoke-direct {p0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation;-><init>()V

    .line 642
    iput-object p2, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->operation:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    .line 643
    iput-object p3, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->version:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    .line 644
    iput-object p4, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->command:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    .line 645
    iput p5, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->timeout:I

    .line 647
    new-instance p5, Ljava/util/ArrayList;

    invoke-direct {p5}, Ljava/util/ArrayList;-><init>()V

    iput-object p5, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->messageBody:Ljava/util/ArrayList;

    .line 649
    new-instance p5, Ljava/lang/StringBuffer;

    invoke-direct {p5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v0, "/"

    .line 650
    invoke-virtual {p5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 651
    sget-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;->remote_operation:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    iget-object v0, v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;->string:Ljava/lang/String;

    invoke-virtual {p5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v0, "/"

    .line 652
    invoke-virtual {p5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 653
    iget-object p3, p3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;->string:Ljava/lang/String;

    invoke-virtual {p5, p3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string p3, "/"

    .line 654
    invoke-virtual {p5, p3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 655
    iget-object p2, p2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;->string:Ljava/lang/String;

    invoke-virtual {p5, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string p2, "/"

    .line 656
    invoke-virtual {p5, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 657
    iget-object p2, p4, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;->string:Ljava/lang/String;

    invoke-virtual {p5, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 659
    new-instance p2, Ljava/lang/StringBuffer;

    sget-object p3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;->HTTP:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    iget-object p3, p3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;->string:Ljava/lang/String;

    .line 662
    invoke-virtual {p5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p4

    .line 659
    invoke-static {p3, p1, p4}, Lepson/common/IPAddressUtils;->buildURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    iput-object p2, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->requestURI:Ljava/lang/StringBuffer;

    .line 664
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->remoteParam:Ljava/util/HashMap;

    return-void
.end method

.method public static requestCombo(Ljava/lang/String;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;I)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;
    .locals 7

    .line 705
    new-instance v6, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;

    sget-object v2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;->combo:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;->v1:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    move-object v0, v6

    move-object v1, p0

    move-object v4, p1

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;-><init>(Ljava/lang/String;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;I)V

    return-object v6
.end method

.method public static requestCopy(Ljava/lang/String;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;I)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;
    .locals 7

    .line 694
    new-instance v6, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;

    sget-object v2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;->repeat_copy:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;->v1:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    move-object v0, v6

    move-object v1, p0

    move-object v4, p1

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;-><init>(Ljava/lang/String;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;I)V

    return-object v6
.end method

.method public static requestCopyPhoto(Ljava/lang/String;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;I)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;
    .locals 7

    .line 701
    new-instance v6, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;

    sget-object v2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;->copy_photo:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;->v1:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    move-object v0, v6

    move-object v1, p0

    move-object v4, p1

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;-><init>(Ljava/lang/String;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;I)V

    return-object v6
.end method

.method public static requestDevice(Ljava/lang/String;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;I)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;
    .locals 7

    .line 681
    new-instance v6, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;

    sget-object v2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;->device:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;->v1:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    move-object v0, v6

    move-object v1, p0

    move-object v4, p1

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;-><init>(Ljava/lang/String;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;I)V

    return-object v6
.end method

.method public static requestPrinter(Ljava/lang/String;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;I)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;
    .locals 7

    .line 685
    new-instance v6, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;

    sget-object v2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;->printer:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;->v1:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    move-object v0, v6

    move-object v1, p0

    move-object v4, p1

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;-><init>(Ljava/lang/String;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;I)V

    return-object v6
.end method

.method public static requestScanner(Ljava/lang/String;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;I)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;
    .locals 7

    .line 689
    new-instance v6, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;

    sget-object v2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;->scanner:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;->v1:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    move-object v0, v6

    move-object v1, p0

    move-object v4, p1

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;-><init>(Ljava/lang/String;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;I)V

    return-object v6
.end method


# virtual methods
.method public add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;I)V
    .locals 2

    .line 714
    iget-object v0, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->remoteParam:Ljava/util/HashMap;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, p2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 715
    iget-object v0, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->messageBody:Ljava/util/ArrayList;

    new-instance v1, Lepson/common/httpclient/BasicNameValuePair;

    iget-object p1, p1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->string:Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p2

    invoke-direct {v1, p1, p2}, Lepson/common/httpclient/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;ILepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder$VariantValueConverter;)V
    .locals 1

    .line 738
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 739
    invoke-interface {p3, p2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder$VariantValueConverter;->convertValue(I)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object p2

    if-eqz p2, :cond_0

    .line 741
    iget-object v0, p2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->string:Ljava/lang/String;

    .line 743
    :cond_0
    iget-object p2, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->remoteParam:Ljava/util/HashMap;

    invoke-virtual {p2, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 744
    iget-object p2, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->messageBody:Ljava/util/ArrayList;

    new-instance p3, Lepson/common/httpclient/BasicNameValuePair;

    iget-object p1, p1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->string:Ljava/lang/String;

    invoke-direct {p3, p1, v0}, Lepson/common/httpclient/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p2, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V
    .locals 2

    .line 748
    sget-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_null:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    if-ne p2, v0, :cond_0

    return-void

    .line 751
    :cond_0
    iget-object v0, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->remoteParam:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 752
    iget-object v0, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->messageBody:Ljava/util/ArrayList;

    new-instance v1, Lepson/common/httpclient/BasicNameValuePair;

    iget-object p1, p1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->string:Ljava/lang/String;

    iget-object p2, p2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->string:Ljava/lang/String;

    invoke-direct {v1, p1, p2}, Lepson/common/httpclient/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/lang/String;)V
    .locals 2

    .line 730
    sget-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_null:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    iget-object v0, v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->string:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 733
    :cond_0
    iget-object v0, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->remoteParam:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 734
    iget-object v0, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->messageBody:Ljava/util/ArrayList;

    new-instance v1, Lepson/common/httpclient/BasicNameValuePair;

    iget-object p1, p1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->string:Ljava/lang/String;

    invoke-direct {v1, p1, p2}, Lepson/common/httpclient/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            "Ljava/util/ArrayList<",
            "Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ">;)V"
        }
    .end annotation

    .line 756
    iget-object v0, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->remoteParam:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 757
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 758
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 759
    iget-object v1, v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->string:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, ","

    .line 760
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    :cond_0
    const/16 p2, 0x2c

    .line 762
    invoke-virtual {p0, v0, p2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->deleteLastCharIf(Ljava/lang/StringBuffer;C)V

    .line 763
    iget-object p2, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->messageBody:Ljava/util/ArrayList;

    new-instance v1, Lepson/common/httpclient/BasicNameValuePair;

    iget-object p1, p1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->string:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, p1, v0}, Lepson/common/httpclient/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 767
    iget-object v0, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->remoteParam:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 768
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 769
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 770
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, ","

    .line 771
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    :cond_0
    const/16 p2, 0x2c

    .line 773
    invoke-virtual {p0, v0, p2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->deleteLastCharIf(Ljava/lang/StringBuffer;C)V

    .line 774
    iget-object p2, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->messageBody:Ljava/util/ArrayList;

    new-instance v1, Lepson/common/httpclient/BasicNameValuePair;

    iget-object p1, p1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->string:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, p1, v0}, Lepson/common/httpclient/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Z)V
    .locals 2

    .line 709
    iget-object v0, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->remoteParam:Ljava/util/HashMap;

    new-instance v1, Ljava/lang/Boolean;

    invoke-direct {v1, p2}, Ljava/lang/Boolean;-><init>(Z)V

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 710
    iget-object v0, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->messageBody:Ljava/util/ArrayList;

    new-instance v1, Lepson/common/httpclient/BasicNameValuePair;

    iget-object p1, p1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->string:Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object p2

    invoke-direct {v1, p1, p2}, Lepson/common/httpclient/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;[I)V
    .locals 4

    .line 719
    iget-object v0, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->remoteParam:Ljava/util/HashMap;

    const/4 v1, 0x1

    new-array v1, v1, [[I

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 720
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 721
    array-length v1, p2

    :goto_0
    if-ge v2, v1, :cond_0

    aget v3, p2, v2

    .line 722
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    const-string v3, ","

    .line 723
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    const/16 p2, 0x2c

    .line 725
    invoke-virtual {p0, v0, p2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->deleteLastCharIf(Ljava/lang/StringBuffer;C)V

    .line 726
    iget-object p2, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->messageBody:Ljava/util/ArrayList;

    new-instance v1, Lepson/common/httpclient/BasicNameValuePair;

    iget-object p1, p1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->string:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, p1, v0}, Lepson/common/httpclient/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method deleteLastCharIf(Ljava/lang/StringBuffer;C)V
    .locals 2

    .line 668
    invoke-virtual {p1}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 669
    invoke-virtual {p1}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 670
    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v1

    if-ne v1, p2, :cond_0

    .line 671
    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    :cond_0
    return-void
.end method

.method public getRemoteRequest()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;
    .locals 4

    .line 778
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;

    iget-object v1, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->requestURI:Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->messageBody:Ljava/util/ArrayList;

    iget v3, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->timeout:I

    invoke-direct {v0, p0, v1, v2, v3}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;-><init>(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation;Ljava/lang/String;Ljava/util/ArrayList;I)V

    return-object v0
.end method

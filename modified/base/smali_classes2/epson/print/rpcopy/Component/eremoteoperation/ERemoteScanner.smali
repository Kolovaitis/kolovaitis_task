.class public Lepson/print/rpcopy/Component/eremoteoperation/ERemoteScanner;
.super Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation;
.source "ERemoteScanner.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/rpcopy/Component/eremoteoperation/ERemoteScanner$ERemoteScannerStatusResult;,
        Lepson/print/rpcopy/Component/eremoteoperation/ERemoteScanner$ERemoteScannerComponentsResult;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation;-><init>()V

    return-void
.end method


# virtual methods
.method public clearError(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$IRemoteOperationParameter;)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;
    .locals 3

    .line 54
    iget-object v0, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteScanner;->hostIP:Ljava/lang/String;

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;->clear_error:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    invoke-virtual {p0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteScanner;->getRequestConnectionTimeout()I

    move-result v2

    invoke-static {v0, v1, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->requestScanner(Ljava/lang/String;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;I)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;

    move-result-object v0

    .line 55
    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->client_id:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$IRemoteOperationParameter;->client_id()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/lang/String;)V

    .line 56
    new-instance p1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;

    invoke-virtual {v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->getRemoteRequest()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;

    move-result-object v1

    invoke-virtual {v1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;->execute()Lorg/json/JSONObject;

    move-result-object v1

    invoke-direct {p1, v1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;-><init>(Lorg/json/JSONObject;)V

    .line 57
    invoke-virtual {p1, v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;->setRemoteRequestBuilder(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;)V

    return-object p1
.end method

.method public getComponents(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$IRemoteOperationParameter;)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteScanner$ERemoteScannerComponentsResult;
    .locals 3

    .line 38
    iget-object v0, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteScanner;->hostIP:Ljava/lang/String;

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;->get_components:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    invoke-virtual {p0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteScanner;->getRequestConnectionTimeout()I

    move-result v2

    invoke-static {v0, v1, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->requestScanner(Ljava/lang/String;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;I)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;

    move-result-object v0

    .line 39
    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->client_id:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$IRemoteOperationParameter;->client_id()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/lang/String;)V

    .line 40
    new-instance p1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteScanner$ERemoteScannerComponentsResult;

    invoke-virtual {v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->getRemoteRequest()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;

    move-result-object v1

    invoke-virtual {v1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;->execute()Lorg/json/JSONObject;

    move-result-object v1

    invoke-direct {p1, p0, v1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteScanner$ERemoteScannerComponentsResult;-><init>(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteScanner;Lorg/json/JSONObject;)V

    .line 41
    invoke-virtual {p1, v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteScanner$ERemoteScannerComponentsResult;->setRemoteRequestBuilder(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;)V

    return-object p1
.end method

.method public getStatus(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$IRemoteStatusParameter;)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteScanner$ERemoteScannerStatusResult;
    .locals 3

    .line 46
    iget-object v0, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteScanner;->hostIP:Ljava/lang/String;

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;->get_status:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    invoke-virtual {p0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteScanner;->getRequestConnectionTimeout()I

    move-result v2

    invoke-static {v0, v1, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->requestScanner(Ljava/lang/String;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;I)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;

    move-result-object v0

    .line 47
    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->keys:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$IRemoteStatusParameter;->keys()Ljava/util/ArrayList;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/util/ArrayList;)V

    .line 48
    new-instance p1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteScanner$ERemoteScannerStatusResult;

    invoke-virtual {v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->getRemoteRequest()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;

    move-result-object v1

    invoke-virtual {v1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;->execute()Lorg/json/JSONObject;

    move-result-object v1

    invoke-direct {p1, p0, v1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteScanner$ERemoteScannerStatusResult;-><init>(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteScanner;Lorg/json/JSONObject;)V

    .line 49
    invoke-virtual {p1, v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteScanner$ERemoteScannerStatusResult;->setRemoteRequestBuilder(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;)V

    return-object p1
.end method

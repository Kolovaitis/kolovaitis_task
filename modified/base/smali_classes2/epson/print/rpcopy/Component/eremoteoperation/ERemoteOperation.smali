.class public Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation;
.super Ljava/lang/Object;
.source "ERemoteOperation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;,
        Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;,
        Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;,
        Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;,
        Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$IRemoteStatusParameter;,
        Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$IRemoteOperationParameter;,
        Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;,
        Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    }
.end annotation


# instance fields
.field hostIP:Ljava/lang/String;

.field requestConnectionTimeout:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x2710

    .line 23
    iput v0, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation;->requestConnectionTimeout:I

    return-void
.end method


# virtual methods
.method public getHostIP()Ljava/lang/String;
    .locals 1

    .line 29
    iget-object v0, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation;->hostIP:Ljava/lang/String;

    return-object v0
.end method

.method public getRequestConnectionTimeout()I
    .locals 1

    .line 37
    iget v0, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation;->requestConnectionTimeout:I

    return v0
.end method

.method public setHostIP(Ljava/lang/String;)V
    .locals 1

    .line 25
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation;->hostIP:Ljava/lang/String;

    return-void
.end method

.method public setRequestConnectionTimeout(I)V
    .locals 0

    .line 33
    iput p1, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation;->requestConnectionTimeout:I

    return-void
.end method

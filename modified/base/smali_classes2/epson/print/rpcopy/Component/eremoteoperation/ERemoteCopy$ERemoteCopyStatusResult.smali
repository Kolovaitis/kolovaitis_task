.class public Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;
.super Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;
.source "ERemoteCopy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ERemoteCopyStatusResult"
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;


# direct methods
.method public constructor <init>(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;Lorg/json/JSONObject;)V
    .locals 0

    .line 206
    iput-object p1, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->this$0:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;

    .line 207
    invoke-direct {p0, p1, p2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;-><init>(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation;Lorg/json/JSONObject;)V

    return-void
.end method


# virtual methods
.method public document_on_adf()Z
    .locals 1

    .line 223
    sget-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->document_on_adf:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p0, v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->getBooleanValueOf(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Z

    move-result v0

    return v0
.end method

.method public job_print_current_pages()I
    .locals 1

    .line 251
    sget-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->job_print_current_pages:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p0, v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->getIntValueOf(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)I

    move-result v0

    return v0
.end method

.method public job_print_total_pages()I
    .locals 1

    .line 247
    sget-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->job_print_total_pages:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p0, v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->getIntValueOf(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)I

    move-result v0

    return v0
.end method

.method public job_result()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 1

    .line 239
    sget-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->job_result:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p0, v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->getParamValueOf(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v0

    return-object v0
.end method

.method public job_state()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 1

    .line 235
    sget-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->job_state:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p0, v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->getParamValueOf(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v0

    return-object v0
.end method

.method public job_tokens()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 243
    sget-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->job_tokens:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p0, v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->getStringsValueOf(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public print_x_disc_tray_state()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 1

    .line 211
    sget-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_x_disc_tray_state:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p0, v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->getParamValueOf(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v0

    return-object v0
.end method

.method public printer_state()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 1

    .line 215
    sget-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->printer_state:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p0, v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->getParamValueOf(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v0

    return-object v0
.end method

.method public printer_state_reasons()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ">;"
        }
    .end annotation

    .line 219
    sget-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->printer_state_reasons:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p0, v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->getParamsValueOf(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public scanner_state()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 1

    .line 227
    sget-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scanner_state:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p0, v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->getParamValueOf(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v0

    return-object v0
.end method

.method public scanner_state_reasons()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ">;"
        }
    .end annotation

    .line 231
    sget-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scanner_state_reasons:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p0, v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->getParamsValueOf(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

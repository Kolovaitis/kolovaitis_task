.class public Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;
.super Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation;
.source "ERemoteCopy.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;,
        Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyResult;,
        Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;,
        Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCancelParameter;,
        Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyStatusParameter;,
        Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyDocumentChangedParameter;,
        Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyParameter;,
        Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyOptionsParameter;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation;-><init>()V

    return-void
.end method


# virtual methods
.method public cancel(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCancelParameter;)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;
    .locals 3

    .line 390
    iget-object v0, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;->hostIP:Ljava/lang/String;

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;->cancel:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    invoke-virtual {p0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;->getRequestConnectionTimeout()I

    move-result v2

    invoke-static {v0, v1, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->requestCopy(Ljava/lang/String;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;I)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;

    move-result-object v0

    .line 391
    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->client_id:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCancelParameter;->client_id()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/lang/String;)V

    .line 392
    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->job_token:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCancelParameter;->job_token()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/lang/String;)V

    .line 393
    new-instance p1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;

    invoke-virtual {v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->getRemoteRequest()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;

    move-result-object v1

    invoke-virtual {v1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;->execute()Lorg/json/JSONObject;

    move-result-object v1

    invoke-direct {p1, p0, v1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;-><init>(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation;Lorg/json/JSONObject;)V

    .line 394
    invoke-virtual {p1, v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;->setRemoteRequestBuilder(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;)V

    return-object p1
.end method

.method public copy(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyParameter;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyResult;
    .locals 3

    .line 263
    iget-object v0, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;->hostIP:Ljava/lang/String;

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;->copy:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    invoke-virtual {p0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;->getRequestConnectionTimeout()I

    move-result v2

    invoke-static {v0, v1, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->requestCopy(Ljava/lang/String;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;I)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;

    move-result-object v0

    .line 264
    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->client_id:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyParameter;->client_id()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/lang/String;)V

    .line 265
    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->layout:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1, p2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 267
    sget-object p2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_type:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyParameter;->print_media_type()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 268
    sget-object p2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_size:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyParameter;->print_media_size()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 269
    sget-object p2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_source:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyParameter;->print_media_source()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 270
    sget-object p2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_quality:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyParameter;->print_quality()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 271
    sget-object p2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_remove_background:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, p2, p3}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 274
    sget-object p2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->dash:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p4, p2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_0

    goto :goto_0

    .line 277
    :cond_0
    sget-object p2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_cut_line:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, p2, p4}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 278
    sget-object p2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_cut_line_style:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, p2, p5}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 279
    sget-object p2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_cut_line_weight:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, p2, p6}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 282
    :goto_0
    sget-object p2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->copies:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyParameter;->copies()I

    move-result p3

    invoke-virtual {v0, p2, p3}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;I)V

    .line 283
    sget-object p2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->color_effects_type:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyParameter;->color_effects_type()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object p3

    invoke-virtual {v0, p2, p3}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 284
    sget-object p2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_density:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyParameter;->x_density()I

    move-result p1

    invoke-virtual {v0, p2, p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;I)V

    .line 287
    invoke-virtual {v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->getRemoteRequest()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;

    move-result-object p1

    .line 289
    new-instance p2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyResult;

    invoke-virtual {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;->execute()Lorg/json/JSONObject;

    move-result-object p1

    invoke-direct {p2, p0, p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyResult;-><init>(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;Lorg/json/JSONObject;)V

    return-object p2
.end method

.method public documentChanged(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyDocumentChangedParameter;)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;
    .locals 3

    .line 399
    iget-object v0, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;->hostIP:Ljava/lang/String;

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;->document_changed:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    invoke-virtual {p0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;->getRequestConnectionTimeout()I

    move-result v2

    invoke-static {v0, v1, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->requestCopy(Ljava/lang/String;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;I)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;

    move-result-object v0

    .line 400
    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->client_id:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyDocumentChangedParameter;->client_id()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/lang/String;)V

    .line 401
    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->job_token:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyDocumentChangedParameter;->job_token()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/lang/String;)V

    .line 402
    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->next_document:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyDocumentChangedParameter;->next_document()Z

    move-result p1

    invoke-virtual {v0, v1, p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Z)V

    .line 403
    new-instance p1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;

    invoke-virtual {v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->getRemoteRequest()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;

    move-result-object v1

    invoke-virtual {v1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;->execute()Lorg/json/JSONObject;

    move-result-object v1

    invoke-direct {p1, p0, v1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;-><init>(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation;Lorg/json/JSONObject;)V

    .line 404
    invoke-virtual {p1, v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;->setRemoteRequestBuilder(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;)V

    return-object p1
.end method

.method public getOptions(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyOptionsParameter;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;
    .locals 3

    .line 300
    iget-object v0, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;->hostIP:Ljava/lang/String;

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;->get_options:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    invoke-virtual {p0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;->getRequestConnectionTimeout()I

    move-result v2

    invoke-static {v0, v1, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->requestCopy(Ljava/lang/String;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;I)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;

    move-result-object v0

    .line 301
    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->client_id:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyOptionsParameter;->client_id()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/lang/String;)V

    .line 302
    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->layout:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyOptionsParameter;->layout()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 303
    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_type:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyOptionsParameter;->print_media_type()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 304
    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_size:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyOptionsParameter;->print_media_size()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 305
    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_source:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyOptionsParameter;->print_media_source()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 306
    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_quality:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyOptionsParameter;->print_quality()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 307
    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_remove_background:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1, p2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 308
    sget-object p2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->color_effects_type:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, p2, p3}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 311
    sget-object p2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_cut_line:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, p2, p4}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 312
    sget-object p2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_cut_line_style:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, p2, p5}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 313
    sget-object p2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_cut_line_weight:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, p2, p6}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 315
    sget-object p2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->fixed_parameters:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyOptionsParameter;->fixed_parameters()Ljava/util/ArrayList;

    move-result-object p3

    invoke-virtual {v0, p2, p3}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/util/ArrayList;)V

    .line 316
    sget-object p2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->preferred_parameters:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyOptionsParameter;->priority_order()Ljava/util/ArrayList;

    move-result-object p3

    invoke-virtual {v0, p2, p3}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/util/ArrayList;)V

    .line 317
    sget-object p2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->priority_order:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyOptionsParameter;->priority_order()Ljava/util/ArrayList;

    move-result-object p3

    invoke-virtual {v0, p2, p3}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/util/ArrayList;)V

    .line 318
    sget-object p2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->default_as_fixed:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyOptionsParameter;->default_as_fixed()Z

    move-result p1

    invoke-virtual {v0, p2, p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Z)V

    .line 319
    new-instance p1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;

    invoke-virtual {v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->getRemoteRequest()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;

    move-result-object p2

    invoke-virtual {p2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;->execute()Lorg/json/JSONObject;

    move-result-object p2

    invoke-direct {p1, p0, p2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;-><init>(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;Lorg/json/JSONObject;)V

    .line 320
    invoke-virtual {p1, v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;->setRemoteRequestBuilder(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;)V

    return-object p1
.end method

.method public getSelectableOptions()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;
    .locals 4

    .line 330
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 352
    iget-object v1, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;->hostIP:Ljava/lang/String;

    sget-object v2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;->get_options:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    invoke-virtual {p0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;->getRequestConnectionTimeout()I

    move-result v3

    invoke-static {v1, v2, v3}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->requestCopy(Ljava/lang/String;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;I)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;

    move-result-object v1

    .line 353
    sget-object v2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->client_id:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_null:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    iget-object v3, v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->string:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/lang/String;)V

    .line 354
    sget-object v2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->color_effects_type:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_null:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v1, v2, v3}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 355
    sget-object v2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->layout:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_null:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v1, v2, v3}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 356
    sget-object v2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_size:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_null:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v1, v2, v3}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 357
    sget-object v2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_type:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_null:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v1, v2, v3}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 358
    sget-object v2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_source:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_null:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v1, v2, v3}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 359
    sget-object v2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_quality:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_null:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v1, v2, v3}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 361
    sget-object v2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_remove_background:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_null:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v1, v2, v3}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 364
    sget-object v2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_cut_line:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_null:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v1, v2, v3}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 365
    sget-object v2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_cut_line_style:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_null:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v1, v2, v3}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 366
    sget-object v2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_cut_line_weight:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_null:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v1, v2, v3}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 368
    sget-object v2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->fixed_parameters:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v1, v2, v3}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/util/ArrayList;)V

    .line 369
    sget-object v2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->preferred_parameters:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v1, v2, v3}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/util/ArrayList;)V

    .line 371
    sget-object v2, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->priority_order:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v1, v2, v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/util/ArrayList;)V

    .line 372
    sget-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->default_as_fixed:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Z)V

    .line 374
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;

    invoke-virtual {v1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->getRemoteRequest()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;

    move-result-object v2

    invoke-virtual {v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;->execute()Lorg/json/JSONObject;

    move-result-object v2

    invoke-direct {v0, p0, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;-><init>(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;Lorg/json/JSONObject;)V

    .line 375
    invoke-virtual {v0, v1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;->setRemoteRequestBuilder(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;)V

    return-object v0
.end method

.method public getStatus(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyStatusParameter;)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;
    .locals 3

    .line 380
    iget-object v0, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;->hostIP:Ljava/lang/String;

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;->get_status:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    invoke-virtual {p0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;->getRequestConnectionTimeout()I

    move-result v2

    invoke-static {v0, v1, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->requestCopy(Ljava/lang/String;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;I)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;

    move-result-object v0

    .line 381
    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->client_id:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyStatusParameter;->client_id()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/lang/String;)V

    .line 382
    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->keys:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyStatusParameter;->keys()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/util/ArrayList;)V

    .line 383
    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->job_token:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyStatusParameter;->job_token()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/lang/String;)V

    .line 384
    new-instance p1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;

    invoke-virtual {v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->getRemoteRequest()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;

    move-result-object v1

    invoke-virtual {v1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;->execute()Lorg/json/JSONObject;

    move-result-object v1

    invoke-direct {p1, p0, v1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;-><init>(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy;Lorg/json/JSONObject;)V

    .line 385
    invoke-virtual {p1, v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->setRemoteRequestBuilder(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;)V

    return-object p1
.end method

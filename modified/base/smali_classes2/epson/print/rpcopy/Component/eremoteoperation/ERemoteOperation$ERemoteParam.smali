.class public final enum Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
.super Ljava/lang/Enum;
.source "ERemoteOperation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ERemoteParam"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum Long:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum _16_up:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum _3_up:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum _4_up:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum _9_up:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum adf:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum already_finished:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum auto:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum auto_repeat:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum best:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum book_two_sided:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum borderless:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum bottom:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum busy:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum canceled:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum canceling:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum client_id:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum collated:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum color:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum color_effects_type:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum communication_error:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum components:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum continuous:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum copies:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum copy:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum copy_magnification:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum copy_photo:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum copying:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum cover_open:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum cover_open_error:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum custom_4psize_254x305mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum custom_cardsize_55x91mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum custom_creditcardsize_54x86mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum custom_epson_18_120x176mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum custom_epson_1A_98x148mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum custom_epson_indianlegal_215x345mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum custom_hivision_101_6x180_6mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum custom_media_type_epson_02:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum custom_media_type_epson_19:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum custom_media_type_epson_1b:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum custom_media_type_epson_20:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum custom_media_type_epson_22:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum custom_media_type_epson_2a:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum custom_media_type_epson_39:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum custom_media_type_epson_44:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum custom_media_type_epson_46:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum custom_media_type_epson_47:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum dash:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum default_as_fixed:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum device:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum disc_tray:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum document_error:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum document_on_adf:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum dot:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum draft:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum envelope:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum finished:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum fixed_parameters:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum four_repeat:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum frame:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum functions:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum high:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum idle:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum illegal_combination:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum illegal_state:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum image_too_large:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum input_tray_missing_error:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum iso_a3_297x420mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum iso_a4_210x297mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum iso_a5_148x210mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum iso_a6_105x148mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum iso_c4_229x324mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum iso_c6_114x162mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum iso_dl_110x220mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum jis_b4_257x364mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum jis_b5_182x257mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum jis_b6_128x182mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum job_image_post_url:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum job_print_current_pages:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum job_print_total_pages:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum job_result:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum job_state:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum job_token:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum job_tokens:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum jpn_chou3_120x235mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum jpn_chou4_90x205mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum jpn_hagaki_100x148mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum jpn_kaku2_240x332mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum jpn_you4_105x235mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum keys:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum labels:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum landscape:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum layout:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum longer:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum lower_half:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum manual:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum marker_supply_empty_error:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum marker_supply_low_warning:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum marker_waste_full_error:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum media_empty:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum media_empty_error:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum media_jam_error:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum media_size_missmatch_error:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum medium:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum memory_full:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum midium:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum minimum:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum mirror:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum mixed:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum monochrome_grayscale:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum na_5x7_5x7in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum na_edp_11x14in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum na_executive_7_25x10_5in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum na_foolscap_8_5x13in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum na_govt_letter_8x10in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum na_index_4x6_4x6in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum na_invoice_5_5x8_5in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum na_ledger_11x17in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum na_legal_8_5x14in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum na_letter_8_5x11in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum na_number_10_4_125x9_5in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum na_oficio_8_5x13_4in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum next_document:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum nextpaper:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum none:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum normal:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum oe_photo_l_3_5x5in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum off:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum om_16k_195x270mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum om_8k_270x390mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum om_folio_sp_215x315mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum on:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum one_sided:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum orientation:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum other_error:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum output_area_full_error:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum oval:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum oval_soft_edge:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum pending:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum pending_for_template:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum photographic:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum photographic_glossy:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum photographic_high_gloss:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum photographic_matte:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum photographic_semi_gloss:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum portrait:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum preferred_parameters:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum print_media_size:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum print_media_source:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum print_media_type:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum print_quality:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum print_sheet_collate:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum print_sides:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum print_x_auto_pg:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum print_x_bleed:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum print_x_disc_tray_state:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum print_x_dry_time:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum printer:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum printer_state:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum printer_state_reasons:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum printing:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum printing_template:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum priority_order:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum processing:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum rear:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum reason:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum remove_adf_paper:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum repeat_copy:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum scan_area_height:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum scan_area_resolution:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum scan_area_width:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum scan_area_x:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum scan_area_y:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum scan_content_type:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum scan_count:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum scan_media_size:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum scan_sides:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum scanner:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum scanner_state:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum scanner_state_reasons:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum scanning:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum set_adf_paper:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum standard:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum stationery:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum stationery_coated:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum stationery_heavyweight:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum stationery_inkjet:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum stationery_letterhead:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum stopped:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum success:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum text:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum thick:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum thin:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum top:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum tray_missing:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum tray_set:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum two_repeat:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum two_sided_long_edge:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum two_sided_short_edge:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum two_up:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum two_up_book:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum uncollated:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum unknown_token:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum unused:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum upper_half:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum waiting_image:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum waiting_image_for_template:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum waiting_template:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum x_apf:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum x_borderless:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum x_color_restoration:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum x_cut_line:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum x_cut_line_style:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum x_cut_line_weight:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum x_density:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum x_failed_communication:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum x_fit_gamma:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum x_fit_matrix:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum x_invalid_photo_setting:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum x_null:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum x_remove_background:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum x_scale:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum x_scale_2l_to_a4:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum x_scale_2l_to_kg:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum x_scale_2l_to_letter:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum x_scale_2l_to_postcard:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum x_scale_8x10_to_2l:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum x_scale_a4_to_2l:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum x_scale_a4_to_a3:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum x_scale_a4_to_a5:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum x_scale_a4_to_b5:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum x_scale_a4_to_kg:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum x_scale_a4_to_postcard:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum x_scale_a5_to_a4:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum x_scale_autofit:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum x_scale_b5_to_a4:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum x_scale_custom:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum x_scale_fullsize:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum x_scale_kg_to_2l:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum x_scale_kg_to_8x10:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum x_scale_kg_to_a4:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum x_scale_kg_to_letter:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum x_scale_l_to_2l:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum x_scale_l_to_a4:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum x_scale_l_to_postcard:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum x_scale_legal_to_letter:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum x_scale_letter_to_11x17:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum x_scale_letter_to_2l:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum x_scale_letter_to_kg:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum x_scale_postcard_to_a4:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum x_two_sided:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

.field public static final enum x_unknown:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;


# instance fields
.field public string:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 41
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "client_id"

    const-string v2, "client-id"

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->client_id:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 42
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "success"

    const-string v2, "success"

    const/4 v4, 0x1

    invoke-direct {v0, v1, v4, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->success:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 43
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "reason"

    const-string v2, "reason"

    const/4 v5, 0x2

    invoke-direct {v0, v1, v5, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->reason:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 44
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "illegal_combination"

    const-string v2, "illegal_combination"

    const/4 v6, 0x3

    invoke-direct {v0, v1, v6, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->illegal_combination:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 45
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "illegal_state"

    const-string v2, "illegal_state"

    const/4 v7, 0x4

    invoke-direct {v0, v1, v7, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->illegal_state:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 46
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "already_finished"

    const-string v2, "already_finished"

    const/4 v8, 0x5

    invoke-direct {v0, v1, v8, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->already_finished:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 47
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "unknown_token"

    const-string v2, "unknown_token"

    const/4 v9, 0x6

    invoke-direct {v0, v1, v9, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->unknown_token:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 48
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "busy"

    const-string v2, "busy"

    const/4 v10, 0x7

    invoke-direct {v0, v1, v10, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->busy:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 50
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "job_token"

    const-string v2, "job-token"

    const/16 v11, 0x8

    invoke-direct {v0, v1, v11, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->job_token:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 51
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "next_document"

    const-string v2, "next-document"

    const/16 v12, 0x9

    invoke-direct {v0, v1, v12, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->next_document:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 53
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "functions"

    const-string v2, "functions"

    const/16 v13, 0xa

    invoke-direct {v0, v1, v13, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->functions:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 54
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "keys"

    const-string v2, "keys"

    const/16 v14, 0xb

    invoke-direct {v0, v1, v14, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->keys:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 56
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "device"

    const-string v2, "device"

    const/16 v15, 0xc

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->device:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 57
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "printer"

    const-string v2, "printer"

    const/16 v15, 0xd

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->printer:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 58
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "scanner"

    const-string v2, "scanner"

    const/16 v15, 0xe

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scanner:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 59
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "copy"

    const-string v2, "copy"

    const/16 v15, 0xf

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->copy:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 60
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "copy_photo"

    const-string v2, "copy_photo"

    const/16 v15, 0x10

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->copy_photo:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 61
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "repeat_copy"

    const-string v2, "repeat_copy"

    const/16 v15, 0x11

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->repeat_copy:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 63
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "print_x_disc_tray_state"

    const-string v2, "print-x-disc-tray-state"

    const/16 v15, 0x12

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_x_disc_tray_state:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 64
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "media_empty"

    const-string v2, "media-empty"

    const/16 v15, 0x13

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->media_empty:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 65
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "tray_set"

    const-string v2, "tray-set"

    const/16 v15, 0x14

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->tray_set:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 66
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "tray_missing"

    const-string v2, "tray-missing"

    const/16 v15, 0x15

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->tray_missing:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 69
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "x_remove_background"

    const-string v2, "x-remove-background"

    const/16 v15, 0x16

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_remove_background:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 72
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "x_cut_line"

    const-string v2, "x-cut-line"

    const/16 v15, 0x17

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_cut_line:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 73
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "x_cut_line_style"

    const-string v2, "x-cut-line-style"

    const/16 v15, 0x18

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_cut_line_style:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 74
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "dash"

    const-string v2, "-"

    const/16 v15, 0x19

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->dash:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 75
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "dot"

    const-string v2, "dot"

    const/16 v15, 0x1a

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->dot:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 76
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "continuous"

    const-string v2, "continuous"

    const/16 v15, 0x1b

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->continuous:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 77
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "x_cut_line_weight"

    const-string v2, "x-cut-line-weight"

    const/16 v15, 0x1c

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_cut_line_weight:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 78
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "thin"

    const-string v2, "thin"

    const/16 v15, 0x1d

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->thin:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 79
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "medium"

    const-string v2, "medium"

    const/16 v15, 0x1e

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->medium:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 80
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "thick"

    const-string v2, "thick"

    const/16 v15, 0x1f

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->thick:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 82
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "printer_state"

    const-string v2, "printer-state"

    const/16 v15, 0x20

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->printer_state:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 83
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "scanner_state"

    const-string v2, "scanner-state"

    const/16 v15, 0x21

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scanner_state:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 84
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "idle"

    const-string v2, "idle"

    const/16 v15, 0x22

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->idle:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 85
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "processing"

    const-string v2, "processing"

    const/16 v15, 0x23

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->processing:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 86
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "stopped"

    const-string v2, "stopped"

    const/16 v15, 0x24

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->stopped:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 88
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "printer_state_reasons"

    const-string v2, "printer-state-reasons"

    const/16 v15, 0x25

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->printer_state_reasons:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 89
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "none"

    const-string v2, "none"

    const/16 v15, 0x26

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->none:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 90
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "marker_supply_empty_error"

    const-string v2, "marker-supply-empty-error"

    const/16 v15, 0x27

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->marker_supply_empty_error:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 91
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "marker_waste_full_error"

    const-string v2, "marker-waste-full-error"

    const/16 v15, 0x28

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->marker_waste_full_error:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 92
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "media_jam_error"

    const-string v2, "media-jam-error"

    const/16 v15, 0x29

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->media_jam_error:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 93
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "media_empty_error"

    const-string v2, "media-empty-error"

    const/16 v15, 0x2a

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->media_empty_error:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 94
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "input_tray_missing_error"

    const-string v2, "input-tray-missing-error"

    const/16 v15, 0x2b

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->input_tray_missing_error:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 95
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "cover_open_error"

    const-string v2, "cover-open-error"

    const/16 v15, 0x2c

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->cover_open_error:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 96
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "output_area_full_error"

    const-string v2, "output-area-full-error"

    const/16 v15, 0x2d

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->output_area_full_error:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 97
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "other_error"

    const-string v2, "other-error"

    const/16 v15, 0x2e

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->other_error:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 98
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "marker_supply_low_warning"

    const-string v2, "marker-supply-low-warning"

    const/16 v15, 0x2f

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->marker_supply_low_warning:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 99
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "cover_open"

    const-string v2, "cover-open"

    const/16 v15, 0x30

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->cover_open:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 101
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "scanner_state_reasons"

    const-string v2, "scanner-state-reasons"

    const/16 v15, 0x31

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scanner_state_reasons:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 105
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "media_size_missmatch_error"

    const-string v2, "media-size-missmatch-error"

    const/16 v15, 0x32

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->media_size_missmatch_error:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 108
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "document_on_adf"

    const-string v2, "document-on-adf"

    const/16 v15, 0x33

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->document_on_adf:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 110
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "components"

    const-string v2, "components"

    const/16 v15, 0x34

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->components:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 111
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "disc_tray"

    const-string v2, "disc-tray"

    const/16 v15, 0x35

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->disc_tray:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 112
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "adf"

    const-string v2, "adf"

    const/16 v15, 0x36

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->adf:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 114
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "layout"

    const-string v2, "layout"

    const/16 v15, 0x37

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->layout:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 115
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "standard"

    const-string v2, "standard"

    const/16 v15, 0x38

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->standard:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 116
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "borderless"

    const-string v2, "borderless"

    const/16 v15, 0x39

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->borderless:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 117
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "two_up"

    const-string v2, "2-up"

    const/16 v15, 0x3a

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->two_up:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 118
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "two_up_book"

    const-string v2, "2-up-book"

    const/16 v15, 0x3b

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->two_up_book:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 119
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "book_two_sided"

    const-string v2, "book-two-sided"

    const/16 v15, 0x3c

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->book_two_sided:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 120
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "mirror"

    const-string v2, "mirror"

    const/16 v15, 0x3d

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->mirror:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 121
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "upper_half"

    const-string v2, "upper-half"

    const/16 v15, 0x3e

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->upper_half:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 122
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "lower_half"

    const-string v2, "lower_half"

    const/16 v15, 0x3f

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->lower_half:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 123
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "_3_up"

    const-string v2, "3-up"

    const/16 v15, 0x40

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->_3_up:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 124
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "_4_up"

    const-string v2, "4-up"

    const/16 v15, 0x41

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->_4_up:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 125
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "_9_up"

    const-string v2, "9-up"

    const/16 v15, 0x42

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->_9_up:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 126
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "_16_up"

    const-string v2, "16-up"

    const/16 v15, 0x43

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->_16_up:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 128
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "two_repeat"

    const-string v2, "2-repeat"

    const/16 v15, 0x44

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->two_repeat:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 129
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "four_repeat"

    const-string v2, "4-repeat"

    const/16 v15, 0x45

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->four_repeat:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 130
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "auto_repeat"

    const-string v2, "auto-repeat"

    const/16 v15, 0x46

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->auto_repeat:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 132
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "scan_sides"

    const-string v2, "scan-sides"

    const/16 v15, 0x47

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scan_sides:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 133
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "print_sides"

    const-string v2, "print-sides"

    const/16 v15, 0x48

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_sides:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 134
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "one_sided"

    const-string v2, "one-sided"

    const/16 v15, 0x49

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->one_sided:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 135
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "two_sided_short_edge"

    const-string v2, "two-sided-short-edge"

    const/16 v15, 0x4a

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->two_sided_short_edge:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 136
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "two_sided_long_edge"

    const-string v2, "two-sided-long-edge"

    const/16 v15, 0x4b

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->two_sided_long_edge:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 137
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "x_two_sided"

    const-string v2, "x-two-sided"

    const/16 v15, 0x4c

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_two_sided:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 139
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "scan_media_size"

    const-string v2, "scan-media-size"

    const/16 v15, 0x4d

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scan_media_size:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 140
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "unused"

    const-string v2, "unused"

    const/16 v15, 0x4e

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->unused:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 141
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "iso_a4_210x297mm"

    const-string v2, "iso_a4_210x297mm"

    const/16 v15, 0x4f

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->iso_a4_210x297mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 142
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "jis_b5_182x257mm"

    const-string v2, "jis_b5_182x257mm"

    const/16 v15, 0x50

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->jis_b5_182x257mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 144
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "print_media_type"

    const-string v2, "print-media-type"

    const/16 v15, 0x51

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_type:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 145
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "stationery"

    const-string v2, "stationery"

    const/16 v15, 0x52

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->stationery:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 146
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "photographic_high_gloss"

    const-string v2, "photographic-high-gloss"

    const/16 v15, 0x53

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->photographic_high_gloss:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 147
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "photographic"

    const-string v2, "photographic"

    const/16 v15, 0x54

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->photographic:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 148
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "photographic_semi_gloss"

    const-string v2, "photographic-semi-gloss"

    const/16 v15, 0x55

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->photographic_semi_gloss:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 149
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "photographic_glossy"

    const-string v2, "photographic-glossy"

    const/16 v15, 0x56

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->photographic_glossy:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 150
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "custom_media_type_epson_44"

    const-string v2, "custom-media-type-epson-44"

    const/16 v15, 0x57

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_media_type_epson_44:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 151
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "photographic_matte"

    const-string v2, "photographic-matte"

    const/16 v15, 0x58

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->photographic_matte:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 152
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "stationery_coated"

    const-string v2, "stationery-coated"

    const/16 v15, 0x59

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->stationery_coated:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 153
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "custom_media_type_epson_2a"

    const-string v2, "custom-media-type-epson-2a"

    const/16 v15, 0x5a

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_media_type_epson_2a:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 154
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "stationery_inkjet"

    const-string v2, "stationery-inkjet"

    const/16 v15, 0x5b

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->stationery_inkjet:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 155
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "custom_media_type_epson_1b"

    const-string v2, "custom-media-type-epson-1b"

    const/16 v15, 0x5c

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_media_type_epson_1b:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 156
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "custom_media_type_epson_02"

    const-string v2, "custom-media-type-epson-02"

    const/16 v15, 0x5d

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_media_type_epson_02:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 157
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "custom_media_type_epson_19"

    const-string v2, "custom-media-type-epson-19"

    const/16 v15, 0x5e

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_media_type_epson_19:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 158
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "custom_media_type_epson_22"

    const-string v2, "custom-media-type_epson-22"

    const/16 v15, 0x5f

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_media_type_epson_22:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 159
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "labels"

    const-string v2, "labels"

    const/16 v15, 0x60

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->labels:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 160
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "jis_b4_257x364mm"

    const-string v2, "jis_b4_257x364mm"

    const/16 v15, 0x61

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->jis_b4_257x364mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 161
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "envelope"

    const-string v2, "envelope"

    const/16 v15, 0x62

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->envelope:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 162
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "custom_media_type_epson_47"

    const-string v2, "custom-media-type-epson-47"

    const/16 v15, 0x63

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_media_type_epson_47:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 163
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "custom_media_type_epson_20"

    const-string v2, "custom-media-type-epson-20"

    const/16 v15, 0x64

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_media_type_epson_20:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 164
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "custom_media_type_epson_39"

    const-string v2, "custom-media-type-epson-39"

    const/16 v15, 0x65

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_media_type_epson_39:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 165
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "custom_media_type_epson_46"

    const-string v2, "custom-media-type-epson-46"

    const/16 v15, 0x66

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_media_type_epson_46:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 166
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "stationery_heavyweight"

    const-string v2, "stationery-heavyweight"

    const/16 v15, 0x67

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->stationery_heavyweight:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 167
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "stationery_letterhead"

    const-string v2, "stationery-letterhead"

    const/16 v15, 0x68

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->stationery_letterhead:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 169
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "print_media_size"

    const-string v2, "print-media-size"

    const/16 v15, 0x69

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_size:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 172
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "oe_photo_l_3_5x5in"

    const-string v2, "oe_photo-l_3.5x5in"

    const/16 v15, 0x6a

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->oe_photo_l_3_5x5in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 173
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "na_5x7_5x7in"

    const-string v2, "na_5x7_5x7in"

    const/16 v15, 0x6b

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_5x7_5x7in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 174
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "jpn_hagaki_100x148mm"

    const-string v2, "jpn_hagaki_100x148mm"

    const/16 v15, 0x6c

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->jpn_hagaki_100x148mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 175
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "na_index_4x6_4x6in"

    const-string v2, "na_index-4x6_4x6in"

    const/16 v15, 0x6d

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_index_4x6_4x6in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 176
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "na_govt_letter_8x10in"

    const-string v2, "na_govt-letter_8x10in"

    const/16 v15, 0x6e

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_govt_letter_8x10in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 177
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "na_letter_8_5x11in"

    const-string v2, "na_letter_8.5x11in"

    const/16 v15, 0x6f

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_letter_8_5x11in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 178
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "na_legal_8_5x14in"

    const-string v2, "na_legal_8.5x14in"

    const/16 v15, 0x70

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_legal_8_5x14in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 179
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "iso_a5_148x210mm"

    const-string v2, "iso_a5_148x210mm"

    const/16 v15, 0x71

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->iso_a5_148x210mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 180
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "custom_4psize_254x305mm"

    const-string v2, "custom_4psize_254x305mm"

    const/16 v15, 0x72

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_4psize_254x305mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 181
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "iso_a3_297x420mm"

    const-string v2, "iso_a3_297x420mm"

    const/16 v15, 0x73

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->iso_a3_297x420mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 182
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "na_ledger_11x17in"

    const-string v2, "na_ledger_11x17in"

    const/16 v15, 0x74

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_ledger_11x17in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 183
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "iso_a6_105x148mm"

    const-string v2, "iso_a6_105x148mm"

    const/16 v15, 0x75

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->iso_a6_105x148mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 184
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "jpn_chou3_120x235mm"

    const-string v2, "jpn_chou3_120x235mm"

    const/16 v15, 0x76

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->jpn_chou3_120x235mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 185
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "jpn_chou4_90x205mm"

    const-string v2, "jpn_chou4_90x205mm"

    const/16 v15, 0x77

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->jpn_chou4_90x205mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 186
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "custom_epson_18_120x176mm"

    const-string v2, "custom_epson-18_120x176mm"

    const/16 v15, 0x78

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_epson_18_120x176mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 187
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "custom_epson_1A_98x148mm"

    const-string v2, "custom_epson-1A_98x148mm"

    const/16 v15, 0x79

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_epson_1A_98x148mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 188
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "jpn_you4_105x235mm"

    const-string v2, "jpn_you4_105x235mm"

    const/16 v15, 0x7a

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->jpn_you4_105x235mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 189
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "iso_c6_114x162mm"

    const-string v2, "iso_c6_114x162mm"

    const/16 v15, 0x7b

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->iso_c6_114x162mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 190
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "jpn_kaku2_240x332mm"

    const-string v2, "jpn_kaku2_240x332mm"

    const/16 v15, 0x7c

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->jpn_kaku2_240x332mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 191
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "iso_c4_229x324mm"

    const-string v2, "iso_c4_229x324mm"

    const/16 v15, 0x7d

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->iso_c4_229x324mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 192
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "custom_cardsize_55x91mm"

    const-string v2, "custom_cardsize_55x91mm"

    const/16 v15, 0x7e

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_cardsize_55x91mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 193
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "custom_creditcardsize_54x86mm"

    const-string v2, "custom_creditcardsize_54x86mm"

    const/16 v15, 0x7f

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_creditcardsize_54x86mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 194
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "custom_hivision_101_6x180_6mm"

    const-string v2, "custom_hivision_101.6x180.6mm"

    const/16 v15, 0x80

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_hivision_101_6x180_6mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 195
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "iso_dl_110x220mm"

    const-string v2, "iso_dl_110x220mm"

    const/16 v15, 0x81

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->iso_dl_110x220mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 196
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "jis_b6_128x182mm"

    const-string v2, "jis_b6_128x182mm"

    const/16 v15, 0x82

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->jis_b6_128x182mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 197
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "na_executive_7_25x10_5in"

    const-string v2, "na_executive_7.25x10.5in"

    const/16 v15, 0x83

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_executive_7_25x10_5in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 198
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "na_foolscap_8_5x13in"

    const-string v2, "na_foolscap_8.5x13in"

    const/16 v15, 0x84

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_foolscap_8_5x13in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 199
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "na_edp_11x14in"

    const-string v2, "na_edp_11x14in"

    const/16 v15, 0x85

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_edp_11x14in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 200
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "na_number_10_4_125x9_5in"

    const-string v2, "na_number-10_4.125x9.5in"

    const/16 v15, 0x86

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_number_10_4_125x9_5in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 201
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "om_8k_270x390mm"

    const-string v2, "om_8k_270x390mm"

    const/16 v15, 0x87

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->om_8k_270x390mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 202
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "om_16k_195x270mm"

    const-string v2, "om_16k_195x270mm"

    const/16 v15, 0x88

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->om_16k_195x270mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 203
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "na_invoice_5_5x8_5in"

    const-string v2, "na_invoice_5.5x8.5in"

    const/16 v15, 0x89

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_invoice_5_5x8_5in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 204
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "custom_epson_indianlegal_215x345mm"

    const-string v2, "custom_epson-indianlegal_215x345mm"

    const/16 v15, 0x8a

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_epson_indianlegal_215x345mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 205
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "na_oficio_8_5x13_4in"

    const-string v2, "na_oficio_8.5x13.4in"

    const/16 v15, 0x8b

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_oficio_8_5x13_4in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 206
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "om_folio_sp_215x315mm"

    const-string v2, "om_folio-sp_215x315mm"

    const/16 v15, 0x8c

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->om_folio_sp_215x315mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 208
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "scan_content_type"

    const-string v2, "scan-content-type"

    const/16 v15, 0x8d

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scan_content_type:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 209
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "text"

    const-string v2, "text"

    const/16 v15, 0x8e

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->text:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 210
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "mixed"

    const-string v2, "mixed"

    const/16 v15, 0x8f

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->mixed:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 213
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "print_media_source"

    const-string v2, "print-media-source"

    const/16 v15, 0x90

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_source:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 214
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "top"

    const-string v2, "top"

    const/16 v15, 0x91

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->top:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 215
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "bottom"

    const-string v2, "bottom"

    const/16 v15, 0x92

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->bottom:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 216
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "manual"

    const-string v2, "manual"

    const/16 v15, 0x93

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->manual:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 217
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "rear"

    const-string v2, "rear"

    const/16 v15, 0x94

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->rear:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 219
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "print_quality"

    const-string v2, "print-quality"

    const/16 v15, 0x95

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_quality:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 220
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "draft"

    const-string v2, "draft"

    const/16 v15, 0x96

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->draft:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 221
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "normal"

    const-string v2, "normal"

    const/16 v15, 0x97

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->normal:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 222
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "high"

    const-string v2, "high"

    const/16 v15, 0x98

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->high:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 223
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "best"

    const-string v2, "x-best"

    const/16 v15, 0x99

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->best:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 225
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "print_sheet_collate"

    const-string v2, "print-sheet-collate"

    const/16 v15, 0x9a

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_sheet_collate:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 226
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "collated"

    const-string v2, "collated"

    const/16 v15, 0x9b

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->collated:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 227
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "uncollated"

    const-string v2, "uncollated"

    const/16 v15, 0x9c

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->uncollated:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 229
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "fixed_parameters"

    const-string v2, "fixed-parameters"

    const/16 v15, 0x9d

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->fixed_parameters:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 230
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "preferred_parameters"

    const-string v2, "preferred-parameters"

    const/16 v15, 0x9e

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->preferred_parameters:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 231
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "priority_order"

    const-string v2, "priority-order"

    const/16 v15, 0x9f

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->priority_order:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 232
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "default_as_fixed"

    const-string v2, "default-as-fixed"

    const/16 v15, 0xa0

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->default_as_fixed:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 234
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "frame"

    const-string v2, "frame"

    const/16 v15, 0xa1

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->frame:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 236
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "oval"

    const-string v2, "oval"

    const/16 v15, 0xa2

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->oval:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 237
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "oval_soft_edge"

    const-string v2, "oval-soft-edge"

    const/16 v15, 0xa3

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->oval_soft_edge:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 239
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "copies"

    const-string v2, "copies"

    const/16 v15, 0xa4

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->copies:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 241
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "color_effects_type"

    const-string v2, "color-effects-type"

    const/16 v15, 0xa5

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->color_effects_type:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 242
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "color"

    const-string v2, "color"

    const/16 v15, 0xa6

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->color:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 243
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "monochrome_grayscale"

    const-string v2, "monochrome-grayscale"

    const/16 v15, 0xa7

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->monochrome_grayscale:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 245
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "copy_magnification"

    const-string v2, "copy-magnification"

    const/16 v15, 0xa8

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->copy_magnification:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 246
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "auto"

    const-string v2, "auto"

    const/16 v15, 0xa9

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->auto:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 248
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "x_density"

    const-string v2, "x-density"

    const/16 v15, 0xaa

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_density:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 250
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "print_x_bleed"

    const-string v2, "print-x-bleed"

    const/16 v15, 0xab

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_x_bleed:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 252
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "midium"

    const-string v2, "midium"

    const/16 v15, 0xac

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->midium:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 253
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "minimum"

    const-string v2, "minimum"

    const/16 v15, 0xad

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->minimum:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 255
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "orientation"

    const-string v2, "orientation"

    const/16 v15, 0xae

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->orientation:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 256
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "portrait"

    const-string v2, "portrait"

    const/16 v15, 0xaf

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->portrait:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 257
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "landscape"

    const-string v2, "landscape"

    const/16 v15, 0xb0

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->landscape:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 259
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "print_x_dry_time"

    const-string v2, "print-x-dry-time"

    const/16 v15, 0xb1

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_x_dry_time:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 261
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "Long"

    const-string v2, "long"

    const/16 v15, 0xb2

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->Long:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 262
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "longer"

    const-string v2, "longer"

    const/16 v15, 0xb3

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->longer:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 264
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "print_x_auto_pg"

    const-string v2, "print-x-auto-pg"

    const/16 v15, 0xb4

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_x_auto_pg:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 265
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "on"

    const-string v2, "on"

    const/16 v15, 0xb5

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->on:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 266
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "off"

    const-string v2, "off"

    const/16 v15, 0xb6

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->off:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 268
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "job_state"

    const-string v2, "job-state"

    const/16 v15, 0xb7

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->job_state:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 269
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "nextpaper"

    const-string v2, "nextpaper"

    const/16 v15, 0xb8

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->nextpaper:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 270
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "scanning"

    const-string v2, "scanning"

    const/16 v15, 0xb9

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scanning:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 271
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "printing"

    const-string v2, "printing"

    const/16 v15, 0xba

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->printing:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 272
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "copying"

    const-string v2, "copying"

    const/16 v15, 0xbb

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->copying:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 273
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "canceling"

    const-string v2, "canceling"

    const/16 v15, 0xbc

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->canceling:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 274
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "finished"

    const-string v2, "finished"

    const/16 v15, 0xbd

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->finished:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 275
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "pending"

    const-string v2, "pending"

    const/16 v15, 0xbe

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->pending:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 276
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "pending_for_template"

    const-string v2, "pending-for-template"

    const/16 v15, 0xbf

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->pending_for_template:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 277
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "waiting_image_for_template"

    const-string v2, "waiting-image-for-template"

    const/16 v15, 0xc0

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->waiting_image_for_template:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 278
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "printing_template"

    const-string v2, "printing-template"

    const/16 v15, 0xc1

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->printing_template:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 279
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "waiting_image"

    const-string v2, "waiting-image"

    const/16 v15, 0xc2

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->waiting_image:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 280
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "waiting_template"

    const-string v2, "waiting-template"

    const/16 v15, 0xc3

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->waiting_template:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 283
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "job_result"

    const-string v2, "job-result"

    const/16 v15, 0xc4

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->job_result:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 286
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "canceled"

    const-string v2, "canceled"

    const/16 v15, 0xc5

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->canceled:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 288
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "memory_full"

    const-string v2, "memory_full"

    const/16 v15, 0xc6

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->memory_full:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 289
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "remove_adf_paper"

    const-string v2, "remove_adf_paper"

    const/16 v15, 0xc7

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->remove_adf_paper:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 290
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "set_adf_paper"

    const-string v2, "set_adf_paper"

    const/16 v15, 0xc8

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->set_adf_paper:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 291
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "document_error"

    const-string v2, "document_error"

    const/16 v15, 0xc9

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->document_error:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 292
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "image_too_large"

    const-string v2, "image-too-large"

    const/16 v15, 0xca

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->image_too_large:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 293
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "communication_error"

    const-string v2, "communication-error"

    const/16 v15, 0xcb

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->communication_error:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 295
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "job_tokens"

    const-string v2, "job-tokens"

    const/16 v15, 0xcc

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->job_tokens:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 296
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "job_print_total_pages"

    const-string v2, "job-print-total-pages"

    const/16 v15, 0xcd

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->job_print_total_pages:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 297
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "job_print_current_pages"

    const-string v2, "job-print-current-pages"

    const/16 v15, 0xce

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->job_print_current_pages:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 298
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "job_image_post_url"

    const-string v2, "job-image-post-url"

    const/16 v15, 0xcf

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->job_image_post_url:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 300
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "x_apf"

    const-string v2, "x-apf"

    const/16 v15, 0xd0

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_apf:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 301
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "x_color_restoration"

    const-string v2, "x-color-restoration"

    const/16 v15, 0xd1

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_color_restoration:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 302
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "scan_count"

    const-string v2, "scan-count"

    const/16 v15, 0xd2

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scan_count:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 303
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "scan_area_x"

    const-string v2, "scan-area-x"

    const/16 v15, 0xd3

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scan_area_x:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 304
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "scan_area_y"

    const-string v2, "scan-area-y"

    const/16 v15, 0xd4

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scan_area_y:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 305
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "scan_area_width"

    const-string v2, "scan-area-width"

    const/16 v15, 0xd5

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scan_area_width:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 306
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "scan_area_height"

    const-string v2, "scan-area-height"

    const/16 v15, 0xd6

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scan_area_height:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 307
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "scan_area_resolution"

    const-string v2, "scan-area-resolution"

    const/16 v15, 0xd7

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scan_area_resolution:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 308
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "x_fit_gamma"

    const-string v2, "x-fit-gamma"

    const/16 v15, 0xd8

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_fit_gamma:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 309
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "x_fit_matrix"

    const-string v2, "x-fit-matrix"

    const/16 v15, 0xd9

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_fit_matrix:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 315
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "x_scale"

    const-string v2, "x-scale"

    const/16 v15, 0xda

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 316
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "x_scale_fullsize"

    const-string v2, "x-scale-fullsize"

    const/16 v15, 0xdb

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_fullsize:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 317
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "x_scale_custom"

    const-string v2, "x-scale-custom"

    const/16 v15, 0xdc

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_custom:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 318
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "x_scale_autofit"

    const-string v2, "x-scale-autofit"

    const/16 v15, 0xdd

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_autofit:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 319
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "x_scale_a4_to_postcard"

    const-string v2, "x-scale-a4-to-postcard"

    const/16 v15, 0xde

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_a4_to_postcard:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 320
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "x_scale_2l_to_postcard"

    const-string v2, "x-scale-2l-to-postcard"

    const/16 v15, 0xdf

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_2l_to_postcard:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 321
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "x_scale_l_to_postcard"

    const-string v2, "x-scale-l-to-postcard"

    const/16 v15, 0xe0

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_l_to_postcard:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 322
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "x_scale_l_to_2l"

    const-string v2, "x-scale-l-to-2l"

    const/16 v15, 0xe1

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_l_to_2l:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 323
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "x_scale_2l_to_a4"

    const-string v2, "x-scale-2l-to-a4"

    const/16 v15, 0xe2

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_2l_to_a4:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 324
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "x_scale_postcard_to_a4"

    const-string v2, "x-scale-postcard-to-a4"

    const/16 v15, 0xe3

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_postcard_to_a4:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 325
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "x_scale_l_to_a4"

    const-string v2, "x-scale-l-to-a4"

    const/16 v15, 0xe4

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_l_to_a4:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 326
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "x_scale_a4_to_b5"

    const-string v2, "x-scale-a4-to-b5"

    const/16 v15, 0xe5

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_a4_to_b5:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 327
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "x_scale_b5_to_a4"

    const-string v2, "x-scale-b5-to-a4"

    const/16 v15, 0xe6

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_b5_to_a4:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 328
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "x_scale_letter_to_kg"

    const-string v2, "x-scale-letter-to-kg"

    const/16 v15, 0xe7

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_letter_to_kg:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 329
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "x_scale_kg_to_letter"

    const-string v2, "x-scale-kg-to-letter"

    const/16 v15, 0xe8

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_kg_to_letter:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 330
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "x_scale_letter_to_2l"

    const-string v2, "x-scale-letter-to-2l"

    const/16 v15, 0xe9

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_letter_to_2l:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 331
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "x_scale_2l_to_letter"

    const-string v2, "x-scale-2l-to-letter"

    const/16 v15, 0xea

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_2l_to_letter:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 332
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "x_scale_kg_to_a4"

    const-string v2, "x-scale-kg-to-a4"

    const/16 v15, 0xeb

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_kg_to_a4:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 333
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "x_scale_a4_to_kg"

    const-string v2, "x-scale-a4-to-kg"

    const/16 v15, 0xec

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_a4_to_kg:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 334
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "x_scale_a4_to_2l"

    const-string v2, "x-scale-a4-to-2l"

    const/16 v15, 0xed

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_a4_to_2l:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 335
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "x_scale_kg_to_8x10"

    const-string v2, "x-scale-kg-to-8x10"

    const/16 v15, 0xee

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_kg_to_8x10:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 336
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "x_scale_8x10_to_2l"

    const-string v2, "x-scale-8x10-to-2l"

    const/16 v15, 0xef

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_8x10_to_2l:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 337
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "x_scale_legal_to_letter"

    const-string v2, "x-scale-legal-to-letter"

    const/16 v15, 0xf0

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_legal_to_letter:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 338
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "x_scale_2l_to_kg"

    const-string v2, "x-scale-2l-to-kg"

    const/16 v15, 0xf1

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_2l_to_kg:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 339
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "x_scale_kg_to_2l"

    const-string v2, "x-scale-kg-to-2l"

    const/16 v15, 0xf2

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_kg_to_2l:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 340
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "x_scale_a5_to_a4"

    const-string v2, "x-scale-a5-to-a4"

    const/16 v15, 0xf3

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_a5_to_a4:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 341
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "x_scale_a4_to_a5"

    const-string v2, "x-scale-a4-to-a5"

    const/16 v15, 0xf4

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_a4_to_a5:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 342
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "x_scale_a4_to_a3"

    const-string v2, "x-scale-a4-to-a3"

    const/16 v15, 0xf5

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_a4_to_a3:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 343
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "x_scale_letter_to_11x17"

    const-string v2, "x-scale-letter-to-11x17"

    const/16 v15, 0xf6

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_letter_to_11x17:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 350
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "x_borderless"

    const-string v2, "layout"

    const/16 v15, 0xf7

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_borderless:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 352
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "x_null"

    const-string v2, ""

    const/16 v15, 0xf8

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_null:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 353
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "x_failed_communication"

    const-string v2, "x-failed-communication"

    const/16 v15, 0xf9

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_failed_communication:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 354
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "x_unknown"

    const-string v2, "x-unknown"

    const/16 v15, 0xfa

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_unknown:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 355
    new-instance v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const-string v1, "x_invalid_photo_setting"

    const-string v2, "x-invalid-photo-setting"

    const/16 v15, 0xfb

    invoke-direct {v0, v1, v15, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_invalid_photo_setting:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v0, 0xfc

    .line 40
    new-array v0, v0, [Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->client_id:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    aput-object v1, v0, v3

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->success:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    aput-object v1, v0, v4

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->reason:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    aput-object v1, v0, v5

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->illegal_combination:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    aput-object v1, v0, v6

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->illegal_state:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    aput-object v1, v0, v7

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->already_finished:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    aput-object v1, v0, v8

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->unknown_token:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    aput-object v1, v0, v9

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->busy:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    aput-object v1, v0, v10

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->job_token:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    aput-object v1, v0, v11

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->next_document:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    aput-object v1, v0, v12

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->functions:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    aput-object v1, v0, v13

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->keys:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    aput-object v1, v0, v14

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->device:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->printer:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scanner:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->copy:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->copy_photo:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->repeat_copy:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_x_disc_tray_state:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->media_empty:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->tray_set:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->tray_missing:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_remove_background:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_cut_line:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_cut_line_style:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->dash:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->dot:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->continuous:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_cut_line_weight:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->thin:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->medium:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->thick:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->printer_state:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scanner_state:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->idle:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->processing:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x23

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->stopped:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x24

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->printer_state_reasons:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x25

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->none:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x26

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->marker_supply_empty_error:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x27

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->marker_waste_full_error:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x28

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->media_jam_error:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x29

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->media_empty_error:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x2a

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->input_tray_missing_error:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x2b

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->cover_open_error:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x2c

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->output_area_full_error:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x2d

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->other_error:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x2e

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->marker_supply_low_warning:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x2f

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->cover_open:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x30

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scanner_state_reasons:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x31

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->media_size_missmatch_error:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x32

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->document_on_adf:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x33

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->components:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x34

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->disc_tray:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x35

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->adf:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x36

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->layout:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x37

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->standard:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x38

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->borderless:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x39

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->two_up:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x3a

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->two_up_book:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x3b

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->book_two_sided:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x3c

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->mirror:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x3d

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->upper_half:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x3e

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->lower_half:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x3f

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->_3_up:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x40

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->_4_up:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x41

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->_9_up:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x42

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->_16_up:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x43

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->two_repeat:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x44

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->four_repeat:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x45

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->auto_repeat:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x46

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scan_sides:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x47

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_sides:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x48

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->one_sided:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x49

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->two_sided_short_edge:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x4a

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->two_sided_long_edge:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x4b

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_two_sided:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x4c

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scan_media_size:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x4d

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->unused:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x4e

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->iso_a4_210x297mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x4f

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->jis_b5_182x257mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x50

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_type:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x51

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->stationery:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x52

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->photographic_high_gloss:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x53

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->photographic:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x54

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->photographic_semi_gloss:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x55

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->photographic_glossy:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x56

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_media_type_epson_44:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x57

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->photographic_matte:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x58

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->stationery_coated:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x59

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_media_type_epson_2a:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x5a

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->stationery_inkjet:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x5b

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_media_type_epson_1b:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x5c

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_media_type_epson_02:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x5d

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_media_type_epson_19:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x5e

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_media_type_epson_22:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x5f

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->labels:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x60

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->jis_b4_257x364mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x61

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->envelope:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x62

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_media_type_epson_47:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x63

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_media_type_epson_20:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x64

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_media_type_epson_39:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x65

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_media_type_epson_46:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x66

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->stationery_heavyweight:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x67

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->stationery_letterhead:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x68

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_size:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x69

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->oe_photo_l_3_5x5in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x6a

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_5x7_5x7in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x6b

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->jpn_hagaki_100x148mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x6c

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_index_4x6_4x6in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x6d

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_govt_letter_8x10in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x6e

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_letter_8_5x11in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x6f

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_legal_8_5x14in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x70

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->iso_a5_148x210mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x71

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_4psize_254x305mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x72

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->iso_a3_297x420mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x73

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_ledger_11x17in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x74

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->iso_a6_105x148mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x75

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->jpn_chou3_120x235mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x76

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->jpn_chou4_90x205mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x77

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_epson_18_120x176mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x78

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_epson_1A_98x148mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x79

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->jpn_you4_105x235mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x7a

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->iso_c6_114x162mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x7b

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->jpn_kaku2_240x332mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x7c

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->iso_c4_229x324mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x7d

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_cardsize_55x91mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x7e

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_creditcardsize_54x86mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x7f

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_hivision_101_6x180_6mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x80

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->iso_dl_110x220mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x81

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->jis_b6_128x182mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x82

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_executive_7_25x10_5in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x83

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_foolscap_8_5x13in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x84

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_edp_11x14in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x85

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_number_10_4_125x9_5in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x86

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->om_8k_270x390mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x87

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->om_16k_195x270mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x88

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_invoice_5_5x8_5in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x89

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_epson_indianlegal_215x345mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x8a

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_oficio_8_5x13_4in:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x8b

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->om_folio_sp_215x315mm:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x8c

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scan_content_type:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x8d

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->text:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x8e

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->mixed:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x8f

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_source:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x90

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->top:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x91

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->bottom:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x92

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->manual:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x93

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->rear:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x94

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_quality:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x95

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->draft:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x96

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->normal:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x97

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->high:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x98

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->best:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x99

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_sheet_collate:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x9a

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->collated:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x9b

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->uncollated:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x9c

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->fixed_parameters:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x9d

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->preferred_parameters:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x9e

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->priority_order:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0x9f

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->default_as_fixed:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xa0

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->frame:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xa1

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->oval:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xa2

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->oval_soft_edge:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xa3

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->copies:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xa4

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->color_effects_type:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xa5

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->color:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xa6

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->monochrome_grayscale:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xa7

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->copy_magnification:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xa8

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->auto:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xa9

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_density:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xaa

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_x_bleed:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xab

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->midium:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xac

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->minimum:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xad

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->orientation:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xae

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->portrait:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xaf

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->landscape:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xb0

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_x_dry_time:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xb1

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->Long:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xb2

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->longer:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xb3

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_x_auto_pg:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xb4

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->on:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xb5

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->off:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xb6

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->job_state:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xb7

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->nextpaper:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xb8

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scanning:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xb9

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->printing:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xba

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->copying:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xbb

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->canceling:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xbc

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->finished:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xbd

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->pending:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xbe

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->pending_for_template:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xbf

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->waiting_image_for_template:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xc0

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->printing_template:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xc1

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->waiting_image:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xc2

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->waiting_template:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xc3

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->job_result:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xc4

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->canceled:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xc5

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->memory_full:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xc6

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->remove_adf_paper:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xc7

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->set_adf_paper:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xc8

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->document_error:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xc9

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->image_too_large:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xca

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->communication_error:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xcb

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->job_tokens:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xcc

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->job_print_total_pages:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xcd

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->job_print_current_pages:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xce

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->job_image_post_url:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xcf

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_apf:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xd0

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_color_restoration:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xd1

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scan_count:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xd2

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scan_area_x:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xd3

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scan_area_y:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xd4

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scan_area_width:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xd5

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scan_area_height:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xd6

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scan_area_resolution:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xd7

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_fit_gamma:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xd8

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_fit_matrix:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xd9

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xda

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_fullsize:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xdb

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_custom:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xdc

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_autofit:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xdd

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_a4_to_postcard:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xde

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_2l_to_postcard:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xdf

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_l_to_postcard:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xe0

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_l_to_2l:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xe1

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_2l_to_a4:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xe2

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_postcard_to_a4:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xe3

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_l_to_a4:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xe4

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_a4_to_b5:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xe5

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_b5_to_a4:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xe6

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_letter_to_kg:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xe7

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_kg_to_letter:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xe8

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_letter_to_2l:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xe9

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_2l_to_letter:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xea

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_kg_to_a4:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xeb

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_a4_to_kg:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xec

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_a4_to_2l:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xed

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_kg_to_8x10:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xee

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_8x10_to_2l:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xef

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_legal_to_letter:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xf0

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_2l_to_kg:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xf1

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_kg_to_2l:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xf2

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_a5_to_a4:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xf3

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_a4_to_a5:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xf4

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_a4_to_a3:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xf5

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_letter_to_11x17:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xf6

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_borderless:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xf7

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_null:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xf8

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_failed_communication:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xf9

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_unknown:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xfa

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_invalid_photo_setting:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v2, 0xfb

    aput-object v1, v0, v2

    sput-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->$VALUES:[Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 359
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 360
    iput-object p3, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->string:Ljava/lang/String;

    return-void
.end method

.method public static stringOf(Ljava/lang/String;)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 5

    .line 368
    invoke-static {}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->values()[Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 369
    iget-object v4, v3, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->string:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 373
    :cond_1
    sget-object p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_null:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 1

    .line 40
    const-class v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    return-object p0
.end method

.method public static values()[Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 1

    .line 40
    sget-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->$VALUES:[Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0}, [Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .line 377
    iget-object v0, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->string:Ljava/lang/String;

    return-object v0
.end method

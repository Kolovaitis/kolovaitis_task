.class public Lepson/print/rpcopy/Component/eremoteoperation/ERemoteScanner$ERemoteScannerStatusResult;
.super Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;
.source "ERemoteScanner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/rpcopy/Component/eremoteoperation/ERemoteScanner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ERemoteScannerStatusResult"
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteScanner;


# direct methods
.method public constructor <init>(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteScanner;Lorg/json/JSONObject;)V
    .locals 0

    .line 20
    iput-object p1, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteScanner$ERemoteScannerStatusResult;->this$0:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteScanner;

    .line 21
    invoke-direct {p0, p2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;-><init>(Lorg/json/JSONObject;)V

    return-void
.end method


# virtual methods
.method public document_on_adf()Z
    .locals 1

    .line 25
    sget-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->document_on_adf:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p0, v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteScanner$ERemoteScannerStatusResult;->getBooleanValueOf(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Z

    move-result v0

    return v0
.end method

.method public scanner_state()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 1

    .line 29
    sget-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scanner_state:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p0, v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteScanner$ERemoteScannerStatusResult;->getParamValueOf(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v0

    return-object v0
.end method

.method public scanner_state_reasons()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ">;"
        }
    .end annotation

    .line 33
    sget-object v0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scanner_state_reasons:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p0, v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteScanner$ERemoteScannerStatusResult;->getParamsValueOf(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.class public Lepson/print/rpcopy/Component/eremoteoperation/ERemoteDevice;
.super Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation;
.source "ERemoteDevice.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/rpcopy/Component/eremoteoperation/ERemoteDevice$ERemoteDeviceStatusResult;,
        Lepson/print/rpcopy/Component/eremoteoperation/ERemoteDevice$ERemoteDeviceFunctionsResult;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation;-><init>()V

    return-void
.end method


# virtual methods
.method public getFunctions(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$IRemoteOperationParameter;)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteDevice$ERemoteDeviceFunctionsResult;
    .locals 3

    .line 54
    iget-object v0, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteDevice;->hostIP:Ljava/lang/String;

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;->get_functions:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    .line 55
    invoke-virtual {p0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteDevice;->getRequestConnectionTimeout()I

    move-result v2

    .line 54
    invoke-static {v0, v1, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->requestDevice(Ljava/lang/String;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;I)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;

    move-result-object v0

    .line 56
    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->client_id:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$IRemoteOperationParameter;->client_id()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/lang/String;)V

    .line 57
    invoke-virtual {v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->getRemoteRequest()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;

    move-result-object p1

    .line 59
    invoke-virtual {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;->execute()Lorg/json/JSONObject;

    move-result-object p1

    .line 60
    new-instance v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteDevice$ERemoteDeviceFunctionsResult;

    invoke-direct {v1, p0, p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteDevice$ERemoteDeviceFunctionsResult;-><init>(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteDevice;Lorg/json/JSONObject;)V

    .line 61
    invoke-virtual {v1, v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteDevice$ERemoteDeviceFunctionsResult;->setRemoteRequestBuilder(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;)V

    return-object v1
.end method

.method public getStatus(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$IRemoteStatusParameter;)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteDevice$ERemoteDeviceStatusResult;
    .locals 3

    .line 66
    iget-object v0, p0, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteDevice;->hostIP:Ljava/lang/String;

    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;->get_status:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    .line 67
    invoke-virtual {p0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteDevice;->getRequestConnectionTimeout()I

    move-result v2

    .line 66
    invoke-static {v0, v1, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->requestDevice(Ljava/lang/String;Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$RequestParam;I)Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;

    move-result-object v0

    .line 68
    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->client_id:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$IRemoteStatusParameter;->client_id()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/lang/String;)V

    .line 69
    sget-object v1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->keys:Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$IRemoteStatusParameter;->keys()Ljava/util/ArrayList;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/util/ArrayList;)V

    .line 70
    new-instance p1, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteDevice$ERemoteDeviceStatusResult;

    invoke-virtual {v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->getRemoteRequest()Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;

    move-result-object v1

    invoke-virtual {v1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;->execute()Lorg/json/JSONObject;

    move-result-object v1

    invoke-direct {p1, p0, v1}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteDevice$ERemoteDeviceStatusResult;-><init>(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteDevice;Lorg/json/JSONObject;)V

    .line 71
    invoke-virtual {p1, v0}, Lepson/print/rpcopy/Component/eremoteoperation/ERemoteDevice$ERemoteDeviceStatusResult;->setRemoteRequestBuilder(Lepson/print/rpcopy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;)V

    return-object p1
.end method

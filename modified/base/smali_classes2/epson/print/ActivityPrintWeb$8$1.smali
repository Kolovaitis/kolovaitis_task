.class Lepson/print/ActivityPrintWeb$8$1;
.super Lepson/print/Util/AsyncTaskExecutor;
.source "ActivityPrintWeb.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/ActivityPrintWeb$8;->handleMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lepson/print/Util/AsyncTaskExecutor<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field progress:Lepson/print/screen/WorkingDialog;

.field final synthetic this$1:Lepson/print/ActivityPrintWeb$8;


# direct methods
.method constructor <init>(Lepson/print/ActivityPrintWeb$8;)V
    .locals 1

    .line 490
    iput-object p1, p0, Lepson/print/ActivityPrintWeb$8$1;->this$1:Lepson/print/ActivityPrintWeb$8;

    invoke-direct {p0}, Lepson/print/Util/AsyncTaskExecutor;-><init>()V

    .line 493
    new-instance p1, Lepson/print/screen/WorkingDialog;

    iget-object v0, p0, Lepson/print/ActivityPrintWeb$8$1;->this$1:Lepson/print/ActivityPrintWeb$8;

    iget-object v0, v0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-direct {p1, v0}, Lepson/print/screen/WorkingDialog;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lepson/print/ActivityPrintWeb$8$1;->progress:Lepson/print/screen/WorkingDialog;

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 490
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/print/ActivityPrintWeb$8$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 1

    .line 507
    :try_start_0
    iget-object p1, p0, Lepson/print/ActivityPrintWeb$8$1;->this$1:Lepson/print/ActivityPrintWeb$8;

    iget-object p1, p1, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-static {p1}, Lepson/print/ActivityPrintWeb;->access$700(Lepson/print/ActivityPrintWeb;)Lepson/print/service/IEpsonService;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 509
    iget-object p1, p0, Lepson/print/ActivityPrintWeb$8$1;->this$1:Lepson/print/ActivityPrintWeb$8;

    iget-object p1, p1, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-static {p1}, Lepson/print/ActivityPrintWeb;->access$800(Lepson/print/ActivityPrintWeb;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 510
    iget-object p1, p0, Lepson/print/ActivityPrintWeb$8$1;->this$1:Lepson/print/ActivityPrintWeb$8;

    iget-object p1, p1, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-static {p1}, Lepson/print/ActivityPrintWeb;->access$700(Lepson/print/ActivityPrintWeb;)Lepson/print/service/IEpsonService;

    move-result-object p1

    sget-object v0, Lepson/print/screen/PrintSetting$Kind;->document:Lepson/print/screen/PrintSetting$Kind;

    invoke-virtual {v0}, Lepson/print/screen/PrintSetting$Kind;->name()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lepson/print/service/IEpsonService;->EpsonConnectUpdatePrinterSettings(Ljava/lang/String;)I

    goto :goto_0

    .line 515
    :cond_0
    iget-object p1, p0, Lepson/print/ActivityPrintWeb$8$1;->this$1:Lepson/print/ActivityPrintWeb$8;

    iget-object p1, p1, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-static {p1}, Lepson/print/ActivityPrintWeb;->access$700(Lepson/print/ActivityPrintWeb;)Lepson/print/service/IEpsonService;

    move-result-object p1

    sget-object v0, Lepson/print/screen/PrintSetting$Kind;->document:Lepson/print/screen/PrintSetting$Kind;

    invoke-virtual {v0}, Lepson/print/screen/PrintSetting$Kind;->name()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lepson/print/service/IEpsonService;->updatePrinterSettings(Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 519
    invoke-virtual {p1}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 490
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/print/ActivityPrintWeb$8$1;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 2

    .line 527
    invoke-super {p0, p1}, Lepson/print/Util/AsyncTaskExecutor;->onPostExecute(Ljava/lang/Object;)V

    .line 532
    iget-object p1, p0, Lepson/print/ActivityPrintWeb$8$1;->this$1:Lepson/print/ActivityPrintWeb$8;

    iget-object p1, p1, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    iget-object v0, p0, Lepson/print/ActivityPrintWeb$8$1;->this$1:Lepson/print/ActivityPrintWeb$8;

    iget-object v0, v0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-static {v0}, Lepson/print/ActivityPrintWeb;->access$900(Lepson/print/ActivityPrintWeb;)Z

    move-result v0

    invoke-virtual {p1, v0}, Lepson/print/ActivityPrintWeb;->setOrentationView(Z)V

    .line 533
    iget-object p1, p0, Lepson/print/ActivityPrintWeb$8$1;->this$1:Lepson/print/ActivityPrintWeb$8;

    iget-object p1, p1, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-static {p1}, Lepson/print/ActivityPrintWeb;->access$1000(Lepson/print/ActivityPrintWeb;)Landroid/widget/LinearLayout;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 536
    iget-object p1, p0, Lepson/print/ActivityPrintWeb$8$1;->progress:Lepson/print/screen/WorkingDialog;

    invoke-virtual {p1}, Lepson/print/screen/WorkingDialog;->isShowing()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 537
    iget-object p1, p0, Lepson/print/ActivityPrintWeb$8$1;->progress:Lepson/print/screen/WorkingDialog;

    invoke-virtual {p1}, Lepson/print/screen/WorkingDialog;->dismiss()V

    .line 541
    :cond_0
    iget-object p1, p0, Lepson/print/ActivityPrintWeb$8$1;->this$1:Lepson/print/ActivityPrintWeb$8;

    iget-object p1, p1, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    iget-boolean p1, p1, Lepson/print/ActivityPrintWeb;->bAutoStartPrint:Z

    if-eqz p1, :cond_1

    .line 543
    iget-object p1, p0, Lepson/print/ActivityPrintWeb$8$1;->this$1:Lepson/print/ActivityPrintWeb$8;

    iget-object p1, p1, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    iget-object v1, p0, Lepson/print/ActivityPrintWeb$8$1;->this$1:Lepson/print/ActivityPrintWeb$8;

    iget-object v1, v1, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-static {v1}, Lepson/print/ActivityPrintWeb;->access$1100(Lepson/print/ActivityPrintWeb;)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {p1, v1}, Lepson/print/ActivityPrintWeb;->onClick(Landroid/view/View;)V

    const-string p1, "ActivityPrintWeb"

    const-string v1, "onClick(mPrint)"

    .line 544
    invoke-static {p1, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 545
    iget-object p1, p0, Lepson/print/ActivityPrintWeb$8$1;->this$1:Lepson/print/ActivityPrintWeb$8;

    iget-object p1, p1, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    iput-boolean v0, p1, Lepson/print/ActivityPrintWeb;->bAutoStartPrint:Z

    :cond_1
    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .line 497
    invoke-super {p0}, Lepson/print/Util/AsyncTaskExecutor;->onPreExecute()V

    .line 500
    iget-object v0, p0, Lepson/print/ActivityPrintWeb$8$1;->progress:Lepson/print/screen/WorkingDialog;

    invoke-virtual {v0}, Lepson/print/screen/WorkingDialog;->show()V

    return-void
.end method

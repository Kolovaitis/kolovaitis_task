.class Lepson/print/ActivityViewImageSelect$PreviewImageSelectTask;
.super Lepson/print/Util/AsyncTaskExecutor;
.source "ActivityViewImageSelect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/ActivityViewImageSelect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PreviewImageSelectTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lepson/print/Util/AsyncTaskExecutor<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private mError:Z

.field final synthetic this$0:Lepson/print/ActivityViewImageSelect;


# direct methods
.method private constructor <init>(Lepson/print/ActivityViewImageSelect;)V
    .locals 0

    .line 2435
    iput-object p1, p0, Lepson/print/ActivityViewImageSelect$PreviewImageSelectTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-direct {p0}, Lepson/print/Util/AsyncTaskExecutor;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lepson/print/ActivityViewImageSelect;Lepson/print/ActivityViewImageSelect$1;)V
    .locals 0

    .line 2435
    invoke-direct {p0, p1}, Lepson/print/ActivityViewImageSelect$PreviewImageSelectTask;-><init>(Lepson/print/ActivityViewImageSelect;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 2435
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/print/ActivityViewImageSelect$PreviewImageSelectTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 5

    const/4 p1, 0x1

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 2444
    :goto_0
    :try_start_0
    iget-object v2, p0, Lepson/print/ActivityViewImageSelect$PreviewImageSelectTask;->this$0:Lepson/print/ActivityViewImageSelect;

    iget-object v2, v2, Lepson/print/ActivityViewImageSelect;->previewImageView:Lcom/epson/iprint/apf/ApfPreviewView;

    iget-object v3, p0, Lepson/print/ActivityViewImageSelect$PreviewImageSelectTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {v3}, Lepson/print/ActivityViewImageSelect;->access$100(Lepson/print/ActivityViewImageSelect;)I

    move-result v3

    iget-object v4, p0, Lepson/print/ActivityViewImageSelect$PreviewImageSelectTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-virtual {v2, v3, v0, v4}, Lcom/epson/iprint/apf/ApfPreviewView;->setImage(IILandroid/app/Activity;)Z
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    nop

    .line 2449
    iput-boolean p1, p0, Lepson/print/ActivityViewImageSelect$PreviewImageSelectTask;->mError:Z

    if-nez v1, :cond_0

    .line 2452
    iget-object v1, p0, Lepson/print/ActivityViewImageSelect$PreviewImageSelectTask;->this$0:Lepson/print/ActivityViewImageSelect;

    iget-object v1, v1, Lepson/print/ActivityViewImageSelect;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 2456
    :cond_0
    iget-object v1, p0, Lepson/print/ActivityViewImageSelect$PreviewImageSelectTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {v1}, Lepson/print/ActivityViewImageSelect;->access$1600(Lepson/print/ActivityViewImageSelect;)Lepson/print/phlayout/PhotoPreviewImageList;

    move-result-object v1

    invoke-virtual {v1}, Lepson/print/phlayout/PhotoPreviewImageList;->size()I

    move-result v1

    if-gtz v1, :cond_1

    :goto_1
    const/4 p1, 0x0

    return-object p1

    .line 2462
    :cond_1
    iget-object v1, p0, Lepson/print/ActivityViewImageSelect$PreviewImageSelectTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {v1}, Lepson/print/ActivityViewImageSelect;->access$100(Lepson/print/ActivityViewImageSelect;)I

    move-result v1

    if-ltz v1, :cond_2

    iget-object v1, p0, Lepson/print/ActivityViewImageSelect$PreviewImageSelectTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {v1}, Lepson/print/ActivityViewImageSelect;->access$100(Lepson/print/ActivityViewImageSelect;)I

    move-result v1

    iget-object v2, p0, Lepson/print/ActivityViewImageSelect$PreviewImageSelectTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {v2}, Lepson/print/ActivityViewImageSelect;->access$1600(Lepson/print/ActivityViewImageSelect;)Lepson/print/phlayout/PhotoPreviewImageList;

    move-result-object v2

    invoke-virtual {v2}, Lepson/print/phlayout/PhotoPreviewImageList;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 2463
    iget-object v1, p0, Lepson/print/ActivityViewImageSelect$PreviewImageSelectTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {v1}, Lepson/print/ActivityViewImageSelect;->access$1600(Lepson/print/ActivityViewImageSelect;)Lepson/print/phlayout/PhotoPreviewImageList;

    move-result-object v1

    iget-object v2, p0, Lepson/print/ActivityViewImageSelect$PreviewImageSelectTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {v2}, Lepson/print/ActivityViewImageSelect;->access$100(Lepson/print/ActivityViewImageSelect;)I

    move-result v2

    invoke-virtual {v1, v2}, Lepson/print/phlayout/PhotoPreviewImageList;->remove(I)V

    .line 2467
    :cond_2
    iget-object v1, p0, Lepson/print/ActivityViewImageSelect$PreviewImageSelectTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {v1, v0}, Lepson/print/ActivityViewImageSelect;->access$102(Lepson/print/ActivityViewImageSelect;I)I

    const/4 v1, 0x1

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 2435
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/print/ActivityViewImageSelect$PreviewImageSelectTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 1

    .line 2481
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$PreviewImageSelectTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-virtual {p1}, Lepson/print/ActivityViewImageSelect;->isFinishing()Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    .line 2485
    :cond_0
    iget-boolean p1, p0, Lepson/print/ActivityViewImageSelect$PreviewImageSelectTask;->mError:Z

    if-eqz p1, :cond_1

    .line 2488
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$PreviewImageSelectTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {p1}, Lepson/print/ActivityViewImageSelect;->access$2000(Lepson/print/ActivityViewImageSelect;)V

    .line 2491
    :cond_1
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$PreviewImageSelectTask;->this$0:Lepson/print/ActivityViewImageSelect;

    iget-object p1, p1, Lepson/print/ActivityViewImageSelect;->previewImageView:Lcom/epson/iprint/apf/ApfPreviewView;

    invoke-virtual {p1}, Lcom/epson/iprint/apf/ApfPreviewView;->invalidate()V

    .line 2493
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$PreviewImageSelectTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-virtual {p1}, Lepson/print/ActivityViewImageSelect;->updateScreen()V

    .line 2495
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$PreviewImageSelectTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {p1}, Lepson/print/ActivityViewImageSelect;->access$900(Lepson/print/ActivityViewImageSelect;)Lepson/common/DialogProgressViewModel;

    move-result-object p1

    const-string v0, "dialog_progress"

    invoke-virtual {p1, v0}, Lepson/common/DialogProgressViewModel;->doDismiss(Ljava/lang/String;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .line 2476
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect$PreviewImageSelectTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {v0}, Lepson/print/ActivityViewImageSelect;->access$900(Lepson/print/ActivityViewImageSelect;)Lepson/common/DialogProgressViewModel;

    move-result-object v0

    const-string v1, "dialog_progress"

    invoke-virtual {v0, v1}, Lepson/common/DialogProgressViewModel;->doShow(Ljava/lang/String;)V

    return-void
.end method

.class public Lepson/print/MyPrinter;
.super Ljava/lang/Object;
.source "MyPrinter.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lepson/print/MyPrinter;",
            ">;"
        }
    .end annotation
.end field

.field public static final PRINTER_IP:I = 0x3

.field public static final PRINTER_LOCAL:I = 0x1

.field public static final PRINTER_REMOTE:I = 0x2

.field public static final PRINTER_UNKNOWN:I = 0x0

.field public static final PRINTER_USB:Ljava/lang/String; = "USB"


# instance fields
.field private mCommonDeviceName:Ljava/lang/String;

.field private mEmailAddress:Ljava/lang/String;

.field private mIp:Ljava/lang/String;

.field private mLang:I

.field private mLocation:I

.field private mName:Ljava/lang/String;

.field private mPrinterId:Ljava/lang/String;

.field private mPrinterIndex:I

.field private mScannerId:Ljava/lang/String;

.field private mSerialNo:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 253
    new-instance v0, Lepson/print/MyPrinter$1;

    invoke-direct {v0}, Lepson/print/MyPrinter$1;-><init>()V

    sput-object v0, Lepson/print/MyPrinter;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 41
    iput-object v0, p0, Lepson/print/MyPrinter;->mName:Ljava/lang/String;

    .line 42
    iput-object v0, p0, Lepson/print/MyPrinter;->mIp:Ljava/lang/String;

    .line 43
    iput-object v0, p0, Lepson/print/MyPrinter;->mPrinterId:Ljava/lang/String;

    .line 44
    iput-object v0, p0, Lepson/print/MyPrinter;->mScannerId:Ljava/lang/String;

    .line 45
    iput-object v0, p0, Lepson/print/MyPrinter;->mSerialNo:Ljava/lang/String;

    .line 46
    iput-object v0, p0, Lepson/print/MyPrinter;->mEmailAddress:Ljava/lang/String;

    const/4 v1, 0x0

    .line 47
    iput v1, p0, Lepson/print/MyPrinter;->mLocation:I

    const/4 v1, 0x1

    .line 48
    iput v1, p0, Lepson/print/MyPrinter;->mLang:I

    .line 49
    iput-object v0, p0, Lepson/print/MyPrinter;->mCommonDeviceName:Ljava/lang/String;

    const/4 v0, -0x1

    .line 52
    iput v0, p0, Lepson/print/MyPrinter;->mPrinterIndex:I

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .line 265
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 41
    iput-object v0, p0, Lepson/print/MyPrinter;->mName:Ljava/lang/String;

    .line 42
    iput-object v0, p0, Lepson/print/MyPrinter;->mIp:Ljava/lang/String;

    .line 43
    iput-object v0, p0, Lepson/print/MyPrinter;->mPrinterId:Ljava/lang/String;

    .line 44
    iput-object v0, p0, Lepson/print/MyPrinter;->mScannerId:Ljava/lang/String;

    .line 45
    iput-object v0, p0, Lepson/print/MyPrinter;->mSerialNo:Ljava/lang/String;

    .line 46
    iput-object v0, p0, Lepson/print/MyPrinter;->mEmailAddress:Ljava/lang/String;

    const/4 v1, 0x0

    .line 47
    iput v1, p0, Lepson/print/MyPrinter;->mLocation:I

    const/4 v1, 0x1

    .line 48
    iput v1, p0, Lepson/print/MyPrinter;->mLang:I

    .line 49
    iput-object v0, p0, Lepson/print/MyPrinter;->mCommonDeviceName:Ljava/lang/String;

    const/4 v0, -0x1

    .line 52
    iput v0, p0, Lepson/print/MyPrinter;->mPrinterIndex:I

    .line 266
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepson/print/MyPrinter;->mName:Ljava/lang/String;

    .line 267
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepson/print/MyPrinter;->mIp:Ljava/lang/String;

    .line 268
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepson/print/MyPrinter;->mPrinterId:Ljava/lang/String;

    .line 269
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepson/print/MyPrinter;->mSerialNo:Ljava/lang/String;

    .line 270
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepson/print/MyPrinter;->mEmailAddress:Ljava/lang/String;

    .line 271
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lepson/print/MyPrinter;->mLocation:I

    .line 272
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepson/print/MyPrinter;->mScannerId:Ljava/lang/String;

    .line 273
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lepson/print/MyPrinter;->mLang:I

    .line 274
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result p1

    iput p1, p0, Lepson/print/MyPrinter;->mPrinterIndex:I

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lepson/print/MyPrinter$1;)V
    .locals 0

    .line 32
    invoke-direct {p0, p1}, Lepson/print/MyPrinter;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 41
    iput-object v0, p0, Lepson/print/MyPrinter;->mName:Ljava/lang/String;

    .line 42
    iput-object v0, p0, Lepson/print/MyPrinter;->mIp:Ljava/lang/String;

    .line 43
    iput-object v0, p0, Lepson/print/MyPrinter;->mPrinterId:Ljava/lang/String;

    .line 44
    iput-object v0, p0, Lepson/print/MyPrinter;->mScannerId:Ljava/lang/String;

    .line 45
    iput-object v0, p0, Lepson/print/MyPrinter;->mSerialNo:Ljava/lang/String;

    .line 46
    iput-object v0, p0, Lepson/print/MyPrinter;->mEmailAddress:Ljava/lang/String;

    const/4 v1, 0x0

    .line 47
    iput v1, p0, Lepson/print/MyPrinter;->mLocation:I

    const/4 v1, 0x1

    .line 48
    iput v1, p0, Lepson/print/MyPrinter;->mLang:I

    .line 49
    iput-object v0, p0, Lepson/print/MyPrinter;->mCommonDeviceName:Ljava/lang/String;

    const/4 v0, -0x1

    .line 52
    iput v0, p0, Lepson/print/MyPrinter;->mPrinterIndex:I

    .line 65
    iput-object p1, p0, Lepson/print/MyPrinter;->mName:Ljava/lang/String;

    .line 66
    iput-object p2, p0, Lepson/print/MyPrinter;->mIp:Ljava/lang/String;

    .line 67
    iput-object p3, p0, Lepson/print/MyPrinter;->mPrinterId:Ljava/lang/String;

    .line 68
    iput-object p4, p0, Lepson/print/MyPrinter;->mSerialNo:Ljava/lang/String;

    .line 69
    iput v1, p0, Lepson/print/MyPrinter;->mLocation:I

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 41
    iput-object v0, p0, Lepson/print/MyPrinter;->mName:Ljava/lang/String;

    .line 42
    iput-object v0, p0, Lepson/print/MyPrinter;->mIp:Ljava/lang/String;

    .line 43
    iput-object v0, p0, Lepson/print/MyPrinter;->mPrinterId:Ljava/lang/String;

    .line 44
    iput-object v0, p0, Lepson/print/MyPrinter;->mScannerId:Ljava/lang/String;

    .line 45
    iput-object v0, p0, Lepson/print/MyPrinter;->mSerialNo:Ljava/lang/String;

    .line 46
    iput-object v0, p0, Lepson/print/MyPrinter;->mEmailAddress:Ljava/lang/String;

    const/4 v1, 0x0

    .line 47
    iput v1, p0, Lepson/print/MyPrinter;->mLocation:I

    const/4 v1, 0x1

    .line 48
    iput v1, p0, Lepson/print/MyPrinter;->mLang:I

    .line 49
    iput-object v0, p0, Lepson/print/MyPrinter;->mCommonDeviceName:Ljava/lang/String;

    const/4 v0, -0x1

    .line 52
    iput v0, p0, Lepson/print/MyPrinter;->mPrinterIndex:I

    .line 82
    iput-object p1, p0, Lepson/print/MyPrinter;->mName:Ljava/lang/String;

    .line 83
    iput-object p2, p0, Lepson/print/MyPrinter;->mIp:Ljava/lang/String;

    .line 84
    iput-object p3, p0, Lepson/print/MyPrinter;->mPrinterId:Ljava/lang/String;

    .line 85
    iput-object p4, p0, Lepson/print/MyPrinter;->mSerialNo:Ljava/lang/String;

    .line 86
    iput-object p5, p0, Lepson/print/MyPrinter;->mEmailAddress:Ljava/lang/String;

    const/4 p1, 0x2

    .line 87
    iput p1, p0, Lepson/print/MyPrinter;->mLocation:I

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2

    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 41
    iput-object v0, p0, Lepson/print/MyPrinter;->mName:Ljava/lang/String;

    .line 42
    iput-object v0, p0, Lepson/print/MyPrinter;->mIp:Ljava/lang/String;

    .line 43
    iput-object v0, p0, Lepson/print/MyPrinter;->mPrinterId:Ljava/lang/String;

    .line 44
    iput-object v0, p0, Lepson/print/MyPrinter;->mScannerId:Ljava/lang/String;

    .line 45
    iput-object v0, p0, Lepson/print/MyPrinter;->mSerialNo:Ljava/lang/String;

    .line 46
    iput-object v0, p0, Lepson/print/MyPrinter;->mEmailAddress:Ljava/lang/String;

    const/4 v1, 0x0

    .line 47
    iput v1, p0, Lepson/print/MyPrinter;->mLocation:I

    const/4 v1, 0x1

    .line 48
    iput v1, p0, Lepson/print/MyPrinter;->mLang:I

    .line 49
    iput-object v0, p0, Lepson/print/MyPrinter;->mCommonDeviceName:Ljava/lang/String;

    const/4 v0, -0x1

    .line 52
    iput v0, p0, Lepson/print/MyPrinter;->mPrinterIndex:I

    .line 100
    iput-object p1, p0, Lepson/print/MyPrinter;->mName:Ljava/lang/String;

    .line 101
    iput-object p2, p0, Lepson/print/MyPrinter;->mIp:Ljava/lang/String;

    .line 102
    iput-object p3, p0, Lepson/print/MyPrinter;->mPrinterId:Ljava/lang/String;

    .line 103
    iput-object p4, p0, Lepson/print/MyPrinter;->mSerialNo:Ljava/lang/String;

    .line 104
    iput-object p5, p0, Lepson/print/MyPrinter;->mEmailAddress:Ljava/lang/String;

    .line 105
    iput p6, p0, Lepson/print/MyPrinter;->mLocation:I

    return-void
.end method

.method public static clearCurPrinter(Landroid/content/Context;)V
    .locals 2

    const-string v0, "PrintSetting"

    const/4 v1, 0x0

    .line 362
    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p0

    .line 363
    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p0

    const-string v0, "PRINTER_NAME"

    .line 365
    invoke-interface {p0, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v0, "PRINTER_IP"

    .line 366
    invoke-interface {p0, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v0, "PRINTER_ID"

    .line 367
    invoke-interface {p0, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v0, "PRINTER_SERIAL_NO"

    .line 368
    invoke-interface {p0, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v0, "PRINTER_EMAIL_ADDRESS"

    .line 369
    invoke-interface {p0, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v0, "PRINTER_LOCATION"

    .line 370
    invoke-interface {p0, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v0, "LANG"

    .line 371
    invoke-interface {p0, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v0, "PRINTER_ACCESS_KEY"

    .line 375
    invoke-interface {p0, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 377
    invoke-interface {p0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public static getCurPrinter(Landroid/content/Context;)Lepson/print/MyPrinter;
    .locals 3

    .line 301
    new-instance v0, Lepson/print/MyPrinter;

    invoke-direct {v0}, Lepson/print/MyPrinter;-><init>()V

    .line 304
    invoke-static {p0}, Lepson/provider/SharedPreferencesProvider;->getInstace(Landroid/content/Context;)Lepson/provider/SharedPreferencesProvider$SharedPreferencesMulti;

    move-result-object p0

    const-string v1, "PRINTER_NAME"

    const/4 v2, 0x0

    .line 310
    invoke-virtual {p0, v1, v2}, Lepson/provider/SharedPreferencesProvider$SharedPreferencesMulti;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lepson/print/MyPrinter;->mName:Ljava/lang/String;

    const-string v1, "PRINTER_ID"

    .line 312
    invoke-virtual {p0, v1, v2}, Lepson/provider/SharedPreferencesProvider$SharedPreferencesMulti;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lepson/print/MyPrinter;->mPrinterId:Ljava/lang/String;

    const-string v1, "PRINTER_IP"

    .line 314
    invoke-virtual {p0, v1, v2}, Lepson/provider/SharedPreferencesProvider$SharedPreferencesMulti;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lepson/print/MyPrinter;->mIp:Ljava/lang/String;

    const-string v1, "PRINTER_SERIAL_NO"

    .line 316
    invoke-virtual {p0, v1, v2}, Lepson/provider/SharedPreferencesProvider$SharedPreferencesMulti;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lepson/print/MyPrinter;->mSerialNo:Ljava/lang/String;

    const-string v1, "PRINTER_EMAIL_ADDRESS"

    .line 318
    invoke-virtual {p0, v1, v2}, Lepson/provider/SharedPreferencesProvider$SharedPreferencesMulti;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lepson/print/MyPrinter;->mEmailAddress:Ljava/lang/String;

    const-string v1, "PRINTER_LOCATION"

    const/4 v2, 0x0

    .line 321
    invoke-virtual {p0, v1, v2}, Lepson/provider/SharedPreferencesProvider$SharedPreferencesMulti;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, v0, Lepson/print/MyPrinter;->mLocation:I

    const-string v1, "LANG"

    const/4 v2, 0x1

    .line 323
    invoke-virtual {p0, v1, v2}, Lepson/provider/SharedPreferencesProvider$SharedPreferencesMulti;->getInt(Ljava/lang/String;I)I

    move-result p0

    iput p0, v0, Lepson/print/MyPrinter;->mLang:I

    return-object v0
.end method

.method public static getCurrectLocation(Landroid/content/Context;)I
    .locals 0

    .line 386
    invoke-static {p0}, Lepson/print/MyPrinter;->getCurPrinter(Landroid/content/Context;)Lepson/print/MyPrinter;

    move-result-object p0

    invoke-virtual {p0}, Lepson/print/MyPrinter;->getLocation()I

    move-result p0

    return p0
.end method

.method public static getPrinterDeviceId(Landroid/content/Context;Z)Ljava/lang/String;
    .locals 4

    .line 402
    invoke-static {p0}, Lepson/print/MyPrinter;->getCurPrinter(Landroid/content/Context;)Lepson/print/MyPrinter;

    move-result-object v0

    .line 403
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getName()Ljava/lang/String;

    move-result-object v1

    .line 405
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getLocation()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    if-eqz p1, :cond_0

    .line 408
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getEmailAddress()Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1}, Lepson/print/MyPrinter;->getRemotePrinterDeviceIdFromServer(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :cond_0
    return-object v1
.end method

.method static getRemotePrinterDeviceIdFromServer(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    if-eqz p1, :cond_3

    .line 420
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_0

    goto :goto_1

    .line 426
    :cond_0
    new-instance v0, Lepson/print/MyPrinter;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, v0

    move-object v6, p1

    invoke-direct/range {v1 .. v6}, Lepson/print/MyPrinter;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 427
    invoke-virtual {v0, p0}, Lepson/print/MyPrinter;->getRemotePrinterAccessKey(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 430
    invoke-static {p0, p1, v0}, Lepson/print/ecclient/EpsonConnectAccess;->getPrinterInfo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lepson/print/ecclient/EpsonConnectAccess$RemotePrinterInfo;

    move-result-object p0

    if-eqz p0, :cond_2

    .line 431
    iget-object p1, p0, Lepson/print/ecclient/EpsonConnectAccess$RemotePrinterInfo;->mDeviceId:Ljava/lang/String;

    if-nez p1, :cond_1

    goto :goto_0

    .line 435
    :cond_1
    iget-object p0, p0, Lepson/print/ecclient/EpsonConnectAccess$RemotePrinterInfo;->mDeviceId:Ljava/lang/String;

    return-object p0

    :cond_2
    :goto_0
    const-string p0, ""

    return-object p0

    :cond_3
    :goto_1
    const-string p0, ""

    return-object p0
.end method

.method public static isPrinterRouteOtg(Landroid/content/Context;)Z
    .locals 1
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 439
    invoke-static {p0}, Lepson/print/MyPrinter;->getCurPrinter(Landroid/content/Context;)Lepson/print/MyPrinter;

    move-result-object p0

    const-string v0, "USB"

    .line 440
    invoke-virtual {p0}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    return p0
.end method

.method public static isRemotePrinter(Landroid/content/Context;)Z
    .locals 1

    .line 394
    invoke-static {p0}, Lepson/print/MyPrinter;->getCurrectLocation(Landroid/content/Context;)I

    move-result p0

    const/4 v0, 0x2

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getCommonDeviceName()Ljava/lang/String;
    .locals 1

    .line 148
    iget-object v0, p0, Lepson/print/MyPrinter;->mCommonDeviceName:Ljava/lang/String;

    return-object v0
.end method

.method public getEmailAddress()Ljava/lang/String;
    .locals 1

    .line 133
    iget-object v0, p0, Lepson/print/MyPrinter;->mEmailAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getIp()Ljava/lang/String;
    .locals 1

    .line 114
    iget-object v0, p0, Lepson/print/MyPrinter;->mIp:Ljava/lang/String;

    return-object v0
.end method

.method public getLang()I
    .locals 1

    .line 241
    iget v0, p0, Lepson/print/MyPrinter;->mLang:I

    return v0
.end method

.method public getLocation()I
    .locals 1

    .line 140
    iget v0, p0, Lepson/print/MyPrinter;->mLocation:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 110
    iget-object v0, p0, Lepson/print/MyPrinter;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getPrinterId()Ljava/lang/String;
    .locals 1

    .line 125
    iget-object v0, p0, Lepson/print/MyPrinter;->mPrinterId:Ljava/lang/String;

    return-object v0
.end method

.method public getPrinterIndex()I
    .locals 1

    .line 160
    iget v0, p0, Lepson/print/MyPrinter;->mPrinterIndex:I

    return v0
.end method

.method public getRemotePrinterAccessKey(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .line 176
    iget-object v0, p0, Lepson/print/MyPrinter;->mEmailAddress:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 177
    new-instance v0, Lepson/print/EPPrinterManager;

    invoke-direct {v0, p1}, Lepson/print/EPPrinterManager;-><init>(Landroid/content/Context;)V

    .line 179
    invoke-virtual {p0}, Lepson/print/MyPrinter;->getEmailAddress()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lepson/print/EPPrinterManager;->loadRemotePrinterInfo(Ljava/lang/String;)Lepson/print/EPPrinterInfo;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 181
    iget-object p1, p1, Lepson/print/EPPrinterInfo;->printerAccessKey:Ljava/lang/String;

    return-object p1

    :cond_0
    const-string p1, ""

    return-object p1
.end method

.method public getScannerId()Ljava/lang/String;
    .locals 1

    .line 225
    iget-object v0, p0, Lepson/print/MyPrinter;->mScannerId:Ljava/lang/String;

    return-object v0
.end method

.method public getSerialNo()Ljava/lang/String;
    .locals 1

    .line 129
    iget-object v0, p0, Lepson/print/MyPrinter;->mSerialNo:Ljava/lang/String;

    return-object v0
.end method

.method public getUserDefName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .line 194
    new-instance v0, Lepson/print/EPPrinterManager;

    invoke-direct {v0, p1}, Lepson/print/EPPrinterManager;-><init>(Landroid/content/Context;)V

    .line 196
    iget p1, p0, Lepson/print/MyPrinter;->mLocation:I

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 207
    :pswitch_0
    invoke-virtual {p0}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    if-lez p1, :cond_0

    .line 208
    invoke-virtual {p0}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lepson/print/EPPrinterManager;->loadIpPrinterInfo(Ljava/lang/String;)Lepson/print/EPPrinterInfo;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 209
    iget-object v0, p1, Lepson/print/EPPrinterInfo;->userDefName:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lepson/print/EPPrinterInfo;->userDefName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 210
    iget-object p1, p1, Lepson/print/EPPrinterInfo;->userDefName:Ljava/lang/String;

    return-object p1

    .line 198
    :pswitch_1
    invoke-virtual {p0}, Lepson/print/MyPrinter;->getEmailAddress()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lepson/print/MyPrinter;->getEmailAddress()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    if-lez p1, :cond_0

    .line 199
    invoke-virtual {p0}, Lepson/print/MyPrinter;->getEmailAddress()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lepson/print/EPPrinterManager;->loadRemotePrinterInfo(Ljava/lang/String;)Lepson/print/EPPrinterInfo;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 200
    iget-object v0, p1, Lepson/print/EPPrinterInfo;->userDefName:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lepson/print/EPPrinterInfo;->userDefName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 201
    iget-object p1, p1, Lepson/print/EPPrinterInfo;->userDefName:Ljava/lang/String;

    return-object p1

    .line 216
    :cond_0
    :goto_0
    iget-object p1, p0, Lepson/print/MyPrinter;->mName:Ljava/lang/String;

    return-object p1

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setCommonDeviceName(Ljava/lang/String;)V
    .locals 0

    .line 152
    iput-object p1, p0, Lepson/print/MyPrinter;->mCommonDeviceName:Ljava/lang/String;

    return-void
.end method

.method public setCurPrinter(Landroid/content/Context;)V
    .locals 2

    const-string v0, "PrintSetting"

    const/4 v1, 0x0

    .line 337
    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    .line 338
    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    const-string v0, "PRINTER_NAME"

    .line 340
    invoke-virtual {p0}, Lepson/print/MyPrinter;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v0, "PRINTER_IP"

    .line 341
    invoke-virtual {p0}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v0, "PRINTER_ID"

    .line 342
    invoke-virtual {p0}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v0, "PRINTER_SERIAL_NO"

    .line 343
    invoke-virtual {p0}, Lepson/print/MyPrinter;->getSerialNo()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v0, "PRINTER_EMAIL_ADDRESS"

    .line 344
    invoke-virtual {p0}, Lepson/print/MyPrinter;->getEmailAddress()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v0, "PRINTER_LOCATION"

    .line 345
    invoke-virtual {p0}, Lepson/print/MyPrinter;->getLocation()I

    move-result v1

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v0, "LANG"

    .line 346
    invoke-virtual {p0}, Lepson/print/MyPrinter;->getLang()I

    move-result v1

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v0, "PRINTER_ACCESS_KEY"

    .line 350
    invoke-interface {p1, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 352
    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public setIp(Ljava/lang/String;)V
    .locals 0

    .line 121
    iput-object p1, p0, Lepson/print/MyPrinter;->mIp:Ljava/lang/String;

    return-void
.end method

.method public setLang(I)V
    .locals 0

    .line 248
    iput p1, p0, Lepson/print/MyPrinter;->mLang:I

    return-void
.end method

.method public setLocation(I)V
    .locals 0

    .line 144
    iput p1, p0, Lepson/print/MyPrinter;->mLocation:I

    return-void
.end method

.method public setPrinterIndex(I)V
    .locals 0

    .line 168
    iput p1, p0, Lepson/print/MyPrinter;->mPrinterIndex:I

    return-void
.end method

.method public setScannerId(Ljava/lang/String;)V
    .locals 0

    .line 232
    iput-object p1, p0, Lepson/print/MyPrinter;->mScannerId:Ljava/lang/String;

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 282
    iget-object p2, p0, Lepson/print/MyPrinter;->mName:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 283
    iget-object p2, p0, Lepson/print/MyPrinter;->mIp:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 284
    iget-object p2, p0, Lepson/print/MyPrinter;->mPrinterId:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 285
    iget-object p2, p0, Lepson/print/MyPrinter;->mSerialNo:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 286
    iget-object p2, p0, Lepson/print/MyPrinter;->mEmailAddress:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 287
    iget p2, p0, Lepson/print/MyPrinter;->mLocation:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 288
    iget-object p2, p0, Lepson/print/MyPrinter;->mScannerId:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 289
    iget p2, p0, Lepson/print/MyPrinter;->mLang:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 290
    iget p2, p0, Lepson/print/MyPrinter;->mPrinterIndex:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method

.class Lepson/print/ActivityViewImageSelect$AllReloadTask;
.super Lepson/print/Util/AsyncTaskExecutor;
.source "ActivityViewImageSelect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/ActivityViewImageSelect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AllReloadTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lepson/print/Util/AsyncTaskExecutor<",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private context:Landroid/content/Context;

.field private volatile endRedraw:Z

.field private firstDraw:Z

.field handler:Landroid/os/Handler;

.field private index:I

.field private mDeletePreview:Z

.field final synthetic this$0:Lepson/print/ActivityViewImageSelect;


# direct methods
.method public constructor <init>(Lepson/print/ActivityViewImageSelect;Landroid/content/Context;)V
    .locals 1

    .line 2157
    iput-object p1, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-direct {p0}, Lepson/print/Util/AsyncTaskExecutor;-><init>()V

    const/4 p1, 0x0

    .line 2131
    iput-object p1, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->context:Landroid/content/Context;

    const/4 p1, 0x0

    .line 2132
    iput-boolean p1, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->endRedraw:Z

    const/4 v0, 0x1

    .line 2133
    iput-boolean v0, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->firstDraw:Z

    .line 2134
    iput p1, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->index:I

    .line 2137
    new-instance p1, Landroid/os/Handler;

    new-instance v0, Lepson/print/ActivityViewImageSelect$AllReloadTask$1;

    invoke-direct {v0, p0}, Lepson/print/ActivityViewImageSelect$AllReloadTask$1;-><init>(Lepson/print/ActivityViewImageSelect$AllReloadTask;)V

    invoke-direct {p1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object p1, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->handler:Landroid/os/Handler;

    .line 2158
    iput-object p2, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->context:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$2400(Lepson/print/ActivityViewImageSelect$AllReloadTask;)I
    .locals 0

    .line 2129
    iget p0, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->index:I

    return p0
.end method

.method static synthetic access$2500(Lepson/print/ActivityViewImageSelect$AllReloadTask;)Z
    .locals 0

    .line 2129
    iget-boolean p0, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->firstDraw:Z

    return p0
.end method

.method static synthetic access$2502(Lepson/print/ActivityViewImageSelect$AllReloadTask;Z)Z
    .locals 0

    .line 2129
    iput-boolean p1, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->firstDraw:Z

    return p1
.end method

.method static synthetic access$2602(Lepson/print/ActivityViewImageSelect$AllReloadTask;Z)Z
    .locals 0

    .line 2129
    iput-boolean p1, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->endRedraw:Z

    return p1
.end method

.method private showCurrentImage()V
    .locals 5

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 2344
    :goto_0
    :try_start_0
    iget-object v2, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    iget-object v2, v2, Lepson/print/ActivityViewImageSelect;->previewImageView:Lcom/epson/iprint/apf/ApfPreviewView;

    iget-object v3, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {v3}, Lepson/print/ActivityViewImageSelect;->access$100(Lepson/print/ActivityViewImageSelect;)I

    move-result v3

    iget-object v4, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-virtual {v2, v3, v0, v4}, Lcom/epson/iprint/apf/ApfPreviewView;->setImage(IILandroid/app/Activity;)Z
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    nop

    if-nez v1, :cond_0

    .line 2350
    iget-object v1, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    iget-object v1, v1, Lepson/print/ActivityViewImageSelect;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    const/4 v1, 0x1

    .line 2354
    iget-object v2, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {v2}, Lepson/print/ActivityViewImageSelect;->access$1600(Lepson/print/ActivityViewImageSelect;)Lepson/print/phlayout/PhotoPreviewImageList;

    move-result-object v2

    invoke-virtual {v2}, Lepson/print/phlayout/PhotoPreviewImageList;->size()I

    move-result v2

    if-gtz v2, :cond_1

    :goto_1
    return-void

    .line 2360
    :cond_1
    iget-object v2, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {v2}, Lepson/print/ActivityViewImageSelect;->access$100(Lepson/print/ActivityViewImageSelect;)I

    move-result v2

    if-ltz v2, :cond_2

    iget-object v2, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {v2}, Lepson/print/ActivityViewImageSelect;->access$100(Lepson/print/ActivityViewImageSelect;)I

    move-result v2

    iget-object v3, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {v3}, Lepson/print/ActivityViewImageSelect;->access$1600(Lepson/print/ActivityViewImageSelect;)Lepson/print/phlayout/PhotoPreviewImageList;

    move-result-object v3

    invoke-virtual {v3}, Lepson/print/phlayout/PhotoPreviewImageList;->size()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 2361
    iget-object v2, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {v2}, Lepson/print/ActivityViewImageSelect;->access$1600(Lepson/print/ActivityViewImageSelect;)Lepson/print/phlayout/PhotoPreviewImageList;

    move-result-object v2

    iget-object v3, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {v3}, Lepson/print/ActivityViewImageSelect;->access$100(Lepson/print/ActivityViewImageSelect;)I

    move-result v3

    invoke-virtual {v2, v3}, Lepson/print/phlayout/PhotoPreviewImageList;->remove(I)V

    .line 2365
    :cond_2
    iget-object v2, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {v2, v0}, Lepson/print/ActivityViewImageSelect;->access$102(Lepson/print/ActivityViewImageSelect;I)I

    goto :goto_0
.end method

.method private testCheckStatus()Z
    .locals 2

    const-wide/16 v0, 0x3e8

    .line 2376
    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 2378
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 2380
    :goto_0
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {v0}, Lepson/print/ActivityViewImageSelect;->access$3200(Lepson/print/ActivityViewImageSelect;)Z

    move-result v0

    return v0
.end method

.method private updatePreviewImage()V
    .locals 6

    .line 2258
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    iget-object v0, v0, Lepson/print/ActivityViewImageSelect;->previewImageView:Lcom/epson/iprint/apf/ApfPreviewView;

    invoke-virtual {v0}, Lcom/epson/iprint/apf/ApfPreviewView;->invalidateImageNo()V

    .line 2261
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    iget-object v0, v0, Lepson/print/ActivityViewImageSelect;->previewImageView:Lcom/epson/iprint/apf/ApfPreviewView;

    invoke-virtual {v0}, Lcom/epson/iprint/apf/ApfPreviewView;->invalidatePreview()V

    const/4 v0, 0x0

    .line 2263
    iput v0, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->index:I

    :goto_0
    iget v1, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->index:I

    iget-object v2, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {v2}, Lepson/print/ActivityViewImageSelect;->access$1600(Lepson/print/ActivityViewImageSelect;)Lepson/print/phlayout/PhotoPreviewImageList;

    move-result-object v2

    invoke-virtual {v2}, Lepson/print/phlayout/PhotoPreviewImageList;->size()I

    move-result v2

    const/4 v3, 0x2

    if-ge v1, v2, :cond_5

    const-string v1, "ActivityViewImageSelect"

    .line 2264
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "doInBack Start -> "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v4, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->index:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    const/4 v1, 0x1

    .line 2270
    :try_start_0
    iget-object v2, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {v2}, Lepson/print/ActivityViewImageSelect;->access$3200(Lepson/print/ActivityViewImageSelect;)Z

    move-result v2

    if-eqz v2, :cond_0

    return-void

    .line 2273
    :cond_0
    iget-object v2, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    iget-object v2, v2, Lepson/print/ActivityViewImageSelect;->previewImageView:Lcom/epson/iprint/apf/ApfPreviewView;

    iget v4, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->index:I

    iget-object v5, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-virtual {v2, v4, v3, v5}, Lcom/epson/iprint/apf/ApfPreviewView;->setImage(IILandroid/app/Activity;)Z

    move-result v2

    if-ne v2, v1, :cond_2

    .line 2279
    iput-boolean v0, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->endRedraw:Z

    .line 2281
    new-array v2, v1, [Ljava/lang/Integer;

    iget v3, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->index:I

    add-int/2addr v3, v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p0, v2}, Lepson/print/ActivityViewImageSelect$AllReloadTask;->publishProgress([Ljava/lang/Object;)V

    const/4 v2, 0x0

    .line 2285
    :cond_1
    iget-boolean v3, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->endRedraw:Z

    if-nez v3, :cond_4

    const-wide/16 v3, 0x64

    .line 2286
    invoke-static {v3, v4, v0}, Ljava/lang/Thread;->sleep(JI)V

    add-int/2addr v2, v1

    const/16 v3, 0x32

    if-lt v2, v3, :cond_1

    goto :goto_2

    :cond_2
    const-wide/16 v4, 0x12c

    .line 2277
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    nop

    .line 2298
    iget-object v2, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {v2}, Lepson/print/ActivityViewImageSelect;->access$1600(Lepson/print/ActivityViewImageSelect;)Lepson/print/phlayout/PhotoPreviewImageList;

    move-result-object v2

    iget v3, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->index:I

    invoke-virtual {v2, v3}, Lepson/print/phlayout/PhotoPreviewImageList;->remove(I)V

    .line 2299
    iget v2, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->index:I

    sub-int/2addr v2, v1

    iput v2, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->index:I

    .line 2301
    iput-boolean v1, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->mDeletePreview:Z

    .line 2304
    iget-object v2, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {v2}, Lepson/print/ActivityViewImageSelect;->access$3300(Lepson/print/ActivityViewImageSelect;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2305
    iget-object v2, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    iget-object v2, v2, Lepson/print/ActivityViewImageSelect;->mHandler:Landroid/os/Handler;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 2307
    iget-object v2, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {v2, v0}, Lepson/print/ActivityViewImageSelect;->access$102(Lepson/print/ActivityViewImageSelect;I)I

    goto :goto_3

    .line 2310
    :cond_3
    iget-object v2, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    iget-object v2, v2, Lepson/print/ActivityViewImageSelect;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x7

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_3

    :catch_1
    move-exception v2

    .line 2295
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    :cond_4
    :goto_2
    const-string v2, "ActivityViewImageSelect"

    .line 2315
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "doInBack End -> "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v4, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->index:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2263
    :goto_3
    iget v2, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->index:I

    add-int/2addr v2, v1

    iput v2, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->index:I

    goto/16 :goto_0

    .line 2320
    :cond_5
    iget-object v1, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {v1}, Lepson/print/ActivityViewImageSelect;->access$1600(Lepson/print/ActivityViewImageSelect;)Lepson/print/phlayout/PhotoPreviewImageList;

    move-result-object v1

    invoke-virtual {v1}, Lepson/print/phlayout/PhotoPreviewImageList;->size()I

    move-result v1

    if-lez v1, :cond_6

    .line 2321
    invoke-direct {p0}, Lepson/print/ActivityViewImageSelect$AllReloadTask;->showCurrentImage()V

    goto :goto_4

    .line 2325
    :cond_6
    :try_start_1
    iget-object v1, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    iget-object v1, v1, Lepson/print/ActivityViewImageSelect;->previewImageView:Lcom/epson/iprint/apf/ApfPreviewView;

    iget-object v2, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-virtual {v1, v0, v3, v2}, Lcom/epson/iprint/apf/ApfPreviewView;->setImage(IILandroid/app/Activity;)Z
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_2

    :catch_2
    :goto_4
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 6

    .line 2182
    invoke-static {}, Ljava/lang/System;->gc()V

    const/4 p1, 0x0

    const/4 v0, 0x0

    :cond_0
    const/4 v1, 0x1

    .line 2188
    :try_start_0
    iget-object v2, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {v2}, Lepson/print/ActivityViewImageSelect;->access$2700(Lepson/print/ActivityViewImageSelect;)Lepson/print/service/IEpsonService;

    move-result-object v2

    if-nez v2, :cond_1

    const-wide/16 v2, 0x64

    .line 2189
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V

    add-int/2addr v0, v1

    const/16 v2, 0x64

    if-lt v0, v2, :cond_0

    .line 2198
    :cond_1
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {v0}, Lepson/print/ActivityViewImageSelect;->access$2700(Lepson/print/ActivityViewImageSelect;)Lepson/print/service/IEpsonService;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2199
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {v0}, Lepson/print/ActivityViewImageSelect;->access$2800(Lepson/print/ActivityViewImageSelect;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2200
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {v0}, Lepson/print/ActivityViewImageSelect;->access$2700(Lepson/print/ActivityViewImageSelect;)Lepson/print/service/IEpsonService;

    move-result-object v0

    sget-object v2, Lepson/print/screen/PrintSetting$Kind;->photo:Lepson/print/screen/PrintSetting$Kind;

    invoke-virtual {v2}, Lepson/print/screen/PrintSetting$Kind;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lepson/print/service/IEpsonService;->EpsonConnectUpdatePrinterSettings(Ljava/lang/String;)I

    goto :goto_0

    .line 2205
    :cond_2
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {v0}, Lepson/print/ActivityViewImageSelect;->access$2700(Lepson/print/ActivityViewImageSelect;)Lepson/print/service/IEpsonService;

    move-result-object v0

    sget-object v2, Lepson/print/screen/PrintSetting$Kind;->photo:Lepson/print/screen/PrintSetting$Kind;

    invoke-virtual {v2}, Lepson/print/screen/PrintSetting$Kind;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lepson/print/service/IEpsonService;->updatePrinterSettings(Ljava/lang/String;)I

    .line 2213
    :cond_3
    :goto_0
    new-instance v0, Lepson/print/screen/PrintSetting;

    iget-object v2, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    sget-object v3, Lepson/print/screen/PrintSetting$Kind;->photo:Lepson/print/screen/PrintSetting$Kind;

    invoke-direct {v0, v2, v3}, Lepson/print/screen/PrintSetting;-><init>(Landroid/content/Context;Lepson/print/screen/PrintSetting$Kind;)V

    .line 2214
    invoke-virtual {v0}, Lepson/print/screen/PrintSetting;->loadSettings()V

    .line 2216
    iget-object v2, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    iget v3, v0, Lepson/print/screen/PrintSetting;->paperSizeValue:I

    invoke-static {v2, v3}, Lepson/print/ActivityViewImageSelect;->access$1302(Lepson/print/ActivityViewImageSelect;I)I

    .line 2217
    iget-object v2, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    iget v3, v0, Lepson/print/screen/PrintSetting;->layoutValue:I

    invoke-static {v2, v3}, Lepson/print/ActivityViewImageSelect;->access$2902(Lepson/print/ActivityViewImageSelect;I)I

    .line 2218
    iget-object v2, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    iget v3, v0, Lepson/print/screen/PrintSetting;->layoutMultiPageValue:I

    invoke-static {v2, v3}, Lepson/print/ActivityViewImageSelect;->access$3002(Lepson/print/ActivityViewImageSelect;I)I

    .line 2219
    iget-object v2, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    iget v3, v0, Lepson/print/screen/PrintSetting;->colorValue:I

    invoke-static {v2, v3}, Lepson/print/ActivityViewImageSelect;->access$3102(Lepson/print/ActivityViewImageSelect;I)I

    .line 2222
    iget-object v2, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    iget-object v2, v2, Lepson/print/ActivityViewImageSelect;->previewImageView:Lcom/epson/iprint/apf/ApfPreviewView;

    iget-object v3, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {v3}, Lepson/print/ActivityViewImageSelect;->access$2900(Lepson/print/ActivityViewImageSelect;)I

    move-result v3

    iget-object v4, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {v4}, Lepson/print/ActivityViewImageSelect;->access$3000(Lepson/print/ActivityViewImageSelect;)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/epson/iprint/apf/ApfPreviewView;->setLayout(II)V

    .line 2223
    iget-object v2, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    iget-object v2, v2, Lepson/print/ActivityViewImageSelect;->previewImageView:Lcom/epson/iprint/apf/ApfPreviewView;

    iget-object v3, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {v3}, Lepson/print/ActivityViewImageSelect;->access$1300(Lepson/print/ActivityViewImageSelect;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/epson/iprint/apf/ApfPreviewView;->setPaper(I)Z

    .line 2224
    iget-object v2, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    iget-object v2, v2, Lepson/print/ActivityViewImageSelect;->previewImageView:Lcom/epson/iprint/apf/ApfPreviewView;

    iget-object v3, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {v3}, Lepson/print/ActivityViewImageSelect;->access$3100(Lepson/print/ActivityViewImageSelect;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/epson/iprint/apf/ApfPreviewView;->setColor(I)V

    .line 2225
    iget-object v2, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    iget-object v2, v2, Lepson/print/ActivityViewImageSelect;->previewImageView:Lcom/epson/iprint/apf/ApfPreviewView;

    iget-object v3, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-virtual {v3, v0}, Lepson/print/ActivityViewImageSelect;->getApfValue(Lepson/print/screen/PrintSetting;)I

    move-result v3

    iget-object v4, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    .line 2226
    invoke-virtual {v4, v0}, Lepson/print/ActivityViewImageSelect;->getSharpnessValue(Lepson/print/screen/PrintSetting;)I

    move-result v4

    iget-object v5, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-virtual {v5, v0}, Lepson/print/ActivityViewImageSelect;->getClearlyVividValue(Lepson/print/screen/PrintSetting;)I

    move-result v0

    .line 2225
    invoke-virtual {v2, v3, v4, v0}, Lcom/epson/iprint/apf/ApfPreviewView;->setApfLibParams(III)V

    .line 2228
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    iget-object v0, v0, Lepson/print/ActivityViewImageSelect;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x6

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    .line 2232
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v0

    .line 2230
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 2237
    :cond_4
    :goto_1
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {v0, p1}, Lepson/print/ActivityViewImageSelect;->access$3202(Lepson/print/ActivityViewImageSelect;Z)Z

    .line 2239
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    iget-object v0, v0, Lepson/print/ActivityViewImageSelect;->previewImageView:Lcom/epson/iprint/apf/ApfPreviewView;

    iget-object v2, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {v2}, Lepson/print/ActivityViewImageSelect;->access$1300(Lepson/print/ActivityViewImageSelect;)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/epson/iprint/apf/ApfPreviewView;->setPaper(I)Z

    .line 2240
    invoke-direct {p0}, Lepson/print/ActivityViewImageSelect$AllReloadTask;->updatePreviewImage()V

    .line 2241
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-virtual {v0}, Lepson/print/ActivityViewImageSelect;->lastCheckAllReloadTaskRetryRequest()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2244
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {p1, v1}, Lepson/print/ActivityViewImageSelect;->access$3302(Lepson/print/ActivityViewImageSelect;Z)Z

    .line 2246
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 2129
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/print/ActivityViewImageSelect$AllReloadTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic executeOnExecutor([Ljava/lang/Object;)Landroid/os/AsyncTask;
    .locals 0

    .line 2129
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/print/ActivityViewImageSelect$AllReloadTask;->executeOnExecutor([Ljava/lang/Void;)Landroid/os/AsyncTask;

    move-result-object p1

    return-object p1
.end method

.method public varargs executeOnExecutor([Ljava/lang/Void;)Landroid/os/AsyncTask;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Landroid/os/AsyncTask<",
            "Ljava/lang/Void;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 2163
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-virtual {v0}, Lepson/print/ActivityViewImageSelect;->executeAllReloadTaskAgain()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 2166
    :cond_0
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lepson/print/ActivityViewImageSelect;->mIsAllReloadTaskRetryAcceptable:Z

    .line 2168
    invoke-super {p0, p1}, Lepson/print/Util/AsyncTaskExecutor;->executeOnExecutor([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-result-object p1

    return-object p1
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 1

    .line 2401
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-virtual {p1}, Lepson/print/ActivityViewImageSelect;->isFinishing()Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    .line 2405
    :cond_0
    iget-boolean p1, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->mDeletePreview:Z

    if-eqz p1, :cond_1

    .line 2408
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {p1}, Lepson/print/ActivityViewImageSelect;->access$2000(Lepson/print/ActivityViewImageSelect;)V

    .line 2413
    :cond_1
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    iget-object p1, p1, Lepson/print/ActivityViewImageSelect;->previewImageView:Lcom/epson/iprint/apf/ApfPreviewView;

    invoke-virtual {p1}, Lcom/epson/iprint/apf/ApfPreviewView;->invalidate()V

    .line 2414
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-virtual {p1}, Lepson/print/ActivityViewImageSelect;->updateScreen()V

    .line 2416
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    iget-object p1, p1, Lepson/print/ActivityViewImageSelect;->previewImageView:Lcom/epson/iprint/apf/ApfPreviewView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/epson/iprint/apf/ApfPreviewView;->setDrawEndHandler(Landroid/os/Handler;)V

    .line 2418
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {p1}, Lepson/print/ActivityViewImageSelect;->access$900(Lepson/print/ActivityViewImageSelect;)Lepson/common/DialogProgressViewModel;

    move-result-object p1

    const-string v0, "dialog_all_update"

    invoke-virtual {p1, v0}, Lepson/common/DialogProgressViewModel;->doDismiss(Ljava/lang/String;)V

    .line 2421
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    iget-boolean p1, p1, Lepson/print/ActivityViewImageSelect;->bAutoStartPrint:Z

    if-eqz p1, :cond_2

    .line 2423
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {p1}, Lepson/print/ActivityViewImageSelect;->access$1800(Lepson/print/ActivityViewImageSelect;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {p1, v0}, Lepson/print/ActivityViewImageSelect;->onClick(Landroid/view/View;)V

    const-string p1, "ActivityViewImageSelect"

    const-string v0, "onClick(printButton)"

    .line 2424
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2425
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    const/4 v0, 0x0

    iput-boolean v0, p1, Lepson/print/ActivityViewImageSelect;->bAutoStartPrint:Z

    :cond_2
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 2129
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lepson/print/ActivityViewImageSelect$AllReloadTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .line 2173
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {v0}, Lepson/print/ActivityViewImageSelect;->access$900(Lepson/print/ActivityViewImageSelect;)Lepson/common/DialogProgressViewModel;

    move-result-object v0

    const-string v1, "dialog_all_update"

    invoke-virtual {v0, v1}, Lepson/common/DialogProgressViewModel;->doShow(Ljava/lang/String;)V

    .line 2176
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {v0}, Lepson/print/ActivityViewImageSelect;->access$1600(Lepson/print/ActivityViewImageSelect;)Lepson/print/phlayout/PhotoPreviewImageList;

    move-result-object v0

    invoke-virtual {v0}, Lepson/print/phlayout/PhotoPreviewImageList;->clearExceptSrcFileName()V

    .line 2177
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    iget-object v0, v0, Lepson/print/ActivityViewImageSelect;->previewImageView:Lcom/epson/iprint/apf/ApfPreviewView;

    iget-object v1, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/epson/iprint/apf/ApfPreviewView;->setDrawEndHandler(Landroid/os/Handler;)V

    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 3

    .line 2386
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    iget-object v0, v0, Lepson/print/ActivityViewImageSelect;->previewImageView:Lcom/epson/iprint/apf/ApfPreviewView;

    invoke-virtual {v0}, Lcom/epson/iprint/apf/ApfPreviewView;->invalidate()V

    .line 2389
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {v0}, Lepson/print/ActivityViewImageSelect;->access$1900(Lepson/print/ActivityViewImageSelect;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x0

    aget-object p1, p1, v2

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, "/"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {p1}, Lepson/print/ActivityViewImageSelect;->access$1600(Lepson/print/ActivityViewImageSelect;)Lepson/print/phlayout/PhotoPreviewImageList;

    move-result-object p1

    invoke-virtual {p1}, Lepson/print/phlayout/PhotoPreviewImageList;->size()I

    move-result p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2392
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    iget-object p1, p1, Lepson/print/ActivityViewImageSelect;->previewImageView:Lcom/epson/iprint/apf/ApfPreviewView;

    invoke-virtual {p1}, Lcom/epson/iprint/apf/ApfPreviewView;->getIsPaperLandscape()Z

    move-result p1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 2393
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {p1, v0}, Lepson/print/ActivityViewImageSelect;->access$3402(Lepson/print/ActivityViewImageSelect;Z)Z

    goto :goto_0

    .line 2395
    :cond_0
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$AllReloadTask;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {p1, v2}, Lepson/print/ActivityViewImageSelect;->access$3402(Lepson/print/ActivityViewImageSelect;Z)Z

    :goto_0
    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .line 2129
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lepson/print/ActivityViewImageSelect$AllReloadTask;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method

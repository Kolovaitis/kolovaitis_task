.class Lepson/print/ActivityDocsPrintPreview$PrintUploadTask;
.super Landroid/os/AsyncTask;
.source "ActivityDocsPrintPreview.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/ActivityDocsPrintPreview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PrintUploadTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field serverConnectDialog:Lepson/print/ActivityDocsPrintPreview$CustomProDialog;

.field final synthetic this$0:Lepson/print/ActivityDocsPrintPreview;


# direct methods
.method private constructor <init>(Lepson/print/ActivityDocsPrintPreview;)V
    .locals 0

    .line 2453
    iput-object p1, p0, Lepson/print/ActivityDocsPrintPreview$PrintUploadTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lepson/print/ActivityDocsPrintPreview;Lepson/print/ActivityDocsPrintPreview$1;)V
    .locals 0

    .line 2453
    invoke-direct {p0, p1}, Lepson/print/ActivityDocsPrintPreview$PrintUploadTask;-><init>(Lepson/print/ActivityDocsPrintPreview;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 2453
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lepson/print/ActivityDocsPrintPreview$PrintUploadTask;->doInBackground([Ljava/lang/String;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Void;
    .locals 21

    move-object/from16 v1, p0

    .line 2466
    iget-object v0, v1, Lepson/print/ActivityDocsPrintPreview$PrintUploadTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    iget-boolean v0, v0, Lepson/print/ActivityDocsPrintPreview;->isCreateJobDone:Z

    if-nez v0, :cond_3

    const/4 v0, 0x0

    .line 2470
    :try_start_0
    new-instance v2, Lepson/print/screen/PrintSetting;

    iget-object v3, v1, Lepson/print/ActivityDocsPrintPreview$PrintUploadTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    sget-object v4, Lepson/print/screen/PrintSetting$Kind;->document:Lepson/print/screen/PrintSetting$Kind;

    invoke-direct {v2, v3, v4}, Lepson/print/screen/PrintSetting;-><init>(Landroid/content/Context;Lepson/print/screen/PrintSetting$Kind;)V

    .line 2471
    invoke-virtual {v2}, Lepson/print/screen/PrintSetting;->loadSettings()V

    .line 2473
    iget v8, v2, Lepson/print/screen/PrintSetting;->paperSizeValue:I

    .line 2474
    iget v9, v2, Lepson/print/screen/PrintSetting;->paperTypeValue:I

    .line 2475
    iget v10, v2, Lepson/print/screen/PrintSetting;->layoutValue:I

    .line 2476
    iget v11, v2, Lepson/print/screen/PrintSetting;->qualityValue:I

    .line 2477
    iget v12, v2, Lepson/print/screen/PrintSetting;->paperSourceValue:I

    .line 2478
    iget v13, v2, Lepson/print/screen/PrintSetting;->colorValue:I

    .line 2480
    iget v14, v2, Lepson/print/screen/PrintSetting;->brightnessValue:I

    .line 2481
    iget v15, v2, Lepson/print/screen/PrintSetting;->contrastValue:I

    .line 2482
    iget v3, v2, Lepson/print/screen/PrintSetting;->saturationValue:I

    .line 2483
    iget v4, v2, Lepson/print/screen/PrintSetting;->duplexValue:I

    .line 2484
    iget v2, v2, Lepson/print/screen/PrintSetting;->feedDirectionValue:I

    const/4 v6, 0x1

    const/16 v19, 0x57b

    const/16 v20, 0x57b

    .line 2493
    iget-object v5, v1, Lepson/print/ActivityDocsPrintPreview$PrintUploadTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v5}, Lepson/print/ActivityDocsPrintPreview;->access$800(Lepson/print/ActivityDocsPrintPreview;)Lepson/print/service/IEpsonService;

    move-result-object v5

    invoke-interface {v5}, Lepson/print/service/IEpsonService;->EpsonConnectEndJob()I

    .line 2496
    iget-object v5, v1, Lepson/print/ActivityDocsPrintPreview$PrintUploadTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    aget-object v7, p1, v0

    invoke-virtual {v5, v7}, Lepson/print/ActivityDocsPrintPreview;->checkJobNameLength(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2502
    iget-object v5, v1, Lepson/print/ActivityDocsPrintPreview$PrintUploadTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v5}, Lepson/print/ActivityDocsPrintPreview;->access$800(Lepson/print/ActivityDocsPrintPreview;)Lepson/print/service/IEpsonService;

    move-result-object v5

    aget-object v7, p1, v0

    move/from16 v16, v3

    move/from16 v17, v2

    move/from16 v18, v4

    invoke-interface/range {v5 .. v20}, Lepson/print/service/IEpsonService;->EpsonConnectCreateJob(ILjava/lang/String;IIIIIIIIIIIII)I

    move-result v2
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-nez v2, :cond_1

    .line 2509
    :try_start_1
    iget-object v3, v1, Lepson/print/ActivityDocsPrintPreview$PrintUploadTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    const/4 v4, 0x1

    iput-boolean v4, v3, Lepson/print/ActivityDocsPrintPreview;->isCreateJobDone:Z

    .line 2512
    iget-object v3, v1, Lepson/print/ActivityDocsPrintPreview$PrintUploadTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v3}, Lepson/print/ActivityDocsPrintPreview;->access$800(Lepson/print/ActivityDocsPrintPreview;)Lepson/print/service/IEpsonService;

    move-result-object v3

    aget-object v0, p1, v0

    invoke-interface {v3, v0, v4}, Lepson/print/service/IEpsonService;->EpsonConnectUploadFile(Ljava/lang/String;I)I

    move-result v0
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-nez v0, :cond_0

    .line 2518
    :try_start_2
    iget-object v2, v1, Lepson/print/ActivityDocsPrintPreview$PrintUploadTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    iget-object v2, v2, Lepson/print/ActivityDocsPrintPreview;->mUiHandler:Landroid/os/Handler;

    const/16 v3, 0x16

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    .line 2514
    :cond_0
    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2}, Ljava/lang/Exception;-><init>()V

    throw v2
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :catch_0
    move v0, v2

    goto :goto_0

    .line 2507
    :cond_1
    :try_start_3
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    throw v0
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    :cond_2
    const/4 v0, -0x1

    .line 2498
    :try_start_4
    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2}, Ljava/lang/Exception;-><init>()V

    throw v2
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    .line 2523
    :catch_1
    :goto_0
    new-instance v2, Landroid/os/Message;

    invoke-direct {v2}, Landroid/os/Message;-><init>()V

    const/16 v3, 0x27

    .line 2524
    iput v3, v2, Landroid/os/Message;->what:I

    .line 2525
    iput v0, v2, Landroid/os/Message;->arg1:I

    .line 2526
    iget-object v0, v1, Lepson/print/ActivityDocsPrintPreview$PrintUploadTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    iget-object v0, v0, Lepson/print/ActivityDocsPrintPreview;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1

    :catch_2
    move-exception v0

    .line 2520
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_3
    :goto_1
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 2453
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/print/ActivityDocsPrintPreview$PrintUploadTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 0

    .line 2535
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$PrintUploadTask;->serverConnectDialog:Lepson/print/ActivityDocsPrintPreview$CustomProDialog;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lepson/print/ActivityDocsPrintPreview$CustomProDialog;->isShowing()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 2536
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$PrintUploadTask;->serverConnectDialog:Lepson/print/ActivityDocsPrintPreview$CustomProDialog;

    invoke-virtual {p1}, Lepson/print/ActivityDocsPrintPreview$CustomProDialog;->dismiss()V

    :cond_0
    return-void
.end method

.method protected onPreExecute()V
    .locals 7

    .line 2458
    new-instance v6, Lepson/print/ActivityDocsPrintPreview$CustomProDialog;

    iget-object v2, p0, Lepson/print/ActivityDocsPrintPreview$PrintUploadTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    const v3, 0x7f0f0009

    const v4, 0x7f0a00cc

    const/4 v5, 0x0

    move-object v0, v6

    move-object v1, v2

    invoke-direct/range {v0 .. v5}, Lepson/print/ActivityDocsPrintPreview$CustomProDialog;-><init>(Lepson/print/ActivityDocsPrintPreview;Landroid/content/Context;IIZ)V

    iput-object v6, p0, Lepson/print/ActivityDocsPrintPreview$PrintUploadTask;->serverConnectDialog:Lepson/print/ActivityDocsPrintPreview$CustomProDialog;

    .line 2459
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$PrintUploadTask;->serverConnectDialog:Lepson/print/ActivityDocsPrintPreview$CustomProDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lepson/print/ActivityDocsPrintPreview$CustomProDialog;->setCancelable(Z)V

    .line 2460
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$PrintUploadTask;->serverConnectDialog:Lepson/print/ActivityDocsPrintPreview$CustomProDialog;

    invoke-virtual {v0}, Lepson/print/ActivityDocsPrintPreview$CustomProDialog;->show()V

    return-void
.end method

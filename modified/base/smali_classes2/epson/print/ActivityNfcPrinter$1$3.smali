.class Lepson/print/ActivityNfcPrinter$1$3;
.super Landroid/os/AsyncTask;
.source "ActivityNfcPrinter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/ActivityNfcPrinter$1;->handleMessage(Landroid/os/Message;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lepson/print/ActivityNfcPrinter$1;

.field final synthetic val$printer:Lepson/print/MyPrinter;


# direct methods
.method constructor <init>(Lepson/print/ActivityNfcPrinter$1;Lepson/print/MyPrinter;)V
    .locals 0

    .line 666
    iput-object p1, p0, Lepson/print/ActivityNfcPrinter$1$3;->this$1:Lepson/print/ActivityNfcPrinter$1;

    iput-object p2, p0, Lepson/print/ActivityNfcPrinter$1$3;->val$printer:Lepson/print/MyPrinter;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 4

    .line 676
    invoke-static {}, Lepson/print/ActivityNfcPrinter;->access$400()Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object p1

    iget-object v0, p0, Lepson/print/ActivityNfcPrinter$1$3;->val$printer:Lepson/print/MyPrinter;

    .line 677
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lepson/print/ActivityNfcPrinter$1$3;->val$printer:Lepson/print/MyPrinter;

    invoke-virtual {v1}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x3c

    const/4 v3, 0x3

    .line 676
    invoke-virtual {p1, v2, v0, v1, v3}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doProbePrinter(ILjava/lang/String;Ljava/lang/String;I)I

    move-result p1

    if-eqz p1, :cond_0

    .line 680
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    .line 684
    :cond_0
    invoke-static {}, Lepson/print/ActivityNfcPrinter;->access$400()Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doSetPrinter()I

    move-result p1

    if-eqz p1, :cond_1

    .line 686
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    .line 690
    :cond_1
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1$3;->val$printer:Lepson/print/MyPrinter;

    invoke-static {}, Lepson/print/ActivityNfcPrinter;->access$400()Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object v0

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doGetLang()I

    move-result v0

    invoke-virtual {p1, v0}, Lepson/print/MyPrinter;->setLang(I)V

    .line 693
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1$3;->this$1:Lepson/print/ActivityNfcPrinter$1;

    iget-object p1, p1, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    invoke-static {p1}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object p1

    .line 696
    invoke-virtual {p1}, Lepson/common/ExternalFileUtils;->getSupportedMediaDir()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lepson/common/ExternalFileUtils;->createTempFolder(Ljava/lang/String;)Z

    .line 697
    invoke-static {}, Lepson/print/ActivityNfcPrinter;->access$400()Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object v0

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->getMEscpLib()Lcom/epson/mobilephone/common/escpr/EscprLib;

    move-result-object v0

    iget-object v1, p0, Lepson/print/ActivityNfcPrinter$1$3;->this$1:Lepson/print/ActivityNfcPrinter$1;

    iget-object v1, v1, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    .line 698
    invoke-static {v1}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v1

    invoke-virtual {v1}, Lepson/common/ExternalFileUtils;->getSupportedMediaDir()Ljava/lang/String;

    move-result-object v1

    .line 697
    invoke-virtual {v0, v1}, Lcom/epson/mobilephone/common/escpr/EscprLib;->epsWrapperGetSupportedMedia(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_2

    .line 700
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    .line 704
    :cond_2
    invoke-virtual {p1}, Lepson/common/ExternalFileUtils;->getSupportedMedia()Ljava/io/File;

    move-result-object v1

    .line 705
    invoke-virtual {p1}, Lepson/common/ExternalFileUtils;->getSavedSupportedMedia()Ljava/io/File;

    move-result-object v2

    .line 707
    :try_start_0
    invoke-static {v1, v2}, Lepson/common/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V

    const-string v1, "ActivityChangeWifiPrinter"

    const-string v2, "Success epsWrapperGetSupportedMedia"

    .line 708
    invoke-static {v1, v2}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 710
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 715
    :goto_0
    invoke-virtual {p1}, Lepson/common/ExternalFileUtils;->getAreaInfo()Ljava/io/File;

    move-result-object v1

    .line 716
    invoke-virtual {p1}, Lepson/common/ExternalFileUtils;->getSavedAreaInfo()Ljava/io/File;

    move-result-object p1

    .line 718
    :try_start_1
    invoke-static {v1, p1}, Lepson/common/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V

    const-string p1, "ActivityChangeWifiPrinter"

    const-string v1, "Success copy AreaInfo"

    .line 719
    invoke-static {p1, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception p1

    .line 721
    invoke-virtual {p1}, Ljava/io/IOException;->printStackTrace()V

    .line 725
    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 666
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/print/ActivityNfcPrinter$1$3;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 3

    .line 730
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    if-nez p1, :cond_4

    .line 732
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1$3;->val$printer:Lepson/print/MyPrinter;

    invoke-virtual {p1}, Lepson/print/MyPrinter;->getLocation()I

    move-result p1

    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    .line 734
    new-instance p1, Lepson/print/EPPrinterManager;

    iget-object v0, p0, Lepson/print/ActivityNfcPrinter$1$3;->this$1:Lepson/print/ActivityNfcPrinter$1;

    iget-object v0, v0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    invoke-direct {p1, v0}, Lepson/print/EPPrinterManager;-><init>(Landroid/content/Context;)V

    .line 736
    iget-object v0, p0, Lepson/print/ActivityNfcPrinter$1$3;->val$printer:Lepson/print/MyPrinter;

    invoke-virtual {v0}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lepson/print/EPPrinterManager;->loadIpPrinterInfo(Ljava/lang/String;)Lepson/print/EPPrinterInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 738
    iget-object v1, p0, Lepson/print/ActivityNfcPrinter$1$3;->val$printer:Lepson/print/MyPrinter;

    invoke-virtual {v1}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lepson/print/EPPrinterInfo;->printerIP:Ljava/lang/String;

    goto :goto_0

    .line 740
    :cond_0
    new-instance v0, Lepson/print/EPPrinterInfo;

    invoke-direct {v0}, Lepson/print/EPPrinterInfo;-><init>()V

    .line 741
    iget-object v1, p0, Lepson/print/ActivityNfcPrinter$1$3;->val$printer:Lepson/print/MyPrinter;

    invoke-virtual {v1}, Lepson/print/MyPrinter;->getName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lepson/print/EPPrinterInfo;->printerName:Ljava/lang/String;

    .line 742
    iget-object v1, p0, Lepson/print/ActivityNfcPrinter$1$3;->val$printer:Lepson/print/MyPrinter;

    invoke-virtual {v1}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lepson/print/EPPrinterInfo;->printerID:Ljava/lang/String;

    .line 743
    iget-object v1, p0, Lepson/print/ActivityNfcPrinter$1$3;->val$printer:Lepson/print/MyPrinter;

    invoke-virtual {v1}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lepson/print/EPPrinterInfo;->printerIP:Ljava/lang/String;

    .line 744
    iget-object v1, p0, Lepson/print/ActivityNfcPrinter$1$3;->val$printer:Lepson/print/MyPrinter;

    invoke-virtual {v1}, Lepson/print/MyPrinter;->getName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lepson/print/EPPrinterInfo;->userDefName:Ljava/lang/String;

    .line 745
    iget-object v1, p0, Lepson/print/ActivityNfcPrinter$1$3;->val$printer:Lepson/print/MyPrinter;

    invoke-virtual {v1}, Lepson/print/MyPrinter;->getLocation()I

    move-result v1

    iput v1, v0, Lepson/print/EPPrinterInfo;->printerLocation:I

    .line 746
    iget-object v1, p0, Lepson/print/ActivityNfcPrinter$1$3;->val$printer:Lepson/print/MyPrinter;

    invoke-virtual {v1}, Lepson/print/MyPrinter;->getSerialNo()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lepson/print/EPPrinterInfo;->printerSerialNo:Ljava/lang/String;

    .line 749
    :goto_0
    iget-object v1, p0, Lepson/print/ActivityNfcPrinter$1$3;->val$printer:Lepson/print/MyPrinter;

    invoke-virtual {v1}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1, v0}, Lepson/print/EPPrinterManager;->saveIPPrinterInfo(Ljava/lang/String;Lepson/print/EPPrinterInfo;)V

    .line 750
    invoke-virtual {p1}, Lepson/print/EPPrinterManager;->commitIPPrinterInfo()V

    .line 754
    :cond_1
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1$3;->val$printer:Lepson/print/MyPrinter;

    iget-object v0, p0, Lepson/print/ActivityNfcPrinter$1$3;->this$1:Lepson/print/ActivityNfcPrinter$1;

    iget-object v0, v0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    invoke-virtual {p1, v0}, Lepson/print/MyPrinter;->setCurPrinter(Landroid/content/Context;)V

    .line 756
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1$3;->this$1:Lepson/print/ActivityNfcPrinter$1;

    iget-object p1, p1, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    iget-object v0, p0, Lepson/print/ActivityNfcPrinter$1$3;->val$printer:Lepson/print/MyPrinter;

    .line 758
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/wifidirect/MacAddrUtils;->getMacAddressFromPrinterId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 756
    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->getCurConnectInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 761
    iget-object v0, p0, Lepson/print/ActivityNfcPrinter$1$3;->this$1:Lepson/print/ActivityNfcPrinter$1;

    iget-object v0, v0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    const-string v1, "printer"

    iget-object v2, p0, Lepson/print/ActivityNfcPrinter$1$3;->val$printer:Lepson/print/MyPrinter;

    .line 763
    invoke-virtual {v2}, Lepson/print/MyPrinter;->getName()Ljava/lang/String;

    move-result-object v2

    .line 761
    invoke-static {v0, p1, v1, v2}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->setConnectInfo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 765
    :cond_2
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1$3;->this$1:Lepson/print/ActivityNfcPrinter$1;

    iget-object p1, p1, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    const-string v0, "printer"

    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->resetConnectInfo(Landroid/content/Context;Ljava/lang/String;)V

    .line 769
    :goto_1
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1$3;->this$1:Lepson/print/ActivityNfcPrinter$1;

    iget-object p1, p1, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    invoke-static {p1}, Lepson/print/ActivityNfcPrinter;->access$700(Lepson/print/ActivityNfcPrinter;)I

    move-result p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    .line 771
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1$3;->this$1:Lepson/print/ActivityNfcPrinter$1;

    iget-object p1, p1, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    iget-object p1, p1, Lepson/print/ActivityNfcPrinter;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object p1

    const/16 v0, 0xd

    .line 772
    iput v0, p1, Landroid/os/Message;->what:I

    .line 773
    iget-object v0, p0, Lepson/print/ActivityNfcPrinter$1$3;->val$printer:Lepson/print/MyPrinter;

    iput-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 774
    iget-object v0, p0, Lepson/print/ActivityNfcPrinter$1$3;->this$1:Lepson/print/ActivityNfcPrinter$1;

    iget-object v0, v0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    iget-object v0, v0, Lepson/print/ActivityNfcPrinter;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_2

    .line 778
    :cond_3
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1$3;->this$1:Lepson/print/ActivityNfcPrinter$1;

    iget-object p1, p1, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    iget-object p1, p1, Lepson/print/ActivityNfcPrinter;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object p1

    const/16 v0, 0x14

    .line 779
    iput v0, p1, Landroid/os/Message;->what:I

    .line 780
    iget-object v0, p0, Lepson/print/ActivityNfcPrinter$1$3;->val$printer:Lepson/print/MyPrinter;

    iput-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 781
    iget-object v0, p0, Lepson/print/ActivityNfcPrinter$1$3;->this$1:Lepson/print/ActivityNfcPrinter$1;

    iget-object v0, v0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    iget-object v0, v0, Lepson/print/ActivityNfcPrinter;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :goto_2
    return-void

    .line 786
    :cond_4
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1$3;->this$1:Lepson/print/ActivityNfcPrinter$1;

    iget-object p1, p1, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    const v0, 0x7f0e03ed

    const v1, 0x7f0e03e3

    invoke-virtual {p1, v0, v1}, Lepson/print/ActivityNfcPrinter;->showErrorMessage(II)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 666
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lepson/print/ActivityNfcPrinter$1$3;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

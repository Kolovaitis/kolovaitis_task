.class Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;
.super Lepson/print/Util/AsyncTaskExecutor;
.source "ActivityDocsPrintPreview.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/ActivityDocsPrintPreview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PreparePrintTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lepson/print/Util/AsyncTaskExecutor<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private mIsLandscape:Z

.field final synthetic this$0:Lepson/print/ActivityDocsPrintPreview;

.field tmpList:Lepson/print/EPImageList;


# direct methods
.method public constructor <init>(Lepson/print/ActivityDocsPrintPreview;)V
    .locals 0

    .line 1922
    iput-object p1, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-direct {p0}, Lepson/print/Util/AsyncTaskExecutor;-><init>()V

    .line 1919
    new-instance p1, Lepson/print/EPImageList;

    invoke-direct {p1}, Lepson/print/EPImageList;-><init>()V

    iput-object p1, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->tmpList:Lepson/print/EPImageList;

    return-void
.end method

.method private OLD_setImageListForLocalPrinter()V
    .locals 9

    .line 2015
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v0

    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->initPrintDir()V

    .line 2018
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v0}, Lepson/print/ActivityDocsPrintPreview;->access$4000(Lepson/print/ActivityDocsPrintPreview;)I

    move-result v0

    :goto_0
    iget-object v1, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v1}, Lepson/print/ActivityDocsPrintPreview;->access$4100(Lepson/print/ActivityDocsPrintPreview;)I

    move-result v1

    if-gt v0, v1, :cond_0

    .line 2019
    iget-object v1, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v1}, Lepson/print/ActivityDocsPrintPreview;->access$1300(Lepson/print/ActivityDocsPrintPreview;)Lepson/print/pdf/pdfRender;

    move-result-object v1

    invoke-virtual {v1, v0}, Lepson/print/pdf/pdfRender;->getDecodeJpegFilename(I)Ljava/lang/String;

    move-result-object v1

    .line 2020
    iget-object v2, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->tmpList:Lepson/print/EPImageList;

    invoke-virtual {v2, v1}, Lepson/print/EPImageList;->add(Ljava/lang/String;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2024
    :cond_0
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v0}, Lepson/print/ActivityDocsPrintPreview;->access$1300(Lepson/print/ActivityDocsPrintPreview;)Lepson/print/pdf/pdfRender;

    move-result-object v0

    iget-object v1, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v1}, Lepson/print/ActivityDocsPrintPreview;->access$4000(Lepson/print/ActivityDocsPrintPreview;)I

    move-result v1

    iget-object v2, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v2}, Lepson/print/ActivityDocsPrintPreview;->access$4100(Lepson/print/ActivityDocsPrintPreview;)I

    move-result v2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lepson/print/pdf/pdfRender;->startConvertPage(III)V

    .line 2027
    :goto_1
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v0}, Lepson/print/ActivityDocsPrintPreview;->access$1300(Lepson/print/ActivityDocsPrintPreview;)Lepson/print/pdf/pdfRender;

    move-result-object v0

    invoke-virtual {v0}, Lepson/print/pdf/pdfRender;->RendererStart()Z

    move-result v0

    if-nez v0, :cond_1

    const-wide/16 v0, 0x64

    .line 2029
    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    .line 2031
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    .line 2039
    :cond_1
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->tmpList:Lepson/print/EPImageList;

    iget-object v0, v0, Lepson/print/EPImageList;->imageList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    if-lez v0, :cond_2

    .line 2040
    new-instance v0, Lepson/print/EPImage;

    iget-object v2, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->tmpList:Lepson/print/EPImageList;

    iget-object v2, v2, Lepson/print/EPImageList;->imageList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lepson/print/EPImage;

    invoke-virtual {v2}, Lepson/print/EPImage;->getLoadImageFileName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Lepson/print/EPImage;-><init>(Ljava/lang/String;I)V

    .line 2041
    iget v2, v0, Lepson/print/EPImage;->srcHeight:I

    .line 2042
    iget v0, v0, Lepson/print/EPImage;->srcWidth:I

    const-string v4, "ActivityDocsPrintPreview"

    .line 2044
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Finish Renderer Page No.1 : srcWidth = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v6, " srcHeight = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    const/4 v2, 0x0

    :goto_2
    const/4 v4, 0x0

    .line 2048
    :goto_3
    iget-object v5, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->tmpList:Lepson/print/EPImageList;

    iget-object v5, v5, Lepson/print/EPImageList;->imageList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v4, v5, :cond_4

    .line 2049
    iget-object v5, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->tmpList:Lepson/print/EPImageList;

    invoke-virtual {v5, v4}, Lepson/print/EPImageList;->get(I)Lepson/print/EPImage;

    move-result-object v5

    .line 2052
    iget-object v6, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v6}, Lepson/print/ActivityDocsPrintPreview;->access$2900(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/LinearLayout;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getLeft()I

    move-result v6

    iget-object v7, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v7}, Lepson/print/ActivityDocsPrintPreview;->access$2900(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/LinearLayout;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getPaddingLeft()I

    move-result v7

    add-int/2addr v6, v7

    iput v6, v5, Lepson/print/EPImage;->previewPaperRectLeft:I

    .line 2053
    iget-object v6, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v6}, Lepson/print/ActivityDocsPrintPreview;->access$2900(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/LinearLayout;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getTop()I

    move-result v6

    iget-object v7, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v7}, Lepson/print/ActivityDocsPrintPreview;->access$2900(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/LinearLayout;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getPaddingTop()I

    move-result v7

    add-int/2addr v6, v7

    iput v6, v5, Lepson/print/EPImage;->previewPaperRectTop:I

    .line 2054
    iget-object v6, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v6}, Lepson/print/ActivityDocsPrintPreview;->access$2900(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/LinearLayout;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getRight()I

    move-result v6

    iget-object v7, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v7}, Lepson/print/ActivityDocsPrintPreview;->access$2900(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/LinearLayout;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getPaddingRight()I

    move-result v7

    sub-int/2addr v6, v7

    iput v6, v5, Lepson/print/EPImage;->previewPaperRectRight:I

    .line 2055
    iget-object v6, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v6}, Lepson/print/ActivityDocsPrintPreview;->access$2900(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/LinearLayout;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getBottom()I

    move-result v6

    iget-object v7, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v7}, Lepson/print/ActivityDocsPrintPreview;->access$2900(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/LinearLayout;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getPaddingBottom()I

    move-result v7

    sub-int/2addr v6, v7

    iput v6, v5, Lepson/print/EPImage;->previewPaperRectBottom:I

    .line 2058
    iget-object v6, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v6}, Lepson/print/ActivityDocsPrintPreview;->access$2900(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/LinearLayout;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getLeft()I

    move-result v6

    iget-object v7, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v7}, Lepson/print/ActivityDocsPrintPreview;->access$1600(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/ImageView;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLeft()I

    move-result v7

    add-int/2addr v6, v7

    int-to-float v6, v6

    iput v6, v5, Lepson/print/EPImage;->previewImageRectLeft:F

    .line 2059
    iget-object v6, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v6}, Lepson/print/ActivityDocsPrintPreview;->access$2900(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/LinearLayout;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getTop()I

    move-result v6

    iget-object v7, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v7}, Lepson/print/ActivityDocsPrintPreview;->access$1600(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/ImageView;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/ImageView;->getTop()I

    move-result v7

    add-int/2addr v6, v7

    int-to-float v6, v6

    iput v6, v5, Lepson/print/EPImage;->previewImageRectTop:F

    .line 2060
    iget-object v6, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v6}, Lepson/print/ActivityDocsPrintPreview;->access$2900(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/LinearLayout;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getLeft()I

    move-result v6

    iget-object v7, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v7}, Lepson/print/ActivityDocsPrintPreview;->access$1600(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/ImageView;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/ImageView;->getRight()I

    move-result v7

    add-int/2addr v6, v7

    int-to-float v6, v6

    iput v6, v5, Lepson/print/EPImage;->previewImageRectRight:F

    .line 2061
    iget-object v6, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v6}, Lepson/print/ActivityDocsPrintPreview;->access$2900(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/LinearLayout;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getTop()I

    move-result v6

    iget-object v7, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v7}, Lepson/print/ActivityDocsPrintPreview;->access$1600(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/ImageView;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/ImageView;->getBottom()I

    move-result v7

    add-int/2addr v6, v7

    int-to-float v6, v6

    iput v6, v5, Lepson/print/EPImage;->previewImageRectBottom:F

    .line 2064
    iget v6, v5, Lepson/print/EPImage;->previewImageRectRight:F

    iget v7, v5, Lepson/print/EPImage;->previewImageRectLeft:F

    sub-float/2addr v6, v7

    float-to-int v6, v6

    iput v6, v5, Lepson/print/EPImage;->previewWidth:I

    .line 2065
    iget v6, v5, Lepson/print/EPImage;->previewImageRectBottom:F

    iget v7, v5, Lepson/print/EPImage;->previewImageRectTop:F

    sub-float/2addr v6, v7

    float-to-int v6, v6

    iput v6, v5, Lepson/print/EPImage;->previewHeight:I

    .line 2068
    iget v6, v5, Lepson/print/EPImage;->previewPaperRectRight:I

    iget v7, v5, Lepson/print/EPImage;->previewPaperRectLeft:I

    sub-int/2addr v6, v7

    iget v7, v5, Lepson/print/EPImage;->previewPaperRectBottom:I

    iget v8, v5, Lepson/print/EPImage;->previewPaperRectTop:I

    sub-int/2addr v7, v8

    if-le v6, v7, :cond_3

    .line 2070
    iput-boolean v3, v5, Lepson/print/EPImage;->isPaperLandScape:Z

    goto :goto_4

    .line 2072
    :cond_3
    iput-boolean v1, v5, Lepson/print/EPImage;->isPaperLandScape:Z

    .line 2075
    :goto_4
    iput v0, v5, Lepson/print/EPImage;->srcWidth:I

    .line 2076
    iput v2, v5, Lepson/print/EPImage;->srcHeight:I

    .line 2079
    iget-object v6, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v6}, Lepson/print/ActivityDocsPrintPreview;->access$1100(Lepson/print/ActivityDocsPrintPreview;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lepson/print/EPImage;->setOrgName(Ljava/lang/String;)V

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_3

    :cond_4
    return-void
.end method

.method private calcLandscape()Z
    .locals 5

    .line 1939
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v0}, Lepson/print/ActivityDocsPrintPreview;->access$2900(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v1}, Lepson/print/ActivityDocsPrintPreview;->access$2900(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getPaddingLeft()I

    move-result v1

    add-int/2addr v0, v1

    .line 1940
    iget-object v1, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v1}, Lepson/print/ActivityDocsPrintPreview;->access$2900(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getRight()I

    move-result v1

    iget-object v2, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v2}, Lepson/print/ActivityDocsPrintPreview;->access$2900(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    .line 1941
    iget-object v2, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v2}, Lepson/print/ActivityDocsPrintPreview;->access$2900(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getTop()I

    move-result v2

    iget-object v3, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v3}, Lepson/print/ActivityDocsPrintPreview;->access$2900(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/LinearLayout;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getPaddingTop()I

    move-result v3

    add-int/2addr v2, v3

    .line 1942
    iget-object v3, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v3}, Lepson/print/ActivityDocsPrintPreview;->access$2900(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/LinearLayout;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getBottom()I

    move-result v3

    iget-object v4, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v4}, Lepson/print/ActivityDocsPrintPreview;->access$2900(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/LinearLayout;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getPaddingBottom()I

    move-result v4

    sub-int/2addr v3, v4

    sub-int/2addr v1, v0

    sub-int/2addr v3, v2

    if-le v1, v3, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private setImageListForLocalPrinter2()V
    .locals 10

    .line 2003
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v0

    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->initPrintDir()V

    .line 2004
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v0}, Lepson/print/ActivityDocsPrintPreview;->access$3700(Lepson/print/ActivityDocsPrintPreview;)Lcom/epson/iprint/prtlogger/PrintLog;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2006
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    new-instance v1, Lcom/epson/iprint/prtlogger/PrintLog;

    invoke-direct {v1}, Lcom/epson/iprint/prtlogger/PrintLog;-><init>()V

    invoke-static {v0, v1}, Lepson/print/ActivityDocsPrintPreview;->access$3702(Lepson/print/ActivityDocsPrintPreview;Lcom/epson/iprint/prtlogger/PrintLog;)Lcom/epson/iprint/prtlogger/PrintLog;

    .line 2008
    :cond_0
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v0}, Lepson/print/ActivityDocsPrintPreview;->access$3700(Lepson/print/ActivityDocsPrintPreview;)Lcom/epson/iprint/prtlogger/PrintLog;

    move-result-object v0

    const/4 v1, 0x2

    iput v1, v0, Lcom/epson/iprint/prtlogger/PrintLog;->previewType:I

    .line 2009
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    new-instance v9, Lepson/print/screen/LocalDocumentParams;

    invoke-static {v0}, Lepson/print/ActivityDocsPrintPreview;->access$1000(Lepson/print/ActivityDocsPrintPreview;)Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v1}, Lepson/print/ActivityDocsPrintPreview;->access$1100(Lepson/print/ActivityDocsPrintPreview;)Ljava/lang/String;

    move-result-object v3

    iget-object v1, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    .line 2010
    invoke-static {v1}, Lepson/print/ActivityDocsPrintPreview;->access$2300(Lepson/print/ActivityDocsPrintPreview;)Ljava/lang/String;

    move-result-object v4

    iget-object v1, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v1}, Lepson/print/ActivityDocsPrintPreview;->access$3900(Lepson/print/ActivityDocsPrintPreview;)Z

    move-result v5

    iget-object v1, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v1}, Lepson/print/ActivityDocsPrintPreview;->access$4000(Lepson/print/ActivityDocsPrintPreview;)I

    move-result v6

    iget-object v1, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v1}, Lepson/print/ActivityDocsPrintPreview;->access$4100(Lepson/print/ActivityDocsPrintPreview;)I

    move-result v7

    iget-object v1, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v1}, Lepson/print/ActivityDocsPrintPreview;->access$3700(Lepson/print/ActivityDocsPrintPreview;)Lcom/epson/iprint/prtlogger/PrintLog;

    move-result-object v8

    move-object v1, v9

    invoke-direct/range {v1 .. v8}, Lepson/print/screen/LocalDocumentParams;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIILcom/epson/iprint/prtlogger/PrintLog;)V

    .line 2009
    invoke-static {v0, v9}, Lepson/print/ActivityDocsPrintPreview;->access$3802(Lepson/print/ActivityDocsPrintPreview;Lepson/print/screen/PrintProgress$ProgressParams;)Lepson/print/screen/PrintProgress$ProgressParams;

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1916
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 5

    .line 1952
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$300(Lepson/print/ActivityDocsPrintPreview;)I

    move-result p1

    and-int/lit8 p1, p1, 0x4

    const/4 v0, 0x1

    if-nez p1, :cond_1

    .line 1954
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$300(Lepson/print/ActivityDocsPrintPreview;)I

    move-result p1

    and-int/2addr p1, v0

    if-eqz p1, :cond_0

    .line 1955
    invoke-direct {p0}, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->OLD_setImageListForLocalPrinter()V

    goto :goto_0

    .line 1957
    :cond_0
    invoke-direct {p0}, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->setImageListForLocalPrinter2()V

    goto :goto_0

    .line 1962
    :cond_1
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-virtual {p1}, Lepson/print/ActivityDocsPrintPreview;->cancelLoadRemotePreviewTask()V

    .line 1965
    :goto_0
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$300(Lepson/print/ActivityDocsPrintPreview;)I

    move-result p1

    and-int/2addr p1, v0

    if-eqz p1, :cond_4

    .line 1968
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    const-string v1, "PrintSetting"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Lepson/print/ActivityDocsPrintPreview;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    .line 1969
    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    .line 1971
    iget-object v1, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v1}, Lepson/print/ActivityDocsPrintPreview;->access$300(Lepson/print/ActivityDocsPrintPreview;)I

    move-result v1

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    const-string v1, "SOURCE_TYPE"

    .line 1973
    invoke-interface {p1, v1, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1976
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->tmpList:Lepson/print/EPImageList;

    invoke-virtual {v0}, Lepson/print/EPImageList;->clear()V

    .line 1977
    new-instance v0, Lepson/print/EPImage;

    invoke-direct {v0}, Lepson/print/EPImage;-><init>()V

    .line 1978
    iget-object v1, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v1}, Lepson/print/ActivityDocsPrintPreview;->access$1100(Lepson/print/ActivityDocsPrintPreview;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lepson/print/EPImage;->loadImageFileName:Ljava/lang/String;

    .line 1979
    iget-object v1, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v1}, Lepson/print/ActivityDocsPrintPreview;->access$1100(Lepson/print/ActivityDocsPrintPreview;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lepson/print/EPImage;->setOrgName(Ljava/lang/String;)V

    .line 1980
    iget-object v1, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->tmpList:Lepson/print/EPImageList;

    invoke-virtual {v1, v0}, Lepson/print/EPImageList;->add(Lepson/print/EPImage;)Z

    goto :goto_1

    :cond_2
    const-string v1, "SOURCE_TYPE"

    const/4 v3, 0x3

    .line 1985
    invoke-interface {p1, v1, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1988
    iget-object v1, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->tmpList:Lepson/print/EPImageList;

    invoke-virtual {v1, v2}, Lepson/print/EPImageList;->get(I)Lepson/print/EPImage;

    move-result-object v1

    .line 1989
    iget-object v2, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v2}, Lepson/print/ActivityDocsPrintPreview;->access$1100(Lepson/print/ActivityDocsPrintPreview;)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x2f

    invoke-virtual {v2, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    if-lez v2, :cond_3

    .line 1990
    iget-object v2, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v2}, Lepson/print/ActivityDocsPrintPreview;->access$1100(Lepson/print/ActivityDocsPrintPreview;)Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v4}, Lepson/print/ActivityDocsPrintPreview;->access$1100(Lepson/print/ActivityDocsPrintPreview;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    add-int/2addr v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lepson/print/EPImage;->webUrl:Ljava/lang/String;

    goto :goto_1

    .line 1992
    :cond_3
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v0}, Lepson/print/ActivityDocsPrintPreview;->access$1100(Lepson/print/ActivityDocsPrintPreview;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lepson/print/EPImage;->webUrl:Ljava/lang/String;

    .line 1996
    :goto_1
    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_4
    const/4 p1, 0x0

    return-object p1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 1916
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 1

    .line 2085
    invoke-super {p0, p1}, Lepson/print/Util/AsyncTaskExecutor;->onPostExecute(Ljava/lang/Object;)V

    .line 2087
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->tmpList:Lepson/print/EPImageList;

    invoke-static {p1, v0}, Lepson/print/ActivityDocsPrintPreview;->access$4202(Lepson/print/ActivityDocsPrintPreview;Lepson/print/EPImageList;)Lepson/print/EPImageList;

    .line 2089
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lepson/print/ActivityDocsPrintPreview;->enableProgressBar(Z)V

    .line 2092
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-virtual {p1}, Lepson/print/ActivityDocsPrintPreview;->check3GAndStartPrint()V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .line 1926
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lepson/print/ActivityDocsPrintPreview;->enableProgressBar(Z)V

    .line 1929
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v0}, Lepson/print/ActivityDocsPrintPreview;->access$2500(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1931
    invoke-direct {p0}, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->calcLandscape()Z

    move-result v0

    iput-boolean v0, p0, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->mIsLandscape:Z

    return-void
.end method

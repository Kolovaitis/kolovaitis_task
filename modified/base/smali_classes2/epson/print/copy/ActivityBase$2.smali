.class synthetic Lepson/print/copy/ActivityBase$2;
.super Ljava/lang/Object;
.source "ActivityBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/copy/ActivityBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$epson$print$copy$ActivityBase$DialogButtons:[I

.field static final synthetic $SwitchMap$epson$print$copy$Component$ecopycomponent$ECopyComponent$ICopyOptionContextListener$ContextCreationError:[I

.field static final synthetic $SwitchMap$epson$print$copy$Component$ecopycomponent$ECopyComponent$ICopyOptionListener$CopyOptionChangedError:[I

.field static final synthetic $SwitchMap$epson$print$copy$Component$ecopycomponent$ECopyComponent$ICopyResumeRequest$StopReason:[I

.field static final synthetic $SwitchMap$epson$print$copy$Component$ecopycomponent$ECopyComponent$ICopyStatusListener$CopyTaskResult:[I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 335
    invoke-static {}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->values()[Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lepson/print/copy/ActivityBase$2;->$SwitchMap$epson$print$copy$Component$ecopycomponent$ECopyComponent$ICopyResumeRequest$StopReason:[I

    const/4 v0, 0x1

    :try_start_0
    sget-object v1, Lepson/print/copy/ActivityBase$2;->$SwitchMap$epson$print$copy$Component$ecopycomponent$ECopyComponent$ICopyResumeRequest$StopReason:[I

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->PrinterMarkerSupplyEmptyError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    invoke-virtual {v2}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->ordinal()I

    move-result v2

    aput v0, v1, v2
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v1, 0x2

    :try_start_1
    sget-object v2, Lepson/print/copy/ActivityBase$2;->$SwitchMap$epson$print$copy$Component$ecopycomponent$ECopyComponent$ICopyResumeRequest$StopReason:[I

    sget-object v3, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->PrinterMarkerWasteFullError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    invoke-virtual {v3}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->ordinal()I

    move-result v3

    aput v1, v2, v3
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    const/4 v2, 0x3

    :try_start_2
    sget-object v3, Lepson/print/copy/ActivityBase$2;->$SwitchMap$epson$print$copy$Component$ecopycomponent$ECopyComponent$ICopyResumeRequest$StopReason:[I

    sget-object v4, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->PrinterMediaJamError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    invoke-virtual {v4}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->ordinal()I

    move-result v4

    aput v2, v3, v4
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    const/4 v3, 0x4

    :try_start_3
    sget-object v4, Lepson/print/copy/ActivityBase$2;->$SwitchMap$epson$print$copy$Component$ecopycomponent$ECopyComponent$ICopyResumeRequest$StopReason:[I

    sget-object v5, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->PrinterMediaEmptyError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    invoke-virtual {v5}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->ordinal()I

    move-result v5

    aput v3, v4, v5
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    :try_start_4
    sget-object v4, Lepson/print/copy/ActivityBase$2;->$SwitchMap$epson$print$copy$Component$ecopycomponent$ECopyComponent$ICopyResumeRequest$StopReason:[I

    sget-object v5, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->ManualfeedGuide:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    invoke-virtual {v5}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->ordinal()I

    move-result v5

    const/4 v6, 0x5

    aput v6, v4, v5
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :catch_4
    :try_start_5
    sget-object v4, Lepson/print/copy/ActivityBase$2;->$SwitchMap$epson$print$copy$Component$ecopycomponent$ECopyComponent$ICopyResumeRequest$StopReason:[I

    sget-object v5, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->PrinterCoverOpenError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    invoke-virtual {v5}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->ordinal()I

    move-result v5

    const/4 v6, 0x6

    aput v6, v4, v5
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    :catch_5
    :try_start_6
    sget-object v4, Lepson/print/copy/ActivityBase$2;->$SwitchMap$epson$print$copy$Component$ecopycomponent$ECopyComponent$ICopyResumeRequest$StopReason:[I

    sget-object v5, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->PrinterOutputAreaFullError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    invoke-virtual {v5}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->ordinal()I

    move-result v5

    const/4 v6, 0x7

    aput v6, v4, v5
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_6

    :catch_6
    :try_start_7
    sget-object v4, Lepson/print/copy/ActivityBase$2;->$SwitchMap$epson$print$copy$Component$ecopycomponent$ECopyComponent$ICopyResumeRequest$StopReason:[I

    sget-object v5, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->PrinterOtherError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    invoke-virtual {v5}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->ordinal()I

    move-result v5

    const/16 v6, 0x8

    aput v6, v4, v5
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_7

    :catch_7
    :try_start_8
    sget-object v4, Lepson/print/copy/ActivityBase$2;->$SwitchMap$epson$print$copy$Component$ecopycomponent$ECopyComponent$ICopyResumeRequest$StopReason:[I

    sget-object v5, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->ScannerOtherError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    invoke-virtual {v5}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->ordinal()I

    move-result v5

    const/16 v6, 0x9

    aput v6, v4, v5
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_8

    .line 312
    :catch_8
    invoke-static {}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;->values()[Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;

    move-result-object v4

    array-length v4, v4

    new-array v4, v4, [I

    sput-object v4, Lepson/print/copy/ActivityBase$2;->$SwitchMap$epson$print$copy$Component$ecopycomponent$ECopyComponent$ICopyOptionListener$CopyOptionChangedError:[I

    :try_start_9
    sget-object v4, Lepson/print/copy/ActivityBase$2;->$SwitchMap$epson$print$copy$Component$ecopycomponent$ECopyComponent$ICopyOptionListener$CopyOptionChangedError:[I

    sget-object v5, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;->ErrorCommunication:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;

    invoke-virtual {v5}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;->ordinal()I

    move-result v5

    aput v0, v4, v5
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_9

    :catch_9
    :try_start_a
    sget-object v4, Lepson/print/copy/ActivityBase$2;->$SwitchMap$epson$print$copy$Component$ecopycomponent$ECopyComponent$ICopyOptionListener$CopyOptionChangedError:[I

    sget-object v5, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;->ErrorInvalidOption:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;

    invoke-virtual {v5}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;->ordinal()I

    move-result v5

    aput v1, v4, v5
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_a

    :catch_a
    :try_start_b
    sget-object v4, Lepson/print/copy/ActivityBase$2;->$SwitchMap$epson$print$copy$Component$ecopycomponent$ECopyComponent$ICopyOptionListener$CopyOptionChangedError:[I

    sget-object v5, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;->Error:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;

    invoke-virtual {v5}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;->ordinal()I

    move-result v5

    aput v2, v4, v5
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_b

    .line 289
    :catch_b
    invoke-static {}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;->values()[Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;

    move-result-object v4

    array-length v4, v4

    new-array v4, v4, [I

    sput-object v4, Lepson/print/copy/ActivityBase$2;->$SwitchMap$epson$print$copy$Component$ecopycomponent$ECopyComponent$ICopyOptionContextListener$ContextCreationError:[I

    :try_start_c
    sget-object v4, Lepson/print/copy/ActivityBase$2;->$SwitchMap$epson$print$copy$Component$ecopycomponent$ECopyComponent$ICopyOptionContextListener$ContextCreationError:[I

    sget-object v5, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;->ErrorCommunication:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;

    invoke-virtual {v5}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;->ordinal()I

    move-result v5

    aput v0, v4, v5
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_c

    :catch_c
    :try_start_d
    sget-object v4, Lepson/print/copy/ActivityBase$2;->$SwitchMap$epson$print$copy$Component$ecopycomponent$ECopyComponent$ICopyOptionContextListener$ContextCreationError:[I

    sget-object v5, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;->ErrorNotCopySupported:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;

    invoke-virtual {v5}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;->ordinal()I

    move-result v5

    aput v1, v4, v5
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_d

    :catch_d
    :try_start_e
    sget-object v4, Lepson/print/copy/ActivityBase$2;->$SwitchMap$epson$print$copy$Component$ecopycomponent$ECopyComponent$ICopyOptionContextListener$ContextCreationError:[I

    sget-object v5, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;->Error:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;

    invoke-virtual {v5}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;->ordinal()I

    move-result v5

    aput v2, v4, v5
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_e

    .line 259
    :catch_e
    invoke-static {}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;->values()[Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    move-result-object v4

    array-length v4, v4

    new-array v4, v4, [I

    sput-object v4, Lepson/print/copy/ActivityBase$2;->$SwitchMap$epson$print$copy$Component$ecopycomponent$ECopyComponent$ICopyStatusListener$CopyTaskResult:[I

    :try_start_f
    sget-object v4, Lepson/print/copy/ActivityBase$2;->$SwitchMap$epson$print$copy$Component$ecopycomponent$ECopyComponent$ICopyStatusListener$CopyTaskResult:[I

    sget-object v5, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;->ErrorCommunication:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    invoke-virtual {v5}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;->ordinal()I

    move-result v5

    aput v0, v4, v5
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_f

    :catch_f
    :try_start_10
    sget-object v4, Lepson/print/copy/ActivityBase$2;->$SwitchMap$epson$print$copy$Component$ecopycomponent$ECopyComponent$ICopyStatusListener$CopyTaskResult:[I

    sget-object v5, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;->Busy:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    invoke-virtual {v5}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;->ordinal()I

    move-result v5

    aput v1, v4, v5
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_10

    :catch_10
    :try_start_11
    sget-object v4, Lepson/print/copy/ActivityBase$2;->$SwitchMap$epson$print$copy$Component$ecopycomponent$ECopyComponent$ICopyStatusListener$CopyTaskResult:[I

    sget-object v5, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;->RemoveAdfPaper:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    invoke-virtual {v5}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;->ordinal()I

    move-result v5

    aput v2, v4, v5
    :try_end_11
    .catch Ljava/lang/NoSuchFieldError; {:try_start_11 .. :try_end_11} :catch_11

    :catch_11
    :try_start_12
    sget-object v4, Lepson/print/copy/ActivityBase$2;->$SwitchMap$epson$print$copy$Component$ecopycomponent$ECopyComponent$ICopyStatusListener$CopyTaskResult:[I

    sget-object v5, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;->ErrorOther:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    invoke-virtual {v5}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;->ordinal()I

    move-result v5

    aput v3, v4, v5
    :try_end_12
    .catch Ljava/lang/NoSuchFieldError; {:try_start_12 .. :try_end_12} :catch_12

    .line 196
    :catch_12
    invoke-static {}, Lepson/print/copy/ActivityBase$DialogButtons;->values()[Lepson/print/copy/ActivityBase$DialogButtons;

    move-result-object v3

    array-length v3, v3

    new-array v3, v3, [I

    sput-object v3, Lepson/print/copy/ActivityBase$2;->$SwitchMap$epson$print$copy$ActivityBase$DialogButtons:[I

    :try_start_13
    sget-object v3, Lepson/print/copy/ActivityBase$2;->$SwitchMap$epson$print$copy$ActivityBase$DialogButtons:[I

    sget-object v4, Lepson/print/copy/ActivityBase$DialogButtons;->Ok:Lepson/print/copy/ActivityBase$DialogButtons;

    invoke-virtual {v4}, Lepson/print/copy/ActivityBase$DialogButtons;->ordinal()I

    move-result v4

    aput v0, v3, v4
    :try_end_13
    .catch Ljava/lang/NoSuchFieldError; {:try_start_13 .. :try_end_13} :catch_13

    :catch_13
    :try_start_14
    sget-object v0, Lepson/print/copy/ActivityBase$2;->$SwitchMap$epson$print$copy$ActivityBase$DialogButtons:[I

    sget-object v3, Lepson/print/copy/ActivityBase$DialogButtons;->Cancel:Lepson/print/copy/ActivityBase$DialogButtons;

    invoke-virtual {v3}, Lepson/print/copy/ActivityBase$DialogButtons;->ordinal()I

    move-result v3

    aput v1, v0, v3
    :try_end_14
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14 .. :try_end_14} :catch_14

    :catch_14
    :try_start_15
    sget-object v0, Lepson/print/copy/ActivityBase$2;->$SwitchMap$epson$print$copy$ActivityBase$DialogButtons:[I

    sget-object v1, Lepson/print/copy/ActivityBase$DialogButtons;->ClearErrorCancel:Lepson/print/copy/ActivityBase$DialogButtons;

    invoke-virtual {v1}, Lepson/print/copy/ActivityBase$DialogButtons;->ordinal()I

    move-result v1

    aput v2, v0, v1
    :try_end_15
    .catch Ljava/lang/NoSuchFieldError; {:try_start_15 .. :try_end_15} :catch_15

    :catch_15
    return-void
.end method

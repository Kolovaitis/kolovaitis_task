.class public Lepson/print/copy/ActivityBase$errorDialog;
.super Ljava/lang/Object;
.source "ActivityBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/copy/ActivityBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "errorDialog"
.end annotation


# static fields
.field public static final MESSAGE:I = 0x1

.field public static final TITLE:I


# instance fields
.field context:Landroid/content/Context;

.field dialog:Landroid/app/Dialog;

.field final synthetic this$0:Lepson/print/copy/ActivityBase;


# direct methods
.method public constructor <init>(Lepson/print/copy/ActivityBase;Landroid/content/Context;)V
    .locals 0

    .line 172
    iput-object p1, p0, Lepson/print/copy/ActivityBase$errorDialog;->this$0:Lepson/print/copy/ActivityBase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 173
    iput-object p2, p0, Lepson/print/copy/ActivityBase$errorDialog;->context:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method Cancel()V
    .locals 1

    .line 246
    iget-object v0, p0, Lepson/print/copy/ActivityBase$errorDialog;->dialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 247
    invoke-virtual {v0}, Landroid/app/Dialog;->cancel()V

    :cond_0
    return-void
.end method

.method public getReasonText(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;)[Ljava/lang/String;
    .locals 4

    const/4 v0, 0x2

    .line 288
    new-array v0, v0, [Ljava/lang/String;

    .line 289
    sget-object v1, Lepson/print/copy/ActivityBase$2;->$SwitchMap$epson$print$copy$Component$ecopycomponent$ECopyComponent$ICopyOptionContextListener$ContextCreationError:[I

    invoke-virtual {p1}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;->ordinal()I

    move-result p1

    aget p1, v1, p1

    const/4 v1, 0x1

    const/4 v2, 0x0

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 296
    :pswitch_0
    iget-object p1, p0, Lepson/print/copy/ActivityBase$errorDialog;->this$0:Lepson/print/copy/ActivityBase;

    const v3, 0x7f0e01c4

    invoke-virtual {p1, v3}, Lepson/print/copy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v2

    .line 297
    iget-object p1, p0, Lepson/print/copy/ActivityBase$errorDialog;->this$0:Lepson/print/copy/ActivityBase;

    const v2, 0x7f0e0041

    invoke-virtual {p1, v2}, Lepson/print/copy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v1

    goto :goto_0

    .line 291
    :pswitch_1
    iget-object p1, p0, Lepson/print/copy/ActivityBase$errorDialog;->this$0:Lepson/print/copy/ActivityBase;

    const v3, 0x7f0e01b1

    invoke-virtual {p1, v3}, Lepson/print/copy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v2

    .line 292
    iget-object p1, p0, Lepson/print/copy/ActivityBase$errorDialog;->this$0:Lepson/print/copy/ActivityBase;

    const v2, 0x7f0e01af

    invoke-virtual {p1, v2}, Lepson/print/copy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v1

    :goto_0
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public getReasonText(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;)[Ljava/lang/String;
    .locals 4

    const/4 v0, 0x2

    .line 311
    new-array v0, v0, [Ljava/lang/String;

    .line 312
    sget-object v1, Lepson/print/copy/ActivityBase$2;->$SwitchMap$epson$print$copy$Component$ecopycomponent$ECopyComponent$ICopyOptionListener$CopyOptionChangedError:[I

    invoke-virtual {p1}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;->ordinal()I

    move-result p1

    aget p1, v1, p1

    const/4 v1, 0x1

    const/4 v2, 0x0

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 319
    :pswitch_0
    iget-object p1, p0, Lepson/print/copy/ActivityBase$errorDialog;->this$0:Lepson/print/copy/ActivityBase;

    const v3, 0x7f0e01c4

    invoke-virtual {p1, v3}, Lepson/print/copy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v2

    .line 320
    iget-object p1, p0, Lepson/print/copy/ActivityBase$errorDialog;->this$0:Lepson/print/copy/ActivityBase;

    const v2, 0x7f0e0041

    invoke-virtual {p1, v2}, Lepson/print/copy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v1

    goto :goto_0

    .line 314
    :pswitch_1
    iget-object p1, p0, Lepson/print/copy/ActivityBase$errorDialog;->this$0:Lepson/print/copy/ActivityBase;

    const v3, 0x7f0e01b1

    invoke-virtual {p1, v3}, Lepson/print/copy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v2

    .line 315
    iget-object p1, p0, Lepson/print/copy/ActivityBase$errorDialog;->this$0:Lepson/print/copy/ActivityBase;

    const v2, 0x7f0e01af

    invoke-virtual {p1, v2}, Lepson/print/copy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v1

    :goto_0
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public getReasonText(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;)[Ljava/lang/String;
    .locals 4

    const/4 v0, 0x2

    .line 334
    new-array v0, v0, [Ljava/lang/String;

    .line 335
    sget-object v1, Lepson/print/copy/ActivityBase$2;->$SwitchMap$epson$print$copy$Component$ecopycomponent$ECopyComponent$ICopyResumeRequest$StopReason:[I

    invoke-virtual {p1}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->ordinal()I

    move-result p1

    aget p1, v1, p1

    const/4 v1, 0x1

    const/4 v2, 0x0

    packed-switch p1, :pswitch_data_0

    goto/16 :goto_0

    .line 369
    :pswitch_0
    iget-object p1, p0, Lepson/print/copy/ActivityBase$errorDialog;->this$0:Lepson/print/copy/ActivityBase;

    const v3, 0x7f0e04ac

    invoke-virtual {p1, v3}, Lepson/print/copy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v2

    .line 370
    iget-object p1, p0, Lepson/print/copy/ActivityBase$errorDialog;->this$0:Lepson/print/copy/ActivityBase;

    const v2, 0x7f0e04ab

    invoke-virtual {p1, v2}, Lepson/print/copy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v1

    goto/16 :goto_0

    .line 365
    :pswitch_1
    iget-object p1, p0, Lepson/print/copy/ActivityBase$errorDialog;->this$0:Lepson/print/copy/ActivityBase;

    const v3, 0x7f0e01c4

    invoke-virtual {p1, v3}, Lepson/print/copy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v2

    .line 366
    iget-object p1, p0, Lepson/print/copy/ActivityBase$errorDialog;->this$0:Lepson/print/copy/ActivityBase;

    const v2, 0x7f0e0041

    invoke-virtual {p1, v2}, Lepson/print/copy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v1

    goto/16 :goto_0

    .line 361
    :pswitch_2
    iget-object p1, p0, Lepson/print/copy/ActivityBase$errorDialog;->this$0:Lepson/print/copy/ActivityBase;

    const v3, 0x7f0e01a0

    invoke-virtual {p1, v3}, Lepson/print/copy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v2

    .line 362
    iget-object p1, p0, Lepson/print/copy/ActivityBase$errorDialog;->this$0:Lepson/print/copy/ActivityBase;

    const v2, 0x7f0e0033

    invoke-virtual {p1, v2}, Lepson/print/copy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v1

    goto/16 :goto_0

    .line 357
    :pswitch_3
    iget-object p1, p0, Lepson/print/copy/ActivityBase$errorDialog;->this$0:Lepson/print/copy/ActivityBase;

    const v3, 0x7f0e01b7

    invoke-virtual {p1, v3}, Lepson/print/copy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v2

    .line 358
    iget-object p1, p0, Lepson/print/copy/ActivityBase$errorDialog;->this$0:Lepson/print/copy/ActivityBase;

    const v2, 0x7f0e01b6

    invoke-virtual {p1, v2}, Lepson/print/copy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v1

    goto :goto_0

    .line 353
    :pswitch_4
    iget-object p1, p0, Lepson/print/copy/ActivityBase$errorDialog;->this$0:Lepson/print/copy/ActivityBase;

    const v3, 0x7f0e01e0

    invoke-virtual {p1, v3}, Lepson/print/copy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v2

    .line 354
    iget-object p1, p0, Lepson/print/copy/ActivityBase$errorDialog;->this$0:Lepson/print/copy/ActivityBase;

    const v2, 0x7f0e01dd

    invoke-virtual {p1, v2}, Lepson/print/copy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v1

    goto :goto_0

    .line 349
    :pswitch_5
    iget-object p1, p0, Lepson/print/copy/ActivityBase$errorDialog;->this$0:Lepson/print/copy/ActivityBase;

    const v3, 0x7f0e01f4

    invoke-virtual {p1, v3}, Lepson/print/copy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v2

    .line 350
    iget-object p1, p0, Lepson/print/copy/ActivityBase$errorDialog;->this$0:Lepson/print/copy/ActivityBase;

    const v2, 0x7f0e01f3

    invoke-virtual {p1, v2}, Lepson/print/copy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v1

    goto :goto_0

    .line 345
    :pswitch_6
    iget-object p1, p0, Lepson/print/copy/ActivityBase$errorDialog;->this$0:Lepson/print/copy/ActivityBase;

    const v3, 0x7f0e01f0

    invoke-virtual {p1, v3}, Lepson/print/copy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v2

    .line 346
    iget-object p1, p0, Lepson/print/copy/ActivityBase$errorDialog;->this$0:Lepson/print/copy/ActivityBase;

    const v2, 0x7f0e0037

    invoke-virtual {p1, v2}, Lepson/print/copy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v1

    goto :goto_0

    .line 341
    :pswitch_7
    iget-object p1, p0, Lepson/print/copy/ActivityBase$errorDialog;->this$0:Lepson/print/copy/ActivityBase;

    const v3, 0x7f0e0214

    invoke-virtual {p1, v3}, Lepson/print/copy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v2

    .line 342
    iget-object p1, p0, Lepson/print/copy/ActivityBase$errorDialog;->this$0:Lepson/print/copy/ActivityBase;

    const v2, 0x7f0e0213

    invoke-virtual {p1, v2}, Lepson/print/copy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v1

    goto :goto_0

    .line 337
    :pswitch_8
    iget-object p1, p0, Lepson/print/copy/ActivityBase$errorDialog;->this$0:Lepson/print/copy/ActivityBase;

    const v3, 0x7f0e01cc

    invoke-virtual {p1, v3}, Lepson/print/copy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v2

    .line 338
    iget-object p1, p0, Lepson/print/copy/ActivityBase$errorDialog;->this$0:Lepson/print/copy/ActivityBase;

    const v2, 0x7f0e01cb

    invoke-virtual {p1, v2}, Lepson/print/copy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v1

    :goto_0
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getReasonText(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;)[Ljava/lang/String;
    .locals 4

    const/4 v0, 0x2

    .line 258
    new-array v0, v0, [Ljava/lang/String;

    .line 259
    sget-object v1, Lepson/print/copy/ActivityBase$2;->$SwitchMap$epson$print$copy$Component$ecopycomponent$ECopyComponent$ICopyStatusListener$CopyTaskResult:[I

    invoke-virtual {p1}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;->ordinal()I

    move-result p1

    aget p1, v1, p1

    const/4 v1, 0x1

    const/4 v2, 0x0

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 273
    :pswitch_0
    iget-object p1, p0, Lepson/print/copy/ActivityBase$errorDialog;->this$0:Lepson/print/copy/ActivityBase;

    const v3, 0x7f0e01c4

    invoke-virtual {p1, v3}, Lepson/print/copy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v2

    .line 274
    iget-object p1, p0, Lepson/print/copy/ActivityBase$errorDialog;->this$0:Lepson/print/copy/ActivityBase;

    const v2, 0x7f0e0041

    invoke-virtual {p1, v2}, Lepson/print/copy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v1

    goto :goto_0

    .line 269
    :pswitch_1
    iget-object p1, p0, Lepson/print/copy/ActivityBase$errorDialog;->this$0:Lepson/print/copy/ActivityBase;

    const v3, 0x7f0e0043

    invoke-virtual {p1, v3}, Lepson/print/copy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v2

    .line 270
    iget-object p1, p0, Lepson/print/copy/ActivityBase$errorDialog;->this$0:Lepson/print/copy/ActivityBase;

    const v2, 0x7f0e0042

    invoke-virtual {p1, v2}, Lepson/print/copy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v1

    goto :goto_0

    .line 265
    :pswitch_2
    iget-object p1, p0, Lepson/print/copy/ActivityBase$errorDialog;->this$0:Lepson/print/copy/ActivityBase;

    const v3, 0x7f0e0218

    invoke-virtual {p1, v3}, Lepson/print/copy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v2

    .line 266
    iget-object p1, p0, Lepson/print/copy/ActivityBase$errorDialog;->this$0:Lepson/print/copy/ActivityBase;

    const v2, 0x7f0e0217

    invoke-virtual {p1, v2}, Lepson/print/copy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v1

    goto :goto_0

    .line 261
    :pswitch_3
    iget-object p1, p0, Lepson/print/copy/ActivityBase$errorDialog;->this$0:Lepson/print/copy/ActivityBase;

    const v3, 0x7f0e01b1

    invoke-virtual {p1, v3}, Lepson/print/copy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v2

    .line 262
    iget-object p1, p0, Lepson/print/copy/ActivityBase$errorDialog;->this$0:Lepson/print/copy/ActivityBase;

    const v2, 0x7f0e0035

    invoke-virtual {p1, v2}, Lepson/print/copy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v1

    :goto_0
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method showErrorDialog(I)V
    .locals 1

    .line 181
    iget-object v0, p0, Lepson/print/copy/ActivityBase$errorDialog;->this$0:Lepson/print/copy/ActivityBase;

    invoke-virtual {v0, p1}, Lepson/print/copy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lepson/print/copy/ActivityBase$errorDialog;->showErrorDialog(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method showErrorDialog(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 177
    sget-object v0, Lepson/print/copy/ActivityBase$DialogButtons;->Ok:Lepson/print/copy/ActivityBase$DialogButtons;

    const/4 v1, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, Lepson/print/copy/ActivityBase$errorDialog;->showErrorDialog(Ljava/lang/String;Ljava/lang/String;Lepson/print/copy/ActivityBase$DialogButtons;Lepson/print/copy/ActivityBase$IClose;)V

    return-void
.end method

.method showErrorDialog(Ljava/lang/String;Ljava/lang/String;Lepson/print/copy/ActivityBase$DialogButtons;Lepson/print/copy/ActivityBase$IClose;)V
    .locals 5

    .line 192
    new-instance v0, Lepson/print/widgets/CustomTitleAlertDialogBuilder;

    iget-object v1, p0, Lepson/print/copy/ActivityBase$errorDialog;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lepson/print/widgets/CustomTitleAlertDialogBuilder;-><init>(Landroid/content/Context;)V

    .line 196
    sget-object v1, Lepson/print/copy/ActivityBase$2;->$SwitchMap$epson$print$copy$ActivityBase$DialogButtons:[I

    invoke-virtual {p3}, Lepson/print/copy/ActivityBase$DialogButtons;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const v2, 0x7f0e0476

    const/4 v3, 0x0

    packed-switch v1, :pswitch_data_0

    move-object v1, v3

    goto :goto_0

    .line 206
    :pswitch_0
    iget-object v1, p0, Lepson/print/copy/ActivityBase$errorDialog;->this$0:Lepson/print/copy/ActivityBase;

    const v3, 0x7f0e0482

    invoke-virtual {v1, v3}, Lepson/print/copy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 207
    iget-object v1, p0, Lepson/print/copy/ActivityBase$errorDialog;->this$0:Lepson/print/copy/ActivityBase;

    invoke-virtual {v1, v2}, Lepson/print/copy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 202
    :pswitch_1
    iget-object v1, p0, Lepson/print/copy/ActivityBase$errorDialog;->this$0:Lepson/print/copy/ActivityBase;

    invoke-virtual {v1, v2}, Lepson/print/copy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 198
    :pswitch_2
    iget-object v1, p0, Lepson/print/copy/ActivityBase$errorDialog;->this$0:Lepson/print/copy/ActivityBase;

    const v2, 0x7f0e04f2

    invoke-virtual {v1, v2}, Lepson/print/copy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object v4, v3

    move-object v3, v1

    move-object v1, v4

    :goto_0
    const/4 v2, 0x0

    if-eqz p1, :cond_0

    .line 213
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 215
    :cond_0
    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 216
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    if-eqz v3, :cond_1

    .line 218
    new-instance p1, Lepson/print/copy/ActivityBase$errorDialog$1;

    invoke-direct {p1, p0, p4, p3}, Lepson/print/copy/ActivityBase$errorDialog$1;-><init>(Lepson/print/copy/ActivityBase$errorDialog;Lepson/print/copy/ActivityBase$IClose;Lepson/print/copy/ActivityBase$DialogButtons;)V

    invoke-virtual {v0, v3, p1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    :cond_1
    if-eqz v1, :cond_2

    .line 229
    new-instance p1, Lepson/print/copy/ActivityBase$errorDialog$2;

    invoke-direct {p1, p0, p4}, Lepson/print/copy/ActivityBase$errorDialog$2;-><init>(Lepson/print/copy/ActivityBase$errorDialog;Lepson/print/copy/ActivityBase$IClose;)V

    invoke-virtual {v0, v1, p1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 238
    :cond_2
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    iput-object p1, p0, Lepson/print/copy/ActivityBase$errorDialog;->dialog:Landroid/app/Dialog;

    .line 239
    iget-object p1, p0, Lepson/print/copy/ActivityBase$errorDialog;->dialog:Landroid/app/Dialog;

    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

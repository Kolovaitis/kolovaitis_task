.class Lepson/print/copy/CopyActivity$3;
.super Ljava/lang/Object;
.source "CopyActivity.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/copy/CopyActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/copy/CopyActivity;


# direct methods
.method constructor <init>(Lepson/print/copy/CopyActivity;)V
    .locals 0

    .line 240
    iput-object p1, p0, Lepson/print/copy/CopyActivity$3;->this$0:Lepson/print/copy/CopyActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 4

    .line 243
    iget-object v0, p0, Lepson/print/copy/CopyActivity$3;->this$0:Lepson/print/copy/CopyActivity;

    invoke-static {v0}, Lepson/print/copy/CopyActivity;->access$200(Lepson/print/copy/CopyActivity;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "HandlerCallback"

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    iget p1, p1, Landroid/os/Message;->what:I

    packed-switch p1, :pswitch_data_0

    goto/16 :goto_0

    .line 264
    :pswitch_0
    iget-object p1, p0, Lepson/print/copy/CopyActivity$3;->this$0:Lepson/print/copy/CopyActivity;

    invoke-static {p1}, Lepson/print/copy/CopyActivity;->access$200(Lepson/print/copy/CopyActivity;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "UPDATE_SETTING"

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    iget-object p1, p0, Lepson/print/copy/CopyActivity$3;->this$0:Lepson/print/copy/CopyActivity;

    iget-object p1, p1, Lepson/print/copy/CopyActivity;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    if-nez p1, :cond_0

    .line 267
    iget-object p1, p0, Lepson/print/copy/CopyActivity$3;->this$0:Lepson/print/copy/CopyActivity;

    invoke-virtual {p1}, Lepson/print/copy/CopyActivity;->fetchCopyOptionContext()V

    goto/16 :goto_0

    .line 270
    :cond_0
    iget-object p1, p0, Lepson/print/copy/CopyActivity$3;->this$0:Lepson/print/copy/CopyActivity;

    iget-object v0, p1, Lepson/print/copy/CopyActivity;->copyComponent:Lepson/print/copy/Component/ecopycomponent/ECopyComponent;

    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->getBindedCopyOptionContext()Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    move-result-object v0

    iput-object v0, p1, Lepson/print/copy/CopyActivity;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    .line 271
    iget-object p1, p0, Lepson/print/copy/CopyActivity$3;->this$0:Lepson/print/copy/CopyActivity;

    iget-object p1, p1, Lepson/print/copy/CopyActivity;->copyComponent:Lepson/print/copy/Component/ecopycomponent/ECopyComponent;

    iget-object v0, p0, Lepson/print/copy/CopyActivity$3;->this$0:Lepson/print/copy/CopyActivity;

    iget-object v0, v0, Lepson/print/copy/CopyActivity;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    iget-object v1, p0, Lepson/print/copy/CopyActivity$3;->this$0:Lepson/print/copy/CopyActivity;

    iget-object v1, v1, Lepson/print/copy/CopyActivity;->optionListener:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;

    invoke-virtual {p1, v0, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->bindCopyOptionContext(Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;)V

    .line 272
    iget-object p1, p0, Lepson/print/copy/CopyActivity$3;->this$0:Lepson/print/copy/CopyActivity;

    iget-object v0, p1, Lepson/print/copy/CopyActivity;->copyComponent:Lepson/print/copy/Component/ecopycomponent/ECopyComponent;

    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->getCopyOptionItems()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p1, v0}, Lepson/print/copy/CopyActivity;->buildCopyOptions(Ljava/util/ArrayList;)V

    goto :goto_0

    .line 253
    :pswitch_1
    iget-object p1, p0, Lepson/print/copy/CopyActivity$3;->this$0:Lepson/print/copy/CopyActivity;

    invoke-static {p1}, Lepson/print/copy/CopyActivity;->access$200(Lepson/print/copy/CopyActivity;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "COMM_ERROR"

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    iget-object p1, p0, Lepson/print/copy/CopyActivity$3;->this$0:Lepson/print/copy/CopyActivity;

    new-instance v0, Lepson/print/copy/ActivityBase$errorDialog;

    invoke-direct {v0, p1, p1}, Lepson/print/copy/ActivityBase$errorDialog;-><init>(Lepson/print/copy/ActivityBase;Landroid/content/Context;)V

    invoke-static {p1, v0}, Lepson/print/copy/CopyActivity;->access$502(Lepson/print/copy/CopyActivity;Lepson/print/copy/ActivityBase$errorDialog;)Lepson/print/copy/ActivityBase$errorDialog;

    .line 255
    iget-object p1, p0, Lepson/print/copy/CopyActivity$3;->this$0:Lepson/print/copy/CopyActivity;

    invoke-static {p1}, Lepson/print/copy/CopyActivity;->access$500(Lepson/print/copy/CopyActivity;)Lepson/print/copy/ActivityBase$errorDialog;

    move-result-object p1

    iget-object v0, p0, Lepson/print/copy/CopyActivity$3;->this$0:Lepson/print/copy/CopyActivity;

    const v1, 0x7f0e01b1

    invoke-virtual {v0, v1}, Lepson/print/copy/CopyActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lepson/print/copy/CopyActivity$3;->this$0:Lepson/print/copy/CopyActivity;

    const v2, 0x7f0e01af

    invoke-virtual {v1, v2}, Lepson/print/copy/CopyActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lepson/print/copy/ActivityBase$DialogButtons;->Ok:Lepson/print/copy/ActivityBase$DialogButtons;

    new-instance v3, Lepson/print/copy/CopyActivity$3$1;

    invoke-direct {v3, p0}, Lepson/print/copy/CopyActivity$3$1;-><init>(Lepson/print/copy/CopyActivity$3;)V

    invoke-virtual {p1, v0, v1, v2, v3}, Lepson/print/copy/ActivityBase$errorDialog;->showErrorDialog(Ljava/lang/String;Ljava/lang/String;Lepson/print/copy/ActivityBase$DialogButtons;Lepson/print/copy/ActivityBase$IClose;)V

    goto :goto_0

    .line 247
    :pswitch_2
    iget-object p1, p0, Lepson/print/copy/CopyActivity$3;->this$0:Lepson/print/copy/CopyActivity;

    invoke-static {p1}, Lepson/print/copy/CopyActivity;->access$200(Lepson/print/copy/CopyActivity;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "PROBE_PRINTER"

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    iget-object p1, p0, Lepson/print/copy/CopyActivity$3;->this$0:Lepson/print/copy/CopyActivity;

    new-instance v0, Lepson/print/copy/CopyActivity$ProbePrinter;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lepson/print/copy/CopyActivity$ProbePrinter;-><init>(Lepson/print/copy/CopyActivity;Lepson/print/copy/CopyActivity$1;)V

    invoke-static {p1, v0}, Lepson/print/copy/CopyActivity;->access$302(Lepson/print/copy/CopyActivity;Lepson/print/copy/CopyActivity$ProbePrinter;)Lepson/print/copy/CopyActivity$ProbePrinter;

    .line 249
    iget-object p1, p0, Lepson/print/copy/CopyActivity$3;->this$0:Lepson/print/copy/CopyActivity;

    invoke-static {p1}, Lepson/print/copy/CopyActivity;->access$300(Lepson/print/copy/CopyActivity;)Lepson/print/copy/CopyActivity$ProbePrinter;

    move-result-object p1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {p1, v0}, Lepson/print/copy/CopyActivity$ProbePrinter;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :goto_0
    const/4 p1, 0x1

    return p1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

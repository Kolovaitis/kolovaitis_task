.class Lepson/print/copy/CopyScaleActivity$XScaleValue$3;
.super Ljava/lang/Object;
.source "CopyScaleActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/copy/CopyScaleActivity$XScaleValue;->capabilitySetting(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lepson/print/copy/CopyScaleActivity$XScaleValue;


# direct methods
.method constructor <init>(Lepson/print/copy/CopyScaleActivity$XScaleValue;)V
    .locals 0

    .line 175
    iput-object p1, p0, Lepson/print/copy/CopyScaleActivity$XScaleValue$3;->this$1:Lepson/print/copy/CopyScaleActivity$XScaleValue;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .line 178
    iget-object p1, p0, Lepson/print/copy/CopyScaleActivity$XScaleValue$3;->this$1:Lepson/print/copy/CopyScaleActivity$XScaleValue;

    iget-object p2, p1, Lepson/print/copy/CopyScaleActivity$XScaleValue;->choices:Ljava/util/List;

    add-int/lit8 p3, p3, 0x3

    invoke-interface {p2, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    iput-object p2, p1, Lepson/print/copy/CopyScaleActivity$XScaleValue;->selected:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 179
    iget-object p1, p0, Lepson/print/copy/CopyScaleActivity$XScaleValue$3;->this$1:Lepson/print/copy/CopyScaleActivity$XScaleValue;

    iget-object p1, p1, Lepson/print/copy/CopyScaleActivity$XScaleValue;->optionItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    iget-object p2, p0, Lepson/print/copy/CopyScaleActivity$XScaleValue$3;->this$1:Lepson/print/copy/CopyScaleActivity$XScaleValue;

    iget-object p2, p2, Lepson/print/copy/CopyScaleActivity$XScaleValue;->selected:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    invoke-virtual {p1, p2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->selectChoice(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;)V

    .line 180
    iget-object p1, p0, Lepson/print/copy/CopyScaleActivity$XScaleValue$3;->this$1:Lepson/print/copy/CopyScaleActivity$XScaleValue;

    iget-object p1, p1, Lepson/print/copy/CopyScaleActivity$XScaleValue;->changedListener:Lepson/print/copy/ActivityBase$OptionItemChangedListener;

    if-eqz p1, :cond_0

    .line 181
    iget-object p1, p0, Lepson/print/copy/CopyScaleActivity$XScaleValue$3;->this$1:Lepson/print/copy/CopyScaleActivity$XScaleValue;

    iget-object p1, p1, Lepson/print/copy/CopyScaleActivity$XScaleValue;->changedListener:Lepson/print/copy/ActivityBase$OptionItemChangedListener;

    iget-object p2, p0, Lepson/print/copy/CopyScaleActivity$XScaleValue$3;->this$1:Lepson/print/copy/CopyScaleActivity$XScaleValue;

    iget-object p2, p2, Lepson/print/copy/CopyScaleActivity$XScaleValue;->optionItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    invoke-interface {p1, p2}, Lepson/print/copy/ActivityBase$OptionItemChangedListener;->onOptionItemChanged(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    .line 183
    :cond_0
    iget-object p1, p0, Lepson/print/copy/CopyScaleActivity$XScaleValue$3;->this$1:Lepson/print/copy/CopyScaleActivity$XScaleValue;

    iget-object p1, p1, Lepson/print/copy/CopyScaleActivity$XScaleValue;->this$0:Lepson/print/copy/CopyScaleActivity;

    const/4 p2, 0x1

    iput-boolean p2, p1, Lepson/print/copy/CopyScaleActivity;->isKeepSimpleAPConnection:Z

    .line 184
    iget-object p1, p0, Lepson/print/copy/CopyScaleActivity$XScaleValue$3;->this$1:Lepson/print/copy/CopyScaleActivity$XScaleValue;

    iget-object p1, p1, Lepson/print/copy/CopyScaleActivity$XScaleValue;->this$0:Lepson/print/copy/CopyScaleActivity;

    invoke-virtual {p1}, Lepson/print/copy/CopyScaleActivity;->finish()V

    return-void
.end method

.class Lepson/print/copy/CopySettingActivity$2;
.super Ljava/lang/Object;
.source "CopySettingActivity.java"

# interfaces
.implements Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/copy/CopySettingActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/copy/CopySettingActivity;


# direct methods
.method constructor <init>(Lepson/print/copy/CopySettingActivity;)V
    .locals 0

    .line 58
    iput-object p1, p0, Lepson/print/copy/CopySettingActivity$2;->this$0:Lepson/print/copy/CopySettingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCopyOptionChanged(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;Ljava/util/ArrayList;Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;",
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;",
            ">;",
            "Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;",
            ")V"
        }
    .end annotation

    .line 62
    iget-object p1, p0, Lepson/print/copy/CopySettingActivity$2;->this$0:Lepson/print/copy/CopySettingActivity;

    iget-object p1, p1, Lepson/print/copy/CopySettingActivity;->loading:Lepson/print/screen/WorkingDialog;

    invoke-virtual {p1}, Lepson/print/screen/WorkingDialog;->dismiss()V

    if-eqz p3, :cond_0

    .line 64
    new-instance p1, Lepson/print/copy/ActivityBase$errorDialog;

    iget-object p2, p0, Lepson/print/copy/CopySettingActivity$2;->this$0:Lepson/print/copy/CopySettingActivity;

    invoke-direct {p1, p2, p2}, Lepson/print/copy/ActivityBase$errorDialog;-><init>(Lepson/print/copy/ActivityBase;Landroid/content/Context;)V

    .line 65
    invoke-virtual {p1, p3}, Lepson/print/copy/ActivityBase$errorDialog;->getReasonText(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;)[Ljava/lang/String;

    move-result-object p2

    const/4 p3, 0x0

    .line 66
    aget-object p3, p2, p3

    const/4 v0, 0x1

    .line 67
    aget-object p2, p2, v0

    .line 68
    invoke-virtual {p1, p3, p2}, Lepson/print/copy/ActivityBase$errorDialog;->showErrorDialog(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 71
    :cond_0
    iget-object p1, p0, Lepson/print/copy/CopySettingActivity$2;->this$0:Lepson/print/copy/CopySettingActivity;

    invoke-virtual {p1}, Lepson/print/copy/CopySettingActivity;->finish()V

    return-void
.end method

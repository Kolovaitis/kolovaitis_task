.class Lepson/print/copy/CopyScaleActivity$XScaleValue$ScaleAdapter;
.super Landroid/widget/ArrayAdapter;
.source "CopyScaleActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/copy/CopyScaleActivity$XScaleValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ScaleAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter<",
        "Lepson/print/copy/CopyScaleActivity$XScaleValue$ScaleInfo;",
        ">;"
    }
.end annotation


# instance fields
.field private inflater:Landroid/view/LayoutInflater;

.field private items:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/CopyScaleActivity$XScaleValue$ScaleInfo;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$1:Lepson/print/copy/CopyScaleActivity$XScaleValue;


# direct methods
.method public constructor <init>(Lepson/print/copy/CopyScaleActivity$XScaleValue;Landroid/content/Context;ILjava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/CopyScaleActivity$XScaleValue$ScaleInfo;",
            ">;)V"
        }
    .end annotation

    .line 199
    iput-object p1, p0, Lepson/print/copy/CopyScaleActivity$XScaleValue$ScaleAdapter;->this$1:Lepson/print/copy/CopyScaleActivity$XScaleValue;

    .line 200
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 201
    iput-object p4, p0, Lepson/print/copy/CopyScaleActivity$XScaleValue$ScaleAdapter;->items:Ljava/util/ArrayList;

    const-string p1, "layout_inflater"

    .line 202
    invoke-virtual {p2, p1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/LayoutInflater;

    iput-object p1, p0, Lepson/print/copy/CopyScaleActivity$XScaleValue$ScaleAdapter;->inflater:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    if-nez p2, :cond_0

    .line 209
    iget-object p2, p0, Lepson/print/copy/CopyScaleActivity$XScaleValue$ScaleAdapter;->inflater:Landroid/view/LayoutInflater;

    const p3, 0x7f0a00be

    const/4 v0, 0x0

    invoke-virtual {p2, p3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 211
    :cond_0
    iget-object p3, p0, Lepson/print/copy/CopyScaleActivity$XScaleValue$ScaleAdapter;->items:Ljava/util/ArrayList;

    invoke-virtual {p3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lepson/print/copy/CopyScaleActivity$XScaleValue$ScaleInfo;

    iget-object p3, p3, Lepson/print/copy/CopyScaleActivity$XScaleValue$ScaleInfo;->scalePresetName:Ljava/lang/String;

    const v0, 0x7f0802f6

    .line 212
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 213
    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 215
    iget-object p3, p0, Lepson/print/copy/CopyScaleActivity$XScaleValue$ScaleAdapter;->items:Ljava/util/ArrayList;

    invoke-virtual {p3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lepson/print/copy/CopyScaleActivity$XScaleValue$ScaleInfo;

    iget p3, p3, Lepson/print/copy/CopyScaleActivity$XScaleValue$ScaleInfo;->scaleNum:I

    const v0, 0x7f0802f7

    .line 216
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p3, " %"

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 218
    iget-object p3, p0, Lepson/print/copy/CopyScaleActivity$XScaleValue$ScaleAdapter;->this$1:Lepson/print/copy/CopyScaleActivity$XScaleValue;

    iget p3, p3, Lepson/print/copy/CopyScaleActivity$XScaleValue;->selectedItemPos:I

    add-int/lit8 p3, p3, -0x3

    const v0, 0x7f0802f5

    if-ne p1, p3, :cond_1

    .line 219
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    const/4 p3, 0x0

    invoke-virtual {p1, p3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 221
    :cond_1
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    const/16 p3, 0x8

    invoke-virtual {p1, p3}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    return-object p2
.end method

.class Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;
.super Ljava/lang/Object;
.source "CopyScaleActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->clickCustomScale()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;


# direct methods
.method constructor <init>(Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;)V
    .locals 0

    .line 267
    iput-object p1, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .line 270
    iget-object p1, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    iget-object p1, p1, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->this$0:Lepson/print/copy/CopyScaleActivity;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    const v0, 0x7f0a004d

    const/4 v1, 0x0

    .line 271
    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    .line 272
    iget-object v0, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    const v1, 0x7f08002c

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->editText:Landroid/widget/TextView;

    .line 273
    iget-object v0, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    iget-object v1, v0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->optionItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    invoke-virtual {v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedValue()I

    move-result v1

    iput v1, v0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->value:I

    .line 274
    iget-object v0, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    iget v0, v0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->value:I

    sget v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->CopyMagnificationAutofitValue:I

    if-ne v0, v1, :cond_0

    .line 275
    iget-object v0, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    const/16 v1, 0x64

    iput v1, v0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->value:I

    .line 277
    :cond_0
    iget-object v0, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    iget-object v0, v0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->editText:Landroid/widget/TextView;

    iget-object v1, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    iget v1, v1, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->value:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 280
    iget-object v0, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    const v1, 0x7f080029

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, v0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->countUp:Landroid/widget/Button;

    .line 281
    iget-object v0, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    iget-object v0, v0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->countUp:Landroid/widget/Button;

    new-instance v1, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$CountCustomScale;

    iget-object v2, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$CountCustomScale;-><init>(Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;I)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 282
    iget-object v0, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    iget v0, v0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->value:I

    iget-object v1, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    iget-object v1, v1, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->optionItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    invoke-virtual {v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getMaximumValue()I

    move-result v1

    const/4 v2, 0x0

    if-ne v0, v1, :cond_1

    .line 283
    iget-object v0, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    iget-object v0, v0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->countUp:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 285
    :cond_1
    iget-object v0, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    iget-object v0, v0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->countUp:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 287
    :goto_0
    iget-object v0, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    iget-object v0, v0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->countUp:Landroid/widget/Button;

    invoke-static {v0}, Lepson/print/widgets/LongTapRepeatAdapter;->bless(Landroid/view/View;)V

    .line 290
    iget-object v0, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    const v1, 0x7f080028

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, v0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->countDown:Landroid/widget/Button;

    .line 291
    iget-object v0, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    iget-object v0, v0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->countDown:Landroid/widget/Button;

    new-instance v1, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$CountCustomScale;

    iget-object v4, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    const/4 v5, -0x1

    invoke-direct {v1, v4, v5}, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$CountCustomScale;-><init>(Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;I)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 292
    iget-object v0, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    iget v0, v0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->value:I

    iget-object v1, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    iget-object v1, v1, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->optionItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    invoke-virtual {v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getMinimumValue()I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 293
    iget-object v0, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    iget-object v0, v0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->countDown:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_1

    .line 295
    :cond_2
    iget-object v0, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    iget-object v0, v0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->countDown:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 297
    :goto_1
    iget-object v0, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    iget-object v0, v0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->countDown:Landroid/widget/Button;

    invoke-static {v0}, Lepson/print/widgets/LongTapRepeatAdapter;->bless(Landroid/view/View;)V

    .line 300
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    iget-object v1, v1, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->this$0:Lepson/print/copy/CopyScaleActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0e026a

    .line 301
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 302
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 303
    iget-object p1, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    iget-object p1, p1, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->this$0:Lepson/print/copy/CopyScaleActivity;

    const v1, 0x7f0e04f2

    .line 304
    invoke-virtual {p1, v1}, Lepson/print/copy/CopyScaleActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    new-instance v1, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1$1;

    invoke-direct {v1, p0}, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1$1;-><init>(Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;)V

    .line 303
    invoke-virtual {v0, p1, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 325
    iget-object p1, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    iget-object p1, p1, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->this$0:Lepson/print/copy/CopyScaleActivity;

    const v1, 0x7f0e0476

    .line 326
    invoke-virtual {p1, v1}, Lepson/print/copy/CopyScaleActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    new-instance v1, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1$2;

    invoke-direct {v1, p0}, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1$2;-><init>(Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;)V

    .line 325
    invoke-virtual {v0, p1, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 333
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 335
    iget-object p1, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    iget-object v0, p1, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->this$0:Lepson/print/copy/CopyScaleActivity;

    iget-object v0, v0, Lepson/print/copy/CopyScaleActivity;->optionValueChangedListener:Lepson/print/copy/ActivityBase$OptionItemChangedListener;

    invoke-virtual {p1, v0}, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->setOptionValueChangedListener(Lepson/print/copy/ActivityBase$OptionItemChangedListener;)V

    return-void
.end method

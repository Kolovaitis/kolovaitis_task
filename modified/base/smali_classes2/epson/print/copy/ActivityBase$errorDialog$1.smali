.class Lepson/print/copy/ActivityBase$errorDialog$1;
.super Ljava/lang/Object;
.source "ActivityBase.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/copy/ActivityBase$errorDialog;->showErrorDialog(Ljava/lang/String;Ljava/lang/String;Lepson/print/copy/ActivityBase$DialogButtons;Lepson/print/copy/ActivityBase$IClose;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lepson/print/copy/ActivityBase$errorDialog;

.field final synthetic val$buttons:Lepson/print/copy/ActivityBase$DialogButtons;

.field final synthetic val$closeListener:Lepson/print/copy/ActivityBase$IClose;


# direct methods
.method constructor <init>(Lepson/print/copy/ActivityBase$errorDialog;Lepson/print/copy/ActivityBase$IClose;Lepson/print/copy/ActivityBase$DialogButtons;)V
    .locals 0

    .line 218
    iput-object p1, p0, Lepson/print/copy/ActivityBase$errorDialog$1;->this$1:Lepson/print/copy/ActivityBase$errorDialog;

    iput-object p2, p0, Lepson/print/copy/ActivityBase$errorDialog$1;->val$closeListener:Lepson/print/copy/ActivityBase$IClose;

    iput-object p3, p0, Lepson/print/copy/ActivityBase$errorDialog$1;->val$buttons:Lepson/print/copy/ActivityBase$DialogButtons;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    .line 220
    invoke-interface {p1}, Landroid/content/DialogInterface;->cancel()V

    .line 221
    iget-object p1, p0, Lepson/print/copy/ActivityBase$errorDialog$1;->val$closeListener:Lepson/print/copy/ActivityBase$IClose;

    if-eqz p1, :cond_1

    .line 222
    iget-object p2, p0, Lepson/print/copy/ActivityBase$errorDialog$1;->val$buttons:Lepson/print/copy/ActivityBase$DialogButtons;

    sget-object v0, Lepson/print/copy/ActivityBase$DialogButtons;->Ok:Lepson/print/copy/ActivityBase$DialogButtons;

    if-ne p2, v0, :cond_0

    sget-object p2, Lepson/print/copy/ActivityBase$ClickButton;->Ok:Lepson/print/copy/ActivityBase$ClickButton;

    goto :goto_0

    :cond_0
    sget-object p2, Lepson/print/copy/ActivityBase$ClickButton;->ClearError:Lepson/print/copy/ActivityBase$ClickButton;

    :goto_0
    invoke-interface {p1, p2}, Lepson/print/copy/ActivityBase$IClose;->onClose(Lepson/print/copy/ActivityBase$ClickButton;)V

    :cond_1
    return-void
.end method

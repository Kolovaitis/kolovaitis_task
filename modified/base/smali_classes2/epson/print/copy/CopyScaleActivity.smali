.class public Lepson/print/copy/CopyScaleActivity;
.super Lepson/print/copy/ActivityBase;
.source "CopyScaleActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;,
        Lepson/print/copy/CopyScaleActivity$XScaleValue;
    }
.end annotation


# instance fields
.field private TAG:Ljava/lang/String;

.field optionValueChangedListener:Lepson/print/copy/ActivityBase$OptionItemChangedListener;

.field settingItemKey:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 35
    invoke-direct {p0}, Lepson/print/copy/ActivityBase;-><init>()V

    const-string v0, "CppyScaleActivity"

    .line 36
    iput-object v0, p0, Lepson/print/copy/CopyScaleActivity;->TAG:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method buildCopyOptions(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;",
            ">;)V"
        }
    .end annotation

    .line 77
    iget-object v0, p0, Lepson/print/copy/CopyScaleActivity;->TAG:Ljava/lang/String;

    const-string v1, "buildCopyOptions"

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    .line 79
    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getKey()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    move-result-object v1

    .line 80
    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->CopyMagnification:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    if-ne v1, v2, :cond_1

    .line 81
    iget-object v2, p0, Lepson/print/copy/CopyScaleActivity;->optionValueMap:Ljava/util/HashMap;

    new-instance v3, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    invoke-direct {v3, p0, v0}, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;-><init>(Lepson/print/copy/CopyScaleActivity;Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    invoke-virtual {v2, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    :cond_1
    invoke-virtual {v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->name()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lepson/print/copy/CopyScaleActivity;->settingItemKey:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 84
    iget-object v2, p0, Lepson/print/copy/CopyScaleActivity;->optionValueMap:Ljava/util/HashMap;

    new-instance v3, Lepson/print/copy/CopyScaleActivity$XScaleValue;

    invoke-direct {v3, p0, v0}, Lepson/print/copy/CopyScaleActivity$XScaleValue;-><init>(Lepson/print/copy/CopyScaleActivity;Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    invoke-virtual {v2, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    const/4 v0, 0x1

    .line 387
    iput-boolean v0, p0, Lepson/print/copy/CopyScaleActivity;->isKeepSimpleAPConnection:Z

    .line 388
    invoke-virtual {p0}, Lepson/print/copy/CopyScaleActivity;->finish()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 43
    invoke-super {p0, p1}, Lepson/print/copy/ActivityBase;->onCreate(Landroid/os/Bundle;)V

    .line 44
    iget-object p1, p0, Lepson/print/copy/CopyScaleActivity;->TAG:Ljava/lang/String;

    const-string v0, "onCreate"

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const p1, 0x7f0a004e

    .line 45
    invoke-virtual {p0, p1}, Lepson/print/copy/CopyScaleActivity;->setContentView(I)V

    .line 47
    invoke-virtual {p0}, Lepson/print/copy/CopyScaleActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    if-eqz p1, :cond_0

    const-string v0, "Key"

    .line 49
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/print/copy/CopyScaleActivity;->settingItemKey:Ljava/lang/String;

    .line 52
    iget-object p1, p0, Lepson/print/copy/CopyScaleActivity;->settingItemKey:Ljava/lang/String;

    invoke-virtual {p0, p1}, Lepson/print/copy/CopyScaleActivity;->string(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lepson/print/copy/CopyScaleActivity;->setActionBar(Ljava/lang/String;Z)V

    .line 55
    :cond_0
    new-instance p1, Lepson/print/copy/CopyScaleActivity$1;

    invoke-direct {p1, p0}, Lepson/print/copy/CopyScaleActivity$1;-><init>(Lepson/print/copy/CopyScaleActivity;)V

    iput-object p1, p0, Lepson/print/copy/CopyScaleActivity;->optionValueChangedListener:Lepson/print/copy/ActivityBase$OptionItemChangedListener;

    .line 68
    iget-object p1, p0, Lepson/print/copy/CopyScaleActivity;->copyComponent:Lepson/print/copy/Component/ecopycomponent/ECopyComponent;

    invoke-virtual {p1}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->getCopyOptionItems()Ljava/util/ArrayList;

    move-result-object p1

    invoke-virtual {p0, p1}, Lepson/print/copy/CopyScaleActivity;->buildCopyOptions(Ljava/util/ArrayList;)V

    return-void
.end method

.method protected onPause()V
    .locals 2

    .line 413
    invoke-super {p0}, Lepson/print/copy/ActivityBase;->onPause()V

    .line 415
    iget-boolean v0, p0, Lepson/print/copy/CopyScaleActivity;->isKeepSimpleAPConnection:Z

    if-nez v0, :cond_0

    const-string v0, "printer"

    .line 417
    sget-object v1, Lepson/print/copy/ActivityBase;->printerIp:Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->disconnect(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 3

    .line 393
    invoke-super {p0}, Lepson/print/copy/ActivityBase;->onResume()V

    const-string v0, "printer"

    .line 396
    invoke-static {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->isNeedConnect(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 398
    iput-boolean v0, p0, Lepson/print/copy/CopyScaleActivity;->isTryConnectSimpleAp:Z

    goto :goto_0

    .line 399
    :cond_0
    iget-boolean v0, p0, Lepson/print/copy/CopyScaleActivity;->isTryConnectSimpleAp:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 401
    iput-boolean v0, p0, Lepson/print/copy/CopyScaleActivity;->isTryConnectSimpleAp:Z

    const-string v1, "printer"

    const/4 v2, -0x1

    .line 402
    invoke-static {p0, v1, v2}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->reconnect(Landroid/app/Activity;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 403
    iput-boolean v0, p0, Lepson/print/copy/CopyScaleActivity;->isKeepSimpleAPConnection:Z

    return-void

    :cond_1
    :goto_0
    return-void
.end method

.class Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1$1;
.super Ljava/lang/Object;
.source "CopyScaleActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;


# direct methods
.method constructor <init>(Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;)V
    .locals 0

    .line 305
    iput-object p1, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1$1;->this$2:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    .line 309
    iget-object p1, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1$1;->this$2:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;

    iget-object p1, p1, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    iget-object p1, p1, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->this$0:Lepson/print/copy/CopyScaleActivity;

    iget-object p1, p1, Lepson/print/copy/CopyScaleActivity;->optionValueMap:Ljava/util/HashMap;

    sget-object p2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XScale:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {p1, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lepson/print/copy/ActivityBase$ListOptionValue;

    .line 310
    iget-object p2, p1, Lepson/print/copy/ActivityBase$ListOptionValue;->optionItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_Custom:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    invoke-virtual {p2, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->selectChoice(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;)V

    .line 311
    iget-object p2, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1$1;->this$2:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;

    iget-object p2, p2, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    iget-object p2, p2, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->changedListener:Lepson/print/copy/ActivityBase$OptionItemChangedListener;

    if-eqz p2, :cond_0

    .line 312
    iget-object p2, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1$1;->this$2:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;

    iget-object p2, p2, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    iget-object p2, p2, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->changedListener:Lepson/print/copy/ActivityBase$OptionItemChangedListener;

    iget-object p1, p1, Lepson/print/copy/ActivityBase$ListOptionValue;->optionItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    invoke-interface {p2, p1}, Lepson/print/copy/ActivityBase$OptionItemChangedListener;->onOptionItemChanged(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    .line 316
    :cond_0
    iget-object p1, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1$1;->this$2:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;

    iget-object p1, p1, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    iget-object p1, p1, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->optionItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    iget-object p2, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1$1;->this$2:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;

    iget-object p2, p2, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    iget-object p2, p2, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->editText:Landroid/widget/TextView;

    invoke-virtual {p2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object p2

    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p2

    invoke-virtual {p1, p2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->selectValue(I)V

    .line 317
    iget-object p1, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1$1;->this$2:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;

    iget-object p1, p1, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    iget-object p1, p1, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->changedListener:Lepson/print/copy/ActivityBase$OptionItemChangedListener;

    if-eqz p1, :cond_1

    .line 318
    iget-object p1, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1$1;->this$2:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;

    iget-object p1, p1, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    iget-object p1, p1, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->changedListener:Lepson/print/copy/ActivityBase$OptionItemChangedListener;

    iget-object p2, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1$1;->this$2:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;

    iget-object p2, p2, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    iget-object p2, p2, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->optionItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    invoke-interface {p1, p2}, Lepson/print/copy/ActivityBase$OptionItemChangedListener;->onOptionItemChanged(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    .line 320
    :cond_1
    iget-object p1, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1$1;->this$2:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;

    iget-object p1, p1, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    iget-object p1, p1, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->this$0:Lepson/print/copy/CopyScaleActivity;

    const/4 p2, 0x1

    iput-boolean p2, p1, Lepson/print/copy/CopyScaleActivity;->isKeepSimpleAPConnection:Z

    .line 321
    iget-object p1, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1$1;->this$2:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;

    iget-object p1, p1, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    invoke-virtual {p1}, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->onDismissDialog()V

    .line 322
    iget-object p1, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1$1;->this$2:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;

    iget-object p1, p1, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    iget-object p1, p1, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->this$0:Lepson/print/copy/CopyScaleActivity;

    invoke-virtual {p1}, Lepson/print/copy/CopyScaleActivity;->finish()V

    return-void
.end method

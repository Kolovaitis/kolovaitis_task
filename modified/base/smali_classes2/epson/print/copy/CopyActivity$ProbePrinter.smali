.class Lepson/print/copy/CopyActivity$ProbePrinter;
.super Landroid/os/AsyncTask;
.source "CopyActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/copy/CopyActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ProbePrinter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private volatile mRepeatCopyAvailable:Z

.field final synthetic this$0:Lepson/print/copy/CopyActivity;


# direct methods
.method private constructor <init>(Lepson/print/copy/CopyActivity;)V
    .locals 0

    .line 653
    iput-object p1, p0, Lepson/print/copy/CopyActivity$ProbePrinter;->this$0:Lepson/print/copy/CopyActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lepson/print/copy/CopyActivity;Lepson/print/copy/CopyActivity$1;)V
    .locals 0

    .line 653
    invoke-direct {p0, p1}, Lepson/print/copy/CopyActivity$ProbePrinter;-><init>(Lepson/print/copy/CopyActivity;)V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 6

    .line 663
    iget-object p1, p0, Lepson/print/copy/CopyActivity$ProbePrinter;->this$0:Lepson/print/copy/CopyActivity;

    invoke-static {p1}, Lepson/print/copy/CopyActivity;->access$200(Lepson/print/copy/CopyActivity;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "ProbePrinter doInBackground"

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x0

    .line 664
    iput-boolean p1, p0, Lepson/print/copy/CopyActivity$ProbePrinter;->mRepeatCopyAvailable:Z

    .line 666
    iget-object v0, p0, Lepson/print/copy/CopyActivity$ProbePrinter;->this$0:Lepson/print/copy/CopyActivity;

    invoke-static {v0}, Lepson/print/copy/CopyActivity;->access$800(Lepson/print/copy/CopyActivity;)I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lepson/print/copy/CopyActivity$ProbePrinter;->this$0:Lepson/print/copy/CopyActivity;

    invoke-static {v0}, Lepson/print/copy/CopyActivity;->access$800(Lepson/print/copy/CopyActivity;)I

    move-result v0

    const/4 v2, 0x3

    if-ne v0, v2, :cond_3

    .line 668
    :cond_0
    sget-object v0, Lepson/print/copy/ActivityBase;->printerId:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 669
    iget-object v0, p0, Lepson/print/copy/CopyActivity$ProbePrinter;->this$0:Lepson/print/copy/CopyActivity;

    invoke-static {v0}, Lepson/print/copy/CopyActivity;->access$900(Lepson/print/copy/CopyActivity;)Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object v0

    iget-object v2, p0, Lepson/print/copy/CopyActivity$ProbePrinter;->this$0:Lepson/print/copy/CopyActivity;

    const/4 v3, 0x2

    invoke-virtual {v0, v2, v3}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doInitDriver(Landroid/content/Context;I)I

    .line 670
    iget-object v0, p0, Lepson/print/copy/CopyActivity$ProbePrinter;->this$0:Lepson/print/copy/CopyActivity;

    invoke-static {v0}, Lepson/print/copy/CopyActivity;->access$900(Lepson/print/copy/CopyActivity;)Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object v0

    const/16 v2, 0x3c

    sget-object v3, Lepson/print/copy/ActivityBase;->printerId:Ljava/lang/String;

    sget-object v4, Lepson/print/copy/ActivityBase;->printerIp:Ljava/lang/String;

    iget-object v5, p0, Lepson/print/copy/CopyActivity$ProbePrinter;->this$0:Lepson/print/copy/CopyActivity;

    invoke-static {v5}, Lepson/print/copy/CopyActivity;->access$800(Lepson/print/copy/CopyActivity;)I

    move-result v5

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doProbePrinter(ILjava/lang/String;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_1

    .line 673
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    .line 677
    :cond_1
    iget-object v0, p0, Lepson/print/copy/CopyActivity$ProbePrinter;->this$0:Lepson/print/copy/CopyActivity;

    invoke-static {v0}, Lepson/print/copy/CopyActivity;->access$900(Lepson/print/copy/CopyActivity;)Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object v0

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doSetPrinter()I

    move-result v0

    .line 678
    iget-object v2, p0, Lepson/print/copy/CopyActivity$ProbePrinter;->this$0:Lepson/print/copy/CopyActivity;

    invoke-static {v2}, Lepson/print/copy/CopyActivity;->access$200(Lepson/print/copy/CopyActivity;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Set Printer result: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v0, :cond_2

    .line 681
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    .line 684
    :cond_2
    iget-object p1, p0, Lepson/print/copy/CopyActivity$ProbePrinter;->this$0:Lepson/print/copy/CopyActivity;

    invoke-static {p1}, Lepson/print/copy/CopyActivity;->access$900(Lepson/print/copy/CopyActivity;)Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object v0

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doGetIp()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/copy/CopyActivity;->access$1002(Lepson/print/copy/CopyActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 685
    iget-object p1, p0, Lepson/print/copy/CopyActivity$ProbePrinter;->this$0:Lepson/print/copy/CopyActivity;

    invoke-static {p1}, Lepson/print/copy/CopyActivity;->access$200(Lepson/print/copy/CopyActivity;)Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "IPAdress : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lepson/print/copy/CopyActivity$ProbePrinter;->this$0:Lepson/print/copy/CopyActivity;

    invoke-static {v2}, Lepson/print/copy/CopyActivity;->access$1000(Lepson/print/copy/CopyActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 686
    iget-object p1, p0, Lepson/print/copy/CopyActivity$ProbePrinter;->this$0:Lepson/print/copy/CopyActivity;

    iput-boolean v1, p1, Lepson/print/copy/CopyActivity;->bProbedPrinter:Z

    .line 688
    new-instance v0, Lepson/print/copy/CopyActivity$ProbePrinter$1;

    invoke-direct {v0, p0}, Lepson/print/copy/CopyActivity$ProbePrinter$1;-><init>(Lepson/print/copy/CopyActivity$ProbePrinter;)V

    iput-object v0, p1, Lepson/print/copy/CopyActivity;->systemSettings:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;

    .line 700
    invoke-static {}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->sharedComponent()Lepson/print/copy/Component/ecopycomponent/ECopyComponent;

    move-result-object p1

    .line 701
    iget-object v0, p0, Lepson/print/copy/CopyActivity$ProbePrinter;->this$0:Lepson/print/copy/CopyActivity;

    iget-object v0, v0, Lepson/print/copy/CopyActivity;->systemSettings:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;

    invoke-virtual {p1, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->setSystemSettings(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;)V

    .line 702
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$Property;->RequestConnectionTimeout:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$Property;

    const/16 v2, 0x7530

    invoke-virtual {p1, v0, v2}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->setProperty(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$Property;I)V

    .line 703
    iget-object p1, p0, Lepson/print/copy/CopyActivity$ProbePrinter;->this$0:Lepson/print/copy/CopyActivity;

    invoke-static {p1}, Lepson/print/copy/CopyActivity;->access$1100(Lepson/print/copy/CopyActivity;)Z

    move-result p1

    iput-boolean p1, p0, Lepson/print/copy/CopyActivity$ProbePrinter;->mRepeatCopyAvailable:Z

    .line 705
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    .line 708
    :cond_3
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 653
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/print/copy/CopyActivity$ProbePrinter;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method protected onCancelled()V
    .locals 2

    .line 714
    iget-object v0, p0, Lepson/print/copy/CopyActivity$ProbePrinter;->this$0:Lepson/print/copy/CopyActivity;

    invoke-static {v0}, Lepson/print/copy/CopyActivity;->access$200(Lepson/print/copy/CopyActivity;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "ProbePrinter onCancelled"

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 715
    invoke-super {p0}, Landroid/os/AsyncTask;->onCancelled()V

    .line 716
    iget-object v0, p0, Lepson/print/copy/CopyActivity$ProbePrinter;->this$0:Lepson/print/copy/CopyActivity;

    invoke-static {v0}, Lepson/print/copy/CopyActivity;->access$900(Lepson/print/copy/CopyActivity;)Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object v0

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doCancelFindPrinter()I

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 2

    .line 722
    iget-object v0, p0, Lepson/print/copy/CopyActivity$ProbePrinter;->this$0:Lepson/print/copy/CopyActivity;

    invoke-static {v0}, Lepson/print/copy/CopyActivity;->access$200(Lepson/print/copy/CopyActivity;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "ProbePrinter onPostExecute"

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 724
    iget-object v0, p0, Lepson/print/copy/CopyActivity$ProbePrinter;->this$0:Lepson/print/copy/CopyActivity;

    iget-boolean v1, p0, Lepson/print/copy/CopyActivity$ProbePrinter;->mRepeatCopyAvailable:Z

    invoke-static {v0, v1}, Lepson/print/copy/CopyActivity;->access$1200(Lepson/print/copy/CopyActivity;Z)V

    .line 725
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 726
    iget-object p1, p0, Lepson/print/copy/CopyActivity$ProbePrinter;->this$0:Lepson/print/copy/CopyActivity;

    iget-object p1, p1, Lepson/print/copy/CopyActivity;->mHandler:Landroid/os/Handler;

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 728
    :cond_0
    iget-object p1, p0, Lepson/print/copy/CopyActivity$ProbePrinter;->this$0:Lepson/print/copy/CopyActivity;

    iget-object p1, p1, Lepson/print/copy/CopyActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 653
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lepson/print/copy/CopyActivity$ProbePrinter;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    return-void
.end method

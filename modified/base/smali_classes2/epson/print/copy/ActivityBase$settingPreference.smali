.class Lepson/print/copy/ActivityBase$settingPreference;
.super Ljava/lang/Object;
.source "ActivityBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/copy/ActivityBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "settingPreference"
.end annotation


# instance fields
.field editer:Landroid/content/SharedPreferences$Editor;

.field presettings:Landroid/content/SharedPreferences;

.field final synthetic this$0:Lepson/print/copy/ActivityBase;


# direct methods
.method constructor <init>(Lepson/print/copy/ActivityBase;)V
    .locals 0

    .line 552
    iput-object p1, p0, Lepson/print/copy/ActivityBase$settingPreference;->this$0:Lepson/print/copy/ActivityBase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method loadPrePrinter()Ljava/lang/String;
    .locals 3

    .line 560
    iget-object v0, p0, Lepson/print/copy/ActivityBase$settingPreference;->this$0:Lepson/print/copy/ActivityBase;

    const-string v1, "CopySetting"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lepson/print/copy/ActivityBase;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lepson/print/copy/ActivityBase$settingPreference;->presettings:Landroid/content/SharedPreferences;

    .line 561
    iget-object v0, p0, Lepson/print/copy/ActivityBase$settingPreference;->presettings:Landroid/content/SharedPreferences;

    const-string v1, "PRINTER_ID"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method saveCopyOptions(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;",
            ">;)V"
        }
    .end annotation

    .line 625
    invoke-virtual {p0}, Lepson/print/copy/ActivityBase$settingPreference;->savePrePrinter()V

    .line 627
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    .line 628
    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getKey()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    move-result-object v1

    .line 629
    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->Copies:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v1, v2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 631
    invoke-virtual {p0, v1, v0}, Lepson/print/copy/ActivityBase$settingPreference;->saveOption(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method saveOption(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V
    .locals 3

    .line 641
    iget-object v0, p0, Lepson/print/copy/ActivityBase$settingPreference;->this$0:Lepson/print/copy/ActivityBase;

    const-string v1, "CopySetting"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lepson/print/copy/ActivityBase;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lepson/print/copy/ActivityBase$settingPreference;->presettings:Landroid/content/SharedPreferences;

    .line 642
    invoke-virtual {p2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getChoiceType()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;

    move-result-object v0

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;->ChoiceArray:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;

    if-ne v0, v1, :cond_0

    .line 644
    iget-object v0, p0, Lepson/print/copy/ActivityBase$settingPreference;->this$0:Lepson/print/copy/ActivityBase;

    invoke-static {v0}, Lepson/print/copy/ActivityBase;->access$000(Lepson/print/copy/ActivityBase;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cursetting:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v2

    invoke-virtual {v2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 645
    iget-object v0, p0, Lepson/print/copy/ActivityBase$settingPreference;->presettings:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iput-object v0, p0, Lepson/print/copy/ActivityBase$settingPreference;->editer:Landroid/content/SharedPreferences$Editor;

    .line 646
    iget-object v0, p0, Lepson/print/copy/ActivityBase$settingPreference;->editer:Landroid/content/SharedPreferences$Editor;

    invoke-virtual {p1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->name()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object p2

    invoke-virtual {p2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->getParam()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object p2

    invoke-virtual {p2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 647
    iget-object p1, p0, Lepson/print/copy/ActivityBase$settingPreference;->editer:Landroid/content/SharedPreferences$Editor;

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0

    .line 650
    :cond_0
    iget-object v0, p0, Lepson/print/copy/ActivityBase$settingPreference;->this$0:Lepson/print/copy/ActivityBase;

    invoke-static {v0}, Lepson/print/copy/ActivityBase;->access$000(Lepson/print/copy/ActivityBase;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cursetting:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 651
    iget-object v0, p0, Lepson/print/copy/ActivityBase$settingPreference;->presettings:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iput-object v0, p0, Lepson/print/copy/ActivityBase$settingPreference;->editer:Landroid/content/SharedPreferences$Editor;

    .line 652
    iget-object v0, p0, Lepson/print/copy/ActivityBase$settingPreference;->editer:Landroid/content/SharedPreferences$Editor;

    invoke-virtual {p1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->name()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedValue()I

    move-result p2

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 653
    iget-object p1, p0, Lepson/print/copy/ActivityBase$settingPreference;->editer:Landroid/content/SharedPreferences$Editor;

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :goto_0
    return-void
.end method

.method savePrePrinter()V
    .locals 3

    .line 568
    iget-object v0, p0, Lepson/print/copy/ActivityBase$settingPreference;->this$0:Lepson/print/copy/ActivityBase;

    const-string v1, "CopySetting"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lepson/print/copy/ActivityBase;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lepson/print/copy/ActivityBase$settingPreference;->presettings:Landroid/content/SharedPreferences;

    .line 570
    iget-object v0, p0, Lepson/print/copy/ActivityBase$settingPreference;->presettings:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iput-object v0, p0, Lepson/print/copy/ActivityBase$settingPreference;->editer:Landroid/content/SharedPreferences$Editor;

    .line 571
    iget-object v0, p0, Lepson/print/copy/ActivityBase$settingPreference;->editer:Landroid/content/SharedPreferences$Editor;

    const-string v1, "PRINTER_ID"

    sget-object v2, Lepson/print/copy/ActivityBase;->printerId:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 572
    iget-object v0, p0, Lepson/print/copy/ActivityBase$settingPreference;->editer:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method setCopyOptions(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;",
            ">;)V"
        }
    .end annotation

    .line 580
    iget-object v0, p0, Lepson/print/copy/ActivityBase$settingPreference;->this$0:Lepson/print/copy/ActivityBase;

    iget-object v0, v0, Lepson/print/copy/ActivityBase;->copyComponent:Lepson/print/copy/Component/ecopycomponent/ECopyComponent;

    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->getBindedCopyOptionContext()Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    move-result-object v0

    .line 582
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    .line 583
    invoke-virtual {v2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getKey()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    move-result-object v3

    .line 584
    sget-object v4, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->Copies:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v3, v4}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 586
    invoke-virtual {p0, v3, v2}, Lepson/print/copy/ActivityBase$settingPreference;->setOption(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    .line 587
    invoke-virtual {v0, v2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->replace(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    goto :goto_0

    .line 591
    :cond_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    .line 594
    iget-object v1, p0, Lepson/print/copy/ActivityBase$settingPreference;->this$0:Lepson/print/copy/ActivityBase;

    iget-object v1, v1, Lepson/print/copy/ActivityBase;->copyComponent:Lepson/print/copy/Component/ecopycomponent/ECopyComponent;

    invoke-virtual {v1, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->setCopyOptionItem(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method setOption(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V
    .locals 3

    .line 604
    iget-object v0, p0, Lepson/print/copy/ActivityBase$settingPreference;->this$0:Lepson/print/copy/ActivityBase;

    const-string v1, "CopySetting"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lepson/print/copy/ActivityBase;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lepson/print/copy/ActivityBase$settingPreference;->presettings:Landroid/content/SharedPreferences;

    .line 605
    invoke-virtual {p2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getChoiceType()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;

    move-result-object v0

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;->ChoiceArray:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;

    if-ne v0, v1, :cond_0

    .line 607
    iget-object v0, p0, Lepson/print/copy/ActivityBase$settingPreference;->presettings:Landroid/content/SharedPreferences;

    invoke-virtual {p1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getDefaultChoice()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v2

    invoke-virtual {v2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->getParam()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v2

    invoke-virtual {v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 608
    invoke-static {v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->stringOf(Ljava/lang/String;)Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v0

    .line 609
    invoke-static {p1, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->valueOf(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object p1

    invoke-virtual {p2, p1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->selectChoice(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;)V

    goto :goto_0

    .line 612
    :cond_0
    iget-object v0, p0, Lepson/print/copy/ActivityBase$settingPreference;->presettings:Landroid/content/SharedPreferences;

    invoke-virtual {p1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getDefaultValue()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 613
    invoke-virtual {p2, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->selectValue(I)V

    .line 614
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->CopyMagnification:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {p1, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 615
    iget-object v0, p0, Lepson/print/copy/ActivityBase$settingPreference;->this$0:Lepson/print/copy/ActivityBase;

    iget-object v0, v0, Lepson/print/copy/ActivityBase;->optionValueMap:Ljava/util/HashMap;

    new-instance v1, Lepson/print/copy/ActivityBase$CopyMagnificationValue;

    iget-object v2, p0, Lepson/print/copy/ActivityBase$settingPreference;->this$0:Lepson/print/copy/ActivityBase;

    invoke-direct {v1, v2, p2}, Lepson/print/copy/ActivityBase$CopyMagnificationValue;-><init>(Lepson/print/copy/ActivityBase;Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    :goto_0
    return-void
.end method

.class Lepson/print/copy/ActivityBase$1;
.super Ljava/lang/Object;
.source "ActivityBase.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/copy/ActivityBase;->showNextPageDialog(Lepson/print/copy/ActivityBase$INextPageClose;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/copy/ActivityBase;

.field final synthetic val$closeListener:Lepson/print/copy/ActivityBase$INextPageClose;


# direct methods
.method constructor <init>(Lepson/print/copy/ActivityBase;Lepson/print/copy/ActivityBase$INextPageClose;)V
    .locals 0

    .line 384
    iput-object p1, p0, Lepson/print/copy/ActivityBase$1;->this$0:Lepson/print/copy/ActivityBase;

    iput-object p2, p0, Lepson/print/copy/ActivityBase$1;->val$closeListener:Lepson/print/copy/ActivityBase$INextPageClose;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 386
    invoke-interface {p1}, Landroid/content/DialogInterface;->cancel()V

    const/4 p1, -0x1

    if-ne p2, p1, :cond_0

    .line 389
    sget-object p1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$ResumeState;->NextPageReady:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$ResumeState;

    goto :goto_0

    :cond_0
    const/4 p1, -0x3

    if-ne p2, p1, :cond_1

    .line 391
    sget-object p1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$ResumeState;->NextPageNotExist:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$ResumeState;

    goto :goto_0

    .line 393
    :cond_1
    sget-object p1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$ResumeState;->Cancel:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$ResumeState;

    .line 395
    :goto_0
    iget-object p2, p0, Lepson/print/copy/ActivityBase$1;->val$closeListener:Lepson/print/copy/ActivityBase$INextPageClose;

    invoke-interface {p2, p1}, Lepson/print/copy/ActivityBase$INextPageClose;->onClose(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$ResumeState;)V

    return-void
.end method

.class public Lepson/print/copy/CopySettingActivity;
.super Lepson/print/copy/ActivityBase;
.source "CopySettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/copy/CopySettingActivity$OthersettingValue;
    }
.end annotation


# instance fields
.field private TAG:Ljava/lang/String;

.field optionListener:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;

.field optionValueChangedListener:Lepson/print/copy/ActivityBase$OptionItemChangedListener;

.field settingItemKey:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 30
    invoke-direct {p0}, Lepson/print/copy/ActivityBase;-><init>()V

    const-string v0, "CopySettingActivity"

    .line 31
    iput-object v0, p0, Lepson/print/copy/CopySettingActivity;->TAG:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method buildCopyOptions(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;",
            ">;)V"
        }
    .end annotation

    .line 86
    iget-object v0, p0, Lepson/print/copy/CopySettingActivity;->TAG:Ljava/lang/String;

    const-string v1, "buildCopyOptions"

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    .line 88
    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getKey()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    move-result-object v1

    .line 89
    invoke-virtual {v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->name()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lepson/print/copy/CopySettingActivity;->settingItemKey:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 90
    iget-object v2, p0, Lepson/print/copy/CopySettingActivity;->optionValueMap:Ljava/util/HashMap;

    new-instance v3, Lepson/print/copy/CopySettingActivity$OthersettingValue;

    invoke-direct {v3, p0, v0}, Lepson/print/copy/CopySettingActivity$OthersettingValue;-><init>(Lepson/print/copy/CopySettingActivity;Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    invoke-virtual {v2, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    const/4 v0, 0x1

    .line 180
    iput-boolean v0, p0, Lepson/print/copy/CopySettingActivity;->isKeepSimpleAPConnection:Z

    .line 181
    invoke-virtual {p0}, Lepson/print/copy/CopySettingActivity;->finish()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .line 39
    invoke-super {p0, p1}, Lepson/print/copy/ActivityBase;->onCreate(Landroid/os/Bundle;)V

    .line 40
    iget-object p1, p0, Lepson/print/copy/CopySettingActivity;->TAG:Ljava/lang/String;

    const-string v0, "onCreate"

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const p1, 0x7f0a004f

    .line 41
    invoke-virtual {p0, p1}, Lepson/print/copy/CopySettingActivity;->setContentView(I)V

    .line 43
    invoke-virtual {p0}, Lepson/print/copy/CopySettingActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    if-eqz p1, :cond_0

    const-string v0, "Key"

    .line 45
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/print/copy/CopySettingActivity;->settingItemKey:Ljava/lang/String;

    .line 48
    iget-object p1, p0, Lepson/print/copy/CopySettingActivity;->settingItemKey:Ljava/lang/String;

    invoke-virtual {p0, p1}, Lepson/print/copy/CopySettingActivity;->string(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lepson/print/copy/CopySettingActivity;->setActionBar(Ljava/lang/String;Z)V

    .line 51
    :cond_0
    new-instance p1, Lepson/print/copy/CopySettingActivity$1;

    invoke-direct {p1, p0}, Lepson/print/copy/CopySettingActivity$1;-><init>(Lepson/print/copy/CopySettingActivity;)V

    iput-object p1, p0, Lepson/print/copy/CopySettingActivity;->optionValueChangedListener:Lepson/print/copy/ActivityBase$OptionItemChangedListener;

    .line 58
    new-instance p1, Lepson/print/copy/CopySettingActivity$2;

    invoke-direct {p1, p0}, Lepson/print/copy/CopySettingActivity$2;-><init>(Lepson/print/copy/CopySettingActivity;)V

    iput-object p1, p0, Lepson/print/copy/CopySettingActivity;->optionListener:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;

    .line 74
    iget-object p1, p0, Lepson/print/copy/CopySettingActivity;->copyComponent:Lepson/print/copy/Component/ecopycomponent/ECopyComponent;

    invoke-virtual {p1}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->getBindedCopyOptionContext()Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    move-result-object p1

    iput-object p1, p0, Lepson/print/copy/CopySettingActivity;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    .line 75
    iget-object p1, p0, Lepson/print/copy/CopySettingActivity;->copyComponent:Lepson/print/copy/Component/ecopycomponent/ECopyComponent;

    iget-object v0, p0, Lepson/print/copy/CopySettingActivity;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    iget-object v1, p0, Lepson/print/copy/CopySettingActivity;->optionListener:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;

    invoke-virtual {p1, v0, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->bindCopyOptionContext(Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;)V

    .line 77
    iget-object p1, p0, Lepson/print/copy/CopySettingActivity;->copyComponent:Lepson/print/copy/Component/ecopycomponent/ECopyComponent;

    invoke-virtual {p1}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->getCopyOptionItems()Ljava/util/ArrayList;

    move-result-object p1

    invoke-virtual {p0, p1}, Lepson/print/copy/CopySettingActivity;->buildCopyOptions(Ljava/util/ArrayList;)V

    return-void
.end method

.method protected onPause()V
    .locals 2

    .line 206
    invoke-super {p0}, Lepson/print/copy/ActivityBase;->onPause()V

    .line 208
    iget-boolean v0, p0, Lepson/print/copy/CopySettingActivity;->isKeepSimpleAPConnection:Z

    if-nez v0, :cond_0

    const-string v0, "printer"

    .line 210
    sget-object v1, Lepson/print/copy/ActivityBase;->printerIp:Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->disconnect(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 3

    .line 186
    invoke-super {p0}, Lepson/print/copy/ActivityBase;->onResume()V

    const-string v0, "printer"

    .line 189
    invoke-static {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->isNeedConnect(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 191
    iput-boolean v0, p0, Lepson/print/copy/CopySettingActivity;->isTryConnectSimpleAp:Z

    goto :goto_0

    .line 192
    :cond_0
    iget-boolean v0, p0, Lepson/print/copy/CopySettingActivity;->isTryConnectSimpleAp:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 194
    iput-boolean v0, p0, Lepson/print/copy/CopySettingActivity;->isTryConnectSimpleAp:Z

    const-string v1, "printer"

    const/4 v2, -0x1

    .line 195
    invoke-static {p0, v1, v2}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->reconnect(Landroid/app/Activity;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 196
    iput-boolean v0, p0, Lepson/print/copy/CopySettingActivity;->isKeepSimpleAPConnection:Z

    return-void

    :cond_1
    :goto_0
    return-void
.end method

.class public interface abstract Lepson/print/copy/Component/escandevice/IScanController;
.super Ljava/lang/Object;
.source "IScanController.java"


# static fields
.field public static final CPPREV_KEY_SCANNER_IP:I = 0x4

.field public static final CPPREV_KEY_SCAN_COLORTYPE:I = 0x1

.field public static final CPPREV_KEY_SCAN_COPYFRAME:I = 0x3

.field public static final CPPREV_KEY_SCAN_RESO:I = 0x2

.field public static final CPPREV_STATUS_ERROR_CANCELED:I = 0x1

.field public static final CPPREV_STATUS_ERROR_DOCDTCT:I = 0x5

.field public static final CPPREV_STATUS_ERROR_IMGCRCT:I = 0x6

.field public static final CPPREV_STATUS_ERROR_NONE:I = 0x0

.field public static final CPPREV_STATUS_ERROR_SCANNER_BUSY:I = 0x3

.field public static final CPPREV_STATUS_ERROR_SCANNER_COMMUNICATION:I = 0x7

.field public static final CPPREV_STATUS_ERROR_SCANNER_GENERAL:I = 0x2

.field public static final CPPREV_STATUS_ERROR_SCANNER_OTHERS:I = 0x4

.field public static final CPPREV_STATUS_PAGE_ENDED:I = 0x4

.field public static final CPPREV_STATUS_PAGE_STARTED:I = 0x3

.field public static final CPPREV_STATUS_SCAN_FINISHED:I = 0x2

.field public static final CPPREV_STATUS_SCAN_STARTED:I = 0x1

.field public static final CPPREV_VALUE_SCAN_COLORTYPE_COLOR:I = 0x10000

.field public static final CPPREV_VALUE_SCAN_COLORTYPE_GRAYSCALE:I = 0x10001

.field public static final CPPREV_VALUE_SCAN_COPYFRAME_2L:I = 0x30004

.field public static final CPPREV_VALUE_SCAN_COPYFRAME_6GIRI:I = 0x30007

.field public static final CPPREV_VALUE_SCAN_COPYFRAME_A4:I = 0x30001

.field public static final CPPREV_VALUE_SCAN_COPYFRAME_AUTOFIT:I = 0x30000

.field public static final CPPREV_VALUE_SCAN_COPYFRAME_B5:I = 0x30002

.field public static final CPPREV_VALUE_SCAN_COPYFRAME_CD:I = 0x30008

.field public static final CPPREV_VALUE_SCAN_COPYFRAME_HAGAKI:I = 0x30005

.field public static final CPPREV_VALUE_SCAN_COPYFRAME_KG:I = 0x30006

.field public static final CPPREV_VALUE_SCAN_COPYFRAME_L:I = 0x30003

.field public static final CPPREV_VALUE_SCAN_RESO_PREVIEW:I = 0x20001

.field public static final CPPREV_VALUE_SCAN_RESO_PREVIEW_LOW:I = 0x20000


# virtual methods
.method public abstract getIntegerValue(I)I
.end method

.method public abstract getStringValue(I)Ljava/lang/String;
.end method

.method public abstract isCancelRequested()Z
.end method

.method public abstract setCopyFrameSize(IIII)V
.end method

.method public abstract setScanImageBlockReceived([IIIII)V
.end method

.method public abstract setScanImageSize(II)V
.end method

.method public abstract setScanStatusChanged(II)V
.end method

.class public Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;
.super Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;
.source "ERemoteCopyPhoto.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$ERemoteCopyPhotoOptionResult;,
        Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$IRemoteCopyPhotoParameter;,
        Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$IRemoteCopyPhotoOptionsParameter;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;-><init>()V

    return-void
.end method


# virtual methods
.method public cancel(Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$IRemoteCancelParameter;)Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;
    .locals 3

    .line 167
    iget-object v0, p0, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;->hostIP:Ljava/lang/String;

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->cancel:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    invoke-virtual {p0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;->getRequestConnectionTimeout()I

    move-result v2

    invoke-static {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->requestCopyPhoto(Ljava/lang/String;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;I)Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;

    move-result-object v0

    .line 168
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->client_id:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$IRemoteCancelParameter;->client_id()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/lang/String;)V

    .line 169
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->job_token:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$IRemoteCancelParameter;->job_token()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/lang/String;)V

    .line 170
    new-instance p1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;

    invoke-virtual {v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->getRemoteRequest()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;

    move-result-object v1

    invoke-virtual {v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;->execute()Lorg/json/JSONObject;

    move-result-object v1

    invoke-direct {p1, p0, v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;-><init>(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation;Lorg/json/JSONObject;)V

    .line 171
    invoke-virtual {p1, v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;->setRemoteRequestBuilder(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;)V

    return-object p1
.end method

.method public copy(Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$IRemoteCopyPhotoParameter;)Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyResult;
    .locals 3

    .line 86
    iget-object v0, p0, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;->hostIP:Ljava/lang/String;

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->copy:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    invoke-virtual {p0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;->getRequestConnectionTimeout()I

    move-result v2

    invoke-static {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->requestCopyPhoto(Ljava/lang/String;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;I)Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;

    move-result-object v0

    .line 87
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->client_id:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$IRemoteCopyPhotoParameter;->client_id()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/lang/String;)V

    .line 88
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->layout:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$IRemoteCopyPhotoParameter;->layout()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 89
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_type:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$IRemoteCopyPhotoParameter;->print_media_type()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 90
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_size:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$IRemoteCopyPhotoParameter;->print_media_size()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 91
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_quality:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$IRemoteCopyPhotoParameter;->print_quality()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 92
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_source:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$IRemoteCopyPhotoParameter;->print_media_source()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 93
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_apf:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$IRemoteCopyPhotoParameter;->x_apf()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 94
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_color_restoration:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$IRemoteCopyPhotoParameter;->x_color_restoration()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 95
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->color_effects_type:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$IRemoteCopyPhotoParameter;->color_effects_type()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 96
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scan_count:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$IRemoteCopyPhotoParameter;->scan_count()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;I)V

    .line 97
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->copies:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$IRemoteCopyPhotoParameter;->copies()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/util/List;)V

    .line 98
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scan_area_x:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$IRemoteCopyPhotoParameter;->scan_area_x()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/util/List;)V

    .line 99
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scan_area_y:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$IRemoteCopyPhotoParameter;->scan_area_y()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/util/List;)V

    .line 100
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scan_area_width:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$IRemoteCopyPhotoParameter;->scan_area_width()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/util/List;)V

    .line 101
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scan_area_height:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$IRemoteCopyPhotoParameter;->scan_area_height()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/util/List;)V

    .line 102
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scan_area_resolution:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$IRemoteCopyPhotoParameter;->scan_area_resolution()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/util/List;)V

    .line 103
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_x_bleed:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$IRemoteCopyPhotoParameter;->print_x_bleed()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 104
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_x_auto_pg:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$IRemoteCopyPhotoParameter;->print_x_auto_pg()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 105
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_fit_gamma:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$IRemoteCopyPhotoParameter;->x_fit_gamma()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/util/List;)V

    .line 106
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_fit_matrix:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$IRemoteCopyPhotoParameter;->x_fit_matrix()Ljava/util/ArrayList;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/util/List;)V

    .line 107
    new-instance p1, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyResult;

    invoke-virtual {v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->getRemoteRequest()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;

    move-result-object v1

    invoke-virtual {v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;->execute()Lorg/json/JSONObject;

    move-result-object v1

    invoke-direct {p1, p0, v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyResult;-><init>(Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;Lorg/json/JSONObject;)V

    .line 108
    invoke-virtual {p1, v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyResult;->setRemoteRequestBuilder(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;)V

    return-object p1
.end method

.method public getOptions(Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$IRemoteCopyPhotoOptionsParameter;)Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$ERemoteCopyPhotoOptionResult;
    .locals 3

    .line 113
    iget-object v0, p0, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;->hostIP:Ljava/lang/String;

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->get_options:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    invoke-virtual {p0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;->getRequestConnectionTimeout()I

    move-result v2

    invoke-static {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->requestCopyPhoto(Ljava/lang/String;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;I)Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;

    move-result-object v0

    .line 114
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->client_id:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$IRemoteCopyPhotoOptionsParameter;->client_id()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/lang/String;)V

    .line 115
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->layout:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$IRemoteCopyPhotoOptionsParameter;->layout()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 116
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_type:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$IRemoteCopyPhotoOptionsParameter;->print_media_type()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 117
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_size:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$IRemoteCopyPhotoOptionsParameter;->print_media_size()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 118
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_quality:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$IRemoteCopyPhotoOptionsParameter;->print_quality()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 119
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_source:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$IRemoteCopyPhotoOptionsParameter;->print_media_source()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 120
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_apf:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$IRemoteCopyPhotoOptionsParameter;->x_apf()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 121
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->fixed_parameters:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$IRemoteCopyPhotoOptionsParameter;->fixed_parameters()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/util/ArrayList;)V

    .line 122
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->preferred_parameters:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$IRemoteCopyPhotoOptionsParameter;->preferred_parameters()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/util/ArrayList;)V

    .line 123
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->priority_order:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$IRemoteCopyPhotoOptionsParameter;->priority_order()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/util/ArrayList;)V

    .line 124
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->default_as_fixed:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$IRemoteCopyPhotoOptionsParameter;->default_as_fixed()Z

    move-result p1

    invoke-virtual {v0, v1, p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Z)V

    .line 125
    new-instance p1, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$ERemoteCopyPhotoOptionResult;

    invoke-virtual {v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->getRemoteRequest()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;

    move-result-object v1

    invoke-virtual {v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;->execute()Lorg/json/JSONObject;

    move-result-object v1

    invoke-direct {p1, p0, v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$ERemoteCopyPhotoOptionResult;-><init>(Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;Lorg/json/JSONObject;)V

    .line 126
    invoke-virtual {p1, v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$ERemoteCopyPhotoOptionResult;->setRemoteRequestBuilder(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;)V

    return-object p1
.end method

.method public bridge synthetic getSelectableOptions()Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;->getSelectableOptions()Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$ERemoteCopyPhotoOptionResult;

    move-result-object v0

    return-object v0
.end method

.method public getSelectableOptions()Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$ERemoteCopyPhotoOptionResult;
    .locals 4

    .line 131
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 132
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->layout:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 133
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_size:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 134
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_type:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 135
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_apf:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 136
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_quality:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 137
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_source:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 139
    iget-object v1, p0, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;->hostIP:Ljava/lang/String;

    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->get_options:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    invoke-virtual {p0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;->getRequestConnectionTimeout()I

    move-result v3

    invoke-static {v1, v2, v3}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->requestCopyPhoto(Ljava/lang/String;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;I)Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;

    move-result-object v1

    .line 140
    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->client_id:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_null:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    iget-object v3, v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->string:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/lang/String;)V

    .line 141
    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->layout:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_null:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v1, v2, v3}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 142
    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_type:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_null:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v1, v2, v3}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 143
    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_size:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_null:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v1, v2, v3}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 144
    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_quality:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_null:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v1, v2, v3}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 145
    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_source:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_null:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v1, v2, v3}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 146
    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_apf:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_null:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v1, v2, v3}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 147
    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->fixed_parameters:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v1, v2, v3}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/util/ArrayList;)V

    .line 148
    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->preferred_parameters:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v1, v2, v3}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/util/ArrayList;)V

    .line 149
    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->priority_order:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v1, v2, v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/util/ArrayList;)V

    .line 150
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->default_as_fixed:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Z)V

    .line 151
    new-instance v0, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$ERemoteCopyPhotoOptionResult;

    invoke-virtual {v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->getRemoteRequest()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;

    move-result-object v2

    invoke-virtual {v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;->execute()Lorg/json/JSONObject;

    move-result-object v2

    invoke-direct {v0, p0, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$ERemoteCopyPhotoOptionResult;-><init>(Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;Lorg/json/JSONObject;)V

    .line 152
    invoke-virtual {v0, v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$ERemoteCopyPhotoOptionResult;->setRemoteRequestBuilder(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;)V

    return-object v0
.end method

.method public getStatus(Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyStatusParameter;)Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;
    .locals 3

    .line 157
    iget-object v0, p0, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;->hostIP:Ljava/lang/String;

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->get_status:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    invoke-virtual {p0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;->getRequestConnectionTimeout()I

    move-result v2

    invoke-static {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->requestCopyPhoto(Ljava/lang/String;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;I)Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;

    move-result-object v0

    .line 158
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->client_id:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyStatusParameter;->client_id()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/lang/String;)V

    .line 159
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->keys:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyStatusParameter;->keys()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/util/ArrayList;)V

    .line 160
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->job_token:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyStatusParameter;->job_token()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/lang/String;)V

    .line 161
    new-instance p1, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;

    invoke-virtual {v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->getRemoteRequest()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;

    move-result-object v1

    invoke-virtual {v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;->execute()Lorg/json/JSONObject;

    move-result-object v1

    invoke-direct {p1, p0, v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;-><init>(Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;Lorg/json/JSONObject;)V

    .line 162
    invoke-virtual {p1, v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->setRemoteRequestBuilder(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;)V

    return-object p1
.end method

.class public Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult$ScaleOptions;
.super Ljava/lang/Object;
.source "ERemoteCopy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ScaleOptions"
.end annotation


# instance fields
.field mediaSizeList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ">;"
        }
    .end annotation
.end field

.field scalePresets:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$1:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;


# direct methods
.method public constructor <init>(Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ">;)V"
        }
    .end annotation

    .line 147
    iput-object p1, p0, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult$ScaleOptions;->this$1:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 148
    iput-object p2, p0, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult$ScaleOptions;->mediaSizeList:Ljava/util/ArrayList;

    return-void
.end method

.method private checkPaperSize(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V
    .locals 1

    .line 218
    iget-object v0, p0, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult$ScaleOptions;->mediaSizeList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult$ScaleOptions;->mediaSizeList:Ljava/util/ArrayList;

    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 219
    iget-object p1, p0, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult$ScaleOptions;->scalePresets:Ljava/util/ArrayList;

    invoke-virtual {p1, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method


# virtual methods
.method public scale_options()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ">;"
        }
    .end annotation

    .line 152
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult$ScaleOptions;->scalePresets:Ljava/util/ArrayList;

    .line 153
    iget-object v0, p0, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult$ScaleOptions;->scalePresets:Ljava/util/ArrayList;

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_autofit:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 154
    iget-object v0, p0, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult$ScaleOptions;->scalePresets:Ljava/util/ArrayList;

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_fullsize:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 155
    iget-object v0, p0, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult$ScaleOptions;->scalePresets:Ljava/util/ArrayList;

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_custom:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 158
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_letter_8_5x11in:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_index_4x6_4x6in:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_letter_to_kg:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-direct {p0, v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult$ScaleOptions;->checkPaperSize(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 160
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_index_4x6_4x6in:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_letter_8_5x11in:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_kg_to_letter:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-direct {p0, v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult$ScaleOptions;->checkPaperSize(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 162
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_letter_8_5x11in:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_5x7_5x7in:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_letter_to_2l:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-direct {p0, v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult$ScaleOptions;->checkPaperSize(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 164
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_5x7_5x7in:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_letter_8_5x11in:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_2l_to_letter:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-direct {p0, v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult$ScaleOptions;->checkPaperSize(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 166
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_index_4x6_4x6in:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->iso_a4_210x297mm:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_kg_to_a4:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-direct {p0, v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult$ScaleOptions;->checkPaperSize(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 168
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->iso_a4_210x297mm:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_index_4x6_4x6in:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_a4_to_kg:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-direct {p0, v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult$ScaleOptions;->checkPaperSize(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 170
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_5x7_5x7in:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->iso_a4_210x297mm:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_2l_to_a4:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-direct {p0, v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult$ScaleOptions;->checkPaperSize(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 172
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->iso_a4_210x297mm:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_5x7_5x7in:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_a4_to_2l:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-direct {p0, v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult$ScaleOptions;->checkPaperSize(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 174
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_index_4x6_4x6in:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_govt_letter_8x10in:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_kg_to_8x10:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-direct {p0, v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult$ScaleOptions;->checkPaperSize(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 176
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_govt_letter_8x10in:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_5x7_5x7in:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_8x10_to_2l:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-direct {p0, v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult$ScaleOptions;->checkPaperSize(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 183
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->iso_a4_210x297mm:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->jpn_hagaki_100x148mm:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_a4_to_postcard:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-direct {p0, v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult$ScaleOptions;->checkPaperSize(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 185
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_5x7_5x7in:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->jpn_hagaki_100x148mm:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_2l_to_postcard:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-direct {p0, v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult$ScaleOptions;->checkPaperSize(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 187
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->oe_photo_l_3_5x5in:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->jpn_hagaki_100x148mm:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_l_to_postcard:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-direct {p0, v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult$ScaleOptions;->checkPaperSize(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 189
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->oe_photo_l_3_5x5in:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_5x7_5x7in:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_l_to_2l:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-direct {p0, v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult$ScaleOptions;->checkPaperSize(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 191
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->jpn_hagaki_100x148mm:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->iso_a4_210x297mm:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_postcard_to_a4:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-direct {p0, v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult$ScaleOptions;->checkPaperSize(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 193
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->oe_photo_l_3_5x5in:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->iso_a4_210x297mm:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_l_to_a4:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-direct {p0, v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult$ScaleOptions;->checkPaperSize(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 195
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->iso_a4_210x297mm:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->jis_b5_182x257mm:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_a4_to_b5:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-direct {p0, v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult$ScaleOptions;->checkPaperSize(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 197
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->jis_b5_182x257mm:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->iso_a4_210x297mm:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_b5_to_a4:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-direct {p0, v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult$ScaleOptions;->checkPaperSize(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 199
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_5x7_5x7in:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_index_4x6_4x6in:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_2l_to_kg:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-direct {p0, v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult$ScaleOptions;->checkPaperSize(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 201
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_index_4x6_4x6in:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_5x7_5x7in:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_kg_to_2l:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-direct {p0, v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult$ScaleOptions;->checkPaperSize(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 203
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->iso_a5_148x210mm:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->iso_a4_210x297mm:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_a5_to_a4:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-direct {p0, v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult$ScaleOptions;->checkPaperSize(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 205
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->iso_a4_210x297mm:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->iso_a5_148x210mm:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_a4_to_a5:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-direct {p0, v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult$ScaleOptions;->checkPaperSize(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 207
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->iso_a4_210x297mm:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->iso_a3_297x420mm:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_a4_to_a3:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-direct {p0, v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult$ScaleOptions;->checkPaperSize(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 209
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_letter_8_5x11in:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_ledger_11x17in:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_letter_to_11x17:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-direct {p0, v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult$ScaleOptions;->checkPaperSize(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 211
    iget-object v0, p0, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult$ScaleOptions;->scalePresets:Ljava/util/ArrayList;

    return-object v0
.end method

.class public Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;
.super Ljava/lang/Object;
.source "ERemoteOperation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/copy/Component/eremoteoperation/ERemoteOperation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ERemoteOperationResult"
.end annotation


# instance fields
.field json:Lorg/json/JSONObject;

.field request:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;


# direct methods
.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 0

    .line 417
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 418
    iput-object p1, p0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;->json:Lorg/json/JSONObject;

    return-void
.end method

.method static parseResponse(Lepson/common/httpclient/IAHttpClient$HttpResponse;)Lorg/json/JSONObject;
    .locals 3

    .line 403
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 405
    :try_start_0
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 406
    invoke-virtual {p0}, Lepson/common/httpclient/IAHttpClient$HttpResponse;->getEntity()Ljava/io/ByteArrayOutputStream;

    move-result-object p0

    invoke-virtual {p0, v1}, Ljava/io/ByteArrayOutputStream;->writeTo(Ljava/io/OutputStream;)V

    const-string p0, "ERequest C"

    .line 408
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 410
    new-instance p0, Lorg/json/JSONObject;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    .line 412
    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V

    move-object p0, v0

    :goto_0
    return-object p0
.end method


# virtual methods
.method getBooleanValueOf(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Z
    .locals 1

    .line 446
    :try_start_0
    iget-object v0, p0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;->json:Lorg/json/JSONObject;

    iget-object p1, p1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->string:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 448
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method getDefaultValueOf(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 3

    .line 497
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_null:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 499
    :try_start_0
    iget-object v1, p0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;->json:Lorg/json/JSONObject;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object p1, p1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->string:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "-default"

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->stringOf(Ljava/lang/String;)Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 501
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-object v0
.end method

.method getIntValueOf(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)I
    .locals 1

    .line 456
    :try_start_0
    iget-object v0, p0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;->json:Lorg/json/JSONObject;

    iget-object p1, p1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->string:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 458
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method getOptionsValueOf(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ")",
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ">;"
        }
    .end annotation

    .line 507
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 509
    :try_start_0
    iget-object v1, p0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;->json:Lorg/json/JSONObject;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object p1, p1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->string:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "-options"

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v1, ","

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    .line 510
    array-length v1, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, p1, v2

    .line 511
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->stringOf(Ljava/lang/String;)Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v3

    .line 512
    sget-object v4, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_null:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    if-eq v3, v4, :cond_0

    .line 513
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :catch_0
    move-exception p1

    .line 517
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :cond_1
    return-object v0
.end method

.method getParamValueOf(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 2

    .line 487
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_null:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 489
    :try_start_0
    iget-object v1, p0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;->json:Lorg/json/JSONObject;

    iget-object p1, p1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->string:Ljava/lang/String;

    invoke-virtual {v1, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->stringOf(Ljava/lang/String;)Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 491
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-object v0
.end method

.method getParamsValueOf(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ")",
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ">;"
        }
    .end annotation

    .line 523
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 525
    :try_start_0
    invoke-virtual {p0, p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;->getStringValueOf(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Ljava/lang/String;

    move-result-object p1

    const-string v1, ","

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    .line 526
    array-length v1, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, p1, v2

    .line 527
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->stringOf(Ljava/lang/String;)Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :catch_0
    move-exception p1

    .line 530
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :cond_0
    return-object v0
.end method

.method getStringValueOf(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Ljava/lang/String;
    .locals 2

    const-string v0, ""

    .line 466
    :try_start_0
    iget-object v1, p0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;->json:Lorg/json/JSONObject;

    iget-object p1, p1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->string:Ljava/lang/String;

    invoke-virtual {v1, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 468
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-object v0
.end method

.method getStringsValueOf(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ")",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 474
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 476
    :try_start_0
    iget-object v1, p0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;->json:Lorg/json/JSONObject;

    iget-object p1, p1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->string:Ljava/lang/String;

    invoke-virtual {v1, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v1, ","

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    .line 477
    array-length v1, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, p1, v2

    .line 478
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :catch_0
    move-exception p1

    .line 481
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :cond_0
    return-object v0
.end method

.method public isNull(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Z
    .locals 1

    .line 426
    iget-object v0, p0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;->json:Lorg/json/JSONObject;

    iget-object p1, p1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->string:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method public setRemoteRequestBuilder(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;)V
    .locals 0

    .line 430
    iput-object p1, p0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;->request:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;

    return-void
.end method

.method public success()Z
    .locals 2

    .line 436
    :try_start_0
    iget-object v0, p0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;->json:Lorg/json/JSONObject;

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->success:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    iget-object v1, v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->string:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 438
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 422
    iget-object v0, p0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;->json:Lorg/json/JSONObject;

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

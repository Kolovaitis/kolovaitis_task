.class public Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;
.super Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;
.source "ERemoteCopy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ERemoteCopyStatusResult"
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;


# direct methods
.method public constructor <init>(Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;Lorg/json/JSONObject;)V
    .locals 0

    .line 237
    iput-object p1, p0, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->this$0:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;

    .line 238
    invoke-direct {p0, p1, p2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;-><init>(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation;Lorg/json/JSONObject;)V

    return-void
.end method


# virtual methods
.method public document_on_adf()Z
    .locals 1

    .line 254
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->document_on_adf:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p0, v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->getBooleanValueOf(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Z

    move-result v0

    return v0
.end method

.method public job_print_current_pages()I
    .locals 1

    .line 282
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->job_print_current_pages:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p0, v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->getIntValueOf(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)I

    move-result v0

    return v0
.end method

.method public job_print_total_pages()I
    .locals 1

    .line 278
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->job_print_total_pages:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p0, v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->getIntValueOf(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)I

    move-result v0

    return v0
.end method

.method public job_result()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 1

    .line 270
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->job_result:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p0, v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->getParamValueOf(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v0

    return-object v0
.end method

.method public job_state()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 1

    .line 266
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->job_state:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p0, v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->getParamValueOf(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v0

    return-object v0
.end method

.method public job_tokens()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 274
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->job_tokens:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p0, v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->getStringsValueOf(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public print_x_disc_tray_state()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 1

    .line 242
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_x_disc_tray_state:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p0, v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->getParamValueOf(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v0

    return-object v0
.end method

.method public printer_state()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 1

    .line 246
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->printer_state:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p0, v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->getParamValueOf(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v0

    return-object v0
.end method

.method public printer_state_reasons()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ">;"
        }
    .end annotation

    .line 250
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->printer_state_reasons:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p0, v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->getParamsValueOf(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public scanner_state()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 1

    .line 258
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scanner_state:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p0, v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->getParamValueOf(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v0

    return-object v0
.end method

.method public scanner_state_reasons()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ">;"
        }
    .end annotation

    .line 262
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scanner_state_reasons:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p0, v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->getParamsValueOf(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.class public interface abstract Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyParameter;
.super Ljava/lang/Object;
.source "ERemoteCopy.java"

# interfaces
.implements Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$IRemoteOperationParameter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "IRemoteCopyParameter"
.end annotation


# virtual methods
.method public abstract color_effects_type()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
.end method

.method public abstract copies()I
.end method

.method public abstract copy_magnification()I
.end method

.method public abstract layout()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
.end method

.method public abstract orientation()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
.end method

.method public abstract print_media_size()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
.end method

.method public abstract print_media_source()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
.end method

.method public abstract print_media_type()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
.end method

.method public abstract print_quality()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
.end method

.method public abstract print_sheet_collate()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
.end method

.method public abstract print_sides()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
.end method

.method public abstract print_x_auto_pg()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
.end method

.method public abstract print_x_bleed()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
.end method

.method public abstract print_x_dry_time()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
.end method

.method public abstract scan_content_type()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
.end method

.method public abstract scan_media_size()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
.end method

.method public abstract scan_sides()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
.end method

.method public abstract x_density()I
.end method

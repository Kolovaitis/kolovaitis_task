.class public Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$ERemoteCopyPhotoOptionResult;
.super Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;
.source "ERemoteCopyPhoto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ERemoteCopyPhotoOptionResult"
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;


# direct methods
.method public constructor <init>(Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;Lorg/json/JSONObject;)V
    .locals 0

    .line 46
    iput-object p1, p0, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$ERemoteCopyPhotoOptionResult;->this$0:Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;

    .line 47
    invoke-direct {p0, p1, p2}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;-><init>(Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;Lorg/json/JSONObject;)V

    return-void
.end method


# virtual methods
.method public local_default(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 1

    .line 72
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->color_effects_type:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    if-ne p1, v0, :cond_0

    .line 73
    sget-object p1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->color:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    return-object p1

    .line 75
    :cond_0
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_x_bleed:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    if-ne p1, v0, :cond_1

    .line 76
    sget-object p1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->standard:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    return-object p1

    .line 78
    :cond_1
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_color_restoration:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    if-ne p1, v0, :cond_2

    .line 79
    sget-object p1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->off:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    return-object p1

    .line 81
    :cond_2
    sget-object p1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_null:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    return-object p1
.end method

.method public local_options(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ")",
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ">;"
        }
    .end annotation

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 52
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->color_effects_type:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    if-ne p1, v1, :cond_0

    .line 53
    sget-object p1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->color:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 54
    sget-object p1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->monochrome_grayscale:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0

    .line 57
    :cond_0
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_x_bleed:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    if-ne p1, v1, :cond_1

    .line 58
    sget-object p1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->standard:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 59
    sget-object p1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->midium:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 60
    sget-object p1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->minimum:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0

    .line 63
    :cond_1
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_color_restoration:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    if-ne p1, v1, :cond_2

    .line 64
    sget-object p1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->on:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 65
    sget-object p1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->off:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0

    :cond_2
    return-object v0
.end method

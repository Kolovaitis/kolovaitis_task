.class public Lepson/print/copy/Component/eremoteoperation/ERemotePrinter$ERemotePrinterStatusResult;
.super Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;
.source "ERemotePrinter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/copy/Component/eremoteoperation/ERemotePrinter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ERemotePrinterStatusResult"
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/copy/Component/eremoteoperation/ERemotePrinter;


# direct methods
.method public constructor <init>(Lepson/print/copy/Component/eremoteoperation/ERemotePrinter;Lorg/json/JSONObject;)V
    .locals 0

    .line 19
    iput-object p1, p0, Lepson/print/copy/Component/eremoteoperation/ERemotePrinter$ERemotePrinterStatusResult;->this$0:Lepson/print/copy/Component/eremoteoperation/ERemotePrinter;

    .line 20
    invoke-direct {p0, p2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;-><init>(Lorg/json/JSONObject;)V

    return-void
.end method


# virtual methods
.method public print_x_disc_try_state()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 1

    .line 24
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_x_disc_tray_state:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p0, v0}, Lepson/print/copy/Component/eremoteoperation/ERemotePrinter$ERemotePrinterStatusResult;->getParamValueOf(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v0

    return-object v0
.end method

.method public printer_state()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 1

    .line 28
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->printer_state:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p0, v0}, Lepson/print/copy/Component/eremoteoperation/ERemotePrinter$ERemotePrinterStatusResult;->getParamValueOf(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v0

    return-object v0
.end method

.method public printer_state_reasons()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ">;"
        }
    .end annotation

    .line 32
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->printer_state_reasons:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p0, v0}, Lepson/print/copy/Component/eremoteoperation/ERemotePrinter$ERemotePrinterStatusResult;->getParamsValueOf(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

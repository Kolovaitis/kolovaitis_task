.class public Lepson/print/copy/Component/eremoteoperation/ERemotePrinter;
.super Lepson/print/copy/Component/eremoteoperation/ERemoteOperation;
.source "ERemotePrinter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/copy/Component/eremoteoperation/ERemotePrinter$ERemotePrinterStatusResult;,
        Lepson/print/copy/Component/eremoteoperation/ERemotePrinter$ERemotePrinterComponentsResult;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation;-><init>()V

    return-void
.end method


# virtual methods
.method public clearError(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$IRemoteOperationParameter;)Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;
    .locals 3

    .line 52
    iget-object v0, p0, Lepson/print/copy/Component/eremoteoperation/ERemotePrinter;->hostIP:Ljava/lang/String;

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->clear_error:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    invoke-virtual {p0}, Lepson/print/copy/Component/eremoteoperation/ERemotePrinter;->getRequestConnectionTimeout()I

    move-result v2

    invoke-static {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->requestPrinter(Ljava/lang/String;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;I)Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;

    move-result-object v0

    .line 53
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->client_id:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$IRemoteOperationParameter;->client_id()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/lang/String;)V

    .line 54
    new-instance p1, Lepson/print/copy/Component/eremoteoperation/ERemotePrinter$ERemotePrinterStatusResult;

    invoke-virtual {v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->getRemoteRequest()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;

    move-result-object v1

    invoke-virtual {v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;->execute()Lorg/json/JSONObject;

    move-result-object v1

    invoke-direct {p1, p0, v1}, Lepson/print/copy/Component/eremoteoperation/ERemotePrinter$ERemotePrinterStatusResult;-><init>(Lepson/print/copy/Component/eremoteoperation/ERemotePrinter;Lorg/json/JSONObject;)V

    .line 55
    invoke-virtual {p1, v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;->setRemoteRequestBuilder(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;)V

    return-object p1
.end method

.method public getComponents(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$IRemoteOperationParameter;)Lepson/print/copy/Component/eremoteoperation/ERemotePrinter$ERemotePrinterComponentsResult;
    .locals 3

    .line 37
    iget-object v0, p0, Lepson/print/copy/Component/eremoteoperation/ERemotePrinter;->hostIP:Ljava/lang/String;

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->get_components:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    invoke-virtual {p0}, Lepson/print/copy/Component/eremoteoperation/ERemotePrinter;->getRequestConnectionTimeout()I

    move-result v2

    invoke-static {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->requestPrinter(Ljava/lang/String;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;I)Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;

    move-result-object v0

    .line 38
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->client_id:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$IRemoteOperationParameter;->client_id()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/lang/String;)V

    .line 39
    new-instance p1, Lepson/print/copy/Component/eremoteoperation/ERemotePrinter$ERemotePrinterComponentsResult;

    invoke-virtual {v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->getRemoteRequest()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;

    move-result-object v0

    invoke-virtual {v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;->execute()Lorg/json/JSONObject;

    move-result-object v0

    invoke-direct {p1, p0, v0}, Lepson/print/copy/Component/eremoteoperation/ERemotePrinter$ERemotePrinterComponentsResult;-><init>(Lepson/print/copy/Component/eremoteoperation/ERemotePrinter;Lorg/json/JSONObject;)V

    return-object p1
.end method

.method public getStatus(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$IRemoteStatusParameter;)Lepson/print/copy/Component/eremoteoperation/ERemotePrinter$ERemotePrinterStatusResult;
    .locals 3

    .line 43
    iget-object v0, p0, Lepson/print/copy/Component/eremoteoperation/ERemotePrinter;->hostIP:Ljava/lang/String;

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->get_status:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    invoke-virtual {p0}, Lepson/print/copy/Component/eremoteoperation/ERemotePrinter;->getRequestConnectionTimeout()I

    move-result v2

    invoke-static {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->requestPrinter(Ljava/lang/String;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;I)Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;

    move-result-object v0

    .line 44
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->client_id:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$IRemoteStatusParameter;->client_id()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/lang/String;)V

    .line 45
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->keys:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$IRemoteStatusParameter;->keys()Ljava/util/ArrayList;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/util/ArrayList;)V

    .line 46
    new-instance p1, Lepson/print/copy/Component/eremoteoperation/ERemotePrinter$ERemotePrinterStatusResult;

    invoke-virtual {v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->getRemoteRequest()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;

    move-result-object v1

    invoke-virtual {v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;->execute()Lorg/json/JSONObject;

    move-result-object v1

    invoke-direct {p1, p0, v1}, Lepson/print/copy/Component/eremoteoperation/ERemotePrinter$ERemotePrinterStatusResult;-><init>(Lepson/print/copy/Component/eremoteoperation/ERemotePrinter;Lorg/json/JSONObject;)V

    .line 47
    invoke-virtual {p1, v0}, Lepson/print/copy/Component/eremoteoperation/ERemotePrinter$ERemotePrinterStatusResult;->setRemoteRequestBuilder(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;)V

    return-object p1
.end method

.class public interface abstract Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$IRemoteCopyPhotoParameter;
.super Ljava/lang/Object;
.source "ERemoteCopyPhoto.java"

# interfaces
.implements Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$IRemoteOperationParameter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "IRemoteCopyPhotoParameter"
.end annotation


# virtual methods
.method public abstract color_effects_type()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
.end method

.method public abstract copies()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract layout()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
.end method

.method public abstract print_media_size()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
.end method

.method public abstract print_media_source()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
.end method

.method public abstract print_media_type()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
.end method

.method public abstract print_quality()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
.end method

.method public abstract print_x_auto_pg()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
.end method

.method public abstract print_x_bleed()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
.end method

.method public abstract scan_area_height()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract scan_area_resolution()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract scan_area_width()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract scan_area_x()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract scan_area_y()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract scan_count()I
.end method

.method public abstract x_apf()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
.end method

.method public abstract x_color_restoration()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
.end method

.method public abstract x_fit_gamma()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract x_fit_matrix()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

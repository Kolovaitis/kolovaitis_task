.class public interface abstract Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$IRemoteCopyPhotoOptionsParameter;
.super Ljava/lang/Object;
.source "ERemoteCopyPhoto.java"

# interfaces
.implements Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$IRemoteOperationParameter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "IRemoteCopyPhotoOptionsParameter"
.end annotation


# virtual methods
.method public abstract default_as_fixed()Z
.end method

.method public abstract fixed_parameters()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ">;"
        }
    .end annotation
.end method

.method public abstract layout()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
.end method

.method public abstract preferred_parameters()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ">;"
        }
    .end annotation
.end method

.method public abstract print_media_size()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
.end method

.method public abstract print_media_source()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
.end method

.method public abstract print_media_type()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
.end method

.method public abstract print_quality()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
.end method

.method public abstract priority_order()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ">;"
        }
    .end annotation
.end method

.method public abstract x_apf()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
.end method

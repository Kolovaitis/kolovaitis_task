.class final enum Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;
.super Ljava/lang/Enum;
.source "ERemoteOperation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/copy/Component/eremoteoperation/ERemoteOperation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "RequestParam"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

.field public static final enum Content_Type:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

.field public static final enum HTTP:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

.field public static final enum User_Agent:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

.field public static final enum cancel:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

.field public static final enum clear_error:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

.field public static final enum combo:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

.field public static final enum copy:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

.field public static final enum copy_photo:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

.field public static final enum device:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

.field public static final enum document_changed:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

.field public static final enum get_components:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

.field public static final enum get_functions:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

.field public static final enum get_options:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

.field public static final enum get_status:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

.field public static final enum printer:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

.field public static final enum remote_operation:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

.field public static final enum scanner:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

.field public static final enum v1:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;


# instance fields
.field public string:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 362
    new-instance v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    const-string v1, "remote_operation"

    const-string v2, "remote_operation"

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->remote_operation:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    .line 363
    new-instance v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    const-string v1, "v1"

    const-string v2, "v1"

    const/4 v4, 0x1

    invoke-direct {v0, v1, v4, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->v1:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    .line 364
    new-instance v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    const-string v1, "HTTP"

    const-string v2, "http"

    const/4 v5, 0x2

    invoke-direct {v0, v1, v5, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->HTTP:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    .line 365
    new-instance v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    const-string v1, "Content_Type"

    const-string v2, "application/x-www-form-urlencoded"

    const/4 v6, 0x3

    invoke-direct {v0, v1, v6, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->Content_Type:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    .line 366
    new-instance v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    const-string v1, "User_Agent"

    const-string v2, "Mozilla/5.0"

    const/4 v7, 0x4

    invoke-direct {v0, v1, v7, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->User_Agent:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    .line 368
    new-instance v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    const-string v1, "device"

    const-string v2, "device"

    const/4 v8, 0x5

    invoke-direct {v0, v1, v8, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->device:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    .line 369
    new-instance v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    const-string v1, "printer"

    const-string v2, "printer"

    const/4 v9, 0x6

    invoke-direct {v0, v1, v9, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->printer:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    .line 370
    new-instance v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    const-string v1, "scanner"

    const-string v2, "scanner"

    const/4 v10, 0x7

    invoke-direct {v0, v1, v10, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->scanner:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    .line 371
    new-instance v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    const-string v1, "copy"

    const-string v2, "copy"

    const/16 v11, 0x8

    invoke-direct {v0, v1, v11, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->copy:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    .line 372
    new-instance v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    const-string v1, "copy_photo"

    const-string v2, "copy_photo"

    const/16 v12, 0x9

    invoke-direct {v0, v1, v12, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->copy_photo:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    .line 373
    new-instance v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    const-string v1, "combo"

    const-string v2, "combo"

    const/16 v13, 0xa

    invoke-direct {v0, v1, v13, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->combo:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    .line 375
    new-instance v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    const-string v1, "get_functions"

    const-string v2, "get_functions"

    const/16 v14, 0xb

    invoke-direct {v0, v1, v14, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->get_functions:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    .line 376
    new-instance v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    const-string v1, "get_status"

    const-string v2, "get_status"

    const/16 v15, 0xc

    invoke-direct {v0, v1, v15, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->get_status:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    .line 377
    new-instance v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    const-string v1, "get_components"

    const-string v2, "get_components"

    const/16 v15, 0xd

    invoke-direct {v0, v1, v15, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->get_components:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    .line 378
    new-instance v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    const-string v1, "get_options"

    const-string v2, "get_options"

    const/16 v15, 0xe

    invoke-direct {v0, v1, v15, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->get_options:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    .line 379
    new-instance v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    const-string v1, "clear_error"

    const-string v2, "clear_error"

    const/16 v15, 0xf

    invoke-direct {v0, v1, v15, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->clear_error:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    .line 380
    new-instance v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    const-string v1, "cancel"

    const-string v2, "cancel"

    const/16 v15, 0x10

    invoke-direct {v0, v1, v15, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->cancel:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    .line 381
    new-instance v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    const-string v1, "document_changed"

    const-string v2, "document_changed"

    const/16 v15, 0x11

    invoke-direct {v0, v1, v15, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->document_changed:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    const/16 v0, 0x12

    .line 361
    new-array v0, v0, [Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->remote_operation:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    aput-object v1, v0, v3

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->v1:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    aput-object v1, v0, v4

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->HTTP:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    aput-object v1, v0, v5

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->Content_Type:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    aput-object v1, v0, v6

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->User_Agent:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    aput-object v1, v0, v7

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->device:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    aput-object v1, v0, v8

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->printer:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    aput-object v1, v0, v9

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->scanner:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    aput-object v1, v0, v10

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->copy:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    aput-object v1, v0, v11

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->copy_photo:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    aput-object v1, v0, v12

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->combo:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    aput-object v1, v0, v13

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->get_functions:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    aput-object v1, v0, v14

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->get_status:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->get_components:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->get_options:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->clear_error:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->cancel:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->document_changed:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sput-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->$VALUES:[Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 385
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 386
    iput-object p3, p0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->string:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;
    .locals 1

    .line 361
    const-class v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    return-object p0
.end method

.method public static values()[Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;
    .locals 1

    .line 361
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->$VALUES:[Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    invoke-virtual {v0}, [Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    return-object v0
.end method

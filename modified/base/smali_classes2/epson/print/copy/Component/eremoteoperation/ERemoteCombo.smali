.class public Lepson/print/copy/Component/eremoteoperation/ERemoteCombo;
.super Lepson/print/copy/Component/eremoteoperation/ERemoteOperation;
.source "ERemoteCombo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$ERemoteComboStatusResult;,
        Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$ERemoteComboWaitTemplateResult;,
        Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$ERemoteComboPrintTemplateResult;,
        Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$ERemoteComboOptionsResult;,
        Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$IRemoteComboStatusParameter;,
        Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$IRemoteComboWaitTemplateParameter;,
        Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$IRemoteComboPrintTemplateParameter;,
        Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$IRemoteComboOptionsParameter;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation;-><init>()V

    return-void
.end method


# virtual methods
.method public getOptions(Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$IRemoteComboOptionsParameter;)Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$ERemoteComboOptionsResult;
    .locals 3

    .line 117
    iget-object v0, p0, Lepson/print/copy/Component/eremoteoperation/ERemoteCombo;->hostIP:Ljava/lang/String;

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->get_options:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    invoke-virtual {p0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCombo;->getRequestConnectionTimeout()I

    move-result v2

    invoke-static {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->requestCombo(Ljava/lang/String;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;I)Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;

    move-result-object v0

    .line 118
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->client_id:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$IRemoteComboOptionsParameter;->client_id()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/lang/String;)V

    .line 119
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->layout:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$IRemoteComboOptionsParameter;->layout()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 120
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_type:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$IRemoteComboOptionsParameter;->print_media_type()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 121
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_size:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$IRemoteComboOptionsParameter;->print_media_size()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 122
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->frame:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$IRemoteComboOptionsParameter;->frame()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 123
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->fixed_parameters:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$IRemoteComboOptionsParameter;->fixed_parameters()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/util/ArrayList;)V

    .line 124
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->priority_order:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$IRemoteComboOptionsParameter;->priority_order()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/util/ArrayList;)V

    .line 125
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->default_as_fixed:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$IRemoteComboOptionsParameter;->default_as_fixed()Z

    move-result p1

    invoke-virtual {v0, v1, p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Z)V

    .line 126
    new-instance p1, Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$ERemoteComboOptionsResult;

    invoke-virtual {v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->getRemoteRequest()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;

    move-result-object v1

    invoke-virtual {v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;->execute()Lorg/json/JSONObject;

    move-result-object v1

    invoke-direct {p1, p0, v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$ERemoteComboOptionsResult;-><init>(Lepson/print/copy/Component/eremoteoperation/ERemoteCombo;Lorg/json/JSONObject;)V

    .line 127
    invoke-virtual {p1, v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$ERemoteComboOptionsResult;->setRemoteRequestBuilder(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;)V

    return-object p1
.end method

.method public getSelectableOptions()Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$ERemoteComboOptionsResult;
    .locals 4

    .line 132
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 133
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->layout:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 134
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_size:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 135
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_type:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 136
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->frame:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 138
    iget-object v1, p0, Lepson/print/copy/Component/eremoteoperation/ERemoteCombo;->hostIP:Ljava/lang/String;

    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->get_options:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    invoke-virtual {p0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCombo;->getRequestConnectionTimeout()I

    move-result v3

    invoke-static {v1, v2, v3}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->requestCombo(Ljava/lang/String;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;I)Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;

    move-result-object v1

    .line 139
    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->client_id:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_null:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    iget-object v3, v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->string:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/lang/String;)V

    .line 140
    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->layout:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_null:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v1, v2, v3}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 141
    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_type:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_null:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v1, v2, v3}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 142
    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_size:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_null:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v1, v2, v3}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 143
    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->frame:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_null:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v1, v2, v3}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 144
    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->fixed_parameters:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v1, v2, v3}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/util/ArrayList;)V

    .line 145
    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->priority_order:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v1, v2, v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/util/ArrayList;)V

    .line 146
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->default_as_fixed:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Z)V

    .line 147
    new-instance v0, Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$ERemoteComboOptionsResult;

    invoke-virtual {v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->getRemoteRequest()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;

    move-result-object v2

    invoke-virtual {v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;->execute()Lorg/json/JSONObject;

    move-result-object v2

    invoke-direct {v0, p0, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$ERemoteComboOptionsResult;-><init>(Lepson/print/copy/Component/eremoteoperation/ERemoteCombo;Lorg/json/JSONObject;)V

    .line 148
    invoke-virtual {v0, v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$ERemoteComboOptionsResult;->setRemoteRequestBuilder(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;)V

    return-object v0
.end method

.method public getStatus(Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$IRemoteComboStatusParameter;)Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$ERemoteComboStatusResult;
    .locals 3

    .line 173
    iget-object v0, p0, Lepson/print/copy/Component/eremoteoperation/ERemoteCombo;->hostIP:Ljava/lang/String;

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->get_options:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    invoke-virtual {p0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCombo;->getRequestConnectionTimeout()I

    move-result v2

    invoke-static {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->requestCombo(Ljava/lang/String;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;I)Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;

    move-result-object v0

    .line 174
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->client_id:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$IRemoteComboStatusParameter;->client_id()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/lang/String;)V

    .line 175
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->keys:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$IRemoteComboStatusParameter;->keys()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/util/ArrayList;)V

    .line 176
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->job_token:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$IRemoteComboStatusParameter;->job_token()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 177
    new-instance p1, Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$ERemoteComboStatusResult;

    invoke-virtual {v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->getRemoteRequest()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;

    move-result-object v1

    invoke-virtual {v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;->execute()Lorg/json/JSONObject;

    move-result-object v1

    invoke-direct {p1, p0, v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$ERemoteComboStatusResult;-><init>(Lepson/print/copy/Component/eremoteoperation/ERemoteCombo;Lorg/json/JSONObject;)V

    .line 178
    invoke-virtual {p1, v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$ERemoteComboStatusResult;->setRemoteRequestBuilder(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;)V

    return-object p1
.end method

.method public printTemplate(Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$IRemoteComboPrintTemplateParameter;)Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$ERemoteComboPrintTemplateResult;
    .locals 3

    .line 153
    iget-object v0, p0, Lepson/print/copy/Component/eremoteoperation/ERemoteCombo;->hostIP:Ljava/lang/String;

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->get_options:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    invoke-virtual {p0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCombo;->getRequestConnectionTimeout()I

    move-result v2

    invoke-static {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->requestCombo(Ljava/lang/String;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;I)Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;

    move-result-object v0

    .line 154
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->client_id:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$IRemoteComboPrintTemplateParameter;->client_id()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/lang/String;)V

    .line 155
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->layout:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$IRemoteComboPrintTemplateParameter;->layout()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 156
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_type:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$IRemoteComboPrintTemplateParameter;->print_media_type()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 157
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_size:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$IRemoteComboPrintTemplateParameter;->print_media_size()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 158
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->frame:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$IRemoteComboPrintTemplateParameter;->frame()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 159
    new-instance p1, Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$ERemoteComboPrintTemplateResult;

    invoke-virtual {v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->getRemoteRequest()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;

    move-result-object v1

    invoke-virtual {v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;->execute()Lorg/json/JSONObject;

    move-result-object v1

    invoke-direct {p1, p0, v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$ERemoteComboPrintTemplateResult;-><init>(Lepson/print/copy/Component/eremoteoperation/ERemoteCombo;Lorg/json/JSONObject;)V

    .line 160
    invoke-virtual {p1, v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$ERemoteComboPrintTemplateResult;->setRemoteRequestBuilder(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;)V

    return-object p1
.end method

.method public waitTemplate(Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$IRemoteComboWaitTemplateParameter;)Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$ERemoteComboWaitTemplateResult;
    .locals 3

    .line 165
    iget-object v0, p0, Lepson/print/copy/Component/eremoteoperation/ERemoteCombo;->hostIP:Ljava/lang/String;

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->get_options:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    invoke-virtual {p0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCombo;->getRequestConnectionTimeout()I

    move-result v2

    invoke-static {v0, v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->requestCombo(Ljava/lang/String;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;I)Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;

    move-result-object v0

    .line 166
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->client_id:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$IRemoteComboWaitTemplateParameter;->client_id()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->add(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Ljava/lang/String;)V

    .line 167
    new-instance p1, Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$ERemoteComboWaitTemplateResult;

    invoke-virtual {v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->getRemoteRequest()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;

    move-result-object v1

    invoke-virtual {v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;->execute()Lorg/json/JSONObject;

    move-result-object v1

    invoke-direct {p1, p0, v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$ERemoteComboWaitTemplateResult;-><init>(Lepson/print/copy/Component/eremoteoperation/ERemoteCombo;Lorg/json/JSONObject;)V

    .line 168
    invoke-virtual {p1, v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$ERemoteComboWaitTemplateResult;->setRemoteRequestBuilder(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;)V

    return-object p1
.end method

.class public Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$ERemoteComboOptionsResult;
.super Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;
.source "ERemoteCombo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/copy/Component/eremoteoperation/ERemoteCombo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ERemoteComboOptionsResult"
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/copy/Component/eremoteoperation/ERemoteCombo;


# direct methods
.method public constructor <init>(Lepson/print/copy/Component/eremoteoperation/ERemoteCombo;Lorg/json/JSONObject;)V
    .locals 0

    .line 36
    iput-object p1, p0, Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$ERemoteComboOptionsResult;->this$0:Lepson/print/copy/Component/eremoteoperation/ERemoteCombo;

    .line 37
    invoke-direct {p0, p1, p2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;-><init>(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation;Lorg/json/JSONObject;)V

    return-void
.end method


# virtual methods
.method public parameter_default(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 0

    .line 45
    invoke-virtual {p0, p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$ERemoteComboOptionsResult;->getDefaultValueOf(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object p1

    return-object p1
.end method

.method public parameter_options(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Ljava/util/ArrayList;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ")",
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ">;"
        }
    .end annotation

    .line 41
    invoke-virtual {p0, p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCombo$ERemoteComboOptionsResult;->getOptionsValueOf(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Ljava/util/ArrayList;

    move-result-object p1

    return-object p1
.end method

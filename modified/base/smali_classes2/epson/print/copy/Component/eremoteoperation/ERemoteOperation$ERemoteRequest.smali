.class Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;
.super Ljava/lang/Object;
.source "ERemoteOperation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/copy/Component/eremoteoperation/ERemoteOperation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ERemoteRequest"
.end annotation


# instance fields
.field request:Lepson/common/httpclient/IAHttpClient$HttpPost;

.field final synthetic this$0:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation;

.field timeout:I


# direct methods
.method public constructor <init>(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation;Ljava/lang/String;Ljava/util/ArrayList;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Lepson/common/httpclient/BasicNameValuePair;",
            ">;I)V"
        }
    .end annotation

    .line 550
    iput-object p1, p0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;->this$0:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 552
    :try_start_0
    new-instance p1, Lepson/common/httpclient/IAHttpClient$HttpPost;

    invoke-direct {p1, p2}, Lepson/common/httpclient/IAHttpClient$HttpPost;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;->request:Lepson/common/httpclient/IAHttpClient$HttpPost;

    .line 553
    iget-object p1, p0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;->request:Lepson/common/httpclient/IAHttpClient$HttpPost;

    const-string p2, "UTF-8"

    invoke-virtual {p1, p2}, Lepson/common/httpclient/IAHttpClient$HttpPost;->setContentEncoding(Ljava/lang/String;)V

    .line 554
    iget-object p1, p0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;->request:Lepson/common/httpclient/IAHttpClient$HttpPost;

    sget-object p2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->Content_Type:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;

    iget-object p2, p2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$RequestParam;->string:Ljava/lang/String;

    invoke-virtual {p1, p2}, Lepson/common/httpclient/IAHttpClient$HttpPost;->setContentType(Ljava/lang/String;)V

    .line 556
    iget-object p1, p0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;->request:Lepson/common/httpclient/IAHttpClient$HttpPost;

    const-string p2, "UTF-8"

    invoke-static {p3, p2}, Lepson/common/httpclient/IAHttpClient$HttpPost;->getUrlEncodedFormEntity(Ljava/util/List;Ljava/lang/String;)[B

    move-result-object p2

    invoke-virtual {p1, p2}, Lepson/common/httpclient/IAHttpClient$HttpPost;->setEntity([B)V

    .line 558
    iput p4, p0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;->timeout:I
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 561
    invoke-virtual {p1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    :goto_0
    return-void
.end method


# virtual methods
.method public execute()Lorg/json/JSONObject;
    .locals 2

    :try_start_0
    const-string v0, "ERequest A"

    .line 567
    iget-object v1, p0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;->request:Lepson/common/httpclient/IAHttpClient$HttpPost;

    invoke-virtual {v1}, Lepson/common/httpclient/IAHttpClient$HttpPost;->getURI()Ljava/net/URL;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 569
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 570
    iget-object v1, p0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;->request:Lepson/common/httpclient/IAHttpClient$HttpPost;

    invoke-virtual {v1}, Lepson/common/httpclient/IAHttpClient$HttpPost;->getEntity()Ljava/io/ByteArrayOutputStream;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/io/ByteArrayOutputStream;->writeTo(Ljava/io/OutputStream;)V

    .line 572
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ERequest B"

    .line 573
    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 575
    new-instance v0, Lepson/common/httpclient/IAHttpClient;

    invoke-direct {v0}, Lepson/common/httpclient/IAHttpClient;-><init>()V

    .line 576
    iget v1, p0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;->timeout:I

    invoke-virtual {v0, v1}, Lepson/common/httpclient/IAHttpClient;->setConnectionTimeout(I)V

    .line 577
    iget v1, p0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;->timeout:I

    invoke-virtual {v0, v1}, Lepson/common/httpclient/IAHttpClient;->setSoTimeout(I)V

    .line 578
    iget-object v1, p0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequest;->request:Lepson/common/httpclient/IAHttpClient$HttpPost;

    invoke-virtual {v0, v1}, Lepson/common/httpclient/IAHttpClient;->execute(Lepson/common/httpclient/IAHttpClient$HttpPost;)Lepson/common/httpclient/IAHttpClient$HttpResponse;

    move-result-object v0

    invoke-static {v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;->parseResponse(Lepson/common/httpclient/IAHttpClient$HttpResponse;)Lorg/json/JSONObject;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    .line 580
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 585
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    return-object v0
.end method

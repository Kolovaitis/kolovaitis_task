.class public interface abstract Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyOptionsParameter;
.super Ljava/lang/Object;
.source "ERemoteCopy.java"

# interfaces
.implements Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$IRemoteOperationParameter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "IRemoteCopyOptionsParameter"
.end annotation


# virtual methods
.method public abstract default_as_fixed()Z
.end method

.method public abstract fixed_parameters()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ">;"
        }
    .end annotation
.end method

.method public abstract layout()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
.end method

.method public abstract preferred_parameters()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ">;"
        }
    .end annotation
.end method

.method public abstract print_media_size()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
.end method

.method public abstract print_media_source()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
.end method

.method public abstract print_media_type()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
.end method

.method public abstract print_quality()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
.end method

.method public abstract print_sheet_collate()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
.end method

.method public abstract print_sides()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
.end method

.method public abstract priority_order()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ">;"
        }
    .end annotation
.end method

.method public abstract scan_content_type()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
.end method

.method public abstract scan_media_size()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
.end method

.method public abstract scan_sides()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
.end method

.class public Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;
.super Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;
.source "ERemoteCopy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ERemoteCopyOptionsResult"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult$ScaleOptions;
    }
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;


# direct methods
.method public constructor <init>(Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;Lorg/json/JSONObject;)V
    .locals 0

    .line 69
    iput-object p1, p0, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;->this$0:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;

    .line 70
    invoke-direct {p0, p1, p2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;-><init>(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation;Lorg/json/JSONObject;)V

    return-void
.end method


# virtual methods
.method public local_default(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 1

    .line 131
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->color_effects_type:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    if-ne p1, v0, :cond_0

    .line 132
    sget-object p1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->color:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    return-object p1

    .line 134
    :cond_0
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_x_bleed:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    if-ne p1, v0, :cond_1

    .line 135
    sget-object p1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->standard:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    return-object p1

    .line 137
    :cond_1
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    if-ne p1, v0, :cond_2

    .line 138
    sget-object p1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_autofit:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    return-object p1

    .line 140
    :cond_2
    sget-object p1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_null:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    return-object p1
.end method

.method public local_options(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ")",
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ">;"
        }
    .end annotation

    .line 82
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 83
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->color_effects_type:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    if-ne p1, v1, :cond_0

    .line 84
    sget-object p1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->color:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 85
    sget-object p1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->monochrome_grayscale:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0

    .line 88
    :cond_0
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_x_bleed:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    if-ne p1, v1, :cond_1

    .line 89
    iget-object p1, p0, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;->request:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;

    iget-object p1, p1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteRequestBuilder;->remoteParam:Ljava/util/HashMap;

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_size:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p1, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 90
    sget-object p1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->standard:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 91
    sget-object p1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->midium:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 92
    sget-object p1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->minimum:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0

    :cond_1
    return-object v0
.end method

.method public parameter_default(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 0

    .line 78
    invoke-virtual {p0, p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;->getDefaultValueOf(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object p1

    return-object p1
.end method

.method public parameter_options(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Ljava/util/ArrayList;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ")",
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ">;"
        }
    .end annotation

    .line 74
    invoke-virtual {p0, p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;->getOptionsValueOf(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Ljava/util/ArrayList;

    move-result-object p1

    return-object p1
.end method

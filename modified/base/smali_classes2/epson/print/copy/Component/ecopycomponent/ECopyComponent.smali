.class public final Lepson/print/copy/Component/ecopycomponent/ECopyComponent;
.super Ljava/lang/Object;
.source "ECopyComponent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ITask;,
        Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener;,
        Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;,
        Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener;,
        Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;,
        Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyInvalidateResumeRequest;,
        Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest;,
        Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyCancelRequest;,
        Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;,
        Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyComponentHolder;,
        Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;,
        Lepson/print/copy/Component/ecopycomponent/ECopyComponent$Property;
    }
.end annotation


# instance fields
.field clientID:Ljava/lang/String;

.field optionChangedListener:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;

.field optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

.field properties:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Lepson/print/copy/Component/ecopycomponent/ECopyComponent$Property;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field systemSettings:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;


# direct methods
.method constructor <init>()V
    .locals 3

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    invoke-direct {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;-><init>()V

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    .line 31
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->properties:Ljava/util/HashMap;

    .line 32
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->properties:Ljava/util/HashMap;

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$Property;->RequestConnectionTimeout:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$Property;

    const/16 v2, 0x2710

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$1;

    invoke-direct {v0, p0}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$1;-><init>(Lepson/print/copy/Component/ecopycomponent/ECopyComponent;)V

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->systemSettings:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;

    return-void
.end method

.method public static sharedComponent()Lepson/print/copy/Component/ecopycomponent/ECopyComponent;
    .locals 1

    .line 114
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyComponentHolder;->instance:Lepson/print/copy/Component/ecopycomponent/ECopyComponent;

    return-object v0
.end method


# virtual methods
.method public bindCopyOptionContext(Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;)V
    .locals 1

    .line 642
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    invoke-direct {v0, p1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;-><init>(Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;)V

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    .line 643
    iput-object p2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->optionChangedListener:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;

    return-void
.end method

.method public createCopyOptionContext(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener;)V
    .locals 1

    .line 699
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;->Photo:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    if-ne p1, v0, :cond_0

    .line 700
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoGetOptionTask;

    invoke-direct {v0, p1, p2}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoGetOptionTask;-><init>(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener;)V

    goto :goto_0

    .line 702
    :cond_0
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask;

    invoke-direct {v0, p1, p2}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask;-><init>(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener;)V

    .line 704
    :goto_0
    invoke-virtual {p0, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->execute(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ITask;)Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyCancelRequest;

    return-void
.end method

.method protected execute(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ITask;)Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyCancelRequest;
    .locals 2

    .line 790
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->properties:Ljava/util/HashMap;

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$Property;->RequestConnectionTimeout:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$Property;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 791
    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->systemSettings:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;

    invoke-interface {p1, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ITask;->setSystemSettings(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;)V

    .line 792
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {p1, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ITask;->setRequestConnectionTimeout(I)V

    .line 793
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->clientID:Ljava/lang/String;

    invoke-interface {p1, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ITask;->setClientID(Ljava/lang/String;)V

    .line 794
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    invoke-interface {p1, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ITask;->setOptionContext(Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;)V

    .line 795
    invoke-interface {p1}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ITask;->start()Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyCancelRequest;

    move-result-object p1

    return-object p1
.end method

.method public getBindedCopyOptionContext()Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;
    .locals 1

    .line 651
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->isNull()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    :goto_0
    return-object v0
.end method

.method public getComposedCopyPreview(Landroid/content/Context;IILepson/print/copy/Component/ecopycomponent/ECopyPreview$ECopyPreviewInvalidateListener;)Lepson/print/copy/Component/ecopycomponent/ECopyPreview;
    .locals 0

    .line 663
    invoke-static {p1, p2, p3, p4}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->createPreview(Landroid/content/Context;IILepson/print/copy/Component/ecopycomponent/ECopyPreview$ECopyPreviewInvalidateListener;)Lepson/print/copy/Component/ecopycomponent/ECopyPreview;

    move-result-object p1

    .line 664
    new-instance p2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    iget-object p3, p0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    invoke-direct {p2, p3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;-><init>(Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;)V

    new-instance p3, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$2;

    invoke-direct {p3, p0}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$2;-><init>(Lepson/print/copy/Component/ecopycomponent/ECopyComponent;)V

    invoke-virtual {p1, p2, p3}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->compose(Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;Lepson/print/copy/Component/ecopycomponent/ECopyPreview$ICopyPreviewCommitEditedOptionContext;)V

    return-object p1
.end method

.method public getCopyOptionItems()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;",
            ">;"
        }
    .end annotation

    .line 685
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 686
    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    iget-object v1, v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->optionItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    .line 687
    new-instance v3, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    invoke-direct {v3, v2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;-><init>(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public getCopyValue(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;)I
    .locals 0

    .line 778
    invoke-static {p1, p2}, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->getcopyMagnification(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;)I

    move-result p1

    return p1
.end method

.method public getDeviceStatus(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener;)V
    .locals 1

    .line 768
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask;

    invoke-direct {v0, p1}, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask;-><init>(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener;)V

    invoke-virtual {p0, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->execute(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ITask;)Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyCancelRequest;

    return-void
.end method

.method public getRemoteOperationUUID(Landroid/content/Context;)V
    .locals 0

    .line 122
    invoke-static {p1}, Lepson/common/Utils;->getRemoteOperationUUID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->clientID:Ljava/lang/String;

    return-void
.end method

.method public recoverCopy(Ljava/lang/String;Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;)Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyCancelRequest;
    .locals 3

    .line 760
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$CopyMode;->Recover:Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$CopyMode;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, p2, v2}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;-><init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$CopyMode;Ljava/lang/String;Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->execute(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ITask;)Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyCancelRequest;

    move-result-object p1

    return-object p1
.end method

.method public setCopyOptionItem(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V
    .locals 2

    .line 715
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->getCopyType()Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    move-result-object v0

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;->Photo:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    if-ne v0, v1, :cond_0

    .line 716
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;

    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->optionChangedListener:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;

    invoke-direct {v0, p1, v1}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;-><init>(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;)V

    goto :goto_0

    .line 718
    :cond_0
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;

    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->optionChangedListener:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;

    invoke-direct {v0, p1, v1}, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;-><init>(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;)V

    .line 720
    :goto_0
    invoke-virtual {p0, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->execute(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ITask;)Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyCancelRequest;

    return-void
.end method

.method public setProperty(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$Property;I)V
    .locals 2

    .line 63
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$3;->$SwitchMap$epson$print$copy$Component$ecopycomponent$ECopyComponent$Property:[I

    invoke-virtual {p1}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$Property;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 65
    :cond_0
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->properties:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-void
.end method

.method public setSystemSettings(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;)V
    .locals 0

    .line 102
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->systemSettings:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;

    return-void
.end method

.method public startCopy(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;Landroid/content/Context;)Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyCancelRequest;
    .locals 2

    .line 740
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$CopyMode;->Copy:Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$CopyMode;

    invoke-direct {v0, v1, p1, p2}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;-><init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$CopyMode;Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->execute(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ITask;)Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyCancelRequest;

    move-result-object p1

    return-object p1
.end method

.method public startCopyPhoto(Ljava/lang/String;Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;)Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyCancelRequest;
    .locals 1

    .line 750
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;

    invoke-direct {v0, p1, p2}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;-><init>(Ljava/lang/String;Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;)V

    invoke-virtual {p0, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->execute(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ITask;)Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyCancelRequest;

    move-result-object p1

    return-object p1
.end method

.method public startPreview(Lepson/print/copy/Component/ecopycomponent/ECopyPreview;Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;)Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyCancelRequest;
    .locals 1

    .line 730
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;

    invoke-direct {v0, p1, p2}, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;-><init>(Lepson/print/copy/Component/ecopycomponent/ECopyPreview;Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;)V

    invoke-virtual {p0, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->execute(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ITask;)Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyCancelRequest;

    move-result-object p1

    return-object p1
.end method

.class Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;
.super Landroid/os/AsyncTask;
.source "RemoteCopyPhotoTask.java"

# interfaces
.implements Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ITask;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Result;,
        Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Progress;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Progress;",
        "Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Result;",
        ">;",
        "Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ITask;"
    }
.end annotation


# instance fields
.field cancelParameter:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$IRemoteCancelParameter;

.field cancelRequested:Z

.field clientID:Ljava/lang/String;

.field jobToken:Ljava/lang/String;

.field operation:Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;

.field optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

.field photoSetting:Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;

.field progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Progress;

.field statusListener:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;

.field statusParameter:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyStatusParameter;

.field systemSettings:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;)V
    .locals 1

    .line 164
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 165
    new-instance v0, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;

    invoke-direct {v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;-><init>()V

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->operation:Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;

    .line 166
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Progress;

    invoke-direct {v0, p0}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Progress;-><init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;)V

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Progress;

    .line 167
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;

    invoke-direct {v0, p1}, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->photoSetting:Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;

    .line 168
    iput-object p2, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->statusListener:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;

    return-void
.end method

.method static synthetic access$000(Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;[Ljava/lang/Object;)V
    .locals 0

    .line 113
    invoke-virtual {p0, p1}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->publishProgress([Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Result;
    .locals 9

    .line 443
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->operation:Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;

    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->systemSettings:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;

    invoke-interface {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;->getPrinterIPAddress()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;->setHostIP(Ljava/lang/String;)V

    .line 450
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->photoSetting:Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;

    invoke-virtual {p1}, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;->parse()Z

    move-result p1

    if-nez p1, :cond_0

    .line 451
    new-instance p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Result;

    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_invalid_photo_setting:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-direct {p1, p0, v0}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Result;-><init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    return-object p1

    .line 454
    :cond_0
    invoke-virtual {p0}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->startCopy()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;

    move-result-object p1

    .line 455
    invoke-virtual {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;->success()Z

    move-result v0

    if-nez v0, :cond_1

    .line 456
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Result;

    invoke-direct {v0, p0, p1}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Result;-><init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;)V

    return-object v0

    :cond_1
    const/4 p1, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x1

    move-object v3, p1

    const/4 v2, 0x0

    const/4 v4, 0x0

    :goto_0
    if-nez v2, :cond_8

    .line 460
    iget-boolean v3, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->cancelRequested:Z

    if-eqz v3, :cond_3

    if-nez v4, :cond_3

    .line 461
    iget-object v3, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->operation:Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;

    iget-object v4, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->cancelParameter:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$IRemoteCancelParameter;

    invoke-virtual {v3, v4}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;->cancel(Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$IRemoteCancelParameter;)Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;

    move-result-object v3

    .line 465
    invoke-virtual {v3}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;->success()Z

    move-result v3

    if-nez v3, :cond_2

    .line 466
    new-instance p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Result;

    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->canceled:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-direct {p1, p0, v0}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Result;-><init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    return-object p1

    :cond_2
    const/4 v4, 0x1

    .line 471
    :cond_3
    iget-object v3, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->operation:Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;

    iget-object v5, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->statusParameter:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyStatusParameter;

    invoke-virtual {v3, v5}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;->getStatus(Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyStatusParameter;)Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;

    move-result-object v3

    .line 472
    invoke-virtual {v3}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->success()Z

    move-result v5

    if-nez v5, :cond_4

    .line 473
    new-instance p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Result;

    invoke-direct {p1, p0, v3}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Result;-><init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;)V

    return-object p1

    .line 476
    :cond_4
    new-instance v5, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Result;

    invoke-virtual {v3}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->job_result()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v6

    invoke-direct {v5, p0, v6}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Result;-><init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 477
    iget-object v6, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Progress;

    invoke-virtual {v3}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->job_print_total_pages()I

    move-result v7

    iput v7, v6, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Progress;->totalPages:I

    .line 478
    iget-object v6, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Progress;

    invoke-virtual {v3}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->job_print_current_pages()I

    move-result v7

    iput v7, v6, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Progress;->currentPages:I

    .line 479
    iget-object v6, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Progress;

    iget v6, v6, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Progress;->currentPages:I

    if-ge v6, v1, :cond_5

    .line 480
    iget-object v6, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Progress;

    iput v1, v6, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Progress;->currentPages:I

    .line 483
    :cond_5
    invoke-virtual {v3}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->job_state()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v6

    .line 484
    sget-object v7, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$7;->$SwitchMap$epson$print$copy$Component$eremoteoperation$ERemoteOperation$ERemoteParam:[I

    invoke-virtual {v6}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_0

    goto :goto_1

    :pswitch_0
    move-object v3, v5

    const/4 v2, 0x1

    goto :goto_0

    .line 487
    :pswitch_1
    iget-object v7, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Progress;

    sget-object v8, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;->Canceling:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    iput-object v8, v7, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Progress;->taskProgress:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    goto :goto_1

    .line 486
    :pswitch_2
    iget-object v7, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Progress;

    sget-object v8, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;->Copying:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    iput-object v8, v7, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Progress;->taskProgress:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    goto :goto_1

    .line 485
    :pswitch_3
    iget-object v7, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Progress;

    sget-object v8, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;->Scanning:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    iput-object v8, v7, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Progress;->taskProgress:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    .line 502
    :goto_1
    sget-object v7, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$7;->$SwitchMap$epson$print$copy$Component$eremoteoperation$ERemoteOperation$ERemoteParam:[I

    invoke-virtual {v3}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->printer_state()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v8

    invoke-virtual {v8}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_1

    goto :goto_2

    .line 506
    :pswitch_4
    sget-object v7, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->copying:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    if-eq v6, v7, :cond_6

    sget-object v7, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scanning:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    if-ne v6, v7, :cond_7

    .line 507
    :cond_6
    iget-object v6, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Progress;

    iput-object p1, v6, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Progress;->resumeState:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$ResumeState;

    .line 508
    sget-object v7, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;->Stopped:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    iput-object v7, v6, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Progress;->taskProgress:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    .line 509
    iget-object v6, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Progress;

    new-instance v7, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$6;

    invoke-direct {v7, p0, v3}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$6;-><init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;)V

    iput-object v7, v6, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Progress;->resumeRequest:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyInvalidateResumeRequest;

    .line 565
    :cond_7
    :goto_2
    :pswitch_5
    new-array v3, v1, [Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Progress;

    iget-object v6, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Progress;

    aput-object v6, v3, v0

    invoke-virtual {p0, v3}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->publishProgress([Ljava/lang/Object;)V

    .line 569
    invoke-virtual {p0}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->resumeExecute()I

    move-result v3

    int-to-long v6, v3

    .line 571
    :try_start_0
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    move-exception v3

    .line 573
    invoke-virtual {v3}, Ljava/lang/InterruptedException;->printStackTrace()V

    :goto_3
    move-object v3, v5

    goto/16 :goto_0

    :cond_8
    return-object v3

    nop

    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0xd
        :pswitch_5
        :pswitch_5
        :pswitch_4
    .end packed-switch
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 113
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->doInBackground([Ljava/lang/Void;)Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Result;

    move-result-object p1

    return-object p1
.end method

.method protected onPostExecute(Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Result;)V
    .locals 2

    .line 581
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->statusListener:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;->Copy:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;

    iget-object p1, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Result;->taskResult:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    invoke-interface {v0, v1, p1}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;->onFinished(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 113
    check-cast p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Result;

    invoke-virtual {p0, p1}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->onPostExecute(Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Result;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .line 206
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$2;

    invoke-direct {v0, p0}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$2;-><init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;)V

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->cancelParameter:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$IRemoteCancelParameter;

    .line 218
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$3;

    invoke-direct {v0, p0}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$3;-><init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;)V

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->statusParameter:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyStatusParameter;

    .line 245
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->statusListener:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;->Copy:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;

    invoke-interface {v0, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;->onStarted(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;)V

    return-void
.end method

.method protected varargs onProgressUpdate([Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Progress;)V
    .locals 6

    const/4 v0, 0x0

    .line 250
    aget-object p1, p1, v0

    .line 251
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->statusListener:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;->Copy:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;

    iget v2, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Progress;->totalPages:I

    iget v3, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Progress;->currentPages:I

    iget-object v4, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Progress;->taskProgress:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    iget-object v5, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Progress;->resumeRequest:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyInvalidateResumeRequest;

    invoke-interface/range {v0 .. v5}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;->onProcessed(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;IILepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest;)V

    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .line 113
    check-cast p1, [Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Progress;

    invoke-virtual {p0, p1}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->onProgressUpdate([Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Progress;)V

    return-void
.end method

.method resumeExecute()I
    .locals 5

    .line 263
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Progress;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Progress;->resumeRequest:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyInvalidateResumeRequest;

    const/16 v1, 0x3e8

    if-nez v0, :cond_0

    return v1

    .line 267
    :cond_0
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Progress;

    monitor-enter v0

    .line 269
    :goto_0
    :try_start_0
    iget-object v2, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Progress;

    iget-object v2, v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Progress;->resumeState:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$ResumeState;

    if-nez v2, :cond_1

    .line 270
    iget-object v2, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Progress;

    const-wide/16 v3, 0x3e8

    invoke-virtual {v2, v3, v4}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    goto :goto_2

    :catch_0
    move-exception v2

    .line 273
    :try_start_1
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 275
    :cond_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 277
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$7;->$SwitchMap$epson$print$copy$Component$ecopycomponent$ECopyComponent$ICopyResumeRequest$ResumeState:[I

    iget-object v2, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Progress;

    iget-object v2, v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Progress;->resumeState:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$ResumeState;

    invoke-virtual {v2}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$ResumeState;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    goto :goto_1

    :pswitch_0
    const/4 v0, 0x1

    .line 305
    iput-boolean v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->cancelRequested:Z

    goto :goto_1

    .line 284
    :pswitch_1
    new-instance v0, Lepson/print/copy/Component/eremoteoperation/ERemotePrinter;

    invoke-direct {v0}, Lepson/print/copy/Component/eremoteoperation/ERemotePrinter;-><init>()V

    .line 285
    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->operation:Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;

    invoke-virtual {v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;->getHostIP()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/eremoteoperation/ERemotePrinter;->setHostIP(Ljava/lang/String;)V

    .line 286
    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->operation:Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;

    invoke-virtual {v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;->getRequestConnectionTimeout()I

    move-result v1

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/eremoteoperation/ERemotePrinter;->setRequestConnectionTimeout(I)V

    .line 287
    new-instance v1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$4;

    invoke-direct {v1, p0}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$4;-><init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;)V

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/eremoteoperation/ERemotePrinter;->clearError(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$IRemoteOperationParameter;)Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;

    const/16 v1, 0x3a98

    .line 311
    :goto_1
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Progress;

    const/4 v2, 0x0

    iput-object v2, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Progress;->resumeState:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$ResumeState;

    .line 312
    iput-object v2, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Progress;->resumeRequest:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyInvalidateResumeRequest;

    return v1

    .line 275
    :goto_2
    :try_start_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method resumeNotify(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$ResumeState;)V
    .locals 2

    .line 255
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Progress;

    monitor-enter v0

    .line 256
    :try_start_0
    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Progress;

    iput-object p1, v1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Progress;->resumeState:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$ResumeState;

    .line 257
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Progress;

    invoke-virtual {p1}, Ljava/lang/Object;->notify()V

    .line 258
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public setClientID(Ljava/lang/String;)V
    .locals 0

    .line 196
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->clientID:Ljava/lang/String;

    return-void
.end method

.method public setOptionContext(Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;)V
    .locals 0

    .line 201
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    return-void
.end method

.method public setRequestConnectionTimeout(I)V
    .locals 1

    .line 191
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->operation:Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;

    invoke-virtual {v0, p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;->setRequestConnectionTimeout(I)V

    return-void
.end method

.method public setSystemSettings(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;)V
    .locals 0

    .line 186
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->systemSettings:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;

    return-void
.end method

.method public start()Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyCancelRequest;
    .locals 1

    const/4 v0, 0x0

    .line 173
    new-array v0, v0, [Ljava/lang/Void;

    invoke-super {p0, v0}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 174
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$1;

    invoke-direct {v0, p0}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$1;-><init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;)V

    return-object v0
.end method

.method startCopy()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;
    .locals 2

    .line 319
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->operation:Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;

    new-instance v1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$5;

    invoke-direct {v1, p0}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$5;-><init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;)V

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;->copy(Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$IRemoteCopyPhotoParameter;)Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyResult;

    move-result-object v0

    .line 435
    invoke-virtual {v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyResult;->success()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 436
    invoke-virtual {v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyResult;->job_token()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->jobToken:Ljava/lang/String;

    :cond_0
    return-object v0
.end method

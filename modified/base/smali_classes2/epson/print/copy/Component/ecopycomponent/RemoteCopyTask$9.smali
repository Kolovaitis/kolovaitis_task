.class Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$9;
.super Ljava/lang/Object;
.source "RemoteCopyTask.java"

# interfaces
.implements Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyInvalidateResumeRequest;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->doInBackground([Ljava/lang/Void;)Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Result;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;

.field final synthetic val$statusResult:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;


# direct methods
.method constructor <init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;)V
    .locals 0

    .line 641
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$9;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;

    iput-object p2, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$9;->val$statusResult:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getStopReason()Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;
    .locals 2

    .line 644
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$9;->val$statusResult:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;

    invoke-virtual {v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->printer_state_reasons()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->getPrinterStopReason(Ljava/util/ArrayList;)Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    move-result-object v0

    .line 645
    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->None:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    if-ne v0, v1, :cond_0

    .line 646
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$9;->val$statusResult:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;

    invoke-virtual {v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->scanner_state_reasons()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->getScannerStopReason(Ljava/util/ArrayList;)Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public invalidate()V
    .locals 0

    return-void
.end method

.method public isPossibleClearError()Z
    .locals 4

    .line 655
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$10;->$SwitchMap$epson$print$copy$Component$ecopycomponent$ECopyComponent$ICopyResumeRequest$StopReason:[I

    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$9;->val$statusResult:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;

    invoke-virtual {v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->printer_state_reasons()Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v1}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->getPrinterStopReason(Ljava/util/ArrayList;)Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    move-result-object v1

    invoke-virtual {v1}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x0

    const/4 v2, 0x1

    packed-switch v0, :pswitch_data_0

    .line 668
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$10;->$SwitchMap$epson$print$copy$Component$ecopycomponent$ECopyComponent$ICopyResumeRequest$StopReason:[I

    iget-object v3, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$9;->val$statusResult:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;

    invoke-virtual {v3}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->scanner_state_reasons()Ljava/util/ArrayList;

    move-result-object v3

    invoke-static {v3}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->getScannerStopReason(Ljava/util/ArrayList;)Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    move-result-object v3

    invoke-virtual {v3}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_1

    return v1

    :pswitch_0
    return v1

    :pswitch_1
    return v2

    :pswitch_2
    return v1

    :pswitch_3
    return v2

    :pswitch_4
    return v2

    :pswitch_5
    return v2

    :pswitch_6
    return v1

    :pswitch_7
    return v1

    :pswitch_8
    return v2

    :pswitch_9
    return v2

    :pswitch_a
    return v2

    :pswitch_b
    return v2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x9
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
    .end packed-switch
.end method

.method public resume(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$ResumeState;)V
    .locals 4

    .line 683
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$9;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;->Processing:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    iput-object v1, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;->taskProgress:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    .line 684
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$9;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;

    const/4 v1, 0x1

    new-array v1, v1, [Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;

    iget-object v2, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->access$000(Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;[Ljava/lang/Object;)V

    .line 685
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$9;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;

    invoke-virtual {v0, p1}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->resumeNotify(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$ResumeState;)V

    return-void
.end method

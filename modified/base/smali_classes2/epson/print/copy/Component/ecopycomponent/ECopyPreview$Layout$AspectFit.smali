.class final enum Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;
.super Ljava/lang/Enum;
.source "ECopyPreview.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "AspectFit"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;

.field public static final enum Max:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;

.field public static final enum Min:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 464
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;

    const-string v1, "Max"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;->Max:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;

    .line 465
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;

    const-string v1, "Min"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;->Min:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;

    const/4 v0, 0x2

    .line 463
    new-array v0, v0, [Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;->Max:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;->Min:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;

    aput-object v1, v0, v3

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;->$VALUES:[Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 463
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;
    .locals 1

    .line 463
    const-class v0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;

    return-object p0
.end method

.method public static values()[Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;
    .locals 1

    .line 463
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;->$VALUES:[Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;

    invoke-virtual {v0}, [Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;

    return-object v0
.end method

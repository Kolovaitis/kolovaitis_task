.class Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask$1;
.super Ljava/lang/Object;
.source "RemoteCopyPhotoSetOptionTask.java"

# interfaces
.implements Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$IRemoteCopyPhotoOptionsParameter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;->doInBackground([Ljava/lang/Void;)Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask$Result;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;


# direct methods
.method constructor <init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;)V
    .locals 0

    .line 96
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask$1;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public client_id()Ljava/lang/String;
    .locals 1

    .line 99
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask$1;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;->clientID:Ljava/lang/String;

    return-object v0
.end method

.method public default_as_fixed()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public fixed_parameters()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ">;"
        }
    .end annotation

    .line 140
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method public layout()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 2

    .line 104
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask$1;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XBorderless:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionItemOf(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v0

    .line 105
    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v0

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->param:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    return-object v0
.end method

.method public preferred_parameters()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ">;"
        }
    .end annotation

    .line 145
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method public print_media_size()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 2

    .line 116
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask$1;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionItemOf(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v0

    .line 117
    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v0

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->param:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    return-object v0
.end method

.method public print_media_source()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 2

    .line 128
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask$1;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSource:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionItemOf(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v0

    .line 129
    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v0

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->param:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    return-object v0
.end method

.method public print_media_type()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 2

    .line 110
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask$1;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionItemOf(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v0

    .line 111
    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v0

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->param:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    return-object v0
.end method

.method public print_quality()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 2

    .line 122
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask$1;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintQuality:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionItemOf(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v0

    .line 123
    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v0

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->param:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    return-object v0
.end method

.method public priority_order()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ">;"
        }
    .end annotation

    .line 150
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 151
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->layout:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 152
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_size:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 153
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_type:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 154
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_apf:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 155
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_quality:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 156
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_source:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method public x_apf()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 2

    .line 134
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask$1;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XApf:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionItemOf(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v0

    .line 135
    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v0

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->param:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    return-object v0
.end method

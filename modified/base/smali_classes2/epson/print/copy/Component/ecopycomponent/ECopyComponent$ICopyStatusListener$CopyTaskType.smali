.class public final enum Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;
.super Ljava/lang/Enum;
.source "ECopyComponent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CopyTaskType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;

.field public static final enum Copy:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;

.field public static final enum Preview:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 323
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;

    const-string v1, "Preview"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;->Preview:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;

    .line 327
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;

    const-string v1, "Copy"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;->Copy:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;

    const/4 v0, 0x2

    .line 319
    new-array v0, v0, [Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;->Preview:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;->Copy:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;

    aput-object v1, v0, v3

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;->$VALUES:[Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 319
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;
    .locals 1

    .line 319
    const-class v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;

    return-object p0
.end method

.method public static values()[Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;
    .locals 1

    .line 319
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;->$VALUES:[Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;

    invoke-virtual {v0}, [Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;

    return-object v0
.end method

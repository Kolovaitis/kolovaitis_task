.class Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoGetOptionTask;
.super Landroid/os/AsyncTask;
.source "RemoteCopyPhotoGetOptionTask.java"

# interfaces
.implements Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ITask;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoGetOptionTask$Result;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoGetOptionTask$Result;",
        ">;",
        "Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ITask;"
    }
.end annotation


# instance fields
.field clientID:Ljava/lang/String;

.field contextListener:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener;

.field copyType:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

.field operation:Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;

.field optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

.field systemSettings:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;


# direct methods
.method public constructor <init>(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener;)V
    .locals 1

    .line 31
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 32
    new-instance v0, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;

    invoke-direct {v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;-><init>()V

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoGetOptionTask;->operation:Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;

    .line 33
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoGetOptionTask;->copyType:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    .line 34
    iput-object p2, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoGetOptionTask;->contextListener:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener;

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoGetOptionTask$Result;
    .locals 3

    .line 65
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoGetOptionTask;->operation:Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;

    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoGetOptionTask;->systemSettings:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;

    invoke-interface {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;->getPrinterIPAddress()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;->setHostIP(Ljava/lang/String;)V

    .line 67
    new-instance p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoGetOptionTask$Result;

    invoke-direct {p1, p0}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoGetOptionTask$Result;-><init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoGetOptionTask;)V

    .line 68
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoGetOptionTask;->copyType:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    iput-object v0, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoGetOptionTask$Result;->copyType:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    .line 70
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoGetOptionTask;->operation:Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;

    invoke-virtual {v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;->getSelectableOptions()Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$ERemoteCopyPhotoOptionResult;

    move-result-object v0

    .line 71
    invoke-virtual {v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;->success()Z

    move-result v1

    if-nez v1, :cond_1

    .line 72
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->success:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;->isNull(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;->ErrorCommunication:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;

    iput-object v0, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoGetOptionTask$Result;->error:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;

    goto :goto_0

    .line 75
    :cond_0
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;->Error:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;

    iput-object v0, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoGetOptionTask$Result;->error:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;

    :goto_0
    return-object p1

    .line 80
    :cond_1
    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoGetOptionTask;->operation:Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;

    new-instance v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoGetOptionTask$1;

    invoke-direct {v2, p0, v0}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoGetOptionTask$1;-><init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoGetOptionTask;Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;)V

    invoke-virtual {v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;->getOptions(Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$IRemoteCopyPhotoOptionsParameter;)Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$ERemoteCopyPhotoOptionResult;

    move-result-object v0

    .line 145
    invoke-virtual {v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;->success()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 146
    new-instance v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    iget-object v2, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoGetOptionTask;->copyType:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    invoke-direct {v1, v2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;-><init>(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;)V

    iput-object v1, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoGetOptionTask$Result;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    .line 147
    iget-object v1, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoGetOptionTask$Result;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    invoke-virtual {v1, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->setCopyOptionsResult(Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;)V

    .line 148
    iget-object v1, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoGetOptionTask$Result;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_size:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-static {v2, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->createOptionItem(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->add(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    .line 149
    iget-object v1, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoGetOptionTask$Result;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_type:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-static {v2, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->createOptionItem(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->add(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    .line 150
    iget-object v1, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoGetOptionTask$Result;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_source:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-static {v2, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->createOptionItem(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->add(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    .line 151
    iget-object v1, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoGetOptionTask$Result;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_quality:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-static {v2, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->createOptionItem(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->add(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    .line 152
    iget-object v1, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoGetOptionTask$Result;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_apf:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-static {v2, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->createOptionItem(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->add(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    .line 153
    iget-object v1, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoGetOptionTask$Result;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_borderless:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-static {v2, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->createOptionItem(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->add(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    .line 154
    iget-object v1, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoGetOptionTask$Result;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_color_restoration:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-static {v2, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->createLocalOptionItem(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->add(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    .line 155
    iget-object v1, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoGetOptionTask$Result;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_x_bleed:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-static {v2, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->createLocalOptionItem(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->add(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    .line 156
    iget-object v1, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoGetOptionTask$Result;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->color_effects_type:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-static {v2, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->createLocalOptionItem(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v0

    invoke-virtual {v1, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->add(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    goto :goto_1

    .line 158
    :cond_2
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->success:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;->isNull(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 159
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;->ErrorCommunication:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;

    iput-object v0, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoGetOptionTask$Result;->error:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;

    goto :goto_1

    .line 161
    :cond_3
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;->Error:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;

    iput-object v0, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoGetOptionTask$Result;->error:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;

    :goto_1
    return-object p1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 17
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoGetOptionTask;->doInBackground([Ljava/lang/Void;)Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoGetOptionTask$Result;

    move-result-object p1

    return-object p1
.end method

.method protected onPostExecute(Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoGetOptionTask$Result;)V
    .locals 3

    .line 169
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoGetOptionTask;->contextListener:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener;

    iget-object v1, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoGetOptionTask$Result;->copyType:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    iget-object v2, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoGetOptionTask$Result;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    iget-object p1, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoGetOptionTask$Result;->error:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;

    invoke-interface {v0, v1, v2, p1}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener;->onCopyOptionContextCreated(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 17
    check-cast p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoGetOptionTask$Result;

    invoke-virtual {p0, p1}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoGetOptionTask;->onPostExecute(Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoGetOptionTask$Result;)V

    return-void
.end method

.method public setClientID(Ljava/lang/String;)V
    .locals 0

    .line 60
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoGetOptionTask;->clientID:Ljava/lang/String;

    return-void
.end method

.method public setOptionContext(Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;)V
    .locals 0

    .line 45
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoGetOptionTask;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    return-void
.end method

.method public setRequestConnectionTimeout(I)V
    .locals 1

    .line 55
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoGetOptionTask;->operation:Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;

    invoke-virtual {v0, p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;->setRequestConnectionTimeout(I)V

    return-void
.end method

.method public setSystemSettings(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;)V
    .locals 0

    .line 50
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoGetOptionTask;->systemSettings:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;

    return-void
.end method

.method public start()Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyCancelRequest;
    .locals 1

    const/4 v0, 0x0

    .line 39
    new-array v0, v0, [Ljava/lang/Void;

    invoke-super {p0, v0}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    const/4 v0, 0x0

    return-object v0
.end method

.class Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;
.super Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout;
.source "ECopyPreview.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/copy/Component/ecopycomponent/ECopyPreview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Layout2Up"
.end annotation


# instance fields
.field copyFrame:Landroid/graphics/RectF;

.field imageScale:F

.field offset:Landroid/graphics/Point;

.field page1:Landroid/graphics/RectF;

.field page1Bitmap:Landroid/graphics/Bitmap;

.field page2:Landroid/graphics/RectF;

.field paper:Landroid/graphics/RectF;

.field final synthetic this$0:Lepson/print/copy/Component/ecopycomponent/ECopyPreview;


# direct methods
.method constructor <init>(Lepson/print/copy/Component/ecopycomponent/ECopyPreview;II)V
    .locals 12

    .line 785
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;->this$0:Lepson/print/copy/Component/ecopycomponent/ECopyPreview;

    invoke-direct {p0}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout;-><init>()V

    .line 786
    iget-object v0, p1, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->A4_2Up:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    .line 787
    iget-object p1, p1, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->A4_2UpPage:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    .line 788
    new-instance v1, Landroid/graphics/RectF;

    iget v2, v0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;->width:I

    int-to-float v2, v2

    iget v3, v0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;->height:I

    int-to-float v3, v3

    const/4 v4, 0x0

    invoke-direct {v1, v4, v4, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;->paper:Landroid/graphics/RectF;

    .line 789
    new-instance v1, Landroid/graphics/RectF;

    iget v2, p1, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;->width:I

    add-int/lit8 v2, v2, 0x1e

    int-to-float v2, v2

    iget v3, p1, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;->height:I

    add-int/lit8 v3, v3, 0x20

    int-to-float v3, v3

    const/high16 v4, 0x41f00000    # 30.0f

    const/high16 v5, 0x42000000    # 32.0f

    invoke-direct {v1, v4, v5, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;->page1:Landroid/graphics/RectF;

    .line 790
    new-instance v1, Landroid/graphics/RectF;

    iget-object v2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;->page1:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->right:F

    const/high16 v3, 0x420c0000    # 35.0f

    add-float/2addr v2, v3

    iget-object v4, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;->page1:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    iget-object v5, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;->page1:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->right:F

    add-float/2addr v5, v3

    iget p1, p1, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;->width:I

    int-to-float p1, p1

    add-float/2addr v5, p1

    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;->page1:Landroid/graphics/RectF;

    iget p1, p1, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v1, v2, v4, v5, p1}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;->page2:Landroid/graphics/RectF;

    int-to-float p1, p2

    int-to-float p2, p3

    .line 791
    iget p3, v0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;->width:I

    int-to-float v9, p3

    iget p3, v0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;->height:I

    int-to-float v10, p3

    sget-object v11, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;->Min:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;

    move-object v6, p0

    move v7, p1

    move v8, p2

    invoke-virtual/range {v6 .. v11}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;->getAspectFitScale(FFFFLepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;)F

    move-result p3

    .line 792
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;->paper:Landroid/graphics/RectF;

    invoke-virtual {p0, p3, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;->scaleRect(FLandroid/graphics/RectF;)V

    .line 793
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;->page1:Landroid/graphics/RectF;

    invoke-virtual {p0, p3, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;->scaleRect(FLandroid/graphics/RectF;)V

    .line 794
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;->page2:Landroid/graphics/RectF;

    invoke-virtual {p0, p3, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;->scaleRect(FLandroid/graphics/RectF;)V

    .line 796
    new-instance p3, Landroid/graphics/Point;

    invoke-direct {p3}, Landroid/graphics/Point;-><init>()V

    iput-object p3, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;->offset:Landroid/graphics/Point;

    .line 797
    iget-object p3, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;->offset:Landroid/graphics/Point;

    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;->paper:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->right:F

    sub-float/2addr p1, v0

    float-to-int p1, p1

    div-int/lit8 p1, p1, 0x2

    iput p1, p3, Landroid/graphics/Point;->x:I

    .line 798
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;->offset:Landroid/graphics/Point;

    iget-object p3, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;->paper:Landroid/graphics/RectF;

    iget p3, p3, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr p2, p3

    float-to-int p2, p2

    div-int/lit8 p2, p2, 0x2

    iput p2, p1, Landroid/graphics/Point;->y:I

    return-void
.end method


# virtual methods
.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 6

    .line 803
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;->paper:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    float-to-int v0, v0

    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;->paper:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    float-to-int v1, v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 804
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 805
    iget-object v2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;->this$0:Lepson/print/copy/Component/ecopycomponent/ECopyPreview;

    iget-object v2, v2, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->paperDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v5, 0x0

    invoke-virtual {v2, v5, v5, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 806
    iget-object v2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;->this$0:Lepson/print/copy/Component/ecopycomponent/ECopyPreview;

    iget-object v2, v2, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->paperDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 808
    iget-object v2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;->page1:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    float-to-int v2, v2

    iget-object v3, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;->page1:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    float-to-int v3, v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 809
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    const v4, -0x333334

    .line 810
    invoke-virtual {v3, v4}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 811
    iget-object v3, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;->page1Bitmap:Landroid/graphics/Bitmap;

    if-nez v3, :cond_0

    move-object v3, v2

    :cond_0
    iget-object v4, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;->page1:Landroid/graphics/RectF;

    const/4 v5, 0x0

    invoke-virtual {v1, v3, v5, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 812
    iget-object v3, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;->page2:Landroid/graphics/RectF;

    invoke-virtual {v1, v2, v5, v3, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    return-object v0
.end method

.method getCopyFrame()Landroid/graphics/RectF;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method getOffset()Landroid/graphics/Point;
    .locals 2

    .line 838
    new-instance v0, Landroid/graphics/Point;

    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;->offset:Landroid/graphics/Point;

    invoke-direct {v0, v1}, Landroid/graphics/Point;-><init>(Landroid/graphics/Point;)V

    return-object v0
.end method

.method getPaperRect()Landroid/graphics/RectF;
    .locals 4

    .line 858
    new-instance v0, Landroid/graphics/RectF;

    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;->paper:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    iget-object v2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;->paper:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    const/4 v3, 0x0

    invoke-direct {v0, v3, v3, v1, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    return-object v0
.end method

.method getScanImageScale(II)F
    .locals 7

    .line 819
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;->page1:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v2

    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;->page1:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v3

    int-to-float v4, p1

    int-to-float v5, p2

    sget-object v6, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;->Min:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;

    move-object v1, p0

    invoke-virtual/range {v1 .. v6}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;->getAspectFitScale(FFFFLepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;)F

    move-result p1

    iput p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;->imageScale:F

    .line 820
    iget p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;->imageScale:F

    return p1
.end method

.method setCopyScale(F)V
    .locals 0

    return-void
.end method

.method setPaperFrame(Landroid/graphics/RectF;)V
    .locals 1

    .line 843
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, p1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;->copyFrame:Landroid/graphics/RectF;

    .line 844
    iget p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;->imageScale:F

    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;->copyFrame:Landroid/graphics/RectF;

    invoke-virtual {p0, p1, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;->scaleRect(FLandroid/graphics/RectF;)V

    return-void
.end method

.method setScanImage(Landroid/graphics/Bitmap;)V
    .locals 6

    .line 825
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;->copyFrame:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    float-to-int v0, v0

    .line 826
    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;->copyFrame:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    float-to-int v1, v1

    .line 827
    iget-object v2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;->copyFrame:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->right:F

    float-to-int v2, v2

    .line 828
    iget-object v3, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;->copyFrame:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    float-to-int v3, v3

    .line 829
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4, v0, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 831
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;->page1:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    float-to-int v0, v0

    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;->page1:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    float-to-int v1, v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;->page1Bitmap:Landroid/graphics/Bitmap;

    .line 832
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;->page1Bitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 833
    new-instance v1, Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/Canvas;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0}, Landroid/graphics/Canvas;->getHeight()I

    move-result v3

    int-to-float v3, v3

    const/4 v5, 0x0

    invoke-direct {v1, v5, v5, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v4, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    return-void
.end method

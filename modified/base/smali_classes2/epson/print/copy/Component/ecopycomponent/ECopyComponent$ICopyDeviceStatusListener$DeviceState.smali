.class public final enum Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;
.super Ljava/lang/Enum;
.source "ECopyComponent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DeviceState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;

.field public static final enum Idle:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;

.field public static final enum Processing:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;

.field public static final enum Stopped:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 455
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;

    const-string v1, "Idle"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;->Idle:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;

    .line 459
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;

    const-string v1, "Processing"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;->Processing:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;

    .line 463
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;

    const-string v1, "Stopped"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;->Stopped:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;

    const/4 v0, 0x3

    .line 451
    new-array v0, v0, [Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;->Idle:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;->Processing:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;

    aput-object v1, v0, v3

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;->Stopped:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;

    aput-object v1, v0, v4

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;->$VALUES:[Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 451
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;
    .locals 1

    .line 451
    const-class v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;

    return-object p0
.end method

.method public static values()[Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;
    .locals 1

    .line 451
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;->$VALUES:[Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;

    invoke-virtual {v0}, [Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;

    return-object v0
.end method

.class Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;
.super Landroid/os/AsyncTask;
.source "RemoteCopyTask.java"

# interfaces
.implements Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ITask;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Result;,
        Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;,
        Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$CopyMode;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;",
        "Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Result;",
        ">;",
        "Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ITask;"
    }
.end annotation


# instance fields
.field cancelParameter:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$IRemoteCancelParameter;

.field cancelRequested:Z

.field clientID:Ljava/lang/String;

.field copyMode:Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$CopyMode;

.field jobToken:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mCopyParams:Lepson/print/copy/Component/ecopycomponent/CopyParams;

.field operation:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;

.field optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

.field progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;

.field recoverJobToken:Ljava/lang/String;

.field statusListener:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;

.field statusParameter:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyStatusParameter;

.field systemSettings:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;


# direct methods
.method public constructor <init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$CopyMode;Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;Landroid/content/Context;)V
    .locals 1

    const-string v0, ""

    .line 160
    invoke-direct {p0, p1, v0, p2, p3}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;-><init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$CopyMode;Ljava/lang/String;Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$CopyMode;Ljava/lang/String;Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;Landroid/content/Context;)V
    .locals 1

    .line 149
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 150
    new-instance v0, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;

    invoke-direct {v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;-><init>()V

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->operation:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;

    .line 151
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;

    invoke-direct {v0, p0}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;-><init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;)V

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;

    .line 152
    iput-object p3, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->statusListener:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;

    .line 153
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->copyMode:Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$CopyMode;

    .line 154
    iput-object p2, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->recoverJobToken:Ljava/lang/String;

    .line 156
    iput-object p4, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->mContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$000(Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;[Ljava/lang/Object;)V
    .locals 0

    .line 36
    invoke-virtual {p0, p1}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->publishProgress([Ljava/lang/Object;)V

    return-void
.end method

.method private getCopyParams(Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyParameter;)Lepson/print/copy/Component/ecopycomponent/CopyParams;
    .locals 2
    .param p1    # Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyParameter;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 528
    :try_start_0
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/CopyParams;

    invoke-direct {v0}, Lepson/print/copy/Component/ecopycomponent/CopyParams;-><init>()V

    .line 530
    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyParameter;->color_effects_type()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v1

    invoke-virtual {v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lepson/print/copy/Component/ecopycomponent/CopyParams;->colorMode:Ljava/lang/String;

    .line 531
    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyParameter;->x_density()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lepson/print/copy/Component/ecopycomponent/CopyParams;->density:Ljava/lang/String;

    .line 533
    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyParameter;->copy_magnification()I

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "-999"

    goto :goto_0

    .line 534
    :cond_0
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    iput-object v1, v0, Lepson/print/copy/Component/ecopycomponent/CopyParams;->magnification:Ljava/lang/String;

    .line 535
    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyParameter;->print_media_size()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v1

    invoke-virtual {v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lepson/print/copy/Component/ecopycomponent/CopyParams;->paperSize:Ljava/lang/String;

    .line 536
    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyParameter;->print_media_type()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v1

    invoke-virtual {v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lepson/print/copy/Component/ecopycomponent/CopyParams;->paperType:Ljava/lang/String;

    .line 537
    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyParameter;->print_media_source()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v1

    invoke-virtual {v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lepson/print/copy/Component/ecopycomponent/CopyParams;->printDevice:Ljava/lang/String;

    .line 538
    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyParameter;->scan_content_type()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v1

    invoke-virtual {v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lepson/print/copy/Component/ecopycomponent/CopyParams;->copyType:Ljava/lang/String;

    .line 539
    invoke-interface {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyParameter;->print_quality()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object p1

    invoke-virtual {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, v0, Lepson/print/copy/Component/ecopycomponent/CopyParams;->copyQuality:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 p1, 0x0

    return-object p1
.end method

.method static getPrinterStopReason(Ljava/util/ArrayList;)Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ">;)",
            "Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;"
        }
    .end annotation

    .line 100
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->None:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    .line 101
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->none:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    iget-object v1, v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->string:Ljava/lang/String;

    iput-object v1, v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->string:Ljava/lang/String;

    .line 102
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/4 v2, 0x1

    .line 104
    sget-object v3, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$10;->$SwitchMap$epson$print$copy$Component$eremoteoperation$ERemoteOperation$ERemoteParam:[I

    invoke-virtual {v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    const/4 v2, 0x0

    goto :goto_1

    .line 112
    :pswitch_0
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->PrinterOtherError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    goto :goto_1

    .line 111
    :pswitch_1
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->PrinterOutputAreaFullError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    goto :goto_1

    .line 110
    :pswitch_2
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->PrinterCoverOpenError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    goto :goto_1

    .line 109
    :pswitch_3
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->PrinterOtherError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    goto :goto_1

    .line 108
    :pswitch_4
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->PrinterMediaEmptyError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    goto :goto_1

    .line 107
    :pswitch_5
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->PrinterMediaJamError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    goto :goto_1

    .line 106
    :pswitch_6
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->PrinterMarkerWasteFullError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    goto :goto_1

    .line 105
    :pswitch_7
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->PrinterMarkerSupplyEmptyError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    :goto_1
    if-eqz v2, :cond_0

    .line 116
    iget-object v1, v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->string:Ljava/lang/String;

    iput-object v1, v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->string:Ljava/lang/String;

    goto :goto_0

    :cond_1
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0xc
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static getScannerStopReason(Ljava/util/ArrayList;)Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ">;)",
            "Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;"
        }
    .end annotation

    .line 123
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->None:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    .line 124
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->none:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    iget-object v1, v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->string:Ljava/lang/String;

    iput-object v1, v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->string:Ljava/lang/String;

    .line 125
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 127
    sget-object v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$10;->$SwitchMap$epson$print$copy$Component$eremoteoperation$ERemoteOperation$ERemoteParam:[I

    invoke-virtual {v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    goto :goto_1

    .line 130
    :pswitch_1
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->ScannerOtherError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    goto :goto_1

    .line 131
    :pswitch_2
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->ScannerOtherError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    goto :goto_1

    .line 128
    :pswitch_3
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->ScannerOtherError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    goto :goto_1

    .line 129
    :pswitch_4
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->ScannerOtherError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    .line 135
    :goto_1
    iget-object v1, v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->string:Ljava/lang/String;

    iput-object v1, v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->string:Ljava/lang/String;

    goto :goto_0

    :cond_0
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0xe
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private sendLog(I)V
    .locals 4

    .line 721
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->mCopyParams:Lepson/print/copy/Component/ecopycomponent/CopyParams;

    .line 722
    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_1

    if-nez v0, :cond_0

    goto :goto_0

    .line 728
    :cond_0
    new-instance v2, Lcom/epson/iprint/prtlogger/CommonLog;

    invoke-direct {v2}, Lcom/epson/iprint/prtlogger/CommonLog;-><init>()V

    .line 729
    invoke-virtual {v2, v1}, Lcom/epson/iprint/prtlogger/CommonLog;->setConnectionType(Landroid/content/Context;)V

    .line 730
    invoke-virtual {v2, v1}, Lcom/epson/iprint/prtlogger/CommonLog;->setPrinterName(Landroid/content/Context;)V

    const/16 v3, 0x2004

    .line 731
    iput v3, v2, Lcom/epson/iprint/prtlogger/CommonLog;->action:I

    .line 732
    iput p1, v2, Lcom/epson/iprint/prtlogger/CommonLog;->numberOfSheet:I

    .line 734
    invoke-static {v1, v0, v2}, Lcom/epson/iprint/prtlogger/Analytics;->sendCopyLog(Landroid/content/Context;Lepson/print/copy/Component/ecopycomponent/CopyParams;Lcom/epson/iprint/prtlogger/CommonLog;)V

    return-void

    :cond_1
    :goto_0
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Result;
    .locals 9

    .line 550
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->operation:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;

    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->systemSettings:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;

    invoke-interface {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;->getPrinterIPAddress()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;->setHostIP(Ljava/lang/String;)V

    .line 557
    invoke-virtual {p0}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->startCopy()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;

    move-result-object p1

    .line 558
    invoke-virtual {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;->success()Z

    move-result v0

    if-nez v0, :cond_0

    .line 559
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Result;

    invoke-direct {v0, p0, p1}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Result;-><init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;)V

    return-object v0

    :cond_0
    const/4 p1, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x1

    move-object v3, v0

    const/4 v2, 0x0

    const/4 v4, 0x0

    :goto_0
    if-nez v2, :cond_7

    .line 563
    iget-boolean v3, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->cancelRequested:Z

    if-eqz v3, :cond_2

    if-nez v4, :cond_2

    .line 564
    iget-object v3, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->operation:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;

    iget-object v4, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->cancelParameter:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$IRemoteCancelParameter;

    invoke-virtual {v3, v4}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;->cancel(Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$IRemoteCancelParameter;)Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;

    move-result-object v3

    .line 568
    invoke-virtual {v3}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;->success()Z

    move-result v3

    if-nez v3, :cond_1

    .line 569
    new-instance p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Result;

    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->canceled:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-direct {p1, p0, v0}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Result;-><init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    return-object p1

    :cond_1
    const/4 v4, 0x1

    .line 574
    :cond_2
    iget-object v3, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->operation:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;

    iget-object v5, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->statusParameter:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyStatusParameter;

    invoke-virtual {v3, v5}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;->getStatus(Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyStatusParameter;)Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;

    move-result-object v3

    .line 575
    invoke-virtual {v3}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->success()Z

    move-result v5

    if-nez v5, :cond_3

    .line 576
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;

    iget p1, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;->currentPages:I

    invoke-direct {p0, p1}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->sendLog(I)V

    .line 577
    new-instance p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Result;

    invoke-direct {p1, p0, v3}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Result;-><init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;)V

    return-object p1

    .line 580
    :cond_3
    new-instance v5, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Result;

    invoke-virtual {v3}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->job_result()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v6

    invoke-direct {v5, p0, v6}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Result;-><init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    .line 581
    iget-object v6, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;

    invoke-virtual {v3}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->job_print_total_pages()I

    move-result v7

    iput v7, v6, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;->totalPages:I

    .line 582
    iget-object v6, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;

    invoke-virtual {v3}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->job_print_current_pages()I

    move-result v7

    iput v7, v6, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;->currentPages:I

    .line 583
    iget-object v6, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;

    iget v6, v6, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;->currentPages:I

    if-ge v6, v1, :cond_4

    .line 584
    iget-object v6, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;

    iput v1, v6, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;->currentPages:I

    .line 587
    :cond_4
    invoke-virtual {v3}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->job_state()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v6

    .line 588
    sget-object v7, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$10;->$SwitchMap$epson$print$copy$Component$eremoteoperation$ERemoteOperation$ERemoteParam:[I

    invoke-virtual {v6}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_0

    goto :goto_1

    .line 621
    :pswitch_0
    iget-object v2, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;

    iget v2, v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;->currentPages:I

    invoke-direct {p0, v2}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->sendLog(I)V

    move-object v3, v5

    const/4 v2, 0x1

    goto :goto_0

    .line 593
    :pswitch_1
    iget-object v7, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;

    iput-object v0, v7, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;->resumeState:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$ResumeState;

    .line 594
    sget-object v8, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;->Waiting2ndPage:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    iput-object v8, v7, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;->taskProgress:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    .line 595
    iget-object v7, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;

    new-instance v8, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$8;

    invoke-direct {v8, p0}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$8;-><init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;)V

    iput-object v8, v7, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;->resumeRequest:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyInvalidateResumeRequest;

    goto :goto_1

    .line 591
    :pswitch_2
    iget-object v7, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;

    sget-object v8, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;->Canceling:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    iput-object v8, v7, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;->taskProgress:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    goto :goto_1

    .line 590
    :pswitch_3
    iget-object v7, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;

    sget-object v8, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;->Copying:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    iput-object v8, v7, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;->taskProgress:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    goto :goto_1

    .line 589
    :pswitch_4
    iget-object v7, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;

    sget-object v8, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;->Scanning:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    iput-object v8, v7, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;->taskProgress:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    .line 634
    :goto_1
    sget-object v7, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$10;->$SwitchMap$epson$print$copy$Component$eremoteoperation$ERemoteOperation$ERemoteParam:[I

    invoke-virtual {v3}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->printer_state()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v8

    invoke-virtual {v8}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_1

    goto :goto_2

    .line 638
    :pswitch_5
    sget-object v7, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->copying:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    if-eq v6, v7, :cond_5

    sget-object v7, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scanning:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    if-ne v6, v7, :cond_6

    .line 639
    :cond_5
    iget-object v6, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;

    iput-object v0, v6, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;->resumeState:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$ResumeState;

    .line 640
    sget-object v7, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;->Stopped:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    iput-object v7, v6, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;->taskProgress:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    .line 641
    iget-object v6, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;

    new-instance v7, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$9;

    invoke-direct {v7, p0, v3}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$9;-><init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;)V

    iput-object v7, v6, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;->resumeRequest:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyInvalidateResumeRequest;

    .line 701
    :cond_6
    :goto_2
    :pswitch_6
    new-array v3, v1, [Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;

    iget-object v6, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;

    aput-object v6, v3, p1

    invoke-virtual {p0, v3}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->publishProgress([Ljava/lang/Object;)V

    .line 705
    invoke-virtual {p0}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->resumeExecute()V

    const/16 v3, 0x3e8

    int-to-long v6, v3

    .line 707
    :try_start_0
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    move-exception v3

    .line 709
    invoke-virtual {v3}, Ljava/lang/InterruptedException;->printStackTrace()V

    :goto_3
    move-object v3, v5

    goto/16 :goto_0

    :cond_7
    return-object v3

    :pswitch_data_0
    .packed-switch 0x18
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x15
        :pswitch_6
        :pswitch_6
        :pswitch_5
    .end packed-switch
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 36
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->doInBackground([Ljava/lang/Void;)Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Result;

    move-result-object p1

    return-object p1
.end method

.method protected onPostExecute(Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Result;)V
    .locals 2

    .line 738
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->statusListener:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;->Copy:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;

    iget-object p1, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Result;->taskResult:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    invoke-interface {v0, v1, p1}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;->onFinished(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 36
    check-cast p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Result;

    invoke-virtual {p0, p1}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->onPostExecute(Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Result;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .line 198
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$2;

    invoke-direct {v0, p0}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$2;-><init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;)V

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->cancelParameter:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$IRemoteCancelParameter;

    .line 210
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$3;

    invoke-direct {v0, p0}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$3;-><init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;)V

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->statusParameter:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyStatusParameter;

    .line 236
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->statusListener:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;->Copy:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;

    invoke-interface {v0, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;->onStarted(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;)V

    return-void
.end method

.method protected varargs onProgressUpdate([Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;)V
    .locals 6

    const/4 v0, 0x0

    .line 241
    aget-object p1, p1, v0

    .line 242
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->statusListener:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;->Copy:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;

    iget v2, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;->totalPages:I

    iget v3, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;->currentPages:I

    iget-object v4, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;->taskProgress:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    iget-object v5, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;->resumeRequest:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyInvalidateResumeRequest;

    invoke-interface/range {v0 .. v5}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;->onProcessed(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;IILepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest;)V

    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .line 36
    check-cast p1, [Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;

    invoke-virtual {p0, p1}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->onProgressUpdate([Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;)V

    return-void
.end method

.method resumeExecute()V
    .locals 6

    .line 253
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;->resumeRequest:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyInvalidateResumeRequest;

    if-nez v0, :cond_0

    return-void

    .line 257
    :cond_0
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;

    monitor-enter v0

    :cond_1
    :goto_0
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 259
    :try_start_0
    iget-object v3, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;

    iget-object v3, v3, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;->resumeState:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$ResumeState;

    if-nez v3, :cond_2

    .line 260
    iget-object v3, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;

    const-wide/16 v4, 0x3e8

    invoke-virtual {v3, v4, v5}, Ljava/lang/Object;->wait(J)V

    .line 263
    iget-object v3, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->operation:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;

    iget-object v4, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->statusParameter:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyStatusParameter;

    invoke-virtual {v3, v4}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;->getStatus(Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyStatusParameter;)Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;

    move-result-object v3

    .line 264
    sget-object v4, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$10;->$SwitchMap$epson$print$copy$Component$eremoteoperation$ERemoteOperation$ERemoteParam:[I

    invoke-virtual {v3}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->printer_state()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v5

    invoke-virtual {v5}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 276
    :pswitch_0
    iget-object v4, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;

    iget-object v4, v4, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;->resumeRequest:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyInvalidateResumeRequest;

    invoke-interface {v4}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyInvalidateResumeRequest;->getStopReason()Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    move-result-object v4

    invoke-virtual {v3}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->printer_state_reasons()Ljava/util/ArrayList;

    move-result-object v3

    invoke-static {v3}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->getPrinterStopReason(Ljava/util/ArrayList;)Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    move-result-object v3

    invoke-virtual {v4, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->equals(Ljava/lang/Object;)Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_1

    .line 280
    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void

    .line 271
    :pswitch_1
    :try_start_2
    iget-object v3, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;

    sget-object v4, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;->Processing:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    iput-object v4, v3, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;->taskProgress:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    .line 272
    new-array v3, v2, [Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;

    iget-object v4, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;

    aput-object v4, v3, v1

    invoke-virtual {p0, v3}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->publishProgress([Ljava/lang/Object;)V

    .line 273
    sget-object v3, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$ResumeState;->ClearError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$ResumeState;

    invoke-virtual {p0, v3}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->resumeNotify(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$ResumeState;)V

    goto :goto_0

    .line 266
    :pswitch_2
    iget-object v3, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;

    sget-object v4, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;->Processing:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    iput-object v4, v3, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;->taskProgress:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskProgress;

    .line 267
    new-array v3, v2, [Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;

    iget-object v4, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;

    aput-object v4, v3, v1

    invoke-virtual {p0, v3}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->publishProgress([Ljava/lang/Object;)V

    .line 268
    sget-object v3, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$ResumeState;->Cancel:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$ResumeState;

    invoke-virtual {p0, v3}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->resumeNotify(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$ResumeState;)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    goto/16 :goto_4

    :catch_0
    move-exception v3

    .line 288
    :try_start_3
    invoke-virtual {v3}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 290
    :cond_2
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 292
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$10;->$SwitchMap$epson$print$copy$Component$ecopycomponent$ECopyComponent$ICopyResumeRequest$ResumeState:[I

    iget-object v3, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;

    iget-object v3, v3, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;->resumeState:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$ResumeState;

    invoke-virtual {v3}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$ResumeState;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_1

    goto :goto_3

    .line 336
    :pswitch_3
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->operation:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;

    new-instance v1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$5;

    invoke-direct {v1, p0}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$5;-><init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;)V

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;->documentChanged(Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyDocumentChangedParameter;)Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;

    move-result-object v0

    .line 353
    invoke-virtual {v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;->success()Z

    move-result v0

    goto :goto_3

    .line 331
    :pswitch_4
    iput-boolean v2, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->cancelRequested:Z

    goto :goto_3

    .line 299
    :pswitch_5
    new-instance v0, Lepson/print/copy/Component/eremoteoperation/ERemotePrinter;

    invoke-direct {v0}, Lepson/print/copy/Component/eremoteoperation/ERemotePrinter;-><init>()V

    .line 300
    iget-object v2, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->operation:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;

    invoke-virtual {v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;->getHostIP()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lepson/print/copy/Component/eremoteoperation/ERemotePrinter;->setHostIP(Ljava/lang/String;)V

    .line 301
    iget-object v2, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->operation:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;

    invoke-virtual {v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;->getRequestConnectionTimeout()I

    move-result v2

    invoke-virtual {v0, v2}, Lepson/print/copy/Component/eremoteoperation/ERemotePrinter;->setRequestConnectionTimeout(I)V

    .line 302
    new-instance v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$4;

    invoke-direct {v2, p0}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$4;-><init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;)V

    invoke-virtual {v0, v2}, Lepson/print/copy/Component/eremoteoperation/ERemotePrinter;->clearError(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$IRemoteOperationParameter;)Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;

    :goto_1
    const/4 v0, 0x6

    if-ge v1, v0, :cond_4

    .line 318
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->operation:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;

    iget-object v2, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->statusParameter:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyStatusParameter;

    invoke-virtual {v0, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;->getStatus(Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyStatusParameter;)Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;

    move-result-object v0

    .line 319
    invoke-virtual {v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->printer_state()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v0

    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->processing:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    goto :goto_3

    :cond_3
    const-wide/16 v2, 0x1388

    .line 323
    :try_start_4
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_2

    :catch_1
    move-exception v0

    .line 325
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 364
    :cond_4
    :goto_3
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;

    const/4 v1, 0x0

    iput-object v1, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;->resumeState:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$ResumeState;

    .line 365
    iput-object v1, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;->resumeRequest:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyInvalidateResumeRequest;

    return-void

    .line 290
    :goto_4
    :try_start_5
    monitor-exit v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    throw v1

    :pswitch_data_0
    .packed-switch 0x15
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method resumeNotify(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$ResumeState;)V
    .locals 2

    .line 246
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;

    monitor-enter v0

    .line 247
    :try_start_0
    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;

    iput-object p1, v1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;->resumeState:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$ResumeState;

    .line 248
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->progress:Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Progress;

    invoke-virtual {p1}, Ljava/lang/Object;->notify()V

    .line 249
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public setClientID(Ljava/lang/String;)V
    .locals 0

    .line 188
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->clientID:Ljava/lang/String;

    return-void
.end method

.method public setOptionContext(Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;)V
    .locals 0

    .line 193
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    return-void
.end method

.method public setRequestConnectionTimeout(I)V
    .locals 1

    .line 183
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->operation:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;

    invoke-virtual {v0, p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;->setRequestConnectionTimeout(I)V

    return-void
.end method

.method public setSystemSettings(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;)V
    .locals 0

    .line 178
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->systemSettings:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;

    return-void
.end method

.method public start()Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyCancelRequest;
    .locals 1

    const/4 v0, 0x0

    .line 165
    new-array v0, v0, [Ljava/lang/Void;

    invoke-super {p0, v0}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 166
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$1;

    invoke-direct {v0, p0}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$1;-><init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;)V

    return-object v0
.end method

.method startCopy()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;
    .locals 4

    .line 370
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->copyMode:Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$CopyMode;

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$CopyMode;->Copy:Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$CopyMode;

    if-ne v0, v1, :cond_0

    .line 371
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$6;

    invoke-direct {v0, p0}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$6;-><init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;)V

    .line 483
    invoke-direct {p0, v0}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->getCopyParams(Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyParameter;)Lepson/print/copy/Component/ecopycomponent/CopyParams;

    move-result-object v1

    iput-object v1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->mCopyParams:Lepson/print/copy/Component/ecopycomponent/CopyParams;

    .line 484
    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->operation:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;

    invoke-virtual {v1, v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;->copy(Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyParameter;)Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyResult;

    move-result-object v0

    .line 485
    invoke-virtual {v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyResult;->success()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 486
    invoke-virtual {v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyResult;->job_token()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->jobToken:Ljava/lang/String;

    goto :goto_1

    .line 493
    :cond_0
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->operation:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;

    new-instance v1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$7;

    invoke-direct {v1, p0}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$7;-><init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;)V

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;->getStatus(Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyStatusParameter;)Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;

    move-result-object v0

    .line 511
    invoke-virtual {v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->success()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 512
    invoke-virtual {v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->job_tokens()Ljava/util/ArrayList;

    move-result-object v1

    .line 513
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 514
    iget-object v3, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->recoverJobToken:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 515
    iput-object v2, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->jobToken:Ljava/lang/String;

    goto :goto_0

    :cond_2
    :goto_1
    return-object v0
.end method

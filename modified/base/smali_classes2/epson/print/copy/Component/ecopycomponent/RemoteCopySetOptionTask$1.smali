.class Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask$1;
.super Ljava/lang/Object;
.source "RemoteCopySetOptionTask.java"

# interfaces
.implements Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyOptionsParameter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->doInBackground([Ljava/lang/Void;)Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;


# direct methods
.method constructor <init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;)V
    .locals 0

    .line 285
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask$1;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public client_id()Ljava/lang/String;
    .locals 1

    .line 288
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask$1;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->clientID:Ljava/lang/String;

    return-object v0
.end method

.method public default_as_fixed()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public fixed_parameters()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ">;"
        }
    .end annotation

    .line 348
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method public layout()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 1

    .line 293
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask$1;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->getCopyType()Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    move-result-object v0

    invoke-static {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->layoutOf(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;)Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v0

    return-object v0
.end method

.method public preferred_parameters()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ">;"
        }
    .end annotation

    .line 353
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 354
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_sheet_collate:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 355
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scan_sides:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 356
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->layout:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 357
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_sides:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 358
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scan_media_size:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 359
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_size:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 360
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_type:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 361
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scan_content_type:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 362
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_quality:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 363
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_source:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method public print_media_size()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 2

    .line 325
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask$1;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionItemOf(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v0

    .line 326
    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v0

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->param:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    return-object v0
.end method

.method public print_media_source()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 2

    .line 331
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask$1;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSource:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionItemOf(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v0

    .line 332
    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v0

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->param:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    return-object v0
.end method

.method public print_media_type()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 2

    .line 319
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask$1;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionItemOf(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v0

    .line 320
    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v0

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->param:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    return-object v0
.end method

.method public print_quality()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 2

    .line 337
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask$1;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintQuality:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionItemOf(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v0

    .line 338
    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v0

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->param:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    return-object v0
.end method

.method public print_sheet_collate()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 2

    .line 343
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask$1;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionsResult()Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;

    move-result-object v0

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_sheet_collate:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;->parameter_default(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v0

    return-object v0
.end method

.method public print_sides()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 2

    .line 303
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask$1;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionsResult()Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;

    move-result-object v0

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_sides:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;->parameter_default(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v0

    return-object v0
.end method

.method public priority_order()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ">;"
        }
    .end annotation

    .line 369
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 370
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_sheet_collate:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 371
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scan_sides:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 372
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->layout:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 373
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_sides:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 374
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scan_media_size:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 375
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_size:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 376
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_type:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 377
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scan_content_type:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 378
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_quality:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 379
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_source:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method public scan_content_type()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 2

    .line 313
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask$1;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->ScanContentType:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionItemOf(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v0

    .line 314
    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v0

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->param:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    return-object v0
.end method

.method public scan_media_size()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 2

    .line 308
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask$1;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionsResult()Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;

    move-result-object v0

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scan_media_size:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;->parameter_default(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v0

    return-object v0
.end method

.method public scan_sides()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 2

    .line 298
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask$1;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionsResult()Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;

    move-result-object v0

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scan_sides:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;->parameter_default(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v0

    return-object v0
.end method

.class Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutMirror;
.super Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;
.source "ECopyPreview.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/copy/Component/ecopycomponent/ECopyPreview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "LayoutMirror"
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/copy/Component/ecopycomponent/ECopyPreview;


# direct methods
.method constructor <init>(Lepson/print/copy/Component/ecopycomponent/ECopyPreview;II)V
    .locals 0

    .line 661
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutMirror;->this$0:Lepson/print/copy/Component/ecopycomponent/ECopyPreview;

    .line 662
    invoke-direct {p0, p1, p2, p3}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;-><init>(Lepson/print/copy/Component/ecopycomponent/ECopyPreview;II)V

    return-void
.end method


# virtual methods
.method getBitmap()Landroid/graphics/Bitmap;
    .locals 11

    .line 667
    invoke-virtual {p0}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutMirror;->showPaper()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 668
    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutMirror;->paperImage:Landroid/graphics/Bitmap;

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutMirror;->maxPaperImage:Landroid/graphics/Bitmap;

    .line 669
    :goto_0
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutMirror;->canvas:Landroid/graphics/Canvas;

    .line 670
    iget-object v2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutMirror;->canvas:Landroid/graphics/Canvas;

    invoke-virtual {v2}, Landroid/graphics/Canvas;->save()I

    .line 672
    iget-object v2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutMirror;->this$0:Lepson/print/copy/Component/ecopycomponent/ECopyPreview;

    iget-object v2, v2, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->paperDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v5, 0x0

    invoke-virtual {v2, v5, v5, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 673
    iget-object v2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutMirror;->this$0:Lepson/print/copy/Component/ecopycomponent/ECopyPreview;

    iget-object v2, v2, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->paperDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutMirror;->canvas:Landroid/graphics/Canvas;

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 675
    iget-object v2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutMirror;->scanImage:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_3

    const/4 v2, 0x0

    const/4 v3, 0x0

    if-eqz v0, :cond_2

    .line 677
    iget v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutMirror;->copyScale:F

    iget v4, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutMirror;->limitCopyScaleDown:F

    div-float/2addr v0, v4

    .line 678
    iget-boolean v4, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutMirror;->autofitEnabled:Z

    if-eqz v4, :cond_1

    const/high16 v0, 0x3f800000    # 1.0f

    .line 679
    iget-object v4, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutMirror;->scanFrame:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v6

    iget-object v4, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutMirror;->scanFrame:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v7

    iget-object v4, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutMirror;->canvas:Landroid/graphics/Canvas;

    invoke-virtual {v4}, Landroid/graphics/Canvas;->getWidth()I

    move-result v4

    int-to-float v8, v4

    iget-object v4, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutMirror;->canvas:Landroid/graphics/Canvas;

    invoke-virtual {v4}, Landroid/graphics/Canvas;->getHeight()I

    move-result v4

    int-to-float v9, v4

    sget-object v10, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;->Max:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;

    move-object v5, p0

    invoke-virtual/range {v5 .. v10}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutMirror;->getAspectFitScale(FFFFLepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;)F

    move-result v4

    div-float/2addr v0, v4

    .line 681
    :cond_1
    new-instance v4, Landroid/graphics/Matrix;

    invoke-direct {v4}, Landroid/graphics/Matrix;-><init>()V

    .line 682
    invoke-virtual {v4, v0, v0}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 688
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    int-to-float v5, v5

    iget-object v6, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutMirror;->scanImage:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    int-to-float v6, v6

    mul-float v6, v6, v0

    sub-float/2addr v5, v6

    invoke-virtual {v4, v5, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 689
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutMirror;->canvas:Landroid/graphics/Canvas;

    iget-object v3, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutMirror;->paperBorderClipPath:Landroid/graphics/Path;

    invoke-virtual {v0, v3}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;)Z

    .line 690
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutMirror;->canvas:Landroid/graphics/Canvas;

    iget-object v3, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutMirror;->scanImage:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v3, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    goto :goto_1

    .line 692
    :cond_2
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutMirror;->canvas:Landroid/graphics/Canvas;

    iget-object v4, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutMirror;->scanImage:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v4, v3, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 696
    :cond_3
    :goto_1
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutMirror;->canvas:Landroid/graphics/Canvas;

    invoke-virtual {v0}, Landroid/graphics/Canvas;->restore()V

    return-object v1
.end method

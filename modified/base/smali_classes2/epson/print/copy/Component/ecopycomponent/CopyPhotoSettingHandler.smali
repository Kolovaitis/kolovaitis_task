.class Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;
.super Lorg/xml/sax/helpers/DefaultHandler;
.source "RemoteCopyPhotoTask.java"


# instance fields
.field final b:Ljava/lang/String;

.field final copies:Ljava/lang/String;

.field final dpi:Ljava/lang/String;

.field final g:Ljava/lang/String;

.field final height:Ljava/lang/String;

.field path:Ljava/lang/String;

.field final photoCopySetting:Ljava/lang/String;

.field final photoCopySettings:Ljava/lang/String;

.field final r:Ljava/lang/String;

.field public scanAreaHeight:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public scanAreaResolution:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public scanAreaWidth:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public scanAreaX:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public scanAreaY:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public scanCopies:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final trimmingRect:Ljava/lang/String;

.field final value:Ljava/lang/String;

.field final width:Ljava/lang/String;

.field final x:Ljava/lang/String;

.field public xFitGamma:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public xFitMatrix:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final xGamma:Ljava/lang/String;

.field final xMatrix:Ljava/lang/String;

.field final y:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 61
    invoke-direct {p0}, Lorg/xml/sax/helpers/DefaultHandler;-><init>()V

    const-string v0, "photoCopySettings"

    .line 35
    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;->photoCopySettings:Ljava/lang/String;

    const-string v0, "photoCopySetting"

    .line 36
    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;->photoCopySetting:Ljava/lang/String;

    const-string v0, "xGamma"

    .line 37
    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;->xGamma:Ljava/lang/String;

    const-string v0, "r"

    .line 38
    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;->r:Ljava/lang/String;

    const-string v0, "g"

    .line 39
    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;->g:Ljava/lang/String;

    const-string v0, "b"

    .line 40
    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;->b:Ljava/lang/String;

    const-string v0, "xMatrix"

    .line 41
    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;->xMatrix:Ljava/lang/String;

    const-string v0, "value"

    .line 42
    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;->value:Ljava/lang/String;

    const-string v0, "copies"

    .line 43
    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;->copies:Ljava/lang/String;

    const-string v0, "trimmingRect"

    .line 44
    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;->trimmingRect:Ljava/lang/String;

    const-string v0, "dpi"

    .line 45
    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;->dpi:Ljava/lang/String;

    const-string v0, "x"

    .line 46
    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;->x:Ljava/lang/String;

    const-string v0, "y"

    .line 47
    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;->y:Ljava/lang/String;

    const-string v0, "width"

    .line 48
    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;->width:Ljava/lang/String;

    const-string v0, "height"

    .line 49
    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;->height:Ljava/lang/String;

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;->scanCopies:Ljava/util/ArrayList;

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;->scanAreaResolution:Ljava/util/ArrayList;

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;->scanAreaX:Ljava/util/ArrayList;

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;->scanAreaY:Ljava/util/ArrayList;

    .line 55
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;->scanAreaHeight:Ljava/util/ArrayList;

    .line 56
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;->scanAreaWidth:Ljava/util/ArrayList;

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;->xFitGamma:Ljava/util/ArrayList;

    .line 58
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;->xFitMatrix:Ljava/util/ArrayList;

    .line 62
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;->path:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getScanCount()I
    .locals 1

    .line 66
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;->scanCopies:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public parse()Z
    .locals 3

    .line 70
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v0

    .line 72
    :try_start_0
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;->path:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 73
    invoke-virtual {v0}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v0

    invoke-virtual {v0, v1, p0}, Ljavax/xml/parsers/SAXParser;->parse(Ljava/io/File;Lorg/xml/sax/helpers/DefaultHandler;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 75
    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;->scanCopies:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 76
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 78
    :goto_0
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;->scanCopies:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    :goto_1
    return v0
.end method

.method public startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0

    const-string p1, "photoCopySettings"

    .line 90
    invoke-virtual {p2, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto/16 :goto_0

    :cond_0
    const-string p1, "photoCopySetting"

    .line 91
    invoke-virtual {p2, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto/16 :goto_0

    :cond_1
    const-string p1, "xGamma"

    .line 92
    invoke-virtual {p2, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 93
    new-instance p1, Ljava/lang/StringBuffer;

    invoke-direct {p1}, Ljava/lang/StringBuffer;-><init>()V

    const-string p2, "r"

    .line 94
    invoke-interface {p4, p2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    const-string p3, "%02X"

    invoke-virtual {p0, p2, p3}, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;->xFormat(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string p2, "g"

    .line 95
    invoke-interface {p4, p2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    const-string p3, "%02X"

    invoke-virtual {p0, p2, p3}, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;->xFormat(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string p2, "b"

    .line 96
    invoke-interface {p4, p2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    const-string p3, "%02X"

    invoke-virtual {p0, p2, p3}, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;->xFormat(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 97
    iget-object p2, p0, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;->xFitGamma:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    const-string p1, "xMatrix"

    .line 98
    invoke-virtual {p2, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 99
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;->xFitMatrix:Ljava/util/ArrayList;

    const-string p2, "value"

    invoke-interface {p4, p2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    const-string p3, "%04X"

    invoke-virtual {p0, p2, p3}, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;->xFormat(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    const-string p1, "copies"

    .line 100
    invoke-virtual {p2, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 101
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;->scanCopies:Ljava/util/ArrayList;

    const-string p2, "value"

    invoke-interface {p4, p2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    const-string p1, "trimmingRect"

    .line 102
    invoke-virtual {p2, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_5

    .line 103
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;->scanAreaResolution:Ljava/util/ArrayList;

    const-string p2, "dpi"

    invoke-interface {p4, p2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 104
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;->scanAreaX:Ljava/util/ArrayList;

    const-string p2, "x"

    invoke-interface {p4, p2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 105
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;->scanAreaY:Ljava/util/ArrayList;

    const-string p2, "y"

    invoke-interface {p4, p2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 106
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;->scanAreaWidth:Ljava/util/ArrayList;

    const-string p2, "width"

    invoke-interface {p4, p2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 107
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;->scanAreaHeight:Ljava/util/ArrayList;

    const-string p2, "height"

    invoke-interface {p4, p2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    :goto_0
    return-void
.end method

.method xFormat(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .line 82
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, ","

    .line 83
    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    array-length v1, p1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_0

    aget-object v4, p1, v3

    const/4 v5, 0x1

    .line 84
    new-array v5, v5, [Ljava/lang/Object;

    const/16 v6, 0x10

    invoke-static {v4, v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v5, v2

    invoke-static {p2, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 86
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

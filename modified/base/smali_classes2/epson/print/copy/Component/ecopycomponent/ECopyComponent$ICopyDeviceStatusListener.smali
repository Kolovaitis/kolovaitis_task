.class public interface abstract Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener;
.super Ljava/lang/Object;
.source "ECopyComponent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/copy/Component/ecopycomponent/ECopyComponent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ICopyDeviceStatusListener"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;,
        Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;,
        Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;,
        Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$Connection;
    }
.end annotation


# virtual methods
.method public abstract onDeviceStatus(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$Connection;Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$Connection;",
            "Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;",
            "Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;",
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;",
            ">;",
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;",
            ">;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.class Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$3;
.super Ljava/lang/Object;
.source "RemoteCopyTask.java"

# interfaces
.implements Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyStatusParameter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->onPreExecute()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;


# direct methods
.method constructor <init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;)V
    .locals 0

    .line 210
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$3;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public client_id()Ljava/lang/String;
    .locals 1

    .line 213
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$3;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->clientID:Ljava/lang/String;

    return-object v0
.end method

.method public job_token()Ljava/lang/String;
    .locals 1

    .line 218
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$3;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;->jobToken:Ljava/lang/String;

    return-object v0
.end method

.method public keys()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ">;"
        }
    .end annotation

    .line 223
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 224
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->job_state:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 225
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->job_result:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 226
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->job_tokens:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 227
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->printer_state:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 228
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->printer_state_reasons:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 229
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scanner_state:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 230
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scanner_state_reasons:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 231
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->job_print_total_pages:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 232
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->job_print_current_pages:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

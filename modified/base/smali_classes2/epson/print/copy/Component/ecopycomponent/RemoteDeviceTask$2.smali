.class Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask$2;
.super Ljava/lang/Object;
.source "RemoteDeviceTask.java"

# interfaces
.implements Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$IRemoteStatusParameter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask;->doInBackground([Ljava/lang/Void;)Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask$Result;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask;


# direct methods
.method constructor <init>(Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask;)V
    .locals 0

    .line 171
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask$2;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public client_id()Ljava/lang/String;
    .locals 1

    .line 174
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask$2;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask;->clientID:Ljava/lang/String;

    return-object v0
.end method

.method public keys()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ">;"
        }
    .end annotation

    .line 179
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 180
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->printer_state:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 181
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->printer_state_reasons:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 182
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scanner_state:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 183
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scanner_state_reasons:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

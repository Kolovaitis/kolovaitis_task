.class Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Result;
.super Ljava/lang/Object;
.source "RemoteCopyPhotoTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Result"
.end annotation


# instance fields
.field taskResult:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

.field final synthetic this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;


# direct methods
.method public constructor <init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V
    .locals 0

    .line 151
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Result;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 152
    invoke-virtual {p0, p2}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Result;->setResult(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    return-void
.end method

.method public constructor <init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;)V
    .locals 0

    .line 155
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Result;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 156
    sget-object p1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->success:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p2, p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;->isNull(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 157
    sget-object p1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_failed_communication:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p0, p1}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Result;->setResult(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    goto :goto_0

    .line 159
    :cond_0
    invoke-virtual {p2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;->reason()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object p1

    invoke-virtual {p0, p1}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Result;->setResult(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    :goto_0
    return-void
.end method


# virtual methods
.method setResult(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V
    .locals 1

    .line 138
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$7;->$SwitchMap$epson$print$copy$Component$eremoteoperation$ERemoteOperation$ERemoteParam:[I

    invoke-virtual {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    .line 147
    sget-object p1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;->ErrorOther:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Result;->taskResult:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    goto :goto_0

    .line 146
    :pswitch_0
    sget-object p1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;->ErrorInvalidOption:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Result;->taskResult:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    goto :goto_0

    .line 145
    :pswitch_1
    sget-object p1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;->ErrorOther:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Result;->taskResult:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    goto :goto_0

    .line 144
    :pswitch_2
    sget-object p1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;->ErrorCommunication:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Result;->taskResult:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    goto :goto_0

    .line 143
    :pswitch_3
    sget-object p1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;->ErrorOther:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Result;->taskResult:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    goto :goto_0

    .line 142
    :pswitch_4
    sget-object p1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;->Busy:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Result;->taskResult:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    goto :goto_0

    .line 141
    :pswitch_5
    sget-object p1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;->Canceled:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Result;->taskResult:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    goto :goto_0

    .line 140
    :pswitch_6
    sget-object p1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;->Succeed:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Result;->taskResult:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    goto :goto_0

    .line 139
    :pswitch_7
    sget-object p1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;->Succeed:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$Result;->taskResult:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

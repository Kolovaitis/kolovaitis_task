.class public final enum Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;
.super Ljava/lang/Enum;
.source "ECopyComponent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PrinterStateReason"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

.field public static final enum CoverOpen:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

.field public static final enum CoverOpenError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

.field public static final enum InputTrayMissingError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

.field public static final enum MarkerSupplyEmptyError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

.field public static final enum MarkerSupplyLowWarning:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

.field public static final enum MarkerWasteFullError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

.field public static final enum MediaEmptyError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

.field public static final enum MediaJamError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

.field public static final enum None:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

.field public static final enum OtherError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

.field public static final enum OutputAreaFullError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;


# instance fields
.field string:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .line 473
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    const-string v1, "None"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;->None:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    .line 477
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    const-string v1, "MarkerSupplyEmptyError"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;->MarkerSupplyEmptyError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    .line 481
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    const-string v1, "MarkerWasteFullError"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;->MarkerWasteFullError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    .line 485
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    const-string v1, "MediaJamError"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;->MediaJamError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    .line 489
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    const-string v1, "MediaEmptyError"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;->MediaEmptyError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    .line 493
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    const-string v1, "InputTrayMissingError"

    const/4 v7, 0x5

    invoke-direct {v0, v1, v7}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;->InputTrayMissingError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    .line 497
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    const-string v1, "CoverOpenError"

    const/4 v8, 0x6

    invoke-direct {v0, v1, v8}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;->CoverOpenError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    .line 501
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    const-string v1, "OutputAreaFullError"

    const/4 v9, 0x7

    invoke-direct {v0, v1, v9}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;->OutputAreaFullError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    .line 505
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    const-string v1, "OtherError"

    const/16 v10, 0x8

    invoke-direct {v0, v1, v10}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;->OtherError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    .line 509
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    const-string v1, "MarkerSupplyLowWarning"

    const/16 v11, 0x9

    invoke-direct {v0, v1, v11}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;->MarkerSupplyLowWarning:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    .line 513
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    const-string v1, "CoverOpen"

    const/16 v12, 0xa

    invoke-direct {v0, v1, v12}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;->CoverOpen:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    const/16 v0, 0xb

    .line 469
    new-array v0, v0, [Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;->None:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;->MarkerSupplyEmptyError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    aput-object v1, v0, v3

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;->MarkerWasteFullError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    aput-object v1, v0, v4

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;->MediaJamError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    aput-object v1, v0, v5

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;->MediaEmptyError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    aput-object v1, v0, v6

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;->InputTrayMissingError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    aput-object v1, v0, v7

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;->CoverOpenError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    aput-object v1, v0, v8

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;->OutputAreaFullError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    aput-object v1, v0, v9

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;->OtherError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    aput-object v1, v0, v10

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;->MarkerSupplyLowWarning:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    aput-object v1, v0, v11

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;->CoverOpen:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    aput-object v1, v0, v12

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;->$VALUES:[Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 469
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;
    .locals 1

    .line 469
    const-class v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    return-object p0
.end method

.method public static values()[Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;
    .locals 1

    .line 469
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;->$VALUES:[Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    invoke-virtual {v0}, [Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    return-object v0
.end method


# virtual methods
.method public getDebugString()Ljava/lang/String;
    .locals 1

    .line 517
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;->string:Ljava/lang/String;

    return-object v0
.end method

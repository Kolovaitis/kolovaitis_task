.class public final enum Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;
.super Ljava/lang/Enum;
.source "ECopyComponent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ScannerStateReason"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;

.field public static final enum MediaEmptyError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;

.field public static final enum MediaJamError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;

.field public static final enum MediaSizeMissmatchError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;

.field public static final enum None:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;

.field public static final enum OtherError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;


# instance fields
.field public string:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 528
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;

    const-string v1, "None"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;->None:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;

    .line 532
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;

    const-string v1, "MediaEmptyError"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;->MediaEmptyError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;

    .line 536
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;

    const-string v1, "MediaJamError"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;->MediaJamError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;

    .line 540
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;

    const-string v1, "MediaSizeMissmatchError"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;->MediaSizeMissmatchError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;

    .line 544
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;

    const-string v1, "OtherError"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;->OtherError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;

    const/4 v0, 0x5

    .line 524
    new-array v0, v0, [Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;->None:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;->MediaEmptyError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;

    aput-object v1, v0, v3

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;->MediaJamError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;

    aput-object v1, v0, v4

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;->MediaSizeMissmatchError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;

    aput-object v1, v0, v5

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;->OtherError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;

    aput-object v1, v0, v6

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;->$VALUES:[Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 524
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;
    .locals 1

    .line 524
    const-class v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;

    return-object p0
.end method

.method public static values()[Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;
    .locals 1

    .line 524
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;->$VALUES:[Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;

    invoke-virtual {v0}, [Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;

    return-object v0
.end method


# virtual methods
.method public getDebugString()Ljava/lang/String;
    .locals 1

    .line 548
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;->string:Ljava/lang/String;

    return-object v0
.end method

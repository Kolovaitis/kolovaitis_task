.class Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;
.super Landroid/os/AsyncTask;
.source "RemoteCopySetOptionTask.java"

# interfaces
.implements Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ITask;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;",
        ">;",
        "Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ITask;"
    }
.end annotation


# static fields
.field public static XScale_2L_to_A4_Value:I = 0xa5

.field public static XScale_2L_to_A4_ValueEx:I = 0xad

.field public static XScale_2L_to_KG_Value:I = 0x4d

.field public static XScale_2L_to_Letter_Value:I = 0x9d

.field public static XScale_2L_to_Postcard_Value:I = 0x4c

.field public static XScale_2L_to_Postcard_ValueEx:I = 0x59

.field public static XScale_8x10_to_2L_Value:I = 0x3d

.field public static XScale_A4_to_2L_Value:I = 0x3b

.field public static XScale_A4_to_A3_Value:I = 0x8d

.field public static XScale_A4_to_A5_Value:I = 0x45

.field public static XScale_A4_to_B5_Value:I = 0x55

.field public static XScale_A4_to_KG_Value:I = 0x2f

.field public static XScale_A4_to_Postcard_Value:I = 0x2e

.field public static XScale_A4_to_Postcard_ValueEx:I = 0x35

.field public static XScale_A5_to_A4_Value:I = 0x8d

.field public static XScale_Autofit_Value:I = 0x0

.field public static XScale_B5_to_A4_Value:I = 0x72

.field public static XScale_B5_to_A4_ValueEx:I = 0x78

.field public static XScale_Custom_Value:I = 0x0

.field public static XScale_FullSize_Value:I = 0x64

.field public static XScale_KG_to_2L_Value:I = 0x73

.field public static XScale_KG_to_8x10_Value:I = 0xa6

.field public static XScale_KG_to_A4_Value:I = 0xc3

.field public static XScale_KG_to_Letter_Value:I = 0xb7

.field public static XScale_L_to_2L_Value:I = 0x8b

.field public static XScale_L_to_2L_ValueEx:I = 0x97

.field public static XScale_L_to_A4_Value:I = 0xeb

.field public static XScale_L_to_A4_ValueEx:I = 0xf6

.field public static XScale_L_to_Postcard_Value:I = 0x6e

.field public static XScale_L_to_Postcard_ValueEx:I = 0x7d

.field public static XScale_Legal_to_Letter_Value:I = 0x4e

.field public static XScale_Letter_to_11x17_Value:I = 0x81

.field public static XScale_Letter_to_2L_Value:I = 0x39

.field public static XScale_Letter_to_KG_Value:I = 0x2d

.field public static XScale_Postcard_to_A4_Value:I = 0xc9

.field public static XScale_Postcard_to_A4_ValueEx:I = 0xdb


# instance fields
.field changableParams:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ">;"
        }
    .end annotation
.end field

.field clientID:Ljava/lang/String;

.field copyScaleMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Integer;",
            "Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;",
            ">;"
        }
    .end annotation
.end field

.field operation:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;

.field optionChangedListener:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;

.field optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

.field replacedItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

.field selectedItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

.field systemSettings:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;

.field xscaleMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 176
    sget v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->CopyMagnificationAutofitValue:I

    sput v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_Autofit_Value:I

    return-void
.end method

.method public constructor <init>(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;)V
    .locals 1

    .line 195
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 196
    new-instance v0, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;

    invoke-direct {v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;-><init>()V

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->operation:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;

    .line 197
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->selectedItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    .line 198
    iput-object p2, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->optionChangedListener:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;

    return-void
.end method

.method static getCopyMagnificationMap(Ljava/util/HashMap;)Ljava/util/HashMap;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/HashMap<",
            "Ljava/lang/Integer;",
            "Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;",
            ">;"
        }
    .end annotation

    .line 86
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 87
    invoke-virtual {p0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 88
    invoke-virtual {p0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public static getScaleChoice(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;I)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;
    .locals 0

    .line 94
    invoke-static {p0}, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->getXScaleMap(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;)Ljava/util/HashMap;

    move-result-object p0

    .line 95
    invoke-static {p0}, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->getCopyMagnificationMap(Ljava/util/HashMap;)Ljava/util/HashMap;

    move-result-object p0

    .line 96
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    if-eqz p0, :cond_0

    goto :goto_0

    .line 97
    :cond_0
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_Custom:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    :goto_0
    return-object p0
.end method

.method static getXScaleMap(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;)Ljava/util/HashMap;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;",
            ")",
            "Ljava/util/HashMap<",
            "Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 22
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 23
    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;->Borderless:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    if-ne p0, v1, :cond_0

    .line 24
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_FullSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_FullSize_Value:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 25
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_A4_to_Postcard:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_A4_to_Postcard_ValueEx:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_2L_to_Postcard:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_2L_to_Postcard_ValueEx:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_L_to_Postcard:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_L_to_Postcard_ValueEx:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_L_to_2L:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_L_to_2L_ValueEx:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_2L_to_A4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_2L_to_A4_ValueEx:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_Postcard_to_A4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_Postcard_to_A4_ValueEx:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_L_to_A4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_L_to_A4_ValueEx:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_B5_to_A4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_B5_to_A4_ValueEx:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_Custom:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_Custom_Value:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_Autofit:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_Autofit_Value:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 35
    :cond_0
    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;->Mirror:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    if-ne p0, v1, :cond_1

    .line 36
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_FullSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_FullSize_Value:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_Custom:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_Custom_Value:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_Autofit:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_Autofit_Value:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 39
    :cond_1
    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;->Standard:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    if-ne p0, v1, :cond_2

    .line 40
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_FullSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_FullSize_Value:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_Custom:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_Custom_Value:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_Autofit:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_Autofit_Value:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_Letter_to_KG:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_Letter_to_KG_Value:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_KG_to_Letter:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_KG_to_Letter_Value:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_Letter_to_2L:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_Letter_to_2L_Value:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_2L_to_Letter:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_2L_to_Letter_Value:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_KG_to_A4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_KG_to_A4_Value:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_A4_to_KG:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_A4_to_KG_Value:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_2L_to_A4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_2L_to_A4_Value:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_A4_to_2L:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_A4_to_2L_Value:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_KG_to_8x10:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_KG_to_8x10_Value:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_8x10_to_2L:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_8x10_to_2L_Value:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_Legal_to_Letter:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_Legal_to_Letter_Value:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_A4_to_Postcard:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_A4_to_Postcard_Value:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_2L_to_Postcard:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_2L_to_Postcard_Value:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_L_to_Postcard:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_L_to_Postcard_Value:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_L_to_2L:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_L_to_2L_Value:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_Postcard_to_A4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_Postcard_to_A4_Value:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_L_to_A4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_L_to_A4_Value:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_A4_to_B5:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_A4_to_B5_Value:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_B5_to_A4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_B5_to_A4_Value:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_2L_to_KG:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_2L_to_KG_Value:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_KG_to_2L:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_KG_to_2L_Value:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_A5_to_A4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_A5_to_A4_Value:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_A4_to_A5:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_A4_to_A5_Value:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_A4_to_A3:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_A4_to_A3_Value:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_Letter_to_11x17:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_Letter_to_11x17_Value:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 69
    :cond_2
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_FullSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_FullSize_Value:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_A4_to_Postcard:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_A4_to_Postcard_Value:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_2L_to_Postcard:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_2L_to_Postcard_Value:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_L_to_Postcard:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_L_to_Postcard_Value:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_L_to_2L:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_L_to_2L_Value:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_2L_to_A4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_2L_to_A4_Value:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_Postcard_to_A4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_Postcard_to_A4_Value:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_L_to_A4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_L_to_A4_Value:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_A4_to_B5:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_A4_to_B5_Value:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_B5_to_A4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_B5_to_A4_Value:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_Custom:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_Custom_Value:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_Autofit:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    new-instance v1, Ljava/lang/Integer;

    sget v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_Autofit_Value:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-object v0
.end method

.method public static getcopyMagnification(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;)I
    .locals 0

    .line 101
    invoke-static {p0}, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->getXScaleMap(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;)Ljava/util/HashMap;

    move-result-object p0

    .line 102
    invoke-virtual {p0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static scaleValue(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;)I
    .locals 1

    .line 202
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;->Standard:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    invoke-static {v0}, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->getXScaleMap(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;)Ljava/util/HashMap;

    move-result-object v0

    .line 203
    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    return p0
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;
    .locals 4

    .line 279
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->operation:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;

    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->systemSettings:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;

    invoke-interface {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;->getPrinterIPAddress()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;->setHostIP(Ljava/lang/String;)V

    .line 281
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->selectedItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    iget-boolean p1, p1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->isLocalOption:Z

    if-eqz p1, :cond_0

    .line 282
    invoke-virtual {p0}, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->getResultLocalOptions()Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;

    move-result-object p1

    return-object p1

    .line 285
    :cond_0
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->operation:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;

    new-instance v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask$1;

    invoke-direct {v0, p0}, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask$1;-><init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;)V

    invoke-virtual {p1, v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;->getOptions(Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyOptionsParameter;)Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;

    move-result-object p1

    .line 390
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;

    invoke-direct {v0, p0}, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;-><init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;)V

    .line 391
    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->selectedItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    iput-object v1, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;->selectedItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    .line 392
    invoke-virtual {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;->success()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 393
    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    invoke-virtual {v1, p1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->setCopyOptionsResult(Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;)V

    .line 395
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;->changedItems:Ljava/util/ArrayList;

    .line 396
    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->changableParams:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 397
    invoke-static {v2, p1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->createOptionItem(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v2

    .line 398
    iget-object v3, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    invoke-virtual {v3, v2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->isChanged(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 399
    iget-object v3, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;->changedItems:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 400
    iget-object v3, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    invoke-virtual {v3, v2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->replace(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    goto :goto_0

    .line 404
    :cond_2
    iget-object p1, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;->changedItems:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    if-nez p1, :cond_5

    const/4 p1, 0x0

    .line 405
    iput-object p1, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;->changedItems:Ljava/util/ArrayList;

    goto :goto_2

    .line 408
    :cond_3
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->success:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p1, v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;->isNull(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 409
    sget-object p1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;->ErrorCommunication:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;

    iput-object p1, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;->error:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;

    goto :goto_1

    .line 411
    :cond_4
    sget-object p1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;->Error:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;

    iput-object p1, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;->error:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;

    .line 421
    :goto_1
    new-instance p1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->replacedItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    invoke-direct {p1, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;-><init>(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    iput-object p1, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;->selectedItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    .line 422
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->replacedItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    invoke-virtual {p1, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->replace(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    :cond_5
    :goto_2
    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 20
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->doInBackground([Ljava/lang/Void;)Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;

    move-result-object p1

    return-object p1
.end method

.method public getResultLocalOptions()Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;
    .locals 4

    .line 250
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;

    invoke-direct {v0, p0}, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;-><init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;)V

    .line 251
    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->selectedItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    iput-object v1, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;->selectedItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    .line 252
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;->changedItems:Ljava/util/ArrayList;

    .line 254
    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->selectedItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    invoke-virtual {v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getKey()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    move-result-object v1

    .line 255
    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->CopyMagnification:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    if-ne v1, v2, :cond_0

    .line 256
    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XScale:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v1, v2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionItemOf(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v1

    .line 257
    iget-object v2, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;->changedItems:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 258
    :cond_0
    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XScale:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    if-ne v1, v2, :cond_3

    .line 259
    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->CopyMagnification:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v1, v2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionItemOf(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v1

    .line 260
    iget-object v2, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->xscaleMap:Ljava/util/HashMap;

    iget-object v3, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->selectedItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    invoke-virtual {v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 261
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 262
    sget v3, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->XScale_Custom_Value:I

    if-eq v2, v3, :cond_1

    .line 263
    invoke-virtual {v1, v2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->selectValue(I)V

    .line 265
    :cond_1
    sget v3, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->CopyMagnificationAutofitValue:I

    if-eq v2, v3, :cond_2

    const/4 v2, 0x1

    .line 266
    iput-boolean v2, v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->enabled:Z

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    .line 268
    iput-boolean v2, v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->enabled:Z

    .line 270
    :goto_0
    iget-object v2, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;->changedItems:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    .line 272
    iput-object v1, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;->changedItems:Ljava/util/ArrayList;

    :goto_1
    return-object v0
.end method

.method protected onPostExecute(Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;)V
    .locals 3

    .line 429
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->optionChangedListener:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;

    iget-object v1, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;->selectedItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    iget-object v2, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;->changedItems:Ljava/util/ArrayList;

    iget-object p1, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;->error:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;

    invoke-interface {v0, v1, v2, p1}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;->onCopyOptionChanged(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;Ljava/util/ArrayList;Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 20
    check-cast p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;

    invoke-virtual {p0, p1}, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->onPostExecute(Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask$Result;)V

    return-void
.end method

.method public setClientID(Ljava/lang/String;)V
    .locals 0

    .line 224
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->clientID:Ljava/lang/String;

    return-void
.end method

.method public setOptionContext(Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;)V
    .locals 3

    .line 229
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    .line 236
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    new-instance v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    iget-object v2, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->selectedItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    invoke-direct {v1, v2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;-><init>(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->replace(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v0

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->replacedItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    .line 238
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->changableParams:Ljava/util/ArrayList;

    .line 239
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->changableParams:Ljava/util/ArrayList;

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_size:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 240
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->changableParams:Ljava/util/ArrayList;

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_type:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 241
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->changableParams:Ljava/util/ArrayList;

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scan_content_type:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 242
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->changableParams:Ljava/util/ArrayList;

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_source:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 243
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->changableParams:Ljava/util/ArrayList;

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_quality:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 245
    iget-object p1, p1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->copyType:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    invoke-static {p1}, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->getXScaleMap(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;)Ljava/util/HashMap;

    move-result-object p1

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->xscaleMap:Ljava/util/HashMap;

    .line 246
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->xscaleMap:Ljava/util/HashMap;

    invoke-static {p1}, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->getCopyMagnificationMap(Ljava/util/HashMap;)Ljava/util/HashMap;

    move-result-object p1

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->copyScaleMap:Ljava/util/HashMap;

    return-void
.end method

.method public setRequestConnectionTimeout(I)V
    .locals 1

    .line 219
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->operation:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;

    invoke-virtual {v0, p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;->setRequestConnectionTimeout(I)V

    return-void
.end method

.method public setSystemSettings(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;)V
    .locals 0

    .line 214
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->systemSettings:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;

    return-void
.end method

.method public start()Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyCancelRequest;
    .locals 1

    const/4 v0, 0x0

    .line 208
    new-array v0, v0, [Ljava/lang/Void;

    invoke-super {p0, v0}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    const/4 v0, 0x0

    return-object v0
.end method

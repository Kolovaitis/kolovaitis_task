.class public final enum Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;
.super Ljava/lang/Enum;
.source "ECopyComponent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/copy/Component/ecopycomponent/ECopyComponent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ECopyType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

.field public static final enum A4_2up:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

.field public static final enum A4_2up_Book:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

.field public static final enum B5_2up:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

.field public static final enum B5_2up_Book:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

.field public static final enum Borderless:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

.field public static final enum Mirror:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

.field public static final enum Photo:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

.field public static final enum Standard:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .line 132
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    const-string v1, "Standard"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;->Standard:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    .line 136
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    const-string v1, "Borderless"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;->Borderless:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    .line 140
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    const-string v1, "A4_2up"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;->A4_2up:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    .line 144
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    const-string v1, "B5_2up"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;->B5_2up:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    .line 148
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    const-string v1, "A4_2up_Book"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;->A4_2up_Book:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    .line 152
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    const-string v1, "B5_2up_Book"

    const/4 v7, 0x5

    invoke-direct {v0, v1, v7}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;->B5_2up_Book:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    .line 156
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    const-string v1, "Mirror"

    const/4 v8, 0x6

    invoke-direct {v0, v1, v8}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;->Mirror:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    .line 160
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    const-string v1, "Photo"

    const/4 v9, 0x7

    invoke-direct {v0, v1, v9}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;->Photo:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    const/16 v0, 0x8

    .line 128
    new-array v0, v0, [Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;->Standard:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;->Borderless:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    aput-object v1, v0, v3

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;->A4_2up:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    aput-object v1, v0, v4

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;->B5_2up:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    aput-object v1, v0, v5

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;->A4_2up_Book:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    aput-object v1, v0, v6

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;->B5_2up_Book:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    aput-object v1, v0, v7

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;->Mirror:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    aput-object v1, v0, v8

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;->Photo:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    aput-object v1, v0, v9

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;->$VALUES:[Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 128
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;
    .locals 1

    .line 128
    const-class v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    return-object p0
.end method

.method public static values()[Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;
    .locals 1

    .line 128
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;->$VALUES:[Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    invoke-virtual {v0}, [Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    return-object v0
.end method

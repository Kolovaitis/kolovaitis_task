.class Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$5;
.super Ljava/lang/Object;
.source "RemoteCopyPhotoTask.java"

# interfaces
.implements Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$IRemoteCopyPhotoParameter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->startCopy()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;


# direct methods
.method constructor <init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;)V
    .locals 0

    .line 319
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$5;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public client_id()Ljava/lang/String;
    .locals 1

    .line 322
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$5;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->clientID:Ljava/lang/String;

    return-object v0
.end method

.method public color_effects_type()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 2

    .line 369
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$5;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->ColorEffectsType:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionItemOf(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v0

    .line 370
    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v0

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->param:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    return-object v0
.end method

.method public copies()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 380
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$5;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->photoSetting:Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;->scanCopies:Ljava/util/ArrayList;

    return-object v0
.end method

.method public layout()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 2

    .line 327
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$5;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XBorderless:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionItemOf(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v0

    .line 328
    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v0

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->param:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    return-object v0
.end method

.method public print_media_size()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 2

    .line 339
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$5;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionItemOf(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v0

    .line 340
    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v0

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->param:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    return-object v0
.end method

.method public print_media_source()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 2

    .line 351
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$5;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSource:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionItemOf(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v0

    .line 352
    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v0

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->param:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    return-object v0
.end method

.method public print_media_type()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 2

    .line 333
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$5;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionItemOf(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v0

    .line 334
    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v0

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->param:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    return-object v0
.end method

.method public print_quality()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 2

    .line 345
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$5;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintQuality:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionItemOf(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v0

    .line 346
    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v0

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->param:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    return-object v0
.end method

.method public print_x_auto_pg()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 1

    .line 421
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->off:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    return-object v0
.end method

.method public print_x_bleed()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 2

    .line 410
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$5;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XBorderless:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionItemOf(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v0

    .line 411
    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v0

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->param:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->standard:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    if-ne v0, v1, :cond_0

    .line 412
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_null:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    return-object v0

    .line 415
    :cond_0
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$5;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintXBleed:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionItemOf(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v0

    .line 416
    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v0

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->param:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    return-object v0
.end method

.method public scan_area_height()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 400
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$5;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->photoSetting:Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;->scanAreaHeight:Ljava/util/ArrayList;

    return-object v0
.end method

.method public scan_area_resolution()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 405
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$5;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->photoSetting:Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;->scanAreaResolution:Ljava/util/ArrayList;

    return-object v0
.end method

.method public scan_area_width()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 395
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$5;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->photoSetting:Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;->scanAreaWidth:Ljava/util/ArrayList;

    return-object v0
.end method

.method public scan_area_x()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 385
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$5;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->photoSetting:Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;->scanAreaX:Ljava/util/ArrayList;

    return-object v0
.end method

.method public scan_area_y()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 390
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$5;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->photoSetting:Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;->scanAreaY:Ljava/util/ArrayList;

    return-object v0
.end method

.method public scan_count()I
    .locals 1

    .line 375
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$5;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->photoSetting:Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;

    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;->getScanCount()I

    move-result v0

    return v0
.end method

.method public x_apf()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 2

    .line 357
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$5;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XApf:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionItemOf(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v0

    .line 358
    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v0

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->param:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    return-object v0
.end method

.method public x_color_restoration()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 2

    .line 363
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$5;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XColorRestoration:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionItemOf(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v0

    .line 364
    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v0

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->param:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    return-object v0
.end method

.method public x_fit_gamma()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 426
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$5;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->photoSetting:Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;->xFitGamma:Ljava/util/ArrayList;

    return-object v0
.end method

.method public x_fit_matrix()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 431
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask$5;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoTask;->photoSetting:Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/CopyPhotoSettingHandler;->xFitMatrix:Ljava/util/ArrayList;

    return-object v0
.end method

.class Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;
.super Landroid/os/AsyncTask;
.source "RemoteCopyPhotoSetOptionTask.java"

# interfaces
.implements Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ITask;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask$Result;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask$Result;",
        ">;",
        "Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ITask;"
    }
.end annotation


# instance fields
.field changableParams:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ">;"
        }
    .end annotation
.end field

.field clientID:Ljava/lang/String;

.field operation:Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;

.field optionChangedListener:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;

.field optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

.field replacedItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

.field selectedItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

.field systemSettings:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;


# direct methods
.method public constructor <init>(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;)V
    .locals 1

    .line 34
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 35
    new-instance v0, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;

    invoke-direct {v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;-><init>()V

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;->operation:Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;

    .line 36
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;->selectedItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    .line 37
    iput-object p2, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;->optionChangedListener:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask$Result;
    .locals 5

    .line 90
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;->operation:Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;

    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;->systemSettings:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;

    invoke-interface {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;->getPrinterIPAddress()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;->setHostIP(Ljava/lang/String;)V

    .line 92
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;->selectedItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    iget-boolean p1, p1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->isLocalOption:Z

    if-eqz p1, :cond_0

    .line 93
    invoke-virtual {p0}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;->getResultLocalOptions()Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask$Result;

    move-result-object p1

    return-object p1

    .line 96
    :cond_0
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;->operation:Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;

    new-instance v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask$1;

    invoke-direct {v0, p0}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask$1;-><init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;)V

    invoke-virtual {p1, v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;->getOptions(Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$IRemoteCopyPhotoOptionsParameter;)Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto$ERemoteCopyPhotoOptionResult;

    move-result-object p1

    .line 166
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask$Result;

    invoke-direct {v0, p0}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask$Result;-><init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;)V

    .line 167
    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;->selectedItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    iput-object v1, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask$Result;->selectedItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    .line 168
    invoke-virtual {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;->success()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 169
    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    invoke-virtual {v1, p1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->setCopyOptionsResult(Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;)V

    .line 171
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask$Result;->changedItems:Ljava/util/ArrayList;

    .line 172
    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;->changableParams:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 173
    invoke-static {v2, p1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->createOptionItem(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v2

    .line 174
    iget-object v3, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    invoke-virtual {v3, v2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->isChanged(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 175
    iget-object v3, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask$Result;->changedItems:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 176
    iget-object v3, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    invoke-virtual {v3, v2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->replace(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    goto :goto_0

    .line 183
    :cond_2
    iget-object v1, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask$Result;->changedItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    .line 184
    invoke-virtual {v2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getKey()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    move-result-object v3

    sget-object v4, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XBorderless:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    if-ne v3, v4, :cond_3

    .line 185
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_x_bleed:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-static {v1, p1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->createLocalOptionItem(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object p1

    .line 186
    invoke-virtual {v2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v1

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XBorderless_Standard:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    if-ne v1, v2, :cond_4

    const/4 v1, 0x0

    .line 187
    iput-boolean v1, p1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->enabled:Z

    .line 189
    :cond_4
    iget-object v1, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask$Result;->changedItems:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 194
    :cond_5
    iget-object p1, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask$Result;->changedItems:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    if-nez p1, :cond_8

    const/4 p1, 0x0

    .line 195
    iput-object p1, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask$Result;->changedItems:Ljava/util/ArrayList;

    goto :goto_2

    .line 198
    :cond_6
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->success:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p1, v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;->isNull(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Z

    move-result p1

    if-eqz p1, :cond_7

    .line 199
    sget-object p1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;->ErrorCommunication:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;

    iput-object p1, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask$Result;->error:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;

    goto :goto_1

    .line 201
    :cond_7
    sget-object p1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;->Error:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;

    iput-object p1, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask$Result;->error:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;

    .line 211
    :goto_1
    new-instance p1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;->replacedItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    invoke-direct {p1, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;-><init>(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    iput-object p1, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask$Result;->selectedItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    .line 212
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;->replacedItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    invoke-virtual {p1, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->replace(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    :cond_8
    :goto_2
    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 18
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;->doInBackground([Ljava/lang/Void;)Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask$Result;

    move-result-object p1

    return-object p1
.end method

.method public getResultLocalOptions()Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask$Result;
    .locals 2

    .line 82
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask$Result;

    invoke-direct {v0, p0}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask$Result;-><init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;)V

    .line 83
    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;->selectedItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    iput-object v1, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask$Result;->selectedItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    .line 84
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask$Result;->changedItems:Ljava/util/ArrayList;

    return-object v0
.end method

.method protected onPostExecute(Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask$Result;)V
    .locals 3

    .line 219
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;->optionChangedListener:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;

    iget-object v1, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask$Result;->selectedItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    iget-object v2, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask$Result;->changedItems:Ljava/util/ArrayList;

    iget-object p1, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask$Result;->error:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;

    invoke-interface {v0, v1, v2, p1}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;->onCopyOptionChanged(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;Ljava/util/ArrayList;Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 18
    check-cast p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask$Result;

    invoke-virtual {p0, p1}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;->onPostExecute(Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask$Result;)V

    return-void
.end method

.method public setClientID(Ljava/lang/String;)V
    .locals 0

    .line 58
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;->clientID:Ljava/lang/String;

    return-void
.end method

.method public setOptionContext(Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;)V
    .locals 2

    .line 63
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    .line 70
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;->selectedItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    invoke-direct {v0, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;-><init>(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    invoke-virtual {p1, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->replace(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object p1

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;->replacedItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    .line 72
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;->changableParams:Ljava/util/ArrayList;

    .line 73
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;->changableParams:Ljava/util/ArrayList;

    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_borderless:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 74
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;->changableParams:Ljava/util/ArrayList;

    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_size:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 75
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;->changableParams:Ljava/util/ArrayList;

    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_type:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 76
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;->changableParams:Ljava/util/ArrayList;

    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_apf:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 77
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;->changableParams:Ljava/util/ArrayList;

    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_quality:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 78
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;->changableParams:Ljava/util/ArrayList;

    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_source:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public setRequestConnectionTimeout(I)V
    .locals 1

    .line 53
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;->operation:Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;

    invoke-virtual {v0, p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopyPhoto;->setRequestConnectionTimeout(I)V

    return-void
.end method

.method public setSystemSettings(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;)V
    .locals 0

    .line 48
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyPhotoSetOptionTask;->systemSettings:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;

    return-void
.end method

.method public start()Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyCancelRequest;
    .locals 1

    const/4 v0, 0x0

    .line 42
    new-array v0, v0, [Ljava/lang/Void;

    invoke-super {p0, v0}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    const/4 v0, 0x0

    return-object v0
.end method

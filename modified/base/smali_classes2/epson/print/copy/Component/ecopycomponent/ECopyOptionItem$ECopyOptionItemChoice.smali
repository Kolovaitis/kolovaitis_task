.class public final enum Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;
.super Ljava/lang/Enum;
.source "ECopyOptionItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ECopyOptionItemChoice"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum ColorEffectsType_Color:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum ColorEffectsType_MonochromeGrayscale:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_11x14in:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_16K:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_254x305mm:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_2L:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_8K:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_8d5x13in:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_8x10in:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_A3:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_A4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_A5:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_A6:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_B4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_B5:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_B6:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_CARD:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_CHOU3:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_CHOU4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_EnvelopeDL:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_EnvelopeNumber10:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_Executive:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_HalfLetter:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_Hivision:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_IndianLegal215x345mm:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_KAKU2:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_KAKU20:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_KG:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_L:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_Legal:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_Letter:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_MEISHI:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_MexicoOficio8d5x13d4in:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_Oficio9_8d46x12d4in:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_Postcard:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_US_B:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_YOU1:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_YOU2:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_YOU3:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSize_YOU4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSource_Bottom:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSource_Manual:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSource_Rear:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaSource_Top:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaType_BrightColorPlain:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaType_BussnessPlain:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaType_CustomMediaTypeEpson02:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaType_CustomMediaTypeEpson19:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaType_CustomMediaTypeEpson1B:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaType_CustomMediaTypeEpson2A:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaType_CustomMediaTypeEpson44:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaType_DBLMEISHI_HALFGROSSY:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaType_Envelope:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaType_HagakiAtena:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaType_Lebals:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaType_Photographic:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaType_PhotographicGlossy:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaType_PhotographicHighGloss:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaType_PhotographicMatte:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaType_PhotographicSemiGloss:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaType_Stationery:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaType_StationeryCoated:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaType_StationeryHeavyweight:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaType_StationeryInkjet:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintMediaType_StationeryLetterhead:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintQuality_Best:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintQuality_Economy:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintQuality_High:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintQuality_Normal:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintXBleed_Midium:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintXBleed_Minimum:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum PrintXBleed_Standard:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum ScanContentType_Mixed:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum ScanContentType_Photographic:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum ScanContentType_Text:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum XApf_Off:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum XApf_On:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum XBorderless_Borderless:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum XBorderless_Standard:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum XColorRestoration_Off:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum XColorRestoration_On:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum XScale_2L_to_A4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum XScale_2L_to_KG:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum XScale_2L_to_Letter:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum XScale_2L_to_Postcard:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum XScale_8x10_to_2L:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum XScale_A4_to_2L:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum XScale_A4_to_A3:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum XScale_A4_to_A5:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum XScale_A4_to_B5:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum XScale_A4_to_KG:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum XScale_A4_to_Postcard:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum XScale_A5_to_A4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum XScale_Autofit:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum XScale_B5_to_A4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum XScale_Custom:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum XScale_FullSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum XScale_KG_to_2L:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum XScale_KG_to_8x10:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum XScale_KG_to_A4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum XScale_KG_to_Letter:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum XScale_L_to_2L:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum XScale_L_to_A4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum XScale_L_to_Postcard:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum XScale_Legal_to_Letter:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum XScale_Letter_to_11x17:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum XScale_Letter_to_2L:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum XScale_Letter_to_KG:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field public static final enum XScale_Postcard_to_A4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;


# instance fields
.field key:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

.field param:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 349
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "ColorEffectsType_Color"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->ColorEffectsType:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->color:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/4 v4, 0x0

    invoke-direct {v0, v1, v4, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->ColorEffectsType_Color:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 353
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "ColorEffectsType_MonochromeGrayscale"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->ColorEffectsType:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->monochrome_grayscale:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/4 v5, 0x1

    invoke-direct {v0, v1, v5, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->ColorEffectsType_MonochromeGrayscale:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 358
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "ScanContentType_Text"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->ScanContentType:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->text:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/4 v6, 0x2

    invoke-direct {v0, v1, v6, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->ScanContentType_Text:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 362
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "ScanContentType_Mixed"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->ScanContentType:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->mixed:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/4 v7, 0x3

    invoke-direct {v0, v1, v7, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->ScanContentType_Mixed:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 366
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "ScanContentType_Photographic"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->ScanContentType:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->photographic:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/4 v8, 0x4

    invoke-direct {v0, v1, v8, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->ScanContentType_Photographic:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 371
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSource_Top"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSource:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->top:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/4 v9, 0x5

    invoke-direct {v0, v1, v9, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSource_Top:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 375
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSource_Bottom"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSource:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->bottom:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/4 v10, 0x6

    invoke-direct {v0, v1, v10, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSource_Bottom:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 379
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSource_Rear"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSource:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->rear:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/4 v11, 0x7

    invoke-direct {v0, v1, v11, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSource_Rear:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 383
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSource_Manual"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSource:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->manual:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v12, 0x8

    invoke-direct {v0, v1, v12, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSource_Manual:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 388
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaType_Stationery"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->stationery:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v13, 0x9

    invoke-direct {v0, v1, v13, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_Stationery:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 392
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaType_PhotographicHighGloss"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->photographic_high_gloss:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v14, 0xa

    invoke-direct {v0, v1, v14, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_PhotographicHighGloss:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 396
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaType_Photographic"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->photographic:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0xb

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_Photographic:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 400
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaType_PhotographicSemiGloss"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->photographic_semi_gloss:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0xc

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_PhotographicSemiGloss:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 404
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaType_PhotographicGlossy"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->photographic_glossy:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0xd

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_PhotographicGlossy:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 408
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaType_CustomMediaTypeEpson44"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_media_type_epson_44:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0xe

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_CustomMediaTypeEpson44:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 412
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaType_PhotographicMatte"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->photographic_matte:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0xf

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_PhotographicMatte:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 416
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaType_StationeryCoated"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->stationery_coated:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x10

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_StationeryCoated:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 420
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaType_CustomMediaTypeEpson2A"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_media_type_epson_2a:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x11

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_CustomMediaTypeEpson2A:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 424
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaType_StationeryInkjet"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->stationery_inkjet:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x12

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_StationeryInkjet:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 428
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaType_CustomMediaTypeEpson1B"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_media_type_epson_1b:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x13

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_CustomMediaTypeEpson1B:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 432
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaType_CustomMediaTypeEpson02"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_media_type_epson_02:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x14

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_CustomMediaTypeEpson02:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 436
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaType_CustomMediaTypeEpson19"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_media_type_epson_19:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x15

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_CustomMediaTypeEpson19:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 440
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaType_Lebals"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->labels:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x16

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_Lebals:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 444
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaType_Envelope"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->envelope:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x17

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_Envelope:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 448
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaType_DBLMEISHI_HALFGROSSY"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_media_type_epson_47:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x18

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_DBLMEISHI_HALFGROSSY:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 452
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaType_HagakiAtena"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_media_type_epson_20:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x19

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_HagakiAtena:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 457
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaType_BussnessPlain"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_media_type_epson_39:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x1a

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_BussnessPlain:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 463
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaType_StationeryHeavyweight"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->stationery_heavyweight:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x1b

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_StationeryHeavyweight:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 468
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaType_StationeryLetterhead"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->stationery_letterhead:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x1c

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_StationeryLetterhead:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 473
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaType_BrightColorPlain"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_media_type_epson_46:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x1d

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_BrightColorPlain:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 479
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_A4"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->iso_a4_210x297mm:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x1e

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_A4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 483
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_B4"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->jis_b4_257x364mm:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x1f

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_B4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 487
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_B5"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->jis_b5_182x257mm:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x20

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_B5:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 491
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_L"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->oe_photo_l_3_5x5in:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x21

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_L:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 495
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_2L"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_5x7_5x7in:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x22

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_2L:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 499
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_Postcard"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->jpn_hagaki_100x148mm:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x23

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_Postcard:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 503
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_KG"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_index_4x6_4x6in:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x24

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_KG:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 507
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_8x10in"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_govt_letter_8x10in:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x25

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_8x10in:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 511
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_Letter"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_letter_8_5x11in:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x26

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_Letter:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 515
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_Legal"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_legal_8_5x14in:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x27

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_Legal:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 519
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_A5"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->iso_a5_148x210mm:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x28

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_A5:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 523
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_254x305mm"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_4psize_254x305mm:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x29

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_254x305mm:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 527
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_A3"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->iso_a3_297x420mm:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x2a

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_A3:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 531
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_US_B"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_ledger_11x17in:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x2b

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_US_B:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 535
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_A6"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->iso_a6_105x148mm:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x2c

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_A6:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 539
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_CHOU3"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->jpn_chou3_120x235mm:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x2d

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_CHOU3:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 543
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_CHOU4"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->jpn_chou4_90x205mm:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x2e

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_CHOU4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 547
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_YOU1"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_epson_18_120x176mm:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x2f

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_YOU1:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 551
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_YOU3"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_epson_1A_98x148mm:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x30

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_YOU3:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 555
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_YOU4"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->jpn_you4_105x235mm:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x31

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_YOU4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 559
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_YOU2"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->iso_c6_114x162mm:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x32

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_YOU2:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 563
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_KAKU2"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->jpn_kaku2_240x332mm:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x33

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_KAKU2:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 567
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_KAKU20"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->iso_c4_229x324mm:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x34

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_KAKU20:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 571
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_MEISHI"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_cardsize_55x91mm:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x35

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_MEISHI:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 575
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_CARD"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_creditcardsize_54x86mm:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x36

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_CARD:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 579
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_Hivision"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_hivision_101_6x180_6mm:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x37

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_Hivision:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 584
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_EnvelopeDL"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->iso_dl_110x220mm:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x38

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_EnvelopeDL:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 589
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_B6"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->jis_b6_128x182mm:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x39

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_B6:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 594
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_Executive"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_executive_7_25x10_5in:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x3a

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_Executive:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 599
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_8d5x13in"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_foolscap_8_5x13in:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x3b

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_8d5x13in:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 604
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_11x14in"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_edp_11x14in:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x3c

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_11x14in:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 609
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_EnvelopeNumber10"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_number_10_4_125x9_5in:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x3d

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_EnvelopeNumber10:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 614
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_8K"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->om_8k_270x390mm:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x3e

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_8K:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 619
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_16K"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->om_16k_195x270mm:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x3f

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_16K:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 624
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_HalfLetter"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_invoice_5_5x8_5in:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x40

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_HalfLetter:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 630
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_IndianLegal215x345mm"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->custom_epson_indianlegal_215x345mm:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x41

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_IndianLegal215x345mm:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 636
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_MexicoOficio8d5x13d4in"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->na_oficio_8_5x13_4in:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x42

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_MexicoOficio8d5x13d4in:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 642
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintMediaSize_Oficio9_8d46x12d4in"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->om_folio_sp_215x315mm:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x43

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_Oficio9_8d46x12d4in:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 650
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintXBleed_Standard"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintXBleed:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->standard:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x44

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintXBleed_Standard:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 654
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintXBleed_Midium"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintXBleed:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->midium:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x45

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintXBleed_Midium:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 658
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintXBleed_Minimum"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintXBleed:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->minimum:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x46

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintXBleed_Minimum:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 663
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintQuality_Economy"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintQuality:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->draft:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x47

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintQuality_Economy:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 667
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintQuality_Normal"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintQuality:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->normal:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x48

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintQuality_Normal:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 671
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintQuality_High"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintQuality:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->high:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x49

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintQuality_High:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 675
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "PrintQuality_Best"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintQuality:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->best:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x4a

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintQuality_Best:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 680
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "XApf_On"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XApf:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->on:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x4b

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XApf_On:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 685
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "XApf_Off"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XApf:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->off:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x4c

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XApf_Off:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 691
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "XColorRestoration_On"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XColorRestoration:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->on:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x4d

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XColorRestoration_On:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 696
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "XColorRestoration_Off"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XColorRestoration:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->off:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x4e

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XColorRestoration_Off:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 701
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "XBorderless_Borderless"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XBorderless:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->borderless:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x4f

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XBorderless_Borderless:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 706
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "XBorderless_Standard"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XBorderless:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->standard:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x50

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XBorderless_Standard:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 711
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "XScale_FullSize"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XScale:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_fullsize:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x51

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_FullSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 715
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "XScale_Custom"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XScale:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_custom:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x52

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_Custom:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 719
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "XScale_Autofit"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XScale:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_autofit:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x53

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_Autofit:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 723
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "XScale_A4_to_Postcard"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XScale:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_a4_to_postcard:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x54

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_A4_to_Postcard:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 727
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "XScale_2L_to_Postcard"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XScale:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_2l_to_postcard:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x55

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_2L_to_Postcard:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 731
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "XScale_L_to_Postcard"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XScale:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_l_to_postcard:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x56

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_L_to_Postcard:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 735
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "XScale_L_to_2L"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XScale:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_l_to_2l:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x57

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_L_to_2L:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 739
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "XScale_2L_to_A4"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XScale:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_2l_to_a4:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x58

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_2L_to_A4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 743
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "XScale_Postcard_to_A4"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XScale:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_postcard_to_a4:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x59

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_Postcard_to_A4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 747
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "XScale_L_to_A4"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XScale:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_l_to_a4:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x5a

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_L_to_A4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 751
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "XScale_A4_to_B5"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XScale:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_a4_to_b5:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x5b

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_A4_to_B5:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 755
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "XScale_B5_to_A4"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XScale:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_b5_to_a4:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x5c

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_B5_to_A4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 759
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "XScale_Letter_to_KG"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XScale:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_letter_to_kg:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x5d

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_Letter_to_KG:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 763
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "XScale_KG_to_Letter"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XScale:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_kg_to_letter:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x5e

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_KG_to_Letter:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 767
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "XScale_Letter_to_2L"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XScale:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_letter_to_2l:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x5f

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_Letter_to_2L:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 771
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "XScale_2L_to_Letter"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XScale:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_2l_to_letter:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x60

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_2L_to_Letter:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 775
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "XScale_KG_to_A4"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XScale:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_kg_to_a4:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x61

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_KG_to_A4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 779
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "XScale_A4_to_KG"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XScale:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_a4_to_kg:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x62

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_A4_to_KG:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 783
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "XScale_A4_to_2L"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XScale:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_a4_to_2l:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x63

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_A4_to_2L:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 787
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "XScale_KG_to_8x10"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XScale:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_kg_to_8x10:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x64

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_KG_to_8x10:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 791
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "XScale_8x10_to_2L"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XScale:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_8x10_to_2l:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x65

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_8x10_to_2L:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 795
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "XScale_Legal_to_Letter"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XScale:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_legal_to_letter:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x66

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_Legal_to_Letter:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 799
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "XScale_2L_to_KG"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XScale:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_2l_to_kg:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x67

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_2L_to_KG:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 803
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "XScale_KG_to_2L"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XScale:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_kg_to_2l:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x68

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_KG_to_2L:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 807
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "XScale_A5_to_A4"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XScale:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_a5_to_a4:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x69

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_A5_to_A4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 811
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "XScale_A4_to_A5"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XScale:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_a4_to_a5:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x6a

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_A4_to_A5:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 815
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "XScale_A4_to_A3"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XScale:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_a4_to_a3:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x6b

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_A4_to_A3:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 819
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const-string v1, "XScale_Letter_to_11x17"

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XScale:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    sget-object v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale_letter_to_11x17:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v15, 0x6c

    invoke-direct {v0, v1, v15, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;-><init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_Letter_to_11x17:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v0, 0x6d

    .line 345
    new-array v0, v0, [Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->ColorEffectsType_Color:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    aput-object v1, v0, v4

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->ColorEffectsType_MonochromeGrayscale:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    aput-object v1, v0, v5

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->ScanContentType_Text:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    aput-object v1, v0, v6

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->ScanContentType_Mixed:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    aput-object v1, v0, v7

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->ScanContentType_Photographic:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    aput-object v1, v0, v8

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSource_Top:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    aput-object v1, v0, v9

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSource_Bottom:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    aput-object v1, v0, v10

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSource_Rear:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    aput-object v1, v0, v11

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSource_Manual:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    aput-object v1, v0, v12

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_Stationery:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    aput-object v1, v0, v13

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_PhotographicHighGloss:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    aput-object v1, v0, v14

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_Photographic:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_PhotographicSemiGloss:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_PhotographicGlossy:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_CustomMediaTypeEpson44:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_PhotographicMatte:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_StationeryCoated:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_CustomMediaTypeEpson2A:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_StationeryInkjet:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_CustomMediaTypeEpson1B:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_CustomMediaTypeEpson02:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_CustomMediaTypeEpson19:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_Lebals:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_Envelope:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_DBLMEISHI_HALFGROSSY:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_HagakiAtena:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_BussnessPlain:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_StationeryHeavyweight:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_StationeryLetterhead:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_BrightColorPlain:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_A4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_B4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_B5:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_L:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_2L:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_Postcard:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x23

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_KG:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x24

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_8x10in:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x25

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_Letter:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x26

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_Legal:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x27

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_A5:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x28

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_254x305mm:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x29

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_A3:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x2a

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_US_B:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x2b

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_A6:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x2c

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_CHOU3:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x2d

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_CHOU4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x2e

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_YOU1:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x2f

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_YOU3:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x30

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_YOU4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x31

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_YOU2:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x32

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_KAKU2:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x33

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_KAKU20:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x34

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_MEISHI:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x35

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_CARD:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x36

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_Hivision:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x37

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_EnvelopeDL:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x38

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_B6:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x39

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_Executive:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x3a

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_8d5x13in:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x3b

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_11x14in:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x3c

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_EnvelopeNumber10:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x3d

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_8K:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x3e

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_16K:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x3f

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_HalfLetter:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x40

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_IndianLegal215x345mm:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x41

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_MexicoOficio8d5x13d4in:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x42

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_Oficio9_8d46x12d4in:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x43

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintXBleed_Standard:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x44

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintXBleed_Midium:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x45

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintXBleed_Minimum:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x46

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintQuality_Economy:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x47

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintQuality_Normal:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x48

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintQuality_High:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x49

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintQuality_Best:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x4a

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XApf_On:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x4b

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XApf_Off:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x4c

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XColorRestoration_On:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x4d

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XColorRestoration_Off:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x4e

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XBorderless_Borderless:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x4f

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XBorderless_Standard:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x50

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_FullSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x51

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_Custom:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x52

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_Autofit:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x53

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_A4_to_Postcard:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x54

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_2L_to_Postcard:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x55

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_L_to_Postcard:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x56

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_L_to_2L:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x57

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_2L_to_A4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x58

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_Postcard_to_A4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x59

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_L_to_A4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x5a

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_A4_to_B5:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x5b

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_B5_to_A4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x5c

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_Letter_to_KG:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x5d

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_KG_to_Letter:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x5e

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_Letter_to_2L:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x5f

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_2L_to_Letter:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x60

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_KG_to_A4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x61

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_A4_to_KG:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x62

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_A4_to_2L:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x63

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_KG_to_8x10:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x64

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_8x10_to_2L:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x65

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_Legal_to_Letter:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x66

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_2L_to_KG:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x67

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_KG_to_2L:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x68

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_A5_to_A4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x69

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_A4_to_A5:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x6a

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_A4_to_A3:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x6b

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_Letter_to_11x17:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 v2, 0x6c

    aput-object v1, v0, v2

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->$VALUES:[Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;",
            "Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ")V"
        }
    .end annotation

    .line 824
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 825
    iput-object p4, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->param:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    return-void
.end method

.method public static valueOf(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;
    .locals 1

    .line 833
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$2;->$SwitchMap$epson$print$copy$Component$ecopycomponent$ECopyOptionItem$ECopyOptionItemKey:[I

    invoke-virtual {p0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->ordinal()I

    move-result p0

    aget p0, v0, p0

    packed-switch p0, :pswitch_data_0

    goto/16 :goto_9

    .line 835
    :pswitch_0
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$2;->$SwitchMap$epson$print$copy$Component$eremoteoperation$ERemoteOperation$ERemoteParam:[I

    invoke-virtual {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->ordinal()I

    move-result v0

    aget p0, p0, v0

    packed-switch p0, :pswitch_data_1

    goto :goto_0

    .line 837
    :pswitch_1
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->ColorEffectsType_MonochromeGrayscale:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 836
    :pswitch_2
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->ColorEffectsType_Color:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 841
    :goto_0
    :pswitch_3
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$2;->$SwitchMap$epson$print$copy$Component$eremoteoperation$ERemoteOperation$ERemoteParam:[I

    invoke-virtual {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->ordinal()I

    move-result v0

    aget p0, p0, v0

    packed-switch p0, :pswitch_data_2

    goto :goto_1

    .line 844
    :pswitch_4
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->ScanContentType_Photographic:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 843
    :pswitch_5
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->ScanContentType_Mixed:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 842
    :pswitch_6
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->ScanContentType_Text:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 848
    :goto_1
    :pswitch_7
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$2;->$SwitchMap$epson$print$copy$Component$eremoteoperation$ERemoteOperation$ERemoteParam:[I

    invoke-virtual {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->ordinal()I

    move-result v0

    aget p0, p0, v0

    packed-switch p0, :pswitch_data_3

    goto :goto_2

    .line 869
    :pswitch_8
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_BrightColorPlain:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 868
    :pswitch_9
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_StationeryLetterhead:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 867
    :pswitch_a
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_StationeryHeavyweight:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 866
    :pswitch_b
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_BussnessPlain:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 865
    :pswitch_c
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_HagakiAtena:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 864
    :pswitch_d
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_DBLMEISHI_HALFGROSSY:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 863
    :pswitch_e
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_Envelope:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 862
    :pswitch_f
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_Lebals:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 861
    :pswitch_10
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_CustomMediaTypeEpson19:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 860
    :pswitch_11
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_CustomMediaTypeEpson02:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 859
    :pswitch_12
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_CustomMediaTypeEpson1B:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 858
    :pswitch_13
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_StationeryInkjet:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 857
    :pswitch_14
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_CustomMediaTypeEpson2A:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 856
    :pswitch_15
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_StationeryCoated:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 855
    :pswitch_16
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_PhotographicMatte:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 854
    :pswitch_17
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_CustomMediaTypeEpson44:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 853
    :pswitch_18
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_PhotographicGlossy:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 852
    :pswitch_19
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_PhotographicSemiGloss:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 850
    :pswitch_1a
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_PhotographicHighGloss:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 849
    :pswitch_1b
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_Stationery:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 851
    :pswitch_1c
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_Photographic:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 873
    :goto_2
    :pswitch_1d
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$2;->$SwitchMap$epson$print$copy$Component$eremoteoperation$ERemoteOperation$ERemoteParam:[I

    invoke-virtual {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->ordinal()I

    move-result v0

    aget p0, p0, v0

    packed-switch p0, :pswitch_data_4

    goto :goto_3

    .line 911
    :pswitch_1e
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_Oficio9_8d46x12d4in:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 910
    :pswitch_1f
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_MexicoOficio8d5x13d4in:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 909
    :pswitch_20
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_IndianLegal215x345mm:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 908
    :pswitch_21
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_HalfLetter:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 907
    :pswitch_22
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_16K:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 906
    :pswitch_23
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_8K:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 905
    :pswitch_24
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_EnvelopeNumber10:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 904
    :pswitch_25
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_11x14in:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 903
    :pswitch_26
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_8d5x13in:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 902
    :pswitch_27
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_Executive:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 901
    :pswitch_28
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_B6:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 900
    :pswitch_29
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_EnvelopeDL:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 899
    :pswitch_2a
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_Hivision:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 898
    :pswitch_2b
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_CARD:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 897
    :pswitch_2c
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_MEISHI:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 896
    :pswitch_2d
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_KAKU20:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 895
    :pswitch_2e
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_KAKU2:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 894
    :pswitch_2f
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_YOU2:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 893
    :pswitch_30
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_YOU4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 892
    :pswitch_31
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_YOU3:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 891
    :pswitch_32
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_YOU1:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 890
    :pswitch_33
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_CHOU4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 889
    :pswitch_34
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_CHOU3:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 888
    :pswitch_35
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_A6:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 887
    :pswitch_36
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_US_B:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 886
    :pswitch_37
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_A3:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 885
    :pswitch_38
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_254x305mm:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 884
    :pswitch_39
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_A5:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 883
    :pswitch_3a
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_Legal:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 882
    :pswitch_3b
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_Letter:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 881
    :pswitch_3c
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_8x10in:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 880
    :pswitch_3d
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_KG:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 879
    :pswitch_3e
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_Postcard:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 878
    :pswitch_3f
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_2L:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 877
    :pswitch_40
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_L:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 876
    :pswitch_41
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_B5:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 875
    :pswitch_42
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_B4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 874
    :pswitch_43
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_A4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 915
    :goto_3
    :pswitch_44
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$2;->$SwitchMap$epson$print$copy$Component$eremoteoperation$ERemoteOperation$ERemoteParam:[I

    invoke-virtual {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->ordinal()I

    move-result v0

    aget p0, p0, v0

    packed-switch p0, :pswitch_data_5

    goto :goto_4

    .line 919
    :pswitch_45
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSource_Manual:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 918
    :pswitch_46
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSource_Rear:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 917
    :pswitch_47
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSource_Bottom:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 916
    :pswitch_48
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSource_Top:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 923
    :goto_4
    :pswitch_49
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$2;->$SwitchMap$epson$print$copy$Component$eremoteoperation$ERemoteOperation$ERemoteParam:[I

    invoke-virtual {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->ordinal()I

    move-result v0

    aget p0, p0, v0

    packed-switch p0, :pswitch_data_6

    goto :goto_5

    .line 927
    :pswitch_4a
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintQuality_Best:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 926
    :pswitch_4b
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintQuality_High:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 925
    :pswitch_4c
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintQuality_Normal:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 924
    :pswitch_4d
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintQuality_Economy:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 931
    :goto_5
    :pswitch_4e
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$2;->$SwitchMap$epson$print$copy$Component$eremoteoperation$ERemoteOperation$ERemoteParam:[I

    invoke-virtual {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->ordinal()I

    move-result v0

    aget p0, p0, v0

    packed-switch p0, :pswitch_data_7

    goto :goto_6

    .line 934
    :pswitch_4f
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintXBleed_Minimum:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 933
    :pswitch_50
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintXBleed_Midium:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 932
    :pswitch_51
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintXBleed_Standard:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 938
    :goto_6
    :pswitch_52
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$2;->$SwitchMap$epson$print$copy$Component$eremoteoperation$ERemoteOperation$ERemoteParam:[I

    invoke-virtual {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->ordinal()I

    move-result v0

    aget p0, p0, v0

    packed-switch p0, :pswitch_data_8

    goto :goto_7

    .line 940
    :pswitch_53
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XApf_Off:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 939
    :pswitch_54
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XApf_On:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 944
    :goto_7
    :pswitch_55
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$2;->$SwitchMap$epson$print$copy$Component$eremoteoperation$ERemoteOperation$ERemoteParam:[I

    invoke-virtual {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->ordinal()I

    move-result v0

    aget p0, p0, v0

    packed-switch p0, :pswitch_data_9

    goto :goto_8

    .line 946
    :pswitch_56
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XColorRestoration_Off:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 945
    :pswitch_57
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XColorRestoration_On:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 950
    :goto_8
    :pswitch_58
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$2;->$SwitchMap$epson$print$copy$Component$eremoteoperation$ERemoteOperation$ERemoteParam:[I

    invoke-virtual {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->ordinal()I

    move-result v0

    aget p0, p0, v0

    const/16 v0, 0x48

    if-eq p0, v0, :cond_1

    const/16 v0, 0x4d

    if-eq p0, v0, :cond_0

    .line 956
    :pswitch_59
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$2;->$SwitchMap$epson$print$copy$Component$eremoteoperation$ERemoteOperation$ERemoteParam:[I

    invoke-virtual {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->ordinal()I

    move-result p1

    aget p0, p0, p1

    packed-switch p0, :pswitch_data_a

    goto :goto_9

    .line 984
    :pswitch_5a
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_Letter_to_11x17:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 983
    :pswitch_5b
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_A4_to_A3:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 982
    :pswitch_5c
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_A4_to_A5:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 981
    :pswitch_5d
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_A5_to_A4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 980
    :pswitch_5e
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_KG_to_2L:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 979
    :pswitch_5f
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_2L_to_KG:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 978
    :pswitch_60
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_Legal_to_Letter:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 977
    :pswitch_61
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_8x10_to_2L:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 976
    :pswitch_62
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_KG_to_8x10:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 975
    :pswitch_63
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_A4_to_KG:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 974
    :pswitch_64
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_KG_to_A4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 973
    :pswitch_65
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_2L_to_Letter:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 972
    :pswitch_66
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_Letter_to_2L:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 971
    :pswitch_67
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_KG_to_Letter:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 970
    :pswitch_68
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_Letter_to_KG:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 969
    :pswitch_69
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_B5_to_A4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 968
    :pswitch_6a
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_A4_to_B5:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 967
    :pswitch_6b
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_L_to_A4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 966
    :pswitch_6c
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_Postcard_to_A4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 965
    :pswitch_6d
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_A4_to_2L:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 964
    :pswitch_6e
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_2L_to_A4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 963
    :pswitch_6f
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_L_to_2L:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 962
    :pswitch_70
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_L_to_Postcard:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 961
    :pswitch_71
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_2L_to_Postcard:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 960
    :pswitch_72
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_A4_to_Postcard:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 959
    :pswitch_73
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_Autofit:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 958
    :pswitch_74
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_Custom:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 957
    :pswitch_75
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_FullSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 951
    :cond_0
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XBorderless_Borderless:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    .line 952
    :cond_1
    sget-object p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XBorderless_Standard:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0

    :goto_9
    const/4 p0, 0x0

    return-object p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_7
        :pswitch_1d
        :pswitch_44
        :pswitch_49
        :pswitch_4e
        :pswitch_52
        :pswitch_55
        :pswitch_58
        :pswitch_59
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x3
        :pswitch_6
        :pswitch_5
        :pswitch_4
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x5
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x1a
        :pswitch_43
        :pswitch_42
        :pswitch_41
        :pswitch_40
        :pswitch_3f
        :pswitch_3e
        :pswitch_3d
        :pswitch_3c
        :pswitch_3b
        :pswitch_3a
        :pswitch_39
        :pswitch_38
        :pswitch_37
        :pswitch_36
        :pswitch_35
        :pswitch_34
        :pswitch_33
        :pswitch_32
        :pswitch_31
        :pswitch_30
        :pswitch_2f
        :pswitch_2e
        :pswitch_2d
        :pswitch_2c
        :pswitch_2b
        :pswitch_2a
        :pswitch_29
        :pswitch_28
        :pswitch_27
        :pswitch_26
        :pswitch_25
        :pswitch_24
        :pswitch_23
        :pswitch_22
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x40
        :pswitch_48
        :pswitch_47
        :pswitch_46
        :pswitch_45
    .end packed-switch

    :pswitch_data_6
    .packed-switch 0x44
        :pswitch_4d
        :pswitch_4c
        :pswitch_4b
        :pswitch_4a
    .end packed-switch

    :pswitch_data_7
    .packed-switch 0x48
        :pswitch_51
        :pswitch_50
        :pswitch_4f
    .end packed-switch

    :pswitch_data_8
    .packed-switch 0x4b
        :pswitch_54
        :pswitch_53
    .end packed-switch

    :pswitch_data_9
    .packed-switch 0x4b
        :pswitch_57
        :pswitch_56
    .end packed-switch

    :pswitch_data_a
    .packed-switch 0x4e
        :pswitch_75
        :pswitch_74
        :pswitch_73
        :pswitch_72
        :pswitch_71
        :pswitch_70
        :pswitch_6f
        :pswitch_6e
        :pswitch_6d
        :pswitch_6c
        :pswitch_6b
        :pswitch_6a
        :pswitch_69
        :pswitch_68
        :pswitch_67
        :pswitch_66
        :pswitch_65
        :pswitch_64
        :pswitch_63
        :pswitch_62
        :pswitch_61
        :pswitch_60
        :pswitch_5f
        :pswitch_5e
        :pswitch_5d
        :pswitch_5c
        :pswitch_5b
        :pswitch_5a
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;
    .locals 1

    .line 345
    const-class v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object p0
.end method

.method public static values()[Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;
    .locals 1

    .line 345
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->$VALUES:[Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    invoke-virtual {v0}, [Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-object v0
.end method


# virtual methods
.method public getParam()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 1

    .line 829
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->param:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    return-object v0
.end method

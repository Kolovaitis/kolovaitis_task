.class Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask;
.super Landroid/os/AsyncTask;
.source "RemoteDeviceTask.java"

# interfaces
.implements Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ITask;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask$Result;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask$Result;",
        ">;",
        "Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ITask;"
    }
.end annotation


# instance fields
.field clientID:Ljava/lang/String;

.field operation:Lepson/print/copy/Component/eremoteoperation/ERemoteDevice;

.field optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

.field statusListener:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener;

.field systemSettings:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;


# direct methods
.method public constructor <init>(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener;)V
    .locals 1

    .line 39
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 40
    new-instance v0, Lepson/print/copy/Component/eremoteoperation/ERemoteDevice;

    invoke-direct {v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteDevice;-><init>()V

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask;->operation:Lepson/print/copy/Component/eremoteoperation/ERemoteDevice;

    .line 41
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask;->statusListener:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener;

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask$Result;
    .locals 2

    .line 169
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask;->operation:Lepson/print/copy/Component/eremoteoperation/ERemoteDevice;

    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask;->systemSettings:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;

    invoke-interface {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;->getPrinterIPAddress()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteDevice;->setHostIP(Ljava/lang/String;)V

    .line 171
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask;->operation:Lepson/print/copy/Component/eremoteoperation/ERemoteDevice;

    new-instance v0, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask$2;

    invoke-direct {v0, p0}, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask$2;-><init>(Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask;)V

    invoke-virtual {p1, v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteDevice;->getStatus(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$IRemoteStatusParameter;)Lepson/print/copy/Component/eremoteoperation/ERemoteDevice$ERemoteDeviceStatusResult;

    move-result-object p1

    .line 188
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask$Result;

    invoke-direct {v0, p0}, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask$Result;-><init>(Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask;)V

    .line 189
    invoke-virtual {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteDevice$ERemoteDeviceStatusResult;->success()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 190
    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$Connection;->Succeed:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$Connection;

    iput-object v1, v0, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask$Result;->connetion:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$Connection;

    .line 191
    invoke-virtual {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteDevice$ERemoteDeviceStatusResult;->printer_state()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v1

    invoke-virtual {p0, v1}, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask;->getDeviceState(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;

    move-result-object v1

    iput-object v1, v0, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask$Result;->printer:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;

    .line 192
    invoke-virtual {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteDevice$ERemoteDeviceStatusResult;->scanner_state()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v1

    invoke-virtual {p0, v1}, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask;->getDeviceState(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;

    move-result-object v1

    iput-object v1, v0, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask$Result;->scanner:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;

    .line 193
    invoke-virtual {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteDevice$ERemoteDeviceStatusResult;->printer_state_reasons()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p0, v1}, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask;->getPrinterReasons(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, v0, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask$Result;->printerReasons:Ljava/util/ArrayList;

    .line 194
    invoke-virtual {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteDevice$ERemoteDeviceStatusResult;->scanner_state_reasons()Ljava/util/ArrayList;

    move-result-object p1

    invoke-virtual {p0, p1}, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask;->getScannerReasons(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object p1

    iput-object p1, v0, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask$Result;->scannerReasons:Ljava/util/ArrayList;

    .line 195
    invoke-virtual {p0}, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask;->getJobTokens()Ljava/util/ArrayList;

    move-result-object p1

    iput-object p1, v0, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask$Result;->jobTokens:Ljava/util/ArrayList;

    goto :goto_0

    .line 197
    :cond_0
    sget-object p1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$Connection;->Failed:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$Connection;

    iput-object p1, v0, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask$Result;->connetion:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$Connection;

    .line 198
    sget-object p1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;->Stopped:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;

    iput-object p1, v0, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask$Result;->printer:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;

    .line 199
    sget-object p1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;->Stopped:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;

    iput-object p1, v0, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask$Result;->scanner:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;

    .line 200
    invoke-virtual {p0}, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask;->getPrinterErrorReasons()Ljava/util/ArrayList;

    move-result-object p1

    iput-object p1, v0, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask$Result;->printerReasons:Ljava/util/ArrayList;

    .line 201
    invoke-virtual {p0}, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask;->getScannerErrorReasons()Ljava/util/ArrayList;

    move-result-object p1

    iput-object p1, v0, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask$Result;->scannerReasons:Ljava/util/ArrayList;

    .line 202
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, v0, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask$Result;->jobTokens:Ljava/util/ArrayList;

    :goto_0
    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 23
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask;->doInBackground([Ljava/lang/Void;)Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask$Result;

    move-result-object p1

    return-object p1
.end method

.method getDeviceState(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;
    .locals 2

    .line 71
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;->Stopped:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;

    .line 72
    sget-object v1, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask$3;->$SwitchMap$epson$print$copy$Component$eremoteoperation$ERemoteOperation$ERemoteParam:[I

    invoke-virtual {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->ordinal()I

    move-result p1

    aget p1, v1, p1

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 75
    :pswitch_0
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;->Stopped:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;

    goto :goto_0

    .line 74
    :pswitch_1
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;->Processing:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;

    goto :goto_0

    .line 73
    :pswitch_2
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;->Idle:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;

    :goto_0
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method getJobTokens()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 140
    new-instance v0, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;

    invoke-direct {v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;-><init>()V

    .line 141
    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask;->operation:Lepson/print/copy/Component/eremoteoperation/ERemoteDevice;

    invoke-virtual {v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteDevice;->getHostIP()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;->setHostIP(Ljava/lang/String;)V

    .line 142
    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask;->operation:Lepson/print/copy/Component/eremoteoperation/ERemoteDevice;

    invoke-virtual {v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteDevice;->getRequestConnectionTimeout()I

    move-result v1

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;->setRequestConnectionTimeout(I)V

    .line 143
    new-instance v1, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask$1;

    invoke-direct {v1, p0}, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask$1;-><init>(Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask;)V

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;->getStatus(Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$IRemoteCopyStatusParameter;)Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;

    move-result-object v0

    .line 161
    invoke-virtual {v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->success()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 162
    invoke-virtual {v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyStatusResult;->job_tokens()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0

    .line 164
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method getPrinterErrorReasons()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;",
            ">;"
        }
    .end annotation

    .line 82
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 83
    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;->OtherError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    .line 84
    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->other_error:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    iget-object v2, v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->string:Ljava/lang/String;

    iput-object v2, v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;->string:Ljava/lang/String;

    .line 85
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method getPrinterReasons(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ">;)",
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;",
            ">;"
        }
    .end annotation

    .line 90
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 91
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 92
    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;->None:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    .line 93
    sget-object v2, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask$3;->$SwitchMap$epson$print$copy$Component$eremoteoperation$ERemoteOperation$ERemoteParam:[I

    invoke-virtual {v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 105
    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;->OtherError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    goto :goto_1

    .line 104
    :pswitch_0
    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;->CoverOpen:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    goto :goto_1

    .line 103
    :pswitch_1
    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;->MarkerSupplyLowWarning:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    goto :goto_1

    .line 102
    :pswitch_2
    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;->OtherError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    goto :goto_1

    .line 101
    :pswitch_3
    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;->OutputAreaFullError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    goto :goto_1

    .line 100
    :pswitch_4
    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;->CoverOpenError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    goto :goto_1

    .line 99
    :pswitch_5
    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;->InputTrayMissingError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    goto :goto_1

    .line 98
    :pswitch_6
    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;->MediaEmptyError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    goto :goto_1

    .line 97
    :pswitch_7
    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;->MediaJamError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    goto :goto_1

    .line 96
    :pswitch_8
    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;->MarkerWasteFullError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    goto :goto_1

    .line 95
    :pswitch_9
    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;->MarkerSupplyEmptyError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    goto :goto_1

    .line 94
    :pswitch_a
    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;->None:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;

    .line 107
    :goto_1
    iget-object v1, v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->string:Ljava/lang/String;

    iput-object v1, v2, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$PrinterStateReason;->string:Ljava/lang/String;

    .line 108
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method getScannerErrorReasons()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;",
            ">;"
        }
    .end annotation

    .line 114
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 115
    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;->OtherError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;

    .line 116
    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->other_error:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    iget-object v2, v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->string:Ljava/lang/String;

    iput-object v2, v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;->string:Ljava/lang/String;

    .line 117
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method getScannerReasons(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ">;)",
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;",
            ">;"
        }
    .end annotation

    .line 122
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 123
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 124
    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;->None:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;

    .line 125
    sget-object v2, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask$3;->$SwitchMap$epson$print$copy$Component$eremoteoperation$ERemoteOperation$ERemoteParam:[I

    invoke-virtual {v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->ordinal()I

    move-result v3

    aget v2, v2, v3

    const/4 v3, 0x4

    if-eq v2, v3, :cond_2

    const/16 v3, 0xc

    if-eq v2, v3, :cond_1

    const/16 v3, 0xf

    if-eq v2, v3, :cond_0

    packed-switch v2, :pswitch_data_0

    .line 131
    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;->OtherError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;

    goto :goto_1

    .line 127
    :pswitch_0
    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;->MediaEmptyError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;

    goto :goto_1

    .line 128
    :pswitch_1
    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;->MediaJamError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;

    goto :goto_1

    .line 129
    :cond_0
    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;->MediaSizeMissmatchError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;

    goto :goto_1

    .line 130
    :cond_1
    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;->OtherError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;

    goto :goto_1

    .line 126
    :cond_2
    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;->None:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;

    .line 133
    :goto_1
    iget-object v1, v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->string:Ljava/lang/String;

    iput-object v1, v2, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$ScannerStateReason;->string:Ljava/lang/String;

    .line 134
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected onPostExecute(Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask$Result;)V
    .locals 7

    .line 209
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask;->statusListener:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener;

    iget-object v1, p1, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask$Result;->connetion:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$Connection;

    iget-object v2, p1, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask$Result;->printer:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;

    iget-object v3, p1, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask$Result;->scanner:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;

    iget-object v4, p1, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask$Result;->printerReasons:Ljava/util/ArrayList;

    iget-object v5, p1, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask$Result;->scannerReasons:Ljava/util/ArrayList;

    iget-object v6, p1, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask$Result;->jobTokens:Ljava/util/ArrayList;

    invoke-interface/range {v0 .. v6}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener;->onDeviceStatus(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$Connection;Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$DeviceState;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 23
    check-cast p1, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask$Result;

    invoke-virtual {p0, p1}, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask;->onPostExecute(Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask$Result;)V

    return-void
.end method

.method public setClientID(Ljava/lang/String;)V
    .locals 0

    .line 62
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask;->clientID:Ljava/lang/String;

    return-void
.end method

.method public setOptionContext(Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;)V
    .locals 0

    .line 67
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    return-void
.end method

.method public setRequestConnectionTimeout(I)V
    .locals 1

    .line 57
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask;->operation:Lepson/print/copy/Component/eremoteoperation/ERemoteDevice;

    invoke-virtual {v0, p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteDevice;->setRequestConnectionTimeout(I)V

    return-void
.end method

.method public setSystemSettings(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;)V
    .locals 0

    .line 52
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteDeviceTask;->systemSettings:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;

    return-void
.end method

.method public start()Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyCancelRequest;
    .locals 1

    const/4 v0, 0x0

    .line 46
    new-array v0, v0, [Ljava/lang/Void;

    invoke-super {p0, v0}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    const/4 v0, 0x0

    return-object v0
.end method

.class public final enum Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$Connection;
.super Ljava/lang/Enum;
.source "ECopyComponent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Connection"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$Connection;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$Connection;

.field public static final enum Failed:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$Connection;

.field public static final enum Succeed:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$Connection;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 441
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$Connection;

    const-string v1, "Succeed"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$Connection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$Connection;->Succeed:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$Connection;

    .line 445
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$Connection;

    const-string v1, "Failed"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$Connection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$Connection;->Failed:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$Connection;

    const/4 v0, 0x2

    .line 437
    new-array v0, v0, [Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$Connection;

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$Connection;->Succeed:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$Connection;

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$Connection;->Failed:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$Connection;

    aput-object v1, v0, v3

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$Connection;->$VALUES:[Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$Connection;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 437
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$Connection;
    .locals 1

    .line 437
    const-class v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$Connection;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$Connection;

    return-object p0
.end method

.method public static values()[Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$Connection;
    .locals 1

    .line 437
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$Connection;->$VALUES:[Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$Connection;

    invoke-virtual {v0}, [Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$Connection;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyDeviceStatusListener$Connection;

    return-object v0
.end method

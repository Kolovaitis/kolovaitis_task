.class public Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;
.super Ljava/lang/Object;
.source "ECopyOptionContext.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field copyOptionsResult:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;

.field copyType:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

.field optionItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext$1;

    invoke-direct {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext$1;-><init>()V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->optionItems:Ljava/util/ArrayList;

    const/4 v0, 0x0

    .line 70
    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->copyType:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->optionItems:Ljava/util/ArrayList;

    .line 46
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;->valueOf(Ljava/lang/String;)Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    move-result-object v0

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->copyType:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    .line 47
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    .line 48
    const-class v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    .line 49
    iget-object v3, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->optionItems:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 53
    :cond_0
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 54
    new-instance p1, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;

    new-instance v1, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;

    invoke-direct {v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;-><init>()V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {p1, v1, v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;-><init>(Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;Lorg/json/JSONObject;)V

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->copyOptionsResult:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    .line 56
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    :goto_1
    return-void
.end method

.method constructor <init>(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;)V
    .locals 1

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->optionItems:Ljava/util/ArrayList;

    .line 74
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->copyType:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    return-void
.end method

.method constructor <init>(Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;)V
    .locals 4

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->optionItems:Ljava/util/ArrayList;

    const/4 v0, 0x0

    .line 78
    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->copyType:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    if-eqz p1, :cond_1

    .line 80
    iget-object v0, p1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->copyType:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->copyType:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    .line 81
    iget-object v0, p1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->optionItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    .line 82
    iget-object v2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->optionItems:Ljava/util/ArrayList;

    new-instance v3, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    invoke-direct {v3, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;-><init>(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 84
    :cond_0
    iget-object p1, p1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->copyOptionsResult:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->copyOptionsResult:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;

    :cond_1
    return-void
.end method


# virtual methods
.method add(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V
    .locals 1

    .line 108
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->optionItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method getCopyOptionItemOf(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;
    .locals 3

    .line 98
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->optionItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    .line 99
    iget-object v2, v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->key:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    if-ne v2, p1, :cond_0

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    return-object v1
.end method

.method getCopyOptionItems()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;",
            ">;"
        }
    .end annotation

    .line 93
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->optionItems:Ljava/util/ArrayList;

    return-object v0
.end method

.method getCopyOptionsResult()Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;
    .locals 1

    .line 147
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->copyOptionsResult:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;

    return-object v0
.end method

.method getCopyType()Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;
    .locals 1

    .line 89
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->copyType:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    return-object v0
.end method

.method isChanged(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)Z
    .locals 4

    .line 131
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->optionItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    .line 132
    iget-object v2, v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->key:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    iget-object v3, p1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->key:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    if-ne v2, v3, :cond_0

    .line 133
    invoke-virtual {v1, p1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method isNull()Z
    .locals 1

    .line 66
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->copyType:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public replace(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;
    .locals 4

    .line 119
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->optionItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    .line 120
    iget-object v2, v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->key:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    iget-object v3, p1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->key:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    if-ne v2, v3, :cond_0

    .line 121
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->optionItems:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    goto :goto_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_1
    return-object p1
.end method

.method setCopyOptionsResult(Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;)V
    .locals 0

    .line 143
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->copyOptionsResult:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .line 37
    iget-object p2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->copyType:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    invoke-virtual {p2}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 38
    iget-object p2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->optionItems:Ljava/util/ArrayList;

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 39
    iget-object p2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->optionItems:Ljava/util/ArrayList;

    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    const/4 v1, 0x0

    .line 40
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    goto :goto_0

    .line 42
    :cond_0
    iget-object p2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->copyOptionsResult:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;

    invoke-virtual {p2}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method

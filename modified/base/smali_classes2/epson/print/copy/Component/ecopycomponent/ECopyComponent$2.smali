.class Lepson/print/copy/Component/ecopycomponent/ECopyComponent$2;
.super Ljava/lang/Object;
.source "ECopyComponent.java"

# interfaces
.implements Lepson/print/copy/Component/ecopycomponent/ECopyPreview$ICopyPreviewCommitEditedOptionContext;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->getComposedCopyPreview(Landroid/content/Context;IILepson/print/copy/Component/ecopycomponent/ECopyPreview$ECopyPreviewInvalidateListener;)Lepson/print/copy/Component/ecopycomponent/ECopyPreview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/copy/Component/ecopycomponent/ECopyComponent;


# direct methods
.method constructor <init>(Lepson/print/copy/Component/ecopycomponent/ECopyComponent;)V
    .locals 0

    .line 664
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$2;->this$0:Lepson/print/copy/Component/ecopycomponent/ECopyComponent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public commit(Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;)V
    .locals 3

    .line 671
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->CopyMagnification:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {p1, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionItemOf(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v0

    .line 672
    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XScale:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {p1, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionItemOf(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v1

    .line 673
    iget-object v2, p1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->copyType:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedValue()I

    move-result v0

    invoke-static {v2, v0}, Lepson/print/copy/Component/ecopycomponent/RemoteCopySetOptionTask;->getScaleChoice(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;I)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v0

    invoke-virtual {v1, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->selectChoice(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;)V

    .line 674
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$2;->this$0:Lepson/print/copy/Component/ecopycomponent/ECopyComponent;

    iput-object p1, v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    return-void
.end method

.class Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask;
.super Landroid/os/AsyncTask;
.source "RemoteCopyGetOptionTask.java"

# interfaces
.implements Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ITask;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;",
        ">;",
        "Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ITask;"
    }
.end annotation


# instance fields
.field clientID:Ljava/lang/String;

.field contextListener:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener;

.field copyType:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

.field operation:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;

.field systemSettings:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;


# direct methods
.method public constructor <init>(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener;)V
    .locals 1

    .line 39
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 40
    new-instance v0, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;

    invoke-direct {v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;-><init>()V

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask;->operation:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;

    .line 41
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask;->copyType:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    .line 42
    iput-object p2, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask;->contextListener:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener;

    return-void
.end method


# virtual methods
.method checkDeviceError()Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;
    .locals 5

    .line 81
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;

    invoke-direct {v0, p0}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;-><init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask;)V

    .line 82
    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask;->copyType:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    iput-object v1, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;->copyType:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    .line 84
    new-instance v1, Lepson/print/copy/Component/eremoteoperation/ERemoteDevice;

    invoke-direct {v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteDevice;-><init>()V

    .line 85
    iget-object v2, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask;->operation:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;

    invoke-virtual {v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;->getHostIP()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteDevice;->setHostIP(Ljava/lang/String;)V

    .line 86
    iget-object v2, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask;->operation:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;

    invoke-virtual {v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;->getRequestConnectionTimeout()I

    move-result v2

    invoke-virtual {v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteDevice;->setRequestConnectionTimeout(I)V

    .line 87
    new-instance v2, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask$1;

    invoke-direct {v2, p0}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask$1;-><init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask;)V

    invoke-virtual {v1, v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteDevice;->getFunctions(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$IRemoteOperationParameter;)Lepson/print/copy/Component/eremoteoperation/ERemoteDevice$ERemoteDeviceFunctionsResult;

    move-result-object v1

    .line 94
    invoke-virtual {v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteDevice$ERemoteDeviceFunctionsResult;->success()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x0

    .line 96
    invoke-virtual {v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteDevice$ERemoteDeviceFunctionsResult;->functions()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 97
    sget-object v4, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->copy:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    if-ne v3, v4, :cond_0

    const/4 v2, 0x1

    :cond_1
    if-nez v2, :cond_2

    .line 104
    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;->ErrorNotCopySupported:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;

    iput-object v1, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;->error:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;

    return-object v0

    .line 111
    :cond_2
    new-instance v0, Lepson/print/copy/Component/eremoteoperation/ERemotePrinter;

    invoke-direct {v0}, Lepson/print/copy/Component/eremoteoperation/ERemotePrinter;-><init>()V

    .line 112
    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask;->operation:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;

    invoke-virtual {v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;->getHostIP()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/eremoteoperation/ERemotePrinter;->setHostIP(Ljava/lang/String;)V

    .line 113
    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask;->operation:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;

    invoke-virtual {v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;->getRequestConnectionTimeout()I

    move-result v1

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/eremoteoperation/ERemotePrinter;->setRequestConnectionTimeout(I)V

    .line 114
    new-instance v1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask$2;

    invoke-direct {v1, p0}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask$2;-><init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask;)V

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/eremoteoperation/ERemotePrinter;->getComponents(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$IRemoteOperationParameter;)Lepson/print/copy/Component/eremoteoperation/ERemotePrinter$ERemotePrinterComponentsResult;

    move-result-object v0

    .line 120
    invoke-virtual {v0}, Lepson/print/copy/Component/eremoteoperation/ERemotePrinter$ERemotePrinterComponentsResult;->success()Z

    move-result v1

    if-nez v1, :cond_3

    .line 121
    invoke-virtual {p0, v0}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask;->getErrorResult(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;)Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;

    move-result-object v0

    return-object v0

    .line 124
    :cond_3
    new-instance v0, Lepson/print/copy/Component/eremoteoperation/ERemoteScanner;

    invoke-direct {v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteScanner;-><init>()V

    .line 125
    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask;->operation:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;

    invoke-virtual {v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;->getHostIP()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteScanner;->setHostIP(Ljava/lang/String;)V

    .line 126
    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask;->operation:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;

    invoke-virtual {v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;->getRequestConnectionTimeout()I

    move-result v1

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteScanner;->setRequestConnectionTimeout(I)V

    .line 127
    new-instance v1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask$3;

    invoke-direct {v1, p0}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask$3;-><init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask;)V

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteScanner;->getComponents(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$IRemoteOperationParameter;)Lepson/print/copy/Component/eremoteoperation/ERemoteScanner$ERemoteScannerComponentsResult;

    move-result-object v0

    .line 133
    invoke-virtual {v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteScanner$ERemoteScannerComponentsResult;->success()Z

    move-result v1

    if-nez v1, :cond_4

    .line 134
    invoke-virtual {p0, v0}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask;->getErrorResult(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;)Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;

    move-result-object v0

    return-object v0

    :cond_4
    const/4 v0, 0x0

    return-object v0

    .line 108
    :cond_5
    invoke-virtual {p0, v1}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask;->getErrorResult(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;)Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;
    .locals 7

    .line 143
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask;->operation:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;

    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask;->systemSettings:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;

    invoke-interface {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;->getPrinterIPAddress()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;->setHostIP(Ljava/lang/String;)V

    .line 145
    invoke-virtual {p0}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask;->checkDeviceError()Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;

    move-result-object p1

    if-eqz p1, :cond_0

    return-object p1

    .line 150
    :cond_0
    new-instance p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;

    invoke-direct {p1, p0}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;-><init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask;)V

    .line 151
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask;->copyType:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    iput-object v0, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;->copyType:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    .line 153
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask;->operation:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;

    invoke-virtual {v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;->getSelectableOptions()Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;

    move-result-object v0

    .line 268
    invoke-virtual {v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;->success()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 269
    new-instance v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    iget-object v2, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;->copyType:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    invoke-direct {v1, v2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;-><init>(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;)V

    iput-object v1, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    .line 270
    iget-object v1, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    invoke-virtual {v1, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->setCopyOptionsResult(Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;)V

    .line 271
    iget-object v1, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_size:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-static {v2, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->createOptionItem(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->add(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    .line 272
    iget-object v1, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_type:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-static {v2, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->createOptionItem(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->add(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    .line 273
    iget-object v1, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_source:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-static {v2, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->createOptionItem(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->add(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    .line 274
    iget-object v1, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_quality:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-static {v2, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->createOptionItem(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->add(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    .line 275
    iget-object v1, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->scan_content_type:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-static {v2, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->createOptionItem(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->add(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    .line 277
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_scale:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-static {v1, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->createScaleOptionItem(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v1

    .line 278
    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->copy_magnification:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v3, 0x19

    const/16 v4, 0x190

    sget v5, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->CopyMagnificationAutofitValue:I

    invoke-static {v2, v3, v4, v5}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->createLocalOptionItem(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;III)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v2

    .line 279
    iget-object v3, v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->numberRange:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    sget v4, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->CopyMagnificationAutofitValue:I

    iput v4, v3, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->exceptionValue:I

    .line 285
    iget-object v3, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    invoke-virtual {v3, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->add(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    .line 286
    iget-object v1, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    invoke-virtual {v1, v2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->add(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    .line 287
    iget-object v1, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->copies:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/16 v3, 0x63

    const/4 v4, 0x1

    invoke-static {v2, v4, v3, v4}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->createLocalOptionItem(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;III)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->add(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    .line 288
    iget-object v1, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_density:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    const/4 v3, -0x4

    const/4 v5, 0x4

    const/4 v6, 0x0

    invoke-static {v2, v3, v5, v6}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->createLocalOptionItem(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;III)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->add(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    .line 289
    iget-object v1, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v2, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->color_effects_type:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-static {v2, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->createLocalOptionItem(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->add(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    .line 291
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_x_bleed:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-static {v1, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->createLocalOptionItem(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v0

    .line 292
    iget-object v1, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;->copyType:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;->Borderless:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    if-ne v1, v2, :cond_1

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    :goto_0
    iput-boolean v4, v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->enabled:Z

    .line 293
    iget-object v1, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    invoke-virtual {v1, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->add(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    goto :goto_1

    .line 295
    :cond_2
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->success:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;->isNull(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 296
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;->ErrorCommunication:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;

    iput-object v0, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;->error:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;

    goto :goto_1

    .line 298
    :cond_3
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;->Error:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;

    iput-object v0, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;->error:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;

    :goto_1
    return-object p1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 26
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask;->doInBackground([Ljava/lang/Void;)Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;

    move-result-object p1

    return-object p1
.end method

.method getErrorResult(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;)Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;
    .locals 2

    .line 71
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;

    invoke-direct {v0, p0}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;-><init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask;)V

    .line 72
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->success:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p1, v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteOperationResult;->isNull(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 73
    sget-object p1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;->ErrorCommunication:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;

    iput-object p1, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;->error:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;

    goto :goto_0

    .line 75
    :cond_0
    sget-object p1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;->Error:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;

    iput-object p1, v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;->error:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;

    :goto_0
    return-object v0
.end method

.method protected onPostExecute(Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;)V
    .locals 3

    .line 306
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask;->contextListener:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener;

    iget-object v1, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;->copyType:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    iget-object v2, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    iget-object p1, p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;->error:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;

    invoke-interface {v0, v1, v2, p1}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener;->onCopyOptionContextCreated(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 26
    check-cast p1, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;

    invoke-virtual {p0, p1}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask;->onPostExecute(Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask$Result;)V

    return-void
.end method

.method public setClientID(Ljava/lang/String;)V
    .locals 0

    .line 67
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask;->clientID:Ljava/lang/String;

    return-void
.end method

.method public setOptionContext(Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;)V
    .locals 0

    return-void
.end method

.method public setRequestConnectionTimeout(I)V
    .locals 1

    .line 62
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask;->operation:Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;

    invoke-virtual {v0, p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy;->setRequestConnectionTimeout(I)V

    return-void
.end method

.method public setSystemSettings(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;)V
    .locals 0

    .line 57
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyGetOptionTask;->systemSettings:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;

    return-void
.end method

.method public start()Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyCancelRequest;
    .locals 1

    const/4 v0, 0x0

    .line 47
    new-array v0, v0, [Ljava/lang/Void;

    invoke-super {p0, v0}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    const/4 v0, 0x0

    return-object v0
.end method

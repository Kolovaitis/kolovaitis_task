.class public Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;
.super Ljava/lang/Object;
.source "ECopyOptionItem.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;,
        Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;,
        Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;,
        Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;,
        Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;",
            ">;"
        }
    .end annotation
.end field

.field public static CopyMagnificationAutofitValue:I

.field static DefaultExceptionValue:I


# instance fields
.field choiceArray:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

.field choiceType:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;

.field enabled:Z

.field isLocalOption:Z

.field key:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

.field numberRange:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 110
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$1;

    invoke-direct {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$1;-><init>()V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->CREATOR:Landroid/os/Parcelable$Creator;

    const/16 v0, -0x3e7

    .line 252
    sput v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->CopyMagnificationAutofitValue:I

    const/16 v0, -0x2710

    .line 253
    sput v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->DefaultExceptionValue:I

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 5

    .line 143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->valueOf(Ljava/lang/String;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    move-result-object v0

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->key:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    .line 145
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;->valueOf(Ljava/lang/String;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;

    move-result-object v0

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->choiceType:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;

    .line 146
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->enabled:Z

    .line 147
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v2, :cond_1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    iput-boolean v2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->isLocalOption:Z

    .line 149
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->choiceType:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;->ChoiceArray:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;

    if-ne v0, v2, :cond_3

    .line 150
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    invoke-direct {v0, p0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;-><init>(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    .line 151
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    :goto_2
    if-ge v1, v0, :cond_2

    .line 152
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->valueOf(Ljava/lang/String;)Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object v2

    .line 153
    iget-object v3, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    iget-object v3, v3, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->choices:Ljava/util/ArrayList;

    iget-object v4, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->key:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-static {v4, v2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->valueOf(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 155
    :cond_2
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->selectedChoice:I

    .line 156
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result p1

    iput p1, v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->defaultChoice:I

    goto :goto_3

    .line 158
    :cond_3
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    invoke-direct {v0, p0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;-><init>(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->numberRange:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    .line 159
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->numberRange:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->min:I

    .line 160
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->numberRange:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->max:I

    .line 161
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->numberRange:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->selectedValue:I

    .line 162
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->numberRange:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->defaultValue:I

    .line 163
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->numberRange:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result p1

    iput p1, v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->exceptionValue:I

    :goto_3
    return-void
.end method

.method constructor <init>(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;IIII)V
    .locals 0

    .line 167
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 168
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->key:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    .line 169
    sget-object p1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;->NumberRange:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->choiceType:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;

    const/4 p1, 0x1

    .line 170
    iput-boolean p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->enabled:Z

    .line 172
    new-instance p1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    invoke-direct {p1, p0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;-><init>(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->numberRange:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    .line 173
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->numberRange:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    iput p2, p1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->min:I

    .line 174
    iput p3, p1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->max:I

    .line 175
    iput p4, p1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->selectedValue:I

    .line 176
    iput p4, p1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->defaultValue:I

    .line 177
    iput p5, p1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->exceptionValue:I

    return-void
.end method

.method constructor <init>(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Ljava/util/ArrayList;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;",
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ">;",
            "Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;",
            ")V"
        }
    .end annotation

    .line 180
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 181
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->key:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    .line 182
    sget-object p1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;->ChoiceArray:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->choiceType:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;

    .line 183
    new-instance p1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    invoke-direct {p1, p0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;-><init>(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    .line 184
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    const/4 v0, 0x0

    iput v0, p1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->defaultChoice:I

    .line 185
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 186
    iget-object v3, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    iget-object v3, v3, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->choices:Ljava/util/ArrayList;

    iget-object v4, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->key:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-static {v4, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->valueOf(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-ne v1, p3, :cond_0

    .line 188
    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    iget-object v3, v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->choices:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    sub-int/2addr v3, v2

    iput v3, v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->defaultChoice:I

    goto :goto_0

    .line 191
    :cond_1
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    iget p3, p1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->defaultChoice:I

    iput p3, p1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->selectedChoice:I

    .line 193
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result p1

    if-le p1, v2, :cond_2

    .line 194
    iput-boolean v2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->enabled:Z

    goto :goto_1

    .line 196
    :cond_2
    iput-boolean v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->enabled:Z

    :goto_1
    return-void
.end method

.method constructor <init>(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V
    .locals 2

    .line 200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 201
    iget-object v0, p1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->key:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->key:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    .line 202
    iget-object v0, p1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->choiceType:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->choiceType:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;

    .line 203
    iget-boolean v0, p1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->enabled:Z

    iput-boolean v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->enabled:Z

    .line 204
    iget-boolean v0, p1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->isLocalOption:Z

    iput-boolean v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->isLocalOption:Z

    .line 205
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->choiceType:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;->ChoiceArray:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;

    if-ne v0, v1, :cond_0

    .line 206
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    iget-object p1, p1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    invoke-direct {v0, p0, p1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;-><init>(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;)V

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    goto :goto_0

    .line 208
    :cond_0
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    iget-object p1, p1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->numberRange:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    invoke-direct {v0, p0, p1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;-><init>(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;)V

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->numberRange:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    :goto_0
    return-void
.end method

.method static createLocalOptionItem(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;III)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;
    .locals 7

    .line 1197
    new-instance v6, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    invoke-static {p0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->key(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    move-result-object v1

    sget v5, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->DefaultExceptionValue:I

    move-object v0, v6

    move v2, p1

    move v3, p2

    move v4, p3

    invoke-direct/range {v0 .. v5}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;-><init>(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;IIII)V

    const/4 p0, 0x1

    .line 1198
    iput-boolean p0, v6, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->isLocalOption:Z

    return-object v6
.end method

.method static createLocalOptionItem(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;
    .locals 2

    .line 1179
    invoke-virtual {p1, p0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;->local_options(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1180
    invoke-virtual {p1, p0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;->local_default(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object p1

    .line 1181
    new-instance v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    invoke-static {p0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->key(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    move-result-object p0

    invoke-direct {v1, p0, v0, p1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;-><init>(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Ljava/util/ArrayList;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    const/4 p0, 0x1

    .line 1182
    iput-boolean p0, v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->isLocalOption:Z

    return-object v1
.end method

.method public static createOptionItem(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;III)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;
    .locals 7

    .line 1175
    new-instance v6, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    invoke-static {p0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->key(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    move-result-object v1

    sget v5, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->DefaultExceptionValue:I

    move-object v0, v6

    move v2, p1

    move v3, p2

    move v4, p3

    invoke-direct/range {v0 .. v5}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;-><init>(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;IIII)V

    return-object v6
.end method

.method static createOptionItem(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;
    .locals 2

    .line 1169
    invoke-virtual {p1, p0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;->parameter_options(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1170
    invoke-virtual {p1, p0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;->parameter_default(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object p1

    .line 1171
    new-instance v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    invoke-static {p0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->key(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    move-result-object p0

    invoke-direct {v1, p0, v0, p1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;-><init>(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Ljava/util/ArrayList;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    return-object v1
.end method

.method static createScaleOptionItem(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;
    .locals 2

    .line 1187
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->print_media_size:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p1, v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;->parameter_options(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1188
    new-instance v1, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult$ScaleOptions;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v1, p1, v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult$ScaleOptions;-><init>(Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;Ljava/util/ArrayList;)V

    .line 1189
    invoke-virtual {v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult$ScaleOptions;->scale_options()Ljava/util/ArrayList;

    move-result-object v0

    .line 1190
    invoke-virtual {p1, p0}, Lepson/print/copy/Component/eremoteoperation/ERemoteCopy$ERemoteCopyOptionsResult;->local_default(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object p1

    .line 1191
    new-instance v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    invoke-static {p0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->key(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    move-result-object p0

    invoke-direct {v1, p0, v0, p1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;-><init>(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;Ljava/util/ArrayList;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    const/4 p0, 0x1

    .line 1192
    iput-boolean p0, v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->isLocalOption:Z

    return-object v1
.end method

.method static key(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;
    .locals 2

    .line 1146
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->InvalidKey:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    .line 1147
    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$2;->$SwitchMap$epson$print$copy$Component$eremoteoperation$ERemoteOperation$ERemoteParam:[I

    invoke-virtual {p0}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->ordinal()I

    move-result p0

    aget p0, v1, p0

    packed-switch p0, :pswitch_data_0

    goto :goto_0

    .line 1161
    :pswitch_0
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XDensity:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    goto :goto_0

    .line 1160
    :pswitch_1
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XScale:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    goto :goto_0

    .line 1159
    :pswitch_2
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XBorderless:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    goto :goto_0

    .line 1158
    :pswitch_3
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XColorRestoration:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    goto :goto_0

    .line 1157
    :pswitch_4
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XApf:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    goto :goto_0

    .line 1156
    :pswitch_5
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->ScanContentType:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    goto :goto_0

    .line 1155
    :pswitch_6
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintXBleed:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    goto :goto_0

    .line 1154
    :pswitch_7
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintQuality:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    goto :goto_0

    .line 1153
    :pswitch_8
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    goto :goto_0

    .line 1152
    :pswitch_9
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSource:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    goto :goto_0

    .line 1151
    :pswitch_a
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    goto :goto_0

    .line 1150
    :pswitch_b
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->CopyMagnification:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    goto :goto_0

    .line 1149
    :pswitch_c
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->Copies:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    goto :goto_0

    .line 1148
    :pswitch_d
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->ColorEffectsType:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    :goto_0
    return-object v0

    :pswitch_data_0
    .packed-switch 0x6a
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static layoutOf(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;)Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;
    .locals 2

    .line 1131
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->standard:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    .line 1132
    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$2;->$SwitchMap$epson$print$copy$Component$ecopycomponent$ECopyComponent$ECopyType:[I

    invoke-virtual {p0}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;->ordinal()I

    move-result p0

    aget p0, v1, p0

    packed-switch p0, :pswitch_data_0

    goto :goto_0

    .line 1140
    :pswitch_0
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->standard:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    goto :goto_0

    .line 1139
    :pswitch_1
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->mirror:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    goto :goto_0

    .line 1138
    :pswitch_2
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->two_up_book:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    goto :goto_0

    .line 1137
    :pswitch_3
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->two_up_book:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    goto :goto_0

    .line 1136
    :pswitch_4
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->two_up:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    goto :goto_0

    .line 1135
    :pswitch_5
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->two_up:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    goto :goto_0

    .line 1134
    :pswitch_6
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->borderless:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    goto :goto_0

    .line 1133
    :pswitch_7
    sget-object v0, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->standard:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    :goto_0
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    .line 218
    :cond_0
    instance-of v0, p1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    const/4 v1, 0x0

    if-nez v0, :cond_1

    return v1

    .line 222
    :cond_1
    check-cast p1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    .line 223
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->key:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    iget-object v2, p1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->key:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    if-eq v0, v2, :cond_2

    return v1

    .line 227
    :cond_2
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->choiceType:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;

    iget-object v2, p1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->choiceType:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;

    if-eq v0, v2, :cond_3

    return v1

    .line 231
    :cond_3
    iget-boolean v2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->isLocalOption:Z

    iget-boolean v3, p1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->isLocalOption:Z

    if-eq v2, v3, :cond_4

    return v1

    .line 235
    :cond_4
    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;->ChoiceArray:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;

    if-ne v0, v1, :cond_5

    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    iget-object p1, p1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    .line 236
    invoke-virtual {v0, p1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->equals(Ljava/lang/Object;)Z

    move-result p1

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->numberRange:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    iget-object p1, p1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->numberRange:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    .line 237
    invoke-virtual {v0, p1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->equals(Ljava/lang/Object;)Z

    move-result p1

    :goto_0
    return p1
.end method

.method public getChoiceType()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;
    .locals 1

    .line 1013
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->choiceType:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;

    return-object v0
.end method

.method public getDefaultChoice()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;
    .locals 2

    .line 1093
    :try_start_0
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->choices:Ljava/util/ArrayList;

    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    iget v1, v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->defaultChoice:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getDefaultValue()I
    .locals 1

    .line 1043
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->numberRange:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    iget v0, v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->defaultValue:I

    return v0
.end method

.method public getKey()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;
    .locals 1

    .line 997
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->key:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    return-object v0
.end method

.method public getMaximumValue()I
    .locals 1

    .line 1033
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->numberRange:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    iget v0, v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->max:I

    return v0
.end method

.method public getMinimumValue()I
    .locals 1

    .line 1023
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->numberRange:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    iget v0, v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->min:I

    return v0
.end method

.method public getSelectableChoices()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;",
            ">;"
        }
    .end annotation

    .line 1079
    :try_start_0
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->choices:Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSelectedChoice()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;
    .locals 2

    .line 1107
    :try_start_0
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->choices:Ljava/util/ArrayList;

    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    iget v1, v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->selectedChoice:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSelectedValue()I
    .locals 1

    .line 1058
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->numberRange:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    iget v0, v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->selectedValue:I

    return v0
.end method

.method public isEnabled()Z
    .locals 1

    .line 1005
    iget-boolean v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->enabled:Z

    return v0
.end method

.method public selectChoice(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;)V
    .locals 3

    .line 1121
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->choices:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    if-ne v2, p1, :cond_0

    .line 1123
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    iput v1, p1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->selectedChoice:I

    goto :goto_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    return-void
.end method

.method public selectValue(I)V
    .locals 2

    .line 1067
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->numberRange:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    iget v1, v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->exceptionValue:I

    if-ne p1, v1, :cond_0

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->numberRange:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    iget v1, v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->min:I

    .line 1068
    invoke-static {v1, p1}, Ljava/lang/Math;->max(II)I

    move-result p1

    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->numberRange:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    iget v1, v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->max:I

    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result p1

    :goto_0
    iput p1, v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->selectedValue:I

    return-void
.end method

.method protected setDefaultValue(I)V
    .locals 1

    .line 1047
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->numberRange:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    iput p1, v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->defaultValue:I

    .line 1048
    invoke-virtual {p0, p1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->selectValue(I)V

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 122
    iget-object p2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->key:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {p2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 123
    iget-object p2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->choiceType:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;

    invoke-virtual {p2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 124
    iget-boolean p2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->enabled:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 125
    iget-boolean p2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->isLocalOption:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 127
    iget-object p2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->choiceType:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;

    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;->ChoiceArray:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoiceType;

    if-ne p2, v0, :cond_1

    .line 128
    iget-object p2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    iget-object p2, p2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->choices:Ljava/util/ArrayList;

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 129
    iget-object p2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    iget-object p2, p2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->choices:Ljava/util/ArrayList;

    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 130
    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->param:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 132
    :cond_0
    iget-object p2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    iget p2, p2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->selectedChoice:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 133
    iget-object p2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->choiceArray:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    iget p2, p2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->defaultChoice:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_1

    .line 135
    :cond_1
    iget-object p2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->numberRange:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    iget p2, p2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->min:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 136
    iget-object p2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->numberRange:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    iget p2, p2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->max:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 137
    iget-object p2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->numberRange:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    iget p2, p2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->selectedValue:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 138
    iget-object p2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->numberRange:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    iget p2, p2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->defaultValue:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 139
    iget-object p2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->numberRange:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;

    iget p2, p2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$NumberRange;->exceptionValue:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    :goto_1
    return-void
.end method

.class public final enum Lepson/print/copy/Component/ecopycomponent/ECopyPreview$ECopyPreviewColor;
.super Ljava/lang/Enum;
.source "ECopyPreview.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/copy/Component/ecopycomponent/ECopyPreview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ECopyPreviewColor"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lepson/print/copy/Component/ecopycomponent/ECopyPreview$ECopyPreviewColor;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lepson/print/copy/Component/ecopycomponent/ECopyPreview$ECopyPreviewColor;

.field public static final enum Color:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$ECopyPreviewColor;

.field public static final enum Monochrome:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$ECopyPreviewColor;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 147
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$ECopyPreviewColor;

    const-string v1, "Color"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$ECopyPreviewColor;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$ECopyPreviewColor;->Color:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$ECopyPreviewColor;

    .line 151
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$ECopyPreviewColor;

    const-string v1, "Monochrome"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$ECopyPreviewColor;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$ECopyPreviewColor;->Monochrome:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$ECopyPreviewColor;

    const/4 v0, 0x2

    .line 143
    new-array v0, v0, [Lepson/print/copy/Component/ecopycomponent/ECopyPreview$ECopyPreviewColor;

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$ECopyPreviewColor;->Color:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$ECopyPreviewColor;

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$ECopyPreviewColor;->Monochrome:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$ECopyPreviewColor;

    aput-object v1, v0, v3

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$ECopyPreviewColor;->$VALUES:[Lepson/print/copy/Component/ecopycomponent/ECopyPreview$ECopyPreviewColor;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 143
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lepson/print/copy/Component/ecopycomponent/ECopyPreview$ECopyPreviewColor;
    .locals 1

    .line 143
    const-class v0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$ECopyPreviewColor;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$ECopyPreviewColor;

    return-object p0
.end method

.method public static values()[Lepson/print/copy/Component/ecopycomponent/ECopyPreview$ECopyPreviewColor;
    .locals 1

    .line 143
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$ECopyPreviewColor;->$VALUES:[Lepson/print/copy/Component/ecopycomponent/ECopyPreview$ECopyPreviewColor;

    invoke-virtual {v0}, [Lepson/print/copy/Component/ecopycomponent/ECopyPreview$ECopyPreviewColor;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lepson/print/copy/Component/ecopycomponent/ECopyPreview$ECopyPreviewColor;

    return-object v0
.end method

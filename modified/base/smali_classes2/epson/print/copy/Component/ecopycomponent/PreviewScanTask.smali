.class Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;
.super Landroid/os/AsyncTask;
.source "PreviewScanTask.java"

# interfaces
.implements Lepson/print/copy/Component/escandevice/IScanController;
.implements Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ITask;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;",
        "Lepson/print/copy/Component/escandevice/IScanController;",
        "Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ITask;"
    }
.end annotation


# instance fields
.field cancelRequested:Z

.field canvas:Landroid/graphics/Canvas;

.field copyPreview:Lepson/print/copy/Component/ecopycomponent/ECopyPreview;

.field optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

.field previewScanBitmap:Landroid/graphics/Bitmap;

.field properties:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field scale:F

.field statusListener:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;

.field systemSettings:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;


# direct methods
.method public constructor <init>(Lepson/print/copy/Component/ecopycomponent/ECopyPreview;Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;)V
    .locals 1

    .line 36
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 30
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;->properties:Ljava/util/HashMap;

    .line 37
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;->copyPreview:Lepson/print/copy/Component/ecopycomponent/ECopyPreview;

    .line 38
    iput-object p2, p0, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;->statusListener:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 25
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 1

    .line 107
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;->systemSettings:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;

    invoke-interface {p1}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;->getPrinterIPAddress()Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x4

    invoke-virtual {p0, v0, p1}, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;->setPropertyValue(ILjava/lang/Object;)V

    .line 108
    invoke-static {p0}, Lepson/print/copy/Component/escandevice/EScanDevice;->nativeStartScan(Lepson/print/copy/Component/escandevice/IScanController;)I

    const/4 p1, 0x0

    return-object p1
.end method

.method public getIntegerValue(I)I
    .locals 1

    .line 114
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;->properties:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    .line 115
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    return p1
.end method

.method public getStringValue(I)Ljava/lang/String;
    .locals 1

    .line 120
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;->properties:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    return-object p1
.end method

.method public isCancelRequested()Z
    .locals 1

    .line 125
    monitor-enter p0

    .line 126
    :try_start_0
    iget-boolean v0, p0, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;->cancelRequested:Z

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    .line 127
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setClientID(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public setCopyFrameSize(IIII)V
    .locals 1

    .line 187
    new-instance v0, Landroid/graphics/RectF;

    int-to-float p1, p1

    int-to-float p2, p2

    int-to-float p3, p3

    int-to-float p4, p4

    invoke-direct {v0, p1, p2, p3, p4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 188
    monitor-enter p0

    .line 189
    :try_start_0
    iget-boolean p1, p0, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;->cancelRequested:Z

    const/4 p2, 0x1

    if-ne p1, p2, :cond_0

    .line 190
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 192
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 193
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;->copyPreview:Lepson/print/copy/Component/ecopycomponent/ECopyPreview;

    invoke-virtual {p1, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->setPaperFrame(Landroid/graphics/RectF;)V

    return-void

    :catchall_0
    move-exception p1

    .line 192
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method public setOptionContext(Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;)V
    .locals 4

    .line 73
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    .line 76
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XScale:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {p1, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionItemOf(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object p1

    .line 77
    invoke-virtual {p1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object p1

    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_Autofit:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const v1, 0x30002

    const v2, 0x30001

    if-eq p1, v0, :cond_0

    .line 78
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {p1, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionItemOf(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object p1

    .line 79
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask$4;->$SwitchMap$epson$print$copy$Component$ecopycomponent$ECopyOptionItem$ECopyOptionItemChoice:[I

    invoke-virtual {p1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object p1

    invoke-virtual {p1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const p1, 0x30007

    goto :goto_1

    :pswitch_1
    const p1, 0x30006

    goto :goto_1

    :pswitch_2
    const p1, 0x30005

    goto :goto_1

    :pswitch_3
    const p1, 0x30004

    goto :goto_1

    :pswitch_4
    const p1, 0x30003

    goto :goto_1

    :pswitch_5
    const p1, 0x30002

    goto :goto_1

    :pswitch_6
    const p1, 0x30001

    goto :goto_1

    :cond_0
    :goto_0
    const/high16 p1, 0x30000

    .line 91
    :goto_1
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->getCopyType()Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    move-result-object v0

    .line 92
    sget-object v3, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;->A4_2up:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    if-eq v0, v3, :cond_3

    sget-object v3, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;->A4_2up_Book:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    if-ne v0, v3, :cond_1

    goto :goto_2

    .line 94
    :cond_1
    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;->B5_2up:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    if-eq v0, v2, :cond_2

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;->B5_2up_Book:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    if-ne v0, v2, :cond_4

    :cond_2
    const p1, 0x30002

    goto :goto_3

    :cond_3
    :goto_2
    const p1, 0x30001

    :cond_4
    :goto_3
    const/4 v0, 0x3

    .line 100
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p0, v0, p1}, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;->setPropertyValue(ILjava/lang/Object;)V

    const/4 p1, 0x1

    const/high16 v0, 0x10000

    .line 101
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;->setPropertyValue(ILjava/lang/Object;)V

    const/4 p1, 0x2

    const v0, 0x20001

    .line 102
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;->setPropertyValue(ILjava/lang/Object;)V

    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method setPropertyValue(ILjava/lang/Object;)V
    .locals 1

    .line 42
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;->properties:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public setRequestConnectionTimeout(I)V
    .locals 0

    return-void
.end method

.method public setScanImageBlockReceived([IIIII)V
    .locals 1

    .line 178
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p4, p5, v0}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object p1

    .line 179
    new-instance p4, Landroid/graphics/Matrix;

    invoke-direct {p4}, Landroid/graphics/Matrix;-><init>()V

    .line 180
    iget p5, p0, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;->scale:F

    invoke-virtual {p4, p5, p5}, Landroid/graphics/Matrix;->postScale(FF)Z

    int-to-float p2, p2

    .line 181
    iget p5, p0, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;->scale:F

    mul-float p2, p2, p5

    int-to-float p3, p3

    mul-float p3, p3, p5

    invoke-virtual {p4, p2, p3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 182
    iget-object p2, p0, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;->canvas:Landroid/graphics/Canvas;

    const/4 p3, 0x0

    invoke-virtual {p2, p1, p4, p3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    return-void
.end method

.method public setScanImageSize(II)V
    .locals 2

    .line 166
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;->copyPreview:Lepson/print/copy/Component/ecopycomponent/ECopyPreview;

    new-instance v1, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask$3;

    invoke-direct {v1, p0}, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask$3;-><init>(Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;)V

    invoke-virtual {v0, p1, p2, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->calculateScanImageSize(IILepson/print/copy/Component/ecopycomponent/ECopyPreview$ScanImageSizeCalculator;)V

    return-void
.end method

.method public setScanStatusChanged(II)V
    .locals 2

    .line 132
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;->copyPreview:Lepson/print/copy/Component/ecopycomponent/ECopyPreview;

    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 133
    new-instance v1, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask$2;

    invoke-direct {v1, p0, p1, p2}, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask$2;-><init>(Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;II)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public setSystemSettings(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;)V
    .locals 0

    .line 60
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;->systemSettings:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;

    return-void
.end method

.method public start()Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyCancelRequest;
    .locals 1

    const/4 v0, 0x0

    .line 47
    new-array v0, v0, [Ljava/lang/Void;

    invoke-super {p0, v0}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 48
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask$1;

    invoke-direct {v0, p0}, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask$1;-><init>(Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;)V

    return-object v0
.end method

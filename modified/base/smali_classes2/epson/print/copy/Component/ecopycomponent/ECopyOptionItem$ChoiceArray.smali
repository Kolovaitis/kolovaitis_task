.class Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;
.super Ljava/lang/Object;
.source "ECopyOptionItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ChoiceArray"
.end annotation


# instance fields
.field choices:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;",
            ">;"
        }
    .end annotation
.end field

.field defaultChoice:I

.field selectedChoice:I

.field final synthetic this$0:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;


# direct methods
.method public constructor <init>(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V
    .locals 0

    .line 29
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->this$0:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->choices:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;)V
    .locals 2

    .line 32
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->this$0:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->choices:Ljava/util/ArrayList;

    .line 33
    iget-object p1, p2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->choices:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 34
    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->choices:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 36
    :cond_0
    iget p1, p2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->selectedChoice:I

    iput p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->selectedChoice:I

    .line 37
    iget p1, p2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->defaultChoice:I

    iput p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->defaultChoice:I

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    .line 46
    :cond_0
    instance-of v1, p1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 50
    :cond_1
    check-cast p1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;

    .line 51
    iget v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->selectedChoice:I

    iget v3, p1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->selectedChoice:I

    if-eq v1, v3, :cond_2

    return v2

    .line 55
    :cond_2
    iget v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->defaultChoice:I

    iget v3, p1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->defaultChoice:I

    if-eq v1, v3, :cond_3

    return v2

    .line 59
    :cond_3
    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->choices:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget-object v3, p1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->choices:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-eq v1, v3, :cond_4

    return v2

    :cond_4
    const/4 v1, 0x0

    .line 63
    :goto_0
    iget-object v3, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->choices:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_6

    .line 64
    iget-object v3, p0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->choices:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ChoiceArray;->choices:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    if-eq v3, v4, :cond_5

    return v2

    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_6
    return v0
.end method

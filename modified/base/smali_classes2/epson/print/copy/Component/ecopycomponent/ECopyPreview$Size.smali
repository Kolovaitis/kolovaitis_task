.class Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;
.super Ljava/lang/Object;
.source "ECopyPreview.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/copy/Component/ecopycomponent/ECopyPreview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Size"
.end annotation


# instance fields
.field height:I

.field final synthetic this$0:Lepson/print/copy/Component/ecopycomponent/ECopyPreview;

.field width:I


# direct methods
.method constructor <init>(Lepson/print/copy/Component/ecopycomponent/ECopyPreview;II)V
    .locals 0

    .line 431
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;->this$0:Lepson/print/copy/Component/ecopycomponent/ECopyPreview;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 432
    iput p2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;->width:I

    .line 433
    iput p3, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;->height:I

    return-void
.end method

.method constructor <init>(Lepson/print/copy/Component/ecopycomponent/ECopyPreview;Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;)V
    .locals 0

    .line 436
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;->this$0:Lepson/print/copy/Component/ecopycomponent/ECopyPreview;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 437
    iget p1, p2, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;->width:I

    iput p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;->width:I

    .line 438
    iget p1, p2, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;->height:I

    iput p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;->height:I

    return-void
.end method


# virtual methods
.method scale(F)V
    .locals 1

    .line 442
    iget v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;->width:I

    int-to-float v0, v0

    mul-float v0, v0, p1

    float-to-int v0, v0

    iput v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;->width:I

    .line 443
    iget v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;->height:I

    int-to-float v0, v0

    mul-float v0, v0, p1

    float-to-int p1, v0

    iput p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;->height:I

    return-void
.end method

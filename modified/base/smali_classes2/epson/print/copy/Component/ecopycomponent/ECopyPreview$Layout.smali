.class abstract Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout;
.super Ljava/lang/Object;
.source "ECopyPreview.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/copy/Component/ecopycomponent/ECopyPreview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x408
    name = "Layout"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 447
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method getAspectFitScale(FFFFLepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;)F
    .locals 1

    .line 469
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    div-float/2addr p1, p3

    .line 470
    iput p1, v0, Landroid/graphics/PointF;->x:F

    div-float/2addr p2, p4

    .line 471
    iput p2, v0, Landroid/graphics/PointF;->y:F

    .line 472
    sget-object p1, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;->Min:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;

    if-ne p5, p1, :cond_0

    iget p1, v0, Landroid/graphics/PointF;->x:F

    iget p2, v0, Landroid/graphics/PointF;->y:F

    invoke-static {p1, p2}, Ljava/lang/Math;->min(FF)F

    move-result p1

    goto :goto_0

    :cond_0
    iget p1, v0, Landroid/graphics/PointF;->x:F

    iget p2, v0, Landroid/graphics/PointF;->y:F

    invoke-static {p1, p2}, Ljava/lang/Math;->max(FF)F

    move-result p1

    :goto_0
    return p1
.end method

.method abstract getBitmap()Landroid/graphics/Bitmap;
.end method

.method abstract getCopyFrame()Landroid/graphics/RectF;
.end method

.method abstract getOffset()Landroid/graphics/Point;
.end method

.method abstract getPaperRect()Landroid/graphics/RectF;
.end method

.method abstract getScanImageScale(II)F
.end method

.method scaleRect(FLandroid/graphics/RectF;)V
    .locals 1

    .line 458
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 459
    invoke-virtual {v0, p1, p1}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 460
    invoke-virtual {v0, p2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    return-void
.end method

.method abstract setCopyScale(F)V
.end method

.method abstract setPaperFrame(Landroid/graphics/RectF;)V
.end method

.method abstract setScanImage(Landroid/graphics/Bitmap;)V
.end method

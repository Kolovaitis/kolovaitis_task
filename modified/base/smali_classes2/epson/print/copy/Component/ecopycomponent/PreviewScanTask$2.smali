.class Lepson/print/copy/Component/ecopycomponent/PreviewScanTask$2;
.super Ljava/lang/Object;
.source "PreviewScanTask.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;->setScanStatusChanged(II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;

.field final synthetic val$error:I

.field final synthetic val$status:I


# direct methods
.method constructor <init>(Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;II)V
    .locals 0

    .line 133
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask$2;->this$0:Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;

    iput p2, p0, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask$2;->val$status:I

    iput p3, p0, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask$2;->val$error:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 135
    iget v0, p0, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask$2;->val$status:I

    packed-switch v0, :pswitch_data_0

    goto :goto_1

    .line 155
    :pswitch_0
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask$2;->this$0:Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;->copyPreview:Lepson/print/copy/Component/ecopycomponent/ECopyPreview;

    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask$2;->this$0:Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;

    iget-object v1, v1, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;->previewScanBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->drawScanPreview(Landroid/graphics/Bitmap;)V

    goto :goto_1

    .line 152
    :pswitch_1
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask$2;->this$0:Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;->previewScanBitmap:Landroid/graphics/Bitmap;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->eraseColor(I)V

    goto :goto_1

    .line 142
    :pswitch_2
    iget v0, p0, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask$2;->val$error:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    packed-switch v0, :pswitch_data_1

    .line 146
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;->ErrorOther:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    goto :goto_0

    .line 144
    :pswitch_3
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;->Canceled:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    goto :goto_0

    .line 143
    :pswitch_4
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;->Succeed:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    goto :goto_0

    .line 145
    :cond_0
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;->Busy:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    .line 148
    :goto_0
    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask$2;->this$0:Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;

    iget-object v1, v1, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;->statusListener:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;->Preview:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;

    invoke-interface {v1, v2, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;->onFinished(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;)V

    goto :goto_1

    .line 137
    :pswitch_5
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask$2;->this$0:Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;

    const/4 v1, 0x0

    iput-object v1, v0, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;->previewScanBitmap:Landroid/graphics/Bitmap;

    .line 138
    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;->statusListener:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;->Preview:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;

    invoke-interface {v0, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener;->onStarted(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskType;)V

    :goto_1
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.class public Lepson/print/copy/Component/ecopycomponent/ECopyPreview;
.super Landroid/view/View;
.source "ECopyPreview.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;,
        Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;,
        Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutMirror;,
        Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;,
        Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout;,
        Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;,
        Lepson/print/copy/Component/ecopycomponent/ECopyPreview$ScanImageSizeCalculator;,
        Lepson/print/copy/Component/ecopycomponent/ECopyPreview$ICopyPreviewCommitEditedOptionContext;,
        Lepson/print/copy/Component/ecopycomponent/ECopyPreview$ECopyPreviewColor;,
        Lepson/print/copy/Component/ecopycomponent/ECopyPreview$ECopyPreviewInvalidateListener;
    }
.end annotation


# instance fields
.field final A4:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

.field final A4_2Up:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

.field final A4_2UpPage:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

.field final B5:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

.field final BM_2Up:I

.field final BM_Border:I

.field final BO_Type1:I

.field final BO_Type2:I

.field final Hagaki:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

.field final KG:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

.field final L:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

.field final L2:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

.field final LM_2Up:I

.field final LM_Border:I

.field final LO_Type1:I

.field final LO_Type2:I

.field final Max:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

.field final P6:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

.field final PG_2Up:I

.field final PL_A4:I

.field final PL_B5:I

.field final PL_Hagaki:I

.field final PL_KG:I

.field final PL_L:I

.field final PL_L2:I

.field final PL_MAX:I

.field final PL_P6:I

.field final PW_A4:I

.field final PW_B5:I

.field final PW_Hagaki:I

.field final PW_KG:I

.field final PW_L:I

.field final PW_L2:I

.field final PW_MAX:I

.field final PW_P6:I

.field final RM_2Up:I

.field final RM_Border:I

.field final RO_Type1:I

.field final RO_Type2:I

.field final TM_2Up:I

.field final TM_Border:I

.field final TO_Type1:I

.field final TO_Type2:I

.field backgroundDrawable:Landroid/graphics/drawable/Drawable;

.field colorOption:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

.field copiesOpiton:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

.field copyFramePaint:Landroid/graphics/Paint;

.field drawSize:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

.field grayscalePaint:Landroid/graphics/Paint;

.field layout:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout;

.field listener:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$ECopyPreviewInvalidateListener;

.field optionCommitter:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$ICopyPreviewCommitEditedOptionContext;

.field optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

.field paperDrawable:Landroid/graphics/drawable/Drawable;

.field paperFrame:Landroid/graphics/RectF;

.field paperPadding:Landroid/graphics/Rect;

.field paperShadowPaint:Landroid/graphics/Paint;

.field scaleOption:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 14

    .line 52
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 32
    new-instance p1, Landroid/graphics/drawable/ColorDrawable;

    const/4 v0, 0x0

    invoke-direct {p1, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->backgroundDrawable:Landroid/graphics/drawable/Drawable;

    .line 33
    new-instance p1, Landroid/graphics/drawable/ColorDrawable;

    const/4 v0, -0x1

    invoke-direct {p1, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->paperDrawable:Landroid/graphics/drawable/Drawable;

    const/16 p1, 0x4fb

    .line 378
    iput p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->PW_MAX:I

    const/16 v0, 0x6db

    .line 379
    iput v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->PL_MAX:I

    const/16 v1, 0x4d8

    .line 380
    iput v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->PW_A4:I

    .line 381
    iput v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->PL_A4:I

    const/16 v2, 0x433

    .line 382
    iput v2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->PW_B5:I

    const/16 v3, 0x5ee

    .line 383
    iput v3, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->PL_B5:I

    const/16 v4, 0x20e

    .line 384
    iput v4, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->PW_L:I

    const/16 v5, 0x2ee

    .line 385
    iput v5, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->PL_L:I

    .line 386
    iput v5, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->PW_L2:I

    const/16 v6, 0x41b

    .line 387
    iput v6, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->PL_L2:I

    const/16 v7, 0x24f

    .line 388
    iput v7, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->PW_Hagaki:I

    const/16 v8, 0x36a

    .line 389
    iput v8, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->PL_Hagaki:I

    const/16 v9, 0x25a

    .line 390
    iput v9, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->PW_KG:I

    const/16 v10, 0x382

    .line 391
    iput v10, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->PL_KG:I

    const/16 v11, 0x4af

    .line 392
    iput v11, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->PW_P6:I

    const/16 v12, 0x5dc

    .line 393
    iput v12, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->PL_P6:I

    const/16 v12, 0x20

    .line 395
    iput v12, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->LM_2Up:I

    .line 396
    iput v12, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->RM_2Up:I

    const/16 v12, 0x1e

    .line 397
    iput v12, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->TM_2Up:I

    const/16 v12, 0x23

    .line 398
    iput v12, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->BM_2Up:I

    .line 399
    iput v12, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->PG_2Up:I

    const/16 v12, 0x12

    .line 401
    iput v12, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->LM_Border:I

    .line 402
    iput v12, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->RM_Border:I

    .line 403
    iput v12, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->TM_Border:I

    .line 404
    iput v12, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->BM_Border:I

    const/16 v12, 0xf

    .line 406
    iput v12, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->LO_Type1:I

    .line 407
    iput v12, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->RO_Type1:I

    const/16 v13, 0x11

    .line 408
    iput v13, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->TO_Type1:I

    const/16 v13, 0x18

    .line 409
    iput v13, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->BO_Type1:I

    .line 411
    iput v12, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->LO_Type2:I

    .line 412
    iput v12, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->RO_Type2:I

    const/16 v13, 0x8

    .line 413
    iput v13, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->TO_Type2:I

    .line 414
    iput v12, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->BO_Type2:I

    .line 416
    new-instance v12, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    invoke-direct {v12, p0, p1, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;-><init>(Lepson/print/copy/Component/ecopycomponent/ECopyPreview;II)V

    iput-object v12, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->Max:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    .line 417
    new-instance p1, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    invoke-direct {p1, p0, v1, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;-><init>(Lepson/print/copy/Component/ecopycomponent/ECopyPreview;II)V

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->A4:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    .line 418
    new-instance p1, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    invoke-direct {p1, p0, v2, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;-><init>(Lepson/print/copy/Component/ecopycomponent/ECopyPreview;II)V

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->B5:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    .line 419
    new-instance p1, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    invoke-direct {p1, p0, v4, v5}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;-><init>(Lepson/print/copy/Component/ecopycomponent/ECopyPreview;II)V

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->L:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    .line 420
    new-instance p1, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    invoke-direct {p1, p0, v5, v6}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;-><init>(Lepson/print/copy/Component/ecopycomponent/ECopyPreview;II)V

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->L2:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    .line 421
    new-instance p1, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    invoke-direct {p1, p0, v7, v8}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;-><init>(Lepson/print/copy/Component/ecopycomponent/ECopyPreview;II)V

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->Hagaki:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    .line 422
    new-instance p1, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    invoke-direct {p1, p0, v9, v10}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;-><init>(Lepson/print/copy/Component/ecopycomponent/ECopyPreview;II)V

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->KG:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    .line 423
    new-instance p1, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    const/16 v2, 0x5dc

    invoke-direct {p1, p0, v11, v2}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;-><init>(Lepson/print/copy/Component/ecopycomponent/ECopyPreview;II)V

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->P6:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    .line 424
    new-instance p1, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    invoke-direct {p1, p0, v0, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;-><init>(Lepson/print/copy/Component/ecopycomponent/ECopyPreview;II)V

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->A4_2Up:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    .line 425
    new-instance p1, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    const/16 v0, 0x33b

    const/16 v1, 0x498

    invoke-direct {p1, p0, v0, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;-><init>(Lepson/print/copy/Component/ecopycomponent/ECopyPreview;II)V

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->A4_2UpPage:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    .line 54
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->copyFramePaint:Landroid/graphics/Paint;

    .line 55
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->copyFramePaint:Landroid/graphics/Paint;

    const/high16 v0, -0x10000

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 56
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->copyFramePaint:Landroid/graphics/Paint;

    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 57
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->copyFramePaint:Landroid/graphics/Paint;

    const/high16 v0, 0x40000000    # 2.0f

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 59
    new-instance p1, Landroid/graphics/Rect;

    invoke-direct {p1}, Landroid/graphics/Rect;-><init>()V

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->paperPadding:Landroid/graphics/Rect;

    .line 60
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->grayscalePaint:Landroid/graphics/Paint;

    .line 61
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->paperShadowPaint:Landroid/graphics/Paint;

    .line 62
    new-instance p1, Landroid/graphics/RectF;

    invoke-direct {p1}, Landroid/graphics/RectF;-><init>()V

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->paperFrame:Landroid/graphics/RectF;

    .line 64
    new-instance p1, Landroid/graphics/ColorMatrix;

    invoke-direct {p1}, Landroid/graphics/ColorMatrix;-><init>()V

    const/4 v0, 0x0

    .line 65
    invoke-virtual {p1, v0}, Landroid/graphics/ColorMatrix;->setSaturation(F)V

    .line 66
    new-instance v0, Landroid/graphics/ColorMatrixColorFilter;

    invoke-direct {v0, p1}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    .line 67
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->grayscalePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    return-void
.end method

.method protected static createPreview(Landroid/content/Context;IILepson/print/copy/Component/ecopycomponent/ECopyPreview$ECopyPreviewInvalidateListener;)Lepson/print/copy/Component/ecopycomponent/ECopyPreview;
    .locals 1

    .line 262
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;

    invoke-direct {v0, p0}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;-><init>(Landroid/content/Context;)V

    .line 263
    new-instance p0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 264
    invoke-virtual {v0, p0}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 265
    new-instance p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {p0, v0, p1, p2}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;-><init>(Lepson/print/copy/Component/ecopycomponent/ECopyPreview;II)V

    iput-object p0, v0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->drawSize:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    .line 266
    iput-object p3, v0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->listener:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$ECopyPreviewInvalidateListener;

    return-object v0
.end method


# virtual methods
.method protected calculateScanImageSize(IILepson/print/copy/Component/ecopycomponent/ECopyPreview$ScanImageSizeCalculator;)V
    .locals 1

    .line 280
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->layout:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout;

    invoke-virtual {v0, p1, p2}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout;->getScanImageScale(II)F

    move-result v0

    int-to-float p1, p1

    mul-float p1, p1, v0

    float-to-int p1, p1

    int-to-float p2, p2

    mul-float p2, p2, v0

    float-to-int p2, p2

    .line 281
    invoke-interface {p3, p1, p2, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$ScanImageSizeCalculator;->onScanImageSize(IIF)V

    return-void
.end method

.method compose(Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;Lepson/print/copy/Component/ecopycomponent/ECopyPreview$ICopyPreviewCommitEditedOptionContext;)V
    .locals 0

    .line 234
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    .line 235
    iput-object p2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->optionCommitter:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$ICopyPreviewCommitEditedOptionContext;

    .line 237
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    sget-object p2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->Copies:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {p1, p2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionItemOf(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object p1

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->copiesOpiton:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    .line 238
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    sget-object p2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->CopyMagnification:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {p1, p2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionItemOf(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object p1

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->scaleOption:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    .line 239
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    sget-object p2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->ColorEffectsType:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {p1, p2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionItemOf(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object p1

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->colorOption:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    .line 241
    invoke-virtual {p0}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->resize()V

    return-void
.end method

.method public dismiss(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 225
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->optionCommitter:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$ICopyPreviewCommitEditedOptionContext;

    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    invoke-interface {p1, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$ICopyPreviewCommitEditedOptionContext;->commit(Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;)V

    :cond_0
    return-void
.end method

.method protected drawScanPreview(Landroid/graphics/Bitmap;)V
    .locals 1

    .line 271
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->layout:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout;

    invoke-virtual {v0, p1}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout;->setScanImage(Landroid/graphics/Bitmap;)V

    .line 272
    invoke-virtual {p0}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->requestInvalidateView()V

    return-void
.end method

.method public getColor()Lepson/print/copy/Component/ecopycomponent/ECopyPreview$ECopyPreviewColor;
    .locals 3

    .line 172
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$ECopyPreviewColor;->Color:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$ECopyPreviewColor;

    .line 173
    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->colorOption:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    invoke-virtual {v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v1

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->ColorEffectsType_MonochromeGrayscale:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    if-ne v1, v2, :cond_0

    .line 174
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$ECopyPreviewColor;->Monochrome:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$ECopyPreviewColor;

    :cond_0
    return-object v0
.end method

.method public getCopies()I
    .locals 1

    .line 216
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->copiesOpiton:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedValue()I

    move-result v0

    return v0
.end method

.method public getScale()I
    .locals 1

    .line 198
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->scaleOption:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedValue()I

    move-result v0

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 5

    .line 297
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 299
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->backgroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 300
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->backgroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 302
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->layout:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout;

    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout;->getOffset()Landroid/graphics/Point;

    move-result-object v0

    .line 303
    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->paperPadding:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->paperPadding:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->offset(II)V

    .line 305
    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->layout:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout;

    invoke-virtual {v1}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout;->getPaperRect()Landroid/graphics/RectF;

    move-result-object v1

    .line 306
    iget v2, v0, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    iget v3, v0, Landroid/graphics/Point;->y:I

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/RectF;->offset(FF)V

    .line 308
    iget-object v2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    iget-object v2, v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->copyType:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    sget-object v3, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;->Mirror:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    if-ne v2, v3, :cond_0

    const/high16 v2, -0x40800000    # -1.0f

    const/high16 v3, 0x3f800000    # 1.0f

    .line 309
    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->scale(FF)V

    .line 310
    invoke-virtual {v1}, Landroid/graphics/RectF;->centerX()F

    move-result v2

    const/high16 v3, 0x40000000    # 2.0f

    mul-float v2, v2, v3

    neg-float v2, v2

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 313
    :cond_0
    iget-object v2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->paperShadowPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    const/4 v1, 0x0

    .line 316
    iget-object v2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->colorOption:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    invoke-virtual {v2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v2

    sget-object v3, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->ColorEffectsType_MonochromeGrayscale:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    if-ne v2, v3, :cond_1

    .line 317
    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->grayscalePaint:Landroid/graphics/Paint;

    .line 319
    :cond_1
    iget-object v2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->layout:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout;

    invoke-virtual {v2}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    iget v3, v0, Landroid/graphics/Point;->x:I

    int-to-float v3, v3

    iget v4, v0, Landroid/graphics/Point;->y:I

    int-to-float v4, v4

    invoke-virtual {p1, v2, v3, v4, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 321
    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->layout:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout;

    invoke-virtual {v1}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout;->getCopyFrame()Landroid/graphics/RectF;

    move-result-object v1

    .line 322
    iget-object v2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->paperFrame:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    if-eqz v1, :cond_2

    .line 323
    iget v2, v0, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v0, v0

    invoke-virtual {v1, v2, v0}, Landroid/graphics/RectF;->offset(FF)V

    .line 324
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    .line 325
    sget-object v2, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->addRect(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    .line 326
    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->copyFramePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 328
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return-void
.end method

.method requestInvalidateView()V
    .locals 2

    .line 132
    invoke-virtual {p0}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 133
    new-instance v1, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$1;

    invoke-direct {v1, p0}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$1;-><init>(Lepson/print/copy/Component/ecopycomponent/ECopyPreview;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method resize()V
    .locals 4

    .line 245
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->drawSize:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    iget v0, v0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;->width:I

    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->paperPadding:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->paperPadding:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    sub-int/2addr v0, v1

    .line 246
    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->drawSize:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    iget v1, v1, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;->height:I

    iget-object v2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->paperPadding:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v1, v2

    iget-object v2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->paperPadding:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v1, v2

    .line 247
    iget-object v2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    invoke-virtual {v2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->getCopyType()Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    move-result-object v2

    .line 248
    sget-object v3, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;->A4_2up:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    if-eq v2, v3, :cond_3

    sget-object v3, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;->B5_2up:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    if-eq v2, v3, :cond_3

    sget-object v3, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;->A4_2up_Book:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    if-eq v2, v3, :cond_3

    sget-object v3, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;->B5_2up_Book:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    if-ne v2, v3, :cond_0

    goto :goto_0

    .line 251
    :cond_0
    sget-object v3, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;->Mirror:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    if-ne v2, v3, :cond_1

    .line 252
    new-instance v2, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutMirror;

    invoke-direct {v2, p0, v0, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutMirror;-><init>(Lepson/print/copy/Component/ecopycomponent/ECopyPreview;II)V

    iput-object v2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->layout:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout;

    goto :goto_1

    .line 253
    :cond_1
    sget-object v3, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;->Borderless:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    if-ne v2, v3, :cond_2

    .line 254
    new-instance v2, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;

    invoke-direct {v2, p0, v0, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;-><init>(Lepson/print/copy/Component/ecopycomponent/ECopyPreview;II)V

    iput-object v2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->layout:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout;

    goto :goto_1

    .line 256
    :cond_2
    new-instance v2, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;

    invoke-direct {v2, p0, v0, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;-><init>(Lepson/print/copy/Component/ecopycomponent/ECopyPreview;II)V

    iput-object v2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->layout:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout;

    goto :goto_1

    .line 250
    :cond_3
    :goto_0
    new-instance v2, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;

    invoke-direct {v2, p0, v0, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout2Up;-><init>(Lepson/print/copy/Component/ecopycomponent/ECopyPreview;II)V

    iput-object v2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->layout:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout;

    .line 258
    :goto_1
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->layout:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout;

    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->scaleOption:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    invoke-virtual {v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedValue()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x42c80000    # 100.0f

    div-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout;->setCopyScale(F)V

    return-void
.end method

.method public setBackground(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .line 77
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->backgroundDrawable:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method public setColor(Lepson/print/copy/Component/ecopycomponent/ECopyPreview$ECopyPreviewColor;)V
    .locals 2

    .line 159
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->ColorEffectsType_Color:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 160
    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$ECopyPreviewColor;->Monochrome:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$ECopyPreviewColor;

    if-ne p1, v1, :cond_0

    .line 161
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->ColorEffectsType_MonochromeGrayscale:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 163
    :cond_0
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->colorOption:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    invoke-virtual {p1, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->selectChoice(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;)V

    .line 164
    invoke-virtual {p0}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->requestInvalidateView()V

    return-void
.end method

.method public setCopies(I)V
    .locals 1

    .line 208
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->copiesOpiton:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    invoke-virtual {v0, p1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->selectValue(I)V

    return-void
.end method

.method public setCopyFramePaint(Landroid/graphics/Paint;)V
    .locals 0

    .line 100
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->copyFramePaint:Landroid/graphics/Paint;

    return-void
.end method

.method public setPaperDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .line 87
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->paperDrawable:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method protected setPaperFrame(Landroid/graphics/RectF;)V
    .locals 1

    .line 290
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->paperFrame:Landroid/graphics/RectF;

    .line 291
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->layout:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout;

    invoke-virtual {v0, p1}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout;->setPaperFrame(Landroid/graphics/RectF;)V

    .line 292
    invoke-virtual {p0}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->requestInvalidateView()V

    return-void
.end method

.method public setPaperPadding(Landroid/graphics/Rect;)V
    .locals 1

    .line 120
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, p1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->paperPadding:Landroid/graphics/Rect;

    .line 121
    invoke-virtual {p0}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->resize()V

    return-void
.end method

.method public setPaperShadowPaint(Landroid/graphics/Paint;)V
    .locals 0

    .line 110
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->paperShadowPaint:Landroid/graphics/Paint;

    return-void
.end method

.method public setScale(I)V
    .locals 2

    .line 188
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->scaleOption:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    invoke-virtual {v0, p1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->selectValue(I)V

    .line 189
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->layout:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout;

    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->scaleOption:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedValue()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x42c80000    # 100.0f

    div-float/2addr v0, v1

    invoke-virtual {p1, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout;->setCopyScale(F)V

    .line 190
    invoke-virtual {p0}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->requestInvalidateView()V

    return-void
.end method

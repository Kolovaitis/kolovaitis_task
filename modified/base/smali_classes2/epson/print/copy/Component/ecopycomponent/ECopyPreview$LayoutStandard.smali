.class Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;
.super Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout;
.source "ECopyPreview.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/copy/Component/ecopycomponent/ECopyPreview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "LayoutStandard"
.end annotation


# instance fields
.field autofitEnabled:Z

.field canvas:Landroid/graphics/Canvas;

.field copyFrame:Landroid/graphics/RectF;

.field copyScale:F

.field imageScale:F

.field layoutHeight:I

.field layoutWidth:I

.field limitCopyScaleDown:F

.field maxPaperImage:Landroid/graphics/Bitmap;

.field offset:Landroid/graphics/Point;

.field paper:Landroid/graphics/RectF;

.field paperBorderClipPath:Landroid/graphics/Path;

.field paperImage:Landroid/graphics/Bitmap;

.field paperSize:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

.field scanFrame:Landroid/graphics/RectF;

.field scanImage:Landroid/graphics/Bitmap;

.field final synthetic this$0:Lepson/print/copy/Component/ecopycomponent/ECopyPreview;


# direct methods
.method constructor <init>(Lepson/print/copy/Component/ecopycomponent/ECopyPreview;II)V
    .locals 8

    .line 494
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->this$0:Lepson/print/copy/Component/ecopycomponent/ECopyPreview;

    invoke-direct {p0}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout;-><init>()V

    .line 495
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->offset:Landroid/graphics/Point;

    .line 496
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->paperBorderClipPath:Landroid/graphics/Path;

    const/4 v0, 0x0

    .line 497
    iput-boolean v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->autofitEnabled:Z

    const/high16 v0, 0x3f800000    # 1.0f

    .line 498
    iput v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->copyScale:F

    .line 499
    iput v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->imageScale:F

    const/4 v0, 0x0

    .line 500
    iput v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->limitCopyScaleDown:F

    .line 501
    iput p2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->layoutWidth:I

    .line 502
    iput p3, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->layoutHeight:I

    .line 504
    iget-object v1, p1, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XScale:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v1, v2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionItemOf(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v1

    .line 505
    invoke-virtual {v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v1

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_Autofit:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    .line 506
    iput-boolean v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->autofitEnabled:Z

    .line 508
    :cond_0
    iget-object v1, p1, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v1, v2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionItemOf(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object v1

    .line 509
    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$2;->$SwitchMap$epson$print$copy$Component$ecopycomponent$ECopyOptionItem$ECopyOptionItemChoice:[I

    invoke-virtual {v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v1

    invoke-virtual {v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    .line 517
    iget-object v1, p1, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->Max:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    iput-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->paperSize:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    goto :goto_0

    .line 516
    :pswitch_0
    iget-object v1, p1, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->P6:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    iput-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->paperSize:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    goto :goto_0

    .line 515
    :pswitch_1
    iget-object v1, p1, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->KG:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    iput-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->paperSize:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    goto :goto_0

    .line 514
    :pswitch_2
    iget-object v1, p1, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->Hagaki:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    iput-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->paperSize:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    goto :goto_0

    .line 513
    :pswitch_3
    iget-object v1, p1, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->L2:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    iput-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->paperSize:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    goto :goto_0

    .line 512
    :pswitch_4
    iget-object v1, p1, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->L:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    iput-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->paperSize:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    goto :goto_0

    .line 511
    :pswitch_5
    iget-object v1, p1, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->B5:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    iput-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->paperSize:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    goto :goto_0

    .line 510
    :pswitch_6
    iget-object v1, p1, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->A4:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    iput-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->paperSize:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    :goto_0
    int-to-float p2, p2

    int-to-float p3, p3

    .line 521
    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->paperSize:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    iget v1, v1, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;->width:I

    int-to-float v5, v1

    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->paperSize:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    iget v1, v1, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;->height:I

    int-to-float v6, v1

    sget-object v7, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;->Min:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;

    move-object v2, p0

    move v3, p2

    move v4, p3

    invoke-virtual/range {v2 .. v7}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->getAspectFitScale(FFFFLepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;)F

    move-result v1

    .line 522
    new-instance v2, Landroid/graphics/RectF;

    iget-object v3, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->paperSize:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    iget v3, v3, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;->width:I

    int-to-float v3, v3

    iget-object v4, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->paperSize:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    iget v4, v4, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;->height:I

    int-to-float v4, v4

    invoke-direct {v2, v0, v0, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 523
    invoke-virtual {p0, v1, v2}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->scaleRect(FLandroid/graphics/RectF;)V

    .line 524
    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    float-to-int v2, v2

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v2, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->paperImage:Landroid/graphics/Bitmap;

    .line 525
    new-instance v2, Landroid/graphics/RectF;

    iget-object v3, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->paperSize:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    iget v3, v3, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;->width:I

    int-to-float v3, v3

    iget-object v4, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->paperSize:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    iget v4, v4, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;->height:I

    int-to-float v4, v4

    invoke-direct {v2, v0, v0, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 526
    iget v3, v2, Landroid/graphics/RectF;->left:F

    const/high16 v4, 0x41900000    # 18.0f

    add-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->left:F

    .line 527
    iget v3, v2, Landroid/graphics/RectF;->top:F

    add-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->top:F

    .line 528
    iget v3, v2, Landroid/graphics/RectF;->right:F

    sub-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->right:F

    .line 529
    iget v3, v2, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->bottom:F

    .line 530
    invoke-virtual {p0, v1, v2}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->scaleRect(FLandroid/graphics/RectF;)V

    .line 531
    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->paperBorderClipPath:Landroid/graphics/Path;

    sget-object v3, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->addRect(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    .line 534
    iget-object v1, p1, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->Max:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    iget v1, v1, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;->width:I

    int-to-float v5, v1

    iget-object v1, p1, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->Max:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    iget v1, v1, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;->height:I

    int-to-float v6, v1

    sget-object v7, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;->Min:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;

    move-object v2, p0

    move v3, p2

    move v4, p3

    invoke-virtual/range {v2 .. v7}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->getAspectFitScale(FFFFLepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;)F

    move-result p2

    .line 535
    new-instance p3, Landroid/graphics/RectF;

    iget-object v1, p1, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->Max:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    iget v1, v1, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;->width:I

    int-to-float v1, v1

    iget-object p1, p1, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->Max:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    iget p1, p1, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;->height:I

    int-to-float p1, p1

    invoke-direct {p3, v0, v0, v1, p1}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 536
    invoke-virtual {p0, p2, p3}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->scaleRect(FLandroid/graphics/RectF;)V

    .line 537
    invoke-virtual {p3}, Landroid/graphics/RectF;->width()F

    move-result p1

    float-to-int p1, p1

    invoke-virtual {p3}, Landroid/graphics/RectF;->height()F

    move-result p2

    float-to-int p2, p2

    sget-object p3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, p3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object p1

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->maxPaperImage:Landroid/graphics/Bitmap;

    .line 540
    invoke-virtual {p0}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->invalidatePaperSize()V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method calcLimitCopyScaleDown()V
    .locals 7

    .line 651
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->copyFrame:Landroid/graphics/RectF;

    iget v2, v0, Landroid/graphics/RectF;->right:F

    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->copyFrame:Landroid/graphics/RectF;

    iget v3, v0, Landroid/graphics/RectF;->bottom:F

    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->paper:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v4

    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->paper:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v5

    sget-object v6, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;->Max:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;

    move-object v1, p0

    invoke-virtual/range {v1 .. v6}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->getAspectFitScale(FFFFLepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;)F

    move-result v0

    iput v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->limitCopyScaleDown:F

    return-void
.end method

.method getBitmap()Landroid/graphics/Bitmap;
    .locals 10

    .line 545
    invoke-virtual {p0}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->showPaper()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 546
    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->paperImage:Landroid/graphics/Bitmap;

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->maxPaperImage:Landroid/graphics/Bitmap;

    .line 547
    :goto_0
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->canvas:Landroid/graphics/Canvas;

    .line 548
    iget-object v2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->canvas:Landroid/graphics/Canvas;

    invoke-virtual {v2}, Landroid/graphics/Canvas;->save()I

    .line 550
    iget-object v2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->this$0:Lepson/print/copy/Component/ecopycomponent/ECopyPreview;

    iget-object v2, v2, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->paperDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v5, 0x0

    invoke-virtual {v2, v5, v5, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 551
    iget-object v2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->this$0:Lepson/print/copy/Component/ecopycomponent/ECopyPreview;

    iget-object v2, v2, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->paperDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->canvas:Landroid/graphics/Canvas;

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 553
    iget-object v2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->scanImage:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_3

    const/4 v3, 0x0

    if-eqz v0, :cond_2

    .line 555
    iget v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->copyScale:F

    iget v2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->limitCopyScaleDown:F

    div-float/2addr v0, v2

    .line 556
    iget-boolean v2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->autofitEnabled:Z

    if-eqz v2, :cond_1

    const/high16 v0, 0x3f800000    # 1.0f

    .line 557
    iget-object v2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->scanFrame:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v5

    iget-object v2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->scanFrame:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v6

    iget-object v2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->canvas:Landroid/graphics/Canvas;

    invoke-virtual {v2}, Landroid/graphics/Canvas;->getWidth()I

    move-result v2

    int-to-float v7, v2

    iget-object v2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->canvas:Landroid/graphics/Canvas;

    invoke-virtual {v2}, Landroid/graphics/Canvas;->getHeight()I

    move-result v2

    int-to-float v8, v2

    sget-object v9, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;->Max:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;

    move-object v4, p0

    invoke-virtual/range {v4 .. v9}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->getAspectFitScale(FFFFLepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;)F

    move-result v2

    div-float/2addr v0, v2

    .line 559
    :cond_1
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 560
    invoke-virtual {v2, v0, v0}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 561
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->canvas:Landroid/graphics/Canvas;

    iget-object v4, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->paperBorderClipPath:Landroid/graphics/Path;

    invoke-virtual {v0, v4}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;)Z

    .line 562
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->canvas:Landroid/graphics/Canvas;

    iget-object v4, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->scanImage:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v4, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    goto :goto_1

    .line 564
    :cond_2
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->canvas:Landroid/graphics/Canvas;

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v4, v4, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 568
    :cond_3
    :goto_1
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->canvas:Landroid/graphics/Canvas;

    invoke-virtual {v0}, Landroid/graphics/Canvas;->restore()V

    return-object v1
.end method

.method getCopyFrame()Landroid/graphics/RectF;
    .locals 4

    .line 608
    invoke-virtual {p0}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->showPaper()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return-object v1

    .line 612
    :cond_0
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->scanImage:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    return-object v1

    .line 616
    :cond_1
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->copyFrame:Landroid/graphics/RectF;

    if-nez v0, :cond_2

    return-object v1

    .line 620
    :cond_2
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1, v0}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    const/high16 v0, 0x3f800000    # 1.0f

    .line 621
    iget v2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->copyScale:F

    iget v3, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->limitCopyScaleDown:F

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    div-float/2addr v0, v2

    .line 622
    iget v2, v1, Landroid/graphics/RectF;->right:F

    mul-float v2, v2, v0

    iput v2, v1, Landroid/graphics/RectF;->right:F

    .line 623
    iget v2, v1, Landroid/graphics/RectF;->bottom:F

    mul-float v2, v2, v0

    iput v2, v1, Landroid/graphics/RectF;->bottom:F

    return-object v1
.end method

.method getOffset()Landroid/graphics/Point;
    .locals 2

    .line 587
    new-instance v0, Landroid/graphics/Point;

    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->offset:Landroid/graphics/Point;

    invoke-direct {v0, v1}, Landroid/graphics/Point;-><init>(Landroid/graphics/Point;)V

    return-object v0
.end method

.method getPaperRect()Landroid/graphics/RectF;
    .locals 4

    .line 656
    new-instance v0, Landroid/graphics/RectF;

    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->paper:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    iget-object v2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->paper:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    const/4 v3, 0x0

    invoke-direct {v0, v3, v3, v1, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    return-object v0
.end method

.method getScanImageScale(II)F
    .locals 7

    .line 574
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->this$0:Lepson/print/copy/Component/ecopycomponent/ECopyPreview;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->drawSize:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    iget v0, v0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;->width:I

    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->this$0:Lepson/print/copy/Component/ecopycomponent/ECopyPreview;

    iget-object v1, v1, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->paperPadding:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->this$0:Lepson/print/copy/Component/ecopycomponent/ECopyPreview;

    iget-object v1, v1, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->paperPadding:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    sub-int/2addr v0, v1

    int-to-float v2, v0

    .line 575
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->this$0:Lepson/print/copy/Component/ecopycomponent/ECopyPreview;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->drawSize:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    iget v0, v0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;->height:I

    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->this$0:Lepson/print/copy/Component/ecopycomponent/ECopyPreview;

    iget-object v1, v1, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->paperPadding:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v1

    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->this$0:Lepson/print/copy/Component/ecopycomponent/ECopyPreview;

    iget-object v1, v1, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->paperPadding:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v0, v1

    int-to-float v3, v0

    int-to-float v4, p1

    int-to-float v5, p2

    .line 576
    sget-object v6, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;->Min:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;

    move-object v1, p0

    invoke-virtual/range {v1 .. v6}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->getAspectFitScale(FFFFLepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;)F

    move-result p1

    iput p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->imageScale:F

    .line 577
    iget p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->imageScale:F

    return p1
.end method

.method invalidatePaperSize()V
    .locals 8

    .line 642
    invoke-virtual {p0}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->showPaper()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->paperSize:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->this$0:Lepson/print/copy/Component/ecopycomponent/ECopyPreview;

    iget-object v0, v0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->Max:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;

    .line 643
    :goto_0
    iget v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->layoutWidth:I

    int-to-float v3, v1

    iget v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->layoutHeight:I

    int-to-float v4, v1

    iget v1, v0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;->width:I

    int-to-float v5, v1

    iget v1, v0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;->height:I

    int-to-float v6, v1

    sget-object v7, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;->Min:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;

    move-object v2, p0

    invoke-virtual/range {v2 .. v7}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->getAspectFitScale(FFFFLepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;)F

    move-result v1

    .line 644
    new-instance v2, Landroid/graphics/RectF;

    iget v3, v0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;->width:I

    int-to-float v3, v3

    iget v0, v0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Size;->height:I

    int-to-float v0, v0

    const/4 v4, 0x0

    invoke-direct {v2, v4, v4, v3, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->paper:Landroid/graphics/RectF;

    .line 645
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->paper:Landroid/graphics/RectF;

    invoke-virtual {p0, v1, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->scaleRect(FLandroid/graphics/RectF;)V

    .line 646
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->offset:Landroid/graphics/Point;

    iget v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->layoutWidth:I

    int-to-float v1, v1

    iget-object v2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->paper:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->right:F

    sub-float/2addr v1, v2

    float-to-int v1, v1

    div-int/lit8 v1, v1, 0x2

    iput v1, v0, Landroid/graphics/Point;->x:I

    .line 647
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->offset:Landroid/graphics/Point;

    iget v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->layoutHeight:I

    int-to-float v1, v1

    iget-object v2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->paper:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v1, v2

    float-to-int v1, v1

    div-int/lit8 v1, v1, 0x2

    iput v1, v0, Landroid/graphics/Point;->y:I

    return-void
.end method

.method setCopyScale(F)V
    .locals 0

    .line 629
    iput p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->copyScale:F

    .line 630
    invoke-virtual {p0}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->invalidatePaperSize()V

    return-void
.end method

.method setPaperFrame(Landroid/graphics/RectF;)V
    .locals 2

    .line 592
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, p1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->scanFrame:Landroid/graphics/RectF;

    .line 593
    iget v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->imageScale:F

    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->scanFrame:Landroid/graphics/RectF;

    invoke-virtual {p0, v0, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->scaleRect(FLandroid/graphics/RectF;)V

    .line 595
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, p1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->copyFrame:Landroid/graphics/RectF;

    .line 596
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->copyFrame:Landroid/graphics/RectF;

    iget v0, p1, Landroid/graphics/RectF;->left:F

    const/high16 v1, 0x41900000    # 18.0f

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->left:F

    .line 597
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->copyFrame:Landroid/graphics/RectF;

    iget v0, p1, Landroid/graphics/RectF;->top:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->top:F

    .line 598
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->copyFrame:Landroid/graphics/RectF;

    iget v0, p1, Landroid/graphics/RectF;->right:F

    const/high16 v1, 0x42100000    # 36.0f

    sub-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->right:F

    .line 599
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->copyFrame:Landroid/graphics/RectF;

    iget v0, p1, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    .line 601
    iget p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->imageScale:F

    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->copyFrame:Landroid/graphics/RectF;

    invoke-virtual {p0, p1, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->scaleRect(FLandroid/graphics/RectF;)V

    .line 602
    invoke-virtual {p0}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->calcLimitCopyScaleDown()V

    .line 603
    invoke-virtual {p0}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->invalidatePaperSize()V

    return-void
.end method

.method setScanImage(Landroid/graphics/Bitmap;)V
    .locals 0

    .line 582
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->scanImage:Landroid/graphics/Bitmap;

    return-void
.end method

.method showPaper()Z
    .locals 3

    .line 634
    iget v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->copyScale:F

    iget v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->limitCopyScaleDown:F

    const/4 v2, 0x1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 635
    :goto_0
    iget-boolean v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;->autofitEnabled:Z

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

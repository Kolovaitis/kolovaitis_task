.class Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;
.super Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;
.source "ECopyPreview.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/copy/Component/ecopycomponent/ECopyPreview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "LayoutBorderless"
.end annotation


# instance fields
.field BO:I

.field LO:I

.field RO:I

.field TO:I

.field final synthetic this$0:Lepson/print/copy/Component/ecopycomponent/ECopyPreview;


# direct methods
.method constructor <init>(Lepson/print/copy/Component/ecopycomponent/ECopyPreview;II)V
    .locals 0

    .line 708
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->this$0:Lepson/print/copy/Component/ecopycomponent/ECopyPreview;

    .line 709
    invoke-direct {p0, p1, p2, p3}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutStandard;-><init>(Lepson/print/copy/Component/ecopycomponent/ECopyPreview;II)V

    .line 710
    iget-object p1, p1, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    sget-object p2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {p1, p2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;->getCopyOptionItemOf(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;)Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    move-result-object p1

    .line 711
    invoke-virtual {p1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object p1

    .line 712
    sget-object p2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_KG:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    const/16 p3, 0xf

    if-eq p1, p2, :cond_1

    sget-object p2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_L:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    if-eq p1, p2, :cond_1

    sget-object p2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_Postcard:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    if-ne p1, p2, :cond_0

    goto :goto_0

    .line 720
    :cond_0
    iput p3, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->LO:I

    .line 721
    iput p3, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->RO:I

    const/16 p1, 0x11

    .line 722
    iput p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->TO:I

    .line 723
    iput p3, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->BO:I

    goto :goto_1

    .line 715
    :cond_1
    :goto_0
    iput p3, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->LO:I

    .line 716
    iput p3, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->RO:I

    const/16 p1, 0x8

    .line 717
    iput p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->TO:I

    .line 718
    iput p3, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->BO:I

    :goto_1
    return-void
.end method


# virtual methods
.method getBitmap()Landroid/graphics/Bitmap;
    .locals 12

    .line 729
    invoke-virtual {p0}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->showPaper()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 730
    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->paperImage:Landroid/graphics/Bitmap;

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->maxPaperImage:Landroid/graphics/Bitmap;

    .line 731
    :goto_0
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->canvas:Landroid/graphics/Canvas;

    .line 732
    iget-object v2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->canvas:Landroid/graphics/Canvas;

    invoke-virtual {v2}, Landroid/graphics/Canvas;->save()I

    .line 734
    iget-object v2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->this$0:Lepson/print/copy/Component/ecopycomponent/ECopyPreview;

    iget-object v2, v2, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->paperDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v5, 0x0

    invoke-virtual {v2, v5, v5, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 735
    iget-object v2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->this$0:Lepson/print/copy/Component/ecopycomponent/ECopyPreview;

    iget-object v2, v2, Lepson/print/copy/Component/ecopycomponent/ECopyPreview;->paperDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->canvas:Landroid/graphics/Canvas;

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 737
    iget-object v2, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->scanImage:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_3

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    .line 739
    iget v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->copyScale:F

    .line 740
    iget-boolean v3, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->autofitEnabled:Z

    if-eqz v3, :cond_1

    .line 741
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->paperImage:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iget v3, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->LO:I

    add-int/2addr v0, v3

    iget v3, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->RO:I

    add-int/2addr v0, v3

    .line 742
    iget-object v3, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->paperImage:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    iget v4, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->TO:I

    add-int/2addr v3, v4

    iget v4, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->BO:I

    add-int/2addr v3, v4

    const/high16 v4, 0x3f800000    # 1.0f

    .line 743
    iget-object v5, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->scanFrame:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v7

    iget-object v5, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->scanFrame:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v8

    int-to-float v9, v0

    int-to-float v10, v3

    sget-object v11, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;->Min:Lepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;

    move-object v6, p0

    invoke-virtual/range {v6 .. v11}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->getAspectFitScale(FFFFLepson/print/copy/Component/ecopycomponent/ECopyPreview$Layout$AspectFit;)F

    move-result v0

    div-float v0, v4, v0

    .line 746
    :cond_1
    new-instance v3, Landroid/graphics/Matrix;

    invoke-direct {v3}, Landroid/graphics/Matrix;-><init>()V

    .line 747
    iget v4, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->LO:I

    neg-int v4, v4

    int-to-float v4, v4

    iget v5, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->TO:I

    neg-int v5, v5

    int-to-float v5, v5

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 748
    invoke-virtual {v3, v0, v0}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 749
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->canvas:Landroid/graphics/Canvas;

    iget-object v4, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->scanImage:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v4, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    goto :goto_1

    .line 751
    :cond_2
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->canvas:Landroid/graphics/Canvas;

    iget-object v3, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->scanImage:Landroid/graphics/Bitmap;

    iget v4, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->LO:I

    neg-int v4, v4

    int-to-float v4, v4

    iget v5, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->TO:I

    neg-int v5, v5

    int-to-float v5, v5

    invoke-virtual {v0, v3, v4, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 755
    :cond_3
    :goto_1
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->canvas:Landroid/graphics/Canvas;

    invoke-virtual {v0}, Landroid/graphics/Canvas;->restore()V

    return-object v1
.end method

.method setPaperFrame(Landroid/graphics/RectF;)V
    .locals 2

    .line 761
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, p1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->scanFrame:Landroid/graphics/RectF;

    .line 762
    iget v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->imageScale:F

    iget-object v1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->scanFrame:Landroid/graphics/RectF;

    invoke-virtual {p0, v0, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->scaleRect(FLandroid/graphics/RectF;)V

    .line 764
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, p1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    iput-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->copyFrame:Landroid/graphics/RectF;

    .line 765
    iget p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->imageScale:F

    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->copyFrame:Landroid/graphics/RectF;

    invoke-virtual {p0, p1, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->scaleRect(FLandroid/graphics/RectF;)V

    .line 767
    invoke-virtual {p0}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->calcLimitCopyScaleDown()V

    .line 768
    invoke-virtual {p0}, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->invalidatePaperSize()V

    .line 770
    iget p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->LO:I

    int-to-float p1, p1

    iget v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->imageScale:F

    mul-float p1, p1, v0

    float-to-int p1, p1

    iput p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->LO:I

    .line 771
    iget p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->RO:I

    int-to-float p1, p1

    iget v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->imageScale:F

    mul-float p1, p1, v0

    float-to-int p1, p1

    iput p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->RO:I

    .line 772
    iget p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->TO:I

    int-to-float p1, p1

    iget v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->imageScale:F

    mul-float p1, p1, v0

    float-to-int p1, p1

    iput p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->TO:I

    .line 773
    iget p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->BO:I

    int-to-float p1, p1

    iget v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->imageScale:F

    mul-float p1, p1, v0

    float-to-int p1, p1

    iput p1, p0, Lepson/print/copy/Component/ecopycomponent/ECopyPreview$LayoutBorderless;->BO:I

    return-void
.end method

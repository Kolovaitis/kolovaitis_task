.class Lepson/print/copy/Component/ecopycomponent/PreviewScanTask$3;
.super Ljava/lang/Object;
.source "PreviewScanTask.java"

# interfaces
.implements Lepson/print/copy/Component/ecopycomponent/ECopyPreview$ScanImageSizeCalculator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;->setScanImageSize(II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;


# direct methods
.method constructor <init>(Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;)V
    .locals 0

    .line 166
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask$3;->this$0:Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScanImageSize(IIF)V
    .locals 1

    .line 169
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask$3;->this$0:Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;

    iput p3, v0, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;->scale:F

    .line 170
    sget-object p3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, p3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object p1

    iput-object p1, v0, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;->previewScanBitmap:Landroid/graphics/Bitmap;

    .line 171
    iget-object p1, p0, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask$3;->this$0:Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;

    new-instance p2, Landroid/graphics/Canvas;

    iget-object p3, p1, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;->previewScanBitmap:Landroid/graphics/Bitmap;

    invoke-direct {p2, p3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object p2, p1, Lepson/print/copy/Component/ecopycomponent/PreviewScanTask;->canvas:Landroid/graphics/Canvas;

    return-void
.end method

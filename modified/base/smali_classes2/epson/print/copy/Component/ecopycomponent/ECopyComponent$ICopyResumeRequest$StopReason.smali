.class public final enum Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;
.super Ljava/lang/Enum;
.source "ECopyComponent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "StopReason"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

.field public static final enum ChangePage:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

.field public static final enum ManualfeedGuide:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

.field public static final enum None:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

.field public static final enum PrinterCoverOpenError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

.field public static final enum PrinterInputTrayMissingError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

.field public static final enum PrinterMarkerSupplyEmptyError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

.field public static final enum PrinterMarkerWasteFullError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

.field public static final enum PrinterMediaEmptyError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

.field public static final enum PrinterMediaJamError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

.field public static final enum PrinterOtherError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

.field public static final enum PrinterOutputAreaFullError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

.field public static final enum ScannerMediaEmptyError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

.field public static final enum ScannerMediaJamError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

.field public static final enum ScannerMediaSizeMissmatchError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

.field public static final enum ScannerOtherError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;


# instance fields
.field string:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 217
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    const-string v1, "None"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->None:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    .line 221
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    const-string v1, "ChangePage"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->ChangePage:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    .line 225
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    const-string v1, "PrinterMarkerSupplyEmptyError"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->PrinterMarkerSupplyEmptyError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    .line 229
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    const-string v1, "PrinterMarkerWasteFullError"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->PrinterMarkerWasteFullError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    .line 233
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    const-string v1, "PrinterMediaJamError"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->PrinterMediaJamError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    .line 237
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    const-string v1, "PrinterMediaEmptyError"

    const/4 v7, 0x5

    invoke-direct {v0, v1, v7}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->PrinterMediaEmptyError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    .line 241
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    const-string v1, "ManualfeedGuide"

    const/4 v8, 0x6

    invoke-direct {v0, v1, v8}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->ManualfeedGuide:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    .line 245
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    const-string v1, "PrinterInputTrayMissingError"

    const/4 v9, 0x7

    invoke-direct {v0, v1, v9}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->PrinterInputTrayMissingError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    .line 249
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    const-string v1, "PrinterCoverOpenError"

    const/16 v10, 0x8

    invoke-direct {v0, v1, v10}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->PrinterCoverOpenError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    .line 253
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    const-string v1, "PrinterOutputAreaFullError"

    const/16 v11, 0x9

    invoke-direct {v0, v1, v11}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->PrinterOutputAreaFullError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    .line 257
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    const-string v1, "PrinterOtherError"

    const/16 v12, 0xa

    invoke-direct {v0, v1, v12}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->PrinterOtherError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    .line 262
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    const-string v1, "ScannerMediaEmptyError"

    const/16 v13, 0xb

    invoke-direct {v0, v1, v13}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->ScannerMediaEmptyError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    .line 267
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    const-string v1, "ScannerMediaJamError"

    const/16 v14, 0xc

    invoke-direct {v0, v1, v14}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->ScannerMediaJamError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    .line 272
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    const-string v1, "ScannerMediaSizeMissmatchError"

    const/16 v15, 0xd

    invoke-direct {v0, v1, v15}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->ScannerMediaSizeMissmatchError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    .line 276
    new-instance v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    const-string v1, "ScannerOtherError"

    const/16 v15, 0xe

    invoke-direct {v0, v1, v15}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->ScannerOtherError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    const/16 v0, 0xf

    .line 213
    new-array v0, v0, [Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->None:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->ChangePage:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    aput-object v1, v0, v3

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->PrinterMarkerSupplyEmptyError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    aput-object v1, v0, v4

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->PrinterMarkerWasteFullError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    aput-object v1, v0, v5

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->PrinterMediaJamError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    aput-object v1, v0, v6

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->PrinterMediaEmptyError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    aput-object v1, v0, v7

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->ManualfeedGuide:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    aput-object v1, v0, v8

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->PrinterInputTrayMissingError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    aput-object v1, v0, v9

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->PrinterCoverOpenError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    aput-object v1, v0, v10

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->PrinterOutputAreaFullError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    aput-object v1, v0, v11

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->PrinterOtherError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    aput-object v1, v0, v12

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->ScannerMediaEmptyError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    aput-object v1, v0, v13

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->ScannerMediaJamError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    aput-object v1, v0, v14

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->ScannerMediaSizeMissmatchError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->ScannerOtherError:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    aput-object v1, v0, v15

    sput-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->$VALUES:[Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 213
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;
    .locals 1

    .line 213
    const-class v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    return-object p0
.end method

.method public static values()[Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;
    .locals 1

    .line 213
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->$VALUES:[Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    invoke-virtual {v0}, [Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;

    return-object v0
.end method


# virtual methods
.method public getDebugString()Ljava/lang/String;
    .locals 1

    .line 280
    iget-object v0, p0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyResumeRequest$StopReason;->string:Ljava/lang/String;

    return-object v0
.end method

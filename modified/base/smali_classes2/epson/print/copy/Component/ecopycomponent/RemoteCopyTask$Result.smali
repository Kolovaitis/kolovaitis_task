.class Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Result;
.super Ljava/lang/Object;
.source "RemoteCopyTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Result"
.end annotation


# instance fields
.field taskResult:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

.field final synthetic this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;


# direct methods
.method public constructor <init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V
    .locals 0

    .line 86
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Result;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    invoke-virtual {p0, p2}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Result;->setResult(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    return-void
.end method

.method public constructor <init>(Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;)V
    .locals 0

    .line 90
    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Result;->this$0:Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    sget-object p1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->success:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p2, p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;->isNull(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 92
    sget-object p1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->x_failed_communication:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {p0, p1}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Result;->setResult(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    goto :goto_0

    .line 94
    :cond_0
    invoke-virtual {p2}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteReasonResult;->reason()Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    move-result-object p1

    invoke-virtual {p0, p1}, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Result;->setResult(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V

    :goto_0
    return-void
.end method


# virtual methods
.method setResult(Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;)V
    .locals 1

    .line 70
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$10;->$SwitchMap$epson$print$copy$Component$eremoteoperation$ERemoteOperation$ERemoteParam:[I

    invoke-virtual {p1}, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    .line 82
    sget-object p1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;->ErrorOther:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Result;->taskResult:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    goto :goto_0

    .line 81
    :pswitch_0
    sget-object p1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;->ErrorOther:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Result;->taskResult:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    goto :goto_0

    .line 80
    :pswitch_1
    sget-object p1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;->ErrorCommunication:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Result;->taskResult:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    goto :goto_0

    .line 79
    :pswitch_2
    sget-object p1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;->ErrorOther:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Result;->taskResult:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    goto :goto_0

    .line 78
    :pswitch_3
    sget-object p1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;->ErrorOther:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Result;->taskResult:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    goto :goto_0

    .line 77
    :pswitch_4
    sget-object p1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;->RemoveAdfPaper:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Result;->taskResult:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    goto :goto_0

    .line 76
    :pswitch_5
    sget-object p1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;->ErrorOther:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Result;->taskResult:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    goto :goto_0

    .line 75
    :pswitch_6
    sget-object p1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;->ErrorOther:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Result;->taskResult:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    goto :goto_0

    .line 74
    :pswitch_7
    sget-object p1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;->Busy:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Result;->taskResult:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    goto :goto_0

    .line 73
    :pswitch_8
    sget-object p1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;->Canceled:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Result;->taskResult:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    goto :goto_0

    .line 72
    :pswitch_9
    sget-object p1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;->Succeed:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Result;->taskResult:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    goto :goto_0

    .line 71
    :pswitch_a
    sget-object p1, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;->Succeed:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    iput-object p1, p0, Lepson/print/copy/Component/ecopycomponent/RemoteCopyTask$Result;->taskResult:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyStatusListener$CopyTaskResult;

    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

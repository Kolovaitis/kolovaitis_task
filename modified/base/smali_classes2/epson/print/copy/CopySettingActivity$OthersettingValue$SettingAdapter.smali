.class Lepson/print/copy/CopySettingActivity$OthersettingValue$SettingAdapter;
.super Landroid/widget/ArrayAdapter;
.source "CopySettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/copy/CopySettingActivity$OthersettingValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SettingAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private inflater:Landroid/view/LayoutInflater;

.field private items:[Ljava/lang/String;

.field final synthetic this$1:Lepson/print/copy/CopySettingActivity$OthersettingValue;


# direct methods
.method public constructor <init>(Lepson/print/copy/CopySettingActivity$OthersettingValue;Landroid/content/Context;I[Ljava/lang/String;)V
    .locals 0

    .line 153
    iput-object p1, p0, Lepson/print/copy/CopySettingActivity$OthersettingValue$SettingAdapter;->this$1:Lepson/print/copy/CopySettingActivity$OthersettingValue;

    .line 154
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 155
    iput-object p4, p0, Lepson/print/copy/CopySettingActivity$OthersettingValue$SettingAdapter;->items:[Ljava/lang/String;

    const-string p1, "layout_inflater"

    .line 156
    invoke-virtual {p2, p1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/LayoutInflater;

    iput-object p1, p0, Lepson/print/copy/CopySettingActivity$OthersettingValue$SettingAdapter;->inflater:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    if-nez p2, :cond_0

    .line 163
    iget-object p2, p0, Lepson/print/copy/CopySettingActivity$OthersettingValue$SettingAdapter;->inflater:Landroid/view/LayoutInflater;

    const p3, 0x7f0a00be

    const/4 v0, 0x0

    invoke-virtual {p2, p3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 165
    :cond_0
    iget-object p3, p0, Lepson/print/copy/CopySettingActivity$OthersettingValue$SettingAdapter;->items:[Ljava/lang/String;

    aget-object p3, p3, p1

    const v0, 0x7f0802f6

    .line 166
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 167
    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 168
    iget-object p3, p0, Lepson/print/copy/CopySettingActivity$OthersettingValue$SettingAdapter;->this$1:Lepson/print/copy/CopySettingActivity$OthersettingValue;

    iget p3, p3, Lepson/print/copy/CopySettingActivity$OthersettingValue;->selectedItemPos:I

    const v0, 0x7f0802f5

    if-ne p1, p3, :cond_1

    .line 169
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    const/4 p3, 0x0

    invoke-virtual {p1, p3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 171
    :cond_1
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    const/16 p3, 0x8

    invoke-virtual {p1, p3}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    return-object p2
.end method

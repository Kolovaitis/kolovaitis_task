.class Lepson/print/copy/CopyActivity$XDensityValue$CounterImage;
.super Lepson/print/copy/ActivityBase$NumberOptionValue$Counter;
.source "CopyActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/copy/CopyActivity$XDensityValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CounterImage"
.end annotation


# instance fields
.field final synthetic this$1:Lepson/print/copy/CopyActivity$XDensityValue;


# direct methods
.method public constructor <init>(Lepson/print/copy/CopyActivity$XDensityValue;I)V
    .locals 0

    .line 619
    iput-object p1, p0, Lepson/print/copy/CopyActivity$XDensityValue$CounterImage;->this$1:Lepson/print/copy/CopyActivity$XDensityValue;

    .line 620
    invoke-direct {p0, p1, p2}, Lepson/print/copy/ActivityBase$NumberOptionValue$Counter;-><init>(Lepson/print/copy/ActivityBase$NumberOptionValue;I)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 0

    .line 633
    iget p1, p0, Lepson/print/copy/CopyActivity$XDensityValue$CounterImage;->amount:I

    invoke-virtual {p0, p1}, Lepson/print/copy/CopyActivity$XDensityValue$CounterImage;->updateImage(I)V

    return-void
.end method

.method updateImage(I)V
    .locals 2

    .line 624
    iget-object v0, p0, Lepson/print/copy/CopyActivity$XDensityValue$CounterImage;->this$1:Lepson/print/copy/CopyActivity$XDensityValue;

    iget v1, v0, Lepson/print/copy/CopyActivity$XDensityValue;->value:I

    add-int/2addr v1, p1

    iput v1, v0, Lepson/print/copy/CopyActivity$XDensityValue;->value:I

    .line 625
    iget-object p1, p0, Lepson/print/copy/CopyActivity$XDensityValue$CounterImage;->this$1:Lepson/print/copy/CopyActivity$XDensityValue;

    iget-object p1, p1, Lepson/print/copy/CopyActivity$XDensityValue;->optionItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    iget-object v0, p0, Lepson/print/copy/CopyActivity$XDensityValue$CounterImage;->this$1:Lepson/print/copy/CopyActivity$XDensityValue;

    iget v0, v0, Lepson/print/copy/CopyActivity$XDensityValue;->value:I

    invoke-virtual {p1, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->selectValue(I)V

    .line 626
    iget-object p1, p0, Lepson/print/copy/CopyActivity$XDensityValue$CounterImage;->this$1:Lepson/print/copy/CopyActivity$XDensityValue;

    iget-object p1, p1, Lepson/print/copy/CopyActivity$XDensityValue;->changedListener:Lepson/print/copy/ActivityBase$OptionItemChangedListener;

    if-eqz p1, :cond_0

    .line 627
    iget-object p1, p0, Lepson/print/copy/CopyActivity$XDensityValue$CounterImage;->this$1:Lepson/print/copy/CopyActivity$XDensityValue;

    iget-object p1, p1, Lepson/print/copy/CopyActivity$XDensityValue;->changedListener:Lepson/print/copy/ActivityBase$OptionItemChangedListener;

    iget-object v0, p0, Lepson/print/copy/CopyActivity$XDensityValue$CounterImage;->this$1:Lepson/print/copy/CopyActivity$XDensityValue;

    iget-object v0, v0, Lepson/print/copy/CopyActivity$XDensityValue;->optionItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    invoke-interface {p1, v0}, Lepson/print/copy/ActivityBase$OptionItemChangedListener;->onOptionItemChanged(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    :cond_0
    return-void
.end method

.class Lepson/print/copy/CopySettingActivity$OthersettingValue;
.super Lepson/print/copy/ActivityBase$ListOptionValue;
.source "CopySettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/copy/CopySettingActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "OthersettingValue"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/copy/CopySettingActivity$OthersettingValue$SettingAdapter;
    }
.end annotation


# instance fields
.field selectedItemPos:I

.field final synthetic this$0:Lepson/print/copy/CopySettingActivity;


# direct methods
.method constructor <init>(Lepson/print/copy/CopySettingActivity;Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V
    .locals 0

    .line 101
    iput-object p1, p0, Lepson/print/copy/CopySettingActivity$OthersettingValue;->this$0:Lepson/print/copy/CopySettingActivity;

    invoke-direct {p0, p1}, Lepson/print/copy/ActivityBase$ListOptionValue;-><init>(Lepson/print/copy/ActivityBase;)V

    .line 102
    invoke-virtual {p0, p2}, Lepson/print/copy/CopySettingActivity$OthersettingValue;->bindOption(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    .line 103
    invoke-virtual {p0, p2, p1}, Lepson/print/copy/CopySettingActivity$OthersettingValue;->capabilitySetting(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;Landroid/content/Context;)V

    .line 104
    iget-object p1, p1, Lepson/print/copy/CopySettingActivity;->optionValueChangedListener:Lepson/print/copy/ActivityBase$OptionItemChangedListener;

    invoke-virtual {p0, p1}, Lepson/print/copy/CopySettingActivity$OthersettingValue;->setOptionValueChangedListener(Lepson/print/copy/ActivityBase$OptionItemChangedListener;)V

    return-void
.end method


# virtual methods
.method capabilitySetting(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;Landroid/content/Context;)V
    .locals 1

    .line 120
    iget-object p1, p0, Lepson/print/copy/CopySettingActivity$OthersettingValue;->this$0:Lepson/print/copy/CopySettingActivity;

    const v0, 0x7f0800f8

    invoke-virtual {p1, v0}, Lepson/print/copy/CopySettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ListView;

    .line 121
    new-instance v0, Lepson/print/copy/CopySettingActivity$OthersettingValue$1;

    invoke-direct {v0, p0}, Lepson/print/copy/CopySettingActivity$OthersettingValue$1;-><init>(Lepson/print/copy/CopySettingActivity$OthersettingValue;)V

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 135
    invoke-virtual {p0, p0, p2}, Lepson/print/copy/CopySettingActivity$OthersettingValue;->getArrayAdapter(Lepson/print/copy/CopySettingActivity$OthersettingValue;Landroid/content/Context;)Lepson/print/copy/CopySettingActivity$OthersettingValue$SettingAdapter;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    const/4 p1, 0x0

    .line 137
    :goto_0
    iget-object p2, p0, Lepson/print/copy/CopySettingActivity$OthersettingValue;->choices:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    if-ge p1, p2, :cond_1

    .line 138
    iget-object p2, p0, Lepson/print/copy/CopySettingActivity$OthersettingValue;->selected:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    iget-object v0, p0, Lepson/print/copy/CopySettingActivity$OthersettingValue;->choices:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-ne p2, v0, :cond_0

    .line 139
    iput p1, p0, Lepson/print/copy/CopySettingActivity$OthersettingValue;->selectedItemPos:I

    goto :goto_1

    :cond_0
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    return-void
.end method

.method getArrayAdapter(Lepson/print/copy/CopySettingActivity$OthersettingValue;Landroid/content/Context;)Lepson/print/copy/CopySettingActivity$OthersettingValue$SettingAdapter;
    .locals 2

    .line 109
    invoke-virtual {p1}, Lepson/print/copy/CopySettingActivity$OthersettingValue;->getKeyArray()[Ljava/lang/String;

    move-result-object p1

    .line 110
    new-instance v0, Lepson/print/copy/CopySettingActivity$OthersettingValue$SettingAdapter;

    const v1, 0x7f0a00be

    invoke-direct {v0, p0, p2, v1, p1}, Lepson/print/copy/CopySettingActivity$OthersettingValue$SettingAdapter;-><init>(Lepson/print/copy/CopySettingActivity$OthersettingValue;Landroid/content/Context;I[Ljava/lang/String;)V

    return-object v0
.end method

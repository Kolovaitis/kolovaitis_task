.class Lepson/print/copy/ActivityBase$ListOptionValue;
.super Lepson/print/copy/ActivityBase$OptionValue;
.source "ActivityBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/copy/ActivityBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ListOptionValue"
.end annotation


# instance fields
.field choices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;",
            ">;"
        }
    .end annotation
.end field

.field selected:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

.field final synthetic this$0:Lepson/print/copy/ActivityBase;


# direct methods
.method constructor <init>(Lepson/print/copy/ActivityBase;)V
    .locals 0

    .line 436
    iput-object p1, p0, Lepson/print/copy/ActivityBase$ListOptionValue;->this$0:Lepson/print/copy/ActivityBase;

    invoke-direct {p0, p1}, Lepson/print/copy/ActivityBase$OptionValue;-><init>(Lepson/print/copy/ActivityBase;)V

    return-void
.end method


# virtual methods
.method bindOption(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V
    .locals 1

    .line 441
    iput-object p1, p0, Lepson/print/copy/ActivityBase$ListOptionValue;->optionItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    .line 442
    invoke-virtual {p1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectableChoices()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lepson/print/copy/ActivityBase$ListOptionValue;->choices:Ljava/util/List;

    .line 443
    invoke-virtual {p1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object p1

    iput-object p1, p0, Lepson/print/copy/ActivityBase$ListOptionValue;->selected:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    return-void
.end method

.method getKeyArray()[Ljava/lang/String;
    .locals 4

    .line 451
    iget-object v0, p0, Lepson/print/copy/ActivityBase$ListOptionValue;->choices:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 452
    :goto_0
    iget-object v2, p0, Lepson/print/copy/ActivityBase$ListOptionValue;->choices:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 453
    iget-object v2, p0, Lepson/print/copy/ActivityBase$ListOptionValue;->choices:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    invoke-virtual {v2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->name()Ljava/lang/String;

    move-result-object v2

    .line 454
    iget-object v3, p0, Lepson/print/copy/ActivityBase$ListOptionValue;->this$0:Lepson/print/copy/ActivityBase;

    invoke-virtual {v3, v2}, Lepson/print/copy/ActivityBase;->string(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

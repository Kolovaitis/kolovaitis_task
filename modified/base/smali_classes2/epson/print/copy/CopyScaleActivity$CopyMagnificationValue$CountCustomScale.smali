.class Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$CountCustomScale;
.super Lepson/print/copy/ActivityBase$NumberOptionValue$Counter;
.source "CopyScaleActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CountCustomScale"
.end annotation


# instance fields
.field final synthetic this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;


# direct methods
.method public constructor <init>(Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;I)V
    .locals 0

    .line 353
    iput-object p1, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$CountCustomScale;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    .line 354
    invoke-direct {p0, p1, p2}, Lepson/print/copy/ActivityBase$NumberOptionValue$Counter;-><init>(Lepson/print/copy/ActivityBase$NumberOptionValue;I)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .line 361
    :try_start_0
    iget-object p1, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$CountCustomScale;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    iget-object v0, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$CountCustomScale;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    iget-object v0, v0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->editText:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p1, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->value:I

    .line 362
    iget-object p1, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$CountCustomScale;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    iget-object v0, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$CountCustomScale;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    iget-object v0, v0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->optionItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getMinimumValue()I

    move-result v0

    iget-object v1, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$CountCustomScale;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    iget v1, v1, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->value:I

    iget v2, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$CountCustomScale;->amount:I

    add-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget-object v1, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$CountCustomScale;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    iget-object v1, v1, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->optionItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    invoke-virtual {v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getMaximumValue()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p1, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->value:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 366
    :catch_0
    iget-object p1, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$CountCustomScale;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    iget-object p1, p1, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->editText:Landroid/widget/TextView;

    iget-object v0, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$CountCustomScale;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    iget v0, v0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->value:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 369
    iget-object p1, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$CountCustomScale;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    iget p1, p1, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->value:I

    iget-object v0, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$CountCustomScale;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    iget-object v0, v0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->optionItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getMaximumValue()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-lt p1, v0, :cond_0

    .line 370
    iget-object p1, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$CountCustomScale;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    iget-object p1, p1, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->countUp:Landroid/widget/Button;

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 372
    :cond_0
    iget-object p1, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$CountCustomScale;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    iget-object p1, p1, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->countUp:Landroid/widget/Button;

    invoke-virtual {p1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 375
    :goto_0
    iget-object p1, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$CountCustomScale;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    iget p1, p1, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->value:I

    iget-object v0, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$CountCustomScale;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    iget-object v0, v0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->optionItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getMinimumValue()I

    move-result v0

    if-gt p1, v0, :cond_1

    .line 376
    iget-object p1, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$CountCustomScale;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    iget-object p1, p1, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->countDown:Landroid/widget/Button;

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_1

    .line 378
    :cond_1
    iget-object p1, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$CountCustomScale;->this$1:Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;

    iget-object p1, p1, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->countDown:Landroid/widget/Button;

    invoke-virtual {p1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    :goto_1
    return-void
.end method

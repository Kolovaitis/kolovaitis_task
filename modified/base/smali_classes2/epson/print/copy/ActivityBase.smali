.class Lepson/print/copy/ActivityBase;
.super Lepson/print/ActivityIACommon;
.source "ActivityBase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/copy/ActivityBase$settingPreference;,
        Lepson/print/copy/ActivityBase$CopyMagnificationValue;,
        Lepson/print/copy/ActivityBase$NumberOptionValue;,
        Lepson/print/copy/ActivityBase$ListOptionValue;,
        Lepson/print/copy/ActivityBase$OptionValue;,
        Lepson/print/copy/ActivityBase$OptionItemChangedListener;,
        Lepson/print/copy/ActivityBase$INextPageClose;,
        Lepson/print/copy/ActivityBase$errorDialog;,
        Lepson/print/copy/ActivityBase$IClose;,
        Lepson/print/copy/ActivityBase$ClickButton;,
        Lepson/print/copy/ActivityBase$DialogButtons;,
        Lepson/print/copy/ActivityBase$WheelDialog;,
        Lepson/print/copy/ActivityBase$CancelRequestCallback;
    }
.end annotation


# static fields
.field static final Title:Ljava/lang/String; = "Title"

.field static printerId:Ljava/lang/String;

.field static printerIp:Ljava/lang/String;

.field static final strings:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private TAG:Ljava/lang/String;

.field copyComponent:Lepson/print/copy/Component/ecopycomponent/ECopyComponent;

.field copyType:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

.field isKeepSimpleAPConnection:Z

.field isTryConnectSimpleAp:Z

.field loading:Lepson/print/screen/WorkingDialog;

.field optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

.field optionValueMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;",
            "Lepson/print/copy/ActivityBase$OptionValue;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 45
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lepson/print/copy/ActivityBase;->strings:Ljava/util/HashMap;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .line 39
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    const-string v0, "ActivityBase"

    .line 40
    iput-object v0, p0, Lepson/print/copy/ActivityBase;->TAG:Ljava/lang/String;

    .line 49
    invoke-static {}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->sharedComponent()Lepson/print/copy/Component/ecopycomponent/ECopyComponent;

    move-result-object v0

    iput-object v0, p0, Lepson/print/copy/ActivityBase;->copyComponent:Lepson/print/copy/Component/ecopycomponent/ECopyComponent;

    .line 50
    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;->Standard:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    iput-object v0, p0, Lepson/print/copy/ActivityBase;->copyType:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    .line 52
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lepson/print/copy/ActivityBase;->optionValueMap:Ljava/util/HashMap;

    const/4 v0, 0x0

    .line 55
    iput-boolean v0, p0, Lepson/print/copy/ActivityBase;->isKeepSimpleAPConnection:Z

    .line 56
    iput-boolean v0, p0, Lepson/print/copy/ActivityBase;->isTryConnectSimpleAp:Z

    return-void
.end method

.method static synthetic access$000(Lepson/print/copy/ActivityBase;)Ljava/lang/String;
    .locals 0

    .line 39
    iget-object p0, p0, Lepson/print/copy/ActivityBase;->TAG:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method buildCopyOptions(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .line 136
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    .line 138
    new-instance p1, Lepson/print/screen/WorkingDialog;

    invoke-direct {p1, p0}, Lepson/print/screen/WorkingDialog;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lepson/print/copy/ActivityBase;->loading:Lepson/print/screen/WorkingDialog;

    .line 140
    sget-object p1, Lepson/print/copy/ActivityBase;->strings:Ljava/util/HashMap;

    invoke-static {p0, p1}, Lepson/print/copy/DisplayUtil;->setMap(Landroid/content/Context;Ljava/util/HashMap;)V

    return-void
.end method

.method showNextPageDialog(Lepson/print/copy/ActivityBase$INextPageClose;)V
    .locals 2

    .line 384
    new-instance v0, Lepson/print/copy/ActivityBase$1;

    invoke-direct {v0, p0, p1}, Lepson/print/copy/ActivityBase$1;-><init>(Lepson/print/copy/ActivityBase;Lepson/print/copy/ActivityBase$INextPageClose;)V

    .line 399
    new-instance p1, Landroid/app/AlertDialog$Builder;

    invoke-direct {p1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0e0545

    .line 400
    invoke-virtual {p1, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    const/4 v1, 0x1

    .line 401
    invoke-virtual {p1, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f0e03dd

    .line 402
    invoke-virtual {p0, v1}, Lepson/print/copy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f0e03dc

    .line 403
    invoke-virtual {p1, v1, v0}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f0e0476

    .line 404
    invoke-virtual {p0, v1}, Lepson/print/copy/ActivityBase;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 406
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 407
    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method string(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 539
    sget-object v0, Lepson/print/copy/ActivityBase;->strings:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lepson/print/copy/ActivityBase;->strings:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_0

    :cond_0
    sget-object p1, Lepson/print/copy/ActivityBase;->strings:Ljava/util/HashMap;

    const-string v0, "Unknown"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    :goto_0
    check-cast p1, Ljava/lang/String;

    return-object p1
.end method

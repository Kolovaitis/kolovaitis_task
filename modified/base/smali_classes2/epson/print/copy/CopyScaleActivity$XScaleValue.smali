.class Lepson/print/copy/CopyScaleActivity$XScaleValue;
.super Lepson/print/copy/ActivityBase$ListOptionValue;
.source "CopyScaleActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/copy/CopyScaleActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "XScaleValue"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/copy/CopyScaleActivity$XScaleValue$ScaleInfo;,
        Lepson/print/copy/CopyScaleActivity$XScaleValue$ScaleAdapter;
    }
.end annotation


# instance fields
.field final AUTOFIT:I

.field final CUSTOM:I

.field final FULLSIZE:I

.field presetArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/CopyScaleActivity$XScaleValue$ScaleInfo;",
            ">;"
        }
    .end annotation
.end field

.field selectedItemPos:I

.field final synthetic this$0:Lepson/print/copy/CopyScaleActivity;


# direct methods
.method constructor <init>(Lepson/print/copy/CopyScaleActivity;Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V
    .locals 1

    .line 97
    iput-object p1, p0, Lepson/print/copy/CopyScaleActivity$XScaleValue;->this$0:Lepson/print/copy/CopyScaleActivity;

    invoke-direct {p0, p1}, Lepson/print/copy/ActivityBase$ListOptionValue;-><init>(Lepson/print/copy/ActivityBase;)V

    const/4 v0, 0x0

    .line 90
    iput v0, p0, Lepson/print/copy/CopyScaleActivity$XScaleValue;->AUTOFIT:I

    const/4 v0, 0x1

    .line 91
    iput v0, p0, Lepson/print/copy/CopyScaleActivity$XScaleValue;->FULLSIZE:I

    const/4 v0, 0x2

    .line 92
    iput v0, p0, Lepson/print/copy/CopyScaleActivity$XScaleValue;->CUSTOM:I

    .line 95
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lepson/print/copy/CopyScaleActivity$XScaleValue;->presetArray:Ljava/util/ArrayList;

    .line 98
    invoke-virtual {p0, p2}, Lepson/print/copy/CopyScaleActivity$XScaleValue;->bindOption(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    .line 99
    invoke-virtual {p0, p2, p1}, Lepson/print/copy/CopyScaleActivity$XScaleValue;->capabilitySetting(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;Landroid/content/Context;)V

    .line 100
    iget-object p1, p1, Lepson/print/copy/CopyScaleActivity;->optionValueChangedListener:Lepson/print/copy/ActivityBase$OptionItemChangedListener;

    invoke-virtual {p0, p1}, Lepson/print/copy/CopyScaleActivity$XScaleValue;->setOptionValueChangedListener(Lepson/print/copy/ActivityBase$OptionItemChangedListener;)V

    return-void
.end method


# virtual methods
.method capabilitySetting(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;Landroid/content/Context;)V
    .locals 5

    const/4 p1, 0x0

    const/4 v0, 0x0

    .line 121
    :goto_0
    iget-object v1, p0, Lepson/print/copy/CopyScaleActivity$XScaleValue;->choices:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 122
    iget-object v1, p0, Lepson/print/copy/CopyScaleActivity$XScaleValue;->selected:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    iget-object v2, p0, Lepson/print/copy/CopyScaleActivity$XScaleValue;->choices:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 123
    iput v0, p0, Lepson/print/copy/CopyScaleActivity$XScaleValue;->selectedItemPos:I

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 129
    :cond_1
    :goto_1
    iget-object v0, p0, Lepson/print/copy/CopyScaleActivity$XScaleValue;->this$0:Lepson/print/copy/CopyScaleActivity;

    const v1, 0x7f08006a

    invoke-virtual {v0, v1}, Lepson/print/copy/CopyScaleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 130
    iget v1, p0, Lepson/print/copy/CopyScaleActivity$XScaleValue;->selectedItemPos:I

    const v2, 0x7f08006c

    const/16 v3, 0x8

    if-nez v1, :cond_2

    .line 131
    iget-object v1, p0, Lepson/print/copy/CopyScaleActivity$XScaleValue;->this$0:Lepson/print/copy/CopyScaleActivity;

    invoke-virtual {v1, v2}, Lepson/print/copy/CopyScaleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    .line 133
    :cond_2
    iget-object v1, p0, Lepson/print/copy/CopyScaleActivity$XScaleValue;->this$0:Lepson/print/copy/CopyScaleActivity;

    invoke-virtual {v1, v2}, Lepson/print/copy/CopyScaleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 135
    :goto_2
    new-instance v1, Lepson/print/copy/CopyScaleActivity$XScaleValue$1;

    invoke-direct {v1, p0}, Lepson/print/copy/CopyScaleActivity$XScaleValue$1;-><init>(Lepson/print/copy/CopyScaleActivity$XScaleValue;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 149
    iget-object v0, p0, Lepson/print/copy/CopyScaleActivity$XScaleValue;->this$0:Lepson/print/copy/CopyScaleActivity;

    const v1, 0x7f08015a

    invoke-virtual {v0, v1}, Lepson/print/copy/CopyScaleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 150
    iget v1, p0, Lepson/print/copy/CopyScaleActivity$XScaleValue;->selectedItemPos:I

    const v2, 0x7f080159

    const/4 v4, 0x1

    if-ne v1, v4, :cond_3

    .line 151
    iget-object v1, p0, Lepson/print/copy/CopyScaleActivity$XScaleValue;->this$0:Lepson/print/copy/CopyScaleActivity;

    invoke-virtual {v1, v2}, Lepson/print/copy/CopyScaleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_3

    .line 153
    :cond_3
    iget-object v1, p0, Lepson/print/copy/CopyScaleActivity$XScaleValue;->this$0:Lepson/print/copy/CopyScaleActivity;

    invoke-virtual {v1, v2}, Lepson/print/copy/CopyScaleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 155
    :goto_3
    new-instance v1, Lepson/print/copy/CopyScaleActivity$XScaleValue$2;

    invoke-direct {v1, p0}, Lepson/print/copy/CopyScaleActivity$XScaleValue$2;-><init>(Lepson/print/copy/CopyScaleActivity$XScaleValue;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 169
    iget v0, p0, Lepson/print/copy/CopyScaleActivity$XScaleValue;->selectedItemPos:I

    const/4 v1, 0x2

    const v2, 0x7f0800ff

    if-ne v0, v1, :cond_4

    .line 170
    iget-object v0, p0, Lepson/print/copy/CopyScaleActivity$XScaleValue;->this$0:Lepson/print/copy/CopyScaleActivity;

    invoke-virtual {v0, v2}, Lepson/print/copy/CopyScaleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_4

    .line 172
    :cond_4
    iget-object p1, p0, Lepson/print/copy/CopyScaleActivity$XScaleValue;->this$0:Lepson/print/copy/CopyScaleActivity;

    invoke-virtual {p1, v2}, Lepson/print/copy/CopyScaleActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    invoke-virtual {p1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 174
    :goto_4
    iget-object p1, p0, Lepson/print/copy/CopyScaleActivity$XScaleValue;->this$0:Lepson/print/copy/CopyScaleActivity;

    const v0, 0x7f0802d3

    invoke-virtual {p1, v0}, Lepson/print/copy/CopyScaleActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ListView;

    .line 175
    new-instance v0, Lepson/print/copy/CopyScaleActivity$XScaleValue$3;

    invoke-direct {v0, p0}, Lepson/print/copy/CopyScaleActivity$XScaleValue$3;-><init>(Lepson/print/copy/CopyScaleActivity$XScaleValue;)V

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 187
    invoke-virtual {p0, p0, p2}, Lepson/print/copy/CopyScaleActivity$XScaleValue;->getArrayAdapter(Lepson/print/copy/CopyScaleActivity$XScaleValue;Landroid/content/Context;)Lepson/print/copy/CopyScaleActivity$XScaleValue$ScaleAdapter;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method getArrayAdapter(Lepson/print/copy/CopyScaleActivity$XScaleValue;Landroid/content/Context;)Lepson/print/copy/CopyScaleActivity$XScaleValue$ScaleAdapter;
    .locals 4

    const/4 v0, 0x3

    .line 105
    :goto_0
    invoke-virtual {p1}, Lepson/print/copy/CopyScaleActivity$XScaleValue;->getKeyArray()[Ljava/lang/String;

    move-result-object v1

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 107
    new-instance v1, Lepson/print/copy/CopyScaleActivity$XScaleValue$ScaleInfo;

    iget-object v2, p0, Lepson/print/copy/CopyScaleActivity$XScaleValue;->this$0:Lepson/print/copy/CopyScaleActivity;

    iget-object v2, v2, Lepson/print/copy/CopyScaleActivity;->copyType:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    iget-object v3, p1, Lepson/print/copy/CopyScaleActivity$XScaleValue;->choices:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    invoke-direct {v1, p0, v2, v3}, Lepson/print/copy/CopyScaleActivity$XScaleValue$ScaleInfo;-><init>(Lepson/print/copy/CopyScaleActivity$XScaleValue;Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;)V

    .line 108
    iget-object v2, p0, Lepson/print/copy/CopyScaleActivity$XScaleValue;->presetArray:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 110
    :cond_0
    new-instance p1, Lepson/print/copy/CopyScaleActivity$XScaleValue$ScaleAdapter;

    iget-object v0, p0, Lepson/print/copy/CopyScaleActivity$XScaleValue;->presetArray:Ljava/util/ArrayList;

    const v1, 0x7f0a00be

    invoke-direct {p1, p0, p2, v1, v0}, Lepson/print/copy/CopyScaleActivity$XScaleValue$ScaleAdapter;-><init>(Lepson/print/copy/CopyScaleActivity$XScaleValue;Landroid/content/Context;ILjava/util/ArrayList;)V

    return-object p1
.end method

.class Lepson/print/copy/CopyActivity$6;
.super Ljava/lang/Object;
.source "CopyActivity.java"

# interfaces
.implements Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/copy/CopyActivity;->fetchCopyOptionContext()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/copy/CopyActivity;


# direct methods
.method constructor <init>(Lepson/print/copy/CopyActivity;)V
    .locals 0

    .line 461
    iput-object p1, p0, Lepson/print/copy/CopyActivity$6;->this$0:Lepson/print/copy/CopyActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCopyOptionContextCreated(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;)V
    .locals 2

    .line 465
    iget-object p1, p0, Lepson/print/copy/CopyActivity$6;->this$0:Lepson/print/copy/CopyActivity;

    iput-object p2, p1, Lepson/print/copy/CopyActivity;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    if-nez p3, :cond_1

    .line 467
    iget-object p1, p1, Lepson/print/copy/CopyActivity;->copyComponent:Lepson/print/copy/Component/ecopycomponent/ECopyComponent;

    iget-object p2, p0, Lepson/print/copy/CopyActivity$6;->this$0:Lepson/print/copy/CopyActivity;

    iget-object p2, p2, Lepson/print/copy/CopyActivity;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    iget-object p3, p0, Lepson/print/copy/CopyActivity$6;->this$0:Lepson/print/copy/CopyActivity;

    iget-object p3, p3, Lepson/print/copy/CopyActivity;->optionListener:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;

    invoke-virtual {p1, p2, p3}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->bindCopyOptionContext(Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;)V

    .line 468
    iget-object p1, p0, Lepson/print/copy/CopyActivity$6;->this$0:Lepson/print/copy/CopyActivity;

    iget-object p1, p1, Lepson/print/copy/CopyActivity;->copyComponent:Lepson/print/copy/Component/ecopycomponent/ECopyComponent;

    invoke-virtual {p1}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->getCopyOptionItems()Ljava/util/ArrayList;

    move-result-object p1

    .line 469
    sget-object p2, Lepson/print/copy/ActivityBase;->printerId:Ljava/lang/String;

    iget-object p3, p0, Lepson/print/copy/CopyActivity$6;->this$0:Lepson/print/copy/CopyActivity;

    invoke-static {p3}, Lepson/print/copy/CopyActivity;->access$700(Lepson/print/copy/CopyActivity;)Lepson/print/copy/ActivityBase$settingPreference;

    move-result-object p3

    invoke-virtual {p3}, Lepson/print/copy/ActivityBase$settingPreference;->loadPrePrinter()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 471
    iget-object p2, p0, Lepson/print/copy/CopyActivity$6;->this$0:Lepson/print/copy/CopyActivity;

    invoke-static {p2}, Lepson/print/copy/CopyActivity;->access$700(Lepson/print/copy/CopyActivity;)Lepson/print/copy/ActivityBase$settingPreference;

    move-result-object p2

    invoke-virtual {p2, p1}, Lepson/print/copy/ActivityBase$settingPreference;->setCopyOptions(Ljava/util/ArrayList;)V

    goto :goto_0

    .line 474
    :cond_0
    iget-object p2, p0, Lepson/print/copy/CopyActivity$6;->this$0:Lepson/print/copy/CopyActivity;

    invoke-virtual {p2, p1}, Lepson/print/copy/CopyActivity;->buildCopyOptions(Ljava/util/ArrayList;)V

    goto :goto_0

    .line 477
    :cond_1
    new-instance p2, Lepson/print/copy/ActivityBase$errorDialog;

    invoke-direct {p2, p1, p1}, Lepson/print/copy/ActivityBase$errorDialog;-><init>(Lepson/print/copy/ActivityBase;Landroid/content/Context;)V

    invoke-static {p1, p2}, Lepson/print/copy/CopyActivity;->access$502(Lepson/print/copy/CopyActivity;Lepson/print/copy/ActivityBase$errorDialog;)Lepson/print/copy/ActivityBase$errorDialog;

    .line 478
    iget-object p1, p0, Lepson/print/copy/CopyActivity$6;->this$0:Lepson/print/copy/CopyActivity;

    invoke-static {p1}, Lepson/print/copy/CopyActivity;->access$500(Lepson/print/copy/CopyActivity;)Lepson/print/copy/ActivityBase$errorDialog;

    move-result-object p1

    invoke-virtual {p1, p3}, Lepson/print/copy/ActivityBase$errorDialog;->getReasonText(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener$ContextCreationError;)[Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x0

    .line 479
    aget-object p2, p1, p2

    const/4 p3, 0x1

    .line 480
    aget-object p1, p1, p3

    .line 481
    iget-object p3, p0, Lepson/print/copy/CopyActivity$6;->this$0:Lepson/print/copy/CopyActivity;

    invoke-static {p3}, Lepson/print/copy/CopyActivity;->access$500(Lepson/print/copy/CopyActivity;)Lepson/print/copy/ActivityBase$errorDialog;

    move-result-object p3

    sget-object v0, Lepson/print/copy/ActivityBase$DialogButtons;->Ok:Lepson/print/copy/ActivityBase$DialogButtons;

    new-instance v1, Lepson/print/copy/CopyActivity$6$1;

    invoke-direct {v1, p0}, Lepson/print/copy/CopyActivity$6$1;-><init>(Lepson/print/copy/CopyActivity$6;)V

    invoke-virtual {p3, p2, p1, v0, v1}, Lepson/print/copy/ActivityBase$errorDialog;->showErrorDialog(Ljava/lang/String;Ljava/lang/String;Lepson/print/copy/ActivityBase$DialogButtons;Lepson/print/copy/ActivityBase$IClose;)V

    :goto_0
    return-void
.end method

.class Lepson/print/copy/CopyActivity$CopiesValue;
.super Lepson/print/copy/ActivityBase$NumberOptionValue;
.source "CopyActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/copy/CopyActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CopiesValue"
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/copy/CopyActivity;


# direct methods
.method public constructor <init>(Lepson/print/copy/CopyActivity;Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V
    .locals 1

    .line 565
    iput-object p1, p0, Lepson/print/copy/CopyActivity$CopiesValue;->this$0:Lepson/print/copy/CopyActivity;

    invoke-direct {p0, p1}, Lepson/print/copy/ActivityBase$NumberOptionValue;-><init>(Lepson/print/copy/ActivityBase;)V

    const v0, 0x7f08002d

    .line 566
    invoke-virtual {p0, v0, p2}, Lepson/print/copy/CopyActivity$CopiesValue;->bindOption(ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    const p2, 0x7f080027

    .line 567
    invoke-virtual {p0, p2}, Lepson/print/copy/CopyActivity$CopiesValue;->bindCountUp(I)V

    const p2, 0x7f080026

    .line 568
    invoke-virtual {p0, p2}, Lepson/print/copy/CopyActivity$CopiesValue;->bindCountDown(I)V

    .line 569
    iget-object p1, p1, Lepson/print/copy/CopyActivity;->optionValueChangedListener:Lepson/print/copy/ActivityBase$OptionItemChangedListener;

    invoke-virtual {p0, p1}, Lepson/print/copy/CopyActivity$CopiesValue;->setOptionValueChangedListener(Lepson/print/copy/ActivityBase$OptionItemChangedListener;)V

    return-void
.end method

.class Lepson/print/copy/CopyScaleActivity$1;
.super Ljava/lang/Object;
.source "CopyScaleActivity.java"

# interfaces
.implements Lepson/print/copy/ActivityBase$OptionItemChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/copy/CopyScaleActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/copy/CopyScaleActivity;


# direct methods
.method constructor <init>(Lepson/print/copy/CopyScaleActivity;)V
    .locals 0

    .line 55
    iput-object p1, p0, Lepson/print/copy/CopyScaleActivity$1;->this$0:Lepson/print/copy/CopyScaleActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onOptionItemChanged(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V
    .locals 3

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 60
    iget-object v1, p0, Lepson/print/copy/CopyScaleActivity$1;->this$0:Lepson/print/copy/CopyScaleActivity;

    iget-object v1, v1, Lepson/print/copy/CopyScaleActivity;->copyComponent:Lepson/print/copy/Component/ecopycomponent/ECopyComponent;

    invoke-virtual {v1, p1}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->setCopyOptionItem(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    .line 61
    invoke-virtual {p1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getKey()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    move-result-object v1

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->CopyMagnification:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    if-ne v1, v2, :cond_0

    .line 62
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 63
    iget-object p1, p0, Lepson/print/copy/CopyScaleActivity$1;->this$0:Lepson/print/copy/CopyScaleActivity;

    invoke-virtual {p1, v0}, Lepson/print/copy/CopyScaleActivity;->buildCopyOptions(Ljava/util/ArrayList;)V

    :cond_0
    return-void
.end method

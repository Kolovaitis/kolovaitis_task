.class Lepson/print/copy/CopyScaleActivity$XScaleValue$1;
.super Ljava/lang/Object;
.source "CopyScaleActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/copy/CopyScaleActivity$XScaleValue;->capabilitySetting(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lepson/print/copy/CopyScaleActivity$XScaleValue;


# direct methods
.method constructor <init>(Lepson/print/copy/CopyScaleActivity$XScaleValue;)V
    .locals 0

    .line 135
    iput-object p1, p0, Lepson/print/copy/CopyScaleActivity$XScaleValue$1;->this$1:Lepson/print/copy/CopyScaleActivity$XScaleValue;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .line 138
    iget-object p1, p0, Lepson/print/copy/CopyScaleActivity$XScaleValue$1;->this$1:Lepson/print/copy/CopyScaleActivity$XScaleValue;

    iget-object v0, p1, Lepson/print/copy/CopyScaleActivity$XScaleValue;->choices:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    iput-object v0, p1, Lepson/print/copy/CopyScaleActivity$XScaleValue;->selected:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 139
    iget-object p1, p0, Lepson/print/copy/CopyScaleActivity$XScaleValue$1;->this$1:Lepson/print/copy/CopyScaleActivity$XScaleValue;

    iget-object p1, p1, Lepson/print/copy/CopyScaleActivity$XScaleValue;->optionItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    iget-object v0, p0, Lepson/print/copy/CopyScaleActivity$XScaleValue$1;->this$1:Lepson/print/copy/CopyScaleActivity$XScaleValue;

    iget-object v0, v0, Lepson/print/copy/CopyScaleActivity$XScaleValue;->selected:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    invoke-virtual {p1, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->selectChoice(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;)V

    .line 140
    iget-object p1, p0, Lepson/print/copy/CopyScaleActivity$XScaleValue$1;->this$1:Lepson/print/copy/CopyScaleActivity$XScaleValue;

    iget-object p1, p1, Lepson/print/copy/CopyScaleActivity$XScaleValue;->changedListener:Lepson/print/copy/ActivityBase$OptionItemChangedListener;

    if-eqz p1, :cond_0

    .line 141
    iget-object p1, p0, Lepson/print/copy/CopyScaleActivity$XScaleValue$1;->this$1:Lepson/print/copy/CopyScaleActivity$XScaleValue;

    iget-object p1, p1, Lepson/print/copy/CopyScaleActivity$XScaleValue;->changedListener:Lepson/print/copy/ActivityBase$OptionItemChangedListener;

    iget-object v0, p0, Lepson/print/copy/CopyScaleActivity$XScaleValue$1;->this$1:Lepson/print/copy/CopyScaleActivity$XScaleValue;

    iget-object v0, v0, Lepson/print/copy/CopyScaleActivity$XScaleValue;->optionItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    invoke-interface {p1, v0}, Lepson/print/copy/ActivityBase$OptionItemChangedListener;->onOptionItemChanged(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    .line 143
    :cond_0
    iget-object p1, p0, Lepson/print/copy/CopyScaleActivity$XScaleValue$1;->this$1:Lepson/print/copy/CopyScaleActivity$XScaleValue;

    iget-object p1, p1, Lepson/print/copy/CopyScaleActivity$XScaleValue;->this$0:Lepson/print/copy/CopyScaleActivity;

    const/4 v0, 0x1

    iput-boolean v0, p1, Lepson/print/copy/CopyScaleActivity;->isKeepSimpleAPConnection:Z

    .line 144
    iget-object p1, p0, Lepson/print/copy/CopyScaleActivity$XScaleValue$1;->this$1:Lepson/print/copy/CopyScaleActivity$XScaleValue;

    iget-object p1, p1, Lepson/print/copy/CopyScaleActivity$XScaleValue;->this$0:Lepson/print/copy/CopyScaleActivity;

    invoke-virtual {p1}, Lepson/print/copy/CopyScaleActivity;->finish()V

    return-void
.end method

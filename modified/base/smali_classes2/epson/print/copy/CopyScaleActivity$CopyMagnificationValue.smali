.class Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;
.super Lepson/print/copy/ActivityBase$NumberOptionValue;
.source "CopyScaleActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/copy/CopyScaleActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CopyMagnificationValue"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$CountCustomScale;
    }
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/copy/CopyScaleActivity;


# direct methods
.method constructor <init>(Lepson/print/copy/CopyScaleActivity;Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V
    .locals 1

    .line 246
    iput-object p1, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->this$0:Lepson/print/copy/CopyScaleActivity;

    invoke-direct {p0, p1}, Lepson/print/copy/ActivityBase$NumberOptionValue;-><init>(Lepson/print/copy/ActivityBase;)V

    const v0, 0x7f080008

    .line 247
    invoke-virtual {p0, v0, p2}, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->bindOption(ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    .line 248
    invoke-virtual {p0}, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->clickCustomScale()V

    .line 249
    iget-object p1, p1, Lepson/print/copy/CopyScaleActivity;->optionValueChangedListener:Lepson/print/copy/ActivityBase$OptionItemChangedListener;

    invoke-virtual {p0, p1}, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->setOptionValueChangedListener(Lepson/print/copy/ActivityBase$OptionItemChangedListener;)V

    return-void
.end method


# virtual methods
.method bindOption(ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V
    .locals 1

    .line 253
    iput-object p2, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->optionItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    .line 254
    iget-object v0, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->this$0:Lepson/print/copy/CopyScaleActivity;

    invoke-virtual {v0, p1}, Lepson/print/copy/CopyScaleActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->editText:Landroid/widget/TextView;

    .line 255
    invoke-virtual {p2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedValue()I

    move-result p1

    iput p1, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->value:I

    .line 256
    iget p1, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->value:I

    sget v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->CopyMagnificationAutofitValue:I

    if-ne p1, v0, :cond_0

    .line 257
    iget-object p1, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->editText:Landroid/widget/TextView;

    const-string v0, "---"

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 259
    :cond_0
    iget-object p1, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->editText:Landroid/widget/TextView;

    iget v0, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->value:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 261
    :goto_0
    iget-object p1, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->editText:Landroid/widget/TextView;

    invoke-virtual {p2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->isEnabled()Z

    move-result p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void
.end method

.method clickCustomScale()V
    .locals 2

    .line 265
    iget-object v0, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->this$0:Lepson/print/copy/CopyScaleActivity;

    const v1, 0x7f0800f2

    invoke-virtual {v0, v1}, Lepson/print/copy/CopyScaleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 267
    new-instance v1, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;

    invoke-direct {v1, p0}, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue$1;-><init>(Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method onDismissDialog()V
    .locals 8

    .line 342
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0xa

    add-long/2addr v2, v4

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    .line 344
    iget-object v1, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->countDown:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 345
    iget-object v1, p0, Lepson/print/copy/CopyScaleActivity$CopyMagnificationValue;->countUp:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    return-void
.end method

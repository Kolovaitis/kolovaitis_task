.class public Lepson/print/copy/CopyActivity;
.super Lepson/print/copy/ActivityBase;
.source "CopyActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/copy/CopyActivity$ProbePrinter;,
        Lepson/print/copy/CopyActivity$XDensityValue;,
        Lepson/print/copy/CopyActivity$CopiesValue;
    }
.end annotation


# static fields
.field private static final CONNECTION_TIMEOUT:I = 0x7530

.field private static final EPS_COMM_BID:I = 0x2

.field private static final REQUEST_CODE_REPEAT_COPY:I = 0xc


# instance fields
.field private final COMM_ERROR:I

.field private final PROBE_PRINTER:I

.field private TAG:Ljava/lang/String;

.field private final UPDATE_SETTING:I

.field bProbedPrinter:Z

.field private clearindex:I

.field copyProcess:Lepson/print/copy/CopyProcess;

.field private doClear:Z

.field private errordialog:Lepson/print/copy/ActivityBase$errorDialog;

.field private ipAdress:Ljava/lang/String;

.field private mCopyTypeGroup:Landroid/widget/RadioGroup;

.field mHandler:Landroid/os/Handler;

.field private mLayout:Landroid/view/ViewGroup;

.field private mNormalCopyButton:Landroid/widget/RadioButton;

.field private mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

.field private mRepeatCopyButton:Landroid/widget/RadioButton;

.field optionListener:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;

.field optionValueChangedListener:Lepson/print/copy/ActivityBase$OptionItemChangedListener;

.field private presettings:Lepson/print/copy/ActivityBase$settingPreference;

.field private printerLocation:I

.field systemSettings:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopySystemSettings;

.field private task:Lepson/print/copy/CopyActivity$ProbePrinter;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 51
    invoke-direct {p0}, Lepson/print/copy/ActivityBase;-><init>()V

    const-string v0, "CopyActivty"

    .line 54
    iput-object v0, p0, Lepson/print/copy/CopyActivity;->TAG:Ljava/lang/String;

    .line 57
    new-instance v0, Lepson/print/copy/ActivityBase$settingPreference;

    invoke-direct {v0, p0}, Lepson/print/copy/ActivityBase$settingPreference;-><init>(Lepson/print/copy/ActivityBase;)V

    iput-object v0, p0, Lepson/print/copy/CopyActivity;->presettings:Lepson/print/copy/ActivityBase$settingPreference;

    const/4 v0, 0x0

    .line 63
    iput v0, p0, Lepson/print/copy/CopyActivity;->clearindex:I

    .line 64
    iput-boolean v0, p0, Lepson/print/copy/CopyActivity;->doClear:Z

    .line 72
    iput-boolean v0, p0, Lepson/print/copy/CopyActivity;->bProbedPrinter:Z

    .line 236
    iput v0, p0, Lepson/print/copy/CopyActivity;->PROBE_PRINTER:I

    const/4 v0, 0x1

    .line 237
    iput v0, p0, Lepson/print/copy/CopyActivity;->COMM_ERROR:I

    const/4 v0, 0x2

    .line 238
    iput v0, p0, Lepson/print/copy/CopyActivity;->UPDATE_SETTING:I

    .line 240
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lepson/print/copy/CopyActivity$3;

    invoke-direct {v1, p0}, Lepson/print/copy/CopyActivity$3;-><init>(Lepson/print/copy/CopyActivity;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lepson/print/copy/CopyActivity;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lepson/print/copy/CopyActivity;)Z
    .locals 0

    .line 51
    iget-boolean p0, p0, Lepson/print/copy/CopyActivity;->doClear:Z

    return p0
.end method

.method static synthetic access$002(Lepson/print/copy/CopyActivity;Z)Z
    .locals 0

    .line 51
    iput-boolean p1, p0, Lepson/print/copy/CopyActivity;->doClear:Z

    return p1
.end method

.method static synthetic access$100(Lepson/print/copy/CopyActivity;)I
    .locals 0

    .line 51
    iget p0, p0, Lepson/print/copy/CopyActivity;->clearindex:I

    return p0
.end method

.method static synthetic access$1000(Lepson/print/copy/CopyActivity;)Ljava/lang/String;
    .locals 0

    .line 51
    iget-object p0, p0, Lepson/print/copy/CopyActivity;->ipAdress:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1002(Lepson/print/copy/CopyActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 51
    iput-object p1, p0, Lepson/print/copy/CopyActivity;->ipAdress:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$102(Lepson/print/copy/CopyActivity;I)I
    .locals 0

    .line 51
    iput p1, p0, Lepson/print/copy/CopyActivity;->clearindex:I

    return p1
.end method

.method static synthetic access$1100(Lepson/print/copy/CopyActivity;)Z
    .locals 0

    .line 51
    invoke-direct {p0}, Lepson/print/copy/CopyActivity;->checkRepeatCopyFunction()Z

    move-result p0

    return p0
.end method

.method static synthetic access$1200(Lepson/print/copy/CopyActivity;Z)V
    .locals 0

    .line 51
    invoke-direct {p0, p1}, Lepson/print/copy/CopyActivity;->changeRepeatCopyButtonState(Z)V

    return-void
.end method

.method static synthetic access$200(Lepson/print/copy/CopyActivity;)Ljava/lang/String;
    .locals 0

    .line 51
    iget-object p0, p0, Lepson/print/copy/CopyActivity;->TAG:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$300(Lepson/print/copy/CopyActivity;)Lepson/print/copy/CopyActivity$ProbePrinter;
    .locals 0

    .line 51
    iget-object p0, p0, Lepson/print/copy/CopyActivity;->task:Lepson/print/copy/CopyActivity$ProbePrinter;

    return-object p0
.end method

.method static synthetic access$302(Lepson/print/copy/CopyActivity;Lepson/print/copy/CopyActivity$ProbePrinter;)Lepson/print/copy/CopyActivity$ProbePrinter;
    .locals 0

    .line 51
    iput-object p1, p0, Lepson/print/copy/CopyActivity;->task:Lepson/print/copy/CopyActivity$ProbePrinter;

    return-object p1
.end method

.method static synthetic access$500(Lepson/print/copy/CopyActivity;)Lepson/print/copy/ActivityBase$errorDialog;
    .locals 0

    .line 51
    iget-object p0, p0, Lepson/print/copy/CopyActivity;->errordialog:Lepson/print/copy/ActivityBase$errorDialog;

    return-object p0
.end method

.method static synthetic access$502(Lepson/print/copy/CopyActivity;Lepson/print/copy/ActivityBase$errorDialog;)Lepson/print/copy/ActivityBase$errorDialog;
    .locals 0

    .line 51
    iput-object p1, p0, Lepson/print/copy/CopyActivity;->errordialog:Lepson/print/copy/ActivityBase$errorDialog;

    return-object p1
.end method

.method static synthetic access$600(Lepson/print/copy/CopyActivity;)V
    .locals 0

    .line 51
    invoke-direct {p0}, Lepson/print/copy/CopyActivity;->switchToRepeatCopy()V

    return-void
.end method

.method static synthetic access$700(Lepson/print/copy/CopyActivity;)Lepson/print/copy/ActivityBase$settingPreference;
    .locals 0

    .line 51
    iget-object p0, p0, Lepson/print/copy/CopyActivity;->presettings:Lepson/print/copy/ActivityBase$settingPreference;

    return-object p0
.end method

.method static synthetic access$800(Lepson/print/copy/CopyActivity;)I
    .locals 0

    .line 51
    iget p0, p0, Lepson/print/copy/CopyActivity;->printerLocation:I

    return p0
.end method

.method static synthetic access$900(Lepson/print/copy/CopyActivity;)Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;
    .locals 0

    .line 51
    iget-object p0, p0, Lepson/print/copy/CopyActivity;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    return-object p0
.end method

.method private changeRepeatCopyButtonState(Z)V
    .locals 2

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    const/4 p1, 0x0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/16 p1, 0x8

    .line 758
    :goto_0
    iget-object v1, p0, Lepson/print/copy/CopyActivity;->mCopyTypeGroup:Landroid/widget/RadioGroup;

    invoke-virtual {v1, p1}, Landroid/widget/RadioGroup;->setVisibility(I)V

    .line 759
    iget-object p1, p0, Lepson/print/copy/CopyActivity;->mRepeatCopyButton:Landroid/widget/RadioButton;

    invoke-virtual {p1, v0}, Landroid/widget/RadioButton;->setEnabled(Z)V

    return-void
.end method

.method private checkRepeatCopyFunction()Z
    .locals 2

    .line 736
    new-instance v0, Lepson/print/copy/Component/eremoteoperation/ERemoteDevice;

    invoke-direct {v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteDevice;-><init>()V

    .line 737
    iget-object v1, p0, Lepson/print/copy/CopyActivity;->ipAdress:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteDevice;->setHostIP(Ljava/lang/String;)V

    const/16 v1, 0x7530

    .line 738
    invoke-virtual {v0, v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteDevice;->setRequestConnectionTimeout(I)V

    .line 739
    invoke-virtual {p0}, Lepson/print/copy/CopyActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lepson/common/Utils;->getRemoteOperationUUID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 740
    invoke-virtual {v0, v1}, Lepson/print/copy/Component/eremoteoperation/ERemoteDevice;->executeGetFunctionsCommand(Ljava/lang/String;)Lepson/print/copy/Component/eremoteoperation/ERemoteDevice$ERemoteDeviceFunctionsResult;

    move-result-object v0

    .line 741
    invoke-virtual {v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteDevice$ERemoteDeviceFunctionsResult;->success()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 742
    invoke-virtual {v0}, Lepson/print/copy/Component/eremoteoperation/ERemoteDevice$ERemoteDeviceFunctionsResult;->functions()Ljava/util/ArrayList;

    move-result-object v0

    .line 743
    sget-object v1, Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;->repeat_copy:Lepson/print/copy/Component/eremoteoperation/ERemoteOperation$ERemoteParam;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private getPrinterInfo()V
    .locals 2

    .line 643
    invoke-static {p0}, Lepson/print/MyPrinter;->getCurPrinter(Landroid/content/Context;)Lepson/print/MyPrinter;

    move-result-object v0

    .line 644
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lepson/print/copy/CopyActivity;->printerId:Ljava/lang/String;

    .line 645
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lepson/print/copy/CopyActivity;->printerIp:Ljava/lang/String;

    .line 646
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getLocation()I

    move-result v0

    iput v0, p0, Lepson/print/copy/CopyActivity;->printerLocation:I

    return-void
.end method

.method private setupUi()V
    .locals 3

    const v0, 0x7f0800d2

    .line 286
    invoke-virtual {p0, v0}, Lepson/print/copy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0802d1

    .line 287
    invoke-virtual {p0, v0}, Lepson/print/copy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f08024a

    .line 288
    invoke-virtual {p0, v0}, Lepson/print/copy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f080252

    .line 289
    invoke-virtual {p0, v0}, Lepson/print/copy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f08024f

    .line 290
    invoke-virtual {p0, v0}, Lepson/print/copy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f080118

    .line 291
    invoke-virtual {p0, v0}, Lepson/print/copy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0802a0

    .line 292
    invoke-virtual {p0, v0}, Lepson/print/copy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0800f7

    .line 293
    invoke-virtual {p0, v0}, Lepson/print/copy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0800e4

    .line 294
    invoke-virtual {p0, v0}, Lepson/print/copy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f080027

    .line 297
    invoke-virtual {p0, v0}, Lepson/print/copy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lepson/print/widgets/LongTapRepeatAdapter;->bless(Landroid/view/View;)V

    const v0, 0x7f080026

    .line 298
    invoke-virtual {p0, v0}, Lepson/print/copy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lepson/print/widgets/LongTapRepeatAdapter;->bless(Landroid/view/View;)V

    const v0, 0x7f08002b

    .line 299
    invoke-virtual {p0, v0}, Lepson/print/copy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lepson/print/widgets/LongTapRepeatAdapter;->bless(Landroid/view/View;)V

    const v0, 0x7f08002a

    .line 300
    invoke-virtual {p0, v0}, Lepson/print/copy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lepson/print/widgets/LongTapRepeatAdapter;->bless(Landroid/view/View;)V

    const v0, 0x7f0800f9

    .line 302
    invoke-virtual {p0, v0}, Lepson/print/copy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lepson/print/copy/CopyActivity;->mCopyTypeGroup:Landroid/widget/RadioGroup;

    const v0, 0x7f08022a

    .line 304
    invoke-virtual {p0, v0}, Lepson/print/copy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lepson/print/copy/CopyActivity;->mNormalCopyButton:Landroid/widget/RadioButton;

    .line 305
    iget-object v0, p0, Lepson/print/copy/CopyActivity;->mNormalCopyButton:Landroid/widget/RadioButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 306
    iget-object v0, p0, Lepson/print/copy/CopyActivity;->mNormalCopyButton:Landroid/widget/RadioButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setEnabled(Z)V

    .line 307
    iget-object v0, p0, Lepson/print/copy/CopyActivity;->mNormalCopyButton:Landroid/widget/RadioButton;

    new-instance v2, Lepson/print/copy/CopyActivity$4;

    invoke-direct {v2, p0}, Lepson/print/copy/CopyActivity$4;-><init>(Lepson/print/copy/CopyActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    const v0, 0x7f0802aa

    .line 314
    invoke-virtual {p0, v0}, Lepson/print/copy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lepson/print/copy/CopyActivity;->mRepeatCopyButton:Landroid/widget/RadioButton;

    .line 315
    iget-object v0, p0, Lepson/print/copy/CopyActivity;->mRepeatCopyButton:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 316
    iget-object v0, p0, Lepson/print/copy/CopyActivity;->mNormalCopyButton:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setEnabled(Z)V

    .line 317
    iget-object v0, p0, Lepson/print/copy/CopyActivity;->mRepeatCopyButton:Landroid/widget/RadioButton;

    new-instance v1, Lepson/print/copy/CopyActivity$5;

    invoke-direct {v1, p0}, Lepson/print/copy/CopyActivity$5;-><init>(Lepson/print/copy/CopyActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    return-void
.end method

.method private switchToRepeatCopy()V
    .locals 2

    const/4 v0, 0x1

    .line 328
    iput-boolean v0, p0, Lepson/print/copy/CopyActivity;->isKeepSimpleAPConnection:Z

    .line 329
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lepson/print/rpcopy/CopyActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0xc

    .line 330
    invoke-virtual {p0, v0, v1}, Lepson/print/copy/CopyActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method


# virtual methods
.method allClear()V
    .locals 3

    const/4 v0, 0x1

    .line 412
    iput-boolean v0, p0, Lepson/print/copy/CopyActivity;->doClear:Z

    .line 413
    iget-object v0, p0, Lepson/print/copy/CopyActivity;->loading:Lepson/print/screen/WorkingDialog;

    invoke-virtual {v0}, Lepson/print/screen/WorkingDialog;->show()V

    .line 415
    iget-object v0, p0, Lepson/print/copy/CopyActivity;->copyComponent:Lepson/print/copy/Component/ecopycomponent/ECopyComponent;

    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->getCopyOptionItems()Ljava/util/ArrayList;

    move-result-object v0

    .line 416
    iget v1, p0, Lepson/print/copy/CopyActivity;->clearindex:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lepson/print/copy/CopyActivity;->clearindex:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    .line 417
    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getKey()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    move-result-object v1

    .line 418
    sget-object v2, Lepson/print/copy/CopyActivity$7;->$SwitchMap$epson$print$copy$Component$ecopycomponent$ECopyOptionItem$ECopyOptionItemKey:[I

    invoke-virtual {v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 449
    :pswitch_0
    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getDefaultValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->selectValue(I)V

    goto :goto_0

    .line 444
    :pswitch_1
    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_Autofit:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->selectChoice(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;)V

    goto :goto_0

    .line 441
    :pswitch_2
    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintQuality_Normal:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->selectChoice(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;)V

    goto :goto_0

    .line 438
    :pswitch_3
    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSource_Bottom:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->selectChoice(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;)V

    goto :goto_0

    .line 430
    :pswitch_4
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 431
    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectableChoices()Ljava/util/ArrayList;

    move-result-object v1

    sget-object v2, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_Letter:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 432
    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_Letter:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->selectChoice(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;)V

    goto :goto_0

    .line 434
    :cond_0
    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaSize_A4:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->selectChoice(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;)V

    goto :goto_0

    .line 427
    :pswitch_5
    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->PrintMediaType_Stationery:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->selectChoice(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;)V

    goto :goto_0

    .line 424
    :pswitch_6
    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->ScanContentType_Mixed:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->selectChoice(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;)V

    goto :goto_0

    .line 421
    :pswitch_7
    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->ColorEffectsType_Color:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    invoke-virtual {v0, v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->selectChoice(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;)V

    .line 454
    :goto_0
    iget-object v1, p0, Lepson/print/copy/CopyActivity;->copyComponent:Lepson/print/copy/Component/ecopycomponent/ECopyComponent;

    invoke-virtual {v1, v0}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->setCopyOptionItem(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method buildCopyOptions(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;",
            ">;)V"
        }
    .end annotation

    .line 498
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    .line 499
    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getKey()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    move-result-object v1

    .line 500
    sget-object v2, Lepson/print/copy/CopyActivity$7;->$SwitchMap$epson$print$copy$Component$ecopycomponent$ECopyOptionItem$ECopyOptionItemKey:[I

    invoke-virtual {v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto/16 :goto_1

    .line 536
    :pswitch_0
    iget-object v2, p0, Lepson/print/copy/CopyActivity;->optionValueMap:Ljava/util/HashMap;

    new-instance v3, Lepson/print/copy/CopyActivity$CopiesValue;

    invoke-direct {v3, p0, v0}, Lepson/print/copy/CopyActivity$CopiesValue;-><init>(Lepson/print/copy/CopyActivity;Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    invoke-virtual {v2, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 533
    :pswitch_1
    iget-object v2, p0, Lepson/print/copy/CopyActivity;->optionValueMap:Ljava/util/HashMap;

    new-instance v3, Lepson/print/copy/ActivityBase$CopyMagnificationValue;

    invoke-direct {v3, p0, v0}, Lepson/print/copy/ActivityBase$CopyMagnificationValue;-><init>(Lepson/print/copy/ActivityBase;Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    invoke-virtual {v2, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 530
    :pswitch_2
    iget-object v2, p0, Lepson/print/copy/CopyActivity;->optionValueMap:Ljava/util/HashMap;

    new-instance v3, Lepson/print/copy/CopyActivity$XDensityValue;

    invoke-direct {v3, p0, v0}, Lepson/print/copy/CopyActivity$XDensityValue;-><init>(Lepson/print/copy/CopyActivity;Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    invoke-virtual {v2, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 526
    :pswitch_3
    iget-object v2, p0, Lepson/print/copy/CopyActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "show "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ":"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " setting"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const v1, 0x7f0800f6

    .line 527
    invoke-virtual {p0, v1, v0}, Lepson/print/copy/CopyActivity;->updateSetting(ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    goto/16 :goto_1

    .line 522
    :pswitch_4
    iget-object v2, p0, Lepson/print/copy/CopyActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "show "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ":"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " setting"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const v1, 0x7f0800f1

    .line 523
    invoke-virtual {p0, v1, v0}, Lepson/print/copy/CopyActivity;->updateSetting(ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    goto/16 :goto_1

    .line 518
    :pswitch_5
    iget-object v2, p0, Lepson/print/copy/CopyActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "show "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ":"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " setting"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const v1, 0x7f0800ef

    .line 519
    invoke-virtual {p0, v1, v0}, Lepson/print/copy/CopyActivity;->updateSetting(ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    goto/16 :goto_1

    .line 514
    :pswitch_6
    iget-object v2, p0, Lepson/print/copy/CopyActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "show "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ":"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " setting"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const v1, 0x7f0800ee

    .line 515
    invoke-virtual {p0, v1, v0}, Lepson/print/copy/CopyActivity;->updateSetting(ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    goto/16 :goto_1

    .line 510
    :pswitch_7
    iget-object v2, p0, Lepson/print/copy/CopyActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "show "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ":"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " setting"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const v1, 0x7f0800f0

    .line 511
    invoke-virtual {p0, v1, v0}, Lepson/print/copy/CopyActivity;->updateSetting(ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    goto :goto_1

    .line 506
    :pswitch_8
    iget-object v2, p0, Lepson/print/copy/CopyActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "show "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ":"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " setting"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const v1, 0x7f0800e7

    .line 507
    invoke-virtual {p0, v1, v0}, Lepson/print/copy/CopyActivity;->updateSetting(ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    goto :goto_1

    .line 502
    :pswitch_9
    iget-object v2, p0, Lepson/print/copy/CopyActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "show "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ":"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " setting"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const v1, 0x7f0800e5

    .line 503
    invoke-virtual {p0, v1, v0}, Lepson/print/copy/CopyActivity;->updateSetting(ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    .line 541
    :goto_1
    iget-object v0, p0, Lepson/print/copy/CopyActivity;->loading:Lepson/print/screen/WorkingDialog;

    invoke-virtual {v0}, Lepson/print/screen/WorkingDialog;->dismiss()V

    goto/16 :goto_0

    :cond_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected deleteLongTapMessage()V
    .locals 8

    .line 767
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0xa

    add-long/2addr v2, v4

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    const v1, 0x7f080027

    .line 770
    invoke-virtual {p0, v1}, Lepson/print/copy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    const v1, 0x7f080026

    .line 771
    invoke-virtual {p0, v1}, Lepson/print/copy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    const v1, 0x7f08002b

    .line 772
    invoke-virtual {p0, v1}, Lepson/print/copy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    const v1, 0x7f08002a

    .line 773
    invoke-virtual {p0, v1}, Lepson/print/copy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    return-void
.end method

.method fetchCopyOptionContext()V
    .locals 3

    .line 461
    iget-object v0, p0, Lepson/print/copy/CopyActivity;->copyComponent:Lepson/print/copy/Component/ecopycomponent/ECopyComponent;

    iget-object v1, p0, Lepson/print/copy/CopyActivity;->copyType:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;

    new-instance v2, Lepson/print/copy/CopyActivity$6;

    invoke-direct {v2, p0}, Lepson/print/copy/CopyActivity$6;-><init>(Lepson/print/copy/CopyActivity;)V

    invoke-virtual {v0, v1, v2}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->createCopyOptionContext(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ECopyType;Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionContextListener;)V

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    const/16 p3, 0xc

    if-eq p1, p3, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, -0x1

    if-ne p2, p1, :cond_1

    .line 227
    invoke-virtual {p0}, Lepson/print/copy/CopyActivity;->finish()V

    return-void

    .line 231
    :cond_1
    iget-object p1, p0, Lepson/print/copy/CopyActivity;->mNormalCopyButton:Landroid/widget/RadioButton;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Landroid/widget/RadioButton;->setChecked(Z)V

    :goto_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .line 335
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 337
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    sparse-switch p1, :sswitch_data_0

    goto/16 :goto_0

    .line 358
    :sswitch_0
    invoke-virtual {p0}, Lepson/print/copy/CopyActivity;->getBaseContext()Landroid/content/Context;

    move-result-object p1

    const-class v1, Lepson/print/copy/CopyScaleActivity;

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string p1, "Key"

    .line 359
    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->XScale:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 360
    invoke-virtual {p0, v0}, Lepson/print/copy/CopyActivity;->startActivityAndKeepSimpleAp(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 388
    :sswitch_1
    invoke-virtual {p0}, Lepson/print/copy/CopyActivity;->getBaseContext()Landroid/content/Context;

    move-result-object p1

    const-class v1, Lepson/print/copy/CopySettingActivity;

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string p1, "Key"

    .line 389
    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintQuality:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 390
    invoke-virtual {p0, v0}, Lepson/print/copy/CopyActivity;->startActivityAndKeepSimpleAp(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 370
    :sswitch_2
    invoke-virtual {p0}, Lepson/print/copy/CopyActivity;->getBaseContext()Landroid/content/Context;

    move-result-object p1

    const-class v1, Lepson/print/copy/CopySettingActivity;

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string p1, "Key"

    .line 371
    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaType:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 372
    invoke-virtual {p0, v0}, Lepson/print/copy/CopyActivity;->startActivityAndKeepSimpleAp(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 376
    :sswitch_3
    invoke-virtual {p0}, Lepson/print/copy/CopyActivity;->getBaseContext()Landroid/content/Context;

    move-result-object p1

    const-class v1, Lepson/print/copy/CopySettingActivity;

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string p1, "Key"

    .line 377
    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSource:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 378
    invoke-virtual {p0, v0}, Lepson/print/copy/CopyActivity;->startActivityAndKeepSimpleAp(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 364
    :sswitch_4
    invoke-virtual {p0}, Lepson/print/copy/CopyActivity;->getBaseContext()Landroid/content/Context;

    move-result-object p1

    const-class v1, Lepson/print/copy/CopySettingActivity;

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string p1, "Key"

    .line 365
    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->PrintMediaSize:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 366
    invoke-virtual {p0, v0}, Lepson/print/copy/CopyActivity;->startActivityAndKeepSimpleAp(Landroid/content/Intent;)V

    goto :goto_0

    .line 382
    :sswitch_5
    invoke-virtual {p0}, Lepson/print/copy/CopyActivity;->getBaseContext()Landroid/content/Context;

    move-result-object p1

    const-class v1, Lepson/print/copy/CopySettingActivity;

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string p1, "Key"

    .line 383
    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->ScanContentType:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 384
    invoke-virtual {p0, v0}, Lepson/print/copy/CopyActivity;->startActivityAndKeepSimpleAp(Landroid/content/Intent;)V

    goto :goto_0

    .line 348
    :sswitch_6
    invoke-virtual {p0}, Lepson/print/copy/CopyActivity;->allClear()V

    goto :goto_0

    .line 340
    :sswitch_7
    iget-object p1, p0, Lepson/print/copy/CopyActivity;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "NOW_PRINTER:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Lepson/print/copy/CopyActivity;->printerId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    iget-object p1, p0, Lepson/print/copy/CopyActivity;->presettings:Lepson/print/copy/ActivityBase$settingPreference;

    iget-object v0, p0, Lepson/print/copy/CopyActivity;->copyComponent:Lepson/print/copy/Component/ecopycomponent/ECopyComponent;

    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->getCopyOptionItems()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p1, v0}, Lepson/print/copy/ActivityBase$settingPreference;->saveCopyOptions(Ljava/util/ArrayList;)V

    .line 342
    new-instance p1, Lepson/print/copy/CopyProcess;

    invoke-direct {p1, p0}, Lepson/print/copy/CopyProcess;-><init>(Lepson/print/copy/ActivityBase;)V

    iput-object p1, p0, Lepson/print/copy/CopyActivity;->copyProcess:Lepson/print/copy/CopyProcess;

    .line 343
    iget-object p1, p0, Lepson/print/copy/CopyActivity;->copyProcess:Lepson/print/copy/CopyProcess;

    invoke-virtual {p0}, Lepson/print/copy/CopyActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1, v0}, Lepson/print/copy/CopyProcess;->startCopy(Landroid/content/Context;)V

    goto :goto_0

    .line 352
    :sswitch_8
    invoke-virtual {p0}, Lepson/print/copy/CopyActivity;->getBaseContext()Landroid/content/Context;

    move-result-object p1

    const-class v1, Lepson/print/copy/CopySettingActivity;

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string p1, "Key"

    .line 353
    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->ColorEffectsType:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {v1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 354
    invoke-virtual {p0, v0}, Lepson/print/copy/CopyActivity;->startActivityAndKeepSimpleAp(Landroid/content/Intent;)V

    :goto_0
    return-void

    :sswitch_data_0
    .sparse-switch
        0x7f0800d2 -> :sswitch_8
        0x7f0800e4 -> :sswitch_7
        0x7f0800f7 -> :sswitch_6
        0x7f080118 -> :sswitch_5
        0x7f08024a -> :sswitch_4
        0x7f08024f -> :sswitch_3
        0x7f080252 -> :sswitch_2
        0x7f0802a0 -> :sswitch_1
        0x7f0802d1 -> :sswitch_0
    .end sparse-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .line 779
    invoke-super {p0, p1}, Lepson/print/copy/ActivityBase;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 780
    invoke-virtual {p0}, Lepson/print/copy/CopyActivity;->deleteLongTapMessage()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .line 82
    invoke-super {p0, p1}, Lepson/print/copy/ActivityBase;->onCreate(Landroid/os/Bundle;)V

    .line 84
    iget-object p1, p0, Lepson/print/copy/CopyActivity;->copyComponent:Lepson/print/copy/Component/ecopycomponent/ECopyComponent;

    invoke-virtual {p1, p0}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->getRemoteOperationUUID(Landroid/content/Context;)V

    const/4 p1, 0x0

    .line 86
    iput-object p1, p0, Lepson/print/copy/CopyActivity;->optionContext:Lepson/print/copy/Component/ecopycomponent/ECopyOptionContext;

    .line 88
    invoke-virtual {p0}, Lepson/print/copy/CopyActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0a004c

    invoke-virtual {v0, v1, p1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, Lepson/print/copy/CopyActivity;->mLayout:Landroid/view/ViewGroup;

    .line 89
    iget-object p1, p0, Lepson/print/copy/CopyActivity;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {p0, p1}, Lepson/print/copy/CopyActivity;->setContentView(Landroid/view/View;)V

    .line 91
    invoke-direct {p0}, Lepson/print/copy/CopyActivity;->setupUi()V

    const p1, 0x7f0e022a

    const/4 v0, 0x1

    .line 94
    invoke-virtual {p0, p1, v0}, Lepson/print/copy/CopyActivity;->setActionBar(IZ)V

    .line 96
    iget-object p1, p0, Lepson/print/copy/CopyActivity;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    if-nez p1, :cond_0

    .line 97
    invoke-static {}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->getInstance()Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object p1

    iput-object p1, p0, Lepson/print/copy/CopyActivity;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    .line 100
    :cond_0
    new-instance p1, Lepson/print/copy/CopyActivity$1;

    invoke-direct {p1, p0}, Lepson/print/copy/CopyActivity$1;-><init>(Lepson/print/copy/CopyActivity;)V

    iput-object p1, p0, Lepson/print/copy/CopyActivity;->optionValueChangedListener:Lepson/print/copy/ActivityBase$OptionItemChangedListener;

    .line 108
    new-instance p1, Lepson/print/copy/CopyActivity$2;

    invoke-direct {p1, p0}, Lepson/print/copy/CopyActivity$2;-><init>(Lepson/print/copy/CopyActivity;)V

    iput-object p1, p0, Lepson/print/copy/CopyActivity;->optionListener:Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;

    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .line 219
    invoke-super {p0}, Lepson/print/copy/ActivityBase;->onDestroy()V

    return-void
.end method

.method public onPause()V
    .locals 3

    .line 190
    iget-object v0, p0, Lepson/print/copy/CopyActivity;->TAG:Ljava/lang/String;

    const-string v1, "onPause()"

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    invoke-super {p0}, Lepson/print/copy/ActivityBase;->onPause()V

    .line 192
    iget v0, p0, Lepson/print/copy/CopyActivity;->printerLocation:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    const/4 v2, 0x3

    if-ne v0, v2, :cond_3

    .line 193
    :cond_0
    iget-object v0, p0, Lepson/print/copy/CopyActivity;->task:Lepson/print/copy/CopyActivity$ProbePrinter;

    if-eqz v0, :cond_1

    .line 194
    invoke-virtual {v0, v1}, Lepson/print/copy/CopyActivity$ProbePrinter;->cancel(Z)Z

    .line 197
    :cond_1
    iget-object v0, p0, Lepson/print/copy/CopyActivity;->copyProcess:Lepson/print/copy/CopyProcess;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lepson/print/copy/CopyProcess;->isProccessing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 199
    iget-object v0, p0, Lepson/print/copy/CopyActivity;->copyProcess:Lepson/print/copy/CopyProcess;

    invoke-virtual {v0, v1}, Lepson/print/copy/CopyProcess;->setDisconnectWifi(Z)V

    goto :goto_0

    .line 202
    :cond_2
    iget-boolean v0, p0, Lepson/print/copy/CopyActivity;->isKeepSimpleAPConnection:Z

    if-nez v0, :cond_3

    const-string v0, "printer"

    .line 203
    sget-object v1, Lepson/print/copy/ActivityBase;->printerIp:Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->disconnect(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    .line 211
    :cond_3
    :goto_0
    invoke-virtual {p0}, Lepson/print/copy/CopyActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 212
    invoke-virtual {p0}, Lepson/print/copy/CopyActivity;->deleteLongTapMessage()V

    :cond_4
    return-void
.end method

.method public onResume()V
    .locals 4

    .line 147
    iget-object v0, p0, Lepson/print/copy/CopyActivity;->TAG:Ljava/lang/String;

    const-string v1, "onResume()"

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    invoke-super {p0}, Lepson/print/copy/ActivityBase;->onResume()V

    const/4 v0, 0x0

    .line 151
    iput-boolean v0, p0, Lepson/print/copy/CopyActivity;->isKeepSimpleAPConnection:Z

    .line 154
    invoke-direct {p0}, Lepson/print/copy/CopyActivity;->getPrinterInfo()V

    .line 159
    iget-object v1, p0, Lepson/print/copy/CopyActivity;->copyProcess:Lepson/print/copy/CopyProcess;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lepson/print/copy/CopyProcess;->isProccessing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 161
    iget-object v1, p0, Lepson/print/copy/CopyActivity;->copyProcess:Lepson/print/copy/CopyProcess;

    invoke-virtual {v1, v0}, Lepson/print/copy/CopyProcess;->setDisconnectWifi(Z)V

    goto :goto_1

    :cond_0
    const-string v1, "printer"

    .line 164
    invoke-static {p0, v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->isNeedConnect(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 166
    iput-boolean v0, p0, Lepson/print/copy/CopyActivity;->isTryConnectSimpleAp:Z

    goto :goto_0

    .line 167
    :cond_1
    iget-boolean v1, p0, Lepson/print/copy/CopyActivity;->isTryConnectSimpleAp:Z

    if-nez v1, :cond_2

    const/4 v1, 0x1

    .line 169
    iput-boolean v1, p0, Lepson/print/copy/CopyActivity;->isTryConnectSimpleAp:Z

    const-string v2, "printer"

    const/4 v3, -0x1

    .line 170
    invoke-static {p0, v2, v3}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->reconnect(Landroid/app/Activity;Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 171
    iput-boolean v1, p0, Lepson/print/copy/CopyActivity;->isKeepSimpleAPConnection:Z

    .line 172
    iput-boolean v0, p0, Lepson/print/copy/CopyActivity;->bProbedPrinter:Z

    return-void

    .line 176
    :cond_2
    :goto_0
    iget-object v1, p0, Lepson/print/copy/CopyActivity;->loading:Lepson/print/screen/WorkingDialog;

    invoke-virtual {v1}, Lepson/print/screen/WorkingDialog;->show()V

    .line 177
    iget-boolean v1, p0, Lepson/print/copy/CopyActivity;->bProbedPrinter:Z

    if-nez v1, :cond_3

    .line 179
    iget-object v1, p0, Lepson/print/copy/CopyActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    .line 182
    :cond_3
    iget-object v0, p0, Lepson/print/copy/CopyActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_1
    return-void
.end method

.method startActivityAndKeepSimpleAp(Landroid/content/Intent;)V
    .locals 1

    const/4 v0, 0x1

    .line 404
    iput-boolean v0, p0, Lepson/print/copy/CopyActivity;->isKeepSimpleAPConnection:Z

    .line 405
    invoke-virtual {p0, p1}, Lepson/print/copy/CopyActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method updateSetting(ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V
    .locals 2

    .line 551
    iget-object v0, p0, Lepson/print/copy/CopyActivity;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    .line 553
    invoke-virtual {p2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object v0

    sget-object v1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->XScale_Custom:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    if-ne v0, v1, :cond_0

    .line 554
    iget-object p2, p0, Lepson/print/copy/CopyActivity;->optionValueMap:Ljava/util/HashMap;

    sget-object v0, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;->CopyMagnification:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemKey;

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lepson/print/copy/ActivityBase$NumberOptionValue;

    .line 555
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const v1, 0x7f0e026b

    invoke-virtual {p0, v1}, Lepson/print/copy/CopyActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p2, p2, Lepson/print/copy/ActivityBase$NumberOptionValue;->value:I

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, " %"

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 557
    :cond_0
    invoke-virtual {p2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedChoice()Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    move-result-object p2

    invoke-virtual {p2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p2}, Lepson/print/copy/CopyActivity;->string(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void
.end method

.class Lepson/print/copy/CopyActivity$XDensityValue;
.super Lepson/print/copy/ActivityBase$NumberOptionValue;
.source "CopyActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/copy/CopyActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "XDensityValue"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/copy/CopyActivity$XDensityValue$CounterImage;
    }
.end annotation


# instance fields
.field image:Landroid/widget/ImageView;

.field images:Landroid/content/res/TypedArray;

.field final synthetic this$0:Lepson/print/copy/CopyActivity;


# direct methods
.method public constructor <init>(Lepson/print/copy/CopyActivity;Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V
    .locals 2

    .line 580
    iput-object p1, p0, Lepson/print/copy/CopyActivity$XDensityValue;->this$0:Lepson/print/copy/CopyActivity;

    invoke-direct {p0, p1}, Lepson/print/copy/ActivityBase$NumberOptionValue;-><init>(Lepson/print/copy/ActivityBase;)V

    .line 578
    iget-object v0, p0, Lepson/print/copy/CopyActivity$XDensityValue;->this$0:Lepson/print/copy/CopyActivity;

    invoke-virtual {v0}, Lepson/print/copy/CopyActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f020000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v0

    iput-object v0, p0, Lepson/print/copy/CopyActivity$XDensityValue;->images:Landroid/content/res/TypedArray;

    const v0, 0x7f080104

    .line 581
    invoke-virtual {p0, v0, p2}, Lepson/print/copy/CopyActivity$XDensityValue;->bindOption(ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    const p2, 0x7f08002b

    .line 582
    invoke-virtual {p0, p2}, Lepson/print/copy/CopyActivity$XDensityValue;->bindCountUp(I)V

    const p2, 0x7f08002a

    .line 583
    invoke-virtual {p0, p2}, Lepson/print/copy/CopyActivity$XDensityValue;->bindCountDown(I)V

    .line 584
    iget-object p1, p1, Lepson/print/copy/CopyActivity;->optionValueChangedListener:Lepson/print/copy/ActivityBase$OptionItemChangedListener;

    invoke-virtual {p0, p1}, Lepson/print/copy/CopyActivity$XDensityValue;->setOptionValueChangedListener(Lepson/print/copy/ActivityBase$OptionItemChangedListener;)V

    return-void
.end method


# virtual methods
.method bindCountDown(I)V
    .locals 2

    .line 609
    iget-object v0, p0, Lepson/print/copy/CopyActivity$XDensityValue;->this$0:Lepson/print/copy/CopyActivity;

    invoke-virtual {v0, p1}, Lepson/print/copy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lepson/print/copy/CopyActivity$XDensityValue;->countDown:Landroid/widget/Button;

    .line 610
    iget-object p1, p0, Lepson/print/copy/CopyActivity$XDensityValue;->countDown:Landroid/widget/Button;

    new-instance v0, Lepson/print/copy/CopyActivity$XDensityValue$CounterImage;

    const/4 v1, -0x1

    invoke-direct {v0, p0, v1}, Lepson/print/copy/CopyActivity$XDensityValue$CounterImage;-><init>(Lepson/print/copy/CopyActivity$XDensityValue;I)V

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 611
    iget p1, p0, Lepson/print/copy/CopyActivity$XDensityValue;->value:I

    iget-object v0, p0, Lepson/print/copy/CopyActivity$XDensityValue;->optionItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getMinimumValue()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 612
    iget-object p1, p0, Lepson/print/copy/CopyActivity$XDensityValue;->countDown:Landroid/widget/Button;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 614
    :cond_0
    iget-object p1, p0, Lepson/print/copy/CopyActivity$XDensityValue;->countDown:Landroid/widget/Button;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    :goto_0
    return-void
.end method

.method bindCountUp(I)V
    .locals 2

    .line 598
    iget-object v0, p0, Lepson/print/copy/CopyActivity$XDensityValue;->this$0:Lepson/print/copy/CopyActivity;

    invoke-virtual {v0, p1}, Lepson/print/copy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lepson/print/copy/CopyActivity$XDensityValue;->countUp:Landroid/widget/Button;

    .line 599
    iget-object p1, p0, Lepson/print/copy/CopyActivity$XDensityValue;->countUp:Landroid/widget/Button;

    new-instance v0, Lepson/print/copy/CopyActivity$XDensityValue$CounterImage;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Lepson/print/copy/CopyActivity$XDensityValue$CounterImage;-><init>(Lepson/print/copy/CopyActivity$XDensityValue;I)V

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 600
    iget p1, p0, Lepson/print/copy/CopyActivity$XDensityValue;->value:I

    iget-object v0, p0, Lepson/print/copy/CopyActivity$XDensityValue;->optionItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getMaximumValue()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 601
    iget-object p1, p0, Lepson/print/copy/CopyActivity$XDensityValue;->countUp:Landroid/widget/Button;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 603
    :cond_0
    iget-object p1, p0, Lepson/print/copy/CopyActivity$XDensityValue;->countUp:Landroid/widget/Button;

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setEnabled(Z)V

    :goto_0
    return-void
.end method

.method bindOption(ILepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V
    .locals 2

    .line 589
    iput-object p2, p0, Lepson/print/copy/CopyActivity$XDensityValue;->optionItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    .line 590
    invoke-virtual {p2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->getSelectedValue()I

    move-result v0

    iput v0, p0, Lepson/print/copy/CopyActivity$XDensityValue;->value:I

    .line 591
    iget-object v0, p0, Lepson/print/copy/CopyActivity$XDensityValue;->this$0:Lepson/print/copy/CopyActivity;

    invoke-virtual {v0, p1}, Lepson/print/copy/CopyActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lepson/print/copy/CopyActivity$XDensityValue;->image:Landroid/widget/ImageView;

    .line 592
    iget-object p1, p0, Lepson/print/copy/CopyActivity$XDensityValue;->image:Landroid/widget/ImageView;

    iget-object v0, p0, Lepson/print/copy/CopyActivity$XDensityValue;->images:Landroid/content/res/TypedArray;

    iget v1, p0, Lepson/print/copy/CopyActivity$XDensityValue;->value:I

    add-int/lit8 v1, v1, 0x4

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 593
    iget-object p1, p0, Lepson/print/copy/CopyActivity$XDensityValue;->image:Landroid/widget/ImageView;

    invoke-virtual {p2}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->isEnabled()Z

    move-result p2

    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setEnabled(Z)V

    return-void
.end method

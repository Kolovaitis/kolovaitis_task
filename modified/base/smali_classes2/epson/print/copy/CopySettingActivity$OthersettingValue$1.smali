.class Lepson/print/copy/CopySettingActivity$OthersettingValue$1;
.super Ljava/lang/Object;
.source "CopySettingActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/copy/CopySettingActivity$OthersettingValue;->capabilitySetting(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lepson/print/copy/CopySettingActivity$OthersettingValue;


# direct methods
.method constructor <init>(Lepson/print/copy/CopySettingActivity$OthersettingValue;)V
    .locals 0

    .line 121
    iput-object p1, p0, Lepson/print/copy/CopySettingActivity$OthersettingValue$1;->this$1:Lepson/print/copy/CopySettingActivity$OthersettingValue;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .line 124
    iget-object p1, p0, Lepson/print/copy/CopySettingActivity$OthersettingValue$1;->this$1:Lepson/print/copy/CopySettingActivity$OthersettingValue;

    iget-object p1, p1, Lepson/print/copy/CopySettingActivity$OthersettingValue;->this$0:Lepson/print/copy/CopySettingActivity;

    iget-object p1, p1, Lepson/print/copy/CopySettingActivity;->loading:Lepson/print/screen/WorkingDialog;

    invoke-virtual {p1}, Lepson/print/screen/WorkingDialog;->show()V

    .line 125
    iget-object p1, p0, Lepson/print/copy/CopySettingActivity$OthersettingValue$1;->this$1:Lepson/print/copy/CopySettingActivity$OthersettingValue;

    iget-object p1, p1, Lepson/print/copy/CopySettingActivity$OthersettingValue;->choices:Ljava/util/List;

    invoke-interface {p1, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;

    .line 126
    iget-object p2, p0, Lepson/print/copy/CopySettingActivity$OthersettingValue$1;->this$1:Lepson/print/copy/CopySettingActivity$OthersettingValue;

    iget-object p2, p2, Lepson/print/copy/CopySettingActivity$OthersettingValue;->optionItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    invoke-virtual {p2, p1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;->selectChoice(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;)V

    .line 127
    iget-object p1, p0, Lepson/print/copy/CopySettingActivity$OthersettingValue$1;->this$1:Lepson/print/copy/CopySettingActivity$OthersettingValue;

    iget-object p1, p1, Lepson/print/copy/CopySettingActivity$OthersettingValue;->changedListener:Lepson/print/copy/ActivityBase$OptionItemChangedListener;

    if-eqz p1, :cond_0

    .line 128
    iget-object p1, p0, Lepson/print/copy/CopySettingActivity$OthersettingValue$1;->this$1:Lepson/print/copy/CopySettingActivity$OthersettingValue;

    iget-object p1, p1, Lepson/print/copy/CopySettingActivity$OthersettingValue;->changedListener:Lepson/print/copy/ActivityBase$OptionItemChangedListener;

    iget-object p2, p0, Lepson/print/copy/CopySettingActivity$OthersettingValue$1;->this$1:Lepson/print/copy/CopySettingActivity$OthersettingValue;

    iget-object p2, p2, Lepson/print/copy/CopySettingActivity$OthersettingValue;->optionItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

    invoke-interface {p1, p2}, Lepson/print/copy/ActivityBase$OptionItemChangedListener;->onOptionItemChanged(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;)V

    .line 130
    :cond_0
    iget-object p1, p0, Lepson/print/copy/CopySettingActivity$OthersettingValue$1;->this$1:Lepson/print/copy/CopySettingActivity$OthersettingValue;

    iget-object p1, p1, Lepson/print/copy/CopySettingActivity$OthersettingValue;->this$0:Lepson/print/copy/CopySettingActivity;

    const/4 p2, 0x1

    iput-boolean p2, p1, Lepson/print/copy/CopySettingActivity;->isKeepSimpleAPConnection:Z

    return-void
.end method

.class abstract Lepson/print/copy/ActivityBase$OptionValue;
.super Ljava/lang/Object;
.source "ActivityBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/copy/ActivityBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x400
    name = "OptionValue"
.end annotation


# instance fields
.field changedListener:Lepson/print/copy/ActivityBase$OptionItemChangedListener;

.field optionItem:Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;

.field final synthetic this$0:Lepson/print/copy/ActivityBase;


# direct methods
.method constructor <init>(Lepson/print/copy/ActivityBase;)V
    .locals 0

    .line 420
    iput-object p1, p0, Lepson/print/copy/ActivityBase$OptionValue;->this$0:Lepson/print/copy/ActivityBase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method getChoiceString(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;)Ljava/lang/String;
    .locals 1

    .line 429
    iget-object v0, p0, Lepson/print/copy/ActivityBase$OptionValue;->this$0:Lepson/print/copy/ActivityBase;

    invoke-virtual {p1}, Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem$ECopyOptionItemChoice;->name()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lepson/print/copy/ActivityBase;->string(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method setOptionValueChangedListener(Lepson/print/copy/ActivityBase$OptionItemChangedListener;)V
    .locals 0

    .line 425
    iput-object p1, p0, Lepson/print/copy/ActivityBase$OptionValue;->changedListener:Lepson/print/copy/ActivityBase$OptionItemChangedListener;

    return-void
.end method

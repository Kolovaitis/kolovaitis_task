.class Lepson/print/copy/CopyActivity$2;
.super Ljava/lang/Object;
.source "CopyActivity.java"

# interfaces
.implements Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/copy/CopyActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/copy/CopyActivity;


# direct methods
.method constructor <init>(Lepson/print/copy/CopyActivity;)V
    .locals 0

    .line 108
    iput-object p1, p0, Lepson/print/copy/CopyActivity$2;->this$0:Lepson/print/copy/CopyActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCopyOptionChanged(Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;Ljava/util/ArrayList;Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;",
            "Ljava/util/ArrayList<",
            "Lepson/print/copy/Component/ecopycomponent/ECopyOptionItem;",
            ">;",
            "Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p3, :cond_0

    .line 113
    iget-object p1, p0, Lepson/print/copy/CopyActivity$2;->this$0:Lepson/print/copy/CopyActivity;

    iget-object p1, p1, Lepson/print/copy/CopyActivity;->loading:Lepson/print/screen/WorkingDialog;

    invoke-virtual {p1}, Lepson/print/screen/WorkingDialog;->dismiss()V

    .line 114
    new-instance p1, Lepson/print/copy/ActivityBase$errorDialog;

    iget-object p2, p0, Lepson/print/copy/CopyActivity$2;->this$0:Lepson/print/copy/CopyActivity;

    invoke-direct {p1, p2, p2}, Lepson/print/copy/ActivityBase$errorDialog;-><init>(Lepson/print/copy/ActivityBase;Landroid/content/Context;)V

    .line 115
    invoke-virtual {p1, p3}, Lepson/print/copy/ActivityBase$errorDialog;->getReasonText(Lepson/print/copy/Component/ecopycomponent/ECopyComponent$ICopyOptionListener$CopyOptionChangedError;)[Ljava/lang/String;

    move-result-object p2

    .line 116
    aget-object p3, p2, v1

    .line 117
    aget-object p2, p2, v0

    .line 118
    invoke-virtual {p1, p3, p2}, Lepson/print/copy/ActivityBase$errorDialog;->showErrorDialog(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 121
    :cond_0
    new-instance p3, Ljava/util/ArrayList;

    invoke-direct {p3}, Ljava/util/ArrayList;-><init>()V

    .line 122
    iget-object v2, p0, Lepson/print/copy/CopyActivity$2;->this$0:Lepson/print/copy/CopyActivity;

    invoke-static {v2}, Lepson/print/copy/CopyActivity;->access$000(Lepson/print/copy/CopyActivity;)Z

    move-result v2

    if-ne v2, v0, :cond_2

    .line 124
    iget-object v0, p0, Lepson/print/copy/CopyActivity$2;->this$0:Lepson/print/copy/CopyActivity;

    invoke-static {v0}, Lepson/print/copy/CopyActivity;->access$100(Lepson/print/copy/CopyActivity;)I

    move-result v0

    iget-object v2, p0, Lepson/print/copy/CopyActivity$2;->this$0:Lepson/print/copy/CopyActivity;

    iget-object v2, v2, Lepson/print/copy/CopyActivity;->copyComponent:Lepson/print/copy/Component/ecopycomponent/ECopyComponent;

    invoke-virtual {v2}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->getCopyOptionItems()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-eq v0, v2, :cond_1

    .line 126
    iget-object p1, p0, Lepson/print/copy/CopyActivity$2;->this$0:Lepson/print/copy/CopyActivity;

    invoke-virtual {p1}, Lepson/print/copy/CopyActivity;->allClear()V

    return-void

    .line 130
    :cond_1
    iget-object v0, p0, Lepson/print/copy/CopyActivity$2;->this$0:Lepson/print/copy/CopyActivity;

    invoke-static {v0, v1}, Lepson/print/copy/CopyActivity;->access$002(Lepson/print/copy/CopyActivity;Z)Z

    .line 131
    iget-object v0, p0, Lepson/print/copy/CopyActivity$2;->this$0:Lepson/print/copy/CopyActivity;

    invoke-static {v0, v1}, Lepson/print/copy/CopyActivity;->access$102(Lepson/print/copy/CopyActivity;I)I

    .line 132
    iget-object v0, p0, Lepson/print/copy/CopyActivity$2;->this$0:Lepson/print/copy/CopyActivity;

    iget-object v0, v0, Lepson/print/copy/CopyActivity;->copyComponent:Lepson/print/copy/Component/ecopycomponent/ECopyComponent;

    invoke-virtual {v0}, Lepson/print/copy/Component/ecopycomponent/ECopyComponent;->getCopyOptionItems()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 135
    :cond_2
    invoke-virtual {p3, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz p2, :cond_3

    .line 137
    invoke-virtual {p3, p2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 139
    :cond_3
    iget-object p1, p0, Lepson/print/copy/CopyActivity$2;->this$0:Lepson/print/copy/CopyActivity;

    invoke-virtual {p1, p3}, Lepson/print/copy/CopyActivity;->buildCopyOptions(Ljava/util/ArrayList;)V

    return-void
.end method

.class public Lepson/print/IprintHome;
.super Lepson/print/ActivityIACommon;
.source "IprintHome.java"

# interfaces
.implements Lepson/print/CommonDefine;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/IprintHome$BuyInkTask;,
        Lepson/print/IprintHome$ProbePrinter;
    }
.end annotation


# static fields
.field private static final EPS_COMM_BID:I = 0x2

.field private static final EPS_ERR_PRINTER_NOT_FOUND:I = -0x514

.field private static final EPS_PRB_BYID:I = 0x1

.field private static final EPS_PRNERR_COMM:I = 0x66

.field public static final MORE_APP_URL:Ljava/lang/String; = "http://iprint.epsonconnect.com/app/index.htm"

.field private static final REQUEST_CODE_LICENSE_CHECK:I = 0x1

.field private static final REQUEST_CODE_LOCATION_PERMISSION:I = 0x2

.field private static final REQUEST_GUIDE_DISPLAY:I = 0x3

.field private static final STRING_ID_READY_INK:I = 0x7f0e0442

.field private static final TAG:Ljava/lang/String; = "IprintHome"

.field public static mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;


# instance fields
.field private final Menu_Show_Support:I

.field private final PROBE_ERROR:I

.field private final PROBE_PRINTER:I

.field private final STATUS_ERROR:I

.field private final UPDATE_PRINTER_STATUS:I

.field private bRejectLocationPermission:Z

.field deviceID:Ljava/lang/String;

.field finishProbe:Z

.field private mActivityForeground:Z

.field private mForegroundRunnableList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field mHandler:Landroid/os/Handler;

.field final mIprintHomeLogging:Lepson/print/IprintHomeMenuLogging;

.field mLayout:Landroid/view/ViewGroup;

.field private mPrinterStatus:[I

.field menulist:Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;

.field printerIcon:I

.field private printerId:Ljava/lang/String;

.field private printerIp:Ljava/lang/String;

.field private printerLocation:I

.field printerName:Landroid/widget/TextView;

.field printerSerial:Ljava/lang/String;

.field printerStatus:Landroid/widget/TextView;

.field task:Lepson/print/IprintHome$ProbePrinter;

.field util:Lepson/print/Util/Utils;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 72
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    const/4 v0, 0x1

    .line 81
    iput v0, p0, Lepson/print/IprintHome;->Menu_Show_Support:I

    .line 97
    new-instance v1, Lepson/print/Util/Utils;

    invoke-direct {v1}, Lepson/print/Util/Utils;-><init>()V

    iput-object v1, p0, Lepson/print/IprintHome;->util:Lepson/print/Util/Utils;

    .line 98
    new-instance v1, Lepson/print/IprintHomeMenuLogging;

    invoke-direct {v1}, Lepson/print/IprintHomeMenuLogging;-><init>()V

    iput-object v1, p0, Lepson/print/IprintHome;->mIprintHomeLogging:Lepson/print/IprintHomeMenuLogging;

    const/4 v1, 0x0

    .line 112
    iput-boolean v1, p0, Lepson/print/IprintHome;->bRejectLocationPermission:Z

    .line 490
    iput v1, p0, Lepson/print/IprintHome;->PROBE_PRINTER:I

    .line 491
    iput v0, p0, Lepson/print/IprintHome;->UPDATE_PRINTER_STATUS:I

    const/4 v0, 0x2

    .line 492
    iput v0, p0, Lepson/print/IprintHome;->PROBE_ERROR:I

    const/4 v0, 0x3

    .line 493
    iput v0, p0, Lepson/print/IprintHome;->STATUS_ERROR:I

    .line 495
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lepson/print/IprintHome$2;

    invoke-direct {v1, p0}, Lepson/print/IprintHome$2;-><init>(Lepson/print/IprintHome;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lepson/print/IprintHome;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$100(Lepson/print/IprintHome;)[I
    .locals 0

    .line 72
    iget-object p0, p0, Lepson/print/IprintHome;->mPrinterStatus:[I

    return-object p0
.end method

.method static synthetic access$102(Lepson/print/IprintHome;[I)[I
    .locals 0

    .line 72
    iput-object p1, p0, Lepson/print/IprintHome;->mPrinterStatus:[I

    return-object p1
.end method

.method static synthetic access$200(Lepson/print/IprintHome;)I
    .locals 0

    .line 72
    iget p0, p0, Lepson/print/IprintHome;->printerLocation:I

    return p0
.end method

.method static synthetic access$202(Lepson/print/IprintHome;I)I
    .locals 0

    .line 72
    iput p1, p0, Lepson/print/IprintHome;->printerLocation:I

    return p1
.end method

.method static synthetic access$300(Lepson/print/IprintHome;)Ljava/lang/String;
    .locals 0

    .line 72
    iget-object p0, p0, Lepson/print/IprintHome;->printerId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$400(Lepson/print/IprintHome;)Ljava/lang/String;
    .locals 0

    .line 72
    iget-object p0, p0, Lepson/print/IprintHome;->printerIp:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$500(Lepson/print/IprintHome;)V
    .locals 0

    .line 72
    invoke-direct {p0}, Lepson/print/IprintHome;->openReadyInkPage()V

    return-void
.end method

.method private addBuyInkOrReadyInkToMenu(Ljava/util/List;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lepson/print/IconTextArrayItem;",
            ">;)V"
        }
    .end annotation

    .line 911
    invoke-direct {p0}, Lepson/print/IprintHome;->getBuyInkReadyInkButtonType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 927
    :pswitch_0
    invoke-direct {p0, p1}, Lepson/print/IprintHome;->addReadyInkToMenuEnd(Ljava/util/List;)V

    goto :goto_0

    .line 921
    :pswitch_1
    invoke-direct {p0, p1}, Lepson/print/IprintHome;->addReadyInkToMenuEnd(Ljava/util/List;)V

    :goto_0
    :pswitch_2
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private addBuyInkToMenu(Ljava/util/List;)V
    .locals 4
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lepson/print/IconTextArrayItem;",
            ">;)V"
        }
    .end annotation

    .line 961
    iget v0, p0, Lepson/print/IprintHome;->printerLocation:I

    const v1, 0x7f0e0532

    if-nez v0, :cond_0

    .line 963
    new-instance v0, Lepson/print/IconTextArrayItem;

    const v2, 0x7f0700bd

    const-string v3, ""

    invoke-direct {v0, v2, v1, v3}, Lepson/print/IconTextArrayItem;-><init>(IILjava/lang/String;)V

    const/4 v1, 0x0

    .line 964
    iput-boolean v1, v0, Lepson/print/IconTextArrayItem;->showMenu:Z

    goto :goto_0

    .line 966
    :cond_0
    new-instance v0, Lepson/print/IconTextArrayItem;

    const v2, 0x7f0700bc

    const-string v3, ""

    invoke-direct {v0, v2, v1, v3}, Lepson/print/IconTextArrayItem;-><init>(IILjava/lang/String;)V

    :goto_0
    const/4 v1, 0x1

    .line 968
    invoke-interface {p1, v1, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-void
.end method

.method private addReadyInkToMenuEnd(Ljava/util/List;)V
    .locals 4
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lepson/print/IconTextArrayItem;",
            ">;)V"
        }
    .end annotation

    .line 949
    new-instance v0, Lepson/print/IconTextArrayItem;

    const-string v1, ""

    const v2, 0x7f0700d5

    const v3, 0x7f0e0442

    invoke-direct {v0, v2, v3, v1}, Lepson/print/IconTextArrayItem;-><init>(IILjava/lang/String;)V

    .line 952
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private callInformation()V
    .locals 2

    .line 281
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lepson/support/SupportActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 282
    invoke-virtual {p0, v0}, Lepson/print/IprintHome;->startActivity(Landroid/content/Intent;)V

    const-string v0, "Home-Info"

    .line 283
    invoke-static {p0, v0}, Lcom/epson/iprint/prtlogger/Analytics;->sendAction(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method private executeForegroundQueue()Z
    .locals 3

    .line 327
    invoke-virtual {p0}, Lepson/print/IprintHome;->isFinishing()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 331
    :cond_0
    iget-object v0, p0, Lepson/print/IprintHome;->mForegroundRunnableList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    .line 332
    :goto_0
    invoke-interface {v0}, Ljava/util/ListIterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 333
    iget-object v1, p0, Lepson/print/IprintHome;->mHandler:Landroid/os/Handler;

    invoke-interface {v0}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 334
    invoke-interface {v0}, Ljava/util/ListIterator;->remove()V

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    return v1
.end method

.method private getBuyInkReadyInkButtonType()I
    .locals 2

    .line 938
    new-instance v0, Lepson/print/inkrpln/InkRplnRepository;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lepson/print/inkrpln/InkRplnRepository;-><init>(Z)V

    .line 939
    invoke-virtual {v0, p0}, Lepson/print/inkrpln/InkRplnRepository;->getInfo(Landroid/content/Context;)Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 941
    :cond_0
    invoke-virtual {v0}, Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;->getButtonType()I

    move-result v1

    :goto_0
    return v1
.end method

.method private makeMenu()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lepson/print/IconTextArrayItem;",
            ">;"
        }
    .end annotation

    .line 847
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 848
    new-instance v1, Lepson/print/MenudataAdapter;

    invoke-direct {v1}, Lepson/print/MenudataAdapter;-><init>()V

    .line 849
    invoke-virtual {p0}, Lepson/print/IprintHome;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lepson/print/MenudataAdapter;->setContext(Landroid/content/Context;)V

    .line 850
    invoke-virtual {v1}, Lepson/print/MenudataAdapter;->parseJSONFile()V

    .line 852
    iget-object v2, p0, Lepson/print/IprintHome;->deviceID:Ljava/lang/String;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x4

    if-lt v2, v3, :cond_0

    const-string v2, "USB"

    iget-object v3, p0, Lepson/print/IprintHome;->printerIp:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const-string v2, "DEFAULT"

    .line 854
    iput-object v2, p0, Lepson/print/IprintHome;->deviceID:Ljava/lang/String;

    .line 857
    :cond_1
    iget-object v2, p0, Lepson/print/IprintHome;->deviceID:Ljava/lang/String;

    iget v3, p0, Lepson/print/IprintHome;->printerLocation:I

    invoke-virtual {v1, v2, v3, p0}, Lepson/print/MenudataAdapter;->MenuListfordeviceID(Ljava/lang/String;ILandroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v1

    .line 860
    invoke-virtual {p0}, Lepson/print/IprintHome;->isJapaneseDevice()Z

    move-result v2

    const/4 v3, 0x0

    if-nez v2, :cond_3

    const/4 v2, 0x0

    .line 861
    :goto_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v2, v4, :cond_3

    .line 862
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lepson/print/IconTextArrayItem;

    iget-object v4, v4, Lepson/print/IconTextArrayItem;->appId:Ljava/lang/String;

    const-string v5, "CardPrint"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 863
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_1

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 870
    :cond_3
    :goto_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v3, v2, :cond_7

    .line 871
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lepson/print/IconTextArrayItem;

    iget-object v2, v2, Lepson/print/IconTextArrayItem;->appId:Ljava/lang/String;

    const-string v4, "Nenga"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 872
    invoke-static {p0}, Lepson/print/MyPrinter;->getCurPrinter(Landroid/content/Context;)Lepson/print/MyPrinter;

    move-result-object v2

    .line 873
    invoke-virtual {v2}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 874
    invoke-virtual {v2}, Lepson/print/MyPrinter;->getLang()I

    move-result v2

    const/4 v4, 0x1

    if-ne v2, v4, :cond_5

    .line 875
    invoke-virtual {p0}, Lepson/print/IprintHome;->isJapaneseDevice()Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v2, "USB"

    iget-object v4, p0, Lepson/print/IprintHome;->printerIp:Ljava/lang/String;

    .line 876
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    goto :goto_2

    .line 882
    :cond_4
    :try_start_0
    invoke-virtual {p0}, Lepson/print/IprintHome;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lepson/print/IconTextArrayItem;

    iget-object v4, v4, Lepson/print/IconTextArrayItem;->PackageName:Ljava/lang/String;

    const/16 v5, 0x80

    invoke-virtual {v2, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    const v4, 0xc08bfe0

    .line 883
    iget v2, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    if-le v4, v2, :cond_6

    .line 885
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    move-exception v2

    .line 889
    invoke-virtual {v2}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_3

    .line 878
    :cond_5
    :goto_2
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_3

    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 896
    :cond_7
    :goto_3
    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 898
    invoke-direct {p0, v0}, Lepson/print/IprintHome;->addBuyInkOrReadyInkToMenu(Ljava/util/List;)V

    return-object v0
.end method

.method private openReadyInkPage()V
    .locals 2

    .line 1006
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lepson/print/inkrpln/JumpUrlProgressActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lepson/print/IprintHome;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private requestLocationPermission()V
    .locals 1

    .line 212
    iget-boolean v0, p0, Lepson/print/IprintHome;->bRejectLocationPermission:Z

    if-nez v0, :cond_0

    const/4 v0, 0x4

    invoke-static {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->canAccessWiFiInfo(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 213
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/epson/mobilephone/common/ble/BleWork;->isStartBleProcess(Landroid/content/Context;Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    .line 214
    invoke-static {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->requestLocationPermission(Landroid/app/Activity;I)V

    :cond_0
    return-void
.end method

.method private startLicenseCheckActivity()V
    .locals 3

    .line 201
    invoke-virtual {p0}, Lepson/print/IprintHome;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lepson/common/IprintLicenseInfo;->beforeLicenseCheck(Landroid/content/Context;)V

    .line 202
    invoke-virtual {p0}, Lepson/print/IprintHome;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lepson/common/IprintLicenseInfo;

    invoke-direct {v1}, Lepson/common/IprintLicenseInfo;-><init>()V

    new-instance v2, Lepson/common/IprintUserSurveyInfo;

    invoke-direct {v2}, Lepson/common/IprintUserSurveyInfo;-><init>()V

    invoke-static {v0, v1, v2}, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->getStartIntent(Landroid/content/Context;Lcom/epson/mobilephone/common/license/LicenseInfo;Lcom/epson/mobilephone/common/license/UserSurveyInfo;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x1

    .line 204
    invoke-virtual {p0, v0, v1}, Lepson/print/IprintHome;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private updateMenu()V
    .locals 7

    .line 712
    invoke-virtual {p0}, Lepson/print/IprintHome;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 714
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 715
    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 716
    iget v0, v1, Landroid/graphics/Point;->x:I

    .line 717
    iget v1, v1, Landroid/graphics/Point;->y:I

    .line 720
    iget-object v2, p0, Lepson/print/IprintHome;->menulist:Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;

    invoke-virtual {v2}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    int-to-double v3, v1

    const-wide v5, 0x3f9999999999999aL    # 0.025

    mul-double v3, v3, v5

    double-to-int v1, v3

    int-to-double v3, v0

    const-wide v5, 0x3fb999999999999aL    # 0.1

    mul-double v3, v3, v5

    double-to-int v0, v3

    const/4 v3, 0x0

    .line 724
    invoke-virtual {v2, v0, v1, v0, v3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 725
    iget-object v0, p0, Lepson/print/IprintHome;->menulist:Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;

    invoke-virtual {v0, v2}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 728
    invoke-direct {p0}, Lepson/print/IprintHome;->makeMenu()Ljava/util/List;

    move-result-object v0

    .line 734
    new-instance v1, Lepson/print/IconTextArrayAdapter;

    iget-object v2, p0, Lepson/print/IprintHome;->menulist:Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;

    const v3, 0x7f0a0092

    invoke-direct {v1, p0, v3, v0, v2}, Lepson/print/IconTextArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;)V

    .line 737
    iget-object v0, p0, Lepson/print/IprintHome;->menulist:Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;

    invoke-virtual {v0, v1}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 740
    iget-object v0, p0, Lepson/print/IprintHome;->menulist:Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;

    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->setRequestedColumnCount(I)V

    .line 743
    iget-object v0, p0, Lepson/print/IprintHome;->menulist:Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;

    const/high16 v2, 0x40400000    # 3.0f

    invoke-static {p0, v2}, Lcom/felipecsl/asymmetricgridview/library/Utils;->dpToPx(Landroid/content/Context;F)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->setRequestedHorizontalSpacing(I)V

    .line 746
    iget-object v0, p0, Lepson/print/IprintHome;->menulist:Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;

    new-instance v2, Lepson/print/IprintHome$3;

    invoke-direct {v2, p0, v1}, Lepson/print/IprintHome$3;-><init>(Lepson/print/IprintHome;Landroid/widget/ListAdapter;)V

    invoke-virtual {v0, v2}, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method private updatePrinterIcon()V
    .locals 3

    .line 469
    iget-object v0, p0, Lepson/print/IprintHome;->printerName:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    const v1, 0x7f0e0439

    invoke-virtual {p0, v1}, Lepson/print/IprintHome;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    const v1, 0x7f080282

    if-eqz v0, :cond_0

    .line 470
    invoke-virtual {p0, v1}, Lepson/print/IprintHome;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 472
    :cond_0
    invoke-virtual {p0, v1}, Lepson/print/IprintHome;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 475
    iget v0, p0, Lepson/print/IprintHome;->printerLocation:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 480
    :pswitch_0
    invoke-virtual {p0, v1}, Lepson/print/IprintHome;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f07010f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 477
    :pswitch_1
    invoke-virtual {p0, v1}, Lepson/print/IprintHome;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f070111

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 483
    :pswitch_2
    invoke-virtual {p0, v1}, Lepson/print/IprintHome;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f070110

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private updatePrinterInfo()V
    .locals 3

    const-string v0, "IprintHome"

    const-string v1, "updatePrinterInfo"

    .line 421
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    invoke-static {p0}, Lepson/print/MyPrinter;->getCurPrinter(Landroid/content/Context;)Lepson/print/MyPrinter;

    move-result-object v0

    .line 433
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 435
    iget-object v1, p0, Lepson/print/IprintHome;->printerName:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Lepson/print/MyPrinter;->getUserDefName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 437
    :cond_0
    iget-object v1, p0, Lepson/print/IprintHome;->printerName:Landroid/widget/TextView;

    const v2, 0x7f0e0439

    invoke-virtual {p0, v2}, Lepson/print/IprintHome;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 439
    :goto_0
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lepson/print/IprintHome;->printerId:Ljava/lang/String;

    .line 440
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lepson/print/IprintHome;->printerIp:Ljava/lang/String;

    .line 441
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getLocation()I

    move-result v1

    iput v1, p0, Lepson/print/IprintHome;->printerLocation:I

    .line 442
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getSerialNo()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lepson/print/IprintHome;->printerSerial:Ljava/lang/String;

    .line 443
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepson/print/IprintHome;->deviceID:Ljava/lang/String;

    const-string v0, "IprintHome"

    .line 445
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lepson/print/IprintHome;->printerId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lepson/print/IprintHome;->printerIp:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lepson/print/IprintHome;->printerLocation:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lepson/print/IprintHome;->printerSerial:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lepson/print/IprintHome;->deviceID:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    invoke-direct {p0}, Lepson/print/IprintHome;->updatePrinterIcon()V

    .line 450
    invoke-direct {p0}, Lepson/print/IprintHome;->updateMenu()V

    .line 452
    iget v0, p0, Lepson/print/IprintHome;->printerLocation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lepson/print/IprintHome;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "printer"

    invoke-static {v0, v2}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->getConnectInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    :cond_1
    iget v0, p0, Lepson/print/IprintHome;->printerLocation:I

    const/4 v2, 0x3

    if-ne v0, v2, :cond_3

    .line 453
    invoke-virtual {p0}, Lepson/print/IprintHome;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lepson/common/Utils;->isConnectedWifi(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const-string v0, "IprintHome"

    const-string v1, "@@@@@@@@@@@@@@@@@@@@@@"

    .line 455
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 456
    iget-object v0, p0, Lepson/print/IprintHome;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    :cond_3
    const-string v0, "IprintHome"

    const-string v2, "########################"

    .line 459
    invoke-static {v0, v2}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 460
    iget-object v0, p0, Lepson/print/IprintHome;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_1
    return-void
.end method


# virtual methods
.method getBuyInkTask()Lepson/print/IprintHome$BuyInkTask;
    .locals 1

    .line 1001
    new-instance v0, Lepson/print/IprintHome$BuyInkTask;

    invoke-direct {v0, p0}, Lepson/print/IprintHome$BuyInkTask;-><init>(Lepson/print/IprintHome;)V

    return-object v0
.end method

.method isJapaneseDevice()Z
    .locals 2

    .line 1013
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->JAPAN:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1014
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->JAPANESE:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    return v0

    :cond_1
    :goto_0
    const/4 v0, 0x1

    return v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    const/4 p3, -0x1

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 244
    :pswitch_0
    invoke-direct {p0}, Lepson/print/IprintHome;->requestLocationPermission()V

    goto :goto_0

    :pswitch_1
    if-eq p2, p3, :cond_1

    const/4 p1, 0x1

    .line 237
    iput-boolean p1, p0, Lepson/print/IprintHome;->bRejectLocationPermission:Z

    goto :goto_0

    :pswitch_2
    if-eq p2, p3, :cond_0

    .line 224
    invoke-virtual {p0}, Lepson/print/IprintHome;->finish()V

    return-void

    .line 227
    :cond_0
    invoke-virtual {p0}, Lepson/print/IprintHome;->startGuideActivity()V

    return-void

    :cond_1
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 1

    const/4 v0, 0x1

    .line 356
    invoke-virtual {p0, v0}, Lepson/print/IprintHome;->moveTaskToBack(Z)Z

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    const-string v0, "IprintHome"

    const-string v1, "onConfigurationChanged"

    .line 251
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 255
    invoke-direct {p0}, Lepson/print/IprintHome;->updateMenu()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .line 120
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    .line 123
    invoke-virtual {p0}, Lepson/print/IprintHome;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0a0080

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lepson/print/IprintHome;->mLayout:Landroid/view/ViewGroup;

    .line 126
    iget-object v0, p0, Lepson/print/IprintHome;->mLayout:Landroid/view/ViewGroup;

    const v1, 0x7f080214

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;

    iput-object v0, p0, Lepson/print/IprintHome;->menulist:Lcom/felipecsl/asymmetricgridview/library/widget/AsymmetricGridView;

    .line 129
    iget-object v0, p0, Lepson/print/IprintHome;->mLayout:Landroid/view/ViewGroup;

    const v1, 0x7f080181

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lepson/print/IprintHome;->printerName:Landroid/widget/TextView;

    .line 130
    iget-object v0, p0, Lepson/print/IprintHome;->mLayout:Landroid/view/ViewGroup;

    const v1, 0x7f080183

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lepson/print/IprintHome;->printerStatus:Landroid/widget/TextView;

    .line 133
    iget-object v0, p0, Lepson/print/IprintHome;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lepson/print/IprintHome;->setContentView(Landroid/view/View;)V

    const v0, 0x7f0e02ac

    const/4 v1, 0x0

    .line 136
    invoke-virtual {p0, v0, v1}, Lepson/print/IprintHome;->setActionBar(IZ)V

    .line 138
    sget-object v0, Lepson/print/IprintHome;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    if-nez v0, :cond_0

    .line 139
    invoke-static {}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->getInstance()Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object v0

    sput-object v0, Lepson/print/IprintHome;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    .line 144
    :cond_0
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lepson/print/IprintHome;->DEFAULT_FOLDER:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ".print"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 145
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 147
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    goto :goto_0

    .line 156
    :cond_1
    new-instance v0, Ljava/io/File;

    sget-object v1, Lepson/print/IprintHome;->DEFAULT_FOLDER:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 157
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lepson/print/IprintHome;->DEFAULT_FOLDER:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ".print"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 163
    :goto_0
    invoke-static {p0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v0

    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->initTempViewDir()V

    .line 164
    invoke-static {p0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v0

    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->initTempCRDir()V

    .line 165
    invoke-static {p0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v0

    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->initPrintDir()V

    .line 169
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_2

    const-string v0, "LOGIN_PREFERENCES"

    .line 170
    invoke-virtual {p0, v0}, Lepson/print/IprintHome;->deleteSharedPreferences(Ljava/lang/String;)Z

    goto :goto_1

    :cond_2
    const-string v0, "LOGIN_PREFERENCES"

    const/4 v1, 0x3

    .line 172
    invoke-virtual {p0, v0, v1}, Lepson/print/IprintHome;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 173
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 176
    :goto_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lepson/print/IprintHome;->mForegroundRunnableList:Ljava/util/ArrayList;

    const/4 v0, 0x1

    .line 177
    iput-boolean v0, p0, Lepson/print/IprintHome;->mActivityForeground:Z

    const v0, 0x7f080185

    .line 180
    invoke-virtual {p0, v0}, Lepson/print/IprintHome;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lepson/print/IprintHome$1;

    invoke-direct {v1, p0}, Lepson/print/IprintHome$1;-><init>(Lepson/print/IprintHome;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    if-nez p1, :cond_3

    .line 192
    invoke-direct {p0}, Lepson/print/IprintHome;->startLicenseCheckActivity()V

    goto :goto_2

    .line 194
    :cond_3
    invoke-virtual {p0}, Lepson/print/IprintHome;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lepson/common/IprintLicenseInfo;->isAgreedCurrentVersion(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_4

    .line 195
    invoke-virtual {p0}, Lepson/print/IprintHome;->finish()V

    :cond_4
    :goto_2
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .line 273
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 274
    invoke-virtual {p0}, Lepson/print/IprintHome;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0b0001

    .line 275
    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const/4 p1, 0x1

    return p1
.end method

.method protected onDestroy()V
    .locals 1

    .line 388
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onDestroy()V

    .line 390
    sget-object v0, Lepson/print/IprintHome;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doReleaseDriver()I

    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2

    .line 395
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onNewIntent(Landroid/content/Intent;)V

    .line 397
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.nfc.action.NDEF_DISCOVERED"

    .line 398
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 399
    invoke-static {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils;->parseNECTag(Landroid/content/Context;Landroid/content/Intent;)Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 405
    iget-object v0, p0, Lepson/print/IprintHome;->printerId:Ljava/lang/String;

    invoke-static {p0, v0, p1}, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils;->isNeedChangePrinter(Landroid/content/Context;Ljava/lang/String;Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 407
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 408
    const-class v1, Lepson/print/ActivityNfcPrinter;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v1, "connectInfo"

    .line 409
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 410
    invoke-virtual {p0, v0}, Lepson/print/IprintHome;->startActivity(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .line 262
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0801a2

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 264
    :cond_0
    invoke-direct {p0}, Lepson/print/IprintHome;->callInformation()V

    .line 267
    :goto_0
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1
.end method

.method public onPause()V
    .locals 4

    const-string v0, "IprintHome"

    const-string v1, "onPause()"

    .line 360
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onPause()V

    .line 364
    iget-object v0, p0, Lepson/print/IprintHome;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 365
    iget-object v0, p0, Lepson/print/IprintHome;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 367
    iget v0, p0, Lepson/print/IprintHome;->printerLocation:I

    if-eq v0, v2, :cond_0

    const/4 v3, 0x3

    if-ne v0, v3, :cond_1

    .line 368
    :cond_0
    iget-object v0, p0, Lepson/print/IprintHome;->task:Lepson/print/IprintHome$ProbePrinter;

    if-eqz v0, :cond_1

    .line 369
    invoke-virtual {v0, v2}, Lepson/print/IprintHome$ProbePrinter;->cancelTask(Z)Z

    const/4 v0, 0x0

    .line 370
    iput-object v0, p0, Lepson/print/IprintHome;->task:Lepson/print/IprintHome$ProbePrinter;

    .line 374
    :cond_1
    invoke-static {}, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->blePrinterCheckStop()V

    .line 376
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils;->disableForegroundDispatch(Landroid/app/Activity;)V

    .line 378
    iput-boolean v1, p0, Lepson/print/IprintHome;->mActivityForeground:Z

    return-void
.end method

.method protected onResume()V
    .locals 2

    const-string v0, "IprintHome"

    const-string v1, "onResume()"

    .line 290
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    invoke-static {}, Lcom/epson/mobilephone/common/EpLog;->i()V

    .line 292
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onResume()V

    .line 294
    invoke-static {p0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v0

    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->initTempViewDir()V

    .line 295
    invoke-static {p0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v0

    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->initTempCRDir()V

    .line 296
    invoke-static {p0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v0

    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->initPrintDir()V

    .line 299
    new-instance v0, Lepson/print/screen/PrintSetting;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lepson/print/screen/PrintSetting;-><init>(Landroid/content/Context;Lepson/print/screen/PrintSetting$Kind;)V

    .line 300
    invoke-virtual {v0}, Lepson/print/screen/PrintSetting;->resetPrintSettings()V

    .line 303
    invoke-direct {p0}, Lepson/print/IprintHome;->updatePrinterInfo()V

    .line 305
    move-object v0, v1

    check-cast v0, [[Ljava/lang/String;

    invoke-static {p0, v1, v0}, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils;->enableForegroundDispatch(Landroid/app/Activity;[Landroid/content/IntentFilter;[[Ljava/lang/String;)V

    const-string v0, "Home"

    .line 307
    invoke-static {p0, v0}, Lcom/epson/iprint/prtlogger/Analytics;->countScreen(Landroid/content/Context;Ljava/lang/String;)V

    .line 310
    invoke-direct {p0}, Lepson/print/IprintHome;->executeForegroundQueue()Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    .line 311
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/epson/mobilephone/common/ble/BleWork;->isStartBleProcess(Landroid/content/Context;Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 312
    invoke-static {p0}, Lcom/epson/mobilephone/common/ble/BleAsyncTask;->blePrinterCheck(Landroid/content/Context;)V

    .line 316
    :cond_0
    iput-boolean v1, p0, Lepson/print/IprintHome;->mActivityForeground:Z

    return-void
.end method

.method protected onStop()V
    .locals 0

    .line 383
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onStop()V

    return-void
.end method

.method public startGuideActivity()V
    .locals 3

    .line 1024
    new-instance v0, Lepson/common/InformationGuide;

    invoke-virtual {p0}, Lepson/print/IprintHome;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lepson/common/InformationGuide;-><init>(Landroid/content/Context;)V

    .line 1025
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1026
    sget-object v2, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->KEY_HTML_PATH:Ljava/lang/String;

    invoke-virtual {v0}, Lepson/common/InformationGuide;->getHtmlPath()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1027
    sget-object v0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->KEY_GUIDE_VER:Ljava/lang/String;

    sget v2, Lepson/common/InformationGuide;->GUIDE_VER:I

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1028
    sget-object v0, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->KEY_BOOT_MODE:Ljava/lang/String;

    sget v2, Lcom/epson/mobilephone/common/guide/NoticeGuideActivity;->MODE_AUTO:I

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 v0, 0x3

    .line 1029
    invoke-virtual {p0, v1, v0}, Lepson/print/IprintHome;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

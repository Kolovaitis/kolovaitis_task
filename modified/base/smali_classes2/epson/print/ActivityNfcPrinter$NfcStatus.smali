.class final enum Lepson/print/ActivityNfcPrinter$NfcStatus;
.super Ljava/lang/Enum;
.source "ActivityNfcPrinter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/ActivityNfcPrinter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "NfcStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lepson/print/ActivityNfcPrinter$NfcStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lepson/print/ActivityNfcPrinter$NfcStatus;

.field public static final enum CONNECTING_PRINTER_VIA_INFRA:Lepson/print/ActivityNfcPrinter$NfcStatus;

.field public static final enum CONNECTING_PRINTER_VIA_SIMPLEAP:Lepson/print/ActivityNfcPrinter$NfcStatus;

.field public static final enum CONNECTING_SIMPLEAP:Lepson/print/ActivityNfcPrinter$NfcStatus;

.field public static final enum ENABLING_WIFI:Lepson/print/ActivityNfcPrinter$NfcStatus;

.field public static final enum INIT:Lepson/print/ActivityNfcPrinter$NfcStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 89
    new-instance v0, Lepson/print/ActivityNfcPrinter$NfcStatus;

    const-string v1, "INIT"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lepson/print/ActivityNfcPrinter$NfcStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/ActivityNfcPrinter$NfcStatus;->INIT:Lepson/print/ActivityNfcPrinter$NfcStatus;

    .line 90
    new-instance v0, Lepson/print/ActivityNfcPrinter$NfcStatus;

    const-string v1, "ENABLING_WIFI"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lepson/print/ActivityNfcPrinter$NfcStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/ActivityNfcPrinter$NfcStatus;->ENABLING_WIFI:Lepson/print/ActivityNfcPrinter$NfcStatus;

    .line 91
    new-instance v0, Lepson/print/ActivityNfcPrinter$NfcStatus;

    const-string v1, "CONNECTING_PRINTER_VIA_INFRA"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lepson/print/ActivityNfcPrinter$NfcStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/ActivityNfcPrinter$NfcStatus;->CONNECTING_PRINTER_VIA_INFRA:Lepson/print/ActivityNfcPrinter$NfcStatus;

    .line 92
    new-instance v0, Lepson/print/ActivityNfcPrinter$NfcStatus;

    const-string v1, "CONNECTING_SIMPLEAP"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lepson/print/ActivityNfcPrinter$NfcStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/ActivityNfcPrinter$NfcStatus;->CONNECTING_SIMPLEAP:Lepson/print/ActivityNfcPrinter$NfcStatus;

    .line 93
    new-instance v0, Lepson/print/ActivityNfcPrinter$NfcStatus;

    const-string v1, "CONNECTING_PRINTER_VIA_SIMPLEAP"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Lepson/print/ActivityNfcPrinter$NfcStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/ActivityNfcPrinter$NfcStatus;->CONNECTING_PRINTER_VIA_SIMPLEAP:Lepson/print/ActivityNfcPrinter$NfcStatus;

    const/4 v0, 0x5

    .line 88
    new-array v0, v0, [Lepson/print/ActivityNfcPrinter$NfcStatus;

    sget-object v1, Lepson/print/ActivityNfcPrinter$NfcStatus;->INIT:Lepson/print/ActivityNfcPrinter$NfcStatus;

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/ActivityNfcPrinter$NfcStatus;->ENABLING_WIFI:Lepson/print/ActivityNfcPrinter$NfcStatus;

    aput-object v1, v0, v3

    sget-object v1, Lepson/print/ActivityNfcPrinter$NfcStatus;->CONNECTING_PRINTER_VIA_INFRA:Lepson/print/ActivityNfcPrinter$NfcStatus;

    aput-object v1, v0, v4

    sget-object v1, Lepson/print/ActivityNfcPrinter$NfcStatus;->CONNECTING_SIMPLEAP:Lepson/print/ActivityNfcPrinter$NfcStatus;

    aput-object v1, v0, v5

    sget-object v1, Lepson/print/ActivityNfcPrinter$NfcStatus;->CONNECTING_PRINTER_VIA_SIMPLEAP:Lepson/print/ActivityNfcPrinter$NfcStatus;

    aput-object v1, v0, v6

    sput-object v0, Lepson/print/ActivityNfcPrinter$NfcStatus;->$VALUES:[Lepson/print/ActivityNfcPrinter$NfcStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 88
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lepson/print/ActivityNfcPrinter$NfcStatus;
    .locals 1

    .line 88
    const-class v0, Lepson/print/ActivityNfcPrinter$NfcStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lepson/print/ActivityNfcPrinter$NfcStatus;

    return-object p0
.end method

.method public static values()[Lepson/print/ActivityNfcPrinter$NfcStatus;
    .locals 1

    .line 88
    sget-object v0, Lepson/print/ActivityNfcPrinter$NfcStatus;->$VALUES:[Lepson/print/ActivityNfcPrinter$NfcStatus;

    invoke-virtual {v0}, [Lepson/print/ActivityNfcPrinter$NfcStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lepson/print/ActivityNfcPrinter$NfcStatus;

    return-object v0
.end method

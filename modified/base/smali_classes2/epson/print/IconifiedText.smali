.class public Lepson/print/IconifiedText;
.super Ljava/lang/Object;
.source "IconifiedText.java"

# interfaces
.implements Ljava/lang/Comparable;
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "Lepson/print/IconifiedText;",
        ">;",
        "Ljava/util/Comparator<",
        "Lepson/print/IconifiedText;",
        ">;"
    }
.end annotation


# instance fields
.field private mIcon:Landroid/graphics/drawable/Drawable;

.field private mIsFolder:Z

.field private mSelectable:Z

.field private mText:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    .line 9
    iput-object v0, p0, Lepson/print/IconifiedText;->mText:Ljava/lang/String;

    const/4 v0, 0x1

    .line 13
    iput-boolean v0, p0, Lepson/print/IconifiedText;->mSelectable:Z

    const/4 v0, 0x0

    .line 15
    iput-boolean v0, p0, Lepson/print/IconifiedText;->mIsFolder:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/graphics/drawable/Drawable;Z)V
    .locals 1

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    .line 9
    iput-object v0, p0, Lepson/print/IconifiedText;->mText:Ljava/lang/String;

    const/4 v0, 0x1

    .line 13
    iput-boolean v0, p0, Lepson/print/IconifiedText;->mSelectable:Z

    const/4 v0, 0x0

    .line 15
    iput-boolean v0, p0, Lepson/print/IconifiedText;->mIsFolder:Z

    .line 22
    iput-object p2, p0, Lepson/print/IconifiedText;->mIcon:Landroid/graphics/drawable/Drawable;

    .line 24
    iput-object p1, p0, Lepson/print/IconifiedText;->mText:Ljava/lang/String;

    .line 26
    iput-boolean p3, p0, Lepson/print/IconifiedText;->mIsFolder:Z

    return-void
.end method


# virtual methods
.method public compare(Lepson/print/IconifiedText;Lepson/print/IconifiedText;)I
    .locals 2

    if-eqz p1, :cond_3

    if-nez p2, :cond_0

    goto :goto_0

    .line 105
    :cond_0
    iget-object v0, p1, Lepson/print/IconifiedText;->mText:Ljava/lang/String;

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 p1, -0x1

    return p1

    .line 107
    :cond_1
    iget-object v0, p2, Lepson/print/IconifiedText;->mText:Ljava/lang/String;

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 p1, 0x1

    return p1

    .line 111
    :cond_2
    iget-object p1, p1, Lepson/print/IconifiedText;->mText:Ljava/lang/String;

    iget-object p2, p2, Lepson/print/IconifiedText;->mText:Ljava/lang/String;

    invoke-virtual {p1, p2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result p1

    return p1

    :cond_3
    :goto_0
    const/4 p1, 0x0

    return p1
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    .line 7
    check-cast p1, Lepson/print/IconifiedText;

    check-cast p2, Lepson/print/IconifiedText;

    invoke-virtual {p0, p1, p2}, Lepson/print/IconifiedText;->compare(Lepson/print/IconifiedText;Lepson/print/IconifiedText;)I

    move-result p1

    return p1
.end method

.method public compareTo(Lepson/print/IconifiedText;)I
    .locals 1

    .line 88
    iget-object v0, p0, Lepson/print/IconifiedText;->mText:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 90
    invoke-virtual {p1}, Lepson/print/IconifiedText;->getText()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    return p1

    .line 94
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-direct {p1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 7
    check-cast p1, Lepson/print/IconifiedText;

    invoke-virtual {p0, p1}, Lepson/print/IconifiedText;->compareTo(Lepson/print/IconifiedText;)I

    move-result p1

    return p1
.end method

.method public getIcon()Landroid/graphics/drawable/Drawable;
    .locals 1

    .line 78
    iget-object v0, p0, Lepson/print/IconifiedText;->mIcon:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .line 54
    iget-object v0, p0, Lepson/print/IconifiedText;->mText:Ljava/lang/String;

    return-object v0
.end method

.method public isSelectable()Z
    .locals 1

    .line 33
    iget-boolean v0, p0, Lepson/print/IconifiedText;->mSelectable:Z

    return v0
.end method

.method public ismIsFolder()Z
    .locals 1

    .line 47
    iget-boolean v0, p0, Lepson/print/IconifiedText;->mIsFolder:Z

    return v0
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .line 70
    iput-object p1, p0, Lepson/print/IconifiedText;->mIcon:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method public setSelectable(Z)V
    .locals 0

    .line 41
    iput-boolean p1, p0, Lepson/print/IconifiedText;->mSelectable:Z

    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 0

    .line 62
    iput-object p1, p0, Lepson/print/IconifiedText;->mText:Ljava/lang/String;

    return-void
.end method

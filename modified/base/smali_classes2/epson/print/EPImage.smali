.class public Lepson/print/EPImage;
.super Ljava/lang/Object;
.source "EPImage.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lepson/print/EPImage;",
            ">;"
        }
    .end annotation
.end field

.field public static final EPS_ROTATE_0:I = 0x0

.field public static final EPS_ROTATE_180:I = 0xb4

.field public static final EPS_ROTATE_270:I = 0x10e

.field public static final EPS_ROTATE_90:I = 0x5a

.field public static final FILE_INFO_NO_INFO:I = 0x0

.field public static final FILE_INFO_WITH_FILE_TYPE:I = 0x2

.field public static final FILE_INFO_WITH_ORG_FILE_NAME:I = 0x1

.field public static final FILE_TYPE_BMP:I = 0x1

.field public static final FILE_TYPE_JPEG:I = 0x2

.field public static final FILE_TYPE_PNG:I = 0x3

.field public static final FILE_TYPE_SCAN:I = 0x1002

.field public static final FILE_TYPE_TIFF:I = 0x4

.field public static final FILE_TYPE_UNKNOWN:I = 0x0

.field public static final FILE_TYPE_WEB:I = 0x1001

.field private static final TAG:Ljava/lang/String; = "EPImage"


# instance fields
.field public decodeHeight:I

.field public volatile decodeImageFileName:Ljava/lang/String;

.field public decodeWidth:I

.field public index:I

.field public isNotPhot:Z

.field public volatile isPaperLandScape:Z

.field public volatile loadImageFileName:Ljava/lang/String;

.field private mEpsonImage:Lepson/image/epsonImage;

.field private mFileInfo:I

.field private mFileType:I

.field private mOrgFileNameForLogger:Ljava/lang/String;

.field public volatile previewHeight:I

.field public previewImageFileName:Ljava/lang/String;

.field public volatile previewImageRectBottom:F

.field public previewImageRectCenterX:I

.field public previewImageRectCenterY:I

.field public volatile previewImageRectLeft:F

.field public volatile previewImageRectRight:F

.field public volatile previewImageRectTop:F

.field public volatile previewPaperRectBottom:I

.field public volatile previewPaperRectLeft:I

.field public volatile previewPaperRectRight:I

.field public volatile previewPaperRectTop:I

.field public volatile previewWidth:I

.field public printImageFileName:Ljava/lang/String;

.field public rotate:I

.field public scaleFactor:F

.field public volatile srcHeight:I

.field public volatile srcWidth:I

.field public thumbnailImageFileName:Ljava/lang/String;

.field public webUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 288
    new-instance v0, Lepson/print/EPImage$1;

    invoke-direct {v0}, Lepson/print/EPImage$1;-><init>()V

    sput-object v0, Lepson/print/EPImage;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 23
    iput v0, p0, Lepson/print/EPImage;->index:I

    const/4 v1, 0x0

    .line 27
    iput-object v1, p0, Lepson/print/EPImage;->loadImageFileName:Ljava/lang/String;

    .line 30
    iput v0, p0, Lepson/print/EPImage;->srcWidth:I

    .line 31
    iput v0, p0, Lepson/print/EPImage;->srcHeight:I

    .line 34
    iput-object v1, p0, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    .line 35
    iput v0, p0, Lepson/print/EPImage;->decodeWidth:I

    .line 36
    iput v0, p0, Lepson/print/EPImage;->decodeHeight:I

    .line 39
    iput-object v1, p0, Lepson/print/EPImage;->thumbnailImageFileName:Ljava/lang/String;

    .line 42
    iput-object v1, p0, Lepson/print/EPImage;->previewImageFileName:Ljava/lang/String;

    .line 45
    iput v0, p0, Lepson/print/EPImage;->previewWidth:I

    .line 46
    iput v0, p0, Lepson/print/EPImage;->previewHeight:I

    .line 49
    iput-object v1, p0, Lepson/print/EPImage;->printImageFileName:Ljava/lang/String;

    .line 51
    iput-boolean v0, p0, Lepson/print/EPImage;->isPaperLandScape:Z

    .line 52
    iput-boolean v0, p0, Lepson/print/EPImage;->isNotPhot:Z

    .line 53
    iput v0, p0, Lepson/print/EPImage;->rotate:I

    .line 55
    iput v0, p0, Lepson/print/EPImage;->previewPaperRectLeft:I

    .line 56
    iput v0, p0, Lepson/print/EPImage;->previewPaperRectTop:I

    .line 57
    iput v0, p0, Lepson/print/EPImage;->previewPaperRectRight:I

    .line 58
    iput v0, p0, Lepson/print/EPImage;->previewPaperRectBottom:I

    .line 60
    iput v0, p0, Lepson/print/EPImage;->previewImageRectCenterX:I

    .line 61
    iput v0, p0, Lepson/print/EPImage;->previewImageRectCenterY:I

    const/4 v2, 0x0

    .line 62
    iput v2, p0, Lepson/print/EPImage;->previewImageRectLeft:F

    .line 63
    iput v2, p0, Lepson/print/EPImage;->previewImageRectTop:F

    .line 64
    iput v2, p0, Lepson/print/EPImage;->previewImageRectRight:F

    .line 65
    iput v2, p0, Lepson/print/EPImage;->previewImageRectBottom:F

    const/high16 v2, 0x3f800000    # 1.0f

    .line 67
    iput v2, p0, Lepson/print/EPImage;->scaleFactor:F

    .line 69
    iput-object v1, p0, Lepson/print/EPImage;->webUrl:Ljava/lang/String;

    .line 88
    iput v0, p0, Lepson/print/EPImage;->mFileInfo:I

    .line 89
    iput v0, p0, Lepson/print/EPImage;->mFileType:I

    .line 93
    iput-object v1, p0, Lepson/print/EPImage;->mOrgFileNameForLogger:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .line 303
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 23
    iput v0, p0, Lepson/print/EPImage;->index:I

    const/4 v1, 0x0

    .line 27
    iput-object v1, p0, Lepson/print/EPImage;->loadImageFileName:Ljava/lang/String;

    .line 30
    iput v0, p0, Lepson/print/EPImage;->srcWidth:I

    .line 31
    iput v0, p0, Lepson/print/EPImage;->srcHeight:I

    .line 34
    iput-object v1, p0, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    .line 35
    iput v0, p0, Lepson/print/EPImage;->decodeWidth:I

    .line 36
    iput v0, p0, Lepson/print/EPImage;->decodeHeight:I

    .line 39
    iput-object v1, p0, Lepson/print/EPImage;->thumbnailImageFileName:Ljava/lang/String;

    .line 42
    iput-object v1, p0, Lepson/print/EPImage;->previewImageFileName:Ljava/lang/String;

    .line 45
    iput v0, p0, Lepson/print/EPImage;->previewWidth:I

    .line 46
    iput v0, p0, Lepson/print/EPImage;->previewHeight:I

    .line 49
    iput-object v1, p0, Lepson/print/EPImage;->printImageFileName:Ljava/lang/String;

    .line 51
    iput-boolean v0, p0, Lepson/print/EPImage;->isPaperLandScape:Z

    .line 52
    iput-boolean v0, p0, Lepson/print/EPImage;->isNotPhot:Z

    .line 53
    iput v0, p0, Lepson/print/EPImage;->rotate:I

    .line 55
    iput v0, p0, Lepson/print/EPImage;->previewPaperRectLeft:I

    .line 56
    iput v0, p0, Lepson/print/EPImage;->previewPaperRectTop:I

    .line 57
    iput v0, p0, Lepson/print/EPImage;->previewPaperRectRight:I

    .line 58
    iput v0, p0, Lepson/print/EPImage;->previewPaperRectBottom:I

    .line 60
    iput v0, p0, Lepson/print/EPImage;->previewImageRectCenterX:I

    .line 61
    iput v0, p0, Lepson/print/EPImage;->previewImageRectCenterY:I

    const/4 v2, 0x0

    .line 62
    iput v2, p0, Lepson/print/EPImage;->previewImageRectLeft:F

    .line 63
    iput v2, p0, Lepson/print/EPImage;->previewImageRectTop:F

    .line 64
    iput v2, p0, Lepson/print/EPImage;->previewImageRectRight:F

    .line 65
    iput v2, p0, Lepson/print/EPImage;->previewImageRectBottom:F

    const/high16 v2, 0x3f800000    # 1.0f

    .line 67
    iput v2, p0, Lepson/print/EPImage;->scaleFactor:F

    .line 69
    iput-object v1, p0, Lepson/print/EPImage;->webUrl:Ljava/lang/String;

    .line 88
    iput v0, p0, Lepson/print/EPImage;->mFileInfo:I

    .line 89
    iput v0, p0, Lepson/print/EPImage;->mFileType:I

    .line 93
    iput-object v1, p0, Lepson/print/EPImage;->mOrgFileNameForLogger:Ljava/lang/String;

    .line 304
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lepson/print/EPImage;->index:I

    .line 307
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lepson/print/EPImage;->loadImageFileName:Ljava/lang/String;

    .line 308
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lepson/print/EPImage;->srcWidth:I

    .line 309
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lepson/print/EPImage;->srcHeight:I

    .line 312
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    .line 313
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lepson/print/EPImage;->decodeWidth:I

    .line 314
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lepson/print/EPImage;->decodeHeight:I

    .line 317
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lepson/print/EPImage;->thumbnailImageFileName:Ljava/lang/String;

    .line 320
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lepson/print/EPImage;->previewImageFileName:Ljava/lang/String;

    .line 321
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lepson/print/EPImage;->previewWidth:I

    .line 322
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lepson/print/EPImage;->previewHeight:I

    .line 325
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lepson/print/EPImage;->printImageFileName:Ljava/lang/String;

    const/4 v1, 0x1

    .line 327
    new-array v1, v1, [Z

    .line 328
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readBooleanArray([Z)V

    .line 329
    aget-boolean v2, v1, v0

    iput-boolean v2, p0, Lepson/print/EPImage;->isPaperLandScape:Z

    .line 330
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readBooleanArray([Z)V

    .line 331
    aget-boolean v0, v1, v0

    iput-boolean v0, p0, Lepson/print/EPImage;->isNotPhot:Z

    .line 333
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lepson/print/EPImage;->rotate:I

    .line 334
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lepson/print/EPImage;->previewPaperRectLeft:I

    .line 335
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lepson/print/EPImage;->previewPaperRectTop:I

    .line 336
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lepson/print/EPImage;->previewPaperRectRight:I

    .line 337
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lepson/print/EPImage;->previewPaperRectBottom:I

    .line 339
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lepson/print/EPImage;->previewImageRectCenterX:I

    .line 340
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lepson/print/EPImage;->previewImageRectCenterY:I

    .line 341
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lepson/print/EPImage;->previewImageRectLeft:F

    .line 342
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lepson/print/EPImage;->previewImageRectTop:F

    .line 343
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lepson/print/EPImage;->previewImageRectRight:F

    .line 344
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lepson/print/EPImage;->previewImageRectBottom:F

    .line 345
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lepson/print/EPImage;->scaleFactor:F

    .line 348
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepson/print/EPImage;->webUrl:Ljava/lang/String;

    .line 351
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lepson/print/EPImage;->mFileInfo:I

    .line 352
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lepson/print/EPImage;->mFileType:I

    .line 353
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/print/EPImage;->mOrgFileNameForLogger:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lepson/print/EPImage$1;)V
    .locals 0

    .line 14
    invoke-direct {p0, p1}, Lepson/print/EPImage;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lepson/print/EPImage;)V
    .locals 3

    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 23
    iput v0, p0, Lepson/print/EPImage;->index:I

    const/4 v1, 0x0

    .line 27
    iput-object v1, p0, Lepson/print/EPImage;->loadImageFileName:Ljava/lang/String;

    .line 30
    iput v0, p0, Lepson/print/EPImage;->srcWidth:I

    .line 31
    iput v0, p0, Lepson/print/EPImage;->srcHeight:I

    .line 34
    iput-object v1, p0, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    .line 35
    iput v0, p0, Lepson/print/EPImage;->decodeWidth:I

    .line 36
    iput v0, p0, Lepson/print/EPImage;->decodeHeight:I

    .line 39
    iput-object v1, p0, Lepson/print/EPImage;->thumbnailImageFileName:Ljava/lang/String;

    .line 42
    iput-object v1, p0, Lepson/print/EPImage;->previewImageFileName:Ljava/lang/String;

    .line 45
    iput v0, p0, Lepson/print/EPImage;->previewWidth:I

    .line 46
    iput v0, p0, Lepson/print/EPImage;->previewHeight:I

    .line 49
    iput-object v1, p0, Lepson/print/EPImage;->printImageFileName:Ljava/lang/String;

    .line 51
    iput-boolean v0, p0, Lepson/print/EPImage;->isPaperLandScape:Z

    .line 52
    iput-boolean v0, p0, Lepson/print/EPImage;->isNotPhot:Z

    .line 53
    iput v0, p0, Lepson/print/EPImage;->rotate:I

    .line 55
    iput v0, p0, Lepson/print/EPImage;->previewPaperRectLeft:I

    .line 56
    iput v0, p0, Lepson/print/EPImage;->previewPaperRectTop:I

    .line 57
    iput v0, p0, Lepson/print/EPImage;->previewPaperRectRight:I

    .line 58
    iput v0, p0, Lepson/print/EPImage;->previewPaperRectBottom:I

    .line 60
    iput v0, p0, Lepson/print/EPImage;->previewImageRectCenterX:I

    .line 61
    iput v0, p0, Lepson/print/EPImage;->previewImageRectCenterY:I

    const/4 v2, 0x0

    .line 62
    iput v2, p0, Lepson/print/EPImage;->previewImageRectLeft:F

    .line 63
    iput v2, p0, Lepson/print/EPImage;->previewImageRectTop:F

    .line 64
    iput v2, p0, Lepson/print/EPImage;->previewImageRectRight:F

    .line 65
    iput v2, p0, Lepson/print/EPImage;->previewImageRectBottom:F

    const/high16 v2, 0x3f800000    # 1.0f

    .line 67
    iput v2, p0, Lepson/print/EPImage;->scaleFactor:F

    .line 69
    iput-object v1, p0, Lepson/print/EPImage;->webUrl:Ljava/lang/String;

    .line 88
    iput v0, p0, Lepson/print/EPImage;->mFileInfo:I

    .line 89
    iput v0, p0, Lepson/print/EPImage;->mFileType:I

    .line 93
    iput-object v1, p0, Lepson/print/EPImage;->mOrgFileNameForLogger:Ljava/lang/String;

    .line 104
    iget v0, p1, Lepson/print/EPImage;->index:I

    iput v0, p0, Lepson/print/EPImage;->index:I

    .line 105
    iget-object v0, p1, Lepson/print/EPImage;->loadImageFileName:Ljava/lang/String;

    iput-object v0, p0, Lepson/print/EPImage;->loadImageFileName:Ljava/lang/String;

    .line 106
    iget v0, p1, Lepson/print/EPImage;->srcWidth:I

    iput v0, p0, Lepson/print/EPImage;->srcWidth:I

    .line 107
    iget v0, p1, Lepson/print/EPImage;->srcHeight:I

    iput v0, p0, Lepson/print/EPImage;->srcHeight:I

    .line 108
    iget-object v0, p1, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    iput-object v0, p0, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    .line 109
    iget v0, p1, Lepson/print/EPImage;->decodeWidth:I

    iput v0, p0, Lepson/print/EPImage;->decodeWidth:I

    .line 110
    iget v0, p1, Lepson/print/EPImage;->decodeHeight:I

    iput v0, p0, Lepson/print/EPImage;->decodeHeight:I

    .line 111
    iget-object v0, p1, Lepson/print/EPImage;->thumbnailImageFileName:Ljava/lang/String;

    iput-object v0, p0, Lepson/print/EPImage;->thumbnailImageFileName:Ljava/lang/String;

    .line 112
    iget-object v0, p1, Lepson/print/EPImage;->previewImageFileName:Ljava/lang/String;

    iput-object v0, p0, Lepson/print/EPImage;->previewImageFileName:Ljava/lang/String;

    .line 113
    iget v0, p1, Lepson/print/EPImage;->previewWidth:I

    iput v0, p0, Lepson/print/EPImage;->previewWidth:I

    .line 114
    iget v0, p1, Lepson/print/EPImage;->previewHeight:I

    iput v0, p0, Lepson/print/EPImage;->previewHeight:I

    .line 115
    iget-object v0, p1, Lepson/print/EPImage;->printImageFileName:Ljava/lang/String;

    iput-object v0, p0, Lepson/print/EPImage;->printImageFileName:Ljava/lang/String;

    .line 116
    iget-boolean v0, p1, Lepson/print/EPImage;->isPaperLandScape:Z

    iput-boolean v0, p0, Lepson/print/EPImage;->isPaperLandScape:Z

    .line 117
    iget-boolean v0, p1, Lepson/print/EPImage;->isNotPhot:Z

    iput-boolean v0, p0, Lepson/print/EPImage;->isNotPhot:Z

    .line 118
    iget v0, p1, Lepson/print/EPImage;->rotate:I

    iput v0, p0, Lepson/print/EPImage;->rotate:I

    .line 119
    iget v0, p1, Lepson/print/EPImage;->previewPaperRectLeft:I

    iput v0, p0, Lepson/print/EPImage;->previewPaperRectLeft:I

    .line 120
    iget v0, p1, Lepson/print/EPImage;->previewPaperRectTop:I

    iput v0, p0, Lepson/print/EPImage;->previewPaperRectTop:I

    .line 121
    iget v0, p1, Lepson/print/EPImage;->previewPaperRectRight:I

    iput v0, p0, Lepson/print/EPImage;->previewPaperRectRight:I

    .line 122
    iget v0, p1, Lepson/print/EPImage;->previewPaperRectBottom:I

    iput v0, p0, Lepson/print/EPImage;->previewPaperRectBottom:I

    .line 123
    iget v0, p1, Lepson/print/EPImage;->previewImageRectCenterX:I

    iput v0, p0, Lepson/print/EPImage;->previewImageRectCenterX:I

    .line 124
    iget v0, p1, Lepson/print/EPImage;->previewImageRectCenterY:I

    iput v0, p0, Lepson/print/EPImage;->previewImageRectCenterY:I

    .line 125
    iget v0, p1, Lepson/print/EPImage;->previewImageRectLeft:F

    iput v0, p0, Lepson/print/EPImage;->previewImageRectLeft:F

    .line 126
    iget v0, p1, Lepson/print/EPImage;->previewImageRectTop:F

    iput v0, p0, Lepson/print/EPImage;->previewImageRectTop:F

    .line 127
    iget v0, p1, Lepson/print/EPImage;->previewImageRectRight:F

    iput v0, p0, Lepson/print/EPImage;->previewImageRectRight:F

    .line 128
    iget v0, p1, Lepson/print/EPImage;->previewImageRectBottom:F

    iput v0, p0, Lepson/print/EPImage;->previewImageRectBottom:F

    .line 129
    iget v0, p1, Lepson/print/EPImage;->scaleFactor:F

    iput v0, p0, Lepson/print/EPImage;->scaleFactor:F

    .line 130
    iget-object p1, p1, Lepson/print/EPImage;->webUrl:Ljava/lang/String;

    iput-object p1, p0, Lepson/print/EPImage;->webUrl:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 3

    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 23
    iput v0, p0, Lepson/print/EPImage;->index:I

    const/4 v1, 0x0

    .line 27
    iput-object v1, p0, Lepson/print/EPImage;->loadImageFileName:Ljava/lang/String;

    .line 30
    iput v0, p0, Lepson/print/EPImage;->srcWidth:I

    .line 31
    iput v0, p0, Lepson/print/EPImage;->srcHeight:I

    .line 34
    iput-object v1, p0, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    .line 35
    iput v0, p0, Lepson/print/EPImage;->decodeWidth:I

    .line 36
    iput v0, p0, Lepson/print/EPImage;->decodeHeight:I

    .line 39
    iput-object v1, p0, Lepson/print/EPImage;->thumbnailImageFileName:Ljava/lang/String;

    .line 42
    iput-object v1, p0, Lepson/print/EPImage;->previewImageFileName:Ljava/lang/String;

    .line 45
    iput v0, p0, Lepson/print/EPImage;->previewWidth:I

    .line 46
    iput v0, p0, Lepson/print/EPImage;->previewHeight:I

    .line 49
    iput-object v1, p0, Lepson/print/EPImage;->printImageFileName:Ljava/lang/String;

    .line 51
    iput-boolean v0, p0, Lepson/print/EPImage;->isPaperLandScape:Z

    .line 52
    iput-boolean v0, p0, Lepson/print/EPImage;->isNotPhot:Z

    .line 53
    iput v0, p0, Lepson/print/EPImage;->rotate:I

    .line 55
    iput v0, p0, Lepson/print/EPImage;->previewPaperRectLeft:I

    .line 56
    iput v0, p0, Lepson/print/EPImage;->previewPaperRectTop:I

    .line 57
    iput v0, p0, Lepson/print/EPImage;->previewPaperRectRight:I

    .line 58
    iput v0, p0, Lepson/print/EPImage;->previewPaperRectBottom:I

    .line 60
    iput v0, p0, Lepson/print/EPImage;->previewImageRectCenterX:I

    .line 61
    iput v0, p0, Lepson/print/EPImage;->previewImageRectCenterY:I

    const/4 v2, 0x0

    .line 62
    iput v2, p0, Lepson/print/EPImage;->previewImageRectLeft:F

    .line 63
    iput v2, p0, Lepson/print/EPImage;->previewImageRectTop:F

    .line 64
    iput v2, p0, Lepson/print/EPImage;->previewImageRectRight:F

    .line 65
    iput v2, p0, Lepson/print/EPImage;->previewImageRectBottom:F

    const/high16 v2, 0x3f800000    # 1.0f

    .line 67
    iput v2, p0, Lepson/print/EPImage;->scaleFactor:F

    .line 69
    iput-object v1, p0, Lepson/print/EPImage;->webUrl:Ljava/lang/String;

    .line 88
    iput v0, p0, Lepson/print/EPImage;->mFileInfo:I

    .line 89
    iput v0, p0, Lepson/print/EPImage;->mFileType:I

    .line 93
    iput-object v1, p0, Lepson/print/EPImage;->mOrgFileNameForLogger:Ljava/lang/String;

    .line 135
    iput p2, p0, Lepson/print/EPImage;->index:I

    .line 136
    iput-object p1, p0, Lepson/print/EPImage;->loadImageFileName:Ljava/lang/String;

    .line 139
    iget-object p2, p0, Lepson/print/EPImage;->loadImageFileName:Ljava/lang/String;

    if-nez p2, :cond_0

    const-string p1, "EPImage"

    const-string p2, "loadImageFileName is null"

    .line 140
    invoke-static {p1, p2}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 145
    :cond_0
    new-instance p2, Ljava/io/File;

    invoke-direct {p2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result p2

    if-nez p2, :cond_1

    const-string p2, "EPImage"

    .line 146
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " is not exists."

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p2, p1}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 151
    :cond_1
    new-instance p2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {p2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/4 v1, 0x1

    .line 152
    iput-boolean v1, p2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 153
    iget-object v2, p0, Lepson/print/EPImage;->loadImageFileName:Ljava/lang/String;

    invoke-static {v2, p2}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 154
    iget v2, p2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    if-lez v2, :cond_3

    iget v2, p2, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    if-lez v2, :cond_3

    .line 155
    iget p1, p2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iput p1, p0, Lepson/print/EPImage;->srcWidth:I

    .line 156
    iget p1, p2, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    iput p1, p0, Lepson/print/EPImage;->srcHeight:I

    .line 157
    iget p1, p0, Lepson/print/EPImage;->srcWidth:I

    iget p2, p0, Lepson/print/EPImage;->srcHeight:I

    if-le p1, p2, :cond_2

    .line 158
    iput-boolean v1, p0, Lepson/print/EPImage;->isPaperLandScape:Z

    goto :goto_0

    .line 160
    :cond_2
    iput-boolean v0, p0, Lepson/print/EPImage;->isPaperLandScape:Z

    goto :goto_0

    :cond_3
    const-string p2, "EPImage"

    const-string v2, "BitmapFactory.decodeFile Failed"

    .line 163
    invoke-static {p2, v2}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    iget-object p2, p0, Lepson/print/EPImage;->mEpsonImage:Lepson/image/epsonImage;

    if-nez p2, :cond_4

    .line 166
    new-instance p2, Lepson/image/epsonImage;

    invoke-direct {p2}, Lepson/image/epsonImage;-><init>()V

    iput-object p2, p0, Lepson/print/EPImage;->mEpsonImage:Lepson/image/epsonImage;

    .line 169
    :cond_4
    iget-object p2, p0, Lepson/print/EPImage;->mEpsonImage:Lepson/image/epsonImage;

    if-eqz p2, :cond_6

    const/4 v2, 0x2

    .line 170
    new-array v2, v2, [I

    .line 171
    invoke-virtual {p2, p1, v2}, Lepson/image/epsonImage;->epsmpGetImageSize2(Ljava/lang/String;[I)I

    .line 172
    aget p1, v2, v0

    iput p1, p0, Lepson/print/EPImage;->srcWidth:I

    .line 173
    aget p1, v2, v1

    iput p1, p0, Lepson/print/EPImage;->srcHeight:I

    .line 175
    iget p1, p0, Lepson/print/EPImage;->srcWidth:I

    iget p2, p0, Lepson/print/EPImage;->srcHeight:I

    if-le p1, p2, :cond_5

    .line 176
    iput-boolean v1, p0, Lepson/print/EPImage;->isPaperLandScape:Z

    goto :goto_0

    .line 178
    :cond_5
    iput-boolean v0, p0, Lepson/print/EPImage;->isPaperLandScape:Z

    .line 185
    :cond_6
    :goto_0
    :try_start_0
    new-instance p1, Landroid/media/ExifInterface;

    iget-object p2, p0, Lepson/print/EPImage;->loadImageFileName:Ljava/lang/String;

    invoke-direct {p1, p2}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    const-string p2, "Orientation"

    .line 186
    invoke-virtual {p1, p2, v0}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result p1

    const/4 p2, 0x6

    if-eq p1, p2, :cond_7

    const/16 p2, 0x8

    if-eq p1, p2, :cond_7

    goto :goto_1

    .line 191
    :cond_7
    iget p1, p0, Lepson/print/EPImage;->srcWidth:I

    .line 192
    iget p2, p0, Lepson/print/EPImage;->srcHeight:I

    iput p2, p0, Lepson/print/EPImage;->srcWidth:I

    .line 193
    iput p1, p0, Lepson/print/EPImage;->srcHeight:I

    .line 194
    iget-boolean p1, p0, Lepson/print/EPImage;->isPaperLandScape:Z

    if-nez p1, :cond_8

    const/4 v0, 0x1

    :cond_8
    iput-boolean v0, p0, Lepson/print/EPImage;->isPaperLandScape:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    .line 200
    invoke-virtual {p1}, Ljava/io/IOException;->printStackTrace()V

    :goto_1
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getExtention()Ljava/lang/String;
    .locals 3

    .line 226
    iget v0, p0, Lepson/print/EPImage;->mFileInfo:I

    packed-switch v0, :pswitch_data_0

    const-string v0, ""

    return-object v0

    :pswitch_0
    const-string v0, ""

    return-object v0

    .line 231
    :pswitch_1
    iget-object v0, p0, Lepson/print/EPImage;->mOrgFileNameForLogger:Ljava/lang/String;

    goto :goto_0

    .line 228
    :pswitch_2
    iget-object v0, p0, Lepson/print/EPImage;->loadImageFileName:Ljava/lang/String;

    :goto_0
    const/16 v1, 0x2f

    .line 239
    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    const/16 v2, 0x2e

    .line 240
    invoke-virtual {v0, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    if-ltz v2, :cond_1

    if-ge v2, v1, :cond_0

    goto :goto_1

    .line 245
    :cond_0
    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    :goto_1
    const-string v0, ""

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getFileType()I
    .locals 1

    .line 256
    iget v0, p0, Lepson/print/EPImage;->mFileType:I

    return v0
.end method

.method public getLoadImageFileName()Ljava/lang/String;
    .locals 1

    .line 206
    iget-object v0, p0, Lepson/print/EPImage;->loadImageFileName:Ljava/lang/String;

    return-object v0
.end method

.method public getOriginalFileName()Ljava/lang/String;
    .locals 1

    .line 272
    iget-object v0, p0, Lepson/print/EPImage;->mOrgFileNameForLogger:Ljava/lang/String;

    if-eqz v0, :cond_0

    return-object v0

    .line 275
    :cond_0
    iget-object v0, p0, Lepson/print/EPImage;->loadImageFileName:Ljava/lang/String;

    return-object v0
.end method

.method public isExtentionAvailable()Z
    .locals 2

    .line 216
    iget v0, p0, Lepson/print/EPImage;->mFileInfo:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0
.end method

.method public setOrgName(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x1

    .line 264
    iput v0, p0, Lepson/print/EPImage;->mFileInfo:I

    .line 265
    iput-object p1, p0, Lepson/print/EPImage;->mOrgFileNameForLogger:Ljava/lang/String;

    return-void
.end method

.method public setType(I)V
    .locals 1

    const/4 v0, 0x2

    .line 283
    iput v0, p0, Lepson/print/EPImage;->mFileInfo:I

    .line 284
    iput p1, p0, Lepson/print/EPImage;->mFileType:I

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .line 361
    iget p2, p0, Lepson/print/EPImage;->index:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 364
    iget-object p2, p0, Lepson/print/EPImage;->loadImageFileName:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 365
    iget p2, p0, Lepson/print/EPImage;->srcWidth:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 366
    iget p2, p0, Lepson/print/EPImage;->srcHeight:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 369
    iget-object p2, p0, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 370
    iget p2, p0, Lepson/print/EPImage;->decodeWidth:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 371
    iget p2, p0, Lepson/print/EPImage;->decodeHeight:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 374
    iget-object p2, p0, Lepson/print/EPImage;->thumbnailImageFileName:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 377
    iget-object p2, p0, Lepson/print/EPImage;->previewImageFileName:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 378
    iget p2, p0, Lepson/print/EPImage;->previewWidth:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 379
    iget p2, p0, Lepson/print/EPImage;->previewHeight:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 382
    iget-object p2, p0, Lepson/print/EPImage;->printImageFileName:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    const/4 p2, 0x1

    .line 384
    new-array v0, p2, [Z

    iget-boolean v1, p0, Lepson/print/EPImage;->isPaperLandScape:Z

    const/4 v2, 0x0

    aput-boolean v1, v0, v2

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    .line 385
    new-array p2, p2, [Z

    iget-boolean v0, p0, Lepson/print/EPImage;->isNotPhot:Z

    aput-boolean v0, p2, v2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    .line 387
    iget p2, p0, Lepson/print/EPImage;->rotate:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 388
    iget p2, p0, Lepson/print/EPImage;->previewPaperRectLeft:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 389
    iget p2, p0, Lepson/print/EPImage;->previewPaperRectTop:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 390
    iget p2, p0, Lepson/print/EPImage;->previewPaperRectRight:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 391
    iget p2, p0, Lepson/print/EPImage;->previewPaperRectBottom:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 393
    iget p2, p0, Lepson/print/EPImage;->previewImageRectCenterX:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 394
    iget p2, p0, Lepson/print/EPImage;->previewImageRectCenterY:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 395
    iget p2, p0, Lepson/print/EPImage;->previewImageRectLeft:F

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeFloat(F)V

    .line 396
    iget p2, p0, Lepson/print/EPImage;->previewImageRectTop:F

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeFloat(F)V

    .line 397
    iget p2, p0, Lepson/print/EPImage;->previewImageRectRight:F

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeFloat(F)V

    .line 398
    iget p2, p0, Lepson/print/EPImage;->previewImageRectBottom:F

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeFloat(F)V

    .line 399
    iget p2, p0, Lepson/print/EPImage;->scaleFactor:F

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeFloat(F)V

    .line 402
    iget-object p2, p0, Lepson/print/EPImage;->webUrl:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 405
    iget p2, p0, Lepson/print/EPImage;->mFileInfo:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 406
    iget p2, p0, Lepson/print/EPImage;->mFileType:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 407
    iget-object p2, p0, Lepson/print/EPImage;->mOrgFileNameForLogger:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method

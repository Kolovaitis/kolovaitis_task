.class public Lepson/print/ActivityDocsPrintPreview;
.super Lepson/print/ActivityIACommon;
.source "ActivityDocsPrintPreview.java"

# interfaces
.implements Lepson/print/CommonDefine;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/epson/mobilephone/common/ReviewInvitationDialog$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/ActivityDocsPrintPreview$CustomProDialog;,
        Lepson/print/ActivityDocsPrintPreview$PrintUploadTask;,
        Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewNext;,
        Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;,
        Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;
    }
.end annotation


# static fields
.field private static final COMFIRM_REMOTEPRINT:I = 0xd

.field private static final COMFIRM_REMOTEPRINT_PREVIEW:I = 0xc

.field private static final CONFIRM_GDOC_COV:I = 0x5

.field private static final CONVERT_AUTHENTICATE:I = 0x1

.field private static final DIALOG_KEY_STORE_DIALOG:Ljava/lang/String; = "store-dialog"

.field private static final DIALOG_PASS:I = 0x2

.field private static final EPS_ERR_NONE:I = 0x0

.field private static final ERR_FILENAME_OVER_256_CHARACTERS:I = -0x1

.field private static final GDOC_CONVERT_FAIL:I = 0x7

.field private static final GDOC_CONVERT_INTERNET_PROBLEM:I = 0xb

.field private static final GDOC_CONVERT_SIZEOVER_DOC:I = 0x8

.field private static final GDOC_CONVERT_SIZEOVER_PPT:I = 0xa

.field private static final GDOC_CONVERT_SIZEOVER_XLS:I = 0x9

.field private static final NOAVAILABLE_NETWORK:I = 0x6

.field private static final NOT_PRINTER_SELECTED:I = 0x4

.field public static final PARAMS_KEY_DOC_PRINT_LOG:Ljava/lang/String; = "print_log"

.field private static final PREFS_NAME:Ljava/lang/String; = "PrintSetting"

.field private static final PRINT_GDOC_COV:I = 0x8

.field private static final PRINT_LOCAL:I = 0x0

.field private static final PRINT_REMOTE:I = 0x1

.field private static final PRINT_RENDER_SERVER:I = 0x4

.field private static final PRINT_SHOWPREVIEW:I = 0x2

.field private static final REQUEST_CODE_LICENSE_CHECK:I = 0x3

.field private static final RESULT_RUNTIMEPERMMISSION:I = 0x2

.field private static final TAG:Ljava/lang/String; = "ActivityDocsPrintPreview"

.field private static final confirmDlgShowPref:Ljava/lang/String; = "ConfirmDlgShow"

.field private static final mConnect:Ljava/lang/Object;


# instance fields
.field private Relative_para:Landroid/widget/RelativeLayout$LayoutParams;

.field private aPaperSourceSetting:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lepson/print/screen/PaperSourceSetting;",
            ">;"
        }
    .end annotation
.end field

.field bAutoStartPrint:Z

.field private confirmDlgShow:Z

.field private disablePrintArea:Z

.field private docFileName:Ljava/lang/String;

.field private docFileName_org:Ljava/lang/String;

.field private isBackKeyOK:Z

.field isCreateJobDone:Z

.field isEnableNFCTouch:Z

.field private isRemotePrinterOld:Z

.field loadRemotePreviewTask:Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mCallback:Lepson/print/service/IEpsonServiceCallback;

.field private mColorMode:I

.field private mCurPrintSetting:Lepson/print/screen/PrintSetting;

.field private mCurPrinterEmailAdress:Ljava/lang/String;

.field private mCurPrinterName:Ljava/lang/String;

.field private mCurrentPageView:I

.field private mDisablePreviewMsg:Landroid/widget/TextView;

.field private mEndPage:I

.field private mEndPageView:I

.field private mEpsonConnection:Landroid/content/ServiceConnection;

.field private mEpsonService:Lepson/print/service/IEpsonService;

.field private mImageView:Landroid/widget/ImageView;

.field private mIsPortrait:Z

.field private mLayout:I

.field private mLayoutMulti:I

.field private mLn_zoomview:Landroid/widget/LinearLayout;

.field private mLocalPrintProgressParams:Lepson/print/screen/PrintProgress$ProgressParams;

.field private mMoveX:F

.field private mNext:Landroid/widget/Button;

.field private mPageNum:Landroid/widget/TextView;

.field private mPaperSize:Lepson/common/Info_paper;

.field private mPdfRender:Lepson/print/pdf/pdfRender;

.field private mPre:Landroid/widget/Button;

.field private volatile mPreviewPaperAreaLandscape:Z

.field private mPrint:Landroid/widget/Button;

.field private mPrintImageList:Lepson/print/EPImageList;

.field private mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;

.field private mPrinterName:Ljava/lang/String;

.field private mProgress:Landroid/view/View;

.field private mRemotePreviewImageList:Lepson/print/EPImageList;

.field private mReviewInvitationViewModel:Lcom/epson/mobilephone/common/ReviewInvitationViewModel;

.field private mRl_zoomview:Landroid/widget/RelativeLayout;

.field private mSizeInfo:Landroid/widget/TextView;

.field private mStartPage:I

.field private mStartPageView:I

.field private mStartX:F

.field final mUiHandler:Landroid/os/Handler;

.field private paperMissmath:Landroid/widget/ImageView;

.field private paperSize:I

.field paperSourceInfo:Lepson/print/screen/PaperSourceInfo;

.field private pdfpass:Ljava/lang/String;

.field private printMode:I

.field private printerId:Ljava/lang/String;

.field private remoteEnableShowPreview:Z

.field private remoteEnableShowWarning:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 2135
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lepson/print/ActivityDocsPrintPreview;->mConnect:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .line 103
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    const/4 v0, 0x0

    .line 158
    iput-boolean v0, p0, Lepson/print/ActivityDocsPrintPreview;->disablePrintArea:Z

    const/4 v1, 0x2

    .line 173
    iput v1, p0, Lepson/print/ActivityDocsPrintPreview;->printMode:I

    const/4 v1, 0x0

    .line 177
    iput-object v1, p0, Lepson/print/ActivityDocsPrintPreview;->mBitmap:Landroid/graphics/Bitmap;

    const/4 v2, 0x1

    .line 179
    iput-boolean v2, p0, Lepson/print/ActivityDocsPrintPreview;->mIsPortrait:Z

    .line 185
    new-instance v3, Lepson/print/EPImageList;

    invoke-direct {v3}, Lepson/print/EPImageList;-><init>()V

    iput-object v3, p0, Lepson/print/ActivityDocsPrintPreview;->mRemotePreviewImageList:Lepson/print/EPImageList;

    .line 200
    iput-boolean v0, p0, Lepson/print/ActivityDocsPrintPreview;->isRemotePrinterOld:Z

    .line 202
    iput-object v1, p0, Lepson/print/ActivityDocsPrintPreview;->mCurPrintSetting:Lepson/print/screen/PrintSetting;

    .line 211
    iput-object v1, p0, Lepson/print/ActivityDocsPrintPreview;->mPdfRender:Lepson/print/pdf/pdfRender;

    const-string v3, ""

    .line 213
    iput-object v3, p0, Lepson/print/ActivityDocsPrintPreview;->pdfpass:Ljava/lang/String;

    .line 218
    iput-boolean v0, p0, Lepson/print/ActivityDocsPrintPreview;->isCreateJobDone:Z

    .line 225
    iput-boolean v0, p0, Lepson/print/ActivityDocsPrintPreview;->isEnableNFCTouch:Z

    .line 231
    iput-boolean v2, p0, Lepson/print/ActivityDocsPrintPreview;->isBackKeyOK:Z

    .line 236
    iput-boolean v2, p0, Lepson/print/ActivityDocsPrintPreview;->confirmDlgShow:Z

    .line 241
    iput-boolean v2, p0, Lepson/print/ActivityDocsPrintPreview;->remoteEnableShowWarning:Z

    .line 242
    iput-boolean v2, p0, Lepson/print/ActivityDocsPrintPreview;->remoteEnableShowPreview:Z

    .line 268
    iput-object v1, p0, Lepson/print/ActivityDocsPrintPreview;->paperMissmath:Landroid/widget/ImageView;

    .line 279
    iput-object v1, p0, Lepson/print/ActivityDocsPrintPreview;->mEpsonService:Lepson/print/service/IEpsonService;

    .line 282
    iput-object v1, p0, Lepson/print/ActivityDocsPrintPreview;->loadRemotePreviewTask:Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;

    .line 285
    iput-object v1, p0, Lepson/print/ActivityDocsPrintPreview;->paperSourceInfo:Lepson/print/screen/PaperSourceInfo;

    .line 290
    iput-boolean v0, p0, Lepson/print/ActivityDocsPrintPreview;->bAutoStartPrint:Z

    .line 758
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lepson/print/ActivityDocsPrintPreview$5;

    invoke-direct {v1, p0}, Lepson/print/ActivityDocsPrintPreview$5;-><init>(Lepson/print/ActivityDocsPrintPreview;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mUiHandler:Landroid/os/Handler;

    .line 2966
    new-instance v0, Lepson/print/ActivityDocsPrintPreview$25;

    invoke-direct {v0, p0}, Lepson/print/ActivityDocsPrintPreview$25;-><init>(Lepson/print/ActivityDocsPrintPreview;)V

    iput-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mEpsonConnection:Landroid/content/ServiceConnection;

    .line 2994
    new-instance v0, Lepson/print/ActivityDocsPrintPreview$26;

    invoke-direct {v0, p0}, Lepson/print/ActivityDocsPrintPreview$26;-><init>(Lepson/print/ActivityDocsPrintPreview;)V

    iput-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mCallback:Lepson/print/service/IEpsonServiceCallback;

    return-void
.end method

.method static synthetic access$000(Lepson/print/ActivityDocsPrintPreview;)V
    .locals 0

    .line 103
    invoke-direct {p0}, Lepson/print/ActivityDocsPrintPreview;->showStoreDialog()V

    return-void
.end method

.method static synthetic access$100(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/Button;
    .locals 0

    .line 103
    iget-object p0, p0, Lepson/print/ActivityDocsPrintPreview;->mPre:Landroid/widget/Button;

    return-object p0
.end method

.method static synthetic access$1000(Lepson/print/ActivityDocsPrintPreview;)Ljava/lang/String;
    .locals 0

    .line 103
    iget-object p0, p0, Lepson/print/ActivityDocsPrintPreview;->docFileName:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1002(Lepson/print/ActivityDocsPrintPreview;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 103
    iput-object p1, p0, Lepson/print/ActivityDocsPrintPreview;->docFileName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1100(Lepson/print/ActivityDocsPrintPreview;)Ljava/lang/String;
    .locals 0

    .line 103
    iget-object p0, p0, Lepson/print/ActivityDocsPrintPreview;->docFileName_org:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1200(Lepson/print/ActivityDocsPrintPreview;)Z
    .locals 0

    .line 103
    iget-boolean p0, p0, Lepson/print/ActivityDocsPrintPreview;->remoteEnableShowPreview:Z

    return p0
.end method

.method static synthetic access$1300(Lepson/print/ActivityDocsPrintPreview;)Lepson/print/pdf/pdfRender;
    .locals 0

    .line 103
    iget-object p0, p0, Lepson/print/ActivityDocsPrintPreview;->mPdfRender:Lepson/print/pdf/pdfRender;

    return-object p0
.end method

.method static synthetic access$1400(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/TextView;
    .locals 0

    .line 103
    iget-object p0, p0, Lepson/print/ActivityDocsPrintPreview;->mDisablePreviewMsg:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$1500(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/TextView;
    .locals 0

    .line 103
    iget-object p0, p0, Lepson/print/ActivityDocsPrintPreview;->mPageNum:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$1600(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/ImageView;
    .locals 0

    .line 103
    iget-object p0, p0, Lepson/print/ActivityDocsPrintPreview;->mImageView:Landroid/widget/ImageView;

    return-object p0
.end method

.method static synthetic access$1700(Lepson/print/ActivityDocsPrintPreview;)Z
    .locals 0

    .line 103
    iget-boolean p0, p0, Lepson/print/ActivityDocsPrintPreview;->mIsPortrait:Z

    return p0
.end method

.method static synthetic access$1702(Lepson/print/ActivityDocsPrintPreview;Z)Z
    .locals 0

    .line 103
    iput-boolean p1, p0, Lepson/print/ActivityDocsPrintPreview;->mIsPortrait:Z

    return p1
.end method

.method static synthetic access$1800(Lepson/print/ActivityDocsPrintPreview;)Z
    .locals 0

    .line 103
    iget-boolean p0, p0, Lepson/print/ActivityDocsPrintPreview;->confirmDlgShow:Z

    return p0
.end method

.method static synthetic access$1802(Lepson/print/ActivityDocsPrintPreview;Z)Z
    .locals 0

    .line 103
    iput-boolean p1, p0, Lepson/print/ActivityDocsPrintPreview;->confirmDlgShow:Z

    return p1
.end method

.method static synthetic access$1900(Lepson/print/ActivityDocsPrintPreview;)V
    .locals 0

    .line 103
    invoke-direct {p0}, Lepson/print/ActivityDocsPrintPreview;->startConvertActivity()V

    return-void
.end method

.method static synthetic access$200(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/Button;
    .locals 0

    .line 103
    iget-object p0, p0, Lepson/print/ActivityDocsPrintPreview;->mNext:Landroid/widget/Button;

    return-object p0
.end method

.method static synthetic access$2000(Lepson/print/ActivityDocsPrintPreview;)V
    .locals 0

    .line 103
    invoke-direct {p0}, Lepson/print/ActivityDocsPrintPreview;->adjustPrintSettings()V

    return-void
.end method

.method static synthetic access$2102(Lepson/print/ActivityDocsPrintPreview;Z)Z
    .locals 0

    .line 103
    iput-boolean p1, p0, Lepson/print/ActivityDocsPrintPreview;->disablePrintArea:Z

    return p1
.end method

.method static synthetic access$2200(Lepson/print/ActivityDocsPrintPreview;)Z
    .locals 0

    .line 103
    iget-boolean p0, p0, Lepson/print/ActivityDocsPrintPreview;->remoteEnableShowWarning:Z

    return p0
.end method

.method static synthetic access$2300(Lepson/print/ActivityDocsPrintPreview;)Ljava/lang/String;
    .locals 0

    .line 103
    iget-object p0, p0, Lepson/print/ActivityDocsPrintPreview;->pdfpass:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$2302(Lepson/print/ActivityDocsPrintPreview;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 103
    iput-object p1, p0, Lepson/print/ActivityDocsPrintPreview;->pdfpass:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2500(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/Button;
    .locals 0

    .line 103
    iget-object p0, p0, Lepson/print/ActivityDocsPrintPreview;->mPrint:Landroid/widget/Button;

    return-object p0
.end method

.method static synthetic access$2600(Lepson/print/ActivityDocsPrintPreview;)Landroid/graphics/Bitmap;
    .locals 0

    .line 103
    iget-object p0, p0, Lepson/print/ActivityDocsPrintPreview;->mBitmap:Landroid/graphics/Bitmap;

    return-object p0
.end method

.method static synthetic access$2700(Lepson/print/ActivityDocsPrintPreview;)Z
    .locals 0

    .line 103
    invoke-direct {p0}, Lepson/print/ActivityDocsPrintPreview;->isRemotePrintSettingsChanged()Z

    move-result p0

    return p0
.end method

.method static synthetic access$2800(Lepson/print/ActivityDocsPrintPreview;)V
    .locals 0

    .line 103
    invoke-direct {p0}, Lepson/print/ActivityDocsPrintPreview;->loadConfig()V

    return-void
.end method

.method static synthetic access$2900(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/LinearLayout;
    .locals 0

    .line 103
    iget-object p0, p0, Lepson/print/ActivityDocsPrintPreview;->mLn_zoomview:Landroid/widget/LinearLayout;

    return-object p0
.end method

.method static synthetic access$300(Lepson/print/ActivityDocsPrintPreview;)I
    .locals 0

    .line 103
    iget p0, p0, Lepson/print/ActivityDocsPrintPreview;->printMode:I

    return p0
.end method

.method static synthetic access$3000(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/RelativeLayout;
    .locals 0

    .line 103
    iget-object p0, p0, Lepson/print/ActivityDocsPrintPreview;->mRl_zoomview:Landroid/widget/RelativeLayout;

    return-object p0
.end method

.method static synthetic access$302(Lepson/print/ActivityDocsPrintPreview;I)I
    .locals 0

    .line 103
    iput p1, p0, Lepson/print/ActivityDocsPrintPreview;->printMode:I

    return p1
.end method

.method static synthetic access$3202(Lepson/print/ActivityDocsPrintPreview;Z)Z
    .locals 0

    .line 103
    iput-boolean p1, p0, Lepson/print/ActivityDocsPrintPreview;->isBackKeyOK:Z

    return p1
.end method

.method static synthetic access$3300(Lepson/print/ActivityDocsPrintPreview;)Ljava/util/ArrayList;
    .locals 0

    .line 103
    iget-object p0, p0, Lepson/print/ActivityDocsPrintPreview;->aPaperSourceSetting:Ljava/util/ArrayList;

    return-object p0
.end method

.method static synthetic access$3302(Lepson/print/ActivityDocsPrintPreview;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .line 103
    iput-object p1, p0, Lepson/print/ActivityDocsPrintPreview;->aPaperSourceSetting:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic access$3400(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/TextView;
    .locals 0

    .line 103
    iget-object p0, p0, Lepson/print/ActivityDocsPrintPreview;->mSizeInfo:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$3500(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/ImageView;
    .locals 0

    .line 103
    iget-object p0, p0, Lepson/print/ActivityDocsPrintPreview;->paperMissmath:Landroid/widget/ImageView;

    return-object p0
.end method

.method static synthetic access$3700(Lepson/print/ActivityDocsPrintPreview;)Lcom/epson/iprint/prtlogger/PrintLog;
    .locals 0

    .line 103
    iget-object p0, p0, Lepson/print/ActivityDocsPrintPreview;->mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;

    return-object p0
.end method

.method static synthetic access$3702(Lepson/print/ActivityDocsPrintPreview;Lcom/epson/iprint/prtlogger/PrintLog;)Lcom/epson/iprint/prtlogger/PrintLog;
    .locals 0

    .line 103
    iput-object p1, p0, Lepson/print/ActivityDocsPrintPreview;->mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;

    return-object p1
.end method

.method static synthetic access$3802(Lepson/print/ActivityDocsPrintPreview;Lepson/print/screen/PrintProgress$ProgressParams;)Lepson/print/screen/PrintProgress$ProgressParams;
    .locals 0

    .line 103
    iput-object p1, p0, Lepson/print/ActivityDocsPrintPreview;->mLocalPrintProgressParams:Lepson/print/screen/PrintProgress$ProgressParams;

    return-object p1
.end method

.method static synthetic access$3900(Lepson/print/ActivityDocsPrintPreview;)Z
    .locals 0

    .line 103
    iget-boolean p0, p0, Lepson/print/ActivityDocsPrintPreview;->mPreviewPaperAreaLandscape:Z

    return p0
.end method

.method static synthetic access$400(Lepson/print/ActivityDocsPrintPreview;)F
    .locals 0

    .line 103
    iget p0, p0, Lepson/print/ActivityDocsPrintPreview;->mStartX:F

    return p0
.end method

.method static synthetic access$4000(Lepson/print/ActivityDocsPrintPreview;)I
    .locals 0

    .line 103
    iget p0, p0, Lepson/print/ActivityDocsPrintPreview;->mStartPage:I

    return p0
.end method

.method static synthetic access$402(Lepson/print/ActivityDocsPrintPreview;F)F
    .locals 0

    .line 103
    iput p1, p0, Lepson/print/ActivityDocsPrintPreview;->mStartX:F

    return p1
.end method

.method static synthetic access$4100(Lepson/print/ActivityDocsPrintPreview;)I
    .locals 0

    .line 103
    iget p0, p0, Lepson/print/ActivityDocsPrintPreview;->mEndPage:I

    return p0
.end method

.method static synthetic access$4202(Lepson/print/ActivityDocsPrintPreview;Lepson/print/EPImageList;)Lepson/print/EPImageList;
    .locals 0

    .line 103
    iput-object p1, p0, Lepson/print/ActivityDocsPrintPreview;->mPrintImageList:Lepson/print/EPImageList;

    return-object p1
.end method

.method static synthetic access$4300(Lepson/print/ActivityDocsPrintPreview;)Lepson/print/EPImageList;
    .locals 0

    .line 103
    iget-object p0, p0, Lepson/print/ActivityDocsPrintPreview;->mRemotePreviewImageList:Lepson/print/EPImageList;

    return-object p0
.end method

.method static synthetic access$4400()Ljava/lang/Object;
    .locals 1

    .line 103
    sget-object v0, Lepson/print/ActivityDocsPrintPreview;->mConnect:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$4500(Lepson/print/ActivityDocsPrintPreview;)Lepson/print/service/IEpsonServiceCallback;
    .locals 0

    .line 103
    iget-object p0, p0, Lepson/print/ActivityDocsPrintPreview;->mCallback:Lepson/print/service/IEpsonServiceCallback;

    return-object p0
.end method

.method static synthetic access$500(Lepson/print/ActivityDocsPrintPreview;)F
    .locals 0

    .line 103
    iget p0, p0, Lepson/print/ActivityDocsPrintPreview;->mMoveX:F

    return p0
.end method

.method static synthetic access$502(Lepson/print/ActivityDocsPrintPreview;F)F
    .locals 0

    .line 103
    iput p1, p0, Lepson/print/ActivityDocsPrintPreview;->mMoveX:F

    return p1
.end method

.method static synthetic access$600(Lepson/print/ActivityDocsPrintPreview;)I
    .locals 0

    .line 103
    iget p0, p0, Lepson/print/ActivityDocsPrintPreview;->mCurrentPageView:I

    return p0
.end method

.method static synthetic access$602(Lepson/print/ActivityDocsPrintPreview;I)I
    .locals 0

    .line 103
    iput p1, p0, Lepson/print/ActivityDocsPrintPreview;->mCurrentPageView:I

    return p1
.end method

.method static synthetic access$700(Lepson/print/ActivityDocsPrintPreview;)I
    .locals 0

    .line 103
    iget p0, p0, Lepson/print/ActivityDocsPrintPreview;->mEndPageView:I

    return p0
.end method

.method static synthetic access$702(Lepson/print/ActivityDocsPrintPreview;I)I
    .locals 0

    .line 103
    iput p1, p0, Lepson/print/ActivityDocsPrintPreview;->mEndPageView:I

    return p1
.end method

.method static synthetic access$800(Lepson/print/ActivityDocsPrintPreview;)Lepson/print/service/IEpsonService;
    .locals 0

    .line 103
    iget-object p0, p0, Lepson/print/ActivityDocsPrintPreview;->mEpsonService:Lepson/print/service/IEpsonService;

    return-object p0
.end method

.method static synthetic access$802(Lepson/print/ActivityDocsPrintPreview;Lepson/print/service/IEpsonService;)Lepson/print/service/IEpsonService;
    .locals 0

    .line 103
    iput-object p1, p0, Lepson/print/ActivityDocsPrintPreview;->mEpsonService:Lepson/print/service/IEpsonService;

    return-object p1
.end method

.method static synthetic access$902(Lepson/print/ActivityDocsPrintPreview;I)I
    .locals 0

    .line 103
    iput p1, p0, Lepson/print/ActivityDocsPrintPreview;->mStartPageView:I

    return p1
.end method

.method private adjustPrintSettings()V
    .locals 2

    .line 2546
    :try_start_0
    iget v0, p0, Lepson/print/ActivityDocsPrintPreview;->printMode:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 2547
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mEpsonService:Lepson/print/service/IEpsonService;

    sget-object v1, Lepson/print/screen/PrintSetting$Kind;->document:Lepson/print/screen/PrintSetting$Kind;

    invoke-virtual {v1}, Lepson/print/screen/PrintSetting$Kind;->name()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lepson/print/service/IEpsonService;->EpsonConnectUpdatePrinterSettings(Ljava/lang/String;)I

    goto :goto_0

    .line 2549
    :cond_0
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mEpsonService:Lepson/print/service/IEpsonService;

    sget-object v1, Lepson/print/screen/PrintSetting$Kind;->document:Lepson/print/screen/PrintSetting$Kind;

    invoke-virtual {v1}, Lepson/print/screen/PrintSetting$Kind;->name()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lepson/print/service/IEpsonService;->updatePrinterSettings(Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 2553
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    :goto_0
    return-void
.end method

.method private isRemotePrintSettingsChanged()Z
    .locals 12

    .line 2575
    invoke-static {p0}, Lepson/print/MyPrinter;->getCurPrinter(Landroid/content/Context;)Lepson/print/MyPrinter;

    move-result-object v0

    invoke-virtual {v0}, Lepson/print/MyPrinter;->getEmailAddress()Ljava/lang/String;

    move-result-object v0

    .line 2578
    new-instance v1, Lepson/print/screen/PrintSetting;

    sget-object v2, Lepson/print/screen/PrintSetting$Kind;->document:Lepson/print/screen/PrintSetting$Kind;

    invoke-direct {v1, p0, v2}, Lepson/print/screen/PrintSetting;-><init>(Landroid/content/Context;Lepson/print/screen/PrintSetting$Kind;)V

    .line 2579
    invoke-virtual {v1}, Lepson/print/screen/PrintSetting;->loadSettings()V

    .line 2580
    iget v2, v1, Lepson/print/screen/PrintSetting;->paperSizeValue:I

    .line 2581
    iget v3, v1, Lepson/print/screen/PrintSetting;->paperTypeValue:I

    .line 2582
    iget v4, v1, Lepson/print/screen/PrintSetting;->layoutValue:I

    .line 2583
    iget v5, v1, Lepson/print/screen/PrintSetting;->qualityValue:I

    .line 2584
    iget v6, v1, Lepson/print/screen/PrintSetting;->paperSourceValue:I

    .line 2585
    iget v7, v1, Lepson/print/screen/PrintSetting;->colorValue:I

    .line 2586
    iget v8, v1, Lepson/print/screen/PrintSetting;->duplexValue:I

    .line 2587
    iget v9, v1, Lepson/print/screen/PrintSetting;->brightnessValue:I

    .line 2588
    iget v10, v1, Lepson/print/screen/PrintSetting;->contrastValue:I

    .line 2589
    iget v1, v1, Lepson/print/screen/PrintSetting;->saturationValue:I

    .line 2592
    iget-object v11, p0, Lepson/print/ActivityDocsPrintPreview;->mCurPrinterEmailAdress:Ljava/lang/String;

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v11, 0x1

    if-nez v0, :cond_0

    return v11

    .line 2595
    :cond_0
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mCurPrintSetting:Lepson/print/screen/PrintSetting;

    iget v0, v0, Lepson/print/screen/PrintSetting;->paperSizeValue:I

    if-eq v0, v2, :cond_1

    return v11

    .line 2598
    :cond_1
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mCurPrintSetting:Lepson/print/screen/PrintSetting;

    iget v0, v0, Lepson/print/screen/PrintSetting;->paperTypeValue:I

    if-eq v0, v3, :cond_2

    return v11

    .line 2601
    :cond_2
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mCurPrintSetting:Lepson/print/screen/PrintSetting;

    iget v0, v0, Lepson/print/screen/PrintSetting;->layoutValue:I

    if-eq v0, v4, :cond_3

    return v11

    .line 2604
    :cond_3
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mCurPrintSetting:Lepson/print/screen/PrintSetting;

    iget v0, v0, Lepson/print/screen/PrintSetting;->qualityValue:I

    if-eq v0, v5, :cond_4

    return v11

    .line 2607
    :cond_4
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mCurPrintSetting:Lepson/print/screen/PrintSetting;

    iget v0, v0, Lepson/print/screen/PrintSetting;->paperSourceValue:I

    if-eq v0, v6, :cond_5

    return v11

    .line 2610
    :cond_5
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mCurPrintSetting:Lepson/print/screen/PrintSetting;

    iget v0, v0, Lepson/print/screen/PrintSetting;->colorValue:I

    if-eq v0, v7, :cond_6

    return v11

    .line 2613
    :cond_6
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mCurPrintSetting:Lepson/print/screen/PrintSetting;

    iget v0, v0, Lepson/print/screen/PrintSetting;->duplexValue:I

    if-eq v0, v8, :cond_7

    return v11

    .line 2616
    :cond_7
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mCurPrintSetting:Lepson/print/screen/PrintSetting;

    iget v0, v0, Lepson/print/screen/PrintSetting;->brightnessValue:I

    if-eq v0, v9, :cond_8

    return v11

    .line 2619
    :cond_8
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mCurPrintSetting:Lepson/print/screen/PrintSetting;

    iget v0, v0, Lepson/print/screen/PrintSetting;->contrastValue:I

    if-eq v0, v10, :cond_9

    return v11

    .line 2622
    :cond_9
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mCurPrintSetting:Lepson/print/screen/PrintSetting;

    iget v0, v0, Lepson/print/screen/PrintSetting;->saturationValue:I

    if-eq v0, v1, :cond_a

    return v11

    :cond_a
    const/4 v0, 0x0

    return v0
.end method

.method private loadConfig()V
    .locals 5

    const-string v0, "PrintSetting"

    const/4 v1, 0x0

    .line 2673
    invoke-virtual {p0, v0, v1}, Lepson/print/ActivityDocsPrintPreview;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 2676
    new-instance v1, Lepson/print/screen/PrintSetting;

    sget-object v2, Lepson/print/screen/PrintSetting$Kind;->document:Lepson/print/screen/PrintSetting$Kind;

    invoke-direct {v1, p0, v2}, Lepson/print/screen/PrintSetting;-><init>(Landroid/content/Context;Lepson/print/screen/PrintSetting$Kind;)V

    .line 2677
    invoke-virtual {v1}, Lepson/print/screen/PrintSetting;->loadSettings()V

    .line 2679
    iget v2, v1, Lepson/print/screen/PrintSetting;->paperSizeValue:I

    iput v2, p0, Lepson/print/ActivityDocsPrintPreview;->paperSize:I

    .line 2680
    iget v2, v1, Lepson/print/screen/PrintSetting;->colorValue:I

    iput v2, p0, Lepson/print/ActivityDocsPrintPreview;->mColorMode:I

    .line 2681
    iget v2, v1, Lepson/print/screen/PrintSetting;->layoutValue:I

    iput v2, p0, Lepson/print/ActivityDocsPrintPreview;->mLayout:I

    .line 2682
    iget v2, v1, Lepson/print/screen/PrintSetting;->layoutMultiPageValue:I

    iput v2, p0, Lepson/print/ActivityDocsPrintPreview;->mLayoutMulti:I

    .line 2684
    iget v2, p0, Lepson/print/ActivityDocsPrintPreview;->paperSize:I

    invoke-static {p0, v2}, Lepson/common/Info_paper;->getInfoPaper(Landroid/content/Context;I)Lepson/common/Info_paper;

    move-result-object v2

    iput-object v2, p0, Lepson/print/ActivityDocsPrintPreview;->mPaperSize:Lepson/common/Info_paper;

    .line 2686
    new-instance v2, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize;

    invoke-direct {v2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize;-><init>()V

    .line 2687
    iget-object v3, p0, Lepson/print/ActivityDocsPrintPreview;->mSizeInfo:Landroid/widget/TextView;

    iget v4, p0, Lepson/print/ActivityDocsPrintPreview;->paperSize:I

    invoke-virtual {v2, v4}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result v4

    invoke-virtual {p0, v4}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2688
    invoke-virtual {v2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    const-string v2, "PRINTER_NAME"

    const v3, 0x7f0e04d6

    .line 2691
    invoke-virtual {p0, v3}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2690
    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lepson/print/ActivityDocsPrintPreview;->mPrinterName:Ljava/lang/String;

    const-string v2, "PRINTER_ID"

    const/4 v3, 0x0

    .line 2692
    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->printerId:Ljava/lang/String;

    .line 2694
    iget v0, v1, Lepson/print/screen/PrintSetting;->startValue:I

    iput v0, p0, Lepson/print/ActivityDocsPrintPreview;->mStartPage:I

    .line 2695
    iget v0, v1, Lepson/print/screen/PrintSetting;->endValue:I

    iput v0, p0, Lepson/print/ActivityDocsPrintPreview;->mEndPage:I

    return-void
.end method

.method private nextPage()V
    .locals 5

    .line 1597
    iget v0, p0, Lepson/print/ActivityDocsPrintPreview;->printMode:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    .line 1600
    iget v0, p0, Lepson/print/ActivityDocsPrintPreview;->mCurrentPageView:I

    iget-object v3, p0, Lepson/print/ActivityDocsPrintPreview;->mRemotePreviewImageList:Lepson/print/EPImageList;

    invoke-virtual {v3}, Lepson/print/EPImageList;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 1602
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mRemotePreviewImageList:Lepson/print/EPImageList;

    iget v1, p0, Lepson/print/ActivityDocsPrintPreview;->mCurrentPageView:I

    invoke-virtual {v0, v1}, Lepson/print/EPImageList;->get(I)Lepson/print/EPImage;

    move-result-object v0

    .line 1603
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    const/16 v3, 0x1f

    .line 1604
    iput v3, v1, Landroid/os/Message;->what:I

    .line 1605
    iget-object v0, v0, Lepson/print/EPImage;->loadImageFileName:Ljava/lang/String;

    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1606
    invoke-virtual {v1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "CONVERTED_PAGE"

    iget v4, p0, Lepson/print/ActivityDocsPrintPreview;->mCurrentPageView:I

    add-int/2addr v4, v2

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1607
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 1610
    :cond_0
    new-instance v0, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewNext;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2}, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewNext;-><init>(Lepson/print/ActivityDocsPrintPreview;Lepson/print/ActivityDocsPrintPreview$1;)V

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewNext;->executeOnExecutor([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 1615
    :cond_1
    iget v0, p0, Lepson/print/ActivityDocsPrintPreview;->mCurrentPageView:I

    iget v3, p0, Lepson/print/ActivityDocsPrintPreview;->mEndPageView:I

    if-ge v0, v3, :cond_2

    if-ge v0, v3, :cond_2

    .line 1616
    iget-object v3, p0, Lepson/print/ActivityDocsPrintPreview;->mPdfRender:Lepson/print/pdf/pdfRender;

    add-int/2addr v0, v2

    invoke-virtual {v3, v0, v2, v1}, Lepson/print/pdf/pdfRender;->startConvertPage(III)V

    :cond_2
    :goto_0
    return-void
.end method

.method private onInitEnd()V
    .locals 8

    .line 521
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mEpsonService:Lepson/print/service/IEpsonService;

    const/4 v1, 0x1

    if-nez v0, :cond_0

    .line 522
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lepson/print/service/EpsonService;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v2, p0, Lepson/print/ActivityDocsPrintPreview;->mEpsonConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0, v2, v1}, Lepson/print/ActivityDocsPrintPreview;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 527
    :cond_0
    invoke-static {}, Lepson/print/ActivityRequestPermissions;->isRuntimePermissionSupported()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 529
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->docFileName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v2

    invoke-virtual {v2}, Lepson/common/ExternalFileUtils;->getWorkingDir()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "android.permission.WRITE_EXTERNAL_STORAGE"

    .line 530
    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x2

    .line 531
    new-array v3, v2, [Ljava/lang/String;

    const v4, 0x7f0e0422

    invoke-virtual {p0, v4}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    aput-object v5, v3, v6

    invoke-virtual {p0, v4}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    .line 532
    new-array v4, v2, [Ljava/lang/String;

    const v5, 0x7f0e0420

    .line 533
    invoke-virtual {p0, v5}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {p0, v7}, Lepson/print/ActivityRequestPermissions$DialogParameter;->setMessage2(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v6

    .line 534
    invoke-virtual {p0, v5}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v5

    const v7, 0x7f0e0424

    invoke-virtual {p0, v7}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {p0, v5, v7}, Lepson/print/ActivityRequestPermissions$DialogParameter;->setMessage3A(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    .line 540
    new-instance v1, Lepson/print/ActivityRequestPermissions$Permission;

    aget-object v5, v0, v6

    invoke-direct {v1, v5, v3, v4}, Lepson/print/ActivityRequestPermissions$Permission;-><init>(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V

    .line 542
    invoke-static {p0, v0}, Lepson/print/ActivityRequestPermissions;->checkPermission(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 543
    invoke-static {p0, v1, v2}, Lepson/print/ActivityRequestPermissions;->requestPermission(Landroid/app/Activity;Lepson/print/ActivityRequestPermissions$Permission;I)V

    return-void

    .line 550
    :cond_1
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mUiHandler:Landroid/os/Handler;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method private prevPage()V
    .locals 5

    .line 1573
    iget v0, p0, Lepson/print/ActivityDocsPrintPreview;->printMode:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 1576
    iget v0, p0, Lepson/print/ActivityDocsPrintPreview;->mCurrentPageView:I

    if-le v0, v1, :cond_1

    iget v2, p0, Lepson/print/ActivityDocsPrintPreview;->mStartPageView:I

    if-le v0, v2, :cond_1

    .line 1578
    iget-object v2, p0, Lepson/print/ActivityDocsPrintPreview;->mRemotePreviewImageList:Lepson/print/EPImageList;

    add-int/lit8 v0, v0, -0x2

    invoke-virtual {v2, v0}, Lepson/print/EPImageList;->get(I)Lepson/print/EPImage;

    move-result-object v0

    .line 1579
    new-instance v2, Landroid/os/Message;

    invoke-direct {v2}, Landroid/os/Message;-><init>()V

    const/16 v3, 0x1f

    .line 1580
    iput v3, v2, Landroid/os/Message;->what:I

    .line 1581
    iget-object v0, v0, Lepson/print/EPImage;->loadImageFileName:Ljava/lang/String;

    iput-object v0, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1582
    invoke-virtual {v2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "CONVERTED_PAGE"

    iget v4, p0, Lepson/print/ActivityDocsPrintPreview;->mCurrentPageView:I

    sub-int/2addr v4, v1

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1583
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 1590
    :cond_0
    iget v0, p0, Lepson/print/ActivityDocsPrintPreview;->mCurrentPageView:I

    if-le v0, v1, :cond_1

    iget v2, p0, Lepson/print/ActivityDocsPrintPreview;->mStartPageView:I

    if-le v0, v2, :cond_1

    .line 1591
    iget-object v2, p0, Lepson/print/ActivityDocsPrintPreview;->mPdfRender:Lepson/print/pdf/pdfRender;

    sub-int/2addr v0, v1

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v1, v3}, Lepson/print/pdf/pdfRender;->startConvertPage(III)V

    :cond_1
    :goto_0
    return-void
.end method

.method private saveCurSettings()V
    .locals 2

    .line 2562
    invoke-static {p0}, Lepson/print/MyPrinter;->getCurPrinter(Landroid/content/Context;)Lepson/print/MyPrinter;

    move-result-object v0

    invoke-virtual {v0}, Lepson/print/MyPrinter;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mCurPrinterName:Ljava/lang/String;

    .line 2563
    invoke-static {p0}, Lepson/print/MyPrinter;->getCurPrinter(Landroid/content/Context;)Lepson/print/MyPrinter;

    move-result-object v0

    invoke-virtual {v0}, Lepson/print/MyPrinter;->getEmailAddress()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mCurPrinterEmailAdress:Ljava/lang/String;

    .line 2564
    invoke-static {p0}, Lepson/print/MyPrinter;->isRemotePrinter(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lepson/print/ActivityDocsPrintPreview;->isRemotePrinterOld:Z

    .line 2565
    new-instance v0, Lepson/print/screen/PrintSetting;

    sget-object v1, Lepson/print/screen/PrintSetting$Kind;->document:Lepson/print/screen/PrintSetting$Kind;

    invoke-direct {v0, p0, v1}, Lepson/print/screen/PrintSetting;-><init>(Landroid/content/Context;Lepson/print/screen/PrintSetting$Kind;)V

    iput-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mCurPrintSetting:Lepson/print/screen/PrintSetting;

    .line 2566
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mCurPrintSetting:Lepson/print/screen/PrintSetting;

    invoke-virtual {v0}, Lepson/print/screen/PrintSetting;->loadSettings()V

    return-void
.end method

.method private setPrintLogForExternalApp(Ljava/lang/String;)V
    .locals 2

    .line 554
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;

    if-nez v0, :cond_0

    .line 555
    new-instance v0, Lcom/epson/iprint/prtlogger/PrintLog;

    invoke-direct {v0}, Lcom/epson/iprint/prtlogger/PrintLog;-><init>()V

    iput-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;

    .line 558
    :cond_0
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;

    if-nez v0, :cond_1

    .line 559
    new-instance v0, Lcom/epson/iprint/prtlogger/PrintLog;

    invoke-direct {v0}, Lcom/epson/iprint/prtlogger/PrintLog;-><init>()V

    iput-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;

    .line 562
    :cond_1
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;

    iget-object v0, v0, Lcom/epson/iprint/prtlogger/PrintLog;->callerPackage:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 563
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;

    invoke-virtual {p0}, Lepson/print/ActivityDocsPrintPreview;->getCallingPackage()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/epson/iprint/prtlogger/PrintLog;->callerPackage:Ljava/lang/String;

    .line 566
    :cond_2
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;

    iget v0, v0, Lcom/epson/iprint/prtlogger/PrintLog;->uiRoute:I

    if-gtz v0, :cond_3

    .line 569
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;

    const/16 v1, 0x1002

    iput v1, v0, Lcom/epson/iprint/prtlogger/PrintLog;->uiRoute:I

    .line 572
    :cond_3
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;

    iget-object v0, v0, Lcom/epson/iprint/prtlogger/PrintLog;->originalFileExtension:Ljava/lang/String;

    if-nez v0, :cond_4

    .line 573
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;

    invoke-static {p1}, Lcom/epson/iprint/prtlogger/PrintLog;->getFileExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, v0, Lcom/epson/iprint/prtlogger/PrintLog;->originalFileExtension:Ljava/lang/String;

    :cond_4
    return-void
.end method

.method private showStoreDialog()V
    .locals 3

    .line 508
    invoke-static {}, Lcom/epson/mobilephone/common/ReviewInvitationDialog;->newInstance()Lcom/epson/mobilephone/common/ReviewInvitationDialog;

    move-result-object v0

    .line 509
    invoke-virtual {p0}, Lepson/print/ActivityDocsPrintPreview;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "store-dialog"

    invoke-virtual {v0, v1, v2}, Lcom/epson/mobilephone/common/ReviewInvitationDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method private startConvertActivity()V
    .locals 2

    .line 1876
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->docFileName:Ljava/lang/String;

    invoke-static {p0, v0}, Lepson/print/gdconv/ConvertProgressActivity;->getStartIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x1

    .line 1877
    invoke-virtual {p0, v0, v1}, Lepson/print/ActivityDocsPrintPreview;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private startLicenseCheckActivity()V
    .locals 3

    .line 513
    invoke-virtual {p0}, Lepson/print/ActivityDocsPrintPreview;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lepson/common/IprintLicenseInfo;->beforeLicenseCheck(Landroid/content/Context;)V

    .line 514
    invoke-virtual {p0}, Lepson/print/ActivityDocsPrintPreview;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lepson/common/IprintLicenseInfo;

    invoke-direct {v1}, Lepson/common/IprintLicenseInfo;-><init>()V

    new-instance v2, Lepson/common/IprintUserSurveyInfo;

    invoke-direct {v2}, Lepson/common/IprintUserSurveyInfo;-><init>()V

    invoke-static {v0, v1, v2}, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->getStartIntent(Landroid/content/Context;Lcom/epson/mobilephone/common/license/LicenseInfo;Lcom/epson/mobilephone/common/license/UserSurveyInfo;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x3

    .line 516
    invoke-virtual {p0, v0, v1}, Lepson/print/ActivityDocsPrintPreview;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method


# virtual methods
.method public callPrintSetting()V
    .locals 3

    .line 1885
    invoke-virtual {p0}, Lepson/print/ActivityDocsPrintPreview;->cancelLoadRemotePreviewTask()V

    .line 1888
    invoke-direct {p0}, Lepson/print/ActivityDocsPrintPreview;->saveCurSettings()V

    const-string v0, "PrintSetting"

    const/4 v1, 0x0

    .line 1890
    invoke-virtual {p0, v0, v1}, Lepson/print/ActivityDocsPrintPreview;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1891
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "SOURCE_TYPE"

    const/4 v2, 0x1

    .line 1892
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1893
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1895
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lepson/print/screen/SettingScr;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "PRINT_DOCUMENT"

    .line 1896
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1898
    iget v1, p0, Lepson/print/ActivityDocsPrintPreview;->printMode:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_0

    const-string v1, "SHEETS"

    .line 1899
    iget v2, p0, Lepson/print/ActivityDocsPrintPreview;->mEndPageView:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "PRINTAREA"

    .line 1900
    iget-boolean v2, p0, Lepson/print/ActivityDocsPrintPreview;->disablePrintArea:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0

    :cond_0
    const-string v1, "SHEETS"

    .line 1902
    iget-object v2, p0, Lepson/print/ActivityDocsPrintPreview;->mPdfRender:Lepson/print/pdf/pdfRender;

    invoke-virtual {v2}, Lepson/print/pdf/pdfRender;->totalPages()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :goto_0
    const-string v1, "android.intent.action.VIEW"

    .line 1905
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const/4 v1, 0x6

    .line 1906
    invoke-virtual {p0, v0, v1}, Lepson/print/ActivityDocsPrintPreview;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method cancelLoadRemotePreviewTask()V
    .locals 3

    .line 2357
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->loadRemotePreviewTask:Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2358
    sget-object v0, Lepson/print/ActivityDocsPrintPreview;->mConnect:Ljava/lang/Object;

    monitor-enter v0

    .line 2359
    :try_start_0
    iget-object v1, p0, Lepson/print/ActivityDocsPrintPreview;->loadRemotePreviewTask:Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;->cancel(Z)Z

    .line 2360
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x0

    .line 2361
    iput-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->loadRemotePreviewTask:Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;

    goto :goto_0

    :catchall_0
    move-exception v1

    .line 2360
    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    :cond_0
    :goto_0
    return-void
.end method

.method check3GAndStartPrint()V
    .locals 0

    .line 2101
    invoke-virtual {p0}, Lepson/print/ActivityDocsPrintPreview;->startPrintActivity()V

    return-void
.end method

.method checkJobNameLength(Ljava/lang/String;)Z
    .locals 1

    .line 2373
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2374
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p1

    .line 2375
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    const/16 v0, 0x100

    if-le p1, v0, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    const/4 p1, 0x1

    return p1
.end method

.method public enableProgressBar(Z)V
    .locals 2

    const v0, 0x7f08034d

    if-eqz p1, :cond_0

    .line 2887
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview;->mProgress:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2891
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview;->mPre:Landroid/widget/Button;

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 2892
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview;->mNext:Landroid/widget/Button;

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 2894
    invoke-virtual {p0, v0}, Lepson/print/ActivityDocsPrintPreview;->findViewById(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    .line 2896
    iput-boolean v1, p0, Lepson/print/ActivityDocsPrintPreview;->isBackKeyOK:Z

    goto :goto_0

    .line 2898
    :cond_0
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview;->mProgress:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2902
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview;->mPre:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 2903
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview;->mNext:Landroid/widget/Button;

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 2905
    invoke-virtual {p0, v0}, Lepson/print/ActivityDocsPrintPreview;->findViewById(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    .line 2907
    iput-boolean v1, p0, Lepson/print/ActivityDocsPrintPreview;->isBackKeyOK:Z

    :goto_0
    return-void
.end method

.method getDocFileNameFromIntent(Landroid/content/Intent;)Ljava/lang/String;
    .locals 6

    const-string v0, "android.intent.action.VIEW"

    .line 587
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const-string v0, "ActivityDocsPrintPreview"

    const-string v2, "getDocFileNameFromIntent : Intent.ACTION_VIEW.equals(intent.getAction())"

    .line 589
    invoke-static {v0, v2}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 590
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    :cond_0
    const-string v0, "android.intent.action.SEND"

    .line 592
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "ActivityDocsPrintPreview"

    const-string v2, "getDocFileNameFromIntent : Intent.ACTION_SEND.equals(intent.getAction())"

    .line 594
    invoke-static {v0, v2}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "android.intent.extra.STREAM"

    .line 595
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    goto :goto_0

    :cond_1
    move-object v0, v1

    :goto_0
    if-eqz v0, :cond_5

    const-string v2, "ActivityDocsPrintPreview"

    .line 600
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getDocFileNameFromIntent : Call by Intent url ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 601
    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    const-string v3, "content"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "ActivityDocsPrintPreview"

    const-string v3, "getDocFileNameFromIntent : content"

    .line 602
    invoke-static {v2, v3}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 604
    invoke-virtual {p0}, Lepson/print/ActivityDocsPrintPreview;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 607
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_2

    const-string v3, "ActivityDocsPrintPreview"

    const-string v4, "getDocFileNameFromIntent : Not set Type"

    .line 612
    invoke-static {v3, v4}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 613
    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lepson/common/Utils;->getExtention(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 615
    invoke-static {v3}, Lepson/common/Utils;->getExtType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v3, "ActivityDocsPrintPreview"

    .line 616
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getDocFileNameFromIntent : fType = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 622
    :cond_2
    invoke-static {p1}, Lepson/common/Utils;->makeTempFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 624
    invoke-virtual {v2, v0}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    .line 625
    new-instance v2, Ljava/io/File;

    invoke-static {p0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v3

    invoke-virtual {v3}, Lepson/common/ExternalFileUtils;->getTempCRDir()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 626
    new-instance p1, Ljava/io/FileOutputStream;

    invoke-direct {p1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    const/16 v3, 0x400

    .line 627
    new-array v3, v3, [B

    .line 629
    :goto_1
    invoke-virtual {v0, v3}, Ljava/io/InputStream;->read([B)I

    move-result v4

    if-lez v4, :cond_3

    const/4 v5, 0x0

    .line 630
    invoke-virtual {p1, v3, v5, v4}, Ljava/io/FileOutputStream;->write([BII)V

    goto :goto_1

    .line 632
    :cond_3
    invoke-virtual {p1}, Ljava/io/FileOutputStream;->close()V

    .line 633
    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, p1

    goto :goto_3

    :catch_0
    move-exception p1

    goto :goto_2

    :catch_1
    move-exception p1

    :goto_2
    const-string v0, "ActivityDocsPrintPreview"

    .line 635
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getDocFileNameFromIntent : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 639
    invoke-virtual {p0}, Lepson/print/ActivityDocsPrintPreview;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const v0, 0x7f0e0373

    invoke-virtual {p0, v0}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    invoke-static {p1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    .line 640
    invoke-virtual {p0}, Lepson/print/ActivityDocsPrintPreview;->finish()V

    goto :goto_3

    .line 642
    :cond_4
    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object p1

    const-string v2, "file"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_5

    .line 643
    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    const-string p1, "/file:"

    .line 647
    invoke-virtual {v1, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_5

    const/4 p1, 0x6

    .line 648
    invoke-virtual {v1, p1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    :cond_5
    :goto_3
    return-object v1
.end method

.method public invitationDialogClicked(Z)V
    .locals 0

    .line 2131
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview;->mReviewInvitationViewModel:Lcom/epson/mobilephone/common/ReviewInvitationViewModel;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/ReviewInvitationViewModel;->setStartStoreEnd()V

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .line 1341
    invoke-super {p0, p1, p2, p3}, Lepson/print/ActivityIACommon;->onActivityResult(IILandroid/content/Intent;)V

    const/16 v0, 0xff

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eq p1, v0, :cond_4

    const/4 v0, -0x1

    const/16 v3, 0xa

    packed-switch p1, :pswitch_data_0

    packed-switch p1, :pswitch_data_1

    goto/16 :goto_1

    :pswitch_0
    if-ne p2, v0, :cond_6

    .line 1441
    iget-boolean p1, p0, Lepson/print/ActivityDocsPrintPreview;->isRemotePrinterOld:Z

    invoke-static {p0}, Lepson/print/MyPrinter;->isRemotePrinter(Landroid/content/Context;)Z

    move-result p2

    if-eq p1, p2, :cond_0

    const p1, 0x7f0e03f0

    .line 1447
    invoke-static {p0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    const-string p1, "printer"

    .line 1448
    invoke-static {p0}, Lepson/print/MyPrinter;->getCurPrinter(Landroid/content/Context;)Lepson/print/MyPrinter;

    move-result-object p2

    invoke-virtual {p2}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object p2

    invoke-static {p0, p1, p2}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->disconnect(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    .line 1451
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {p1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_1

    .line 1456
    :cond_0
    new-instance p1, Lepson/print/ActivityDocsPrintPreview$7;

    invoke-direct {p1, p0}, Lepson/print/ActivityDocsPrintPreview$7;-><init>(Lepson/print/ActivityDocsPrintPreview;)V

    new-array p2, v2, [Ljava/lang/Void;

    .line 1484
    invoke-virtual {p1, p2}, Lepson/print/ActivityDocsPrintPreview$7;->executeOnExecutor([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_1

    :pswitch_1
    if-eq p2, v0, :cond_1

    .line 1516
    invoke-virtual {p0}, Lepson/print/ActivityDocsPrintPreview;->finish()V

    return-void

    .line 1519
    :cond_1
    invoke-direct {p0}, Lepson/print/ActivityDocsPrintPreview;->onInitEnd()V

    goto/16 :goto_1

    :pswitch_2
    if-eq p2, v0, :cond_2

    .line 1509
    invoke-virtual {p0}, Lepson/print/ActivityDocsPrintPreview;->onBackPressed()V

    goto/16 :goto_1

    .line 1506
    :cond_2
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {p1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_1

    :pswitch_3
    const/4 p1, 0x7

    packed-switch p2, :pswitch_data_2

    :pswitch_4
    const-string p3, "ActivityDocsPrintPreview"

    const-string v0, "onActivityResult : ELSE"

    .line 1416
    invoke-static {p3, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1417
    iget-object p3, p0, Lepson/print/ActivityDocsPrintPreview;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {p3, p1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :pswitch_5
    const-string p1, "ActivityDocsPrintPreview"

    const-string p3, "onActivityResult : SIZE_ERROR"

    .line 1402
    invoke-static {p1, p3}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1404
    invoke-virtual {p0, v3}, Lepson/print/ActivityDocsPrintPreview;->showDialog(I)V

    goto :goto_0

    :pswitch_6
    const-string p1, "ActivityDocsPrintPreview"

    const-string p3, "onActivityResult : SIZE_ERROR"

    .line 1394
    invoke-static {p1, p3}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/16 p1, 0x9

    .line 1397
    invoke-virtual {p0, p1}, Lepson/print/ActivityDocsPrintPreview;->showDialog(I)V

    goto :goto_0

    :pswitch_7
    const-string p1, "ActivityDocsPrintPreview"

    const-string p3, "onActivityResult : SIZE_ERROR"

    .line 1387
    invoke-static {p1, p3}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/16 p1, 0x8

    .line 1390
    invoke-virtual {p0, p1}, Lepson/print/ActivityDocsPrintPreview;->showDialog(I)V

    goto :goto_0

    :pswitch_8
    const-string p1, "ActivityDocsPrintPreview"

    const-string p3, "onActivityResult : SIZE_ERROR"

    .line 1409
    invoke-static {p1, p3}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/16 p1, 0xb

    .line 1411
    invoke-virtual {p0, p1}, Lepson/print/ActivityDocsPrintPreview;->showDialog(I)V

    goto :goto_0

    :pswitch_9
    const-string p3, "ActivityDocsPrintPreview"

    const-string v0, "onActivityResult : CONVERT_FAIL"

    .line 1380
    invoke-static {p3, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1383
    invoke-virtual {p0, p1}, Lepson/print/ActivityDocsPrintPreview;->showDialog(I)V

    goto :goto_0

    :pswitch_a
    const-string p1, "ActivityDocsPrintPreview"

    const-string v0, "onActivityResult : CONVERT_OK"

    .line 1348
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-string p1, "downloadPath"

    .line 1349
    invoke-virtual {p3, p1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/print/ActivityDocsPrintPreview;->docFileName:Ljava/lang/String;

    .line 1352
    new-instance p1, Lepson/print/ActivityDocsPrintPreview$6;

    invoke-direct {p1, p0}, Lepson/print/ActivityDocsPrintPreview$6;-><init>(Lepson/print/ActivityDocsPrintPreview;)V

    new-array p3, v2, [Ljava/lang/Void;

    .line 1375
    invoke-virtual {p1, p3}, Lepson/print/ActivityDocsPrintPreview$6;->executeOnExecutor([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :goto_0
    :pswitch_b
    const/4 p1, 0x3

    if-ne p2, p1, :cond_6

    .line 1426
    iget-boolean p1, p0, Lepson/print/ActivityDocsPrintPreview;->isRemotePrinterOld:Z

    invoke-static {p0}, Lepson/print/MyPrinter;->isRemotePrinter(Landroid/content/Context;)Z

    move-result p2

    if-eq p1, p2, :cond_3

    .line 1428
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {p1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    .line 1432
    :cond_3
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview;->mUiHandler:Landroid/os/Handler;

    const/16 p2, 0x21

    invoke-virtual {p1, p2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    .line 1492
    :cond_4
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview;->mPdfRender:Lepson/print/pdf/pdfRender;

    if-eqz p1, :cond_5

    .line 1493
    invoke-virtual {p1, v2}, Lepson/print/pdf/pdfRender;->setPrintingStt(Z)V

    .line 1497
    :cond_5
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview;->mPrint:Landroid/widget/Button;

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1498
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview;->mReviewInvitationViewModel:Lcom/epson/mobilephone/common/ReviewInvitationViewModel;

    invoke-static {p2}, Lepson/print/screen/PrintProgress;->isPrintSuccess(I)Z

    move-result p2

    invoke-virtual {p1, p2}, Lcom/epson/mobilephone/common/ReviewInvitationViewModel;->onPrintEnd(Z)V

    :cond_6
    :goto_1
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x5
        :pswitch_0
        :pswitch_b
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x64
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_4
        :pswitch_7
        :pswitch_6
        :pswitch_5
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 2

    .line 685
    iget-boolean v0, p0, Lepson/print/ActivityDocsPrintPreview;->isBackKeyOK:Z

    if-eqz v0, :cond_0

    .line 689
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mUiHandler:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .line 1527
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    .line 1554
    :sswitch_0
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview;->aPaperSourceSetting:Ljava/util/ArrayList;

    if-eqz p1, :cond_0

    .line 1556
    new-instance p1, Landroid/content/Intent;

    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    .line 1557
    const-class v0, Lepson/print/screen/PaperSourceSettingScr;

    invoke-virtual {p1, p0, v0}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v0, "print-setting-type"

    .line 1559
    sget-object v1, Lepson/print/screen/PrintSetting$Kind;->document:Lepson/print/screen/PrintSetting$Kind;

    invoke-virtual {v1}, Lepson/print/screen/PrintSetting$Kind;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "PAPERSOURCEINFO"

    .line 1560
    iget-object v1, p0, Lepson/print/ActivityDocsPrintPreview;->aPaperSourceSetting:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    const/4 v0, 0x6

    .line 1561
    invoke-virtual {p0, p1, v0}, Lepson/print/ActivityDocsPrintPreview;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 1566
    :cond_0
    invoke-virtual {p0}, Lepson/print/ActivityDocsPrintPreview;->callPrintSetting()V

    goto :goto_0

    .line 1538
    :sswitch_1
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview;->mPrinterName:Ljava/lang/String;

    const v0, 0x7f0e04d6

    invoke-virtual {p0, v0}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 p1, 0x4

    .line 1540
    invoke-virtual {p0, p1}, Lepson/print/ActivityDocsPrintPreview;->showDialog(I)V

    goto :goto_0

    .line 1542
    :cond_1
    iget p1, p0, Lepson/print/ActivityDocsPrintPreview;->printMode:I

    and-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_2

    iget-boolean p1, p0, Lepson/print/ActivityDocsPrintPreview;->remoteEnableShowWarning:Z

    if-eqz p1, :cond_2

    const/16 p1, 0xd

    .line 1544
    invoke-virtual {p0, p1}, Lepson/print/ActivityDocsPrintPreview;->showDialog(I)V

    goto :goto_0

    .line 1547
    :cond_2
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview;->mUiHandler:Landroid/os/Handler;

    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 1529
    :sswitch_2
    invoke-direct {p0}, Lepson/print/ActivityDocsPrintPreview;->prevPage()V

    goto :goto_0

    .line 1533
    :sswitch_3
    invoke-direct {p0}, Lepson/print/ActivityDocsPrintPreview;->nextPage()V

    :goto_0
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f080099 -> :sswitch_3
        0x7f08009c -> :sswitch_2
        0x7f08009d -> :sswitch_1
        0x7f080371 -> :sswitch_0
    .end sparse-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .line 3026
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 3027
    iget-boolean p1, p0, Lepson/print/ActivityDocsPrintPreview;->mIsPortrait:Z

    invoke-virtual {p0, p1}, Lepson/print/ActivityDocsPrintPreview;->setOrentationView(Z)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .line 303
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    .line 305
    invoke-static {p0}, Lcom/epson/iprint/prtlogger/NewLoggerController;->stopLoggerIfNotAgreed(Landroid/content/Context;)V

    .line 307
    invoke-static {p0}, Landroid/arch/lifecycle/ViewModelProviders;->of(Landroid/support/v4/app/FragmentActivity;)Landroid/arch/lifecycle/ViewModelProvider;

    move-result-object v0

    const-class v1, Lcom/epson/mobilephone/common/ReviewInvitationViewModel;

    invoke-virtual {v0, v1}, Landroid/arch/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object v0

    check-cast v0, Lcom/epson/mobilephone/common/ReviewInvitationViewModel;

    iput-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mReviewInvitationViewModel:Lcom/epson/mobilephone/common/ReviewInvitationViewModel;

    .line 308
    invoke-static {p0}, Landroid/arch/lifecycle/ViewModelProviders;->of(Landroid/support/v4/app/FragmentActivity;)Landroid/arch/lifecycle/ViewModelProvider;

    move-result-object v0

    const-class v1, Lcom/epson/mobilephone/common/ReviewInvitationViewModel;

    invoke-virtual {v0, v1}, Landroid/arch/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object v0

    check-cast v0, Lcom/epson/mobilephone/common/ReviewInvitationViewModel;

    iput-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mReviewInvitationViewModel:Lcom/epson/mobilephone/common/ReviewInvitationViewModel;

    .line 309
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mReviewInvitationViewModel:Lcom/epson/mobilephone/common/ReviewInvitationViewModel;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/ReviewInvitationViewModel;->getShowInvitationLiveData()Landroid/arch/lifecycle/MutableLiveData;

    move-result-object v0

    new-instance v1, Lepson/print/ActivityDocsPrintPreview$1;

    invoke-direct {v1, p0}, Lepson/print/ActivityDocsPrintPreview$1;-><init>(Lepson/print/ActivityDocsPrintPreview;)V

    invoke-virtual {v0, p0, v1}, Landroid/arch/lifecycle/MutableLiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    .line 318
    invoke-virtual {p0}, Lepson/print/ActivityDocsPrintPreview;->getIntent()Landroid/content/Intent;

    move-result-object v0

    :try_start_0
    const-string v1, "print_log"

    .line 321
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/epson/iprint/prtlogger/PrintLog;

    iput-object v1, p0, Lepson/print/ActivityDocsPrintPreview;->mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 330
    :catch_0
    invoke-static {}, Lepson/print/Util/Utils;->getInstance()Lepson/print/Util/Utils;

    .line 331
    invoke-static {p0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v1

    invoke-virtual {v1}, Lepson/common/ExternalFileUtils;->initTempViewDir()V

    .line 332
    invoke-static {p0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v1

    invoke-virtual {v1}, Lepson/common/ExternalFileUtils;->initTempCRDir()V

    .line 333
    invoke-static {p0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v1

    invoke-virtual {v1}, Lepson/common/ExternalFileUtils;->initPrintDir()V

    .line 335
    invoke-virtual {p0}, Lepson/print/ActivityDocsPrintPreview;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v1

    sget-object v2, Lepson/common/ExternalFileUtils$TempCacheDirectory;->GOOGLE_DRIVE_CONVERT:Lepson/common/ExternalFileUtils$TempCacheDirectory;

    .line 336
    invoke-virtual {v1, v2}, Lepson/common/ExternalFileUtils;->initTempCacheDirectory(Lepson/common/ExternalFileUtils$TempCacheDirectory;)Ljava/io/File;

    const v1, 0x7f0a005b

    .line 339
    invoke-virtual {p0, v1}, Lepson/print/ActivityDocsPrintPreview;->setContentView(I)V

    const v1, 0x7f0e031c

    const/4 v2, 0x1

    .line 342
    invoke-virtual {p0, v1, v2}, Lepson/print/ActivityDocsPrintPreview;->setActionBar(IZ)V

    const v3, 0x7f080290

    .line 344
    invoke-virtual {p0, v3}, Lepson/print/ActivityDocsPrintPreview;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lepson/print/ActivityDocsPrintPreview;->mProgress:Landroid/view/View;

    const v3, 0x7f080370

    .line 345
    invoke-virtual {p0, v3}, Lepson/print/ActivityDocsPrintPreview;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lepson/print/ActivityDocsPrintPreview;->mPageNum:Landroid/widget/TextView;

    const v3, 0x7f080371

    .line 346
    invoke-virtual {p0, v3}, Lepson/print/ActivityDocsPrintPreview;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lepson/print/ActivityDocsPrintPreview;->mSizeInfo:Landroid/widget/TextView;

    const v3, 0x7f08009c

    .line 347
    invoke-virtual {p0, v3}, Lepson/print/ActivityDocsPrintPreview;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lepson/print/ActivityDocsPrintPreview;->mPre:Landroid/widget/Button;

    const v3, 0x7f080099

    .line 348
    invoke-virtual {p0, v3}, Lepson/print/ActivityDocsPrintPreview;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lepson/print/ActivityDocsPrintPreview;->mNext:Landroid/widget/Button;

    const v3, 0x7f08009d

    .line 349
    invoke-virtual {p0, v3}, Lepson/print/ActivityDocsPrintPreview;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lepson/print/ActivityDocsPrintPreview;->mPrint:Landroid/widget/Button;

    const v3, 0x7f0801f7

    .line 350
    invoke-virtual {p0, v3}, Lepson/print/ActivityDocsPrintPreview;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lepson/print/ActivityDocsPrintPreview;->mLn_zoomview:Landroid/widget/LinearLayout;

    const v3, 0x7f0802bb

    .line 351
    invoke-virtual {p0, v3}, Lepson/print/ActivityDocsPrintPreview;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lepson/print/ActivityDocsPrintPreview;->mRl_zoomview:Landroid/widget/RelativeLayout;

    const v3, 0x7f080399

    .line 352
    invoke-virtual {p0, v3}, Lepson/print/ActivityDocsPrintPreview;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lepson/print/ActivityDocsPrintPreview;->mImageView:Landroid/widget/ImageView;

    const v3, 0x7f08032d

    .line 353
    invoke-virtual {p0, v3}, Lepson/print/ActivityDocsPrintPreview;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lepson/print/ActivityDocsPrintPreview;->mDisablePreviewMsg:Landroid/widget/TextView;

    const v3, 0x7f08018d

    .line 354
    invoke-virtual {p0, v3}, Lepson/print/ActivityDocsPrintPreview;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lepson/print/ActivityDocsPrintPreview;->paperMissmath:Landroid/widget/ImageView;

    .line 357
    iget-object v3, p0, Lepson/print/ActivityDocsPrintPreview;->mSizeInfo:Landroid/widget/TextView;

    invoke-virtual {v3, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 358
    iget-object v3, p0, Lepson/print/ActivityDocsPrintPreview;->mPrint:Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 361
    iget-object v3, p0, Lepson/print/ActivityDocsPrintPreview;->mPre:Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 362
    iget-object v3, p0, Lepson/print/ActivityDocsPrintPreview;->mPre:Landroid/widget/Button;

    new-instance v4, Lepson/print/ActivityDocsPrintPreview$2;

    invoke-direct {v4, p0}, Lepson/print/ActivityDocsPrintPreview$2;-><init>(Lepson/print/ActivityDocsPrintPreview;)V

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 379
    iget-object v3, p0, Lepson/print/ActivityDocsPrintPreview;->mNext:Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 380
    iget-object v3, p0, Lepson/print/ActivityDocsPrintPreview;->mNext:Landroid/widget/Button;

    new-instance v4, Lepson/print/ActivityDocsPrintPreview$3;

    invoke-direct {v4, p0}, Lepson/print/ActivityDocsPrintPreview$3;-><init>(Lepson/print/ActivityDocsPrintPreview;)V

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 398
    iget-object v3, p0, Lepson/print/ActivityDocsPrintPreview;->mImageView:Landroid/widget/ImageView;

    new-instance v4, Lepson/print/ActivityDocsPrintPreview$4;

    invoke-direct {v4, p0}, Lepson/print/ActivityDocsPrintPreview$4;-><init>(Lepson/print/ActivityDocsPrintPreview;)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 449
    new-instance v3, Lepson/print/pdf/pdfRender;

    iget-object v4, p0, Lepson/print/ActivityDocsPrintPreview;->mUiHandler:Landroid/os/Handler;

    invoke-direct {v3, p0, v4}, Lepson/print/pdf/pdfRender;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v3, p0, Lepson/print/ActivityDocsPrintPreview;->mPdfRender:Lepson/print/pdf/pdfRender;

    const-string v3, "android.intent.action.VIEW"

    .line 452
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "android.intent.action.SEND"

    .line 453
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_0
    const-string v3, "send document"

    .line 471
    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lepson/print/ActivityDocsPrintPreview;->docFileName:Ljava/lang/String;

    goto :goto_1

    .line 455
    :cond_1
    :goto_0
    invoke-virtual {p0, v0}, Lepson/print/ActivityDocsPrintPreview;->getDocFileNameFromIntent(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lepson/print/ActivityDocsPrintPreview;->docFileName:Ljava/lang/String;

    .line 458
    iget-object v3, p0, Lepson/print/ActivityDocsPrintPreview;->docFileName:Ljava/lang/String;

    if-eqz v3, :cond_6

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_2

    goto :goto_4

    .line 464
    :cond_2
    iget-object v3, p0, Lepson/print/ActivityDocsPrintPreview;->docFileName:Ljava/lang/String;

    invoke-direct {p0, v3}, Lepson/print/ActivityDocsPrintPreview;->setPrintLogForExternalApp(Ljava/lang/String;)V

    .line 475
    :goto_1
    iget-object v3, p0, Lepson/print/ActivityDocsPrintPreview;->docFileName:Ljava/lang/String;

    iput-object v3, p0, Lepson/print/ActivityDocsPrintPreview;->docFileName_org:Ljava/lang/String;

    const-string v3, "from"

    const/4 v4, 0x3

    .line 478
    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    const/4 v3, 0x4

    if-ne v0, v3, :cond_3

    const v0, 0x7f0e03d9

    .line 479
    invoke-virtual {p0, v0}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lepson/print/ActivityDocsPrintPreview;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 481
    :cond_3
    invoke-virtual {p0, v1}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lepson/print/ActivityDocsPrintPreview;->setTitle(Ljava/lang/CharSequence;)V

    .line 485
    :goto_2
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "ConfirmDlgShow"

    .line 486
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lepson/print/ActivityDocsPrintPreview;->confirmDlgShow:Z

    const-string v0, "PREFS_EPSON_CONNECT"

    const/4 v1, 0x0

    .line 489
    invoke-virtual {p0, v0, v1}, Lepson/print/ActivityDocsPrintPreview;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "ENABLE_SHOW_PREVIEW"

    .line 490
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lepson/print/ActivityDocsPrintPreview;->remoteEnableShowPreview:Z

    const-string v1, "ENABLE_SHOW_WARNING"

    .line 491
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lepson/print/ActivityDocsPrintPreview;->remoteEnableShowWarning:Z

    .line 494
    invoke-static {p0}, Lepson/print/screen/PaperSourceInfo;->getInstance(Landroid/content/Context;)Lepson/print/screen/PaperSourceInfo;

    move-result-object v0

    iput-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->paperSourceInfo:Lepson/print/screen/PaperSourceInfo;

    if-nez p1, :cond_4

    .line 497
    invoke-direct {p0}, Lepson/print/ActivityDocsPrintPreview;->startLicenseCheckActivity()V

    goto :goto_3

    .line 499
    :cond_4
    invoke-virtual {p0}, Lepson/print/ActivityDocsPrintPreview;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lepson/common/IprintLicenseInfo;->isAgreedCurrentVersion(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_5

    .line 500
    invoke-virtual {p0}, Lepson/print/ActivityDocsPrintPreview;->finish()V

    return-void

    .line 503
    :cond_5
    invoke-direct {p0}, Lepson/print/ActivityDocsPrintPreview;->onInitEnd()V

    :goto_3
    return-void

    :cond_6
    :goto_4
    const-string p1, "ActivityDocsPrintPreview"

    const-string v0, "onCreate : Error ACTION_VIEW or ACTION_SEND"

    .line 459
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    const p1, 0x7f0e0373

    .line 460
    invoke-virtual {p0, p1}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    .line 461
    invoke-virtual {p0}, Lepson/print/ActivityDocsPrintPreview;->finish()V

    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 13

    .line 1623
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    const/4 v1, 0x2

    const v2, 0x7f0e04f2

    const/4 v3, 0x0

    if-eq p1, v1, :cond_0

    const v4, 0x7f0e052b

    const v5, 0x7f0e0363

    const v6, 0x7f0e0362

    const v7, 0x7f0e036b

    const/4 v8, 0x1

    const v9, 0x7f0e0369

    const v10, 0x7f0e036a

    const v11, 0x7f0e04e6

    const/4 v12, -0x1

    packed-switch p1, :pswitch_data_0

    return-object v0

    .line 1847
    :pswitch_0
    new-instance p1, Landroid/app/AlertDialog$Builder;

    invoke-direct {p1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1848
    invoke-virtual {p1, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1849
    invoke-virtual {p0, v6}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1850
    invoke-virtual {p0, v5}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1849
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 1851
    invoke-virtual {p0, v4}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lepson/print/ActivityDocsPrintPreview$23;

    invoke-direct {v1, p0}, Lepson/print/ActivityDocsPrintPreview$23;-><init>(Lepson/print/ActivityDocsPrintPreview;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 1858
    invoke-virtual {p0, v11}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lepson/print/ActivityDocsPrintPreview$22;

    invoke-direct {v1, p0}, Lepson/print/ActivityDocsPrintPreview$22;-><init>(Lepson/print/ActivityDocsPrintPreview;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 1864
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1

    .line 1825
    :pswitch_1
    new-instance p1, Landroid/app/AlertDialog$Builder;

    invoke-direct {p1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1826
    invoke-virtual {p1, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1827
    invoke-virtual {p0, v6}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1828
    invoke-virtual {p0, v5}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1827
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 1829
    invoke-virtual {p0, v4}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lepson/print/ActivityDocsPrintPreview$21;

    invoke-direct {v1, p0}, Lepson/print/ActivityDocsPrintPreview$21;-><init>(Lepson/print/ActivityDocsPrintPreview;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 1836
    invoke-virtual {p0, v11}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lepson/print/ActivityDocsPrintPreview$20;

    invoke-direct {v1, p0}, Lepson/print/ActivityDocsPrintPreview$20;-><init>(Lepson/print/ActivityDocsPrintPreview;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 1842
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1

    .line 1771
    :pswitch_2
    new-instance p1, Landroid/app/AlertDialog$Builder;

    invoke-direct {p1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 1773
    invoke-virtual {p0, v7}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    const v0, 0x7f0e03d8

    .line 1774
    invoke-virtual {p0, v0}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 1777
    invoke-virtual {p0, v2}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lepson/print/ActivityDocsPrintPreview$17;

    invoke-direct {v1, p0, p1}, Lepson/print/ActivityDocsPrintPreview$17;-><init>(Lepson/print/ActivityDocsPrintPreview;Landroid/app/AlertDialog;)V

    invoke-virtual {p1, v12, v0, v1}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    return-object p1

    .line 1754
    :pswitch_3
    new-instance p1, Landroid/app/AlertDialog$Builder;

    invoke-direct {p1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 1756
    invoke-virtual {p0, v10}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 1757
    invoke-virtual {p0, v9}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v8, [Ljava/lang/Object;

    const/16 v4, 0xa

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 1759
    invoke-virtual {p0, v2}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lepson/print/ActivityDocsPrintPreview$16;

    invoke-direct {v1, p0, p1}, Lepson/print/ActivityDocsPrintPreview$16;-><init>(Lepson/print/ActivityDocsPrintPreview;Landroid/app/AlertDialog;)V

    invoke-virtual {p1, v12, v0, v1}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    return-object p1

    .line 1737
    :pswitch_4
    new-instance p1, Landroid/app/AlertDialog$Builder;

    invoke-direct {p1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 1739
    invoke-virtual {p0, v10}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 1740
    invoke-virtual {p0, v9}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v8, [Ljava/lang/Object;

    const/16 v4, 0x14

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 1742
    invoke-virtual {p0, v2}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lepson/print/ActivityDocsPrintPreview$15;

    invoke-direct {v1, p0, p1}, Lepson/print/ActivityDocsPrintPreview$15;-><init>(Lepson/print/ActivityDocsPrintPreview;Landroid/app/AlertDialog;)V

    invoke-virtual {p1, v12, v0, v1}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    return-object p1

    .line 1720
    :pswitch_5
    new-instance p1, Landroid/app/AlertDialog$Builder;

    invoke-direct {p1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 1722
    invoke-virtual {p0, v10}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 1723
    invoke-virtual {p0, v9}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v4, v8, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v3

    invoke-static {v0, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 1725
    invoke-virtual {p0, v2}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lepson/print/ActivityDocsPrintPreview$14;

    invoke-direct {v1, p0, p1}, Lepson/print/ActivityDocsPrintPreview$14;-><init>(Lepson/print/ActivityDocsPrintPreview;Landroid/app/AlertDialog;)V

    invoke-virtual {p1, v12, v0, v1}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    return-object p1

    .line 1702
    :pswitch_6
    new-instance p1, Landroid/app/AlertDialog$Builder;

    invoke-direct {p1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 1704
    invoke-virtual {p0, v7}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    const v0, 0x7f0e0368

    .line 1705
    invoke-virtual {p0, v0}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 1708
    invoke-virtual {p0, v2}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lepson/print/ActivityDocsPrintPreview$13;

    invoke-direct {v1, p0, p1}, Lepson/print/ActivityDocsPrintPreview$13;-><init>(Lepson/print/ActivityDocsPrintPreview;Landroid/app/AlertDialog;)V

    invoke-virtual {p1, v12, v0, v1}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    return-object p1

    .line 1640
    :pswitch_7
    new-instance p1, Landroid/app/AlertDialog$Builder;

    invoke-direct {p1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    const v0, 0x7f0e051c

    .line 1641
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog;->setTitle(I)V

    const v0, 0x7f0e04e7

    .line 1642
    invoke-virtual {p0, v0}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 1643
    invoke-virtual {p0, v2}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lepson/print/ActivityDocsPrintPreview$9;

    invoke-direct {v1, p0, p1}, Lepson/print/ActivityDocsPrintPreview$9;-><init>(Lepson/print/ActivityDocsPrintPreview;Landroid/app/AlertDialog;)V

    invoke-virtual {p1, v12, v0, v1}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    return-object p1

    .line 1657
    :pswitch_8
    new-instance p1, Landroid/app/AlertDialog$Builder;

    invoke-direct {p1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 1659
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0a005a

    const v4, 0x7f0801bb

    .line 1660
    invoke-virtual {p0, v4}, Lepson/print/ActivityDocsPrintPreview;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1662
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog;->setView(Landroid/view/View;)V

    .line 1663
    invoke-virtual {p1, v3}, Landroid/app/AlertDialog;->setCancelable(Z)V

    const v1, 0x7f0e038e

    .line 1664
    invoke-virtual {p0, v1}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    const v1, 0x7f0e02a7

    .line 1665
    invoke-virtual {p0, v1}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    const v1, 0x7f0800ba

    .line 1667
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    const v1, 0x7f0e03f5

    .line 1668
    invoke-virtual {p0, v1}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 1669
    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1670
    new-instance v1, Lepson/print/ActivityDocsPrintPreview$10;

    invoke-direct {v1, p0}, Lepson/print/ActivityDocsPrintPreview$10;-><init>(Lepson/print/ActivityDocsPrintPreview;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1677
    invoke-virtual {p0, v2}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lepson/print/ActivityDocsPrintPreview$11;

    invoke-direct {v1, p0, p1}, Lepson/print/ActivityDocsPrintPreview$11;-><init>(Lepson/print/ActivityDocsPrintPreview;Landroid/app/AlertDialog;)V

    invoke-virtual {p1, v12, v0, v1}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    const/4 v0, -0x2

    .line 1691
    invoke-virtual {p0, v11}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lepson/print/ActivityDocsPrintPreview$12;

    invoke-direct {v2, p0, p1}, Lepson/print/ActivityDocsPrintPreview$12;-><init>(Lepson/print/ActivityDocsPrintPreview;Landroid/app/AlertDialog;)V

    invoke-virtual {p1, v0, v1, v2}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    return-object p1

    .line 1628
    :pswitch_9
    new-instance p1, Landroid/app/AlertDialog$Builder;

    invoke-direct {p1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1629
    invoke-virtual {p1, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const v0, 0x7f0e0439

    .line 1630
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const v0, 0x7f0e043b

    .line 1631
    invoke-virtual {p0, v0}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 1632
    invoke-virtual {p0, v2}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lepson/print/ActivityDocsPrintPreview$8;

    invoke-direct {v1, p0}, Lepson/print/ActivityDocsPrintPreview$8;-><init>(Lepson/print/ActivityDocsPrintPreview;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 1636
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1

    .line 1791
    :cond_0
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    const v0, 0x7f0a00a8

    const/4 v1, 0x0

    .line 1792
    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    const v0, 0x7f080130

    .line 1793
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 1794
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f07009f

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v4, 0x7f0e049f

    .line 1795
    invoke-virtual {p0, v4}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    invoke-virtual {p1, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 1796
    invoke-virtual {p0, v2}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lepson/print/ActivityDocsPrintPreview$19;

    invoke-direct {v2, p0, v0}, Lepson/print/ActivityDocsPrintPreview$19;-><init>(Lepson/print/ActivityDocsPrintPreview;Landroid/widget/EditText;)V

    invoke-virtual {p1, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const v0, 0x7f0e0476

    .line 1814
    invoke-virtual {p0, v0}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lepson/print/ActivityDocsPrintPreview$18;

    invoke-direct {v1, p0}, Lepson/print/ActivityDocsPrintPreview$18;-><init>(Lepson/print/ActivityDocsPrintPreview;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 1820
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .line 3056
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 3057
    invoke-virtual {p0}, Lepson/print/ActivityDocsPrintPreview;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0b0008

    .line 3058
    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const/4 p1, 0x1

    return p1
.end method

.method protected onDestroy()V
    .locals 2

    .line 660
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onDestroy()V

    .line 662
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mBitmap:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 663
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 664
    iput-object v1, p0, Lepson/print/ActivityDocsPrintPreview;->mBitmap:Landroid/graphics/Bitmap;

    .line 667
    :cond_0
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mImageView:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 668
    iput-object v1, p0, Lepson/print/ActivityDocsPrintPreview;->mImageView:Landroid/widget/ImageView;

    .line 672
    :cond_1
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mEpsonService:Lepson/print/service/IEpsonService;

    if-eqz v0, :cond_2

    .line 674
    :try_start_0
    iget-object v1, p0, Lepson/print/ActivityDocsPrintPreview;->mCallback:Lepson/print/service/IEpsonServiceCallback;

    invoke-interface {v0, v1}, Lepson/print/service/IEpsonService;->unregisterCallback(Lepson/print/service/IEpsonServiceCallback;)V

    .line 675
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mEpsonConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lepson/print/ActivityDocsPrintPreview;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 677
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_2
    :goto_0
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2

    .line 720
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onNewIntent(Landroid/content/Intent;)V

    .line 722
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 724
    iget-boolean v1, p0, Lepson/print/ActivityDocsPrintPreview;->isEnableNFCTouch:Z

    if-eqz v1, :cond_0

    const-string v1, "android.nfc.action.NDEF_DISCOVERED"

    .line 726
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 727
    invoke-static {p0, p1}, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils;->parseNECTag(Landroid/content/Context;Landroid/content/Intent;)Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 735
    invoke-virtual {p0}, Lepson/print/ActivityDocsPrintPreview;->cancelLoadRemotePreviewTask()V

    .line 738
    invoke-direct {p0}, Lepson/print/ActivityDocsPrintPreview;->saveCurSettings()V

    .line 741
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 742
    const-class v1, Lepson/print/ActivityNfcPrinter;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v1, "connectInfo"

    .line 743
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string p1, "changeMode"

    const/4 v1, 0x1

    .line 744
    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 p1, 0x5

    .line 745
    invoke-virtual {p0, v0, p1}, Lepson/print/ActivityDocsPrintPreview;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .line 3039
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f080021

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 3041
    :cond_0
    invoke-virtual {p0}, Lepson/print/ActivityDocsPrintPreview;->callPrintSetting()V

    .line 3044
    :goto_0
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1
.end method

.method protected onPause()V
    .locals 1

    .line 709
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onPause()V

    .line 711
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils;->disableForegroundDispatch(Landroid/app/Activity;)V

    .line 714
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->paperSourceInfo:Lepson/print/screen/PaperSourceInfo;

    invoke-virtual {v0}, Lepson/print/screen/PaperSourceInfo;->stop()V

    return-void
.end method

.method protected onResume()V
    .locals 2

    .line 696
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onResume()V

    const/4 v0, 0x0

    .line 699
    move-object v1, v0

    check-cast v1, [[Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils;->enableForegroundDispatch(Landroid/app/Activity;[Landroid/content/IntentFilter;[[Ljava/lang/String;)V

    .line 702
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->paperMissmath:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 703
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->paperSourceInfo:Lepson/print/screen/PaperSourceInfo;

    iget-object v1, p0, Lepson/print/ActivityDocsPrintPreview;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {v0, p0, v1}, Lepson/print/screen/PaperSourceInfo;->start(Landroid/content/Context;Landroid/os/Handler;)V

    return-void
.end method

.method public resetPageRange()V
    .locals 3

    .line 2662
    new-instance v0, Lepson/print/screen/PrintSetting;

    sget-object v1, Lepson/print/screen/PrintSetting$Kind;->document:Lepson/print/screen/PrintSetting$Kind;

    invoke-direct {v0, p0, v1}, Lepson/print/screen/PrintSetting;-><init>(Landroid/content/Context;Lepson/print/screen/PrintSetting$Kind;)V

    .line 2663
    iget v1, p0, Lepson/print/ActivityDocsPrintPreview;->mEndPageView:I

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lepson/print/screen/PrintSetting;->resetPageRange(II)V

    .line 2665
    iput v2, p0, Lepson/print/ActivityDocsPrintPreview;->mStartPage:I

    .line 2666
    iget v0, p0, Lepson/print/ActivityDocsPrintPreview;->mEndPageView:I

    iput v0, p0, Lepson/print/ActivityDocsPrintPreview;->mEndPage:I

    return-void
.end method

.method public resetPageRangeBySettings()Z
    .locals 6

    const-string v0, "PrintSetting"

    const/4 v1, 0x0

    .line 2633
    invoke-virtual {p0, v0, v1}, Lepson/print/ActivityDocsPrintPreview;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v2, "PRINTER_NAME"

    const-string v3, ""

    .line 2635
    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2638
    new-instance v2, Lepson/print/screen/PrintSetting;

    sget-object v3, Lepson/print/screen/PrintSetting$Kind;->document:Lepson/print/screen/PrintSetting$Kind;

    invoke-direct {v2, p0, v3}, Lepson/print/screen/PrintSetting;-><init>(Landroid/content/Context;Lepson/print/screen/PrintSetting$Kind;)V

    .line 2639
    invoke-virtual {v2}, Lepson/print/screen/PrintSetting;->loadSettings()V

    .line 2640
    iget v3, v2, Lepson/print/screen/PrintSetting;->paperSizeValue:I

    .line 2641
    iget v4, v2, Lepson/print/screen/PrintSetting;->layoutValue:I

    .line 2645
    iget-object v5, p0, Lepson/print/ActivityDocsPrintPreview;->mCurPrinterName:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mCurPrintSetting:Lepson/print/screen/PrintSetting;

    iget v0, v0, Lepson/print/screen/PrintSetting;->paperSizeValue:I

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mCurPrintSetting:Lepson/print/screen/PrintSetting;

    iget v0, v0, Lepson/print/screen/PrintSetting;->layoutValue:I

    if-eq v0, v4, :cond_0

    goto :goto_0

    :cond_0
    return v1

    .line 2650
    :cond_1
    :goto_0
    iget v0, p0, Lepson/print/ActivityDocsPrintPreview;->mEndPageView:I

    const/4 v1, 0x1

    invoke-virtual {v2, v1, v0}, Lepson/print/screen/PrintSetting;->resetPageRange(II)V

    return v1
.end method

.method public setNewImageView(Ljava/lang/String;)V
    .locals 5

    .line 2704
    iget v0, p0, Lepson/print/ActivityDocsPrintPreview;->printMode:I

    and-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_0

    return-void

    .line 2708
    :cond_0
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 2709
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 2711
    :cond_1
    iget v0, p0, Lepson/print/ActivityDocsPrintPreview;->mColorMode:I

    const/4 v1, 0x6

    const/16 v2, 0x57b

    const/16 v3, 0x3e0

    const/4 v4, 0x1

    if-ne v0, v4, :cond_2

    .line 2713
    :try_start_0
    invoke-static {p1, v3, v2, v4}, Lepson/print/Util/Photo;->createBitmapWithUri(Ljava/lang/String;IIZ)Landroid/graphics/Bitmap;

    move-result-object p1

    iput-object p1, p0, Lepson/print/ActivityDocsPrintPreview;->mBitmap:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 2715
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 2716
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {p1, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    .line 2721
    :try_start_1
    invoke-static {p1, v3, v2, v0}, Lepson/print/Util/Photo;->createBitmapWithUri(Ljava/lang/String;IIZ)Landroid/graphics/Bitmap;

    move-result-object p1

    iput-object p1, p0, Lepson/print/ActivityDocsPrintPreview;->mBitmap:Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception p1

    .line 2723
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 2724
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {p1, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 2728
    :goto_0
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview;->mImageView:Landroid/widget/ImageView;

    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public setOrentationView(Z)V
    .locals 16

    move-object/from16 v0, p0

    .line 2737
    invoke-direct/range {p0 .. p0}, Lepson/print/ActivityDocsPrintPreview;->loadConfig()V

    .line 2738
    invoke-virtual/range {p0 .. p0}, Lepson/print/ActivityDocsPrintPreview;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 2740
    iget-object v2, v0, Lepson/print/ActivityDocsPrintPreview;->mPaperSize:Lepson/common/Info_paper;

    invoke-virtual {v2}, Lepson/common/Info_paper;->getPaper_width()I

    move-result v2

    .line 2741
    iget-object v3, v0, Lepson/print/ActivityDocsPrintPreview;->mPaperSize:Lepson/common/Info_paper;

    invoke-virtual {v3}, Lepson/common/Info_paper;->getPaper_height()I

    move-result v3

    .line 2745
    iget v4, v0, Lepson/print/ActivityDocsPrintPreview;->mLayoutMulti:I

    const/4 v5, 0x0

    if-eqz v4, :cond_3

    const/high16 v6, 0x10000

    if-eq v4, v6, :cond_1

    const/high16 v6, 0x20000

    if-eq v4, v6, :cond_0

    const/high16 v6, 0x40000

    if-eq v4, v6, :cond_0

    const/4 v4, 0x0

    move v15, v3

    move v3, v2

    move v2, v15

    goto :goto_0

    .line 2761
    :cond_0
    iget-object v2, v0, Lepson/print/ActivityDocsPrintPreview;->mPaperSize:Lepson/common/Info_paper;

    invoke-virtual {v2}, Lepson/common/Info_paper;->getPaperSize_4in1()Landroid/graphics/Rect;

    move-result-object v2

    .line 2762
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v3

    .line 2763
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    .line 2765
    iget-object v4, v0, Lepson/print/ActivityDocsPrintPreview;->mPaperSize:Lepson/common/Info_paper;

    invoke-virtual {v4}, Lepson/common/Info_paper;->getPrintingArea_4in1()Landroid/graphics/Rect;

    move-result-object v4

    .line 2766
    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v5

    .line 2767
    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    goto :goto_0

    .line 2750
    :cond_1
    iget-object v2, v0, Lepson/print/ActivityDocsPrintPreview;->mPaperSize:Lepson/common/Info_paper;

    invoke-virtual {v2}, Lepson/common/Info_paper;->getPaperSize_2in1()Landroid/graphics/Rect;

    move-result-object v2

    .line 2751
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v3

    .line 2752
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    .line 2754
    iget-object v4, v0, Lepson/print/ActivityDocsPrintPreview;->mPaperSize:Lepson/common/Info_paper;

    invoke-virtual {v4}, Lepson/common/Info_paper;->getPrintingArea_2in1()Landroid/graphics/Rect;

    move-result-object v4

    .line 2755
    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v5

    .line 2756
    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    :goto_0
    if-le v3, v2, :cond_2

    move v15, v3

    move v3, v2

    move v2, v15

    goto :goto_1

    :cond_2
    move v15, v5

    move v5, v4

    move v4, v15

    goto :goto_1

    :cond_3
    if-le v2, v3, :cond_4

    const/4 v4, 0x0

    goto :goto_1

    :cond_4
    const/4 v4, 0x0

    move v15, v3

    move v3, v2

    move v2, v15

    :goto_1
    xor-int/lit8 v6, p1, 0x1

    .line 2790
    iput-boolean v6, v0, Lepson/print/ActivityDocsPrintPreview;->mPreviewPaperAreaLandscape:Z

    .line 2792
    new-instance v6, Landroid/graphics/Point;

    invoke-direct {v6}, Landroid/graphics/Point;-><init>()V

    .line 2793
    invoke-virtual {v1, v6}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    if-eqz p1, :cond_6

    .line 2804
    iget v1, v6, Landroid/graphics/Point;->y:I

    iget-object v7, v0, Lepson/print/ActivityDocsPrintPreview;->mPrint:Landroid/widget/Button;

    invoke-virtual {v7}, Landroid/widget/Button;->getHeight()I

    move-result v7

    mul-int/lit8 v7, v7, 0x4

    sub-int/2addr v1, v7

    int-to-double v7, v1

    int-to-double v9, v2

    div-double/2addr v7, v9

    int-to-double v11, v3

    mul-double v7, v7, v11

    double-to-int v7, v7

    .line 2811
    iget v8, v6, Landroid/graphics/Point;->x:I

    mul-int/lit8 v8, v8, 0x4

    div-int/lit8 v8, v8, 0x5

    if-le v7, v8, :cond_5

    .line 2812
    iget v1, v6, Landroid/graphics/Point;->x:I

    mul-int/lit8 v1, v1, 0x4

    div-int/lit8 v7, v1, 0x5

    int-to-double v13, v7

    div-double/2addr v13, v11

    mul-double v13, v13, v9

    double-to-int v1, v13

    .line 2816
    :cond_5
    new-instance v6, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v6, v7, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v6, v0, Lepson/print/ActivityDocsPrintPreview;->Relative_para:Landroid/widget/RelativeLayout$LayoutParams;

    goto :goto_2

    .line 2819
    :cond_6
    iget v1, v6, Landroid/graphics/Point;->x:I

    mul-int/lit8 v1, v1, 0x4

    div-int/lit8 v1, v1, 0x5

    int-to-double v7, v1

    int-to-double v9, v2

    div-double/2addr v7, v9

    int-to-double v11, v3

    mul-double v7, v7, v11

    double-to-int v7, v7

    .line 2826
    iget v8, v6, Landroid/graphics/Point;->y:I

    iget-object v13, v0, Lepson/print/ActivityDocsPrintPreview;->mPrint:Landroid/widget/Button;

    invoke-virtual {v13}, Landroid/widget/Button;->getHeight()I

    move-result v13

    mul-int/lit8 v13, v13, 0x4

    sub-int/2addr v8, v13

    if-le v7, v8, :cond_7

    .line 2827
    iget v1, v6, Landroid/graphics/Point;->y:I

    iget-object v6, v0, Lepson/print/ActivityDocsPrintPreview;->mPrint:Landroid/widget/Button;

    invoke-virtual {v6}, Landroid/widget/Button;->getHeight()I

    move-result v6

    mul-int/lit8 v6, v6, 0x4

    sub-int v7, v1, v6

    int-to-double v13, v7

    div-double/2addr v13, v11

    mul-double v13, v13, v9

    double-to-int v1, v13

    .line 2831
    :cond_7
    new-instance v6, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v6, v1, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v6, v0, Lepson/print/ActivityDocsPrintPreview;->Relative_para:Landroid/widget/RelativeLayout$LayoutParams;

    .line 2834
    :goto_2
    iget-object v1, v0, Lepson/print/ActivityDocsPrintPreview;->Relative_para:Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v6, 0xd

    invoke-virtual {v1, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2836
    iget-object v1, v0, Lepson/print/ActivityDocsPrintPreview;->mLn_zoomview:Landroid/widget/LinearLayout;

    iget-object v6, v0, Lepson/print/ActivityDocsPrintPreview;->Relative_para:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2839
    iget v1, v0, Lepson/print/ActivityDocsPrintPreview;->mLayout:I

    const/4 v6, 0x2

    if-ne v1, v6, :cond_b

    .line 2840
    iget v1, v0, Lepson/print/ActivityDocsPrintPreview;->mLayoutMulti:I

    if-nez v1, :cond_9

    if-eqz p1, :cond_8

    .line 2842
    iget-object v1, v0, Lepson/print/ActivityDocsPrintPreview;->Relative_para:Landroid/widget/RelativeLayout$LayoutParams;

    iget v1, v1, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    int-to-float v1, v1

    int-to-float v2, v3

    div-float/2addr v1, v2

    .line 2843
    iget-object v2, v0, Lepson/print/ActivityDocsPrintPreview;->mLn_zoomview:Landroid/widget/LinearLayout;

    iget-object v3, v0, Lepson/print/ActivityDocsPrintPreview;->mPaperSize:Lepson/common/Info_paper;

    invoke-virtual {v3}, Lepson/common/Info_paper;->getLeftMargin_border()I

    move-result v3

    int-to-float v3, v3

    mul-float v3, v3, v1

    float-to-int v3, v3

    iget-object v4, v0, Lepson/print/ActivityDocsPrintPreview;->mPaperSize:Lepson/common/Info_paper;

    .line 2844
    invoke-virtual {v4}, Lepson/common/Info_paper;->getTopMargin_border()I

    move-result v4

    int-to-float v4, v4

    mul-float v4, v4, v1

    float-to-int v4, v4

    iget-object v5, v0, Lepson/print/ActivityDocsPrintPreview;->mPaperSize:Lepson/common/Info_paper;

    .line 2845
    invoke-virtual {v5}, Lepson/common/Info_paper;->getRightMargin_border()I

    move-result v5

    int-to-float v5, v5

    mul-float v5, v5, v1

    float-to-int v5, v5

    iget-object v7, v0, Lepson/print/ActivityDocsPrintPreview;->mPaperSize:Lepson/common/Info_paper;

    .line 2846
    invoke-virtual {v7}, Lepson/common/Info_paper;->getBottomMargin_border()I

    move-result v7

    int-to-float v7, v7

    mul-float v7, v7, v1

    float-to-int v1, v7

    .line 2843
    invoke-virtual {v2, v3, v4, v5, v1}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    goto :goto_3

    .line 2849
    :cond_8
    iget-object v1, v0, Lepson/print/ActivityDocsPrintPreview;->Relative_para:Landroid/widget/RelativeLayout$LayoutParams;

    iget v1, v1, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    int-to-float v1, v1

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 2850
    iget-object v2, v0, Lepson/print/ActivityDocsPrintPreview;->mLn_zoomview:Landroid/widget/LinearLayout;

    iget-object v3, v0, Lepson/print/ActivityDocsPrintPreview;->mPaperSize:Lepson/common/Info_paper;

    invoke-virtual {v3}, Lepson/common/Info_paper;->getTopMargin_border()I

    move-result v3

    int-to-float v3, v3

    mul-float v3, v3, v1

    float-to-int v3, v3

    iget-object v4, v0, Lepson/print/ActivityDocsPrintPreview;->mPaperSize:Lepson/common/Info_paper;

    .line 2851
    invoke-virtual {v4}, Lepson/common/Info_paper;->getRightMargin_border()I

    move-result v4

    int-to-float v4, v4

    mul-float v4, v4, v1

    float-to-int v4, v4

    iget-object v5, v0, Lepson/print/ActivityDocsPrintPreview;->mPaperSize:Lepson/common/Info_paper;

    .line 2852
    invoke-virtual {v5}, Lepson/common/Info_paper;->getBottomMargin_border()I

    move-result v5

    int-to-float v5, v5

    mul-float v5, v5, v1

    float-to-int v5, v5

    iget-object v7, v0, Lepson/print/ActivityDocsPrintPreview;->mPaperSize:Lepson/common/Info_paper;

    .line 2853
    invoke-virtual {v7}, Lepson/common/Info_paper;->getLeftMargin_border()I

    move-result v7

    int-to-float v7, v7

    mul-float v7, v7, v1

    float-to-int v1, v7

    .line 2850
    invoke-virtual {v2, v3, v4, v5, v1}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    goto :goto_3

    :cond_9
    if-eqz p1, :cond_a

    .line 2858
    iget-object v1, v0, Lepson/print/ActivityDocsPrintPreview;->Relative_para:Landroid/widget/RelativeLayout$LayoutParams;

    iget v1, v1, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    int-to-float v1, v1

    int-to-float v7, v3

    div-float/2addr v1, v7

    .line 2859
    iget-object v7, v0, Lepson/print/ActivityDocsPrintPreview;->mLn_zoomview:Landroid/widget/LinearLayout;

    sub-int/2addr v3, v4

    div-int/2addr v3, v6

    int-to-float v3, v3

    mul-float v3, v3, v1

    float-to-int v3, v3

    sub-int/2addr v2, v5

    div-int/2addr v2, v6

    int-to-float v2, v2

    mul-float v2, v2, v1

    float-to-int v1, v2

    invoke-virtual {v7, v3, v1, v3, v1}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    goto :goto_3

    .line 2864
    :cond_a
    iget-object v1, v0, Lepson/print/ActivityDocsPrintPreview;->Relative_para:Landroid/widget/RelativeLayout$LayoutParams;

    iget v1, v1, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    int-to-float v1, v1

    int-to-float v7, v2

    div-float/2addr v1, v7

    .line 2865
    iget-object v7, v0, Lepson/print/ActivityDocsPrintPreview;->mLn_zoomview:Landroid/widget/LinearLayout;

    sub-int/2addr v2, v5

    div-int/2addr v2, v6

    int-to-float v2, v2

    mul-float v2, v2, v1

    float-to-int v2, v2

    sub-int/2addr v3, v4

    div-int/2addr v3, v6

    int-to-float v3, v3

    mul-float v3, v3, v1

    float-to-int v1, v3

    invoke-virtual {v7, v2, v1, v2, v1}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 2873
    :cond_b
    :goto_3
    iget-object v1, v0, Lepson/print/ActivityDocsPrintPreview;->mUiHandler:Landroid/os/Handler;

    const/16 v2, 0x32

    const-wide/16 v3, 0xc8

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 2875
    iget v1, v0, Lepson/print/ActivityDocsPrintPreview;->printMode:I

    and-int/2addr v1, v6

    if-nez v1, :cond_c

    .line 2876
    iget-object v1, v0, Lepson/print/ActivityDocsPrintPreview;->mDisablePreviewMsg:Landroid/widget/TextView;

    iget-object v2, v0, Lepson/print/ActivityDocsPrintPreview;->Relative_para:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_c
    return-void
.end method

.method public showErrorDialog(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 2950
    new-instance v0, Lepson/print/widgets/CustomTitleAlertDialogBuilder;

    invoke-direct {v0, p0}, Lepson/print/widgets/CustomTitleAlertDialogBuilder;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    .line 2951
    invoke-virtual {v0, v1}, Lepson/print/widgets/CustomTitleAlertDialogBuilder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 2952
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 2953
    invoke-virtual {p1, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const p2, 0x7f0e03fe

    .line 2954
    invoke-virtual {p0, p2}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object p2

    new-instance v0, Lepson/print/ActivityDocsPrintPreview$24;

    invoke-direct {v0, p0}, Lepson/print/ActivityDocsPrintPreview$24;-><init>(Lepson/print/ActivityDocsPrintPreview;)V

    invoke-virtual {p1, p2, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 2960
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method startPrintActivity()V
    .locals 5

    .line 2109
    iget v0, p0, Lepson/print/ActivityDocsPrintPreview;->printMode:I

    const/4 v1, 0x1

    and-int/2addr v0, v1

    const/16 v2, 0xff

    if-nez v0, :cond_1

    .line 2110
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mLocalPrintProgressParams:Lepson/print/screen/PrintProgress$ProgressParams;

    if-nez v0, :cond_0

    return-void

    .line 2114
    :cond_0
    invoke-static {p0, v0}, Lepson/print/screen/PrintProgress;->getPdfPrintIntent(Landroid/content/Context;Lepson/print/screen/PrintProgress$ProgressParams;)Landroid/content/Intent;

    move-result-object v0

    .line 2115
    invoke-virtual {p0, v0, v2}, Lepson/print/ActivityDocsPrintPreview;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 2117
    :cond_1
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;

    if-nez v0, :cond_2

    .line 2119
    new-instance v0, Lcom/epson/iprint/prtlogger/PrintLog;

    invoke-direct {v0}, Lcom/epson/iprint/prtlogger/PrintLog;-><init>()V

    iput-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;

    .line 2122
    :cond_2
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;

    const/4 v3, 0x2

    iput v3, v0, Lcom/epson/iprint/prtlogger/PrintLog;->previewType:I

    .line 2123
    iget-object v3, p0, Lepson/print/ActivityDocsPrintPreview;->mPrintImageList:Lepson/print/EPImageList;

    const/4 v4, 0x0

    invoke-static {p0, v3, v1, v4, v0}, Lepson/print/screen/PrintProgress;->getPrintIntent(Landroid/content/Context;Lepson/print/EPImageList;ZZLcom/epson/iprint/prtlogger/PrintLog;)Landroid/content/Intent;

    move-result-object v0

    .line 2125
    invoke-virtual {p0, v0, v2}, Lepson/print/ActivityDocsPrintPreview;->startActivityForResult(Landroid/content/Intent;I)V

    :goto_0
    return-void
.end method

.method public updatePageTextView()V
    .locals 2

    .line 2915
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lepson/print/ActivityDocsPrintPreview;->mCurrentPageView:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lepson/print/ActivityDocsPrintPreview;->mEndPageView:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2916
    iget-object v1, p0, Lepson/print/ActivityDocsPrintPreview;->mPageNum:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public updatePrevNextButtonView()V
    .locals 4

    .line 2923
    iget v0, p0, Lepson/print/ActivityDocsPrintPreview;->printMode:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_3

    .line 2924
    iget v0, p0, Lepson/print/ActivityDocsPrintPreview;->mCurrentPageView:I

    iget v1, p0, Lepson/print/ActivityDocsPrintPreview;->mStartPageView:I

    const/4 v2, 0x4

    const/4 v3, 0x0

    if-eq v0, v1, :cond_0

    .line 2925
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mPre:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 2927
    :cond_0
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mPre:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 2931
    :goto_0
    iget-boolean v0, p0, Lepson/print/ActivityDocsPrintPreview;->disablePrintArea:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 2933
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mNext:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_1

    .line 2935
    :cond_1
    iget v0, p0, Lepson/print/ActivityDocsPrintPreview;->mCurrentPageView:I

    iget v1, p0, Lepson/print/ActivityDocsPrintPreview;->mEndPageView:I

    if-eq v0, v1, :cond_2

    .line 2936
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mNext:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_1

    .line 2938
    :cond_2
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview;->mNext:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    :cond_3
    :goto_1
    return-void
.end method

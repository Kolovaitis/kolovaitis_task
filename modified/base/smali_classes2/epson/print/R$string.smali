.class public final Lepson/print/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final APP_MLID_DIV2:I = 0x7f0e0000

.field public static final APP_MLID_DIV4N:I = 0x7f0e0001

.field public static final APP_MLID_DIV4Z:I = 0x7f0e0002

.field public static final Advanced_Settings:I = 0x7f0e0003

.field public static final Agree_License:I = 0x7f0e0004

.field public static final AppDescription_3DFramePrint:I = 0x7f0e0005

.field public static final AppDescription_Creative:I = 0x7f0e0006

.field public static final AppDescription_MultiRollPrint:I = 0x7f0e0007

.field public static final AppName_3DFramePrint:I = 0x7f0e0008

.field public static final AppName_CardPrint:I = 0x7f0e0009

.field public static final AppName_Creative:I = 0x7f0e000a

.field public static final AppName_MultiRollPrint:I = 0x7f0e000b

.field public static final AppName_Nenga:I = 0x7f0e000c

.field public static final Auto_Correct:I = 0x7f0e000d

.field public static final BLE_easysetup_printer_not_found_button:I = 0x7f0e000e

.field public static final BLE_easysetup_wireless_lan_setup_button:I = 0x7f0e000f

.field public static final BLE_fail_setup_printer_msg:I = 0x7f0e0010

.field public static final BLE_fail_setup_printer_other_method_button:I = 0x7f0e0011

.field public static final BLE_fail_setup_printer_title:I = 0x7f0e0012

.field public static final BLE_find_unconfigured_printer_not_now_button:I = 0x7f0e0013

.field public static final BLE_find_unconfigured_printer_setup_button:I = 0x7f0e0014

.field public static final BLE_find_unconfigured_printer_title:I = 0x7f0e0015

.field public static final BLE_input_SSID_by_manual_dialog_msg:I = 0x7f0e0016

.field public static final BLE_input_SSID_by_manual_dialog_title:I = 0x7f0e0017

.field public static final BLE_input_password_of_SSID_dialog_msg:I = 0x7f0e0018

.field public static final BLE_input_password_of_SSID_dialog_title:I = 0x7f0e0019

.field public static final BLE_inquiry_access_point_information_msg:I = 0x7f0e001a

.field public static final BLE_not_support_security_type:I = 0x7f0e001b

.field public static final BLE_select_SSID_manual_button:I = 0x7f0e001c

.field public static final BLE_select_SSID_screen_msg:I = 0x7f0e001d

.field public static final BLE_select_SSID_screen_title:I = 0x7f0e001e

.field public static final BLE_select_unconfigured_printer_msg:I = 0x7f0e001f

.field public static final BLE_select_unconfigured_printer_title:I = 0x7f0e0020

.field public static final BLE_setup_abort_button:I = 0x7f0e0021

.field public static final BLE_setup_general_error_msg:I = 0x7f0e0022

.field public static final BLE_setup_general_error_title:I = 0x7f0e0023

.field public static final BLE_success_setup_printer_msg:I = 0x7f0e0024

.field public static final BLE_success_setup_printer_title:I = 0x7f0e0025

.field public static final BLE_wait_setup_complete_msg:I = 0x7f0e0026

.field public static final Brightness:I = 0x7f0e0027

.field public static final Buy_Ink:I = 0x7f0e0028

.field public static final Buy_Ink_Toner:I = 0x7f0e0029

.field public static final Cancel:I = 0x7f0e002a

.field public static final CannotIPMes:I = 0x7f0e002b

.field public static final CannotRemoteMes:I = 0x7f0e002c

.field public static final Cannot_retrieve_any_information_from_the_printer_:I = 0x7f0e002d

.field public static final Check_that_the_device_or_printer_is_connected_to_the_network_:I = 0x7f0e002e

.field public static final Close:I = 0x7f0e002f

.field public static final Continue:I = 0x7f0e0030

.field public static final Contrast:I = 0x7f0e0031

.field public static final Copies:I = 0x7f0e0032

.field public static final CopyCDdiscErrorText:I = 0x7f0e0033

.field public static final CopyCartridgeOverFlowErrorText:I = 0x7f0e0034

.field public static final CopyCommErrorText:I = 0x7f0e0035

.field public static final CopyInkEndErrorText:I = 0x7f0e0036

.field public static final CopyPaperJamErrorText:I = 0x7f0e0037

.field public static final CopyPaperOutErrorText:I = 0x7f0e0038

.field public static final CopyPaperOutErrorTitle:I = 0x7f0e0039

.field public static final CopyQualityBest:I = 0x7f0e003a

.field public static final CopyQualityDraft:I = 0x7f0e003b

.field public static final CopyQualityStandard:I = 0x7f0e003c

.field public static final CopyQualityXBest:I = 0x7f0e003d

.field public static final CopySourceDown:I = 0x7f0e003e

.field public static final CopySourceUp:I = 0x7f0e003f

.field public static final CopyTaskProgressCopying:I = 0x7f0e0040

.field public static final CopyTaskResultErrorOtherText:I = 0x7f0e0041

.field public static final CopyTaskResultRemoveAdfPaper:I = 0x7f0e0042

.field public static final CopyTaskResultRemoveAdfPaperTitle:I = 0x7f0e0043

.field public static final Disagree_License:I = 0x7f0e0044

.field public static final Disagree_License_Button_Message:I = 0x7f0e0045

.field public static final Done:I = 0x7f0e0046

.field public static final ECC_ERR_SVR_BUSY:I = 0x7f0e0047

.field public static final ECC_ERR_SVR_BUSY2:I = 0x7f0e0048

.field public static final ECC_ERR_SVR_BUSY_TITLE:I = 0x7f0e0049

.field public static final ECC_ERR_SVR_FILE_TOO_LONG:I = 0x7f0e004a

.field public static final ECC_ERR_SVR_FILE_TOO_LONG2:I = 0x7f0e004b

.field public static final ECC_ERR_SVR_FILE_TOO_LONG3:I = 0x7f0e004c

.field public static final ECC_ERR_SVR_FILE_TOO_LONG4:I = 0x7f0e004d

.field public static final ECC_ERR_SVR_FILE_TOO_LONG_FORMAT:I = 0x7f0e004e

.field public static final ECC_ERR_SVR_FILE_TOO_LONG_TITLE:I = 0x7f0e004f

.field public static final ECC_ERR_SVR_FILE_TOO_LONG_TITLE3:I = 0x7f0e0050

.field public static final ECC_ERR_SVR_GENERAL:I = 0x7f0e0051

.field public static final ECC_ERR_SVR_GENERAL2:I = 0x7f0e0052

.field public static final ECC_ERR_SVR_GENERAL3:I = 0x7f0e0053

.field public static final ECC_ERR_SVR_GENERAL4:I = 0x7f0e0054

.field public static final ECC_ERR_SVR_GENERAL_TITLE:I = 0x7f0e0055

.field public static final ECC_ERR_SVR_INVALID_ACCESSKEY:I = 0x7f0e0056

.field public static final ECC_ERR_SVR_INVALID_ACCESSKEY_TITLE:I = 0x7f0e0057

.field public static final ECC_ERR_SVR_NOT_REGISTERED:I = 0x7f0e0058

.field public static final ECC_ERR_SVR_NOT_REGISTERED_TITLE:I = 0x7f0e0059

.field public static final ECC_ERR_SVR_PAGE_NOT_PREPARED:I = 0x7f0e005a

.field public static final ECC_ERR_SVR_REMOTE_INVALID:I = 0x7f0e005b

.field public static final ECC_ERR_SVR_REMOTE_INVALID_TITLE:I = 0x7f0e005c

.field public static final ECC_EXERR_SRATUS_FAILE_MSG:I = 0x7f0e005d

.field public static final ECC_EXERR_SRATUS_FAILE_TITLE:I = 0x7f0e005e

.field public static final EC_ERR_COMM_ERROR:I = 0x7f0e005f

.field public static final EC_ERR_COMM_ERROR_TITLE:I = 0x7f0e0060

.field public static final ECopyModeKeyNormal:I = 0x7f0e0061

.field public static final ECopyModeKeyRepeat:I = 0x7f0e0062

.field public static final ECopyOptionItemChoiceLayout_2Repeat:I = 0x7f0e0063

.field public static final ECopyOptionItemChoiceLayout_4Repeat:I = 0x7f0e0064

.field public static final ECopyOptionItemChoiceLayout_AutoRepeat:I = 0x7f0e0065

.field public static final ECopyOptionItemChoiceXPrintCutLineStyle_Dash:I = 0x7f0e0066

.field public static final ECopyOptionItemChoiceXPrintCutLineWeight_Dash:I = 0x7f0e0067

.field public static final ECopyOptionItemChoiceXPrintCutLine_Dash:I = 0x7f0e0068

.field public static final ECopyOptionItemChoiceXRemoveBackground_Off:I = 0x7f0e0069

.field public static final ECopyOptionItemChoiceXRemoveBackground_On:I = 0x7f0e006a

.field public static final ECopyOptionItemKeyLayout:I = 0x7f0e006b

.field public static final ECopyOptionItemKeyXRemoveBackground:I = 0x7f0e006c

.field public static final ECopy_line_style_option_key:I = 0x7f0e006d

.field public static final ECopy_line_style_option_key_dotted_line_item:I = 0x7f0e006e

.field public static final ECopy_line_style_option_key_solidline_item:I = 0x7f0e006f

.field public static final ECopy_line_width_option_key:I = 0x7f0e0070

.field public static final ECopy_line_width_option_key_standard_item:I = 0x7f0e0071

.field public static final ECopy_line_width_option_key_thick_item:I = 0x7f0e0072

.field public static final ECopy_line_width_option_key_thin_item:I = 0x7f0e0073

.field public static final ECopy_print_a_cut_line_option_key:I = 0x7f0e0074

.field public static final ECopy_print_a_cut_line_option_key_off_item:I = 0x7f0e0075

.field public static final ECopy_print_a_cut_line_option_key_on_item:I = 0x7f0e0076

.field public static final EPS_APF_OFF:I = 0x7f0e0077

.field public static final EPS_APF_ON:I = 0x7f0e0078

.field public static final EPS_APF_RDE_CORRECT:I = 0x7f0e0079

.field public static final EPS_APF_RDE_NOTHING:I = 0x7f0e007a

.field public static final EPS_CM_COLOR:I = 0x7f0e007b

.field public static final EPS_CM_MONOCHROME:I = 0x7f0e007c

.field public static final EPS_COLOR_BLACK:I = 0x7f0e007d

.field public static final EPS_COLOR_BLACK1:I = 0x7f0e007e

.field public static final EPS_COLOR_BLACK1_CON:I = 0x7f0e007f

.field public static final EPS_COLOR_BLACK2:I = 0x7f0e0080

.field public static final EPS_COLOR_BLACK2_CON:I = 0x7f0e0081

.field public static final EPS_COLOR_BLACK_CON:I = 0x7f0e0082

.field public static final EPS_COLOR_BLUE:I = 0x7f0e0083

.field public static final EPS_COLOR_BLUE_CON:I = 0x7f0e0084

.field public static final EPS_COLOR_CLEAN:I = 0x7f0e0085

.field public static final EPS_COLOR_CLEAN_CON:I = 0x7f0e0086

.field public static final EPS_COLOR_CLEAR:I = 0x7f0e0087

.field public static final EPS_COLOR_CLEAR_CON:I = 0x7f0e0088

.field public static final EPS_COLOR_COMPOSITE:I = 0x7f0e0089

.field public static final EPS_COLOR_COMPOSITE_CON:I = 0x7f0e008a

.field public static final EPS_COLOR_CYAN:I = 0x7f0e008b

.field public static final EPS_COLOR_CYAN_CON:I = 0x7f0e008c

.field public static final EPS_COLOR_DARKYELLOW:I = 0x7f0e008d

.field public static final EPS_COLOR_DARKYELLOW_CON:I = 0x7f0e008e

.field public static final EPS_COLOR_DEEP_BLUE:I = 0x7f0e008f

.field public static final EPS_COLOR_DEEP_BLUE_CON:I = 0x7f0e0090

.field public static final EPS_COLOR_GRAY:I = 0x7f0e0091

.field public static final EPS_COLOR_GRAY_CON:I = 0x7f0e0092

.field public static final EPS_COLOR_GREEN:I = 0x7f0e0093

.field public static final EPS_COLOR_GREEN_CON:I = 0x7f0e0094

.field public static final EPS_COLOR_LIGHTBLACK:I = 0x7f0e0095

.field public static final EPS_COLOR_LIGHTBLACK_CON:I = 0x7f0e0096

.field public static final EPS_COLOR_LIGHTCYAN:I = 0x7f0e0097

.field public static final EPS_COLOR_LIGHTCYAN_CON:I = 0x7f0e0098

.field public static final EPS_COLOR_LIGHTGRAY:I = 0x7f0e0099

.field public static final EPS_COLOR_LIGHTGRAY_CON:I = 0x7f0e009a

.field public static final EPS_COLOR_LIGHTLIGHTBLACK:I = 0x7f0e009b

.field public static final EPS_COLOR_LIGHTLIGHTBLACK__CON:I = 0x7f0e009c

.field public static final EPS_COLOR_LIGHTMAGENTA:I = 0x7f0e009d

.field public static final EPS_COLOR_LIGHTMAGENTA_CON:I = 0x7f0e009e

.field public static final EPS_COLOR_LIGHTYELLOW:I = 0x7f0e009f

.field public static final EPS_COLOR_LIGHTYELLOW_CON:I = 0x7f0e00a0

.field public static final EPS_COLOR_MAGENTA:I = 0x7f0e00a1

.field public static final EPS_COLOR_MAGENTA_CON:I = 0x7f0e00a2

.field public static final EPS_COLOR_MATTEBLACK:I = 0x7f0e00a3

.field public static final EPS_COLOR_MATTEBLACK_CON:I = 0x7f0e00a4

.field public static final EPS_COLOR_MATTEBLACK_CON_MK:I = 0x7f0e00a5

.field public static final EPS_COLOR_ORANGE:I = 0x7f0e00a6

.field public static final EPS_COLOR_ORANGE_CON:I = 0x7f0e00a7

.field public static final EPS_COLOR_PHOTOBLACK:I = 0x7f0e00a8

.field public static final EPS_COLOR_PHOTOBLACK_CON:I = 0x7f0e00a9

.field public static final EPS_COLOR_PHOTOBLACK_CON_BK:I = 0x7f0e00aa

.field public static final EPS_COLOR_PHOTOBLACK_CON_PK:I = 0x7f0e00ab

.field public static final EPS_COLOR_RED:I = 0x7f0e00ac

.field public static final EPS_COLOR_RED_CON:I = 0x7f0e00ad

.field public static final EPS_COLOR_VIOLET:I = 0x7f0e00ae

.field public static final EPS_COLOR_VIOLET_CON:I = 0x7f0e00af

.field public static final EPS_COLOR_VIVID_LIGHTMAGENTA:I = 0x7f0e00b0

.field public static final EPS_COLOR_VIVID_LIGHTMAGENTA_CON:I = 0x7f0e00b1

.field public static final EPS_COLOR_VIVID_MAGENTA:I = 0x7f0e00b2

.field public static final EPS_COLOR_VIVID_MAGENTA_CON:I = 0x7f0e00b3

.field public static final EPS_COLOR_WHITE:I = 0x7f0e00b4

.field public static final EPS_COLOR_WHITE_CON:I = 0x7f0e00b5

.field public static final EPS_COLOR_YELLOW:I = 0x7f0e00b6

.field public static final EPS_COLOR_YELLOW_CON:I = 0x7f0e00b7

.field public static final EPS_CONNECT_CORRECT:I = 0x7f0e00b8

.field public static final EPS_CONNECT_NOTHING:I = 0x7f0e00b9

.field public static final EPS_DUPLEX_LONG:I = 0x7f0e00ba

.field public static final EPS_DUPLEX_NONE:I = 0x7f0e00bb

.field public static final EPS_DUPLEX_SHORT:I = 0x7f0e00bc

.field public static final EPS_ERR_PRINTER_NOT_FOUND_RESEARCH:I = 0x7f0e00bd

.field public static final EPS_ERR_PRINTER_NOT_FOUND_RESEARCH2:I = 0x7f0e00be

.field public static final EPS_ERR_PRINTER_NOT_FOUND_TITLE:I = 0x7f0e00bf

.field public static final EPS_FEEDDIR_LANDSCAPE:I = 0x7f0e00c0

.field public static final EPS_FEEDDIR_PORTRAIT:I = 0x7f0e00c1

.field public static final EPS_MLID_BORDERLESS:I = 0x7f0e00c2

.field public static final EPS_MLID_BORDERS:I = 0x7f0e00c3

.field public static final EPS_MLID_BORDERS_2in1:I = 0x7f0e00c4

.field public static final EPS_MLID_BORDERS_4in1_N:I = 0x7f0e00c5

.field public static final EPS_MLID_BORDERS_4in1_Z:I = 0x7f0e00c6

.field public static final EPS_MLID_CDLABEL:I = 0x7f0e00c7

.field public static final EPS_MLID_CUSTOM:I = 0x7f0e00c8

.field public static final EPS_MLID_DIVIDE16:I = 0x7f0e00c9

.field public static final EPS_MPID_AUTO:I = 0x7f0e00ca

.field public static final EPS_MPID_CDTRAY:I = 0x7f0e00cb

.field public static final EPS_MPID_FRONT1:I = 0x7f0e00cc

.field public static final EPS_MPID_FRONT2:I = 0x7f0e00cd

.field public static final EPS_MPID_FRONT3:I = 0x7f0e00ce

.field public static final EPS_MPID_FRONT4:I = 0x7f0e00cf

.field public static final EPS_MPID_HIGHCAP:I = 0x7f0e00d0

.field public static final EPS_MPID_MANUAL:I = 0x7f0e00d1

.field public static final EPS_MPID_MANUAL2:I = 0x7f0e00d2

.field public static final EPS_MPID_MPTRAY:I = 0x7f0e00d3

.field public static final EPS_MPID_MPTRAY_IJ:I = 0x7f0e00d4

.field public static final EPS_MPID_NOT_SPEC:I = 0x7f0e00d5

.field public static final EPS_MPID_REAR:I = 0x7f0e00d6

.field public static final EPS_MPID_REARMANUAL:I = 0x7f0e00d7

.field public static final EPS_MPID_ROLL:I = 0x7f0e00d8

.field public static final EPS_MQID_BEST_PLAIN:I = 0x7f0e00d9

.field public static final EPS_MQID_DRAFT:I = 0x7f0e00da

.field public static final EPS_MQID_HIGH:I = 0x7f0e00db

.field public static final EPS_MQID_NORMAL:I = 0x7f0e00dc

.field public static final EPS_MQID_STANDARD:I = 0x7f0e00dd

.field public static final EPS_MSID_10X12:I = 0x7f0e00de

.field public static final EPS_MSID_10X15:I = 0x7f0e00df

.field public static final EPS_MSID_11X14:I = 0x7f0e00e0

.field public static final EPS_MSID_12X12:I = 0x7f0e00e1

.field public static final EPS_MSID_12X18:I = 0x7f0e00e2

.field public static final EPS_MSID_16K:I = 0x7f0e00e3

.field public static final EPS_MSID_16X20:I = 0x7f0e00e4

.field public static final EPS_MSID_200X300:I = 0x7f0e00e5

.field public static final EPS_MSID_2L:I = 0x7f0e00e6

.field public static final EPS_MSID_4X6:I = 0x7f0e00e7

.field public static final EPS_MSID_5X8:I = 0x7f0e00e8

.field public static final EPS_MSID_8K:I = 0x7f0e00e9

.field public static final EPS_MSID_8X10:I = 0x7f0e00ea

.field public static final EPS_MSID_8X10_5:I = 0x7f0e00eb

.field public static final EPS_MSID_8_27X13:I = 0x7f0e00ec

.field public static final EPS_MSID_8_5X13:I = 0x7f0e00ed

.field public static final EPS_MSID_A2:I = 0x7f0e00ee

.field public static final EPS_MSID_A3:I = 0x7f0e00ef

.field public static final EPS_MSID_A3NOBI:I = 0x7f0e00f0

.field public static final EPS_MSID_A4:I = 0x7f0e00f1

.field public static final EPS_MSID_A5:I = 0x7f0e00f2

.field public static final EPS_MSID_A5_24HOLE:I = 0x7f0e00f3

.field public static final EPS_MSID_A6:I = 0x7f0e00f4

.field public static final EPS_MSID_ALBUM_A5:I = 0x7f0e00f5

.field public static final EPS_MSID_ALBUM_L:I = 0x7f0e00f6

.field public static final EPS_MSID_B3:I = 0x7f0e00f7

.field public static final EPS_MSID_B4:I = 0x7f0e00f8

.field public static final EPS_MSID_B5:I = 0x7f0e00f9

.field public static final EPS_MSID_B6:I = 0x7f0e00fa

.field public static final EPS_MSID_BUZCARD_55X91:I = 0x7f0e00fb

.field public static final EPS_MSID_BUZCARD_89X50:I = 0x7f0e00fc

.field public static final EPS_MSID_CARD_54X86:I = 0x7f0e00fd

.field public static final EPS_MSID_CHOKEI_3:I = 0x7f0e00fe

.field public static final EPS_MSID_CHOKEI_4:I = 0x7f0e00ff

.field public static final EPS_MSID_CHOKEI_40:I = 0x7f0e0100

.field public static final EPS_MSID_DBLPOSTCARD:I = 0x7f0e0101

.field public static final EPS_MSID_ENV_10_L:I = 0x7f0e0102

.field public static final EPS_MSID_ENV_10_P:I = 0x7f0e0103

.field public static final EPS_MSID_ENV_B5_P:I = 0x7f0e0104

.field public static final EPS_MSID_ENV_C4_P:I = 0x7f0e0105

.field public static final EPS_MSID_ENV_C5_P:I = 0x7f0e0106

.field public static final EPS_MSID_ENV_C6_L:I = 0x7f0e0107

.field public static final EPS_MSID_ENV_C6_P:I = 0x7f0e0108

.field public static final EPS_MSID_ENV_DL_L:I = 0x7f0e0109

.field public static final EPS_MSID_ENV_DL_P:I = 0x7f0e010a

.field public static final EPS_MSID_EXECUTIVE:I = 0x7f0e010b

.field public static final EPS_MSID_HALFCUT:I = 0x7f0e010c

.field public static final EPS_MSID_HALFLETTER:I = 0x7f0e010d

.field public static final EPS_MSID_HIVISION:I = 0x7f0e010e

.field public static final EPS_MSID_INDIAN_LEGAL:I = 0x7f0e010f

.field public static final EPS_MSID_KAKU_2:I = 0x7f0e0110

.field public static final EPS_MSID_KAKU_20:I = 0x7f0e0111

.field public static final EPS_MSID_L:I = 0x7f0e0112

.field public static final EPS_MSID_LEGAL:I = 0x7f0e0113

.field public static final EPS_MSID_LETTER:I = 0x7f0e0114

.field public static final EPS_MSID_MEISHI:I = 0x7f0e0115

.field public static final EPS_MSID_MEXICO_OFICIO:I = 0x7f0e0116

.field public static final EPS_MSID_NEWENV_P:I = 0x7f0e0117

.field public static final EPS_MSID_NEWEVN_L:I = 0x7f0e0118

.field public static final EPS_MSID_OFICIO9:I = 0x7f0e0119

.field public static final EPS_MSID_PALBUM_2L:I = 0x7f0e011a

.field public static final EPS_MSID_PALBUM_A4:I = 0x7f0e011b

.field public static final EPS_MSID_PALBUM_A5_L:I = 0x7f0e011c

.field public static final EPS_MSID_PALBUM_L_L:I = 0x7f0e011d

.field public static final EPS_MSID_PANORAMIC:I = 0x7f0e011e

.field public static final EPS_MSID_POSTCARD:I = 0x7f0e011f

.field public static final EPS_MSID_QUADRAPLEPOSTCARD:I = 0x7f0e0120

.field public static final EPS_MSID_SP1:I = 0x7f0e0121

.field public static final EPS_MSID_SP2:I = 0x7f0e0122

.field public static final EPS_MSID_SP3:I = 0x7f0e0123

.field public static final EPS_MSID_SP4:I = 0x7f0e0124

.field public static final EPS_MSID_SP5:I = 0x7f0e0125

.field public static final EPS_MSID_SQUARE_5:I = 0x7f0e0126

.field public static final EPS_MSID_SQUARE_8_27:I = 0x7f0e0127

.field public static final EPS_MSID_SRA3:I = 0x7f0e0128

.field public static final EPS_MSID_TRIM_4X6:I = 0x7f0e0129

.field public static final EPS_MSID_USB:I = 0x7f0e012a

.field public static final EPS_MSID_USC:I = 0x7f0e012b

.field public static final EPS_MSID_USER:I = 0x7f0e012c

.field public static final EPS_MSID_YOKEI_0:I = 0x7f0e012d

.field public static final EPS_MSID_YOKEI_1:I = 0x7f0e012e

.field public static final EPS_MSID_YOKEI_2:I = 0x7f0e012f

.field public static final EPS_MSID_YOKEI_3:I = 0x7f0e0130

.field public static final EPS_MSID_YOKEI_4:I = 0x7f0e0131

.field public static final EPS_MSID_YOKEI_6:I = 0x7f0e0132

.field public static final EPS_MTID_3D:I = 0x7f0e0133

.field public static final EPS_MTID_ARCHMATTE:I = 0x7f0e0134

.field public static final EPS_MTID_AUTO_PLAIN:I = 0x7f0e0135

.field public static final EPS_MTID_BARYTA:I = 0x7f0e0136

.field public static final EPS_MTID_BSMATTE_DS:I = 0x7f0e0137

.field public static final EPS_MTID_BS_HALFGLOSSY_DS:I = 0x7f0e0138

.field public static final EPS_MTID_BUSINESS_PLAIN:I = 0x7f0e0139

.field public static final EPS_MTID_CDDVD:I = 0x7f0e013a

.field public static final EPS_MTID_CDDVDHIGH:I = 0x7f0e013b

.field public static final EPS_MTID_COATED:I = 0x7f0e013c

.field public static final EPS_MTID_COLOR:I = 0x7f0e013d

.field public static final EPS_MTID_ECOPHOTO:I = 0x7f0e013e

.field public static final EPS_MTID_ENVELOPE:I = 0x7f0e013f

.field public static final EPS_MTID_GLOSSYCAST:I = 0x7f0e0140

.field public static final EPS_MTID_GLOSSYHAGAKI:I = 0x7f0e0141

.field public static final EPS_MTID_GLOSSYPHOTO:I = 0x7f0e0142

.field public static final EPS_MTID_GROSSY_ROLL_STICKER:I = 0x7f0e0143

.field public static final EPS_MTID_HAGAKIATENA:I = 0x7f0e0144

.field public static final EPS_MTID_HAGAKIINKJET:I = 0x7f0e0145

.field public static final EPS_MTID_HAGAKIRECL:I = 0x7f0e0146

.field public static final EPS_MTID_HIGH_QUALITY_PLAIN:I = 0x7f0e0147

.field public static final EPS_MTID_IRON:I = 0x7f0e0148

.field public static final EPS_MTID_LABEL:I = 0x7f0e0149

.field public static final EPS_MTID_LCPP:I = 0x7f0e014a

.field public static final EPS_MTID_LETTERHEAD:I = 0x7f0e014b

.field public static final EPS_MTID_MATTE:I = 0x7f0e014c

.field public static final EPS_MTID_MATTEMEISHI:I = 0x7f0e014d

.field public static final EPS_MTID_MINIPHOTO:I = 0x7f0e014e

.field public static final EPS_MTID_PGPHOTO:I = 0x7f0e014f

.field public static final EPS_MTID_PHOTO:I = 0x7f0e0150

.field public static final EPS_MTID_PHOTOINKJET:I = 0x7f0e0151

.field public static final EPS_MTID_PHOTOINKJET2:I = 0x7f0e0152

.field public static final EPS_MTID_PHOTOSTD:I = 0x7f0e0153

.field public static final EPS_MTID_PLAIN:I = 0x7f0e0154

.field public static final EPS_MTID_PLAIN1:I = 0x7f0e0155

.field public static final EPS_MTID_PLAIN2:I = 0x7f0e0156

.field public static final EPS_MTID_PLAIN_ROLL_STICKER:I = 0x7f0e0157

.field public static final EPS_MTID_PLATINA:I = 0x7f0e0158

.field public static final EPS_MTID_PLOOFING_WHITE_MAT:I = 0x7f0e0159

.field public static final EPS_MTID_PLPHOTO:I = 0x7f0e015a

.field public static final EPS_MTID_PREPRINTED:I = 0x7f0e015b

.field public static final EPS_MTID_PSPHOTO:I = 0x7f0e015c

.field public static final EPS_MTID_RECYCLED:I = 0x7f0e015d

.field public static final EPS_MTID_SEMI_THICK:I = 0x7f0e015e

.field public static final EPS_MTID_SFHAGAKI:I = 0x7f0e015f

.field public static final EPS_MTID_SPECIAL:I = 0x7f0e0160

.field public static final EPS_MTID_THICKPAPER:I = 0x7f0e0161

.field public static final EPS_MTID_THICKPAPER1:I = 0x7f0e0162

.field public static final EPS_MTID_THICKPAPER2:I = 0x7f0e0163

.field public static final EPS_MTID_THICKPAPER3:I = 0x7f0e0164

.field public static final EPS_MTID_THICKPAPER4:I = 0x7f0e0165

.field public static final EPS_MTID_THICKPAPER5:I = 0x7f0e0166

.field public static final EPS_MTID_THINPAPER1:I = 0x7f0e0167

.field public static final EPS_MTID_TRANSPARENCY:I = 0x7f0e0168

.field public static final EPS_MTID_ULTRASMOOTH:I = 0x7f0e0169

.field public static final EPS_MTID_UNSPECIFIED:I = 0x7f0e016a

.field public static final EPS_MTID_VELVETFINEART:I = 0x7f0e016b

.field public static final EPS_MTID_WATERCOLOR:I = 0x7f0e016c

.field public static final EPS_PD_DATETYPE1:I = 0x7f0e016d

.field public static final EPS_PD_DATETYPE2:I = 0x7f0e016e

.field public static final EPS_PD_DATETYPE3:I = 0x7f0e016f

.field public static final EPS_PD_NONE:I = 0x7f0e0170

.field public static final EPS_PRNERR_3DMEDIA_DIRECTION_MSG:I = 0x7f0e0171

.field public static final EPS_PRNERR_3DMEDIA_DIRECTION_TITLE:I = 0x7f0e0172

.field public static final EPS_PRNERR_3DMEDIA_FACE_MSG:I = 0x7f0e0173

.field public static final EPS_PRNERR_3DMEDIA_FACE_TITLE:I = 0x7f0e0174

.field public static final EPS_PRNERR_ANY_MSG:I = 0x7f0e0175

.field public static final EPS_PRNERR_ANY_TITLE:I = 0x7f0e0176

.field public static final EPS_PRNERR_BATTERYEMPTY_MSG:I = 0x7f0e0177

.field public static final EPS_PRNERR_BATTERYEMPTY_TITLE:I = 0x7f0e0178

.field public static final EPS_PRNERR_BATTERYTEMPERATURE_MSG:I = 0x7f0e0179

.field public static final EPS_PRNERR_BATTERYTEMPERATURE_TITLE:I = 0x7f0e017a

.field public static final EPS_PRNERR_BATTERYVOLTAGE_MSG:I = 0x7f0e017b

.field public static final EPS_PRNERR_BATTERYVOLTAGE_TITLE:I = 0x7f0e017c

.field public static final EPS_PRNERR_BATTERY_CHARGING_MSG:I = 0x7f0e017d

.field public static final EPS_PRNERR_BATTERY_CHARGING_TITLE:I = 0x7f0e017e

.field public static final EPS_PRNERR_BATTERY_TEMPERATURE_HIGH_MSG:I = 0x7f0e017f

.field public static final EPS_PRNERR_BATTERY_TEMPERATURE_HIGH_TITLE:I = 0x7f0e0180

.field public static final EPS_PRNERR_BATTERY_TEMPERATURE_LOW_MSG:I = 0x7f0e0181

.field public static final EPS_PRNERR_BATTERY_TEMPERATURE_LOW_TITLE:I = 0x7f0e0182

.field public static final EPS_PRNERR_BK1MODE_NEED_ACCEPT_MSG:I = 0x7f0e0183

.field public static final EPS_PRNERR_BK1MODE_NEED_ACCEPT_TITLE:I = 0x7f0e0184

.field public static final EPS_PRNERR_BK1MODE_WAITING_ACCEPT_MSG:I = 0x7f0e0185

.field public static final EPS_PRNERR_BK1MODE_WAITING_ACCEPT_TITLE:I = 0x7f0e0186

.field public static final EPS_PRNERR_BORDERLESS_WIP_END_MSG:I = 0x7f0e0187

.field public static final EPS_PRNERR_BORDERLESS_WIP_END_TITLE:I = 0x7f0e0188

.field public static final EPS_PRNERR_BORDERLESS_WIP_NEAR_END_MSG:I = 0x7f0e0189

.field public static final EPS_PRNERR_BORDERLESS_WIP_NEAR_END_TITLE:I = 0x7f0e018a

.field public static final EPS_PRNERR_BUSY_MSG:I = 0x7f0e018b

.field public static final EPS_PRNERR_BUSY_TITLE:I = 0x7f0e018c

.field public static final EPS_PRNERR_CARDLOADING_MSG:I = 0x7f0e018d

.field public static final EPS_PRNERR_CARDLOADING_TITLE:I = 0x7f0e018e

.field public static final EPS_PRNERR_CARTRIDGEOVERFLOW_MSG:I = 0x7f0e018f

.field public static final EPS_PRNERR_CARTRIDGEOVERFLOW_TITLE:I = 0x7f0e0190

.field public static final EPS_PRNERR_CASSETTECOVER_CLOSED_MSG:I = 0x7f0e0191

.field public static final EPS_PRNERR_CASSETTECOVER_CLOSED_TITLE:I = 0x7f0e0192

.field public static final EPS_PRNERR_CASSETTECOVER_OPENED_MSG:I = 0x7f0e0193

.field public static final EPS_PRNERR_CASSETTECOVER_OPENED_TITLE:I = 0x7f0e0194

.field public static final EPS_PRNERR_CDDVDCONFIG_FEEDBUTTON_MSG:I = 0x7f0e0195

.field public static final EPS_PRNERR_CDDVDCONFIG_FEEDBUTTON_TITLE:I = 0x7f0e0196

.field public static final EPS_PRNERR_CDDVDCONFIG_MSG:I = 0x7f0e0197

.field public static final EPS_PRNERR_CDDVDCONFIG_STARTBUTTON_MSG:I = 0x7f0e0198

.field public static final EPS_PRNERR_CDDVDCONFIG_STARTBUTTON_TITLE:I = 0x7f0e0199

.field public static final EPS_PRNERR_CDDVDCONFIG_TITLE:I = 0x7f0e019a

.field public static final EPS_PRNERR_CDGUIDECLOSE_MSG:I = 0x7f0e019b

.field public static final EPS_PRNERR_CDGUIDECLOSE_TITLE:I = 0x7f0e019c

.field public static final EPS_PRNERR_CDREXIST_MAINTE_MSG:I = 0x7f0e019d

.field public static final EPS_PRNERR_CDREXIST_MAINTE_TITLE:I = 0x7f0e019e

.field public static final EPS_PRNERR_CDRGUIDEOPEN_MSG:I = 0x7f0e019f

.field public static final EPS_PRNERR_CDRGUIDEOPEN_TITLE:I = 0x7f0e01a0

.field public static final EPS_PRNERR_CEMPTY_MSG:I = 0x7f0e01a1

.field public static final EPS_PRNERR_CEMPTY_TITLE:I = 0x7f0e01a2

.field public static final EPS_PRNERR_CFAIL_MSG:I = 0x7f0e01a3

.field public static final EPS_PRNERR_CFAIL_TITLE:I = 0x7f0e01a4

.field public static final EPS_PRNERR_COLOR_INKOUT_MSG:I = 0x7f0e01a5

.field public static final EPS_PRNERR_COLOR_INKOUT_TITLE:I = 0x7f0e01a6

.field public static final EPS_PRNERR_COMM:I = 0x7f0e01a7

.field public static final EPS_PRNERR_COMM1:I = 0x7f0e01a8

.field public static final EPS_PRNERR_COMM1_NOWIFI:I = 0x7f0e01a9

.field public static final EPS_PRNERR_COMM2:I = 0x7f0e01aa

.field public static final EPS_PRNERR_COMM2_MSG:I = 0x7f0e01ab

.field public static final EPS_PRNERR_COMM2_TITLE:I = 0x7f0e01ac

.field public static final EPS_PRNERR_COMM3:I = 0x7f0e01ad

.field public static final EPS_PRNERR_COMM4:I = 0x7f0e01ae

.field public static final EPS_PRNERR_COMM5:I = 0x7f0e01af

.field public static final EPS_PRNERR_COMM_MSG:I = 0x7f0e01b0

.field public static final EPS_PRNERR_COMM_TITLE:I = 0x7f0e01b1

.field public static final EPS_PRNERR_COMM_TITLE1:I = 0x7f0e01b2

.field public static final EPS_PRNERR_COMM_TITLE2:I = 0x7f0e01b3

.field public static final EPS_PRNERR_COMM_TITLE3:I = 0x7f0e01b4

.field public static final EPS_PRNERR_COMM_TITLE4:I = 0x7f0e01b5

.field public static final EPS_PRNERR_COVEROPEN_MSG:I = 0x7f0e01b6

.field public static final EPS_PRNERR_COVEROPEN_TITLE:I = 0x7f0e01b7

.field public static final EPS_PRNERR_DISABEL_CLEANING_MSG:I = 0x7f0e01b8

.field public static final EPS_PRNERR_DISABEL_CLEANING_TITLE:I = 0x7f0e01b9

.field public static final EPS_PRNERR_DISABLE_DUPLEX_MSG:I = 0x7f0e01ba

.field public static final EPS_PRNERR_DISABLE_DUPLEX_TITLE:I = 0x7f0e01bb

.field public static final EPS_PRNERR_DOUBLEFEED_MSG:I = 0x7f0e01bc

.field public static final EPS_PRNERR_DOUBLEFEED_TITLE:I = 0x7f0e01bd

.field public static final EPS_PRNERR_FACTORY_MSG:I = 0x7f0e01be

.field public static final EPS_PRNERR_FACTORY_TITLE:I = 0x7f0e01bf

.field public static final EPS_PRNERR_FATAL_MSG:I = 0x7f0e01c0

.field public static final EPS_PRNERR_FATAL_TITLE:I = 0x7f0e01c1

.field public static final EPS_PRNERR_FEEDERCLOSE_MSG:I = 0x7f0e01c2

.field public static final EPS_PRNERR_FEEDERCLOSE_TITLE:I = 0x7f0e01c3

.field public static final EPS_PRNERR_GENERAL_ERR:I = 0x7f0e01c4

.field public static final EPS_PRNERR_GENERAL_MSG:I = 0x7f0e01c5

.field public static final EPS_PRNERR_GENERAL_TITLE:I = 0x7f0e01c6

.field public static final EPS_PRNERR_INKCOVEROPEN_MSG:I = 0x7f0e01c7

.field public static final EPS_PRNERR_INKCOVEROPEN_TITLE:I = 0x7f0e01c8

.field public static final EPS_PRNERR_INKOUT_BK1MODE_MSG:I = 0x7f0e01c9

.field public static final EPS_PRNERR_INKOUT_BK1MODE_TITLE:I = 0x7f0e01ca

.field public static final EPS_PRNERR_INKOUT_MSG:I = 0x7f0e01cb

.field public static final EPS_PRNERR_INKOUT_TITLE:I = 0x7f0e01cc

.field public static final EPS_PRNERR_INK_TONER_OUT_MSG:I = 0x7f0e01cd

.field public static final EPS_PRNERR_INK_TONER_OUT_TITLE:I = 0x7f0e01ce

.field public static final EPS_PRNERR_INTERFACE_MSG:I = 0x7f0e01cf

.field public static final EPS_PRNERR_INTERFACE_TITLE:I = 0x7f0e01d0

.field public static final EPS_PRNERR_INTERRUPT_BY_INKEND_MSG:I = 0x7f0e01d1

.field public static final EPS_PRNERR_INTERRUPT_BY_INKEND_TITLE:I = 0x7f0e01d2

.field public static final EPS_PRNERR_LOW_BATTERY_FNC_MSG:I = 0x7f0e01d3

.field public static final EPS_PRNERR_LOW_BATTERY_FNC_TITLE:I = 0x7f0e01d4

.field public static final EPS_PRNERR_MANUALFEED_EXCESSIVE_MSG:I = 0x7f0e01d5

.field public static final EPS_PRNERR_MANUALFEED_EXCESSIVE_NOLCD_MSG:I = 0x7f0e01d6

.field public static final EPS_PRNERR_MANUALFEED_EXCESSIVE_NOLCD_TITLE:I = 0x7f0e01d7

.field public static final EPS_PRNERR_MANUALFEED_EXCESSIVE_TITLE:I = 0x7f0e01d8

.field public static final EPS_PRNERR_MANUALFEED_FAILED_MSG:I = 0x7f0e01d9

.field public static final EPS_PRNERR_MANUALFEED_FAILED_NOLCD_MSG:I = 0x7f0e01da

.field public static final EPS_PRNERR_MANUALFEED_FAILED_NOLCD_TITLE:I = 0x7f0e01db

.field public static final EPS_PRNERR_MANUALFEED_FAILED_TITLE:I = 0x7f0e01dc

.field public static final EPS_PRNERR_MANUALFEED_SET_PAPER_MSG:I = 0x7f0e01dd

.field public static final EPS_PRNERR_MANUALFEED_SET_PAPER_NOLCD_MSG:I = 0x7f0e01de

.field public static final EPS_PRNERR_MANUALFEED_SET_PAPER_NOLCD_TITLE:I = 0x7f0e01df

.field public static final EPS_PRNERR_MANUALFEED_SET_PAPER_TITLE:I = 0x7f0e01e0

.field public static final EPS_PRNERR_NOTRAY_MSG:I = 0x7f0e01e1

.field public static final EPS_PRNERR_NOTRAY_TITLE:I = 0x7f0e01e2

.field public static final EPS_PRNERR_NOT_INITIALFILL_MSG:I = 0x7f0e01e3

.field public static final EPS_PRNERR_NOT_INITIALFILL_TITLE:I = 0x7f0e01e4

.field public static final EPS_PRNERR_NOT_SELECTED:I = 0x7f0e01e5

.field public static final EPS_PRNERR_NOT_SELECTED_TITTLE:I = 0x7f0e01e6

.field public static final EPS_PRNERR_NO_BATTERY_MSG:I = 0x7f0e01e7

.field public static final EPS_PRNERR_NO_BATTERY_TITLE:I = 0x7f0e01e8

.field public static final EPS_PRNERR_NO_MAINTENANCE_BOX_MSG:I = 0x7f0e01e9

.field public static final EPS_PRNERR_NO_MAINTENANCE_BOX_TITLE:I = 0x7f0e01ea

.field public static final EPS_PRNERR_NO_STAPLE_MSG:I = 0x7f0e01eb

.field public static final EPS_PRNERR_NO_STAPLE_TITLE:I = 0x7f0e01ec

.field public static final EPS_PRNERR_PAPERJAM_MRP_MSG:I = 0x7f0e01ed

.field public static final EPS_PRNERR_PAPERJAM_MRP_TITLE:I = 0x7f0e01ee

.field public static final EPS_PRNERR_PAPERJAM_MSG:I = 0x7f0e01ef

.field public static final EPS_PRNERR_PAPERJAM_TITLE:I = 0x7f0e01f0

.field public static final EPS_PRNERR_PAPEROUT_MRP_MSG:I = 0x7f0e01f1

.field public static final EPS_PRNERR_PAPEROUT_MRP_TITLE:I = 0x7f0e01f2

.field public static final EPS_PRNERR_PAPEROUT_MSG:I = 0x7f0e01f3

.field public static final EPS_PRNERR_PAPEROUT_TITLE:I = 0x7f0e01f4

.field public static final EPS_PRNERR_PC_DIRECTION1_MSG:I = 0x7f0e01f5

.field public static final EPS_PRNERR_PC_DIRECTION1_TITLE:I = 0x7f0e01f6

.field public static final EPS_PRNERR_PC_DIRECTION2_MSG:I = 0x7f0e01f7

.field public static final EPS_PRNERR_PC_DIRECTION2_TITLE:I = 0x7f0e01f8

.field public static final EPS_PRNERR_PC_FACE1_MSG:I = 0x7f0e01f9

.field public static final EPS_PRNERR_PC_FACE1_TITLE:I = 0x7f0e01fa

.field public static final EPS_PRNERR_PC_FACE2_MSG:I = 0x7f0e01fb

.field public static final EPS_PRNERR_PC_FACE2_TITLE:I = 0x7f0e01fc

.field public static final EPS_PRNERR_PRINTPACKEND_MSG:I = 0x7f0e01fd

.field public static final EPS_PRNERR_PRINTPACKEND_TITLE:I = 0x7f0e01fe

.field public static final EPS_PRNERR_READYPRINT_SERVICE_MSG:I = 0x7f0e01ff

.field public static final EPS_PRNERR_READYPRINT_SERVICE_TITLE:I = 0x7f0e0200

.field public static final EPS_PRNERR_REPLACE_MAINTENANCE_BOX_MSG:I = 0x7f0e0201

.field public static final EPS_PRNERR_REPLACE_MAINTENANCE_BOX_TITLE:I = 0x7f0e0202

.field public static final EPS_PRNERR_ROLLPAPER_TOOSHORT_MSG:I = 0x7f0e0203

.field public static final EPS_PRNERR_ROLLPAPER_TOOSHORT_TITLE:I = 0x7f0e0204

.field public static final EPS_PRNERR_SCANNEROPEN_MSG:I = 0x7f0e0205

.field public static final EPS_PRNERR_SCANNEROPEN_TITLE:I = 0x7f0e0206

.field public static final EPS_PRNERR_SERVICEREQ_MSG:I = 0x7f0e0207

.field public static final EPS_PRNERR_SERVICEREQ_TITLE:I = 0x7f0e0208

.field public static final EPS_PRNERR_SHUTOFF_MSG:I = 0x7f0e0209

.field public static final EPS_PRNERR_SHUTOFF_TITLE:I = 0x7f0e020a

.field public static final EPS_PRNERR_SIZE_TYPE_PATH_MRP_MSG:I = 0x7f0e020b

.field public static final EPS_PRNERR_SIZE_TYPE_PATH_MRP_TITLE:I = 0x7f0e020c

.field public static final EPS_PRNERR_SIZE_TYPE_PATH_MSG:I = 0x7f0e020d

.field public static final EPS_PRNERR_SIZE_TYPE_PATH_TITLE:I = 0x7f0e020e

.field public static final EPS_PRNERR_STACKER_FULL_MSG:I = 0x7f0e020f

.field public static final EPS_PRNERR_STACKER_FULL_TITLE:I = 0x7f0e0210

.field public static final EPS_PRNERR_TRAYCLOSE_MSG:I = 0x7f0e0211

.field public static final EPS_PRNERR_TRAYCLOSE_TITLE:I = 0x7f0e0212

.field public static final EPS_PRNERR_WEB_REMOTE_MAINTENANCE_BOX_MSG:I = 0x7f0e0213

.field public static final EPS_PRNERR_WEB_REMOTE_MAINTENANCE_BOX_TITLE:I = 0x7f0e0214

.field public static final EPS_PRNERR_WIP_NEAR_END_MSG:I = 0x7f0e0215

.field public static final EPS_PRNERR_WIP_NEAR_END_TITLE:I = 0x7f0e0216

.field public static final EPS_PRNST_BUSY_MSG:I = 0x7f0e0217

.field public static final EPS_PRNST_BUSY_TITLE:I = 0x7f0e0218

.field public static final EPS_PRNST_CANCELLING_MSG:I = 0x7f0e0219

.field public static final EPS_PRNST_CANCELLING_TITLE:I = 0x7f0e021a

.field public static final EPS_PRNST_IDLE_MSG:I = 0x7f0e021b

.field public static final EPS_PRNST_IDLE_TITLE:I = 0x7f0e021c

.field public static final EPS_PRNST_PRINTING_MSG:I = 0x7f0e021d

.field public static final EPS_PRNST_PRINTING_TITLE:I = 0x7f0e021e

.field public static final EPS_SHARPNESS_OFF:I = 0x7f0e021f

.field public static final EPS_SHARPNESS_ON:I = 0x7f0e0220

.field public static final EULA_Title:I = 0x7f0e0221

.field public static final End_page:I = 0x7f0e0222

.field public static final Executing___:I = 0x7f0e0223

.field public static final FunctionName_Creative_Coloring:I = 0x7f0e0224

.field public static final FunctionName_Creative_DiscLabel:I = 0x7f0e0225

.field public static final FunctionName_Creative_Letter:I = 0x7f0e0226

.field public static final FunctionName_Creative_QRPrint:I = 0x7f0e0227

.field public static final FunctionName_Creative_SyntheticSheet:I = 0x7f0e0228

.field public static final FunctionName_iPrint_CameraCopy:I = 0x7f0e0229

.field public static final FunctionName_iPrint_Copy:I = 0x7f0e022a

.field public static final FunctionName_iPrint_Document:I = 0x7f0e022b

.field public static final FunctionName_iPrint_MemoryCard:I = 0x7f0e022c

.field public static final FunctionName_iPrint_MoreApps:I = 0x7f0e022d

.field public static final FunctionName_iPrint_OnlineStorage:I = 0x7f0e022e

.field public static final FunctionName_iPrint_Photo:I = 0x7f0e022f

.field public static final FunctionName_iPrint_PhotoTransfer:I = 0x7f0e0230

.field public static final FunctionName_iPrint_Scan:I = 0x7f0e0231

.field public static final FunctionName_iPrint_Web:I = 0x7f0e0232

.field public static final GALicense:I = 0x7f0e0233

.field public static final GALicense_Label:I = 0x7f0e0234

.field public static final Head_Cleaning:I = 0x7f0e0235

.field public static final Layout:I = 0x7f0e0236

.field public static final Maintenance:I = 0x7f0e0237

.field public static final Maintenance_Box_Service_Life:I = 0x7f0e0238

.field public static final Media_Type:I = 0x7f0e0239

.field public static final NOT_IMPLEMENT:I = 0x7f0e023a

.field public static final NOT_IMPLEMENT_TITLE:I = 0x7f0e023b

.field public static final No:I = 0x7f0e023c

.field public static final Nozzle_Check:I = 0x7f0e023d

.field public static final OK:I = 0x7f0e023e

.field public static final OSS_License_Title:I = 0x7f0e023f

.field public static final Off:I = 0x7f0e0240

.field public static final On:I = 0x7f0e0241

.field public static final Orientation:I = 0x7f0e0242

.field public static final Paper_Size:I = 0x7f0e0243

.field public static final Paper_Source:I = 0x7f0e0244

.field public static final Print_All:I = 0x7f0e0245

.field public static final Print_Date:I = 0x7f0e0246

.field public static final Print_Quality:I = 0x7f0e0247

.field public static final Print_Settings:I = 0x7f0e0248

.field public static final Printer:I = 0x7f0e0249

.field public static final Printer_Settings:I = 0x7f0e024a

.field public static final Printer_Status:I = 0x7f0e024b

.field public static final Printing_Range:I = 0x7f0e024c

.field public static final Privacy_Statement_Title:I = 0x7f0e024d

.field public static final Remaining_Ink:I = 0x7f0e024e

.field public static final Remaining_Ink_Toner:I = 0x7f0e024f

.field public static final Saturation:I = 0x7f0e0250

.field public static final ScanContentType_Mixed:I = 0x7f0e0251

.field public static final ScanContentType_Photographic:I = 0x7f0e0252

.field public static final ScanContentType_Text:I = 0x7f0e0253

.field public static final Select_a_printer_:I = 0x7f0e0254

.field public static final Serial_Number:I = 0x7f0e0255

.field public static final Sharpness:I = 0x7f0e0256

.field public static final Start_page:I = 0x7f0e0257

.field public static final Tap_Printer_to_select_a_printer_:I = 0x7f0e0258

.field public static final Update_Message:I = 0x7f0e0259

.field public static final Use_Information_Message:I = 0x7f0e025a

.field public static final Use_Information_Title:I = 0x7f0e025b

.field public static final XScale_2L_to_A4:I = 0x7f0e025c

.field public static final XScale_2L_to_KG:I = 0x7f0e025d

.field public static final XScale_2L_to_Letter:I = 0x7f0e025e

.field public static final XScale_2L_to_Postcard:I = 0x7f0e025f

.field public static final XScale_8x10_to_2L:I = 0x7f0e0260

.field public static final XScale_A4_to_2L:I = 0x7f0e0261

.field public static final XScale_A4_to_A3:I = 0x7f0e0262

.field public static final XScale_A4_to_A5:I = 0x7f0e0263

.field public static final XScale_A4_to_B5:I = 0x7f0e0264

.field public static final XScale_A4_to_KG:I = 0x7f0e0265

.field public static final XScale_A4_to_Postcard:I = 0x7f0e0266

.field public static final XScale_A5_to_A4:I = 0x7f0e0267

.field public static final XScale_Autofit:I = 0x7f0e0268

.field public static final XScale_B5_to_A4:I = 0x7f0e0269

.field public static final XScale_Custom:I = 0x7f0e026a

.field public static final XScale_CustomSetting:I = 0x7f0e026b

.field public static final XScale_FullSize:I = 0x7f0e026c

.field public static final XScale_KG_to_2L:I = 0x7f0e026d

.field public static final XScale_KG_to_8x10:I = 0x7f0e026e

.field public static final XScale_KG_to_A4:I = 0x7f0e026f

.field public static final XScale_KG_to_Letter:I = 0x7f0e0270

.field public static final XScale_L_to_2L:I = 0x7f0e0271

.field public static final XScale_L_to_A4:I = 0x7f0e0272

.field public static final XScale_L_to_Postcard:I = 0x7f0e0273

.field public static final XScale_Legal_to_Letter:I = 0x7f0e0274

.field public static final XScale_Letter_to_2L:I = 0x7f0e0275

.field public static final XScale_Letter_to_KG:I = 0x7f0e0276

.field public static final XScale_Letter_to_USB:I = 0x7f0e0277

.field public static final XScale_Postcard_to_A4:I = 0x7f0e0278

.field public static final Yes:I = 0x7f0e0279

.field public static final abc_action_bar_home_description:I = 0x7f0e027a

.field public static final abc_action_bar_up_description:I = 0x7f0e027b

.field public static final abc_action_menu_overflow_description:I = 0x7f0e027c

.field public static final abc_action_mode_done:I = 0x7f0e027d

.field public static final abc_activity_chooser_view_see_all:I = 0x7f0e027e

.field public static final abc_activitychooserview_choose_application:I = 0x7f0e027f

.field public static final abc_capital_off:I = 0x7f0e0280

.field public static final abc_capital_on:I = 0x7f0e0281

.field public static final abc_font_family_body_1_material:I = 0x7f0e0282

.field public static final abc_font_family_body_2_material:I = 0x7f0e0283

.field public static final abc_font_family_button_material:I = 0x7f0e0284

.field public static final abc_font_family_caption_material:I = 0x7f0e0285

.field public static final abc_font_family_display_1_material:I = 0x7f0e0286

.field public static final abc_font_family_display_2_material:I = 0x7f0e0287

.field public static final abc_font_family_display_3_material:I = 0x7f0e0288

.field public static final abc_font_family_display_4_material:I = 0x7f0e0289

.field public static final abc_font_family_headline_material:I = 0x7f0e028a

.field public static final abc_font_family_menu_material:I = 0x7f0e028b

.field public static final abc_font_family_subhead_material:I = 0x7f0e028c

.field public static final abc_font_family_title_material:I = 0x7f0e028d

.field public static final abc_menu_alt_shortcut_label:I = 0x7f0e028e

.field public static final abc_menu_ctrl_shortcut_label:I = 0x7f0e028f

.field public static final abc_menu_delete_shortcut_label:I = 0x7f0e0290

.field public static final abc_menu_enter_shortcut_label:I = 0x7f0e0291

.field public static final abc_menu_function_shortcut_label:I = 0x7f0e0292

.field public static final abc_menu_meta_shortcut_label:I = 0x7f0e0293

.field public static final abc_menu_shift_shortcut_label:I = 0x7f0e0294

.field public static final abc_menu_space_shortcut_label:I = 0x7f0e0295

.field public static final abc_menu_sym_shortcut_label:I = 0x7f0e0296

.field public static final abc_prepend_shortcut_label:I = 0x7f0e0297

.field public static final abc_search_hint:I = 0x7f0e0298

.field public static final abc_searchview_description_clear:I = 0x7f0e0299

.field public static final abc_searchview_description_query:I = 0x7f0e029a

.field public static final abc_searchview_description_search:I = 0x7f0e029b

.field public static final abc_searchview_description_submit:I = 0x7f0e029c

.field public static final abc_searchview_description_voice:I = 0x7f0e029d

.field public static final abc_shareactionprovider_share_with:I = 0x7f0e029e

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f0e029f

.field public static final abc_toolbar_collapse_description:I = 0x7f0e02a0

.field public static final about_all:I = 0x7f0e02a1

.field public static final about_copy:I = 0x7f0e02a2

.field public static final about_for:I = 0x7f0e02a3

.field public static final about_ver:I = 0x7f0e02a4

.field public static final add_button:I = 0x7f0e02a5

.field public static final add_new_folder:I = 0x7f0e02a6

.field public static final agreement_google_docs_convert:I = 0x7f0e02a7

.field public static final all_album:I = 0x7f0e02a8

.field public static final all_clear_setting:I = 0x7f0e02a9

.field public static final apf_processing:I = 0x7f0e02aa

.field public static final apf_setting_title:I = 0x7f0e02ab

.field public static final app_name:I = 0x7f0e02ac

.field public static final authenticate_error_mes:I = 0x7f0e02ad

.field public static final authenticate_error_title:I = 0x7f0e02ae

.field public static final auto_picture_taking_str:I = 0x7f0e02af

.field public static final back_button:I = 0x7f0e02b0

.field public static final back_camera_not_exists_message:I = 0x7f0e02b1

.field public static final bi_no_percentage:I = 0x7f0e02b2

.field public static final bi_remain:I = 0x7f0e02b3

.field public static final bi_title:I = 0x7f0e02b4

.field public static final blocked_ip_error_message:I = 0x7f0e02b5

.field public static final box_button_message:I = 0x7f0e02b6

.field public static final box_net:I = 0x7f0e02b7

.field public static final boxsdk_Authenticating:I = 0x7f0e02b8

.field public static final boxsdk_Authentication_fail:I = 0x7f0e02b9

.field public static final boxsdk_Authentication_fail_forbidden:I = 0x7f0e02ba

.field public static final boxsdk_Authentication_fail_url_mismatch:I = 0x7f0e02bb

.field public static final boxsdk_Go_back:I = 0x7f0e02bc

.field public static final boxsdk_Please_wait:I = 0x7f0e02bd

.field public static final boxsdk_Security_Warning:I = 0x7f0e02be

.field public static final boxsdk_Select_an_account_to_use:I = 0x7f0e02bf

.field public static final boxsdk_There_are_problems_with_the_security_certificate_for_this_site:I = 0x7f0e02c0

.field public static final boxsdk_Use_a_different_account:I = 0x7f0e02c1

.field public static final boxsdk_alert_dialog_cancel:I = 0x7f0e02c2

.field public static final boxsdk_alert_dialog_ok:I = 0x7f0e02c3

.field public static final boxsdk_alert_dialog_password:I = 0x7f0e02c4

.field public static final boxsdk_alert_dialog_text_entry:I = 0x7f0e02c5

.field public static final boxsdk_alert_dialog_username:I = 0x7f0e02c6

.field public static final boxsdk_box_app_signature:I = 0x7f0e02c7

.field public static final boxsdk_button_ok:I = 0x7f0e02c8

.field public static final boxsdk_button_okay:I = 0x7f0e02c9

.field public static final boxsdk_bytes:I = 0x7f0e02ca

.field public static final boxsdk_common_name:I = 0x7f0e02cb

.field public static final boxsdk_details:I = 0x7f0e02cc

.field public static final boxsdk_error_fatal_refresh:I = 0x7f0e02cd

.field public static final boxsdk_error_network_connection:I = 0x7f0e02ce

.field public static final boxsdk_error_terms_of_service:I = 0x7f0e02cf

.field public static final boxsdk_expires_on:I = 0x7f0e02d0

.field public static final boxsdk_gigabytes:I = 0x7f0e02d1

.field public static final boxsdk_issued_by:I = 0x7f0e02d2

.field public static final boxsdk_issued_on:I = 0x7f0e02d3

.field public static final boxsdk_issued_to:I = 0x7f0e02d4

.field public static final boxsdk_kilobytes:I = 0x7f0e02d5

.field public static final boxsdk_megabytes:I = 0x7f0e02d6

.field public static final boxsdk_no_offline_access:I = 0x7f0e02d7

.field public static final boxsdk_no_offline_access_detail:I = 0x7f0e02d8

.field public static final boxsdk_no_offline_access_todo:I = 0x7f0e02d9

.field public static final boxsdk_org_name:I = 0x7f0e02da

.field public static final boxsdk_org_unit:I = 0x7f0e02db

.field public static final boxsdk_ssl_error_details:I = 0x7f0e02dc

.field public static final boxsdk_ssl_error_warning_DATE_INVALID:I = 0x7f0e02dd

.field public static final boxsdk_ssl_error_warning_EXPIRED:I = 0x7f0e02de

.field public static final boxsdk_ssl_error_warning_ID_MISMATCH:I = 0x7f0e02df

.field public static final boxsdk_ssl_error_warning_INVALID:I = 0x7f0e02e0

.field public static final boxsdk_ssl_error_warning_NOT_YET_VALID:I = 0x7f0e02e1

.field public static final boxsdk_ssl_error_warning_UNTRUSTED:I = 0x7f0e02e2

.field public static final boxsdk_ssl_should_not_proceed:I = 0x7f0e02e3

.field public static final boxsdk_terabytes:I = 0x7f0e02e4

.field public static final boxsdk_unable_to_connect:I = 0x7f0e02e5

.field public static final boxsdk_unable_to_connect_detail:I = 0x7f0e02e6

.field public static final boxsdk_unable_to_connect_todo:I = 0x7f0e02e7

.field public static final boxsdk_validity_period:I = 0x7f0e02e8

.field public static final brightness:I = 0x7f0e02e9

.field public static final browse_site:I = 0x7f0e02ea

.field public static final build_count:I = 0x7f0e02eb

.field public static final build_type:I = 0x7f0e02ec

.field public static final camera_option_title:I = 0x7f0e02ed

.field public static final camera_picture_low_message:I = 0x7f0e02ee

.field public static final cancel_button:I = 0x7f0e02ef

.field public static final cannot_connect:I = 0x7f0e02f0

.field public static final capture_title:I = 0x7f0e02f1

.field public static final clear_button:I = 0x7f0e02f2

.field public static final color:I = 0x7f0e02f3

.field public static final color_adjustment_button:I = 0x7f0e02f4

.field public static final color_adjustment_title:I = 0x7f0e02f5

.field public static final comm_error:I = 0x7f0e02f6

.field public static final common_google_play_services_enable_button:I = 0x7f0e02f7

.field public static final common_google_play_services_enable_text:I = 0x7f0e02f8

.field public static final common_google_play_services_enable_title:I = 0x7f0e02f9

.field public static final common_google_play_services_install_button:I = 0x7f0e02fa

.field public static final common_google_play_services_install_text:I = 0x7f0e02fb

.field public static final common_google_play_services_install_title:I = 0x7f0e02fc

.field public static final common_google_play_services_notification_channel_name:I = 0x7f0e02fd

.field public static final common_google_play_services_notification_ticker:I = 0x7f0e02fe

.field public static final common_google_play_services_unknown_issue:I = 0x7f0e02ff

.field public static final common_google_play_services_unsupported_text:I = 0x7f0e0300

.field public static final common_google_play_services_update_button:I = 0x7f0e0301

.field public static final common_google_play_services_update_text:I = 0x7f0e0302

.field public static final common_google_play_services_update_title:I = 0x7f0e0303

.field public static final common_google_play_services_updating_text:I = 0x7f0e0304

.field public static final common_google_play_services_wear_update_text:I = 0x7f0e0305

.field public static final common_open_on_phone:I = 0x7f0e0306

.field public static final common_signin_button_text:I = 0x7f0e0307

.field public static final common_signin_button_text_long:I = 0x7f0e0308

.field public static final confirm_browse_wlan_setup_info:I = 0x7f0e0309

.field public static final contrast:I = 0x7f0e030a

.field public static final convert_google_docs_convert:I = 0x7f0e030b

.field public static final copies:I = 0x7f0e030c

.field public static final copy_copies:I = 0x7f0e030d

.field public static final copy_density:I = 0x7f0e030e

.field public static final copy_scale:I = 0x7f0e030f

.field public static final copy_setting_label_countdown:I = 0x7f0e0310

.field public static final copy_setting_label_countup:I = 0x7f0e0311

.field public static final crop_image_message:I = 0x7f0e0312

.field public static final crop_image_title:I = 0x7f0e0313

.field public static final date_print:I = 0x7f0e0314

.field public static final delete_documentsize_message:I = 0x7f0e0315

.field public static final delete_file:I = 0x7f0e0316

.field public static final density_setting_label_countdown:I = 0x7f0e0317

.field public static final density_setting_label_countup:I = 0x7f0e0318

.field public static final docsize_delete:I = 0x7f0e0319

.field public static final docsize_edit:I = 0x7f0e031a

.field public static final docsize_select:I = 0x7f0e031b

.field public static final document_btn_label:I = 0x7f0e031c

.field public static final document_size:I = 0x7f0e031d

.field public static final document_size_button:I = 0x7f0e031e

.field public static final document_size_edit_title:I = 0x7f0e031f

.field public static final document_size_title:I = 0x7f0e0320

.field public static final document_type:I = 0x7f0e0321

.field public static final donot_show_again:I = 0x7f0e0322

.field public static final download_error_mes:I = 0x7f0e0323

.field public static final download_error_title:I = 0x7f0e0324

.field public static final download_google_docs_convert:I = 0x7f0e0325

.field public static final downloading_dialog_title:I = 0x7f0e0326

.field public static final downloading_notification:I = 0x7f0e0327

.field public static final drop_box:I = 0x7f0e0328

.field public static final duplex:I = 0x7f0e0329

.field public static final ec_ble_registration_dialog_title:I = 0x7f0e032a

.field public static final ec_enabled_message:I = 0x7f0e032b

.field public static final ec_enabled_title:I = 0x7f0e032c

.field public static final ec_enabling:I = 0x7f0e032d

.field public static final ec_enabling_error_message:I = 0x7f0e032e

.field public static final ec_enabling_error_title:I = 0x7f0e032f

.field public static final ec_registration:I = 0x7f0e0330

.field public static final ec_registration_button_close:I = 0x7f0e0331

.field public static final ec_registration_button_signup:I = 0x7f0e0332

.field public static final ec_registration_button_skip:I = 0x7f0e0333

.field public static final ec_registration_dialog_admin_mode:I = 0x7f0e0334

.field public static final ec_registration_dialog_communication_error:I = 0x7f0e0335

.field public static final ec_registration_dialog_msg:I = 0x7f0e0336

.field public static final ec_registration_dialog_not_support_ec:I = 0x7f0e0337

.field public static final ec_registration_dialog_sub_msg:I = 0x7f0e0338

.field public static final ec_registration_dialog_title:I = 0x7f0e0339

.field public static final end_page:I = 0x7f0e033a

.field public static final enhance_text_button:I = 0x7f0e033b

.field public static final enhance_text_more_button:I = 0x7f0e033c

.field public static final enhance_text_title:I = 0x7f0e033d

.field public static final entirely_button:I = 0x7f0e033e

.field public static final epson:I = 0x7f0e033f

.field public static final epsonconnect_err_regist_remote_printer_max_size:I = 0x7f0e0340

.field public static final epsonconnect_err_regist_remote_printer_max_size_title:I = 0x7f0e0341

.field public static final epsonconnect_str_edit_printer:I = 0x7f0e0342

.field public static final epsonconnect_str_epsonconnect:I = 0x7f0e0343

.field public static final epsonconnect_str_epsonconnect_alertmessage:I = 0x7f0e0344

.field public static final epsonconnect_str_epsonconnect_usepreview:I = 0x7f0e0345

.field public static final epsonconnect_str_error_filename_too_long:I = 0x7f0e0346

.field public static final epsonconnect_str_error_filename_too_long_title:I = 0x7f0e0347

.field public static final epsonconnect_str_error_general_from_server:I = 0x7f0e0348

.field public static final epsonconnect_str_error_get_info_from_printer:I = 0x7f0e0349

.field public static final epsonconnect_str_error_get_info_from_printer_title:I = 0x7f0e034a

.field public static final epsonconnect_str_get_server_preview:I = 0x7f0e034b

.field public static final epsonconnect_str_not_supported_remote_print:I = 0x7f0e034c

.field public static final epsonconnect_str_preview_warning:I = 0x7f0e034d

.field public static final epsonconnect_str_preview_warning2:I = 0x7f0e034e

.field public static final epsonconnect_str_print_access_key:I = 0x7f0e034f

.field public static final epsonconnect_str_print_email:I = 0x7f0e0350

.field public static final epsonconnect_str_print_history:I = 0x7f0e0351

.field public static final epsonconnect_str_print_job_too_many:I = 0x7f0e0352

.field public static final epsonconnect_str_print_job_too_many_title:I = 0x7f0e0353

.field public static final epsonconnect_str_print_path_local:I = 0x7f0e0354

.field public static final epsonconnect_str_print_path_remote:I = 0x7f0e0355

.field public static final epsonconnect_str_printer_name:I = 0x7f0e0356

.field public static final epsonconnect_str_printer_name_error_empty:I = 0x7f0e0357

.field public static final epsonconnect_str_printer_name_error_over_max_length:I = 0x7f0e0358

.field public static final epsonconnect_str_printer_setting:I = 0x7f0e0359

.field public static final epsonconnect_str_regist_printer:I = 0x7f0e035a

.field public static final epsonconnect_str_remote_print_access_key_warning:I = 0x7f0e035b

.field public static final epsonconnect_str_remote_print_complete_file_upload:I = 0x7f0e035c

.field public static final epsonconnect_str_remote_print_complete_file_upload2:I = 0x7f0e035d

.field public static final epsonconnect_str_remote_print_complete_file_upload3:I = 0x7f0e035e

.field public static final epsonconnect_str_remote_print_email_address_already_registered:I = 0x7f0e035f

.field public static final epsonconnect_str_remote_print_email_address_warning:I = 0x7f0e0360

.field public static final epsonconnect_str_remote_print_file_uploading:I = 0x7f0e0361

.field public static final epsonconnect_str_remote_print_warning:I = 0x7f0e0362

.field public static final epsonconnect_str_remote_print_warning2:I = 0x7f0e0363

.field public static final epsonconnect_str_serial_no:I = 0x7f0e0364

.field public static final epsonconnect_str_status_inkinfo_error:I = 0x7f0e0365

.field public static final epsonconnect_str_status_login_error:I = 0x7f0e0366

.field public static final epsonconnect_str_status_login_error_title:I = 0x7f0e0367

.field public static final err_convert_gdocs:I = 0x7f0e0368

.field public static final err_file_size_gdocs:I = 0x7f0e0369

.field public static final err_title_file_size_gdocs:I = 0x7f0e036a

.field public static final error:I = 0x7f0e036b

.field public static final esdk_loading:I = 0x7f0e036c

.field public static final esdk_switch_to:I = 0x7f0e036d

.field public static final evernote:I = 0x7f0e036e

.field public static final execute_button:I = 0x7f0e036f

.field public static final exist_documentsize_message:I = 0x7f0e0370

.field public static final feed_direction:I = 0x7f0e0371

.field public static final file_save_error_message:I = 0x7f0e0372

.field public static final file_size_notsupport:I = 0x7f0e0373

.field public static final finish_adjustment_message:I = 0x7f0e0374

.field public static final finish_color_adjustment_message:I = 0x7f0e0375

.field public static final folder_selector_title:I = 0x7f0e0376

.field public static final fw_buttonname:I = 0x7f0e0377

.field public static final fw_checking_version:I = 0x7f0e0378

.field public static final fw_downloading:I = 0x7f0e0379

.field public static final fw_error_batStatus:I = 0x7f0e037a

.field public static final fw_error_communication:I = 0x7f0e037b

.field public static final fw_error_connect_server:I = 0x7f0e037c

.field public static final fw_error_connect_server_detail:I = 0x7f0e037d

.field public static final fw_error_interrupted:I = 0x7f0e037e

.field public static final fw_error_not_AC:I = 0x7f0e037f

.field public static final fw_error_not_enough_space:I = 0x7f0e0380

.field public static final fw_error_not_enough_space_detail:I = 0x7f0e0381

.field public static final fw_latest_version:I = 0x7f0e0382

.field public static final fw_latest_version_available:I = 0x7f0e0383

.field public static final fw_latest_version_available_detail:I = 0x7f0e0384

.field public static final fw_not_supported:I = 0x7f0e0385

.field public static final fw_not_supported_detail:I = 0x7f0e0386

.field public static final fw_transfer_complete:I = 0x7f0e0387

.field public static final fw_transfer_complete_detail:I = 0x7f0e0388

.field public static final fw_transferring:I = 0x7f0e0389

.field public static final fw_try_again:I = 0x7f0e038a

.field public static final g_close:I = 0x7f0e038b

.field public static final g_review_notification:I = 0x7f0e038c

.field public static final go_epson_servername:I = 0x7f0e038d

.field public static final google_drive:I = 0x7f0e038e

.field public static final guide_control:I = 0x7f0e038f

.field public static final guide_language:I = 0x7f0e0390

.field public static final has_no_hireso_picture_message:I = 0x7f0e0391

.field public static final hundred_percent:I = 0x7f0e0392

.field public static final image_preview_title:I = 0x7f0e0393

.field public static final image_save_message:I = 0x7f0e0394

.field public static final imgsel_view_pager_toggle_off:I = 0x7f0e0395

.field public static final imgsel_view_pager_toggle_on:I = 0x7f0e0396

.field public static final imgsel_view_pager_toggle_photo_copy:I = 0x7f0e0397

.field public static final inch_button:I = 0x7f0e0398

.field public static final install:I = 0x7f0e0399

.field public static final invalid_nfc_touch_print:I = 0x7f0e039a

.field public static final ip_can_connect:I = 0x7f0e039b

.field public static final iprint_announce_title:I = 0x7f0e039c

.field public static final layout:I = 0x7f0e039d

.field public static final lbl_scanner_search_processing:I = 0x7f0e039e

.field public static final lbl_scanner_search_result:I = 0x7f0e039f

.field public static final lbl_scanner_search_result_none:I = 0x7f0e03a0

.field public static final license_display_separation_string:I = 0x7f0e03a1

.field public static final local_memory:I = 0x7f0e03a2

.field public static final magnification:I = 0x7f0e03a3

.field public static final maintenance_advanced_settings:I = 0x7f0e03a4

.field public static final maintenance_box_info:I = 0x7f0e03a5

.field public static final manuscript_size_set_message:I = 0x7f0e03a6

.field public static final max_image:I = 0x7f0e03a7

.field public static final mcphotocopy_capacity_shortage_error_message:I = 0x7f0e03a8

.field public static final mcphotocopy_file_size_over_message:I = 0x7f0e03a9

.field public static final mcphotocopy_file_write_error_message:I = 0x7f0e03aa

.field public static final mcphotocopy_media_not_found_message:I = 0x7f0e03ab

.field public static final mcphotocopy_media_not_found_title:I = 0x7f0e03ac

.field public static final mcphotocopy_next:I = 0x7f0e03ad

.field public static final mcphotocopy_sending_message:I = 0x7f0e03ae

.field public static final mcphotocopy_top_description:I = 0x7f0e03af

.field public static final mcphotocopy_top_next:I = 0x7f0e03b0

.field public static final memcard_add_image:I = 0x7f0e03b1

.field public static final memcard_canceling_message:I = 0x7f0e03b2

.field public static final memcard_capacity_shortage_error_message:I = 0x7f0e03b3

.field public static final memcard_capacity_shortage_error_title:I = 0x7f0e03b4

.field public static final memcard_check_capacity:I = 0x7f0e03b5

.field public static final memcard_comm_error_message:I = 0x7f0e03b6

.field public static final memcard_comm_error_title:I = 0x7f0e03b7

.field public static final memcard_connecting_printer:I = 0x7f0e03b8

.field public static final memcard_copy_destination_change:I = 0x7f0e03b9

.field public static final memcard_copy_done_message:I = 0x7f0e03ba

.field public static final memcard_data_making_message:I = 0x7f0e03bb

.field public static final memcard_file_num_format:I = 0x7f0e03bc

.field public static final memcard_file_read_error_message:I = 0x7f0e03bd

.field public static final memcard_file_read_error_title:I = 0x7f0e03be

.field public static final memcard_file_reading_message:I = 0x7f0e03bf

.field public static final memcard_file_write_error_message:I = 0x7f0e03c0

.field public static final memcard_file_write_error_title:I = 0x7f0e03c1

.field public static final memcard_file_writing_message:I = 0x7f0e03c2

.field public static final memcard_folder_save_to_format:I = 0x7f0e03c3

.field public static final memcard_folder_save_to_string:I = 0x7f0e03c4

.field public static final memcard_folder_up:I = 0x7f0e03c5

.field public static final memcard_media_not_found_message:I = 0x7f0e03c6

.field public static final memcard_media_not_found_title:I = 0x7f0e03c7

.field public static final memcard_memory_remove_cation:I = 0x7f0e03c8

.field public static final memcard_no_image_selected_error_message:I = 0x7f0e03c9

.field public static final memcard_no_image_selected_error_title:I = 0x7f0e03ca

.field public static final memcard_password_incorrect_message:I = 0x7f0e03cb

.field public static final memcard_password_input_message:I = 0x7f0e03cc

.field public static final memcard_paused_message:I = 0x7f0e03cd

.field public static final memcard_printer_running_error:I = 0x7f0e03ce

.field public static final memcard_printer_save_to_format:I = 0x7f0e03cf

.field public static final memcard_progress_file:I = 0x7f0e03d0

.field public static final memcard_reader_description:I = 0x7f0e03d1

.field public static final memcard_title_copy_confirm:I = 0x7f0e03d2

.field public static final memcard_total_file_size_format:I = 0x7f0e03d3

.field public static final memcard_writer_description:I = 0x7f0e03d4

.field public static final mm_button:I = 0x7f0e03d5

.field public static final multi_layout_confirm:I = 0x7f0e03d6

.field public static final n2_Sided_Printing:I = 0x7f0e03d7

.field public static final network_error_mes:I = 0x7f0e03d8

.field public static final network_storage_title:I = 0x7f0e03d9

.field public static final new_document_size:I = 0x7f0e03da

.field public static final new_folder:I = 0x7f0e03db

.field public static final next_page_not_exist:I = 0x7f0e03dc

.field public static final next_page_ready:I = 0x7f0e03dd

.field public static final nfc_connect_erorr_wifi:I = 0x7f0e03de

.field public static final nfc_connect_error_aar:I = 0x7f0e03df

.field public static final nfc_connect_error_aar_title:I = 0x7f0e03e0

.field public static final nfc_connect_error_changing_network:I = 0x7f0e03e1

.field public static final nfc_connect_error_changing_network_title:I = 0x7f0e03e2

.field public static final nfc_connect_error_comm:I = 0x7f0e03e3

.field public static final nfc_connect_error_contact_admin:I = 0x7f0e03e4

.field public static final nfc_connect_error_failed_connect:I = 0x7f0e03e5

.field public static final nfc_connect_error_max_connected:I = 0x7f0e03e6

.field public static final nfc_connect_error_max_connected_title:I = 0x7f0e03e7

.field public static final nfc_connect_error_not_support5GHz:I = 0x7f0e03e8

.field public static final nfc_connect_error_not_support5GHz_admin:I = 0x7f0e03e9

.field public static final nfc_connect_error_power_off:I = 0x7f0e03ea

.field public static final nfc_connect_error_power_off_title:I = 0x7f0e03eb

.field public static final nfc_connect_error_simpleap_stopped_title:I = 0x7f0e03ec

.field public static final nfc_connect_error_title:I = 0x7f0e03ed

.field public static final nfc_connect_error_turnon_simpleap:I = 0x7f0e03ee

.field public static final nfc_connect_home:I = 0x7f0e03ef

.field public static final nfc_connect_printpreview:I = 0x7f0e03f0

.field public static final nfc_connect_scan:I = 0x7f0e03f1

.field public static final no_button:I = 0x7f0e03f2

.field public static final no_image:I = 0x7f0e03f3

.field public static final no_name_documentsize_message:I = 0x7f0e03f4

.field public static final not_display_next_time:I = 0x7f0e03f5

.field public static final not_select_status:I = 0x7f0e03f6

.field public static final not_support_format:I = 0x7f0e03f7

.field public static final notice_google_docs_convert:I = 0x7f0e03f8

.field public static final nozzle_check_guidance_message:I = 0x7f0e03f9

.field public static final nozzle_check_guidance_ng_text:I = 0x7f0e03fa

.field public static final nozzle_check_guidance_ok_text:I = 0x7f0e03fb

.field public static final nozzle_check_guidance_title:I = 0x7f0e03fc

.field public static final numberOfPrintersAvailable:I = 0x7f0e03fd

.field public static final ok:I = 0x7f0e03fe

.field public static final ok_button:I = 0x7f0e03ff

.field public static final onedrive:I = 0x7f0e0400

.field public static final open_in:I = 0x7f0e0401

.field public static final original_text_button:I = 0x7f0e0402

.field public static final page:I = 0x7f0e0403

.field public static final page_range:I = 0x7f0e0404

.field public static final paper_size:I = 0x7f0e0405

.field public static final paper_size_button:I = 0x7f0e0406

.field public static final paper_source:I = 0x7f0e0407

.field public static final paper_type:I = 0x7f0e0408

.field public static final papersize_10x15:I = 0x7f0e0409

.field public static final papersize_2l:I = 0x7f0e040a

.field public static final papersize_a3:I = 0x7f0e040b

.field public static final papersize_a4:I = 0x7f0e040c

.field public static final papersize_a5:I = 0x7f0e040d

.field public static final papersize_a6:I = 0x7f0e040e

.field public static final papersize_auto:I = 0x7f0e040f

.field public static final papersize_b4:I = 0x7f0e0410

.field public static final papersize_b5:I = 0x7f0e0411

.field public static final papersize_b6:I = 0x7f0e0412

.field public static final papersize_buzcard:I = 0x7f0e0413

.field public static final papersize_card:I = 0x7f0e0414

.field public static final papersize_l:I = 0x7f0e0415

.field public static final papersize_legal:I = 0x7f0e0416

.field public static final papersize_letter:I = 0x7f0e0417

.field public static final papersize_passport:I = 0x7f0e0418

.field public static final papersize_postcard:I = 0x7f0e0419

.field public static final partly_button:I = 0x7f0e041a

.field public static final password_label:I = 0x7f0e041b

.field public static final path_wifidirect_guide_panelless:I = 0x7f0e041c

.field public static final permission_dialog_message_bottom:I = 0x7f0e041d

.field public static final permission_dialog_message_camera:I = 0x7f0e041e

.field public static final permission_dialog_message_prompt_setting:I = 0x7f0e041f

.field public static final permission_dialog_message_storage:I = 0x7f0e0420

.field public static final permission_dialog_message_top:I = 0x7f0e0421

.field public static final permission_dialog_title:I = 0x7f0e0422

.field public static final permission_function_camera:I = 0x7f0e0423

.field public static final permission_function_storage:I = 0x7f0e0424

.field public static final ph_Detail:I = 0x7f0e0425

.field public static final ph_Title:I = 0x7f0e0426

.field public static final ph_cannotSet_Detail:I = 0x7f0e0427

.field public static final ph_cannotSet_Title:I = 0x7f0e0428

.field public static final photo_btn_label:I = 0x7f0e0429

.field public static final photo_select_button:I = 0x7f0e042a

.field public static final photo_select_mode:I = 0x7f0e042b

.field public static final photo_selected:I = 0x7f0e042c

.field public static final photo_zoom_mode:I = 0x7f0e042d

.field public static final photograph_button:I = 0x7f0e042e

.field public static final picture_size:I = 0x7f0e042f

.field public static final picture_size_auto:I = 0x7f0e0430

.field public static final picture_taking_option:I = 0x7f0e0431

.field public static final print_all_page:I = 0x7f0e0432

.field public static final print_button:I = 0x7f0e0433

.field public static final print_preview_button:I = 0x7f0e0434

.field public static final print_title:I = 0x7f0e0435

.field public static final printer:I = 0x7f0e0436

.field public static final printer_list_empty_message:I = 0x7f0e0437

.field public static final printer_not_found_guidance_url:I = 0x7f0e0438

.field public static final printer_not_select:I = 0x7f0e0439

.field public static final printer_notselect_title:I = 0x7f0e043a

.field public static final printer_notselect_warning:I = 0x7f0e043b

.field public static final printer_ready:I = 0x7f0e043c

.field public static final printer_with_lcd:I = 0x7f0e043d

.field public static final printer_without_lcd:I = 0x7f0e043e

.field public static final printing:I = 0x7f0e043f

.field public static final processing_image_message:I = 0x7f0e0440

.field public static final quality:I = 0x7f0e0441

.field public static final ready_ink_button_name:I = 0x7f0e0442

.field public static final readyink_invitation_description:I = 0x7f0e0443

.field public static final readyink_invitation_title:I = 0x7f0e0444

.field public static final remote_can_connect:I = 0x7f0e0445

.field public static final remoteprinter_about_remote:I = 0x7f0e0446

.field public static final remoteprinter_get_emailadress:I = 0x7f0e0447

.field public static final remoteprinter_notregister:I = 0x7f0e0448

.field public static final reset_button:I = 0x7f0e0449

.field public static final reset_color_adjustment_message:I = 0x7f0e044a

.field public static final reset_documentsize_message:I = 0x7f0e044b

.field public static final retouch_button:I = 0x7f0e044c

.field public static final review_guidance_cancel:I = 0x7f0e044d

.field public static final review_guidance_message:I = 0x7f0e044e

.field public static final review_guidance_ok:I = 0x7f0e044f

.field public static final rotate_button:I = 0x7f0e0450

.field public static final saturation:I = 0x7f0e0451

.field public static final save_button:I = 0x7f0e0452

.field public static final save_directory:I = 0x7f0e0453

.field public static final save_to_title_name:I = 0x7f0e0454

.field public static final scan_alert_and_scan_message:I = 0x7f0e0455

.field public static final scan_alert_upper_limit_message:I = 0x7f0e0456

.field public static final scan_dialog_button_cancel:I = 0x7f0e0457

.field public static final scan_dialog_button_continue:I = 0x7f0e0458

.field public static final scan_dialog_button_overwrite:I = 0x7f0e0459

.field public static final scan_dialog_message:I = 0x7f0e045a

.field public static final scan_dialog_title:I = 0x7f0e045b

.field public static final scanner_not_select:I = 0x7f0e045c

.field public static final scanner_notselect_warning:I = 0x7f0e045d

.field public static final search_menu_title:I = 0x7f0e045e

.field public static final searching_text:I = 0x7f0e045f

.field public static final select_folder:I = 0x7f0e0460

.field public static final setting_button:I = 0x7f0e0461

.field public static final sharpness_setting_title:I = 0x7f0e0462

.field public static final sign_in_btn_label:I = 0x7f0e0463

.field public static final sign_in_request:I = 0x7f0e0464

.field public static final sign_out_btn_label:I = 0x7f0e0465

.field public static final size:I = 0x7f0e0466

.field public static final size_height:I = 0x7f0e0467

.field public static final size_scale:I = 0x7f0e0468

.field public static final size_width:I = 0x7f0e0469

.field public static final start_adjustment_partly_message:I = 0x7f0e046a

.field public static final start_page:I = 0x7f0e046b

.field public static final status_bar_notification_info_overflow:I = 0x7f0e046c

.field public static final storage_item_load_more_photos:I = 0x7f0e046d

.field public static final str_advance_setting:I = 0x7f0e046e

.field public static final str_advanced_settings:I = 0x7f0e046f

.field public static final str_back:I = 0x7f0e0470

.field public static final str_blank:I = 0x7f0e0471

.field public static final str_bookmark:I = 0x7f0e0472

.field public static final str_btn_close:I = 0x7f0e0473

.field public static final str_btn_mail:I = 0x7f0e0474

.field public static final str_btn_printer_settings:I = 0x7f0e0475

.field public static final str_cancel:I = 0x7f0e0476

.field public static final str_connect_error1:I = 0x7f0e0477

.field public static final str_connect_error2:I = 0x7f0e0478

.field public static final str_connect_error3:I = 0x7f0e0479

.field public static final str_connected_otherAP1:I = 0x7f0e047a

.field public static final str_connected_otherAP2:I = 0x7f0e047b

.field public static final str_connected_otherAP3:I = 0x7f0e047c

.field public static final str_connected_printer1:I = 0x7f0e047d

.field public static final str_connected_printer2:I = 0x7f0e047e

.field public static final str_connected_printer3:I = 0x7f0e047f

.field public static final str_connecting_printer:I = 0x7f0e0480

.field public static final str_connecting_printer_p2p:I = 0x7f0e0481

.field public static final str_continue:I = 0x7f0e0482

.field public static final str_copy:I = 0x7f0e0483

.field public static final str_cur_wifi_state:I = 0x7f0e0484

.field public static final str_delete:I = 0x7f0e0485

.field public static final str_delete_warning:I = 0x7f0e0486

.field public static final str_detailtext_ip:I = 0x7f0e0487

.field public static final str_detailtext_ip_scan:I = 0x7f0e0488

.field public static final str_detailtext_local:I = 0x7f0e0489

.field public static final str_detailtext_local_scan:I = 0x7f0e048a

.field public static final str_detailtext_remote:I = 0x7f0e048b

.field public static final str_deviceInformation_BluetoothOFF:I = 0x7f0e048c

.field public static final str_deviceInformation_BluetoothON:I = 0x7f0e048d

.field public static final str_deviceInformation_BluetoothStatus:I = 0x7f0e048e

.field public static final str_deviceInformation_DeviceInformation:I = 0x7f0e048f

.field public static final str_deviceInformation_IPAddress:I = 0x7f0e0490

.field public static final str_deviceInformation_Info:I = 0x7f0e0491

.field public static final str_deviceInformation_OSVersion:I = 0x7f0e0492

.field public static final str_deviceInformation_PrinterIPAddress:I = 0x7f0e0493

.field public static final str_deviceInformation_PrinterInformation:I = 0x7f0e0494

.field public static final str_deviceInformation_PrinterName:I = 0x7f0e0495

.field public static final str_deviceInformation_PrinterStatus:I = 0x7f0e0496

.field public static final str_deviceInformation_SSIDName:I = 0x7f0e0497

.field public static final str_deviceInformation_WifiOFF:I = 0x7f0e0498

.field public static final str_deviceInformation_WifiON:I = 0x7f0e0499

.field public static final str_deviceInformation_WifiState:I = 0x7f0e049a

.field public static final str_done:I = 0x7f0e049b

.field public static final str_donot_support:I = 0x7f0e049c

.field public static final str_edit_bookmark:I = 0x7f0e049d

.field public static final str_edit_password:I = 0x7f0e049e

.field public static final str_enter_pass:I = 0x7f0e049f

.field public static final str_err_msg_out_of_memory_title:I = 0x7f0e04a0

.field public static final str_err_msg_scan_adf_paperout:I = 0x7f0e04a1

.field public static final str_err_msg_scan_adf_paperout_title:I = 0x7f0e04a2

.field public static final str_err_msg_scan_busy:I = 0x7f0e04a3

.field public static final str_err_msg_scan_busy_title:I = 0x7f0e04a4

.field public static final str_err_msg_scan_comm:I = 0x7f0e04a5

.field public static final str_err_msg_scan_comm_title:I = 0x7f0e04a6

.field public static final str_err_msg_scan_coveropen:I = 0x7f0e04a7

.field public static final str_err_msg_scan_coveropen_title:I = 0x7f0e04a8

.field public static final str_err_msg_scan_doublefeed:I = 0x7f0e04a9

.field public static final str_err_msg_scan_doublefeed_title:I = 0x7f0e04aa

.field public static final str_err_msg_scan_generic_internal:I = 0x7f0e04ab

.field public static final str_err_msg_scan_generic_internal_title:I = 0x7f0e04ac

.field public static final str_err_msg_scan_not_found:I = 0x7f0e04ad

.field public static final str_err_msg_scan_not_found_research:I = 0x7f0e04ae

.field public static final str_err_msg_scan_not_found_title:I = 0x7f0e04af

.field public static final str_err_msg_scan_paperjam:I = 0x7f0e04b0

.field public static final str_err_msg_scan_paperjam_title:I = 0x7f0e04b1

.field public static final str_err_msg_scan_paperout:I = 0x7f0e04b2

.field public static final str_err_msg_scan_paperout_title:I = 0x7f0e04b3

.field public static final str_error_Filename:I = 0x7f0e04b4

.field public static final str_error_connecting_printer:I = 0x7f0e04b5

.field public static final str_error_connecting_printer_p2p:I = 0x7f0e04b6

.field public static final str_error_connecting_printer_short:I = 0x7f0e04b7

.field public static final str_error_get_info_from_printer_title:I = 0x7f0e04b8

.field public static final str_error_ms:I = 0x7f0e04b9

.field public static final str_error_ms_print:I = 0x7f0e04ba

.field public static final str_error_tile:I = 0x7f0e04bb

.field public static final str_error_wifi_connecting_failed:I = 0x7f0e04bc

.field public static final str_error_wifi_connecting_simpleAP:I = 0x7f0e04bd

.field public static final str_executing:I = 0x7f0e04be

.field public static final str_exit_btn:I = 0x7f0e04bf

.field public static final str_file_name:I = 0x7f0e04c0

.field public static final str_file_type:I = 0x7f0e04c1

.field public static final str_goto_wifi_setting:I = 0x7f0e04c2

.field public static final str_goto_wifi_settings:I = 0x7f0e04c3

.field public static final str_goto_wifidirect_settings:I = 0x7f0e04c4

.field public static final str_head_clean:I = 0x7f0e04c5

.field public static final str_history:I = 0x7f0e04c6

.field public static final str_ipprinter_add:I = 0x7f0e04c7

.field public static final str_ipprinter_comerror:I = 0x7f0e04c8

.field public static final str_ipprinter_comerror_edit:I = 0x7f0e04c9

.field public static final str_ipprinter_edit:I = 0x7f0e04ca

.field public static final str_ipprinter_ip:I = 0x7f0e04cb

.field public static final str_ipprinter_ip_fomaterror:I = 0x7f0e04cc

.field public static final str_ipprinter_ip_null:I = 0x7f0e04cd

.field public static final str_ipprinter_registed_already:I = 0x7f0e04ce

.field public static final str_iprintconnect1:I = 0x7f0e04cf

.field public static final str_iprintconnect2:I = 0x7f0e04d0

.field public static final str_iprintconnect_start1:I = 0x7f0e04d1

.field public static final str_iprintconnect_start2:I = 0x7f0e04d2

.field public static final str_iprintconnect_start3:I = 0x7f0e04d3

.field public static final str_iprintconnect_start4:I = 0x7f0e04d4

.field public static final str_large_file_mail:I = 0x7f0e04d5

.field public static final str_lbl_title_scan:I = 0x7f0e04d6

.field public static final str_lbl_title_scan_select:I = 0x7f0e04d7

.field public static final str_lbl_title_scan_settings:I = 0x7f0e04d8

.field public static final str_load:I = 0x7f0e04d9

.field public static final str_log_in_failed:I = 0x7f0e04da

.field public static final str_maintenance:I = 0x7f0e04db

.field public static final str_msg_document_preview_viewbase:I = 0x7f0e04dd

.field public static final str_msg_scan_cancel:I = 0x7f0e04de

.field public static final str_msg_scan_in_progress:I = 0x7f0e04df

.field public static final str_msg_scan_viewbase:I = 0x7f0e04e0

.field public static final str_need_location_permission:I = 0x7f0e04e1

.field public static final str_need_location_permission_title:I = 0x7f0e04e2

.field public static final str_need_location_setting:I = 0x7f0e04e3

.field public static final str_need_location_setting_title:I = 0x7f0e04e4

.field public static final str_next:I = 0x7f0e04e5

.field public static final str_no:I = 0x7f0e04e6

.field public static final str_no_internet:I = 0x7f0e04e7

.field public static final str_no_network:I = 0x7f0e04e8

.field public static final str_no_photo:I = 0x7f0e04e9

.field public static final str_no_sdcard:I = 0x7f0e04ea

.field public static final str_no_wifi:I = 0x7f0e04eb

.field public static final str_notice_connecting_simpleAP:I = 0x7f0e04ec

.field public static final str_notice_delete_simpleAP:I = 0x7f0e04ed

.field public static final str_notice_duplicated_simpleAP:I = 0x7f0e04ee

.field public static final str_notice_wifi_connected:I = 0x7f0e04ef

.field public static final str_notice_wifi_disconnected:I = 0x7f0e04f0

.field public static final str_nozzle_check:I = 0x7f0e04f1

.field public static final str_ok:I = 0x7f0e04f2

.field public static final str_online_registration:I = 0x7f0e04f3

.field public static final str_print:I = 0x7f0e04f4

.field public static final str_print_path_ip:I = 0x7f0e04f5

.field public static final str_print_status:I = 0x7f0e04f6

.field public static final str_printer_search_result_none:I = 0x7f0e04f7

.field public static final str_privacy:I = 0x7f0e04f8

.field public static final str_remain_ink:I = 0x7f0e04f9

.field public static final str_rename:I = 0x7f0e04fa

.field public static final str_retry:I = 0x7f0e04fb

.field public static final str_save:I = 0x7f0e04fc

.field public static final str_scan:I = 0x7f0e04fd

.field public static final str_scan_btn:I = 0x7f0e04fe

.field public static final str_scanning_size:I = 0x7f0e04ff

.field public static final str_scanning_size_max:I = 0x7f0e0500

.field public static final str_search:I = 0x7f0e0501

.field public static final str_select_acc:I = 0x7f0e0502

.field public static final str_settings:I = 0x7f0e0503

.field public static final str_settings_2sided_title:I = 0x7f0e0504

.field public static final str_settings_center_aligned:I = 0x7f0e0505

.field public static final str_settings_color_color:I = 0x7f0e0506

.field public static final str_settings_color_grayscale:I = 0x7f0e0507

.field public static final str_settings_color_monochrome:I = 0x7f0e0508

.field public static final str_settings_color_title:I = 0x7f0e0509

.field public static final str_settings_gamma_10:I = 0x7f0e050a

.field public static final str_settings_gamma_18:I = 0x7f0e050b

.field public static final str_settings_gamma_title:I = 0x7f0e050c

.field public static final str_settings_lbl_scanner:I = 0x7f0e050d

.field public static final str_settings_resolution:I = 0x7f0e050e

.field public static final str_settings_resolution_title:I = 0x7f0e050f

.field public static final str_settings_right_aligned:I = 0x7f0e0510

.field public static final str_settings_source_adf:I = 0x7f0e0511

.field public static final str_settings_source_doctable:I = 0x7f0e0512

.field public static final str_settings_source_title:I = 0x7f0e0513

.field public static final str_settings_thumbnail:I = 0x7f0e0514

.field public static final str_settings_undefine_scansize:I = 0x7f0e0515

.field public static final str_total_file_size:I = 0x7f0e0516

.field public static final str_unselect:I = 0x7f0e0517

.field public static final str_use_mediastorage_thumbnail:I = 0x7f0e0518

.field public static final str_use_mediastorage_thumbnail_short:I = 0x7f0e0519

.field public static final str_user_name:I = 0x7f0e051a

.field public static final str_wait:I = 0x7f0e051b

.field public static final str_warnig:I = 0x7f0e051c

.field public static final str_webconfig_warn_message:I = 0x7f0e051d

.field public static final str_wifidirect_manual1:I = 0x7f0e051e

.field public static final str_wifidirect_manual2:I = 0x7f0e051f

.field public static final str_wifidirect_manual3:I = 0x7f0e0520

.field public static final str_wifidirect_manual4:I = 0x7f0e0521

.field public static final str_wifidirect_manual5:I = 0x7f0e0522

.field public static final str_wifidirect_manual6:I = 0x7f0e0523

.field public static final str_wifidirect_settings:I = 0x7f0e0524

.field public static final str_wifidirect_start1:I = 0x7f0e0525

.field public static final str_wifidirect_start_a:I = 0x7f0e0526

.field public static final str_wifidirect_start_b:I = 0x7f0e0527

.field public static final str_wiridirect_error1:I = 0x7f0e0528

.field public static final str_wiridirect_error2:I = 0x7f0e0529

.field public static final str_wiridirect_error3:I = 0x7f0e052a

.field public static final str_yes:I = 0x7f0e052b

.field public static final subscription_NW_err_1:I = 0x7f0e052c

.field public static final subscription_NW_err_2:I = 0x7f0e052d

.field public static final subscription_NW_wran_1:I = 0x7f0e052e

.field public static final subscription_NW_wran_2:I = 0x7f0e052f

.field public static final subscription_SERV_err_1:I = 0x7f0e0530

.field public static final subscription_SERV_err_2:I = 0x7f0e0531

.field public static final support_buy_ink:I = 0x7f0e0532

.field public static final support_faq:I = 0x7f0e0533

.field public static final support_tutorial:I = 0x7f0e0534

.field public static final title_Color:I = 0x7f0e0535

.field public static final title_addprint:I = 0x7f0e0536

.field public static final title_support:I = 0x7f0e0537

.field public static final tv_help_about:I = 0x7f0e0538

.field public static final unit_inch:I = 0x7f0e0539

.field public static final unit_mm:I = 0x7f0e053a

.field public static final unknow_error_mes:I = 0x7f0e053b

.field public static final unknow_error_title:I = 0x7f0e053c

.field public static final upload_btn_label:I = 0x7f0e053d

.field public static final upload_error_mes:I = 0x7f0e053e

.field public static final upload_error_title:I = 0x7f0e053f

.field public static final upload_google_docs_convert:I = 0x7f0e0540

.field public static final uploading_notification:I = 0x7f0e0541

.field public static final upon_error:I = 0x7f0e0542

.field public static final username_label:I = 0x7f0e0543

.field public static final using_Epson_iPrint:I = 0x7f0e0544

.field public static final waiting_2nd_page:I = 0x7f0e0545

.field public static final web_btn_label:I = 0x7f0e0546

.field public static final wifi_direct_status:I = 0x7f0e0547

.field public static final yes_button:I = 0x7f0e0548


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 4512
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

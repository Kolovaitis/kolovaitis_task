.class public Lepson/print/CustomTitleDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "CustomTitleDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/CustomTitleDialogFragment$Callback;
    }
.end annotation


# static fields
.field private static final TAG_DIALOG_ID:Ljava/lang/String; = "dialog_id"

.field private static final TAG_MESSAGE:Ljava/lang/String; = "message"

.field private static final TAG_NEGATIVE_BUTTON_STRING_ID:Ljava/lang/String; = "negative_button_string_id"

.field private static final TAG_POSITIVE_BUTTON_STRING_ID:Ljava/lang/String; = "positive_button_string_id"

.field private static final TAG_TITLE_ID:Ljava/lang/String; = "title_id"

.field private static final TAG_TITLE_STRING:Ljava/lang/String; = "title_string"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 81
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static newInstance(ILjava/lang/String;III)Lepson/print/CustomTitleDialogFragment;
    .locals 3

    .line 42
    new-instance v0, Lepson/print/CustomTitleDialogFragment;

    invoke-direct {v0}, Lepson/print/CustomTitleDialogFragment;-><init>()V

    .line 44
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "dialog_id"

    .line 45
    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string p0, "message"

    .line 46
    invoke-virtual {v1, p0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p0, "title_id"

    .line 47
    invoke-virtual {v1, p0, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string p0, "positive_button_string_id"

    .line 48
    invoke-virtual {v1, p0, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string p0, "negative_button_string_id"

    .line 49
    invoke-virtual {v1, p0, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 50
    invoke-virtual {v0, v1}, Lepson/print/CustomTitleDialogFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v0
.end method

.method public static newInstance(ILjava/lang/String;Ljava/lang/String;II)Lepson/print/CustomTitleDialogFragment;
    .locals 3

    .line 65
    new-instance v0, Lepson/print/CustomTitleDialogFragment;

    invoke-direct {v0}, Lepson/print/CustomTitleDialogFragment;-><init>()V

    .line 67
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "dialog_id"

    .line 68
    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string p0, "message"

    .line 69
    invoke-virtual {v1, p0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p0, "title_string"

    .line 70
    invoke-virtual {v1, p0, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p0, "positive_button_string_id"

    .line 71
    invoke-virtual {v1, p0, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string p0, "negative_button_string_id"

    .line 72
    invoke-virtual {v1, p0, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 73
    invoke-virtual {v0, v1}, Lepson/print/CustomTitleDialogFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 8

    .line 87
    invoke-virtual {p0}, Lepson/print/CustomTitleDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "dialog_id"

    .line 88
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    const-string v1, "message"

    .line 89
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "title_id"

    .line 90
    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    const-string v3, "title_string"

    .line 91
    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "positive_button_string_id"

    .line 92
    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    const-string v5, "negative_button_string_id"

    .line 93
    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    .line 95
    invoke-virtual {p0}, Lepson/print/CustomTitleDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    check-cast v5, Lepson/print/CustomTitleDialogFragment$Callback;

    .line 97
    move-object v6, v5

    check-cast v6, Landroid/app/Activity;

    invoke-static {v6}, Lepson/common/Utils;->getCustomTitleDialogBuilder(Landroid/content/Context;)Lepson/print/widgets/CustomTitleAlertDialogBuilder;

    move-result-object v6

    .line 98
    invoke-virtual {v6, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v7, Lepson/print/CustomTitleDialogFragment$1;

    invoke-direct {v7, p0, v5, v0}, Lepson/print/CustomTitleDialogFragment$1;-><init>(Lepson/print/CustomTitleDialogFragment;Lepson/print/CustomTitleDialogFragment$Callback;I)V

    .line 99
    invoke-virtual {v1, v4, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    if-eqz p1, :cond_0

    .line 107
    new-instance v1, Lepson/print/CustomTitleDialogFragment$2;

    invoke-direct {v1, p0, v5, v0}, Lepson/print/CustomTitleDialogFragment$2;-><init>(Lepson/print/CustomTitleDialogFragment;Lepson/print/CustomTitleDialogFragment$Callback;I)V

    invoke-virtual {v6, p1, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    :cond_0
    if-eqz v2, :cond_1

    .line 117
    invoke-virtual {v6, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    :cond_1
    if-eqz v3, :cond_2

    .line 119
    invoke-virtual {v6, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 122
    :cond_2
    :goto_0
    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    const/4 v0, 0x0

    .line 123
    invoke-virtual {p1, v0}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    return-object p1
.end method

.class Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewNext;
.super Lepson/print/Util/AsyncTaskExecutor;
.source "ActivityDocsPrintPreview.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/ActivityDocsPrintPreview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadRemotePreviewNext"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lepson/print/Util/AsyncTaskExecutor<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/ActivityDocsPrintPreview;


# direct methods
.method private constructor <init>(Lepson/print/ActivityDocsPrintPreview;)V
    .locals 0

    .line 2384
    iput-object p1, p0, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewNext;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-direct {p0}, Lepson/print/Util/AsyncTaskExecutor;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lepson/print/ActivityDocsPrintPreview;Lepson/print/ActivityDocsPrintPreview$1;)V
    .locals 0

    .line 2384
    invoke-direct {p0, p1}, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewNext;-><init>(Lepson/print/ActivityDocsPrintPreview;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 2384
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewNext;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 9

    .line 2390
    invoke-static {}, Lepson/print/ActivityDocsPrintPreview;->access$4400()Ljava/lang/Object;

    move-result-object p1

    monitor-enter p1

    .line 2392
    :try_start_0
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewNext;->this$0:Lepson/print/ActivityDocsPrintPreview;

    iget-object v0, v0, Lepson/print/ActivityDocsPrintPreview;->mUiHandler:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 2396
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewNext;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v0}, Lepson/print/ActivityDocsPrintPreview;->access$600(Lepson/print/ActivityDocsPrintPreview;)I

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x1

    add-int/2addr v0, v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 2399
    :try_start_1
    new-instance v4, Ljava/io/File;

    iget-object v5, p0, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewNext;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v5}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v5

    invoke-virtual {v5}, Lepson/common/ExternalFileUtils;->getTempViewDir()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "preview_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v7, ".jpg"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2400
    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    .line 2401
    iget-object v5, p0, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewNext;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v5}, Lepson/print/ActivityDocsPrintPreview;->access$800(Lepson/print/ActivityDocsPrintPreview;)Lepson/print/service/IEpsonService;

    move-result-object v5

    invoke-interface {v5, v0, v4}, Lepson/print/service/IEpsonService;->EpsonConnectGetPreview(ILjava/lang/String;)I

    move-result v5
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v5, :cond_2

    .line 2408
    :try_start_2
    iget-object v6, p0, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewNext;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v6}, Lepson/print/ActivityDocsPrintPreview;->access$4300(Lepson/print/ActivityDocsPrintPreview;)Lepson/print/EPImageList;

    move-result-object v6

    invoke-virtual {v6, v4}, Lepson/print/EPImageList;->add(Ljava/lang/String;)Z

    const/4 v6, 0x2

    .line 2412
    new-array v7, v6, [I

    .line 2413
    iget-object v8, p0, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewNext;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v8}, Lepson/print/ActivityDocsPrintPreview;->access$800(Lepson/print/ActivityDocsPrintPreview;)Lepson/print/service/IEpsonService;

    move-result-object v8

    invoke-interface {v8, v7}, Lepson/print/service/IEpsonService;->EpsonConnectGetRenderingStatus([I)I

    move-result v5

    if-nez v5, :cond_1

    .line 2417
    iget-object v8, p0, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewNext;->this$0:Lepson/print/ActivityDocsPrintPreview;

    aget v1, v7, v1

    invoke-static {v8, v1}, Lepson/print/ActivityDocsPrintPreview;->access$702(Lepson/print/ActivityDocsPrintPreview;I)I

    .line 2420
    aget v1, v7, v3

    if-ne v1, v6, :cond_0

    .line 2421
    iget-object v1, p0, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewNext;->this$0:Lepson/print/ActivityDocsPrintPreview;

    iget-object v1, v1, Lepson/print/ActivityDocsPrintPreview;->mUiHandler:Landroid/os/Handler;

    const/16 v3, 0x20

    invoke-virtual {v1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 2425
    :cond_0
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    const/16 v3, 0x1f

    .line 2426
    iput v3, v1, Landroid/os/Message;->what:I

    .line 2427
    invoke-virtual {v1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    const-string v6, "CONVERTED_PAGE"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2428
    iput-object v4, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 2430
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewNext;->this$0:Lepson/print/ActivityDocsPrintPreview;

    iget-object v0, v0, Lepson/print/ActivityDocsPrintPreview;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 2415
    :cond_1
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    throw v0

    .line 2404
    :cond_2
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    throw v0
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catch_0
    const/4 v5, 0x0

    .line 2436
    :catch_1
    :try_start_3
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    const/16 v1, 0x27

    .line 2437
    iput v1, v0, Landroid/os/Message;->what:I

    .line 2438
    iput v5, v0, Landroid/os/Message;->arg1:I

    .line 2439
    iget-object v1, p0, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewNext;->this$0:Lepson/print/ActivityDocsPrintPreview;

    iget-object v1, v1, Lepson/print/ActivityDocsPrintPreview;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 2440
    monitor-exit p1

    return-object v2

    :catch_2
    move-exception v0

    .line 2433
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 2442
    :goto_0
    monitor-exit p1

    return-object v2

    :catchall_0
    move-exception v0

    monitor-exit p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0
.end method

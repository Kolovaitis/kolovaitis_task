.class public Lepson/print/WebviewActivity;
.super Lepson/print/ActivityIACommon;
.source "WebviewActivity.java"

# interfaces
.implements Lepson/print/CommonDefine;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/WebviewActivity$ImageCreateThread;,
        Lepson/print/WebviewActivity$InteruptChecker;,
        Lepson/print/WebviewActivity$ImageFileCreator;,
        Lepson/print/WebviewActivity$CustomProDialog;
    }
.end annotation


# static fields
.field private static final ACTION_PRINT_WEB:I = 0x2

.field private static final FIRST_SCREEN_SCALE_VALUE:F = 0.0f

.field private static final MESSAGE_WAIT_WEBVIEW_PROGRESS:I = 0x3

.field private static final Menu_Add_Bookmark:I = 0x4

.field private static final Menu_Back:I = 0x1

.field private static final Menu_Bookmark:I = 0x6

.field private static final Menu_History:I = 0x5

.field private static final Menu_Next:I = 0x2

.field private static final Menu_Print:I = 0x7

.field public static final PARAMS_KEY_WEBVIEW_PRINT_LOG:Ljava/lang/String; = "print_log"

.field private static final REQUEST_CODE_LICENSE_CHECK:I = 0x3

.field protected static final TAG:Ljava/lang/String; = "WebviewActivity"

.field private static final WEBVIEW_MENU_GROUP:I


# instance fields
.field private final ERROR_CODE:I

.field private final SDCARD_ERROR:I

.field private btnClearUrl:Landroid/widget/ImageButton;

.field private captureStart:Z

.field private checkImage:Z

.field private checkSharepage:Z

.field private countUrl:I

.field private inURL:Ljava/lang/String;

.field isBuy:Z

.field private isCustomAction:Z

.field isFaq:Z

.field isInfo:Z

.field private mAddImageView:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mCanClear:Z

.field private mCanRefresh:Z

.field private mCanStop:Z

.field private mCustomPro:Landroid/app/Dialog;

.field public mErrorDialog:Landroid/app/AlertDialog;

.field private mImageArray:[Ljava/lang/Integer;

.field private mImageCreateThread:Lepson/print/WebviewActivity$ImageCreateThread;

.field private mIntent:Landroid/content/Intent;

.field mIsBookmark:Z

.field mIsBookmarkEmpty:Z

.field private mIsFinish:Z

.field mIsFistLoad:Z

.field mIsLoadError:Z

.field private mItem:[Landroid/view/MenuItem;

.field private mLastHeight:I

.field private mListUrl:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mLoadWebLayout:Landroid/widget/LinearLayout;

.field private volatile mPreviewWebInterrupted:Z

.field private mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;

.field mStrictMode:Lepson/print/IprintStrictMode$StrictModeInterface;

.field private mWebClient:Landroid/webkit/WebViewClient;

.field private mWebCromClient:Landroid/webkit/WebChromeClient;

.field private mWebview:Landroid/webkit/WebView;

.field myHandler:Landroid/os/Handler;

.field private onScaleChangedCurrent:F

.field private previewImageTask:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private stateEditor:Landroid/content/SharedPreferences$Editor;

.field private statePre:Landroid/content/SharedPreferences;

.field private strUrlIn:Ljava/lang/String;

.field private teInurl:Landroid/widget/EditText;


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 72
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    const/4 v0, 0x1

    .line 77
    iput v0, p0, Lepson/print/WebviewActivity;->ERROR_CODE:I

    const/4 v0, 0x2

    .line 78
    iput v0, p0, Lepson/print/WebviewActivity;->SDCARD_ERROR:I

    const/4 v1, 0x0

    .line 94
    iput-object v1, p0, Lepson/print/WebviewActivity;->inURL:Ljava/lang/String;

    const/4 v1, 0x0

    .line 97
    iput-boolean v1, p0, Lepson/print/WebviewActivity;->checkSharepage:Z

    .line 98
    iput-boolean v1, p0, Lepson/print/WebviewActivity;->checkImage:Z

    .line 102
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lepson/print/WebviewActivity;->mAddImageView:Ljava/util/ArrayList;

    const/4 v2, 0x6

    .line 106
    new-array v2, v2, [Landroid/view/MenuItem;

    iput-object v2, p0, Lepson/print/WebviewActivity;->mItem:[Landroid/view/MenuItem;

    .line 107
    new-array v0, v0, [Ljava/lang/Integer;

    iput-object v0, p0, Lepson/print/WebviewActivity;->mImageArray:[Ljava/lang/Integer;

    .line 108
    iput-boolean v1, p0, Lepson/print/WebviewActivity;->isFaq:Z

    iput-boolean v1, p0, Lepson/print/WebviewActivity;->isBuy:Z

    iput-boolean v1, p0, Lepson/print/WebviewActivity;->isInfo:Z

    iput-boolean v1, p0, Lepson/print/WebviewActivity;->mIsLoadError:Z

    .line 119
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lepson/print/WebviewActivity;->mListUrl:Ljava/util/ArrayList;

    .line 122
    iput-boolean v1, p0, Lepson/print/WebviewActivity;->isCustomAction:Z

    .line 126
    iput-boolean v1, p0, Lepson/print/WebviewActivity;->captureStart:Z

    .line 128
    iput v1, p0, Lepson/print/WebviewActivity;->mLastHeight:I

    const/4 v0, 0x0

    .line 129
    iput v0, p0, Lepson/print/WebviewActivity;->onScaleChangedCurrent:F

    .line 363
    new-instance v0, Lepson/print/WebviewActivity$3;

    invoke-direct {v0, p0}, Lepson/print/WebviewActivity$3;-><init>(Lepson/print/WebviewActivity;)V

    iput-object v0, p0, Lepson/print/WebviewActivity;->mWebClient:Landroid/webkit/WebViewClient;

    .line 561
    new-instance v0, Lepson/print/WebviewActivity$4;

    invoke-direct {v0, p0}, Lepson/print/WebviewActivity$4;-><init>(Lepson/print/WebviewActivity;)V

    iput-object v0, p0, Lepson/print/WebviewActivity;->mWebCromClient:Landroid/webkit/WebChromeClient;

    .line 635
    new-instance v0, Lepson/print/WebviewActivity$5;

    invoke-direct {v0, p0}, Lepson/print/WebviewActivity$5;-><init>(Lepson/print/WebviewActivity;)V

    iput-object v0, p0, Lepson/print/WebviewActivity;->myHandler:Landroid/os/Handler;

    .line 1499
    invoke-static {}, Lepson/print/IprintStrictMode;->getStrictMode()Lepson/print/IprintStrictMode$StrictModeInterface;

    move-result-object v0

    iput-object v0, p0, Lepson/print/WebviewActivity;->mStrictMode:Lepson/print/IprintStrictMode$StrictModeInterface;

    return-void
.end method

.method static synthetic access$002(Lepson/print/WebviewActivity;Z)Z
    .locals 0

    .line 72
    iput-boolean p1, p0, Lepson/print/WebviewActivity;->mCanRefresh:Z

    return p1
.end method

.method static synthetic access$1000(Lepson/print/WebviewActivity;)Ljava/lang/String;
    .locals 0

    .line 72
    iget-object p0, p0, Lepson/print/WebviewActivity;->inURL:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1002(Lepson/print/WebviewActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 72
    iput-object p1, p0, Lepson/print/WebviewActivity;->inURL:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$102(Lepson/print/WebviewActivity;Z)Z
    .locals 0

    .line 72
    iput-boolean p1, p0, Lepson/print/WebviewActivity;->mCanClear:Z

    return p1
.end method

.method static synthetic access$1100(Lepson/print/WebviewActivity;)Landroid/content/SharedPreferences;
    .locals 0

    .line 72
    iget-object p0, p0, Lepson/print/WebviewActivity;->statePre:Landroid/content/SharedPreferences;

    return-object p0
.end method

.method static synthetic access$1102(Lepson/print/WebviewActivity;Landroid/content/SharedPreferences;)Landroid/content/SharedPreferences;
    .locals 0

    .line 72
    iput-object p1, p0, Lepson/print/WebviewActivity;->statePre:Landroid/content/SharedPreferences;

    return-object p1
.end method

.method static synthetic access$1200(Lepson/print/WebviewActivity;)Ljava/util/ArrayList;
    .locals 0

    .line 72
    iget-object p0, p0, Lepson/print/WebviewActivity;->mListUrl:Ljava/util/ArrayList;

    return-object p0
.end method

.method static synthetic access$1300(Lepson/print/WebviewActivity;)I
    .locals 0

    .line 72
    iget p0, p0, Lepson/print/WebviewActivity;->countUrl:I

    return p0
.end method

.method static synthetic access$1302(Lepson/print/WebviewActivity;I)I
    .locals 0

    .line 72
    iput p1, p0, Lepson/print/WebviewActivity;->countUrl:I

    return p1
.end method

.method static synthetic access$1308(Lepson/print/WebviewActivity;)I
    .locals 2

    .line 72
    iget v0, p0, Lepson/print/WebviewActivity;->countUrl:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lepson/print/WebviewActivity;->countUrl:I

    return v0
.end method

.method static synthetic access$1400(Lepson/print/WebviewActivity;)Landroid/content/SharedPreferences$Editor;
    .locals 0

    .line 72
    iget-object p0, p0, Lepson/print/WebviewActivity;->stateEditor:Landroid/content/SharedPreferences$Editor;

    return-object p0
.end method

.method static synthetic access$1402(Lepson/print/WebviewActivity;Landroid/content/SharedPreferences$Editor;)Landroid/content/SharedPreferences$Editor;
    .locals 0

    .line 72
    iput-object p1, p0, Lepson/print/WebviewActivity;->stateEditor:Landroid/content/SharedPreferences$Editor;

    return-object p1
.end method

.method static synthetic access$1500(Lepson/print/WebviewActivity;)Z
    .locals 0

    .line 72
    iget-boolean p0, p0, Lepson/print/WebviewActivity;->captureStart:Z

    return p0
.end method

.method static synthetic access$1600(Lepson/print/WebviewActivity;)V
    .locals 0

    .line 72
    invoke-direct {p0}, Lepson/print/WebviewActivity;->previewWeb()V

    return-void
.end method

.method static synthetic access$1700(Lepson/print/WebviewActivity;Ljava/lang/String;)Landroid/app/AlertDialog;
    .locals 0

    .line 72
    invoke-direct {p0, p1}, Lepson/print/WebviewActivity;->createDialog(Ljava/lang/String;)Landroid/app/AlertDialog;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$1802(Lepson/print/WebviewActivity;F)F
    .locals 0

    .line 72
    iput p1, p0, Lepson/print/WebviewActivity;->onScaleChangedCurrent:F

    return p1
.end method

.method static synthetic access$1900(Lepson/print/WebviewActivity;)Z
    .locals 0

    .line 72
    iget-boolean p0, p0, Lepson/print/WebviewActivity;->mPreviewWebInterrupted:Z

    return p0
.end method

.method static synthetic access$200(Lepson/print/WebviewActivity;)Landroid/widget/EditText;
    .locals 0

    .line 72
    iget-object p0, p0, Lepson/print/WebviewActivity;->teInurl:Landroid/widget/EditText;

    return-object p0
.end method

.method static synthetic access$2000(Lepson/print/WebviewActivity;)F
    .locals 0

    .line 72
    invoke-direct {p0}, Lepson/print/WebviewActivity;->getScaleValue()F

    move-result p0

    return p0
.end method

.method static synthetic access$2100(Lepson/print/WebviewActivity;)I
    .locals 0

    .line 72
    iget p0, p0, Lepson/print/WebviewActivity;->mLastHeight:I

    return p0
.end method

.method static synthetic access$2102(Lepson/print/WebviewActivity;I)I
    .locals 0

    .line 72
    iput p1, p0, Lepson/print/WebviewActivity;->mLastHeight:I

    return p1
.end method

.method static synthetic access$2200(Lepson/print/WebviewActivity;)V
    .locals 0

    .line 72
    invoke-direct {p0}, Lepson/print/WebviewActivity;->startPreviewWebThread()V

    return-void
.end method

.method static synthetic access$2300(Landroid/graphics/Bitmap;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 72
    invoke-static {p0, p1}, Lepson/print/WebviewActivity;->saveJPG(Landroid/graphics/Bitmap;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$2400(Lepson/print/WebviewActivity;)Ljava/util/ArrayList;
    .locals 0

    .line 72
    iget-object p0, p0, Lepson/print/WebviewActivity;->mAddImageView:Ljava/util/ArrayList;

    return-object p0
.end method

.method static synthetic access$2500(Lepson/print/WebviewActivity;)V
    .locals 0

    .line 72
    invoke-direct {p0}, Lepson/print/WebviewActivity;->startPrintActivity()V

    return-void
.end method

.method static synthetic access$2600(Lepson/print/WebviewActivity;)Z
    .locals 0

    .line 72
    iget-boolean p0, p0, Lepson/print/WebviewActivity;->checkImage:Z

    return p0
.end method

.method static synthetic access$2700(Lepson/print/WebviewActivity;)Landroid/content/Intent;
    .locals 0

    .line 72
    iget-object p0, p0, Lepson/print/WebviewActivity;->mIntent:Landroid/content/Intent;

    return-object p0
.end method

.method static synthetic access$2702(Lepson/print/WebviewActivity;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 0

    .line 72
    iput-object p1, p0, Lepson/print/WebviewActivity;->mIntent:Landroid/content/Intent;

    return-object p1
.end method

.method static synthetic access$2800(Lepson/print/WebviewActivity;)Lcom/epson/iprint/prtlogger/PrintLog;
    .locals 0

    .line 72
    invoke-direct {p0}, Lepson/print/WebviewActivity;->getPrintLog()Lcom/epson/iprint/prtlogger/PrintLog;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$300(Lepson/print/WebviewActivity;)Landroid/webkit/WebView;
    .locals 0

    .line 72
    iget-object p0, p0, Lepson/print/WebviewActivity;->mWebview:Landroid/webkit/WebView;

    return-object p0
.end method

.method static synthetic access$400(Lepson/print/WebviewActivity;)Z
    .locals 0

    .line 72
    iget-boolean p0, p0, Lepson/print/WebviewActivity;->checkSharepage:Z

    return p0
.end method

.method static synthetic access$402(Lepson/print/WebviewActivity;Z)Z
    .locals 0

    .line 72
    iput-boolean p1, p0, Lepson/print/WebviewActivity;->checkSharepage:Z

    return p1
.end method

.method static synthetic access$500(Lepson/print/WebviewActivity;)Landroid/widget/LinearLayout;
    .locals 0

    .line 72
    iget-object p0, p0, Lepson/print/WebviewActivity;->mLoadWebLayout:Landroid/widget/LinearLayout;

    return-object p0
.end method

.method static synthetic access$600(Lepson/print/WebviewActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 72
    invoke-direct {p0, p1}, Lepson/print/WebviewActivity;->fixUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$702(Lepson/print/WebviewActivity;Z)Z
    .locals 0

    .line 72
    iput-boolean p1, p0, Lepson/print/WebviewActivity;->mIsFinish:Z

    return p1
.end method

.method static synthetic access$802(Lepson/print/WebviewActivity;Z)Z
    .locals 0

    .line 72
    iput-boolean p1, p0, Lepson/print/WebviewActivity;->mCanStop:Z

    return p1
.end method

.method static synthetic access$902(Lepson/print/WebviewActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 72
    iput-object p1, p0, Lepson/print/WebviewActivity;->strUrlIn:Ljava/lang/String;

    return-object p1
.end method

.method private canGoBack()Z
    .locals 2

    .line 1481
    iget-object v0, p0, Lepson/print/WebviewActivity;->strUrlIn:Ljava/lang/String;

    invoke-direct {p0, v0}, Lepson/print/WebviewActivity;->getIndexOfLink(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    return v1

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private canGoForward()Z
    .locals 2

    .line 1491
    iget-object v0, p0, Lepson/print/WebviewActivity;->strUrlIn:Ljava/lang/String;

    invoke-direct {p0, v0}, Lepson/print/WebviewActivity;->getIndexOfLink(Ljava/lang/String;)I

    move-result v0

    iget-object v1, p0, Lepson/print/WebviewActivity;->mListUrl:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private createDialog(Ljava/lang/String;)Landroid/app/AlertDialog;
    .locals 3

    .line 1221
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    const/4 v1, 0x0

    .line 1222
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setCancelable(Z)V

    const v1, 0x7f0700ad

    .line 1223
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setIcon(I)V

    const v1, 0x7f0e053c

    .line 1224
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setTitle(I)V

    .line 1225
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    const v1, 0x7f0e04f2

    .line 1226
    invoke-virtual {p0, v1}, Lepson/print/WebviewActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lepson/print/WebviewActivity$6;

    invoke-direct {v2, p0, p1}, Lepson/print/WebviewActivity$6;-><init>(Lepson/print/WebviewActivity;Ljava/lang/String;)V

    const/4 p1, -0x1

    invoke-virtual {v0, p1, v1, v2}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 1236
    new-instance p1, Lepson/print/WebviewActivity$7;

    invoke-direct {p1, p0}, Lepson/print/WebviewActivity$7;-><init>(Lepson/print/WebviewActivity;)V

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    return-object v0
.end method

.method private fixUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    const/16 v0, 0x3a

    .line 1360
    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    move-object v2, p1

    const/4 p1, 0x0

    const/4 v3, 0x1

    :goto_0
    if-ge p1, v0, :cond_2

    .line 1363
    invoke-virtual {v2, p1}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 1364
    invoke-static {v4}, Ljava/lang/Character;->isLetter(C)Z

    move-result v5

    if-nez v5, :cond_0

    goto :goto_1

    .line 1367
    :cond_0
    invoke-static {v4}, Ljava/lang/Character;->isLowerCase(C)Z

    move-result v4

    and-int/2addr v3, v4

    add-int/lit8 v4, v0, -0x1

    if-ne p1, v4, :cond_1

    if-nez v3, :cond_1

    .line 1369
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_1
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    const-string p1, "http://"

    .line 1372
    invoke-virtual {v2, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_9

    const-string p1, "https://"

    invoke-virtual {v2, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_3

    goto :goto_4

    :cond_3
    const-string p1, "http:"

    .line 1375
    invoke-virtual {v2, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_4

    const-string p1, "https:"

    invoke-virtual {v2, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_7

    :cond_4
    const-string p1, "http:/"

    .line 1376
    invoke-virtual {v2, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_6

    const-string p1, "https:/"

    invoke-virtual {v2, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_5

    goto :goto_2

    :cond_5
    const-string p1, ":"

    const-string v0, "://"

    .line 1379
    invoke-virtual {v2, p1, v0}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    :cond_6
    :goto_2
    const-string p1, "/"

    const-string v0, "//"

    .line 1377
    invoke-virtual {v2, p1, v0}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :cond_7
    :goto_3
    const-string p1, "http://"

    .line 1381
    invoke-virtual {v2, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_8

    const-string p1, "file://"

    .line 1382
    invoke-virtual {v2, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_8

    .line 1383
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "http://"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1386
    :cond_8
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_9
    :goto_4
    return-object v2
.end method

.method private getIndexOfLink(Ljava/lang/String;)I
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 1453
    :goto_0
    iget-object v2, p0, Lepson/print/WebviewActivity;->mListUrl:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 1454
    iget-object v2, p0, Lepson/print/WebviewActivity;->mListUrl:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/HashMap;

    const-string v3, "value"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1455
    iget-object v1, p0, Lepson/print/WebviewActivity;->mListUrl:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashMap;

    const-string v2, "key"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return v1
.end method

.method private getLinkOfIndex(I)Ljava/lang/String;
    .locals 4

    const-string v0, ""

    const/4 v1, 0x0

    .line 1469
    :goto_0
    iget-object v2, p0, Lepson/print/WebviewActivity;->mListUrl:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 1470
    iget-object v2, p0, Lepson/print/WebviewActivity;->mListUrl:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/HashMap;

    const-string v3, "key"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    if-ne v2, p1, :cond_0

    .line 1471
    iget-object v0, p0, Lepson/print/WebviewActivity;->mListUrl:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    const-string v2, "value"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method private getPrintLog()Lcom/epson/iprint/prtlogger/PrintLog;
    .locals 2

    .line 867
    iget-object v0, p0, Lepson/print/WebviewActivity;->mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;

    if-nez v0, :cond_0

    .line 869
    new-instance v0, Lcom/epson/iprint/prtlogger/PrintLog;

    invoke-direct {v0}, Lcom/epson/iprint/prtlogger/PrintLog;-><init>()V

    .line 872
    :cond_0
    iget v1, v0, Lcom/epson/iprint/prtlogger/PrintLog;->uiRoute:I

    if-gtz v1, :cond_1

    const/4 v1, 0x3

    .line 873
    iput v1, v0, Lcom/epson/iprint/prtlogger/PrintLog;->uiRoute:I

    .line 876
    :cond_1
    iget-object v1, v0, Lcom/epson/iprint/prtlogger/PrintLog;->originalFileExtension:Ljava/lang/String;

    if-nez v1, :cond_2

    const-string v1, "http"

    .line 877
    iput-object v1, v0, Lcom/epson/iprint/prtlogger/PrintLog;->originalFileExtension:Ljava/lang/String;

    :cond_2
    return-object v0
.end method

.method private getScaleValue()F
    .locals 2

    .line 1124
    iget v0, p0, Lepson/print/WebviewActivity;->onScaleChangedCurrent:F

    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-nez v1, :cond_0

    .line 1131
    invoke-virtual {p0}, Lepson/print/WebviewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    :cond_0
    return v0
.end method

.method private hideSoftKeyboard()V
    .locals 3

    const-string v0, "input_method"

    .line 596
    invoke-virtual {p0, v0}, Lepson/print/WebviewActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 597
    iget-object v1, p0, Lepson/print/WebviewActivity;->teInurl:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    return-void
.end method

.method private loadInputUrl()V
    .locals 2

    .line 580
    invoke-direct {p0}, Lepson/print/WebviewActivity;->hideSoftKeyboard()V

    .line 581
    iget-object v0, p0, Lepson/print/WebviewActivity;->strUrlIn:Ljava/lang/String;

    if-eqz v0, :cond_1

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_1

    .line 582
    iget-object v0, p0, Lepson/print/WebviewActivity;->strUrlIn:Ljava/lang/String;

    iget-object v1, p0, Lepson/print/WebviewActivity;->mWebview:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 583
    iget-object v0, p0, Lepson/print/WebviewActivity;->strUrlIn:Ljava/lang/String;

    invoke-direct {p0, v0}, Lepson/print/WebviewActivity;->fixUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepson/print/WebviewActivity;->strUrlIn:Ljava/lang/String;

    .line 584
    iget-object v0, p0, Lepson/print/WebviewActivity;->strUrlIn:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lepson/print/WebviewActivity;->setTextOnUrlEditText(Ljava/lang/String;)V

    .line 585
    iget-object v0, p0, Lepson/print/WebviewActivity;->mWebview:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->reload()V

    goto :goto_0

    .line 587
    :cond_0
    iget-object v0, p0, Lepson/print/WebviewActivity;->strUrlIn:Ljava/lang/String;

    invoke-direct {p0, v0}, Lepson/print/WebviewActivity;->fixUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepson/print/WebviewActivity;->strUrlIn:Ljava/lang/String;

    .line 588
    iget-object v0, p0, Lepson/print/WebviewActivity;->strUrlIn:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lepson/print/WebviewActivity;->setTextOnUrlEditText(Ljava/lang/String;)V

    .line 589
    iget-object v0, p0, Lepson/print/WebviewActivity;->strUrlIn:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lepson/print/WebviewActivity;->loadUrl(Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void
.end method

.method private onInit()V
    .locals 1

    .line 233
    invoke-virtual {p0}, Lepson/print/WebviewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 234
    invoke-virtual {p0, v0}, Lepson/print/WebviewActivity;->configureFromIntent(Landroid/content/Intent;)V

    return-void
.end method

.method private previewWeb()V
    .locals 2

    const/4 v0, 0x0

    .line 1055
    iput-boolean v0, p0, Lepson/print/WebviewActivity;->mPreviewWebInterrupted:Z

    const/4 v0, 0x1

    .line 1056
    iput-boolean v0, p0, Lepson/print/WebviewActivity;->captureStart:Z

    .line 1057
    invoke-virtual {p0}, Lepson/print/WebviewActivity;->showLoadingDialog()V

    .line 1060
    iget-boolean v0, p0, Lepson/print/WebviewActivity;->checkSharepage:Z

    if-eqz v0, :cond_0

    .line 1061
    iget-object v0, p0, Lepson/print/WebviewActivity;->myHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 1063
    :cond_0
    invoke-direct {p0}, Lepson/print/WebviewActivity;->startPreviewWebThread()V

    :goto_0
    return-void
.end method

.method private static saveJPG(Landroid/graphics/Bitmap;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    .line 741
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 742
    :try_start_1
    sget-object p1, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v0, 0x64

    invoke-virtual {p0, p1, v0, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 745
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    return-void

    :catchall_0
    move-exception p0

    move-object v0, v1

    goto :goto_0

    :catchall_1
    move-exception p0

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V

    :cond_0
    throw p0
.end method

.method private startLicenseCheckActivity()V
    .locals 3

    .line 225
    invoke-virtual {p0}, Lepson/print/WebviewActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lepson/common/IprintLicenseInfo;->beforeLicenseCheck(Landroid/content/Context;)V

    .line 226
    invoke-virtual {p0}, Lepson/print/WebviewActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lepson/common/IprintLicenseInfo;

    invoke-direct {v1}, Lepson/common/IprintLicenseInfo;-><init>()V

    new-instance v2, Lepson/common/IprintUserSurveyInfo;

    invoke-direct {v2}, Lepson/common/IprintUserSurveyInfo;-><init>()V

    invoke-static {v0, v1, v2}, Lcom/epson/mobilephone/common/license/LicenseTopActivity;->getStartIntent(Landroid/content/Context;Lcom/epson/mobilephone/common/license/LicenseInfo;Lcom/epson/mobilephone/common/license/UserSurveyInfo;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x3

    .line 228
    invoke-virtual {p0, v0, v1}, Lepson/print/WebviewActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private startPreviewWebThread()V
    .locals 5

    .line 1089
    iget-object v0, p0, Lepson/print/WebviewActivity;->mAddImageView:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1096
    invoke-direct {p0}, Lepson/print/WebviewActivity;->getScaleValue()F

    move-result v0

    .line 1098
    new-instance v1, Landroid/graphics/Rect;

    iget-object v2, p0, Lepson/print/WebviewActivity;->mWebview:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getWidth()I

    move-result v2

    iget-object v3, p0, Lepson/print/WebviewActivity;->mWebview:Landroid/webkit/WebView;

    invoke-virtual {v3}, Landroid/webkit/WebView;->getContentHeight()I

    move-result v3

    int-to-float v3, v3

    mul-float v3, v3, v0

    float-to-int v3, v3

    const/4 v4, 0x0

    invoke-direct {v1, v4, v4, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    const-string v2, "WebviewActivity"

    .line 1099
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "picture Rect = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Landroid/graphics/Rect;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " currentScale = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1100
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v0

    if-lez v0, :cond_1

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v0

    if-gtz v0, :cond_0

    goto :goto_0

    .line 1116
    :cond_0
    new-instance v0, Lepson/print/WebviewActivity$ImageCreateThread;

    invoke-direct {v0, p0}, Lepson/print/WebviewActivity$ImageCreateThread;-><init>(Lepson/print/WebviewActivity;)V

    iput-object v0, p0, Lepson/print/WebviewActivity;->mImageCreateThread:Lepson/print/WebviewActivity$ImageCreateThread;

    .line 1117
    iget-object v0, p0, Lepson/print/WebviewActivity;->mImageCreateThread:Lepson/print/WebviewActivity$ImageCreateThread;

    invoke-virtual {v0, v1}, Lepson/print/WebviewActivity$ImageCreateThread;->setPicture(Landroid/graphics/Rect;)Lepson/print/WebviewActivity$ImageCreateThread;

    .line 1118
    iget-object v0, p0, Lepson/print/WebviewActivity;->mImageCreateThread:Lepson/print/WebviewActivity$ImageCreateThread;

    invoke-virtual {v0}, Lepson/print/WebviewActivity$ImageCreateThread;->start()V

    return-void

    .line 1101
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lepson/print/WebviewActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_2

    return-void

    .line 1105
    :cond_2
    invoke-virtual {p0}, Lepson/print/WebviewActivity;->cancelShowingDialog()V

    const v0, 0x7f0e03f7

    .line 1106
    invoke-virtual {p0, v0}, Lepson/print/WebviewActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lepson/print/WebviewActivity;->createDialog(Ljava/lang/String;)Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lepson/print/WebviewActivity;->mErrorDialog:Landroid/app/AlertDialog;

    .line 1108
    :try_start_0
    iget-object v0, p0, Lepson/print/WebviewActivity;->mErrorDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V
    :try_end_0
    .catch Landroid/view/WindowManager$BadTokenException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private startPrintActivity()V
    .locals 3

    .line 853
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lepson/print/ActivityPrintWeb;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "print_web"

    .line 854
    iget-object v2, p0, Lepson/print/WebviewActivity;->mAddImageView:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    const-string v1, "print_url"

    .line 855
    iget-object v2, p0, Lepson/print/WebviewActivity;->strUrlIn:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "from_customaction"

    .line 856
    iget-boolean v2, p0, Lepson/print/WebviewActivity;->isCustomAction:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "print_log"

    .line 857
    invoke-direct {p0}, Lepson/print/WebviewActivity;->getPrintLog()Lcom/epson/iprint/prtlogger/PrintLog;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const/4 v1, 0x2

    .line 858
    invoke-virtual {p0, v0, v1}, Lepson/print/WebviewActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 860
    invoke-virtual {p0}, Lepson/print/WebviewActivity;->cancelShowingDialog()V

    .line 861
    iget-boolean v0, p0, Lepson/print/WebviewActivity;->checkSharepage:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lepson/print/WebviewActivity;->isCustomAction:Z

    if-nez v0, :cond_0

    .line 862
    invoke-virtual {p0}, Lepson/print/WebviewActivity;->finish()V

    :cond_0
    return-void
.end method


# virtual methods
.method cancelPreviewWeb()V
    .locals 2

    const/4 v0, 0x1

    .line 1076
    iput-boolean v0, p0, Lepson/print/WebviewActivity;->mPreviewWebInterrupted:Z

    .line 1077
    iget-object v0, p0, Lepson/print/WebviewActivity;->myHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1078
    iget-object v0, p0, Lepson/print/WebviewActivity;->mImageCreateThread:Lepson/print/WebviewActivity$ImageCreateThread;

    if-eqz v0, :cond_0

    .line 1079
    invoke-virtual {v0}, Lepson/print/WebviewActivity$ImageCreateThread;->interrupt()V

    .line 1081
    :cond_0
    invoke-virtual {p0}, Lepson/print/WebviewActivity;->cancelShowingDialog()V

    return-void
.end method

.method public cancelShowingDialog()V
    .locals 1

    .line 765
    iget-object v0, p0, Lepson/print/WebviewActivity;->mCustomPro:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 766
    iget-object v0, p0, Lepson/print/WebviewActivity;->mCustomPro:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    :cond_0
    return-void
.end method

.method configureFromIntent(Landroid/content/Intent;)V
    .locals 4
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .line 243
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.SEND"

    .line 244
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const-string v2, "com.epson.iprint.web"

    .line 246
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v2, 0x1

    if-nez v1, :cond_6

    if-eqz v0, :cond_0

    goto/16 :goto_2

    .line 276
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v1, "android.intent.extra.TEXT"

    .line 278
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/print/WebviewActivity;->inURL:Ljava/lang/String;

    const-string p1, "Text"

    .line 279
    invoke-virtual {p0, p1}, Lepson/print/WebviewActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 281
    :cond_1
    iget-object p1, p0, Lepson/print/WebviewActivity;->inURL:Ljava/lang/String;

    if-nez p1, :cond_3

    .line 282
    iget-object p1, p0, Lepson/print/WebviewActivity;->mStrictMode:Lepson/print/IprintStrictMode$StrictModeInterface;

    invoke-interface {p1}, Lepson/print/IprintStrictMode$StrictModeInterface;->permitAll()V

    const-string p1, "SaveUrl"

    const/4 v1, 0x0

    .line 283
    invoke-virtual {p0, p1, v1}, Lepson/print/WebviewActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    iput-object p1, p0, Lepson/print/WebviewActivity;->statePre:Landroid/content/SharedPreferences;

    .line 284
    iget-object p1, p0, Lepson/print/WebviewActivity;->statePre:Landroid/content/SharedPreferences;

    const-string v1, "SaveUrl"

    const-string v3, ""

    invoke-interface {p1, v1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/print/WebviewActivity;->strUrlIn:Ljava/lang/String;

    .line 285
    iget-object p1, p0, Lepson/print/WebviewActivity;->mStrictMode:Lepson/print/IprintStrictMode$StrictModeInterface;

    invoke-interface {p1}, Lepson/print/IprintStrictMode$StrictModeInterface;->restoreStatus()V

    .line 286
    iget-object p1, p0, Lepson/print/WebviewActivity;->strUrlIn:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 288
    iput-boolean v2, p0, Lepson/print/WebviewActivity;->mIsFistLoad:Z

    const-string p1, "http://"

    .line 289
    invoke-virtual {p0, p1}, Lepson/print/WebviewActivity;->setTextOnUrlEditText(Ljava/lang/String;)V

    .line 290
    invoke-virtual {p0, v2}, Lepson/print/WebviewActivity;->setClearButtonAppearance(I)V

    :cond_2
    if-eqz v0, :cond_4

    const-string p1, "extra_url"

    .line 293
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lepson/print/WebviewActivity;->inURL:Ljava/lang/String;

    const-string p1, "is_faq"

    .line 294
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    iput-boolean p1, p0, Lepson/print/WebviewActivity;->isFaq:Z

    const-string p1, "is_buy"

    .line 295
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    iput-boolean p1, p0, Lepson/print/WebviewActivity;->isBuy:Z

    const-string p1, "is_info"

    .line 296
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    iput-boolean p1, p0, Lepson/print/WebviewActivity;->isInfo:Z

    goto :goto_0

    .line 300
    :cond_3
    iput-boolean v2, p0, Lepson/print/WebviewActivity;->checkSharepage:Z

    .line 303
    iput-object p1, p0, Lepson/print/WebviewActivity;->strUrlIn:Ljava/lang/String;

    .line 306
    :cond_4
    :goto_0
    iget-object p1, p0, Lepson/print/WebviewActivity;->inURL:Ljava/lang/String;

    if-eqz p1, :cond_5

    .line 307
    invoke-virtual {p0, p1}, Lepson/print/WebviewActivity;->loadUrl(Ljava/lang/String;)V

    goto :goto_1

    .line 309
    :cond_5
    iget-object p1, p0, Lepson/print/WebviewActivity;->strUrlIn:Ljava/lang/String;

    invoke-virtual {p0, p1}, Lepson/print/WebviewActivity;->loadUrl(Ljava/lang/String;)V

    .line 312
    :goto_1
    iget-object p1, p0, Lepson/print/WebviewActivity;->mWebview:Landroid/webkit/WebView;

    invoke-virtual {p0, p1}, Lepson/print/WebviewActivity;->registerForContextMenu(Landroid/view/View;)V

    goto :goto_4

    .line 249
    :cond_6
    :goto_2
    invoke-virtual {p0, p1}, Lepson/print/WebviewActivity;->setPrintLogForExternalApp(Landroid/content/Intent;)V

    .line 251
    iput-boolean v0, p0, Lepson/print/WebviewActivity;->isCustomAction:Z

    const-string v0, "android.intent.extra.TEXT"

    .line 253
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/print/WebviewActivity;->inURL:Ljava/lang/String;

    .line 255
    iget-object p1, p0, Lepson/print/WebviewActivity;->inURL:Ljava/lang/String;

    const-string v0, "image/jpeg"

    invoke-static {p1, v0}, Lepson/common/Utils;->checkMimeType(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_7

    .line 259
    iput-boolean v2, p0, Lepson/print/WebviewActivity;->checkImage:Z

    .line 260
    invoke-virtual {p0}, Lepson/print/WebviewActivity;->previewImage()V

    goto :goto_3

    .line 262
    :cond_7
    iget-object p1, p0, Lepson/print/WebviewActivity;->inURL:Ljava/lang/String;

    invoke-virtual {p0, p1}, Lepson/print/WebviewActivity;->loadUrl(Ljava/lang/String;)V

    xor-int/lit8 p1, v1, 0x1

    .line 264
    iput-boolean p1, p0, Lepson/print/WebviewActivity;->checkSharepage:Z

    .line 269
    :goto_3
    iget-object p1, p0, Lepson/print/WebviewActivity;->inURL:Ljava/lang/String;

    if-eqz p1, :cond_8

    .line 270
    iput-object p1, p0, Lepson/print/WebviewActivity;->strUrlIn:Ljava/lang/String;

    goto :goto_4

    :cond_8
    const-string p1, ""

    .line 272
    iput-object p1, p0, Lepson/print/WebviewActivity;->strUrlIn:Ljava/lang/String;

    :goto_4
    return-void
.end method

.method isMediaMounted()Z
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .line 1355
    invoke-static {}, Lepson/common/Utils;->isMediaMounted()Z

    move-result v0

    return v0
.end method

.method loadUrl(Ljava/lang/String;)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    const/4 v0, 0x0

    .line 319
    invoke-static {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->setPriorityToSimpleAP(Landroid/content/Context;Z)V

    .line 321
    iget-object v0, p0, Lepson/print/WebviewActivity;->mWebview:Landroid/webkit/WebView;

    invoke-virtual {v0, p1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    const-string v0, "onActivityResult : requestCode = "

    .line 1419
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1420
    invoke-super {p0, p1, p2, p3}, Lepson/print/ActivityIACommon;->onActivityResult(IILandroid/content/Intent;)V

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/4 p1, -0x1

    if-eq p2, p1, :cond_0

    .line 1437
    invoke-virtual {p0}, Lepson/print/WebviewActivity;->finish()V

    return-void

    .line 1440
    :cond_0
    invoke-direct {p0}, Lepson/print/WebviewActivity;->onInit()V

    goto :goto_0

    :pswitch_1
    const-string p1, "onActivityResult : ACTION_PRINT_WEB = "

    .line 1424
    iget-boolean p3, p0, Lepson/print/WebviewActivity;->isCustomAction:Z

    invoke-static {p3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object p3

    invoke-static {p1, p3}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1425
    iget-object p1, p0, Lepson/print/WebviewActivity;->mAddImageView:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    .line 1426
    invoke-static {p0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object p1

    invoke-virtual {p1}, Lepson/common/ExternalFileUtils;->initTempViewDir()V

    .line 1427
    iget-boolean p1, p0, Lepson/print/WebviewActivity;->isCustomAction:Z

    if-eqz p1, :cond_1

    const-string p1, "onActivityResult : resultCode = "

    .line 1428
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p3

    invoke-static {p1, p3}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1429
    invoke-virtual {p0, p2}, Lepson/print/WebviewActivity;->setResult(I)V

    .line 1430
    invoke-virtual {p0}, Lepson/print/WebviewActivity;->finish()V

    return-void

    :cond_1
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 2

    .line 1392
    iget-object v0, p0, Lepson/print/WebviewActivity;->mErrorDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1394
    :try_start_0
    iget-object v0, p0, Lepson/print/WebviewActivity;->mErrorDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 1397
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 1400
    :cond_0
    :goto_0
    iget-object v0, p0, Lepson/print/WebviewActivity;->mImageCreateThread:Lepson/print/WebviewActivity$ImageCreateThread;

    if-eqz v0, :cond_1

    .line 1401
    invoke-virtual {v0}, Lepson/print/WebviewActivity$ImageCreateThread;->interrupt()V

    .line 1404
    :cond_1
    iget-boolean v0, p0, Lepson/print/WebviewActivity;->isBuy:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lepson/print/WebviewActivity;->isFaq:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lepson/print/WebviewActivity;->isInfo:Z

    if-eqz v0, :cond_2

    goto :goto_1

    .line 1406
    :cond_2
    iget-boolean v0, p0, Lepson/print/WebviewActivity;->isCustomAction:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    const/4 v0, 0x0

    .line 1408
    invoke-virtual {p0, v0}, Lepson/print/WebviewActivity;->setResult(I)V

    .line 1409
    invoke-virtual {p0}, Lepson/print/WebviewActivity;->finish()V

    goto :goto_2

    .line 1411
    :cond_3
    invoke-virtual {p0}, Lepson/print/WebviewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const/4 v1, -0x1

    .line 1412
    invoke-virtual {p0, v1, v0}, Lepson/print/WebviewActivity;->setResult(ILandroid/content/Intent;)V

    .line 1413
    invoke-virtual {p0}, Lepson/print/WebviewActivity;->finish()V

    goto :goto_2

    .line 1405
    :cond_4
    :goto_1
    invoke-virtual {p0}, Lepson/print/WebviewActivity;->finish()V

    :goto_2
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    const-string v0, "WebviewActivity"

    const-string v1, "onCLick()"

    .line 601
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 602
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    const v0, 0x7f080094

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 604
    :cond_0
    iget-boolean p1, p0, Lepson/print/WebviewActivity;->mCanRefresh:Z

    if-eqz p1, :cond_1

    .line 605
    iget-object p1, p0, Lepson/print/WebviewActivity;->mWebview:Landroid/webkit/WebView;

    invoke-virtual {p1}, Landroid/webkit/WebView;->reload()V

    goto :goto_0

    .line 606
    :cond_1
    iget-boolean p1, p0, Lepson/print/WebviewActivity;->mCanStop:Z

    if-eqz p1, :cond_2

    .line 607
    iget-object p1, p0, Lepson/print/WebviewActivity;->mWebview:Landroid/webkit/WebView;

    invoke-virtual {p1}, Landroid/webkit/WebView;->stopLoading()V

    .line 608
    iget-object p1, p0, Lepson/print/WebviewActivity;->mLoadWebLayout:Landroid/widget/LinearLayout;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 609
    :cond_2
    iget-boolean p1, p0, Lepson/print/WebviewActivity;->mCanClear:Z

    if-eqz p1, :cond_3

    const-string p1, ""

    .line 610
    invoke-virtual {p0, p1}, Lepson/print/WebviewActivity;->setTextOnUrlEditText(Ljava/lang/String;)V

    :cond_3
    :goto_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi",
            "SetJavaScriptEnabled"
        }
    .end annotation

    .line 137
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    .line 139
    invoke-static {p0}, Lcom/epson/iprint/prtlogger/NewLoggerController;->stopLoggerIfNotAgreed(Landroid/content/Context;)V

    .line 143
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 144
    invoke-static {}, Landroid/webkit/WebView;->enableSlowWholeDocumentDraw()V

    :cond_0
    const v0, 0x7f0a0088

    .line 147
    invoke-virtual {p0, v0}, Lepson/print/WebviewActivity;->setContentView(I)V

    const v0, 0x7f0e0546

    const/4 v1, 0x1

    .line 149
    invoke-virtual {p0, v0, v1}, Lepson/print/WebviewActivity;->setActionBar(IZ)V

    .line 151
    iget-object v0, p0, Lepson/print/WebviewActivity;->mAddImageView:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 153
    invoke-static {p0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v0

    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->initTempViewDir()V

    .line 154
    invoke-static {p0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v0

    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->initPrintDir()V

    const v0, 0x7f08038a

    .line 156
    invoke-virtual {p0, v0}, Lepson/print/WebviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lepson/print/WebviewActivity;->mWebview:Landroid/webkit/WebView;

    const v0, 0x7f0801b9

    .line 158
    invoke-virtual {p0, v0}, Lepson/print/WebviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lepson/print/WebviewActivity;->mLoadWebLayout:Landroid/widget/LinearLayout;

    const v0, 0x7f08031c

    .line 160
    invoke-virtual {p0, v0}, Lepson/print/WebviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lepson/print/WebviewActivity;->teInurl:Landroid/widget/EditText;

    .line 161
    iget-object v0, p0, Lepson/print/WebviewActivity;->teInurl:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 163
    iget-object v0, p0, Lepson/print/WebviewActivity;->teInurl:Landroid/widget/EditText;

    new-instance v2, Lepson/print/WebviewActivity$1;

    invoke-direct {v2, p0}, Lepson/print/WebviewActivity$1;-><init>(Lepson/print/WebviewActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 172
    iget-object v0, p0, Lepson/print/WebviewActivity;->mImageArray:[Ljava/lang/Integer;

    const v2, 0x7f070081

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v0, v3

    .line 173
    iget-object v0, p0, Lepson/print/WebviewActivity;->mImageArray:[Ljava/lang/Integer;

    const v2, 0x7f070135

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const v0, 0x7f080094

    .line 174
    invoke-virtual {p0, v0}, Lepson/print/WebviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lepson/print/WebviewActivity;->btnClearUrl:Landroid/widget/ImageButton;

    .line 175
    invoke-virtual {p0, v3}, Lepson/print/WebviewActivity;->setClearButtonAppearance(I)V

    .line 176
    iget-object v0, p0, Lepson/print/WebviewActivity;->btnClearUrl:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 179
    iget-object v0, p0, Lepson/print/WebviewActivity;->mWebview:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    .line 180
    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 181
    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setBuiltInZoomControls(Z)V

    .line 182
    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    .line 183
    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    .line 184
    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setLoadWithOverviewMode(Z)V

    .line 186
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setBlockNetworkLoads(Z)V

    .line 187
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setBlockNetworkImage(Z)V

    .line 192
    :try_start_0
    iget-object v0, p0, Lepson/print/WebviewActivity;->mWebview:Landroid/webkit/WebView;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v2, "setIsCacheDrawBitmap"

    new-array v4, v1, [Ljava/lang/Class;

    sget-object v5, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v5, v4, v3

    invoke-virtual {v0, v2, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 194
    iget-object v2, p0, Lepson/print/WebviewActivity;->mWebview:Landroid/webkit/WebView;

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v1, v3

    invoke-virtual {v0, v2, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    iget-object v0, p0, Lepson/print/WebviewActivity;->mWebview:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    sget-object v1, Landroid/webkit/WebSettings$LayoutAlgorithm;->NORMAL:Landroid/webkit/WebSettings$LayoutAlgorithm;

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setLayoutAlgorithm(Landroid/webkit/WebSettings$LayoutAlgorithm;)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    nop

    goto :goto_0

    :catch_1
    nop

    goto :goto_0

    :catch_2
    nop

    .line 202
    :cond_1
    :goto_0
    iget-object v0, p0, Lepson/print/WebviewActivity;->mWebview:Landroid/webkit/WebView;

    const/16 v1, 0x82

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->requestFocus(I)Z

    .line 203
    iget-object v0, p0, Lepson/print/WebviewActivity;->mWebview:Landroid/webkit/WebView;

    iget-object v1, p0, Lepson/print/WebviewActivity;->mWebClient:Landroid/webkit/WebViewClient;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 204
    iget-object v0, p0, Lepson/print/WebviewActivity;->mWebview:Landroid/webkit/WebView;

    iget-object v1, p0, Lepson/print/WebviewActivity;->mWebCromClient:Landroid/webkit/WebChromeClient;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 205
    iget-object v0, p0, Lepson/print/WebviewActivity;->mWebview:Landroid/webkit/WebView;

    new-instance v1, Lepson/print/WebviewActivity$2;

    invoke-direct {v1, p0}, Lepson/print/WebviewActivity$2;-><init>(Lepson/print/WebviewActivity;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 213
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lepson/print/WebviewActivity;->mErrorDialog:Landroid/app/AlertDialog;

    if-nez p1, :cond_2

    .line 216
    invoke-direct {p0}, Lepson/print/WebviewActivity;->startLicenseCheckActivity()V

    goto :goto_1

    .line 218
    :cond_2
    invoke-virtual {p0}, Lepson/print/WebviewActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lepson/common/IprintLicenseInfo;->isAgreedCurrentVersion(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_3

    .line 219
    invoke-virtual {p0}, Lepson/print/WebviewActivity;->finish()V

    :cond_3
    :goto_1
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .line 694
    invoke-virtual {p0}, Lepson/print/WebviewActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0b0009

    .line 695
    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const/4 p1, 0x1

    return p1
.end method

.method protected onDestroy()V
    .locals 2

    .line 828
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onDestroy()V

    .line 830
    invoke-virtual {p0}, Lepson/print/WebviewActivity;->stopDoBackGroundThread()V

    const/4 v0, 0x0

    .line 831
    iput-object v0, p0, Lepson/print/WebviewActivity;->previewImageTask:Landroid/os/AsyncTask;

    .line 832
    iput-object v0, p0, Lepson/print/WebviewActivity;->mImageCreateThread:Lepson/print/WebviewActivity$ImageCreateThread;

    .line 834
    iget-object v1, p0, Lepson/print/WebviewActivity;->mCustomPro:Landroid/app/Dialog;

    if-eqz v1, :cond_0

    .line 835
    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    .line 837
    :cond_0
    iput-object v0, p0, Lepson/print/WebviewActivity;->mCustomPro:Landroid/app/Dialog;

    .line 839
    iget-object v1, p0, Lepson/print/WebviewActivity;->mErrorDialog:Landroid/app/AlertDialog;

    if-eqz v1, :cond_1

    .line 840
    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 842
    :cond_1
    iput-object v0, p0, Lepson/print/WebviewActivity;->mErrorDialog:Landroid/app/AlertDialog;

    return-void
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 2

    .line 617
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result p1

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p1, :cond_0

    const/16 p1, 0x42

    if-ne p2, p1, :cond_0

    .line 618
    iput-boolean v1, p0, Lepson/print/WebviewActivity;->mIsFinish:Z

    .line 619
    invoke-virtual {p0, v1}, Lepson/print/WebviewActivity;->setClearButtonAppearance(I)V

    .line 620
    iget-object p1, p0, Lepson/print/WebviewActivity;->teInurl:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/print/WebviewActivity;->strUrlIn:Ljava/lang/String;

    .line 621
    invoke-direct {p0}, Lepson/print/WebviewActivity;->loadInputUrl()V

    return v0

    .line 623
    :cond_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result p1

    if-nez p1, :cond_2

    const/4 p1, 0x4

    if-ne p2, p1, :cond_2

    .line 624
    iget-boolean p1, p0, Lepson/print/WebviewActivity;->checkSharepage:Z

    if-eqz p1, :cond_1

    .line 625
    invoke-virtual {p0}, Lepson/print/WebviewActivity;->finish()V

    return v0

    :cond_1
    return v1

    :cond_2
    return v1
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0

    .line 688
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onNewIntent(Landroid/content/Intent;)V

    .line 690
    invoke-static {p0, p1}, Lepson/print/Util/CommonMessage;->showInvalidPrintMessageIfEpsonNfcPrinter(Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .line 719
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f080278

    if-eq v0, v1, :cond_0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 726
    :pswitch_0
    iget-object v0, p0, Lepson/print/WebviewActivity;->strUrlIn:Ljava/lang/String;

    invoke-direct {p0, v0}, Lepson/print/WebviewActivity;->getIndexOfLink(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lepson/print/WebviewActivity;->getLinkOfIndex(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Forward to : "

    .line 727
    invoke-static {v1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 728
    iget-object v1, p0, Lepson/print/WebviewActivity;->mWebview:Landroid/webkit/WebView;

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0

    .line 721
    :pswitch_1
    iget-object v0, p0, Lepson/print/WebviewActivity;->strUrlIn:Ljava/lang/String;

    invoke-direct {p0, v0}, Lepson/print/WebviewActivity;->getIndexOfLink(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, Lepson/print/WebviewActivity;->getLinkOfIndex(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Comeback to : "

    .line 722
    invoke-static {v1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 723
    iget-object v1, p0, Lepson/print/WebviewActivity;->mWebview:Landroid/webkit/WebView;

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0

    .line 731
    :cond_0
    invoke-direct {p0}, Lepson/print/WebviewActivity;->previewWeb()V

    .line 735
    :goto_0
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1

    nop

    :pswitch_data_0
    .packed-switch 0x7f080162
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 0

    .line 674
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onPause()V

    .line 675
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils;->disableForegroundDispatch(Landroid/app/Activity;)V

    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 4

    .line 702
    invoke-direct {p0}, Lepson/print/WebviewActivity;->canGoBack()Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lepson/print/WebviewActivity;->mIsFinish:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const v3, 0x7f080162

    .line 703
    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 705
    invoke-direct {p0}, Lepson/print/WebviewActivity;->canGoForward()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lepson/print/WebviewActivity;->mIsFinish:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    const v3, 0x7f080163

    .line 706
    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 708
    iget-boolean v0, p0, Lepson/print/WebviewActivity;->mIsFinish:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lepson/print/WebviewActivity;->mIsLoadError:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    :goto_2
    const v3, 0x7f080278

    .line 709
    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 711
    iget-boolean v0, p0, Lepson/print/WebviewActivity;->checkSharepage:Z

    if-eqz v0, :cond_3

    .line 712
    invoke-interface {p1, v2, v2}, Landroid/view/Menu;->setGroupVisible(IZ)V

    :cond_3
    return v1
.end method

.method protected onResume()V
    .locals 2

    .line 680
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onResume()V

    const/4 v0, 0x0

    .line 683
    move-object v1, v0

    check-cast v1, [[Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils;->enableForegroundDispatch(Landroid/app/Activity;[Landroid/content/IntentFilter;[[Ljava/lang/String;)V

    return-void
.end method

.method previewImage()V
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .line 1250
    invoke-virtual {p0}, Lepson/print/WebviewActivity;->isMediaMounted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1253
    new-instance v0, Lepson/print/WebviewActivity$8;

    invoke-direct {v0, p0}, Lepson/print/WebviewActivity$8;-><init>(Lepson/print/WebviewActivity;)V

    iput-object v0, p0, Lepson/print/WebviewActivity;->previewImageTask:Landroid/os/AsyncTask;

    .line 1340
    iget-object v0, p0, Lepson/print/WebviewActivity;->previewImageTask:Landroid/os/AsyncTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    :cond_0
    const v0, 0x7f0e04ea

    .line 1342
    invoke-virtual {p0, v0}, Lepson/print/WebviewActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lepson/print/WebviewActivity;->createDialog(Ljava/lang/String;)Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lepson/print/WebviewActivity;->mErrorDialog:Landroid/app/AlertDialog;

    const v0, 0x7f08038d

    .line 1344
    :try_start_0
    invoke-virtual {p0, v0}, Lepson/print/WebviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1345
    iget-object v0, p0, Lepson/print/WebviewActivity;->mErrorDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V
    :try_end_0
    .catch Landroid/view/WindowManager$BadTokenException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 1348
    invoke-virtual {v0}, Landroid/view/WindowManager$BadTokenException;->printStackTrace()V

    :goto_0
    return-void
.end method

.method setClearButtonAppearance(I)V
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .line 326
    iget-object v0, p0, Lepson/print/WebviewActivity;->btnClearUrl:Landroid/widget/ImageButton;

    iget-object v1, p0, Lepson/print/WebviewActivity;->mImageArray:[Ljava/lang/Integer;

    aget-object p1, v1, p1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    return-void
.end method

.method setPrintLogForExternalApp(Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Intent;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    if-eqz p1, :cond_0

    :try_start_0
    const-string v0, "print_log"

    .line 341
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    check-cast p1, Lcom/epson/iprint/prtlogger/PrintLog;

    iput-object p1, p0, Lepson/print/WebviewActivity;->mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    nop

    .line 347
    :cond_0
    :goto_0
    iget-object p1, p0, Lepson/print/WebviewActivity;->mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;

    if-nez p1, :cond_1

    .line 348
    new-instance p1, Lcom/epson/iprint/prtlogger/PrintLog;

    invoke-direct {p1}, Lcom/epson/iprint/prtlogger/PrintLog;-><init>()V

    iput-object p1, p0, Lepson/print/WebviewActivity;->mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;

    .line 351
    :cond_1
    iget-object p1, p0, Lepson/print/WebviewActivity;->mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;

    iget p1, p1, Lcom/epson/iprint/prtlogger/PrintLog;->uiRoute:I

    if-gtz p1, :cond_2

    .line 354
    iget-object p1, p0, Lepson/print/WebviewActivity;->mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;

    const/16 v0, 0x1004

    iput v0, p1, Lcom/epson/iprint/prtlogger/PrintLog;->uiRoute:I

    .line 357
    :cond_2
    iget-object p1, p0, Lepson/print/WebviewActivity;->mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;

    iget-object p1, p1, Lcom/epson/iprint/prtlogger/PrintLog;->callerPackage:Ljava/lang/String;

    if-nez p1, :cond_3

    .line 358
    iget-object p1, p0, Lepson/print/WebviewActivity;->mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;

    invoke-virtual {p0}, Lepson/print/WebviewActivity;->getCallingPackage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/epson/iprint/prtlogger/PrintLog;->callerPackage:Ljava/lang/String;

    :cond_3
    return-void
.end method

.method setTextOnUrlEditText(Ljava/lang/String;)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .line 331
    iget-object v0, p0, Lepson/print/WebviewActivity;->teInurl:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public showLoadingDialog()V
    .locals 3

    .line 751
    iget-object v0, p0, Lepson/print/WebviewActivity;->mCustomPro:Landroid/app/Dialog;

    if-nez v0, :cond_0

    .line 752
    new-instance v0, Lepson/print/WebviewActivity$CustomProDialog;

    const v1, 0x7f0f0009

    const v2, 0x7f0a00aa

    invoke-direct {v0, p0, p0, v1, v2}, Lepson/print/WebviewActivity$CustomProDialog;-><init>(Lepson/print/WebviewActivity;Landroid/content/Context;II)V

    iput-object v0, p0, Lepson/print/WebviewActivity;->mCustomPro:Landroid/app/Dialog;

    .line 753
    iget-object v0, p0, Lepson/print/WebviewActivity;->mCustomPro:Landroid/app/Dialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 754
    iget-object v0, p0, Lepson/print/WebviewActivity;->mCustomPro:Landroid/app/Dialog;

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 757
    :cond_0
    :try_start_0
    iget-object v0, p0, Lepson/print/WebviewActivity;->mCustomPro:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V
    :try_end_0
    .catch Landroid/view/WindowManager$BadTokenException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 760
    invoke-virtual {v0}, Landroid/view/WindowManager$BadTokenException;->printStackTrace()V

    :goto_0
    return-void
.end method

.method protected stopDoBackGroundThread()V
    .locals 2

    .line 812
    invoke-virtual {p0}, Lepson/print/WebviewActivity;->cancelPreviewWeb()V

    .line 815
    iget-object v0, p0, Lepson/print/WebviewActivity;->previewImageTask:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    .line 816
    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 821
    :cond_0
    iget-object v0, p0, Lepson/print/WebviewActivity;->mImageCreateThread:Lepson/print/WebviewActivity$ImageCreateThread;

    if-eqz v0, :cond_1

    .line 822
    invoke-virtual {v0}, Lepson/print/WebviewActivity$ImageCreateThread;->interrupt()V

    :cond_1
    return-void
.end method

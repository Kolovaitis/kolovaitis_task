.class Lepson/print/IprintHome$BuyInkTask;
.super Landroid/os/AsyncTask;
.source "IprintHome.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/IprintHome;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "BuyInkTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/IprintHome;


# direct methods
.method constructor <init>(Lepson/print/IprintHome;)V
    .locals 0

    .line 972
    iput-object p1, p0, Lepson/print/IprintHome$BuyInkTask;->this$0:Lepson/print/IprintHome;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 972
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/print/IprintHome$BuyInkTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 2

    .line 987
    iget-object p1, p0, Lepson/print/IprintHome$BuyInkTask;->this$0:Lepson/print/IprintHome;

    invoke-virtual {p1}, Lepson/print/IprintHome;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string v0, "buyink"

    invoke-static {p1, v0}, Lepson/print/Util/BuyInkUrl;->urlSupport(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 988
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    .line 989
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 990
    iget-object p1, p0, Lepson/print/IprintHome$BuyInkTask;->this$0:Lepson/print/IprintHome;

    invoke-virtual {p1, v0}, Lepson/print/IprintHome;->startActivity(Landroid/content/Intent;)V

    const/4 p1, 0x0

    return-object p1
.end method

.method protected onPreExecute()V
    .locals 2

    .line 978
    iget-object v0, p0, Lepson/print/IprintHome$BuyInkTask;->this$0:Lepson/print/IprintHome;

    iget-object v0, v0, Lepson/print/IprintHome;->task:Lepson/print/IprintHome$ProbePrinter;

    if-eqz v0, :cond_0

    .line 979
    iget-object v0, p0, Lepson/print/IprintHome$BuyInkTask;->this$0:Lepson/print/IprintHome;

    iget-object v0, v0, Lepson/print/IprintHome;->task:Lepson/print/IprintHome$ProbePrinter;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lepson/print/IprintHome$ProbePrinter;->cancelTask(Z)Z

    .line 980
    iget-object v0, p0, Lepson/print/IprintHome$BuyInkTask;->this$0:Lepson/print/IprintHome;

    const/4 v1, 0x0

    iput-object v1, v0, Lepson/print/IprintHome;->task:Lepson/print/IprintHome$ProbePrinter;

    :cond_0
    return-void
.end method

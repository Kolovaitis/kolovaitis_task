.class Lepson/print/screen/PrintProgress$6;
.super Ljava/lang/Object;
.source "PrintProgress.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/screen/PrintProgress;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/screen/PrintProgress;


# direct methods
.method constructor <init>(Lepson/print/screen/PrintProgress;)V
    .locals 0

    .line 985
    iput-object p1, p0, Lepson/print/screen/PrintProgress$6;->this$0:Lepson/print/screen/PrintProgress;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .line 988
    iget-object p1, p0, Lepson/print/screen/PrintProgress$6;->this$0:Lepson/print/screen/PrintProgress;

    iget-object p1, p1, Lepson/print/screen/PrintProgress;->mHandler:Landroid/os/Handler;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Landroid/os/Handler;->removeMessages(I)V

    const/4 p1, 0x0

    :try_start_0
    const-string v0, "Epson"

    const-string v1, "user choice cancel print from GUI"

    .line 990
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 992
    invoke-static {}, Lepson/print/screen/PrintProgress;->access$800()Lepson/print/service/IEpsonService;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 993
    invoke-static {}, Lepson/print/screen/PrintProgress;->access$800()Lepson/print/service/IEpsonService;

    move-result-object v0

    invoke-interface {v0, p2}, Lepson/print/service/IEpsonService;->confirmCancel(Z)I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    .line 996
    invoke-static {}, Lepson/print/screen/PrintProgress;->access$2300()Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    const-string v1, "Epson"

    const-string v2, "===> set bCancel = true"

    .line 997
    invoke-static {v1, v2}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 999
    monitor-exit v0

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    .line 1002
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 1004
    :cond_1
    :goto_1
    iget-object v0, p0, Lepson/print/screen/PrintProgress$6;->this$0:Lepson/print/screen/PrintProgress;

    invoke-virtual {v0, p2}, Lepson/print/screen/PrintProgress;->removeDialog(I)V

    .line 1006
    iget-object v0, p0, Lepson/print/screen/PrintProgress$6;->this$0:Lepson/print/screen/PrintProgress;

    iput-boolean p2, v0, Lepson/print/screen/PrintProgress;->mCanceled:Z

    .line 1007
    iget-object v0, v0, Lepson/print/screen/PrintProgress;->mCancelButton:Landroid/widget/Button;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 1008
    iget-object v0, p0, Lepson/print/screen/PrintProgress$6;->this$0:Lepson/print/screen/PrintProgress;

    iget-object v0, v0, Lepson/print/screen/PrintProgress;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1011
    iget-object v0, p0, Lepson/print/screen/PrintProgress$6;->this$0:Lepson/print/screen/PrintProgress;

    invoke-static {v0}, Lepson/print/screen/PrintProgress;->access$700(Lepson/print/screen/PrintProgress;)Z

    move-result v0

    if-ne v0, p2, :cond_2

    .line 1012
    iget-object p2, p0, Lepson/print/screen/PrintProgress$6;->this$0:Lepson/print/screen/PrintProgress;

    iget-object p2, p2, Lepson/print/screen/PrintProgress;->mHandler:Landroid/os/Handler;

    const/4 v0, 0x5

    invoke-virtual {p2, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1013
    iget-object p2, p0, Lepson/print/screen/PrintProgress$6;->this$0:Lepson/print/screen/PrintProgress;

    invoke-static {p2, p1}, Lepson/print/screen/PrintProgress;->access$702(Lepson/print/screen/PrintProgress;Z)Z

    :cond_2
    return-void
.end method

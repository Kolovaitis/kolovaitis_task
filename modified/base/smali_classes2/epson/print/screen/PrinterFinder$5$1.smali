.class Lepson/print/screen/PrinterFinder$5$1;
.super Landroid/os/AsyncTask;
.source "PrinterFinder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/screen/PrinterFinder$5;->handleMessage(Landroid/os/Message;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "[",
        "Lepson/print/MyPrinter;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lepson/print/screen/PrinterFinder$5;

.field final synthetic val$data:Landroid/os/Bundle;


# direct methods
.method constructor <init>(Lepson/print/screen/PrinterFinder$5;Landroid/os/Bundle;)V
    .locals 0

    .line 358
    iput-object p1, p0, Lepson/print/screen/PrinterFinder$5$1;->this$1:Lepson/print/screen/PrinterFinder$5;

    iput-object p2, p0, Lepson/print/screen/PrinterFinder$5$1;->val$data:Landroid/os/Bundle;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 358
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/print/screen/PrinterFinder$5$1;->doInBackground([Ljava/lang/Void;)[Lepson/print/MyPrinter;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)[Lepson/print/MyPrinter;
    .locals 6

    const-string p1, "http"

    .line 362
    iget-object v0, p0, Lepson/print/screen/PrinterFinder$5$1;->val$data:Landroid/os/Bundle;

    const-string v1, "ip"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "/PRESENTATION/EPSONCONNECT"

    invoke-static {p1, v0, v1}, Lepson/common/IPAddressUtils;->buildURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 366
    new-instance v0, Lepson/common/httpclient/IAHttpClient$HttpGet;

    invoke-direct {v0, p1}, Lepson/common/httpclient/IAHttpClient$HttpGet;-><init>(Ljava/lang/String;)V

    .line 369
    new-instance v1, Lepson/common/httpclient/IAHttpClient;

    invoke-direct {v1}, Lepson/common/httpclient/IAHttpClient;-><init>()V

    const/4 v2, 0x0

    .line 371
    invoke-virtual {v1, v2}, Lepson/common/httpclient/IAHttpClient;->setFollowRedirects(Z)V

    const/4 v3, 0x0

    .line 374
    :try_start_0
    invoke-virtual {v1, v0}, Lepson/common/httpclient/IAHttpClient;->execute(Lepson/common/httpclient/IAHttpClient$HttpGet;)Lepson/common/httpclient/IAHttpClient$HttpResponse;

    move-result-object v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const-string v0, "httpResponse"

    const-string v1, "IOException"

    .line 376
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v3

    :goto_0
    if-eqz v0, :cond_3

    .line 379
    invoke-virtual {v0}, Lepson/common/httpclient/IAHttpClient$HttpResponse;->getResponseCode()I

    move-result v0

    const-string v1, "Response"

    .line 380
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "res : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v1, 0xc8

    if-eq v1, v0, :cond_2

    const/16 v1, 0x12c

    if-gt v1, v0, :cond_0

    const/16 v1, 0x190

    if-ge v0, v1, :cond_0

    goto :goto_1

    :cond_0
    const/16 v1, 0x194

    if-ne v1, v0, :cond_1

    const-string v0, "Response"

    .line 392
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "NotFound:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_1
    const-string v0, "Response"

    .line 394
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 384
    :cond_2
    :goto_1
    new-instance p1, Lepson/print/MyPrinter;

    iget-object v0, p0, Lepson/print/screen/PrinterFinder$5$1;->val$data:Landroid/os/Bundle;

    const-string v1, "name"

    .line 385
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lepson/print/screen/PrinterFinder$5$1;->val$data:Landroid/os/Bundle;

    const-string v4, "ip"

    .line 386
    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lepson/print/screen/PrinterFinder$5$1;->val$data:Landroid/os/Bundle;

    const-string v5, "id"

    .line 387
    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p1, v0, v1, v4, v3}, Lepson/print/MyPrinter;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    iget-object v0, p0, Lepson/print/screen/PrinterFinder$5$1;->val$data:Landroid/os/Bundle;

    const-string v1, "common_devicename"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lepson/print/MyPrinter;->setCommonDeviceName(Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 390
    new-array v0, v0, [Lepson/print/MyPrinter;

    aput-object p1, v0, v2

    return-object v0

    :cond_3
    :goto_2
    return-object v3
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 358
    check-cast p1, [Lepson/print/MyPrinter;

    invoke-virtual {p0, p1}, Lepson/print/screen/PrinterFinder$5$1;->onPostExecute([Lepson/print/MyPrinter;)V

    return-void
.end method

.method protected onPostExecute([Lepson/print/MyPrinter;)V
    .locals 2

    .line 404
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    if-eqz p1, :cond_0

    .line 406
    iget-object v0, p0, Lepson/print/screen/PrinterFinder$5$1;->this$1:Lepson/print/screen/PrinterFinder$5;

    iget-object v0, v0, Lepson/print/screen/PrinterFinder$5;->this$0:Lepson/print/screen/PrinterFinder;

    iget-object v0, v0, Lepson/print/screen/PrinterFinder;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    check-cast v0, Lepson/print/widgets/PrinterInfoBuilder;

    const/4 v1, 0x0

    aget-object p1, p1, v1

    invoke-virtual {v0, p1}, Lepson/print/widgets/PrinterInfoBuilder;->addPrinter(Lepson/print/MyPrinter;)V

    .line 407
    iget-object p1, p0, Lepson/print/screen/PrinterFinder$5$1;->this$1:Lepson/print/screen/PrinterFinder$5;

    iget-object p1, p1, Lepson/print/screen/PrinterFinder$5;->this$0:Lepson/print/screen/PrinterFinder;

    invoke-static {p1}, Lepson/print/screen/PrinterFinder;->access$200(Lepson/print/screen/PrinterFinder;)Ljava/util/ArrayList;

    move-result-object p1

    iget-object v0, p0, Lepson/print/screen/PrinterFinder$5$1;->val$data:Landroid/os/Bundle;

    const-string v1, "id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

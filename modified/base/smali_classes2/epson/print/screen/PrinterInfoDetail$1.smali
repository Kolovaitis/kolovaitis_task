.class Lepson/print/screen/PrinterInfoDetail$1;
.super Ljava/lang/Object;
.source "PrinterInfoDetail.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/screen/PrinterInfoDetail;->buildElements()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/screen/PrinterInfoDetail;


# direct methods
.method constructor <init>(Lepson/print/screen/PrinterInfoDetail;)V
    .locals 0

    .line 188
    iput-object p1, p0, Lepson/print/screen/PrinterInfoDetail$1;->this$0:Lepson/print/screen/PrinterInfoDetail;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .line 191
    new-instance p1, Landroid/content/Intent;

    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    .line 192
    new-instance p2, Landroid/os/Bundle;

    invoke-direct {p2}, Landroid/os/Bundle;-><init>()V

    const-string p4, "ID"

    .line 193
    iget-object p5, p0, Lepson/print/screen/PrinterInfoDetail$1;->this$0:Lepson/print/screen/PrinterInfoDetail;

    iget p5, p5, Lepson/print/screen/PrinterInfoDetail;->id:I

    invoke-virtual {p2, p4, p5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string p4, "curValue"

    .line 194
    iget-object p5, p0, Lepson/print/screen/PrinterInfoDetail$1;->this$0:Lepson/print/screen/PrinterInfoDetail;

    iget-object p5, p5, Lepson/print/screen/PrinterInfoDetail;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {p5}, Lepson/print/widgets/AbstractListBuilder;->getData()Ljava/util/Vector;

    move-result-object p5

    invoke-virtual {p5, p3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object p5

    check-cast p5, Lepson/print/widgets/CommonDataKinds$PrinterInfo;

    invoke-virtual {p5}, Lepson/print/widgets/CommonDataKinds$PrinterInfo;->getValue()I

    move-result p5

    invoke-virtual {p2, p4, p5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string p4, "INDEX"

    .line 195
    invoke-virtual {p2, p4, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 196
    invoke-virtual {p1, p2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 197
    iget-object p2, p0, Lepson/print/screen/PrinterInfoDetail$1;->this$0:Lepson/print/screen/PrinterInfoDetail;

    const/4 p3, -0x1

    invoke-virtual {p2, p3, p1}, Lepson/print/screen/PrinterInfoDetail;->setResult(ILandroid/content/Intent;)V

    .line 198
    iget-object p1, p0, Lepson/print/screen/PrinterInfoDetail$1;->this$0:Lepson/print/screen/PrinterInfoDetail;

    invoke-virtual {p1}, Lepson/print/screen/PrinterInfoDetail;->finish()V

    return-void
.end method

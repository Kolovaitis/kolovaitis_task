.class Lepson/print/screen/PrinterFinder$2;
.super Ljava/lang/Object;
.source "PrinterFinder.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/screen/PrinterFinder;->buildElements()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/screen/PrinterFinder;


# direct methods
.method constructor <init>(Lepson/print/screen/PrinterFinder;)V
    .locals 0

    .line 144
    iput-object p1, p0, Lepson/print/screen/PrinterFinder$2;->this$0:Lepson/print/screen/PrinterFinder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .line 147
    invoke-static {}, Lepson/print/screen/PrinterFinder;->access$100()Ljava/lang/Object;

    move-result-object p1

    monitor-enter p1

    .line 148
    :try_start_0
    iget-object v0, p0, Lepson/print/screen/PrinterFinder$2;->this$0:Lepson/print/screen/PrinterFinder;

    invoke-static {v0}, Lepson/print/screen/PrinterFinder;->access$200(Lepson/print/screen/PrinterFinder;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 149
    iget-object v0, p0, Lepson/print/screen/PrinterFinder$2;->this$0:Lepson/print/screen/PrinterFinder;

    iget-object v0, v0, Lepson/print/screen/PrinterFinder;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {v0}, Lepson/print/widgets/AbstractListBuilder;->refresh()V

    .line 150
    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 151
    iget-object p1, p0, Lepson/print/screen/PrinterFinder$2;->this$0:Lepson/print/screen/PrinterFinder;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lepson/print/screen/PrinterFinder;->access$302(Lepson/print/screen/PrinterFinder;Z)Z

    .line 154
    iget-object p1, p0, Lepson/print/screen/PrinterFinder$2;->this$0:Lepson/print/screen/PrinterFinder;

    invoke-virtual {p1, v0}, Lepson/print/screen/PrinterFinder;->searchButtonSetEnabled(Z)V

    .line 156
    iget-object p1, p0, Lepson/print/screen/PrinterFinder$2;->this$0:Lepson/print/screen/PrinterFinder;

    iget-object p1, p1, Lepson/print/screen/PrinterFinder;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {p1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 157
    iget-object p1, p0, Lepson/print/screen/PrinterFinder$2;->this$0:Lepson/print/screen/PrinterFinder;

    iget-object p1, p1, Lepson/print/screen/PrinterFinder;->mLayout:Landroid/view/ViewGroup;

    const v0, 0x7f080123

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iget-object v0, p0, Lepson/print/screen/PrinterFinder$2;->this$0:Lepson/print/screen/PrinterFinder;

    const v1, 0x7f0e045f

    .line 158
    invoke-virtual {v0, v1}, Lepson/print/screen/PrinterFinder;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 159
    iget-object p1, p0, Lepson/print/screen/PrinterFinder$2;->this$0:Lepson/print/screen/PrinterFinder;

    iget-object p1, p1, Lepson/print/screen/PrinterFinder;->mHandler:Landroid/os/Handler;

    const/4 v0, 0x1

    const-wide/16 v1, 0x64

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void

    :catchall_0
    move-exception v0

    .line 150
    :try_start_1
    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.class Lepson/print/screen/PaperSourceSettingScr$2;
.super Ljava/lang/Object;
.source "PaperSourceSettingScr.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/screen/PaperSourceSettingScr;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/screen/PaperSourceSettingScr;


# direct methods
.method constructor <init>(Lepson/print/screen/PaperSourceSettingScr;)V
    .locals 0

    .line 252
    iput-object p1, p0, Lepson/print/screen/PaperSourceSettingScr$2;->this$0:Lepson/print/screen/PaperSourceSettingScr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .line 256
    iget-object p1, p0, Lepson/print/screen/PaperSourceSettingScr$2;->this$0:Lepson/print/screen/PaperSourceSettingScr;

    iget-object p1, p1, Lepson/print/screen/PaperSourceSettingScr;->mListBuilder:Lepson/print/screen/PaperSourceSettingScr$PaperSourceInfoBuilder;

    invoke-virtual {p1}, Lepson/print/screen/PaperSourceSettingScr$PaperSourceInfoBuilder;->getData()Ljava/util/Vector;

    move-result-object p1

    invoke-virtual {p1, p3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lepson/print/screen/PaperSourceSetting;

    .line 258
    iget-boolean p2, p1, Lepson/print/screen/PaperSourceSetting;->bSupportESCPR:Z

    if-nez p2, :cond_0

    .line 259
    iget-object p1, p0, Lepson/print/screen/PaperSourceSettingScr$2;->this$0:Lepson/print/screen/PaperSourceSettingScr;

    const/16 p2, 0xa

    invoke-virtual {p1, p2}, Lepson/print/screen/PaperSourceSettingScr;->showDialog(I)V

    return-void

    .line 264
    :cond_0
    iget-object p2, p0, Lepson/print/screen/PaperSourceSettingScr$2;->this$0:Lepson/print/screen/PaperSourceSettingScr;

    .line 265
    invoke-static {p2}, Lepson/print/screen/PaperSourceSettingScr;->access$000(Lepson/print/screen/PaperSourceSettingScr;)Ljava/lang/String;

    move-result-object p3

    .line 264
    invoke-static {p2, p3}, Lepson/print/screen/PrintSetting;->getInstance(Landroid/content/Context;Ljava/lang/String;)Lepson/print/screen/PrintSetting;

    move-result-object p2

    .line 266
    invoke-virtual {p2}, Lepson/print/screen/PrintSetting;->loadSettings()V

    .line 269
    iget p3, p1, Lepson/print/screen/PaperSourceSetting;->paperSizeId:I

    iput p3, p2, Lepson/print/screen/PrintSetting;->paperSizeValue:I

    .line 270
    iget p3, p1, Lepson/print/screen/PaperSourceSetting;->paperTypeId:I

    iput p3, p2, Lepson/print/screen/PrintSetting;->paperTypeValue:I

    .line 271
    iget p1, p1, Lepson/print/screen/PaperSourceSetting;->paperSource:I

    iput p1, p2, Lepson/print/screen/PrintSetting;->paperSourceValue:I

    .line 272
    invoke-virtual {p2}, Lepson/print/screen/PrintSetting;->saveSettings()V

    .line 276
    :try_start_0
    iget-object p1, p0, Lepson/print/screen/PaperSourceSettingScr$2;->this$0:Lepson/print/screen/PaperSourceSettingScr;

    invoke-static {p1}, Lepson/print/screen/PaperSourceSettingScr;->access$100(Lepson/print/screen/PaperSourceSettingScr;)Lepson/print/service/IEpsonService;

    move-result-object p1

    iget-object p2, p0, Lepson/print/screen/PaperSourceSettingScr$2;->this$0:Lepson/print/screen/PaperSourceSettingScr;

    invoke-static {p2}, Lepson/print/screen/PaperSourceSettingScr;->access$000(Lepson/print/screen/PaperSourceSettingScr;)Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2}, Lepson/print/service/IEpsonService;->updatePrinterSettings(Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 278
    invoke-virtual {p1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 281
    :goto_0
    iget-object p1, p0, Lepson/print/screen/PaperSourceSettingScr$2;->this$0:Lepson/print/screen/PaperSourceSettingScr;

    const/4 p2, 0x3

    invoke-virtual {p1, p2}, Lepson/print/screen/PaperSourceSettingScr;->setResult(I)V

    .line 282
    iget-object p1, p0, Lepson/print/screen/PaperSourceSettingScr$2;->this$0:Lepson/print/screen/PaperSourceSettingScr;

    invoke-virtual {p1}, Lepson/print/screen/PaperSourceSettingScr;->finish()V

    return-void
.end method

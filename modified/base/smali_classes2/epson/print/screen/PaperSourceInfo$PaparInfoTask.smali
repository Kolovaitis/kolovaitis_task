.class public Lepson/print/screen/PaperSourceInfo$PaparInfoTask;
.super Landroid/os/AsyncTask;
.source "PaperSourceInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/screen/PaperSourceInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "PaparInfoTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/screen/PaperSourceInfo;


# direct methods
.method public constructor <init>(Lepson/print/screen/PaperSourceInfo;)V
    .locals 0

    .line 104
    iput-object p1, p0, Lepson/print/screen/PaperSourceInfo$PaparInfoTask;->this$0:Lepson/print/screen/PaperSourceInfo;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 104
    invoke-virtual {p0, p1}, Lepson/print/screen/PaperSourceInfo$PaparInfoTask;->doInBackground([Ljava/lang/Object;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/Object;)Ljava/lang/Void;
    .locals 12

    const/4 v0, 0x0

    .line 109
    aget-object v1, p1, v0

    check-cast v1, Landroid/content/Context;

    const/4 v2, 0x1

    .line 110
    aget-object p1, p1, v2

    check-cast p1, Landroid/os/Handler;

    const/16 v3, 0x64

    .line 115
    :try_start_0
    invoke-static {v1}, Lepson/print/MyPrinter;->getCurPrinter(Landroid/content/Context;)Lepson/print/MyPrinter;

    move-result-object v4

    .line 116
    invoke-virtual {v4}, Lepson/print/MyPrinter;->getLocation()I

    move-result v5

    if-eq v5, v2, :cond_0

    const/4 v1, 0x3

    if-eq v5, v1, :cond_1

    goto/16 :goto_4

    :cond_0
    const-string v5, "printer"

    .line 118
    invoke-static {v1, v5}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->getConnectInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    goto/16 :goto_4

    .line 133
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lepson/print/screen/PaperSourceInfo$PaparInfoTask;->isCancelled()Z

    move-result v1

    const-wide/16 v5, 0xfa0

    if-nez v1, :cond_4

    .line 134
    iget-object v1, p0, Lepson/print/screen/PaperSourceInfo$PaparInfoTask;->this$0:Lepson/print/screen/PaperSourceInfo;

    invoke-static {v1}, Lepson/print/screen/PaperSourceInfo;->access$000(Lepson/print/screen/PaperSourceInfo;)Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object v1

    const/16 v7, 0x3c

    invoke-virtual {v4}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v8

    .line 135
    invoke-virtual {v4}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4}, Lepson/print/MyPrinter;->getLocation()I

    move-result v10

    .line 134
    invoke-virtual {v1, v7, v8, v9, v10}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doProbePrinter(ILjava/lang/String;Ljava/lang/String;I)I

    move-result v1

    const/16 v7, -0x514

    if-eq v1, v7, :cond_3

    if-eqz v1, :cond_2

    const-string v0, "PaperSourceInfo"

    .line 152
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "doProbePrinter() return "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 140
    :cond_2
    iget-object v1, p0, Lepson/print/screen/PaperSourceInfo$PaparInfoTask;->this$0:Lepson/print/screen/PaperSourceInfo;

    invoke-static {v1}, Lepson/print/screen/PaperSourceInfo;->access$000(Lepson/print/screen/PaperSourceInfo;)Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object v1

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doSetPrinter()I

    move-result v1

    if-eqz v1, :cond_4

    goto/16 :goto_4

    :cond_3
    const-string v7, "PaperSourceInfo"

    .line 148
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "doProbePrinter() return "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v7, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    invoke-static {v5, v6}, Ljava/lang/Thread;->sleep(J)V

    goto :goto_0

    .line 160
    :cond_4
    :goto_1
    invoke-virtual {p0}, Lepson/print/screen/PaperSourceInfo$PaparInfoTask;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_a

    .line 165
    iget-object v1, p0, Lepson/print/screen/PaperSourceInfo$PaparInfoTask;->this$0:Lepson/print/screen/PaperSourceInfo;

    invoke-static {v1}, Lepson/print/screen/PaperSourceInfo;->access$000(Lepson/print/screen/PaperSourceInfo;)Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object v1

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->getPaperInfo()[I

    move-result-object v1

    if-nez v1, :cond_5

    const-string v0, "PaperSourceInfo"

    const-string v1, "getPaperInfo() return null"

    .line 169
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 173
    :cond_5
    aget v4, v1, v0

    const/16 v7, -0x44c

    if-eq v4, v7, :cond_9

    if-eqz v4, :cond_6

    const-string v2, "PaperSourceInfo"

    .line 211
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getPaperInfo() return "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget v0, v1, v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 176
    :cond_6
    aget v4, v1, v2

    const-string v7, "PaperSourceInfo"

    .line 177
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getPaperInfo() number = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget v9, v1, v2

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    if-ge v4, v2, :cond_7

    goto :goto_4

    .line 185
    :cond_7
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    const/4 v8, 0x0

    :goto_2
    if-ge v8, v4, :cond_8

    .line 188
    new-instance v9, Lepson/print/screen/PaperSourceSetting;

    invoke-direct {v9}, Lepson/print/screen/PaperSourceSetting;-><init>()V

    mul-int/lit8 v10, v8, 0x3

    add-int/lit8 v11, v10, 0x2

    .line 189
    aget v11, v1, v11

    iput v11, v9, Lepson/print/screen/PaperSourceSetting;->paperSource:I

    add-int/lit8 v11, v10, 0x3

    .line 190
    aget v11, v1, v11

    iput v11, v9, Lepson/print/screen/PaperSourceSetting;->paperSizeId:I

    add-int/lit8 v10, v10, 0x4

    .line 191
    aget v10, v1, v10

    iput v10, v9, Lepson/print/screen/PaperSourceSetting;->paperTypeId:I

    .line 192
    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 196
    :cond_8
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v4, "PAPERSOURCEINFO"

    .line 197
    invoke-virtual {v1, v4, v7}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 199
    new-instance v4, Landroid/os/Message;

    invoke-direct {v4}, Landroid/os/Message;-><init>()V

    .line 200
    iput v3, v4, Landroid/os/Message;->what:I

    .line 201
    invoke-virtual {v4, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 202
    invoke-virtual {p1, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_3

    :cond_9
    const-string v4, "PaperSourceInfo"

    .line 207
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getPaperInfo() return "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget v1, v1, v0

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    :goto_3
    invoke-static {v5, v6}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    :catch_0
    nop

    .line 221
    :cond_a
    :goto_4
    invoke-virtual {p0}, Lepson/print/screen/PaperSourceInfo$PaparInfoTask;->isCancelled()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_b

    .line 224
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "PAPERSOURCEINFO"

    .line 225
    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 226
    new-instance v2, Landroid/os/Message;

    invoke-direct {v2}, Landroid/os/Message;-><init>()V

    .line 227
    iput v3, v2, Landroid/os/Message;->what:I

    .line 228
    invoke-virtual {v2, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 229
    invoke-virtual {p1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_b
    return-object v1
.end method

.class Lepson/print/screen/PrinterFinder$1;
.super Ljava/lang/Object;
.source "PrinterFinder.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/screen/PrinterFinder;->buildElements()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/screen/PrinterFinder;


# direct methods
.method constructor <init>(Lepson/print/screen/PrinterFinder;)V
    .locals 0

    .line 112
    iput-object p1, p0, Lepson/print/screen/PrinterFinder$1;->this$0:Lepson/print/screen/PrinterFinder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .line 117
    iget-object p1, p0, Lepson/print/screen/PrinterFinder$1;->this$0:Lepson/print/screen/PrinterFinder;

    invoke-static {p1}, Lepson/print/screen/PrinterFinder;->access$000(Lepson/print/screen/PrinterFinder;)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    const/4 p2, 0x1

    if-ne p1, p2, :cond_0

    .line 119
    iget-object p1, p0, Lepson/print/screen/PrinterFinder$1;->this$0:Lepson/print/screen/PrinterFinder;

    const/4 p4, 0x0

    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p4

    invoke-static {p1, p4}, Lepson/print/screen/PrinterFinder;->access$002(Lepson/print/screen/PrinterFinder;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    .line 125
    iget-object p1, p0, Lepson/print/screen/PrinterFinder$1;->this$0:Lepson/print/screen/PrinterFinder;

    iput-boolean p2, p1, Lepson/print/screen/PrinterFinder;->mIsClickSelect:Z

    .line 127
    iget-object p1, p1, Lepson/print/screen/PrinterFinder;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object p1

    const/4 p2, 0x4

    .line 128
    iput p2, p1, Landroid/os/Message;->what:I

    .line 129
    new-instance p2, Landroid/os/Bundle;

    invoke-direct {p2}, Landroid/os/Bundle;-><init>()V

    const-string p4, "index"

    .line 130
    invoke-virtual {p2, p4, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string p4, "name"

    .line 132
    iget-object p5, p0, Lepson/print/screen/PrinterFinder$1;->this$0:Lepson/print/screen/PrinterFinder;

    iget-object p5, p5, Lepson/print/screen/PrinterFinder;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    .line 133
    invoke-virtual {p5}, Lepson/print/widgets/AbstractListBuilder;->getData()Ljava/util/Vector;

    move-result-object p5

    invoke-virtual {p5, p3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object p5

    check-cast p5, Lepson/print/MyPrinter;

    invoke-virtual {p5}, Lepson/print/MyPrinter;->getName()Ljava/lang/String;

    move-result-object p5

    .line 132
    invoke-virtual {p2, p4, p5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p4, "id"

    .line 134
    iget-object p5, p0, Lepson/print/screen/PrinterFinder$1;->this$0:Lepson/print/screen/PrinterFinder;

    iget-object p5, p5, Lepson/print/screen/PrinterFinder;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    .line 135
    invoke-virtual {p5}, Lepson/print/widgets/AbstractListBuilder;->getData()Ljava/util/Vector;

    move-result-object p5

    invoke-virtual {p5, p3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object p5

    check-cast p5, Lepson/print/MyPrinter;

    invoke-virtual {p5}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object p5

    .line 134
    invoke-virtual {p2, p4, p5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p4, "ip"

    .line 136
    iget-object p5, p0, Lepson/print/screen/PrinterFinder$1;->this$0:Lepson/print/screen/PrinterFinder;

    iget-object p5, p5, Lepson/print/screen/PrinterFinder;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    .line 137
    invoke-virtual {p5}, Lepson/print/widgets/AbstractListBuilder;->getData()Ljava/util/Vector;

    move-result-object p5

    invoke-virtual {p5, p3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lepson/print/MyPrinter;

    invoke-virtual {p3}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object p3

    .line 136
    invoke-virtual {p2, p4, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    invoke-virtual {p1, p2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 140
    iget-object p2, p0, Lepson/print/screen/PrinterFinder$1;->this$0:Lepson/print/screen/PrinterFinder;

    iget-object p2, p2, Lepson/print/screen/PrinterFinder;->mHandler:Landroid/os/Handler;

    const-wide/16 p3, 0x64

    invoke-virtual {p2, p1, p3, p4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void

    :cond_0
    return-void
.end method

.class Lepson/print/screen/PrinterInfoDetail$2;
.super Ljava/lang/Object;
.source "PrinterInfoDetail.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/screen/PrinterInfoDetail;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/screen/PrinterInfoDetail;


# direct methods
.method constructor <init>(Lepson/print/screen/PrinterInfoDetail;)V
    .locals 0

    .line 226
    iput-object p1, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 6

    const-string v0, "handleMessage"

    .line 230
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "msg.what = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    new-instance v0, Lepson/print/screen/PrintSetting;

    iget-object v1, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    iget-boolean v2, v1, Lepson/print/screen/PrinterInfoDetail;->isDocumentSetting:Z

    if-eqz v2, :cond_0

    sget-object v2, Lepson/print/screen/PrintSetting$Kind;->document:Lepson/print/screen/PrintSetting$Kind;

    goto :goto_0

    :cond_0
    sget-object v2, Lepson/print/screen/PrintSetting$Kind;->photo:Lepson/print/screen/PrintSetting$Kind;

    :goto_0
    invoke-direct {v0, v1, v2}, Lepson/print/screen/PrintSetting;-><init>(Landroid/content/Context;Lepson/print/screen/PrintSetting$Kind;)V

    .line 236
    invoke-virtual {v0}, Lepson/print/screen/PrintSetting;->loadSettings()V

    .line 238
    iget p1, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x12

    const/4 v2, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eq p1, v1, :cond_a

    packed-switch p1, :pswitch_data_0

    goto/16 :goto_2

    .line 343
    :pswitch_0
    iget-object p1, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    new-array v0, v2, [I

    invoke-static {p1, v0}, Lepson/print/screen/PrinterInfoDetail;->access$102(Lepson/print/screen/PrinterInfoDetail;[I)[I

    .line 344
    iget-object p1, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    invoke-static {p1}, Lepson/print/screen/PrinterInfoDetail;->access$100(Lepson/print/screen/PrinterInfoDetail;)[I

    move-result-object p1

    aput v3, p1, v3

    .line 345
    iget-object p1, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    invoke-static {p1}, Lepson/print/screen/PrinterInfoDetail;->access$100(Lepson/print/screen/PrinterInfoDetail;)[I

    move-result-object p1

    aput v4, p1, v4

    goto/16 :goto_2

    .line 332
    :pswitch_1
    iget-object p1, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    invoke-static {p1}, Lepson/print/screen/PrinterInfoDetail;->access$700(Lepson/print/screen/PrinterInfoDetail;)[I

    move-result-object p1

    if-eqz p1, :cond_1

    .line 333
    iget-object p1, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    invoke-static {p1}, Lepson/print/screen/PrinterInfoDetail;->access$700(Lepson/print/screen/PrinterInfoDetail;)[I

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    invoke-static {p1, v0}, Lepson/print/screen/PrinterInfoDetail;->access$102(Lepson/print/screen/PrinterInfoDetail;[I)[I

    .line 334
    iget-object p1, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    invoke-static {p1}, Lepson/print/screen/PrinterInfoDetail;->access$700(Lepson/print/screen/PrinterInfoDetail;)[I

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/screen/PrinterInfoDetail;->access$102(Lepson/print/screen/PrinterInfoDetail;[I)[I

    goto/16 :goto_2

    .line 336
    :cond_1
    iget-object p1, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    new-array v1, v4, [I

    invoke-static {p1, v1}, Lepson/print/screen/PrinterInfoDetail;->access$102(Lepson/print/screen/PrinterInfoDetail;[I)[I

    .line 338
    iget-object p1, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    invoke-static {p1}, Lepson/print/screen/PrinterInfoDetail;->access$100(Lepson/print/screen/PrinterInfoDetail;)[I

    move-result-object p1

    iget v0, v0, Lepson/print/screen/PrintSetting;->duplexValue:I

    aput v0, p1, v3

    goto/16 :goto_2

    .line 321
    :pswitch_2
    iget-object p1, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    invoke-static {p1}, Lepson/print/screen/PrinterInfoDetail;->access$600(Lepson/print/screen/PrinterInfoDetail;)[I

    move-result-object p1

    if-eqz p1, :cond_2

    .line 322
    iget-object p1, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    invoke-static {p1}, Lepson/print/screen/PrinterInfoDetail;->access$600(Lepson/print/screen/PrinterInfoDetail;)[I

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    invoke-static {p1, v0}, Lepson/print/screen/PrinterInfoDetail;->access$102(Lepson/print/screen/PrinterInfoDetail;[I)[I

    .line 323
    iget-object p1, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    invoke-static {p1}, Lepson/print/screen/PrinterInfoDetail;->access$600(Lepson/print/screen/PrinterInfoDetail;)[I

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/screen/PrinterInfoDetail;->access$102(Lepson/print/screen/PrinterInfoDetail;[I)[I

    goto/16 :goto_2

    .line 325
    :cond_2
    iget-object p1, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    new-array v1, v4, [I

    invoke-static {p1, v1}, Lepson/print/screen/PrinterInfoDetail;->access$102(Lepson/print/screen/PrinterInfoDetail;[I)[I

    .line 327
    iget-object p1, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    invoke-static {p1}, Lepson/print/screen/PrinterInfoDetail;->access$100(Lepson/print/screen/PrinterInfoDetail;)[I

    move-result-object p1

    iget v0, v0, Lepson/print/screen/PrintSetting;->colorValue:I

    aput v0, p1, v3

    goto/16 :goto_2

    .line 302
    :pswitch_3
    iget-object p1, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    invoke-static {p1}, Lepson/print/screen/PrinterInfoDetail;->access$500(Lepson/print/screen/PrinterInfoDetail;)[I

    move-result-object p1

    if-eqz p1, :cond_3

    .line 303
    iget-object p1, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    invoke-static {p1}, Lepson/print/screen/PrinterInfoDetail;->access$500(Lepson/print/screen/PrinterInfoDetail;)[I

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    invoke-static {p1, v0}, Lepson/print/screen/PrinterInfoDetail;->access$102(Lepson/print/screen/PrinterInfoDetail;[I)[I

    .line 304
    iget-object p1, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    invoke-static {p1}, Lepson/print/screen/PrinterInfoDetail;->access$500(Lepson/print/screen/PrinterInfoDetail;)[I

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/screen/PrinterInfoDetail;->access$102(Lepson/print/screen/PrinterInfoDetail;[I)[I

    goto/16 :goto_2

    .line 306
    :cond_3
    iget-object p1, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    new-array v1, v4, [I

    invoke-static {p1, v1}, Lepson/print/screen/PrinterInfoDetail;->access$102(Lepson/print/screen/PrinterInfoDetail;[I)[I

    .line 308
    iget-object p1, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    invoke-static {p1}, Lepson/print/screen/PrinterInfoDetail;->access$100(Lepson/print/screen/PrinterInfoDetail;)[I

    move-result-object p1

    iget v0, v0, Lepson/print/screen/PrintSetting;->paperSourceValue:I

    aput v0, p1, v3

    goto/16 :goto_2

    :pswitch_4
    const-string p1, "CMV"

    const-string v1, "Get Quality"

    .line 290
    invoke-static {p1, v1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    iget-object p1, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    invoke-static {p1}, Lepson/print/screen/PrinterInfoDetail;->access$400(Lepson/print/screen/PrinterInfoDetail;)[I

    move-result-object p1

    if-eqz p1, :cond_4

    .line 292
    iget-object p1, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    invoke-static {p1}, Lepson/print/screen/PrinterInfoDetail;->access$400(Lepson/print/screen/PrinterInfoDetail;)[I

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    invoke-static {p1, v0}, Lepson/print/screen/PrinterInfoDetail;->access$102(Lepson/print/screen/PrinterInfoDetail;[I)[I

    .line 293
    iget-object p1, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    invoke-static {p1}, Lepson/print/screen/PrinterInfoDetail;->access$400(Lepson/print/screen/PrinterInfoDetail;)[I

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/screen/PrinterInfoDetail;->access$102(Lepson/print/screen/PrinterInfoDetail;[I)[I

    goto/16 :goto_2

    .line 295
    :cond_4
    iget-object p1, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    new-array v1, v4, [I

    invoke-static {p1, v1}, Lepson/print/screen/PrinterInfoDetail;->access$102(Lepson/print/screen/PrinterInfoDetail;[I)[I

    .line 297
    iget-object p1, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    invoke-static {p1}, Lepson/print/screen/PrinterInfoDetail;->access$100(Lepson/print/screen/PrinterInfoDetail;)[I

    move-result-object p1

    iget v0, v0, Lepson/print/screen/PrintSetting;->qualityValue:I

    aput v0, p1, v3

    goto/16 :goto_2

    .line 263
    :pswitch_5
    iget-object p1, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    invoke-static {p1}, Lepson/print/screen/PrinterInfoDetail;->access$300(Lepson/print/screen/PrinterInfoDetail;)[I

    move-result-object p1

    if-eqz p1, :cond_7

    .line 264
    iget-object p1, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    invoke-static {p1}, Lepson/print/screen/PrinterInfoDetail;->access$300(Lepson/print/screen/PrinterInfoDetail;)[I

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    invoke-static {p1, v0}, Lepson/print/screen/PrinterInfoDetail;->access$102(Lepson/print/screen/PrinterInfoDetail;[I)[I

    .line 265
    iget-object p1, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    invoke-static {p1}, Lepson/print/screen/PrinterInfoDetail;->access$300(Lepson/print/screen/PrinterInfoDetail;)[I

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/screen/PrinterInfoDetail;->access$102(Lepson/print/screen/PrinterInfoDetail;[I)[I

    const/4 p1, 0x0

    .line 267
    :goto_1
    iget-object v0, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    invoke-static {v0}, Lepson/print/screen/PrinterInfoDetail;->access$100(Lepson/print/screen/PrinterInfoDetail;)[I

    move-result-object v0

    array-length v0, v0

    if-ge p1, v0, :cond_b

    .line 268
    iget-object v0, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    invoke-static {v0}, Lepson/print/screen/PrinterInfoDetail;->access$100(Lepson/print/screen/PrinterInfoDetail;)[I

    move-result-object v0

    aget v0, v0, p1

    const/16 v1, 0x8

    if-ne v0, v1, :cond_6

    .line 269
    iget-object v0, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    invoke-static {v0}, Lepson/print/screen/PrinterInfoDetail;->access$100(Lepson/print/screen/PrinterInfoDetail;)[I

    move-result-object v0

    array-length v0, v0

    const/4 v1, 0x0

    if-ne v0, v4, :cond_5

    .line 270
    iget-object p1, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    invoke-static {p1, v1}, Lepson/print/screen/PrinterInfoDetail;->access$102(Lepson/print/screen/PrinterInfoDetail;[I)[I

    goto/16 :goto_2

    .line 273
    :cond_5
    iget-object v0, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    invoke-static {v0}, Lepson/print/screen/PrinterInfoDetail;->access$100(Lepson/print/screen/PrinterInfoDetail;)[I

    move-result-object v0

    array-length v0, v0

    sub-int/2addr v0, v4

    new-array v0, v0, [I

    .line 274
    iget-object v2, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    invoke-static {v2}, Lepson/print/screen/PrinterInfoDetail;->access$100(Lepson/print/screen/PrinterInfoDetail;)[I

    move-result-object v2

    invoke-static {v2, v3, v0, v3, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 275
    iget-object v2, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    invoke-static {v2}, Lepson/print/screen/PrinterInfoDetail;->access$100(Lepson/print/screen/PrinterInfoDetail;)[I

    move-result-object v2

    add-int/lit8 v3, p1, 0x1

    iget-object v5, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    invoke-static {v5}, Lepson/print/screen/PrinterInfoDetail;->access$100(Lepson/print/screen/PrinterInfoDetail;)[I

    move-result-object v5

    array-length v5, v5

    sub-int/2addr v5, v4

    sub-int/2addr v5, p1

    invoke-static {v2, v3, v0, p1, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 276
    iget-object p1, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    invoke-static {p1, v1}, Lepson/print/screen/PrinterInfoDetail;->access$102(Lepson/print/screen/PrinterInfoDetail;[I)[I

    .line 277
    iget-object p1, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    invoke-static {p1, v0}, Lepson/print/screen/PrinterInfoDetail;->access$102(Lepson/print/screen/PrinterInfoDetail;[I)[I

    goto/16 :goto_2

    :cond_6
    add-int/lit8 p1, p1, 0x1

    goto :goto_1

    .line 283
    :cond_7
    iget-object p1, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    new-array v1, v4, [I

    invoke-static {p1, v1}, Lepson/print/screen/PrinterInfoDetail;->access$102(Lepson/print/screen/PrinterInfoDetail;[I)[I

    .line 285
    iget-object p1, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    invoke-static {p1}, Lepson/print/screen/PrinterInfoDetail;->access$100(Lepson/print/screen/PrinterInfoDetail;)[I

    move-result-object p1

    iget v0, v0, Lepson/print/screen/PrintSetting;->layoutValue:I

    aput v0, p1, v3

    goto/16 :goto_2

    .line 252
    :pswitch_6
    iget-object p1, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    invoke-static {p1}, Lepson/print/screen/PrinterInfoDetail;->access$200(Lepson/print/screen/PrinterInfoDetail;)[I

    move-result-object p1

    if-eqz p1, :cond_8

    .line 253
    iget-object p1, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    invoke-static {p1}, Lepson/print/screen/PrinterInfoDetail;->access$200(Lepson/print/screen/PrinterInfoDetail;)[I

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    invoke-static {p1, v0}, Lepson/print/screen/PrinterInfoDetail;->access$102(Lepson/print/screen/PrinterInfoDetail;[I)[I

    .line 254
    iget-object p1, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    invoke-static {p1}, Lepson/print/screen/PrinterInfoDetail;->access$200(Lepson/print/screen/PrinterInfoDetail;)[I

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/screen/PrinterInfoDetail;->access$102(Lepson/print/screen/PrinterInfoDetail;[I)[I

    goto :goto_2

    .line 256
    :cond_8
    iget-object p1, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    new-array v1, v4, [I

    invoke-static {p1, v1}, Lepson/print/screen/PrinterInfoDetail;->access$102(Lepson/print/screen/PrinterInfoDetail;[I)[I

    .line 258
    iget-object p1, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    invoke-static {p1}, Lepson/print/screen/PrinterInfoDetail;->access$100(Lepson/print/screen/PrinterInfoDetail;)[I

    move-result-object p1

    iget v0, v0, Lepson/print/screen/PrintSetting;->paperTypeValue:I

    aput v0, p1, v3

    goto :goto_2

    .line 241
    :pswitch_7
    iget-object p1, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    invoke-static {p1}, Lepson/print/screen/PrinterInfoDetail;->access$000(Lepson/print/screen/PrinterInfoDetail;)[I

    move-result-object p1

    if-eqz p1, :cond_9

    .line 242
    iget-object p1, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    invoke-static {p1}, Lepson/print/screen/PrinterInfoDetail;->access$000(Lepson/print/screen/PrinterInfoDetail;)[I

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    invoke-static {p1, v0}, Lepson/print/screen/PrinterInfoDetail;->access$102(Lepson/print/screen/PrinterInfoDetail;[I)[I

    .line 243
    iget-object p1, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    invoke-static {p1}, Lepson/print/screen/PrinterInfoDetail;->access$000(Lepson/print/screen/PrinterInfoDetail;)[I

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/screen/PrinterInfoDetail;->access$102(Lepson/print/screen/PrinterInfoDetail;[I)[I

    goto :goto_2

    .line 245
    :cond_9
    iget-object p1, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    new-array v1, v4, [I

    invoke-static {p1, v1}, Lepson/print/screen/PrinterInfoDetail;->access$102(Lepson/print/screen/PrinterInfoDetail;[I)[I

    .line 247
    iget-object p1, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    invoke-static {p1}, Lepson/print/screen/PrinterInfoDetail;->access$100(Lepson/print/screen/PrinterInfoDetail;)[I

    move-result-object p1

    iget v0, v0, Lepson/print/screen/PrintSetting;->paperSizeValue:I

    aput v0, p1, v3

    goto :goto_2

    .line 313
    :cond_a
    iget-object p1, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    const/4 v0, 0x4

    new-array v0, v0, [I

    invoke-static {p1, v0}, Lepson/print/screen/PrinterInfoDetail;->access$102(Lepson/print/screen/PrinterInfoDetail;[I)[I

    .line 314
    iget-object p1, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    invoke-static {p1}, Lepson/print/screen/PrinterInfoDetail;->access$100(Lepson/print/screen/PrinterInfoDetail;)[I

    move-result-object p1

    aput v3, p1, v3

    .line 315
    iget-object p1, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    invoke-static {p1}, Lepson/print/screen/PrinterInfoDetail;->access$100(Lepson/print/screen/PrinterInfoDetail;)[I

    move-result-object p1

    aput v4, p1, v4

    .line 316
    iget-object p1, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    invoke-static {p1}, Lepson/print/screen/PrinterInfoDetail;->access$100(Lepson/print/screen/PrinterInfoDetail;)[I

    move-result-object p1

    aput v2, p1, v2

    .line 317
    iget-object p1, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    invoke-static {p1}, Lepson/print/screen/PrinterInfoDetail;->access$100(Lepson/print/screen/PrinterInfoDetail;)[I

    move-result-object p1

    const/4 v0, 0x3

    aput v0, p1, v0

    .line 349
    :cond_b
    :goto_2
    iget-object p1, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    invoke-static {p1}, Lepson/print/screen/PrinterInfoDetail;->access$100(Lepson/print/screen/PrinterInfoDetail;)[I

    move-result-object p1

    if-eqz p1, :cond_c

    .line 350
    iget-object p1, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    iget-object p1, p1, Lepson/print/screen/PrinterInfoDetail;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    check-cast p1, Lepson/print/widgets/PrinterInfoDetailBuilder;

    iget-object v0, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    invoke-static {v0}, Lepson/print/screen/PrinterInfoDetail;->access$100(Lepson/print/screen/PrinterInfoDetail;)[I

    move-result-object v0

    iget-object v1, p0, Lepson/print/screen/PrinterInfoDetail$2;->this$0:Lepson/print/screen/PrinterInfoDetail;

    iget-object v1, v1, Lepson/print/screen/PrinterInfoDetail;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {p1, v0, v1}, Lepson/print/widgets/PrinterInfoDetailBuilder;->addPrinterInfo([ILcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;)V

    :cond_c
    return v4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

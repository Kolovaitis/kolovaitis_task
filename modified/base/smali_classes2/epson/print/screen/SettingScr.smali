.class public Lepson/print/screen/SettingScr;
.super Lepson/print/ActivityIACommon;
.source "SettingScr.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final EXTRA_PRINTER:Ljava/lang/String; = "myprinter"

.field private static final EXTRA_SIMPLEAP:Ljava/lang/String; = "simpleap"

.field private static final REQUEST_CODE_INFO:I = 0x1

.field private static final REQUEST_CODE_INK_REPLENISH_PROGRESS:I = 0x4

.field private static final REQUEST_CODE_PAGE_RANGE:I = 0x2

.field private static final REQUEST_CODE_PRINTER:I = 0x0

.field public static final TAG:Ljava/lang/String; = "PrintSetting"


# instance fields
.field private final COLOR_VALUE_MAX:I

.field private final COLOR_VALUE_MIN:I

.field private final COPIES_MAX:I

.field private final COPIES_MIN:I

.field private final EPS_ERR_COMM_ERROR:I

.field private final EPS_ERR_NONE:I

.field private final EPS_ERR_OPR_FAIL:I

.field private final EPS_ERR_PRINTER_NOT_SET:I

.field private final GET_ADVANCED:I

.field private final GET_COLOR:I

.field private final GET_DUPLEX:I

.field private final GET_LAYOUT:I

.field private final GET_PAPER_SIZE:I

.field private final GET_PAPER_SOURCE:I

.field private final GET_PAPER_TYPE:I

.field private final GET_QUALITY:I

.field private final GET_SUPPORTED_MEDIA:I

.field private final PROBE_PRINTER:I

.field private final SEARCH_BY_ID:I

.field private final SEARCH_ERROR:I

.field private final SETTING_DONE:I

.field private final SHOW_ERROR_DIALOG:I

.field private apfStateText:Landroid/widget/TextView;

.field private autoConnectSsid:Ljava/lang/String;

.field private brightness:Landroid/widget/TextView;

.field private brightnessMinus:Landroid/widget/Button;

.field private brightnessPlus:Landroid/widget/Button;

.field private brightnessValue:I

.field private color:I

.field private colorInfo:Landroid/widget/TextView;

.field private color_info:[I

.field private contrast:Landroid/widget/TextView;

.field private contrastMinus:Landroid/widget/Button;

.field private contrastPlus:Landroid/widget/Button;

.field private contrastValue:I

.field private copies:Landroid/widget/TextView;

.field private copiesMinus:Landroid/widget/Button;

.field private copiesPlus:Landroid/widget/Button;

.field private copiesValue:I

.field private disablePrintArea:Z

.field private duplex:I

.field private duplexInfo:Landroid/widget/TextView;

.field private duplex_info:[I

.field private enableShowPreview:Z

.field private endValue:I

.field private feedDirection:I

.field private feedDirectionInfo:Landroid/widget/TextView;

.field private info:[I

.field private isDocumentSetting:Z

.field isInfoAvai:Z

.field isNotLoading:Ljava/lang/Boolean;

.field private isRetryAfterConnectSimpleAp:Z

.field private isTryConnectSimpleAp:Z

.field private lang:I

.field private layout:I

.field private layoutInfo:Landroid/widget/TextView;

.field private layout_info:[I

.field private mCallback:Lepson/print/service/IEpsonServiceCallback;

.field private mContext:Landroid/content/Context;

.field private mEpsonConnection:Landroid/content/ServiceConnection;

.field private mEpsonService:Lepson/print/service/IEpsonService;

.field private volatile mFinishRequested:Z

.field mHandler:Landroid/os/Handler;

.field private mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

.field private mPaperSizeType:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

.field private mPrinterSelectDone:Z

.field private mSharpnessValue:I

.field private mSharpnessValueText:Landroid/widget/TextView;

.field private mWaiteInkReplenProgress:Z

.field private paperSize:I

.field private paperSizeInfo:Landroid/widget/TextView;

.field private paperSource:I

.field private paperSourceInfo:Landroid/widget/TextView;

.field private paperType:I

.field private paperTypeInfo:Landroid/widget/TextView;

.field private paper_size_info:[I

.field private paper_source_info:[I

.field private paper_type_info:[I

.field private photoApfValueForLocalPrinter:I

.field private printAll:Z

.field private printDateInfo:Landroid/widget/TextView;

.field private printdate:I

.field private printer:Lepson/print/MyPrinter;

.field private printerDeviceId:Ljava/lang/String;

.field private printerEmailAddress:Ljava/lang/String;

.field private printerId:Ljava/lang/String;

.field private printerIp:Ljava/lang/String;

.field private printerLocation:I

.field private printerName:Landroid/widget/TextView;

.field private printerSerialNo:Ljava/lang/String;

.field private progressGetOption:Landroid/view/View;

.field private quality:I

.field private qualityInfo:Landroid/widget/TextView;

.field private quality_info:[I

.field private saturation:Landroid/widget/TextView;

.field private saturationMinus:Landroid/widget/Button;

.field private saturationPlus:Landroid/widget/Button;

.field private saturationValue:I

.field private sheets:I

.field private sizeIndex:I

.field private startValue:I

.field private typeIndex:I

.field undoFlag:Z


# direct methods
.method public constructor <init>()V
    .locals 5

    .line 66
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    const/4 v0, 0x0

    .line 69
    iput v0, p0, Lepson/print/screen/SettingScr;->EPS_ERR_NONE:I

    const/16 v1, -0x3e8

    .line 70
    iput v1, p0, Lepson/print/screen/SettingScr;->EPS_ERR_OPR_FAIL:I

    const/16 v1, -0x44c

    .line 71
    iput v1, p0, Lepson/print/screen/SettingScr;->EPS_ERR_COMM_ERROR:I

    const/16 v1, -0x547

    .line 72
    iput v1, p0, Lepson/print/screen/SettingScr;->EPS_ERR_PRINTER_NOT_SET:I

    const/16 v1, -0x32

    .line 77
    iput v1, p0, Lepson/print/screen/SettingScr;->COLOR_VALUE_MIN:I

    const/16 v1, 0x32

    .line 78
    iput v1, p0, Lepson/print/screen/SettingScr;->COLOR_VALUE_MAX:I

    const/4 v1, 0x1

    .line 102
    iput v1, p0, Lepson/print/screen/SettingScr;->lang:I

    .line 112
    iput-boolean v1, p0, Lepson/print/screen/SettingScr;->isDocumentSetting:Z

    .line 135
    iput v1, p0, Lepson/print/screen/SettingScr;->sheets:I

    const/4 v2, 0x3

    .line 136
    iput v2, p0, Lepson/print/screen/SettingScr;->SETTING_DONE:I

    .line 137
    iput v1, p0, Lepson/print/screen/SettingScr;->COPIES_MIN:I

    const/16 v3, 0x1e

    .line 138
    iput v3, p0, Lepson/print/screen/SettingScr;->COPIES_MAX:I

    .line 140
    iput-boolean v0, p0, Lepson/print/screen/SettingScr;->isInfoAvai:Z

    .line 143
    iput-boolean v0, p0, Lepson/print/screen/SettingScr;->disablePrintArea:Z

    const/4 v3, 0x0

    .line 144
    iput-object v3, p0, Lepson/print/screen/SettingScr;->printer:Lepson/print/MyPrinter;

    .line 146
    iput-boolean v0, p0, Lepson/print/screen/SettingScr;->isTryConnectSimpleAp:Z

    .line 147
    iput-boolean v0, p0, Lepson/print/screen/SettingScr;->isRetryAfterConnectSimpleAp:Z

    .line 148
    iput-object v3, p0, Lepson/print/screen/SettingScr;->autoConnectSsid:Ljava/lang/String;

    .line 151
    iput-object v3, p0, Lepson/print/screen/SettingScr;->mContext:Landroid/content/Context;

    .line 153
    iput-boolean v0, p0, Lepson/print/screen/SettingScr;->enableShowPreview:Z

    .line 530
    iput-boolean v1, p0, Lepson/print/screen/SettingScr;->undoFlag:Z

    .line 1620
    iput-object v3, p0, Lepson/print/screen/SettingScr;->mEpsonService:Lepson/print/service/IEpsonService;

    .line 1621
    new-instance v4, Lepson/print/screen/SettingScr$2;

    invoke-direct {v4, p0}, Lepson/print/screen/SettingScr$2;-><init>(Lepson/print/screen/SettingScr;)V

    iput-object v4, p0, Lepson/print/screen/SettingScr;->mEpsonConnection:Landroid/content/ServiceConnection;

    .line 1649
    iput v0, p0, Lepson/print/screen/SettingScr;->GET_SUPPORTED_MEDIA:I

    .line 1650
    iput v1, p0, Lepson/print/screen/SettingScr;->GET_PAPER_SIZE:I

    const/4 v4, 0x2

    .line 1651
    iput v4, p0, Lepson/print/screen/SettingScr;->GET_PAPER_TYPE:I

    .line 1652
    iput v2, p0, Lepson/print/screen/SettingScr;->GET_LAYOUT:I

    const/4 v2, 0x4

    .line 1653
    iput v2, p0, Lepson/print/screen/SettingScr;->GET_QUALITY:I

    const/4 v2, 0x5

    .line 1654
    iput v2, p0, Lepson/print/screen/SettingScr;->GET_PAPER_SOURCE:I

    const/4 v2, 0x6

    .line 1655
    iput v2, p0, Lepson/print/screen/SettingScr;->GET_COLOR:I

    const/16 v2, 0x20

    .line 1656
    iput v2, p0, Lepson/print/screen/SettingScr;->GET_DUPLEX:I

    const/16 v2, 0x40

    .line 1657
    iput v2, p0, Lepson/print/screen/SettingScr;->GET_ADVANCED:I

    const/4 v2, 0x7

    .line 1658
    iput v2, p0, Lepson/print/screen/SettingScr;->SEARCH_BY_ID:I

    const/16 v2, 0x10

    .line 1659
    iput v2, p0, Lepson/print/screen/SettingScr;->SEARCH_ERROR:I

    const/16 v2, 0x11

    .line 1660
    iput v2, p0, Lepson/print/screen/SettingScr;->PROBE_PRINTER:I

    const/16 v2, 0x12

    .line 1661
    iput v2, p0, Lepson/print/screen/SettingScr;->SHOW_ERROR_DIALOG:I

    .line 1663
    iput v0, p0, Lepson/print/screen/SettingScr;->sizeIndex:I

    iput v0, p0, Lepson/print/screen/SettingScr;->typeIndex:I

    .line 1664
    iput-object v3, p0, Lepson/print/screen/SettingScr;->info:[I

    .line 1665
    iput-object v3, p0, Lepson/print/screen/SettingScr;->paper_source_info:[I

    .line 1666
    iput-object v3, p0, Lepson/print/screen/SettingScr;->color_info:[I

    .line 1667
    iput-object v3, p0, Lepson/print/screen/SettingScr;->paper_size_info:[I

    .line 1668
    iput-object v3, p0, Lepson/print/screen/SettingScr;->paper_type_info:[I

    .line 1669
    iput-object v3, p0, Lepson/print/screen/SettingScr;->layout_info:[I

    .line 1670
    iput-object v3, p0, Lepson/print/screen/SettingScr;->quality_info:[I

    .line 1671
    iput-object v3, p0, Lepson/print/screen/SettingScr;->duplex_info:[I

    .line 1672
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lepson/print/screen/SettingScr;->isNotLoading:Ljava/lang/Boolean;

    .line 1673
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lepson/print/screen/SettingScr$3;

    invoke-direct {v1, p0}, Lepson/print/screen/SettingScr$3;-><init>(Lepson/print/screen/SettingScr;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lepson/print/screen/SettingScr;->mHandler:Landroid/os/Handler;

    .line 2407
    new-instance v0, Lepson/print/screen/SettingScr$4;

    invoke-direct {v0, p0}, Lepson/print/screen/SettingScr$4;-><init>(Lepson/print/screen/SettingScr;)V

    iput-object v0, p0, Lepson/print/screen/SettingScr;->mCallback:Lepson/print/service/IEpsonServiceCallback;

    return-void
.end method

.method static synthetic access$000(Lepson/print/screen/SettingScr;)Ljava/lang/String;
    .locals 0

    .line 66
    iget-object p0, p0, Lepson/print/screen/SettingScr;->printerIp:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$002(Lepson/print/screen/SettingScr;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 66
    iput-object p1, p0, Lepson/print/screen/SettingScr;->printerIp:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$100(Lepson/print/screen/SettingScr;)Ljava/lang/String;
    .locals 0

    .line 66
    iget-object p0, p0, Lepson/print/screen/SettingScr;->autoConnectSsid:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1000(Lepson/print/screen/SettingScr;)Landroid/content/Context;
    .locals 0

    .line 66
    iget-object p0, p0, Lepson/print/screen/SettingScr;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic access$1100(Lepson/print/screen/SettingScr;)[I
    .locals 0

    .line 66
    iget-object p0, p0, Lepson/print/screen/SettingScr;->info:[I

    return-object p0
.end method

.method static synthetic access$1102(Lepson/print/screen/SettingScr;[I)[I
    .locals 0

    .line 66
    iput-object p1, p0, Lepson/print/screen/SettingScr;->info:[I

    return-object p1
.end method

.method static synthetic access$1200(Lepson/print/screen/SettingScr;)I
    .locals 0

    .line 66
    iget p0, p0, Lepson/print/screen/SettingScr;->sizeIndex:I

    return p0
.end method

.method static synthetic access$1202(Lepson/print/screen/SettingScr;I)I
    .locals 0

    .line 66
    iput p1, p0, Lepson/print/screen/SettingScr;->sizeIndex:I

    return p1
.end method

.method static synthetic access$1208(Lepson/print/screen/SettingScr;)I
    .locals 2

    .line 66
    iget v0, p0, Lepson/print/screen/SettingScr;->sizeIndex:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lepson/print/screen/SettingScr;->sizeIndex:I

    return v0
.end method

.method static synthetic access$1300(Lepson/print/screen/SettingScr;)I
    .locals 0

    .line 66
    iget p0, p0, Lepson/print/screen/SettingScr;->paperSize:I

    return p0
.end method

.method static synthetic access$1302(Lepson/print/screen/SettingScr;I)I
    .locals 0

    .line 66
    iput p1, p0, Lepson/print/screen/SettingScr;->paperSize:I

    return p1
.end method

.method static synthetic access$1402(Lepson/print/screen/SettingScr;[I)[I
    .locals 0

    .line 66
    iput-object p1, p0, Lepson/print/screen/SettingScr;->paper_size_info:[I

    return-object p1
.end method

.method static synthetic access$1500(Lepson/print/screen/SettingScr;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;
    .locals 0

    .line 66
    iget-object p0, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    return-object p0
.end method

.method static synthetic access$1502(Lepson/print/screen/SettingScr;Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;
    .locals 0

    .line 66
    iput-object p1, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    return-object p1
.end method

.method static synthetic access$1600(Lepson/print/screen/SettingScr;)Landroid/widget/TextView;
    .locals 0

    .line 66
    iget-object p0, p0, Lepson/print/screen/SettingScr;->paperSizeInfo:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$1700(Lepson/print/screen/SettingScr;)I
    .locals 0

    .line 66
    iget p0, p0, Lepson/print/screen/SettingScr;->typeIndex:I

    return p0
.end method

.method static synthetic access$1702(Lepson/print/screen/SettingScr;I)I
    .locals 0

    .line 66
    iput p1, p0, Lepson/print/screen/SettingScr;->typeIndex:I

    return p1
.end method

.method static synthetic access$1708(Lepson/print/screen/SettingScr;)I
    .locals 2

    .line 66
    iget v0, p0, Lepson/print/screen/SettingScr;->typeIndex:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lepson/print/screen/SettingScr;->typeIndex:I

    return v0
.end method

.method static synthetic access$1800(Lepson/print/screen/SettingScr;)I
    .locals 0

    .line 66
    iget p0, p0, Lepson/print/screen/SettingScr;->paperType:I

    return p0
.end method

.method static synthetic access$1802(Lepson/print/screen/SettingScr;I)I
    .locals 0

    .line 66
    iput p1, p0, Lepson/print/screen/SettingScr;->paperType:I

    return p1
.end method

.method static synthetic access$1902(Lepson/print/screen/SettingScr;[I)[I
    .locals 0

    .line 66
    iput-object p1, p0, Lepson/print/screen/SettingScr;->paper_type_info:[I

    return-object p1
.end method

.method static synthetic access$200(Lepson/print/screen/SettingScr;)Ljava/lang/String;
    .locals 0

    .line 66
    iget-object p0, p0, Lepson/print/screen/SettingScr;->printerId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$2000(Lepson/print/screen/SettingScr;)Landroid/widget/TextView;
    .locals 0

    .line 66
    iget-object p0, p0, Lepson/print/screen/SettingScr;->paperTypeInfo:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$2100(Lepson/print/screen/SettingScr;)I
    .locals 0

    .line 66
    iget p0, p0, Lepson/print/screen/SettingScr;->layout:I

    return p0
.end method

.method static synthetic access$2102(Lepson/print/screen/SettingScr;I)I
    .locals 0

    .line 66
    iput p1, p0, Lepson/print/screen/SettingScr;->layout:I

    return p1
.end method

.method static synthetic access$2200(Lepson/print/screen/SettingScr;)[I
    .locals 0

    .line 66
    iget-object p0, p0, Lepson/print/screen/SettingScr;->layout_info:[I

    return-object p0
.end method

.method static synthetic access$2202(Lepson/print/screen/SettingScr;[I)[I
    .locals 0

    .line 66
    iput-object p1, p0, Lepson/print/screen/SettingScr;->layout_info:[I

    return-object p1
.end method

.method static synthetic access$2300(Lepson/print/screen/SettingScr;)Landroid/widget/TextView;
    .locals 0

    .line 66
    iget-object p0, p0, Lepson/print/screen/SettingScr;->layoutInfo:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$2400(Lepson/print/screen/SettingScr;)I
    .locals 0

    .line 66
    iget p0, p0, Lepson/print/screen/SettingScr;->quality:I

    return p0
.end method

.method static synthetic access$2402(Lepson/print/screen/SettingScr;I)I
    .locals 0

    .line 66
    iput p1, p0, Lepson/print/screen/SettingScr;->quality:I

    return p1
.end method

.method static synthetic access$2502(Lepson/print/screen/SettingScr;[I)[I
    .locals 0

    .line 66
    iput-object p1, p0, Lepson/print/screen/SettingScr;->quality_info:[I

    return-object p1
.end method

.method static synthetic access$2600(Lepson/print/screen/SettingScr;)Landroid/widget/TextView;
    .locals 0

    .line 66
    iget-object p0, p0, Lepson/print/screen/SettingScr;->qualityInfo:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$2700(Lepson/print/screen/SettingScr;)[I
    .locals 0

    .line 66
    iget-object p0, p0, Lepson/print/screen/SettingScr;->paper_source_info:[I

    return-object p0
.end method

.method static synthetic access$2702(Lepson/print/screen/SettingScr;[I)[I
    .locals 0

    .line 66
    iput-object p1, p0, Lepson/print/screen/SettingScr;->paper_source_info:[I

    return-object p1
.end method

.method static synthetic access$2800(Lepson/print/screen/SettingScr;)I
    .locals 0

    .line 66
    iget p0, p0, Lepson/print/screen/SettingScr;->paperSource:I

    return p0
.end method

.method static synthetic access$2802(Lepson/print/screen/SettingScr;I)I
    .locals 0

    .line 66
    iput p1, p0, Lepson/print/screen/SettingScr;->paperSource:I

    return p1
.end method

.method static synthetic access$2900(Lepson/print/screen/SettingScr;)Landroid/widget/TextView;
    .locals 0

    .line 66
    iget-object p0, p0, Lepson/print/screen/SettingScr;->paperSourceInfo:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$300(Lepson/print/screen/SettingScr;)Ljava/lang/String;
    .locals 0

    .line 66
    iget-object p0, p0, Lepson/print/screen/SettingScr;->printerEmailAddress:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$3000(Lepson/print/screen/SettingScr;)[I
    .locals 0

    .line 66
    iget-object p0, p0, Lepson/print/screen/SettingScr;->color_info:[I

    return-object p0
.end method

.method static synthetic access$3002(Lepson/print/screen/SettingScr;[I)[I
    .locals 0

    .line 66
    iput-object p1, p0, Lepson/print/screen/SettingScr;->color_info:[I

    return-object p1
.end method

.method static synthetic access$3100(Lepson/print/screen/SettingScr;)I
    .locals 0

    .line 66
    iget p0, p0, Lepson/print/screen/SettingScr;->color:I

    return p0
.end method

.method static synthetic access$3102(Lepson/print/screen/SettingScr;I)I
    .locals 0

    .line 66
    iput p1, p0, Lepson/print/screen/SettingScr;->color:I

    return p1
.end method

.method static synthetic access$3200(Lepson/print/screen/SettingScr;)Landroid/widget/TextView;
    .locals 0

    .line 66
    iget-object p0, p0, Lepson/print/screen/SettingScr;->colorInfo:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$3300(Lepson/print/screen/SettingScr;)[I
    .locals 0

    .line 66
    iget-object p0, p0, Lepson/print/screen/SettingScr;->duplex_info:[I

    return-object p0
.end method

.method static synthetic access$3302(Lepson/print/screen/SettingScr;[I)[I
    .locals 0

    .line 66
    iput-object p1, p0, Lepson/print/screen/SettingScr;->duplex_info:[I

    return-object p1
.end method

.method static synthetic access$3400(Lepson/print/screen/SettingScr;)I
    .locals 0

    .line 66
    iget p0, p0, Lepson/print/screen/SettingScr;->duplex:I

    return p0
.end method

.method static synthetic access$3402(Lepson/print/screen/SettingScr;I)I
    .locals 0

    .line 66
    iput p1, p0, Lepson/print/screen/SettingScr;->duplex:I

    return p1
.end method

.method static synthetic access$3500(Lepson/print/screen/SettingScr;)Landroid/widget/TextView;
    .locals 0

    .line 66
    iget-object p0, p0, Lepson/print/screen/SettingScr;->duplexInfo:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$3600(Lepson/print/screen/SettingScr;)I
    .locals 0

    .line 66
    iget p0, p0, Lepson/print/screen/SettingScr;->startValue:I

    return p0
.end method

.method static synthetic access$3700(Lepson/print/screen/SettingScr;)I
    .locals 0

    .line 66
    iget p0, p0, Lepson/print/screen/SettingScr;->endValue:I

    return p0
.end method

.method static synthetic access$3800(Lepson/print/screen/SettingScr;)I
    .locals 0

    .line 66
    iget p0, p0, Lepson/print/screen/SettingScr;->feedDirection:I

    return p0
.end method

.method static synthetic access$3900(Lepson/print/screen/SettingScr;)Landroid/widget/TextView;
    .locals 0

    .line 66
    iget-object p0, p0, Lepson/print/screen/SettingScr;->feedDirectionInfo:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$400(Lepson/print/screen/SettingScr;)I
    .locals 0

    .line 66
    iget p0, p0, Lepson/print/screen/SettingScr;->printerLocation:I

    return p0
.end method

.method static synthetic access$4000(Lepson/print/screen/SettingScr;)I
    .locals 0

    .line 66
    iget p0, p0, Lepson/print/screen/SettingScr;->printdate:I

    return p0
.end method

.method static synthetic access$4100(Lepson/print/screen/SettingScr;)Landroid/widget/TextView;
    .locals 0

    .line 66
    iget-object p0, p0, Lepson/print/screen/SettingScr;->printDateInfo:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$4200(Lepson/print/screen/SettingScr;)I
    .locals 0

    .line 66
    iget p0, p0, Lepson/print/screen/SettingScr;->brightnessValue:I

    return p0
.end method

.method static synthetic access$4300(Lepson/print/screen/SettingScr;)Landroid/widget/TextView;
    .locals 0

    .line 66
    iget-object p0, p0, Lepson/print/screen/SettingScr;->brightness:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$4400(Lepson/print/screen/SettingScr;)Landroid/widget/Button;
    .locals 0

    .line 66
    iget-object p0, p0, Lepson/print/screen/SettingScr;->brightnessMinus:Landroid/widget/Button;

    return-object p0
.end method

.method static synthetic access$4500(Lepson/print/screen/SettingScr;)Landroid/widget/Button;
    .locals 0

    .line 66
    iget-object p0, p0, Lepson/print/screen/SettingScr;->brightnessPlus:Landroid/widget/Button;

    return-object p0
.end method

.method static synthetic access$4600(Lepson/print/screen/SettingScr;)I
    .locals 0

    .line 66
    iget p0, p0, Lepson/print/screen/SettingScr;->contrastValue:I

    return p0
.end method

.method static synthetic access$4700(Lepson/print/screen/SettingScr;)Landroid/widget/TextView;
    .locals 0

    .line 66
    iget-object p0, p0, Lepson/print/screen/SettingScr;->contrast:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$4800(Lepson/print/screen/SettingScr;)Landroid/widget/Button;
    .locals 0

    .line 66
    iget-object p0, p0, Lepson/print/screen/SettingScr;->contrastMinus:Landroid/widget/Button;

    return-object p0
.end method

.method static synthetic access$4900(Lepson/print/screen/SettingScr;)Landroid/widget/Button;
    .locals 0

    .line 66
    iget-object p0, p0, Lepson/print/screen/SettingScr;->contrastPlus:Landroid/widget/Button;

    return-object p0
.end method

.method static synthetic access$500(Lepson/print/screen/SettingScr;)Lepson/print/service/IEpsonServiceCallback;
    .locals 0

    .line 66
    iget-object p0, p0, Lepson/print/screen/SettingScr;->mCallback:Lepson/print/service/IEpsonServiceCallback;

    return-object p0
.end method

.method static synthetic access$5000(Lepson/print/screen/SettingScr;)I
    .locals 0

    .line 66
    iget p0, p0, Lepson/print/screen/SettingScr;->saturationValue:I

    return p0
.end method

.method static synthetic access$5100(Lepson/print/screen/SettingScr;)Landroid/widget/TextView;
    .locals 0

    .line 66
    iget-object p0, p0, Lepson/print/screen/SettingScr;->saturation:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$5200(Lepson/print/screen/SettingScr;)Landroid/widget/Button;
    .locals 0

    .line 66
    iget-object p0, p0, Lepson/print/screen/SettingScr;->saturationMinus:Landroid/widget/Button;

    return-object p0
.end method

.method static synthetic access$5300(Lepson/print/screen/SettingScr;)Landroid/widget/Button;
    .locals 0

    .line 66
    iget-object p0, p0, Lepson/print/screen/SettingScr;->saturationPlus:Landroid/widget/Button;

    return-object p0
.end method

.method static synthetic access$5400(Lepson/print/screen/SettingScr;Z)Z
    .locals 0

    .line 66
    invoke-direct {p0, p1}, Lepson/print/screen/SettingScr;->updateSupportedMediaFile(Z)Z

    move-result p0

    return p0
.end method

.method static synthetic access$5500(Lepson/print/screen/SettingScr;)Landroid/widget/TextView;
    .locals 0

    .line 66
    iget-object p0, p0, Lepson/print/screen/SettingScr;->printerName:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$5602(Lepson/print/screen/SettingScr;I)I
    .locals 0

    .line 66
    iput p1, p0, Lepson/print/screen/SettingScr;->lang:I

    return p1
.end method

.method static synthetic access$600(Lepson/print/screen/SettingScr;)Lepson/print/service/IEpsonService;
    .locals 0

    .line 66
    iget-object p0, p0, Lepson/print/screen/SettingScr;->mEpsonService:Lepson/print/service/IEpsonService;

    return-object p0
.end method

.method static synthetic access$602(Lepson/print/screen/SettingScr;Lepson/print/service/IEpsonService;)Lepson/print/service/IEpsonService;
    .locals 0

    .line 66
    iput-object p1, p0, Lepson/print/screen/SettingScr;->mEpsonService:Lepson/print/service/IEpsonService;

    return-object p1
.end method

.method static synthetic access$700(Lepson/print/screen/SettingScr;Ljava/lang/Boolean;)V
    .locals 0

    .line 66
    invoke-direct {p0, p1}, Lepson/print/screen/SettingScr;->setScreenState(Ljava/lang/Boolean;)V

    return-void
.end method

.method static synthetic access$800(Lepson/print/screen/SettingScr;)Z
    .locals 0

    .line 66
    iget-boolean p0, p0, Lepson/print/screen/SettingScr;->mFinishRequested:Z

    return p0
.end method

.method static synthetic access$900(Lepson/print/screen/SettingScr;)Z
    .locals 0

    .line 66
    iget-boolean p0, p0, Lepson/print/screen/SettingScr;->isDocumentSetting:Z

    return p0
.end method

.method private bindEpsonService()V
    .locals 3

    const-string v0, "SettingScr"

    const-string v1, "bindEpsonService"

    .line 825
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 828
    iget-object v0, p0, Lepson/print/screen/SettingScr;->mEpsonService:Lepson/print/service/IEpsonService;

    if-nez v0, :cond_0

    .line 829
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lepson/print/service/EpsonService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lepson/print/screen/SettingScr;->mEpsonConnection:Landroid/content/ServiceConnection;

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lepson/print/screen/SettingScr;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    :cond_0
    return-void
.end method

.method private displayApfState(I)V
    .locals 2

    .line 509
    new-instance v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$ApfState;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$ApfState;-><init>()V

    .line 510
    iget-object v1, p0, Lepson/print/screen/SettingScr;->apfStateText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result p1

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(I)V

    .line 511
    invoke-virtual {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    return-void
.end method

.method private displaySharpnessState(I)V
    .locals 2

    .line 516
    new-instance v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$SharpnessState;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$SharpnessState;-><init>()V

    .line 517
    iget-object v1, p0, Lepson/print/screen/SettingScr;->mSharpnessValueText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$SharpnessState;->getStringId(I)I

    move-result p1

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(I)V

    .line 518
    invoke-virtual {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$SharpnessState;->destructor()V

    return-void
.end method

.method private endInkReplAndGoProbePrinter()V
    .locals 4

    const/4 v0, 0x0

    .line 1420
    iput-boolean v0, p0, Lepson/print/screen/SettingScr;->mWaiteInkReplenProgress:Z

    .line 1422
    invoke-direct {p0}, Lepson/print/screen/SettingScr;->bindEpsonService()V

    .line 1425
    iget-object v0, p0, Lepson/print/screen/SettingScr;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x11

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method

.method private loadConfig()V
    .locals 7

    .line 325
    invoke-static {p0}, Lepson/print/MyPrinter;->getCurPrinter(Landroid/content/Context;)Lepson/print/MyPrinter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 326
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 328
    iget-object v1, p0, Lepson/print/screen/SettingScr;->printerName:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Lepson/print/MyPrinter;->getUserDefName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 330
    :cond_0
    iget-object v1, p0, Lepson/print/screen/SettingScr;->printerName:Landroid/widget/TextView;

    const v2, 0x7f0e04d6

    invoke-virtual {p0, v2}, Lepson/print/screen/SettingScr;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 332
    :goto_0
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lepson/print/screen/SettingScr;->printerDeviceId:Ljava/lang/String;

    .line 333
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lepson/print/screen/SettingScr;->printerId:Ljava/lang/String;

    .line 334
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lepson/print/screen/SettingScr;->printerIp:Ljava/lang/String;

    .line 335
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getSerialNo()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lepson/print/screen/SettingScr;->printerSerialNo:Ljava/lang/String;

    .line 336
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getEmailAddress()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lepson/print/screen/SettingScr;->printerEmailAddress:Ljava/lang/String;

    .line 337
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getLocation()I

    move-result v1

    iput v1, p0, Lepson/print/screen/SettingScr;->printerLocation:I

    .line 338
    iget v1, p0, Lepson/print/screen/SettingScr;->printerLocation:I

    const/4 v2, 0x1

    if-nez v1, :cond_1

    .line 339
    iput v2, p0, Lepson/print/screen/SettingScr;->printerLocation:I

    :cond_1
    const-string v1, "printer"

    .line 343
    invoke-static {p0, v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->getConnectInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lepson/print/screen/SettingScr;->autoConnectSsid:Ljava/lang/String;

    .line 346
    new-instance v1, Lepson/print/screen/PrintSetting;

    iget-boolean v3, p0, Lepson/print/screen/SettingScr;->isDocumentSetting:Z

    if-eqz v3, :cond_2

    sget-object v3, Lepson/print/screen/PrintSetting$Kind;->document:Lepson/print/screen/PrintSetting$Kind;

    goto :goto_1

    :cond_2
    sget-object v3, Lepson/print/screen/PrintSetting$Kind;->photo:Lepson/print/screen/PrintSetting$Kind;

    :goto_1
    invoke-direct {v1, p0, v3}, Lepson/print/screen/PrintSetting;-><init>(Landroid/content/Context;Lepson/print/screen/PrintSetting$Kind;)V

    .line 347
    invoke-virtual {v1}, Lepson/print/screen/PrintSetting;->loadSettings()V

    .line 354
    new-instance v3, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize;

    invoke-direct {v3}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize;-><init>()V

    iput-object v3, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 356
    iget v3, v1, Lepson/print/screen/PrintSetting;->paperSizeValue:I

    iput v3, p0, Lepson/print/screen/SettingScr;->paperSize:I

    .line 357
    iget-object v3, p0, Lepson/print/screen/SettingScr;->paperSizeInfo:Landroid/widget/TextView;

    iget-object v4, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    iget v5, p0, Lepson/print/screen/SettingScr;->paperSize:I

    invoke-virtual {v4, v5}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result v4

    invoke-virtual {p0, v4}, Lepson/print/screen/SettingScr;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 358
    iget-object v3, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {v3}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    .line 363
    new-instance v3, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperType;

    invoke-direct {v3}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperType;-><init>()V

    iput-object v3, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 365
    iget v3, v1, Lepson/print/screen/PrintSetting;->paperTypeValue:I

    iput v3, p0, Lepson/print/screen/SettingScr;->paperType:I

    .line 366
    iget-object v3, p0, Lepson/print/screen/SettingScr;->paperTypeInfo:Landroid/widget/TextView;

    iget-object v4, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    iget v5, p0, Lepson/print/screen/SettingScr;->paperType:I

    invoke-virtual {v4, v5}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result v4

    invoke-virtual {p0, v4}, Lepson/print/screen/SettingScr;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 367
    iget-object v3, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {v3}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    .line 371
    invoke-virtual {v1}, Lepson/print/screen/PrintSetting;->loadPaperSizeTypePear()Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;

    move-result-object v3

    iput-object v3, p0, Lepson/print/screen/SettingScr;->mPaperSizeType:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 389
    iget-object v3, p0, Lepson/print/screen/SettingScr;->mPaperSizeType:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    iget v4, p0, Lepson/print/screen/SettingScr;->paperSize:I

    iget v5, p0, Lepson/print/screen/SettingScr;->paperType:I

    invoke-virtual {v3, v4, v5}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->putID(II)Z

    .line 395
    new-instance v3, Lepson/print/widgets/LayoutEx;

    invoke-direct {v3}, Lepson/print/widgets/LayoutEx;-><init>()V

    iput-object v3, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 402
    iget v3, v1, Lepson/print/screen/PrintSetting;->layoutMultiPageValue:I

    if-eqz v3, :cond_3

    .line 404
    iput v3, p0, Lepson/print/screen/SettingScr;->layout:I

    goto :goto_2

    .line 406
    :cond_3
    iget v3, v1, Lepson/print/screen/PrintSetting;->layoutValue:I

    iput v3, p0, Lepson/print/screen/SettingScr;->layout:I

    .line 409
    :goto_2
    iget-object v3, p0, Lepson/print/screen/SettingScr;->layoutInfo:Landroid/widget/TextView;

    iget-object v4, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    iget v5, p0, Lepson/print/screen/SettingScr;->layout:I

    invoke-virtual {v4, v5}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result v4

    invoke-virtual {p0, v4}, Lepson/print/screen/SettingScr;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 410
    iget-object v3, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {v3}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    .line 413
    new-instance v3, Lcom/epson/mobilephone/common/escpr/MediaInfo$Quality;

    invoke-direct {v3}, Lcom/epson/mobilephone/common/escpr/MediaInfo$Quality;-><init>()V

    iput-object v3, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 415
    iget v3, v1, Lepson/print/screen/PrintSetting;->qualityValue:I

    iput v3, p0, Lepson/print/screen/SettingScr;->quality:I

    .line 416
    iget-object v3, p0, Lepson/print/screen/SettingScr;->qualityInfo:Landroid/widget/TextView;

    iget-object v4, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    iget v5, p0, Lepson/print/screen/SettingScr;->quality:I

    invoke-virtual {v4, v5}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result v4

    invoke-virtual {p0, v4}, Lepson/print/screen/SettingScr;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 417
    iget-object v3, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {v3}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    .line 420
    new-instance v3, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSource;

    invoke-direct {v3}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSource;-><init>()V

    iput-object v3, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 428
    iget v3, v1, Lepson/print/screen/PrintSetting;->paperSourceValue:I

    iput v3, p0, Lepson/print/screen/SettingScr;->paperSource:I

    .line 429
    iget-object v3, p0, Lepson/print/screen/SettingScr;->paperSourceInfo:Landroid/widget/TextView;

    iget-object v4, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    iget v5, p0, Lepson/print/screen/SettingScr;->paperSource:I

    invoke-virtual {v4, v5}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result v4

    invoke-virtual {p0, v4}, Lepson/print/screen/SettingScr;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 430
    iget-object v3, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {v3}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    .line 433
    new-instance v3, Lcom/epson/mobilephone/common/escpr/MediaInfo$Color;

    invoke-direct {v3}, Lcom/epson/mobilephone/common/escpr/MediaInfo$Color;-><init>()V

    iput-object v3, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 435
    iget v3, v1, Lepson/print/screen/PrintSetting;->colorValue:I

    iput v3, p0, Lepson/print/screen/SettingScr;->color:I

    .line 436
    iget-object v3, p0, Lepson/print/screen/SettingScr;->colorInfo:Landroid/widget/TextView;

    iget-object v4, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    iget v5, p0, Lepson/print/screen/SettingScr;->color:I

    invoke-virtual {v4, v5}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result v4

    invoke-virtual {p0, v4}, Lepson/print/screen/SettingScr;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 437
    iget-object v3, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {v3}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    .line 441
    iget v3, v1, Lepson/print/screen/PrintSetting;->copiesValue:I

    iput v3, p0, Lepson/print/screen/SettingScr;->copiesValue:I

    .line 442
    iget-object v3, p0, Lepson/print/screen/SettingScr;->copies:Landroid/widget/TextView;

    iget v4, p0, Lepson/print/screen/SettingScr;->copiesValue:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 443
    iget-object v3, p0, Lepson/print/screen/SettingScr;->copiesMinus:Landroid/widget/Button;

    iget v4, p0, Lepson/print/screen/SettingScr;->copiesValue:I

    const/4 v5, 0x0

    if-eq v4, v2, :cond_4

    const/4 v4, 0x1

    goto :goto_3

    :cond_4
    const/4 v4, 0x0

    :goto_3
    invoke-virtual {v3, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 444
    iget-object v3, p0, Lepson/print/screen/SettingScr;->copiesPlus:Landroid/widget/Button;

    iget v4, p0, Lepson/print/screen/SettingScr;->copiesValue:I

    const/16 v6, 0x1e

    if-eq v4, v6, :cond_5

    goto :goto_4

    :cond_5
    const/4 v2, 0x0

    :goto_4
    invoke-virtual {v3, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 448
    iget v2, v1, Lepson/print/screen/PrintSetting;->brightnessValue:I

    iput v2, p0, Lepson/print/screen/SettingScr;->brightnessValue:I

    .line 452
    iget v2, v1, Lepson/print/screen/PrintSetting;->contrastValue:I

    iput v2, p0, Lepson/print/screen/SettingScr;->contrastValue:I

    .line 456
    iget v2, v1, Lepson/print/screen/PrintSetting;->saturationValue:I

    iput v2, p0, Lepson/print/screen/SettingScr;->saturationValue:I

    .line 459
    new-instance v2, Lcom/epson/mobilephone/common/escpr/MediaInfo$Duplex;

    invoke-direct {v2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$Duplex;-><init>()V

    iput-object v2, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 461
    iget v2, v1, Lepson/print/screen/PrintSetting;->duplexValue:I

    iput v2, p0, Lepson/print/screen/SettingScr;->duplex:I

    .line 462
    iget-object v2, p0, Lepson/print/screen/SettingScr;->duplexInfo:Landroid/widget/TextView;

    iget-object v3, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    iget v4, p0, Lepson/print/screen/SettingScr;->duplex:I

    invoke-virtual {v3, v4}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result v3

    invoke-virtual {p0, v3}, Lepson/print/screen/SettingScr;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 463
    iget-object v2, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    .line 466
    new-instance v2, Lcom/epson/mobilephone/common/escpr/MediaInfo$FeedDirection;

    invoke-direct {v2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$FeedDirection;-><init>()V

    iput-object v2, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 468
    iget v2, v1, Lepson/print/screen/PrintSetting;->feedDirectionValue:I

    iput v2, p0, Lepson/print/screen/SettingScr;->feedDirection:I

    .line 469
    iget-object v2, p0, Lepson/print/screen/SettingScr;->feedDirectionInfo:Landroid/widget/TextView;

    iget-object v3, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    iget v4, p0, Lepson/print/screen/SettingScr;->feedDirection:I

    invoke-virtual {v3, v4}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result v3

    invoke-virtual {p0, v3}, Lepson/print/screen/SettingScr;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 470
    iget-object v2, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    .line 476
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getLang()I

    move-result v0

    iput v0, p0, Lepson/print/screen/SettingScr;->lang:I

    .line 478
    iget-boolean v0, p0, Lepson/print/screen/SettingScr;->isDocumentSetting:Z

    if-eqz v0, :cond_6

    .line 482
    iget-boolean v0, v1, Lepson/print/screen/PrintSetting;->printAll:Z

    iput-boolean v0, p0, Lepson/print/screen/SettingScr;->printAll:Z

    .line 483
    iget v0, v1, Lepson/print/screen/PrintSetting;->startValue:I

    iput v0, p0, Lepson/print/screen/SettingScr;->startValue:I

    .line 484
    iget v0, v1, Lepson/print/screen/PrintSetting;->endValue:I

    iput v0, p0, Lepson/print/screen/SettingScr;->endValue:I

    const v0, 0x7f080245

    .line 486
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lepson/print/screen/SettingScr;->startValue:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ".."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lepson/print/screen/SettingScr;->endValue:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_5

    .line 493
    :cond_6
    new-instance v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PrintDate;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PrintDate;-><init>()V

    iput-object v0, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 495
    iget v0, v1, Lepson/print/screen/PrintSetting;->printdate:I

    iput v0, p0, Lepson/print/screen/SettingScr;->printdate:I

    .line 496
    iget-object v0, p0, Lepson/print/screen/SettingScr;->printDateInfo:Landroid/widget/TextView;

    iget-object v2, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    iget v3, p0, Lepson/print/screen/SettingScr;->printdate:I

    invoke-virtual {v2, v3}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result v2

    invoke-virtual {p0, v2}, Lepson/print/screen/SettingScr;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 497
    iget-object v0, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    .line 499
    iget v0, v1, Lepson/print/screen/PrintSetting;->photoApfValueForLocalPrinter:I

    iput v0, p0, Lepson/print/screen/SettingScr;->photoApfValueForLocalPrinter:I

    .line 500
    iget v0, p0, Lepson/print/screen/SettingScr;->photoApfValueForLocalPrinter:I

    invoke-direct {p0, v0}, Lepson/print/screen/SettingScr;->displayApfState(I)V

    .line 502
    iget v0, v1, Lepson/print/screen/PrintSetting;->sharpnessForPhoto:I

    iput v0, p0, Lepson/print/screen/SettingScr;->mSharpnessValue:I

    .line 503
    iget v0, p0, Lepson/print/screen/SettingScr;->mSharpnessValue:I

    invoke-direct {p0, v0}, Lepson/print/screen/SettingScr;->displaySharpnessState(I)V

    :goto_5
    return-void
.end method

.method private loadSupportedMediaFile()Z
    .locals 6

    .line 535
    invoke-static {p0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v0

    .line 537
    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->getSavedSupportedMedia()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->getSupportedMedia()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->getPreSupportedMedia()Ljava/io/File;

    move-result-object v3

    const-string v4, "PrintSetting"

    const-string v5, "call loadSupportedMedia"

    .line 538
    invoke-static {v4, v5}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v4, 0x0

    .line 540
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_0

    const-string v0, "loadSupportedMedia"

    .line 541
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " not exist"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    return v4

    .line 544
    :cond_0
    invoke-static {v1, v2}, Lepson/common/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V

    .line 545
    invoke-static {v1, v3}, Lepson/common/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 552
    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->getSavedAreaInfo()Ljava/io/File;

    move-result-object v1

    .line 553
    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->getAreaInfo()Ljava/io/File;

    move-result-object v2

    .line 554
    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->getPreAreaInfo()Ljava/io/File;

    move-result-object v0

    .line 556
    :try_start_1
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 557
    invoke-static {v1, v2}, Lepson/common/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V

    .line 558
    invoke-static {v1, v0}, Lepson/common/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    const/4 v0, 0x1

    return v0

    :catch_0
    move-exception v0

    const-string v1, "loadSupportedMedia"

    .line 561
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "failure "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    return v4

    :catch_1
    move-exception v0

    const-string v1, "loadSupportedMedia"

    .line 547
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "failure "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    return v4
.end method

.method private onPrinterSelectEnd(ILandroid/content/Intent;)V
    .locals 6

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, -0x1

    if-ne p1, v2, :cond_2

    .line 1339
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    const-string v2, "myprinter"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Lepson/print/MyPrinter;

    iput-object p1, p0, Lepson/print/screen/SettingScr;->printer:Lepson/print/MyPrinter;

    .line 1340
    iget-object p1, p0, Lepson/print/screen/SettingScr;->printerName:Landroid/widget/TextView;

    iget-object v2, p0, Lepson/print/screen/SettingScr;->printer:Lepson/print/MyPrinter;

    invoke-virtual {v2}, Lepson/print/MyPrinter;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1341
    iget-object p1, p0, Lepson/print/screen/SettingScr;->printer:Lepson/print/MyPrinter;

    invoke-virtual {p1}, Lepson/print/MyPrinter;->getName()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/print/screen/SettingScr;->printerDeviceId:Ljava/lang/String;

    .line 1342
    iget-object p1, p0, Lepson/print/screen/SettingScr;->printer:Lepson/print/MyPrinter;

    invoke-virtual {p1}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/print/screen/SettingScr;->printerId:Ljava/lang/String;

    .line 1343
    iget-object p1, p0, Lepson/print/screen/SettingScr;->printer:Lepson/print/MyPrinter;

    invoke-virtual {p1}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/print/screen/SettingScr;->printerIp:Ljava/lang/String;

    .line 1344
    iget-object p1, p0, Lepson/print/screen/SettingScr;->printer:Lepson/print/MyPrinter;

    invoke-virtual {p1}, Lepson/print/MyPrinter;->getSerialNo()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/print/screen/SettingScr;->printerSerialNo:Ljava/lang/String;

    .line 1345
    iget-object p1, p0, Lepson/print/screen/SettingScr;->printer:Lepson/print/MyPrinter;

    invoke-virtual {p1}, Lepson/print/MyPrinter;->getEmailAddress()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/print/screen/SettingScr;->printerEmailAddress:Ljava/lang/String;

    .line 1346
    iget-object p1, p0, Lepson/print/screen/SettingScr;->printer:Lepson/print/MyPrinter;

    invoke-virtual {p1}, Lepson/print/MyPrinter;->getLocation()I

    move-result p1

    iput p1, p0, Lepson/print/screen/SettingScr;->printerLocation:I

    .line 1349
    new-instance p1, Lepson/print/EPPrinterManager;

    invoke-direct {p1, p0}, Lepson/print/EPPrinterManager;-><init>(Landroid/content/Context;)V

    .line 1351
    iget v2, p0, Lepson/print/screen/SettingScr;->printerLocation:I

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 1353
    :pswitch_0
    iget-object v2, p0, Lepson/print/screen/SettingScr;->printerId:Ljava/lang/String;

    invoke-virtual {p1, v2}, Lepson/print/EPPrinterManager;->loadIpPrinterInfo(Ljava/lang/String;)Lepson/print/EPPrinterInfo;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 1354
    iget-object v2, p1, Lepson/print/EPPrinterInfo;->userDefName:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p1, Lepson/print/EPPrinterInfo;->userDefName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 1355
    iget-object v2, p0, Lepson/print/screen/SettingScr;->printerName:Landroid/widget/TextView;

    iget-object p1, p1, Lepson/print/EPPrinterInfo;->userDefName:Ljava/lang/String;

    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1359
    :pswitch_1
    iget-object v2, p0, Lepson/print/screen/SettingScr;->printerEmailAddress:Ljava/lang/String;

    invoke-virtual {p1, v2}, Lepson/print/EPPrinterManager;->loadRemotePrinterInfo(Ljava/lang/String;)Lepson/print/EPPrinterInfo;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 1360
    iget-object v2, p1, Lepson/print/EPPrinterInfo;->userDefName:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p1, Lepson/print/EPPrinterInfo;->userDefName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 1361
    iget-object v2, p0, Lepson/print/screen/SettingScr;->printerName:Landroid/widget/TextView;

    iget-object p1, p1, Lepson/print/EPPrinterInfo;->userDefName:Ljava/lang/String;

    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1366
    :cond_0
    :goto_0
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    const-string p2, "simpleap"

    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/print/screen/SettingScr;->autoConnectSsid:Ljava/lang/String;

    .line 1369
    invoke-virtual {p0}, Lepson/print/screen/SettingScr;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string p2, "PrintSetting"

    const-string v2, "RE_SEARCH"

    invoke-static {p1, p2, v2, v1}, Lepson/common/Utils;->savePref(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1372
    iput-boolean v0, p0, Lepson/print/screen/SettingScr;->undoFlag:Z

    .line 1374
    iput-boolean v1, p0, Lepson/print/screen/SettingScr;->mPrinterSelectDone:Z

    .line 1375
    iput-boolean v1, p0, Lepson/print/screen/SettingScr;->mWaiteInkReplenProgress:Z

    .line 1376
    invoke-virtual {p0}, Lepson/print/screen/SettingScr;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iget-object p2, p0, Lepson/print/screen/SettingScr;->printerId:Ljava/lang/String;

    invoke-static {p1, p2}, Lepson/print/inkrpln/InkReplnHelper;->isSimpleApOrP2p(Landroid/content/Context;Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 1377
    new-instance p1, Lepson/print/inkrpln/PrintSettingDependencyBuilder;

    iget-object v1, p0, Lepson/print/screen/SettingScr;->printerId:Ljava/lang/String;

    iget-object v2, p0, Lepson/print/screen/SettingScr;->printerIp:Ljava/lang/String;

    iget-object v3, p0, Lepson/print/screen/SettingScr;->printerSerialNo:Ljava/lang/String;

    iget v4, p0, Lepson/print/screen/SettingScr;->printerLocation:I

    iget-object v5, p0, Lepson/print/screen/SettingScr;->printerDeviceId:Ljava/lang/String;

    move-object v0, p1

    invoke-direct/range {v0 .. v5}, Lepson/print/inkrpln/PrintSettingDependencyBuilder;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    invoke-static {p0, p1}, Lepson/print/inkrpln/InkRplnProgressDialog;->getStartIntent2(Landroid/content/Context;Lepson/print/inkrpln/DependencyBuilder;)Landroid/content/Intent;

    move-result-object p1

    const/4 p2, 0x4

    .line 1380
    invoke-virtual {p0, p1, p2}, Lepson/print/screen/SettingScr;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_3

    .line 1382
    :cond_1
    invoke-direct {p0}, Lepson/print/screen/SettingScr;->endInkReplAndGoProbePrinter()V

    goto :goto_3

    .line 1387
    :cond_2
    invoke-direct {p0}, Lepson/print/screen/SettingScr;->bindEpsonService()V

    .line 1391
    new-instance p1, Lepson/print/EPPrinterManager;

    invoke-direct {p1, p0}, Lepson/print/EPPrinterManager;-><init>(Landroid/content/Context;)V

    .line 1393
    iget p2, p0, Lepson/print/screen/SettingScr;->printerLocation:I

    packed-switch p2, :pswitch_data_1

    goto :goto_1

    .line 1400
    :pswitch_2
    iget-object p2, p0, Lepson/print/screen/SettingScr;->printerId:Ljava/lang/String;

    invoke-virtual {p1, p2}, Lepson/print/EPPrinterManager;->loadIpPrinterInfo(Ljava/lang/String;)Lepson/print/EPPrinterInfo;

    move-result-object p1

    if-nez p1, :cond_3

    goto :goto_2

    .line 1395
    :pswitch_3
    iget-object p2, p0, Lepson/print/screen/SettingScr;->printerEmailAddress:Ljava/lang/String;

    invoke-virtual {p1, p2}, Lepson/print/EPPrinterManager;->loadRemotePrinterInfo(Ljava/lang/String;)Lepson/print/EPPrinterInfo;

    move-result-object p1

    if-nez p1, :cond_3

    goto :goto_2

    :cond_3
    :goto_1
    const/4 v1, 0x0

    :goto_2
    if-eqz v1, :cond_4

    .line 1408
    invoke-virtual {p0}, Lepson/print/screen/SettingScr;->resetSettings()V

    :cond_4
    :goto_3
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method private refreshRemoteServiceRemotePrinter()V
    .locals 1

    .line 798
    iget-object v0, p0, Lepson/print/screen/SettingScr;->mEpsonService:Lepson/print/service/IEpsonService;

    if-eqz v0, :cond_0

    .line 800
    :try_start_0
    invoke-interface {v0}, Lepson/print/service/IEpsonService;->refreshRemotePrinterLogin()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method

.method private saveChanged()V
    .locals 11

    const-string v0, "PrintSetting"

    const/4 v1, 0x0

    .line 667
    invoke-virtual {p0, v0, v1}, Lepson/print/screen/SettingScr;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 668
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "PrintSettingSaved"

    const/4 v3, 0x1

    .line 672
    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 673
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 676
    new-instance v0, Lepson/print/MyPrinter;

    iget-object v5, p0, Lepson/print/screen/SettingScr;->printerDeviceId:Ljava/lang/String;

    iget-object v6, p0, Lepson/print/screen/SettingScr;->printerIp:Ljava/lang/String;

    iget-object v7, p0, Lepson/print/screen/SettingScr;->printerId:Ljava/lang/String;

    iget-object v8, p0, Lepson/print/screen/SettingScr;->printerSerialNo:Ljava/lang/String;

    iget-object v9, p0, Lepson/print/screen/SettingScr;->printerEmailAddress:Ljava/lang/String;

    iget v10, p0, Lepson/print/screen/SettingScr;->printerLocation:I

    move-object v4, v0

    invoke-direct/range {v4 .. v10}, Lepson/print/MyPrinter;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 684
    iget v2, p0, Lepson/print/screen/SettingScr;->lang:I

    invoke-virtual {v0, v2}, Lepson/print/MyPrinter;->setLang(I)V

    .line 685
    invoke-virtual {v0, p0}, Lepson/print/MyPrinter;->setCurPrinter(Landroid/content/Context;)V

    .line 688
    new-instance v2, Lepson/print/screen/PrintSetting;

    iget-boolean v3, p0, Lepson/print/screen/SettingScr;->isDocumentSetting:Z

    if-eqz v3, :cond_0

    sget-object v3, Lepson/print/screen/PrintSetting$Kind;->document:Lepson/print/screen/PrintSetting$Kind;

    goto :goto_0

    :cond_0
    sget-object v3, Lepson/print/screen/PrintSetting$Kind;->photo:Lepson/print/screen/PrintSetting$Kind;

    :goto_0
    invoke-direct {v2, p0, v3}, Lepson/print/screen/PrintSetting;-><init>(Landroid/content/Context;Lepson/print/screen/PrintSetting$Kind;)V

    .line 690
    iget v3, p0, Lepson/print/screen/SettingScr;->paperSize:I

    iput v3, v2, Lepson/print/screen/PrintSetting;->paperSizeValue:I

    .line 691
    iget v3, p0, Lepson/print/screen/SettingScr;->paperType:I

    iput v3, v2, Lepson/print/screen/PrintSetting;->paperTypeValue:I

    .line 693
    iget v3, p0, Lepson/print/screen/SettingScr;->layout:I

    const/high16 v4, -0x10000

    and-int/2addr v4, v3

    if-eqz v4, :cond_1

    const/4 v4, 0x2

    .line 695
    iput v4, v2, Lepson/print/screen/PrintSetting;->layoutValue:I

    .line 696
    iput v3, v2, Lepson/print/screen/PrintSetting;->layoutMultiPageValue:I

    goto :goto_1

    .line 698
    :cond_1
    iput v3, v2, Lepson/print/screen/PrintSetting;->layoutValue:I

    .line 699
    iput v1, v2, Lepson/print/screen/PrintSetting;->layoutMultiPageValue:I

    .line 702
    :goto_1
    iget v3, p0, Lepson/print/screen/SettingScr;->quality:I

    iput v3, v2, Lepson/print/screen/PrintSetting;->qualityValue:I

    .line 703
    iget v3, p0, Lepson/print/screen/SettingScr;->paperSource:I

    iput v3, v2, Lepson/print/screen/PrintSetting;->paperSourceValue:I

    .line 704
    iget v3, p0, Lepson/print/screen/SettingScr;->color:I

    iput v3, v2, Lepson/print/screen/PrintSetting;->colorValue:I

    .line 705
    iget v3, p0, Lepson/print/screen/SettingScr;->duplex:I

    iput v3, v2, Lepson/print/screen/PrintSetting;->duplexValue:I

    .line 706
    iget v3, p0, Lepson/print/screen/SettingScr;->feedDirection:I

    iput v3, v2, Lepson/print/screen/PrintSetting;->feedDirectionValue:I

    .line 708
    iget v3, p0, Lepson/print/screen/SettingScr;->copiesValue:I

    iput v3, v2, Lepson/print/screen/PrintSetting;->copiesValue:I

    .line 709
    iget v3, p0, Lepson/print/screen/SettingScr;->brightnessValue:I

    iput v3, v2, Lepson/print/screen/PrintSetting;->brightnessValue:I

    .line 710
    iget v3, p0, Lepson/print/screen/SettingScr;->contrastValue:I

    iput v3, v2, Lepson/print/screen/PrintSetting;->contrastValue:I

    .line 711
    iget v3, p0, Lepson/print/screen/SettingScr;->saturationValue:I

    iput v3, v2, Lepson/print/screen/PrintSetting;->saturationValue:I

    .line 728
    iget-boolean v3, p0, Lepson/print/screen/SettingScr;->isDocumentSetting:Z

    if-eqz v3, :cond_2

    .line 730
    iget-boolean v3, p0, Lepson/print/screen/SettingScr;->printAll:Z

    iput-boolean v3, v2, Lepson/print/screen/PrintSetting;->printAll:Z

    .line 731
    iget v3, p0, Lepson/print/screen/SettingScr;->startValue:I

    iput v3, v2, Lepson/print/screen/PrintSetting;->startValue:I

    .line 732
    iget v3, p0, Lepson/print/screen/SettingScr;->endValue:I

    iput v3, v2, Lepson/print/screen/PrintSetting;->endValue:I

    goto :goto_2

    .line 736
    :cond_2
    iget v3, p0, Lepson/print/screen/SettingScr;->printdate:I

    iput v3, v2, Lepson/print/screen/PrintSetting;->printdate:I

    .line 738
    iget v3, p0, Lepson/print/screen/SettingScr;->photoApfValueForLocalPrinter:I

    iput v3, v2, Lepson/print/screen/PrintSetting;->photoApfValueForLocalPrinter:I

    .line 739
    iget v3, p0, Lepson/print/screen/SettingScr;->mSharpnessValue:I

    iput v3, v2, Lepson/print/screen/PrintSetting;->sharpnessForPhoto:I

    .line 742
    :goto_2
    invoke-virtual {v2}, Lepson/print/screen/PrintSetting;->saveSettings()V

    .line 764
    iget-object v3, p0, Lepson/print/screen/SettingScr;->mPaperSizeType:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    check-cast v3, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;

    invoke-virtual {v2, v3}, Lepson/print/screen/PrintSetting;->savePaperSizeTypePear(Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;)V

    .line 766
    invoke-direct {p0}, Lepson/print/screen/SettingScr;->saveSupportedMediaFile()Z

    .line 769
    iget-object v2, p0, Lepson/print/screen/SettingScr;->autoConnectSsid:Ljava/lang/String;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_3

    .line 770
    iget-object v2, p0, Lepson/print/screen/SettingScr;->autoConnectSsid:Ljava/lang/String;

    const-string v3, "printer"

    invoke-virtual {v0}, Lepson/print/MyPrinter;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v2, v3, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->setConnectInfo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_3
    const-string v0, "printer"

    .line 772
    invoke-static {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->resetConnectInfo(Landroid/content/Context;Ljava/lang/String;)V

    .line 776
    :goto_3
    new-instance v0, Lepson/print/EPPrinterManager;

    iget-object v2, p0, Lepson/print/screen/SettingScr;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Lepson/print/EPPrinterManager;-><init>(Landroid/content/Context;)V

    .line 778
    invoke-virtual {v0}, Lepson/print/EPPrinterManager;->commitIPPrinterInfo()V

    .line 779
    invoke-virtual {v0}, Lepson/print/EPPrinterManager;->commitRemotePrinterInfo()V

    .line 782
    invoke-direct {p0}, Lepson/print/screen/SettingScr;->refreshRemoteServiceRemotePrinter()V

    .line 784
    iget-boolean v0, p0, Lepson/print/screen/SettingScr;->mPrinterSelectDone:Z

    if-eqz v0, :cond_4

    .line 786
    new-instance v0, Lepson/print/inkrpln/InkRplnRepository;

    invoke-direct {v0, v1}, Lepson/print/inkrpln/InkRplnRepository;-><init>(Z)V

    .line 787
    invoke-virtual {v0, p0}, Lepson/print/inkrpln/InkRplnRepository;->savePermanently(Landroid/content/Context;)V

    :cond_4
    return-void
.end method

.method private saveSupportedMediaFile()Z
    .locals 7

    .line 619
    invoke-static {p0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v0

    const-string v1, "PrintSetting"

    const-string v2, "call saveSupportedMedia"

    .line 621
    invoke-static {v1, v2}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 622
    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->getSupportedMedia()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->getSavedSupportedMedia()Ljava/io/File;

    move-result-object v2

    const/4 v3, 0x0

    .line 624
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_0

    const-string v0, "updateSupportedMediaFile"

    .line 625
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " not exist"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    return v3

    .line 628
    :cond_0
    invoke-static {v1, v2}, Lepson/common/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 636
    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->getAreaInfo()Ljava/io/File;

    move-result-object v1

    .line 637
    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->getSavedAreaInfo()Ljava/io/File;

    move-result-object v2

    .line 638
    iget v4, p0, Lepson/print/screen/SettingScr;->printerLocation:I

    const/4 v5, 0x1

    if-eq v4, v5, :cond_1

    const/4 v6, 0x3

    if-eq v4, v6, :cond_1

    const-string v1, "PrintSetting"

    const-string v2, "delete AreaInfo"

    .line 655
    invoke-static {v1, v2}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 656
    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->removeAreaInfo()V

    goto :goto_0

    .line 643
    :cond_1
    :try_start_1
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "updateSupportedMediaFile"

    .line 644
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " not exist"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    return v3

    .line 647
    :cond_2
    invoke-static {v1, v2}, Lepson/common/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :goto_0
    return v5

    :catch_0
    move-exception v0

    const-string v1, "saveSupportedMediaFile"

    .line 649
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "failure "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    return v3

    :catch_1
    move-exception v0

    const-string v1, "saveSupportedMediaFile"

    .line 630
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "failure "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    return v3
.end method

.method private setClickListener()V
    .locals 1

    .line 290
    iget-object v0, p0, Lepson/print/screen/SettingScr;->copiesMinus:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 291
    iget-object v0, p0, Lepson/print/screen/SettingScr;->copiesPlus:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 293
    iget-object v0, p0, Lepson/print/screen/SettingScr;->brightnessMinus:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 294
    iget-object v0, p0, Lepson/print/screen/SettingScr;->brightnessPlus:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 295
    iget-object v0, p0, Lepson/print/screen/SettingScr;->contrastMinus:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 296
    iget-object v0, p0, Lepson/print/screen/SettingScr;->contrastPlus:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 297
    iget-object v0, p0, Lepson/print/screen/SettingScr;->saturationMinus:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 298
    iget-object v0, p0, Lepson/print/screen/SettingScr;->saturationPlus:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 300
    iget-boolean v0, p0, Lepson/print/screen/SettingScr;->isDocumentSetting:Z

    if-eqz v0, :cond_0

    const v0, 0x7f080244

    .line 301
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    const v0, 0x7f08027e

    .line 303
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f08024a

    .line 304
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f080252

    .line 305
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0801b6

    .line 306
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0802a0

    .line 307
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f08024f

    .line 308
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0800d2

    .line 310
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f08011d

    .line 311
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f08013d

    .line 312
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f08027b

    .line 313
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f080063

    .line 314
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0802fa

    .line 315
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private setScreenState(Ljava/lang/Boolean;)V
    .locals 2

    .line 2508
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2509
    iget-object v0, p0, Lepson/print/screen/SettingScr;->progressGetOption:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 2511
    :cond_0
    iget-object v0, p0, Lepson/print/screen/SettingScr;->progressGetOption:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    const v0, 0x7f08027e

    .line 2513
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    const v0, 0x7f08024a

    .line 2514
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    const v0, 0x7f080252

    .line 2515
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    const v0, 0x7f0801b6

    .line 2516
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    const v0, 0x7f0802a0

    .line 2517
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    const v0, 0x7f08024f

    .line 2518
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    const v0, 0x7f0800d2

    .line 2520
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    const v0, 0x7f08011d

    .line 2521
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    const v0, 0x7f08013d

    .line 2522
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    const v0, 0x7f08027b

    .line 2523
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {v0, p1}, Landroid/view/View;->setClickable(Z)V

    return-void
.end method

.method private unbindEpsonService()V
    .locals 2

    const-string v0, "SettingScr"

    const-string v1, "unbindEpsonService"

    .line 837
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 840
    iget-object v0, p0, Lepson/print/screen/SettingScr;->mEpsonService:Lepson/print/service/IEpsonService;

    if-eqz v0, :cond_0

    .line 842
    :try_start_0
    iget-object v1, p0, Lepson/print/screen/SettingScr;->mCallback:Lepson/print/service/IEpsonServiceCallback;

    invoke-interface {v0, v1}, Lepson/print/service/IEpsonService;->unregisterCallback(Lepson/print/service/IEpsonServiceCallback;)V

    .line 843
    iget-object v0, p0, Lepson/print/screen/SettingScr;->mEpsonConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->unbindService(Landroid/content/ServiceConnection;)V

    const/4 v0, 0x0

    .line 844
    iput-object v0, p0, Lepson/print/screen/SettingScr;->mEpsonService:Lepson/print/service/IEpsonService;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 846
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_0
    :goto_0
    return-void
.end method

.method private updatePrinterIcon()V
    .locals 3

    .line 268
    iget-object v0, p0, Lepson/print/screen/SettingScr;->printerName:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    const v1, 0x7f0e04d6

    invoke-virtual {p0, v1}, Lepson/print/screen/SettingScr;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    const v1, 0x7f080198

    if-eqz v0, :cond_0

    .line 269
    invoke-virtual {p0, v1}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 271
    :cond_0
    invoke-virtual {p0, v1}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 274
    iget v0, p0, Lepson/print/screen/SettingScr;->printerLocation:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 279
    :pswitch_0
    invoke-virtual {p0, v1}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f07010f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 276
    :pswitch_1
    invoke-virtual {p0, v1}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f070111

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 282
    :pswitch_2
    invoke-virtual {p0, v1}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f070110

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private updateSupportedMediaFile(Z)Z
    .locals 5

    .line 576
    invoke-static {p0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v0

    const-string v1, "PrintSetting"

    const-string v2, "call updateSupportedMedia"

    .line 578
    invoke-static {v1, v2}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 579
    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->getSupportedMedia()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->getPreSupportedMedia()Ljava/io/File;

    move-result-object v2

    if-eqz p1, :cond_0

    move-object v4, v2

    move-object v2, v1

    move-object v1, v4

    :cond_0
    const/4 v3, 0x0

    .line 586
    :try_start_0
    invoke-static {v1, v2}, Lepson/common/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 595
    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->getAreaInfo()Ljava/io/File;

    move-result-object v1

    .line 596
    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->getPreAreaInfo()Ljava/io/File;

    move-result-object v0

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    .line 603
    :goto_0
    :try_start_1
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 604
    invoke-static {v0, v1}, Lepson/common/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :cond_2
    const/4 p1, 0x1

    return p1

    :catch_0
    move-exception p1

    if-eqz v0, :cond_3

    .line 607
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    :cond_3
    if-eqz v1, :cond_4

    .line 608
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    :cond_4
    const-string v0, "updateSupportedMediaFile"

    .line 609
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "failure "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    return v3

    :catch_1
    move-exception p1

    if-eqz v1, :cond_5

    .line 588
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    :cond_5
    if-eqz v2, :cond_6

    .line 589
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    :cond_6
    const-string v0, "updateSupportedMediaFile"

    .line 590
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "failure "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    return v3
.end method


# virtual methods
.method protected deleteLongTapMessage()V
    .locals 8

    .line 2608
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0xa

    add-long/2addr v2, v4

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    .line 2610
    iget-object v1, p0, Lepson/print/screen/SettingScr;->copiesMinus:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 2611
    iget-object v1, p0, Lepson/print/screen/SettingScr;->copiesPlus:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    return-void
.end method

.method getPrinterLang()I
    .locals 4

    .line 1561
    iget v0, p0, Lepson/print/screen/SettingScr;->printerLocation:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 1562
    iget-object v0, p0, Lepson/print/screen/SettingScr;->mEpsonService:Lepson/print/service/IEpsonService;

    if-eqz v0, :cond_0

    .line 1564
    :try_start_0
    invoke-interface {v0}, Lepson/print/service/IEpsonService;->getLang()I

    move-result v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 1566
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "PrintSetting"

    .line 1570
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getPrinterLang called : ret = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    return v0
.end method

.method launchDetailScreen(II)V
    .locals 4

    .line 1124
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lepson/print/screen/PrinterInfoDetail;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1125
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "ID"

    .line 1126
    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "sizeIndex"

    .line 1127
    iget v3, p0, Lepson/print/screen/SettingScr;->sizeIndex:I

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "typeIndex"

    .line 1128
    iget v3, p0, Lepson/print/screen/SettingScr;->typeIndex:I

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "curValue"

    .line 1129
    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string p1, "isDocumentSetting"

    .line 1130
    iget-boolean v2, p0, Lepson/print/screen/SettingScr;->isDocumentSetting:Z

    invoke-virtual {v1, p1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const/4 p1, 0x1

    sparse-switch p2, :sswitch_data_0

    goto :goto_1

    :sswitch_0
    const-string p2, "PRINT_QUALITY_INFO"

    .line 1146
    iget-object v2, p0, Lepson/print/screen/SettingScr;->quality_info:[I

    invoke-virtual {v1, p2, v2}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    goto :goto_1

    :sswitch_1
    const-string p2, "PAPER_TYPE_INFO"

    .line 1138
    iget-object v2, p0, Lepson/print/screen/SettingScr;->paper_type_info:[I

    invoke-virtual {v1, p2, v2}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    goto :goto_1

    .line 1152
    :sswitch_2
    iget-object p2, p0, Lepson/print/screen/SettingScr;->paper_source_info:[I

    if-eqz p2, :cond_0

    array-length p2, p2

    if-gt p2, p1, :cond_0

    .line 1153
    new-array p2, p1, [I

    const/4 v2, 0x0

    const/16 v3, 0x80

    aput v3, p2, v2

    goto :goto_0

    .line 1156
    :cond_0
    iget-object p2, p0, Lepson/print/screen/SettingScr;->paper_source_info:[I

    :goto_0
    const-string v2, "PAPER_SOURCE_INFO"

    .line 1160
    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    goto :goto_1

    :sswitch_3
    const-string p2, "PAPER_SIZE_INFO"

    .line 1134
    iget-object v2, p0, Lepson/print/screen/SettingScr;->paper_size_info:[I

    invoke-virtual {v1, p2, v2}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    goto :goto_1

    :sswitch_4
    const-string p2, "LAYOUT_INFO"

    .line 1142
    iget-object v2, p0, Lepson/print/screen/SettingScr;->layout_info:[I

    invoke-virtual {v1, p2, v2}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    goto :goto_1

    :sswitch_5
    const-string p2, "DUPLEX_INFO"

    .line 1168
    iget-object v2, p0, Lepson/print/screen/SettingScr;->duplex_info:[I

    invoke-virtual {v1, p2, v2}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    goto :goto_1

    :sswitch_6
    const-string p2, "COLOR_INFO"

    .line 1164
    iget-object v2, p0, Lepson/print/screen/SettingScr;->color_info:[I

    invoke-virtual {v1, p2, v2}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 1186
    :goto_1
    :sswitch_7
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1187
    invoke-virtual {p0, v0, p1}, Lepson/print/screen/SettingScr;->startActivityForResult(Landroid/content/Intent;I)V

    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f080063 -> :sswitch_7
        0x7f0800d2 -> :sswitch_6
        0x7f08011d -> :sswitch_5
        0x7f08013d -> :sswitch_7
        0x7f0801b6 -> :sswitch_4
        0x7f08024a -> :sswitch_3
        0x7f08024f -> :sswitch_2
        0x7f080252 -> :sswitch_1
        0x7f08027b -> :sswitch_7
        0x7f0802a0 -> :sswitch_0
    .end sparse-switch
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .line 1195
    invoke-super {p0, p1, p2, p3}, Lepson/print/ActivityIACommon;->onActivityResult(IILandroid/content/Intent;)V

    .line 1196
    invoke-virtual {p0}, Lepson/print/screen/SettingScr;->getCallingPackage()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "requestCode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, " resultCode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p1, :cond_0

    .line 1199
    invoke-direct {p0, p2, p3}, Lepson/print/screen/SettingScr;->onPrinterSelectEnd(ILandroid/content/Intent;)V

    goto/16 :goto_2

    :cond_0
    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    .line 1203
    invoke-direct {p0}, Lepson/print/screen/SettingScr;->endInkReplAndGoProbePrinter()V

    goto/16 :goto_2

    :cond_1
    const/4 v0, -0x1

    const/4 v1, 0x2

    if-ne p1, v1, :cond_2

    if-ne p2, v0, :cond_6

    .line 1210
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    const-string p2, "printAll"

    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    iput-boolean p1, p0, Lepson/print/screen/SettingScr;->printAll:Z

    .line 1211
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    const-string p2, "startValue"

    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lepson/print/screen/SettingScr;->startValue:I

    .line 1212
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    const-string p2, "endValue"

    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lepson/print/screen/SettingScr;->endValue:I

    .line 1215
    iget-object p1, p0, Lepson/print/screen/SettingScr;->mHandler:Landroid/os/Handler;

    const/16 p2, 0x40

    invoke-virtual {p1, p2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_2

    :cond_2
    const/4 v2, 0x1

    if-ne p1, v2, :cond_6

    if-ne p2, v0, :cond_6

    .line 1220
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    const-string p2, "curValue"

    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    .line 1222
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p2

    const-string v0, "ID"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p2

    const/16 v0, 0x20

    const/4 v3, 0x0

    sparse-switch p2, :sswitch_data_0

    goto/16 :goto_2

    :sswitch_0
    if-ne p1, v2, :cond_3

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    .line 1324
    :goto_0
    iput v2, p0, Lepson/print/screen/SettingScr;->mSharpnessValue:I

    .line 1325
    iget p1, p0, Lepson/print/screen/SettingScr;->mSharpnessValue:I

    invoke-direct {p0, p1}, Lepson/print/screen/SettingScr;->displaySharpnessState(I)V

    goto/16 :goto_2

    .line 1264
    :sswitch_1
    new-instance p2, Lcom/epson/mobilephone/common/escpr/MediaInfo$Quality;

    invoke-direct {p2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$Quality;-><init>()V

    iput-object p2, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 1265
    iget-object p2, p0, Lepson/print/screen/SettingScr;->qualityInfo:Landroid/widget/TextView;

    iget-object p3, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {p3, p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result p3

    invoke-virtual {p0, p3}, Lepson/print/screen/SettingScr;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1266
    iget-object p2, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {p2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    .line 1267
    iput p1, p0, Lepson/print/screen/SettingScr;->quality:I

    goto/16 :goto_2

    .line 1311
    :sswitch_2
    new-instance p2, Lcom/epson/mobilephone/common/escpr/MediaInfo$PrintDate;

    invoke-direct {p2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PrintDate;-><init>()V

    iput-object p2, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 1312
    iget-object p2, p0, Lepson/print/screen/SettingScr;->printDateInfo:Landroid/widget/TextView;

    iget-object p3, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 1313
    invoke-virtual {p3, p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result p3

    .line 1312
    invoke-virtual {p0, p3}, Lepson/print/screen/SettingScr;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1314
    iget-object p2, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {p2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    .line 1315
    iput p1, p0, Lepson/print/screen/SettingScr;->printdate:I

    goto/16 :goto_2

    .line 1238
    :sswitch_3
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p2

    const-string p3, "INDEX"

    invoke-virtual {p2, p3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p2

    iput p2, p0, Lepson/print/screen/SettingScr;->typeIndex:I

    .line 1240
    new-instance p2, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperType;

    invoke-direct {p2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperType;-><init>()V

    iput-object p2, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 1241
    iget-object p2, p0, Lepson/print/screen/SettingScr;->paperTypeInfo:Landroid/widget/TextView;

    iget-object p3, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {p3, p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result p3

    invoke-virtual {p0, p3}, Lepson/print/screen/SettingScr;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1242
    iget-object p2, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {p2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    .line 1244
    iput p1, p0, Lepson/print/screen/SettingScr;->paperType:I

    .line 1245
    iget-object p1, p0, Lepson/print/screen/SettingScr;->mPaperSizeType:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    iget p2, p0, Lepson/print/screen/SettingScr;->paperSize:I

    iget p3, p0, Lepson/print/screen/SettingScr;->paperType:I

    invoke-virtual {p1, p2, p3}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->putID(II)Z

    .line 1247
    iput-boolean v3, p0, Lepson/print/screen/SettingScr;->undoFlag:Z

    .line 1248
    iget-object p1, p0, Lepson/print/screen/SettingScr;->mHandler:Landroid/os/Handler;

    const/4 p2, 0x3

    invoke-virtual {p1, p2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_2

    .line 1271
    :sswitch_4
    new-instance p2, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSource;

    invoke-direct {p2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSource;-><init>()V

    iput-object p2, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 1272
    iget-object p2, p0, Lepson/print/screen/SettingScr;->paperSourceInfo:Landroid/widget/TextView;

    iget-object p3, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {p3, p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result p3

    invoke-virtual {p0, p3}, Lepson/print/screen/SettingScr;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1273
    iget-object p2, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {p2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    .line 1276
    iget-object p2, p0, Lepson/print/screen/SettingScr;->paper_source_info:[I

    if-eqz p2, :cond_4

    array-length p2, p2

    if-le p2, v2, :cond_4

    .line 1277
    iput p1, p0, Lepson/print/screen/SettingScr;->paperSource:I

    .line 1281
    :cond_4
    iput-boolean v3, p0, Lepson/print/screen/SettingScr;->undoFlag:Z

    .line 1282
    iget-object p1, p0, Lepson/print/screen/SettingScr;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_2

    .line 1224
    :sswitch_5
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p2

    const-string p3, "INDEX"

    invoke-virtual {p2, p3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p2

    iput p2, p0, Lepson/print/screen/SettingScr;->sizeIndex:I

    .line 1226
    new-instance p2, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize;

    invoke-direct {p2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize;-><init>()V

    iput-object p2, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 1227
    iget-object p2, p0, Lepson/print/screen/SettingScr;->paperSizeInfo:Landroid/widget/TextView;

    iget-object p3, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {p3, p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result p3

    invoke-virtual {p0, p3}, Lepson/print/screen/SettingScr;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1228
    iget-object p2, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {p2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    .line 1230
    iput p1, p0, Lepson/print/screen/SettingScr;->paperSize:I

    .line 1231
    iget-object p1, p0, Lepson/print/screen/SettingScr;->mPaperSizeType:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    iget p2, p0, Lepson/print/screen/SettingScr;->paperSize:I

    invoke-virtual {p1, p2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getID(I)I

    move-result p1

    iput p1, p0, Lepson/print/screen/SettingScr;->paperType:I

    .line 1233
    iput-boolean v3, p0, Lepson/print/screen/SettingScr;->undoFlag:Z

    .line 1234
    iget-object p1, p0, Lepson/print/screen/SettingScr;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_2

    .line 1252
    :sswitch_6
    new-instance p2, Lepson/print/widgets/LayoutEx;

    invoke-direct {p2}, Lepson/print/widgets/LayoutEx;-><init>()V

    iput-object p2, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 1253
    iget-object p2, p0, Lepson/print/screen/SettingScr;->layoutInfo:Landroid/widget/TextView;

    iget-object p3, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {p3, p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result p3

    invoke-virtual {p0, p3}, Lepson/print/screen/SettingScr;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1254
    iget-object p2, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {p2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    .line 1255
    iput p1, p0, Lepson/print/screen/SettingScr;->layout:I

    .line 1258
    iput-boolean v3, p0, Lepson/print/screen/SettingScr;->undoFlag:Z

    .line 1259
    iget-object p1, p0, Lepson/print/screen/SettingScr;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_2

    .line 1303
    :sswitch_7
    new-instance p2, Lcom/epson/mobilephone/common/escpr/MediaInfo$FeedDirection;

    invoke-direct {p2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$FeedDirection;-><init>()V

    iput-object p2, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 1304
    iget-object p2, p0, Lepson/print/screen/SettingScr;->feedDirectionInfo:Landroid/widget/TextView;

    iget-object p3, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 1305
    invoke-virtual {p3, p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result p3

    invoke-virtual {p0, p3}, Lepson/print/screen/SettingScr;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1306
    iget-object p2, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {p2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    .line 1307
    iput p1, p0, Lepson/print/screen/SettingScr;->feedDirection:I

    goto :goto_2

    .line 1295
    :sswitch_8
    new-instance p2, Lcom/epson/mobilephone/common/escpr/MediaInfo$Duplex;

    invoke-direct {p2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$Duplex;-><init>()V

    iput-object p2, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 1296
    iget-object p2, p0, Lepson/print/screen/SettingScr;->duplexInfo:Landroid/widget/TextView;

    iget-object p3, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 1297
    invoke-virtual {p3, p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result p3

    invoke-virtual {p0, p3}, Lepson/print/screen/SettingScr;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1298
    iget-object p2, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {p2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    .line 1299
    iput p1, p0, Lepson/print/screen/SettingScr;->duplex:I

    goto :goto_2

    .line 1287
    :sswitch_9
    new-instance p2, Lcom/epson/mobilephone/common/escpr/MediaInfo$Color;

    invoke-direct {p2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$Color;-><init>()V

    iput-object p2, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 1288
    iget-object p2, p0, Lepson/print/screen/SettingScr;->colorInfo:Landroid/widget/TextView;

    iget-object p3, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 1289
    invoke-virtual {p3, p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result p3

    invoke-virtual {p0, p3}, Lepson/print/screen/SettingScr;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1290
    iget-object p2, p0, Lepson/print/screen/SettingScr;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {p2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    .line 1291
    iput p1, p0, Lepson/print/screen/SettingScr;->color:I

    goto :goto_2

    :sswitch_a
    if-ne p1, v2, :cond_5

    goto :goto_1

    :cond_5
    const/4 v2, 0x0

    .line 1319
    :goto_1
    iput v2, p0, Lepson/print/screen/SettingScr;->photoApfValueForLocalPrinter:I

    .line 1320
    iget p1, p0, Lepson/print/screen/SettingScr;->photoApfValueForLocalPrinter:I

    invoke-direct {p0, p1}, Lepson/print/screen/SettingScr;->displayApfState(I)V

    .line 1334
    :cond_6
    :goto_2
    invoke-direct {p0}, Lepson/print/screen/SettingScr;->updatePrinterIcon()V

    return-void

    :sswitch_data_0
    .sparse-switch
        0x7f080063 -> :sswitch_a
        0x7f0800d2 -> :sswitch_9
        0x7f08011d -> :sswitch_8
        0x7f08013d -> :sswitch_7
        0x7f0801b6 -> :sswitch_6
        0x7f08024a -> :sswitch_5
        0x7f08024f -> :sswitch_4
        0x7f080252 -> :sswitch_3
        0x7f08027b -> :sswitch_2
        0x7f0802a0 -> :sswitch_1
        0x7f0802fa -> :sswitch_0
    .end sparse-switch
.end method

.method public onBackPressed()V
    .locals 2

    .line 2474
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onBackPressed()V

    const/4 v0, 0x1

    .line 2476
    iput-boolean v0, p0, Lepson/print/screen/SettingScr;->mFinishRequested:Z

    .line 2480
    iget-object v0, p0, Lepson/print/screen/SettingScr;->mEpsonService:Lepson/print/service/IEpsonService;

    if-eqz v0, :cond_0

    .line 2482
    :try_start_0
    invoke-interface {v0}, Lepson/print/service/IEpsonService;->cancelSearchPrinter()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 2485
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 2490
    :cond_0
    :goto_0
    invoke-direct {p0}, Lepson/print/screen/SettingScr;->loadSupportedMediaFile()Z

    .line 2493
    new-instance v0, Lepson/print/EPPrinterManager;

    iget-object v1, p0, Lepson/print/screen/SettingScr;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lepson/print/EPPrinterManager;-><init>(Landroid/content/Context;)V

    .line 2495
    invoke-virtual {v0}, Lepson/print/EPPrinterManager;->rollbackRemotePrinterInfo()V

    .line 2496
    invoke-virtual {v0}, Lepson/print/EPPrinterManager;->rollbackIPPrinterInfo()V

    .line 2499
    new-instance v0, Lepson/print/inkrpln/InkRplnRepository;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lepson/print/inkrpln/InkRplnRepository;-><init>(Z)V

    .line 2500
    invoke-virtual {v0, p0}, Lepson/print/inkrpln/InkRplnRepository;->deleteTemporaryData(Landroid/content/Context;)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .line 859
    iget-object v0, p0, Lepson/print/screen/SettingScr;->progressGetOption:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    return-void

    .line 863
    :cond_0
    iget-object v0, p0, Lepson/print/screen/SettingScr;->mEpsonService:Lepson/print/service/IEpsonService;

    if-eqz v0, :cond_1

    .line 865
    :try_start_0
    invoke-interface {v0}, Lepson/print/service/IEpsonService;->cancelSearchPrinter()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 868
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 871
    :cond_1
    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    const/16 v0, 0x32

    const/16 v1, -0x32

    const/4 v2, 0x0

    const/4 v3, 0x1

    sparse-switch p1, :sswitch_data_0

    goto/16 :goto_9

    .line 1070
    :sswitch_0
    iget p1, p0, Lepson/print/screen/SettingScr;->mSharpnessValue:I

    const v0, 0x7f0802fa

    invoke-virtual {p0, p1, v0}, Lepson/print/screen/SettingScr;->launchDetailScreen(II)V

    goto/16 :goto_9

    .line 966
    :sswitch_1
    iget p1, p0, Lepson/print/screen/SettingScr;->saturationValue:I

    add-int/2addr p1, v3

    iput p1, p0, Lepson/print/screen/SettingScr;->saturationValue:I

    .line 967
    iget p1, p0, Lepson/print/screen/SettingScr;->saturationValue:I

    if-lt p1, v0, :cond_2

    .line 968
    iput v0, p0, Lepson/print/screen/SettingScr;->saturationValue:I

    .line 969
    iget-object p1, p0, Lepson/print/screen/SettingScr;->saturationPlus:Landroid/widget/Button;

    invoke-virtual {p1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_1

    .line 971
    :cond_2
    iget-object p1, p0, Lepson/print/screen/SettingScr;->saturationPlus:Landroid/widget/Button;

    invoke-virtual {p1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 973
    :goto_1
    iget-object p1, p0, Lepson/print/screen/SettingScr;->saturationMinus:Landroid/widget/Button;

    invoke-virtual {p1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 974
    iget-object p1, p0, Lepson/print/screen/SettingScr;->saturation:Landroid/widget/TextView;

    iget v0, p0, Lepson/print/screen/SettingScr;->saturationValue:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    .line 954
    :sswitch_2
    iget p1, p0, Lepson/print/screen/SettingScr;->saturationValue:I

    sub-int/2addr p1, v3

    iput p1, p0, Lepson/print/screen/SettingScr;->saturationValue:I

    .line 955
    iget p1, p0, Lepson/print/screen/SettingScr;->saturationValue:I

    if-gt p1, v1, :cond_3

    .line 956
    iput v1, p0, Lepson/print/screen/SettingScr;->saturationValue:I

    .line 957
    iget-object p1, p0, Lepson/print/screen/SettingScr;->saturationMinus:Landroid/widget/Button;

    invoke-virtual {p1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_2

    .line 959
    :cond_3
    iget-object p1, p0, Lepson/print/screen/SettingScr;->saturationMinus:Landroid/widget/Button;

    invoke-virtual {p1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 961
    :goto_2
    iget-object p1, p0, Lepson/print/screen/SettingScr;->saturationPlus:Landroid/widget/Button;

    invoke-virtual {p1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 962
    iget-object p1, p0, Lepson/print/screen/SettingScr;->saturation:Landroid/widget/TextView;

    iget v0, p0, Lepson/print/screen/SettingScr;->saturationValue:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    .line 1036
    :sswitch_3
    iget p1, p0, Lepson/print/screen/SettingScr;->quality:I

    const v0, 0x7f0802a0

    invoke-virtual {p0, p1, v0}, Lepson/print/screen/SettingScr;->launchDetailScreen(II)V

    goto/16 :goto_9

    .line 980
    :sswitch_4
    invoke-direct {p0}, Lepson/print/screen/SettingScr;->unbindEpsonService()V

    .line 983
    new-instance p1, Lepson/print/screen/SettingScr$1;

    invoke-direct {p1, p0}, Lepson/print/screen/SettingScr$1;-><init>(Lepson/print/screen/SettingScr;)V

    new-array v0, v2, [Ljava/lang/Void;

    .line 1019
    invoke-virtual {p1, v0}, Lepson/print/screen/SettingScr$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_9

    .line 1062
    :sswitch_5
    iget p1, p0, Lepson/print/screen/SettingScr;->printdate:I

    const v0, 0x7f08027b

    invoke-virtual {p0, p1, v0}, Lepson/print/screen/SettingScr;->launchDetailScreen(II)V

    goto/16 :goto_9

    .line 1028
    :sswitch_6
    iget p1, p0, Lepson/print/screen/SettingScr;->paperType:I

    const v0, 0x7f080252

    invoke-virtual {p0, p1, v0}, Lepson/print/screen/SettingScr;->launchDetailScreen(II)V

    goto/16 :goto_9

    .line 1041
    :sswitch_7
    iget p1, p0, Lepson/print/screen/SettingScr;->paperSource:I

    .line 1042
    iget-object v0, p0, Lepson/print/screen/SettingScr;->paper_source_info:[I

    if-eqz v0, :cond_4

    array-length v0, v0

    if-gt v0, v3, :cond_4

    const/16 p1, 0x80

    :cond_4
    const v0, 0x7f08024f

    .line 1046
    invoke-virtual {p0, p1, v0}, Lepson/print/screen/SettingScr;->launchDetailScreen(II)V

    goto/16 :goto_9

    .line 1024
    :sswitch_8
    iget p1, p0, Lepson/print/screen/SettingScr;->paperSize:I

    const v0, 0x7f08024a

    invoke-virtual {p0, p1, v0}, Lepson/print/screen/SettingScr;->launchDetailScreen(II)V

    goto/16 :goto_9

    .line 897
    :sswitch_9
    new-instance p1, Landroid/content/Intent;

    const-class v0, Lepson/print/screen/PageRangeSetting;

    invoke-direct {p1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "SHEETS"

    .line 898
    iget v1, p0, Lepson/print/screen/SettingScr;->sheets:I

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "printAll"

    .line 899
    iget-boolean v1, p0, Lepson/print/screen/SettingScr;->printAll:Z

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "startValue"

    .line 900
    iget v1, p0, Lepson/print/screen/SettingScr;->startValue:I

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "endValue"

    .line 901
    iget v1, p0, Lepson/print/screen/SettingScr;->endValue:I

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 v0, 0x2

    .line 902
    invoke-virtual {p0, p1, v0}, Lepson/print/screen/SettingScr;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_9

    .line 1032
    :sswitch_a
    iget p1, p0, Lepson/print/screen/SettingScr;->layout:I

    const v0, 0x7f0801b6

    invoke-virtual {p0, p1, v0}, Lepson/print/screen/SettingScr;->launchDetailScreen(II)V

    goto/16 :goto_9

    .line 1058
    :sswitch_b
    iget p1, p0, Lepson/print/screen/SettingScr;->feedDirection:I

    const v0, 0x7f08013d

    invoke-virtual {p0, p1, v0}, Lepson/print/screen/SettingScr;->launchDetailScreen(II)V

    goto/16 :goto_9

    .line 1054
    :sswitch_c
    iget p1, p0, Lepson/print/screen/SettingScr;->duplex:I

    const v0, 0x7f08011d

    invoke-virtual {p0, p1, v0}, Lepson/print/screen/SettingScr;->launchDetailScreen(II)V

    goto/16 :goto_9

    .line 885
    :sswitch_d
    iget p1, p0, Lepson/print/screen/SettingScr;->copiesValue:I

    add-int/2addr p1, v3

    iput p1, p0, Lepson/print/screen/SettingScr;->copiesValue:I

    .line 886
    iget p1, p0, Lepson/print/screen/SettingScr;->copiesValue:I

    const/16 v0, 0x1e

    if-lt p1, v0, :cond_5

    .line 887
    iput v0, p0, Lepson/print/screen/SettingScr;->copiesValue:I

    .line 888
    iget-object p1, p0, Lepson/print/screen/SettingScr;->copiesPlus:Landroid/widget/Button;

    invoke-virtual {p1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_3

    .line 890
    :cond_5
    iget-object p1, p0, Lepson/print/screen/SettingScr;->copiesPlus:Landroid/widget/Button;

    invoke-virtual {p1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 892
    :goto_3
    iget-object p1, p0, Lepson/print/screen/SettingScr;->copiesMinus:Landroid/widget/Button;

    invoke-virtual {p1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 893
    iget-object p1, p0, Lepson/print/screen/SettingScr;->copies:Landroid/widget/TextView;

    iget v0, p0, Lepson/print/screen/SettingScr;->copiesValue:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_9

    .line 873
    :sswitch_e
    iget p1, p0, Lepson/print/screen/SettingScr;->copiesValue:I

    sub-int/2addr p1, v3

    iput p1, p0, Lepson/print/screen/SettingScr;->copiesValue:I

    .line 874
    iget p1, p0, Lepson/print/screen/SettingScr;->copiesValue:I

    if-gt p1, v3, :cond_6

    .line 875
    iput v3, p0, Lepson/print/screen/SettingScr;->copiesValue:I

    .line 876
    iget-object p1, p0, Lepson/print/screen/SettingScr;->copiesMinus:Landroid/widget/Button;

    invoke-virtual {p1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_4

    .line 878
    :cond_6
    iget-object p1, p0, Lepson/print/screen/SettingScr;->copiesMinus:Landroid/widget/Button;

    invoke-virtual {p1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 880
    :goto_4
    iget-object p1, p0, Lepson/print/screen/SettingScr;->copiesPlus:Landroid/widget/Button;

    invoke-virtual {p1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 881
    iget-object p1, p0, Lepson/print/screen/SettingScr;->copies:Landroid/widget/TextView;

    iget v0, p0, Lepson/print/screen/SettingScr;->copiesValue:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_9

    .line 942
    :sswitch_f
    iget p1, p0, Lepson/print/screen/SettingScr;->contrastValue:I

    add-int/2addr p1, v3

    iput p1, p0, Lepson/print/screen/SettingScr;->contrastValue:I

    .line 943
    iget p1, p0, Lepson/print/screen/SettingScr;->contrastValue:I

    if-lt p1, v0, :cond_7

    .line 944
    iput v0, p0, Lepson/print/screen/SettingScr;->contrastValue:I

    .line 945
    iget-object p1, p0, Lepson/print/screen/SettingScr;->contrastPlus:Landroid/widget/Button;

    invoke-virtual {p1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_5

    .line 947
    :cond_7
    iget-object p1, p0, Lepson/print/screen/SettingScr;->contrastPlus:Landroid/widget/Button;

    invoke-virtual {p1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 949
    :goto_5
    iget-object p1, p0, Lepson/print/screen/SettingScr;->contrastMinus:Landroid/widget/Button;

    invoke-virtual {p1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 950
    iget-object p1, p0, Lepson/print/screen/SettingScr;->contrast:Landroid/widget/TextView;

    iget v0, p0, Lepson/print/screen/SettingScr;->contrastValue:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    .line 930
    :sswitch_10
    iget p1, p0, Lepson/print/screen/SettingScr;->contrastValue:I

    sub-int/2addr p1, v3

    iput p1, p0, Lepson/print/screen/SettingScr;->contrastValue:I

    .line 931
    iget p1, p0, Lepson/print/screen/SettingScr;->contrastValue:I

    if-gt p1, v1, :cond_8

    .line 932
    iput v1, p0, Lepson/print/screen/SettingScr;->contrastValue:I

    .line 933
    iget-object p1, p0, Lepson/print/screen/SettingScr;->contrastMinus:Landroid/widget/Button;

    invoke-virtual {p1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_6

    .line 935
    :cond_8
    iget-object p1, p0, Lepson/print/screen/SettingScr;->contrastMinus:Landroid/widget/Button;

    invoke-virtual {p1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 937
    :goto_6
    iget-object p1, p0, Lepson/print/screen/SettingScr;->contrastPlus:Landroid/widget/Button;

    invoke-virtual {p1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 938
    iget-object p1, p0, Lepson/print/screen/SettingScr;->contrast:Landroid/widget/TextView;

    iget v0, p0, Lepson/print/screen/SettingScr;->contrastValue:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    .line 1050
    :sswitch_11
    iget p1, p0, Lepson/print/screen/SettingScr;->color:I

    const v0, 0x7f0800d2

    invoke-virtual {p0, p1, v0}, Lepson/print/screen/SettingScr;->launchDetailScreen(II)V

    goto :goto_9

    .line 918
    :sswitch_12
    iget p1, p0, Lepson/print/screen/SettingScr;->brightnessValue:I

    add-int/2addr p1, v3

    iput p1, p0, Lepson/print/screen/SettingScr;->brightnessValue:I

    .line 919
    iget p1, p0, Lepson/print/screen/SettingScr;->brightnessValue:I

    if-lt p1, v0, :cond_9

    .line 920
    iput v0, p0, Lepson/print/screen/SettingScr;->brightnessValue:I

    .line 921
    iget-object p1, p0, Lepson/print/screen/SettingScr;->brightnessPlus:Landroid/widget/Button;

    invoke-virtual {p1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_7

    .line 923
    :cond_9
    iget-object p1, p0, Lepson/print/screen/SettingScr;->brightnessPlus:Landroid/widget/Button;

    invoke-virtual {p1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 925
    :goto_7
    iget-object p1, p0, Lepson/print/screen/SettingScr;->brightnessMinus:Landroid/widget/Button;

    invoke-virtual {p1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 926
    iget-object p1, p0, Lepson/print/screen/SettingScr;->brightness:Landroid/widget/TextView;

    iget v0, p0, Lepson/print/screen/SettingScr;->brightnessValue:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    .line 906
    :sswitch_13
    iget p1, p0, Lepson/print/screen/SettingScr;->brightnessValue:I

    sub-int/2addr p1, v3

    iput p1, p0, Lepson/print/screen/SettingScr;->brightnessValue:I

    .line 907
    iget p1, p0, Lepson/print/screen/SettingScr;->brightnessValue:I

    if-gt p1, v1, :cond_a

    .line 908
    iput v1, p0, Lepson/print/screen/SettingScr;->brightnessValue:I

    .line 909
    iget-object p1, p0, Lepson/print/screen/SettingScr;->brightnessMinus:Landroid/widget/Button;

    invoke-virtual {p1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_8

    .line 911
    :cond_a
    iget-object p1, p0, Lepson/print/screen/SettingScr;->brightnessMinus:Landroid/widget/Button;

    invoke-virtual {p1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 913
    :goto_8
    iget-object p1, p0, Lepson/print/screen/SettingScr;->brightnessPlus:Landroid/widget/Button;

    invoke-virtual {p1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 914
    iget-object p1, p0, Lepson/print/screen/SettingScr;->brightness:Landroid/widget/TextView;

    iget v0, p0, Lepson/print/screen/SettingScr;->brightnessValue:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    .line 1066
    :sswitch_14
    iget p1, p0, Lepson/print/screen/SettingScr;->photoApfValueForLocalPrinter:I

    const v0, 0x7f080063

    invoke-virtual {p0, p1, v0}, Lepson/print/screen/SettingScr;->launchDetailScreen(II)V

    :goto_9
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f080063 -> :sswitch_14
        0x7f080080 -> :sswitch_13
        0x7f080082 -> :sswitch_12
        0x7f0800d2 -> :sswitch_11
        0x7f0800de -> :sswitch_10
        0x7f0800df -> :sswitch_f
        0x7f0800e1 -> :sswitch_e
        0x7f0800e2 -> :sswitch_d
        0x7f08011d -> :sswitch_c
        0x7f08013d -> :sswitch_b
        0x7f0801b6 -> :sswitch_a
        0x7f080244 -> :sswitch_9
        0x7f08024a -> :sswitch_8
        0x7f08024f -> :sswitch_7
        0x7f080252 -> :sswitch_6
        0x7f08027b -> :sswitch_5
        0x7f08027e -> :sswitch_4
        0x7f0802a0 -> :sswitch_3
        0x7f0802c1 -> :sswitch_2
        0x7f0802c2 -> :sswitch_1
        0x7f0802fa -> :sswitch_0
    .end sparse-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .line 2616
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2617
    invoke-virtual {p0}, Lepson/print/screen/SettingScr;->deleteLongTapMessage()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .line 174
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0a00bf

    .line 177
    invoke-virtual {p0, p1}, Lepson/print/screen/SettingScr;->setContentView(I)V

    const/4 p1, 0x1

    const v0, 0x7f0e0475

    .line 180
    invoke-virtual {p0, v0, p1}, Lepson/print/screen/SettingScr;->setActionBar(IZ)V

    .line 183
    invoke-direct {p0}, Lepson/print/screen/SettingScr;->bindEpsonService()V

    .line 186
    iput-object p0, p0, Lepson/print/screen/SettingScr;->mContext:Landroid/content/Context;

    .line 190
    invoke-virtual {p0}, Lepson/print/screen/SettingScr;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "PRINT_DOCUMENT"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lepson/print/screen/SettingScr;->isDocumentSetting:Z

    .line 191
    iget-boolean v0, p0, Lepson/print/screen/SettingScr;->isDocumentSetting:Z

    if-eqz v0, :cond_0

    .line 193
    invoke-virtual {p0}, Lepson/print/screen/SettingScr;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "SHEETS"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lepson/print/screen/SettingScr;->sheets:I

    .line 194
    invoke-virtual {p0}, Lepson/print/screen/SettingScr;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "PRINTAREA"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lepson/print/screen/SettingScr;->disablePrintArea:Z

    :cond_0
    const v0, 0x7f080284

    .line 198
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lepson/print/screen/SettingScr;->printerName:Landroid/widget/TextView;

    const v0, 0x7f08024c

    .line 199
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lepson/print/screen/SettingScr;->paperSizeInfo:Landroid/widget/TextView;

    const v0, 0x7f080253

    .line 200
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lepson/print/screen/SettingScr;->paperTypeInfo:Landroid/widget/TextView;

    const v0, 0x7f0801b8

    .line 201
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lepson/print/screen/SettingScr;->layoutInfo:Landroid/widget/TextView;

    const v0, 0x7f0802a1

    .line 202
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lepson/print/screen/SettingScr;->qualityInfo:Landroid/widget/TextView;

    const v0, 0x7f080250

    .line 203
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lepson/print/screen/SettingScr;->paperSourceInfo:Landroid/widget/TextView;

    const v0, 0x7f0800e0

    .line 204
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lepson/print/screen/SettingScr;->copies:Landroid/widget/TextView;

    const v0, 0x7f0800e1

    .line 207
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lepson/print/screen/SettingScr;->copiesMinus:Landroid/widget/Button;

    const v0, 0x7f0800e2

    .line 208
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lepson/print/screen/SettingScr;->copiesPlus:Landroid/widget/Button;

    .line 211
    iget-object v0, p0, Lepson/print/screen/SettingScr;->copiesMinus:Landroid/widget/Button;

    invoke-static {v0}, Lepson/print/widgets/LongTapRepeatAdapter;->bless(Landroid/view/View;)V

    .line 212
    iget-object v0, p0, Lepson/print/screen/SettingScr;->copiesPlus:Landroid/widget/Button;

    invoke-static {v0}, Lepson/print/widgets/LongTapRepeatAdapter;->bless(Landroid/view/View;)V

    const v0, 0x7f0800d5

    .line 215
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lepson/print/screen/SettingScr;->colorInfo:Landroid/widget/TextView;

    const v0, 0x7f08011e

    .line 216
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lepson/print/screen/SettingScr;->duplexInfo:Landroid/widget/TextView;

    const v0, 0x7f08013e

    .line 217
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lepson/print/screen/SettingScr;->feedDirectionInfo:Landroid/widget/TextView;

    const v0, 0x7f08027c

    .line 218
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lepson/print/screen/SettingScr;->printDateInfo:Landroid/widget/TextView;

    const v0, 0x7f080064

    .line 219
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lepson/print/screen/SettingScr;->apfStateText:Landroid/widget/TextView;

    const v0, 0x7f0802fb

    .line 220
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lepson/print/screen/SettingScr;->mSharpnessValueText:Landroid/widget/TextView;

    const v0, 0x7f08007e

    .line 223
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lepson/print/screen/SettingScr;->brightness:Landroid/widget/TextView;

    const v0, 0x7f080080

    .line 224
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lepson/print/screen/SettingScr;->brightnessMinus:Landroid/widget/Button;

    const v0, 0x7f080082

    .line 225
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lepson/print/screen/SettingScr;->brightnessPlus:Landroid/widget/Button;

    const v0, 0x7f0800dd

    .line 226
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lepson/print/screen/SettingScr;->contrast:Landroid/widget/TextView;

    const v0, 0x7f0800de

    .line 227
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lepson/print/screen/SettingScr;->contrastMinus:Landroid/widget/Button;

    const v0, 0x7f0800df

    .line 228
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lepson/print/screen/SettingScr;->contrastPlus:Landroid/widget/Button;

    const v0, 0x7f0802c0

    .line 229
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lepson/print/screen/SettingScr;->saturation:Landroid/widget/TextView;

    const v0, 0x7f0802c1

    .line 230
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lepson/print/screen/SettingScr;->saturationMinus:Landroid/widget/Button;

    const v0, 0x7f0802c2

    .line 231
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lepson/print/screen/SettingScr;->saturationPlus:Landroid/widget/Button;

    const v0, 0x7f080297

    .line 234
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lepson/print/screen/SettingScr;->progressGetOption:Landroid/view/View;

    const-string v0, "PREFS_EPSON_CONNECT"

    .line 237
    invoke-virtual {p0, v0, v2}, Lepson/print/screen/SettingScr;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "ENABLE_SHOW_PREVIEW"

    .line 238
    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lepson/print/screen/SettingScr;->enableShowPreview:Z

    .line 241
    invoke-direct {p0}, Lepson/print/screen/SettingScr;->loadConfig()V

    .line 243
    invoke-direct {p0}, Lepson/print/screen/SettingScr;->setClickListener()V

    .line 246
    invoke-direct {p0}, Lepson/print/screen/SettingScr;->updatePrinterIcon()V

    .line 248
    iget-object v0, p0, Lepson/print/screen/SettingScr;->printerId:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 250
    iput-boolean v2, p0, Lepson/print/screen/SettingScr;->undoFlag:Z

    .line 251
    invoke-direct {p0}, Lepson/print/screen/SettingScr;->loadSupportedMediaFile()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 252
    iget-object v0, p0, Lepson/print/screen/SettingScr;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 254
    :cond_1
    iget-object p1, p0, Lepson/print/screen/SettingScr;->mHandler:Landroid/os/Handler;

    const/16 v0, 0x11

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 262
    :cond_2
    iget-object v0, p0, Lepson/print/screen/SettingScr;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_0
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .line 2582
    invoke-virtual {p0}, Lepson/print/screen/SettingScr;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0b0003

    .line 2583
    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 2585
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result p1

    return p1
.end method

.method protected onDestroy()V
    .locals 2

    .line 813
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onDestroy()V

    .line 816
    invoke-direct {p0}, Lepson/print/screen/SettingScr;->unbindEpsonService()V

    const-string v0, "SettingScr"

    const-string v1, "onDestroy"

    .line 818
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 819
    iget-object v0, p0, Lepson/print/screen/SettingScr;->mPaperSizeType:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .line 2591
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f080212

    if-eq v0, v1, :cond_0

    .line 2600
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1

    .line 2593
    :cond_0
    invoke-direct {p0}, Lepson/print/screen/SettingScr;->saveChanged()V

    .line 2594
    invoke-virtual {p0}, Lepson/print/screen/SettingScr;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const/4 v0, 0x3

    .line 2595
    invoke-virtual {p0, v0, p1}, Lepson/print/screen/SettingScr;->setResult(ILandroid/content/Intent;)V

    .line 2596
    invoke-virtual {p0}, Lepson/print/screen/SettingScr;->finish()V

    const/4 p1, 0x1

    return p1
.end method

.method protected onPause()V
    .locals 2

    const-string v0, "SettingScr"

    const-string v1, "onPause"

    .line 2564
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2565
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onPause()V

    .line 2567
    invoke-virtual {p0}, Lepson/print/screen/SettingScr;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 2568
    iput-boolean v0, p0, Lepson/print/screen/SettingScr;->mFinishRequested:Z

    .line 2569
    invoke-virtual {p0}, Lepson/print/screen/SettingScr;->deleteLongTapMessage()V

    :cond_0
    const-string v0, "printer"

    .line 2573
    iget-object v1, p0, Lepson/print/screen/SettingScr;->printerIp:Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->disconnect(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    .line 2575
    iget-object v0, p0, Lepson/print/screen/SettingScr;->autoConnectSsid:Ljava/lang/String;

    iget-object v1, p0, Lepson/print/screen/SettingScr;->printerIp:Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->disconnectSimpleAP(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    return-void
.end method

.method protected onResume()V
    .locals 2

    .line 2546
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onResume()V

    const-string v0, "SettingScr"

    const-string v1, "onResume()"

    .line 2547
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2550
    iget-object v0, p0, Lepson/print/screen/SettingScr;->printerId:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget v0, p0, Lepson/print/screen/SettingScr;->printerLocation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-boolean v0, p0, Lepson/print/screen/SettingScr;->isRetryAfterConnectSimpleAp:Z

    if-ne v0, v1, :cond_1

    iget-boolean v0, p0, Lepson/print/screen/SettingScr;->mWaiteInkReplenProgress:Z

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 2552
    iput-boolean v0, p0, Lepson/print/screen/SettingScr;->undoFlag:Z

    .line 2553
    invoke-direct {p0}, Lepson/print/screen/SettingScr;->loadSupportedMediaFile()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2554
    iget-object v0, p0, Lepson/print/screen/SettingScr;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 2556
    :cond_0
    iget-object v0, p0, Lepson/print/screen/SettingScr;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_1
    :goto_0
    return-void
.end method

.method resetSettings()V
    .locals 2

    const/4 v0, 0x0

    .line 1597
    iput-object v0, p0, Lepson/print/screen/SettingScr;->printerDeviceId:Ljava/lang/String;

    .line 1598
    iput-object v0, p0, Lepson/print/screen/SettingScr;->printerId:Ljava/lang/String;

    .line 1599
    iput-object v0, p0, Lepson/print/screen/SettingScr;->printerEmailAddress:Ljava/lang/String;

    .line 1600
    iput-object v0, p0, Lepson/print/screen/SettingScr;->printerIp:Ljava/lang/String;

    const/4 v1, 0x0

    .line 1601
    iput v1, p0, Lepson/print/screen/SettingScr;->printerLocation:I

    .line 1603
    iput-object v0, p0, Lepson/print/screen/SettingScr;->info:[I

    .line 1604
    iput-object v0, p0, Lepson/print/screen/SettingScr;->paper_source_info:[I

    .line 1605
    iput-object v0, p0, Lepson/print/screen/SettingScr;->color_info:[I

    .line 1606
    iput-object v0, p0, Lepson/print/screen/SettingScr;->paper_size_info:[I

    .line 1607
    iput-object v0, p0, Lepson/print/screen/SettingScr;->paper_type_info:[I

    .line 1608
    iput-object v0, p0, Lepson/print/screen/SettingScr;->layout_info:[I

    .line 1609
    iput-object v0, p0, Lepson/print/screen/SettingScr;->quality_info:[I

    .line 1610
    iput-object v0, p0, Lepson/print/screen/SettingScr;->duplex_info:[I

    const-string v0, ""

    .line 1611
    iput-object v0, p0, Lepson/print/screen/SettingScr;->autoConnectSsid:Ljava/lang/String;

    .line 1613
    iget-object v0, p0, Lepson/print/screen/SettingScr;->printerName:Landroid/widget/TextView;

    const v1, 0x7f0e04d6

    invoke-virtual {p0, v1}, Lepson/print/screen/SettingScr;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f080198

    .line 1614
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1616
    invoke-virtual {p0}, Lepson/print/screen/SettingScr;->updateSettingView()V

    const/4 v0, 0x1

    .line 1617
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, v0}, Lepson/print/screen/SettingScr;->setScreenState(Ljava/lang/Boolean;)V

    return-void
.end method

.method setClickablePageRange(Z)V
    .locals 4

    const v0, 0x7f080244

    .line 1588
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setClickable(Z)V

    const v0, 0x7f080242

    .line 1589
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    const/16 v2, 0x8

    if-eqz p1, :cond_0

    const/4 v3, 0x0

    goto :goto_0

    :cond_0
    const/16 v3, 0x8

    :goto_0
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f080245

    .line 1590
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    const/16 v1, 0x8

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method setVisibilityPageRange(Z)V
    .locals 4

    const v0, 0x7f080244

    .line 1578
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    const/16 v2, 0x8

    if-eqz p1, :cond_0

    const/4 v3, 0x0

    goto :goto_0

    :cond_0
    const/16 v3, 0x8

    :goto_0
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f080243

    .line 1579
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    const/16 v1, 0x8

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public showErrorDialog(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 2527
    new-instance v0, Lepson/print/widgets/CustomTitleAlertDialogBuilder;

    invoke-direct {v0, p0}, Lepson/print/widgets/CustomTitleAlertDialogBuilder;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    .line 2528
    invoke-virtual {v0, v1}, Lepson/print/widgets/CustomTitleAlertDialogBuilder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 2529
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 2530
    invoke-virtual {p1, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const p2, 0x7f0e03fe

    .line 2531
    invoke-virtual {p0, p2}, Lepson/print/screen/SettingScr;->getString(I)Ljava/lang/String;

    move-result-object p2

    new-instance v0, Lepson/print/screen/SettingScr$5;

    invoke-direct {v0, p0}, Lepson/print/screen/SettingScr$5;-><init>(Lepson/print/screen/SettingScr;)V

    invoke-virtual {p1, p2, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 2538
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/AlertDialog;->show()V

    const/4 p1, 0x1

    .line 2540
    invoke-direct {p0, p1}, Lepson/print/screen/SettingScr;->updateSupportedMediaFile(Z)Z

    return-void
.end method

.method updateSettingView()V
    .locals 7

    .line 1440
    iget-boolean v0, p0, Lepson/print/screen/SettingScr;->isDocumentSetting:Z

    const/4 v1, 0x2

    const/4 v2, 0x1

    const/16 v3, 0x8

    const/4 v4, 0x0

    if-eqz v0, :cond_4

    .line 1442
    iget v0, p0, Lepson/print/screen/SettingScr;->printerLocation:I

    if-ne v0, v1, :cond_2

    .line 1443
    iget-boolean v0, p0, Lepson/print/screen/SettingScr;->enableShowPreview:Z

    if-nez v0, :cond_0

    .line 1445
    invoke-virtual {p0, v4}, Lepson/print/screen/SettingScr;->setVisibilityPageRange(Z)V

    goto :goto_0

    .line 1446
    :cond_0
    iget-boolean v0, p0, Lepson/print/screen/SettingScr;->disablePrintArea:Z

    if-ne v0, v2, :cond_1

    .line 1448
    invoke-virtual {p0, v2}, Lepson/print/screen/SettingScr;->setVisibilityPageRange(Z)V

    .line 1449
    invoke-virtual {p0, v4}, Lepson/print/screen/SettingScr;->setClickablePageRange(Z)V

    goto :goto_0

    .line 1452
    :cond_1
    invoke-virtual {p0, v2}, Lepson/print/screen/SettingScr;->setVisibilityPageRange(Z)V

    .line 1453
    invoke-virtual {p0, v2}, Lepson/print/screen/SettingScr;->setClickablePageRange(Z)V

    goto :goto_0

    .line 1457
    :cond_2
    invoke-virtual {p0, v2}, Lepson/print/screen/SettingScr;->setVisibilityPageRange(Z)V

    .line 1458
    invoke-virtual {p0, v2}, Lepson/print/screen/SettingScr;->setClickablePageRange(Z)V

    .line 1461
    :goto_0
    iget v0, p0, Lepson/print/screen/SettingScr;->sheets:I

    const v5, 0x7f080242

    if-ne v0, v2, :cond_3

    .line 1462
    invoke-virtual {p0, v5}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 1464
    :cond_3
    invoke-virtual {p0, v5}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 1469
    :cond_4
    invoke-virtual {p0, v4}, Lepson/print/screen/SettingScr;->setVisibilityPageRange(Z)V

    .line 1475
    :goto_1
    iget-object v0, p0, Lepson/print/screen/SettingScr;->color_info:[I

    const v5, 0x7f0800d6

    if-eqz v0, :cond_6

    .line 1476
    array-length v0, v0

    if-gt v0, v2, :cond_5

    .line 1477
    invoke-virtual {p0, v5}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    .line 1479
    :cond_5
    invoke-virtual {p0, v5}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    .line 1482
    :cond_6
    invoke-virtual {p0, v5}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1488
    :goto_2
    iget-object v0, p0, Lepson/print/screen/SettingScr;->duplex_info:[I

    const v5, 0x7f08011d

    const v6, 0x7f080120

    if-eqz v0, :cond_8

    .line 1489
    array-length v0, v0

    if-gt v0, v2, :cond_7

    .line 1490
    invoke-virtual {p0, v6}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1491
    invoke-virtual {p0, v5}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_3

    .line 1493
    :cond_7
    invoke-virtual {p0, v6}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1494
    invoke-virtual {p0, v5}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_3

    .line 1497
    :cond_8
    invoke-virtual {p0, v6}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1498
    invoke-virtual {p0, v5}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1504
    :goto_3
    iget v0, p0, Lepson/print/screen/SettingScr;->lang:I

    const v2, 0x7f08013d

    const v5, 0x7f080140

    packed-switch v0, :pswitch_data_0

    .line 1514
    invoke-virtual {p0, v5}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1515
    invoke-virtual {p0, v2}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4

    .line 1509
    :pswitch_0
    invoke-virtual {p0, v5}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1510
    invoke-virtual {p0, v2}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1522
    :goto_4
    iget-boolean v0, p0, Lepson/print/screen/SettingScr;->isDocumentSetting:Z

    const v2, 0x7f08027b

    if-nez v0, :cond_9

    iget v0, p0, Lepson/print/screen/SettingScr;->printerLocation:I

    if-eq v0, v1, :cond_9

    .line 1524
    invoke-virtual {p0, v2}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_5

    .line 1527
    :cond_9
    invoke-virtual {p0, v2}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1545
    :goto_5
    iget v0, p0, Lepson/print/screen/SettingScr;->printerLocation:I

    if-eq v0, v1, :cond_a

    .line 1546
    iget-boolean v0, p0, Lepson/print/screen/SettingScr;->isDocumentSetting:Z

    if-nez v0, :cond_a

    const/4 v3, 0x0

    :cond_a
    const v0, 0x7f080387

    .line 1551
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f080063

    .line 1552
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0802fa

    .line 1553
    invoke-virtual {p0, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

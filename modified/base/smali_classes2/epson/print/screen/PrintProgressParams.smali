.class Lepson/print/screen/PrintProgressParams;
.super Ljava/lang/Object;
.source "PrintProgressParams.java"

# interfaces
.implements Lepson/print/screen/PrintProgress$ProgressParams;


# instance fields
.field private isDocument:Z

.field private mEpsonColorMode:Z

.field private mImageList:Lepson/print/EPImageList;

.field private mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;


# direct methods
.method public constructor <init>(Landroid/content/Intent;)V
    .locals 3

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "EPImageList"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lepson/print/EPImageList;

    iput-object v0, p0, Lepson/print/screen/PrintProgressParams;->mImageList:Lepson/print/EPImageList;

    .line 25
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "PRINT_DOCUMENT"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lepson/print/screen/PrintProgressParams;->isDocument:Z

    const-string v0, "epson_color"

    .line 26
    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lepson/print/screen/PrintProgressParams;->mEpsonColorMode:Z

    const-string v0, "print_log"

    .line 27
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    check-cast p1, Lcom/epson/iprint/prtlogger/PrintLog;

    iput-object p1, p0, Lepson/print/screen/PrintProgressParams;->mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;

    return-void
.end method


# virtual methods
.method public getApfMode()Z
    .locals 3

    .line 70
    iget-boolean v0, p0, Lepson/print/screen/PrintProgressParams;->isDocument:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 74
    :cond_0
    iget-object v0, p0, Lepson/print/screen/PrintProgressParams;->mImageList:Lepson/print/EPImageList;

    if-nez v0, :cond_1

    return v1

    .line 78
    :cond_1
    iget v0, v0, Lepson/print/EPImageList;->apfModeInPrinting:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_2

    const/4 v1, 0x1

    :cond_2
    return v1
.end method

.method public getEpsonColorMode()Z
    .locals 1

    .line 48
    iget-boolean v0, p0, Lepson/print/screen/PrintProgressParams;->mEpsonColorMode:Z

    return v0
.end method

.method public getOriginalSheetSize()I
    .locals 1

    .line 32
    iget-object v0, p0, Lepson/print/screen/PrintProgressParams;->mImageList:Lepson/print/EPImageList;

    invoke-virtual {v0}, Lepson/print/EPImageList;->size()I

    move-result v0

    return v0
.end method

.method public getPrintLog()Lcom/epson/iprint/prtlogger/PrintLog;
    .locals 1

    .line 60
    iget-object v0, p0, Lepson/print/screen/PrintProgressParams;->mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;

    return-object v0
.end method

.method public getPrintSetting(Landroid/content/Context;)Lepson/print/screen/PrintSetting;
    .locals 2

    .line 42
    new-instance v0, Lepson/print/screen/PrintSetting;

    iget-boolean v1, p0, Lepson/print/screen/PrintProgressParams;->isDocument:Z

    if-eqz v1, :cond_0

    sget-object v1, Lepson/print/screen/PrintSetting$Kind;->document:Lepson/print/screen/PrintSetting$Kind;

    goto :goto_0

    :cond_0
    sget-object v1, Lepson/print/screen/PrintSetting$Kind;->photo:Lepson/print/screen/PrintSetting$Kind;

    :goto_0
    invoke-direct {v0, p1, v1}, Lepson/print/screen/PrintSetting;-><init>(Landroid/content/Context;Lepson/print/screen/PrintSetting$Kind;)V

    return-object v0
.end method

.method public getPrintSettingType()Lepson/print/screen/PrintSetting$Kind;
    .locals 1

    .line 65
    iget-boolean v0, p0, Lepson/print/screen/PrintProgressParams;->isDocument:Z

    if-eqz v0, :cond_0

    sget-object v0, Lepson/print/screen/PrintSetting$Kind;->document:Lepson/print/screen/PrintSetting$Kind;

    goto :goto_0

    :cond_0
    sget-object v0, Lepson/print/screen/PrintSetting$Kind;->photo:Lepson/print/screen/PrintSetting$Kind;

    :goto_0
    return-object v0
.end method

.method public isPaperLandscape()Z
    .locals 2

    .line 37
    iget-object v0, p0, Lepson/print/screen/PrintProgressParams;->mImageList:Lepson/print/EPImageList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lepson/print/EPImageList;->get(I)Lepson/print/EPImage;

    move-result-object v0

    iget-boolean v0, v0, Lepson/print/EPImage;->isPaperLandScape:Z

    return v0
.end method

.method public print(Lepson/print/service/IEpsonService;Z)Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 53
    iget-object v0, p0, Lepson/print/screen/PrintProgressParams;->mImageList:Lepson/print/EPImageList;

    invoke-virtual {p0}, Lepson/print/screen/PrintProgressParams;->getPrintSettingType()Lepson/print/screen/PrintSetting$Kind;

    move-result-object v1

    invoke-virtual {v1}, Lepson/print/screen/PrintSetting$Kind;->name()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1, p2}, Lepson/print/service/IEpsonService;->print(Lepson/print/EPImageList;Ljava/lang/String;Z)I

    const/4 p1, 0x1

    return p1
.end method

.class Lepson/print/screen/ActivityIpPrinterSetting$6;
.super Landroid/os/AsyncTask;
.source "ActivityIpPrinterSetting.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/screen/ActivityIpPrinterSetting;->onSaveButton()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field bAddNewPrinter:Z

.field public mNewIpAddress:Ljava/lang/String;

.field progress:Lepson/print/screen/WorkingDialog;

.field final synthetic this$0:Lepson/print/screen/ActivityIpPrinterSetting;


# direct methods
.method constructor <init>(Lepson/print/screen/ActivityIpPrinterSetting;)V
    .locals 0

    .line 212
    iput-object p1, p0, Lepson/print/screen/ActivityIpPrinterSetting$6;->this$0:Lepson/print/screen/ActivityIpPrinterSetting;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    const/4 p1, 0x0

    .line 215
    iput-boolean p1, p0, Lepson/print/screen/ActivityIpPrinterSetting$6;->bAddNewPrinter:Z

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Integer;
    .locals 7

    const/4 v0, 0x0

    .line 230
    aget-object p1, p1, v0

    .line 239
    iget-object v1, p0, Lepson/print/screen/ActivityIpPrinterSetting$6;->this$0:Lepson/print/screen/ActivityIpPrinterSetting;

    iget-object v1, v1, Lepson/print/screen/ActivityIpPrinterSetting;->printerNameArea:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v1

    const/4 v2, 0x3

    const/4 v3, 0x1

    const/16 v4, 0x8

    if-ne v1, v4, :cond_0

    .line 241
    iget-object v1, p0, Lepson/print/screen/ActivityIpPrinterSetting$6;->this$0:Lepson/print/screen/ActivityIpPrinterSetting;

    new-instance v4, Lepson/print/EPPrinterInfo;

    invoke-direct {v4}, Lepson/print/EPPrinterInfo;-><init>()V

    iput-object v4, v1, Lepson/print/screen/ActivityIpPrinterSetting;->printerInfo:Lepson/print/EPPrinterInfo;

    .line 242
    iget-object v1, p0, Lepson/print/screen/ActivityIpPrinterSetting$6;->this$0:Lepson/print/screen/ActivityIpPrinterSetting;

    iget-object v1, v1, Lepson/print/screen/ActivityIpPrinterSetting;->printerInfo:Lepson/print/EPPrinterInfo;

    iput v2, v1, Lepson/print/EPPrinterInfo;->printerLocation:I

    .line 243
    iget-object v1, p0, Lepson/print/screen/ActivityIpPrinterSetting$6;->this$0:Lepson/print/screen/ActivityIpPrinterSetting;

    iget-object v1, v1, Lepson/print/screen/ActivityIpPrinterSetting;->printerInfo:Lepson/print/EPPrinterInfo;

    const/4 v4, 0x0

    iput-object v4, v1, Lepson/print/EPPrinterInfo;->printerID:Ljava/lang/String;

    .line 244
    iget-object v1, p0, Lepson/print/screen/ActivityIpPrinterSetting$6;->this$0:Lepson/print/screen/ActivityIpPrinterSetting;

    iget-object v1, v1, Lepson/print/screen/ActivityIpPrinterSetting;->printerInfo:Lepson/print/EPPrinterInfo;

    iput-object v4, v1, Lepson/print/EPPrinterInfo;->scannerID:Ljava/lang/String;

    .line 245
    iput-boolean v3, p0, Lepson/print/screen/ActivityIpPrinterSetting$6;->bAddNewPrinter:Z

    .line 249
    :cond_0
    iput-object p1, p0, Lepson/print/screen/ActivityIpPrinterSetting$6;->mNewIpAddress:Ljava/lang/String;

    .line 252
    sget-object v1, Lepson/print/screen/ActivityIpPrinterSetting;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    iget-object v4, p0, Lepson/print/screen/ActivityIpPrinterSetting$6;->this$0:Lepson/print/screen/ActivityIpPrinterSetting;

    const/16 v5, 0xc0

    invoke-virtual {v1, v4, v5}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doInitDriver(Landroid/content/Context;I)I

    move-result v1

    const/16 v4, -0x41a

    if-eq v1, v4, :cond_1

    if-eqz v1, :cond_1

    .line 262
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    .line 265
    :cond_1
    sget-object v1, Lepson/print/screen/ActivityIpPrinterSetting;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->getMEscpLib()Lcom/epson/mobilephone/common/escpr/EscprLib;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/epson/mobilephone/common/escpr/EscprLib;->setSearchStt(Z)V

    .line 268
    sget-object v1, Lepson/print/screen/ActivityIpPrinterSetting;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    const/16 v5, 0x3c

    iget-object v6, p0, Lepson/print/screen/ActivityIpPrinterSetting$6;->this$0:Lepson/print/screen/ActivityIpPrinterSetting;

    iget-object v6, v6, Lepson/print/screen/ActivityIpPrinterSetting;->printerInfo:Lepson/print/EPPrinterInfo;

    iget-object v6, v6, Lepson/print/EPPrinterInfo;->printerID:Ljava/lang/String;

    invoke-virtual {v1, v5, v6, p1, v2}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doProbePrinter(ILjava/lang/String;Ljava/lang/String;I)I

    move-result v1

    .line 271
    sget-object v2, Lepson/print/screen/ActivityIpPrinterSetting;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-virtual {v2}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->getMEscpLib()Lcom/epson/mobilephone/common/escpr/EscprLib;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/epson/mobilephone/common/escpr/EscprLib;->setSearchStt(Z)V

    if-eqz v1, :cond_2

    .line 279
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    .line 283
    :cond_2
    sget-object v1, Lepson/print/screen/ActivityIpPrinterSetting;->mScanner:Lepson/scan/lib/escanLib;

    iget-object v2, p0, Lepson/print/screen/ActivityIpPrinterSetting$6;->this$0:Lepson/print/screen/ActivityIpPrinterSetting;

    invoke-virtual {v1, v2}, Lepson/scan/lib/escanLib;->escanWrapperInitDriver(Landroid/content/Context;)I

    move-result v1

    if-eq v1, v4, :cond_4

    if-eqz v1, :cond_3

    .line 293
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    :cond_4
    const/4 v1, 0x1

    .line 296
    :goto_0
    sget-object v2, Lepson/print/screen/ActivityIpPrinterSetting;->mScanner:Lepson/scan/lib/escanLib;

    invoke-virtual {v2, v3}, Lepson/scan/lib/escanLib;->setSearchStt(Z)V

    .line 299
    sget-object v2, Lepson/print/screen/ActivityIpPrinterSetting;->mScanner:Lepson/scan/lib/escanLib;

    iget-object v4, p0, Lepson/print/screen/ActivityIpPrinterSetting$6;->this$0:Lepson/print/screen/ActivityIpPrinterSetting;

    iget-object v4, v4, Lepson/print/screen/ActivityIpPrinterSetting;->printerInfo:Lepson/print/EPPrinterInfo;

    iget-object v4, v4, Lepson/print/EPPrinterInfo;->scannerID:Ljava/lang/String;

    invoke-virtual {v2, v4, p1}, Lepson/scan/lib/escanLib;->probeScannerByIp(Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    .line 301
    sget-object v2, Lepson/print/screen/ActivityIpPrinterSetting;->mScanner:Lepson/scan/lib/escanLib;

    invoke-virtual {v2, v0}, Lepson/scan/lib/escanLib;->setSearchStt(Z)V

    if-eq v1, v3, :cond_5

    .line 305
    sget-object v1, Lepson/print/screen/ActivityIpPrinterSetting;->mScanner:Lepson/scan/lib/escanLib;

    invoke-virtual {v1}, Lepson/scan/lib/escanLib;->escanWrapperReleaseDriver()I

    :cond_5
    const/16 v1, -0x51a

    if-ne p1, v1, :cond_6

    const-string p1, "ActivityIpPrinterSetting"

    const-string v1, "eScanLib return ESCAN_ERR_SCANNER_NOT_USEFUL"

    .line 309
    invoke-static {p1, v1}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x0

    .line 313
    :cond_6
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 212
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lepson/print/screen/ActivityIpPrinterSetting$6;->doInBackground([Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 7

    .line 319
    iget-object v0, p0, Lepson/print/screen/ActivityIpPrinterSetting$6;->progress:Lepson/print/screen/WorkingDialog;

    invoke-virtual {v0}, Lepson/print/screen/WorkingDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 320
    iget-object v0, p0, Lepson/print/screen/ActivityIpPrinterSetting$6;->progress:Lepson/print/screen/WorkingDialog;

    invoke-virtual {v0}, Lepson/print/screen/WorkingDialog;->dismiss()V

    .line 323
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const v1, 0x7f0e04f2

    const/4 v2, 0x0

    if-nez v0, :cond_4

    .line 327
    iget-boolean p1, p0, Lepson/print/screen/ActivityIpPrinterSetting$6;->bAddNewPrinter:Z

    if-eqz p1, :cond_1

    iget-object p1, p0, Lepson/print/screen/ActivityIpPrinterSetting$6;->this$0:Lepson/print/screen/ActivityIpPrinterSetting;

    iget-object p1, p1, Lepson/print/screen/ActivityIpPrinterSetting;->manager:Lepson/print/EPPrinterManager;

    iget-object v0, p0, Lepson/print/screen/ActivityIpPrinterSetting$6;->this$0:Lepson/print/screen/ActivityIpPrinterSetting;

    iget-object v0, v0, Lepson/print/screen/ActivityIpPrinterSetting;->printerInfo:Lepson/print/EPPrinterInfo;

    iget-object v0, v0, Lepson/print/EPPrinterInfo;->printerID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lepson/print/EPPrinterManager;->loadIpPrinterInfo(Ljava/lang/String;)Lepson/print/EPPrinterInfo;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 329
    new-instance p1, Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lepson/print/screen/ActivityIpPrinterSetting$6;->this$0:Lepson/print/screen/ActivityIpPrinterSetting;

    invoke-direct {p1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 330
    invoke-virtual {p1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    iget-object v0, p0, Lepson/print/screen/ActivityIpPrinterSetting$6;->this$0:Lepson/print/screen/ActivityIpPrinterSetting;

    const v2, 0x7f0e04ce

    .line 331
    invoke-virtual {v0, v2}, Lepson/print/screen/ActivityIpPrinterSetting;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    iget-object v0, p0, Lepson/print/screen/ActivityIpPrinterSetting$6;->this$0:Lepson/print/screen/ActivityIpPrinterSetting;

    .line 332
    invoke-virtual {v0, v1}, Lepson/print/screen/ActivityIpPrinterSetting;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lepson/print/screen/ActivityIpPrinterSetting$6$1;

    invoke-direct {v1, p0}, Lepson/print/screen/ActivityIpPrinterSetting$6$1;-><init>(Lepson/print/screen/ActivityIpPrinterSetting$6;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 339
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void

    .line 345
    :cond_1
    iget-object p1, p0, Lepson/print/screen/ActivityIpPrinterSetting$6;->this$0:Lepson/print/screen/ActivityIpPrinterSetting;

    iget-object p1, p1, Lepson/print/screen/ActivityIpPrinterSetting;->oldItemKey:Ljava/lang/String;

    if-eqz p1, :cond_2

    iget-object p1, p0, Lepson/print/screen/ActivityIpPrinterSetting$6;->this$0:Lepson/print/screen/ActivityIpPrinterSetting;

    iget-object p1, p1, Lepson/print/screen/ActivityIpPrinterSetting;->oldItemKey:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    if-lez p1, :cond_2

    .line 346
    iget-object p1, p0, Lepson/print/screen/ActivityIpPrinterSetting$6;->this$0:Lepson/print/screen/ActivityIpPrinterSetting;

    iget-object p1, p1, Lepson/print/screen/ActivityIpPrinterSetting;->manager:Lepson/print/EPPrinterManager;

    iget-object v0, p0, Lepson/print/screen/ActivityIpPrinterSetting$6;->this$0:Lepson/print/screen/ActivityIpPrinterSetting;

    iget-object v0, v0, Lepson/print/screen/ActivityIpPrinterSetting;->oldItemKey:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lepson/print/EPPrinterManager;->deleteIpPrinterInfo(Ljava/lang/String;)Z

    .line 350
    :cond_2
    iget-object p1, p0, Lepson/print/screen/ActivityIpPrinterSetting$6;->this$0:Lepson/print/screen/ActivityIpPrinterSetting;

    iget-object p1, p1, Lepson/print/screen/ActivityIpPrinterSetting;->printerName:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    .line 351
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    if-lez p1, :cond_3

    .line 352
    iget-object p1, p0, Lepson/print/screen/ActivityIpPrinterSetting$6;->this$0:Lepson/print/screen/ActivityIpPrinterSetting;

    iget-object p1, p1, Lepson/print/screen/ActivityIpPrinterSetting;->printerInfo:Lepson/print/EPPrinterInfo;

    iget-object v0, p0, Lepson/print/screen/ActivityIpPrinterSetting$6;->this$0:Lepson/print/screen/ActivityIpPrinterSetting;

    iget-object v0, v0, Lepson/print/screen/ActivityIpPrinterSetting;->printerName:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lepson/print/EPPrinterInfo;->userDefName:Ljava/lang/String;

    goto :goto_0

    .line 354
    :cond_3
    iget-object p1, p0, Lepson/print/screen/ActivityIpPrinterSetting$6;->this$0:Lepson/print/screen/ActivityIpPrinterSetting;

    iget-object p1, p1, Lepson/print/screen/ActivityIpPrinterSetting;->printerInfo:Lepson/print/EPPrinterInfo;

    iget-object v0, p0, Lepson/print/screen/ActivityIpPrinterSetting$6;->this$0:Lepson/print/screen/ActivityIpPrinterSetting;

    iget-object v0, v0, Lepson/print/screen/ActivityIpPrinterSetting;->printerInfo:Lepson/print/EPPrinterInfo;

    iget-object v0, v0, Lepson/print/EPPrinterInfo;->printerName:Ljava/lang/String;

    iput-object v0, p1, Lepson/print/EPPrinterInfo;->userDefName:Ljava/lang/String;

    .line 358
    :goto_0
    iget-object p1, p0, Lepson/print/screen/ActivityIpPrinterSetting$6;->this$0:Lepson/print/screen/ActivityIpPrinterSetting;

    iget-object p1, p1, Lepson/print/screen/ActivityIpPrinterSetting;->manager:Lepson/print/EPPrinterManager;

    iget-object v0, p0, Lepson/print/screen/ActivityIpPrinterSetting$6;->this$0:Lepson/print/screen/ActivityIpPrinterSetting;

    iget-object v0, v0, Lepson/print/screen/ActivityIpPrinterSetting;->printerInfo:Lepson/print/EPPrinterInfo;

    iget-object v0, v0, Lepson/print/EPPrinterInfo;->printerID:Ljava/lang/String;

    iget-object v1, p0, Lepson/print/screen/ActivityIpPrinterSetting$6;->this$0:Lepson/print/screen/ActivityIpPrinterSetting;

    iget-object v1, v1, Lepson/print/screen/ActivityIpPrinterSetting;->printerInfo:Lepson/print/EPPrinterInfo;

    invoke-virtual {p1, v0, v1}, Lepson/print/EPPrinterManager;->saveIPPrinterInfo(Ljava/lang/String;Lepson/print/EPPrinterInfo;)V

    .line 360
    iget-object p1, p0, Lepson/print/screen/ActivityIpPrinterSetting$6;->this$0:Lepson/print/screen/ActivityIpPrinterSetting;

    iget-object p1, p1, Lepson/print/screen/ActivityIpPrinterSetting;->printerInfo:Lepson/print/EPPrinterInfo;

    iget-object v0, p0, Lepson/print/screen/ActivityIpPrinterSetting$6;->mNewIpAddress:Ljava/lang/String;

    iput-object v0, p1, Lepson/print/EPPrinterInfo;->printerIP:Ljava/lang/String;

    .line 363
    new-instance p1, Landroid/content/Intent;

    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    const-string v0, "PRINTER_NAME"

    .line 364
    iget-object v1, p0, Lepson/print/screen/ActivityIpPrinterSetting$6;->this$0:Lepson/print/screen/ActivityIpPrinterSetting;

    iget-object v1, v1, Lepson/print/screen/ActivityIpPrinterSetting;->printerInfo:Lepson/print/EPPrinterInfo;

    iget-object v1, v1, Lepson/print/EPPrinterInfo;->printerName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "PRINTER_ID"

    .line 365
    iget-object v1, p0, Lepson/print/screen/ActivityIpPrinterSetting$6;->this$0:Lepson/print/screen/ActivityIpPrinterSetting;

    iget-object v1, v1, Lepson/print/screen/ActivityIpPrinterSetting;->printerInfo:Lepson/print/EPPrinterInfo;

    iget-object v1, v1, Lepson/print/EPPrinterInfo;->printerID:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "SCAN_REFS_SCANNER_ID"

    .line 366
    iget-object v1, p0, Lepson/print/screen/ActivityIpPrinterSetting$6;->this$0:Lepson/print/screen/ActivityIpPrinterSetting;

    iget-object v1, v1, Lepson/print/screen/ActivityIpPrinterSetting;->printerInfo:Lepson/print/EPPrinterInfo;

    iget-object v1, v1, Lepson/print/EPPrinterInfo;->scannerID:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "PRINTER_IP"

    .line 367
    iget-object v1, p0, Lepson/print/screen/ActivityIpPrinterSetting$6;->this$0:Lepson/print/screen/ActivityIpPrinterSetting;

    iget-object v1, v1, Lepson/print/screen/ActivityIpPrinterSetting;->printerInfo:Lepson/print/EPPrinterInfo;

    iget-object v1, v1, Lepson/print/EPPrinterInfo;->printerIP:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "PRINTER_SERIAL_NO"

    .line 368
    iget-object v1, p0, Lepson/print/screen/ActivityIpPrinterSetting$6;->this$0:Lepson/print/screen/ActivityIpPrinterSetting;

    iget-object v1, v1, Lepson/print/screen/ActivityIpPrinterSetting;->printerInfo:Lepson/print/EPPrinterInfo;

    iget-object v1, v1, Lepson/print/EPPrinterInfo;->printerSerialNo:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 369
    iget-object v0, p0, Lepson/print/screen/ActivityIpPrinterSetting$6;->this$0:Lepson/print/screen/ActivityIpPrinterSetting;

    const/4 v1, -0x1

    invoke-virtual {v0, v1, p1}, Lepson/print/screen/ActivityIpPrinterSetting;->setResult(ILandroid/content/Intent;)V

    .line 370
    iget-object p1, p0, Lepson/print/screen/ActivityIpPrinterSetting$6;->this$0:Lepson/print/screen/ActivityIpPrinterSetting;

    invoke-virtual {p1}, Lepson/print/screen/ActivityIpPrinterSetting;->finish()V

    return-void

    :cond_4
    const v0, -0x7a121

    .line 375
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x1

    if-ne v0, v3, :cond_5

    .line 377
    new-array p1, v5, [Ljava/lang/Integer;

    const v0, 0x7f0e04c9

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p1, v2

    const v0, 0x7f0e01b3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p1, v6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p1, v4

    goto :goto_1

    .line 381
    :cond_5
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-static {p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$ErrorTable;->getStringId(I)[Ljava/lang/Integer;

    move-result-object p1

    if-nez p1, :cond_6

    .line 383
    new-array p1, v5, [Ljava/lang/Integer;

    const v0, 0x7f0e023a

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p1, v2

    const v0, 0x7f0e023b

    .line 384
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p1, v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p1, v4

    .line 391
    :cond_6
    :goto_1
    iget-object v0, p0, Lepson/print/screen/ActivityIpPrinterSetting$6;->this$0:Lepson/print/screen/ActivityIpPrinterSetting;

    aget-object v3, p1, v6

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v0, v3}, Lepson/print/screen/ActivityIpPrinterSetting;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lepson/print/screen/ActivityIpPrinterSetting$6;->this$0:Lepson/print/screen/ActivityIpPrinterSetting;

    aget-object p1, p1, v2

    .line 392
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {v4, p1}, Lepson/print/screen/ActivityIpPrinterSetting;->getString(I)Ljava/lang/String;

    move-result-object p1

    iget-object v2, p0, Lepson/print/screen/ActivityIpPrinterSetting$6;->this$0:Lepson/print/screen/ActivityIpPrinterSetting;

    invoke-virtual {v2, v1}, Lepson/print/screen/ActivityIpPrinterSetting;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 391
    invoke-static {v0, v3, p1, v1}, Lepson/common/Utils;->makeMessageBox(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/app/AlertDialog;

    move-result-object p1

    .line 393
    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 212
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lepson/print/screen/ActivityIpPrinterSetting$6;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .line 220
    new-instance v0, Lepson/print/screen/WorkingDialog;

    iget-object v1, p0, Lepson/print/screen/ActivityIpPrinterSetting$6;->this$0:Lepson/print/screen/ActivityIpPrinterSetting;

    invoke-direct {v0, v1}, Lepson/print/screen/WorkingDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lepson/print/screen/ActivityIpPrinterSetting$6;->progress:Lepson/print/screen/WorkingDialog;

    .line 221
    iget-object v0, p0, Lepson/print/screen/ActivityIpPrinterSetting$6;->progress:Lepson/print/screen/WorkingDialog;

    invoke-virtual {v0}, Lepson/print/screen/WorkingDialog;->show()V

    .line 223
    sget-object v0, Lepson/print/screen/ActivityIpPrinterSetting;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->getMEscpLib()Lcom/epson/mobilephone/common/escpr/EscprLib;

    move-result-object v0

    iget-object v1, p0, Lepson/print/screen/ActivityIpPrinterSetting$6;->this$0:Lepson/print/screen/ActivityIpPrinterSetting;

    iget-object v1, v1, Lepson/print/screen/ActivityIpPrinterSetting;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/epson/mobilephone/common/escpr/EscprLib;->setHanlder(Landroid/os/Handler;)V

    .line 224
    sget-object v0, Lepson/print/screen/ActivityIpPrinterSetting;->mScanner:Lepson/scan/lib/escanLib;

    iget-object v1, p0, Lepson/print/screen/ActivityIpPrinterSetting$6;->this$0:Lepson/print/screen/ActivityIpPrinterSetting;

    iget-object v1, v1, Lepson/print/screen/ActivityIpPrinterSetting;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lepson/scan/lib/escanLib;->setScanHandler(Landroid/os/Handler;)V

    return-void
.end method

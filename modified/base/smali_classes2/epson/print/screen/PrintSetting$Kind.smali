.class public final enum Lepson/print/screen/PrintSetting$Kind;
.super Ljava/lang/Enum;
.source "PrintSetting.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/screen/PrintSetting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Kind"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lepson/print/screen/PrintSetting$Kind;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lepson/print/screen/PrintSetting$Kind;

.field public static final enum NotSet:Lepson/print/screen/PrintSetting$Kind;

.field public static final enum cameracopy:Lepson/print/screen/PrintSetting$Kind;

.field public static final enum document:Lepson/print/screen/PrintSetting$Kind;

.field public static final enum photo:Lepson/print/screen/PrintSetting$Kind;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 67
    new-instance v0, Lepson/print/screen/PrintSetting$Kind;

    const-string v1, "document"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lepson/print/screen/PrintSetting$Kind;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/screen/PrintSetting$Kind;->document:Lepson/print/screen/PrintSetting$Kind;

    .line 85
    new-instance v0, Lepson/print/screen/PrintSetting$Kind;

    const-string v1, "photo"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lepson/print/screen/PrintSetting$Kind;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/screen/PrintSetting$Kind;->photo:Lepson/print/screen/PrintSetting$Kind;

    .line 90
    new-instance v0, Lepson/print/screen/PrintSetting$Kind;

    const-string v1, "cameracopy"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lepson/print/screen/PrintSetting$Kind;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/screen/PrintSetting$Kind;->cameracopy:Lepson/print/screen/PrintSetting$Kind;

    .line 104
    new-instance v0, Lepson/print/screen/PrintSetting$Kind;

    const-string v1, "NotSet"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lepson/print/screen/PrintSetting$Kind;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/screen/PrintSetting$Kind;->NotSet:Lepson/print/screen/PrintSetting$Kind;

    const/4 v0, 0x4

    .line 50
    new-array v0, v0, [Lepson/print/screen/PrintSetting$Kind;

    sget-object v1, Lepson/print/screen/PrintSetting$Kind;->document:Lepson/print/screen/PrintSetting$Kind;

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/screen/PrintSetting$Kind;->photo:Lepson/print/screen/PrintSetting$Kind;

    aput-object v1, v0, v3

    sget-object v1, Lepson/print/screen/PrintSetting$Kind;->cameracopy:Lepson/print/screen/PrintSetting$Kind;

    aput-object v1, v0, v4

    sget-object v1, Lepson/print/screen/PrintSetting$Kind;->NotSet:Lepson/print/screen/PrintSetting$Kind;

    aput-object v1, v0, v5

    sput-object v0, Lepson/print/screen/PrintSetting$Kind;->$VALUES:[Lepson/print/screen/PrintSetting$Kind;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 50
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lepson/print/screen/PrintSetting$Kind;
    .locals 1

    .line 50
    const-class v0, Lepson/print/screen/PrintSetting$Kind;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lepson/print/screen/PrintSetting$Kind;

    return-object p0
.end method

.method public static values()[Lepson/print/screen/PrintSetting$Kind;
    .locals 1

    .line 50
    sget-object v0, Lepson/print/screen/PrintSetting$Kind;->$VALUES:[Lepson/print/screen/PrintSetting$Kind;

    invoke-virtual {v0}, [Lepson/print/screen/PrintSetting$Kind;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lepson/print/screen/PrintSetting$Kind;

    return-object v0
.end method

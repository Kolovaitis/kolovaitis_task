.class Lepson/print/screen/PrintProgress$1;
.super Lepson/print/service/IEpsonServiceCallback$Stub;
.source "PrintProgress.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/screen/PrintProgress;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/screen/PrintProgress;


# direct methods
.method constructor <init>(Lepson/print/screen/PrintProgress;)V
    .locals 0

    .line 151
    iput-object p1, p0, Lepson/print/screen/PrintProgress$1;->this$0:Lepson/print/screen/PrintProgress;

    invoke-direct {p0}, Lepson/print/service/IEpsonServiceCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onFindPrinterResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 168
    iget-object p1, p0, Lepson/print/screen/PrintProgress$1;->this$0:Lepson/print/screen/PrintProgress;

    invoke-static {p1, p2}, Lepson/print/screen/PrintProgress;->access$002(Lepson/print/screen/PrintProgress;Ljava/lang/String;)Ljava/lang/String;

    return-void
.end method

.method public onGetInkState()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    return-void
.end method

.method public onGetStatusState()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    return-void
.end method

.method public onNotifyContinueable(I)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "Epson"

    const-string v1, "onNotifyContinueable() CALL"

    .line 181
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    invoke-static {}, Lepson/print/screen/PrintProgress;->access$100()Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 184
    :try_start_0
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 187
    sput-boolean v0, Lepson/print/screen/PrintProgress;->isBkRetry:Z

    .line 188
    iget-object p1, p0, Lepson/print/screen/PrintProgress$1;->this$0:Lepson/print/screen/PrintProgress;

    invoke-static {p1}, Lepson/print/screen/PrintProgress;->access$200(Lepson/print/screen/PrintProgress;)V

    goto :goto_0

    :cond_0
    const/16 v1, 0x28

    if-ne p1, v1, :cond_1

    .line 191
    sput v0, Lepson/print/screen/PrintProgress;->curError:I

    .line 192
    iget-object p1, p0, Lepson/print/screen/PrintProgress$1;->this$0:Lepson/print/screen/PrintProgress;

    iget-object p1, p1, Lepson/print/screen/PrintProgress;->mHandler:Landroid/os/Handler;

    const/4 v0, 0x5

    const-wide/16 v1, 0x3e8

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 194
    :cond_1
    iget-object v1, p0, Lepson/print/screen/PrintProgress$1;->this$0:Lepson/print/screen/PrintProgress;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lepson/print/screen/PrintProgress;->access$302(Lepson/print/screen/PrintProgress;Z)Z

    .line 195
    sput p1, Lepson/print/screen/PrintProgress;->curError:I

    .line 196
    sput-boolean v0, Lepson/print/screen/PrintProgress;->isContinue:Z

    .line 197
    iget-object p1, p0, Lepson/print/screen/PrintProgress$1;->this$0:Lepson/print/screen/PrintProgress;

    iget-object p1, p1, Lepson/print/screen/PrintProgress;->mHandler:Landroid/os/Handler;

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_0
    return-void

    :catchall_0
    move-exception p1

    .line 184
    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method public onNotifyEndJob(I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "PrintProgress"

    const-string v1, "onNotifyEndJob() call"

    .line 246
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p1, :cond_1

    .line 249
    iget-object p1, p0, Lepson/print/screen/PrintProgress$1;->this$0:Lepson/print/screen/PrintProgress;

    invoke-static {p1}, Lepson/print/screen/PrintProgress;->access$400(Lepson/print/screen/PrintProgress;)Z

    move-result p1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 250
    iget-object p1, p0, Lepson/print/screen/PrintProgress$1;->this$0:Lepson/print/screen/PrintProgress;

    iget-object p1, p1, Lepson/print/screen/PrintProgress;->mHandler:Landroid/os/Handler;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 252
    :cond_0
    iget-object p1, p0, Lepson/print/screen/PrintProgress$1;->this$0:Lepson/print/screen/PrintProgress;

    iget-object p1, p1, Lepson/print/screen/PrintProgress;->mHandler:Landroid/os/Handler;

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_1
    :goto_0
    return-void
.end method

.method public onNotifyError(IIZ)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 203
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "status: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " err code: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " continue: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->e(Ljava/lang/String;)V

    .line 207
    iget-object v0, p0, Lepson/print/screen/PrintProgress$1;->this$0:Lepson/print/screen/PrintProgress;

    invoke-static {v0}, Lepson/print/screen/PrintProgress;->access$400(Lepson/print/screen/PrintProgress;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    if-eqz p2, :cond_0

    .line 209
    new-instance p1, Landroid/os/Message;

    invoke-direct {p1}, Landroid/os/Message;-><init>()V

    const/16 p3, 0x9

    .line 210
    iput p3, p1, Landroid/os/Message;->what:I

    .line 211
    iput p2, p1, Landroid/os/Message;->arg1:I

    .line 212
    iget-object p2, p0, Lepson/print/screen/PrintProgress$1;->this$0:Lepson/print/screen/PrintProgress;

    iget-object p2, p2, Lepson/print/screen/PrintProgress;->mHandler:Landroid/os/Handler;

    invoke-virtual {p2, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    if-eqz p2, :cond_6

    .line 218
    invoke-static {}, Lepson/print/screen/PrintProgress;->access$100()Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    const/16 p1, 0x66

    const/16 v3, -0x547

    const/16 v4, -0x44c

    const/16 v5, -0x514

    if-eq v4, p2, :cond_2

    if-eq v5, p2, :cond_2

    if-eq v3, p2, :cond_2

    if-ne p1, p2, :cond_3

    .line 219
    :cond_2
    :try_start_0
    iget-object v6, p0, Lepson/print/screen/PrintProgress$1;->this$0:Lepson/print/screen/PrintProgress;

    invoke-static {v6}, Lepson/print/screen/PrintProgress;->access$500(Lepson/print/screen/PrintProgress;)Z

    move-result v6

    if-ne v6, v1, :cond_3

    const/16 p2, -0x32c9

    goto :goto_0

    :cond_3
    if-eq v4, p2, :cond_4

    if-eq v5, p2, :cond_4

    if-eq v3, p2, :cond_4

    if-ne p1, p2, :cond_5

    .line 221
    :cond_4
    iget-object p1, p0, Lepson/print/screen/PrintProgress$1;->this$0:Lepson/print/screen/PrintProgress;

    invoke-static {p1}, Lepson/print/screen/PrintProgress;->access$500(Lepson/print/screen/PrintProgress;)Z

    move-result p1

    if-nez p1, :cond_5

    const/16 p2, -0x514

    .line 224
    :cond_5
    :goto_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 225
    iget-object p1, p0, Lepson/print/screen/PrintProgress$1;->this$0:Lepson/print/screen/PrintProgress;

    invoke-static {p1, v0}, Lepson/print/screen/PrintProgress;->access$502(Lepson/print/screen/PrintProgress;Z)Z

    .line 226
    sput-boolean p3, Lepson/print/screen/PrintProgress;->isContinue:Z

    .line 227
    sput p2, Lepson/print/screen/PrintProgress;->curError:I

    .line 228
    iget-object p1, p0, Lepson/print/screen/PrintProgress$1;->this$0:Lepson/print/screen/PrintProgress;

    invoke-static {p1, v1}, Lepson/print/screen/PrintProgress;->access$302(Lepson/print/screen/PrintProgress;Z)Z

    .line 229
    iget-object p1, p0, Lepson/print/screen/PrintProgress$1;->this$0:Lepson/print/screen/PrintProgress;

    iget-object p1, p1, Lepson/print/screen/PrintProgress;->mHandler:Landroid/os/Handler;

    const/4 p2, 0x4

    invoke-virtual {p1, p2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    :catchall_0
    move-exception p1

    .line 224
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1

    .line 231
    :cond_6
    iget-object p2, p0, Lepson/print/screen/PrintProgress$1;->this$0:Lepson/print/screen/PrintProgress;

    invoke-static {p2}, Lepson/print/screen/PrintProgress;->access$600(Lepson/print/screen/PrintProgress;)Z

    move-result p2

    if-eqz p2, :cond_7

    .line 232
    iget-object p2, p0, Lepson/print/screen/PrintProgress$1;->this$0:Lepson/print/screen/PrintProgress;

    invoke-virtual {p2, v0}, Lepson/print/screen/PrintProgress;->removeDialog(I)V

    .line 233
    iget-object p2, p0, Lepson/print/screen/PrintProgress$1;->this$0:Lepson/print/screen/PrintProgress;

    invoke-static {p2, v0}, Lepson/print/screen/PrintProgress;->access$602(Lepson/print/screen/PrintProgress;Z)Z

    :cond_7
    const/4 p2, -0x1

    if-ne p1, p2, :cond_8

    xor-int/lit8 p1, p3, 0x1

    .line 237
    iget-object p2, p0, Lepson/print/screen/PrintProgress$1;->this$0:Lepson/print/screen/PrintProgress;

    invoke-static {p2}, Lepson/print/screen/PrintProgress;->access$700(Lepson/print/screen/PrintProgress;)Z

    move-result p2

    xor-int/2addr p2, v1

    and-int/2addr p1, p2

    if-eqz p1, :cond_8

    .line 238
    iget-object p1, p0, Lepson/print/screen/PrintProgress$1;->this$0:Lepson/print/screen/PrintProgress;

    iget-object p1, p1, Lepson/print/screen/PrintProgress;->mHandler:Landroid/os/Handler;

    const/4 p2, 0x5

    invoke-virtual {p1, p2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_8
    :goto_1
    return-void
.end method

.method public onNotifyProgress(II)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "Epson"

    const-string v1, "onNotifyProgress() CALL"

    .line 173
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    iget-object v0, p0, Lepson/print/screen/PrintProgress$1;->this$0:Lepson/print/screen/PrintProgress;

    iget-object v0, v0, Lepson/print/screen/PrintProgress;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object p1

    .line 176
    iget-object p2, p0, Lepson/print/screen/PrintProgress$1;->this$0:Lepson/print/screen/PrintProgress;

    iget-object p2, p2, Lepson/print/screen/PrintProgress;->mHandler:Landroid/os/Handler;

    const-wide/16 v0, 0x64

    invoke-virtual {p2, p1, v0, v1}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method

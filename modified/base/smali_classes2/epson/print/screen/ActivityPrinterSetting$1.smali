.class Lepson/print/screen/ActivityPrinterSetting$1;
.super Ljava/lang/Object;
.source "ActivityPrinterSetting.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/screen/ActivityPrinterSetting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/screen/ActivityPrinterSetting;


# direct methods
.method constructor <init>(Lepson/print/screen/ActivityPrinterSetting;)V
    .locals 0

    .line 420
    iput-object p1, p0, Lepson/print/screen/ActivityPrinterSetting$1;->this$0:Lepson/print/screen/ActivityPrinterSetting;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 2

    .line 422
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    .line 455
    :pswitch_0
    new-instance p1, Landroid/content/Intent;

    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    const-string v0, "my_printer"

    .line 460
    iget-object v1, p0, Lepson/print/screen/ActivityPrinterSetting$1;->this$0:Lepson/print/screen/ActivityPrinterSetting;

    iget-object v1, v1, Lepson/print/screen/ActivityPrinterSetting;->mMyPrinter:Lepson/print/MyPrinter;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 461
    iget-object v0, p0, Lepson/print/screen/ActivityPrinterSetting$1;->this$0:Lepson/print/screen/ActivityPrinterSetting;

    const/4 v1, -0x1

    invoke-virtual {v0, v1, p1}, Lepson/print/screen/ActivityPrinterSetting;->setResult(ILandroid/content/Intent;)V

    .line 462
    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting$1;->this$0:Lepson/print/screen/ActivityPrinterSetting;

    invoke-virtual {p1}, Lepson/print/screen/ActivityPrinterSetting;->finish()V

    goto :goto_0

    .line 449
    :pswitch_1
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-eqz v0, :cond_0

    .line 450
    iget-object v0, p0, Lepson/print/screen/ActivityPrinterSetting$1;->this$0:Lepson/print/screen/ActivityPrinterSetting;

    iget p1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, p1}, Lepson/print/screen/ActivityPrinterSetting;->showErrorMessage(I)V

    goto :goto_0

    .line 443
    :pswitch_2
    new-instance p1, Lepson/print/screen/ActivityPrinterSetting$setRemotePrintTask;

    iget-object v0, p0, Lepson/print/screen/ActivityPrinterSetting$1;->this$0:Lepson/print/screen/ActivityPrinterSetting;

    const/4 v1, 0x0

    invoke-direct {p1, v0, v1}, Lepson/print/screen/ActivityPrinterSetting$setRemotePrintTask;-><init>(Lepson/print/screen/ActivityPrinterSetting;Lepson/print/screen/ActivityPrinterSetting$1;)V

    const/4 v0, 0x0

    .line 444
    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {p1, v0}, Lepson/print/screen/ActivityPrinterSetting$setRemotePrintTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 436
    :pswitch_3
    iget-object v0, p0, Lepson/print/screen/ActivityPrinterSetting$1;->this$0:Lepson/print/screen/ActivityPrinterSetting;

    invoke-static {v0}, Lepson/print/screen/ActivityPrinterSetting;->access$400(Lepson/print/screen/ActivityPrinterSetting;)Landroid/widget/TextView;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 437
    iget-object v0, p0, Lepson/print/screen/ActivityPrinterSetting$1;->this$0:Lepson/print/screen/ActivityPrinterSetting;

    invoke-static {v0}, Lepson/print/screen/ActivityPrinterSetting;->access$500(Lepson/print/screen/ActivityPrinterSetting;)Landroid/widget/FrameLayout;

    move-result-object v0

    iget p1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 438
    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting$1;->this$0:Lepson/print/screen/ActivityPrinterSetting;

    invoke-static {p1}, Lepson/print/screen/ActivityPrinterSetting;->access$300(Lepson/print/screen/ActivityPrinterSetting;)Landroid/widget/EditText;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_0

    .line 432
    :pswitch_4
    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting$1;->this$0:Lepson/print/screen/ActivityPrinterSetting;

    invoke-static {p1}, Lepson/print/screen/ActivityPrinterSetting;->access$300(Lepson/print/screen/ActivityPrinterSetting;)Landroid/widget/EditText;

    move-result-object p1

    const-string v0, ""

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 428
    :pswitch_5
    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting$1;->this$0:Lepson/print/screen/ActivityPrinterSetting;

    invoke-static {p1}, Lepson/print/screen/ActivityPrinterSetting;->access$200(Lepson/print/screen/ActivityPrinterSetting;)Landroid/widget/EditText;

    move-result-object p1

    const-string v0, ""

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 424
    :pswitch_6
    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting$1;->this$0:Lepson/print/screen/ActivityPrinterSetting;

    invoke-static {p1}, Lepson/print/screen/ActivityPrinterSetting;->access$100(Lepson/print/screen/ActivityPrinterSetting;)Landroid/widget/EditText;

    move-result-object p1

    const-string v0, ""

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_0
    const/4 p1, 0x1

    return p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

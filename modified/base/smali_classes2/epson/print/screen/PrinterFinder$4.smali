.class Lepson/print/screen/PrinterFinder$4;
.super Lepson/print/service/IEpsonServiceCallback$Stub;
.source "PrinterFinder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/screen/PrinterFinder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/screen/PrinterFinder;


# direct methods
.method constructor <init>(Lepson/print/screen/PrinterFinder;)V
    .locals 0

    .line 190
    iput-object p1, p0, Lepson/print/screen/PrinterFinder$4;->this$0:Lepson/print/screen/PrinterFinder;

    invoke-direct {p0}, Lepson/print/service/IEpsonServiceCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onFindPrinterResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 203
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object p4

    const/4 v0, 0x3

    .line 204
    iput v0, p4, Landroid/os/Message;->what:I

    .line 205
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "name"

    .line 206
    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p1, "ip"

    .line 207
    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p1, "id"

    .line 208
    invoke-virtual {v0, p1, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p1, "common_devicename"

    .line 209
    invoke-virtual {v0, p1, p5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    invoke-virtual {p4, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 211
    iget-object p1, p0, Lepson/print/screen/PrinterFinder$4;->this$0:Lepson/print/screen/PrinterFinder;

    iget-object p1, p1, Lepson/print/screen/PrinterFinder;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, p4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public onGetInkState()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    return-void
.end method

.method public onGetStatusState()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    return-void
.end method

.method public onNotifyContinueable(I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    return-void
.end method

.method public onNotifyEndJob(I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    return-void
.end method

.method public onNotifyError(IIZ)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 221
    invoke-static {}, Lepson/print/screen/PrinterFinder;->access$100()Ljava/lang/Object;

    move-result-object p1

    monitor-enter p1

    .line 222
    :try_start_0
    iget-object p3, p0, Lepson/print/screen/PrinterFinder$4;->this$0:Lepson/print/screen/PrinterFinder;

    invoke-virtual {p3}, Lepson/print/screen/PrinterFinder;->getApplicationContext()Landroid/content/Context;

    move-result-object p3

    const-string v0, "PrintSetting"

    const-string v1, "RE_SEARCH"

    invoke-static {p3, v0, v1}, Lepson/common/Utils;->getPrefBoolean(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result p3

    invoke-static {p3}, Lepson/print/screen/PrinterFinder;->access$602(Z)Z

    const/16 p3, -0x44c

    const/16 v0, -0x547

    const/16 v1, -0x514

    if-eq p2, v1, :cond_0

    if-ne p2, v0, :cond_1

    .line 241
    :cond_0
    invoke-static {}, Lepson/print/screen/PrinterFinder;->access$600()Z

    move-result v2

    if-nez v2, :cond_1

    const/16 p2, -0x44c

    goto :goto_0

    :cond_1
    if-eq p2, v1, :cond_2

    if-eq p2, v0, :cond_2

    if-ne p2, p3, :cond_3

    .line 243
    :cond_2
    invoke-static {}, Lepson/print/screen/PrinterFinder;->access$600()Z

    move-result p3

    const/4 v0, 0x1

    if-ne p3, v0, :cond_3

    const/16 p2, -0x2af9

    .line 246
    :cond_3
    :goto_0
    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 247
    iget-object p1, p0, Lepson/print/screen/PrinterFinder$4;->this$0:Lepson/print/screen/PrinterFinder;

    iput p2, p1, Lepson/print/screen/PrinterFinder;->curError:I

    .line 248
    iget-object p1, p1, Lepson/print/screen/PrinterFinder;->mHandler:Landroid/os/Handler;

    const/4 p2, 0x5

    invoke-virtual {p1, p2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void

    :catchall_0
    move-exception p2

    .line 246
    :try_start_1
    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p2
.end method

.method public onNotifyProgress(II)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    return-void
.end method

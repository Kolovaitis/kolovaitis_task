.class Lepson/print/screen/SearchPrinterScr$16;
.super Lepson/print/service/IEpsonServiceCallback$Stub;
.source "SearchPrinterScr.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/screen/SearchPrinterScr;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/screen/SearchPrinterScr;


# direct methods
.method constructor <init>(Lepson/print/screen/SearchPrinterScr;)V
    .locals 0

    .line 1476
    iput-object p1, p0, Lepson/print/screen/SearchPrinterScr$16;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-direct {p0}, Lepson/print/service/IEpsonServiceCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onFindPrinterResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 1486
    invoke-static {p1}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;)V

    .line 1488
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr$16;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {v0}, Lepson/print/screen/SearchPrinterScr;->access$200(Lepson/print/screen/SearchPrinterScr;)I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x3

    if-eq v0, v1, :cond_1

    if-eq v0, v2, :cond_0

    goto :goto_0

    .line 1507
    :cond_0
    iget-object p5, p0, Lepson/print/screen/SearchPrinterScr$16;->this$0:Lepson/print/screen/SearchPrinterScr;

    new-instance v0, Lepson/print/MyPrinter;

    invoke-direct {v0, p1, p2, p3, p4}, Lepson/print/MyPrinter;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p5, v0}, Lepson/print/screen/SearchPrinterScr;->access$2002(Lepson/print/screen/SearchPrinterScr;Lepson/print/MyPrinter;)Lepson/print/MyPrinter;

    goto :goto_0

    .line 1490
    :cond_1
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 1491
    iput v2, v0, Landroid/os/Message;->what:I

    .line 1492
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "name"

    .line 1493
    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p1, "ip"

    .line 1494
    invoke-virtual {v1, p1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p1, "id"

    .line 1495
    invoke-virtual {v1, p1, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p1, "serial_no"

    .line 1496
    invoke-virtual {v1, p1, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p1, "common_devicename"

    .line 1497
    invoke-virtual {v1, p1, p5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1498
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1499
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$16;->this$0:Lepson/print/screen/SearchPrinterScr;

    iget-object p1, p1, Lepson/print/screen/SearchPrinterScr;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :goto_0
    return-void
.end method

.method public onGetInkState()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    return-void
.end method

.method public onGetStatusState()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    return-void
.end method

.method public onNotifyContinueable(I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 1523
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$16;->this$0:Lepson/print/screen/SearchPrinterScr;

    iget-object p1, p1, Lepson/print/screen/SearchPrinterScr;->progress:Lepson/print/screen/WorkingDialog;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$16;->this$0:Lepson/print/screen/SearchPrinterScr;

    iget-object p1, p1, Lepson/print/screen/SearchPrinterScr;->progress:Lepson/print/screen/WorkingDialog;

    invoke-virtual {p1}, Lepson/print/screen/WorkingDialog;->isShowing()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 1524
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$16;->this$0:Lepson/print/screen/SearchPrinterScr;

    iget-object p1, p1, Lepson/print/screen/SearchPrinterScr;->progress:Lepson/print/screen/WorkingDialog;

    invoke-virtual {p1}, Lepson/print/screen/WorkingDialog;->dismiss()V

    .line 1527
    :cond_0
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$16;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {p1}, Lepson/print/screen/SearchPrinterScr;->access$1900(Lepson/print/screen/SearchPrinterScr;)Lepson/print/MyPrinter;

    move-result-object p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$16;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {p1}, Lepson/print/screen/SearchPrinterScr;->access$2000(Lepson/print/screen/SearchPrinterScr;)Lepson/print/MyPrinter;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 1528
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$16;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {p1}, Lepson/print/screen/SearchPrinterScr;->access$200(Lepson/print/screen/SearchPrinterScr;)I

    move-result p1

    const/4 v0, 0x3

    if-ne p1, v0, :cond_2

    .line 1530
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$16;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {p1}, Lepson/print/screen/SearchPrinterScr;->access$2000(Lepson/print/screen/SearchPrinterScr;)Lepson/print/MyPrinter;

    move-result-object p1

    invoke-virtual {p1}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr$16;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {v0}, Lepson/print/screen/SearchPrinterScr;->access$1900(Lepson/print/screen/SearchPrinterScr;)Lepson/print/MyPrinter;

    move-result-object v0

    invoke-virtual {v0}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 1532
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$16;->this$0:Lepson/print/screen/SearchPrinterScr;

    iget-object p1, p1, Lepson/print/screen/SearchPrinterScr;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object p1

    const/4 v0, 0x4

    .line 1533
    iput v0, p1, Landroid/os/Message;->what:I

    .line 1534
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr$16;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {v0}, Lepson/print/screen/SearchPrinterScr;->access$1900(Lepson/print/screen/SearchPrinterScr;)Lepson/print/MyPrinter;

    move-result-object v0

    iput-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1535
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr$16;->this$0:Lepson/print/screen/SearchPrinterScr;

    iget-object v0, v0, Lepson/print/screen/SearchPrinterScr;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    .line 1538
    :cond_1
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$16;->this$0:Lepson/print/screen/SearchPrinterScr;

    const v0, -0x7a121

    iput v0, p1, Lepson/print/screen/SearchPrinterScr;->curError:I

    .line 1539
    iget-object p1, p1, Lepson/print/screen/SearchPrinterScr;->mHandler:Landroid/os/Handler;

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_2
    return-void
.end method

.method public onNotifyEndJob(I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    return-void
.end method

.method public onNotifyError(IIZ)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 1546
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$16;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {p1}, Lepson/print/screen/SearchPrinterScr;->access$1000(Lepson/print/screen/SearchPrinterScr;)Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    .line 1552
    :cond_0
    invoke-static {}, Lepson/print/screen/SearchPrinterScr;->access$1100()Ljava/lang/Object;

    move-result-object p1

    monitor-enter p1

    .line 1553
    :try_start_0
    iget-object p3, p0, Lepson/print/screen/SearchPrinterScr$16;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-virtual {p3}, Lepson/print/screen/SearchPrinterScr;->getApplicationContext()Landroid/content/Context;

    move-result-object p3

    const-string v0, "PrintSetting"

    const-string v1, "RE_SEARCH"

    invoke-static {p3, v0, v1}, Lepson/common/Utils;->getPrefBoolean(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result p3

    invoke-static {p3}, Lepson/print/screen/SearchPrinterScr;->access$1302(Z)Z

    const/4 p3, 0x1

    const/16 v0, -0x44c

    const/16 v1, -0x547

    const/16 v2, -0x514

    if-eq p2, v2, :cond_1

    if-ne p2, v1, :cond_2

    .line 1555
    :cond_1
    invoke-static {}, Lepson/print/screen/SearchPrinterScr;->access$1300()Z

    move-result v3

    if-nez v3, :cond_2

    const/16 p2, -0x44c

    goto :goto_0

    :cond_2
    if-eq p2, v2, :cond_3

    if-eq p2, v1, :cond_3

    if-ne p2, v0, :cond_4

    .line 1557
    :cond_3
    invoke-static {}, Lepson/print/screen/SearchPrinterScr;->access$1300()Z

    move-result v0

    if-ne v0, p3, :cond_4

    const/16 p2, -0x2af9

    .line 1560
    :cond_4
    :goto_0
    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1562
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$16;->this$0:Lepson/print/screen/SearchPrinterScr;

    iput p2, p1, Lepson/print/screen/SearchPrinterScr;->curError:I

    .line 1564
    iget-object p1, p1, Lepson/print/screen/SearchPrinterScr;->mHandler:Landroid/os/Handler;

    const/16 p2, 0xd

    invoke-virtual {p1, p2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1567
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$16;->this$0:Lepson/print/screen/SearchPrinterScr;

    iget-object p1, p1, Lepson/print/screen/SearchPrinterScr;->progress:Lepson/print/screen/WorkingDialog;

    if-eqz p1, :cond_5

    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$16;->this$0:Lepson/print/screen/SearchPrinterScr;

    iget-object p1, p1, Lepson/print/screen/SearchPrinterScr;->progress:Lepson/print/screen/WorkingDialog;

    invoke-virtual {p1}, Lepson/print/screen/WorkingDialog;->isShowing()Z

    move-result p1

    if-eqz p1, :cond_5

    .line 1568
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$16;->this$0:Lepson/print/screen/SearchPrinterScr;

    iget-object p1, p1, Lepson/print/screen/SearchPrinterScr;->progress:Lepson/print/screen/WorkingDialog;

    invoke-virtual {p1}, Lepson/print/screen/WorkingDialog;->dismiss()V

    .line 1572
    :cond_5
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$16;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-static {p1, p2}, Lepson/print/screen/SearchPrinterScr;->access$102(Lepson/print/screen/SearchPrinterScr;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    return-void

    :catchall_0
    move-exception p2

    .line 1560
    :try_start_1
    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p2
.end method

.method public onNotifyProgress(II)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    return-void
.end method

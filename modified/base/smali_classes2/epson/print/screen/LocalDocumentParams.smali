.class public Lepson/print/screen/LocalDocumentParams;
.super Ljava/lang/Object;
.source "LocalDocumentParams.java"

# interfaces
.implements Lepson/print/screen/PrintProgress$ProgressParams;


# instance fields
.field private mEndPage:I

.field private mIsPaperLandscape:Z

.field private mOriginalFilename:Ljava/lang/String;

.field private mPassword:Ljava/lang/String;

.field private mPdfFilename:Ljava/lang/String;

.field private mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;

.field private mStartPage:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIILcom/epson/iprint/prtlogger/PrintLog;)V
    .locals 0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lepson/print/screen/LocalDocumentParams;->mPdfFilename:Ljava/lang/String;

    .line 28
    iput-object p2, p0, Lepson/print/screen/LocalDocumentParams;->mOriginalFilename:Ljava/lang/String;

    .line 29
    iput-object p3, p0, Lepson/print/screen/LocalDocumentParams;->mPassword:Ljava/lang/String;

    .line 30
    iput-boolean p4, p0, Lepson/print/screen/LocalDocumentParams;->mIsPaperLandscape:Z

    .line 31
    iput p5, p0, Lepson/print/screen/LocalDocumentParams;->mStartPage:I

    .line 32
    iput p6, p0, Lepson/print/screen/LocalDocumentParams;->mEndPage:I

    .line 33
    iput-object p7, p0, Lepson/print/screen/LocalDocumentParams;->mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;

    return-void
.end method


# virtual methods
.method public getApfMode()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getEpsonColorMode()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getOriginalSheetSize()I
    .locals 2

    .line 43
    iget v0, p0, Lepson/print/screen/LocalDocumentParams;->mEndPage:I

    iget v1, p0, Lepson/print/screen/LocalDocumentParams;->mStartPage:I

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public getPrintLog()Lcom/epson/iprint/prtlogger/PrintLog;
    .locals 1

    .line 66
    iget-object v0, p0, Lepson/print/screen/LocalDocumentParams;->mPrintLog:Lcom/epson/iprint/prtlogger/PrintLog;

    return-object v0
.end method

.method public getPrintSetting(Landroid/content/Context;)Lepson/print/screen/PrintSetting;
    .locals 2

    .line 48
    new-instance v0, Lepson/print/screen/PrintSetting;

    sget-object v1, Lepson/print/screen/PrintSetting$Kind;->document:Lepson/print/screen/PrintSetting$Kind;

    invoke-direct {v0, p1, v1}, Lepson/print/screen/PrintSetting;-><init>(Landroid/content/Context;Lepson/print/screen/PrintSetting$Kind;)V

    return-object v0
.end method

.method public getPrintSettingType()Lepson/print/screen/PrintSetting$Kind;
    .locals 1

    .line 71
    sget-object v0, Lepson/print/screen/PrintSetting$Kind;->document:Lepson/print/screen/PrintSetting$Kind;

    return-object v0
.end method

.method public isPaperLandscape()Z
    .locals 1

    .line 53
    iget-boolean v0, p0, Lepson/print/screen/LocalDocumentParams;->mIsPaperLandscape:Z

    return v0
.end method

.method public print(Lepson/print/service/IEpsonService;Z)Z
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 58
    iget-object v1, p0, Lepson/print/screen/LocalDocumentParams;->mPdfFilename:Ljava/lang/String;

    iget-object v2, p0, Lepson/print/screen/LocalDocumentParams;->mOriginalFilename:Ljava/lang/String;

    iget-object v3, p0, Lepson/print/screen/LocalDocumentParams;->mPassword:Ljava/lang/String;

    iget-boolean v4, p0, Lepson/print/screen/LocalDocumentParams;->mIsPaperLandscape:Z

    iget v5, p0, Lepson/print/screen/LocalDocumentParams;->mStartPage:I

    iget v6, p0, Lepson/print/screen/LocalDocumentParams;->mEndPage:I

    move-object v0, p1

    move v7, p2

    invoke-interface/range {v0 .. v7}, Lepson/print/service/IEpsonService;->printLocalPdf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIIZ)I

    const/4 p1, 0x1

    return p1
.end method

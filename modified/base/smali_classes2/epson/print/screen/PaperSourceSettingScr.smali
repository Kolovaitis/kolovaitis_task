.class public Lepson/print/screen/PaperSourceSettingScr;
.super Lepson/print/ActivityIACommon;
.source "PaperSourceSettingScr.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/screen/PaperSourceSettingScr$PaperSourceInfoBuilder;
    }
.end annotation


# static fields
.field private static final ID_BINDSERVICE:I = 0x1

.field private static final ID_NOT_ESCPRSUPPORT:I = 0xa

.field public static final PRINT_SETTING_TYPE:Ljava/lang/String; = "print-setting-type"

.field private static final SETTING_DONE:I = 0x3


# instance fields
.field aPaperSourceSetting:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lepson/print/screen/PaperSourceSetting;",
            ">;"
        }
    .end annotation
.end field

.field listItemClickLister:Landroid/widget/AdapterView$OnItemClickListener;

.field private mCallback:Lepson/print/service/IEpsonServiceCallback;

.field private mEpsonConnection:Landroid/content/ServiceConnection;

.field private mEpsonService:Lepson/print/service/IEpsonService;

.field mHandler:Landroid/os/Handler;

.field mListBuilder:Lepson/print/screen/PaperSourceSettingScr$PaperSourceInfoBuilder;

.field private mPrintSettingTypeName:Ljava/lang/String;

.field paperSourceInfo:Lepson/print/screen/PaperSourceInfo;

.field progress:Lepson/print/screen/WorkingDialog;


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 46
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    .line 57
    sget-object v0, Lepson/print/screen/PrintSetting$Kind;->photo:Lepson/print/screen/PrintSetting$Kind;

    invoke-virtual {v0}, Lepson/print/screen/PrintSetting$Kind;->name()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepson/print/screen/PaperSourceSettingScr;->mPrintSettingTypeName:Ljava/lang/String;

    const/4 v0, 0x0

    .line 60
    iput-object v0, p0, Lepson/print/screen/PaperSourceSettingScr;->mListBuilder:Lepson/print/screen/PaperSourceSettingScr$PaperSourceInfoBuilder;

    .line 66
    iput-object v0, p0, Lepson/print/screen/PaperSourceSettingScr;->paperSourceInfo:Lepson/print/screen/PaperSourceInfo;

    .line 69
    iput-object v0, p0, Lepson/print/screen/PaperSourceSettingScr;->progress:Lepson/print/screen/WorkingDialog;

    .line 151
    new-instance v1, Landroid/os/Handler;

    new-instance v2, Lepson/print/screen/PaperSourceSettingScr$1;

    invoke-direct {v2, p0}, Lepson/print/screen/PaperSourceSettingScr$1;-><init>(Lepson/print/screen/PaperSourceSettingScr;)V

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v1, p0, Lepson/print/screen/PaperSourceSettingScr;->mHandler:Landroid/os/Handler;

    .line 252
    new-instance v1, Lepson/print/screen/PaperSourceSettingScr$2;

    invoke-direct {v1, p0}, Lepson/print/screen/PaperSourceSettingScr$2;-><init>(Lepson/print/screen/PaperSourceSettingScr;)V

    iput-object v1, p0, Lepson/print/screen/PaperSourceSettingScr;->listItemClickLister:Landroid/widget/AdapterView$OnItemClickListener;

    .line 405
    iput-object v0, p0, Lepson/print/screen/PaperSourceSettingScr;->mEpsonService:Lepson/print/service/IEpsonService;

    .line 406
    new-instance v0, Lepson/print/screen/PaperSourceSettingScr$4;

    invoke-direct {v0, p0}, Lepson/print/screen/PaperSourceSettingScr$4;-><init>(Lepson/print/screen/PaperSourceSettingScr;)V

    iput-object v0, p0, Lepson/print/screen/PaperSourceSettingScr;->mEpsonConnection:Landroid/content/ServiceConnection;

    .line 436
    new-instance v0, Lepson/print/screen/PaperSourceSettingScr$5;

    invoke-direct {v0, p0}, Lepson/print/screen/PaperSourceSettingScr$5;-><init>(Lepson/print/screen/PaperSourceSettingScr;)V

    iput-object v0, p0, Lepson/print/screen/PaperSourceSettingScr;->mCallback:Lepson/print/service/IEpsonServiceCallback;

    return-void
.end method

.method static synthetic access$000(Lepson/print/screen/PaperSourceSettingScr;)Ljava/lang/String;
    .locals 0

    .line 46
    iget-object p0, p0, Lepson/print/screen/PaperSourceSettingScr;->mPrintSettingTypeName:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$100(Lepson/print/screen/PaperSourceSettingScr;)Lepson/print/service/IEpsonService;
    .locals 0

    .line 46
    iget-object p0, p0, Lepson/print/screen/PaperSourceSettingScr;->mEpsonService:Lepson/print/service/IEpsonService;

    return-object p0
.end method

.method static synthetic access$102(Lepson/print/screen/PaperSourceSettingScr;Lepson/print/service/IEpsonService;)Lepson/print/service/IEpsonService;
    .locals 0

    .line 46
    iput-object p1, p0, Lepson/print/screen/PaperSourceSettingScr;->mEpsonService:Lepson/print/service/IEpsonService;

    return-object p1
.end method

.method static synthetic access$200(Lepson/print/screen/PaperSourceSettingScr;)Lepson/print/service/IEpsonServiceCallback;
    .locals 0

    .line 46
    iget-object p0, p0, Lepson/print/screen/PaperSourceSettingScr;->mCallback:Lepson/print/service/IEpsonServiceCallback;

    return-object p0
.end method


# virtual methods
.method addPaparSettings()V
    .locals 8

    .line 183
    iget-object v0, p0, Lepson/print/screen/PaperSourceSettingScr;->mEpsonService:Lepson/print/service/IEpsonService;

    if-nez v0, :cond_0

    return-void

    .line 191
    :cond_0
    iget-object v0, p0, Lepson/print/screen/PaperSourceSettingScr;->aPaperSourceSetting:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    return-void

    .line 195
    :cond_1
    iget-object v0, p0, Lepson/print/screen/PaperSourceSettingScr;->mListBuilder:Lepson/print/screen/PaperSourceSettingScr$PaperSourceInfoBuilder;

    invoke-virtual {v0}, Lepson/print/screen/PaperSourceSettingScr$PaperSourceInfoBuilder;->refresh()V

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 197
    :goto_0
    iget-object v2, p0, Lepson/print/screen/PaperSourceSettingScr;->aPaperSourceSetting:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_9

    .line 199
    iget-object v2, p0, Lepson/print/screen/PaperSourceSettingScr;->aPaperSourceSetting:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lepson/print/screen/PaperSourceSetting;

    .line 205
    :try_start_0
    iput-boolean v0, v2, Lepson/print/screen/PaperSourceSetting;->bSupportESCPR:Z

    .line 208
    iget-object v3, p0, Lepson/print/screen/PaperSourceSettingScr;->mEpsonService:Lepson/print/service/IEpsonService;

    invoke-interface {v3}, Lepson/print/service/IEpsonService;->getPaperSize()[I

    move-result-object v3

    if-eqz v3, :cond_3

    const/4 v4, 0x0

    .line 210
    :goto_1
    array-length v5, v3

    if-ge v4, v5, :cond_8

    .line 211
    aget v5, v3, v4

    iget v6, v2, Lepson/print/screen/PaperSourceSetting;->paperSizeId:I

    if-ne v5, v6, :cond_2

    goto :goto_2

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_3
    const/4 v4, 0x0

    .line 219
    :goto_2
    iget-object v3, p0, Lepson/print/screen/PaperSourceSettingScr;->mEpsonService:Lepson/print/service/IEpsonService;

    invoke-interface {v3, v4}, Lepson/print/service/IEpsonService;->getPaperType(I)[I

    move-result-object v3

    if-eqz v3, :cond_5

    const/4 v5, 0x0

    .line 221
    :goto_3
    array-length v6, v3

    if-ge v5, v6, :cond_8

    .line 222
    aget v6, v3, v5

    iget v7, v2, Lepson/print/screen/PaperSourceSetting;->paperTypeId:I

    if-ne v6, v7, :cond_4

    goto :goto_4

    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    :cond_5
    const/4 v5, 0x0

    .line 230
    :goto_4
    iget-object v3, p0, Lepson/print/screen/PaperSourceSettingScr;->mEpsonService:Lepson/print/service/IEpsonService;

    const/4 v6, 0x1

    invoke-interface {v3, v4, v5, v6}, Lepson/print/service/IEpsonService;->getPaperSource(III)[I

    move-result-object v3

    if-eqz v3, :cond_7

    const/4 v4, 0x0

    .line 232
    :goto_5
    array-length v5, v3

    if-ge v4, v5, :cond_8

    .line 233
    aget v5, v3, v4

    iget v7, v2, Lepson/print/screen/PaperSourceSetting;->paperSource:I

    if-ne v5, v7, :cond_6

    goto :goto_6

    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    .line 240
    :cond_7
    :goto_6
    iput-boolean v6, v2, Lepson/print/screen/PaperSourceSetting;->bSupportESCPR:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_7

    :catch_0
    move-exception v3

    .line 243
    invoke-virtual {v3}, Landroid/os/RemoteException;->printStackTrace()V

    .line 246
    :cond_8
    :goto_7
    iget-object v3, p0, Lepson/print/screen/PaperSourceSettingScr;->mListBuilder:Lepson/print/screen/PaperSourceSettingScr$PaperSourceInfoBuilder;

    invoke-virtual {v3, v2}, Lepson/print/screen/PaperSourceSettingScr$PaperSourceInfoBuilder;->addPaperInfoSetting(Lepson/print/screen/PaperSourceSetting;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_9
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .line 73
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    .line 76
    invoke-virtual {p0}, Lepson/print/screen/PaperSourceSettingScr;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object p1

    const v0, 0x7f0a00a7

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    .line 77
    new-instance v0, Lepson/print/screen/PaperSourceSettingScr$PaperSourceInfoBuilder;

    invoke-virtual {p0}, Lepson/print/screen/PaperSourceSettingScr;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1}, Lepson/print/screen/PaperSourceSettingScr$PaperSourceInfoBuilder;-><init>(Lepson/print/screen/PaperSourceSettingScr;Landroid/content/Context;Landroid/view/ViewGroup;)V

    iput-object v0, p0, Lepson/print/screen/PaperSourceSettingScr;->mListBuilder:Lepson/print/screen/PaperSourceSettingScr$PaperSourceInfoBuilder;

    .line 78
    iget-object v0, p0, Lepson/print/screen/PaperSourceSettingScr;->mListBuilder:Lepson/print/screen/PaperSourceSettingScr$PaperSourceInfoBuilder;

    invoke-virtual {v0}, Lepson/print/screen/PaperSourceSettingScr$PaperSourceInfoBuilder;->build()V

    const v0, 0x7f080123

    .line 81
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0e0425

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    const v0, 0x102000a

    .line 83
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iget-object v1, p0, Lepson/print/screen/PaperSourceSettingScr;->listItemClickLister:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 85
    invoke-virtual {p0, p1}, Lepson/print/screen/PaperSourceSettingScr;->setContentView(Landroid/view/View;)V

    const/4 p1, 0x1

    const v0, 0x7f0e0426

    .line 88
    invoke-virtual {p0, v0, p1}, Lepson/print/screen/PaperSourceSettingScr;->setActionBar(IZ)V

    .line 91
    invoke-virtual {p0}, Lepson/print/screen/PaperSourceSettingScr;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "print-setting-type"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepson/print/screen/PaperSourceSettingScr;->mPrintSettingTypeName:Ljava/lang/String;

    .line 92
    iget-object v0, p0, Lepson/print/screen/PaperSourceSettingScr;->mPrintSettingTypeName:Ljava/lang/String;

    invoke-static {p0, v0}, Lepson/print/screen/PrintSetting;->getInstance(Landroid/content/Context;Ljava/lang/String;)Lepson/print/screen/PrintSetting;

    move-result-object v0

    .line 93
    invoke-virtual {v0}, Lepson/print/screen/PrintSetting;->loadSettings()V

    .line 96
    new-instance v1, Lepson/print/screen/PaperSourceSetting;

    invoke-direct {v1}, Lepson/print/screen/PaperSourceSetting;-><init>()V

    .line 97
    iget v2, v0, Lepson/print/screen/PrintSetting;->paperSizeValue:I

    iput v2, v1, Lepson/print/screen/PaperSourceSetting;->paperSizeId:I

    .line 98
    iget v2, v0, Lepson/print/screen/PrintSetting;->paperTypeValue:I

    iput v2, v1, Lepson/print/screen/PaperSourceSetting;->paperTypeId:I

    .line 99
    iget v0, v0, Lepson/print/screen/PrintSetting;->paperSourceValue:I

    iput v0, v1, Lepson/print/screen/PaperSourceSetting;->paperSource:I

    .line 101
    iget-object v0, p0, Lepson/print/screen/PaperSourceSettingScr;->mListBuilder:Lepson/print/screen/PaperSourceSettingScr$PaperSourceInfoBuilder;

    invoke-virtual {v0, v1}, Lepson/print/screen/PaperSourceSettingScr$PaperSourceInfoBuilder;->setResource(Ljava/lang/Object;)V

    .line 104
    invoke-static {p0}, Lepson/print/screen/PaperSourceInfo;->getInstance(Landroid/content/Context;)Lepson/print/screen/PaperSourceInfo;

    move-result-object v0

    iput-object v0, p0, Lepson/print/screen/PaperSourceSettingScr;->paperSourceInfo:Lepson/print/screen/PaperSourceInfo;

    .line 107
    iget-object v0, p0, Lepson/print/screen/PaperSourceSettingScr;->mEpsonService:Lepson/print/service/IEpsonService;

    if-nez v0, :cond_0

    .line 108
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 109
    const-class v1, Lepson/print/service/EpsonService;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 110
    iget-object v1, p0, Lepson/print/screen/PaperSourceSettingScr;->mEpsonConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0, v1, p1}, Lepson/print/screen/PaperSourceSettingScr;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 114
    :cond_0
    new-instance p1, Lepson/print/screen/WorkingDialog;

    invoke-direct {p1, p0}, Lepson/print/screen/WorkingDialog;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lepson/print/screen/PaperSourceSettingScr;->progress:Lepson/print/screen/WorkingDialog;

    .line 115
    iget-object p1, p0, Lepson/print/screen/PaperSourceSettingScr;->progress:Lepson/print/screen/WorkingDialog;

    invoke-virtual {p1}, Lepson/print/screen/WorkingDialog;->show()V

    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 3

    .line 287
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    const/16 v1, 0xa

    if-eq p1, v1, :cond_0

    goto :goto_0

    .line 292
    :cond_0
    new-instance p1, Landroid/app/AlertDialog$Builder;

    invoke-direct {p1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 293
    invoke-virtual {p0}, Lepson/print/screen/PaperSourceSettingScr;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0428

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 294
    invoke-virtual {p0}, Lepson/print/screen/PaperSourceSettingScr;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e0427

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 295
    invoke-virtual {p0}, Lepson/print/screen/PaperSourceSettingScr;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e03fe

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lepson/print/screen/PaperSourceSettingScr$3;

    invoke-direct {v2, p0}, Lepson/print/screen/PaperSourceSettingScr$3;-><init>(Lepson/print/screen/PaperSourceSettingScr;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 302
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method protected onDestroy()V
    .locals 2

    .line 120
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onDestroy()V

    .line 123
    iget-object v0, p0, Lepson/print/screen/PaperSourceSettingScr;->mEpsonService:Lepson/print/service/IEpsonService;

    if-eqz v0, :cond_0

    .line 125
    :try_start_0
    iget-object v1, p0, Lepson/print/screen/PaperSourceSettingScr;->mCallback:Lepson/print/service/IEpsonServiceCallback;

    invoke-interface {v0, v1}, Lepson/print/service/IEpsonService;->unregisterCallback(Lepson/print/service/IEpsonServiceCallback;)V

    .line 126
    iget-object v0, p0, Lepson/print/screen/PaperSourceSettingScr;->mEpsonConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lepson/print/screen/PaperSourceSettingScr;->unbindService(Landroid/content/ServiceConnection;)V

    const/4 v0, 0x0

    .line 127
    iput-object v0, p0, Lepson/print/screen/PaperSourceSettingScr;->mEpsonService:Lepson/print/service/IEpsonService;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 129
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_0
    :goto_0
    return-void
.end method

.method protected onPause()V
    .locals 1

    .line 144
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onPause()V

    .line 146
    iget-object v0, p0, Lepson/print/screen/PaperSourceSettingScr;->paperSourceInfo:Lepson/print/screen/PaperSourceInfo;

    invoke-virtual {v0}, Lepson/print/screen/PaperSourceInfo;->stop()V

    return-void
.end method

.method protected onResume()V
    .locals 2

    .line 137
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onResume()V

    .line 139
    iget-object v0, p0, Lepson/print/screen/PaperSourceSettingScr;->paperSourceInfo:Lepson/print/screen/PaperSourceInfo;

    iget-object v1, p0, Lepson/print/screen/PaperSourceSettingScr;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p0, v1}, Lepson/print/screen/PaperSourceInfo;->start(Landroid/content/Context;Landroid/os/Handler;)V

    return-void
.end method

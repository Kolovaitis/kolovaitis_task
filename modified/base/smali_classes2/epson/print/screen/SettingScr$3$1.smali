.class Lepson/print/screen/SettingScr$3$1;
.super Landroid/os/AsyncTask;
.source "SettingScr.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/screen/SettingScr$3;->handleMessage(Landroid/os/Message;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lepson/print/screen/SettingScr$3;


# direct methods
.method constructor <init>(Lepson/print/screen/SettingScr$3;)V
    .locals 0

    .line 1729
    iput-object p1, p0, Lepson/print/screen/SettingScr$3$1;->this$1:Lepson/print/screen/SettingScr$3;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 5

    const-string p1, ""

    .line 1738
    iget-object v0, p0, Lepson/print/screen/SettingScr$3$1;->this$1:Lepson/print/screen/SettingScr$3;

    iget-object v0, v0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$400(Lepson/print/screen/SettingScr;)I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    .line 1742
    new-instance v0, Lepson/print/EPPrinterManager;

    iget-object v3, p0, Lepson/print/screen/SettingScr$3$1;->this$1:Lepson/print/screen/SettingScr$3;

    iget-object v3, v3, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v3}, Lepson/print/screen/SettingScr;->access$1000(Lepson/print/screen/SettingScr;)Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, Lepson/print/EPPrinterManager;-><init>(Landroid/content/Context;)V

    .line 1744
    iget-object v3, p0, Lepson/print/screen/SettingScr$3$1;->this$1:Lepson/print/screen/SettingScr$3;

    iget-object v3, v3, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v3}, Lepson/print/screen/SettingScr;->access$300(Lepson/print/screen/SettingScr;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lepson/print/EPPrinterManager;->loadRemotePrinterInfo(Ljava/lang/String;)Lepson/print/EPPrinterInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1746
    iget-object p1, v0, Lepson/print/EPPrinterInfo;->printerAccessKey:Ljava/lang/String;

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 1754
    :goto_0
    :try_start_0
    iget-object v3, p0, Lepson/print/screen/SettingScr$3$1;->this$1:Lepson/print/screen/SettingScr$3;

    iget-object v3, v3, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v3}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v3

    .line 1755
    invoke-virtual {v3}, Lepson/common/ExternalFileUtils;->getSupportedMediaDir()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lepson/common/ExternalFileUtils;->createTempFolder(Ljava/lang/String;)Z

    .line 1756
    iget-object v3, p0, Lepson/print/screen/SettingScr$3$1;->this$1:Lepson/print/screen/SettingScr$3;

    iget-object v3, v3, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v3}, Lepson/print/screen/SettingScr;->access$600(Lepson/print/screen/SettingScr;)Lepson/print/service/IEpsonService;

    move-result-object v3

    iget-object v4, p0, Lepson/print/screen/SettingScr$3$1;->this$1:Lepson/print/screen/SettingScr$3;

    iget-object v4, v4, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v4}, Lepson/print/screen/SettingScr;->access$900(Lepson/print/screen/SettingScr;)Z

    move-result v4

    if-eqz v4, :cond_2

    goto :goto_1

    :cond_2
    const/4 v1, 0x2

    :goto_1
    iget-object v2, p0, Lepson/print/screen/SettingScr$3$1;->this$1:Lepson/print/screen/SettingScr$3;

    iget-object v2, v2, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    .line 1757
    invoke-static {v2}, Lepson/print/screen/SettingScr;->access$300(Lepson/print/screen/SettingScr;)Ljava/lang/String;

    move-result-object v2

    .line 1756
    invoke-interface {v3, v0, v1, v2, p1}, Lepson/print/service/IEpsonService;->getSupportedMedia(ZILjava/lang/String;Ljava/lang/String;)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 1759
    invoke-virtual {p1}, Landroid/os/RemoteException;->printStackTrace()V

    const/16 p1, -0x3e8

    .line 1762
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1729
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/print/screen/SettingScr$3$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 3

    .line 1767
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 1769
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, -0x44c

    if-eq v0, v1, :cond_1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v2, -0x547

    if-eq v0, v2, :cond_1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v2, -0x3e8

    if-eq v0, v2, :cond_1

    .line 1773
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v2, -0x4b1

    if-eq v0, v2, :cond_1

    .line 1774
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v2, -0x4b2

    if-eq v0, v2, :cond_1

    .line 1775
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    const/16 v0, -0x4b3

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const-string p1, "SettingScr"

    const-string v0, "mEpsonService.getSupportedMedia()"

    .line 1784
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1785
    iget-object p1, p0, Lepson/print/screen/SettingScr$3$1;->this$1:Lepson/print/screen/SettingScr$3;

    iget-object p1, p1, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    const/4 v0, 0x1

    iput-boolean v0, p1, Lepson/print/screen/SettingScr;->isInfoAvai:Z

    .line 1786
    iget-object p1, p0, Lepson/print/screen/SettingScr$3$1;->this$1:Lepson/print/screen/SettingScr$3;

    iget-object p1, p1, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    iget-object p1, p1, Lepson/print/screen/SettingScr;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    .line 1777
    :cond_1
    :goto_0
    iget-object p1, p0, Lepson/print/screen/SettingScr$3$1;->this$1:Lepson/print/screen/SettingScr$3;

    iget-object p1, p1, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    const/4 v0, 0x0

    iput-boolean v0, p1, Lepson/print/screen/SettingScr;->isInfoAvai:Z

    .line 1778
    new-instance p1, Landroid/os/Message;

    invoke-direct {p1}, Landroid/os/Message;-><init>()V

    const/16 v0, 0x12

    .line 1779
    iput v0, p1, Landroid/os/Message;->what:I

    .line 1780
    iput v1, p1, Landroid/os/Message;->arg1:I

    .line 1781
    iget-object v0, p0, Lepson/print/screen/SettingScr$3$1;->this$1:Lepson/print/screen/SettingScr$3;

    iget-object v0, v0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    iget-object v0, v0, Lepson/print/screen/SettingScr;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :goto_1
    const-string p1, "SettingScr"

    .line 1788
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "isInfoAvai = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lepson/print/screen/SettingScr$3$1;->this$1:Lepson/print/screen/SettingScr$3;

    iget-object v1, v1, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    iget-boolean v1, v1, Lepson/print/screen/SettingScr;->isInfoAvai:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 1729
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lepson/print/screen/SettingScr$3$1;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.class public Lepson/print/screen/StringListSelectDialog;
.super Landroid/support/v4/app/DialogFragment;
.source "StringListSelectDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/screen/StringListSelectDialog$OnItemSelectedListener;
    }
.end annotation


# static fields
.field private static final ARGS_KEY_STRING_LIST:Ljava/lang/String; = "key-stringList"


# instance fields
.field private mListener:Lepson/print/screen/StringListSelectDialog$OnItemSelectedListener;

.field private mStringList:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lepson/print/screen/StringListSelectDialog;I)V
    .locals 0

    .line 15
    invoke-direct {p0, p1}, Lepson/print/screen/StringListSelectDialog;->itemSelected(I)V

    return-void
.end method

.method private itemSelected(I)V
    .locals 2

    .line 62
    iget-object v0, p0, Lepson/print/screen/StringListSelectDialog;->mListener:Lepson/print/screen/StringListSelectDialog$OnItemSelectedListener;

    if-eqz v0, :cond_0

    .line 63
    iget-object v1, p0, Lepson/print/screen/StringListSelectDialog;->mStringList:[Ljava/lang/String;

    aget-object p1, v1, p1

    invoke-interface {v0, p1}, Lepson/print/screen/StringListSelectDialog$OnItemSelectedListener;->onItemSelected(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public static newInstance([Ljava/lang/String;)Lepson/print/screen/StringListSelectDialog;
    .locals 3
    .param p0    # [Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 27
    new-instance v0, Lepson/print/screen/StringListSelectDialog;

    invoke-direct {v0}, Lepson/print/screen/StringListSelectDialog;-><init>()V

    .line 29
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "key-stringList"

    .line 30
    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 31
    invoke-virtual {v0, v1}, Lepson/print/screen/StringListSelectDialog;->setArguments(Landroid/os/Bundle;)V

    return-object v0
.end method


# virtual methods
.method public onAttach(Landroid/content/Context;)V
    .locals 2

    .line 69
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onAttach(Landroid/content/Context;)V

    .line 71
    :try_start_0
    move-object v0, p1

    check-cast v0, Lepson/print/screen/StringListSelectDialog$OnItemSelectedListener;

    iput-object v0, p0, Lepson/print/screen/StringListSelectDialog;->mListener:Lepson/print/screen/StringListSelectDialog$OnItemSelectedListener;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    .line 73
    :catch_0
    new-instance v0, Ljava/lang/ClassCastException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " must implement StringListSelectDialog.OnItemSelectedListener"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 39
    invoke-virtual {p0}, Lepson/print/screen/StringListSelectDialog;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    if-eqz p1, :cond_1

    const-string v0, "key-stringList"

    .line 43
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/print/screen/StringListSelectDialog;->mStringList:[Ljava/lang/String;

    .line 44
    iget-object p1, p0, Lepson/print/screen/StringListSelectDialog;->mStringList:[Ljava/lang/String;

    if-eqz p1, :cond_0

    .line 48
    new-instance p1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lepson/print/screen/StringListSelectDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {p1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 49
    iget-object v0, p0, Lepson/print/screen/StringListSelectDialog;->mStringList:[Ljava/lang/String;

    new-instance v1, Lepson/print/screen/StringListSelectDialog$1;

    invoke-direct {v1, p0}, Lepson/print/screen/StringListSelectDialog$1;-><init>(Lepson/print/screen/StringListSelectDialog;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const/4 v0, 0x0

    .line 56
    invoke-virtual {p0, v0}, Lepson/print/screen/StringListSelectDialog;->setCancelable(Z)V

    .line 58
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1

    .line 45
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-direct {p1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p1

    .line 41
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-direct {p1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p1
.end method

.method public onDetach()V
    .locals 1

    .line 79
    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->onDetach()V

    const/4 v0, 0x0

    .line 80
    iput-object v0, p0, Lepson/print/screen/StringListSelectDialog;->mListener:Lepson/print/screen/StringListSelectDialog$OnItemSelectedListener;

    return-void
.end method

.class Lepson/print/screen/SettingScr$4;
.super Lepson/print/service/IEpsonServiceCallback$Stub;
.source "SettingScr.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/screen/SettingScr;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/screen/SettingScr;


# direct methods
.method constructor <init>(Lepson/print/screen/SettingScr;)V
    .locals 0

    .line 2407
    iput-object p1, p0, Lepson/print/screen/SettingScr$4;->this$0:Lepson/print/screen/SettingScr;

    invoke-direct {p0}, Lepson/print/service/IEpsonServiceCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onFindPrinterResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 2417
    iget-object p1, p0, Lepson/print/screen/SettingScr$4;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1, p2}, Lepson/print/screen/SettingScr;->access$002(Lepson/print/screen/SettingScr;Ljava/lang/String;)Ljava/lang/String;

    return-void
.end method

.method public onGetInkState()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    return-void
.end method

.method public onGetStatusState()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    return-void
.end method

.method public onNotifyContinueable(I)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 2424
    iget-object v0, p0, Lepson/print/screen/SettingScr$4;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$800(Lepson/print/screen/SettingScr;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const-string v0, "IEpsonServiceCallback"

    .line 2428
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onNotifyContinueable code = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p1, :cond_1

    .line 2430
    iget-object p1, p0, Lepson/print/screen/SettingScr$4;->this$0:Lepson/print/screen/SettingScr;

    const/4 v0, 0x1

    iput-boolean v0, p1, Lepson/print/screen/SettingScr;->isInfoAvai:Z

    .line 2433
    invoke-virtual {p1}, Lepson/print/screen/SettingScr;->getPrinterLang()I

    move-result v0

    invoke-static {p1, v0}, Lepson/print/screen/SettingScr;->access$5602(Lepson/print/screen/SettingScr;I)I

    .line 2436
    iget-object p1, p0, Lepson/print/screen/SettingScr$4;->this$0:Lepson/print/screen/SettingScr;

    const/4 v0, 0x0

    iput-boolean v0, p1, Lepson/print/screen/SettingScr;->undoFlag:Z

    .line 2437
    iget-object p1, p1, Lepson/print/screen/SettingScr;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_1
    return-void
.end method

.method public onNotifyEndJob(I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    return-void
.end method

.method public onNotifyError(IIZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 2442
    iget-object p1, p0, Lepson/print/screen/SettingScr$4;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$800(Lepson/print/screen/SettingScr;)Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    :cond_0
    const-string p1, "IEpsonServiceCallback"

    .line 2446
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "onNotifyError errorCode = "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2462
    iget-object p1, p0, Lepson/print/screen/SettingScr$4;->this$0:Lepson/print/screen/SettingScr;

    const/4 p2, 0x1

    iput-boolean p2, p1, Lepson/print/screen/SettingScr;->isInfoAvai:Z

    .line 2463
    invoke-virtual {p1}, Lepson/print/screen/SettingScr;->getPrinterLang()I

    move-result p2

    invoke-static {p1, p2}, Lepson/print/screen/SettingScr;->access$5602(Lepson/print/screen/SettingScr;I)I

    .line 2464
    iget-object p1, p0, Lepson/print/screen/SettingScr$4;->this$0:Lepson/print/screen/SettingScr;

    const/4 p2, 0x0

    iput-boolean p2, p1, Lepson/print/screen/SettingScr;->undoFlag:Z

    .line 2465
    iget-object p1, p1, Lepson/print/screen/SettingScr;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, p2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public onNotifyProgress(II)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    return-void
.end method

.class public Lepson/print/screen/PrintProgress;
.super Landroid/app/Activity;
.source "PrintProgress.java"

# interfaces
.implements Lepson/print/CommonDefine;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/screen/PrintProgress$ProgressParams;
    }
.end annotation


# static fields
.field private static final DIALOG_CONFIRM:I = 0x1

.field private static final DIALOG_INFORM:I = 0x0

.field public static final END_PRINT:I = 0x4

.field public static final PARAM_DOCUMENT_MODE:Ljava/lang/String; = "PRINT_DOCUMENT"

.field public static final PARAM_EPSON_COLOR:Ljava/lang/String; = "epson_color"

.field public static final PARAM_EP_IMAGE_LIST:Ljava/lang/String; = "EPImageList"

.field public static final PARAM_PRINT_LOG:Ljava/lang/String; = "print_log"

.field public static final PARAM_PRINT_PROGRESS:Ljava/lang/String; = "progress-params"

.field public static final PREFS_NAME:Ljava/lang/String; = "PrintSetting"

.field public static final RESULT_ERROR:I = 0x3e8

.field private static bRestartactivity:Z = false

.field static copies:I = 0x0

.field static curError:I = 0x0

.field static fileIndex:I = 0x0

.field static isBkRetry:Z = false

.field static isContinue:Z = false

.field static isDocument:Z = false

.field private static final lockBCancel:Ljava/lang/Object;

.field private static mCallback:Lepson/print/service/IEpsonServiceCallback;

.field private static mEpsonConnection:Landroid/content/ServiceConnection;

.field private static mEpsonService:Lepson/print/service/IEpsonService;

.field private static final mLock:Ljava/lang/Object;

.field private static final mLockInit:Ljava/lang/Object;

.field static mProgressCopies:Landroid/widget/ProgressBar;

.field static mProgressPage:Landroid/widget/ProgressBar;

.field static mProgressPercent:Landroid/widget/ProgressBar;


# instance fields
.field private final CHECK_PRINTER:I

.field private final CHECK_PRINTER_CONTINUE:I

.field private final CONFIRM:I

.field private final CONFORM_MULTI:I

.field private final CONNECTED_SIMPLEAP:I

.field private final CONTINUE_PRINT:I

.field private final ECC_LIB_ERROR:I

.field private final EPS_DUPLEX_NONE:I

.field private final EPS_JOB_CANCELED:I

.field private final ERROR:I

.field private final FINISH:I

.field private final PERCENT:Ljava/lang/String;

.field private final PRINT_NEXT_PAGE:I

.field private final SHOW_COMPLETE_DIALOG:I

.field private final START_PRINT:I

.field private final UPDATE_PERCENT:I

.field bCancel:Z

.field private bSearching:Z

.field private bSearchingPrinter:Z

.field private context:Landroid/content/Context;

.field private curCopy:I

.field private curSheet:I

.field duplex:I

.field private isDialogOpen:Z

.field private isRemotePrinter:Z

.field private mApfProgressDisplay:Z

.field mCancelButton:Landroid/widget/Button;

.field mCanceled:Z

.field mCopies:Landroid/widget/TextView;

.field private volatile mError:Z

.field mHandler:Landroid/os/Handler;

.field mPage:Landroid/widget/TextView;

.field mPercent:Landroid/widget/TextView;

.field private mPrintProgressDisplay:Z

.field private mProgressParams:Lepson/print/screen/PrintProgress$ProgressParams;

.field private volatile mWaitEpsonServiceForFinish:Z

.field private percentString:Ljava/lang/String;

.field private printerIp:Ljava/lang/String;

.field private sheets:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 112
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lepson/print/screen/PrintProgress;->mLock:Ljava/lang/Object;

    .line 113
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lepson/print/screen/PrintProgress;->mLockInit:Ljava/lang/Object;

    .line 114
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lepson/print/screen/PrintProgress;->lockBCancel:Ljava/lang/Object;

    const/4 v0, 0x0

    .line 117
    sput-boolean v0, Lepson/print/screen/PrintProgress;->bRestartactivity:Z

    const/4 v1, 0x0

    .line 483
    sput-object v1, Lepson/print/screen/PrintProgress;->mEpsonService:Lepson/print/service/IEpsonService;

    .line 969
    sput v0, Lepson/print/screen/PrintProgress;->curError:I

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .line 84
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    .line 110
    iput-boolean v0, p0, Lepson/print/screen/PrintProgress;->bCancel:Z

    .line 111
    iput-boolean v0, p0, Lepson/print/screen/PrintProgress;->mCanceled:Z

    .line 116
    iput-boolean v0, p0, Lepson/print/screen/PrintProgress;->bSearchingPrinter:Z

    .line 119
    iput v0, p0, Lepson/print/screen/PrintProgress;->EPS_DUPLEX_NONE:I

    const/16 v1, 0x28

    .line 120
    iput v1, p0, Lepson/print/screen/PrintProgress;->EPS_JOB_CANCELED:I

    .line 122
    iput v0, p0, Lepson/print/screen/PrintProgress;->duplex:I

    .line 124
    iput-boolean v0, p0, Lepson/print/screen/PrintProgress;->isRemotePrinter:Z

    const-string v1, ""

    .line 125
    iput-object v1, p0, Lepson/print/screen/PrintProgress;->percentString:Ljava/lang/String;

    const/4 v1, 0x0

    .line 126
    iput-object v1, p0, Lepson/print/screen/PrintProgress;->context:Landroid/content/Context;

    .line 127
    iput-boolean v0, p0, Lepson/print/screen/PrintProgress;->bSearching:Z

    .line 135
    iput-boolean v0, p0, Lepson/print/screen/PrintProgress;->mError:Z

    .line 139
    iput-boolean v0, p0, Lepson/print/screen/PrintProgress;->mWaitEpsonServiceForFinish:Z

    const-string v1, "PERCENT"

    .line 635
    iput-object v1, p0, Lepson/print/screen/PrintProgress;->PERCENT:Ljava/lang/String;

    .line 636
    iput v0, p0, Lepson/print/screen/PrintProgress;->UPDATE_PERCENT:I

    const/4 v1, 0x1

    .line 637
    iput v1, p0, Lepson/print/screen/PrintProgress;->PRINT_NEXT_PAGE:I

    const/4 v2, 0x2

    .line 638
    iput v2, p0, Lepson/print/screen/PrintProgress;->START_PRINT:I

    const/4 v3, 0x3

    .line 639
    iput v3, p0, Lepson/print/screen/PrintProgress;->CONTINUE_PRINT:I

    const/4 v3, 0x4

    .line 640
    iput v3, p0, Lepson/print/screen/PrintProgress;->ERROR:I

    const/4 v3, 0x5

    .line 641
    iput v3, p0, Lepson/print/screen/PrintProgress;->FINISH:I

    const/4 v3, 0x6

    .line 642
    iput v3, p0, Lepson/print/screen/PrintProgress;->CHECK_PRINTER:I

    const/4 v3, 0x7

    .line 643
    iput v3, p0, Lepson/print/screen/PrintProgress;->CONFIRM:I

    const/16 v3, 0x8

    .line 644
    iput v3, p0, Lepson/print/screen/PrintProgress;->SHOW_COMPLETE_DIALOG:I

    const/16 v3, 0x9

    .line 645
    iput v3, p0, Lepson/print/screen/PrintProgress;->ECC_LIB_ERROR:I

    const/16 v3, 0xa

    .line 646
    iput v3, p0, Lepson/print/screen/PrintProgress;->CHECK_PRINTER_CONTINUE:I

    .line 651
    iput v1, p0, Lepson/print/screen/PrintProgress;->CONNECTED_SIMPLEAP:I

    .line 654
    iput v2, p0, Lepson/print/screen/PrintProgress;->CONFORM_MULTI:I

    .line 656
    iput v1, p0, Lepson/print/screen/PrintProgress;->curSheet:I

    .line 657
    iput v1, p0, Lepson/print/screen/PrintProgress;->curCopy:I

    .line 665
    new-instance v1, Landroid/os/Handler;

    new-instance v2, Lepson/print/screen/PrintProgress$4;

    invoke-direct {v2, p0}, Lepson/print/screen/PrintProgress$4;-><init>(Lepson/print/screen/PrintProgress;)V

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v1, p0, Lepson/print/screen/PrintProgress;->mHandler:Landroid/os/Handler;

    .line 968
    iput-boolean v0, p0, Lepson/print/screen/PrintProgress;->isDialogOpen:Z

    return-void
.end method

.method private CancelPrint()V
    .locals 1

    .line 1211
    :try_start_0
    sget-object v0, Lepson/print/screen/PrintProgress;->mEpsonService:Lepson/print/service/IEpsonService;

    if-eqz v0, :cond_0

    .line 1212
    sget-object v0, Lepson/print/screen/PrintProgress;->mEpsonService:Lepson/print/service/IEpsonService;

    invoke-interface {v0}, Lepson/print/service/IEpsonService;->cancelPrint()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 1216
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_0
    :goto_0
    const/4 v0, 0x1

    .line 1218
    invoke-virtual {p0, v0}, Lepson/print/screen/PrintProgress;->showDialog(I)V

    return-void
.end method

.method static synthetic access$000(Lepson/print/screen/PrintProgress;)Ljava/lang/String;
    .locals 0

    .line 84
    iget-object p0, p0, Lepson/print/screen/PrintProgress;->printerIp:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$002(Lepson/print/screen/PrintProgress;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 84
    iput-object p1, p0, Lepson/print/screen/PrintProgress;->printerIp:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$100()Ljava/lang/Object;
    .locals 1

    .line 84
    sget-object v0, Lepson/print/screen/PrintProgress;->mLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1000(Lepson/print/screen/PrintProgress;)V
    .locals 0

    .line 84
    invoke-direct {p0}, Lepson/print/screen/PrintProgress;->CancelPrint()V

    return-void
.end method

.method static synthetic access$1100(Lepson/print/screen/PrintProgress;)Landroid/content/Context;
    .locals 0

    .line 84
    iget-object p0, p0, Lepson/print/screen/PrintProgress;->context:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic access$1202(Lepson/print/screen/PrintProgress;I)I
    .locals 0

    .line 84
    iput p1, p0, Lepson/print/screen/PrintProgress;->curSheet:I

    return p1
.end method

.method static synthetic access$1302(Lepson/print/screen/PrintProgress;I)I
    .locals 0

    .line 84
    iput p1, p0, Lepson/print/screen/PrintProgress;->curCopy:I

    return p1
.end method

.method static synthetic access$1400(Lepson/print/screen/PrintProgress;I)V
    .locals 0

    .line 84
    invoke-direct {p0, p1}, Lepson/print/screen/PrintProgress;->updatePrintProgress(I)V

    return-void
.end method

.method static synthetic access$1500(Lepson/print/screen/PrintProgress;I)V
    .locals 0

    .line 84
    invoke-direct {p0, p1}, Lepson/print/screen/PrintProgress;->updateApfProgress(I)V

    return-void
.end method

.method static synthetic access$1600(Lepson/print/screen/PrintProgress;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 84
    invoke-direct {p0}, Lepson/print/screen/PrintProgress;->startPrint()V

    return-void
.end method

.method static synthetic access$1700(Lepson/print/screen/PrintProgress;)Ljava/lang/String;
    .locals 0

    .line 84
    iget-object p0, p0, Lepson/print/screen/PrintProgress;->percentString:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1800()Z
    .locals 1

    .line 84
    sget-boolean v0, Lepson/print/screen/PrintProgress;->bRestartactivity:Z

    return v0
.end method

.method static synthetic access$1900(Lepson/print/screen/PrintProgress;)Z
    .locals 0

    .line 84
    iget-boolean p0, p0, Lepson/print/screen/PrintProgress;->mWaitEpsonServiceForFinish:Z

    return p0
.end method

.method static synthetic access$1902(Lepson/print/screen/PrintProgress;Z)Z
    .locals 0

    .line 84
    iput-boolean p1, p0, Lepson/print/screen/PrintProgress;->mWaitEpsonServiceForFinish:Z

    return p1
.end method

.method static synthetic access$200(Lepson/print/screen/PrintProgress;)V
    .locals 0

    .line 84
    invoke-direct {p0}, Lepson/print/screen/PrintProgress;->triggerPrint()V

    return-void
.end method

.method static synthetic access$2000()Landroid/content/ServiceConnection;
    .locals 1

    .line 84
    sget-object v0, Lepson/print/screen/PrintProgress;->mEpsonConnection:Landroid/content/ServiceConnection;

    return-object v0
.end method

.method static synthetic access$2100(Lepson/print/screen/PrintProgress;)V
    .locals 0

    .line 84
    invoke-direct {p0}, Lepson/print/screen/PrintProgress;->sendPrintLog()V

    return-void
.end method

.method static synthetic access$2200(Lepson/print/screen/PrintProgress;)V
    .locals 0

    .line 84
    invoke-direct {p0}, Lepson/print/screen/PrintProgress;->setPrintEnd()V

    return-void
.end method

.method static synthetic access$2300()Ljava/lang/Object;
    .locals 1

    .line 84
    sget-object v0, Lepson/print/screen/PrintProgress;->lockBCancel:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$300(Lepson/print/screen/PrintProgress;)Z
    .locals 0

    .line 84
    iget-boolean p0, p0, Lepson/print/screen/PrintProgress;->mError:Z

    return p0
.end method

.method static synthetic access$302(Lepson/print/screen/PrintProgress;Z)Z
    .locals 0

    .line 84
    iput-boolean p1, p0, Lepson/print/screen/PrintProgress;->mError:Z

    return p1
.end method

.method static synthetic access$400(Lepson/print/screen/PrintProgress;)Z
    .locals 0

    .line 84
    iget-boolean p0, p0, Lepson/print/screen/PrintProgress;->isRemotePrinter:Z

    return p0
.end method

.method static synthetic access$500(Lepson/print/screen/PrintProgress;)Z
    .locals 0

    .line 84
    iget-boolean p0, p0, Lepson/print/screen/PrintProgress;->bSearchingPrinter:Z

    return p0
.end method

.method static synthetic access$502(Lepson/print/screen/PrintProgress;Z)Z
    .locals 0

    .line 84
    iput-boolean p1, p0, Lepson/print/screen/PrintProgress;->bSearchingPrinter:Z

    return p1
.end method

.method static synthetic access$600(Lepson/print/screen/PrintProgress;)Z
    .locals 0

    .line 84
    iget-boolean p0, p0, Lepson/print/screen/PrintProgress;->isDialogOpen:Z

    return p0
.end method

.method static synthetic access$602(Lepson/print/screen/PrintProgress;Z)Z
    .locals 0

    .line 84
    iput-boolean p1, p0, Lepson/print/screen/PrintProgress;->isDialogOpen:Z

    return p1
.end method

.method static synthetic access$700(Lepson/print/screen/PrintProgress;)Z
    .locals 0

    .line 84
    iget-boolean p0, p0, Lepson/print/screen/PrintProgress;->bSearching:Z

    return p0
.end method

.method static synthetic access$702(Lepson/print/screen/PrintProgress;Z)Z
    .locals 0

    .line 84
    iput-boolean p1, p0, Lepson/print/screen/PrintProgress;->bSearching:Z

    return p1
.end method

.method static synthetic access$800()Lepson/print/service/IEpsonService;
    .locals 1

    .line 84
    sget-object v0, Lepson/print/screen/PrintProgress;->mEpsonService:Lepson/print/service/IEpsonService;

    return-object v0
.end method

.method static synthetic access$802(Lepson/print/service/IEpsonService;)Lepson/print/service/IEpsonService;
    .locals 0

    .line 84
    sput-object p0, Lepson/print/screen/PrintProgress;->mEpsonService:Lepson/print/service/IEpsonService;

    return-object p0
.end method

.method static synthetic access$900()Lepson/print/service/IEpsonServiceCallback;
    .locals 1

    .line 84
    sget-object v0, Lepson/print/screen/PrintProgress;->mCallback:Lepson/print/service/IEpsonServiceCallback;

    return-object v0
.end method

.method private changeApfProgress()V
    .locals 2

    .line 567
    sget-object v0, Lepson/print/screen/PrintProgress;->mProgressPage:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 568
    iget-object v0, p0, Lepson/print/screen/PrintProgress;->mPage:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 570
    sget-object v0, Lepson/print/screen/PrintProgress;->mProgressCopies:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 571
    sget-object v0, Lepson/print/screen/PrintProgress;->mProgressCopies:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 573
    iget-object v0, p0, Lepson/print/screen/PrintProgress;->mCopies:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 575
    iget-object v0, p0, Lepson/print/screen/PrintProgress;->mPercent:Landroid/widget/TextView;

    const v1, 0x7f0e02aa

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 577
    invoke-virtual {p0, v1}, Lepson/print/screen/PrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepson/print/screen/PrintProgress;->percentString:Ljava/lang/String;

    return-void
.end method

.method private changePrintProgress()V
    .locals 3

    .line 543
    iget v0, p0, Lepson/print/screen/PrintProgress;->sheets:I

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-le v0, v1, :cond_0

    .line 544
    sget-object v0, Lepson/print/screen/PrintProgress;->mProgressPage:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 545
    iget-object v0, p0, Lepson/print/screen/PrintProgress;->mPage:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 548
    :cond_0
    sget v0, Lepson/print/screen/PrintProgress;->copies:I

    if-le v0, v1, :cond_1

    iget-boolean v0, p0, Lepson/print/screen/PrintProgress;->isRemotePrinter:Z

    if-nez v0, :cond_1

    .line 549
    sget-object v0, Lepson/print/screen/PrintProgress;->mProgressCopies:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 550
    iget-object v0, p0, Lepson/print/screen/PrintProgress;->mCopies:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 552
    :cond_1
    iget-object v0, p0, Lepson/print/screen/PrintProgress;->mCopies:Landroid/widget/TextView;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 555
    :goto_0
    iget-boolean v0, p0, Lepson/print/screen/PrintProgress;->isRemotePrinter:Z

    if-ne v0, v1, :cond_2

    const v0, 0x7f0e0361

    .line 556
    invoke-virtual {p0, v0}, Lepson/print/screen/PrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepson/print/screen/PrintProgress;->percentString:Ljava/lang/String;

    goto :goto_1

    :cond_2
    const v0, 0x7f0e043f

    .line 558
    invoke-virtual {p0, v0}, Lepson/print/screen/PrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepson/print/screen/PrintProgress;->percentString:Ljava/lang/String;

    :goto_1
    return-void
.end method

.method private static getParams(Landroid/content/Intent;)Lepson/print/screen/PrintProgress$ProgressParams;
    .locals 1

    const-string v0, "progress-params"

    .line 1298
    invoke-virtual {p0, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lepson/print/screen/PrintProgress$ProgressParams;

    if-eqz v0, :cond_0

    return-object v0

    .line 1303
    :cond_0
    new-instance v0, Lepson/print/screen/PrintProgressParams;

    invoke-direct {v0, p0}, Lepson/print/screen/PrintProgressParams;-><init>(Landroid/content/Intent;)V

    return-object v0
.end method

.method public static getPdfPrintIntent(Landroid/content/Context;Lepson/print/screen/PrintProgress$ProgressParams;)Landroid/content/Intent;
    .locals 2

    .line 1273
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lepson/print/screen/PrintProgress;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string p0, "progress-params"

    .line 1274
    invoke-virtual {v0, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    return-object v0
.end method

.method public static getPrintIntent(Landroid/content/Context;Lepson/print/EPImageList;ZZLcom/epson/iprint/prtlogger/PrintLog;)Landroid/content/Intent;
    .locals 2
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1    # Lepson/print/EPImageList;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p4    # Lcom/epson/iprint/prtlogger/PrintLog;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .line 1284
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lepson/print/screen/PrintProgress;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string p0, "EPImageList"

    .line 1285
    invoke-virtual {v0, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string p0, "PRINT_DOCUMENT"

    .line 1286
    invoke-virtual {v0, p0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string p0, "epson_color"

    .line 1287
    invoke-virtual {v0, p0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    if-eqz p4, :cond_0

    const-string p0, "print_log"

    .line 1289
    invoke-virtual {v0, p0, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    :cond_0
    return-object v0
.end method

.method private init()V
    .locals 3

    .line 144
    sget-object v0, Lepson/print/screen/PrintProgress;->mLockInit:Ljava/lang/Object;

    monitor-enter v0

    .line 145
    :try_start_0
    sget-object v1, Lepson/print/screen/PrintProgress;->mEpsonService:Lepson/print/service/IEpsonService;

    if-eqz v1, :cond_0

    .line 146
    monitor-exit v0

    return-void

    .line 148
    :cond_0
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 149
    sget-object v0, Lepson/print/screen/PrintProgress;->mEpsonService:Lepson/print/service/IEpsonService;

    if-nez v0, :cond_1

    const-string v0, "Epson"

    const-string v1, "mEpsonService = null"

    .line 150
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    new-instance v0, Lepson/print/screen/PrintProgress$1;

    invoke-direct {v0, p0}, Lepson/print/screen/PrintProgress$1;-><init>(Lepson/print/screen/PrintProgress;)V

    sput-object v0, Lepson/print/screen/PrintProgress;->mCallback:Lepson/print/service/IEpsonServiceCallback;

    .line 262
    new-instance v0, Lepson/print/screen/PrintProgress$2;

    invoke-direct {v0, p0}, Lepson/print/screen/PrintProgress$2;-><init>(Lepson/print/screen/PrintProgress;)V

    sput-object v0, Lepson/print/screen/PrintProgress;->mEpsonConnection:Landroid/content/ServiceConnection;

    const-string v0, "Epson"

    const-string v1, "bindService() call"

    .line 297
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lepson/print/service/EpsonService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v1, Lepson/print/screen/PrintProgress;->mEpsonConnection:Landroid/content/ServiceConnection;

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lepson/print/screen/PrintProgress;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    const-string v0, "Epson"

    const-string v1, "bindService() finish"

    .line 301
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void

    :catchall_0
    move-exception v1

    .line 148
    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public static isPrintSuccess(I)Z
    .locals 1

    const/4 v0, 0x4

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private sendPrintLog()V
    .locals 1

    .line 1262
    iget-object v0, p0, Lepson/print/screen/PrintProgress;->mProgressParams:Lepson/print/screen/PrintProgress$ProgressParams;

    invoke-static {p0, v0}, Lcom/epson/iprint/prtlogger/Analytics;->sendPrintLog(Landroid/content/Context;Lepson/print/screen/PrintProgress$ProgressParams;)V

    return-void
.end method

.method private setPrintEnd()V
    .locals 2

    .line 535
    invoke-virtual {p0}, Lepson/print/screen/PrintProgress;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lepson/print/IprintApplication;

    const/4 v1, 0x0

    .line 536
    invoke-virtual {v0, v1}, Lepson/print/IprintApplication;->setPrinting(Z)V

    return-void
.end method

.method private startPrint()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v0, 0x0

    .line 960
    iput-boolean v0, p0, Lepson/print/screen/PrintProgress;->bSearching:Z

    .line 962
    iget-object v0, p0, Lepson/print/screen/PrintProgress;->mProgressParams:Lepson/print/screen/PrintProgress$ProgressParams;

    sget-object v1, Lepson/print/screen/PrintProgress;->mEpsonService:Lepson/print/service/IEpsonService;

    sget-boolean v2, Lepson/print/screen/PrintProgress;->isBkRetry:Z

    invoke-interface {v0, v1, v2}, Lepson/print/screen/PrintProgress$ProgressParams;->print(Lepson/print/service/IEpsonService;Z)Z

    return-void
.end method

.method private triggerPrint()V
    .locals 2

    .line 488
    iget-object v0, p0, Lepson/print/screen/PrintProgress;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method private updateApfProgress(I)V
    .locals 3

    .line 624
    iget-boolean v0, p0, Lepson/print/screen/PrintProgress;->mApfProgressDisplay:Z

    if-nez v0, :cond_0

    .line 625
    invoke-direct {p0}, Lepson/print/screen/PrintProgress;->changeApfProgress()V

    const/4 v0, 0x1

    .line 626
    iput-boolean v0, p0, Lepson/print/screen/PrintProgress;->mApfProgressDisplay:Z

    .line 629
    :cond_0
    sget-object v0, Lepson/print/screen/PrintProgress;->mProgressPercent:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 630
    iget-object v0, p0, Lepson/print/screen/PrintProgress;->mPercent:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lepson/print/screen/PrintProgress;->percentString:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "         "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, "%"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private updatePrintProgress(I)V
    .locals 4

    .line 585
    iget-boolean v0, p0, Lepson/print/screen/PrintProgress;->mPrintProgressDisplay:Z

    const/4 v1, 0x1

    if-nez v0, :cond_0

    .line 586
    invoke-direct {p0}, Lepson/print/screen/PrintProgress;->changePrintProgress()V

    .line 587
    iput-boolean v1, p0, Lepson/print/screen/PrintProgress;->mPrintProgressDisplay:Z

    :cond_0
    const/16 v0, 0x64

    if-lt p1, v0, :cond_1

    const/16 p1, 0x64

    :cond_1
    if-gtz p1, :cond_4

    .line 595
    sget-object p1, Lepson/print/screen/PrintProgress;->mProgressPercent:Landroid/widget/ProgressBar;

    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 596
    iget-object p1, p0, Lepson/print/screen/PrintProgress;->mPercent:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lepson/print/screen/PrintProgress;->percentString:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "         0%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 598
    iget p1, p0, Lepson/print/screen/PrintProgress;->sheets:I

    if-le p1, v1, :cond_2

    .line 599
    iget-object p1, p0, Lepson/print/screen/PrintProgress;->mPage:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f0e0403

    invoke-virtual {p0, v3}, Lepson/print/screen/PrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lepson/print/screen/PrintProgress;->curSheet:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lepson/print/screen/PrintProgress;->sheets:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 600
    sget-object p1, Lepson/print/screen/PrintProgress;->mProgressPage:Landroid/widget/ProgressBar;

    iget v2, p0, Lepson/print/screen/PrintProgress;->curSheet:I

    mul-int/lit8 v2, v2, 0x64

    iget v3, p0, Lepson/print/screen/PrintProgress;->sheets:I

    div-int/2addr v2, v3

    invoke-virtual {p1, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 603
    :cond_2
    sget p1, Lepson/print/screen/PrintProgress;->copies:I

    if-le p1, v1, :cond_3

    .line 604
    iget-object p1, p0, Lepson/print/screen/PrintProgress;->mCopies:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f0e030c

    invoke-virtual {p0, v3}, Lepson/print/screen/PrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lepson/print/screen/PrintProgress;->curCopy:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v3, Lepson/print/screen/PrintProgress;->copies:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 605
    sget-object p1, Lepson/print/screen/PrintProgress;->mProgressCopies:Landroid/widget/ProgressBar;

    iget v2, p0, Lepson/print/screen/PrintProgress;->curCopy:I

    mul-int/lit8 v2, v2, 0x64

    sget v0, Lepson/print/screen/PrintProgress;->copies:I

    div-int/2addr v2, v0

    invoke-virtual {p1, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 608
    :cond_3
    iget p1, p0, Lepson/print/screen/PrintProgress;->curSheet:I

    add-int/2addr p1, v1

    iput p1, p0, Lepson/print/screen/PrintProgress;->curSheet:I

    .line 609
    iget p1, p0, Lepson/print/screen/PrintProgress;->curSheet:I

    iget v0, p0, Lepson/print/screen/PrintProgress;->sheets:I

    if-le p1, v0, :cond_5

    .line 610
    iput v1, p0, Lepson/print/screen/PrintProgress;->curSheet:I

    .line 611
    iget p1, p0, Lepson/print/screen/PrintProgress;->curCopy:I

    add-int/2addr p1, v1

    iput p1, p0, Lepson/print/screen/PrintProgress;->curCopy:I

    goto :goto_0

    .line 614
    :cond_4
    sget-object v0, Lepson/print/screen/PrintProgress;->mProgressPercent:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 615
    iget-object v0, p0, Lepson/print/screen/PrintProgress;->mPercent:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lepson/print/screen/PrintProgress;->percentString:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "         "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, "%"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_5
    :goto_0
    return-void
.end method


# virtual methods
.method countSheet(II)I
    .locals 1

    const/high16 v0, 0x10000

    if-eq p1, v0, :cond_2

    const/high16 v0, 0x20000

    if-eq p1, v0, :cond_0

    const/high16 v0, 0x40000

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 470
    :cond_0
    div-int/lit8 p1, p2, 0x4

    .line 471
    rem-int/lit8 p2, p2, 0x4

    if-lez p2, :cond_1

    add-int/lit8 p2, p1, 0x1

    goto :goto_0

    :cond_1
    move p2, p1

    goto :goto_0

    .line 463
    :cond_2
    div-int/lit8 p1, p2, 0x2

    .line 464
    rem-int/lit8 p2, p2, 0x2

    if-lez p2, :cond_3

    add-int/lit8 p2, p1, 0x1

    goto :goto_0

    :cond_3
    move p2, p1

    :goto_0
    return p2
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    .line 1174
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    const/4 p3, 0x5

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/4 p1, -0x1

    if-eq p2, p1, :cond_0

    const/4 p1, 0x1

    .line 1197
    iput-boolean p1, p0, Lepson/print/screen/PrintProgress;->mCanceled:Z

    .line 1198
    iget-object p1, p0, Lepson/print/screen/PrintProgress;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, p3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 1194
    :cond_0
    iget-object p1, p0, Lepson/print/screen/PrintProgress;->mHandler:Landroid/os/Handler;

    const/4 p2, 0x6

    invoke-virtual {p1, p2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :pswitch_1
    packed-switch p2, :pswitch_data_1

    goto :goto_0

    .line 1185
    :pswitch_2
    iget-object p1, p0, Lepson/print/screen/PrintProgress;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, p3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 1181
    :pswitch_3
    iget-object p1, p0, Lepson/print/screen/PrintProgress;->mHandler:Landroid/os/Handler;

    const/16 p2, 0xa

    invoke-virtual {p1, p2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch -0x1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 2

    const-string v0, "Epson"

    const-string v1, "onBackPressed()call"

    .line 1226
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    const-string v0, "Epson"

    const-string v1, "PrintProgress.java: onCreate() call"

    .line 309
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "Epson"

    const-string v1, "addFlags : FLAG_KEEP_SCREEN_ON "

    .line 311
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    invoke-virtual {p0}, Lepson/print/screen/PrintProgress;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 314
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 316
    invoke-virtual {p0}, Lepson/print/screen/PrintProgress;->getApplication()Landroid/app/Application;

    move-result-object p1

    check-cast p1, Lepson/print/IprintApplication;

    const/4 v0, 0x1

    .line 317
    invoke-virtual {p1, v0}, Lepson/print/IprintApplication;->setPrinting(Z)V

    .line 320
    invoke-static {p0}, Lcom/epson/iprint/prtlogger/Analytics;->cleanPrintNumber(Landroid/content/Context;)V

    .line 323
    :try_start_0
    invoke-virtual {p0}, Lepson/print/screen/PrintProgress;->getIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-static {p1}, Lepson/print/screen/PrintProgress;->getParams(Landroid/content/Intent;)Lepson/print/screen/PrintProgress$ProgressParams;

    move-result-object p1

    iput-object p1, p0, Lepson/print/screen/PrintProgress;->mProgressParams:Lepson/print/screen/PrintProgress$ProgressParams;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p1, 0x0

    .line 333
    invoke-static {p0, p1}, Lepson/common/Utils;->setFInishOnTOuchOutside(Landroid/app/Activity;Z)V

    .line 334
    iput-boolean p1, p0, Lepson/print/screen/PrintProgress;->mError:Z

    .line 335
    sput p1, Lepson/print/screen/PrintProgress;->curError:I

    .line 336
    iput-boolean p1, p0, Lepson/print/screen/PrintProgress;->mWaitEpsonServiceForFinish:Z

    .line 338
    sget-object v1, Lepson/print/screen/PrintProgress;->mLockInit:Ljava/lang/Object;

    monitor-enter v1

    :try_start_1
    const-string v2, "Epson"

    const-string v3, "PrintProgress.java: onCreate() call init() funciton"

    .line 339
    invoke-static {v2, v3}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    invoke-direct {p0}, Lepson/print/screen/PrintProgress;->init()V

    .line 341
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 343
    invoke-virtual {p0}, Lepson/print/screen/PrintProgress;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "PRINT_DOCUMENT"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, Lepson/print/screen/PrintProgress;->isDocument:Z

    .line 345
    iget-object v1, p0, Lepson/print/screen/PrintProgress;->mProgressParams:Lepson/print/screen/PrintProgress$ProgressParams;

    invoke-interface {v1, p0}, Lepson/print/screen/PrintProgress$ProgressParams;->getPrintSetting(Landroid/content/Context;)Lepson/print/screen/PrintSetting;

    move-result-object v1

    .line 347
    invoke-virtual {v1}, Lepson/print/screen/PrintSetting;->loadSettings()V

    .line 348
    iget v2, v1, Lepson/print/screen/PrintSetting;->copiesValue:I

    sput v2, Lepson/print/screen/PrintProgress;->copies:I

    .line 349
    iget v2, v1, Lepson/print/screen/PrintSetting;->duplexValue:I

    iput v2, p0, Lepson/print/screen/PrintProgress;->duplex:I

    .line 351
    iget v2, v1, Lepson/print/screen/PrintSetting;->layoutMultiPageValue:I

    iget-object v3, p0, Lepson/print/screen/PrintProgress;->mProgressParams:Lepson/print/screen/PrintProgress$ProgressParams;

    invoke-interface {v3}, Lepson/print/screen/PrintProgress$ProgressParams;->getOriginalSheetSize()I

    move-result v3

    invoke-virtual {p0, v2, v3}, Lepson/print/screen/PrintProgress;->countSheet(II)I

    move-result v2

    iput v2, p0, Lepson/print/screen/PrintProgress;->sheets:I

    .line 353
    invoke-static {p0}, Lepson/print/MyPrinter;->isRemotePrinter(Landroid/content/Context;)Z

    move-result v2

    iput-boolean v2, p0, Lepson/print/screen/PrintProgress;->isRemotePrinter:Z

    const v2, 0x7f0a00af

    .line 355
    invoke-virtual {p0, v2}, Lepson/print/screen/PrintProgress;->setContentView(I)V

    .line 356
    iput-object p0, p0, Lepson/print/screen/PrintProgress;->context:Landroid/content/Context;

    const v2, 0x7f08029d

    .line 358
    invoke-virtual {p0, v2}, Lepson/print/screen/PrintProgress;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    sput-object v2, Lepson/print/screen/PrintProgress;->mProgressPercent:Landroid/widget/ProgressBar;

    const v2, 0x7f08025b

    .line 359
    invoke-virtual {p0, v2}, Lepson/print/screen/PrintProgress;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lepson/print/screen/PrintProgress;->mPercent:Landroid/widget/TextView;

    const v2, 0x7f08029a

    .line 360
    invoke-virtual {p0, v2}, Lepson/print/screen/PrintProgress;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    sput-object v2, Lepson/print/screen/PrintProgress;->mProgressCopies:Landroid/widget/ProgressBar;

    const v2, 0x7f0800e0

    .line 361
    invoke-virtual {p0, v2}, Lepson/print/screen/PrintProgress;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lepson/print/screen/PrintProgress;->mCopies:Landroid/widget/TextView;

    const v2, 0x7f08023f

    .line 362
    invoke-virtual {p0, v2}, Lepson/print/screen/PrintProgress;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lepson/print/screen/PrintProgress;->mPage:Landroid/widget/TextView;

    const v2, 0x7f08029c

    .line 363
    invoke-virtual {p0, v2}, Lepson/print/screen/PrintProgress;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    sput-object v2, Lepson/print/screen/PrintProgress;->mProgressPage:Landroid/widget/ProgressBar;

    .line 365
    iget v2, p0, Lepson/print/screen/PrintProgress;->sheets:I

    const/4 v3, 0x4

    if-le v2, v0, :cond_0

    .line 366
    iget-object v2, p0, Lepson/print/screen/PrintProgress;->mPage:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const v5, 0x7f0e0403

    invoke-virtual {p0, v5}, Lepson/print/screen/PrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v5, p0, Lepson/print/screen/PrintProgress;->curSheet:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v5, p0, Lepson/print/screen/PrintProgress;->sheets:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 367
    sget-object v2, Lepson/print/screen/PrintProgress;->mProgressPage:Landroid/widget/ProgressBar;

    iget v4, p0, Lepson/print/screen/PrintProgress;->curSheet:I

    mul-int/lit8 v4, v4, 0x64

    iget v5, p0, Lepson/print/screen/PrintProgress;->sheets:I

    div-int/2addr v4, v5

    invoke-virtual {v2, v4}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto :goto_0

    .line 369
    :cond_0
    sget-object v2, Lepson/print/screen/PrintProgress;->mProgressPage:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 370
    iget-object v2, p0, Lepson/print/screen/PrintProgress;->mPage:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 373
    :goto_0
    sget v2, Lepson/print/screen/PrintProgress;->copies:I

    if-le v2, v0, :cond_1

    iget-boolean v2, p0, Lepson/print/screen/PrintProgress;->isRemotePrinter:Z

    if-nez v2, :cond_1

    .line 374
    iget-object v2, p0, Lepson/print/screen/PrintProgress;->mCopies:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const v4, 0x7f0e030c

    invoke-virtual {p0, v4}, Lepson/print/screen/PrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v4, p0, Lepson/print/screen/PrintProgress;->curCopy:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v4, Lepson/print/screen/PrintProgress;->copies:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 375
    sget-object v2, Lepson/print/screen/PrintProgress;->mProgressCopies:Landroid/widget/ProgressBar;

    iget v3, p0, Lepson/print/screen/PrintProgress;->curCopy:I

    mul-int/lit8 v3, v3, 0x64

    sget v4, Lepson/print/screen/PrintProgress;->copies:I

    div-int/2addr v3, v4

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto :goto_1

    .line 377
    :cond_1
    sget-object v2, Lepson/print/screen/PrintProgress;->mProgressCopies:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 378
    iget-object v2, p0, Lepson/print/screen/PrintProgress;->mCopies:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 379
    sput v0, Lepson/print/screen/PrintProgress;->copies:I

    :goto_1
    const v2, 0x7f0800b3

    .line 382
    invoke-virtual {p0, v2}, Lepson/print/screen/PrintProgress;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lepson/print/screen/PrintProgress;->mCancelButton:Landroid/widget/Button;

    .line 383
    iget-object v2, p0, Lepson/print/screen/PrintProgress;->mCancelButton:Landroid/widget/Button;

    new-instance v3, Lepson/print/screen/PrintProgress$3;

    invoke-direct {v3, p0}, Lepson/print/screen/PrintProgress$3;-><init>(Lepson/print/screen/PrintProgress;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 391
    iput-boolean p1, p0, Lepson/print/screen/PrintProgress;->bSearching:Z

    .line 394
    iget-boolean v2, p0, Lepson/print/screen/PrintProgress;->isRemotePrinter:Z

    if-ne v2, v0, :cond_2

    const v2, 0x7f0e0361

    .line 395
    invoke-virtual {p0, v2}, Lepson/print/screen/PrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lepson/print/screen/PrintProgress;->percentString:Ljava/lang/String;

    goto :goto_2

    :cond_2
    const v2, 0x7f0e043f

    .line 397
    invoke-virtual {p0, v2}, Lepson/print/screen/PrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lepson/print/screen/PrintProgress;->percentString:Ljava/lang/String;

    .line 399
    :goto_2
    iget-object v2, p0, Lepson/print/screen/PrintProgress;->mPercent:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lepson/print/screen/PrintProgress;->percentString:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "         0%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 400
    sget-object v2, Lepson/print/screen/PrintProgress;->mProgressPercent:Landroid/widget/ProgressBar;

    invoke-virtual {v2, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    const/16 v2, 0x8

    .line 405
    iget-object v3, p0, Lepson/print/screen/PrintProgress;->mProgressParams:Lepson/print/screen/PrintProgress$ProgressParams;

    invoke-interface {v3}, Lepson/print/screen/PrintProgress$ProgressParams;->getEpsonColorMode()Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_3

    :cond_3
    const/16 p1, 0x8

    :goto_3
    const v2, 0x7f080129

    .line 408
    invoke-virtual {p0, v2}, Lepson/print/screen/PrintProgress;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/view/View;->setVisibility(I)V

    .line 410
    iget-boolean p1, p0, Lepson/print/screen/PrintProgress;->isRemotePrinter:Z

    const/4 v2, 0x2

    if-ne p1, v0, :cond_4

    .line 411
    iget-object p1, p0, Lepson/print/screen/PrintProgress;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_7

    .line 417
    :cond_4
    new-instance p1, Landroid/content/Intent;

    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    .line 418
    const-class v0, Lepson/print/screen/ConfirmMultiScr;

    invoke-virtual {p1, p0, v0}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 419
    iget-object v0, p0, Lepson/print/screen/PrintProgress;->mProgressParams:Lepson/print/screen/PrintProgress$ProgressParams;

    invoke-interface {v0}, Lepson/print/screen/PrintProgress$ProgressParams;->isPaperLandscape()Z

    move-result v0

    .line 420
    iget v1, v1, Lepson/print/screen/PrintSetting;->layoutMultiPageValue:I

    const/high16 v3, 0x10000

    if-eq v1, v3, :cond_9

    const/high16 v3, 0x20000

    if-eq v1, v3, :cond_7

    const/high16 v3, 0x40000

    if-eq v1, v3, :cond_5

    .line 449
    iget-object p1, p0, Lepson/print/screen/PrintProgress;->mHandler:Landroid/os/Handler;

    const/4 v0, 0x6

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_7

    :cond_5
    if-eqz v0, :cond_6

    .line 441
    sget-object v0, Lepson/print/screen/ConfirmMultiScr;->EXTRA_DRAWABLE_ID:Ljava/lang/String;

    const v1, 0x7f0700ec

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_4

    .line 443
    :cond_6
    sget-object v0, Lepson/print/screen/ConfirmMultiScr;->EXTRA_DRAWABLE_ID:Ljava/lang/String;

    const v1, 0x7f0700ee

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 445
    :goto_4
    invoke-virtual {p0, p1, v2}, Lepson/print/screen/PrintProgress;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_7

    :cond_7
    if-eqz v0, :cond_8

    .line 432
    sget-object v0, Lepson/print/screen/ConfirmMultiScr;->EXTRA_DRAWABLE_ID:Ljava/lang/String;

    const v1, 0x7f0700ed

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_5

    .line 434
    :cond_8
    sget-object v0, Lepson/print/screen/ConfirmMultiScr;->EXTRA_DRAWABLE_ID:Ljava/lang/String;

    const v1, 0x7f0700ef

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 436
    :goto_5
    invoke-virtual {p0, p1, v2}, Lepson/print/screen/PrintProgress;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_7

    :cond_9
    if-eqz v0, :cond_a

    .line 423
    sget-object v0, Lepson/print/screen/ConfirmMultiScr;->EXTRA_DRAWABLE_ID:Ljava/lang/String;

    const v1, 0x7f0700eb

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_6

    .line 425
    :cond_a
    sget-object v0, Lepson/print/screen/ConfirmMultiScr;->EXTRA_DRAWABLE_ID:Ljava/lang/String;

    const v1, 0x7f0700ea

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 427
    :goto_6
    invoke-virtual {p0, p1, v2}, Lepson/print/screen/PrintProgress;->startActivityForResult(Landroid/content/Intent;I)V

    :goto_7
    const-string p1, "Epson"

    const-string v0, "printProgress.java: onCreate() finish"

    .line 454
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :catchall_0
    move-exception p1

    .line 341
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw p1

    .line 329
    :catch_0
    invoke-virtual {p0}, Lepson/print/screen/PrintProgress;->finish()V

    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 5

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x1

    packed-switch p1, :pswitch_data_0

    return-object v0

    .line 976
    :pswitch_0
    iget-boolean p1, p0, Lepson/print/screen/PrintProgress;->isDialogOpen:Z

    if-ne p1, v2, :cond_0

    return-object v0

    .line 980
    :cond_0
    new-instance p1, Landroid/app/AlertDialog$Builder;

    invoke-direct {p1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 981
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 982
    invoke-virtual {p1, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const v0, 0x7f0e04de

    .line 983
    invoke-virtual {p0, v0}, Lepson/print/screen/PrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const v0, 0x7f0e052b

    .line 984
    invoke-virtual {p0, v0}, Lepson/print/screen/PrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lepson/print/screen/PrintProgress$6;

    invoke-direct {v1, p0}, Lepson/print/screen/PrintProgress$6;-><init>(Lepson/print/screen/PrintProgress;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const v0, 0x7f0e04e6

    .line 1020
    invoke-virtual {p0, v0}, Lepson/print/screen/PrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lepson/print/screen/PrintProgress$5;

    invoke-direct {v1, p0}, Lepson/print/screen/PrintProgress$5;-><init>(Lepson/print/screen/PrintProgress;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 1029
    new-instance v0, Lepson/print/screen/PrintProgress$7;

    invoke-direct {v0, p0}, Lepson/print/screen/PrintProgress$7;-><init>(Lepson/print/screen/PrintProgress;)V

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 1040
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1

    :pswitch_1
    const-string p1, "Epson"

    .line 1043
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "show dialog: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v3, Lepson/print/screen/PrintProgress;->curError:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, " cancontinue: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-boolean v3, Lepson/print/screen/PrintProgress;->isContinue:Z

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1044
    invoke-virtual {p0, v2}, Lepson/print/screen/PrintProgress;->removeDialog(I)V

    .line 1045
    sget p1, Lepson/print/screen/PrintProgress;->curError:I

    invoke-static {p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$ErrorTable;->getStringId(I)[Ljava/lang/Integer;

    move-result-object p1

    if-nez p1, :cond_1

    const/4 p1, 0x3

    .line 1047
    new-array p1, p1, [Ljava/lang/Integer;

    const v0, 0x7f0e023a

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p1, v1

    const v0, 0x7f0e023b

    .line 1048
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p1, v2

    const/4 v0, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, p1, v0

    .line 1056
    :cond_1
    sget v0, Lepson/print/screen/PrintProgress;->curError:I

    invoke-virtual {p0}, Lepson/print/screen/PrintProgress;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {p1, v0, v3}, Lepson/common/Utils;->replaceMessage([Ljava/lang/Integer;ILandroid/content/Context;)[Ljava/lang/String;

    move-result-object p1

    .line 1059
    sget-boolean v0, Lepson/print/screen/PrintProgress;->isContinue:Z

    const v3, 0x7f0e0476

    if-nez v0, :cond_2

    const-string v0, "Epson"

    const-string v4, "show str_cancel button"

    .line 1060
    invoke-static {v0, v4}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1061
    new-instance v0, Lepson/print/widgets/CustomTitleAlertDialogBuilder;

    invoke-direct {v0, p0}, Lepson/print/widgets/CustomTitleAlertDialogBuilder;-><init>(Landroid/content/Context;)V

    .line 1062
    invoke-virtual {v0, v1}, Lepson/print/widgets/CustomTitleAlertDialogBuilder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    aget-object v2, p1, v2

    .line 1063
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    aget-object p1, p1, v1

    .line 1064
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 1065
    invoke-virtual {p0, v3}, Lepson/print/screen/PrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lepson/print/screen/PrintProgress$8;

    invoke-direct {v1, p0}, Lepson/print/screen/PrintProgress$8;-><init>(Lepson/print/screen/PrintProgress;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 1085
    new-instance v0, Lepson/print/screen/PrintProgress$9;

    invoke-direct {v0, p0}, Lepson/print/screen/PrintProgress$9;-><init>(Lepson/print/screen/PrintProgress;)V

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 1095
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1

    .line 1097
    :cond_2
    new-instance v0, Lepson/print/widgets/CustomTitleAlertDialogBuilder;

    invoke-direct {v0, p0}, Lepson/print/widgets/CustomTitleAlertDialogBuilder;-><init>(Landroid/content/Context;)V

    aget-object v2, p1, v2

    .line 1098
    invoke-virtual {v0, v2}, Lepson/print/widgets/CustomTitleAlertDialogBuilder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    aget-object p1, p1, v1

    .line 1099
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 1100
    invoke-virtual {p1, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const v0, 0x7f0e0482

    .line 1101
    invoke-virtual {p0, v0}, Lepson/print/screen/PrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lepson/print/screen/PrintProgress$11;

    invoke-direct {v1, p0}, Lepson/print/screen/PrintProgress$11;-><init>(Lepson/print/screen/PrintProgress;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 1124
    invoke-virtual {p0, v3}, Lepson/print/screen/PrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lepson/print/screen/PrintProgress$10;

    invoke-direct {v1, p0}, Lepson/print/screen/PrintProgress$10;-><init>(Lepson/print/screen/PrintProgress;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 1153
    new-instance v0, Lepson/print/screen/PrintProgress$12;

    invoke-direct {v0, p0}, Lepson/print/screen/PrintProgress$12;-><init>(Lepson/print/screen/PrintProgress;)V

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 1164
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 2

    .line 493
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    const-string v0, "Epson"

    const-string v1, "PrintProgress.java: onDestroy()call"

    .line 494
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "Epson"

    const-string v1, "clearFlags : FLAG_KEEP_SCREEN_ON "

    .line 496
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 497
    invoke-virtual {p0}, Lepson/print/screen/PrintProgress;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 499
    iget-object v0, p0, Lepson/print/screen/PrintProgress;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 500
    iget-object v0, p0, Lepson/print/screen/PrintProgress;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 501
    iget-object v0, p0, Lepson/print/screen/PrintProgress;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 502
    iget-object v0, p0, Lepson/print/screen/PrintProgress;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    const-string v0, "Epson"

    const-string v1, "PrintProgress.java: onDestroy() finish"

    .line 504
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2

    const-string v0, "Epson"

    const-string v1, "onKeyDown() call"

    .line 1233
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1234
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result p2

    if-nez p2, :cond_0

    const/16 p2, 0x54

    if-ne p1, p2, :cond_0

    const-string p1, "Epson"

    const-string p2, "onKeyDown() KEYCODE_SEARCH key press"

    .line 1236
    invoke-static {p1, p2}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method protected onPause()V
    .locals 0

    .line 528
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 530
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils;->disableForegroundDispatch(Landroid/app/Activity;)V

    return-void
.end method

.method protected onResume()V
    .locals 2

    .line 519
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const-string v0, "Epson"

    const-string v1, "PrintProgress.java call onResume()"

    .line 520
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 523
    move-object v1, v0

    check-cast v1, [[Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils;->enableForegroundDispatch(Landroid/app/Activity;[Landroid/content/IntentFilter;[[Ljava/lang/String;)V

    return-void
.end method

.method protected onStop()V
    .locals 2

    .line 509
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 511
    sget-object v0, Lepson/print/screen/PrintProgress;->mEpsonService:Lepson/print/service/IEpsonService;

    if-nez v0, :cond_0

    const-string v0, "Epson"

    const-string v1, "set bRestartactivity = true (2)"

    .line 512
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    return-void
.end method

.method public showErrorDialog(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 1245
    new-instance v0, Lepson/print/widgets/CustomTitleAlertDialogBuilder;

    invoke-direct {v0, p0}, Lepson/print/widgets/CustomTitleAlertDialogBuilder;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    .line 1246
    invoke-virtual {v0, v1}, Lepson/print/widgets/CustomTitleAlertDialogBuilder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 1247
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 1248
    invoke-virtual {p1, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const p2, 0x7f0e03fe

    .line 1249
    invoke-virtual {p0, p2}, Lepson/print/screen/PrintProgress;->getString(I)Ljava/lang/String;

    move-result-object p2

    new-instance v0, Lepson/print/screen/PrintProgress$13;

    invoke-direct {v0, p0}, Lepson/print/screen/PrintProgress$13;-><init>(Lepson/print/screen/PrintProgress;)V

    invoke-virtual {p1, p2, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 1257
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.class Lepson/print/screen/PageRangeSetting$1;
.super Ljava/lang/Object;
.source "PageRangeSetting.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/screen/PageRangeSetting;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/screen/PageRangeSetting;


# direct methods
.method constructor <init>(Lepson/print/screen/PageRangeSetting;)V
    .locals 0

    .line 124
    iput-object p1, p0, Lepson/print/screen/PageRangeSetting$1;->this$0:Lepson/print/screen/PageRangeSetting;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .line 127
    iget-object p1, p0, Lepson/print/screen/PageRangeSetting$1;->this$0:Lepson/print/screen/PageRangeSetting;

    iget-object p1, p1, Lepson/print/screen/PageRangeSetting;->btnPrintAll:Landroid/widget/Switch;

    invoke-virtual {p1}, Landroid/widget/Switch;->isChecked()Z

    move-result p1

    const/4 v0, 0x1

    if-eqz p1, :cond_0

    .line 128
    iget-object p1, p0, Lepson/print/screen/PageRangeSetting$1;->this$0:Lepson/print/screen/PageRangeSetting;

    iget-object p1, p1, Lepson/print/screen/PageRangeSetting;->startPagePlus:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 129
    iget-object p1, p0, Lepson/print/screen/PageRangeSetting$1;->this$0:Lepson/print/screen/PageRangeSetting;

    iget-object p1, p1, Lepson/print/screen/PageRangeSetting;->startPageMinus:Landroid/widget/Button;

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 130
    iget-object p1, p0, Lepson/print/screen/PageRangeSetting$1;->this$0:Lepson/print/screen/PageRangeSetting;

    iget-object p1, p1, Lepson/print/screen/PageRangeSetting;->endPagePlus:Landroid/widget/Button;

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 131
    iget-object p1, p0, Lepson/print/screen/PageRangeSetting$1;->this$0:Lepson/print/screen/PageRangeSetting;

    iget-object p1, p1, Lepson/print/screen/PageRangeSetting;->endPageMinus:Landroid/widget/Button;

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 132
    iget-object p1, p0, Lepson/print/screen/PageRangeSetting$1;->this$0:Lepson/print/screen/PageRangeSetting;

    iput v0, p1, Lepson/print/screen/PageRangeSetting;->start:I

    .line 133
    iget v1, p1, Lepson/print/screen/PageRangeSetting;->sheets:I

    iput v1, p1, Lepson/print/screen/PageRangeSetting;->end:I

    .line 134
    iget-object p1, p0, Lepson/print/screen/PageRangeSetting$1;->this$0:Lepson/print/screen/PageRangeSetting;

    iget-object p1, p1, Lepson/print/screen/PageRangeSetting;->startPage:Landroid/widget/TextView;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 135
    iget-object p1, p0, Lepson/print/screen/PageRangeSetting$1;->this$0:Lepson/print/screen/PageRangeSetting;

    iget-object p1, p1, Lepson/print/screen/PageRangeSetting;->endPage:Landroid/widget/TextView;

    iget-object v0, p0, Lepson/print/screen/PageRangeSetting$1;->this$0:Lepson/print/screen/PageRangeSetting;

    iget v0, v0, Lepson/print/screen/PageRangeSetting;->sheets:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 137
    :cond_0
    iget-object p1, p0, Lepson/print/screen/PageRangeSetting$1;->this$0:Lepson/print/screen/PageRangeSetting;

    iget-object p1, p1, Lepson/print/screen/PageRangeSetting;->startPagePlus:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 138
    iget-object p1, p0, Lepson/print/screen/PageRangeSetting$1;->this$0:Lepson/print/screen/PageRangeSetting;

    iget-object p1, p1, Lepson/print/screen/PageRangeSetting;->startPageMinus:Landroid/widget/Button;

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 139
    iget-object p1, p0, Lepson/print/screen/PageRangeSetting$1;->this$0:Lepson/print/screen/PageRangeSetting;

    iget p1, p1, Lepson/print/screen/PageRangeSetting;->start:I

    if-ne p1, v0, :cond_1

    .line 141
    iget-object p1, p0, Lepson/print/screen/PageRangeSetting$1;->this$0:Lepson/print/screen/PageRangeSetting;

    iget-object p1, p1, Lepson/print/screen/PageRangeSetting;->startPageMinus:Landroid/widget/Button;

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 142
    iget-object p1, p0, Lepson/print/screen/PageRangeSetting$1;->this$0:Lepson/print/screen/PageRangeSetting;

    iget p1, p1, Lepson/print/screen/PageRangeSetting;->start:I

    iget-object v2, p0, Lepson/print/screen/PageRangeSetting$1;->this$0:Lepson/print/screen/PageRangeSetting;

    iget v2, v2, Lepson/print/screen/PageRangeSetting;->end:I

    if-ge p1, v2, :cond_1

    .line 144
    iget-object p1, p0, Lepson/print/screen/PageRangeSetting$1;->this$0:Lepson/print/screen/PageRangeSetting;

    iget-object p1, p1, Lepson/print/screen/PageRangeSetting;->startPagePlus:Landroid/widget/Button;

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 148
    :cond_1
    iget-object p1, p0, Lepson/print/screen/PageRangeSetting$1;->this$0:Lepson/print/screen/PageRangeSetting;

    iget-object p1, p1, Lepson/print/screen/PageRangeSetting;->endPagePlus:Landroid/widget/Button;

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 149
    iget-object p1, p0, Lepson/print/screen/PageRangeSetting$1;->this$0:Lepson/print/screen/PageRangeSetting;

    iget-object p1, p1, Lepson/print/screen/PageRangeSetting;->endPageMinus:Landroid/widget/Button;

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 150
    iget-object p1, p0, Lepson/print/screen/PageRangeSetting$1;->this$0:Lepson/print/screen/PageRangeSetting;

    iget-object p1, p1, Lepson/print/screen/PageRangeSetting;->startPage:Landroid/widget/TextView;

    iget-object v2, p0, Lepson/print/screen/PageRangeSetting$1;->this$0:Lepson/print/screen/PageRangeSetting;

    iget v2, v2, Lepson/print/screen/PageRangeSetting;->start:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 151
    iget-object p1, p0, Lepson/print/screen/PageRangeSetting$1;->this$0:Lepson/print/screen/PageRangeSetting;

    iget-object p1, p1, Lepson/print/screen/PageRangeSetting;->endPage:Landroid/widget/TextView;

    iget-object v2, p0, Lepson/print/screen/PageRangeSetting$1;->this$0:Lepson/print/screen/PageRangeSetting;

    iget v2, v2, Lepson/print/screen/PageRangeSetting;->end:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 152
    iget-object p1, p0, Lepson/print/screen/PageRangeSetting$1;->this$0:Lepson/print/screen/PageRangeSetting;

    iget p1, p1, Lepson/print/screen/PageRangeSetting;->end:I

    iget-object v2, p0, Lepson/print/screen/PageRangeSetting$1;->this$0:Lepson/print/screen/PageRangeSetting;

    iget v2, v2, Lepson/print/screen/PageRangeSetting;->sheets:I

    if-ne p1, v2, :cond_2

    .line 154
    iget-object p1, p0, Lepson/print/screen/PageRangeSetting$1;->this$0:Lepson/print/screen/PageRangeSetting;

    iget-object p1, p1, Lepson/print/screen/PageRangeSetting;->endPagePlus:Landroid/widget/Button;

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 155
    iget-object p1, p0, Lepson/print/screen/PageRangeSetting$1;->this$0:Lepson/print/screen/PageRangeSetting;

    iget p1, p1, Lepson/print/screen/PageRangeSetting;->end:I

    iget-object v1, p0, Lepson/print/screen/PageRangeSetting$1;->this$0:Lepson/print/screen/PageRangeSetting;

    iget v1, v1, Lepson/print/screen/PageRangeSetting;->start:I

    if-le p1, v1, :cond_2

    .line 157
    iget-object p1, p0, Lepson/print/screen/PageRangeSetting$1;->this$0:Lepson/print/screen/PageRangeSetting;

    iget-object p1, p1, Lepson/print/screen/PageRangeSetting;->endPageMinus:Landroid/widget/Button;

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    :cond_2
    :goto_0
    return-void
.end method

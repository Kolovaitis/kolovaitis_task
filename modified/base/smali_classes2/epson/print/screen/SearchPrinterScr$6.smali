.class Lepson/print/screen/SearchPrinterScr$6;
.super Ljava/lang/Object;
.source "SearchPrinterScr.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/screen/SearchPrinterScr;->buildElements()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/screen/SearchPrinterScr;


# direct methods
.method constructor <init>(Lepson/print/screen/SearchPrinterScr;)V
    .locals 0

    .line 464
    iput-object p1, p0, Lepson/print/screen/SearchPrinterScr$6;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .line 467
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$6;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {p1}, Lepson/print/screen/SearchPrinterScr;->access$200(Lepson/print/screen/SearchPrinterScr;)I

    move-result p1

    const/4 v0, 0x0

    const v1, 0x7f0e0340

    const v2, 0x7f0e0341

    const/16 v3, 0x20

    const v4, 0x7f0e052b

    const/4 v5, 0x0

    packed-switch p1, :pswitch_data_0

    goto/16 :goto_0

    .line 525
    :pswitch_0
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$6;->this$0:Lepson/print/screen/SearchPrinterScr;

    iget-object p1, p1, Lepson/print/screen/SearchPrinterScr;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {p1}, Lepson/print/widgets/AbstractListBuilder;->getData()Ljava/util/Vector;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result p1

    if-lt p1, v3, :cond_0

    .line 526
    new-instance p1, Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr$6;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {v0}, Lepson/print/screen/SearchPrinterScr;->access$600(Lepson/print/screen/SearchPrinterScr;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 527
    invoke-virtual {p1, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr$6;->this$0:Lepson/print/screen/SearchPrinterScr;

    .line 528
    invoke-virtual {v0, v2}, Lepson/print/screen/SearchPrinterScr;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr$6;->this$0:Lepson/print/screen/SearchPrinterScr;

    .line 529
    invoke-virtual {v0, v1}, Lepson/print/screen/SearchPrinterScr;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr$6;->this$0:Lepson/print/screen/SearchPrinterScr;

    .line 530
    invoke-virtual {v0, v4}, Lepson/print/screen/SearchPrinterScr;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lepson/print/screen/SearchPrinterScr$6$4;

    invoke-direct {v1, p0}, Lepson/print/screen/SearchPrinterScr$6$4;-><init>(Lepson/print/screen/SearchPrinterScr$6;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 535
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void

    .line 540
    :cond_0
    new-instance p1, Landroid/os/Message;

    invoke-direct {p1}, Landroid/os/Message;-><init>()V

    const/16 v1, 0xb

    .line 541
    iput v1, p1, Landroid/os/Message;->what:I

    .line 542
    iput-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 543
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr$6;->this$0:Lepson/print/screen/SearchPrinterScr;

    iget-object v0, v0, Lepson/print/screen/SearchPrinterScr;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 472
    :pswitch_1
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$6;->this$0:Lepson/print/screen/SearchPrinterScr;

    iget-object p1, p1, Lepson/print/screen/SearchPrinterScr;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {p1}, Lepson/print/widgets/AbstractListBuilder;->getData()Ljava/util/Vector;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result p1

    if-lt p1, v3, :cond_1

    .line 473
    new-instance p1, Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr$6;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {v0}, Lepson/print/screen/SearchPrinterScr;->access$600(Lepson/print/screen/SearchPrinterScr;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 474
    invoke-virtual {p1, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr$6;->this$0:Lepson/print/screen/SearchPrinterScr;

    .line 475
    invoke-virtual {v0, v2}, Lepson/print/screen/SearchPrinterScr;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr$6;->this$0:Lepson/print/screen/SearchPrinterScr;

    .line 476
    invoke-virtual {v0, v1}, Lepson/print/screen/SearchPrinterScr;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr$6;->this$0:Lepson/print/screen/SearchPrinterScr;

    .line 477
    invoke-virtual {v0, v4}, Lepson/print/screen/SearchPrinterScr;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lepson/print/screen/SearchPrinterScr$6$1;

    invoke-direct {v1, p0}, Lepson/print/screen/SearchPrinterScr$6$1;-><init>(Lepson/print/screen/SearchPrinterScr$6;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 482
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void

    .line 488
    :cond_1
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$6;->this$0:Lepson/print/screen/SearchPrinterScr;

    const-string v1, "PREFS_EPSON_CONNECT"

    invoke-virtual {p1, v1, v5}, Lepson/print/screen/SearchPrinterScr;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    const-string v1, "ENABLE_SHOW_WARNING"

    const/4 v2, 0x1

    .line 489
    invoke-interface {p1, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result p1

    if-ne p1, v2, :cond_2

    .line 492
    new-instance p1, Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr$6;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {v0}, Lepson/print/screen/SearchPrinterScr;->access$600(Lepson/print/screen/SearchPrinterScr;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 493
    invoke-virtual {p1, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr$6;->this$0:Lepson/print/screen/SearchPrinterScr;

    const v2, 0x7f0e0362

    .line 494
    invoke-virtual {v1, v2}, Lepson/print/screen/SearchPrinterScr;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr$6;->this$0:Lepson/print/screen/SearchPrinterScr;

    const v2, 0x7f0e0363

    .line 495
    invoke-virtual {v1, v2}, Lepson/print/screen/SearchPrinterScr;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 494
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr$6;->this$0:Lepson/print/screen/SearchPrinterScr;

    .line 496
    invoke-virtual {v0, v4}, Lepson/print/screen/SearchPrinterScr;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lepson/print/screen/SearchPrinterScr$6$3;

    invoke-direct {v1, p0}, Lepson/print/screen/SearchPrinterScr$6$3;-><init>(Lepson/print/screen/SearchPrinterScr$6;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr$6;->this$0:Lepson/print/screen/SearchPrinterScr;

    const v1, 0x7f0e04e6

    .line 506
    invoke-virtual {v0, v1}, Lepson/print/screen/SearchPrinterScr;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lepson/print/screen/SearchPrinterScr$6$2;

    invoke-direct {v1, p0}, Lepson/print/screen/SearchPrinterScr$6$2;-><init>(Lepson/print/screen/SearchPrinterScr$6;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 511
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0

    .line 514
    :cond_2
    new-instance p1, Landroid/os/Message;

    invoke-direct {p1}, Landroid/os/Message;-><init>()V

    const/16 v1, 0x8

    .line 515
    iput v1, p1, Landroid/os/Message;->what:I

    .line 516
    iput-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 517
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr$6;->this$0:Lepson/print/screen/SearchPrinterScr;

    iget-object v0, v0, Lepson/print/screen/SearchPrinterScr;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

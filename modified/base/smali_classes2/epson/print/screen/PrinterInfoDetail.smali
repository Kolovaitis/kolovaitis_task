.class public Lepson/print/screen/PrinterInfoDetail;
.super Lepson/print/ActivityIACommon;
.source "PrinterInfoDetail.java"


# instance fields
.field private final GET_BORDER:I

.field private final GET_COLOR:I

.field private final GET_DUPLEX:I

.field private final GET_FEED_DIRECTION:I

.field private final GET_PAPER_SIZE:I

.field private final GET_PAPER_SOURCE:I

.field private final GET_PAPER_TYPE:I

.field private final GET_PRINTDATE:I

.field private final GET_QUALITY:I

.field private color_info:[I

.field private duplex_info:[I

.field private feed_direction_info:[I

.field id:I

.field private info:[I

.field isDocumentSetting:Z

.field private layout_info:[I

.field mBuilder:Lepson/print/widgets/AbstractListBuilder;

.field mHandler:Landroid/os/Handler;

.field mLayout:Landroid/view/ViewGroup;

.field mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

.field private paper_size_info:[I

.field private paper_source_info:[I

.field private paper_type_info:[I

.field private printdate_info:[I

.field private quality_info:[I

.field private sizeIndex:I

.field private typeIndex:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 28
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    const/4 v0, 0x0

    .line 33
    iput-object v0, p0, Lepson/print/screen/PrinterInfoDetail;->printdate_info:[I

    .line 34
    iput-object v0, p0, Lepson/print/screen/PrinterInfoDetail;->paper_source_info:[I

    .line 35
    iput-object v0, p0, Lepson/print/screen/PrinterInfoDetail;->color_info:[I

    .line 36
    iput-object v0, p0, Lepson/print/screen/PrinterInfoDetail;->paper_size_info:[I

    .line 37
    iput-object v0, p0, Lepson/print/screen/PrinterInfoDetail;->paper_type_info:[I

    .line 38
    iput-object v0, p0, Lepson/print/screen/PrinterInfoDetail;->layout_info:[I

    .line 39
    iput-object v0, p0, Lepson/print/screen/PrinterInfoDetail;->quality_info:[I

    .line 40
    iput-object v0, p0, Lepson/print/screen/PrinterInfoDetail;->duplex_info:[I

    .line 41
    iput-object v0, p0, Lepson/print/screen/PrinterInfoDetail;->feed_direction_info:[I

    const/4 v1, 0x1

    .line 213
    iput v1, p0, Lepson/print/screen/PrinterInfoDetail;->GET_PAPER_SIZE:I

    const/4 v1, 0x2

    .line 214
    iput v1, p0, Lepson/print/screen/PrinterInfoDetail;->GET_PAPER_TYPE:I

    const/4 v1, 0x3

    .line 215
    iput v1, p0, Lepson/print/screen/PrinterInfoDetail;->GET_BORDER:I

    const/4 v1, 0x4

    .line 216
    iput v1, p0, Lepson/print/screen/PrinterInfoDetail;->GET_QUALITY:I

    const/4 v1, 0x5

    .line 217
    iput v1, p0, Lepson/print/screen/PrinterInfoDetail;->GET_PAPER_SOURCE:I

    const/4 v1, 0x6

    .line 218
    iput v1, p0, Lepson/print/screen/PrinterInfoDetail;->GET_COLOR:I

    const/4 v1, 0x7

    .line 219
    iput v1, p0, Lepson/print/screen/PrinterInfoDetail;->GET_DUPLEX:I

    const/16 v1, 0x8

    .line 220
    iput v1, p0, Lepson/print/screen/PrinterInfoDetail;->GET_FEED_DIRECTION:I

    const/16 v1, 0x12

    .line 221
    iput v1, p0, Lepson/print/screen/PrinterInfoDetail;->GET_PRINTDATE:I

    const/4 v1, 0x0

    .line 223
    iput v1, p0, Lepson/print/screen/PrinterInfoDetail;->sizeIndex:I

    iput v1, p0, Lepson/print/screen/PrinterInfoDetail;->typeIndex:I

    .line 224
    iput-object v0, p0, Lepson/print/screen/PrinterInfoDetail;->info:[I

    .line 226
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lepson/print/screen/PrinterInfoDetail$2;

    invoke-direct {v1, p0}, Lepson/print/screen/PrinterInfoDetail$2;-><init>(Lepson/print/screen/PrinterInfoDetail;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lepson/print/screen/PrinterInfoDetail;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lepson/print/screen/PrinterInfoDetail;)[I
    .locals 0

    .line 28
    iget-object p0, p0, Lepson/print/screen/PrinterInfoDetail;->paper_size_info:[I

    return-object p0
.end method

.method static synthetic access$100(Lepson/print/screen/PrinterInfoDetail;)[I
    .locals 0

    .line 28
    iget-object p0, p0, Lepson/print/screen/PrinterInfoDetail;->info:[I

    return-object p0
.end method

.method static synthetic access$102(Lepson/print/screen/PrinterInfoDetail;[I)[I
    .locals 0

    .line 28
    iput-object p1, p0, Lepson/print/screen/PrinterInfoDetail;->info:[I

    return-object p1
.end method

.method static synthetic access$200(Lepson/print/screen/PrinterInfoDetail;)[I
    .locals 0

    .line 28
    iget-object p0, p0, Lepson/print/screen/PrinterInfoDetail;->paper_type_info:[I

    return-object p0
.end method

.method static synthetic access$300(Lepson/print/screen/PrinterInfoDetail;)[I
    .locals 0

    .line 28
    iget-object p0, p0, Lepson/print/screen/PrinterInfoDetail;->layout_info:[I

    return-object p0
.end method

.method static synthetic access$400(Lepson/print/screen/PrinterInfoDetail;)[I
    .locals 0

    .line 28
    iget-object p0, p0, Lepson/print/screen/PrinterInfoDetail;->quality_info:[I

    return-object p0
.end method

.method static synthetic access$500(Lepson/print/screen/PrinterInfoDetail;)[I
    .locals 0

    .line 28
    iget-object p0, p0, Lepson/print/screen/PrinterInfoDetail;->paper_source_info:[I

    return-object p0
.end method

.method static synthetic access$600(Lepson/print/screen/PrinterInfoDetail;)[I
    .locals 0

    .line 28
    iget-object p0, p0, Lepson/print/screen/PrinterInfoDetail;->color_info:[I

    return-object p0
.end method

.method static synthetic access$700(Lepson/print/screen/PrinterInfoDetail;)[I
    .locals 0

    .line 28
    iget-object p0, p0, Lepson/print/screen/PrinterInfoDetail;->duplex_info:[I

    return-object p0
.end method

.method private buildElements()V
    .locals 3

    .line 104
    new-instance v0, Lepson/print/widgets/PrinterInfoDetailBuilder;

    invoke-virtual {p0}, Lepson/print/screen/PrinterInfoDetail;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lepson/print/screen/PrinterInfoDetail;->mLayout:Landroid/view/ViewGroup;

    invoke-direct {v0, v1, v2}, Lepson/print/widgets/PrinterInfoDetailBuilder;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;)V

    iput-object v0, p0, Lepson/print/screen/PrinterInfoDetail;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    .line 105
    iget-object v0, p0, Lepson/print/screen/PrinterInfoDetail;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {v0}, Lepson/print/widgets/AbstractListBuilder;->build()V

    .line 106
    iget-object v0, p0, Lepson/print/screen/PrinterInfoDetail;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {p0}, Lepson/print/screen/PrinterInfoDetail;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "curValue"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lepson/print/widgets/AbstractListBuilder;->setResource(Ljava/lang/Object;)V

    .line 107
    iget-object v0, p0, Lepson/print/screen/PrinterInfoDetail;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {v0}, Lepson/print/widgets/AbstractListBuilder;->refresh()V

    const-string v0, "buildElements"

    .line 108
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lepson/print/screen/PrinterInfoDetail;->id:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    iget v0, p0, Lepson/print/screen/PrinterInfoDetail;->id:I

    const/4 v1, 0x2

    sparse-switch v0, :sswitch_data_0

    goto/16 :goto_0

    :sswitch_0
    const v0, 0x7f0e0462

    .line 176
    invoke-virtual {p0, v0}, Lepson/print/screen/PrinterInfoDetail;->setTitle(I)V

    .line 181
    new-instance v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$SharpnessState;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$SharpnessState;-><init>()V

    iput-object v0, p0, Lepson/print/screen/PrinterInfoDetail;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 183
    iget-object v0, p0, Lepson/print/screen/PrinterInfoDetail;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    check-cast v0, Lepson/print/widgets/PrinterInfoDetailBuilder;

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    iget-object v2, p0, Lepson/print/screen/PrinterInfoDetail;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {v0, v1, v2}, Lepson/print/widgets/PrinterInfoDetailBuilder;->addPrinterInfo([ILcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;)V

    goto/16 :goto_0

    .line 129
    :sswitch_1
    new-instance v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$Quality;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$Quality;-><init>()V

    iput-object v0, p0, Lepson/print/screen/PrinterInfoDetail;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    const v0, 0x7f0e0441

    .line 130
    invoke-virtual {p0, v0}, Lepson/print/screen/PrinterInfoDetail;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lepson/print/screen/PrinterInfoDetail;->setTitle(Ljava/lang/CharSequence;)V

    .line 131
    iget-object v0, p0, Lepson/print/screen/PrinterInfoDetail;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 141
    :sswitch_2
    new-instance v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PrintDate;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PrintDate;-><init>()V

    iput-object v0, p0, Lepson/print/screen/PrinterInfoDetail;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    const v0, 0x7f0e0314

    .line 142
    invoke-virtual {p0, v0}, Lepson/print/screen/PrinterInfoDetail;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lepson/print/screen/PrinterInfoDetail;->setTitle(Ljava/lang/CharSequence;)V

    .line 143
    iget-object v0, p0, Lepson/print/screen/PrinterInfoDetail;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x12

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 117
    :sswitch_3
    new-instance v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperType;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperType;-><init>()V

    iput-object v0, p0, Lepson/print/screen/PrinterInfoDetail;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    const v0, 0x7f0e0408

    .line 118
    invoke-virtual {p0, v0}, Lepson/print/screen/PrinterInfoDetail;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lepson/print/screen/PrinterInfoDetail;->setTitle(Ljava/lang/CharSequence;)V

    .line 119
    iget-object v0, p0, Lepson/print/screen/PrinterInfoDetail;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 135
    :sswitch_4
    new-instance v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSource;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSource;-><init>()V

    iput-object v0, p0, Lepson/print/screen/PrinterInfoDetail;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    const v0, 0x7f0e0407

    .line 136
    invoke-virtual {p0, v0}, Lepson/print/screen/PrinterInfoDetail;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lepson/print/screen/PrinterInfoDetail;->setTitle(Ljava/lang/CharSequence;)V

    .line 137
    iget-object v0, p0, Lepson/print/screen/PrinterInfoDetail;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 111
    :sswitch_5
    new-instance v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize;-><init>()V

    iput-object v0, p0, Lepson/print/screen/PrinterInfoDetail;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    const v0, 0x7f0e0405

    .line 112
    invoke-virtual {p0, v0}, Lepson/print/screen/PrinterInfoDetail;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lepson/print/screen/PrinterInfoDetail;->setTitle(Ljava/lang/CharSequence;)V

    .line 113
    iget-object v0, p0, Lepson/print/screen/PrinterInfoDetail;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 123
    :sswitch_6
    new-instance v0, Lepson/print/widgets/LayoutEx;

    invoke-direct {v0}, Lepson/print/widgets/LayoutEx;-><init>()V

    iput-object v0, p0, Lepson/print/screen/PrinterInfoDetail;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    const v0, 0x7f0e039d

    .line 124
    invoke-virtual {p0, v0}, Lepson/print/screen/PrinterInfoDetail;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lepson/print/screen/PrinterInfoDetail;->setTitle(Ljava/lang/CharSequence;)V

    .line 125
    iget-object v0, p0, Lepson/print/screen/PrinterInfoDetail;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 159
    :sswitch_7
    new-instance v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$FeedDirection;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$FeedDirection;-><init>()V

    iput-object v0, p0, Lepson/print/screen/PrinterInfoDetail;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    const v0, 0x7f0e0371

    .line 160
    invoke-virtual {p0, v0}, Lepson/print/screen/PrinterInfoDetail;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lepson/print/screen/PrinterInfoDetail;->setTitle(Ljava/lang/CharSequence;)V

    .line 161
    iget-object v0, p0, Lepson/print/screen/PrinterInfoDetail;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 153
    :sswitch_8
    new-instance v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$Duplex;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$Duplex;-><init>()V

    iput-object v0, p0, Lepson/print/screen/PrinterInfoDetail;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    const v0, 0x7f0e0329

    .line 154
    invoke-virtual {p0, v0}, Lepson/print/screen/PrinterInfoDetail;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lepson/print/screen/PrinterInfoDetail;->setTitle(Ljava/lang/CharSequence;)V

    .line 155
    iget-object v0, p0, Lepson/print/screen/PrinterInfoDetail;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 147
    :sswitch_9
    new-instance v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$Color;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$Color;-><init>()V

    iput-object v0, p0, Lepson/print/screen/PrinterInfoDetail;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    const v0, 0x7f0e02f3

    .line 148
    invoke-virtual {p0, v0}, Lepson/print/screen/PrinterInfoDetail;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lepson/print/screen/PrinterInfoDetail;->setTitle(Ljava/lang/CharSequence;)V

    .line 149
    iget-object v0, p0, Lepson/print/screen/PrinterInfoDetail;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :sswitch_a
    const v0, 0x7f0e02ab

    .line 165
    invoke-virtual {p0, v0}, Lepson/print/screen/PrinterInfoDetail;->setTitle(I)V

    .line 169
    new-instance v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$ApfState;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$ApfState;-><init>()V

    iput-object v0, p0, Lepson/print/screen/PrinterInfoDetail;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 171
    iget-object v0, p0, Lepson/print/screen/PrinterInfoDetail;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    check-cast v0, Lepson/print/widgets/PrinterInfoDetailBuilder;

    new-array v1, v1, [I

    fill-array-data v1, :array_1

    iget-object v2, p0, Lepson/print/screen/PrinterInfoDetail;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    invoke-virtual {v0, v1, v2}, Lepson/print/widgets/PrinterInfoDetailBuilder;->addPrinterInfo([ILcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;)V

    .line 188
    :goto_0
    iget-object v0, p0, Lepson/print/screen/PrinterInfoDetail;->mLayout:Landroid/view/ViewGroup;

    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    new-instance v1, Lepson/print/screen/PrinterInfoDetail$1;

    invoke-direct {v1, p0}, Lepson/print/screen/PrinterInfoDetail$1;-><init>(Lepson/print/screen/PrinterInfoDetail;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void

    :sswitch_data_0
    .sparse-switch
        0x7f080063 -> :sswitch_a
        0x7f0800d2 -> :sswitch_9
        0x7f08011d -> :sswitch_8
        0x7f08013d -> :sswitch_7
        0x7f0801b6 -> :sswitch_6
        0x7f08024a -> :sswitch_5
        0x7f08024f -> :sswitch_4
        0x7f080252 -> :sswitch_3
        0x7f08027b -> :sswitch_2
        0x7f0802a0 -> :sswitch_1
        0x7f0802fa -> :sswitch_0
    .end sparse-switch

    :array_0
    .array-data 4
        0x1
        0x0
    .end array-data

    :array_1
    .array-data 4
        0x1
        0x0
    .end array-data
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .line 47
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    .line 50
    invoke-virtual {p0}, Lepson/print/screen/PrinterInfoDetail;->getIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "ID"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lepson/print/screen/PrinterInfoDetail;->id:I

    .line 51
    iget p1, p0, Lepson/print/screen/PrinterInfoDetail;->id:I

    sparse-switch p1, :sswitch_data_0

    goto/16 :goto_0

    .line 65
    :sswitch_0
    invoke-virtual {p0}, Lepson/print/screen/PrinterInfoDetail;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "PRINT_QUALITY_INFO"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object p1

    iput-object p1, p0, Lepson/print/screen/PrinterInfoDetail;->quality_info:[I

    goto :goto_0

    .line 69
    :sswitch_1
    invoke-virtual {p0}, Lepson/print/screen/PrinterInfoDetail;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "PRINTDATE_INFO"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object p1

    iput-object p1, p0, Lepson/print/screen/PrinterInfoDetail;->printdate_info:[I

    goto :goto_0

    .line 57
    :sswitch_2
    invoke-virtual {p0}, Lepson/print/screen/PrinterInfoDetail;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "PAPER_TYPE_INFO"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object p1

    iput-object p1, p0, Lepson/print/screen/PrinterInfoDetail;->paper_type_info:[I

    goto :goto_0

    .line 73
    :sswitch_3
    invoke-virtual {p0}, Lepson/print/screen/PrinterInfoDetail;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "PAPER_SOURCE_INFO"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object p1

    iput-object p1, p0, Lepson/print/screen/PrinterInfoDetail;->paper_source_info:[I

    goto :goto_0

    .line 53
    :sswitch_4
    invoke-virtual {p0}, Lepson/print/screen/PrinterInfoDetail;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "PAPER_SIZE_INFO"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object p1

    iput-object p1, p0, Lepson/print/screen/PrinterInfoDetail;->paper_size_info:[I

    goto :goto_0

    .line 61
    :sswitch_5
    invoke-virtual {p0}, Lepson/print/screen/PrinterInfoDetail;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "LAYOUT_INFO"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object p1

    iput-object p1, p0, Lepson/print/screen/PrinterInfoDetail;->layout_info:[I

    goto :goto_0

    .line 85
    :sswitch_6
    invoke-virtual {p0}, Lepson/print/screen/PrinterInfoDetail;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "FEED_DIRECTION_INFO"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object p1

    iput-object p1, p0, Lepson/print/screen/PrinterInfoDetail;->feed_direction_info:[I

    goto :goto_0

    .line 81
    :sswitch_7
    invoke-virtual {p0}, Lepson/print/screen/PrinterInfoDetail;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "DUPLEX_INFO"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object p1

    iput-object p1, p0, Lepson/print/screen/PrinterInfoDetail;->duplex_info:[I

    goto :goto_0

    .line 77
    :sswitch_8
    invoke-virtual {p0}, Lepson/print/screen/PrinterInfoDetail;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "COLOR_INFO"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object p1

    iput-object p1, p0, Lepson/print/screen/PrinterInfoDetail;->color_info:[I

    .line 88
    :goto_0
    invoke-virtual {p0}, Lepson/print/screen/PrinterInfoDetail;->getIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "sizeIndex"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lepson/print/screen/PrinterInfoDetail;->sizeIndex:I

    .line 89
    invoke-virtual {p0}, Lepson/print/screen/PrinterInfoDetail;->getIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "typeIndex"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lepson/print/screen/PrinterInfoDetail;->typeIndex:I

    .line 91
    invoke-virtual {p0}, Lepson/print/screen/PrinterInfoDetail;->getIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "isDocumentSetting"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    iput-boolean p1, p0, Lepson/print/screen/PrinterInfoDetail;->isDocumentSetting:Z

    .line 93
    invoke-virtual {p0}, Lepson/print/screen/PrinterInfoDetail;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object p1

    const v0, 0x7f0a00bd

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, Lepson/print/screen/PrinterInfoDetail;->mLayout:Landroid/view/ViewGroup;

    .line 95
    invoke-direct {p0}, Lepson/print/screen/PrinterInfoDetail;->buildElements()V

    .line 96
    iget-object p1, p0, Lepson/print/screen/PrinterInfoDetail;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {p0, p1}, Lepson/print/screen/PrinterInfoDetail;->setContentView(Landroid/view/View;)V

    .line 99
    invoke-virtual {p0}, Lepson/print/screen/PrinterInfoDetail;->getTitle()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lepson/print/screen/PrinterInfoDetail;->setActionBar(Ljava/lang/String;Z)V

    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0800d2 -> :sswitch_8
        0x7f08011d -> :sswitch_7
        0x7f08013d -> :sswitch_6
        0x7f0801b6 -> :sswitch_5
        0x7f08024a -> :sswitch_4
        0x7f08024f -> :sswitch_3
        0x7f080252 -> :sswitch_2
        0x7f08027b -> :sswitch_1
        0x7f0802a0 -> :sswitch_0
    .end sparse-switch
.end method

.method protected onDestroy()V
    .locals 1

    .line 205
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onDestroy()V

    .line 207
    iget-object v0, p0, Lepson/print/screen/PrinterInfoDetail;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {v0}, Lepson/print/widgets/AbstractListBuilder;->destructor()V

    .line 208
    iget-object v0, p0, Lepson/print/screen/PrinterInfoDetail;->mLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    if-eqz v0, :cond_0

    .line 209
    invoke-virtual {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    :cond_0
    return-void
.end method

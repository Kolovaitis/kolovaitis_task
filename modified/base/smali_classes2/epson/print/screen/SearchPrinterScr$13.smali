.class Lepson/print/screen/SearchPrinterScr$13;
.super Ljava/lang/Object;
.source "SearchPrinterScr.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/screen/SearchPrinterScr;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/screen/SearchPrinterScr;


# direct methods
.method constructor <init>(Lepson/print/screen/SearchPrinterScr;)V
    .locals 0

    .line 754
    iput-object p1, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 9

    .line 758
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x8

    const/4 v2, 0x0

    const/4 v3, -0x1

    const/4 v4, 0x0

    const/4 v5, 0x1

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto/16 :goto_4

    .line 768
    :pswitch_1
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    iget-object v0, p1, Lepson/print/screen/SearchPrinterScr;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x9

    const/16 v2, 0x3c

    invoke-static {p1, v0, v1, v2}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->searchWiFiDirectPrinter(Landroid/content/Context;Landroid/os/Handler;II)Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/screen/SearchPrinterScr;->access$902(Lepson/print/screen/SearchPrinterScr;Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;)Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    .line 770
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {p1}, Lepson/print/screen/SearchPrinterScr;->access$900(Lepson/print/screen/SearchPrinterScr;)Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    move-result-object p1

    if-nez p1, :cond_11

    .line 771
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {p1, v4}, Lepson/print/screen/SearchPrinterScr;->access$1002(Lepson/print/screen/SearchPrinterScr;Z)Z

    goto/16 :goto_4

    .line 902
    :pswitch_2
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {p1}, Lepson/print/screen/SearchPrinterScr;->access$1700(Lepson/print/screen/SearchPrinterScr;)Landroid/support/v4/app/DialogFragment;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/screen/SearchPrinterScr;->access$1800(Lepson/print/screen/SearchPrinterScr;Landroid/support/v4/app/DialogFragment;)V

    goto/16 :goto_4

    .line 908
    :pswitch_3
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lepson/print/MyPrinter;

    invoke-static {v0, p1}, Lepson/print/screen/SearchPrinterScr;->access$1902(Lepson/print/screen/SearchPrinterScr;Lepson/print/MyPrinter;)Lepson/print/MyPrinter;

    .line 909
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {p1, v2}, Lepson/print/screen/SearchPrinterScr;->access$2002(Lepson/print/screen/SearchPrinterScr;Lepson/print/MyPrinter;)Lepson/print/MyPrinter;

    .line 912
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    iget-object p1, p1, Lepson/print/screen/SearchPrinterScr;->progress:Lepson/print/screen/WorkingDialog;

    if-nez p1, :cond_0

    .line 913
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    new-instance v0, Lepson/print/screen/WorkingDialog;

    invoke-direct {v0, p1}, Lepson/print/screen/WorkingDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p1, Lepson/print/screen/SearchPrinterScr;->progress:Lepson/print/screen/WorkingDialog;

    .line 915
    :cond_0
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    iget-object p1, p1, Lepson/print/screen/SearchPrinterScr;->progress:Lepson/print/screen/WorkingDialog;

    invoke-virtual {p1}, Lepson/print/screen/WorkingDialog;->show()V

    .line 919
    :try_start_0
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {p1}, Lepson/print/screen/SearchPrinterScr;->access$1500(Lepson/print/screen/SearchPrinterScr;)Lepson/print/service/IEpsonService;

    move-result-object p1

    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {v0}, Lepson/print/screen/SearchPrinterScr;->access$1900(Lepson/print/screen/SearchPrinterScr;)Lepson/print/MyPrinter;

    move-result-object v0

    invoke-virtual {v0}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {v1}, Lepson/print/screen/SearchPrinterScr;->access$1900(Lepson/print/screen/SearchPrinterScr;)Lepson/print/MyPrinter;

    move-result-object v1

    invoke-virtual {v1}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    invoke-interface {p1, v0, v1, v2}, Lepson/print/service/IEpsonService;->searchPrinters(Ljava/lang/String;Ljava/lang/String;I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_4

    :catch_0
    move-exception p1

    .line 921
    invoke-virtual {p1}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_4

    .line 959
    :pswitch_4
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {v0}, Lepson/print/screen/SearchPrinterScr;->access$300(Lepson/print/screen/SearchPrinterScr;)V

    .line 962
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-eqz v0, :cond_1

    .line 963
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object v2, p1

    check-cast v2, Lepson/print/MyPrinter;

    .line 967
    :cond_1
    new-instance p1, Landroid/content/Intent;

    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    const-class v1, Lepson/print/screen/ActivityIpPrinterSetting;

    invoke-direct {p1, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 969
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    if-eqz v2, :cond_2

    .line 970
    invoke-virtual {v2}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    const-string v1, "PRINTER_KEY"

    .line 971
    invoke-virtual {v2}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const-string v1, "PRINTER_IP"

    const-string v2, ""

    .line 974
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 977
    :goto_0
    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {v1, v3}, Lepson/print/screen/SearchPrinterScr;->access$402(Lepson/print/screen/SearchPrinterScr;I)I

    .line 979
    invoke-virtual {p1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 980
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    const/16 v1, 0xb

    invoke-virtual {v0, p1, v1}, Lepson/print/screen/SearchPrinterScr;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_4

    .line 819
    :pswitch_5
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object p1

    if-eqz p1, :cond_4

    .line 820
    invoke-virtual {p1}, Landroid/os/Bundle;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "ssid"

    .line 823
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "name"

    .line 824
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "addr_infra"

    .line 825
    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-nez v1, :cond_3

    move-object v1, v0

    .line 834
    :cond_3
    new-instance v2, Lepson/print/MyPrinter;

    const-string v3, ""

    invoke-direct {v2, v1, v0, p1, v3}, Lepson/print/MyPrinter;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 835
    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    iget-object v1, v1, Lepson/print/screen/SearchPrinterScr;->wiFiDirectPrinterListUtils:Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;

    invoke-virtual {v1, v2, v4, v0, p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->addPrinter(Ljava/lang/Object;ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 839
    :cond_4
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {p1, v4}, Lepson/print/screen/SearchPrinterScr;->access$1002(Lepson/print/screen/SearchPrinterScr;Z)Z

    .line 840
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {p1}, Lepson/print/screen/SearchPrinterScr;->access$1400(Lepson/print/screen/SearchPrinterScr;)Z

    move-result p1

    if-eqz p1, :cond_11

    .line 841
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-virtual {p1}, Lepson/print/screen/SearchPrinterScr;->displaySearchResult()V

    .line 844
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    iget-object p1, p1, Lepson/print/screen/SearchPrinterScr;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {p1}, Lepson/print/widgets/AbstractListBuilder;->getData()Ljava/util/Vector;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result p1

    if-nez p1, :cond_11

    .line 845
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    iget-object p1, p1, Lepson/print/screen/SearchPrinterScr;->mHandler:Landroid/os/Handler;

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_4

    .line 931
    :pswitch_6
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {v0}, Lepson/print/screen/SearchPrinterScr;->access$300(Lepson/print/screen/SearchPrinterScr;)V

    .line 934
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-eqz v0, :cond_5

    .line 935
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object v2, p1

    check-cast v2, Lepson/print/MyPrinter;

    .line 941
    :cond_5
    new-instance p1, Landroid/content/Intent;

    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    const-class v5, Lepson/print/screen/ActivityPrinterSetting;

    invoke-direct {p1, v0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 942
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    if-eqz v2, :cond_6

    .line 943
    invoke-virtual {v2}, Lepson/print/MyPrinter;->getEmailAddress()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_6

    const-string v5, "PRINTER_EMAIL_ADDRESS"

    .line 944
    invoke-virtual {v2}, Lepson/print/MyPrinter;->getEmailAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v5, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_6
    const-string v2, "PRINTER_EMAIL_ADDRESS"

    const-string v5, ""

    .line 947
    invoke-virtual {v0, v2, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 950
    :goto_1
    iget-object v2, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {v2, v3}, Lepson/print/screen/SearchPrinterScr;->access$402(Lepson/print/screen/SearchPrinterScr;I)I

    .line 952
    invoke-virtual {p1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 953
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-virtual {v0, p1, v1}, Lepson/print/screen/SearchPrinterScr;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_4

    .line 986
    :pswitch_7
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {p1}, Lepson/print/screen/SearchPrinterScr;->access$400(Lepson/print/screen/SearchPrinterScr;)I

    move-result p1

    if-ltz p1, :cond_11

    .line 988
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {p1}, Lepson/print/screen/SearchPrinterScr;->access$200(Lepson/print/screen/SearchPrinterScr;)I

    move-result p1

    packed-switch p1, :pswitch_data_1

    goto/16 :goto_2

    .line 1022
    :pswitch_8
    new-instance p1, Lepson/print/EPPrinterManager;

    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {v0}, Lepson/print/screen/SearchPrinterScr;->access$600(Lepson/print/screen/SearchPrinterScr;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p1, v0}, Lepson/print/EPPrinterManager;-><init>(Landroid/content/Context;)V

    .line 1024
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    iget-object v0, v0, Lepson/print/screen/SearchPrinterScr;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    .line 1025
    invoke-virtual {v0}, Lepson/print/widgets/AbstractListBuilder;->getData()Ljava/util/Vector;

    move-result-object v0

    iget-object v2, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {v2}, Lepson/print/screen/SearchPrinterScr;->access$400(Lepson/print/screen/SearchPrinterScr;)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/print/MyPrinter;

    invoke-virtual {v0}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v0

    .line 1024
    invoke-virtual {p1, v0}, Lepson/print/EPPrinterManager;->deleteIpPrinterInfo(Ljava/lang/String;)Z

    .line 1026
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    iget-object p1, p1, Lepson/print/screen/SearchPrinterScr;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {p1}, Lepson/print/widgets/AbstractListBuilder;->getAdapter()Landroid/widget/BaseAdapter;

    move-result-object p1

    .line 1027
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    iget-object v0, v0, Lepson/print/screen/SearchPrinterScr;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {v0}, Lepson/print/widgets/AbstractListBuilder;->getData()Ljava/util/Vector;

    move-result-object v0

    .line 1028
    iget-object v2, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {v2}, Lepson/print/screen/SearchPrinterScr;->access$400(Lepson/print/screen/SearchPrinterScr;)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    .line 1029
    invoke-virtual {p1}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    goto :goto_2

    .line 1000
    :pswitch_9
    new-instance p1, Lepson/print/EPPrinterManager;

    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {v0}, Lepson/print/screen/SearchPrinterScr;->access$600(Lepson/print/screen/SearchPrinterScr;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p1, v0}, Lepson/print/EPPrinterManager;-><init>(Landroid/content/Context;)V

    .line 1002
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    iget-object v0, v0, Lepson/print/screen/SearchPrinterScr;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    .line 1003
    invoke-virtual {v0}, Lepson/print/widgets/AbstractListBuilder;->getData()Ljava/util/Vector;

    move-result-object v0

    iget-object v2, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {v2}, Lepson/print/screen/SearchPrinterScr;->access$400(Lepson/print/screen/SearchPrinterScr;)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/print/MyPrinter;

    invoke-virtual {v0}, Lepson/print/MyPrinter;->getEmailAddress()Ljava/lang/String;

    move-result-object v0

    .line 1002
    invoke-virtual {p1, v0}, Lepson/print/EPPrinterManager;->deleteRemotePrinterInfo(Ljava/lang/String;)Z

    .line 1004
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    iget-object p1, p1, Lepson/print/screen/SearchPrinterScr;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {p1}, Lepson/print/widgets/AbstractListBuilder;->getAdapter()Landroid/widget/BaseAdapter;

    move-result-object p1

    .line 1005
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    iget-object v0, v0, Lepson/print/screen/SearchPrinterScr;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {v0}, Lepson/print/widgets/AbstractListBuilder;->getData()Ljava/util/Vector;

    move-result-object v0

    .line 1006
    iget-object v2, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {v2}, Lepson/print/screen/SearchPrinterScr;->access$400(Lepson/print/screen/SearchPrinterScr;)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    .line 1007
    invoke-virtual {p1}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 1034
    :goto_2
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-virtual {p1}, Lepson/print/screen/SearchPrinterScr;->displaySearchResult()V

    .line 1038
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {p1}, Lepson/print/screen/SearchPrinterScr;->access$200(Lepson/print/screen/SearchPrinterScr;)I

    move-result p1

    if-eq p1, v5, :cond_7

    .line 1039
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    iget-object p1, p1, Lepson/print/screen/SearchPrinterScr;->mSearchButton:Landroid/widget/Button;

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 1042
    :cond_7
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {p1, v3}, Lepson/print/screen/SearchPrinterScr;->access$402(Lepson/print/screen/SearchPrinterScr;I)I

    goto/16 :goto_4

    .line 894
    :pswitch_a
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {p1}, Lepson/print/screen/SearchPrinterScr;->access$1600(Lepson/print/screen/SearchPrinterScr;)Z

    move-result p1

    if-nez p1, :cond_11

    .line 895
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {p1, v5}, Lepson/print/screen/SearchPrinterScr;->access$1602(Lepson/print/screen/SearchPrinterScr;Z)Z

    const-string p1, "SearchPrinterScr"

    .line 896
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mHandler curError = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    iget v1, v1, Lepson/print/screen/SearchPrinterScr;->curError:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 897
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-virtual {p1, v4}, Lepson/print/screen/SearchPrinterScr;->showDialog(I)V

    goto/16 :goto_4

    .line 855
    :pswitch_b
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {v0}, Lepson/print/screen/SearchPrinterScr;->access$1400(Lepson/print/screen/SearchPrinterScr;)Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {v0}, Lepson/print/screen/SearchPrinterScr;->access$1500(Lepson/print/screen/SearchPrinterScr;)Lepson/print/service/IEpsonService;

    move-result-object v0

    if-nez v0, :cond_9

    :cond_8
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {v0}, Lepson/print/screen/SearchPrinterScr;->access$1000(Lepson/print/screen/SearchPrinterScr;)Z

    move-result v0

    if-eqz v0, :cond_a

    :cond_9
    const-string v0, "SearchPrinterScr"

    const-string v1, "cancelSearch for SELECT_PRINTER"

    .line 856
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 859
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {v0}, Lepson/print/screen/SearchPrinterScr;->access$300(Lepson/print/screen/SearchPrinterScr;)V

    .line 862
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    iget-object v0, v0, Lepson/print/screen/SearchPrinterScr;->mHandler:Landroid/os/Handler;

    iget v1, p1, Landroid/os/Message;->what:I

    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    .line 863
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    iget-object v0, v0, Lepson/print/screen/SearchPrinterScr;->mHandler:Landroid/os/Handler;

    const-wide/16 v1, 0xc8

    invoke-virtual {v0, p1, v1, v2}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_4

    .line 871
    :cond_a
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lepson/print/MyPrinter;

    .line 873
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "myprinter"

    .line 874
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 876
    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {v1}, Lepson/print/screen/SearchPrinterScr;->access$200(Lepson/print/screen/SearchPrinterScr;)I

    move-result v1

    if-ne v1, v5, :cond_b

    .line 878
    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    .line 880
    invoke-virtual {p1}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/MacAddrUtils;->getMacAddressFromPrinterId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 878
    invoke-static {v1, p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->getCurConnectInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_c

    const-string v1, "simpleap"

    .line 882
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_3

    :cond_b
    const-string p1, "simpleap"

    const-string v1, ""

    .line 885
    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 888
    :cond_c
    :goto_3
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-virtual {p1, v3, v0}, Lepson/print/screen/SearchPrinterScr;->setResult(ILandroid/content/Intent;)V

    .line 889
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-virtual {p1}, Lepson/print/screen/SearchPrinterScr;->finish()V

    goto/16 :goto_4

    .line 781
    :pswitch_c
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "name"

    .line 782
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_e

    const-string v1, "FINISH"

    .line 783
    invoke-virtual {v0, v1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 784
    invoke-static {}, Lepson/print/screen/SearchPrinterScr;->access$1100()Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 785
    :try_start_1
    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {v1}, Lepson/print/screen/SearchPrinterScr;->access$1200(Lepson/print/screen/SearchPrinterScr;)Ljava/util/ArrayList;

    move-result-object v1

    const-string v2, "id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 786
    monitor-exit v0

    goto/16 :goto_4

    .line 789
    :cond_d
    new-instance v1, Lepson/print/MyPrinter;

    const-string v2, "name"

    .line 790
    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v6, "ip"

    .line 791
    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "id"

    .line 792
    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "serial_no"

    .line 793
    invoke-virtual {p1, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v1, v2, v6, v7, v8}, Lepson/print/MyPrinter;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "common_devicename"

    .line 794
    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lepson/print/MyPrinter;->setCommonDeviceName(Ljava/lang/String;)V

    .line 798
    iget-object v2, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    iget-object v2, v2, Lepson/print/screen/SearchPrinterScr;->wiFiDirectPrinterListUtils:Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;

    .line 799
    invoke-virtual {v1}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/epson/mobilephone/common/wifidirect/MacAddrUtils;->getMacAddressFromPrinterId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 798
    invoke-virtual {v2, v1, v3, v6, v7}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->addPrinter(Ljava/lang/Object;ILjava/lang/String;Ljava/lang/String;)V

    .line 801
    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {v1}, Lepson/print/screen/SearchPrinterScr;->access$1200(Lepson/print/screen/SearchPrinterScr;)Ljava/util/ArrayList;

    move-result-object v1

    const-string v2, "id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 802
    invoke-static {v5}, Lepson/print/screen/SearchPrinterScr;->access$1302(Z)Z

    .line 803
    monitor-exit v0

    goto :goto_4

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1

    .line 805
    :cond_e
    invoke-static {}, Lepson/print/screen/SearchPrinterScr;->access$1100()Ljava/lang/Object;

    move-result-object p1

    monitor-enter p1

    .line 806
    :try_start_2
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {v0}, Lepson/print/screen/SearchPrinterScr;->access$1400(Lepson/print/screen/SearchPrinterScr;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 807
    monitor-exit p1

    goto :goto_4

    .line 809
    :cond_f
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {v0, v5}, Lepson/print/screen/SearchPrinterScr;->access$1402(Lepson/print/screen/SearchPrinterScr;Z)Z

    .line 810
    monitor-exit p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 811
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {p1}, Lepson/print/screen/SearchPrinterScr;->access$1000(Lepson/print/screen/SearchPrinterScr;)Z

    move-result p1

    if-nez p1, :cond_11

    .line 812
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-virtual {p1}, Lepson/print/screen/SearchPrinterScr;->displaySearchResult()V

    goto :goto_4

    :catchall_1
    move-exception v0

    .line 810
    :try_start_3
    monitor-exit p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 776
    :pswitch_d
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {p1}, Lepson/print/screen/SearchPrinterScr;->access$300(Lepson/print/screen/SearchPrinterScr;)V

    goto :goto_4

    .line 760
    :pswitch_e
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {p1}, Lepson/print/screen/SearchPrinterScr;->access$200(Lepson/print/screen/SearchPrinterScr;)I

    move-result p1

    if-eq p1, v5, :cond_10

    goto :goto_4

    .line 764
    :cond_10
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$13;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {p1}, Lepson/print/screen/SearchPrinterScr;->access$800(Lepson/print/screen/SearchPrinterScr;)V

    :cond_11
    :goto_4
    return v4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_0
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_9
        :pswitch_8
    .end packed-switch
.end method

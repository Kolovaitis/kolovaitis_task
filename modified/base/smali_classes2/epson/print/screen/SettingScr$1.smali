.class Lepson/print/screen/SettingScr$1;
.super Landroid/os/AsyncTask;
.source "SettingScr.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/screen/SettingScr;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/screen/SettingScr;


# direct methods
.method constructor <init>(Lepson/print/screen/SettingScr;)V
    .locals 0

    .line 983
    iput-object p1, p0, Lepson/print/screen/SettingScr$1;->this$0:Lepson/print/screen/SettingScr;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 983
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/print/screen/SettingScr$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 2

    .line 995
    iget-object p1, p0, Lepson/print/screen/SettingScr$1;->this$0:Lepson/print/screen/SettingScr;

    const-string v0, "printer"

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$000(Lepson/print/screen/SettingScr;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->disconnect(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    .line 997
    iget-object p1, p0, Lepson/print/screen/SettingScr$1;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$100(Lepson/print/screen/SettingScr;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lepson/print/screen/SettingScr$1;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v1}, Lepson/print/screen/SettingScr;->access$000(Lepson/print/screen/SettingScr;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->disconnectSimpleAP(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    const/4 p1, 0x0

    return-object p1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 983
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/print/screen/SettingScr$1;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 3

    .line 1003
    new-instance p1, Landroid/content/Intent;

    iget-object v0, p0, Lepson/print/screen/SettingScr$1;->this$0:Lepson/print/screen/SettingScr;

    const-class v1, Lepson/print/screen/SearchPrinterScr;

    invoke-direct {p1, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1004
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "PRINTER_ID"

    .line 1005
    iget-object v2, p0, Lepson/print/screen/SettingScr$1;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v2}, Lepson/print/screen/SettingScr;->access$200(Lepson/print/screen/SettingScr;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "PRINTER_IP"

    .line 1006
    iget-object v2, p0, Lepson/print/screen/SettingScr$1;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v2}, Lepson/print/screen/SettingScr;->access$000(Lepson/print/screen/SettingScr;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "PRINTER_EMAIL_ADDRESS"

    .line 1007
    iget-object v2, p0, Lepson/print/screen/SettingScr$1;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v2}, Lepson/print/screen/SettingScr;->access$300(Lepson/print/screen/SettingScr;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "PRINTER_LOCATION"

    .line 1008
    iget-object v2, p0, Lepson/print/screen/SettingScr$1;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v2}, Lepson/print/screen/SettingScr;->access$400(Lepson/print/screen/SettingScr;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "simpleap"

    .line 1009
    iget-object v2, p0, Lepson/print/screen/SettingScr$1;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v2}, Lepson/print/screen/SettingScr;->access$100(Lepson/print/screen/SettingScr;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1010
    invoke-virtual {p1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1012
    iget-object v0, p0, Lepson/print/screen/SettingScr$1;->this$0:Lepson/print/screen/SettingScr;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lepson/print/screen/SettingScr;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1016
    iget-object p1, p0, Lepson/print/screen/SettingScr$1;->this$0:Lepson/print/screen/SettingScr;

    const v0, 0x7f08027e

    invoke-virtual {p1, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setClickable(Z)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .line 988
    iget-object v0, p0, Lepson/print/screen/SettingScr$1;->this$0:Lepson/print/screen/SettingScr;

    const v1, 0x7f08027e

    invoke-virtual {v0, v1}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    return-void
.end method

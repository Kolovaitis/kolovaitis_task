.class Lepson/print/screen/PrintProgress$8;
.super Ljava/lang/Object;
.source "PrintProgress.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/screen/PrintProgress;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/screen/PrintProgress;


# direct methods
.method constructor <init>(Lepson/print/screen/PrintProgress;)V
    .locals 0

    .line 1066
    iput-object p1, p0, Lepson/print/screen/PrintProgress$8;->this$0:Lepson/print/screen/PrintProgress;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    const/4 p1, 0x0

    .line 1071
    :try_start_0
    invoke-static {}, Lepson/print/screen/PrintProgress;->access$800()Lepson/print/service/IEpsonService;

    move-result-object p2

    invoke-interface {p2, p1}, Lepson/print/service/IEpsonService;->confirmContinueable(Z)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p2

    .line 1073
    invoke-virtual {p2}, Landroid/os/RemoteException;->printStackTrace()V

    :goto_0
    const-string p2, "Epson"

    const-string v0, "user click str_cancel button"

    .line 1076
    invoke-static {p2, v0}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1077
    sput p1, Lepson/print/screen/PrintProgress;->curError:I

    .line 1079
    iget-object p2, p0, Lepson/print/screen/PrintProgress$8;->this$0:Lepson/print/screen/PrintProgress;

    invoke-static {p2, p1}, Lepson/print/screen/PrintProgress;->access$602(Lepson/print/screen/PrintProgress;Z)Z

    .line 1080
    iget-object p2, p0, Lepson/print/screen/PrintProgress$8;->this$0:Lepson/print/screen/PrintProgress;

    invoke-virtual {p2, p1}, Lepson/print/screen/PrintProgress;->removeDialog(I)V

    .line 1081
    iget-object p1, p0, Lepson/print/screen/PrintProgress$8;->this$0:Lepson/print/screen/PrintProgress;

    iget-object p1, p1, Lepson/print/screen/PrintProgress;->mHandler:Landroid/os/Handler;

    const/4 p2, 0x5

    invoke-virtual {p1, p2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

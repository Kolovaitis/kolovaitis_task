.class public Lepson/print/screen/PaperSourceSetting;
.super Ljava/lang/Object;
.source "PaperSourceSetting.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lepson/print/screen/PaperSourceSetting;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public bSupportESCPR:Z

.field public paperSizeId:I

.field public paperSource:I

.field public paperTypeId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 26
    new-instance v0, Lepson/print/screen/PaperSourceSetting$1;

    invoke-direct {v0}, Lepson/print/screen/PaperSourceSetting$1;-><init>()V

    sput-object v0, Lepson/print/screen/PaperSourceSetting;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 20
    iput-boolean v0, p0, Lepson/print/screen/PaperSourceSetting;->bSupportESCPR:Z

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 20
    iput-boolean v0, p0, Lepson/print/screen/PaperSourceSetting;->bSupportESCPR:Z

    .line 39
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lepson/print/screen/PaperSourceSetting;->paperSource:I

    .line 40
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lepson/print/screen/PaperSourceSetting;->paperSizeId:I

    .line 41
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lepson/print/screen/PaperSourceSetting;->paperTypeId:I

    .line 42
    const-class v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    iput-boolean p1, p0, Lepson/print/screen/PaperSourceSetting;->bSupportESCPR:Z

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lepson/print/screen/PaperSourceSetting$1;)V
    .locals 0

    .line 15
    invoke-direct {p0, p1}, Lepson/print/screen/PaperSourceSetting;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Lepson/print/screen/PaperSourceSetting;)Z
    .locals 3

    .line 59
    iget v0, p0, Lepson/print/screen/PaperSourceSetting;->paperSizeId:I

    iget v1, p1, Lepson/print/screen/PaperSourceSetting;->paperSizeId:I

    const/4 v2, 0x0

    if-eq v0, v1, :cond_0

    return v2

    .line 62
    :cond_0
    iget v0, p0, Lepson/print/screen/PaperSourceSetting;->paperTypeId:I

    iget v1, p1, Lepson/print/screen/PaperSourceSetting;->paperTypeId:I

    if-eq v0, v1, :cond_1

    return v2

    .line 65
    :cond_1
    iget v0, p0, Lepson/print/screen/PaperSourceSetting;->paperSource:I

    iget p1, p1, Lepson/print/screen/PaperSourceSetting;->paperSource:I

    if-eq v0, p1, :cond_2

    return v2

    :cond_2
    const/4 p1, 0x1

    return p1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 52
    iget p2, p0, Lepson/print/screen/PaperSourceSetting;->paperSource:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 53
    iget p2, p0, Lepson/print/screen/PaperSourceSetting;->paperSizeId:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 54
    iget p2, p0, Lepson/print/screen/PaperSourceSetting;->paperTypeId:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 55
    iget-boolean p2, p0, Lepson/print/screen/PaperSourceSetting;->bSupportESCPR:Z

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    return-void
.end method

.class Lepson/print/screen/SettingScr$3;
.super Ljava/lang/Object;
.source "SettingScr.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/screen/SettingScr;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/screen/SettingScr;


# direct methods
.method constructor <init>(Lepson/print/screen/SettingScr;)V
    .locals 0

    .line 1673
    iput-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 14

    const-string v0, "SettingScr"

    const-string v1, "handleMessage call setScreenState = false"

    .line 1677
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1678
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v0, v2}, Lepson/print/screen/SettingScr;->access$700(Lepson/print/screen/SettingScr;Ljava/lang/Boolean;)V

    .line 1680
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$800(Lepson/print/screen/SettingScr;)Z

    move-result v0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    return v2

    .line 1685
    :cond_0
    new-instance v0, Lepson/print/screen/PrintSetting;

    iget-object v3, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    .line 1686
    invoke-static {v3}, Lepson/print/screen/SettingScr;->access$900(Lepson/print/screen/SettingScr;)Z

    move-result v4

    if-eqz v4, :cond_1

    sget-object v4, Lepson/print/screen/PrintSetting$Kind;->document:Lepson/print/screen/PrintSetting$Kind;

    goto :goto_0

    :cond_1
    sget-object v4, Lepson/print/screen/PrintSetting$Kind;->photo:Lepson/print/screen/PrintSetting$Kind;

    :goto_0
    invoke-direct {v0, v3, v4}, Lepson/print/screen/PrintSetting;-><init>(Landroid/content/Context;Lepson/print/screen/PrintSetting$Kind;)V

    .line 1688
    iget v3, p1, Landroid/os/Message;->what:I

    const/16 v4, 0x40

    const/16 v5, 0x20

    const/4 v6, 0x3

    const/4 v7, 0x0

    const-wide/16 v8, 0x64

    const/4 v10, 0x2

    if-eq v3, v5, :cond_50

    if-eq v3, v4, :cond_49

    const/4 v4, 0x6

    const/4 v11, 0x5

    const/4 v12, 0x4

    const/16 v13, 0x8

    packed-switch v3, :pswitch_data_0

    packed-switch v3, :pswitch_data_1

    const-string p1, "SettingScr"

    const-string v0, "default"

    .line 2394
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2395
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$5500(Lepson/print/screen/SettingScr;)Landroid/widget/TextView;

    move-result-object p1

    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    const v1, 0x7f0e04d6

    invoke-virtual {v0, v1}, Lepson/print/screen/SettingScr;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2396
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    const v0, 0x7f080198

    invoke-virtual {p1, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    invoke-virtual {p1, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2398
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/screen/SettingScr;->access$700(Lepson/print/screen/SettingScr;Ljava/lang/Boolean;)V

    goto/16 :goto_39

    .line 2369
    :pswitch_0
    iget v0, p1, Landroid/os/Message;->arg1:I

    const/16 v3, -0x44c

    if-eq v0, v3, :cond_3

    const/16 v3, -0x3e8

    if-eq v0, v3, :cond_3

    .line 2379
    iget p1, p1, Landroid/os/Message;->arg1:I

    invoke-static {p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$ErrorTable;->getStringId(I)[Ljava/lang/Integer;

    move-result-object p1

    if-nez p1, :cond_2

    .line 2381
    new-array p1, v6, [Ljava/lang/Integer;

    const v0, 0x7f0e023a

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p1, v1

    const v0, 0x7f0e023b

    .line 2382
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p1, v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p1, v10

    .line 2384
    :cond_2
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    aget-object v3, p1, v2

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v0, v3}, Lepson/print/screen/SettingScr;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    aget-object p1, p1, v1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {v4, p1}, Lepson/print/screen/SettingScr;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v3, p1}, Lepson/print/screen/SettingScr;->showErrorDialog(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 2372
    :cond_3
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    const v0, 0x7f0e034a

    invoke-virtual {p1, v0}, Lepson/print/screen/SettingScr;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 2373
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    const v1, 0x7f0e0349

    invoke-virtual {v0, v1}, Lepson/print/screen/SettingScr;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2374
    iget-object v1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-virtual {v1, p1, v0}, Lepson/print/screen/SettingScr;->showErrorDialog(Ljava/lang/String;Ljava/lang/String;)V

    .line 2389
    :goto_1
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-virtual {p1}, Lepson/print/screen/SettingScr;->resetSettings()V

    goto/16 :goto_39

    .line 1704
    :pswitch_1
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$600(Lepson/print/screen/SettingScr;)Lepson/print/service/IEpsonService;

    move-result-object p1

    const/16 v0, 0x11

    if-eqz p1, :cond_6

    .line 1708
    :try_start_0
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$400(Lepson/print/screen/SettingScr;)I

    move-result p1

    if-eq p1, v10, :cond_5

    .line 1710
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$600(Lepson/print/screen/SettingScr;)Lepson/print/service/IEpsonService;

    move-result-object p1

    iget-object v1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v1}, Lepson/print/screen/SettingScr;->access$200(Lepson/print/screen/SettingScr;)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v3}, Lepson/print/screen/SettingScr;->access$000(Lepson/print/screen/SettingScr;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    .line 1711
    invoke-static {v4}, Lepson/print/screen/SettingScr;->access$400(Lepson/print/screen/SettingScr;)I

    move-result v4

    if-ne v4, v6, :cond_4

    goto :goto_2

    :cond_4
    const/4 v10, 0x1

    .line 1710
    :goto_2
    invoke-interface {p1, v1, v3, v10}, Lepson/print/service/IEpsonService;->searchPrinters(Ljava/lang/String;Ljava/lang/String;I)I

    move-result p1

    if-eqz p1, :cond_5a

    .line 1713
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    iget-object p1, p1, Lepson/print/screen/SettingScr;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v0, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_39

    .line 1717
    :cond_5
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    iget-object p1, p1, Lepson/print/screen/SettingScr;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_39

    :catch_0
    move-exception p1

    .line 1720
    invoke-virtual {p1}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_39

    .line 1723
    :cond_6
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    iget-object p1, p1, Lepson/print/screen/SettingScr;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v0, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_39

    .line 2218
    :pswitch_2
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$600(Lepson/print/screen/SettingScr;)Lepson/print/service/IEpsonService;

    move-result-object p1

    if-nez p1, :cond_7

    .line 2219
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    iget-object p1, p1, Lepson/print/screen/SettingScr;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v4, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_39

    :cond_7
    const-string p1, "SettingScr"

    const-string v0, "GET_COLOR"

    .line 2222
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2223
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1, v7}, Lepson/print/screen/SettingScr;->access$3002(Lepson/print/screen/SettingScr;[I)[I

    .line 2226
    :try_start_1
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$600(Lepson/print/screen/SettingScr;)Lepson/print/service/IEpsonService;

    move-result-object v0

    iget-object v3, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v3}, Lepson/print/screen/SettingScr;->access$400(Lepson/print/screen/SettingScr;)I

    move-result v3

    if-ne v3, v10, :cond_8

    const/4 v3, 0x1

    goto :goto_3

    :cond_8
    const/4 v3, 0x0

    :goto_3
    iget-object v4, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    .line 2227
    invoke-static {v4}, Lepson/print/screen/SettingScr;->access$900(Lepson/print/screen/SettingScr;)Z

    move-result v4

    if-eqz v4, :cond_9

    const/4 v4, 0x1

    goto :goto_4

    :cond_9
    const/4 v4, 0x2

    :goto_4
    iget-object v6, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    .line 2228
    invoke-static {v6}, Lepson/print/screen/SettingScr;->access$1200(Lepson/print/screen/SettingScr;)I

    move-result v6

    iget-object v7, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v7}, Lepson/print/screen/SettingScr;->access$1700(Lepson/print/screen/SettingScr;)I

    move-result v7

    .line 2226
    invoke-interface {v0, v3, v4, v6, v7}, Lepson/print/service/IEpsonService;->getColor(ZIII)[I

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/screen/SettingScr;->access$3002(Lepson/print/screen/SettingScr;[I)[I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_5

    :catch_1
    move-exception p1

    .line 2230
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    iput-boolean v2, v0, Lepson/print/screen/SettingScr;->undoFlag:Z

    .line 2231
    invoke-virtual {p1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 2233
    :goto_5
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$3000(Lepson/print/screen/SettingScr;)[I

    move-result-object p1

    if-nez p1, :cond_a

    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    iput-boolean v2, p1, Lepson/print/screen/SettingScr;->undoFlag:Z

    .line 2235
    :cond_a
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$3000(Lepson/print/screen/SettingScr;)[I

    move-result-object p1

    if-eqz p1, :cond_b

    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$3000(Lepson/print/screen/SettingScr;)[I

    move-result-object p1

    array-length p1, p1

    if-gtz p1, :cond_c

    .line 2236
    :cond_b
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    new-array v0, v10, [I

    invoke-static {p1, v0}, Lepson/print/screen/SettingScr;->access$3002(Lepson/print/screen/SettingScr;[I)[I

    .line 2237
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$3000(Lepson/print/screen/SettingScr;)[I

    move-result-object p1

    aput v1, p1, v1

    .line 2238
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$3000(Lepson/print/screen/SettingScr;)[I

    move-result-object p1

    aput v2, p1, v2

    .line 2240
    :cond_c
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$3000(Lepson/print/screen/SettingScr;)[I

    move-result-object p1

    if-eqz p1, :cond_5a

    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$3000(Lepson/print/screen/SettingScr;)[I

    move-result-object p1

    array-length p1, p1

    if-lez p1, :cond_5a

    const/4 p1, 0x0

    .line 2242
    :goto_6
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$3000(Lepson/print/screen/SettingScr;)[I

    move-result-object v0

    array-length v0, v0

    if-ge p1, v0, :cond_e

    .line 2243
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$3100(Lepson/print/screen/SettingScr;)I

    move-result v0

    iget-object v3, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v3}, Lepson/print/screen/SettingScr;->access$3000(Lepson/print/screen/SettingScr;)[I

    move-result-object v3

    aget v3, v3, p1

    if-ne v0, v3, :cond_d

    goto :goto_7

    :cond_d
    add-int/lit8 p1, p1, 0x1

    goto :goto_6

    .line 2247
    :cond_e
    :goto_7
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$3000(Lepson/print/screen/SettingScr;)[I

    move-result-object v0

    array-length v0, v0

    if-lt p1, v0, :cond_f

    .line 2249
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$3000(Lepson/print/screen/SettingScr;)[I

    move-result-object v0

    aget v0, v0, v1

    invoke-static {p1, v0}, Lepson/print/screen/SettingScr;->access$3102(Lepson/print/screen/SettingScr;I)I

    .line 2251
    :cond_f
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    new-instance v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$Color;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$Color;-><init>()V

    invoke-static {p1, v0}, Lepson/print/screen/SettingScr;->access$1502(Lepson/print/screen/SettingScr;Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 2252
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$3200(Lepson/print/screen/SettingScr;)Landroid/widget/TextView;

    move-result-object p1

    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$1500(Lepson/print/screen/SettingScr;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    move-result-object v1

    iget-object v3, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v3}, Lepson/print/screen/SettingScr;->access$3100(Lepson/print/screen/SettingScr;)I

    move-result v3

    invoke-virtual {v1, v3}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lepson/print/screen/SettingScr;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2253
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1500(Lepson/print/screen/SettingScr;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    .line 2254
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    iget-object p1, p1, Lepson/print/screen/SettingScr;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_39

    .line 2148
    :pswitch_3
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$600(Lepson/print/screen/SettingScr;)Lepson/print/service/IEpsonService;

    move-result-object p1

    if-nez p1, :cond_10

    .line 2149
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    iget-object p1, p1, Lepson/print/screen/SettingScr;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v11, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_39

    :cond_10
    const-string p1, "SettingScr"

    const-string v0, "GET_PAPER_SOURCE"

    .line 2152
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2153
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1, v7}, Lepson/print/screen/SettingScr;->access$2702(Lepson/print/screen/SettingScr;[I)[I

    .line 2156
    :try_start_2
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$600(Lepson/print/screen/SettingScr;)Lepson/print/service/IEpsonService;

    move-result-object v0

    iget-object v3, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v3}, Lepson/print/screen/SettingScr;->access$1200(Lepson/print/screen/SettingScr;)I

    move-result v3

    iget-object v5, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v5}, Lepson/print/screen/SettingScr;->access$1700(Lepson/print/screen/SettingScr;)I

    move-result v5

    iget-object v6, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v6}, Lepson/print/screen/SettingScr;->access$400(Lepson/print/screen/SettingScr;)I

    move-result v6

    invoke-interface {v0, v3, v5, v6}, Lepson/print/service/IEpsonService;->getPaperSource(III)[I

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/screen/SettingScr;->access$2702(Lepson/print/screen/SettingScr;[I)[I

    .line 2159
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$2700(Lepson/print/screen/SettingScr;)[I

    move-result-object p1

    if-eqz p1, :cond_14

    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$900(Lepson/print/screen/SettingScr;)Z

    move-result p1

    if-eqz p1, :cond_14

    .line 2160
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 2161
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$2700(Lepson/print/screen/SettingScr;)[I

    move-result-object v0

    array-length v3, v0

    const/4 v5, 0x0

    :goto_8
    if-ge v5, v3, :cond_12

    aget v6, v0, v5

    if-ne v6, v13, :cond_11

    goto :goto_9

    .line 2165
    :cond_11
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_9
    add-int/lit8 v5, v5, 0x1

    goto :goto_8

    .line 2167
    :cond_12
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_13

    .line 2168
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v3, v3, [I

    invoke-static {v0, v3}, Lepson/print/screen/SettingScr;->access$2702(Lepson/print/screen/SettingScr;[I)[I

    const/4 v0, 0x0

    .line 2169
    :goto_a
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_14

    .line 2170
    iget-object v3, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v3}, Lepson/print/screen/SettingScr;->access$2700(Lepson/print/screen/SettingScr;)[I

    move-result-object v3

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    aput v5, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 2173
    :cond_13
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1, v7}, Lepson/print/screen/SettingScr;->access$2702(Lepson/print/screen/SettingScr;[I)[I
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_b

    :catch_2
    move-exception p1

    .line 2178
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    iput-boolean v2, v0, Lepson/print/screen/SettingScr;->undoFlag:Z

    .line 2179
    invoke-virtual {p1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 2182
    :cond_14
    :goto_b
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$2700(Lepson/print/screen/SettingScr;)[I

    move-result-object p1

    if-nez p1, :cond_15

    .line 2183
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    iput-boolean v2, p1, Lepson/print/screen/SettingScr;->undoFlag:Z

    .line 2184
    new-array v0, v2, [I

    invoke-static {p1, v0}, Lepson/print/screen/SettingScr;->access$2702(Lepson/print/screen/SettingScr;[I)[I

    .line 2185
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$2700(Lepson/print/screen/SettingScr;)[I

    move-result-object p1

    const/16 v0, 0x80

    aput v0, p1, v1

    .line 2187
    :cond_15
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$2700(Lepson/print/screen/SettingScr;)[I

    move-result-object p1

    if-eqz p1, :cond_5a

    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$2700(Lepson/print/screen/SettingScr;)[I

    move-result-object p1

    array-length p1, p1

    if-lez p1, :cond_5a

    const/4 p1, 0x0

    .line 2189
    :goto_c
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$2700(Lepson/print/screen/SettingScr;)[I

    move-result-object v0

    array-length v0, v0

    if-ge p1, v0, :cond_17

    .line 2190
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$2800(Lepson/print/screen/SettingScr;)I

    move-result v0

    iget-object v3, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v3}, Lepson/print/screen/SettingScr;->access$2700(Lepson/print/screen/SettingScr;)[I

    move-result-object v3

    aget v3, v3, p1

    if-ne v0, v3, :cond_16

    goto :goto_d

    :cond_16
    add-int/lit8 p1, p1, 0x1

    goto :goto_c

    .line 2194
    :cond_17
    :goto_d
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$2700(Lepson/print/screen/SettingScr;)[I

    move-result-object v0

    array-length v0, v0

    if-lt p1, v0, :cond_18

    .line 2196
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$2700(Lepson/print/screen/SettingScr;)[I

    move-result-object v0

    aget v0, v0, v1

    invoke-static {p1, v0}, Lepson/print/screen/SettingScr;->access$2802(Lepson/print/screen/SettingScr;I)I

    .line 2199
    :cond_18
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$2800(Lepson/print/screen/SettingScr;)I

    .line 2200
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$2700(Lepson/print/screen/SettingScr;)[I

    move-result-object p1

    if-eqz p1, :cond_19

    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$2700(Lepson/print/screen/SettingScr;)[I

    move-result-object p1

    array-length p1, p1

    if-gt p1, v2, :cond_19

    .line 2201
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    const v0, 0x7f080251

    invoke-virtual {p1, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    invoke-virtual {p1, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    const/16 p1, 0x80

    goto :goto_e

    .line 2205
    :cond_19
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    const v0, 0x7f080251

    invoke-virtual {p1, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2206
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$2800(Lepson/print/screen/SettingScr;)I

    move-result p1

    .line 2208
    :goto_e
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    new-instance v1, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSource;

    invoke-direct {v1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSource;-><init>()V

    invoke-static {v0, v1}, Lepson/print/screen/SettingScr;->access$1502(Lepson/print/screen/SettingScr;Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 2210
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$2900(Lepson/print/screen/SettingScr;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v1}, Lepson/print/screen/SettingScr;->access$1500(Lepson/print/screen/SettingScr;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result p1

    invoke-virtual {v1, p1}, Lepson/print/screen/SettingScr;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2211
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1500(Lepson/print/screen/SettingScr;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    .line 2212
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    iget-object p1, p1, Lepson/print/screen/SettingScr;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_39

    .line 2102
    :pswitch_4
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$600(Lepson/print/screen/SettingScr;)Lepson/print/service/IEpsonService;

    move-result-object p1

    if-nez p1, :cond_1a

    .line 2103
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    iget-object p1, p1, Lepson/print/screen/SettingScr;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v12, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_39

    :cond_1a
    const-string p1, "SettingScr"

    const-string v0, "GET_QUALITY"

    .line 2106
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2107
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1, v7}, Lepson/print/screen/SettingScr;->access$1102(Lepson/print/screen/SettingScr;[I)[I

    .line 2110
    :try_start_3
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$600(Lepson/print/screen/SettingScr;)Lepson/print/service/IEpsonService;

    move-result-object v0

    iget-object v3, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v3}, Lepson/print/screen/SettingScr;->access$1200(Lepson/print/screen/SettingScr;)I

    move-result v3

    iget-object v4, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v4}, Lepson/print/screen/SettingScr;->access$1700(Lepson/print/screen/SettingScr;)I

    move-result v4

    invoke-interface {v0, v3, v4}, Lepson/print/service/IEpsonService;->getQuality(II)[I

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/screen/SettingScr;->access$1102(Lepson/print/screen/SettingScr;[I)[I
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_f

    :catch_3
    move-exception p1

    .line 2112
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    iput-boolean v2, v0, Lepson/print/screen/SettingScr;->undoFlag:Z

    .line 2113
    invoke-virtual {p1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 2115
    :goto_f
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object p1

    if-nez p1, :cond_1b

    .line 2116
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    iput-boolean v2, p1, Lepson/print/screen/SettingScr;->undoFlag:Z

    .line 2117
    new-array v0, v2, [I

    invoke-static {p1, v0}, Lepson/print/screen/SettingScr;->access$1102(Lepson/print/screen/SettingScr;[I)[I

    .line 2118
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object p1

    aput v10, p1, v1

    .line 2120
    :cond_1b
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object p1

    if-eqz p1, :cond_5a

    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object p1

    array-length p1, p1

    if-lez p1, :cond_5a

    .line 2121
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object p1

    array-length p1, p1

    if-gt p1, v2, :cond_1c

    .line 2122
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    const v0, 0x7f0802a2

    invoke-virtual {p1, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    invoke-virtual {p1, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_10

    .line 2124
    :cond_1c
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    const v0, 0x7f0802a2

    invoke-virtual {p1, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_10
    const/4 p1, 0x0

    .line 2127
    :goto_11
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object v0

    array-length v0, v0

    if-ge p1, v0, :cond_1e

    .line 2128
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$2400(Lepson/print/screen/SettingScr;)I

    move-result v0

    iget-object v3, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v3}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object v3

    aget v3, v3, p1

    if-ne v0, v3, :cond_1d

    goto :goto_12

    :cond_1d
    add-int/lit8 p1, p1, 0x1

    goto :goto_11

    .line 2132
    :cond_1e
    :goto_12
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object v0

    array-length v0, v0

    if-lt p1, v0, :cond_1f

    .line 2134
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object v0

    aget v0, v0, v1

    invoke-static {p1, v0}, Lepson/print/screen/SettingScr;->access$2402(Lepson/print/screen/SettingScr;I)I

    .line 2136
    :cond_1f
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    invoke-static {p1, v0}, Lepson/print/screen/SettingScr;->access$2502(Lepson/print/screen/SettingScr;[I)[I

    .line 2137
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/screen/SettingScr;->access$2502(Lepson/print/screen/SettingScr;[I)[I

    .line 2138
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    new-instance v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$Quality;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$Quality;-><init>()V

    invoke-static {p1, v0}, Lepson/print/screen/SettingScr;->access$1502(Lepson/print/screen/SettingScr;Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 2139
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$2600(Lepson/print/screen/SettingScr;)Landroid/widget/TextView;

    move-result-object p1

    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$1500(Lepson/print/screen/SettingScr;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    move-result-object v1

    iget-object v3, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v3}, Lepson/print/screen/SettingScr;->access$2400(Lepson/print/screen/SettingScr;)I

    move-result v3

    invoke-virtual {v1, v3}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lepson/print/screen/SettingScr;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2140
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1500(Lepson/print/screen/SettingScr;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    .line 2141
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    iget-object p1, p1, Lepson/print/screen/SettingScr;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v11}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_39

    .line 1991
    :pswitch_5
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$600(Lepson/print/screen/SettingScr;)Lepson/print/service/IEpsonService;

    move-result-object p1

    if-nez p1, :cond_20

    .line 1992
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    iget-object p1, p1, Lepson/print/screen/SettingScr;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v6, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_39

    :cond_20
    const-string p1, "SettingScr"

    const-string v0, "GET_LAYOUT"

    .line 1995
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1996
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1, v7}, Lepson/print/screen/SettingScr;->access$1102(Lepson/print/screen/SettingScr;[I)[I

    .line 1999
    :try_start_4
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$600(Lepson/print/screen/SettingScr;)Lepson/print/service/IEpsonService;

    move-result-object v0

    iget-object v3, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v3}, Lepson/print/screen/SettingScr;->access$1200(Lepson/print/screen/SettingScr;)I

    move-result v3

    iget-object v4, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v4}, Lepson/print/screen/SettingScr;->access$1700(Lepson/print/screen/SettingScr;)I

    move-result v4

    invoke-interface {v0, v3, v4}, Lepson/print/service/IEpsonService;->getLayout(II)[I

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/screen/SettingScr;->access$1102(Lepson/print/screen/SettingScr;[I)[I
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_4

    goto :goto_13

    :catch_4
    move-exception p1

    .line 2001
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    iput-boolean v2, v0, Lepson/print/screen/SettingScr;->undoFlag:Z

    .line 2002
    invoke-virtual {p1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 2005
    :goto_13
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$900(Lepson/print/screen/SettingScr;)Z

    move-result p1

    if-ne p1, v2, :cond_21

    .line 2006
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    new-array v0, v2, [I

    invoke-static {p1, v0}, Lepson/print/screen/SettingScr;->access$1102(Lepson/print/screen/SettingScr;[I)[I

    .line 2007
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object p1

    aput v10, p1, v1

    goto :goto_14

    .line 2008
    :cond_21
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object p1

    if-nez p1, :cond_22

    .line 2009
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    iput-boolean v2, p1, Lepson/print/screen/SettingScr;->undoFlag:Z

    .line 2013
    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object p1

    if-nez p1, :cond_22

    .line 2014
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    new-array v0, v10, [I

    fill-array-data v0, :array_0

    invoke-static {p1, v0}, Lepson/print/screen/SettingScr;->access$1102(Lepson/print/screen/SettingScr;[I)[I

    .line 2022
    :cond_22
    :goto_14
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object p1

    invoke-static {p1, v10}, Ljava/util/Arrays;->binarySearch([II)I

    move-result p1

    if-ltz p1, :cond_24

    .line 2023
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$400(Lepson/print/screen/SettingScr;)I

    move-result p1

    if-eq p1, v10, :cond_24

    .line 2028
    new-instance p1, Ljava/util/ArrayList;

    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object v0

    array-length v0, v0

    invoke-direct {p1, v0}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v0, 0x0

    .line 2029
    :goto_15
    iget-object v3, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v3}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object v3

    array-length v3, v3

    if-ge v0, v3, :cond_23

    .line 2030
    iget-object v3, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v3}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object v3

    aget v3, v3, v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_15

    :cond_23
    const/high16 v0, 0x10000

    .line 2033
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/high16 v0, 0x20000

    .line 2034
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/high16 v0, 0x40000

    .line 2035
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2037
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [I

    invoke-static {v0, v3}, Lepson/print/screen/SettingScr;->access$1102(Lepson/print/screen/SettingScr;[I)[I

    const/4 v0, 0x0

    .line 2038
    :goto_16
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_24

    .line 2039
    iget-object v3, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v3}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object v3

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    aput v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_16

    .line 2045
    :cond_24
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object p1

    if-eqz p1, :cond_5a

    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object p1

    array-length p1, p1

    if-lez p1, :cond_5a

    .line 2046
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object p1

    array-length p1, p1

    if-gt p1, v2, :cond_25

    .line 2047
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    const v0, 0x7f0801ba

    invoke-virtual {p1, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    invoke-virtual {p1, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_17

    .line 2049
    :cond_25
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    const v0, 0x7f0801ba

    invoke-virtual {p1, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_17
    const/4 p1, 0x0

    .line 2052
    :goto_18
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object v0

    array-length v0, v0

    if-ge p1, v0, :cond_27

    const-string v0, "layout info"

    .line 2053
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "layout info "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v4}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object v4

    aget v4, v4, p1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lepson/print/Util/EPLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 2054
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$2100(Lepson/print/screen/SettingScr;)I

    move-result v0

    iget-object v3, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v3}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object v3

    aget v3, v3, p1

    if-ne v0, v3, :cond_26

    goto :goto_19

    :cond_26
    add-int/lit8 p1, p1, 0x1

    goto :goto_18

    .line 2058
    :cond_27
    :goto_19
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object v0

    array-length v0, v0

    if-lt p1, v0, :cond_28

    .line 2060
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object v0

    aget v0, v0, v1

    invoke-static {p1, v0}, Lepson/print/screen/SettingScr;->access$2102(Lepson/print/screen/SettingScr;I)I

    .line 2065
    :cond_28
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    new-instance v0, Lepson/print/widgets/LayoutEx;

    invoke-direct {v0}, Lepson/print/widgets/LayoutEx;-><init>()V

    invoke-static {p1, v0}, Lepson/print/screen/SettingScr;->access$1502(Lepson/print/screen/SettingScr;Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 2066
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$2100(Lepson/print/screen/SettingScr;)I

    move-result p1

    if-ne p1, v13, :cond_29

    .line 2067
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1, v10}, Lepson/print/screen/SettingScr;->access$2102(Lepson/print/screen/SettingScr;I)I

    .line 2080
    :cond_29
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    .line 2081
    :goto_1a
    iget-object v3, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v3}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object v3

    array-length v3, v3

    if-ge v0, v3, :cond_2b

    .line 2082
    iget-object v3, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v3}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object v3

    aget v3, v3, v0

    if-eq v3, v13, :cond_2a

    .line 2083
    iget-object v3, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v3}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object v3

    aget v3, v3, v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2a
    add-int/lit8 v0, v0, 0x1

    goto :goto_1a

    .line 2086
    :cond_2b
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2c

    .line 2087
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2089
    :cond_2c
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v3, v3, [I

    invoke-static {v0, v3}, Lepson/print/screen/SettingScr;->access$2202(Lepson/print/screen/SettingScr;[I)[I

    .line 2090
    :goto_1b
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2d

    .line 2091
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$2200(Lepson/print/screen/SettingScr;)[I

    move-result-object v0

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    aput v3, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1b

    .line 2094
    :cond_2d
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$2300(Lepson/print/screen/SettingScr;)Landroid/widget/TextView;

    move-result-object p1

    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$1500(Lepson/print/screen/SettingScr;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    move-result-object v1

    iget-object v3, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v3}, Lepson/print/screen/SettingScr;->access$2100(Lepson/print/screen/SettingScr;)I

    move-result v3

    invoke-virtual {v1, v3}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lepson/print/screen/SettingScr;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2095
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1500(Lepson/print/screen/SettingScr;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    .line 2096
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    iget-object p1, p1, Lepson/print/screen/SettingScr;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v12}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_39

    .line 1871
    :pswitch_6
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$600(Lepson/print/screen/SettingScr;)Lepson/print/service/IEpsonService;

    move-result-object p1

    if-nez p1, :cond_2e

    .line 1872
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    iget-object p1, p1, Lepson/print/screen/SettingScr;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v10, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_39

    :cond_2e
    const-string p1, "SettingScr"

    const-string v3, "GET_PAPER_TYPE"

    .line 1875
    invoke-static {p1, v3}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1876
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1, v7}, Lepson/print/screen/SettingScr;->access$1102(Lepson/print/screen/SettingScr;[I)[I

    .line 1879
    :try_start_5
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    iget-object v3, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v3}, Lepson/print/screen/SettingScr;->access$600(Lepson/print/screen/SettingScr;)Lepson/print/service/IEpsonService;

    move-result-object v3

    iget-object v4, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v4}, Lepson/print/screen/SettingScr;->access$1200(Lepson/print/screen/SettingScr;)I

    move-result v4

    invoke-interface {v3, v4}, Lepson/print/service/IEpsonService;->getPaperType(I)[I

    move-result-object v3

    invoke-static {p1, v3}, Lepson/print/screen/SettingScr;->access$1102(Lepson/print/screen/SettingScr;[I)[I

    .line 1882
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object p1

    if-eqz p1, :cond_32

    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$900(Lepson/print/screen/SettingScr;)Z

    move-result p1

    if-eqz p1, :cond_32

    .line 1883
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 1884
    iget-object v3, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v3}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object v3

    array-length v4, v3

    const/4 v5, 0x0

    :goto_1c
    if-ge v5, v4, :cond_30

    aget v8, v3, v5

    const/16 v9, 0x5b

    if-lt v8, v9, :cond_2f

    const/16 v9, 0x5d

    if-gt v8, v9, :cond_2f

    goto :goto_1d

    .line 1888
    :cond_2f
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {p1, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1d
    add-int/lit8 v5, v5, 0x1

    goto :goto_1c

    .line 1890
    :cond_30
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_31

    .line 1891
    iget-object v3, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [I

    invoke-static {v3, v4}, Lepson/print/screen/SettingScr;->access$1102(Lepson/print/screen/SettingScr;[I)[I

    const/4 v3, 0x0

    .line 1892
    :goto_1e
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v3, v4, :cond_32

    .line 1893
    iget-object v4, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v4}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object v4

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    aput v5, v4, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1e

    .line 1896
    :cond_31
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1, v7}, Lepson/print/screen/SettingScr;->access$1102(Lepson/print/screen/SettingScr;[I)[I
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_5

    goto :goto_1f

    :catch_5
    move-exception p1

    .line 1901
    iget-object v3, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    iput-boolean v2, v3, Lepson/print/screen/SettingScr;->undoFlag:Z

    .line 1902
    invoke-virtual {p1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 1904
    :cond_32
    :goto_1f
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object p1

    if-nez p1, :cond_33

    .line 1905
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    iput-boolean v2, p1, Lepson/print/screen/SettingScr;->undoFlag:Z

    .line 1909
    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object p1

    if-nez p1, :cond_33

    .line 1910
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    new-array v3, v10, [I

    sget-object v4, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PLAIN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 1911
    invoke-virtual {v4}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v4

    aput v4, v3, v1

    sget-object v4, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    .line 1912
    invoke-virtual {v4}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v4

    aput v4, v3, v2

    .line 1910
    invoke-static {p1, v3}, Lepson/print/screen/SettingScr;->access$1102(Lepson/print/screen/SettingScr;[I)[I

    .line 1917
    :cond_33
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object p1

    if-eqz p1, :cond_5a

    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object p1

    array-length p1, p1

    if-lez p1, :cond_5a

    .line 1918
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object p1

    array-length p1, p1

    if-gt p1, v2, :cond_34

    .line 1919
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    const v3, 0x7f080254

    invoke-virtual {p1, v3}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    invoke-virtual {p1, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_20

    .line 1921
    :cond_34
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    const v3, 0x7f080254

    invoke-virtual {p1, v3}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1924
    :goto_20
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1, v1}, Lepson/print/screen/SettingScr;->access$1702(Lepson/print/screen/SettingScr;I)I

    :goto_21
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1700(Lepson/print/screen/SettingScr;)I

    move-result p1

    iget-object v3, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v3}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object v3

    array-length v3, v3

    if-ge p1, v3, :cond_36

    .line 1925
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1800(Lepson/print/screen/SettingScr;)I

    move-result p1

    iget-object v3, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v3}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object v3

    iget-object v4, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v4}, Lepson/print/screen/SettingScr;->access$1700(Lepson/print/screen/SettingScr;)I

    move-result v4

    aget v3, v3, v4

    if-ne p1, v3, :cond_35

    goto :goto_22

    .line 1924
    :cond_35
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1708(Lepson/print/screen/SettingScr;)I

    goto :goto_21

    .line 1931
    :cond_36
    :goto_22
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1700(Lepson/print/screen/SettingScr;)I

    move-result p1

    iget-object v3, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v3}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object v3

    array-length v3, v3

    if-lt p1, v3, :cond_38

    .line 1932
    invoke-virtual {v0}, Lepson/print/screen/PrintSetting;->getDefaultPaperType()I

    move-result p1

    .line 1933
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0, v1}, Lepson/print/screen/SettingScr;->access$1702(Lepson/print/screen/SettingScr;I)I

    :goto_23
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$1700(Lepson/print/screen/SettingScr;)I

    move-result v0

    iget-object v3, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v3}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object v3

    array-length v3, v3

    if-ge v0, v3, :cond_38

    .line 1934
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object v0

    iget-object v3, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v3}, Lepson/print/screen/SettingScr;->access$1700(Lepson/print/screen/SettingScr;)I

    move-result v3

    aget v0, v0, v3

    if-ne p1, v0, :cond_37

    .line 1935
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0, p1}, Lepson/print/screen/SettingScr;->access$1802(Lepson/print/screen/SettingScr;I)I

    goto :goto_24

    .line 1933
    :cond_37
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$1708(Lepson/print/screen/SettingScr;)I

    goto :goto_23

    .line 1942
    :cond_38
    :goto_24
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1700(Lepson/print/screen/SettingScr;)I

    move-result p1

    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object v0

    array-length v0, v0

    if-lt p1, v0, :cond_3e

    .line 1943
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$900(Lepson/print/screen/SettingScr;)Z

    move-result p1

    if-nez p1, :cond_3a

    .line 1945
    sget-object p1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_MATTE:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result p1

    .line 1946
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0, v1}, Lepson/print/screen/SettingScr;->access$1702(Lepson/print/screen/SettingScr;I)I

    :goto_25
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$1700(Lepson/print/screen/SettingScr;)I

    move-result v0

    iget-object v3, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v3}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object v3

    array-length v3, v3

    if-ge v0, v3, :cond_3e

    .line 1947
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object v0

    iget-object v3, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v3}, Lepson/print/screen/SettingScr;->access$1700(Lepson/print/screen/SettingScr;)I

    move-result v3

    aget v0, v0, v3

    if-ne p1, v0, :cond_39

    .line 1948
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0, p1}, Lepson/print/screen/SettingScr;->access$1802(Lepson/print/screen/SettingScr;I)I

    goto/16 :goto_29

    .line 1946
    :cond_39
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$1708(Lepson/print/screen/SettingScr;)I

    goto :goto_25

    .line 1954
    :cond_3a
    sget-object p1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PLAIN1:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result p1

    .line 1955
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0, v1}, Lepson/print/screen/SettingScr;->access$1702(Lepson/print/screen/SettingScr;I)I

    :goto_26
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$1700(Lepson/print/screen/SettingScr;)I

    move-result v0

    iget-object v3, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v3}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object v3

    array-length v3, v3

    if-ge v0, v3, :cond_3c

    .line 1956
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object v0

    iget-object v3, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v3}, Lepson/print/screen/SettingScr;->access$1700(Lepson/print/screen/SettingScr;)I

    move-result v3

    aget v0, v0, v3

    if-ne p1, v0, :cond_3b

    .line 1957
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0, p1}, Lepson/print/screen/SettingScr;->access$1802(Lepson/print/screen/SettingScr;I)I

    goto :goto_27

    .line 1955
    :cond_3b
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$1708(Lepson/print/screen/SettingScr;)I

    goto :goto_26

    .line 1961
    :cond_3c
    :goto_27
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1700(Lepson/print/screen/SettingScr;)I

    move-result p1

    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object v0

    array-length v0, v0

    if-lt p1, v0, :cond_3e

    .line 1963
    sget-object p1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_AUTO_PLAIN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result p1

    .line 1964
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0, v1}, Lepson/print/screen/SettingScr;->access$1702(Lepson/print/screen/SettingScr;I)I

    :goto_28
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$1700(Lepson/print/screen/SettingScr;)I

    move-result v0

    iget-object v3, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v3}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object v3

    array-length v3, v3

    if-ge v0, v3, :cond_3e

    .line 1965
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object v0

    iget-object v3, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v3}, Lepson/print/screen/SettingScr;->access$1700(Lepson/print/screen/SettingScr;)I

    move-result v3

    aget v0, v0, v3

    if-ne p1, v0, :cond_3d

    .line 1966
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0, p1}, Lepson/print/screen/SettingScr;->access$1802(Lepson/print/screen/SettingScr;I)I

    goto :goto_29

    .line 1964
    :cond_3d
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$1708(Lepson/print/screen/SettingScr;)I

    goto :goto_28

    .line 1975
    :cond_3e
    :goto_29
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1700(Lepson/print/screen/SettingScr;)I

    move-result p1

    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object v0

    array-length v0, v0

    if-lt p1, v0, :cond_3f

    .line 1976
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1, v1}, Lepson/print/screen/SettingScr;->access$1702(Lepson/print/screen/SettingScr;I)I

    .line 1977
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object v0

    iget-object v1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v1}, Lepson/print/screen/SettingScr;->access$1700(Lepson/print/screen/SettingScr;)I

    move-result v1

    aget v0, v0, v1

    invoke-static {p1, v0}, Lepson/print/screen/SettingScr;->access$1802(Lepson/print/screen/SettingScr;I)I

    .line 1980
    :cond_3f
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    invoke-static {p1, v0}, Lepson/print/screen/SettingScr;->access$1902(Lepson/print/screen/SettingScr;[I)[I

    .line 1981
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/screen/SettingScr;->access$1902(Lepson/print/screen/SettingScr;[I)[I

    .line 1982
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    new-instance v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperType;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperType;-><init>()V

    invoke-static {p1, v0}, Lepson/print/screen/SettingScr;->access$1502(Lepson/print/screen/SettingScr;Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 1983
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$2000(Lepson/print/screen/SettingScr;)Landroid/widget/TextView;

    move-result-object p1

    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$1500(Lepson/print/screen/SettingScr;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    move-result-object v1

    iget-object v3, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v3}, Lepson/print/screen/SettingScr;->access$1800(Lepson/print/screen/SettingScr;)I

    move-result v3

    invoke-virtual {v1, v3}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lepson/print/screen/SettingScr;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1984
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1500(Lepson/print/screen/SettingScr;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    .line 1985
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    iget-object p1, p1, Lepson/print/screen/SettingScr;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_39

    .line 1798
    :pswitch_7
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$600(Lepson/print/screen/SettingScr;)Lepson/print/service/IEpsonService;

    move-result-object p1

    if-nez p1, :cond_40

    .line 1799
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    iget-object p1, p1, Lepson/print/screen/SettingScr;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v2, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_39

    :cond_40
    const-string p1, "SettingScr"

    const-string v3, "GET_PAPER_SIZE"

    .line 1802
    invoke-static {p1, v3}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1803
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1, v7}, Lepson/print/screen/SettingScr;->access$1102(Lepson/print/screen/SettingScr;[I)[I

    .line 1806
    :try_start_6
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    iget-object v3, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v3}, Lepson/print/screen/SettingScr;->access$600(Lepson/print/screen/SettingScr;)Lepson/print/service/IEpsonService;

    move-result-object v3

    invoke-interface {v3}, Lepson/print/service/IEpsonService;->getPaperSize()[I

    move-result-object v3

    invoke-static {p1, v3}, Lepson/print/screen/SettingScr;->access$1102(Lepson/print/screen/SettingScr;[I)[I
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_6

    goto :goto_2a

    :catch_6
    move-exception p1

    .line 1808
    invoke-virtual {p1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 1809
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    iput-boolean v2, p1, Lepson/print/screen/SettingScr;->undoFlag:Z

    .line 1811
    :goto_2a
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object p1

    if-nez p1, :cond_41

    .line 1812
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    iput-boolean v2, p1, Lepson/print/screen/SettingScr;->undoFlag:Z

    const/16 v3, 0x9

    .line 1816
    new-array v3, v3, [I

    sget-object v5, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_A4:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 1817
    invoke-virtual {v5}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v5

    aput v5, v3, v1

    sget-object v5, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_A3:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 1818
    invoke-virtual {v5}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v5

    aput v5, v3, v2

    sget-object v5, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_B4:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 1819
    invoke-virtual {v5}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v5

    aput v5, v3, v10

    sget-object v5, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_B5:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 1820
    invoke-virtual {v5}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v5

    aput v5, v3, v6

    sget-object v5, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_LETTER:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 1821
    invoke-virtual {v5}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v5

    aput v5, v3, v12

    sget-object v5, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_LEGAL:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 1822
    invoke-virtual {v5}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v5

    aput v5, v3, v11

    sget-object v5, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_POSTCARD:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 1823
    invoke-virtual {v5}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v5

    aput v5, v3, v4

    const/4 v4, 0x7

    sget-object v5, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_4X6:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 1824
    invoke-virtual {v5}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v5

    aput v5, v3, v4

    sget-object v4, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_L:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 1825
    invoke-virtual {v4}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v4

    aput v4, v3, v13

    .line 1816
    invoke-static {p1, v3}, Lepson/print/screen/SettingScr;->access$1102(Lepson/print/screen/SettingScr;[I)[I

    .line 1829
    :cond_41
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object p1

    if-eqz p1, :cond_5a

    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object p1

    array-length p1, p1

    if-lez p1, :cond_5a

    .line 1830
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object p1

    array-length p1, p1

    if-gt p1, v2, :cond_42

    .line 1831
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    const v3, 0x7f08024d

    invoke-virtual {p1, v3}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    invoke-virtual {p1, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2b

    .line 1833
    :cond_42
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    const v3, 0x7f08024d

    invoke-virtual {p1, v3}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1836
    :goto_2b
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1, v1}, Lepson/print/screen/SettingScr;->access$1202(Lepson/print/screen/SettingScr;I)I

    :goto_2c
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1200(Lepson/print/screen/SettingScr;)I

    move-result p1

    iget-object v3, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v3}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object v3

    array-length v3, v3

    if-ge p1, v3, :cond_44

    .line 1837
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1300(Lepson/print/screen/SettingScr;)I

    move-result p1

    iget-object v3, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v3}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object v3

    iget-object v4, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v4}, Lepson/print/screen/SettingScr;->access$1200(Lepson/print/screen/SettingScr;)I

    move-result v4

    aget v3, v3, v4

    if-ne p1, v3, :cond_43

    goto :goto_2d

    .line 1836
    :cond_43
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1208(Lepson/print/screen/SettingScr;)I

    goto :goto_2c

    .line 1843
    :cond_44
    :goto_2d
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1200(Lepson/print/screen/SettingScr;)I

    move-result p1

    iget-object v3, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v3}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object v3

    array-length v3, v3

    if-lt p1, v3, :cond_46

    .line 1844
    invoke-virtual {v0}, Lepson/print/screen/PrintSetting;->getDefaultPaperSize()I

    move-result p1

    .line 1845
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0, v1}, Lepson/print/screen/SettingScr;->access$1202(Lepson/print/screen/SettingScr;I)I

    :goto_2e
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$1200(Lepson/print/screen/SettingScr;)I

    move-result v0

    iget-object v3, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v3}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object v3

    array-length v3, v3

    if-ge v0, v3, :cond_46

    .line 1846
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object v0

    iget-object v3, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v3}, Lepson/print/screen/SettingScr;->access$1200(Lepson/print/screen/SettingScr;)I

    move-result v3

    aget v0, v0, v3

    if-ne p1, v0, :cond_45

    .line 1847
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0, p1}, Lepson/print/screen/SettingScr;->access$1302(Lepson/print/screen/SettingScr;I)I

    goto :goto_2f

    .line 1845
    :cond_45
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$1208(Lepson/print/screen/SettingScr;)I

    goto :goto_2e

    .line 1854
    :cond_46
    :goto_2f
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1200(Lepson/print/screen/SettingScr;)I

    move-result p1

    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object v0

    array-length v0, v0

    if-lt p1, v0, :cond_47

    .line 1855
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1, v1}, Lepson/print/screen/SettingScr;->access$1202(Lepson/print/screen/SettingScr;I)I

    .line 1856
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object v0

    iget-object v1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v1}, Lepson/print/screen/SettingScr;->access$1200(Lepson/print/screen/SettingScr;)I

    move-result v1

    aget v0, v0, v1

    invoke-static {p1, v0}, Lepson/print/screen/SettingScr;->access$1302(Lepson/print/screen/SettingScr;I)I

    .line 1859
    :cond_47
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    invoke-static {p1, v0}, Lepson/print/screen/SettingScr;->access$1402(Lepson/print/screen/SettingScr;[I)[I

    .line 1860
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1100(Lepson/print/screen/SettingScr;)[I

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/screen/SettingScr;->access$1402(Lepson/print/screen/SettingScr;[I)[I

    .line 1862
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    new-instance v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize;-><init>()V

    invoke-static {p1, v0}, Lepson/print/screen/SettingScr;->access$1502(Lepson/print/screen/SettingScr;Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 1863
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1600(Lepson/print/screen/SettingScr;)Landroid/widget/TextView;

    move-result-object p1

    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$1500(Lepson/print/screen/SettingScr;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    move-result-object v1

    iget-object v3, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v3}, Lepson/print/screen/SettingScr;->access$1300(Lepson/print/screen/SettingScr;)I

    move-result v3

    invoke-virtual {v1, v3}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lepson/print/screen/SettingScr;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1864
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1500(Lepson/print/screen/SettingScr;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    .line 1865
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    iget-object p1, p1, Lepson/print/screen/SettingScr;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v10}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_39

    .line 1728
    :pswitch_8
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$600(Lepson/print/screen/SettingScr;)Lepson/print/service/IEpsonService;

    move-result-object p1

    if-eqz p1, :cond_48

    .line 1729
    new-instance p1, Lepson/print/screen/SettingScr$3$1;

    invoke-direct {p1, p0}, Lepson/print/screen/SettingScr$3$1;-><init>(Lepson/print/screen/SettingScr$3;)V

    new-array v0, v1, [Ljava/lang/Void;

    .line 1790
    invoke-virtual {p1, v0}, Lepson/print/screen/SettingScr$3$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_39

    .line 1793
    :cond_48
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    iget-object p1, p1, Lepson/print/screen/SettingScr;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v1, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_39

    .line 2330
    :cond_49
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    const v0, 0x7f080245

    invoke-virtual {p1, v0}, Lepson/print/screen/SettingScr;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v3}, Lepson/print/screen/SettingScr;->access$3600(Lepson/print/screen/SettingScr;)I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, ".."

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v3}, Lepson/print/screen/SettingScr;->access$3700(Lepson/print/screen/SettingScr;)I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2333
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    new-instance v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$FeedDirection;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$FeedDirection;-><init>()V

    invoke-static {p1, v0}, Lepson/print/screen/SettingScr;->access$1502(Lepson/print/screen/SettingScr;Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 2334
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$3900(Lepson/print/screen/SettingScr;)Landroid/widget/TextView;

    move-result-object p1

    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    .line 2335
    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$1500(Lepson/print/screen/SettingScr;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    move-result-object v3

    iget-object v4, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v4}, Lepson/print/screen/SettingScr;->access$3800(Lepson/print/screen/SettingScr;)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result v3

    invoke-virtual {v0, v3}, Lepson/print/screen/SettingScr;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2336
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1500(Lepson/print/screen/SettingScr;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    .line 2339
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    new-instance v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PrintDate;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PrintDate;-><init>()V

    invoke-static {p1, v0}, Lepson/print/screen/SettingScr;->access$1502(Lepson/print/screen/SettingScr;Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 2340
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$4100(Lepson/print/screen/SettingScr;)Landroid/widget/TextView;

    move-result-object p1

    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$1500(Lepson/print/screen/SettingScr;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    move-result-object v3

    iget-object v4, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v4}, Lepson/print/screen/SettingScr;->access$4000(Lepson/print/screen/SettingScr;)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result v3

    invoke-virtual {v0, v3}, Lepson/print/screen/SettingScr;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2341
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1500(Lepson/print/screen/SettingScr;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    .line 2344
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$4300(Lepson/print/screen/SettingScr;)Landroid/widget/TextView;

    move-result-object p1

    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$4200(Lepson/print/screen/SettingScr;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2345
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$4400(Lepson/print/screen/SettingScr;)Landroid/widget/Button;

    move-result-object p1

    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$4200(Lepson/print/screen/SettingScr;)I

    move-result v0

    const/16 v3, -0x32

    if-eq v0, v3, :cond_4a

    const/4 v0, 0x1

    goto :goto_30

    :cond_4a
    const/4 v0, 0x0

    :goto_30
    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 2346
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$4500(Lepson/print/screen/SettingScr;)Landroid/widget/Button;

    move-result-object p1

    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$4200(Lepson/print/screen/SettingScr;)I

    move-result v0

    const/16 v4, 0x32

    if-eq v0, v4, :cond_4b

    const/4 v0, 0x1

    goto :goto_31

    :cond_4b
    const/4 v0, 0x0

    :goto_31
    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 2349
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$4700(Lepson/print/screen/SettingScr;)Landroid/widget/TextView;

    move-result-object p1

    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$4600(Lepson/print/screen/SettingScr;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2350
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$4800(Lepson/print/screen/SettingScr;)Landroid/widget/Button;

    move-result-object p1

    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$4600(Lepson/print/screen/SettingScr;)I

    move-result v0

    if-eq v0, v3, :cond_4c

    const/4 v0, 0x1

    goto :goto_32

    :cond_4c
    const/4 v0, 0x0

    :goto_32
    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 2351
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$4900(Lepson/print/screen/SettingScr;)Landroid/widget/Button;

    move-result-object p1

    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$4600(Lepson/print/screen/SettingScr;)I

    move-result v0

    if-eq v0, v4, :cond_4d

    const/4 v0, 0x1

    goto :goto_33

    :cond_4d
    const/4 v0, 0x0

    :goto_33
    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 2354
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$5100(Lepson/print/screen/SettingScr;)Landroid/widget/TextView;

    move-result-object p1

    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$5000(Lepson/print/screen/SettingScr;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2355
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$5200(Lepson/print/screen/SettingScr;)Landroid/widget/Button;

    move-result-object p1

    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$5000(Lepson/print/screen/SettingScr;)I

    move-result v0

    if-eq v0, v3, :cond_4e

    const/4 v0, 0x1

    goto :goto_34

    :cond_4e
    const/4 v0, 0x0

    :goto_34
    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 2356
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$5300(Lepson/print/screen/SettingScr;)Landroid/widget/Button;

    move-result-object p1

    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$5000(Lepson/print/screen/SettingScr;)I

    move-result v0

    if-eq v0, v4, :cond_4f

    const/4 v1, 0x1

    :cond_4f
    invoke-virtual {p1, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 2359
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-virtual {p1}, Lepson/print/screen/SettingScr;->updateSettingView()V

    .line 2360
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/screen/SettingScr;->access$700(Lepson/print/screen/SettingScr;Ljava/lang/Boolean;)V

    .line 2361
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    iget-boolean v0, p1, Lepson/print/screen/SettingScr;->undoFlag:Z

    invoke-static {p1, v0}, Lepson/print/screen/SettingScr;->access$5400(Lepson/print/screen/SettingScr;Z)Z

    goto/16 :goto_39

    .line 2260
    :cond_50
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$600(Lepson/print/screen/SettingScr;)Lepson/print/service/IEpsonService;

    move-result-object p1

    if-nez p1, :cond_51

    .line 2261
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    iget-object p1, p1, Lepson/print/screen/SettingScr;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v5, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_39

    :cond_51
    const-string p1, "SettingScr"

    const-string v0, "GET_DUPLEX"

    .line 2264
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2265
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1, v7}, Lepson/print/screen/SettingScr;->access$3302(Lepson/print/screen/SettingScr;[I)[I

    .line 2269
    :try_start_7
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$600(Lepson/print/screen/SettingScr;)Lepson/print/service/IEpsonService;

    move-result-object p1

    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$1200(Lepson/print/screen/SettingScr;)I

    move-result v0

    iget-object v3, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v3}, Lepson/print/screen/SettingScr;->access$1700(Lepson/print/screen/SettingScr;)I

    move-result v3

    invoke-interface {p1, v0, v3}, Lepson/print/service/IEpsonService;->getDuplex(II)I

    move-result p1
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_7} :catch_7

    goto :goto_35

    :catch_7
    move-exception p1

    .line 2271
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    iput-boolean v2, v0, Lepson/print/screen/SettingScr;->undoFlag:Z

    .line 2272
    invoke-virtual {p1}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 p1, 0x0

    .line 2275
    :goto_35
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$200(Lepson/print/screen/SettingScr;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_52

    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$1800(Lepson/print/screen/SettingScr;)I

    move-result v0

    sget-object v3, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PLAIN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v3}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v3

    if-ne v0, v3, :cond_52

    .line 2279
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    new-array v0, v6, [I

    invoke-static {p1, v0}, Lepson/print/screen/SettingScr;->access$3302(Lepson/print/screen/SettingScr;[I)[I

    .line 2280
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$3300(Lepson/print/screen/SettingScr;)[I

    move-result-object p1

    aput v1, p1, v1

    .line 2281
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$3300(Lepson/print/screen/SettingScr;)[I

    move-result-object p1

    aput v2, p1, v2

    .line 2282
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$3300(Lepson/print/screen/SettingScr;)[I

    move-result-object p1

    aput v10, p1, v10

    goto/16 :goto_36

    :cond_52
    if-eqz p1, :cond_55

    .line 2284
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$2800(Lepson/print/screen/SettingScr;)I

    move-result v0

    const/16 v3, 0x10

    if-eq v0, v3, :cond_55

    .line 2285
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$2100(Lepson/print/screen/SettingScr;)I

    move-result v0

    if-ne v0, v2, :cond_54

    and-int/2addr p1, v10

    if-eqz p1, :cond_53

    .line 2287
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    new-array v0, v6, [I

    invoke-static {p1, v0}, Lepson/print/screen/SettingScr;->access$3302(Lepson/print/screen/SettingScr;[I)[I

    .line 2288
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$3300(Lepson/print/screen/SettingScr;)[I

    move-result-object p1

    aput v1, p1, v1

    .line 2289
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$3300(Lepson/print/screen/SettingScr;)[I

    move-result-object p1

    aput v2, p1, v2

    .line 2290
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$3300(Lepson/print/screen/SettingScr;)[I

    move-result-object p1

    aput v10, p1, v10

    goto :goto_36

    .line 2292
    :cond_53
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    new-array v0, v2, [I

    invoke-static {p1, v0}, Lepson/print/screen/SettingScr;->access$3302(Lepson/print/screen/SettingScr;[I)[I

    .line 2293
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$3300(Lepson/print/screen/SettingScr;)[I

    move-result-object p1

    aput v1, p1, v1

    goto :goto_36

    .line 2296
    :cond_54
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    new-array v0, v6, [I

    invoke-static {p1, v0}, Lepson/print/screen/SettingScr;->access$3302(Lepson/print/screen/SettingScr;[I)[I

    .line 2297
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$3300(Lepson/print/screen/SettingScr;)[I

    move-result-object p1

    aput v1, p1, v1

    .line 2298
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$3300(Lepson/print/screen/SettingScr;)[I

    move-result-object p1

    aput v2, p1, v2

    .line 2299
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$3300(Lepson/print/screen/SettingScr;)[I

    move-result-object p1

    aput v10, p1, v10

    goto :goto_36

    .line 2302
    :cond_55
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    new-array v0, v2, [I

    invoke-static {p1, v0}, Lepson/print/screen/SettingScr;->access$3302(Lepson/print/screen/SettingScr;[I)[I

    .line 2303
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$3300(Lepson/print/screen/SettingScr;)[I

    move-result-object p1

    aput v1, p1, v1

    .line 2306
    :goto_36
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$3300(Lepson/print/screen/SettingScr;)[I

    move-result-object p1

    if-eqz p1, :cond_59

    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$3300(Lepson/print/screen/SettingScr;)[I

    move-result-object p1

    array-length p1, p1

    if-lez p1, :cond_59

    const/4 p1, 0x0

    .line 2308
    :goto_37
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$3300(Lepson/print/screen/SettingScr;)[I

    move-result-object v0

    array-length v0, v0

    if-ge p1, v0, :cond_57

    .line 2309
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$3400(Lepson/print/screen/SettingScr;)I

    move-result v0

    iget-object v3, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v3}, Lepson/print/screen/SettingScr;->access$3300(Lepson/print/screen/SettingScr;)[I

    move-result-object v3

    aget v3, v3, p1

    if-ne v0, v3, :cond_56

    goto :goto_38

    :cond_56
    add-int/lit8 p1, p1, 0x1

    goto :goto_37

    .line 2313
    :cond_57
    :goto_38
    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$3300(Lepson/print/screen/SettingScr;)[I

    move-result-object v0

    array-length v0, v0

    if-lt p1, v0, :cond_58

    .line 2315
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$3300(Lepson/print/screen/SettingScr;)[I

    move-result-object v0

    aget v0, v0, v1

    invoke-static {p1, v0}, Lepson/print/screen/SettingScr;->access$3402(Lepson/print/screen/SettingScr;I)I

    .line 2317
    :cond_58
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    new-instance v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$Duplex;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$Duplex;-><init>()V

    invoke-static {p1, v0}, Lepson/print/screen/SettingScr;->access$1502(Lepson/print/screen/SettingScr;Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    .line 2318
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$3500(Lepson/print/screen/SettingScr;)Landroid/widget/TextView;

    move-result-object p1

    iget-object v0, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$1500(Lepson/print/screen/SettingScr;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    move-result-object v1

    iget-object v3, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v3}, Lepson/print/screen/SettingScr;->access$3400(Lepson/print/screen/SettingScr;)I

    move-result v3

    invoke-virtual {v1, v3}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->getStringId(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lepson/print/screen/SettingScr;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2319
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$1500(Lepson/print/screen/SettingScr;)Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$AbstractInfo;->destructor()V

    :cond_59
    const-string p1, "SettingScr"

    const-string v0, "get Color setScreenState = true"

    .line 2323
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2324
    iget-object p1, p0, Lepson/print/screen/SettingScr$3;->this$0:Lepson/print/screen/SettingScr;

    iget-object p1, p1, Lepson/print/screen/SettingScr;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_5a
    :goto_39
    return v2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x11
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :array_0
    .array-data 4
        0x2
        0x1
    .end array-data
.end method

.class Lepson/print/screen/SettingScr$2;
.super Ljava/lang/Object;
.source "SettingScr.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/screen/SettingScr;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/screen/SettingScr;


# direct methods
.method constructor <init>(Lepson/print/screen/SettingScr;)V
    .locals 0

    .line 1621
    iput-object p1, p0, Lepson/print/screen/SettingScr$2;->this$0:Lepson/print/screen/SettingScr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 0

    .line 1634
    iget-object p1, p0, Lepson/print/screen/SettingScr$2;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p2}, Lepson/print/service/IEpsonService$Stub;->asInterface(Landroid/os/IBinder;)Lepson/print/service/IEpsonService;

    move-result-object p2

    invoke-static {p1, p2}, Lepson/print/screen/SettingScr;->access$602(Lepson/print/screen/SettingScr;Lepson/print/service/IEpsonService;)Lepson/print/service/IEpsonService;

    const-string p1, "SettingScr"

    const-string p2, "onServiceConnected"

    .line 1638
    invoke-static {p1, p2}, Lepson/print/Util/EPLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1639
    iget-object p1, p0, Lepson/print/screen/SettingScr$2;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$600(Lepson/print/screen/SettingScr;)Lepson/print/service/IEpsonService;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 1641
    :try_start_0
    iget-object p1, p0, Lepson/print/screen/SettingScr$2;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$600(Lepson/print/screen/SettingScr;)Lepson/print/service/IEpsonService;

    move-result-object p1

    iget-object p2, p0, Lepson/print/screen/SettingScr$2;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p2}, Lepson/print/screen/SettingScr;->access$500(Lepson/print/screen/SettingScr;)Lepson/print/service/IEpsonServiceCallback;

    move-result-object p2

    invoke-interface {p1, p2}, Lepson/print/service/IEpsonService;->registerCallback(Lepson/print/service/IEpsonServiceCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 1643
    invoke-virtual {p1}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_0
    :goto_0
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 1

    const-string p1, "SettingScr"

    const-string v0, "onServiceDisconnected"

    .line 1624
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1626
    :try_start_0
    iget-object p1, p0, Lepson/print/screen/SettingScr$2;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {p1}, Lepson/print/screen/SettingScr;->access$600(Lepson/print/screen/SettingScr;)Lepson/print/service/IEpsonService;

    move-result-object p1

    iget-object v0, p0, Lepson/print/screen/SettingScr$2;->this$0:Lepson/print/screen/SettingScr;

    invoke-static {v0}, Lepson/print/screen/SettingScr;->access$500(Lepson/print/screen/SettingScr;)Lepson/print/service/IEpsonServiceCallback;

    move-result-object v0

    invoke-interface {p1, v0}, Lepson/print/service/IEpsonService;->unregisterCallback(Lepson/print/service/IEpsonServiceCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 1628
    invoke-virtual {p1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 1630
    :goto_0
    iget-object p1, p0, Lepson/print/screen/SettingScr$2;->this$0:Lepson/print/screen/SettingScr;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lepson/print/screen/SettingScr;->access$602(Lepson/print/screen/SettingScr;Lepson/print/service/IEpsonService;)Lepson/print/service/IEpsonService;

    return-void
.end method

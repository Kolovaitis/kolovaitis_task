.class Lepson/print/screen/SearchPrinterScr$10;
.super Ljava/lang/Object;
.source "SearchPrinterScr.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/screen/SearchPrinterScr;->buildElements()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/screen/SearchPrinterScr;


# direct methods
.method constructor <init>(Lepson/print/screen/SearchPrinterScr;)V
    .locals 0

    .line 604
    iput-object p1, p0, Lepson/print/screen/SearchPrinterScr$10;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1

    .line 607
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$10;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {p1}, Lepson/print/screen/SearchPrinterScr;->access$200(Lepson/print/screen/SearchPrinterScr;)I

    move-result p1

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    return-void

    .line 610
    :cond_0
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$10;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {p1, v0}, Lepson/print/screen/SearchPrinterScr;->access$202(Lepson/print/screen/SearchPrinterScr;I)I

    .line 611
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$10;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {p1}, Lepson/print/screen/SearchPrinterScr;->access$300(Lepson/print/screen/SearchPrinterScr;)V

    .line 613
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$10;->this$0:Lepson/print/screen/SearchPrinterScr;

    iget-object p1, p1, Lepson/print/screen/SearchPrinterScr;->mLayout:Landroid/view/ViewGroup;

    const v0, 0x7f080123

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 617
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$10;->this$0:Lepson/print/screen/SearchPrinterScr;

    iget-object p1, p1, Lepson/print/screen/SearchPrinterScr;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {p1}, Lepson/print/widgets/AbstractListBuilder;->refresh()V

    .line 618
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$10;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-static {p1}, Lepson/print/screen/SearchPrinterScr;->access$700(Lepson/print/screen/SearchPrinterScr;)V

    .line 619
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$10;->this$0:Lepson/print/screen/SearchPrinterScr;

    invoke-virtual {p1}, Lepson/print/screen/SearchPrinterScr;->displaySearchResult()V

    .line 622
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr$10;->this$0:Lepson/print/screen/SearchPrinterScr;

    iget-object p1, p1, Lepson/print/screen/SearchPrinterScr;->mSearchButton:Landroid/widget/Button;

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setVisibility(I)V

    return-void
.end method

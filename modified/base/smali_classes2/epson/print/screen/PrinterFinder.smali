.class public Lepson/print/screen/PrinterFinder;
.super Lepson/print/ActivityIACommon;
.source "PrinterFinder.java"


# static fields
.field private static final DELAY:I = 0x64

.field private static final DIALOG_INFORM:I = 0x0

.field private static foundPrinter:Z = false

.field private static final mLock:Ljava/lang/Object;


# instance fields
.field private final CANCEL_FIND_PRINTER:I

.field private final DISPLAY_FIND_RESULT:I

.field private final INFORM_DIALOG:I

.field private final PRINTER_COMMON_DEVICENAME:Ljava/lang/String;

.field private final PRINTER_ID:Ljava/lang/String;

.field private final PRINTER_INDEX:Ljava/lang/String;

.field private final PRINTER_IP:Ljava/lang/String;

.field private final PRINTER_NAME:Ljava/lang/String;

.field private final SEARCH_PRINTER:I

.field private final SELECT_PRINTER:I

.field private final UPDATE_PRINTER:I

.field curError:I

.field private isDialogOpen:Z

.field private isFinishSearchPrinter:Z

.field private isFocused:Ljava/lang/Boolean;

.field mBuilder:Lepson/print/widgets/AbstractListBuilder;

.field private mCallback:Lepson/print/service/IEpsonServiceCallback;

.field mContext:Landroid/content/Context;

.field private mEpsonConnection:Landroid/content/ServiceConnection;

.field private mEpsonService:Lepson/print/service/IEpsonService;

.field mHandler:Landroid/os/Handler;

.field mIsClickSelect:Z

.field mLayout:Landroid/view/ViewGroup;

.field mProgressBar:Landroid/widget/ProgressBar;

.field mSearchButton:Landroid/widget/Button;

.field private printerList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 56
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lepson/print/screen/PrinterFinder;->mLock:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 48
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    const/4 v0, 0x0

    .line 54
    iput-boolean v0, p0, Lepson/print/screen/PrinterFinder;->mIsClickSelect:Z

    .line 57
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lepson/print/screen/PrinterFinder;->printerList:Ljava/util/ArrayList;

    const/4 v1, 0x1

    .line 86
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lepson/print/screen/PrinterFinder;->isFocused:Ljava/lang/Boolean;

    const/4 v2, 0x0

    .line 164
    iput-object v2, p0, Lepson/print/screen/PrinterFinder;->mEpsonService:Lepson/print/service/IEpsonService;

    .line 165
    new-instance v2, Lepson/print/screen/PrinterFinder$3;

    invoke-direct {v2, p0}, Lepson/print/screen/PrinterFinder$3;-><init>(Lepson/print/screen/PrinterFinder;)V

    iput-object v2, p0, Lepson/print/screen/PrinterFinder;->mEpsonConnection:Landroid/content/ServiceConnection;

    .line 190
    new-instance v2, Lepson/print/screen/PrinterFinder$4;

    invoke-direct {v2, p0}, Lepson/print/screen/PrinterFinder$4;-><init>(Lepson/print/screen/PrinterFinder;)V

    iput-object v2, p0, Lepson/print/screen/PrinterFinder;->mCallback:Lepson/print/service/IEpsonServiceCallback;

    const-string v2, "name"

    .line 304
    iput-object v2, p0, Lepson/print/screen/PrinterFinder;->PRINTER_NAME:Ljava/lang/String;

    const-string v2, "ip"

    .line 305
    iput-object v2, p0, Lepson/print/screen/PrinterFinder;->PRINTER_IP:Ljava/lang/String;

    const-string v2, "id"

    .line 306
    iput-object v2, p0, Lepson/print/screen/PrinterFinder;->PRINTER_ID:Ljava/lang/String;

    const-string v2, "index"

    .line 307
    iput-object v2, p0, Lepson/print/screen/PrinterFinder;->PRINTER_INDEX:Ljava/lang/String;

    const-string v2, "common_devicename"

    .line 308
    iput-object v2, p0, Lepson/print/screen/PrinterFinder;->PRINTER_COMMON_DEVICENAME:Ljava/lang/String;

    .line 309
    iput v1, p0, Lepson/print/screen/PrinterFinder;->SEARCH_PRINTER:I

    const/4 v1, 0x2

    .line 310
    iput v1, p0, Lepson/print/screen/PrinterFinder;->CANCEL_FIND_PRINTER:I

    const/4 v1, 0x3

    .line 311
    iput v1, p0, Lepson/print/screen/PrinterFinder;->UPDATE_PRINTER:I

    const/4 v1, 0x4

    .line 312
    iput v1, p0, Lepson/print/screen/PrinterFinder;->SELECT_PRINTER:I

    const/4 v1, 0x5

    .line 313
    iput v1, p0, Lepson/print/screen/PrinterFinder;->INFORM_DIALOG:I

    const/4 v1, 0x6

    .line 314
    iput v1, p0, Lepson/print/screen/PrinterFinder;->DISPLAY_FIND_RESULT:I

    .line 316
    iput-boolean v0, p0, Lepson/print/screen/PrinterFinder;->isFinishSearchPrinter:Z

    .line 318
    new-instance v1, Landroid/os/Handler;

    new-instance v2, Lepson/print/screen/PrinterFinder$5;

    invoke-direct {v2, p0}, Lepson/print/screen/PrinterFinder$5;-><init>(Lepson/print/screen/PrinterFinder;)V

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v1, p0, Lepson/print/screen/PrinterFinder;->mHandler:Landroid/os/Handler;

    .line 478
    iput-boolean v0, p0, Lepson/print/screen/PrinterFinder;->isDialogOpen:Z

    return-void
.end method

.method static synthetic access$000(Lepson/print/screen/PrinterFinder;)Ljava/lang/Boolean;
    .locals 0

    .line 48
    iget-object p0, p0, Lepson/print/screen/PrinterFinder;->isFocused:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic access$002(Lepson/print/screen/PrinterFinder;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0

    .line 48
    iput-object p1, p0, Lepson/print/screen/PrinterFinder;->isFocused:Ljava/lang/Boolean;

    return-object p1
.end method

.method static synthetic access$100()Ljava/lang/Object;
    .locals 1

    .line 48
    sget-object v0, Lepson/print/screen/PrinterFinder;->mLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$200(Lepson/print/screen/PrinterFinder;)Ljava/util/ArrayList;
    .locals 0

    .line 48
    iget-object p0, p0, Lepson/print/screen/PrinterFinder;->printerList:Ljava/util/ArrayList;

    return-object p0
.end method

.method static synthetic access$302(Lepson/print/screen/PrinterFinder;Z)Z
    .locals 0

    .line 48
    iput-boolean p1, p0, Lepson/print/screen/PrinterFinder;->isFinishSearchPrinter:Z

    return p1
.end method

.method static synthetic access$400(Lepson/print/screen/PrinterFinder;)Lepson/print/service/IEpsonServiceCallback;
    .locals 0

    .line 48
    iget-object p0, p0, Lepson/print/screen/PrinterFinder;->mCallback:Lepson/print/service/IEpsonServiceCallback;

    return-object p0
.end method

.method static synthetic access$500(Lepson/print/screen/PrinterFinder;)Lepson/print/service/IEpsonService;
    .locals 0

    .line 48
    iget-object p0, p0, Lepson/print/screen/PrinterFinder;->mEpsonService:Lepson/print/service/IEpsonService;

    return-object p0
.end method

.method static synthetic access$502(Lepson/print/screen/PrinterFinder;Lepson/print/service/IEpsonService;)Lepson/print/service/IEpsonService;
    .locals 0

    .line 48
    iput-object p1, p0, Lepson/print/screen/PrinterFinder;->mEpsonService:Lepson/print/service/IEpsonService;

    return-object p1
.end method

.method static synthetic access$600()Z
    .locals 1

    .line 48
    sget-boolean v0, Lepson/print/screen/PrinterFinder;->foundPrinter:Z

    return v0
.end method

.method static synthetic access$602(Z)Z
    .locals 0

    .line 48
    sput-boolean p0, Lepson/print/screen/PrinterFinder;->foundPrinter:Z

    return p0
.end method

.method static synthetic access$700(Lepson/print/screen/PrinterFinder;)Z
    .locals 0

    .line 48
    iget-boolean p0, p0, Lepson/print/screen/PrinterFinder;->isDialogOpen:Z

    return p0
.end method

.method static synthetic access$702(Lepson/print/screen/PrinterFinder;Z)Z
    .locals 0

    .line 48
    iput-boolean p1, p0, Lepson/print/screen/PrinterFinder;->isDialogOpen:Z

    return p1
.end method

.method private buildElements()V
    .locals 4

    .line 88
    iget-object v0, p0, Lepson/print/screen/PrinterFinder;->mLayout:Landroid/view/ViewGroup;

    const v1, 0x7f0802b8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 92
    iget-object v0, p0, Lepson/print/screen/PrinterFinder;->mLayout:Landroid/view/ViewGroup;

    const v2, 0x7f08015c

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lepson/print/screen/PrinterFinder;->mSearchButton:Landroid/widget/Button;

    .line 93
    iget-object v0, p0, Lepson/print/screen/PrinterFinder;->mLayout:Landroid/view/ViewGroup;

    const v2, 0x7f080298

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lepson/print/screen/PrinterFinder;->mProgressBar:Landroid/widget/ProgressBar;

    const/4 v0, 0x0

    .line 97
    invoke-virtual {p0, v0}, Lepson/print/screen/PrinterFinder;->searchButtonSetEnabled(Z)V

    .line 99
    iget-object v2, p0, Lepson/print/screen/PrinterFinder;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 102
    iget-object v2, p0, Lepson/print/screen/PrinterFinder;->mLayout:Landroid/view/ViewGroup;

    const v3, 0x7f080071

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 103
    iget-object v2, p0, Lepson/print/screen/PrinterFinder;->mLayout:Landroid/view/ViewGroup;

    const v3, 0x7f08022b

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 105
    new-instance v1, Lepson/print/widgets/PrinterInfoBuilder;

    invoke-virtual {p0}, Lepson/print/screen/PrinterFinder;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lepson/print/screen/PrinterFinder;->mLayout:Landroid/view/ViewGroup;

    invoke-direct {v1, v2, v3, v0}, Lepson/print/widgets/PrinterInfoBuilder;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;I)V

    iput-object v1, p0, Lepson/print/screen/PrinterFinder;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    .line 106
    iget-object v0, p0, Lepson/print/screen/PrinterFinder;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {p0}, Lepson/print/screen/PrinterFinder;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "PRINTER_NAME"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lepson/print/widgets/AbstractListBuilder;->setResource(Ljava/lang/Object;)V

    .line 108
    iget-object v0, p0, Lepson/print/screen/PrinterFinder;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {v0}, Lepson/print/widgets/AbstractListBuilder;->build()V

    .line 109
    iget-object v0, p0, Lepson/print/screen/PrinterFinder;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {v0}, Lepson/print/widgets/AbstractListBuilder;->refresh()V

    .line 111
    iget-object v0, p0, Lepson/print/screen/PrinterFinder;->mLayout:Landroid/view/ViewGroup;

    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    new-instance v1, Lepson/print/screen/PrinterFinder$1;

    invoke-direct {v1, p0}, Lepson/print/screen/PrinterFinder$1;-><init>(Lepson/print/screen/PrinterFinder;)V

    .line 112
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 144
    iget-object v0, p0, Lepson/print/screen/PrinterFinder;->mSearchButton:Landroid/widget/Button;

    new-instance v1, Lepson/print/screen/PrinterFinder$2;

    invoke-direct {v1, p0}, Lepson/print/screen/PrinterFinder$2;-><init>(Lepson/print/screen/PrinterFinder;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    .line 275
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onBackPressed()V

    .line 278
    :try_start_0
    iget-object v0, p0, Lepson/print/screen/PrinterFinder;->mEpsonService:Lepson/print/service/IEpsonService;

    invoke-interface {v0}, Lepson/print/service/IEpsonService;->cancelSearchPrinter()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 281
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    :goto_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .line 65
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    .line 67
    iget-object p1, p0, Lepson/print/screen/PrinterFinder;->mEpsonService:Lepson/print/service/IEpsonService;

    const/4 v0, 0x1

    if-nez p1, :cond_0

    .line 68
    new-instance p1, Landroid/content/Intent;

    const-class v1, Lepson/print/service/EpsonService;

    invoke-direct {p1, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lepson/print/screen/PrinterFinder;->mEpsonConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, p1, v1, v0}, Lepson/print/screen/PrinterFinder;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 71
    :cond_0
    iget-object p1, p0, Lepson/print/screen/PrinterFinder;->printerList:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    .line 72
    invoke-virtual {p0}, Lepson/print/screen/PrinterFinder;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object p1

    const v1, 0x7f0a00b8

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, Lepson/print/screen/PrinterFinder;->mLayout:Landroid/view/ViewGroup;

    .line 74
    invoke-direct {p0}, Lepson/print/screen/PrinterFinder;->buildElements()V

    const/4 p1, 0x0

    .line 75
    iput-boolean p1, p0, Lepson/print/screen/PrinterFinder;->mIsClickSelect:Z

    .line 76
    iget-object p1, p0, Lepson/print/screen/PrinterFinder;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {p0, p1}, Lepson/print/screen/PrinterFinder;->setContentView(Landroid/view/View;)V

    .line 77
    iput-object p0, p0, Lepson/print/screen/PrinterFinder;->mContext:Landroid/content/Context;

    const p1, 0x7f0e0447

    .line 80
    invoke-virtual {p0, p1, v0}, Lepson/print/screen/PrinterFinder;->setActionBar(IZ)V

    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 6

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    const-string p1, "DIALOG_INFORM"

    .line 483
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "curError = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lepson/print/screen/PrinterFinder;->curError:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 484
    iget p1, p0, Lepson/print/screen/PrinterFinder;->curError:I

    const/16 v0, -0x44c

    const/16 v1, -0x2af9

    if-ne p1, v0, :cond_1

    if-eq p1, v1, :cond_3

    .line 487
    :cond_1
    invoke-virtual {p0}, Lepson/print/screen/PrinterFinder;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string v2, "PrintSetting"

    const-string v3, "RE_SEARCH"

    invoke-static {p1, v2, v3}, Lepson/common/Utils;->getPrefBoolean(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 490
    iput v1, p0, Lepson/print/screen/PrinterFinder;->curError:I

    goto :goto_0

    .line 494
    :cond_2
    iput v0, p0, Lepson/print/screen/PrinterFinder;->curError:I

    .line 497
    :cond_3
    :goto_0
    iget p1, p0, Lepson/print/screen/PrinterFinder;->curError:I

    invoke-static {p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$ErrorTable;->getStringId(I)[Ljava/lang/Integer;

    move-result-object p1

    const/4 v0, 0x1

    const/4 v2, 0x0

    if-nez p1, :cond_4

    const/4 p1, 0x2

    .line 499
    new-array p1, p1, [Ljava/lang/Integer;

    const v3, 0x7f0e023a

    .line 500
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, p1, v2

    const v3, 0x7f0e023b

    .line 501
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, p1, v0

    .line 504
    :cond_4
    aget-object v3, p1, v2

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p0, v3}, Lepson/print/screen/PrinterFinder;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 505
    invoke-static {p0}, Lepson/common/Utils;->getSSID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 506
    invoke-static {p0}, Lepson/common/Utils;->isConnectedWifi(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_5

    iget v5, p0, Lepson/print/screen/PrinterFinder;->curError:I

    if-ne v5, v1, :cond_5

    if-eqz v4, :cond_5

    const v1, 0x7f0e00bd

    .line 509
    invoke-virtual {p0, v1}, Lepson/print/screen/PrinterFinder;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v3, v0, [Ljava/lang/Object;

    aput-object v4, v3, v2

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 512
    :cond_5
    new-instance v1, Lepson/print/widgets/CustomTitleAlertDialogBuilder;

    invoke-direct {v1, p0}, Lepson/print/widgets/CustomTitleAlertDialogBuilder;-><init>(Landroid/content/Context;)V

    .line 513
    invoke-virtual {v1, v2}, Lepson/print/widgets/CustomTitleAlertDialogBuilder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    aget-object p1, p1, v0

    .line 514
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {p0, p1}, Lepson/print/screen/PrinterFinder;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 515
    invoke-virtual {p1, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const v0, 0x7f0e04f2

    .line 522
    invoke-virtual {p0, v0}, Lepson/print/screen/PrinterFinder;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lepson/print/screen/PrinterFinder$6;

    invoke-direct {v1, p0}, Lepson/print/screen/PrinterFinder$6;-><init>(Lepson/print/screen/PrinterFinder;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 528
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method

.method protected onDestroy()V
    .locals 4

    .line 257
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onDestroy()V

    .line 258
    iget-object v0, p0, Lepson/print/screen/PrinterFinder;->mEpsonService:Lepson/print/service/IEpsonService;

    if-eqz v0, :cond_1

    .line 259
    iget-boolean v0, p0, Lepson/print/screen/PrinterFinder;->isFinishSearchPrinter:Z

    if-nez v0, :cond_0

    .line 260
    iget-object v0, p0, Lepson/print/screen/PrinterFinder;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 262
    :cond_0
    :try_start_0
    iget-object v0, p0, Lepson/print/screen/PrinterFinder;->mEpsonService:Lepson/print/service/IEpsonService;

    iget-object v1, p0, Lepson/print/screen/PrinterFinder;->mCallback:Lepson/print/service/IEpsonServiceCallback;

    invoke-interface {v0, v1}, Lepson/print/service/IEpsonService;->unregisterCallback(Lepson/print/service/IEpsonServiceCallback;)V

    .line 263
    iget-object v0, p0, Lepson/print/screen/PrinterFinder;->mEpsonConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lepson/print/screen/PrinterFinder;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 265
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 268
    :cond_1
    :goto_0
    iget-object v0, p0, Lepson/print/screen/PrinterFinder;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {v0}, Lepson/print/widgets/AbstractListBuilder;->destructor()V

    return-void
.end method

.method protected onPause()V
    .locals 2

    const-string v0, "SearchPrinterScr"

    const-string v1, "onPause"

    .line 299
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 4

    .line 288
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onResume()V

    const-string v0, "Epson"

    const-string v1, "PrinterFinder.java call onResume()"

    .line 289
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    iget-object v0, p0, Lepson/print/screen/PrinterFinder;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    const-string v0, "PrinterFinder"

    const-string v1, "Send CHECK_PRINTER Message."

    .line 294
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public searchButtonSetEnabled(Z)V
    .locals 2

    .line 534
    iget-object v0, p0, Lepson/print/screen/PrinterFinder;->mSearchButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 537
    iget-object p1, p0, Lepson/print/screen/PrinterFinder;->mSearchButton:Landroid/widget/Button;

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 539
    :cond_0
    iget-object p1, p0, Lepson/print/screen/PrinterFinder;->mSearchButton:Landroid/widget/Button;

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setEnabled(Z)V

    :goto_0
    return-void
.end method

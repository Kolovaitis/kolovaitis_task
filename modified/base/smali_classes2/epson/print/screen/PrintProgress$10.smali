.class Lepson/print/screen/PrintProgress$10;
.super Ljava/lang/Object;
.source "PrintProgress.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/screen/PrintProgress;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/screen/PrintProgress;


# direct methods
.method constructor <init>(Lepson/print/screen/PrintProgress;)V
    .locals 0

    .line 1125
    iput-object p1, p0, Lepson/print/screen/PrintProgress$10;->this$0:Lepson/print/screen/PrintProgress;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1129
    iget-object p1, p0, Lepson/print/screen/PrintProgress$10;->this$0:Lepson/print/screen/PrintProgress;

    const/4 p2, 0x0

    invoke-static {p1, p2}, Lepson/print/screen/PrintProgress;->access$602(Lepson/print/screen/PrintProgress;Z)Z

    .line 1130
    iget-object p1, p0, Lepson/print/screen/PrintProgress$10;->this$0:Lepson/print/screen/PrintProgress;

    invoke-virtual {p1, p2}, Lepson/print/screen/PrintProgress;->removeDialog(I)V

    .line 1132
    :try_start_0
    invoke-static {}, Lepson/print/screen/PrintProgress;->access$800()Lepson/print/service/IEpsonService;

    move-result-object p1

    invoke-interface {p1, p2}, Lepson/print/service/IEpsonService;->confirmContinueable(Z)I

    .line 1133
    sput p2, Lepson/print/screen/PrintProgress;->curError:I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 1136
    invoke-virtual {p1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 1147
    :goto_0
    iget-object p1, p0, Lepson/print/screen/PrintProgress$10;->this$0:Lepson/print/screen/PrintProgress;

    const/4 p2, 0x1

    invoke-static {p1, p2}, Lepson/print/screen/PrintProgress;->access$1902(Lepson/print/screen/PrintProgress;Z)Z

    .line 1148
    iget-object p1, p0, Lepson/print/screen/PrintProgress$10;->this$0:Lepson/print/screen/PrintProgress;

    invoke-static {p1, p2}, Lepson/print/screen/PrintProgress;->access$302(Lepson/print/screen/PrintProgress;Z)Z

    const-string p1, "PrintProgress"

    const-string p2, "cont/cancel dialog. cancel clicked"

    .line 1149
    invoke-static {p1, p2}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1150
    iget-object p1, p0, Lepson/print/screen/PrintProgress$10;->this$0:Lepson/print/screen/PrintProgress;

    iget-object p1, p1, Lepson/print/screen/PrintProgress;->mHandler:Landroid/os/Handler;

    const/4 p2, 0x5

    invoke-virtual {p1, p2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.class Lepson/print/screen/PrinterFinder$5;
.super Ljava/lang/Object;
.source "PrinterFinder.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/screen/PrinterFinder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/screen/PrinterFinder;


# direct methods
.method constructor <init>(Lepson/print/screen/PrinterFinder;)V
    .locals 0

    .line 318
    iput-object p1, p0, Lepson/print/screen/PrinterFinder$5;->this$0:Lepson/print/screen/PrinterFinder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 6

    .line 320
    iget v0, p1, Landroid/os/Message;->what:I

    const v1, 0x7f080123

    const/4 v2, 0x1

    const/4 v3, 0x0

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_3

    .line 467
    :pswitch_0
    iget-object p1, p0, Lepson/print/screen/PrinterFinder$5;->this$0:Lepson/print/screen/PrinterFinder;

    invoke-static {p1}, Lepson/print/screen/PrinterFinder;->access$700(Lepson/print/screen/PrinterFinder;)Z

    move-result p1

    if-nez p1, :cond_7

    .line 468
    iget-object p1, p0, Lepson/print/screen/PrinterFinder$5;->this$0:Lepson/print/screen/PrinterFinder;

    invoke-static {p1, v2}, Lepson/print/screen/PrinterFinder;->access$702(Lepson/print/screen/PrinterFinder;Z)Z

    const-string p1, "mHandler"

    .line 469
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "curError = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lepson/print/screen/PrinterFinder$5;->this$0:Lepson/print/screen/PrinterFinder;

    iget v1, v1, Lepson/print/screen/PrinterFinder;->curError:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 470
    iget-object p1, p0, Lepson/print/screen/PrinterFinder$5;->this$0:Lepson/print/screen/PrinterFinder;

    invoke-virtual {p1, v3}, Lepson/print/screen/PrinterFinder;->showDialog(I)V

    goto/16 :goto_3

    .line 440
    :pswitch_1
    iget-object v0, p0, Lepson/print/screen/PrinterFinder$5;->this$0:Lepson/print/screen/PrinterFinder;

    invoke-static {v0}, Lepson/print/screen/PrinterFinder;->access$500(Lepson/print/screen/PrinterFinder;)Lepson/print/service/IEpsonService;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 442
    :try_start_0
    iget-object v0, p0, Lepson/print/screen/PrinterFinder$5;->this$0:Lepson/print/screen/PrinterFinder;

    invoke-static {v0}, Lepson/print/screen/PrinterFinder;->access$500(Lepson/print/screen/PrinterFinder;)Lepson/print/service/IEpsonService;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "index"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Lepson/print/service/IEpsonService;->setCurPrinter(I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 445
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 448
    :cond_0
    :goto_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 449
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "PRINTER_NAME"

    .line 450
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "name"

    .line 451
    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 450
    invoke-virtual {v1, v2, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "PRINTER_IP"

    .line 452
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "ip"

    .line 453
    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 452
    invoke-virtual {v1, v2, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "PRINTER_ID"

    .line 454
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "id"

    .line 455
    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 454
    invoke-virtual {v1, v2, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 457
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 458
    iget-object v1, p0, Lepson/print/screen/PrinterFinder$5;->this$0:Lepson/print/screen/PrinterFinder;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Lepson/print/screen/PrinterFinder;->setResult(ILandroid/content/Intent;)V

    const-string v0, "http"

    .line 459
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object p1

    const-string v1, "ip"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v1, "/PRESENTATION/EPSONCONNECT"

    invoke-static {v0, p1, v1}, Lepson/common/IPAddressUtils;->buildURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 461
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    .line 462
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 463
    iget-object p1, p0, Lepson/print/screen/PrinterFinder$5;->this$0:Lepson/print/screen/PrinterFinder;

    invoke-virtual {p1, v0}, Lepson/print/screen/PrinterFinder;->startActivity(Landroid/content/Intent;)V

    .line 464
    iget-object p1, p0, Lepson/print/screen/PrinterFinder$5;->this$0:Lepson/print/screen/PrinterFinder;

    invoke-virtual {p1}, Lepson/print/screen/PrinterFinder;->finish()V

    goto/16 :goto_3

    .line 350
    :pswitch_2
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "name"

    .line 351
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v4, "FINISH"

    .line 352
    invoke-virtual {v0, v4}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 353
    invoke-static {}, Lepson/print/screen/PrinterFinder;->access$100()Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 354
    :try_start_1
    iget-object v1, p0, Lepson/print/screen/PrinterFinder$5;->this$0:Lepson/print/screen/PrinterFinder;

    invoke-static {v1}, Lepson/print/screen/PrinterFinder;->access$200(Lepson/print/screen/PrinterFinder;)Ljava/util/ArrayList;

    move-result-object v1

    const-string v4, "id"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 355
    monitor-exit v0

    goto/16 :goto_3

    .line 358
    :cond_1
    new-instance v1, Lepson/print/screen/PrinterFinder$5$1;

    invoke-direct {v1, p0, p1}, Lepson/print/screen/PrinterFinder$5$1;-><init>(Lepson/print/screen/PrinterFinder$5;Landroid/os/Bundle;)V

    new-array p1, v3, [Ljava/lang/Void;

    .line 410
    invoke-virtual {v1, p1}, Lepson/print/screen/PrinterFinder$5$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 412
    invoke-static {v2}, Lepson/print/screen/PrinterFinder;->access$602(Z)Z

    .line 413
    monitor-exit v0

    goto/16 :goto_3

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1

    .line 414
    :cond_2
    iget-object p1, p0, Lepson/print/screen/PrinterFinder$5;->this$0:Lepson/print/screen/PrinterFinder;

    iget-boolean p1, p1, Lepson/print/screen/PrinterFinder;->mIsClickSelect:Z

    if-nez p1, :cond_7

    .line 415
    iget-object p1, p0, Lepson/print/screen/PrinterFinder$5;->this$0:Lepson/print/screen/PrinterFinder;

    iget-object p1, p1, Lepson/print/screen/PrinterFinder;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {p1}, Lepson/print/widgets/AbstractListBuilder;->getData()Ljava/util/Vector;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result p1

    if-lez p1, :cond_5

    .line 416
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object p1

    sget-object v0, Ljava/util/Locale;->JAPAN:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    const v0, 0x7f0e03fd

    if-nez p1, :cond_4

    .line 417
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object p1

    sget-object v4, Ljava/util/Locale;->JAPANESE:Ljava/util/Locale;

    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_3

    goto :goto_1

    .line 423
    :cond_3
    iget-object p1, p0, Lepson/print/screen/PrinterFinder$5;->this$0:Lepson/print/screen/PrinterFinder;

    iget-object p1, p1, Lepson/print/screen/PrinterFinder;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iget-object v1, p0, Lepson/print/screen/PrinterFinder$5;->this$0:Lepson/print/screen/PrinterFinder;

    .line 424
    invoke-virtual {v1, v0}, Lepson/print/screen/PrinterFinder;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/Object;

    iget-object v4, p0, Lepson/print/screen/PrinterFinder$5;->this$0:Lepson/print/screen/PrinterFinder;

    iget-object v4, v4, Lepson/print/screen/PrinterFinder;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {v4}, Lepson/print/widgets/AbstractListBuilder;->getData()Ljava/util/Vector;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 418
    :cond_4
    :goto_1
    iget-object p1, p0, Lepson/print/screen/PrinterFinder$5;->this$0:Lepson/print/screen/PrinterFinder;

    iget-object p1, p1, Lepson/print/screen/PrinterFinder;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iget-object v4, p0, Lepson/print/screen/PrinterFinder$5;->this$0:Lepson/print/screen/PrinterFinder;

    .line 419
    invoke-virtual {v4, v0}, Lepson/print/screen/PrinterFinder;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 420
    iget-object p1, p0, Lepson/print/screen/PrinterFinder$5;->this$0:Lepson/print/screen/PrinterFinder;

    iget-object p1, p1, Lepson/print/screen/PrinterFinder;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iget-object v1, p0, Lepson/print/screen/PrinterFinder$5;->this$0:Lepson/print/screen/PrinterFinder;

    .line 421
    invoke-virtual {v1, v0}, Lepson/print/screen/PrinterFinder;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 427
    :cond_5
    iget-object p1, p0, Lepson/print/screen/PrinterFinder$5;->this$0:Lepson/print/screen/PrinterFinder;

    iget-object p1, p1, Lepson/print/screen/PrinterFinder;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iget-object v0, p0, Lepson/print/screen/PrinterFinder$5;->this$0:Lepson/print/screen/PrinterFinder;

    const v1, 0x7f0e01b2

    .line 428
    invoke-virtual {v0, v1}, Lepson/print/screen/PrinterFinder;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 430
    :goto_2
    iget-object p1, p0, Lepson/print/screen/PrinterFinder$5;->this$0:Lepson/print/screen/PrinterFinder;

    iget-object p1, p1, Lepson/print/screen/PrinterFinder;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {p1}, Lepson/print/widgets/AbstractListBuilder;->getAdapter()Landroid/widget/BaseAdapter;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 431
    iget-object p1, p0, Lepson/print/screen/PrinterFinder$5;->this$0:Lepson/print/screen/PrinterFinder;

    iget-object p1, p1, Lepson/print/screen/PrinterFinder;->mProgressBar:Landroid/widget/ProgressBar;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 434
    iget-object p1, p0, Lepson/print/screen/PrinterFinder$5;->this$0:Lepson/print/screen/PrinterFinder;

    invoke-virtual {p1, v2}, Lepson/print/screen/PrinterFinder;->searchButtonSetEnabled(Z)V

    .line 436
    iget-object p1, p0, Lepson/print/screen/PrinterFinder$5;->this$0:Lepson/print/screen/PrinterFinder;

    invoke-static {p1, v2}, Lepson/print/screen/PrinterFinder;->access$302(Lepson/print/screen/PrinterFinder;Z)Z

    goto :goto_3

    .line 341
    :pswitch_3
    iget-object p1, p0, Lepson/print/screen/PrinterFinder$5;->this$0:Lepson/print/screen/PrinterFinder;

    invoke-static {p1}, Lepson/print/screen/PrinterFinder;->access$500(Lepson/print/screen/PrinterFinder;)Lepson/print/service/IEpsonService;

    move-result-object p1

    if-eqz p1, :cond_7

    .line 343
    :try_start_2
    iget-object p1, p0, Lepson/print/screen/PrinterFinder$5;->this$0:Lepson/print/screen/PrinterFinder;

    invoke-static {p1}, Lepson/print/screen/PrinterFinder;->access$500(Lepson/print/screen/PrinterFinder;)Lepson/print/service/IEpsonService;

    move-result-object p1

    invoke-interface {p1}, Lepson/print/service/IEpsonService;->cancelSearchPrinter()I
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_3

    :catch_1
    move-exception p1

    .line 345
    invoke-virtual {p1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_3

    .line 322
    :pswitch_4
    iget-object p1, p0, Lepson/print/screen/PrinterFinder$5;->this$0:Lepson/print/screen/PrinterFinder;

    invoke-static {p1}, Lepson/print/screen/PrinterFinder;->access$500(Lepson/print/screen/PrinterFinder;)Lepson/print/service/IEpsonService;

    move-result-object p1

    if-eqz p1, :cond_6

    .line 325
    :try_start_3
    iget-object p1, p0, Lepson/print/screen/PrinterFinder$5;->this$0:Lepson/print/screen/PrinterFinder;

    invoke-static {p1, v3}, Lepson/print/screen/PrinterFinder;->access$302(Lepson/print/screen/PrinterFinder;Z)Z

    .line 327
    iget-object p1, p0, Lepson/print/screen/PrinterFinder$5;->this$0:Lepson/print/screen/PrinterFinder;

    invoke-virtual {p1, v3}, Lepson/print/screen/PrinterFinder;->searchButtonSetEnabled(Z)V

    .line 328
    iget-object p1, p0, Lepson/print/screen/PrinterFinder$5;->this$0:Lepson/print/screen/PrinterFinder;

    iget-object p1, p1, Lepson/print/screen/PrinterFinder;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {p1, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 329
    iget-object p1, p0, Lepson/print/screen/PrinterFinder$5;->this$0:Lepson/print/screen/PrinterFinder;

    iget-object p1, p1, Lepson/print/screen/PrinterFinder;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iget-object v0, p0, Lepson/print/screen/PrinterFinder$5;->this$0:Lepson/print/screen/PrinterFinder;

    const v1, 0x7f0e045f

    .line 330
    invoke-virtual {v0, v1}, Lepson/print/screen/PrinterFinder;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 332
    iget-object p1, p0, Lepson/print/screen/PrinterFinder$5;->this$0:Lepson/print/screen/PrinterFinder;

    invoke-static {p1}, Lepson/print/screen/PrinterFinder;->access$500(Lepson/print/screen/PrinterFinder;)Lepson/print/service/IEpsonService;

    move-result-object p1

    const/4 v0, 0x0

    invoke-interface {p1, v0, v0, v2}, Lepson/print/service/IEpsonService;->searchPrinters(Ljava/lang/String;Ljava/lang/String;I)I
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_3

    :catch_2
    move-exception p1

    .line 334
    invoke-virtual {p1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_3

    .line 337
    :cond_6
    iget-object p1, p0, Lepson/print/screen/PrinterFinder$5;->this$0:Lepson/print/screen/PrinterFinder;

    iget-object p1, p1, Lepson/print/screen/PrinterFinder;->mHandler:Landroid/os/Handler;

    const-wide/16 v0, 0x64

    invoke-virtual {p1, v2, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_7
    :goto_3
    return v3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

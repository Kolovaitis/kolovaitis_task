.class Lepson/print/screen/ActivityPrinterSetting$LocalTextWatcher;
.super Ljava/lang/Object;
.source "ActivityPrinterSetting.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/screen/ActivityPrinterSetting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LocalTextWatcher"
.end annotation


# instance fields
.field private final INPUT_STATUS_IGNORE:I

.field private final INPUT_STATUS_NORMAL:I

.field private final INPUT_STATUS_SKIP:I

.field private inputStatus:I

.field final synthetic this$0:Lepson/print/screen/ActivityPrinterSetting;


# direct methods
.method private constructor <init>(Lepson/print/screen/ActivityPrinterSetting;)V
    .locals 1

    .line 807
    iput-object p1, p0, Lepson/print/screen/ActivityPrinterSetting$LocalTextWatcher;->this$0:Lepson/print/screen/ActivityPrinterSetting;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    .line 808
    iput p1, p0, Lepson/print/screen/ActivityPrinterSetting$LocalTextWatcher;->INPUT_STATUS_NORMAL:I

    const/4 v0, 0x1

    .line 809
    iput v0, p0, Lepson/print/screen/ActivityPrinterSetting$LocalTextWatcher;->INPUT_STATUS_SKIP:I

    const/4 v0, 0x2

    .line 810
    iput v0, p0, Lepson/print/screen/ActivityPrinterSetting$LocalTextWatcher;->INPUT_STATUS_IGNORE:I

    .line 812
    iput p1, p0, Lepson/print/screen/ActivityPrinterSetting$LocalTextWatcher;->inputStatus:I

    return-void
.end method

.method synthetic constructor <init>(Lepson/print/screen/ActivityPrinterSetting;Lepson/print/screen/ActivityPrinterSetting$1;)V
    .locals 0

    .line 807
    invoke-direct {p0, p1}, Lepson/print/screen/ActivityPrinterSetting$LocalTextWatcher;-><init>(Lepson/print/screen/ActivityPrinterSetting;)V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .line 825
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "@"

    .line 831
    iget p3, p0, Lepson/print/screen/ActivityPrinterSetting$LocalTextWatcher;->inputStatus:I

    if-nez p3, :cond_0

    invoke-virtual {p1, p2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p1

    const/4 p2, 0x1

    if-ne p1, p2, :cond_0

    const/4 p1, 0x2

    .line 832
    iput p1, p0, Lepson/print/screen/ActivityPrinterSetting$LocalTextWatcher;->inputStatus:I

    :cond_0
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .line 837
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    .line 839
    iget p3, p0, Lepson/print/screen/ActivityPrinterSetting$LocalTextWatcher;->inputStatus:I

    const/4 p4, 0x0

    const/4 v0, 0x1

    if-ne p3, v0, :cond_0

    .line 842
    iput p4, p0, Lepson/print/screen/ActivityPrinterSetting$LocalTextWatcher;->inputStatus:I

    return-void

    .line 844
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p3

    if-ge p2, p3, :cond_3

    invoke-virtual {p1, p2}, Ljava/lang/String;->charAt(I)C

    move-result p3

    const/16 v1, 0x40

    if-ne p3, v1, :cond_3

    .line 846
    iget p3, p0, Lepson/print/screen/ActivityPrinterSetting$LocalTextWatcher;->inputStatus:I

    const/4 v1, 0x2

    if-ne p3, v1, :cond_1

    .line 847
    iput v0, p0, Lepson/print/screen/ActivityPrinterSetting$LocalTextWatcher;->inputStatus:I

    .line 848
    iget-object p3, p0, Lepson/print/screen/ActivityPrinterSetting$LocalTextWatcher;->this$0:Lepson/print/screen/ActivityPrinterSetting;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, p4, p2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p4

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 p4, p2, 0x1

    invoke-virtual {p1, p4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p3, p1}, Lepson/print/screen/ActivityPrinterSetting;->access$802(Lepson/print/screen/ActivityPrinterSetting;Ljava/lang/String;)Ljava/lang/String;

    .line 849
    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting$LocalTextWatcher;->this$0:Lepson/print/screen/ActivityPrinterSetting;

    invoke-static {p1}, Lepson/print/screen/ActivityPrinterSetting;->access$200(Lepson/print/screen/ActivityPrinterSetting;)Landroid/widget/EditText;

    move-result-object p1

    iget-object p3, p0, Lepson/print/screen/ActivityPrinterSetting$LocalTextWatcher;->this$0:Lepson/print/screen/ActivityPrinterSetting;

    invoke-static {p3}, Lepson/print/screen/ActivityPrinterSetting;->access$800(Lepson/print/screen/ActivityPrinterSetting;)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 850
    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting$LocalTextWatcher;->this$0:Lepson/print/screen/ActivityPrinterSetting;

    invoke-static {p1}, Lepson/print/screen/ActivityPrinterSetting;->access$200(Lepson/print/screen/ActivityPrinterSetting;)Landroid/widget/EditText;

    move-result-object p1

    invoke-virtual {p1, p2}, Landroid/widget/EditText;->setSelection(I)V

    goto :goto_0

    :cond_1
    if-nez p3, :cond_2

    .line 854
    iput v0, p0, Lepson/print/screen/ActivityPrinterSetting$LocalTextWatcher;->inputStatus:I

    .line 855
    iget-object p3, p0, Lepson/print/screen/ActivityPrinterSetting$LocalTextWatcher;->this$0:Lepson/print/screen/ActivityPrinterSetting;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    add-int/2addr p2, v0

    invoke-virtual {p1, p4, p2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting$LocalTextWatcher;->this$0:Lepson/print/screen/ActivityPrinterSetting;

    invoke-static {p1}, Lepson/print/screen/ActivityPrinterSetting;->access$1100(Lepson/print/screen/ActivityPrinterSetting;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p3, p1}, Lepson/print/screen/ActivityPrinterSetting;->access$802(Lepson/print/screen/ActivityPrinterSetting;Ljava/lang/String;)Ljava/lang/String;

    .line 856
    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting$LocalTextWatcher;->this$0:Lepson/print/screen/ActivityPrinterSetting;

    invoke-static {p1}, Lepson/print/screen/ActivityPrinterSetting;->access$200(Lepson/print/screen/ActivityPrinterSetting;)Landroid/widget/EditText;

    move-result-object p1

    iget-object p2, p0, Lepson/print/screen/ActivityPrinterSetting$LocalTextWatcher;->this$0:Lepson/print/screen/ActivityPrinterSetting;

    invoke-static {p2}, Lepson/print/screen/ActivityPrinterSetting;->access$800(Lepson/print/screen/ActivityPrinterSetting;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 857
    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting$LocalTextWatcher;->this$0:Lepson/print/screen/ActivityPrinterSetting;

    invoke-static {p1}, Lepson/print/screen/ActivityPrinterSetting;->access$1200(Lepson/print/screen/ActivityPrinterSetting;)V

    goto :goto_0

    .line 859
    :cond_2
    iput p4, p0, Lepson/print/screen/ActivityPrinterSetting$LocalTextWatcher;->inputStatus:I

    goto :goto_0

    .line 862
    :cond_3
    iput p4, p0, Lepson/print/screen/ActivityPrinterSetting$LocalTextWatcher;->inputStatus:I

    :goto_0
    return-void
.end method

.method skipNextInputConversion()V
    .locals 1

    const/4 v0, 0x1

    .line 821
    iput v0, p0, Lepson/print/screen/ActivityPrinterSetting$LocalTextWatcher;->inputStatus:I

    return-void
.end method

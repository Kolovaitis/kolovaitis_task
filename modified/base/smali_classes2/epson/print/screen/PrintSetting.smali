.class public Lepson/print/screen/PrintSetting;
.super Ljava/lang/Object;
.source "PrintSetting.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/screen/PrintSetting$Kind;
    }
.end annotation


# static fields
.field private static final CAMERA_COPY_KEYS:[Ljava/lang/String;

.field public static final EMPTY_PRE_VALUE:I = -0x1

.field private static final TAG:Ljava/lang/String; = "PrintSetting"


# instance fields
.field allValueFromContentProvider:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public brightnessValue:I

.field public colorValue:I

.field context:Landroid/content/Context;

.field public contrastValue:I

.field public copiesValue:I

.field public duplexValue:I

.field public endValue:I

.field public feedDirectionValue:I

.field private kind:Lepson/print/screen/PrintSetting$Kind;

.field public layoutMultiPageValue:I

.field public layoutValue:I

.field public paperSizeValue:I

.field public paperSourceValue:I

.field public paperTypeValue:I

.field public photoApfValueForLocalPrinter:I

.field public printAll:Z

.field public printdate:I

.field public qualityValue:I

.field public saturationValue:I

.field settings:Landroid/content/SharedPreferences;

.field public sharpnessForPhoto:I

.field public startValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const-string v0, "PrintSizeTypeSet_CAMERACOPY"

    const-string v1, "PAPER_SOURCE_CAMERACOPY"

    const-string v2, "COLOR_CAMERACOPY"

    const-string v3, "LAYOUT_CAMERACOPY"

    const-string v4, "PAPER_TYPE_CAMERACOPY"

    const-string v5, "PAPER_SIZE_CAMERACOPY"

    const-string v6, "QUALITY_CAMERACOPY"

    const-string v7, "FEED_DIRECTION_CAMERACOPY"

    .line 157
    filled-new-array/range {v0 .. v7}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lepson/print/screen/PrintSetting;->CAMERA_COPY_KEYS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lepson/print/screen/PrintSetting$Kind;)V
    .locals 3

    .line 176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 121
    iput-object v0, p0, Lepson/print/screen/PrintSetting;->settings:Landroid/content/SharedPreferences;

    .line 123
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lepson/print/screen/PrintSetting;->allValueFromContentProvider:Ljava/util/Map;

    const/4 v1, 0x1

    .line 139
    iput-boolean v1, p0, Lepson/print/screen/PrintSetting;->printAll:Z

    .line 140
    iput v1, p0, Lepson/print/screen/PrintSetting;->startValue:I

    .line 141
    iput v1, p0, Lepson/print/screen/PrintSetting;->endValue:I

    const/4 v2, 0x0

    .line 143
    iput v2, p0, Lepson/print/screen/PrintSetting;->printdate:I

    .line 145
    iput v1, p0, Lepson/print/screen/PrintSetting;->copiesValue:I

    .line 146
    iput v2, p0, Lepson/print/screen/PrintSetting;->brightnessValue:I

    .line 147
    iput v2, p0, Lepson/print/screen/PrintSetting;->contrastValue:I

    .line 148
    iput v2, p0, Lepson/print/screen/PrintSetting;->saturationValue:I

    .line 151
    iput v2, p0, Lepson/print/screen/PrintSetting;->photoApfValueForLocalPrinter:I

    .line 154
    iput v2, p0, Lepson/print/screen/PrintSetting;->sharpnessForPhoto:I

    .line 178
    invoke-static {p1}, Lepson/provider/SharedPreferencesProvider;->checkPackageUseSharedPreferencesProvider(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 180
    iput-object v0, p0, Lepson/print/screen/PrintSetting;->context:Landroid/content/Context;

    .line 181
    iput-object v0, p0, Lepson/print/screen/PrintSetting;->settings:Landroid/content/SharedPreferences;

    goto :goto_0

    .line 183
    :cond_0
    iput-object p1, p0, Lepson/print/screen/PrintSetting;->context:Landroid/content/Context;

    const-string v0, "PrintSetting"

    .line 184
    invoke-virtual {p1, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    iput-object p1, p0, Lepson/print/screen/PrintSetting;->settings:Landroid/content/SharedPreferences;

    :goto_0
    if-eqz p2, :cond_1

    .line 187
    iput-object p2, p0, Lepson/print/screen/PrintSetting;->kind:Lepson/print/screen/PrintSetting$Kind;

    goto :goto_1

    .line 189
    :cond_1
    sget-object p1, Lepson/print/screen/PrintSetting$Kind;->NotSet:Lepson/print/screen/PrintSetting$Kind;

    iput-object p1, p0, Lepson/print/screen/PrintSetting;->kind:Lepson/print/screen/PrintSetting$Kind;

    :goto_1
    return-void
.end method

.method private getColor()I
    .locals 3

    .line 811
    sget-object v0, Lepson/print/screen/PrintSetting$1;->$SwitchMap$epson$print$screen$PrintSetting$Kind:[I

    iget-object v1, p0, Lepson/print/screen/PrintSetting;->kind:Lepson/print/screen/PrintSetting$Kind;

    invoke-virtual {v1}, Lepson/print/screen/PrintSetting$Kind;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x0

    const/4 v2, 0x0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const-string v0, "COLOR_CAMERACOPY"

    .line 825
    invoke-virtual {p0, v0, v1, v2}, Lepson/print/screen/PrintSetting;->getSetting(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v2

    goto :goto_0

    :pswitch_1
    const-string v0, "COLOR_PHOTO"

    .line 820
    invoke-virtual {p0, v0, v1, v2}, Lepson/print/screen/PrintSetting;->getSetting(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v2

    goto :goto_0

    :pswitch_2
    const-string v0, "COLOR"

    .line 814
    invoke-virtual {p0, v0, v1, v2}, Lepson/print/screen/PrintSetting;->getSetting(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v2

    :goto_0
    return v2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private getDuplex()I
    .locals 3

    .line 756
    sget-object v0, Lepson/print/screen/PrintSetting$1;->$SwitchMap$epson$print$screen$PrintSetting$Kind:[I

    iget-object v1, p0, Lepson/print/screen/PrintSetting;->kind:Lepson/print/screen/PrintSetting$Kind;

    invoke-virtual {v1}, Lepson/print/screen/PrintSetting$Kind;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x0

    const/4 v2, 0x0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const-string v0, "DUPLEX_PHOTO"

    .line 765
    invoke-virtual {p0, v0, v1, v2}, Lepson/print/screen/PrintSetting;->getSetting(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v2

    goto :goto_0

    :pswitch_1
    const-string v0, "DUPLEX"

    .line 759
    invoke-virtual {p0, v0, v1, v2}, Lepson/print/screen/PrintSetting;->getSetting(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v2

    :goto_0
    return v2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private getFeedDirection()I
    .locals 3

    .line 781
    sget-object v0, Lepson/print/screen/PrintSetting$1;->$SwitchMap$epson$print$screen$PrintSetting$Kind:[I

    iget-object v1, p0, Lepson/print/screen/PrintSetting;->kind:Lepson/print/screen/PrintSetting$Kind;

    invoke-virtual {v1}, Lepson/print/screen/PrintSetting$Kind;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x0

    const/4 v2, 0x0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const-string v0, "FEED_DIRECTION_CAMERACOPY"

    .line 795
    invoke-virtual {p0, v0, v1, v2}, Lepson/print/screen/PrintSetting;->getSetting(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v2

    goto :goto_0

    :pswitch_1
    const-string v0, "FEED_DIRECTION_PHOTO"

    .line 790
    invoke-virtual {p0, v0, v1, v2}, Lepson/print/screen/PrintSetting;->getSetting(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v2

    goto :goto_0

    :pswitch_2
    const-string v0, "FEED_DIRECTION"

    .line 784
    invoke-virtual {p0, v0, v1, v2}, Lepson/print/screen/PrintSetting;->getSetting(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v2

    :goto_0
    return v2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static getInstance(Landroid/content/Context;Ljava/lang/String;)Lepson/print/screen/PrintSetting;
    .locals 3

    .line 198
    sget-object v0, Lepson/print/screen/PrintSetting$Kind;->photo:Lepson/print/screen/PrintSetting$Kind;

    .line 200
    :try_start_0
    invoke-static {p1}, Lepson/print/screen/PrintSetting$Kind;->valueOf(Ljava/lang/String;)Lepson/print/screen/PrintSetting$Kind;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 204
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;)V

    .line 206
    :goto_0
    new-instance p1, Lepson/print/screen/PrintSetting;

    invoke-direct {p1, p0, v0}, Lepson/print/screen/PrintSetting;-><init>(Landroid/content/Context;Lepson/print/screen/PrintSetting$Kind;)V

    return-object p1
.end method

.method private getLayout()I
    .locals 4

    .line 618
    sget-object v0, Lepson/print/screen/PrintSetting$1;->$SwitchMap$epson$print$screen$PrintSetting$Kind:[I

    iget-object v1, p0, Lepson/print/screen/PrintSetting;->kind:Lepson/print/screen/PrintSetting$Kind;

    invoke-virtual {v1}, Lepson/print/screen/PrintSetting$Kind;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x0

    const/4 v2, 0x2

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const-string v0, "LAYOUT_CAMERACOPY"

    .line 638
    invoke-virtual {p0, v0, v1, v2}, Lepson/print/screen/PrintSetting;->getSetting(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    goto :goto_1

    :pswitch_1
    const/4 v0, 0x1

    const-string v3, "LAYOUT_PHOTO"

    .line 633
    invoke-virtual {p0, v3, v1, v0}, Lepson/print/screen/PrintSetting;->getSetting(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    goto :goto_1

    :goto_0
    :pswitch_2
    const/4 v0, 0x2

    :goto_1
    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x2

    :cond_0
    return v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private getLayoutMultiPage()I
    .locals 3

    .line 659
    sget-object v0, Lepson/print/screen/PrintSetting$1;->$SwitchMap$epson$print$screen$PrintSetting$Kind:[I

    iget-object v1, p0, Lepson/print/screen/PrintSetting;->kind:Lepson/print/screen/PrintSetting$Kind;

    invoke-virtual {v1}, Lepson/print/screen/PrintSetting$Kind;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x0

    const/4 v2, 0x0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const-string v0, "LAYOUT_MULTIPAGE_PHOTO"

    .line 667
    invoke-virtual {p0, v0, v1, v2}, Lepson/print/screen/PrintSetting;->getSetting(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v2

    goto :goto_0

    :pswitch_1
    const-string v0, "LAYOUT_MULTIPAGE"

    .line 662
    invoke-virtual {p0, v0, v1, v2}, Lepson/print/screen/PrintSetting;->getSetting(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v2

    :goto_0
    return v2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private getPaperSize()I
    .locals 3

    .line 526
    invoke-virtual {p0}, Lepson/print/screen/PrintSetting;->getDefaultPaperSize()I

    move-result v0

    .line 528
    sget-object v1, Lepson/print/screen/PrintSetting$1;->$SwitchMap$epson$print$screen$PrintSetting$Kind:[I

    iget-object v2, p0, Lepson/print/screen/PrintSetting;->kind:Lepson/print/screen/PrintSetting$Kind;

    invoke-virtual {v2}, Lepson/print/screen/PrintSetting$Kind;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x0

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const-string v1, "PAPER_SIZE_CAMERACOPY"

    .line 542
    invoke-virtual {p0, v1, v2, v0}, Lepson/print/screen/PrintSetting;->getSetting(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    goto :goto_0

    :pswitch_1
    const-string v1, "PAPER_SIZE_PHOTO"

    .line 537
    invoke-virtual {p0, v1, v2, v0}, Lepson/print/screen/PrintSetting;->getSetting(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    goto :goto_0

    :pswitch_2
    const-string v1, "PAPER_SIZE"

    .line 531
    invoke-virtual {p0, v1, v2, v0}, Lepson/print/screen/PrintSetting;->getSetting(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    :goto_0
    return v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private getPaperSizeTypePreferences()Landroid/content/SharedPreferences;
    .locals 3

    .line 960
    sget-object v0, Lepson/print/screen/PrintSetting$1;->$SwitchMap$epson$print$screen$PrintSetting$Kind:[I

    iget-object v1, p0, Lepson/print/screen/PrintSetting;->kind:Lepson/print/screen/PrintSetting$Kind;

    invoke-virtual {v1}, Lepson/print/screen/PrintSetting$Kind;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    goto :goto_0

    :pswitch_0
    const-string v0, "PrintSizeTypeSet_CAMERACOPY"

    goto :goto_0

    :pswitch_1
    const-string v0, "PrintSizeTypeSet_PHOTO"

    goto :goto_0

    :pswitch_2
    const-string v0, "PrintSizeTypeSet"

    .line 974
    :goto_0
    iget-object v1, p0, Lepson/print/screen/PrintSetting;->context:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private getPaperSorce()I
    .locals 3

    .line 719
    sget-object v0, Lepson/print/screen/PrintSetting$1;->$SwitchMap$epson$print$screen$PrintSetting$Kind:[I

    iget-object v1, p0, Lepson/print/screen/PrintSetting;->kind:Lepson/print/screen/PrintSetting$Kind;

    invoke-virtual {v1}, Lepson/print/screen/PrintSetting$Kind;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/16 v1, 0x80

    const/4 v2, 0x0

    packed-switch v0, :pswitch_data_0

    const/16 v0, 0x80

    goto :goto_0

    :pswitch_0
    const-string v0, "PAPER_SOURCE_CAMERACOPY"

    .line 733
    invoke-virtual {p0, v0, v2, v1}, Lepson/print/screen/PrintSetting;->getSetting(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    goto :goto_0

    :pswitch_1
    const-string v0, "PAPER_SOURCE_PHOTO"

    .line 728
    invoke-virtual {p0, v0, v2, v1}, Lepson/print/screen/PrintSetting;->getSetting(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    goto :goto_0

    :pswitch_2
    const-string v0, "PAPER_SOURCE"

    .line 722
    invoke-virtual {p0, v0, v2, v1}, Lepson/print/screen/PrintSetting;->getSetting(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    :goto_0
    if-nez v0, :cond_0

    const/16 v0, 0x80

    :cond_0
    return v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private getPaperType()I
    .locals 3

    .line 586
    invoke-virtual {p0}, Lepson/print/screen/PrintSetting;->getDefaultPaperType()I

    move-result v0

    .line 588
    sget-object v1, Lepson/print/screen/PrintSetting$1;->$SwitchMap$epson$print$screen$PrintSetting$Kind:[I

    iget-object v2, p0, Lepson/print/screen/PrintSetting;->kind:Lepson/print/screen/PrintSetting$Kind;

    invoke-virtual {v2}, Lepson/print/screen/PrintSetting$Kind;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x0

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const-string v1, "PAPER_TYPE_CAMERACOPY"

    .line 602
    invoke-virtual {p0, v1, v2, v0}, Lepson/print/screen/PrintSetting;->getSetting(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    goto :goto_0

    :pswitch_1
    const-string v1, "PAPER_TYPE_PHOTO"

    .line 597
    invoke-virtual {p0, v1, v2, v0}, Lepson/print/screen/PrintSetting;->getSetting(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    goto :goto_0

    :pswitch_2
    const-string v1, "PAPER_TYPE"

    .line 591
    invoke-virtual {p0, v1, v2, v0}, Lepson/print/screen/PrintSetting;->getSetting(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    :goto_0
    return v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private getQuality()I
    .locals 3

    .line 689
    sget-object v0, Lepson/print/screen/PrintSetting$1;->$SwitchMap$epson$print$screen$PrintSetting$Kind:[I

    iget-object v1, p0, Lepson/print/screen/PrintSetting;->kind:Lepson/print/screen/PrintSetting$Kind;

    invoke-virtual {v1}, Lepson/print/screen/PrintSetting$Kind;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x0

    const/4 v2, 0x2

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const-string v0, "QUALITY_CAMERACOPY"

    .line 703
    invoke-virtual {p0, v0, v1, v2}, Lepson/print/screen/PrintSetting;->getSetting(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v2

    goto :goto_0

    :pswitch_1
    const-string v0, "QUALITY_PHOTO"

    .line 698
    invoke-virtual {p0, v0, v1, v2}, Lepson/print/screen/PrintSetting;->getSetting(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v2

    goto :goto_0

    :pswitch_2
    const-string v0, "QUALITY"

    .line 692
    invoke-virtual {p0, v0, v1, v2}, Lepson/print/screen/PrintSetting;->getSetting(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v2

    :goto_0
    return v2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private preparateSettings()V
    .locals 8

    .line 284
    iget-object v0, p0, Lepson/print/screen/PrintSetting;->settings:Landroid/content/SharedPreferences;

    if-nez v0, :cond_9

    const-string v0, "content://epson.print.provider.SharedPreferences/"

    .line 286
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 287
    invoke-static {}, Lepson/print/IprintApplication;->getInstance()Lepson/print/IprintApplication;

    move-result-object v0

    invoke-virtual {v0}, Lepson/print/IprintApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 288
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 290
    iget-object v1, p0, Lepson/print/screen/PrintSetting;->allValueFromContentProvider:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 293
    invoke-static {}, Lcom/epson/mobilephone/common/EpLog;->i()V

    const/4 v1, 0x0

    move-object v2, v1

    move-object v3, v2

    :cond_0
    const/4 v4, 0x0

    .line 295
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->getColumnCount()I

    move-result v5

    if-ge v4, v5, :cond_5

    .line 296
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnName(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "key"

    .line 297
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 298
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_1
    const-string v6, "value"

    .line 299
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    const-string v5, "\u3055\u30fc\u3073\u3059"

    .line 300
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "PUT key: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "  getType: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getType(I)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/epson/mobilephone/common/EpLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getType(I)I

    .line 302
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getType(I)I

    move-result v5

    const/4 v6, 0x1

    if-eq v5, v6, :cond_3

    const/4 v6, 0x3

    if-eq v5, v6, :cond_2

    goto :goto_1

    .line 307
    :cond_2
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 304
    :cond_3
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto :goto_1

    :cond_4
    move-object v2, v1

    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_5
    if-eqz v2, :cond_8

    .line 317
    instance-of v4, v3, Ljava/lang/String;

    if-eqz v4, :cond_7

    const-string v4, "true"

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_6

    const-string v4, "false"

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 319
    :cond_6
    iget-object v4, p0, Lepson/print/screen/PrintSetting;->allValueFromContentProvider:Ljava/util/Map;

    move-object v5, v3

    check-cast v5, Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v5

    invoke-interface {v4, v2, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 321
    :cond_7
    iget-object v4, p0, Lepson/print/screen/PrintSetting;->allValueFromContentProvider:Ljava/util/Map;

    invoke-interface {v4, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_2
    move-object v2, v1

    .line 326
    :cond_8
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 327
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_9
    return-void
.end method


# virtual methods
.method public clearSettings()V
    .locals 6

    const-string v0, "PrintSetting"

    const-string v1, "clearSettings()"

    .line 1104
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1106
    iget-object v0, p0, Lepson/print/screen/PrintSetting;->settings:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "PAPER_SIZE"

    .line 1108
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "PAPER_TYPE"

    .line 1109
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "LAYOUT"

    .line 1110
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "QUALITY"

    .line 1111
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "PAPER_SOURCE"

    .line 1112
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "COLOR"

    .line 1113
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "DUPLEX"

    .line 1114
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "FEED_DIRECTION"

    .line 1115
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "PAPER_SIZE_PHOTO"

    .line 1117
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "PAPER_TYPE_PHOTO"

    .line 1118
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "LAYOUT_PHOTO"

    .line 1119
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "QUALITY_PHOTO"

    .line 1120
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "PAPER_SOURCE_PHOTO"

    .line 1121
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "COLOR_PHOTO"

    .line 1122
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "DUPLEX_PHOTO"

    .line 1123
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "FEED_DIRECTION_PHOTO"

    .line 1124
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "PRINT_ALL"

    .line 1126
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "START_PAGE"

    .line 1127
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "END_PAGE"

    .line 1128
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "PRINTDATE"

    .line 1130
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "APF_PHOTO"

    .line 1131
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "COPIES"

    .line 1133
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "BRIGHTNESS"

    .line 1134
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "CONTRAST"

    .line 1135
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "SATURATION"

    .line 1136
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1139
    sget-object v1, Lepson/print/screen/PrintSetting;->CAMERA_COPY_KEYS:[Ljava/lang/String;

    array-length v2, v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v2, :cond_0

    aget-object v5, v1, v4

    .line 1140
    invoke-interface {v0, v5}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1145
    :cond_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1151
    iget-object v0, p0, Lepson/print/screen/PrintSetting;->context:Landroid/content/Context;

    const-string v1, "PrintSizeTypeSet"

    invoke-virtual {v0, v1, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1152
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1155
    iget-object v0, p0, Lepson/print/screen/PrintSetting;->context:Landroid/content/Context;

    const-string v1, "PrintSizeTypeSet_PHOTO"

    invoke-virtual {v0, v1, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1156
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1159
    iget-object v0, p0, Lepson/print/screen/PrintSetting;->context:Landroid/content/Context;

    const-string v1, "PrintSizeTypeSet_CAMERACOPY"

    invoke-virtual {v0, v1, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1160
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public getDefaultPaperSize()I
    .locals 4

    .line 487
    sget-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_A4:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v0

    .line 489
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    .line 492
    sget-object v2, Lepson/print/screen/PrintSetting$1;->$SwitchMap$epson$print$screen$PrintSetting$Kind:[I

    iget-object v3, p0, Lepson/print/screen/PrintSetting;->kind:Lepson/print/screen/PrintSetting$Kind;

    invoke-virtual {v3}, Lepson/print/screen/PrintSetting$Kind;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 507
    :pswitch_0
    sget-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_4X6:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v0

    .line 509
    sget-object v2, Ljava/util/Locale;->JAPAN:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 510
    sget-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_L:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v0

    goto :goto_0

    .line 497
    :pswitch_1
    sget-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_A4:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v0

    .line 499
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Ljava/util/Locale;->CANADA:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 500
    :cond_0
    sget-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_LETTER:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v0

    :cond_1
    :goto_0
    return v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getDefaultPaperType()I
    .locals 2

    .line 559
    sget-object v0, Lepson/print/screen/PrintSetting$1;->$SwitchMap$epson$print$screen$PrintSetting$Kind:[I

    iget-object v1, p0, Lepson/print/screen/PrintSetting;->kind:Lepson/print/screen/PrintSetting$Kind;

    invoke-virtual {v1}, Lepson/print/screen/PrintSetting$Kind;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    goto :goto_0

    .line 570
    :pswitch_0
    sget-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PGPHOTO:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v0

    goto :goto_0

    .line 564
    :pswitch_1
    sget-object v0, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->EPS_MTID_PLAIN:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$MediaName;->getCode()I

    move-result v0

    :goto_0
    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getEcClientLibSourceType()I
    .locals 2

    .line 943
    sget-object v0, Lepson/print/screen/PrintSetting$1;->$SwitchMap$epson$print$screen$PrintSetting$Kind:[I

    iget-object v1, p0, Lepson/print/screen/PrintSetting;->kind:Lepson/print/screen/PrintSetting$Kind;

    invoke-virtual {v1}, Lepson/print/screen/PrintSetting$Kind;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x2

    return v0

    :cond_0
    return v1
.end method

.method public getEmailAddress()Ljava/lang/String;
    .locals 2

    const-string v0, "PRINTER_EMAIL_ADDRESS"

    const/4 v1, 0x0

    .line 851
    invoke-virtual {p0, v0, v1}, Lepson/print/screen/PrintSetting;->getSettingString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getIP()Ljava/lang/String;
    .locals 2

    const-string v0, "PRINTER_IP"

    const/4 v1, 0x0

    .line 843
    invoke-virtual {p0, v0, v1}, Lepson/print/screen/PrintSetting;->getSettingString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getKind()Lepson/print/screen/PrintSetting$Kind;
    .locals 1

    .line 115
    iget-object v0, p0, Lepson/print/screen/PrintSetting;->kind:Lepson/print/screen/PrintSetting$Kind;

    return-object v0
.end method

.method public getLang()I
    .locals 3

    const-string v0, "LANG"

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 859
    invoke-virtual {p0, v0, v1, v2}, Lepson/print/screen/PrintSetting;->getSetting(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getLocation()I
    .locals 3

    const-string v0, "PRINTER_LOCATION"

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 855
    invoke-virtual {p0, v0, v1, v2}, Lepson/print/screen/PrintSetting;->getSetting(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getPrinterID()Ljava/lang/String;
    .locals 2

    const-string v0, "PRINTER_ID"

    const/4 v1, 0x0

    .line 839
    invoke-virtual {p0, v0, v1}, Lepson/print/screen/PrintSetting;->getSettingString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPrinterName()Ljava/lang/String;
    .locals 2

    const-string v0, "PRINTER_NAME"

    const/4 v1, 0x0

    .line 835
    invoke-virtual {p0, v0, v1}, Lepson/print/screen/PrintSetting;->getSettingString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getSetting(Ljava/lang/String;Ljava/lang/String;I)I
    .locals 1

    .line 874
    iget-object v0, p0, Lepson/print/screen/PrintSetting;->settings:Landroid/content/SharedPreferences;

    if-nez v0, :cond_2

    .line 876
    iget-object v0, p0, Lepson/print/screen/PrintSetting;->allValueFromContentProvider:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 877
    iget-object p2, p0, Lepson/print/screen/PrintSetting;->allValueFromContentProvider:Ljava/util/Map;

    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 878
    iget-object p2, p0, Lepson/print/screen/PrintSetting;->allValueFromContentProvider:Ljava/util/Map;

    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p3

    goto :goto_1

    :cond_0
    if-nez p2, :cond_1

    goto :goto_1

    .line 881
    :cond_1
    iget-object p1, p0, Lepson/print/screen/PrintSetting;->allValueFromContentProvider:Ljava/util/Map;

    invoke-interface {p1, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_5

    .line 882
    iget-object p1, p0, Lepson/print/screen/PrintSetting;->allValueFromContentProvider:Ljava/util/Map;

    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p3

    goto :goto_1

    .line 887
    :cond_2
    invoke-interface {v0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    .line 889
    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    if-nez p2, :cond_3

    goto :goto_0

    .line 892
    :cond_3
    iget-object p1, p0, Lepson/print/screen/PrintSetting;->settings:Landroid/content/SharedPreferences;

    invoke-interface {p1, p2, p3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result p3

    goto :goto_1

    .line 890
    :cond_4
    :goto_0
    iget-object p2, p0, Lepson/print/screen/PrintSetting;->settings:Landroid/content/SharedPreferences;

    invoke-interface {p2, p1, p3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result p3

    :cond_5
    :goto_1
    return p3
.end method

.method getSettingBoolean(Ljava/lang/String;Z)Z
    .locals 1

    .line 922
    iget-object v0, p0, Lepson/print/screen/PrintSetting;->settings:Landroid/content/SharedPreferences;

    if-nez v0, :cond_0

    .line 924
    iget-object v0, p0, Lepson/print/screen/PrintSetting;->allValueFromContentProvider:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 925
    iget-object p2, p0, Lepson/print/screen/PrintSetting;->allValueFromContentProvider:Ljava/util/Map;

    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 926
    iget-object p2, p0, Lepson/print/screen/PrintSetting;->allValueFromContentProvider:Ljava/util/Map;

    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    :cond_0
    return p2
.end method

.method getSettingString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 901
    iget-object v0, p0, Lepson/print/screen/PrintSetting;->settings:Landroid/content/SharedPreferences;

    if-nez v0, :cond_0

    .line 903
    iget-object v0, p0, Lepson/print/screen/PrintSetting;->allValueFromContentProvider:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 904
    iget-object v0, p0, Lepson/print/screen/PrintSetting;->allValueFromContentProvider:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 905
    iget-object p2, p0, Lepson/print/screen/PrintSetting;->allValueFromContentProvider:Ljava/util/Map;

    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    move-object p2, p1

    check-cast p2, Ljava/lang/String;

    goto :goto_0

    .line 911
    :cond_0
    invoke-interface {v0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    .line 912
    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 913
    iget-object v0, p0, Lepson/print/screen/PrintSetting;->settings:Landroid/content/SharedPreferences;

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    :cond_1
    :goto_0
    return-object p2
.end method

.method public getSrialNo()Ljava/lang/String;
    .locals 2

    const-string v0, "PRINTER_SERIAL_NO"

    const/4 v1, 0x0

    .line 847
    invoke-virtual {p0, v0, v1}, Lepson/print/screen/PrintSetting;->getSettingString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUpdateSettingsRemoteSourceType()I
    .locals 2

    .line 1086
    sget-object v0, Lepson/print/screen/PrintSetting$1;->$SwitchMap$epson$print$screen$PrintSetting$Kind:[I

    iget-object v1, p0, Lepson/print/screen/PrintSetting;->kind:Lepson/print/screen/PrintSetting$Kind;

    invoke-virtual {v1}, Lepson/print/screen/PrintSetting$Kind;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x2

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x1

    return v0

    :pswitch_0
    return v1

    :pswitch_1
    return v1

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public loadPaperSizeTypePear()Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;
    .locals 6

    .line 986
    new-instance v0, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;

    invoke-direct {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;-><init>()V

    .line 988
    invoke-direct {p0}, Lepson/print/screen/PrintSetting;->getPaperSizeTypePreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 996
    invoke-virtual {v0}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->getKeys()Ljava/util/Enumeration;

    move-result-object v2

    .line 1000
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1001
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 1002
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, -0x1

    invoke-interface {v1, v4, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    if-eq v4, v5, :cond_0

    .line 1004
    invoke-virtual {v0, v3, v4}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->putID(II)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public loadSettings()V
    .locals 5

    .line 214
    invoke-direct {p0}, Lepson/print/screen/PrintSetting;->preparateSettings()V

    .line 216
    invoke-direct {p0}, Lepson/print/screen/PrintSetting;->getPaperSize()I

    move-result v0

    iput v0, p0, Lepson/print/screen/PrintSetting;->paperSizeValue:I

    .line 217
    invoke-direct {p0}, Lepson/print/screen/PrintSetting;->getPaperType()I

    move-result v0

    iput v0, p0, Lepson/print/screen/PrintSetting;->paperTypeValue:I

    .line 218
    invoke-direct {p0}, Lepson/print/screen/PrintSetting;->getLayout()I

    move-result v0

    iput v0, p0, Lepson/print/screen/PrintSetting;->layoutValue:I

    .line 219
    invoke-direct {p0}, Lepson/print/screen/PrintSetting;->getLayoutMultiPage()I

    move-result v0

    iput v0, p0, Lepson/print/screen/PrintSetting;->layoutMultiPageValue:I

    .line 220
    invoke-direct {p0}, Lepson/print/screen/PrintSetting;->getQuality()I

    move-result v0

    iput v0, p0, Lepson/print/screen/PrintSetting;->qualityValue:I

    .line 221
    invoke-direct {p0}, Lepson/print/screen/PrintSetting;->getPaperSorce()I

    move-result v0

    iput v0, p0, Lepson/print/screen/PrintSetting;->paperSourceValue:I

    .line 222
    invoke-direct {p0}, Lepson/print/screen/PrintSetting;->getColor()I

    move-result v0

    iput v0, p0, Lepson/print/screen/PrintSetting;->colorValue:I

    .line 223
    invoke-direct {p0}, Lepson/print/screen/PrintSetting;->getDuplex()I

    move-result v0

    iput v0, p0, Lepson/print/screen/PrintSetting;->duplexValue:I

    .line 224
    invoke-direct {p0}, Lepson/print/screen/PrintSetting;->getFeedDirection()I

    move-result v0

    iput v0, p0, Lepson/print/screen/PrintSetting;->feedDirectionValue:I

    .line 226
    sget-object v0, Lepson/print/screen/PrintSetting$1;->$SwitchMap$epson$print$screen$PrintSetting$Kind:[I

    iget-object v1, p0, Lepson/print/screen/PrintSetting;->kind:Lepson/print/screen/PrintSetting$Kind;

    invoke-virtual {v1}, Lepson/print/screen/PrintSetting$Kind;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 242
    :pswitch_0
    iget-object v0, p0, Lepson/print/screen/PrintSetting;->settings:Landroid/content/SharedPreferences;

    if-nez v0, :cond_0

    const-string v0, "PRINTDATE"

    .line 243
    invoke-virtual {p0, v0, v2, v1}, Lepson/print/screen/PrintSetting;->getSetting(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lepson/print/screen/PrintSetting;->printdate:I

    const-string v0, "APF_PHOTO"

    .line 244
    invoke-virtual {p0, v0, v2, v3}, Lepson/print/screen/PrintSetting;->getSetting(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lepson/print/screen/PrintSetting;->photoApfValueForLocalPrinter:I

    const-string v0, "SHARPNESS_PHOTO"

    .line 245
    invoke-virtual {p0, v0, v2, v3}, Lepson/print/screen/PrintSetting;->getSetting(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lepson/print/screen/PrintSetting;->sharpnessForPhoto:I

    goto :goto_0

    :cond_0
    const-string v4, "PRINTDATE"

    .line 248
    invoke-interface {v0, v4, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lepson/print/screen/PrintSetting;->printdate:I

    .line 250
    iget-object v0, p0, Lepson/print/screen/PrintSetting;->settings:Landroid/content/SharedPreferences;

    const-string v4, "APF_PHOTO"

    invoke-interface {v0, v4, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lepson/print/screen/PrintSetting;->photoApfValueForLocalPrinter:I

    .line 252
    iget-object v0, p0, Lepson/print/screen/PrintSetting;->settings:Landroid/content/SharedPreferences;

    const-string v4, "SHARPNESS_PHOTO"

    invoke-interface {v0, v4, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lepson/print/screen/PrintSetting;->sharpnessForPhoto:I

    goto :goto_0

    .line 229
    :pswitch_1
    iget-object v0, p0, Lepson/print/screen/PrintSetting;->settings:Landroid/content/SharedPreferences;

    if-nez v0, :cond_1

    const-string v0, "PRINT_ALL"

    .line 230
    invoke-virtual {p0, v0, v3}, Lepson/print/screen/PrintSetting;->getSettingBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lepson/print/screen/PrintSetting;->printAll:Z

    const-string v0, "START_PAGE"

    .line 231
    invoke-virtual {p0, v0, v2, v3}, Lepson/print/screen/PrintSetting;->getSetting(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lepson/print/screen/PrintSetting;->startValue:I

    const-string v0, "END_PAGE"

    .line 232
    invoke-virtual {p0, v0, v2, v3}, Lepson/print/screen/PrintSetting;->getSetting(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lepson/print/screen/PrintSetting;->endValue:I

    goto :goto_0

    :cond_1
    const-string v4, "PRINT_ALL"

    .line 234
    invoke-interface {v0, v4, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lepson/print/screen/PrintSetting;->printAll:Z

    .line 235
    iget-object v0, p0, Lepson/print/screen/PrintSetting;->settings:Landroid/content/SharedPreferences;

    const-string v4, "START_PAGE"

    invoke-interface {v0, v4, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lepson/print/screen/PrintSetting;->startValue:I

    .line 236
    iget-object v0, p0, Lepson/print/screen/PrintSetting;->settings:Landroid/content/SharedPreferences;

    const-string v4, "END_PAGE"

    invoke-interface {v0, v4, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lepson/print/screen/PrintSetting;->endValue:I

    .line 263
    :goto_0
    iget-object v0, p0, Lepson/print/screen/PrintSetting;->settings:Landroid/content/SharedPreferences;

    if-nez v0, :cond_2

    const-string v0, "COPIES"

    .line 264
    invoke-virtual {p0, v0, v2, v3}, Lepson/print/screen/PrintSetting;->getSetting(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lepson/print/screen/PrintSetting;->copiesValue:I

    goto :goto_1

    :cond_2
    const-string v2, "COPIES"

    .line 266
    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lepson/print/screen/PrintSetting;->copiesValue:I

    .line 270
    :goto_1
    iput v1, p0, Lepson/print/screen/PrintSetting;->brightnessValue:I

    .line 273
    iput v1, p0, Lepson/print/screen/PrintSetting;->contrastValue:I

    .line 276
    iput v1, p0, Lepson/print/screen/PrintSetting;->saturationValue:I

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public resetPageRange(II)V
    .locals 2

    const-string v0, "PrintSetting"

    const-string v1, "resetPrintSettings()"

    .line 1041
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1043
    iget-object v0, p0, Lepson/print/screen/PrintSetting;->settings:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const/4 v1, 0x1

    .line 1046
    iput-boolean v1, p0, Lepson/print/screen/PrintSetting;->printAll:Z

    .line 1047
    iput p1, p0, Lepson/print/screen/PrintSetting;->startValue:I

    .line 1048
    iput p2, p0, Lepson/print/screen/PrintSetting;->endValue:I

    const-string p1, "PRINT_ALL"

    .line 1051
    iget-boolean p2, p0, Lepson/print/screen/PrintSetting;->printAll:Z

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const-string p1, "START_PAGE"

    .line 1052
    iget p2, p0, Lepson/print/screen/PrintSetting;->startValue:I

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string p1, "END_PAGE"

    .line 1053
    iget p2, p0, Lepson/print/screen/PrintSetting;->endValue:I

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1055
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public resetPrintSettings()V
    .locals 3

    const-string v0, "PrintSetting"

    const-string v1, "resetPrintSettings()"

    .line 1063
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1065
    iget-object v0, p0, Lepson/print/screen/PrintSetting;->settings:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "COPIES"

    const/4 v2, 0x1

    .line 1067
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v1, "BRIGHTNESS"

    const/4 v2, 0x0

    .line 1068
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v1, "CONTRAST"

    .line 1069
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v1, "SATURATION"

    .line 1070
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1072
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public savePaperSizeTypePear(Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;)V
    .locals 4

    .line 1019
    invoke-direct {p0}, Lepson/print/screen/PrintSetting;->getPaperSizeTypePreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1021
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1023
    invoke-virtual {p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->getKeys()Ljava/util/Enumeration;

    move-result-object v1

    .line 1025
    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1026
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 1028
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSizeType;->getID(I)I

    move-result v2

    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 1031
    :cond_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public saveSettings()V
    .locals 5

    const-string v0, "PrintSetting"

    .line 335
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "saveSettings() "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lepson/print/screen/PrintSetting;->kind:Lepson/print/screen/PrintSetting$Kind;

    invoke-virtual {v2}, Lepson/print/screen/PrintSetting$Kind;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    iget-object v0, p0, Lepson/print/screen/PrintSetting;->settings:Landroid/content/SharedPreferences;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 343
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    move-object v2, v0

    move-object v0, v1

    goto :goto_0

    .line 345
    :cond_0
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    move-object v2, v1

    .line 348
    :goto_0
    sget-object v3, Lepson/print/screen/PrintSetting$1;->$SwitchMap$epson$print$screen$PrintSetting$Kind:[I

    iget-object v4, p0, Lepson/print/screen/PrintSetting;->kind:Lepson/print/screen/PrintSetting$Kind;

    invoke-virtual {v4}, Lepson/print/screen/PrintSetting$Kind;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    goto/16 :goto_1

    :pswitch_0
    if-nez v0, :cond_1

    const-string v3, "PAPER_SIZE_CAMERACOPY"

    .line 421
    iget v4, p0, Lepson/print/screen/PrintSetting;->paperSizeValue:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "PAPER_TYPE_CAMERACOPY"

    .line 422
    iget v4, p0, Lepson/print/screen/PrintSetting;->paperTypeValue:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "LAYOUT_CAMERACOPY"

    .line 423
    iget v4, p0, Lepson/print/screen/PrintSetting;->layoutValue:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "QUALITY_CAMERACOPY"

    .line 424
    iget v4, p0, Lepson/print/screen/PrintSetting;->qualityValue:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "PAPER_SOURCE_CAMERACOPY"

    .line 425
    iget v4, p0, Lepson/print/screen/PrintSetting;->paperSourceValue:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "COLOR_CAMERACOPY"

    .line 426
    iget v4, p0, Lepson/print/screen/PrintSetting;->colorValue:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "FEED_DIRECTION_CAMERACOPY"

    .line 427
    iget v4, p0, Lepson/print/screen/PrintSetting;->feedDirectionValue:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_1

    :cond_1
    const-string v3, "PAPER_SIZE_CAMERACOPY"

    .line 429
    iget v4, p0, Lepson/print/screen/PrintSetting;->paperSizeValue:I

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v3, "PAPER_TYPE_CAMERACOPY"

    .line 430
    iget v4, p0, Lepson/print/screen/PrintSetting;->paperTypeValue:I

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v3, "LAYOUT_CAMERACOPY"

    .line 431
    iget v4, p0, Lepson/print/screen/PrintSetting;->layoutValue:I

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v3, "QUALITY_CAMERACOPY"

    .line 432
    iget v4, p0, Lepson/print/screen/PrintSetting;->qualityValue:I

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v3, "PAPER_SOURCE_CAMERACOPY"

    .line 433
    iget v4, p0, Lepson/print/screen/PrintSetting;->paperSourceValue:I

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v3, "COLOR_CAMERACOPY"

    .line 434
    iget v4, p0, Lepson/print/screen/PrintSetting;->colorValue:I

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v3, "FEED_DIRECTION_CAMERACOPY"

    .line 435
    iget v4, p0, Lepson/print/screen/PrintSetting;->feedDirectionValue:I

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto/16 :goto_1

    :pswitch_1
    if-nez v0, :cond_2

    const-string v3, "PAPER_SIZE_PHOTO"

    .line 385
    iget v4, p0, Lepson/print/screen/PrintSetting;->paperSizeValue:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "PAPER_TYPE_PHOTO"

    .line 386
    iget v4, p0, Lepson/print/screen/PrintSetting;->paperTypeValue:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "LAYOUT_PHOTO"

    .line 387
    iget v4, p0, Lepson/print/screen/PrintSetting;->layoutValue:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "LAYOUT_MULTIPAGE_PHOTO"

    .line 388
    iget v4, p0, Lepson/print/screen/PrintSetting;->layoutMultiPageValue:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "QUALITY_PHOTO"

    .line 389
    iget v4, p0, Lepson/print/screen/PrintSetting;->qualityValue:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "PAPER_SOURCE_PHOTO"

    .line 390
    iget v4, p0, Lepson/print/screen/PrintSetting;->paperSourceValue:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "COLOR_PHOTO"

    .line 391
    iget v4, p0, Lepson/print/screen/PrintSetting;->colorValue:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "DUPLEX_PHOTO"

    .line 392
    iget v4, p0, Lepson/print/screen/PrintSetting;->duplexValue:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "FEED_DIRECTION_PHOTO"

    .line 393
    iget v4, p0, Lepson/print/screen/PrintSetting;->feedDirectionValue:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "PRINTDATE"

    .line 396
    iget v4, p0, Lepson/print/screen/PrintSetting;->printdate:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "APF_PHOTO"

    .line 398
    iget v4, p0, Lepson/print/screen/PrintSetting;->photoApfValueForLocalPrinter:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "SHARPNESS_PHOTO"

    .line 399
    iget v4, p0, Lepson/print/screen/PrintSetting;->sharpnessForPhoto:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_1

    :cond_2
    const-string v3, "PAPER_SIZE_PHOTO"

    .line 401
    iget v4, p0, Lepson/print/screen/PrintSetting;->paperSizeValue:I

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v3, "PAPER_TYPE_PHOTO"

    .line 402
    iget v4, p0, Lepson/print/screen/PrintSetting;->paperTypeValue:I

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v3, "LAYOUT_PHOTO"

    .line 403
    iget v4, p0, Lepson/print/screen/PrintSetting;->layoutValue:I

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v3, "LAYOUT_MULTIPAGE_PHOTO"

    .line 404
    iget v4, p0, Lepson/print/screen/PrintSetting;->layoutMultiPageValue:I

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v3, "QUALITY_PHOTO"

    .line 405
    iget v4, p0, Lepson/print/screen/PrintSetting;->qualityValue:I

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v3, "PAPER_SOURCE_PHOTO"

    .line 406
    iget v4, p0, Lepson/print/screen/PrintSetting;->paperSourceValue:I

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v3, "COLOR_PHOTO"

    .line 407
    iget v4, p0, Lepson/print/screen/PrintSetting;->colorValue:I

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v3, "DUPLEX_PHOTO"

    .line 408
    iget v4, p0, Lepson/print/screen/PrintSetting;->duplexValue:I

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v3, "FEED_DIRECTION_PHOTO"

    .line 409
    iget v4, p0, Lepson/print/screen/PrintSetting;->feedDirectionValue:I

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v3, "PRINTDATE"

    .line 412
    iget v4, p0, Lepson/print/screen/PrintSetting;->printdate:I

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v3, "APF_PHOTO"

    .line 414
    iget v4, p0, Lepson/print/screen/PrintSetting;->photoApfValueForLocalPrinter:I

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v3, "SHARPNESS_PHOTO"

    .line 415
    iget v4, p0, Lepson/print/screen/PrintSetting;->sharpnessForPhoto:I

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto/16 :goto_1

    :pswitch_2
    if-nez v0, :cond_3

    const-string v3, "PAPER_SIZE"

    .line 351
    iget v4, p0, Lepson/print/screen/PrintSetting;->paperSizeValue:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "PAPER_TYPE"

    .line 352
    iget v4, p0, Lepson/print/screen/PrintSetting;->paperTypeValue:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "LAYOUT"

    .line 353
    iget v4, p0, Lepson/print/screen/PrintSetting;->layoutValue:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "LAYOUT_MULTIPAGE"

    .line 354
    iget v4, p0, Lepson/print/screen/PrintSetting;->layoutMultiPageValue:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "QUALITY"

    .line 355
    iget v4, p0, Lepson/print/screen/PrintSetting;->qualityValue:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "PAPER_SOURCE"

    .line 356
    iget v4, p0, Lepson/print/screen/PrintSetting;->paperSourceValue:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "COLOR"

    .line 357
    iget v4, p0, Lepson/print/screen/PrintSetting;->colorValue:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "DUPLEX"

    .line 358
    iget v4, p0, Lepson/print/screen/PrintSetting;->duplexValue:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "FEED_DIRECTION"

    .line 359
    iget v4, p0, Lepson/print/screen/PrintSetting;->feedDirectionValue:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "PRINT_ALL"

    .line 362
    iget-boolean v4, p0, Lepson/print/screen/PrintSetting;->printAll:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v3, "START_PAGE"

    .line 363
    iget v4, p0, Lepson/print/screen/PrintSetting;->startValue:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "END_PAGE"

    .line 364
    iget v4, p0, Lepson/print/screen/PrintSetting;->endValue:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_1

    :cond_3
    const-string v3, "PAPER_SIZE"

    .line 367
    iget v4, p0, Lepson/print/screen/PrintSetting;->paperSizeValue:I

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v3, "PAPER_TYPE"

    .line 368
    iget v4, p0, Lepson/print/screen/PrintSetting;->paperTypeValue:I

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v3, "LAYOUT"

    .line 369
    iget v4, p0, Lepson/print/screen/PrintSetting;->layoutValue:I

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v3, "LAYOUT_MULTIPAGE"

    .line 370
    iget v4, p0, Lepson/print/screen/PrintSetting;->layoutMultiPageValue:I

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v3, "QUALITY"

    .line 371
    iget v4, p0, Lepson/print/screen/PrintSetting;->qualityValue:I

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v3, "PAPER_SOURCE"

    .line 372
    iget v4, p0, Lepson/print/screen/PrintSetting;->paperSourceValue:I

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v3, "COLOR"

    .line 373
    iget v4, p0, Lepson/print/screen/PrintSetting;->colorValue:I

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v3, "DUPLEX"

    .line 374
    iget v4, p0, Lepson/print/screen/PrintSetting;->duplexValue:I

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v3, "FEED_DIRECTION"

    .line 375
    iget v4, p0, Lepson/print/screen/PrintSetting;->feedDirectionValue:I

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v3, "PRINT_ALL"

    .line 378
    iget-boolean v4, p0, Lepson/print/screen/PrintSetting;->printAll:Z

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const-string v3, "START_PAGE"

    .line 379
    iget v4, p0, Lepson/print/screen/PrintSetting;->startValue:I

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v3, "END_PAGE"

    .line 380
    iget v4, p0, Lepson/print/screen/PrintSetting;->endValue:I

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    :goto_1
    if-nez v0, :cond_4

    const-string v3, "COPIES"

    .line 442
    iget v4, p0, Lepson/print/screen/PrintSetting;->copiesValue:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "BRIGHTNESS"

    .line 445
    iget v4, p0, Lepson/print/screen/PrintSetting;->brightnessValue:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "CONTRAST"

    .line 447
    iget v4, p0, Lepson/print/screen/PrintSetting;->contrastValue:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "SATURATION"

    .line 449
    iget v4, p0, Lepson/print/screen/PrintSetting;->saturationValue:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_2

    :cond_4
    const-string v3, "COPIES"

    .line 453
    iget v4, p0, Lepson/print/screen/PrintSetting;->copiesValue:I

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v3, "BRIGHTNESS"

    .line 456
    iget v4, p0, Lepson/print/screen/PrintSetting;->brightnessValue:I

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v3, "CONTRAST"

    .line 458
    iget v4, p0, Lepson/print/screen/PrintSetting;->contrastValue:I

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v3, "SATURATION"

    .line 460
    iget v4, p0, Lepson/print/screen/PrintSetting;->saturationValue:I

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 466
    :goto_2
    iget-object v3, p0, Lepson/print/screen/PrintSetting;->settings:Landroid/content/SharedPreferences;

    if-nez v3, :cond_5

    const-string v0, "content://epson.print.provider.SharedPreferences/"

    .line 467
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 468
    invoke-static {}, Lepson/print/IprintApplication;->getInstance()Lepson/print/IprintApplication;

    move-result-object v3

    invoke-virtual {v3}, Lepson/print/IprintApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-virtual {v3, v0, v2, v1, v1}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_3

    .line 471
    :cond_5
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :goto_3
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

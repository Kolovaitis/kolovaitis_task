.class Lepson/print/screen/PrintProgress$11;
.super Ljava/lang/Object;
.source "PrintProgress.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/screen/PrintProgress;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/screen/PrintProgress;


# direct methods
.method constructor <init>(Lepson/print/screen/PrintProgress;)V
    .locals 0

    .line 1102
    iput-object p1, p0, Lepson/print/screen/PrintProgress$11;->this$0:Lepson/print/screen/PrintProgress;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    const/4 p1, 0x0

    .line 1108
    :try_start_0
    sget p2, Lepson/print/screen/PrintProgress;->curError:I

    const/16 v0, 0x6d

    const/4 v1, 0x1

    if-ne p2, v0, :cond_0

    .line 1109
    sput-boolean v1, Lepson/print/screen/PrintProgress;->isBkRetry:Z

    .line 1110
    iget-object p2, p0, Lepson/print/screen/PrintProgress$11;->this$0:Lepson/print/screen/PrintProgress;

    invoke-static {p2}, Lepson/print/screen/PrintProgress;->access$200(Lepson/print/screen/PrintProgress;)V

    .line 1112
    :cond_0
    invoke-static {}, Lepson/print/screen/PrintProgress;->access$800()Lepson/print/service/IEpsonService;

    move-result-object p2

    invoke-interface {p2, v1}, Lepson/print/service/IEpsonService;->confirmContinueable(Z)I

    .line 1113
    sput p1, Lepson/print/screen/PrintProgress;->curError:I

    .line 1115
    iget-object p2, p0, Lepson/print/screen/PrintProgress$11;->this$0:Lepson/print/screen/PrintProgress;

    invoke-static {p2, p1}, Lepson/print/screen/PrintProgress;->access$302(Lepson/print/screen/PrintProgress;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p2

    .line 1117
    invoke-virtual {p2}, Landroid/os/RemoteException;->printStackTrace()V

    .line 1119
    :goto_0
    iget-object p2, p0, Lepson/print/screen/PrintProgress$11;->this$0:Lepson/print/screen/PrintProgress;

    invoke-static {p2, p1}, Lepson/print/screen/PrintProgress;->access$602(Lepson/print/screen/PrintProgress;Z)Z

    .line 1120
    iget-object p2, p0, Lepson/print/screen/PrintProgress$11;->this$0:Lepson/print/screen/PrintProgress;

    invoke-virtual {p2, p1}, Lepson/print/screen/PrintProgress;->removeDialog(I)V

    return-void
.end method

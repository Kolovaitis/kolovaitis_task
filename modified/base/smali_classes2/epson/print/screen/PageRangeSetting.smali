.class public Lepson/print/screen/PageRangeSetting;
.super Lepson/print/ActivityIACommon;
.source "PageRangeSetting.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field public static final PREFS_NAME:Ljava/lang/String; = "PrintSetting"


# instance fields
.field btnPrintAll:Landroid/widget/Switch;

.field end:I

.field endPage:Landroid/widget/TextView;

.field endPageMinus:Landroid/widget/Button;

.field endPagePlus:Landroid/widget/Button;

.field sheets:I

.field start:I

.field startPage:Landroid/widget/TextView;

.field startPageMinus:Landroid/widget/Button;

.field startPagePlus:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .line 166
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    const/4 v0, 0x0

    const/4 v1, 0x1

    sparse-switch p1, :sswitch_data_0

    goto/16 :goto_1

    .line 168
    :sswitch_0
    iget p1, p0, Lepson/print/screen/PageRangeSetting;->start:I

    iget v2, p0, Lepson/print/screen/PageRangeSetting;->end:I

    if-ge p1, v2, :cond_1

    add-int/2addr p1, v1

    .line 169
    iput p1, p0, Lepson/print/screen/PageRangeSetting;->start:I

    .line 170
    iget-object p1, p0, Lepson/print/screen/PageRangeSetting;->startPage:Landroid/widget/TextView;

    iget v2, p0, Lepson/print/screen/PageRangeSetting;->start:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 171
    iget-object p1, p0, Lepson/print/screen/PageRangeSetting;->startPageMinus:Landroid/widget/Button;

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 172
    iget p1, p0, Lepson/print/screen/PageRangeSetting;->start:I

    iget v1, p0, Lepson/print/screen/PageRangeSetting;->end:I

    if-ne p1, v1, :cond_1

    .line 173
    iget-object p1, p0, Lepson/print/screen/PageRangeSetting;->startPagePlus:Landroid/widget/Button;

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 174
    iget-object p1, p0, Lepson/print/screen/PageRangeSetting;->endPageMinus:Landroid/widget/Button;

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    goto/16 :goto_1

    .line 179
    :sswitch_1
    iget p1, p0, Lepson/print/screen/PageRangeSetting;->start:I

    if-le p1, v1, :cond_1

    sub-int/2addr p1, v1

    .line 180
    iput p1, p0, Lepson/print/screen/PageRangeSetting;->start:I

    .line 181
    iget-object p1, p0, Lepson/print/screen/PageRangeSetting;->startPage:Landroid/widget/TextView;

    iget v2, p0, Lepson/print/screen/PageRangeSetting;->start:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 182
    iget-object p1, p0, Lepson/print/screen/PageRangeSetting;->startPagePlus:Landroid/widget/Button;

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 183
    iget-object p1, p0, Lepson/print/screen/PageRangeSetting;->endPageMinus:Landroid/widget/Button;

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 184
    iget p1, p0, Lepson/print/screen/PageRangeSetting;->start:I

    if-ne p1, v1, :cond_1

    .line 185
    iget-object p1, p0, Lepson/print/screen/PageRangeSetting;->startPageMinus:Landroid/widget/Button;

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_1

    .line 190
    :sswitch_2
    iget p1, p0, Lepson/print/screen/PageRangeSetting;->end:I

    iget v2, p0, Lepson/print/screen/PageRangeSetting;->sheets:I

    if-ge p1, v2, :cond_1

    add-int/2addr p1, v1

    .line 191
    iput p1, p0, Lepson/print/screen/PageRangeSetting;->end:I

    .line 192
    iget-object p1, p0, Lepson/print/screen/PageRangeSetting;->endPage:Landroid/widget/TextView;

    iget v2, p0, Lepson/print/screen/PageRangeSetting;->end:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 193
    iget-object p1, p0, Lepson/print/screen/PageRangeSetting;->endPageMinus:Landroid/widget/Button;

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 194
    iget-object p1, p0, Lepson/print/screen/PageRangeSetting;->startPagePlus:Landroid/widget/Button;

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 195
    iget p1, p0, Lepson/print/screen/PageRangeSetting;->end:I

    iget v1, p0, Lepson/print/screen/PageRangeSetting;->sheets:I

    if-ne p1, v1, :cond_1

    .line 196
    iget-object p1, p0, Lepson/print/screen/PageRangeSetting;->endPagePlus:Landroid/widget/Button;

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_1

    .line 201
    :sswitch_3
    iget p1, p0, Lepson/print/screen/PageRangeSetting;->end:I

    iget v2, p0, Lepson/print/screen/PageRangeSetting;->start:I

    if-le p1, v2, :cond_1

    sub-int/2addr p1, v1

    .line 202
    iput p1, p0, Lepson/print/screen/PageRangeSetting;->end:I

    .line 203
    iget-object p1, p0, Lepson/print/screen/PageRangeSetting;->endPage:Landroid/widget/TextView;

    iget v2, p0, Lepson/print/screen/PageRangeSetting;->end:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 204
    iget p1, p0, Lepson/print/screen/PageRangeSetting;->start:I

    if-le p1, v1, :cond_0

    .line 205
    iget-object p1, p0, Lepson/print/screen/PageRangeSetting;->startPageMinus:Landroid/widget/Button;

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 207
    :cond_0
    iget-object p1, p0, Lepson/print/screen/PageRangeSetting;->startPageMinus:Landroid/widget/Button;

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 209
    :goto_0
    iget-object p1, p0, Lepson/print/screen/PageRangeSetting;->endPagePlus:Landroid/widget/Button;

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 210
    iget p1, p0, Lepson/print/screen/PageRangeSetting;->end:I

    iget v1, p0, Lepson/print/screen/PageRangeSetting;->start:I

    if-ne p1, v1, :cond_1

    .line 211
    iget-object p1, p0, Lepson/print/screen/PageRangeSetting;->endPageMinus:Landroid/widget/Button;

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 212
    iget-object p1, p0, Lepson/print/screen/PageRangeSetting;->startPagePlus:Landroid/widget/Button;

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    :cond_1
    :goto_1
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f080126 -> :sswitch_3
        0x7f080127 -> :sswitch_2
        0x7f08030a -> :sswitch_1
        0x7f08030b -> :sswitch_0
    .end sparse-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .line 28
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0a00a6

    .line 29
    invoke-virtual {p0, p1}, Lepson/print/screen/PageRangeSetting;->setContentView(I)V

    const/4 p1, 0x1

    const v0, 0x7f0e0404

    .line 31
    invoke-virtual {p0, v0, p1}, Lepson/print/screen/PageRangeSetting;->setActionBar(IZ)V

    const v0, 0x7f080267

    .line 33
    invoke-virtual {p0, v0}, Lepson/print/screen/PageRangeSetting;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Switch;

    iput-object v0, p0, Lepson/print/screen/PageRangeSetting;->btnPrintAll:Landroid/widget/Switch;

    .line 35
    invoke-virtual {p0}, Lepson/print/screen/PageRangeSetting;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "SHEETS"

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lepson/print/screen/PageRangeSetting;->sheets:I

    const v0, 0x7f08030c

    .line 38
    invoke-virtual {p0, v0}, Lepson/print/screen/PageRangeSetting;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lepson/print/screen/PageRangeSetting;->startPage:Landroid/widget/TextView;

    .line 40
    invoke-virtual {p0}, Lepson/print/screen/PageRangeSetting;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "startValue"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lepson/print/screen/PageRangeSetting;->start:I

    .line 41
    iget-object v0, p0, Lepson/print/screen/PageRangeSetting;->startPage:Landroid/widget/TextView;

    iget v1, p0, Lepson/print/screen/PageRangeSetting;->start:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f080128

    .line 43
    invoke-virtual {p0, v0}, Lepson/print/screen/PageRangeSetting;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lepson/print/screen/PageRangeSetting;->endPage:Landroid/widget/TextView;

    .line 45
    invoke-virtual {p0}, Lepson/print/screen/PageRangeSetting;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "endValue"

    iget v2, p0, Lepson/print/screen/PageRangeSetting;->sheets:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lepson/print/screen/PageRangeSetting;->end:I

    .line 46
    iget-object v0, p0, Lepson/print/screen/PageRangeSetting;->endPage:Landroid/widget/TextView;

    iget v1, p0, Lepson/print/screen/PageRangeSetting;->end:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f08030b

    .line 48
    invoke-virtual {p0, v0}, Lepson/print/screen/PageRangeSetting;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lepson/print/screen/PageRangeSetting;->startPagePlus:Landroid/widget/Button;

    const v0, 0x7f08030a

    .line 49
    invoke-virtual {p0, v0}, Lepson/print/screen/PageRangeSetting;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lepson/print/screen/PageRangeSetting;->startPageMinus:Landroid/widget/Button;

    const v0, 0x7f080127

    .line 50
    invoke-virtual {p0, v0}, Lepson/print/screen/PageRangeSetting;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lepson/print/screen/PageRangeSetting;->endPagePlus:Landroid/widget/Button;

    const v0, 0x7f080126

    .line 51
    invoke-virtual {p0, v0}, Lepson/print/screen/PageRangeSetting;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lepson/print/screen/PageRangeSetting;->endPageMinus:Landroid/widget/Button;

    .line 52
    iget-object v0, p0, Lepson/print/screen/PageRangeSetting;->startPagePlus:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 53
    iget-object v0, p0, Lepson/print/screen/PageRangeSetting;->startPageMinus:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 54
    iget-object v0, p0, Lepson/print/screen/PageRangeSetting;->endPagePlus:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    iget-object v0, p0, Lepson/print/screen/PageRangeSetting;->endPageMinus:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 56
    iget v0, p0, Lepson/print/screen/PageRangeSetting;->start:I

    const/4 v1, 0x0

    if-ne v0, p1, :cond_1

    .line 58
    iget-object v0, p0, Lepson/print/screen/PageRangeSetting;->startPageMinus:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 59
    iget v0, p0, Lepson/print/screen/PageRangeSetting;->start:I

    iget v2, p0, Lepson/print/screen/PageRangeSetting;->end:I

    if-ge v0, v2, :cond_0

    .line 61
    iget-object v0, p0, Lepson/print/screen/PageRangeSetting;->startPagePlus:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 65
    :cond_0
    iget-object v0, p0, Lepson/print/screen/PageRangeSetting;->startPagePlus:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 68
    :cond_1
    :goto_0
    iget v0, p0, Lepson/print/screen/PageRangeSetting;->end:I

    iget v2, p0, Lepson/print/screen/PageRangeSetting;->sheets:I

    if-ne v0, v2, :cond_3

    .line 70
    iget-object v0, p0, Lepson/print/screen/PageRangeSetting;->endPagePlus:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 71
    iget v0, p0, Lepson/print/screen/PageRangeSetting;->end:I

    iget v2, p0, Lepson/print/screen/PageRangeSetting;->start:I

    if-le v0, v2, :cond_2

    .line 73
    iget-object v0, p0, Lepson/print/screen/PageRangeSetting;->endPageMinus:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_1

    .line 77
    :cond_2
    iget-object v0, p0, Lepson/print/screen/PageRangeSetting;->endPageMinus:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 82
    :cond_3
    :goto_1
    iget v0, p0, Lepson/print/screen/PageRangeSetting;->start:I

    iget v2, p0, Lepson/print/screen/PageRangeSetting;->end:I

    if-ne v0, v2, :cond_4

    .line 83
    iget-object v0, p0, Lepson/print/screen/PageRangeSetting;->startPagePlus:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 84
    iget-object v0, p0, Lepson/print/screen/PageRangeSetting;->endPageMinus:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 88
    :cond_4
    iget-object v0, p0, Lepson/print/screen/PageRangeSetting;->startPagePlus:Landroid/widget/Button;

    invoke-static {v0}, Lepson/print/widgets/LongTapRepeatAdapter;->bless(Landroid/view/View;)V

    .line 89
    iget-object v0, p0, Lepson/print/screen/PageRangeSetting;->endPagePlus:Landroid/widget/Button;

    invoke-static {v0}, Lepson/print/widgets/LongTapRepeatAdapter;->bless(Landroid/view/View;)V

    .line 90
    iget-object v0, p0, Lepson/print/screen/PageRangeSetting;->startPageMinus:Landroid/widget/Button;

    invoke-static {v0}, Lepson/print/widgets/LongTapRepeatAdapter;->bless(Landroid/view/View;)V

    .line 91
    iget-object v0, p0, Lepson/print/screen/PageRangeSetting;->endPageMinus:Landroid/widget/Button;

    invoke-static {v0}, Lepson/print/widgets/LongTapRepeatAdapter;->bless(Landroid/view/View;)V

    .line 94
    invoke-virtual {p0}, Lepson/print/screen/PageRangeSetting;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "printAll"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 95
    iget-object v2, p0, Lepson/print/screen/PageRangeSetting;->btnPrintAll:Landroid/widget/Switch;

    invoke-virtual {v2, v0}, Landroid/widget/Switch;->setChecked(Z)V

    .line 97
    iget-object v0, p0, Lepson/print/screen/PageRangeSetting;->btnPrintAll:Landroid/widget/Switch;

    invoke-virtual {v0}, Landroid/widget/Switch;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 98
    iget-object v0, p0, Lepson/print/screen/PageRangeSetting;->startPagePlus:Landroid/widget/Button;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 99
    iget-object v0, p0, Lepson/print/screen/PageRangeSetting;->startPageMinus:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 100
    iget-object v0, p0, Lepson/print/screen/PageRangeSetting;->endPagePlus:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 101
    iget-object v0, p0, Lepson/print/screen/PageRangeSetting;->endPageMinus:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 102
    iput p1, p0, Lepson/print/screen/PageRangeSetting;->start:I

    .line 103
    iget v0, p0, Lepson/print/screen/PageRangeSetting;->sheets:I

    iput v0, p0, Lepson/print/screen/PageRangeSetting;->end:I

    .line 104
    iget-object v0, p0, Lepson/print/screen/PageRangeSetting;->startPage:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 105
    iget-object v0, p0, Lepson/print/screen/PageRangeSetting;->endPage:Landroid/widget/TextView;

    iget v2, p0, Lepson/print/screen/PageRangeSetting;->sheets:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 106
    iget v0, p0, Lepson/print/screen/PageRangeSetting;->start:I

    if-ne v0, p1, :cond_5

    .line 108
    iget-object v0, p0, Lepson/print/screen/PageRangeSetting;->startPageMinus:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 109
    iget v0, p0, Lepson/print/screen/PageRangeSetting;->start:I

    iget v2, p0, Lepson/print/screen/PageRangeSetting;->end:I

    if-ge v0, v2, :cond_5

    .line 111
    iget-object v0, p0, Lepson/print/screen/PageRangeSetting;->startPagePlus:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 114
    :cond_5
    iget v0, p0, Lepson/print/screen/PageRangeSetting;->end:I

    iget v2, p0, Lepson/print/screen/PageRangeSetting;->sheets:I

    if-ne v0, v2, :cond_6

    .line 116
    iget-object v0, p0, Lepson/print/screen/PageRangeSetting;->endPagePlus:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 117
    iget v0, p0, Lepson/print/screen/PageRangeSetting;->end:I

    iget v1, p0, Lepson/print/screen/PageRangeSetting;->start:I

    if-le v0, v1, :cond_6

    .line 119
    iget-object v0, p0, Lepson/print/screen/PageRangeSetting;->endPageMinus:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 124
    :cond_6
    iget-object p1, p0, Lepson/print/screen/PageRangeSetting;->btnPrintAll:Landroid/widget/Switch;

    new-instance v0, Lepson/print/screen/PageRangeSetting$1;

    invoke-direct {v0, p0}, Lepson/print/screen/PageRangeSetting$1;-><init>(Lepson/print/screen/PageRangeSetting;)V

    invoke-virtual {p1, v0}, Landroid/widget/Switch;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .line 222
    invoke-virtual {p0}, Lepson/print/screen/PageRangeSetting;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0b0003

    .line 223
    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 225
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result p1

    return p1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .line 231
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f080212

    if-eq v0, v1, :cond_0

    .line 245
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1

    .line 233
    :cond_0
    new-instance p1, Landroid/content/Intent;

    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    .line 234
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "printAll"

    .line 235
    iget-object v2, p0, Lepson/print/screen/PageRangeSetting;->btnPrintAll:Landroid/widget/Switch;

    invoke-virtual {v2}, Landroid/widget/Switch;->isChecked()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "startValue"

    .line 236
    iget v2, p0, Lepson/print/screen/PageRangeSetting;->start:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "endValue"

    .line 237
    iget v2, p0, Lepson/print/screen/PageRangeSetting;->end:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 238
    invoke-virtual {p1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    const/4 v0, -0x1

    .line 239
    invoke-virtual {p0, v0, p1}, Lepson/print/screen/PageRangeSetting;->setResult(ILandroid/content/Intent;)V

    .line 241
    invoke-virtual {p0}, Lepson/print/screen/PageRangeSetting;->finish()V

    const/4 p1, 0x1

    return p1
.end method

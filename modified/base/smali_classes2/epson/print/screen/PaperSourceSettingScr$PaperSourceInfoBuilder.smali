.class public Lepson/print/screen/PaperSourceSettingScr$PaperSourceInfoBuilder;
.super Lepson/print/widgets/AbstractListBuilder;
.source "PaperSourceSettingScr.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/screen/PaperSourceSettingScr;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "PaperSourceInfoBuilder"
.end annotation


# static fields
.field private static final ACTIVE:I = 0x2

.field private static final SIZE:I = 0x0

.field private static final TYPE:I = 0x1


# instance fields
.field curSetting:Lepson/print/screen/PaperSourceSetting;

.field mPaperSizeLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize;

.field mPaperTypeLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperType;

.field final synthetic this$0:Lepson/print/screen/PaperSourceSettingScr;


# direct methods
.method public constructor <init>(Lepson/print/screen/PaperSourceSettingScr;Landroid/content/Context;Landroid/view/ViewGroup;)V
    .locals 0

    .line 326
    iput-object p1, p0, Lepson/print/screen/PaperSourceSettingScr$PaperSourceInfoBuilder;->this$0:Lepson/print/screen/PaperSourceSettingScr;

    .line 327
    invoke-direct {p0, p2, p3}, Lepson/print/widgets/AbstractListBuilder;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;)V

    .line 320
    new-instance p1, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize;

    invoke-direct {p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize;-><init>()V

    iput-object p1, p0, Lepson/print/screen/PaperSourceSettingScr$PaperSourceInfoBuilder;->mPaperSizeLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize;

    .line 321
    new-instance p1, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperType;

    invoke-direct {p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperType;-><init>()V

    iput-object p1, p0, Lepson/print/screen/PaperSourceSettingScr$PaperSourceInfoBuilder;->mPaperTypeLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperType;

    const/4 p1, 0x0

    .line 324
    iput-object p1, p0, Lepson/print/screen/PaperSourceSettingScr$PaperSourceInfoBuilder;->curSetting:Lepson/print/screen/PaperSourceSetting;

    return-void
.end method


# virtual methods
.method public addPaperInfoSetting(Lepson/print/screen/PaperSourceSetting;)V
    .locals 1

    .line 399
    iget-object v0, p0, Lepson/print/screen/PaperSourceSettingScr$PaperSourceInfoBuilder;->mData:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 400
    iget-object p1, p0, Lepson/print/screen/PaperSourceSettingScr$PaperSourceInfoBuilder;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {p1}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public addPrinter([Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method protected buildData()V
    .locals 1

    .line 332
    iget-object v0, p0, Lepson/print/screen/PaperSourceSettingScr$PaperSourceInfoBuilder;->mData:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    return-void
.end method

.method public buildViewHolder()Landroid/view/View;
    .locals 3

    .line 342
    iget-object v0, p0, Lepson/print/screen/PaperSourceSettingScr$PaperSourceInfoBuilder;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f0a00ad

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public buildViewHolderContent(Landroid/view/View;)Ljava/util/Vector;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")",
            "Ljava/util/Vector<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .line 347
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    const v1, 0x7f080222

    .line 348
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    const v1, 0x7f0801aa

    .line 349
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    const v1, 0x7f080050

    .line 350
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method public buildViewHolderContentData(ILjava/util/Vector;Ljava/util/Vector;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Vector<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/Vector<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 358
    invoke-virtual {p3, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lepson/print/screen/PaperSourceSetting;

    .line 361
    iget-object p3, p0, Lepson/print/screen/PaperSourceSettingScr$PaperSourceInfoBuilder;->this$0:Lepson/print/screen/PaperSourceSettingScr;

    invoke-virtual {p3}, Lepson/print/screen/PaperSourceSettingScr;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    iget-object v0, p0, Lepson/print/screen/PaperSourceSettingScr$PaperSourceInfoBuilder;->mPaperSizeLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize;

    iget v1, p1, Lepson/print/screen/PaperSourceSetting;->paperSizeId:I

    .line 362
    invoke-virtual {v0, v1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize;->getStringId(I)I

    move-result v0

    .line 361
    invoke-virtual {p3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p3

    const/4 v0, 0x0

    .line 363
    invoke-virtual {p2, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 366
    iget-object p3, p0, Lepson/print/screen/PaperSourceSettingScr$PaperSourceInfoBuilder;->this$0:Lepson/print/screen/PaperSourceSettingScr;

    invoke-virtual {p3}, Lepson/print/screen/PaperSourceSettingScr;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    iget-object v1, p0, Lepson/print/screen/PaperSourceSettingScr$PaperSourceInfoBuilder;->mPaperTypeLookupTable:Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperType;

    iget v2, p1, Lepson/print/screen/PaperSourceSetting;->paperTypeId:I

    .line 367
    invoke-virtual {v1, v2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperType;->getStringId(I)I

    move-result v1

    .line 366
    invoke-virtual {p3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p3

    const/4 v1, 0x1

    .line 368
    invoke-virtual {p2, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 p3, 0x2

    .line 371
    invoke-virtual {p2, p3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Landroid/view/View;

    iget-object v1, p0, Lepson/print/screen/PaperSourceSettingScr$PaperSourceInfoBuilder;->curSetting:Lepson/print/screen/PaperSourceSetting;

    invoke-virtual {v1, p1}, Lepson/print/screen/PaperSourceSetting;->equals(Lepson/print/screen/PaperSourceSetting;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    const/16 v1, 0x8

    :goto_0
    invoke-virtual {p3, v1}, Landroid/view/View;->setVisibility(I)V

    .line 374
    iget-boolean p1, p1, Lepson/print/screen/PaperSourceSetting;->bSupportESCPR:Z

    if-nez p1, :cond_1

    .line 375
    invoke-virtual {p2, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iget-object p2, p0, Lepson/print/screen/PaperSourceSettingScr$PaperSourceInfoBuilder;->mContext:Landroid/content/Context;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const p3, 0x7f05007b

    invoke-virtual {p2, p3}, Landroid/content/res/Resources;->getColor(I)I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    .line 377
    :cond_1
    invoke-virtual {p2, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iget-object p2, p0, Lepson/print/screen/PaperSourceSettingScr$PaperSourceInfoBuilder;->mContext:Landroid/content/Context;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const p3, 0x7f05001a

    invoke-virtual {p2, p3}, Landroid/content/res/Resources;->getColor(I)I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_1
    return-void
.end method

.method public getIndexer()Landroid/widget/AlphabetIndexer;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected notifyDataChange()V
    .locals 0

    return-void
.end method

.method public setResource(Ljava/lang/Object;)V
    .locals 0

    .line 390
    check-cast p1, Lepson/print/screen/PaperSourceSetting;

    iput-object p1, p0, Lepson/print/screen/PaperSourceSettingScr$PaperSourceInfoBuilder;->curSetting:Lepson/print/screen/PaperSourceSetting;

    return-void
.end method

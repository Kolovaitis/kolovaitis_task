.class public Lepson/print/screen/PaperSourceInfo;
.super Ljava/lang/Object;
.source "PaperSourceInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/screen/PaperSourceInfo$PaparInfoTask;
    }
.end annotation


# static fields
.field private static final CHECK_INTEVAL:J = 0xfa0L

.field private static final EPS_COMM_BID:I = 0x2

.field public static final KEY_PAPERSOURCEINFO:Ljava/lang/String; = "PAPERSOURCEINFO"

.field public static final NOTIFY_PAPERSOURCEINFO:I = 0x64

.field private static final TAG:Ljava/lang/String; = "PaperSourceInfo"

.field public static instance:Lepson/print/screen/PaperSourceInfo;


# instance fields
.field private mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

.field private paparInfoTask:Lepson/print/screen/PaperSourceInfo$PaparInfoTask;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 45
    iput-object v0, p0, Lepson/print/screen/PaperSourceInfo;->paparInfoTask:Lepson/print/screen/PaperSourceInfo$PaparInfoTask;

    .line 50
    invoke-static {}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->getInstance()Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object v0

    iput-object v0, p0, Lepson/print/screen/PaperSourceInfo;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    .line 54
    invoke-static {}, Lcom/epson/mobilephone/common/EpLog;->i()V

    .line 56
    iget-object v0, p0, Lepson/print/screen/PaperSourceInfo;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    const/4 v1, 0x2

    invoke-virtual {v0, p1, v1}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doInitDriver(Landroid/content/Context;I)I

    return-void
.end method

.method static synthetic access$000(Lepson/print/screen/PaperSourceInfo;)Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;
    .locals 0

    .line 28
    iget-object p0, p0, Lepson/print/screen/PaperSourceInfo;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    return-object p0
.end method

.method public static getInstance(Landroid/content/Context;)Lepson/print/screen/PaperSourceInfo;
    .locals 1

    .line 64
    sget-object v0, Lepson/print/screen/PaperSourceInfo;->instance:Lepson/print/screen/PaperSourceInfo;

    if-nez v0, :cond_0

    .line 65
    new-instance v0, Lepson/print/screen/PaperSourceInfo;

    invoke-direct {v0, p0}, Lepson/print/screen/PaperSourceInfo;-><init>(Landroid/content/Context;)V

    sput-object v0, Lepson/print/screen/PaperSourceInfo;->instance:Lepson/print/screen/PaperSourceInfo;

    .line 68
    :cond_0
    sget-object p0, Lepson/print/screen/PaperSourceInfo;->instance:Lepson/print/screen/PaperSourceInfo;

    return-object p0
.end method


# virtual methods
.method public checkPaperMissmatch(Lepson/print/screen/PrintSetting;Ljava/util/ArrayList;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lepson/print/screen/PrintSetting;",
            "Ljava/util/ArrayList<",
            "Lepson/print/screen/PaperSourceSetting;",
            ">;)Z"
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 246
    :goto_0
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 248
    iget v2, p1, Lepson/print/screen/PrintSetting;->paperSourceValue:I

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lepson/print/screen/PaperSourceSetting;

    iget v3, v3, Lepson/print/screen/PaperSourceSetting;->paperSource:I

    if-ne v2, v3, :cond_1

    .line 250
    iget v2, p1, Lepson/print/screen/PrintSetting;->paperSizeValue:I

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lepson/print/screen/PaperSourceSetting;

    iget v3, v3, Lepson/print/screen/PaperSourceSetting;->paperSizeId:I

    if-ne v2, v3, :cond_0

    iget v2, p1, Lepson/print/screen/PrintSetting;->paperTypeValue:I

    .line 251
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lepson/print/screen/PaperSourceSetting;

    iget v3, v3, Lepson/print/screen/PaperSourceSetting;->paperTypeId:I

    if-eq v2, v3, :cond_1

    :cond_0
    const/4 p1, 0x1

    return p1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return v0
.end method

.method public start(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 3

    const-string v0, "PaperSourceInfo"

    const-string v1, "call start()"

    .line 75
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    iget-object v0, p0, Lepson/print/screen/PaperSourceInfo;->paparInfoTask:Lepson/print/screen/PaperSourceInfo$PaparInfoTask;

    if-nez v0, :cond_0

    .line 79
    new-instance v0, Lepson/print/screen/PaperSourceInfo$PaparInfoTask;

    invoke-direct {v0, p0}, Lepson/print/screen/PaperSourceInfo$PaparInfoTask;-><init>(Lepson/print/screen/PaperSourceInfo;)V

    iput-object v0, p0, Lepson/print/screen/PaperSourceInfo;->paparInfoTask:Lepson/print/screen/PaperSourceInfo$PaparInfoTask;

    .line 80
    iget-object v0, p0, Lepson/print/screen/PaperSourceInfo;->paparInfoTask:Lepson/print/screen/PaperSourceInfo$PaparInfoTask;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 p1, 0x1

    aput-object p2, v1, p1

    invoke-virtual {v0, v1}, Lepson/print/screen/PaperSourceInfo$PaparInfoTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    return-void
.end method

.method public stop()V
    .locals 2

    const-string v0, "PaperSourceInfo"

    const-string v1, "call stop()"

    .line 89
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    iget-object v0, p0, Lepson/print/screen/PaperSourceInfo;->paparInfoTask:Lepson/print/screen/PaperSourceInfo$PaparInfoTask;

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lepson/print/screen/PaperSourceInfo;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doCancelFindPrinter()I

    .line 93
    iget-object v0, p0, Lepson/print/screen/PaperSourceInfo;->paparInfoTask:Lepson/print/screen/PaperSourceInfo$PaparInfoTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lepson/print/screen/PaperSourceInfo$PaparInfoTask;->cancel(Z)Z

    const/4 v0, 0x0

    .line 94
    iput-object v0, p0, Lepson/print/screen/PaperSourceInfo;->paparInfoTask:Lepson/print/screen/PaperSourceInfo$PaparInfoTask;

    :cond_0
    return-void
.end method

.class Lepson/print/screen/ActivityIpPrinterSetting$7;
.super Ljava/lang/Object;
.source "ActivityIpPrinterSetting.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/screen/ActivityIpPrinterSetting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/screen/ActivityIpPrinterSetting;


# direct methods
.method constructor <init>(Lepson/print/screen/ActivityIpPrinterSetting;)V
    .locals 0

    .line 400
    iput-object p1, p0, Lepson/print/screen/ActivityIpPrinterSetting$7;->this$0:Lepson/print/screen/ActivityIpPrinterSetting;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 2

    .line 405
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 407
    iget p1, p1, Landroid/os/Message;->what:I

    if-eqz p1, :cond_1

    const/16 v1, 0x8

    if-eq p1, v1, :cond_0

    goto :goto_0

    :cond_0
    const-string p1, "ActivityIpPrinterSetting"

    const-string v1, "Recieve ADD_SCANNER"

    .line 416
    invoke-static {p1, v1}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    iget-object p1, p0, Lepson/print/screen/ActivityIpPrinterSetting$7;->this$0:Lepson/print/screen/ActivityIpPrinterSetting;

    iget-object p1, p1, Lepson/print/screen/ActivityIpPrinterSetting;->printerInfo:Lepson/print/EPPrinterInfo;

    const-string v1, "id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lepson/print/EPPrinterInfo;->scannerID:Ljava/lang/String;

    goto :goto_0

    :cond_1
    const-string p1, "ActivityIpPrinterSetting"

    const-string v1, "Recieve ADD_PRINTER"

    .line 409
    invoke-static {p1, v1}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    iget-object p1, p0, Lepson/print/screen/ActivityIpPrinterSetting$7;->this$0:Lepson/print/screen/ActivityIpPrinterSetting;

    iget-object p1, p1, Lepson/print/screen/ActivityIpPrinterSetting;->printerInfo:Lepson/print/EPPrinterInfo;

    const-string v1, "name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p1, Lepson/print/EPPrinterInfo;->printerName:Ljava/lang/String;

    .line 411
    iget-object p1, p0, Lepson/print/screen/ActivityIpPrinterSetting$7;->this$0:Lepson/print/screen/ActivityIpPrinterSetting;

    iget-object p1, p1, Lepson/print/screen/ActivityIpPrinterSetting;->printerInfo:Lepson/print/EPPrinterInfo;

    const-string v1, "id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p1, Lepson/print/EPPrinterInfo;->printerID:Ljava/lang/String;

    .line 412
    iget-object p1, p0, Lepson/print/screen/ActivityIpPrinterSetting$7;->this$0:Lepson/print/screen/ActivityIpPrinterSetting;

    iget-object p1, p1, Lepson/print/screen/ActivityIpPrinterSetting;->printerInfo:Lepson/print/EPPrinterInfo;

    const-string v1, "serial_no"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lepson/print/EPPrinterInfo;->printerSerialNo:Ljava/lang/String;

    :goto_0
    const/4 p1, 0x0

    return p1
.end method

.class Lepson/print/screen/ActivityPrinterSetting$setRemotePrintTask;
.super Landroid/os/AsyncTask;
.source "ActivityPrinterSetting.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/screen/ActivityPrinterSetting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "setRemotePrintTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field progress:Lepson/print/screen/WorkingDialog;

.field final synthetic this$0:Lepson/print/screen/ActivityPrinterSetting;


# direct methods
.method private constructor <init>(Lepson/print/screen/ActivityPrinterSetting;)V
    .locals 0

    .line 707
    iput-object p1, p0, Lepson/print/screen/ActivityPrinterSetting$setRemotePrintTask;->this$0:Lepson/print/screen/ActivityPrinterSetting;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    const/4 p1, 0x0

    .line 708
    iput-object p1, p0, Lepson/print/screen/ActivityPrinterSetting$setRemotePrintTask;->progress:Lepson/print/screen/WorkingDialog;

    return-void
.end method

.method synthetic constructor <init>(Lepson/print/screen/ActivityPrinterSetting;Lepson/print/screen/ActivityPrinterSetting$1;)V
    .locals 0

    .line 707
    invoke-direct {p0, p1}, Lepson/print/screen/ActivityPrinterSetting$setRemotePrintTask;-><init>(Lepson/print/screen/ActivityPrinterSetting;)V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 4

    .line 719
    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting$setRemotePrintTask;->this$0:Lepson/print/screen/ActivityPrinterSetting;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->setPriorityToSimpleAP(Landroid/content/Context;Z)V

    .line 721
    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting$setRemotePrintTask;->this$0:Lepson/print/screen/ActivityPrinterSetting;

    .line 722
    invoke-static {p1}, Lepson/print/screen/ActivityPrinterSetting;->access$800(Lepson/print/screen/ActivityPrinterSetting;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lepson/print/screen/ActivityPrinterSetting$setRemotePrintTask;->this$0:Lepson/print/screen/ActivityPrinterSetting;

    invoke-static {v2}, Lepson/print/screen/ActivityPrinterSetting;->access$900(Lepson/print/screen/ActivityPrinterSetting;)Ljava/lang/String;

    move-result-object v2

    .line 721
    invoke-static {p1, v1, v2}, Lepson/print/ecclient/EpsonConnectAccess;->registRemotePrinter(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    const/4 v1, 0x6

    const/16 v2, -0x4b3

    if-ne p1, v2, :cond_1

    .line 727
    new-instance v2, Landroid/os/Message;

    invoke-direct {v2}, Landroid/os/Message;-><init>()V

    .line 728
    iget-object v3, p0, Lepson/print/screen/ActivityPrinterSetting$setRemotePrintTask;->this$0:Lepson/print/screen/ActivityPrinterSetting;

    invoke-static {v3}, Lepson/print/screen/ActivityPrinterSetting;->access$500(Lepson/print/screen/ActivityPrinterSetting;)Landroid/widget/FrameLayout;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v3

    if-eqz v3, :cond_0

    .line 730
    new-instance p1, Landroid/os/Message;

    invoke-direct {p1}, Landroid/os/Message;-><init>()V

    const/4 v1, 0x4

    .line 731
    iput v1, p1, Landroid/os/Message;->what:I

    .line 732
    iput v0, p1, Landroid/os/Message;->arg1:I

    .line 733
    iget-object v1, p0, Lepson/print/screen/ActivityPrinterSetting$setRemotePrintTask;->this$0:Lepson/print/screen/ActivityPrinterSetting;

    iget-object v1, v1, Lepson/print/screen/ActivityPrinterSetting;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 736
    :cond_0
    iput v1, v2, Landroid/os/Message;->what:I

    .line 737
    iput p1, v2, Landroid/os/Message;->arg1:I

    .line 738
    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting$setRemotePrintTask;->this$0:Lepson/print/screen/ActivityPrinterSetting;

    iget-object p1, p1, Lepson/print/screen/ActivityPrinterSetting;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 740
    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    :cond_1
    if-eqz p1, :cond_2

    .line 743
    new-instance v2, Landroid/os/Message;

    invoke-direct {v2}, Landroid/os/Message;-><init>()V

    .line 744
    iput v1, v2, Landroid/os/Message;->what:I

    .line 745
    iput p1, v2, Landroid/os/Message;->arg1:I

    .line 746
    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting$setRemotePrintTask;->this$0:Lepson/print/screen/ActivityPrinterSetting;

    iget-object p1, p1, Lepson/print/screen/ActivityPrinterSetting;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 747
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    .line 750
    :cond_2
    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting$setRemotePrintTask;->this$0:Lepson/print/screen/ActivityPrinterSetting;

    .line 752
    invoke-static {p1}, Lepson/print/screen/ActivityPrinterSetting;->access$800(Lepson/print/screen/ActivityPrinterSetting;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lepson/print/screen/ActivityPrinterSetting$setRemotePrintTask;->this$0:Lepson/print/screen/ActivityPrinterSetting;

    invoke-static {v1}, Lepson/print/screen/ActivityPrinterSetting;->access$900(Lepson/print/screen/ActivityPrinterSetting;)Ljava/lang/String;

    move-result-object v1

    .line 751
    invoke-static {p1, v0, v1}, Lepson/print/ecclient/EpsonConnectAccess;->getPrinterInfo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lepson/print/ecclient/EpsonConnectAccess$RemotePrinterInfo;

    move-result-object p1

    if-eqz p1, :cond_4

    .line 756
    iget-object v0, p0, Lepson/print/screen/ActivityPrinterSetting$setRemotePrintTask;->this$0:Lepson/print/screen/ActivityPrinterSetting;

    invoke-static {v0}, Lepson/print/screen/ActivityPrinterSetting;->access$1000(Lepson/print/screen/ActivityPrinterSetting;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lepson/print/screen/ActivityPrinterSetting$setRemotePrintTask;->this$0:Lepson/print/screen/ActivityPrinterSetting;

    invoke-static {v0}, Lepson/print/screen/ActivityPrinterSetting;->access$1000(Lepson/print/screen/ActivityPrinterSetting;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lepson/print/screen/ActivityPrinterSetting$setRemotePrintTask;->this$0:Lepson/print/screen/ActivityPrinterSetting;

    invoke-static {v1}, Lepson/print/screen/ActivityPrinterSetting;->access$800(Lepson/print/screen/ActivityPrinterSetting;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 757
    iget-object v0, p0, Lepson/print/screen/ActivityPrinterSetting$setRemotePrintTask;->this$0:Lepson/print/screen/ActivityPrinterSetting;

    invoke-static {v0}, Lepson/print/screen/ActivityPrinterSetting;->access$1000(Lepson/print/screen/ActivityPrinterSetting;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lepson/print/screen/ActivityPrinterSetting;->deletePrinterInfo(Ljava/lang/String;)Z

    .line 761
    :cond_3
    iget-object v0, p0, Lepson/print/screen/ActivityPrinterSetting$setRemotePrintTask;->this$0:Lepson/print/screen/ActivityPrinterSetting;

    invoke-virtual {v0, p1}, Lepson/print/screen/ActivityPrinterSetting;->savePrinterInfo(Lepson/print/ecclient/EpsonConnectAccess$RemotePrinterInfo;)V

    :cond_4
    const/4 p1, 0x1

    .line 764
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 707
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/print/screen/ActivityPrinterSetting$setRemotePrintTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 1

    .line 769
    iget-object v0, p0, Lepson/print/screen/ActivityPrinterSetting$setRemotePrintTask;->progress:Lepson/print/screen/WorkingDialog;

    invoke-virtual {v0}, Lepson/print/screen/WorkingDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 770
    iget-object v0, p0, Lepson/print/screen/ActivityPrinterSetting$setRemotePrintTask;->progress:Lepson/print/screen/WorkingDialog;

    invoke-virtual {v0}, Lepson/print/screen/WorkingDialog;->dismiss()V

    .line 774
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 775
    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting$setRemotePrintTask;->this$0:Lepson/print/screen/ActivityPrinterSetting;

    iget-object p1, p1, Lepson/print/screen/ActivityPrinterSetting;->mHandler:Landroid/os/Handler;

    const/4 v0, 0x7

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_1
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 707
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lepson/print/screen/ActivityPrinterSetting$setRemotePrintTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .line 711
    new-instance v0, Lepson/print/screen/WorkingDialog;

    iget-object v1, p0, Lepson/print/screen/ActivityPrinterSetting$setRemotePrintTask;->this$0:Lepson/print/screen/ActivityPrinterSetting;

    invoke-static {v1}, Lepson/print/screen/ActivityPrinterSetting;->access$700(Lepson/print/screen/ActivityPrinterSetting;)Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lepson/print/screen/WorkingDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lepson/print/screen/ActivityPrinterSetting$setRemotePrintTask;->progress:Lepson/print/screen/WorkingDialog;

    .line 712
    iget-object v0, p0, Lepson/print/screen/ActivityPrinterSetting$setRemotePrintTask;->progress:Lepson/print/screen/WorkingDialog;

    invoke-virtual {v0}, Lepson/print/screen/WorkingDialog;->show()V

    return-void
.end method

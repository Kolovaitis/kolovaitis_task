.class public Lepson/print/screen/ConfirmMultiScr;
.super Landroid/app/Activity;
.source "ConfirmMultiScr.java"


# static fields
.field public static EXTRA_DRAWABLE_ID:Ljava/lang/String; = "extra_drawable_id"


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .line 20
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0a0021

    .line 21
    invoke-virtual {p0, p1}, Lepson/print/screen/ConfirmMultiScr;->setContentView(I)V

    .line 23
    invoke-virtual {p0}, Lepson/print/screen/ConfirmMultiScr;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const v0, 0x7f0801b7

    .line 26
    invoke-virtual {p0, v0}, Lepson/print/screen/ConfirmMultiScr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    sget-object v1, Lepson/print/screen/ConfirmMultiScr;->EXTRA_DRAWABLE_ID:Ljava/lang/String;

    const v2, 0x7f0700ea

    .line 27
    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p1

    .line 26
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    const p1, 0x7f0800b3

    .line 31
    invoke-virtual {p0, p1}, Lepson/print/screen/ConfirmMultiScr;->findViewById(I)Landroid/view/View;

    move-result-object p1

    new-instance v0, Lepson/print/screen/ConfirmMultiScr$1;

    invoke-direct {v0, p0}, Lepson/print/screen/ConfirmMultiScr$1;-><init>(Lepson/print/screen/ConfirmMultiScr;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f0800dc

    .line 39
    invoke-virtual {p0, p1}, Lepson/print/screen/ConfirmMultiScr;->findViewById(I)Landroid/view/View;

    move-result-object p1

    new-instance v0, Lepson/print/screen/ConfirmMultiScr$2;

    invoke-direct {v0, p0}, Lepson/print/screen/ConfirmMultiScr$2;-><init>(Lepson/print/screen/ConfirmMultiScr;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

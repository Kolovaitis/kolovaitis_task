.class public Lepson/print/screen/SearchPrinterScr;
.super Lepson/print/ActivityIACommon;
.source "SearchPrinterScr.java"

# interfaces
.implements Lepson/print/CustomTitleDialogFragment$Callback;


# static fields
.field private static final DELAY:I = 0x64

.field private static final DIALOG_ID_PRINTER_NOT_FOUND_WITH_WEB_GUIDANCE:I = 0x1

.field private static final DIALOG_INFORM:I = 0x0

.field private static final DIALOG_TAG_PRINTER_NOT_FOUND:Ljava/lang/String; = "printer_not_found_dialog"

.field private static final EXTRA_PRINTER:Ljava/lang/String; = "myprinter"

.field private static final EXTRA_SIMPLEAP:Ljava/lang/String; = "simpleap"

.field private static final Menu_Delete:I = 0x1

.field private static final Menu_Edit:I = 0x2

.field private static final TAG:Ljava/lang/String; = "SearchPrinterScr"

.field private static foundPrinter:Z = false

.field private static final mLock:Ljava/lang/Object;


# instance fields
.field private final CANCEL_FIND_PRINTER:I

.field private final CONNECT_SIMPLEAP:I

.field private final DELETE_PRINTER:I

.field private final EDIT_IPPRINTER:I

.field private final EDIT_PRINTER:I

.field private final FOUND_SIMPLEAP:I

.field private final INFORM_DIALOG:I

.field private final PRINTER_COMMON_DEVICENAME:Ljava/lang/String;

.field private final PRINTER_ID:Ljava/lang/String;

.field private final PRINTER_IP:Ljava/lang/String;

.field private final PRINTER_NAME:Ljava/lang/String;

.field private final PRINTER_SERIAL_NO:Ljava/lang/String;

.field private final PROBE_PRINTER:I

.field private final SEARCH_PRINTER:I

.field private final SEARCH_PRINTER_P2P:I

.field private final SELECT_PRINTER:I

.field private final SHOW_PRINTER_NOT_FOUND_DIALOG:I

.field private final UPDATE_PRINTER:I

.field private bCheckWiFiStatus:Z

.field private connectionInfo:Ljava/lang/String;

.field curError:I

.field helper:Lepson/print/widgets/ListControlHelper;

.field private isDialogOpen:Z

.field private volatile isFinishSearchPrinter:Z

.field private isFocused:Ljava/lang/Boolean;

.field private isJapaneseLocale:Z

.field private volatile isSearchSimpleAp:Z

.field private listItemIndex:Landroid/widget/AdapterView$AdapterContextMenuInfo;

.field mAboutRemoteButton:Landroid/view/View;

.field private mActivityForegroundLifetime:Z

.field mAddButton:Landroid/widget/Button;

.field mBuilder:Lepson/print/widgets/AbstractListBuilder;

.field private mCallback:Lepson/print/service/IEpsonServiceCallback;

.field private mContext:Landroid/content/Context;

.field private mDeletePos:I

.field private mEpsonConnection:Landroid/content/ServiceConnection;

.field private mEpsonService:Lepson/print/service/IEpsonService;

.field mHandler:Landroid/os/Handler;

.field mIpButton:Landroid/widget/RadioButton;

.field mIsClickSelect:Z

.field mLayout:Landroid/view/ViewGroup;

.field private mListEmptyMessageTextView:Landroid/widget/TextView;

.field private mListView:Landroid/widget/ListView;

.field mLocalButton:Landroid/widget/RadioButton;

.field mProgressBar:Landroid/widget/ProgressBar;

.field mRemoteButton:Landroid/widget/RadioButton;

.field mSearchButton:Landroid/widget/Button;

.field mTextDetail:Landroid/widget/TextView;

.field mWiFiSettingButton:Landroid/view/View;

.field private printerEmailAddress:Ljava/lang/String;

.field private printerId:Ljava/lang/String;

.field private printerKey:Ljava/lang/String;

.field private printerList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private volatile printer_location:I

.field private probedPrinter:Lepson/print/MyPrinter;

.field progress:Lepson/print/screen/WorkingDialog;

.field private searchWiFiDirectPrinter:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

.field private selectedPrinter:Lepson/print/MyPrinter;

.field wiFiDirectPrinterListUtils:Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 114
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lepson/print/screen/SearchPrinterScr;->mLock:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .line 70
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    const/4 v0, 0x0

    .line 107
    iput-boolean v0, p0, Lepson/print/screen/SearchPrinterScr;->mIsClickSelect:Z

    const/4 v1, 0x1

    .line 108
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lepson/print/screen/SearchPrinterScr;->isFocused:Ljava/lang/Boolean;

    .line 109
    iput-boolean v1, p0, Lepson/print/screen/SearchPrinterScr;->isFinishSearchPrinter:Z

    .line 112
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lepson/print/screen/SearchPrinterScr;->printerList:Ljava/util/ArrayList;

    const/4 v2, 0x0

    .line 116
    iput-object v2, p0, Lepson/print/screen/SearchPrinterScr;->mContext:Landroid/content/Context;

    const/4 v3, -0x1

    .line 117
    iput v3, p0, Lepson/print/screen/SearchPrinterScr;->mDeletePos:I

    const-string v3, ""

    .line 118
    iput-object v3, p0, Lepson/print/screen/SearchPrinterScr;->printerId:Ljava/lang/String;

    const-string v3, ""

    .line 119
    iput-object v3, p0, Lepson/print/screen/SearchPrinterScr;->printerEmailAddress:Ljava/lang/String;

    const-string v3, ""

    .line 120
    iput-object v3, p0, Lepson/print/screen/SearchPrinterScr;->printerKey:Ljava/lang/String;

    .line 122
    iput-object v2, p0, Lepson/print/screen/SearchPrinterScr;->probedPrinter:Lepson/print/MyPrinter;

    .line 123
    iput-object v2, p0, Lepson/print/screen/SearchPrinterScr;->selectedPrinter:Lepson/print/MyPrinter;

    .line 125
    iput v0, p0, Lepson/print/screen/SearchPrinterScr;->printer_location:I

    .line 127
    iput-boolean v0, p0, Lepson/print/screen/SearchPrinterScr;->isJapaneseLocale:Z

    .line 129
    iput-object v2, p0, Lepson/print/screen/SearchPrinterScr;->searchWiFiDirectPrinter:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    .line 130
    iput-boolean v0, p0, Lepson/print/screen/SearchPrinterScr;->bCheckWiFiStatus:Z

    .line 131
    iput-boolean v0, p0, Lepson/print/screen/SearchPrinterScr;->isSearchSimpleAp:Z

    .line 134
    iput-object v2, p0, Lepson/print/screen/SearchPrinterScr;->connectionInfo:Ljava/lang/String;

    .line 137
    iput-object v2, p0, Lepson/print/screen/SearchPrinterScr;->wiFiDirectPrinterListUtils:Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;

    .line 138
    iput-object v2, p0, Lepson/print/screen/SearchPrinterScr;->helper:Lepson/print/widgets/ListControlHelper;

    const-string v3, "name"

    .line 733
    iput-object v3, p0, Lepson/print/screen/SearchPrinterScr;->PRINTER_NAME:Ljava/lang/String;

    const-string v3, "ip"

    .line 734
    iput-object v3, p0, Lepson/print/screen/SearchPrinterScr;->PRINTER_IP:Ljava/lang/String;

    const-string v3, "id"

    .line 735
    iput-object v3, p0, Lepson/print/screen/SearchPrinterScr;->PRINTER_ID:Ljava/lang/String;

    const-string v3, "serial_no"

    .line 736
    iput-object v3, p0, Lepson/print/screen/SearchPrinterScr;->PRINTER_SERIAL_NO:Ljava/lang/String;

    const-string v3, "common_devicename"

    .line 737
    iput-object v3, p0, Lepson/print/screen/SearchPrinterScr;->PRINTER_COMMON_DEVICENAME:Ljava/lang/String;

    .line 738
    iput v1, p0, Lepson/print/screen/SearchPrinterScr;->SEARCH_PRINTER:I

    const/4 v1, 0x2

    .line 739
    iput v1, p0, Lepson/print/screen/SearchPrinterScr;->CANCEL_FIND_PRINTER:I

    const/4 v1, 0x3

    .line 740
    iput v1, p0, Lepson/print/screen/SearchPrinterScr;->UPDATE_PRINTER:I

    const/4 v1, 0x4

    .line 741
    iput v1, p0, Lepson/print/screen/SearchPrinterScr;->SELECT_PRINTER:I

    const/4 v1, 0x5

    .line 742
    iput v1, p0, Lepson/print/screen/SearchPrinterScr;->INFORM_DIALOG:I

    const/4 v1, 0x7

    .line 743
    iput v1, p0, Lepson/print/screen/SearchPrinterScr;->DELETE_PRINTER:I

    const/16 v1, 0x8

    .line 744
    iput v1, p0, Lepson/print/screen/SearchPrinterScr;->EDIT_PRINTER:I

    const/16 v1, 0x9

    .line 745
    iput v1, p0, Lepson/print/screen/SearchPrinterScr;->FOUND_SIMPLEAP:I

    const/16 v1, 0xa

    .line 746
    iput v1, p0, Lepson/print/screen/SearchPrinterScr;->CONNECT_SIMPLEAP:I

    const/16 v1, 0xb

    .line 747
    iput v1, p0, Lepson/print/screen/SearchPrinterScr;->EDIT_IPPRINTER:I

    const/16 v1, 0xc

    .line 748
    iput v1, p0, Lepson/print/screen/SearchPrinterScr;->PROBE_PRINTER:I

    const/16 v1, 0xd

    .line 749
    iput v1, p0, Lepson/print/screen/SearchPrinterScr;->SHOW_PRINTER_NOT_FOUND_DIALOG:I

    const/16 v1, 0xe

    .line 750
    iput v1, p0, Lepson/print/screen/SearchPrinterScr;->SEARCH_PRINTER_P2P:I

    .line 754
    new-instance v1, Landroid/os/Handler;

    new-instance v3, Lepson/print/screen/SearchPrinterScr$13;

    invoke-direct {v3, p0}, Lepson/print/screen/SearchPrinterScr$13;-><init>(Lepson/print/screen/SearchPrinterScr;)V

    invoke-direct {v1, v3}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v1, p0, Lepson/print/screen/SearchPrinterScr;->mHandler:Landroid/os/Handler;

    .line 1312
    iput-boolean v0, p0, Lepson/print/screen/SearchPrinterScr;->isDialogOpen:Z

    .line 1450
    iput-object v2, p0, Lepson/print/screen/SearchPrinterScr;->mEpsonService:Lepson/print/service/IEpsonService;

    .line 1451
    new-instance v0, Lepson/print/screen/SearchPrinterScr$15;

    invoke-direct {v0, p0}, Lepson/print/screen/SearchPrinterScr$15;-><init>(Lepson/print/screen/SearchPrinterScr;)V

    iput-object v0, p0, Lepson/print/screen/SearchPrinterScr;->mEpsonConnection:Landroid/content/ServiceConnection;

    .line 1476
    new-instance v0, Lepson/print/screen/SearchPrinterScr$16;

    invoke-direct {v0, p0}, Lepson/print/screen/SearchPrinterScr$16;-><init>(Lepson/print/screen/SearchPrinterScr;)V

    iput-object v0, p0, Lepson/print/screen/SearchPrinterScr;->mCallback:Lepson/print/service/IEpsonServiceCallback;

    return-void
.end method

.method static synthetic access$000(Lepson/print/screen/SearchPrinterScr;)V
    .locals 0

    .line 70
    invoke-direct {p0}, Lepson/print/screen/SearchPrinterScr;->startWifiPrinterSelect()V

    return-void
.end method

.method static synthetic access$100(Lepson/print/screen/SearchPrinterScr;)Ljava/lang/Boolean;
    .locals 0

    .line 70
    iget-object p0, p0, Lepson/print/screen/SearchPrinterScr;->isFocused:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic access$1000(Lepson/print/screen/SearchPrinterScr;)Z
    .locals 0

    .line 70
    iget-boolean p0, p0, Lepson/print/screen/SearchPrinterScr;->isSearchSimpleAp:Z

    return p0
.end method

.method static synthetic access$1002(Lepson/print/screen/SearchPrinterScr;Z)Z
    .locals 0

    .line 70
    iput-boolean p1, p0, Lepson/print/screen/SearchPrinterScr;->isSearchSimpleAp:Z

    return p1
.end method

.method static synthetic access$102(Lepson/print/screen/SearchPrinterScr;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0

    .line 70
    iput-object p1, p0, Lepson/print/screen/SearchPrinterScr;->isFocused:Ljava/lang/Boolean;

    return-object p1
.end method

.method static synthetic access$1100()Ljava/lang/Object;
    .locals 1

    .line 70
    sget-object v0, Lepson/print/screen/SearchPrinterScr;->mLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1200(Lepson/print/screen/SearchPrinterScr;)Ljava/util/ArrayList;
    .locals 0

    .line 70
    iget-object p0, p0, Lepson/print/screen/SearchPrinterScr;->printerList:Ljava/util/ArrayList;

    return-object p0
.end method

.method static synthetic access$1300()Z
    .locals 1

    .line 70
    sget-boolean v0, Lepson/print/screen/SearchPrinterScr;->foundPrinter:Z

    return v0
.end method

.method static synthetic access$1302(Z)Z
    .locals 0

    .line 70
    sput-boolean p0, Lepson/print/screen/SearchPrinterScr;->foundPrinter:Z

    return p0
.end method

.method static synthetic access$1400(Lepson/print/screen/SearchPrinterScr;)Z
    .locals 0

    .line 70
    iget-boolean p0, p0, Lepson/print/screen/SearchPrinterScr;->isFinishSearchPrinter:Z

    return p0
.end method

.method static synthetic access$1402(Lepson/print/screen/SearchPrinterScr;Z)Z
    .locals 0

    .line 70
    iput-boolean p1, p0, Lepson/print/screen/SearchPrinterScr;->isFinishSearchPrinter:Z

    return p1
.end method

.method static synthetic access$1500(Lepson/print/screen/SearchPrinterScr;)Lepson/print/service/IEpsonService;
    .locals 0

    .line 70
    iget-object p0, p0, Lepson/print/screen/SearchPrinterScr;->mEpsonService:Lepson/print/service/IEpsonService;

    return-object p0
.end method

.method static synthetic access$1502(Lepson/print/screen/SearchPrinterScr;Lepson/print/service/IEpsonService;)Lepson/print/service/IEpsonService;
    .locals 0

    .line 70
    iput-object p1, p0, Lepson/print/screen/SearchPrinterScr;->mEpsonService:Lepson/print/service/IEpsonService;

    return-object p1
.end method

.method static synthetic access$1600(Lepson/print/screen/SearchPrinterScr;)Z
    .locals 0

    .line 70
    iget-boolean p0, p0, Lepson/print/screen/SearchPrinterScr;->isDialogOpen:Z

    return p0
.end method

.method static synthetic access$1602(Lepson/print/screen/SearchPrinterScr;Z)Z
    .locals 0

    .line 70
    iput-boolean p1, p0, Lepson/print/screen/SearchPrinterScr;->isDialogOpen:Z

    return p1
.end method

.method static synthetic access$1700(Lepson/print/screen/SearchPrinterScr;)Landroid/support/v4/app/DialogFragment;
    .locals 0

    .line 70
    invoke-direct {p0}, Lepson/print/screen/SearchPrinterScr;->getPrinterNotFoundDialog()Landroid/support/v4/app/DialogFragment;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$1800(Lepson/print/screen/SearchPrinterScr;Landroid/support/v4/app/DialogFragment;)V
    .locals 0

    .line 70
    invoke-direct {p0, p1}, Lepson/print/screen/SearchPrinterScr;->showPrinterNotFoundDialog(Landroid/support/v4/app/DialogFragment;)V

    return-void
.end method

.method static synthetic access$1900(Lepson/print/screen/SearchPrinterScr;)Lepson/print/MyPrinter;
    .locals 0

    .line 70
    iget-object p0, p0, Lepson/print/screen/SearchPrinterScr;->selectedPrinter:Lepson/print/MyPrinter;

    return-object p0
.end method

.method static synthetic access$1902(Lepson/print/screen/SearchPrinterScr;Lepson/print/MyPrinter;)Lepson/print/MyPrinter;
    .locals 0

    .line 70
    iput-object p1, p0, Lepson/print/screen/SearchPrinterScr;->selectedPrinter:Lepson/print/MyPrinter;

    return-object p1
.end method

.method static synthetic access$200(Lepson/print/screen/SearchPrinterScr;)I
    .locals 0

    .line 70
    iget p0, p0, Lepson/print/screen/SearchPrinterScr;->printer_location:I

    return p0
.end method

.method static synthetic access$2000(Lepson/print/screen/SearchPrinterScr;)Lepson/print/MyPrinter;
    .locals 0

    .line 70
    iget-object p0, p0, Lepson/print/screen/SearchPrinterScr;->probedPrinter:Lepson/print/MyPrinter;

    return-object p0
.end method

.method static synthetic access$2002(Lepson/print/screen/SearchPrinterScr;Lepson/print/MyPrinter;)Lepson/print/MyPrinter;
    .locals 0

    .line 70
    iput-object p1, p0, Lepson/print/screen/SearchPrinterScr;->probedPrinter:Lepson/print/MyPrinter;

    return-object p1
.end method

.method static synthetic access$202(Lepson/print/screen/SearchPrinterScr;I)I
    .locals 0

    .line 70
    iput p1, p0, Lepson/print/screen/SearchPrinterScr;->printer_location:I

    return p1
.end method

.method static synthetic access$2100(Lepson/print/screen/SearchPrinterScr;)Lepson/print/service/IEpsonServiceCallback;
    .locals 0

    .line 70
    iget-object p0, p0, Lepson/print/screen/SearchPrinterScr;->mCallback:Lepson/print/service/IEpsonServiceCallback;

    return-object p0
.end method

.method static synthetic access$300(Lepson/print/screen/SearchPrinterScr;)V
    .locals 0

    .line 70
    invoke-direct {p0}, Lepson/print/screen/SearchPrinterScr;->interruptSearch()V

    return-void
.end method

.method static synthetic access$400(Lepson/print/screen/SearchPrinterScr;)I
    .locals 0

    .line 70
    iget p0, p0, Lepson/print/screen/SearchPrinterScr;->mDeletePos:I

    return p0
.end method

.method static synthetic access$402(Lepson/print/screen/SearchPrinterScr;I)I
    .locals 0

    .line 70
    iput p1, p0, Lepson/print/screen/SearchPrinterScr;->mDeletePos:I

    return p1
.end method

.method static synthetic access$500(Lepson/print/screen/SearchPrinterScr;)Landroid/widget/ListView;
    .locals 0

    .line 70
    iget-object p0, p0, Lepson/print/screen/SearchPrinterScr;->mListView:Landroid/widget/ListView;

    return-object p0
.end method

.method static synthetic access$600(Lepson/print/screen/SearchPrinterScr;)Landroid/content/Context;
    .locals 0

    .line 70
    iget-object p0, p0, Lepson/print/screen/SearchPrinterScr;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic access$700(Lepson/print/screen/SearchPrinterScr;)V
    .locals 0

    .line 70
    invoke-direct {p0}, Lepson/print/screen/SearchPrinterScr;->buildElements()V

    return-void
.end method

.method static synthetic access$800(Lepson/print/screen/SearchPrinterScr;)V
    .locals 0

    .line 70
    invoke-direct {p0}, Lepson/print/screen/SearchPrinterScr;->search()V

    return-void
.end method

.method static synthetic access$900(Lepson/print/screen/SearchPrinterScr;)Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;
    .locals 0

    .line 70
    iget-object p0, p0, Lepson/print/screen/SearchPrinterScr;->searchWiFiDirectPrinter:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    return-object p0
.end method

.method static synthetic access$902(Lepson/print/screen/SearchPrinterScr;Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;)Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;
    .locals 0

    .line 70
    iput-object p1, p0, Lepson/print/screen/SearchPrinterScr;->searchWiFiDirectPrinter:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    return-object p1
.end method

.method private buildElements()V
    .locals 9

    .line 278
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr;->mLayout:Landroid/view/ViewGroup;

    const v1, 0x7f08015c

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lepson/print/screen/SearchPrinterScr;->mSearchButton:Landroid/widget/Button;

    .line 279
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr;->mLayout:Landroid/view/ViewGroup;

    const v1, 0x7f080298

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lepson/print/screen/SearchPrinterScr;->mProgressBar:Landroid/widget/ProgressBar;

    .line 280
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr;->mLayout:Landroid/view/ViewGroup;

    const v1, 0x7f080121

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lepson/print/screen/SearchPrinterScr;->mAddButton:Landroid/widget/Button;

    .line 281
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr;->mLayout:Landroid/view/ViewGroup;

    const v1, 0x7f080105

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lepson/print/screen/SearchPrinterScr;->mTextDetail:Landroid/widget/TextView;

    const/4 v0, 0x0

    .line 285
    invoke-virtual {p0, v0}, Lepson/print/screen/SearchPrinterScr;->searchButtonSetEnabled(Z)V

    .line 287
    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 288
    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr;->mAddButton:Landroid/widget/Button;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 289
    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr;->mTextDetail:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 292
    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr;->mWiFiSettingButton:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 294
    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr;->mLayout:Landroid/view/ViewGroup;

    const v3, 0x7f080098

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p0, Lepson/print/screen/SearchPrinterScr;->mLocalButton:Landroid/widget/RadioButton;

    .line 295
    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr;->mLayout:Landroid/view/ViewGroup;

    const v3, 0x7f080097

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p0, Lepson/print/screen/SearchPrinterScr;->mIpButton:Landroid/widget/RadioButton;

    .line 296
    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr;->mLayout:Landroid/view/ViewGroup;

    const v3, 0x7f08009e

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p0, Lepson/print/screen/SearchPrinterScr;->mRemoteButton:Landroid/widget/RadioButton;

    .line 298
    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr;->mLocalButton:Landroid/widget/RadioButton;

    const v3, 0x7f07011c

    invoke-static {p0, v1, v3}, Lepson/common/Utils;->setDrawble2TextView(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 299
    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr;->mIpButton:Landroid/widget/RadioButton;

    const v3, 0x7f07011b

    invoke-static {p0, v1, v3}, Lepson/common/Utils;->setDrawble2TextView(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 300
    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr;->mRemoteButton:Landroid/widget/RadioButton;

    const v3, 0x7f07011d

    invoke-static {p0, v1, v3}, Lepson/common/Utils;->setDrawble2TextView(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 306
    iget v1, p0, Lepson/print/screen/SearchPrinterScr;->printer_location:I

    const v3, 0x7f0e0437

    const v4, 0x7f08022b

    const/4 v5, 0x1

    const/4 v6, 0x0

    packed-switch v1, :pswitch_data_0

    goto/16 :goto_0

    .line 339
    :pswitch_0
    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr;->mAddButton:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 341
    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr;->mListEmptyMessageTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 342
    iget-object v6, p0, Lepson/print/screen/SearchPrinterScr;->mListEmptyMessageTextView:Landroid/widget/TextView;

    .line 344
    new-instance v1, Lepson/print/widgets/PrinterInfoIpBuilder;

    invoke-virtual {p0}, Lepson/print/screen/SearchPrinterScr;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    iget-object v7, p0, Lepson/print/screen/SearchPrinterScr;->mLayout:Landroid/view/ViewGroup;

    invoke-direct {v1, v3, v7, v0}, Lepson/print/widgets/PrinterInfoIpBuilder;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;I)V

    iput-object v1, p0, Lepson/print/screen/SearchPrinterScr;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    .line 347
    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    iget-object v3, p0, Lepson/print/screen/SearchPrinterScr;->printerKey:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lepson/print/widgets/AbstractListBuilder;->setResource(Ljava/lang/Object;)V

    .line 350
    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr;->mIpButton:Landroid/widget/RadioButton;

    invoke-virtual {v1, v5}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 352
    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr;->mTextDetail:Landroid/widget/TextView;

    const v3, 0x7f0e0487

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 355
    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr;->mSearchButton:Landroid/widget/Button;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 358
    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 361
    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr;->mWiFiSettingButton:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 310
    :pswitch_1
    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr;->mAddButton:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 313
    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr;->mListEmptyMessageTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 314
    iget-object v6, p0, Lepson/print/screen/SearchPrinterScr;->mListEmptyMessageTextView:Landroid/widget/TextView;

    .line 316
    new-instance v1, Lepson/print/widgets/PrinterInfoECBuilder;

    invoke-virtual {p0}, Lepson/print/screen/SearchPrinterScr;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    iget-object v7, p0, Lepson/print/screen/SearchPrinterScr;->mLayout:Landroid/view/ViewGroup;

    invoke-direct {v1, v3, v7}, Lepson/print/widgets/PrinterInfoECBuilder;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;)V

    iput-object v1, p0, Lepson/print/screen/SearchPrinterScr;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    .line 319
    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    iget-object v3, p0, Lepson/print/screen/SearchPrinterScr;->printerEmailAddress:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lepson/print/widgets/AbstractListBuilder;->setResource(Ljava/lang/Object;)V

    .line 322
    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr;->mRemoteButton:Landroid/widget/RadioButton;

    invoke-virtual {v1, v5}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 324
    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr;->mTextDetail:Landroid/widget/TextView;

    const v3, 0x7f0e048b

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 327
    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr;->mSearchButton:Landroid/widget/Button;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 330
    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 333
    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr;->mWiFiSettingButton:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 368
    :pswitch_2
    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, v6}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 369
    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr;->mListEmptyMessageTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 370
    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr;->mListEmptyMessageTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 372
    new-instance v1, Lepson/print/widgets/PrinterInfoBuilder;

    invoke-virtual {p0}, Lepson/print/screen/SearchPrinterScr;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    iget-object v7, p0, Lepson/print/screen/SearchPrinterScr;->mLayout:Landroid/view/ViewGroup;

    invoke-direct {v1, v3, v7, v0}, Lepson/print/widgets/PrinterInfoBuilder;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;I)V

    iput-object v1, p0, Lepson/print/screen/SearchPrinterScr;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    .line 376
    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    iget-object v3, p0, Lepson/print/screen/SearchPrinterScr;->printerId:Ljava/lang/String;

    iget-object v7, p0, Lepson/print/screen/SearchPrinterScr;->connectionInfo:Ljava/lang/String;

    invoke-static {p0, v3, v7}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->getCurPrinterString(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lepson/print/widgets/AbstractListBuilder;->setResource(Ljava/lang/Object;)V

    .line 379
    new-instance v1, Lepson/print/widgets/ListControlHelper;

    iget-object v3, p0, Lepson/print/screen/SearchPrinterScr;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    check-cast v3, Lepson/print/widgets/PrinterInfoBuilder;

    invoke-direct {v1, v3}, Lepson/print/widgets/ListControlHelper;-><init>(Lepson/print/widgets/PrinterInfoBuilder;)V

    iput-object v1, p0, Lepson/print/screen/SearchPrinterScr;->helper:Lepson/print/widgets/ListControlHelper;

    .line 380
    new-instance v1, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;

    iget-object v3, p0, Lepson/print/screen/SearchPrinterScr;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {v3}, Lepson/print/widgets/AbstractListBuilder;->getData()Ljava/util/Vector;

    move-result-object v3

    iget-object v7, p0, Lepson/print/screen/SearchPrinterScr;->helper:Lepson/print/widgets/ListControlHelper;

    iget-object v8, p0, Lepson/print/screen/SearchPrinterScr;->connectionInfo:Ljava/lang/String;

    invoke-direct {v1, p0, v3, v7, v8}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;-><init>(Landroid/content/Context;Ljava/util/AbstractList;Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils$ListController;Ljava/lang/String;)V

    iput-object v1, p0, Lepson/print/screen/SearchPrinterScr;->wiFiDirectPrinterListUtils:Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;

    .line 383
    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr;->mLocalButton:Landroid/widget/RadioButton;

    invoke-virtual {v1, v5}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 385
    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr;->mTextDetail:Landroid/widget/TextView;

    const v3, 0x7f0e0489

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 388
    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 393
    :goto_0
    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {v1}, Lepson/print/widgets/AbstractListBuilder;->build()V

    .line 394
    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {v1}, Lepson/print/widgets/AbstractListBuilder;->refresh()V

    .line 395
    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, v6}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 398
    iget v1, p0, Lepson/print/screen/SearchPrinterScr;->printer_location:I

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 399
    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr;->mAboutRemoteButton:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 401
    :cond_0
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr;->mAboutRemoteButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 404
    :goto_1
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr;->mListView:Landroid/widget/ListView;

    new-instance v1, Lepson/print/screen/SearchPrinterScr$4;

    invoke-direct {v1, p0}, Lepson/print/screen/SearchPrinterScr$4;-><init>(Lepson/print/screen/SearchPrinterScr;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 449
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr;->mListView:Landroid/widget/ListView;

    new-instance v1, Lepson/print/screen/SearchPrinterScr$5;

    invoke-direct {v1, p0}, Lepson/print/screen/SearchPrinterScr$5;-><init>(Lepson/print/screen/SearchPrinterScr;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 464
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr;->mAddButton:Landroid/widget/Button;

    new-instance v1, Lepson/print/screen/SearchPrinterScr$6;

    invoke-direct {v1, p0}, Lepson/print/screen/SearchPrinterScr$6;-><init>(Lepson/print/screen/SearchPrinterScr;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 551
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr;->mSearchButton:Landroid/widget/Button;

    new-instance v1, Lepson/print/screen/SearchPrinterScr$7;

    invoke-direct {v1, p0}, Lepson/print/screen/SearchPrinterScr$7;-><init>(Lepson/print/screen/SearchPrinterScr;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 560
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr;->mLocalButton:Landroid/widget/RadioButton;

    new-instance v1, Lepson/print/screen/SearchPrinterScr$8;

    invoke-direct {v1, p0}, Lepson/print/screen/SearchPrinterScr$8;-><init>(Lepson/print/screen/SearchPrinterScr;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 582
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr;->mRemoteButton:Landroid/widget/RadioButton;

    new-instance v1, Lepson/print/screen/SearchPrinterScr$9;

    invoke-direct {v1, p0}, Lepson/print/screen/SearchPrinterScr$9;-><init>(Lepson/print/screen/SearchPrinterScr;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 604
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr;->mIpButton:Landroid/widget/RadioButton;

    new-instance v1, Lepson/print/screen/SearchPrinterScr$10;

    invoke-direct {v1, p0}, Lepson/print/screen/SearchPrinterScr$10;-><init>(Lepson/print/screen/SearchPrinterScr;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private dismissPrinterNotFoundDialog()V
    .locals 2

    .line 1364
    invoke-virtual {p0}, Lepson/print/screen/SearchPrinterScr;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "printer_not_found_dialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    if-eqz v0, :cond_0

    .line 1366
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_0
    return-void
.end method

.method private getPrinterNotFoundDialog()Landroid/support/v4/app/DialogFragment;
    .locals 7

    .line 1273
    invoke-virtual {p0}, Lepson/print/screen/SearchPrinterScr;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "PrintSetting"

    const-string v2, "RE_SEARCH"

    invoke-static {v0, v1, v2}, Lepson/common/Utils;->getPrefBoolean(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    const v1, 0x7f0e01b5

    const v2, 0x7f0e0473

    const/4 v3, 0x1

    if-eqz v0, :cond_1

    const/16 v0, -0x2af9

    .line 1276
    iput v0, p0, Lepson/print/screen/SearchPrinterScr;->curError:I

    .line 1278
    invoke-static {p0}, Lepson/common/Utils;->getSSID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 1279
    invoke-static {p0}, Lepson/common/Utils;->isConnectedWifi(Landroid/content/Context;)Z

    move-result v4

    const/4 v5, 0x0

    if-eqz v4, :cond_0

    if-eqz v0, :cond_0

    const v4, 0x7f0e00be

    .line 1281
    invoke-virtual {p0, v4}, Lepson/print/screen/SearchPrinterScr;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v6, v3, [Ljava/lang/Object;

    aput-object v0, v6, v5

    invoke-static {v4, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const v0, 0x7f0e01ae

    .line 1285
    invoke-virtual {p0, v0}, Lepson/print/screen/SearchPrinterScr;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1284
    invoke-static {v3, v0, v1, v5, v2}, Lepson/print/CustomLayoutDialogFragment;->newInstance(ILjava/lang/String;III)Lepson/print/CustomLayoutDialogFragment;

    move-result-object v0

    return-object v0

    :cond_1
    const/16 v0, -0x44c

    .line 1290
    iput v0, p0, Lepson/print/screen/SearchPrinterScr;->curError:I

    const v1, 0x7f0e01b2

    const v0, 0x7f0e01a8

    .line 1293
    invoke-virtual {p0, v0}, Lepson/print/screen/SearchPrinterScr;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1296
    :goto_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\n\n"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const v0, 0x7f0e0309

    invoke-virtual {p0, v0}, Lepson/print/screen/SearchPrinterScr;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const v4, 0x7f0e02ea

    .line 1297
    invoke-static {v3, v0, v1, v4, v2}, Lepson/print/CustomLayoutDialogFragment;->newInstance(ILjava/lang/String;III)Lepson/print/CustomLayoutDialogFragment;

    move-result-object v0

    return-object v0
.end method

.method private interruptSearch()V
    .locals 3

    const-string v0, "SearchPrinterScr"

    const-string v1, "interruptSearch()"

    .line 1199
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1202
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1203
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr;->mHandler:Landroid/os/Handler;

    const/16 v2, 0xe

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 1204
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 1205
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x9

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 1206
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr;->mHandler:Landroid/os/Handler;

    const/16 v2, 0xd

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 1209
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr;->searchWiFiDirectPrinter:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    if-eqz v0, :cond_0

    .line 1210
    invoke-virtual {v0}, Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;->interrupt()V

    const/4 v0, 0x0

    .line 1211
    iput-object v0, p0, Lepson/print/screen/SearchPrinterScr;->searchWiFiDirectPrinter:Lcom/epson/mobilephone/common/wifidirect/SearchWiFiDirectPrinterTask;

    :cond_0
    const/4 v0, 0x0

    .line 1213
    iput-boolean v0, p0, Lepson/print/screen/SearchPrinterScr;->isSearchSimpleAp:Z

    .line 1215
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr;->mEpsonService:Lepson/print/service/IEpsonService;

    if-eqz v0, :cond_1

    .line 1217
    :try_start_0
    invoke-interface {v0}, Lepson/print/service/IEpsonService;->cancelSearchPrinter()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 1219
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 1224
    :cond_1
    :goto_0
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr;->progress:Lepson/print/screen/WorkingDialog;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lepson/print/screen/WorkingDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1225
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr;->progress:Lepson/print/screen/WorkingDialog;

    invoke-virtual {v0}, Lepson/print/screen/WorkingDialog;->dismiss()V

    .line 1229
    :cond_2
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lepson/print/screen/SearchPrinterScr;->isFocused:Ljava/lang/Boolean;

    return-void
.end method

.method private search()V
    .locals 6

    .line 1126
    invoke-direct {p0}, Lepson/print/screen/SearchPrinterScr;->interruptSearch()V

    .line 1129
    sget-object v0, Lepson/print/screen/SearchPrinterScr;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1130
    :try_start_0
    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr;->printerList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 1131
    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {v1}, Lepson/print/widgets/AbstractListBuilder;->refresh()V

    .line 1132
    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr;->wiFiDirectPrinterListUtils:Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;

    invoke-virtual {v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectPrinterListUtils;->clearPrinterInfoList()V

    .line 1133
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v0, 0x0

    .line 1136
    iput-boolean v0, p0, Lepson/print/screen/SearchPrinterScr;->mIsClickSelect:Z

    .line 1139
    iput-boolean v0, p0, Lepson/print/screen/SearchPrinterScr;->isFinishSearchPrinter:Z

    .line 1140
    invoke-virtual {p0, v0}, Lepson/print/screen/SearchPrinterScr;->searchButtonSetEnabled(Z)V

    .line 1142
    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1143
    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr;->mLayout:Landroid/view/ViewGroup;

    const v2, 0x7f080123

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f0e045f

    .line 1144
    invoke-virtual {p0, v2}, Lepson/print/screen/SearchPrinterScr;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v1, 0x1

    .line 1147
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lepson/print/screen/SearchPrinterScr;->isFocused:Ljava/lang/Boolean;

    .line 1149
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->getCurSSID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0xe

    if-eqz v2, :cond_0

    .line 1152
    iget-object v2, p0, Lepson/print/screen/SearchPrinterScr;->mHandler:Landroid/os/Handler;

    const-wide/16 v4, 0x1388

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1153
    iput-boolean v1, p0, Lepson/print/screen/SearchPrinterScr;->isSearchSimpleAp:Z

    goto :goto_0

    .line 1156
    :cond_0
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->isWifiEnabled(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1158
    iget-object v2, p0, Lepson/print/screen/SearchPrinterScr;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1159
    iput-boolean v1, p0, Lepson/print/screen/SearchPrinterScr;->isSearchSimpleAp:Z

    goto :goto_0

    .line 1160
    :cond_1
    iget-boolean v2, p0, Lepson/print/screen/SearchPrinterScr;->bCheckWiFiStatus:Z

    if-nez v2, :cond_2

    if-nez v2, :cond_2

    .line 1163
    iput-boolean v1, p0, Lepson/print/screen/SearchPrinterScr;->bCheckWiFiStatus:Z

    const/4 v0, -0x1

    .line 1164
    invoke-static {p0, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->enableWiFi(Landroid/app/Activity;I)V

    return-void

    .line 1176
    :cond_2
    :goto_0
    invoke-static {p0, v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->setPriorityToSimpleAP(Landroid/content/Context;Z)V

    .line 1178
    iget-object v2, p0, Lepson/print/screen/SearchPrinterScr;->mEpsonService:Lepson/print/service/IEpsonService;

    if-eqz v2, :cond_3

    .line 1180
    :try_start_1
    sget-object v2, Lepson/print/screen/SearchPrinterScr;->mLock:Ljava/lang/Object;

    monitor-enter v2
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1181
    :try_start_2
    iput-boolean v0, p0, Lepson/print/screen/SearchPrinterScr;->isFinishSearchPrinter:Z

    .line 1182
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1183
    :try_start_3
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr;->mEpsonService:Lepson/print/service/IEpsonService;

    const/4 v2, 0x0

    invoke-interface {v0, v2, v2, v1}, Lepson/print/service/IEpsonService;->searchPrinters(Ljava/lang/String;Ljava/lang/String;I)I
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_1

    :catchall_0
    move-exception v0

    .line 1182
    :try_start_4
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v0
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_0

    :catch_0
    move-exception v0

    .line 1186
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 1189
    :cond_3
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :goto_1
    return-void

    :catchall_1
    move-exception v1

    .line 1133
    :try_start_6
    monitor-exit v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v1
.end method

.method private setBleWifiSetupButton()V
    .locals 2

    .line 213
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr;->mLayout:Landroid/view/ViewGroup;

    const v1, 0x7f080071

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private setNotFoundPrinterButton()V
    .locals 2

    .line 221
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr;->mLayout:Landroid/view/ViewGroup;

    const v1, 0x7f08022b

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lepson/print/screen/SearchPrinterScr$1;

    invoke-direct {v1, p0}, Lepson/print/screen/SearchPrinterScr$1;-><init>(Lepson/print/screen/SearchPrinterScr;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private showPrinterNotFoundDialog(Landroid/support/v4/app/DialogFragment;)V
    .locals 2

    .line 1304
    iget-boolean v0, p0, Lepson/print/screen/SearchPrinterScr;->mActivityForegroundLifetime:Z

    if-eqz v0, :cond_0

    .line 1305
    invoke-virtual {p0}, Lepson/print/screen/SearchPrinterScr;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "printer_not_found_dialog"

    invoke-virtual {p1, v0, v1}, Landroid/support/v4/app/DialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private startWifiPrinterSelect()V
    .locals 2

    .line 255
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/epson/mobilephone/common/wifidirect/ActivityWiFiDirectStart;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0xa

    invoke-virtual {p0, v0, v1}, Lepson/print/screen/SearchPrinterScr;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method


# virtual methods
.method addAboutRemoteButton()V
    .locals 2

    .line 263
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr;->mLayout:Landroid/view/ViewGroup;

    const v1, 0x7f080001

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lepson/print/screen/SearchPrinterScr;->mAboutRemoteButton:Landroid/view/View;

    .line 266
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr;->mAboutRemoteButton:Landroid/view/View;

    new-instance v1, Lepson/print/screen/SearchPrinterScr$3;

    invoke-direct {v1, p0}, Lepson/print/screen/SearchPrinterScr$3;-><init>(Lepson/print/screen/SearchPrinterScr;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method addWiFiSetupButton()V
    .locals 2

    .line 235
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr;->mLayout:Landroid/view/ViewGroup;

    const v1, 0x7f0802b4

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lepson/print/screen/SearchPrinterScr;->mWiFiSettingButton:Landroid/view/View;

    .line 238
    iget-boolean v0, p0, Lepson/print/screen/SearchPrinterScr;->isJapaneseLocale:Z

    if-eqz v0, :cond_0

    .line 239
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr;->mWiFiSettingButton:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0e04c4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 241
    :cond_0
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr;->mWiFiSettingButton:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0e0524

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 245
    :goto_0
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr;->mWiFiSettingButton:Landroid/view/View;

    new-instance v1, Lepson/print/screen/SearchPrinterScr$2;

    invoke-direct {v1, p0}, Lepson/print/screen/SearchPrinterScr$2;-><init>(Lepson/print/screen/SearchPrinterScr;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public displaySearchResult()V
    .locals 6

    .line 1234
    iget-boolean v0, p0, Lepson/print/screen/SearchPrinterScr;->mIsClickSelect:Z

    const/4 v1, 0x1

    if-nez v0, :cond_3

    .line 1235
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {v0}, Lepson/print/widgets/AbstractListBuilder;->getData()Ljava/util/Vector;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    const v2, 0x7f080123

    if-lez v0, :cond_2

    .line 1236
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    sget-object v3, Ljava/util/Locale;->JAPAN:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    const v3, 0x7f0e03fd

    if-nez v0, :cond_1

    .line 1237
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    sget-object v4, Ljava/util/Locale;->JAPANESE:Ljava/util/Locale;

    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 1241
    :cond_0
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1242
    invoke-virtual {p0, v3}, Lepson/print/screen/SearchPrinterScr;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v1, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lepson/print/screen/SearchPrinterScr;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {v5}, Lepson/print/widgets/AbstractListBuilder;->getData()Ljava/util/Vector;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Vector;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1238
    :cond_1
    :goto_0
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1239
    invoke-virtual {p0, v3}, Lepson/print/screen/SearchPrinterScr;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1245
    :cond_2
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v2, 0x7f0e01b2

    .line 1246
    invoke-virtual {p0, v2}, Lepson/print/screen/SearchPrinterScr;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1249
    :cond_3
    :goto_1
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {v0}, Lepson/print/widgets/AbstractListBuilder;->getAdapter()Landroid/widget/BaseAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 1250
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr;->mProgressBar:Landroid/widget/ProgressBar;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1254
    invoke-virtual {p0, v1}, Lepson/print/screen/SearchPrinterScr;->searchButtonSetEnabled(Z)V

    .line 1256
    iput-boolean v1, p0, Lepson/print/screen/SearchPrinterScr;->isFinishSearchPrinter:Z

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 8

    .line 1053
    invoke-super {p0, p1, p2, p3}, Lepson/print/ActivityIACommon;->onActivityResult(IILandroid/content/Intent;)V

    .line 1055
    invoke-direct {p0}, Lepson/print/screen/SearchPrinterScr;->interruptSearch()V

    const/4 v0, -0x1

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    goto/16 :goto_1

    :pswitch_1
    if-ne p2, v0, :cond_2

    .line 1073
    new-instance p1, Lepson/print/MyPrinter;

    const-string p2, "PRINTER_NAME"

    invoke-virtual {p3, p2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string p2, "PRINTER_IP"

    .line 1074
    invoke-virtual {p3, p2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string p2, "PRINTER_ID"

    .line 1075
    invoke-virtual {p3, p2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string p2, "PRINTER_SERIAL_NO"

    .line 1076
    invoke-virtual {p3, p2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, ""

    const/4 v7, 0x3

    move-object v1, p1

    invoke-direct/range {v1 .. v7}, Lepson/print/MyPrinter;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 1080
    new-instance p2, Landroid/content/Intent;

    invoke-direct {p2}, Landroid/content/Intent;-><init>()V

    const-string p3, "myprinter"

    .line 1081
    invoke-virtual {p2, p3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1082
    invoke-virtual {p0, v0, p2}, Lepson/print/screen/SearchPrinterScr;->setResult(ILandroid/content/Intent;)V

    .line 1083
    invoke-virtual {p0}, Lepson/print/screen/SearchPrinterScr;->finish()V

    goto/16 :goto_1

    :pswitch_2
    const/4 p1, 0x0

    const/4 v1, 0x1

    if-ne p2, v0, :cond_1

    .line 1090
    new-instance p2, Lepson/print/MyPrinter;

    const-string v2, "name"

    .line 1091
    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "ip"

    .line 1092
    invoke-virtual {p3, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "id"

    .line 1093
    invoke-virtual {p3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "serial_no"

    .line 1094
    invoke-virtual {p3, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {p2, v2, v3, v4, v5}, Lepson/print/MyPrinter;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1097
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v3, "myprinter"

    .line 1098
    invoke-virtual {v2, v3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1103
    invoke-virtual {p2}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Lcom/epson/mobilephone/common/wifidirect/MacAddrUtils;->getMacAddressFromPrinterId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 1101
    invoke-static {p0, p2}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->getCurConnectInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 1104
    iget v3, p0, Lepson/print/screen/SearchPrinterScr;->printer_location:I

    if-ne v3, v1, :cond_0

    if-eqz p2, :cond_0

    const-string v1, "simpleap"

    .line 1105
    invoke-virtual {v2, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    :cond_0
    const-string p2, "simpleap"

    const-string v1, ""

    .line 1107
    invoke-virtual {v2, p2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :goto_0
    const-string p2, "easysetup"

    .line 1110
    invoke-virtual {p3, p2, p1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 1114
    invoke-virtual {p0, v0, v2}, Lepson/print/screen/SearchPrinterScr;->setResult(ILandroid/content/Intent;)V

    .line 1115
    invoke-virtual {p0}, Lepson/print/screen/SearchPrinterScr;->finish()V

    goto :goto_1

    .line 1117
    :cond_1
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    iput-object p2, p0, Lepson/print/screen/SearchPrinterScr;->isFocused:Ljava/lang/Boolean;

    .line 1118
    iput-boolean p1, p0, Lepson/print/screen/SearchPrinterScr;->mIsClickSelect:Z

    goto :goto_1

    :pswitch_3
    if-ne p2, v0, :cond_2

    const-string p1, "my_printer"

    .line 1060
    invoke-virtual {p3, p1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Lepson/print/MyPrinter;

    .line 1062
    new-instance p2, Landroid/content/Intent;

    invoke-direct {p2}, Landroid/content/Intent;-><init>()V

    const-string p3, "myprinter"

    .line 1063
    invoke-virtual {p2, p3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1064
    invoke-virtual {p0, v0, p2}, Lepson/print/screen/SearchPrinterScr;->setResult(ILandroid/content/Intent;)V

    .line 1065
    invoke-virtual {p0}, Lepson/print/screen/SearchPrinterScr;->finish()V

    :cond_2
    :goto_1
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 0

    .line 694
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onBackPressed()V

    .line 698
    invoke-direct {p0}, Lepson/print/screen/SearchPrinterScr;->interruptSearch()V

    .line 700
    invoke-virtual {p0}, Lepson/print/screen/SearchPrinterScr;->finish()V

    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .line 637
    invoke-interface {p1}, Landroid/view/MenuItem;->getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;

    move-result-object v0

    check-cast v0, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    iput-object v0, p0, Lepson/print/screen/SearchPrinterScr;->listItemIndex:Landroid/widget/AdapterView$AdapterContextMenuInfo;

    .line 638
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr;->listItemIndex:Landroid/widget/AdapterView$AdapterContextMenuInfo;

    const/4 v1, 0x1

    if-nez v0, :cond_0

    return v1

    .line 641
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result p1

    packed-switch p1, :pswitch_data_0

    goto :goto_1

    .line 666
    :pswitch_0
    new-instance p1, Landroid/os/Message;

    invoke-direct {p1}, Landroid/os/Message;-><init>()V

    .line 668
    iget v0, p0, Lepson/print/screen/SearchPrinterScr;->printer_location:I

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    const/16 v0, 0xb

    .line 673
    iput v0, p1, Landroid/os/Message;->what:I

    goto :goto_0

    :pswitch_2
    const/16 v0, 0x8

    .line 670
    iput v0, p1, Landroid/os/Message;->what:I

    .line 677
    :goto_0
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {v0}, Lepson/print/widgets/AbstractListBuilder;->getData()Ljava/util/Vector;

    move-result-object v0

    iget v2, p0, Lepson/print/screen/SearchPrinterScr;->mDeletePos:I

    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 678
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1

    .line 644
    :pswitch_3
    new-instance p1, Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr;->mContext:Landroid/content/Context;

    invoke-direct {p1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    .line 645
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const v0, 0x7f0e0316

    .line 646
    invoke-virtual {p0, v0}, Lepson/print/screen/SearchPrinterScr;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 647
    invoke-virtual {p0}, Lepson/print/screen/SearchPrinterScr;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f070099

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const v0, 0x7f0e04f2

    .line 648
    invoke-virtual {p0, v0}, Lepson/print/screen/SearchPrinterScr;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lepson/print/screen/SearchPrinterScr$12;

    invoke-direct {v2, p0}, Lepson/print/screen/SearchPrinterScr$12;-><init>(Lepson/print/screen/SearchPrinterScr;)V

    invoke-virtual {p1, v0, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const v0, 0x7f0e0476

    .line 657
    invoke-virtual {p0, v0}, Lepson/print/screen/SearchPrinterScr;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lepson/print/screen/SearchPrinterScr$11;

    invoke-direct {v2, p0}, Lepson/print/screen/SearchPrinterScr$11;-><init>(Lepson/print/screen/SearchPrinterScr;)V

    invoke-virtual {p1, v0, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 662
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    :goto_1
    return v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .line 145
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    .line 146
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object p1

    sget-object v0, Ljava/util/Locale;->JAPAN:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-nez p1, :cond_1

    .line 147
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object p1

    sget-object v2, Ljava/util/Locale;->JAPANESE:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    .line 150
    :cond_0
    iput-boolean v0, p0, Lepson/print/screen/SearchPrinterScr;->isJapaneseLocale:Z

    goto :goto_1

    .line 148
    :cond_1
    :goto_0
    iput-boolean v1, p0, Lepson/print/screen/SearchPrinterScr;->isJapaneseLocale:Z

    .line 153
    :goto_1
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr;->mEpsonService:Lepson/print/service/IEpsonService;

    if-nez p1, :cond_2

    .line 154
    new-instance p1, Landroid/content/Intent;

    const-class v2, Lepson/print/service/EpsonService;

    invoke-direct {p1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v2, p0, Lepson/print/screen/SearchPrinterScr;->mEpsonConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, p1, v2, v1}, Lepson/print/screen/SearchPrinterScr;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 157
    :cond_2
    invoke-virtual {p0}, Lepson/print/screen/SearchPrinterScr;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object p1

    const v2, 0x7f0a00b8

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, Lepson/print/screen/SearchPrinterScr;->mLayout:Landroid/view/ViewGroup;

    .line 160
    invoke-virtual {p0}, Lepson/print/screen/SearchPrinterScr;->addWiFiSetupButton()V

    .line 163
    invoke-virtual {p0}, Lepson/print/screen/SearchPrinterScr;->addAboutRemoteButton()V

    .line 165
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr;->mLayout:Landroid/view/ViewGroup;

    const v2, 0x7f0801e1

    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lepson/print/screen/SearchPrinterScr;->mListEmptyMessageTextView:Landroid/widget/TextView;

    .line 166
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr;->mLayout:Landroid/view/ViewGroup;

    const v2, 0x102000a

    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ListView;

    iput-object p1, p0, Lepson/print/screen/SearchPrinterScr;->mListView:Landroid/widget/ListView;

    .line 169
    invoke-virtual {p0}, Lepson/print/screen/SearchPrinterScr;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v2, "PRINTER_LOCATION"

    .line 170
    invoke-virtual {p1, v2, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lepson/print/screen/SearchPrinterScr;->printer_location:I

    .line 171
    iget v2, p0, Lepson/print/screen/SearchPrinterScr;->printer_location:I

    if-nez v2, :cond_3

    .line 172
    iput v1, p0, Lepson/print/screen/SearchPrinterScr;->printer_location:I

    .line 176
    :cond_3
    iget v2, p0, Lepson/print/screen/SearchPrinterScr;->printer_location:I

    packed-switch v2, :pswitch_data_0

    const-string v2, "PRINTER_ID"

    .line 184
    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lepson/print/screen/SearchPrinterScr;->printerId:Ljava/lang/String;

    const-string v2, "simpleap"

    .line 185
    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/print/screen/SearchPrinterScr;->connectionInfo:Ljava/lang/String;

    goto :goto_2

    :pswitch_0
    const-string v2, "PRINTER_ID"

    .line 181
    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/print/screen/SearchPrinterScr;->printerKey:Ljava/lang/String;

    goto :goto_2

    :pswitch_1
    const-string v2, "PRINTER_EMAIL_ADDRESS"

    .line 178
    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/print/screen/SearchPrinterScr;->printerEmailAddress:Ljava/lang/String;

    .line 189
    :goto_2
    invoke-direct {p0}, Lepson/print/screen/SearchPrinterScr;->buildElements()V

    .line 190
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {p0, p1}, Lepson/print/screen/SearchPrinterScr;->setContentView(Landroid/view/View;)V

    .line 191
    iput-object p0, p0, Lepson/print/screen/SearchPrinterScr;->mContext:Landroid/content/Context;

    .line 193
    iput-boolean v0, p0, Lepson/print/screen/SearchPrinterScr;->mIsClickSelect:Z

    .line 195
    iget p1, p0, Lepson/print/screen/SearchPrinterScr;->printer_location:I

    if-eq p1, v1, :cond_4

    .line 197
    invoke-virtual {p0}, Lepson/print/screen/SearchPrinterScr;->displaySearchResult()V

    .line 198
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr;->mSearchButton:Landroid/widget/Button;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 199
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr;->mLayout:Landroid/view/ViewGroup;

    const v2, 0x7f080123

    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 203
    :cond_4
    invoke-direct {p0}, Lepson/print/screen/SearchPrinterScr;->setNotFoundPrinterButton()V

    .line 205
    invoke-direct {p0}, Lepson/print/screen/SearchPrinterScr;->setBleWifiSetupButton()V

    const p1, 0x7f0e0536

    .line 208
    invoke-virtual {p0, p1, v1}, Lepson/print/screen/SearchPrinterScr;->setActionBar(IZ)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 2

    .line 628
    invoke-super {p0, p1, p2, p3}, Lepson/print/ActivityIACommon;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    const p2, 0x7f0e049d

    .line 630
    invoke-interface {p1, p2}, Landroid/view/ContextMenu;->setHeaderTitle(I)Landroid/view/ContextMenu;

    const/4 p3, 0x0

    const/4 v0, 0x1

    const v1, 0x7f0e0485

    .line 631
    invoke-interface {p1, p3, v0, p3, v1}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    const/4 v0, 0x2

    .line 632
    invoke-interface {p1, p3, v0, p3, p2}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 6

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    const-string p1, "SearchPrinterScr"

    .line 1317
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DIALOG_INFORM curError = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lepson/print/screen/SearchPrinterScr;->curError:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1318
    iget p1, p0, Lepson/print/screen/SearchPrinterScr;->curError:I

    const/16 v0, -0x44c

    const/16 v1, -0x2af9

    if-eq p1, v0, :cond_1

    if-ne p1, v1, :cond_3

    .line 1321
    :cond_1
    invoke-virtual {p0}, Lepson/print/screen/SearchPrinterScr;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string v2, "PrintSetting"

    const-string v3, "RE_SEARCH"

    invoke-static {p1, v2, v3}, Lepson/common/Utils;->getPrefBoolean(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 1324
    iput v1, p0, Lepson/print/screen/SearchPrinterScr;->curError:I

    goto :goto_0

    .line 1328
    :cond_2
    iput v0, p0, Lepson/print/screen/SearchPrinterScr;->curError:I

    .line 1331
    :cond_3
    :goto_0
    iget p1, p0, Lepson/print/screen/SearchPrinterScr;->curError:I

    invoke-static {p1}, Lcom/epson/mobilephone/common/escpr/MediaInfo$ErrorTable;->getStringId(I)[Ljava/lang/Integer;

    move-result-object p1

    const/4 v0, 0x1

    const/4 v2, 0x0

    if-nez p1, :cond_4

    const/4 p1, 0x2

    .line 1333
    new-array p1, p1, [Ljava/lang/Integer;

    const v3, 0x7f0e023a

    .line 1334
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, p1, v2

    const v3, 0x7f0e023b

    .line 1335
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, p1, v0

    .line 1338
    :cond_4
    aget-object v3, p1, v2

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p0, v3}, Lepson/print/screen/SearchPrinterScr;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1339
    invoke-static {p0}, Lepson/common/Utils;->getSSID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 1340
    invoke-static {p0}, Lepson/common/Utils;->isConnectedWifi(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_5

    iget v5, p0, Lepson/print/screen/SearchPrinterScr;->curError:I

    if-ne v5, v1, :cond_5

    if-eqz v4, :cond_5

    const v1, 0x7f0e00bd

    .line 1343
    invoke-virtual {p0, v1}, Lepson/print/screen/SearchPrinterScr;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v3, v0, [Ljava/lang/Object;

    aput-object v4, v3, v2

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 1346
    :cond_5
    new-instance v1, Lepson/print/widgets/CustomTitleAlertDialogBuilder;

    invoke-direct {v1, p0}, Lepson/print/widgets/CustomTitleAlertDialogBuilder;-><init>(Landroid/content/Context;)V

    aget-object p1, p1, v0

    .line 1347
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {p0, p1}, Lepson/print/screen/SearchPrinterScr;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lepson/print/widgets/CustomTitleAlertDialogBuilder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 1348
    invoke-virtual {p1, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 1349
    invoke-virtual {p1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const v0, 0x7f0e04f2

    .line 1350
    invoke-virtual {p0, v0}, Lepson/print/screen/SearchPrinterScr;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lepson/print/screen/SearchPrinterScr$14;

    invoke-direct {v1, p0}, Lepson/print/screen/SearchPrinterScr$14;-><init>(Lepson/print/screen/SearchPrinterScr;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 1356
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method

.method protected onDestroy()V
    .locals 1

    .line 686
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr;->mBuilder:Lepson/print/widgets/AbstractListBuilder;

    invoke-virtual {v0}, Lepson/print/widgets/AbstractListBuilder;->destructor()V

    .line 687
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onDestroy()V

    .line 689
    invoke-virtual {p0}, Lepson/print/screen/SearchPrinterScr;->unbindEpsonService()V

    return-void
.end method

.method public onLocalNegativeCallback(I)V
    .locals 0

    const/4 p1, 0x0

    .line 1596
    iput-boolean p1, p0, Lepson/print/screen/SearchPrinterScr;->isDialogOpen:Z

    return-void
.end method

.method public onLocalPositiveCallback(I)V
    .locals 1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    return-void

    .line 1587
    :cond_0
    invoke-static {p0}, Lepson/maintain/activity/PrinterNotFoundDialogCreator;->getStartIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object p1

    invoke-virtual {p0, p1}, Lepson/print/screen/SearchPrinterScr;->startActivity(Landroid/content/Intent;)V

    const/4 p1, 0x0

    .line 1588
    iput-boolean p1, p0, Lepson/print/screen/SearchPrinterScr;->isDialogOpen:Z

    return-void
.end method

.method protected onPause()V
    .locals 2

    const-string v0, "SearchPrinterScr"

    const-string v1, "onPause()"

    .line 722
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 723
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onPause()V

    const/4 v0, 0x0

    .line 725
    iput-boolean v0, p0, Lepson/print/screen/SearchPrinterScr;->mActivityForegroundLifetime:Z

    .line 726
    invoke-direct {p0}, Lepson/print/screen/SearchPrinterScr;->interruptSearch()V

    .line 729
    invoke-virtual {p0}, Lepson/print/screen/SearchPrinterScr;->removeAllDialog()V

    return-void
.end method

.method protected onResume()V
    .locals 4

    .line 705
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onResume()V

    const-string v0, "SearchPrinterScr"

    const-string v1, "onResume()"

    .line 706
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 708
    iput-boolean v0, p0, Lepson/print/screen/SearchPrinterScr;->mActivityForegroundLifetime:Z

    .line 710
    invoke-direct {p0}, Lepson/print/screen/SearchPrinterScr;->dismissPrinterNotFoundDialog()V

    .line 713
    iget v1, p0, Lepson/print/screen/SearchPrinterScr;->printer_location:I

    if-ne v1, v0, :cond_0

    .line 714
    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    const-string v0, "SearchPrinterScr"

    const-string v1, "Send CHECK_PRINTER Message."

    .line 715
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method removeAllDialog()V
    .locals 1

    const/4 v0, 0x0

    .line 1376
    :try_start_0
    invoke-virtual {p0, v0}, Lepson/print/screen/SearchPrinterScr;->removeDialog(I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1382
    :catch_0
    :try_start_1
    invoke-direct {p0}, Lepson/print/screen/SearchPrinterScr;->dismissPrinterNotFoundDialog()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    return-void
.end method

.method public searchButtonSetEnabled(Z)V
    .locals 2

    .line 1421
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr;->mSearchButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 1424
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr;->mSearchButton:Landroid/widget/Button;

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 1426
    :cond_0
    iget-object p1, p0, Lepson/print/screen/SearchPrinterScr;->mSearchButton:Landroid/widget/Button;

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setEnabled(Z)V

    :goto_0
    return-void
.end method

.method unbindEpsonService()V
    .locals 4

    .line 1435
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr;->mEpsonService:Lepson/print/service/IEpsonService;

    if-eqz v0, :cond_1

    .line 1436
    iget-boolean v0, p0, Lepson/print/screen/SearchPrinterScr;->isFinishSearchPrinter:Z

    if-nez v0, :cond_0

    .line 1437
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1441
    :cond_0
    :try_start_0
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr;->mEpsonService:Lepson/print/service/IEpsonService;

    iget-object v1, p0, Lepson/print/screen/SearchPrinterScr;->mCallback:Lepson/print/service/IEpsonServiceCallback;

    invoke-interface {v0, v1}, Lepson/print/service/IEpsonService;->unregisterCallback(Lepson/print/service/IEpsonServiceCallback;)V

    .line 1442
    iget-object v0, p0, Lepson/print/screen/SearchPrinterScr;->mEpsonConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lepson/print/screen/SearchPrinterScr;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 1445
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_1
    :goto_0
    return-void
.end method

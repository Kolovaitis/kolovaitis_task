.class public Lepson/print/screen/ActivityIpPrinterSetting;
.super Lepson/print/ActivityIACommon;
.source "ActivityIpPrinterSetting.java"


# static fields
.field private static final IPADDRESS_EXP:Ljava/lang/String; = "^(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})$"

.field public static final PRINTER:I = 0x0

.field private static final PRINTER_ID:Ljava/lang/String; = "id"

.field private static final PRINTER_NAME:Ljava/lang/String; = "name"

.field private static final PRINTER_SERIAL_NO:Ljava/lang/String; = "serial_no"

.field public static final SCANNER:I = 0x1

.field private static final TAG:Ljava/lang/String; = "ActivityIpPrinterSetting"

.field static mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

.field static mScanner:Lepson/scan/lib/escanLib;


# instance fields
.field private final ADD_PRINTER:I

.field private final ADD_SCANNER:I

.field clearPrinterIpAdressButton:Landroid/widget/Button;

.field clearPrinterNameButton:Landroid/widget/Button;

.field mHandler:Landroid/os/Handler;

.field manager:Lepson/print/EPPrinterManager;

.field oldItemKey:Ljava/lang/String;

.field printerInfo:Lepson/print/EPPrinterInfo;

.field printerIpAdress:Landroid/widget/TextView;

.field printerName:Landroid/widget/TextView;

.field printerNameArea:Landroid/view/ViewGroup;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 79
    invoke-static {}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->getInstance()Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object v0

    sput-object v0, Lepson/print/screen/ActivityIpPrinterSetting;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    .line 80
    new-instance v0, Lepson/scan/lib/escanLib;

    invoke-direct {v0}, Lepson/scan/lib/escanLib;-><init>()V

    sput-object v0, Lepson/print/screen/ActivityIpPrinterSetting;->mScanner:Lepson/scan/lib/escanLib;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 49
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    const/4 v0, 0x0

    .line 53
    iput v0, p0, Lepson/print/screen/ActivityIpPrinterSetting;->ADD_PRINTER:I

    const/16 v0, 0x8

    .line 54
    iput v0, p0, Lepson/print/screen/ActivityIpPrinterSetting;->ADD_SCANNER:I

    const/4 v0, 0x0

    .line 74
    iput-object v0, p0, Lepson/print/screen/ActivityIpPrinterSetting;->oldItemKey:Ljava/lang/String;

    .line 400
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lepson/print/screen/ActivityIpPrinterSetting$7;

    invoke-direct {v1, p0}, Lepson/print/screen/ActivityIpPrinterSetting$7;-><init>(Lepson/print/screen/ActivityIpPrinterSetting;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lepson/print/screen/ActivityIpPrinterSetting;->mHandler:Landroid/os/Handler;

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 91
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0a007f

    .line 94
    invoke-virtual {p0, p1}, Lepson/print/screen/ActivityIpPrinterSetting;->setContentView(I)V

    const p1, 0x7f0e04c7

    const/4 v0, 0x1

    .line 96
    invoke-virtual {p0, p1, v0}, Lepson/print/screen/ActivityIpPrinterSetting;->setActionBar(IZ)V

    const p1, 0x7f08027f

    .line 98
    invoke-virtual {p0, p1}, Lepson/print/screen/ActivityIpPrinterSetting;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, Lepson/print/screen/ActivityIpPrinterSetting;->printerNameArea:Landroid/view/ViewGroup;

    const p1, 0x7f080285

    .line 99
    invoke-virtual {p0, p1}, Lepson/print/screen/ActivityIpPrinterSetting;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lepson/print/screen/ActivityIpPrinterSetting;->printerName:Landroid/widget/TextView;

    const p1, 0x7f080283

    .line 100
    invoke-virtual {p0, p1}, Lepson/print/screen/ActivityIpPrinterSetting;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lepson/print/screen/ActivityIpPrinterSetting;->printerIpAdress:Landroid/widget/TextView;

    const p1, 0x7f0800c9

    .line 101
    invoke-virtual {p0, p1}, Lepson/print/screen/ActivityIpPrinterSetting;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lepson/print/screen/ActivityIpPrinterSetting;->clearPrinterNameButton:Landroid/widget/Button;

    const p1, 0x7f0800c8

    .line 102
    invoke-virtual {p0, p1}, Lepson/print/screen/ActivityIpPrinterSetting;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lepson/print/screen/ActivityIpPrinterSetting;->clearPrinterIpAdressButton:Landroid/widget/Button;

    .line 105
    new-instance p1, Lepson/print/EPPrinterManager;

    invoke-direct {p1, p0}, Lepson/print/EPPrinterManager;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lepson/print/screen/ActivityIpPrinterSetting;->manager:Lepson/print/EPPrinterManager;

    .line 108
    invoke-virtual {p0}, Lepson/print/screen/ActivityIpPrinterSetting;->getIntent()Landroid/content/Intent;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 110
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    if-eqz p1, :cond_0

    const-string v0, "PRINTER_KEY"

    .line 112
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/print/screen/ActivityIpPrinterSetting;->oldItemKey:Ljava/lang/String;

    .line 113
    iget-object p1, p0, Lepson/print/screen/ActivityIpPrinterSetting;->oldItemKey:Ljava/lang/String;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    if-lez p1, :cond_0

    .line 115
    iget-object p1, p0, Lepson/print/screen/ActivityIpPrinterSetting;->manager:Lepson/print/EPPrinterManager;

    iget-object v0, p0, Lepson/print/screen/ActivityIpPrinterSetting;->oldItemKey:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lepson/print/EPPrinterManager;->loadIpPrinterInfo(Ljava/lang/String;)Lepson/print/EPPrinterInfo;

    move-result-object p1

    iput-object p1, p0, Lepson/print/screen/ActivityIpPrinterSetting;->printerInfo:Lepson/print/EPPrinterInfo;

    .line 120
    :cond_0
    iget-object p1, p0, Lepson/print/screen/ActivityIpPrinterSetting;->printerInfo:Lepson/print/EPPrinterInfo;

    if-nez p1, :cond_1

    .line 125
    iget-object p1, p0, Lepson/print/screen/ActivityIpPrinterSetting;->printerNameArea:Landroid/view/ViewGroup;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0

    .line 129
    :cond_1
    iget-object v0, p0, Lepson/print/screen/ActivityIpPrinterSetting;->printerName:Landroid/widget/TextView;

    iget-object p1, p1, Lepson/print/EPPrinterInfo;->userDefName:Ljava/lang/String;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 130
    iget-object p1, p0, Lepson/print/screen/ActivityIpPrinterSetting;->printerIpAdress:Landroid/widget/TextView;

    iget-object v0, p0, Lepson/print/screen/ActivityIpPrinterSetting;->printerInfo:Lepson/print/EPPrinterInfo;

    iget-object v0, v0, Lepson/print/EPPrinterInfo;->printerIP:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const p1, 0x7f0e04ca

    .line 133
    invoke-virtual {p0, p1}, Lepson/print/screen/ActivityIpPrinterSetting;->setTitle(I)V

    .line 137
    :goto_0
    iget-object p1, p0, Lepson/print/screen/ActivityIpPrinterSetting;->clearPrinterNameButton:Landroid/widget/Button;

    new-instance v0, Lepson/print/screen/ActivityIpPrinterSetting$1;

    invoke-direct {v0, p0}, Lepson/print/screen/ActivityIpPrinterSetting$1;-><init>(Lepson/print/screen/ActivityIpPrinterSetting;)V

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 145
    iget-object p1, p0, Lepson/print/screen/ActivityIpPrinterSetting;->clearPrinterIpAdressButton:Landroid/widget/Button;

    new-instance v0, Lepson/print/screen/ActivityIpPrinterSetting$2;

    invoke-direct {v0, p0}, Lepson/print/screen/ActivityIpPrinterSetting$2;-><init>(Lepson/print/screen/ActivityIpPrinterSetting;)V

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .line 428
    invoke-virtual {p0}, Lepson/print/screen/ActivityIpPrinterSetting;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0b0003

    .line 429
    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 431
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result p1

    return p1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .line 437
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f080212

    if-eq v0, v1, :cond_0

    .line 443
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1

    .line 439
    :cond_0
    invoke-virtual {p0}, Lepson/print/screen/ActivityIpPrinterSetting;->onSaveButton()V

    const/4 p1, 0x1

    return p1
.end method

.method public onSaveButton()V
    .locals 5

    .line 158
    iget-object v0, p0, Lepson/print/screen/ActivityIpPrinterSetting;->printerIpAdress:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 161
    iget-object v1, p0, Lepson/print/screen/ActivityIpPrinterSetting;->printerNameArea:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v1

    const v2, 0x7f0e04f2

    const/4 v3, 0x0

    const/16 v4, 0x8

    if-eq v1, v4, :cond_0

    .line 163
    iget-object v1, p0, Lepson/print/screen/ActivityIpPrinterSetting;->printerName:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->length()I

    move-result v1

    if-gtz v1, :cond_0

    .line 165
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 166
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0e0357

    .line 167
    invoke-virtual {p0, v1}, Lepson/print/screen/ActivityIpPrinterSetting;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 168
    invoke-virtual {p0, v2}, Lepson/print/screen/ActivityIpPrinterSetting;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lepson/print/screen/ActivityIpPrinterSetting$3;

    invoke-direct {v2, p0}, Lepson/print/screen/ActivityIpPrinterSetting$3;-><init>(Lepson/print/screen/ActivityIpPrinterSetting;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 175
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void

    .line 181
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 182
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 183
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0e04cd

    .line 184
    invoke-virtual {p0, v1}, Lepson/print/screen/ActivityIpPrinterSetting;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 185
    invoke-virtual {p0, v2}, Lepson/print/screen/ActivityIpPrinterSetting;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lepson/print/screen/ActivityIpPrinterSetting$4;

    invoke-direct {v2, p0}, Lepson/print/screen/ActivityIpPrinterSetting$4;-><init>(Lepson/print/screen/ActivityIpPrinterSetting;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 192
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void

    :cond_1
    const-string v1, "^(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})$"

    .line 197
    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 198
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 199
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0e04cc

    .line 200
    invoke-virtual {p0, v1}, Lepson/print/screen/ActivityIpPrinterSetting;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 201
    invoke-virtual {p0, v2}, Lepson/print/screen/ActivityIpPrinterSetting;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lepson/print/screen/ActivityIpPrinterSetting$5;

    invoke-direct {v2, p0}, Lepson/print/screen/ActivityIpPrinterSetting$5;-><init>(Lepson/print/screen/ActivityIpPrinterSetting;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 208
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void

    .line 212
    :cond_2
    new-instance v1, Lepson/print/screen/ActivityIpPrinterSetting$6;

    invoke-direct {v1, p0}, Lepson/print/screen/ActivityIpPrinterSetting$6;-><init>(Lepson/print/screen/ActivityIpPrinterSetting;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    aput-object v0, v2, v3

    .line 396
    invoke-virtual {v1, v2}, Lepson/print/screen/ActivityIpPrinterSetting$6;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.class public Lepson/print/screen/ActivityPrinterSetting;
.super Lepson/print/ActivityIACommon;
.source "ActivityPrinterSetting.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lepson/print/screen/StringListSelectDialog$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/screen/ActivityPrinterSetting$LocalTextWatcher;,
        Lepson/print/screen/ActivityPrinterSetting$setRemotePrintTask;
    }
.end annotation


# static fields
.field private static final DIALOG_TAG_DOMAIN_SELECT:Ljava/lang/String; = "domain-select-dialog"

.field public static final KEY_MYPRINTER:Ljava/lang/String; = "my_printer"

.field private static final MAIL_DOMAIN_CHINA:Ljava/lang/String; = "print.rpt.epson.com.cn"

.field private static final REQUEST_CODE_PRINTER:I


# instance fields
.field private final ACCESS_KEY_MAX_SIZE:I

.field private final ACCESS_KEY_MIN_SIZE:I

.field private final CLEAR_ACCESS_KEY:I

.field private final CLEAR_EMAIL_ADDRESS:I

.field private final CLEAR_PRINTER_NAME:I

.field private final EMAIL_ADDRESS_MAX_SIZE:I

.field private final ERROR_GET_INFO_FROM_PRINTER:I

.field private final ERROR_PRINTER_ALREADY_REGISTERED:I

.field private final ERROR_PRINTER_EMAIL_ADDRESS_EMPTY:I

.field private final ERROR_PRINTER_NAME_EMPTY:I

.field private final ERROR_PRINTER_NAME_OVER_MAX_LENGTH:I

.field private final FINISH:I

.field private final MAIL_DOMAIN_NAME:Ljava/lang/String;

.field private final NOT_SUPPORTED_REMOTE_PRINT:I

.field private final PRINTER_NAME_MAX_SIZE:I

.field private final SAVE_DONE:I

.field private final SHOW_ERROR_MESSAGE:I

.field private final SUPPORTED_REMOTE_PRINT:I

.field private final UPDATE_ACCESSKEY_LAYOUT:I

.field private accessKeyEdit:Landroid/widget/EditText;

.field private accessKeyLayout:Landroid/widget/FrameLayout;

.field private accessKeyText:Landroid/widget/TextView;

.field private bundle:Landroid/os/Bundle;

.field private clearAccessKeyButton:Landroid/widget/Button;

.field private clearPrinterEmailAddressButton:Landroid/widget/Button;

.field private clearPrinterNameButton:Landroid/widget/Button;

.field private context:Landroid/content/Context;

.field private getprinterAdress:Landroid/widget/LinearLayout;

.field private getprinterAdressLink:Landroid/widget/LinearLayout;

.field mHandler:Landroid/os/Handler;

.field private mLocalTextWatcher:Lepson/print/screen/ActivityPrinterSetting$LocalTextWatcher;

.field mMyPrinter:Lepson/print/MyPrinter;

.field private oldPrinterEmailAddress:Ljava/lang/String;

.field private printerAccessKey:Ljava/lang/String;

.field private printerEmailAddress:Ljava/lang/String;

.field private printerEmailAddressEdit:Landroid/widget/EditText;

.field private printerEmailText:Landroid/widget/TextView;

.field private printerName:Ljava/lang/String;

.field private printerNameEdit:Landroid/widget/EditText;

.field private printerNameLayout:Landroid/widget/FrameLayout;

.field private printerNameText:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 4

    .line 48
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    const/4 v0, 0x1

    .line 60
    iput v0, p0, Lepson/print/screen/ActivityPrinterSetting;->CLEAR_PRINTER_NAME:I

    const/4 v1, 0x2

    .line 61
    iput v1, p0, Lepson/print/screen/ActivityPrinterSetting;->CLEAR_EMAIL_ADDRESS:I

    const/4 v2, 0x3

    .line 62
    iput v2, p0, Lepson/print/screen/ActivityPrinterSetting;->CLEAR_ACCESS_KEY:I

    const/4 v2, 0x4

    .line 63
    iput v2, p0, Lepson/print/screen/ActivityPrinterSetting;->UPDATE_ACCESSKEY_LAYOUT:I

    const/4 v2, 0x5

    .line 64
    iput v2, p0, Lepson/print/screen/ActivityPrinterSetting;->SAVE_DONE:I

    const/4 v2, 0x6

    .line 65
    iput v2, p0, Lepson/print/screen/ActivityPrinterSetting;->SHOW_ERROR_MESSAGE:I

    const/4 v3, 0x7

    .line 66
    iput v3, p0, Lepson/print/screen/ActivityPrinterSetting;->FINISH:I

    .line 68
    iput v0, p0, Lepson/print/screen/ActivityPrinterSetting;->SUPPORTED_REMOTE_PRINT:I

    .line 69
    iput v1, p0, Lepson/print/screen/ActivityPrinterSetting;->NOT_SUPPORTED_REMOTE_PRINT:I

    const/4 v1, -0x1

    .line 71
    iput v1, p0, Lepson/print/screen/ActivityPrinterSetting;->ERROR_GET_INFO_FROM_PRINTER:I

    const/4 v1, -0x2

    .line 72
    iput v1, p0, Lepson/print/screen/ActivityPrinterSetting;->ERROR_PRINTER_NAME_EMPTY:I

    const/4 v1, -0x3

    .line 73
    iput v1, p0, Lepson/print/screen/ActivityPrinterSetting;->ERROR_PRINTER_NAME_OVER_MAX_LENGTH:I

    const/4 v1, -0x4

    .line 74
    iput v1, p0, Lepson/print/screen/ActivityPrinterSetting;->ERROR_PRINTER_EMAIL_ADDRESS_EMPTY:I

    const/4 v1, -0x5

    .line 75
    iput v1, p0, Lepson/print/screen/ActivityPrinterSetting;->ERROR_PRINTER_ALREADY_REGISTERED:I

    const/16 v1, 0x3f

    .line 77
    iput v1, p0, Lepson/print/screen/ActivityPrinterSetting;->PRINTER_NAME_MAX_SIZE:I

    const/16 v1, 0xb4

    .line 78
    iput v1, p0, Lepson/print/screen/ActivityPrinterSetting;->EMAIL_ADDRESS_MAX_SIZE:I

    .line 79
    iput v2, p0, Lepson/print/screen/ActivityPrinterSetting;->ACCESS_KEY_MIN_SIZE:I

    const/16 v1, 0x20

    .line 80
    iput v1, p0, Lepson/print/screen/ActivityPrinterSetting;->ACCESS_KEY_MAX_SIZE:I

    const/4 v1, 0x0

    .line 82
    iput-object v1, p0, Lepson/print/screen/ActivityPrinterSetting;->bundle:Landroid/os/Bundle;

    const-string v2, ""

    .line 83
    iput-object v2, p0, Lepson/print/screen/ActivityPrinterSetting;->printerName:Ljava/lang/String;

    const-string v2, ""

    .line 84
    iput-object v2, p0, Lepson/print/screen/ActivityPrinterSetting;->printerEmailAddress:Ljava/lang/String;

    const-string v2, ""

    .line 85
    iput-object v2, p0, Lepson/print/screen/ActivityPrinterSetting;->printerAccessKey:Ljava/lang/String;

    const-string v2, ""

    .line 86
    iput-object v2, p0, Lepson/print/screen/ActivityPrinterSetting;->oldPrinterEmailAddress:Ljava/lang/String;

    .line 88
    iput-object v1, p0, Lepson/print/screen/ActivityPrinterSetting;->printerNameText:Landroid/widget/TextView;

    .line 89
    iput-object v1, p0, Lepson/print/screen/ActivityPrinterSetting;->printerNameEdit:Landroid/widget/EditText;

    .line 90
    iput-object v1, p0, Lepson/print/screen/ActivityPrinterSetting;->clearPrinterNameButton:Landroid/widget/Button;

    .line 91
    iput-object v1, p0, Lepson/print/screen/ActivityPrinterSetting;->printerNameLayout:Landroid/widget/FrameLayout;

    .line 92
    iput-object v1, p0, Lepson/print/screen/ActivityPrinterSetting;->printerEmailText:Landroid/widget/TextView;

    .line 93
    iput-object v1, p0, Lepson/print/screen/ActivityPrinterSetting;->printerEmailAddressEdit:Landroid/widget/EditText;

    .line 94
    iput-object v1, p0, Lepson/print/screen/ActivityPrinterSetting;->clearPrinterEmailAddressButton:Landroid/widget/Button;

    .line 95
    iput-object v1, p0, Lepson/print/screen/ActivityPrinterSetting;->accessKeyText:Landroid/widget/TextView;

    .line 96
    iput-object v1, p0, Lepson/print/screen/ActivityPrinterSetting;->accessKeyLayout:Landroid/widget/FrameLayout;

    .line 97
    iput-object v1, p0, Lepson/print/screen/ActivityPrinterSetting;->accessKeyEdit:Landroid/widget/EditText;

    .line 98
    iput-object v1, p0, Lepson/print/screen/ActivityPrinterSetting;->clearAccessKeyButton:Landroid/widget/Button;

    .line 99
    iput-object v1, p0, Lepson/print/screen/ActivityPrinterSetting;->getprinterAdress:Landroid/widget/LinearLayout;

    .line 100
    iput-object v1, p0, Lepson/print/screen/ActivityPrinterSetting;->getprinterAdressLink:Landroid/widget/LinearLayout;

    .line 102
    iput-object v1, p0, Lepson/print/screen/ActivityPrinterSetting;->context:Landroid/content/Context;

    .line 120
    invoke-static {}, Lepson/print/IprintApplication;->isConnectStaging()Z

    move-result v1

    if-ne v1, v0, :cond_0

    const-string v0, "stg-print.epsonconnect.com"

    .line 121
    iput-object v0, p0, Lepson/print/screen/ActivityPrinterSetting;->MAIL_DOMAIN_NAME:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const-string v0, "print.epsonconnect.com"

    .line 123
    iput-object v0, p0, Lepson/print/screen/ActivityPrinterSetting;->MAIL_DOMAIN_NAME:Ljava/lang/String;

    .line 420
    :goto_0
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lepson/print/screen/ActivityPrinterSetting$1;

    invoke-direct {v1, p0}, Lepson/print/screen/ActivityPrinterSetting$1;-><init>(Lepson/print/screen/ActivityPrinterSetting;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lepson/print/screen/ActivityPrinterSetting;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$100(Lepson/print/screen/ActivityPrinterSetting;)Landroid/widget/EditText;
    .locals 0

    .line 48
    iget-object p0, p0, Lepson/print/screen/ActivityPrinterSetting;->printerNameEdit:Landroid/widget/EditText;

    return-object p0
.end method

.method static synthetic access$1000(Lepson/print/screen/ActivityPrinterSetting;)Ljava/lang/String;
    .locals 0

    .line 48
    iget-object p0, p0, Lepson/print/screen/ActivityPrinterSetting;->oldPrinterEmailAddress:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1100(Lepson/print/screen/ActivityPrinterSetting;)Ljava/lang/String;
    .locals 0

    .line 48
    invoke-direct {p0}, Lepson/print/screen/ActivityPrinterSetting;->getDefaultMailDomain()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$1200(Lepson/print/screen/ActivityPrinterSetting;)V
    .locals 0

    .line 48
    invoke-direct {p0}, Lepson/print/screen/ActivityPrinterSetting;->showMailDomainSelectDialog()V

    return-void
.end method

.method static synthetic access$200(Lepson/print/screen/ActivityPrinterSetting;)Landroid/widget/EditText;
    .locals 0

    .line 48
    iget-object p0, p0, Lepson/print/screen/ActivityPrinterSetting;->printerEmailAddressEdit:Landroid/widget/EditText;

    return-object p0
.end method

.method static synthetic access$300(Lepson/print/screen/ActivityPrinterSetting;)Landroid/widget/EditText;
    .locals 0

    .line 48
    iget-object p0, p0, Lepson/print/screen/ActivityPrinterSetting;->accessKeyEdit:Landroid/widget/EditText;

    return-object p0
.end method

.method static synthetic access$400(Lepson/print/screen/ActivityPrinterSetting;)Landroid/widget/TextView;
    .locals 0

    .line 48
    iget-object p0, p0, Lepson/print/screen/ActivityPrinterSetting;->accessKeyText:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$500(Lepson/print/screen/ActivityPrinterSetting;)Landroid/widget/FrameLayout;
    .locals 0

    .line 48
    iget-object p0, p0, Lepson/print/screen/ActivityPrinterSetting;->accessKeyLayout:Landroid/widget/FrameLayout;

    return-object p0
.end method

.method static synthetic access$700(Lepson/print/screen/ActivityPrinterSetting;)Landroid/content/Context;
    .locals 0

    .line 48
    iget-object p0, p0, Lepson/print/screen/ActivityPrinterSetting;->context:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic access$800(Lepson/print/screen/ActivityPrinterSetting;)Ljava/lang/String;
    .locals 0

    .line 48
    iget-object p0, p0, Lepson/print/screen/ActivityPrinterSetting;->printerEmailAddress:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$802(Lepson/print/screen/ActivityPrinterSetting;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 48
    iput-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->printerEmailAddress:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$900(Lepson/print/screen/ActivityPrinterSetting;)Ljava/lang/String;
    .locals 0

    .line 48
    iget-object p0, p0, Lepson/print/screen/ActivityPrinterSetting;->printerAccessKey:Ljava/lang/String;

    return-object p0
.end method

.method static deleteFirstAtMark(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    const-string v0, "^@?"

    .line 702
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 703
    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object p0

    const-string v0, ""

    .line 704
    invoke-virtual {p0, v0}, Ljava/util/regex/Matcher;->replaceFirst(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private getDefaultMailDomain()Ljava/lang/String;
    .locals 1

    .line 662
    invoke-direct {p0}, Lepson/print/screen/ActivityPrinterSetting;->guessIfMainlandChina()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "print.rpt.epson.com.cn"

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lepson/print/screen/ActivityPrinterSetting;->MAIL_DOMAIN_NAME:Ljava/lang/String;

    :goto_0
    return-object v0
.end method

.method private guessIfMainlandChina()Z
    .locals 2

    .line 671
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    .line 672
    invoke-virtual {v0}, Ljava/util/Locale;->getScript()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Hans"

    .line 673
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method static replaceMailDomain(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    const-string v0, "@(.*)$"

    .line 691
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 692
    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 693
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-nez v1, :cond_0

    .line 694
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "@"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 697
    :cond_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceFirst(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "@"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private replaceMailDomainEditText(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 677
    iget-object v0, p0, Lepson/print/screen/ActivityPrinterSetting;->printerEmailAddressEdit:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 678
    invoke-static {p1, v0}, Lepson/print/screen/ActivityPrinterSetting;->replaceMailDomain(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 679
    iget-object v0, p0, Lepson/print/screen/ActivityPrinterSetting;->printerEmailAddressEdit:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 681
    iput-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->printerEmailAddress:Ljava/lang/String;

    return-void
.end method

.method private showMailDomainSelectDialog()V
    .locals 3

    const/4 v0, 0x2

    .line 240
    new-array v0, v0, [Ljava/lang/String;

    iget-object v1, p0, Lepson/print/screen/ActivityPrinterSetting;->MAIL_DOMAIN_NAME:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "print.rpt.epson.com.cn"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Lepson/print/screen/StringListSelectDialog;->newInstance([Ljava/lang/String;)Lepson/print/screen/StringListSelectDialog;

    move-result-object v0

    .line 241
    invoke-virtual {p0}, Lepson/print/screen/ActivityPrinterSetting;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "domain-select-dialog"

    invoke-virtual {v0, v1, v2}, Lepson/print/screen/StringListSelectDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public deletePrinterInfo(Ljava/lang/String;)Z
    .locals 1

    .line 643
    new-instance v0, Lepson/print/EPPrinterManager;

    invoke-direct {v0, p0}, Lepson/print/EPPrinterManager;-><init>(Landroid/content/Context;)V

    .line 646
    invoke-virtual {v0, p1}, Lepson/print/EPPrinterManager;->deleteRemotePrinterInfo(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method isEnableRegistPrinter(Ljava/lang/String;)Z
    .locals 1

    .line 587
    new-instance v0, Lepson/print/EPPrinterManager;

    invoke-direct {v0, p0}, Lepson/print/EPPrinterManager;-><init>(Landroid/content/Context;)V

    .line 589
    invoke-virtual {v0, p1}, Lepson/print/EPPrinterManager;->loadRemotePrinterInfo(Ljava/lang/String;)Lepson/print/EPPrinterInfo;

    move-result-object p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public onBackPressed()V
    .locals 1

    const/16 v0, 0x12

    .line 476
    invoke-virtual {p0, v0}, Lepson/print/screen/ActivityPrinterSetting;->setResult(I)V

    .line 477
    invoke-virtual {p0}, Lepson/print/screen/ActivityPrinterSetting;->finish()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .line 245
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    const v0, 0x7f0800c9

    if-ne p1, v0, :cond_0

    .line 248
    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->mHandler:Landroid/os/Handler;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    :cond_0
    const v0, 0x7f0800c7

    if-ne p1, v0, :cond_1

    .line 251
    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->mHandler:Landroid/os/Handler;

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    :cond_1
    const v0, 0x7f0800c1

    if-ne p1, v0, :cond_2

    .line 254
    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->mHandler:Landroid/os/Handler;

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    :cond_2
    const v0, 0x7f0802cc

    if-ne p1, v0, :cond_b

    .line 258
    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->printerNameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {p1}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result p1

    const/4 v0, 0x6

    if-nez p1, :cond_4

    .line 259
    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->printerNameEdit:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->printerName:Ljava/lang/String;

    .line 260
    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->printerName:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    if-gtz p1, :cond_3

    .line 262
    new-instance p1, Landroid/os/Message;

    invoke-direct {p1}, Landroid/os/Message;-><init>()V

    .line 263
    iput v0, p1, Landroid/os/Message;->what:I

    const/4 v0, -0x2

    .line 264
    iput v0, p1, Landroid/os/Message;->arg1:I

    .line 265
    iget-object v0, p0, Lepson/print/screen/ActivityPrinterSetting;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    .line 267
    :cond_3
    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->printerName:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    const/16 v1, 0x3f

    if-le p1, v1, :cond_4

    .line 269
    new-instance p1, Landroid/os/Message;

    invoke-direct {p1}, Landroid/os/Message;-><init>()V

    .line 270
    iput v0, p1, Landroid/os/Message;->what:I

    const/4 v0, -0x3

    .line 271
    iput v0, p1, Landroid/os/Message;->arg1:I

    .line 272
    iget-object v0, p0, Lepson/print/screen/ActivityPrinterSetting;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    .line 278
    :cond_4
    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->printerEmailAddressEdit:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->printerEmailAddress:Ljava/lang/String;

    .line 279
    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->printerEmailAddress:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    if-gtz p1, :cond_5

    .line 281
    new-instance p1, Landroid/os/Message;

    invoke-direct {p1}, Landroid/os/Message;-><init>()V

    .line 282
    iput v0, p1, Landroid/os/Message;->what:I

    const/4 v0, -0x4

    .line 283
    iput v0, p1, Landroid/os/Message;->arg1:I

    .line 284
    iget-object v0, p0, Lepson/print/screen/ActivityPrinterSetting;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    .line 286
    :cond_5
    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->printerEmailAddress:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    const/16 v1, 0xb4

    const/16 v2, -0x4b3

    if-le p1, v1, :cond_6

    .line 294
    new-instance p1, Landroid/os/Message;

    invoke-direct {p1}, Landroid/os/Message;-><init>()V

    .line 295
    iput v0, p1, Landroid/os/Message;->what:I

    .line 296
    iput v2, p1, Landroid/os/Message;->arg1:I

    .line 297
    iget-object v0, p0, Lepson/print/screen/ActivityPrinterSetting;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    .line 301
    :cond_6
    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->oldPrinterEmailAddress:Ljava/lang/String;

    iget-object v1, p0, Lepson/print/screen/ActivityPrinterSetting;->printerEmailAddress:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_7

    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->printerEmailAddress:Ljava/lang/String;

    .line 302
    invoke-virtual {p0, p1}, Lepson/print/screen/ActivityPrinterSetting;->isEnableRegistPrinter(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_7

    .line 304
    new-instance p1, Landroid/os/Message;

    invoke-direct {p1}, Landroid/os/Message;-><init>()V

    .line 305
    iput v0, p1, Landroid/os/Message;->what:I

    const/4 v0, -0x5

    .line 306
    iput v0, p1, Landroid/os/Message;->arg1:I

    .line 307
    iget-object v0, p0, Lepson/print/screen/ActivityPrinterSetting;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    .line 312
    :cond_7
    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->accessKeyEdit:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->printerAccessKey:Ljava/lang/String;

    .line 313
    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->accessKeyLayout:Landroid/widget/FrameLayout;

    invoke-virtual {p1}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result p1

    if-nez p1, :cond_8

    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->printerAccessKey:Ljava/lang/String;

    .line 314
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    if-lt p1, v0, :cond_9

    :cond_8
    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->printerAccessKey:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    const/16 v1, 0x20

    if-le p1, v1, :cond_a

    .line 319
    :cond_9
    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->printerAccessKey:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    if-eqz p1, :cond_a

    .line 320
    new-instance p1, Landroid/os/Message;

    invoke-direct {p1}, Landroid/os/Message;-><init>()V

    .line 321
    iput v0, p1, Landroid/os/Message;->what:I

    .line 322
    iput v2, p1, Landroid/os/Message;->arg1:I

    .line 323
    iget-object v0, p0, Lepson/print/screen/ActivityPrinterSetting;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    .line 329
    :cond_a
    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->mHandler:Landroid/os/Handler;

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :cond_b
    const v0, 0x7f0802b2

    if-ne p1, v0, :cond_c

    .line 332
    new-instance p1, Landroid/content/Intent;

    const-class v0, Lepson/print/screen/PrinterFinder;

    invoke-direct {p1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 333
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "PRINTER_NAME"

    const-string v2, ""

    .line 334
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    invoke-virtual {p1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    const/4 v0, 0x0

    .line 336
    invoke-virtual {p0, p1, v0}, Lepson/print/screen/ActivityPrinterSetting;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_c
    :goto_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .line 130
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    .line 131
    invoke-virtual {p0}, Lepson/print/screen/ActivityPrinterSetting;->getWindow()Landroid/view/Window;

    move-result-object p1

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Landroid/view/Window;->setSoftInputMode(I)V

    const p1, 0x7f0a005f

    .line 132
    invoke-virtual {p0, p1}, Lepson/print/screen/ActivityPrinterSetting;->setContentView(I)V

    .line 133
    iput-object p0, p0, Lepson/print/screen/ActivityPrinterSetting;->context:Landroid/content/Context;

    const p1, 0x7f0e035a

    const/4 v0, 0x1

    .line 136
    invoke-virtual {p0, p1, v0}, Lepson/print/screen/ActivityPrinterSetting;->setActionBar(IZ)V

    const p1, 0x7f080287

    .line 138
    invoke-virtual {p0, p1}, Lepson/print/screen/ActivityPrinterSetting;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->printerNameText:Landroid/widget/TextView;

    const p1, 0x7f080285

    .line 139
    invoke-virtual {p0, p1}, Lepson/print/screen/ActivityPrinterSetting;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/EditText;

    iput-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->printerNameEdit:Landroid/widget/EditText;

    const p1, 0x7f0800c9

    .line 140
    invoke-virtual {p0, p1}, Lepson/print/screen/ActivityPrinterSetting;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->clearPrinterNameButton:Landroid/widget/Button;

    const p1, 0x7f080286

    .line 141
    invoke-virtual {p0, p1}, Lepson/print/screen/ActivityPrinterSetting;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/FrameLayout;

    iput-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->printerNameLayout:Landroid/widget/FrameLayout;

    const p1, 0x7f080281

    .line 143
    invoke-virtual {p0, p1}, Lepson/print/screen/ActivityPrinterSetting;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->printerEmailText:Landroid/widget/TextView;

    const p1, 0x7f080280

    .line 144
    invoke-virtual {p0, p1}, Lepson/print/screen/ActivityPrinterSetting;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/EditText;

    iput-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->printerEmailAddressEdit:Landroid/widget/EditText;

    const p1, 0x7f0800c7

    .line 145
    invoke-virtual {p0, p1}, Lepson/print/screen/ActivityPrinterSetting;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->clearPrinterEmailAddressButton:Landroid/widget/Button;

    const p1, 0x7f080038

    .line 147
    invoke-virtual {p0, p1}, Lepson/print/screen/ActivityPrinterSetting;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->accessKeyText:Landroid/widget/TextView;

    const p1, 0x7f080037

    .line 148
    invoke-virtual {p0, p1}, Lepson/print/screen/ActivityPrinterSetting;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/FrameLayout;

    iput-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->accessKeyLayout:Landroid/widget/FrameLayout;

    const p1, 0x7f080036

    .line 149
    invoke-virtual {p0, p1}, Lepson/print/screen/ActivityPrinterSetting;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/EditText;

    iput-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->accessKeyEdit:Landroid/widget/EditText;

    const p1, 0x7f0800c1

    .line 150
    invoke-virtual {p0, p1}, Lepson/print/screen/ActivityPrinterSetting;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->clearAccessKeyButton:Landroid/widget/Button;

    const p1, 0x7f080161

    .line 152
    invoke-virtual {p0, p1}, Lepson/print/screen/ActivityPrinterSetting;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->getprinterAdress:Landroid/widget/LinearLayout;

    const p1, 0x7f0802b2

    .line 153
    invoke-virtual {p0, p1}, Lepson/print/screen/ActivityPrinterSetting;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->getprinterAdressLink:Landroid/widget/LinearLayout;

    .line 156
    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->clearPrinterNameButton:Landroid/widget/Button;

    invoke-virtual {p1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 157
    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->clearPrinterEmailAddressButton:Landroid/widget/Button;

    invoke-virtual {p1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 158
    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->clearAccessKeyButton:Landroid/widget/Button;

    invoke-virtual {p1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 159
    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->getprinterAdressLink:Landroid/widget/LinearLayout;

    invoke-virtual {p1, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 162
    new-instance p1, Lepson/print/screen/ActivityPrinterSetting$LocalTextWatcher;

    const/4 v0, 0x0

    invoke-direct {p1, p0, v0}, Lepson/print/screen/ActivityPrinterSetting$LocalTextWatcher;-><init>(Lepson/print/screen/ActivityPrinterSetting;Lepson/print/screen/ActivityPrinterSetting$1;)V

    iput-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->mLocalTextWatcher:Lepson/print/screen/ActivityPrinterSetting$LocalTextWatcher;

    .line 163
    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->printerEmailAddressEdit:Landroid/widget/EditText;

    iget-object v0, p0, Lepson/print/screen/ActivityPrinterSetting;->mLocalTextWatcher:Lepson/print/screen/ActivityPrinterSetting$LocalTextWatcher;

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 168
    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->printerNameText:Landroid/widget/TextView;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 169
    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->printerNameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {p1, v0}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 170
    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->accessKeyText:Landroid/widget/TextView;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 171
    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->accessKeyLayout:Landroid/widget/FrameLayout;

    invoke-virtual {p1, v0}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 175
    invoke-virtual {p0}, Lepson/print/screen/ActivityPrinterSetting;->getIntent()Landroid/content/Intent;

    move-result-object p1

    if-eqz p1, :cond_5

    .line 177
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    iput-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->bundle:Landroid/os/Bundle;

    .line 179
    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->bundle:Landroid/os/Bundle;

    if-eqz p1, :cond_5

    const-string v1, "PRINTER_EMAIL_ADDRESS"

    .line 180
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->printerEmailAddress:Ljava/lang/String;

    .line 184
    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->printerEmailAddress:Ljava/lang/String;

    if-eqz p1, :cond_0

    const-string v1, ""

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    :cond_0
    const-string p1, ""

    .line 185
    iput-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->printerEmailAddress:Ljava/lang/String;

    .line 187
    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->printerEmailText:Landroid/widget/TextView;

    const v1, 0x7f0e0360

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(I)V

    .line 189
    :cond_1
    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->printerEmailAddressEdit:Landroid/widget/EditText;

    iget-object v1, p0, Lepson/print/screen/ActivityPrinterSetting;->printerEmailAddress:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 193
    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->printerEmailAddress:Ljava/lang/String;

    iput-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->oldPrinterEmailAddress:Ljava/lang/String;

    const-string v1, ""

    .line 196
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_5

    .line 198
    new-instance p1, Lepson/print/EPPrinterManager;

    invoke-direct {p1, p0}, Lepson/print/EPPrinterManager;-><init>(Landroid/content/Context;)V

    .line 200
    iget-object v1, p0, Lepson/print/screen/ActivityPrinterSetting;->printerEmailAddress:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lepson/print/EPPrinterManager;->loadRemotePrinterInfo(Ljava/lang/String;)Lepson/print/EPPrinterInfo;

    move-result-object p1

    if-eqz p1, :cond_4

    .line 203
    iget-object v1, p1, Lepson/print/EPPrinterInfo;->userDefName:Ljava/lang/String;

    iput-object v1, p0, Lepson/print/screen/ActivityPrinterSetting;->printerName:Ljava/lang/String;

    .line 204
    iget-object v1, p0, Lepson/print/screen/ActivityPrinterSetting;->printerName:Ljava/lang/String;

    if-nez v1, :cond_2

    .line 208
    iget-object v1, p1, Lepson/print/EPPrinterInfo;->printerName:Ljava/lang/String;

    iput-object v1, p0, Lepson/print/screen/ActivityPrinterSetting;->printerName:Ljava/lang/String;

    .line 210
    :cond_2
    iget-object v1, p0, Lepson/print/screen/ActivityPrinterSetting;->printerNameEdit:Landroid/widget/EditText;

    iget-object v2, p0, Lepson/print/screen/ActivityPrinterSetting;->printerName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 211
    iget-object v1, p0, Lepson/print/screen/ActivityPrinterSetting;->printerNameText:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 212
    iget-object v1, p0, Lepson/print/screen/ActivityPrinterSetting;->printerNameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 215
    iget-object p1, p1, Lepson/print/EPPrinterInfo;->printerAccessKey:Ljava/lang/String;

    iput-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->printerAccessKey:Ljava/lang/String;

    .line 216
    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->printerAccessKey:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_3

    .line 217
    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->accessKeyEdit:Landroid/widget/EditText;

    iget-object v1, p0, Lepson/print/screen/ActivityPrinterSetting;->printerAccessKey:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 218
    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->accessKeyText:Landroid/widget/TextView;

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 219
    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->accessKeyLayout:Landroid/widget/FrameLayout;

    invoke-virtual {p1, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 223
    :cond_3
    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->getprinterAdress:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 227
    :cond_4
    iget-object p1, p0, Lepson/print/screen/ActivityPrinterSetting;->printerEmailText:Landroid/widget/TextView;

    const v0, 0x7f0e0350

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    const p1, 0x7f0e0342

    .line 229
    invoke-virtual {p0, p1}, Lepson/print/screen/ActivityPrinterSetting;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lepson/print/screen/ActivityPrinterSetting;->setTitle(Ljava/lang/CharSequence;)V

    :cond_5
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .line 789
    invoke-virtual {p0}, Lepson/print/screen/ActivityPrinterSetting;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0b0003

    .line 790
    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 792
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result p1

    return p1
.end method

.method onDone()V
    .locals 4

    .line 346
    iget-object v0, p0, Lepson/print/screen/ActivityPrinterSetting;->printerNameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v0

    const/4 v1, 0x6

    if-nez v0, :cond_1

    .line 347
    iget-object v0, p0, Lepson/print/screen/ActivityPrinterSetting;->printerNameEdit:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepson/print/screen/ActivityPrinterSetting;->printerName:Ljava/lang/String;

    .line 348
    iget-object v0, p0, Lepson/print/screen/ActivityPrinterSetting;->printerName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_0

    .line 350
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 351
    iput v1, v0, Landroid/os/Message;->what:I

    const/4 v1, -0x2

    .line 352
    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 353
    iget-object v1, p0, Lepson/print/screen/ActivityPrinterSetting;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    .line 355
    :cond_0
    iget-object v0, p0, Lepson/print/screen/ActivityPrinterSetting;->printerName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v2, 0x3f

    if-le v0, v2, :cond_1

    .line 357
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 358
    iput v1, v0, Landroid/os/Message;->what:I

    const/4 v1, -0x3

    .line 359
    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 360
    iget-object v1, p0, Lepson/print/screen/ActivityPrinterSetting;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    .line 366
    :cond_1
    iget-object v0, p0, Lepson/print/screen/ActivityPrinterSetting;->printerEmailAddressEdit:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepson/print/screen/ActivityPrinterSetting;->printerEmailAddress:Ljava/lang/String;

    .line 367
    iget-object v0, p0, Lepson/print/screen/ActivityPrinterSetting;->printerEmailAddress:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_2

    .line 369
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 370
    iput v1, v0, Landroid/os/Message;->what:I

    const/4 v1, -0x4

    .line 371
    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 372
    iget-object v1, p0, Lepson/print/screen/ActivityPrinterSetting;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    .line 374
    :cond_2
    iget-object v0, p0, Lepson/print/screen/ActivityPrinterSetting;->printerEmailAddress:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v2, 0xb4

    const/16 v3, -0x4b3

    if-le v0, v2, :cond_3

    .line 382
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 383
    iput v1, v0, Landroid/os/Message;->what:I

    .line 384
    iput v3, v0, Landroid/os/Message;->arg1:I

    .line 385
    iget-object v1, p0, Lepson/print/screen/ActivityPrinterSetting;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    .line 389
    :cond_3
    iget-object v0, p0, Lepson/print/screen/ActivityPrinterSetting;->oldPrinterEmailAddress:Ljava/lang/String;

    iget-object v2, p0, Lepson/print/screen/ActivityPrinterSetting;->printerEmailAddress:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lepson/print/screen/ActivityPrinterSetting;->printerEmailAddress:Ljava/lang/String;

    .line 390
    invoke-virtual {p0, v0}, Lepson/print/screen/ActivityPrinterSetting;->isEnableRegistPrinter(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 392
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 393
    iput v1, v0, Landroid/os/Message;->what:I

    const/4 v1, -0x5

    .line 394
    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 395
    iget-object v1, p0, Lepson/print/screen/ActivityPrinterSetting;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    .line 400
    :cond_4
    iget-object v0, p0, Lepson/print/screen/ActivityPrinterSetting;->accessKeyEdit:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepson/print/screen/ActivityPrinterSetting;->printerAccessKey:Ljava/lang/String;

    .line 401
    iget-object v0, p0, Lepson/print/screen/ActivityPrinterSetting;->accessKeyLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lepson/print/screen/ActivityPrinterSetting;->printerAccessKey:Ljava/lang/String;

    .line 402
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lt v0, v1, :cond_6

    :cond_5
    iget-object v0, p0, Lepson/print/screen/ActivityPrinterSetting;->printerAccessKey:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v2, 0x20

    if-le v0, v2, :cond_7

    .line 407
    :cond_6
    iget-object v0, p0, Lepson/print/screen/ActivityPrinterSetting;->printerAccessKey:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_7

    .line 408
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 409
    iput v1, v0, Landroid/os/Message;->what:I

    .line 410
    iput v3, v0, Landroid/os/Message;->arg1:I

    .line 411
    iget-object v1, p0, Lepson/print/screen/ActivityPrinterSetting;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    .line 417
    :cond_7
    iget-object v0, p0, Lepson/print/screen/ActivityPrinterSetting;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public onItemSelected(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 657
    iget-object v0, p0, Lepson/print/screen/ActivityPrinterSetting;->mLocalTextWatcher:Lepson/print/screen/ActivityPrinterSetting$LocalTextWatcher;

    invoke-virtual {v0}, Lepson/print/screen/ActivityPrinterSetting$LocalTextWatcher;->skipNextInputConversion()V

    .line 658
    invoke-static {p1}, Lepson/print/screen/ActivityPrinterSetting;->deleteFirstAtMark(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lepson/print/screen/ActivityPrinterSetting;->replaceMailDomainEditText(Ljava/lang/String;)V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .line 798
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f080212

    if-eq v0, v1, :cond_0

    .line 804
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1

    .line 800
    :cond_0
    invoke-virtual {p0}, Lepson/print/screen/ActivityPrinterSetting;->onDone()V

    const/4 p1, 0x1

    return p1
.end method

.method protected onResume()V
    .locals 2

    .line 782
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onResume()V

    const-string v0, "ActiviyPrinterSettings"

    const-string v1, "onResume()"

    .line 783
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public savePrinterInfo(Lepson/print/ecclient/EpsonConnectAccess$RemotePrinterInfo;)V
    .locals 9

    .line 605
    new-instance v0, Lepson/print/EPPrinterManager;

    invoke-direct {v0, p0}, Lepson/print/EPPrinterManager;-><init>(Landroid/content/Context;)V

    .line 607
    iget-object v1, p0, Lepson/print/screen/ActivityPrinterSetting;->printerEmailAddress:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lepson/print/EPPrinterManager;->loadRemotePrinterInfo(Ljava/lang/String;)Lepson/print/EPPrinterInfo;

    move-result-object v1

    if-nez v1, :cond_0

    .line 609
    new-instance v1, Lepson/print/EPPrinterInfo;

    invoke-direct {v1}, Lepson/print/EPPrinterInfo;-><init>()V

    .line 612
    :cond_0
    iget-object v2, p0, Lepson/print/screen/ActivityPrinterSetting;->printerName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-gtz v2, :cond_1

    .line 613
    iget-object v2, p1, Lepson/print/ecclient/EpsonConnectAccess$RemotePrinterInfo;->mPrinterName:Ljava/lang/String;

    iput-object v2, p0, Lepson/print/screen/ActivityPrinterSetting;->printerName:Ljava/lang/String;

    .line 614
    iget-object v2, p0, Lepson/print/screen/ActivityPrinterSetting;->printerName:Ljava/lang/String;

    if-nez v2, :cond_1

    const-string v2, ""

    .line 615
    iput-object v2, p0, Lepson/print/screen/ActivityPrinterSetting;->printerName:Ljava/lang/String;

    .line 618
    :cond_1
    iget-object v2, p1, Lepson/print/ecclient/EpsonConnectAccess$RemotePrinterInfo;->mDeviceId:Ljava/lang/String;

    iput-object v2, v1, Lepson/print/EPPrinterInfo;->printerName:Ljava/lang/String;

    const-string v2, ""

    .line 619
    iput-object v2, v1, Lepson/print/EPPrinterInfo;->printerID:Ljava/lang/String;

    const-string v2, ""

    .line 620
    iput-object v2, v1, Lepson/print/EPPrinterInfo;->printerIP:Ljava/lang/String;

    .line 621
    iget-object v2, p1, Lepson/print/ecclient/EpsonConnectAccess$RemotePrinterInfo;->mSerialNumber:Ljava/lang/String;

    iput-object v2, v1, Lepson/print/EPPrinterInfo;->printerSerialNo:Ljava/lang/String;

    const/4 v2, 0x2

    .line 622
    iput v2, v1, Lepson/print/EPPrinterInfo;->printerLocation:I

    .line 623
    iget-object v2, p0, Lepson/print/screen/ActivityPrinterSetting;->printerEmailAddress:Ljava/lang/String;

    iput-object v2, v1, Lepson/print/EPPrinterInfo;->printerEmailAddress:Ljava/lang/String;

    .line 624
    iget-object v2, p0, Lepson/print/screen/ActivityPrinterSetting;->printerAccessKey:Ljava/lang/String;

    iput-object v2, v1, Lepson/print/EPPrinterInfo;->printerAccessKey:Ljava/lang/String;

    .line 626
    iget-object v2, p0, Lepson/print/screen/ActivityPrinterSetting;->printerName:Ljava/lang/String;

    iput-object v2, v1, Lepson/print/EPPrinterInfo;->userDefName:Ljava/lang/String;

    .line 628
    invoke-virtual {v0, v1}, Lepson/print/EPPrinterManager;->saveRemotePrinterInfo(Lepson/print/EPPrinterInfo;)V

    .line 631
    new-instance v0, Lepson/print/MyPrinter;

    iget-object v4, p1, Lepson/print/ecclient/EpsonConnectAccess$RemotePrinterInfo;->mDeviceId:Ljava/lang/String;

    const-string v5, ""

    const-string v6, ""

    iget-object v7, p1, Lepson/print/ecclient/EpsonConnectAccess$RemotePrinterInfo;->mSerialNumber:Ljava/lang/String;

    iget-object v8, p0, Lepson/print/screen/ActivityPrinterSetting;->printerEmailAddress:Ljava/lang/String;

    move-object v3, v0

    invoke-direct/range {v3 .. v8}, Lepson/print/MyPrinter;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lepson/print/screen/ActivityPrinterSetting;->mMyPrinter:Lepson/print/MyPrinter;

    return-void
.end method

.method public showErrorMessage(I)V
    .locals 6

    const/16 v0, -0x44c

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    packed-switch p1, :pswitch_data_0

    packed-switch p1, :pswitch_data_1

    packed-switch p1, :pswitch_data_2

    const p1, 0x7f0e0055

    .line 552
    invoke-virtual {p0, p1}, Lepson/print/screen/ActivityPrinterSetting;->getString(I)Ljava/lang/String;

    move-result-object p1

    const v0, 0x7f0e0054

    .line 553
    invoke-virtual {p0, v0}, Lepson/print/screen/ActivityPrinterSetting;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_0
    const p1, 0x7f0e034a

    .line 492
    invoke-virtual {p0, p1}, Lepson/print/screen/ActivityPrinterSetting;->getString(I)Ljava/lang/String;

    move-result-object p1

    const v0, 0x7f0e0349

    .line 493
    invoke-virtual {p0, v0}, Lepson/print/screen/ActivityPrinterSetting;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_1
    const-string p1, ""

    const v0, 0x7f0e0357

    .line 518
    invoke-virtual {p0, v0}, Lepson/print/screen/ActivityPrinterSetting;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_2
    const-string p1, ""

    const v0, 0x7f0e0358

    .line 523
    invoke-virtual {p0, v0}, Lepson/print/screen/ActivityPrinterSetting;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_3
    const-string p1, ""

    const v0, 0x7f0e0360

    .line 533
    invoke-virtual {p0, v0}, Lepson/print/screen/ActivityPrinterSetting;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_4
    const-string p1, ""

    const v0, 0x7f0e035f

    .line 538
    invoke-virtual {p0, v0}, Lepson/print/screen/ActivityPrinterSetting;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_5
    const p1, 0x7f0e0059

    .line 507
    invoke-virtual {p0, p1}, Lepson/print/screen/ActivityPrinterSetting;->getString(I)Ljava/lang/String;

    move-result-object p1

    const v0, 0x7f0e0058

    .line 508
    invoke-virtual {p0, v0}, Lepson/print/screen/ActivityPrinterSetting;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_6
    const p1, 0x7f0e005c

    .line 512
    invoke-virtual {p0, p1}, Lepson/print/screen/ActivityPrinterSetting;->getString(I)Ljava/lang/String;

    move-result-object p1

    const v0, 0x7f0e005b

    .line 513
    invoke-virtual {p0, v0}, Lepson/print/screen/ActivityPrinterSetting;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_7
    const p1, 0x7f0e0057

    .line 502
    invoke-virtual {p0, p1}, Lepson/print/screen/ActivityPrinterSetting;->getString(I)Ljava/lang/String;

    move-result-object p1

    const v0, 0x7f0e0056

    .line 503
    invoke-virtual {p0, v0}, Lepson/print/screen/ActivityPrinterSetting;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_8
    const v0, 0x7f0e0049

    .line 543
    invoke-virtual {p0, v0}, Lepson/print/screen/ActivityPrinterSetting;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 544
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f0e0047

    invoke-virtual {p0, v2}, Lepson/print/screen/ActivityPrinterSetting;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "0X"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 545
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const p1, 0x7f0e0048

    .line 546
    invoke-virtual {p0, p1}, Lepson/print/screen/ActivityPrinterSetting;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    move-object v5, v0

    move-object v0, p1

    move-object p1, v5

    goto :goto_0

    :cond_0
    const-string p1, ""

    const v0, 0x7f0e034c

    .line 488
    invoke-virtual {p0, v0}, Lepson/print/screen/ActivityPrinterSetting;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const p1, 0x7f0e0060

    .line 497
    invoke-virtual {p0, p1}, Lepson/print/screen/ActivityPrinterSetting;->getString(I)Ljava/lang/String;

    move-result-object p1

    const v0, 0x7f0e005f

    .line 498
    invoke-virtual {p0, v0}, Lepson/print/screen/ActivityPrinterSetting;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 558
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const v2, 0x7f0e04f2

    const/4 v3, 0x0

    if-lez v1, :cond_2

    .line 559
    new-instance v1, Lepson/print/widgets/CustomTitleAlertDialogBuilder;

    iget-object v4, p0, Lepson/print/screen/ActivityPrinterSetting;->context:Landroid/content/Context;

    invoke-direct {v1, v4}, Lepson/print/widgets/CustomTitleAlertDialogBuilder;-><init>(Landroid/content/Context;)V

    .line 560
    invoke-virtual {v1, v3}, Lepson/print/widgets/CustomTitleAlertDialogBuilder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 561
    invoke-virtual {v1, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 562
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 563
    invoke-virtual {p0, v2}, Lepson/print/screen/ActivityPrinterSetting;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lepson/print/screen/ActivityPrinterSetting$2;

    invoke-direct {v1, p0}, Lepson/print/screen/ActivityPrinterSetting$2;-><init>(Lepson/print/screen/ActivityPrinterSetting;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 568
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_1

    .line 570
    :cond_2
    new-instance p1, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lepson/print/screen/ActivityPrinterSetting;->context:Landroid/content/Context;

    invoke-direct {p1, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 571
    invoke-virtual {p1, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 572
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 573
    invoke-virtual {p0, v2}, Lepson/print/screen/ActivityPrinterSetting;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lepson/print/screen/ActivityPrinterSetting$3;

    invoke-direct {v1, p0}, Lepson/print/screen/ActivityPrinterSetting$3;-><init>(Lepson/print/screen/ActivityPrinterSetting;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 578
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    :goto_1
    return-void

    nop

    :pswitch_data_0
    .packed-switch -0x4b9
        :pswitch_8
        :pswitch_8
    .end packed-switch

    :pswitch_data_1
    .packed-switch -0x4b3
        :pswitch_7
        :pswitch_6
        :pswitch_5
    .end packed-switch

    :pswitch_data_2
    .packed-switch -0x5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.class Lepson/print/screen/PrintProgress$4;
.super Ljava/lang/Object;
.source "PrintProgress.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/screen/PrintProgress;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/screen/PrintProgress;


# direct methods
.method constructor <init>(Lepson/print/screen/PrintProgress;)V
    .locals 0

    .line 665
    iput-object p1, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 9

    .line 668
    iget-object v0, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    iget-boolean v0, v0, Lepson/print/screen/PrintProgress;->mCanceled:Z

    const/4 v1, 0x5

    const/4 v2, 0x1

    if-eqz v0, :cond_2

    .line 669
    iget v0, p1, Landroid/os/Message;->what:I

    if-eq v0, v1, :cond_1

    const/16 p1, 0x8

    if-eq v0, p1, :cond_0

    return v2

    .line 678
    :cond_0
    iget-object p1, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    iget-object p1, p1, Lepson/print/screen/PrintProgress;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return v2

    .line 674
    :cond_1
    iget-object v0, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    invoke-static {v0}, Lepson/print/screen/PrintProgress;->access$1100(Lepson/print/screen/PrintProgress;)Landroid/content/Context;

    move-result-object v0

    const-string v3, "printer"

    iget-object v4, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    invoke-static {v4}, Lepson/print/screen/PrintProgress;->access$000(Lepson/print/screen/PrintProgress;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v3, v4}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->disconnect(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    .line 685
    :cond_2
    iget-object v0, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    invoke-virtual {v0}, Lepson/print/screen/PrintProgress;->isFinishing()Z

    move-result v0

    const/4 v3, 0x4

    const/4 v4, 0x2

    if-eqz v0, :cond_4

    .line 686
    iget v0, p1, Landroid/os/Message;->what:I

    if-eqz v0, :cond_3

    if-eq v0, v4, :cond_3

    if-eq v0, v3, :cond_3

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :cond_3
    :pswitch_0
    return v2

    .line 702
    :cond_4
    :goto_0
    iget v0, p1, Landroid/os/Message;->what:I

    const-wide/16 v5, 0x64

    const/4 v7, 0x0

    packed-switch v0, :pswitch_data_1

    :pswitch_1
    goto/16 :goto_8

    .line 797
    :pswitch_2
    iget-object v0, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    invoke-static {v0, v2}, Lepson/print/screen/PrintProgress;->access$302(Lepson/print/screen/PrintProgress;Z)Z

    .line 802
    iget v0, p1, Landroid/os/Message;->arg1:I

    const/16 v1, -0x44c

    if-eq v0, v1, :cond_6

    const/16 v1, -0x420

    if-eq v0, v1, :cond_5

    packed-switch v0, :pswitch_data_2

    packed-switch v0, :pswitch_data_3

    return v2

    .line 824
    :pswitch_3
    iget-object p1, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    const v0, 0x7f0e0059

    invoke-virtual {p1, v0}, Lepson/print/screen/PrintProgress;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 825
    iget-object v0, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    const v1, 0x7f0e0058

    invoke-virtual {v0, v1}, Lepson/print/screen/PrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 829
    :pswitch_4
    iget-object p1, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    const v0, 0x7f0e005c

    invoke-virtual {p1, v0}, Lepson/print/screen/PrintProgress;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 830
    iget-object v0, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    const v1, 0x7f0e005b

    invoke-virtual {v0, v1}, Lepson/print/screen/PrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 819
    :pswitch_5
    iget-object p1, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    const v0, 0x7f0e0057

    invoke-virtual {p1, v0}, Lepson/print/screen/PrintProgress;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 820
    iget-object v0, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    const v1, 0x7f0e0056

    invoke-virtual {v0, v1}, Lepson/print/screen/PrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 812
    :pswitch_6
    iget-object p1, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    const v0, 0x7f0e0050

    invoke-virtual {p1, v0}, Lepson/print/screen/PrintProgress;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 813
    iget-object v0, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    const v1, 0x7f0e004c

    invoke-virtual {v0, v1}, Lepson/print/screen/PrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 845
    :pswitch_7
    iget-object p1, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    const v0, 0x7f0e0353

    invoke-virtual {p1, v0}, Lepson/print/screen/PrintProgress;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 846
    iget-object v0, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    const v1, 0x7f0e0352

    invoke-virtual {v0, v1}, Lepson/print/screen/PrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 835
    :pswitch_8
    iget-object v0, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    const v1, 0x7f0e0049

    invoke-virtual {v0, v1}, Lepson/print/screen/PrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 836
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    const v4, 0x7f0e0047

    invoke-virtual {v3, v4}, Lepson/print/screen/PrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "0X"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p1, p1, Landroid/os/Message;->arg1:I

    .line 837
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    const v3, 0x7f0e0048

    .line 838
    invoke-virtual {p1, v3}, Lepson/print/screen/PrintProgress;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    move-object v8, v0

    move-object v0, p1

    move-object p1, v8

    goto :goto_1

    .line 855
    :cond_5
    :pswitch_9
    iget-object v0, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    const v1, 0x7f0e0055

    invoke-virtual {v0, v1}, Lepson/print/screen/PrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 856
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    const v4, 0x7f0e0054

    invoke-virtual {v3, v4}, Lepson/print/screen/PrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    const v4, 0x7f0e0052

    .line 857
    invoke-virtual {v3, v4}, Lepson/print/screen/PrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "0X"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p1, p1, Landroid/os/Message;->arg1:I

    .line 858
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    const v3, 0x7f0e0053

    .line 859
    invoke-virtual {p1, v3}, Lepson/print/screen/PrintProgress;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    move-object v8, v0

    move-object v0, p1

    move-object p1, v8

    goto :goto_1

    .line 804
    :cond_6
    iget-object p1, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    const v0, 0x7f0e0060

    invoke-virtual {p1, v0}, Lepson/print/screen/PrintProgress;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 805
    iget-object v0, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    const v1, 0x7f0e005f

    invoke-virtual {v0, v1}, Lepson/print/screen/PrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 866
    :goto_1
    iget-object v1, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    invoke-virtual {v1, p1, v0}, Lepson/print/screen/PrintProgress;->showErrorDialog(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_8

    .line 870
    :pswitch_a
    new-instance p1, Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    invoke-static {v0}, Lepson/print/screen/PrintProgress;->access$1100(Lepson/print/screen/PrintProgress;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 871
    invoke-virtual {p1, v7}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    const v3, 0x7f0e035c

    .line 872
    invoke-virtual {v1, v3}, Lepson/print/screen/PrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    const v3, 0x7f0e035d

    .line 873
    invoke-virtual {v1, v3}, Lepson/print/screen/PrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    const v3, 0x7f0e035e

    .line 874
    invoke-virtual {v1, v3}, Lepson/print/screen/PrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 872
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    iget-object v0, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    const v1, 0x7f0e04f2

    .line 875
    invoke-virtual {v0, v1}, Lepson/print/screen/PrintProgress;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lepson/print/screen/PrintProgress$4$1;

    invoke-direct {v1, p0}, Lepson/print/screen/PrintProgress$4$1;-><init>(Lepson/print/screen/PrintProgress$4;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 882
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_8

    .line 704
    :pswitch_b
    iget-object p1, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    invoke-static {p1, v2}, Lepson/print/screen/PrintProgress;->access$1202(Lepson/print/screen/PrintProgress;I)I

    .line 705
    iget-object p1, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    invoke-static {p1, v2}, Lepson/print/screen/PrintProgress;->access$1302(Lepson/print/screen/PrintProgress;I)I

    .line 708
    iget-object p1, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    invoke-static {p1}, Lepson/print/screen/PrintProgress;->access$1100(Lepson/print/screen/PrintProgress;)Landroid/content/Context;

    move-result-object p1

    const-string v0, "printer"

    invoke-static {p1, v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->isNeedConnect(Landroid/content/Context;Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_7

    goto :goto_2

    .line 712
    :cond_7
    iget-object p1, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    const-string v0, "printer"

    invoke-static {p1, v0, v2}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->reconnect(Landroid/app/Activity;Ljava/lang/String;I)Z

    move-result p1

    if-eqz p1, :cond_8

    goto/16 :goto_8

    .line 720
    :cond_8
    :goto_2
    :pswitch_c
    invoke-static {}, Lepson/print/screen/PrintProgress;->access$800()Lepson/print/service/IEpsonService;

    move-result-object p1

    if-eqz p1, :cond_b

    :try_start_0
    const-string p1, "Epson"

    const-string v0, "begin probe printer before printing"

    .line 722
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 724
    iget-object p1, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    invoke-static {p1}, Lepson/print/MyPrinter;->getCurPrinter(Landroid/content/Context;)Lepson/print/MyPrinter;

    move-result-object p1

    .line 725
    invoke-virtual {p1}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v0

    .line 726
    invoke-virtual {p1}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object v1

    if-eqz v0, :cond_9

    .line 728
    invoke-virtual {p1}, Lepson/print/MyPrinter;->getLocation()I

    move-result v5

    if-ne v5, v2, :cond_9

    .line 729
    iget-object p1, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    invoke-static {p1, v2}, Lepson/print/screen/PrintProgress;->access$702(Lepson/print/screen/PrintProgress;Z)Z

    .line 730
    invoke-static {}, Lepson/print/screen/PrintProgress;->access$800()Lepson/print/service/IEpsonService;

    move-result-object p1

    invoke-interface {p1, v0, v1, v2}, Lepson/print/service/IEpsonService;->searchPrinters(Ljava/lang/String;Ljava/lang/String;I)I

    goto/16 :goto_8

    :cond_9
    if-eqz v1, :cond_a

    .line 731
    invoke-virtual {p1}, Lepson/print/MyPrinter;->getLocation()I

    move-result p1

    const/4 v5, 0x3

    if-ne p1, v5, :cond_a

    .line 732
    iget-object p1, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    invoke-static {p1, v2}, Lepson/print/screen/PrintProgress;->access$702(Lepson/print/screen/PrintProgress;Z)Z

    .line 733
    invoke-static {}, Lepson/print/screen/PrintProgress;->access$800()Lepson/print/service/IEpsonService;

    move-result-object p1

    invoke-interface {p1, v0, v1, v4}, Lepson/print/service/IEpsonService;->searchPrinters(Ljava/lang/String;Ljava/lang/String;I)I

    goto/16 :goto_8

    .line 735
    :cond_a
    sput-boolean v7, Lepson/print/screen/PrintProgress;->isContinue:Z

    const/16 p1, -0x547

    .line 736
    sput p1, Lepson/print/screen/PrintProgress;->curError:I

    .line 737
    iget-object p1, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    invoke-static {p1, v2}, Lepson/print/screen/PrintProgress;->access$302(Lepson/print/screen/PrintProgress;Z)Z

    .line 738
    iget-object p1, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    iget-object p1, p1, Lepson/print/screen/PrintProgress;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_8

    :catch_0
    move-exception p1

    .line 741
    invoke-virtual {p1}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_8

    .line 744
    :cond_b
    iget-object p1, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    iget-object p1, p1, Lepson/print/screen/PrintProgress;->mHandler:Landroid/os/Handler;

    const/16 v0, 0xa

    invoke-virtual {p1, v0, v5, v6}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_8

    :pswitch_d
    const-string p1, "PrintProgress"

    .line 886
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "message FINISH : mError => "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    invoke-static {v4}, Lepson/print/screen/PrintProgress;->access$300(Lepson/print/screen/PrintProgress;)Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v4, " mCanceled =>"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    iget-boolean v4, v4, Lepson/print/screen/PrintProgress;->mCanceled:Z

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 888
    invoke-static {}, Lepson/print/screen/PrintProgress;->access$800()Lepson/print/service/IEpsonService;

    move-result-object p1

    if-eqz p1, :cond_f

    .line 891
    :try_start_1
    invoke-static {}, Lepson/print/screen/PrintProgress;->access$100()Ljava/lang/Object;

    move-result-object p1

    monitor-enter p1
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 892
    :try_start_2
    iget-object v0, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    invoke-static {v0}, Lepson/print/screen/PrintProgress;->access$500(Lepson/print/screen/PrintProgress;)Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-static {}, Lepson/print/screen/PrintProgress;->access$1800()Z

    move-result v0

    if-nez v0, :cond_c

    invoke-static {}, Lepson/print/screen/PrintProgress;->access$800()Lepson/print/service/IEpsonService;

    move-result-object v0

    invoke-interface {v0}, Lepson/print/service/IEpsonService;->isSearchingPrinter()Z

    move-result v0

    if-eqz v0, :cond_c

    const-string v0, "PrintProgress"

    const-string v4, "message FINISH: mEpsonService.cancelSearchPrinter()"

    .line 893
    invoke-static {v0, v4}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 895
    invoke-static {}, Lepson/print/screen/PrintProgress;->access$800()Lepson/print/service/IEpsonService;

    move-result-object v0

    invoke-interface {v0}, Lepson/print/service/IEpsonService;->cancelSearchPrinter()I

    const/4 v0, 0x1

    goto :goto_3

    :cond_c
    const/4 v0, 0x0

    .line 897
    :goto_3
    monitor-exit p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 899
    :try_start_3
    invoke-static {}, Lepson/print/screen/PrintProgress;->access$800()Lepson/print/service/IEpsonService;

    move-result-object p1

    if-eqz p1, :cond_d

    invoke-static {}, Lepson/print/screen/PrintProgress;->access$800()Lepson/print/service/IEpsonService;

    move-result-object p1

    invoke-interface {p1}, Lepson/print/service/IEpsonService;->isPrinting()Z

    move-result p1

    if-eqz p1, :cond_d

    invoke-static {}, Lepson/print/screen/PrintProgress;->access$1800()Z

    move-result p1

    if-nez p1, :cond_d

    const-string p1, "PrintProgress"

    const-string v0, "message FINISH: mEpsonService.confirmCancel(true) call"

    .line 900
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 901
    invoke-static {}, Lepson/print/screen/PrintProgress;->access$800()Lepson/print/service/IEpsonService;

    move-result-object p1

    invoke-interface {p1, v2}, Lepson/print/service/IEpsonService;->confirmCancel(Z)I

    const/4 v0, 0x1

    :cond_d
    if-eqz v0, :cond_e

    .line 904
    iget-object p1, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    invoke-static {p1}, Lepson/print/screen/PrintProgress;->access$1900(Lepson/print/screen/PrintProgress;)Z

    move-result p1

    if-eqz p1, :cond_e

    const-string p1, "PrintProgress"

    const-string v0, "on message FINISH: sendEmptyMessageDelayed(FINISH...)"

    .line 905
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 907
    iget-object p1, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    iget-object p1, p1, Lepson/print/screen/PrintProgress;->mHandler:Landroid/os/Handler;

    const-wide/16 v4, 0x1f4

    invoke-virtual {p1, v1, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_1

    return v2

    :catchall_0
    move-exception v0

    .line 897
    :try_start_4
    monitor-exit p1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v0
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_1

    :catch_1
    move-exception p1

    .line 911
    invoke-virtual {p1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 914
    :cond_e
    :try_start_6
    invoke-static {}, Lepson/print/screen/PrintProgress;->access$800()Lepson/print/service/IEpsonService;

    move-result-object p1

    if-eqz p1, :cond_f

    const-string p1, "PrintProgress"

    const-string v0, "message FINISH: mEpsonService.unregisterCallback() call"

    .line 915
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 916
    invoke-static {}, Lepson/print/screen/PrintProgress;->access$800()Lepson/print/service/IEpsonService;

    move-result-object p1

    invoke-static {}, Lepson/print/screen/PrintProgress;->access$900()Lepson/print/service/IEpsonServiceCallback;

    move-result-object v0

    invoke-interface {p1, v0}, Lepson/print/service/IEpsonService;->unregisterCallback(Lepson/print/service/IEpsonServiceCallback;)V

    .line 917
    iget-object p1, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    invoke-static {}, Lepson/print/screen/PrintProgress;->access$2000()Landroid/content/ServiceConnection;

    move-result-object v0

    invoke-virtual {p1, v0}, Lepson/print/screen/PrintProgress;->unbindService(Landroid/content/ServiceConnection;)V

    const/4 p1, 0x0

    .line 918
    invoke-static {p1}, Lepson/print/screen/PrintProgress;->access$802(Lepson/print/service/IEpsonService;)Lepson/print/service/IEpsonService;
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_2

    goto :goto_4

    :catch_2
    move-exception p1

    .line 921
    invoke-virtual {p1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 927
    :cond_f
    :goto_4
    iget-object p1, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    iget-boolean p1, p1, Lepson/print/screen/PrintProgress;->mCanceled:Z

    if-eqz p1, :cond_10

    const/4 v3, 0x0

    goto :goto_5

    .line 929
    :cond_10
    iget-object p1, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    invoke-static {p1}, Lepson/print/screen/PrintProgress;->access$300(Lepson/print/screen/PrintProgress;)Z

    move-result p1

    if-eqz p1, :cond_11

    const/16 v3, 0x3e8

    .line 932
    :cond_11
    :goto_5
    iget-object p1, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    invoke-virtual {p1}, Lepson/print/screen/PrintProgress;->getParent()Landroid/app/Activity;

    move-result-object p1

    if-nez p1, :cond_12

    .line 933
    iget-object p1, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    invoke-virtual {p1, v3}, Lepson/print/screen/PrintProgress;->setResult(I)V

    goto :goto_6

    .line 935
    :cond_12
    iget-object p1, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    invoke-virtual {p1}, Lepson/print/screen/PrintProgress;->getParent()Landroid/app/Activity;

    move-result-object p1

    invoke-virtual {p1, v3}, Landroid/app/Activity;->setResult(I)V

    .line 938
    :goto_6
    iget-object p1, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    invoke-static {p1, v7}, Lepson/print/screen/PrintProgress;->access$702(Lepson/print/screen/PrintProgress;Z)Z

    const-string p1, "PrintProgress"

    .line 939
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "finish with return code => "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 942
    iget-object p1, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    invoke-static {p1}, Lepson/print/screen/PrintProgress;->access$1100(Lepson/print/screen/PrintProgress;)Landroid/content/Context;

    move-result-object p1

    const-string v0, "printer"

    iget-object v1, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    invoke-static {v1}, Lepson/print/screen/PrintProgress;->access$000(Lepson/print/screen/PrintProgress;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->disconnect(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    .line 944
    iget-object p1, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    invoke-static {p1}, Lepson/print/screen/PrintProgress;->access$2100(Lepson/print/screen/PrintProgress;)V

    .line 946
    iget-object p1, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    invoke-static {p1}, Lepson/print/screen/PrintProgress;->access$2200(Lepson/print/screen/PrintProgress;)V

    .line 948
    iget-object p1, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    invoke-virtual {p1}, Lepson/print/screen/PrintProgress;->finish()V

    goto/16 :goto_8

    :pswitch_e
    const-string p1, "Epson"

    const-string v0, "NOTIFY ERROR"

    .line 783
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 784
    iget-object p1, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    invoke-static {p1}, Lepson/print/screen/PrintProgress;->access$600(Lepson/print/screen/PrintProgress;)Z

    move-result p1

    if-nez p1, :cond_13

    const-string p1, "Epson"

    .line 785
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Show Error code:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v1, Lepson/print/screen/PrintProgress;->curError:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_7

    :cond_13
    const-string p1, "Epson"

    .line 788
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Remove and show Error code:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v1, Lepson/print/screen/PrintProgress;->curError:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 790
    iget-object p1, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    invoke-virtual {p1, v7}, Lepson/print/screen/PrintProgress;->removeDialog(I)V

    .line 792
    :goto_7
    iget-object p1, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    invoke-static {p1, v2}, Lepson/print/screen/PrintProgress;->access$602(Lepson/print/screen/PrintProgress;Z)Z

    .line 793
    iget-object p1, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    invoke-virtual {p1, v7}, Lepson/print/screen/PrintProgress;->showDialog(I)V

    goto :goto_8

    .line 765
    :pswitch_f
    iget-object p1, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    invoke-static {p1, v7}, Lepson/print/screen/PrintProgress;->access$302(Lepson/print/screen/PrintProgress;Z)Z

    .line 767
    invoke-static {}, Lepson/print/screen/PrintProgress;->access$800()Lepson/print/service/IEpsonService;

    move-result-object p1

    if-eqz p1, :cond_14

    .line 769
    :try_start_7
    iget-object p1, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    invoke-static {p1}, Lepson/print/screen/PrintProgress;->access$1600(Lepson/print/screen/PrintProgress;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3

    goto :goto_8

    :catch_3
    move-exception p1

    .line 771
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_8

    :cond_14
    const-string p1, "Epson"

    const-string v0, "Service or resource file not ready, please wait..."

    .line 774
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 775
    iget-object p1, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    iget-object p1, p1, Lepson/print/screen/PrintProgress;->mPercent:Landroid/widget/TextView;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    invoke-static {v1}, Lepson/print/screen/PrintProgress;->access$1700(Lepson/print/screen/PrintProgress;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "         0%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 776
    sget-object p1, Lepson/print/screen/PrintProgress;->mProgressPercent:Landroid/widget/ProgressBar;

    invoke-virtual {p1, v7}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 777
    iget-object p1, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    iget-object p1, p1, Lepson/print/screen/PrintProgress;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v4, v5, v6}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_8

    .line 750
    :pswitch_10
    iget v0, p1, Landroid/os/Message;->arg2:I

    .line 751
    iget p1, p1, Landroid/os/Message;->arg1:I

    .line 752
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "UPDATE_PERCENT "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;)V

    packed-switch p1, :pswitch_data_4

    goto :goto_8

    .line 758
    :pswitch_11
    iget-object p1, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    invoke-static {p1, v0}, Lepson/print/screen/PrintProgress;->access$1500(Lepson/print/screen/PrintProgress;I)V

    goto :goto_8

    .line 755
    :pswitch_12
    iget-object p1, p0, Lepson/print/screen/PrintProgress$4;->this$0:Lepson/print/screen/PrintProgress;

    invoke-static {p1, v0}, Lepson/print/screen/PrintProgress;->access$1400(Lepson/print/screen/PrintProgress;I)V

    :goto_8
    return v2

    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_10
        :pswitch_1
        :pswitch_f
        :pswitch_1
        :pswitch_e
        :pswitch_d
        :pswitch_b
        :pswitch_1
        :pswitch_a
        :pswitch_2
        :pswitch_c
    .end packed-switch

    :pswitch_data_2
    .packed-switch -0x4b9
        :pswitch_8
        :pswitch_8
    .end packed-switch

    :pswitch_data_3
    .packed-switch -0x4b6
        :pswitch_7
        :pswitch_6
        :pswitch_9
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_9
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x1
        :pswitch_12
        :pswitch_11
    .end packed-switch
.end method

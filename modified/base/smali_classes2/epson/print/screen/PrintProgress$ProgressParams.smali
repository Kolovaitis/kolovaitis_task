.class public interface abstract Lepson/print/screen/PrintProgress$ProgressParams;
.super Ljava/lang/Object;
.source "PrintProgress.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/screen/PrintProgress;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ProgressParams"
.end annotation


# virtual methods
.method public abstract getApfMode()Z
.end method

.method public abstract getEpsonColorMode()Z
.end method

.method public abstract getOriginalSheetSize()I
.end method

.method public abstract getPrintLog()Lcom/epson/iprint/prtlogger/PrintLog;
.end method

.method public abstract getPrintSetting(Landroid/content/Context;)Lepson/print/screen/PrintSetting;
.end method

.method public abstract getPrintSettingType()Lepson/print/screen/PrintSetting$Kind;
.end method

.method public abstract isPaperLandscape()Z
.end method

.method public abstract print(Lepson/print/service/IEpsonService;Z)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.class Lepson/print/screen/PaperSourceSettingScr$5;
.super Lepson/print/service/IEpsonServiceCallback$Stub;
.source "PaperSourceSettingScr.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/screen/PaperSourceSettingScr;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/screen/PaperSourceSettingScr;


# direct methods
.method constructor <init>(Lepson/print/screen/PaperSourceSettingScr;)V
    .locals 0

    .line 436
    iput-object p1, p0, Lepson/print/screen/PaperSourceSettingScr$5;->this$0:Lepson/print/screen/PaperSourceSettingScr;

    invoke-direct {p0}, Lepson/print/service/IEpsonServiceCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onFindPrinterResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    return-void
.end method

.method public onGetInkState()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    return-void
.end method

.method public onGetStatusState()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    return-void
.end method

.method public onNotifyContinueable(I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    return-void
.end method

.method public onNotifyEndJob(I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    return-void
.end method

.method public onNotifyError(IIZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    return-void
.end method

.method public onNotifyProgress(II)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    return-void
.end method

.class Lepson/print/IprintHome$ProbePrinter;
.super Landroid/os/AsyncTask;
.source "IprintHome.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/IprintHome;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ProbePrinter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/IprintHome;


# direct methods
.method private constructor <init>(Lepson/print/IprintHome;)V
    .locals 0

    .line 597
    iput-object p1, p0, Lepson/print/IprintHome$ProbePrinter;->this$0:Lepson/print/IprintHome;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lepson/print/IprintHome;Lepson/print/IprintHome$1;)V
    .locals 0

    .line 597
    invoke-direct {p0, p1}, Lepson/print/IprintHome$ProbePrinter;-><init>(Lepson/print/IprintHome;)V

    return-void
.end method


# virtual methods
.method public cancelTask(Z)Z
    .locals 2

    const-string v0, "IprintHome"

    const-string v1, "ProbePrinter cancelTask IN"

    .line 693
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 694
    sget-object v0, Lepson/print/IprintHome;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-virtual {v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doCancelFindPrinter()I

    const-string v0, "IprintHome"

    const-string v1, "ProbePrinter cancelTask OUT"

    .line 695
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 696
    invoke-virtual {p0, p1}, Lepson/print/IprintHome$ProbePrinter;->cancel(Z)Z

    move-result p1

    return p1
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 9

    const-string p1, "IprintHome"

    const-string v0, "ProbePrinter doInBackground"

    .line 608
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 610
    sget-object p1, Lepson/print/IprintHome;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    iget-object v0, p0, Lepson/print/IprintHome$ProbePrinter;->this$0:Lepson/print/IprintHome;

    const/4 v1, 0x2

    invoke-virtual {p1, v0, v1}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doInitDriver(Landroid/content/Context;I)I

    .line 613
    iget-object p1, p0, Lepson/print/IprintHome$ProbePrinter;->this$0:Lepson/print/IprintHome;

    invoke-static {p1}, Lepson/print/IprintHome;->access$200(Lepson/print/IprintHome;)I

    move-result p1

    const/4 v0, 0x3

    const/4 v2, 0x1

    if-eq p1, v2, :cond_0

    iget-object p1, p0, Lepson/print/IprintHome$ProbePrinter;->this$0:Lepson/print/IprintHome;

    invoke-static {p1}, Lepson/print/IprintHome;->access$200(Lepson/print/IprintHome;)I

    move-result p1

    if-ne p1, v0, :cond_8

    .line 614
    :cond_0
    iget-object p1, p0, Lepson/print/IprintHome$ProbePrinter;->this$0:Lepson/print/IprintHome;

    invoke-static {p1}, Lepson/print/IprintHome;->access$300(Lepson/print/IprintHome;)Ljava/lang/String;

    move-result-object p1

    const/4 v3, 0x0

    if-eqz p1, :cond_7

    .line 615
    sget-object p1, Lepson/print/IprintHome;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    iget-object v4, p0, Lepson/print/IprintHome$ProbePrinter;->this$0:Lepson/print/IprintHome;

    invoke-static {v4}, Lepson/print/IprintHome;->access$300(Lepson/print/IprintHome;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lepson/print/IprintHome$ProbePrinter;->this$0:Lepson/print/IprintHome;

    invoke-static {v5}, Lepson/print/IprintHome;->access$400(Lepson/print/IprintHome;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lepson/print/IprintHome$ProbePrinter;->this$0:Lepson/print/IprintHome;

    invoke-static {v6}, Lepson/print/IprintHome;->access$200(Lepson/print/IprintHome;)I

    move-result v6

    const/16 v7, 0x3c

    invoke-virtual {p1, v7, v4, v5, v6}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doProbePrinter(ILjava/lang/String;Ljava/lang/String;I)I

    move-result p1

    :goto_0
    const-wide/16 v4, 0x2710

    if-eqz p1, :cond_1

    .line 618
    :try_start_0
    invoke-virtual {p0}, Lepson/print/IprintHome$ProbePrinter;->isCancelled()Z

    move-result p1

    if-nez p1, :cond_1

    .line 620
    iget-object p1, p0, Lepson/print/IprintHome$ProbePrinter;->this$0:Lepson/print/IprintHome;

    iget-object p1, p1, Lepson/print/IprintHome;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 622
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V

    .line 623
    sget-object p1, Lepson/print/IprintHome;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    iget-object v4, p0, Lepson/print/IprintHome$ProbePrinter;->this$0:Lepson/print/IprintHome;

    invoke-static {v4}, Lepson/print/IprintHome;->access$300(Lepson/print/IprintHome;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lepson/print/IprintHome$ProbePrinter;->this$0:Lepson/print/IprintHome;

    invoke-static {v5}, Lepson/print/IprintHome;->access$400(Lepson/print/IprintHome;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lepson/print/IprintHome$ProbePrinter;->this$0:Lepson/print/IprintHome;

    invoke-static {v6}, Lepson/print/IprintHome;->access$200(Lepson/print/IprintHome;)I

    move-result v6

    invoke-virtual {p1, v7, v4, v5, v6}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doProbePrinter(ILjava/lang/String;Ljava/lang/String;I)I

    move-result p1
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 626
    invoke-virtual {p1}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 627
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    .line 629
    :cond_1
    iget-object p1, p0, Lepson/print/IprintHome$ProbePrinter;->this$0:Lepson/print/IprintHome;

    iput-boolean v2, p1, Lepson/print/IprintHome;->finishProbe:Z

    .line 638
    invoke-virtual {p0}, Lepson/print/IprintHome$ProbePrinter;->isCancelled()Z

    move-result p1

    if-eqz p1, :cond_2

    const-string p1, "IprintHome"

    const-string v0, "Cancelled ProbePrinter"

    .line 641
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 642
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    .line 644
    :cond_2
    sget-object p1, Lepson/print/IprintHome;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doSetPrinter()I

    move-result p1

    const-string v6, "IprintHome"

    .line 645
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Set Printer result: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p1, :cond_3

    .line 648
    iget-object p1, p0, Lepson/print/IprintHome$ProbePrinter;->this$0:Lepson/print/IprintHome;

    iget-object p1, p1, Lepson/print/IprintHome;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 649
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    .line 652
    :cond_3
    :goto_1
    invoke-virtual {p0}, Lepson/print/IprintHome$ProbePrinter;->isCancelled()Z

    move-result p1

    if-nez p1, :cond_6

    .line 654
    :try_start_1
    sget-object p1, Lepson/print/IprintHome;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doGetStatus()I

    move-result p1

    const-string v6, "IprintHome"

    .line 655
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Printer Status result: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p1, :cond_4

    .line 658
    iget-object p1, p0, Lepson/print/IprintHome$ProbePrinter;->this$0:Lepson/print/IprintHome;

    iget-object p1, p1, Lepson/print/IprintHome;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_2

    .line 660
    :cond_4
    iget-object p1, p0, Lepson/print/IprintHome$ProbePrinter;->this$0:Lepson/print/IprintHome;

    sget-object v6, Lepson/print/IprintHome;->mPrinter:Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    invoke-virtual {v6}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->getMPrinterInfor()Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;

    move-result-object v6

    invoke-virtual {v6}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinterInfo2;->getMStatus()[I

    move-result-object v6

    invoke-static {p1, v6}, Lepson/print/IprintHome;->access$102(Lepson/print/IprintHome;[I)[I

    .line 661
    iget-object p1, p0, Lepson/print/IprintHome$ProbePrinter;->this$0:Lepson/print/IprintHome;

    invoke-static {p1}, Lepson/print/IprintHome;->access$100(Lepson/print/IprintHome;)[I

    move-result-object p1

    aget p1, p1, v3

    if-eqz p1, :cond_5

    .line 663
    new-instance p1, Landroid/os/Message;

    invoke-direct {p1}, Landroid/os/Message;-><init>()V

    .line 664
    iput v0, p1, Landroid/os/Message;->what:I

    .line 665
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    const-string v7, "STATUS_ERROR"

    .line 666
    iget-object v8, p0, Lepson/print/IprintHome$ProbePrinter;->this$0:Lepson/print/IprintHome;

    invoke-static {v8}, Lepson/print/IprintHome;->access$100(Lepson/print/IprintHome;)[I

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 667
    invoke-virtual {p1, v6}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 668
    iget-object v6, p0, Lepson/print/IprintHome$ProbePrinter;->this$0:Lepson/print/IprintHome;

    iget-object v6, v6, Lepson/print/IprintHome;->mHandler:Landroid/os/Handler;

    invoke-virtual {v6, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_2

    .line 671
    :cond_5
    iget-object p1, p0, Lepson/print/IprintHome$ProbePrinter;->this$0:Lepson/print/IprintHome;

    iget-object p1, p1, Lepson/print/IprintHome;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 675
    :goto_2
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception p1

    const-string v6, "Background"

    const-string v7, "InterruptedException"

    .line 677
    invoke-static {v6, v7}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 678
    invoke-virtual {p1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    :cond_6
    const-string p1, "IprintHome"

    const-string v0, "task Cancelled"

    .line 681
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 683
    :cond_7
    iget-object p1, p0, Lepson/print/IprintHome$ProbePrinter;->this$0:Lepson/print/IprintHome;

    invoke-static {p1, v3}, Lepson/print/IprintHome;->access$202(Lepson/print/IprintHome;I)I

    .line 686
    :cond_8
    :goto_3
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 597
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/print/IprintHome$ProbePrinter;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 2

    const-string v0, "IprintHome"

    const-string v1, "ProbePrinter onPostExecute"

    .line 701
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 702
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 703
    iget-object p1, p0, Lepson/print/IprintHome$ProbePrinter;->this$0:Lepson/print/IprintHome;

    iget-object p1, p1, Lepson/print/IprintHome;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 597
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lepson/print/IprintHome$ProbePrinter;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    const-string v0, "IprintHome"

    const-string v1, "ProbePrinter onPreExecute"

    .line 601
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 602
    iget-object v0, p0, Lepson/print/IprintHome$ProbePrinter;->this$0:Lepson/print/IprintHome;

    iget-object v0, v0, Lepson/print/IprintHome;->printerStatus:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

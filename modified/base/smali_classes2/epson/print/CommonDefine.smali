.class public interface abstract Lepson/print/CommonDefine;
.super Ljava/lang/Object;
.source "CommonDefine.java"


# static fields
.field public static final ACC_NAME:Ljava/lang/String; = "accountName"

.field public static final ADD_PHOTO:I = 0x16

.field public static final ADD_PHOTO_CODE:I = 0x15

.field public static final ADD_PHOTO_PRINT:I = 0x11

.field public static final ADD_SCANNER:I = 0x4

.field public static final ALBUM:Ljava/lang/String; = "album"

.field public static final ALL_REFRESH:I = 0x6f

.field public static final ALL_REFRESH_DRAW_END:I = 0x72

.field public static final ALL_REFRESH_FINISH:I = 0x70

.field public static final ALL_REFRESH_START:I = 0x71

.field public static final APP_NAME:Ljava/lang/String; = "Google/1.0"

.field public static final ARD:Ljava/lang/String; = "ARD"

.field public static final ARDAPI:Ljava/lang/String; = "ARDAPI"

.field public static final ARDAPINUM:Ljava/lang/String; = "1.0."

.field public static final BODER:I = 0x2

.field public static final BODERLESS:I = 0x1

.field public static final CAMERA:I = 0x1

.field public static final CANCEL:Ljava/lang/String; = "Cancel"

.field public static final CDRadiusIn:I = 0x2b

.field public static final CDRadiusOut:I = 0x74

.field public static final CD_BODER:I = 0x4

.field public static final CHANGE_ORENTATION:I = 0x5

.field public static final CN2:Ljava/lang/String; = "&CN2="

.field public static final COLOR_CURRENT:Ljava/lang/String; = "current color"

.field public static final CONVERTED_OK:I = 0x0

.field public static final CONVERTED_OK_EC:I = 0x1f

.field public static final CONVERTED_PAGE:Ljava/lang/String; = "CONVERTED_PAGE"

.field public static final CONVERT_FINISH:I = 0x2

.field public static final CSV_TYPE:Ljava/lang/String; = "text/csv"

.field public static final CTC:Ljava/lang/String; = "&CTC="

.field public static final CUPCAKE:Ljava/lang/String; = "1.5.0"

.field public static final DEFAULT_DIR:Ljava/lang/String;

.field public static final DEFAULT_ENDPAGE:I = 0x1

.field public static final DEFAULT_FOLDER:Ljava/lang/String;

.field public static final DEFAULT_IMGANE_NAME:Ljava/lang/String; = "image_"

.field public static final DEFAULT_STARTPAGE:I = 0x1

.field public static final DEVICE_INFO:Ljava/lang/String; = "DeviceInfo"

.field public static final DISSDIALOG:I = 0x22

.field public static final DOCUMENT:I = 0x5

.field public static final DOCX_TYPE_2:Ljava/lang/String; = "application/vnd.openxmlformats-officedocument.wordprocessingml.document"

.field public static final DOC_SERVICE:Ljava/lang/String; = "writely"

.field public static final DOC_TYPE_1:Ljava/lang/String; = "application/msword"

.field public static final DONUT:Ljava/lang/String; = "1.6.0"

.field public static final DOT:Ljava/lang/String; = "."

.field public static final DOUBLE_TAB:I = 0x8

.field public static final DOWNLOAD_PATH:Ljava/lang/String; = "downloadPath"

.field public static final ECLAIR:Ljava/lang/String; = "2.0.0"

.field public static final ECLAIR_0_1:Ljava/lang/String; = "2.0.1"

.field public static final ECLAIR_MR1:Ljava/lang/String; = "2.1.0"

.field public static final END_CONVERT:I = 0x14

.field public static final END_POLLING_PREVIEW:I = 0x20

.field public static final END_PRINT:I = 0x4

.field public static final END_SETTING:I = 0x8

.field public static final EPSON:Ljava/lang/String; = "EPSON"

.field public static final EPSONIPRINT_FOLDER:Ljava/lang/String;

.field public static final EPSON_VENDERID:I = 0x4b8

.field public static final EQUAL_MARK:Ljava/lang/String; = "="

.field public static final EXCEL_TYPE_1:Ljava/lang/String; = "application/vnd.ms-excel"

.field public static final EXCEL_TYPE_2:Ljava/lang/String; = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"

.field public static final EXEL_SERVICE:Ljava/lang/String; = "wise"

.field public static final EXTRA_DOCFILENAME:Ljava/lang/String; = "extraDocFile"

.field public static final EXTRA_URL:Ljava/lang/String; = "extra_url"

.field public static final FILE_NAME:Ljava/lang/String; = "fileName"

.field public static final FILE_PATH:Ljava/lang/String; = "FILE_PATH"

.field public static final FILE_SECURE_INSDCARD:Ljava/lang/String; = ".android_secure"

.field public static final FILE_THUMBNAIL:Ljava/lang/String; = "/dcim/.thumbnails"

.field public static final FINISH:I = 0x7

.field public static final FIRST_ZOOMCONTROL:I = 0x7

.field public static final FROM_DOCUMENT:I = 0x3

.field public static final FROM_ONLINE_STORAGE:I = 0x4

.field public static final FROM_PHOTO:I = 0x0

.field public static final FROM_SCAN:I = 0x1

.field public static final FROM_WEB:I = 0x2

.field public static final FROYO:Ljava/lang/String; = "2.2.0"

.field public static final FileType_BMP:Ljava/lang/String; = ".bmp"

.field public static final FileType_CSV:Ljava/lang/String; = ".csv"

.field public static final FileType_EXCEL_1:Ljava/lang/String; = ".xls"

.field public static final FileType_EXCEL_2:Ljava/lang/String; = ".xlsx"

.field public static final FileType_HEIF:Ljava/lang/String; = ".heic"

.field public static final FileType_HTML_1:Ljava/lang/String; = ".htm"

.field public static final FileType_HTML_2:Ljava/lang/String; = ".html"

.field public static final FileType_JPEG:Ljava/lang/String; = ".jpeg"

.field public static final FileType_JPG:Ljava/lang/String; = ".jpg"

.field public static final FileType_PDF:Ljava/lang/String; = ".pdf"

.field public static final FileType_PNG:Ljava/lang/String; = ".png"

.field public static final FileType_POWERPOINT_1:Ljava/lang/String; = ".ppt"

.field public static final FileType_POWERPOINT_2:Ljava/lang/String; = ".pptx"

.field public static final FileType_RTF:Ljava/lang/String; = ".rtf"

.field public static final FileType_TXT:Ljava/lang/String; = ".txt"

.field public static final FileType_WORD_1:Ljava/lang/String; = ".doc"

.field public static final FileType_WORD_2:Ljava/lang/String; = ".docx"

.field public static final GETSIZEMAX:I = 0x21

.field public static final GINGERBREAD:Ljava/lang/String; = "2.3.0"

.field public static final GINGERBREAD_MR1:Ljava/lang/String; = "2.3.3"

.field public static final GOOGLE:Ljava/lang/String; = "google"

.field public static final GOOGLEUSERCONTENT:Ljava/lang/String; = "googleusercontent"

.field public static final GOOGLE_TYPE:Ljava/lang/String; = "com.google"

.field public static final GOTTOTALPAGE:I = 0x2

.field public static final HELP:I = 0x7

.field public static final HONEYCOMB:Ljava/lang/String; = "3.0.0"

.field public static final HONEYCOMB_R1:Ljava/lang/String; = "3.1.0"

.field public static final HONEYCOMB_R2:Ljava/lang/String; = "3.2.0"

.field public static final HTML_TYPE:Ljava/lang/String; = "text/html"

.field public static final HTTP_PREFIX:Ljava/lang/String; = "http://"

.field public static final ID_SAVEIMAGE:I = 0x0

.field public static final ID_VIEWDOCUMENT:I = 0x3

.field public static final ID_VIEWIMAGE:I = 0x1

.field public static final ID_VIEWPDF:I = 0x2

.field public static final IMAGE_EXTENSIVE_1:Ljava/lang/String; = ".jpg"

.field public static final IMAGE_EXTENSIVE_2:Ljava/lang/String; = ".png"

.field public static final IMAGE_EXTENSIVE_4:Ljava/lang/String; = ".bmp"

.field public static final IMAGE_EXTENSIVE_7:Ljava/lang/String; = ".jpeg"

.field public static final IMAGE_TEMP:Ljava/lang/String; = ".temp"

.field public static final IMAGE_TYPE:Ljava/lang/String; = "image/jpeg"

.field public static final IMAGE_TYPE_BMP:Ljava/lang/String; = "image/bmp"

.field public static final IMAGE_TYPE_HEIF:Ljava/lang/String; = "image/heif"

.field public static final IMAGE_TYPE_PNG:Ljava/lang/String; = "image/png"

.field public static final INFO_IMAGE:Ljava/lang/String; = "Info Image"

.field public static final INIT:I = 0xa

.field public static final INITVIEW:I = 0x14

.field public static final ITEM_HEIGHT:I = 0x60

.field public static final ITEM_WIDTH:I = 0x60

.field public static final ImageExist:Ljava/lang/String; = "ImageExist"

.field public static final LG2:Ljava/lang/String; = "LG2="

.field public static final LOAD_IMAGE:I = 0x11

.field public static final LOG:Ljava/lang/String; = "logging"

.field public static final MAX_PRINT_FILE:I = 0x2

.field public static final MAX_SIZE_FILE:J = 0x100000L

.field public static final MNT:Ljava/lang/String; = "/mnt"

.field public static final MOUNT_DIR:Ljava/lang/String; = "/mnt/"

.field public static final MOVEEND:I = 0x15

.field public static final MOVESTART:I = 0x13

.field public static final NEWPHOTO:I = 0x13

.field public static final NO_PRINT:Ljava/lang/String; = "InkjetCommon"

.field public static final NUMBER_IMAGE:Ljava/lang/String; = "Number_image"

.field public static final OATR:Ljava/lang/String; = "&OATR="

.field public static final OK:Ljava/lang/String; = "OK"

.field public static final ON_BACK:I = 0x12

.field public static final OPEN_ERROR:I = 0x6

.field public static final OPEN_PDF:I = 0xd

.field public static final OSC:Ljava/lang/String; = "&OSC="

.field public static final OSV:Ljava/lang/String; = "&OSV="

.field public static final PASSPROTECTED:I = 0x1

.field public static final PDF_TYPE:Ljava/lang/String; = "application/pdf"

.field public static final PERFORM_PRINT:I = 0x5

.field public static final PICTURE_HEIGHT:I = 0x96

.field public static final PICTURE_RESULT:I = 0x0

.field public static final PICTURE_WIDTH:I = 0x96

.field public static final POWERPOINT_TYPE_1:Ljava/lang/String; = "application/vnd.ms-powerpoint"

.field public static final POWERPOINT_TYPE_2:Ljava/lang/String; = "application/vnd.openxmlformats-officedocument.presentationml.presentation"

.field public static final PREFS_INFO_COPY:Ljava/lang/String; = "CopySetting"

.field public static final PREFS_INFO_PRINT:Ljava/lang/String; = "PrintSetting"

.field public static final PREFS_INFO_REPEAT_COPY:Ljava/lang/String; = "RepeatCopySetting"

.field public static final PREFS_NAME_COUNT_NUMBER:Ljava/lang/String; = "CountNumber"

.field public static final PREFS_NAME_PATH:Ljava/lang/String; = "path_photo"

.field public static final PREFS_NAME_PRESCAN_COUNT_NUMBER:Ljava/lang/String; = "PreScanCountNumber"

.field public static final PREVIEW_MODE:I = 0x0

.field public static final PRINTER_CHANGED:I = 0x5

.field public static final PRINT_MODE:I = 0x1

.field public static final PRINT_PHOTO:I = 0x0

.field public static final PRINT_WEB:I = 0x1

.field public static final PRN:Ljava/lang/String; = "&PRN="

.field public static final PSI4:Ljava/lang/String; = "&PSI4="

.field public static final QUESTION_MARK:Ljava/lang/String; = "?"

.field public static final REDRAW_PREVIEW:I = 0x32

.field public static final REFRESH:I = 0x3

.field public static final REMOTE_OPERATION_UUID:Ljava/lang/String; = "UUID"

.field public static final REQUEST_PRINT:I = 0xff

.field public static final REQUEST_SETTING:I = 0xfe

.field public static final RE_SEARCH:Ljava/lang/String; = "RE_SEARCH"

.field public static final RTF_TYPE:Ljava/lang/String; = "application/rtf"

.field public static final SAVE_BITMAP:I = 0x1

.field public static final SAVE_URL:Ljava/lang/String; = "SaveUrl"

.field public static final SELECTED_IM:Ljava/lang/String; = "selected_image"

.field public static final SELECT_IMAGE:I = 0x1

.field public static final SEND_DOCUMENT_VIEW:Ljava/lang/String; = "send document"

.field public static final SETTING:I = 0x6

.field public static final SETTING_DONE:I = 0x3

.field public static final SETTING_PHOTO:Ljava/lang/String; = "settingPhoto"

.field public static final SHOW_EC_ERROR_DIALOG:I = 0x27

.field public static final SID:Ljava/lang/String; = "&SID="

.field public static final SLASH:Ljava/lang/String; = "/"

.field public static final SPACE:Ljava/lang/String; = " "

.field public static final SPREADSHEETS:Ljava/lang/String; = "spreadsheets"

.field public static final STARTTHREADSAVE:I = 0xef

.field public static final START_CONVERT:I = 0x4

.field public static final START_POLLING_PREVIEW:I = 0x1e

.field public static final START_PREVIEW:I = 0xb

.field public static final START_PRINT:I = 0x16

.field public static final STR_SETTING_PHOTO:Ljava/lang/String; = "strSettingPhoto"

.field public static final STR_SETTING_PHOTO_NUMBER:Ljava/lang/String; = "strSettingPhotoNumber"

.field public static final STYPE:Ljava/lang/String; = "typeprint"

.field public static final TEXT_TYPE:Ljava/lang/String; = "text/plain"

.field public static final UNDER_BAR:Ljava/lang/String; = "_"

.field public static final UPDATE_BUTTON_TEXT_VIEW:I = 0x15

.field public static final UPDATE_COUNT_IMAGE:I = 0x9

.field public static final UPDATE_DATA:I = 0x0

.field public static final UPDATE_GALLERY:I = 0x6

.field public static final UPDATE_PRINTSETTINGS:I = 0x21

.field public static final UPDATE_STATE_VIEW:I = 0x12

.field public static final UPDATE_UI:I = 0x4

.field public static final UPDATE_ZOOMCONTROL:I = 0x5

.field public static final URL:Ljava/lang/String; = "url"

.field public static final URL_FAQ:Ljava/lang/String; = "iprt"

.field public static final URL_INK:Ljava/lang/String; = "buyink"

.field public static final URL_ONLINE_REGISTRATION:Ljava/lang/String; = "eRegistration"

.field public static final URL_PRIVACY:Ljava/lang/String; = "https://www.epsonconnect.com/privacyw"

.field public static final URL_TUTORIAL:Ljava/lang/String; = "iPrintOtherInfo"

.field public static final VIEW_ALBUM:I = 0x0

.field public static final VIEW_WEB:I = 0x2

.field public static final headUrl:Ljava/lang/String; = "https://go.epson.com/redirect.aspx?"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 20
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lepson/print/CommonDefine;->DEFAULT_DIR:Ljava/lang/String;

    .line 205
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lepson/print/CommonDefine;->DEFAULT_DIR:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "/Android/data/epson"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lepson/print/CommonDefine;->DEFAULT_FOLDER:Ljava/lang/String;

    .line 273
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lepson/print/CommonDefine;->DEFAULT_DIR:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "/Epson iPrint/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lepson/print/CommonDefine;->EPSONIPRINT_FOLDER:Ljava/lang/String;

    return-void
.end method

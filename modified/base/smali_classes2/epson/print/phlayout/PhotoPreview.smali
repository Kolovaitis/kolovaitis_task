.class public Lepson/print/phlayout/PhotoPreview;
.super Landroid/view/View;
.source "PhotoPreview.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/phlayout/PhotoPreview$MyScaledGestureListener;,
        Lepson/print/phlayout/PhotoPreview$MyGestureListener;,
        Lepson/print/phlayout/PhotoPreview$PreviewBitmapMaker;,
        Lepson/print/phlayout/PhotoPreview$ViewSizeChangeListener;,
        Lepson/print/phlayout/PhotoPreview$FitMode;
    }
.end annotation


# static fields
.field private static final ALL_REFRESH_DRAW_END:I = 0x4

.field private static final CREATE_PREVIEW:I = 0x1

.field static final FIT_MODE_CIRCUMSCRIBED:I = 0x1

.field static final FIT_MODE_FREE:I = 0x0

.field static final FIT_MODE_INSCRIBED:I = 0x2

.field private static final INVALID_IMAGE_NO:I = -0x1

.field private static final LOG_TAG:Ljava/lang/String; = "previewView"

.field private static final MAX_MAGNIFICATION_FACTOR:F = 8.0f

.field private static final MINI_MAGNIFICATION_FACTOR:F = 0.25f

.field private static final MOVEMENT_MARGIN_RATE:D = 0.2

.field public static final RESET_PREVIEW:I = 0x2

.field public static final RESTORE_PREVIEW:I = 0x0

.field private static final ZOOM_CONTROL:I = 0x2


# instance fields
.field private baseScaleFactor:F

.field private color:I

.field private drawEndHandler:Landroid/os/Handler;

.field private imageList:Lepson/print/phlayout/PhotoPreviewImageList;

.field private volatile imageNo:I

.field private final lockObj:Ljava/lang/Object;

.field private mBitmap:Landroid/graphics/Bitmap;

.field private final mBitmapDrawRect:Landroid/graphics/Rect;

.field private final mDrawLock:Ljava/util/concurrent/locks/ReentrantLock;

.field private final mFitLayout:Lepson/print/phlayout/FitLayout;

.field private volatile mFitMode:I

.field private mGestureDetector:Landroid/support/v4/view/GestureDetectorCompat;

.field private volatile mLayoutPosition:Lepson/print/phlayout/ILayoutPosition;

.field private mPaperSizeId:I

.field private final mPath:Landroid/graphics/Path;

.field private volatile mPreviewBitmapMaker:Lepson/print/phlayout/PhotoPreview$PreviewBitmapMaker;

.field private mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

.field private mViewSize:[I

.field private mViewSizeChangeListener:Lepson/print/phlayout/PhotoPreview$ViewSizeChangeListener;

.field private maskExpandPreview:Z

.field private maxBitmapHeight:I

.field private maxBitmapWidth:I

.field private final paint:Landroid/graphics/Paint;

.field private scaleFactor:F

.field private final trans:Landroid/graphics/Point;

.field private zoomControlHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .line 136
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 84
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lepson/print/phlayout/PhotoPreview;->lockObj:Ljava/lang/Object;

    .line 89
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lepson/print/phlayout/PhotoPreview;->mDrawLock:Ljava/util/concurrent/locks/ReentrantLock;

    const/4 v0, 0x0

    .line 92
    iput-object v0, p0, Lepson/print/phlayout/PhotoPreview;->imageList:Lepson/print/phlayout/PhotoPreviewImageList;

    const/4 v1, 0x0

    .line 95
    iput v1, p0, Lepson/print/phlayout/PhotoPreview;->imageNo:I

    .line 97
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lepson/print/phlayout/PhotoPreview;->paint:Landroid/graphics/Paint;

    .line 99
    iput v1, p0, Lepson/print/phlayout/PhotoPreview;->color:I

    .line 101
    iput v1, p0, Lepson/print/phlayout/PhotoPreview;->mPaperSizeId:I

    .line 102
    iput-object v0, p0, Lepson/print/phlayout/PhotoPreview;->mBitmap:Landroid/graphics/Bitmap;

    const/4 v1, -0x1

    .line 104
    iput v1, p0, Lepson/print/phlayout/PhotoPreview;->maxBitmapWidth:I

    .line 105
    iput v1, p0, Lepson/print/phlayout/PhotoPreview;->maxBitmapHeight:I

    .line 106
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    iput-object v1, p0, Lepson/print/phlayout/PhotoPreview;->trans:Landroid/graphics/Point;

    const/high16 v1, 0x3f800000    # 1.0f

    .line 108
    iput v1, p0, Lepson/print/phlayout/PhotoPreview;->baseScaleFactor:F

    .line 110
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lepson/print/phlayout/PhotoPreview;->mBitmapDrawRect:Landroid/graphics/Rect;

    .line 112
    iput-object v0, p0, Lepson/print/phlayout/PhotoPreview;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    const/4 v2, 0x1

    .line 115
    iput-boolean v2, p0, Lepson/print/phlayout/PhotoPreview;->maskExpandPreview:Z

    .line 116
    iput-object v0, p0, Lepson/print/phlayout/PhotoPreview;->drawEndHandler:Landroid/os/Handler;

    .line 117
    iput-object v0, p0, Lepson/print/phlayout/PhotoPreview;->zoomControlHandler:Landroid/os/Handler;

    .line 120
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    iput-object v2, p0, Lepson/print/phlayout/PhotoPreview;->mPath:Landroid/graphics/Path;

    .line 122
    iput v1, p0, Lepson/print/phlayout/PhotoPreview;->scaleFactor:F

    .line 128
    new-instance v1, Lepson/print/phlayout/FitLayout;

    invoke-direct {v1}, Lepson/print/phlayout/FitLayout;-><init>()V

    iput-object v1, p0, Lepson/print/phlayout/PhotoPreview;->mFitLayout:Lepson/print/phlayout/FitLayout;

    .line 131
    iput-object v0, p0, Lepson/print/phlayout/PhotoPreview;->mLayoutPosition:Lepson/print/phlayout/ILayoutPosition;

    const/4 v0, 0x2

    .line 133
    new-array v0, v0, [I

    iput-object v0, p0, Lepson/print/phlayout/PhotoPreview;->mViewSize:[I

    .line 137
    invoke-direct {p0, p1}, Lepson/print/phlayout/PhotoPreview;->initLocal(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .line 142
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 84
    new-instance p2, Ljava/lang/Object;

    invoke-direct {p2}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lepson/print/phlayout/PhotoPreview;->lockObj:Ljava/lang/Object;

    .line 89
    new-instance p2, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {p2}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object p2, p0, Lepson/print/phlayout/PhotoPreview;->mDrawLock:Ljava/util/concurrent/locks/ReentrantLock;

    const/4 p2, 0x0

    .line 92
    iput-object p2, p0, Lepson/print/phlayout/PhotoPreview;->imageList:Lepson/print/phlayout/PhotoPreviewImageList;

    const/4 v0, 0x0

    .line 95
    iput v0, p0, Lepson/print/phlayout/PhotoPreview;->imageNo:I

    .line 97
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lepson/print/phlayout/PhotoPreview;->paint:Landroid/graphics/Paint;

    .line 99
    iput v0, p0, Lepson/print/phlayout/PhotoPreview;->color:I

    .line 101
    iput v0, p0, Lepson/print/phlayout/PhotoPreview;->mPaperSizeId:I

    .line 102
    iput-object p2, p0, Lepson/print/phlayout/PhotoPreview;->mBitmap:Landroid/graphics/Bitmap;

    const/4 v0, -0x1

    .line 104
    iput v0, p0, Lepson/print/phlayout/PhotoPreview;->maxBitmapWidth:I

    .line 105
    iput v0, p0, Lepson/print/phlayout/PhotoPreview;->maxBitmapHeight:I

    .line 106
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lepson/print/phlayout/PhotoPreview;->trans:Landroid/graphics/Point;

    const/high16 v0, 0x3f800000    # 1.0f

    .line 108
    iput v0, p0, Lepson/print/phlayout/PhotoPreview;->baseScaleFactor:F

    .line 110
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lepson/print/phlayout/PhotoPreview;->mBitmapDrawRect:Landroid/graphics/Rect;

    .line 112
    iput-object p2, p0, Lepson/print/phlayout/PhotoPreview;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    const/4 v1, 0x1

    .line 115
    iput-boolean v1, p0, Lepson/print/phlayout/PhotoPreview;->maskExpandPreview:Z

    .line 116
    iput-object p2, p0, Lepson/print/phlayout/PhotoPreview;->drawEndHandler:Landroid/os/Handler;

    .line 117
    iput-object p2, p0, Lepson/print/phlayout/PhotoPreview;->zoomControlHandler:Landroid/os/Handler;

    .line 120
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lepson/print/phlayout/PhotoPreview;->mPath:Landroid/graphics/Path;

    .line 122
    iput v0, p0, Lepson/print/phlayout/PhotoPreview;->scaleFactor:F

    .line 128
    new-instance v0, Lepson/print/phlayout/FitLayout;

    invoke-direct {v0}, Lepson/print/phlayout/FitLayout;-><init>()V

    iput-object v0, p0, Lepson/print/phlayout/PhotoPreview;->mFitLayout:Lepson/print/phlayout/FitLayout;

    .line 131
    iput-object p2, p0, Lepson/print/phlayout/PhotoPreview;->mLayoutPosition:Lepson/print/phlayout/ILayoutPosition;

    const/4 p2, 0x2

    .line 133
    new-array p2, p2, [I

    iput-object p2, p0, Lepson/print/phlayout/PhotoPreview;->mViewSize:[I

    .line 143
    invoke-direct {p0, p1}, Lepson/print/phlayout/PhotoPreview;->initLocal(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$000(Lepson/print/phlayout/PhotoPreview;FF)V
    .locals 0

    .line 48
    invoke-direct {p0, p1, p2}, Lepson/print/phlayout/PhotoPreview;->onMoving(FF)V

    return-void
.end method

.method static synthetic access$100(Lepson/print/phlayout/PhotoPreview;FF)V
    .locals 0

    .line 48
    invoke-direct {p0, p1, p2}, Lepson/print/phlayout/PhotoPreview;->onMoveEventStarted(FF)V

    return-void
.end method

.method static synthetic access$200(Lepson/print/phlayout/PhotoPreview;)V
    .locals 0

    .line 48
    invoke-direct {p0}, Lepson/print/phlayout/PhotoPreview;->changeFitModeAndRefresh()V

    return-void
.end method

.method static synthetic access$300(Lepson/print/phlayout/PhotoPreview;)V
    .locals 0

    .line 48
    invoke-direct {p0}, Lepson/print/phlayout/PhotoPreview;->startScaleChange()V

    return-void
.end method

.method static synthetic access$400(Lepson/print/phlayout/PhotoPreview;F)V
    .locals 0

    .line 48
    invoke-direct {p0, p1}, Lepson/print/phlayout/PhotoPreview;->changeScale(F)V

    return-void
.end method

.method private adjustPosition(Landroid/graphics/Point;DLandroid/graphics/RectF;DD)V
    .locals 15
    .param p1    # Landroid/graphics/Point;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    move-object/from16 v0, p1

    move-object/from16 v1, p4

    .line 890
    iget v2, v1, Landroid/graphics/RectF;->right:F

    iget v3, v1, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v3

    float-to-double v2, v2

    mul-double v2, v2, p2

    .line 891
    iget v4, v1, Landroid/graphics/RectF;->bottom:F

    iget v5, v1, Landroid/graphics/RectF;->top:F

    sub-float/2addr v4, v5

    float-to-double v4, v4

    mul-double v4, v4, p2

    .line 894
    iget v6, v1, Landroid/graphics/RectF;->left:F

    float-to-double v6, v6

    add-double/2addr v6, v2

    iget v8, v0, Landroid/graphics/Point;->x:I

    int-to-double v8, v8

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    div-double v12, p5, v10

    add-double/2addr v8, v12

    cmpl-double v14, v6, v8

    if-lez v14, :cond_0

    .line 895
    iget v6, v1, Landroid/graphics/RectF;->left:F

    float-to-double v6, v6

    add-double/2addr v6, v2

    sub-double/2addr v6, v12

    double-to-int v6, v6

    iput v6, v0, Landroid/graphics/Point;->x:I

    .line 898
    :cond_0
    iget v6, v1, Landroid/graphics/RectF;->top:F

    float-to-double v6, v6

    add-double/2addr v6, v4

    iget v8, v0, Landroid/graphics/Point;->y:I

    int-to-double v8, v8

    div-double v10, p7, v10

    add-double/2addr v8, v10

    cmpl-double v14, v6, v8

    if-lez v14, :cond_1

    .line 899
    iget v6, v1, Landroid/graphics/RectF;->top:F

    float-to-double v6, v6

    add-double/2addr v6, v4

    sub-double/2addr v6, v10

    double-to-int v6, v6

    iput v6, v0, Landroid/graphics/Point;->y:I

    .line 902
    :cond_1
    iget v6, v1, Landroid/graphics/RectF;->right:F

    float-to-double v6, v6

    sub-double/2addr v6, v2

    iget v8, v0, Landroid/graphics/Point;->x:I

    int-to-double v8, v8

    sub-double/2addr v8, v12

    cmpg-double v14, v6, v8

    if-gez v14, :cond_2

    .line 903
    iget v6, v1, Landroid/graphics/RectF;->right:F

    float-to-double v6, v6

    sub-double/2addr v6, v2

    add-double/2addr v6, v12

    double-to-int v2, v6

    iput v2, v0, Landroid/graphics/Point;->x:I

    .line 906
    :cond_2
    iget v2, v1, Landroid/graphics/RectF;->bottom:F

    float-to-double v2, v2

    sub-double/2addr v2, v4

    iget v6, v0, Landroid/graphics/Point;->y:I

    int-to-double v6, v6

    sub-double/2addr v6, v10

    cmpg-double v8, v2, v6

    if-gez v8, :cond_3

    .line 907
    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    float-to-double v1, v1

    sub-double/2addr v1, v4

    add-double/2addr v1, v10

    double-to-int v1, v1

    iput v1, v0, Landroid/graphics/Point;->y:I

    :cond_3
    return-void
.end method

.method private adjustTrans(Landroid/graphics/Point;)V
    .locals 11
    .param p1    # Landroid/graphics/Point;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 861
    iget-object v0, p0, Lepson/print/phlayout/PhotoPreview;->lockObj:Ljava/lang/Object;

    monitor-enter v0

    .line 862
    :try_start_0
    iget-object v1, p0, Lepson/print/phlayout/PhotoPreview;->mLayoutPosition:Lepson/print/phlayout/ILayoutPosition;

    if-nez v1, :cond_0

    .line 863
    monitor-exit v0

    return-void

    .line 865
    :cond_0
    iget-object v1, p0, Lepson/print/phlayout/PhotoPreview;->mLayoutPosition:Lepson/print/phlayout/ILayoutPosition;

    invoke-interface {v1}, Lepson/print/phlayout/ILayoutPosition;->getPreviewImageCircumscribedTargetSize()Landroid/graphics/RectF;

    move-result-object v6

    .line 867
    invoke-direct {p0}, Lepson/print/phlayout/PhotoPreview;->getCurrentEpImage()Lepson/print/EPImage;

    move-result-object v1

    if-nez v1, :cond_1

    .line 870
    monitor-exit v0

    return-void

    .line 873
    :cond_1
    iget v2, v1, Lepson/print/EPImage;->previewWidth:I

    int-to-float v2, v2

    iget v3, p0, Lepson/print/phlayout/PhotoPreview;->scaleFactor:F

    mul-float v2, v2, v3

    float-to-double v7, v2

    .line 874
    iget v1, v1, Lepson/print/EPImage;->previewHeight:I

    int-to-float v1, v1

    iget v2, p0, Lepson/print/phlayout/PhotoPreview;->scaleFactor:F

    mul-float v1, v1, v2

    float-to-double v9, v1

    const-wide v4, 0x3fc999999999999aL    # 0.2

    move-object v2, p0

    move-object v3, p1

    .line 876
    invoke-direct/range {v2 .. v10}, Lepson/print/phlayout/PhotoPreview;->adjustPosition(Landroid/graphics/Point;DLandroid/graphics/RectF;DD)V

    .line 877
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method private calcPreviewImageRect(IIFLandroid/graphics/Point;)Landroid/graphics/RectF;
    .locals 4
    .param p4    # Landroid/graphics/Point;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .line 1152
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 1153
    iget v1, p4, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    int-to-float p1, p1

    mul-float p1, p1, p3

    const/high16 v2, 0x40000000    # 2.0f

    div-float v3, p1, v2

    sub-float/2addr v1, v3

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 1154
    iget p4, p4, Landroid/graphics/Point;->y:I

    int-to-float p4, p4

    int-to-float p2, p2

    mul-float p2, p2, p3

    div-float p3, p2, v2

    sub-float/2addr p4, p3

    iput p4, v0, Landroid/graphics/RectF;->top:F

    .line 1155
    iget p3, v0, Landroid/graphics/RectF;->left:F

    add-float/2addr p3, p1

    iput p3, v0, Landroid/graphics/RectF;->right:F

    .line 1156
    iget p1, v0, Landroid/graphics/RectF;->top:F

    add-float/2addr p1, p2

    iput p1, v0, Landroid/graphics/RectF;->bottom:F

    return-object v0
.end method

.method private changeFitMode()V
    .locals 2

    .line 201
    iget-object v0, p0, Lepson/print/phlayout/PhotoPreview;->mLayoutPosition:Lepson/print/phlayout/ILayoutPosition;

    invoke-interface {v0}, Lepson/print/phlayout/ILayoutPosition;->getLayoutId()I

    move-result v0

    const/4 v1, 0x1

    if-ne v1, v0, :cond_0

    .line 203
    invoke-direct {p0}, Lepson/print/phlayout/PhotoPreview;->fitCircumscribed()V

    goto :goto_0

    .line 205
    :cond_0
    iget v0, p0, Lepson/print/phlayout/PhotoPreview;->mFitMode:I

    if-ne v0, v1, :cond_1

    .line 207
    invoke-direct {p0}, Lepson/print/phlayout/PhotoPreview;->fitInscribed()V

    goto :goto_0

    .line 209
    :cond_1
    invoke-direct {p0}, Lepson/print/phlayout/PhotoPreview;->fitCircumscribed()V

    :goto_0
    return-void
.end method

.method private changeFitModeAndRefresh()V
    .locals 2

    const/4 v0, 0x1

    .line 185
    iput-boolean v0, p0, Lepson/print/phlayout/PhotoPreview;->maskExpandPreview:Z

    .line 187
    invoke-direct {p0}, Lepson/print/phlayout/PhotoPreview;->changeFitMode()V

    .line 188
    invoke-virtual {p0}, Lepson/print/phlayout/PhotoPreview;->invalidate()V

    .line 191
    iget-object v0, p0, Lepson/print/phlayout/PhotoPreview;->zoomControlHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    const/4 v1, 0x2

    .line 192
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    return-void
.end method

.method private changeScale(F)V
    .locals 2

    .line 164
    iget v0, p0, Lepson/print/phlayout/PhotoPreview;->baseScaleFactor:F

    mul-float p1, p1, v0

    iput p1, p0, Lepson/print/phlayout/PhotoPreview;->scaleFactor:F

    .line 167
    iget p1, p0, Lepson/print/phlayout/PhotoPreview;->scaleFactor:F

    const/high16 v0, 0x3e800000    # 0.25f

    cmpg-float v1, p1, v0

    if-gez v1, :cond_0

    .line 168
    iput v0, p0, Lepson/print/phlayout/PhotoPreview;->scaleFactor:F

    goto :goto_0

    :cond_0
    const/high16 v0, 0x41000000    # 8.0f

    cmpl-float p1, p1, v0

    if-lez p1, :cond_1

    .line 170
    iput v0, p0, Lepson/print/phlayout/PhotoPreview;->scaleFactor:F

    .line 174
    :cond_1
    :goto_0
    iget-object p1, p0, Lepson/print/phlayout/PhotoPreview;->zoomControlHandler:Landroid/os/Handler;

    if-eqz p1, :cond_2

    const/4 v0, 0x2

    .line 175
    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 178
    :cond_2
    iget-object p1, p0, Lepson/print/phlayout/PhotoPreview;->trans:Landroid/graphics/Point;

    invoke-direct {p0, p1}, Lepson/print/phlayout/PhotoPreview;->adjustTrans(Landroid/graphics/Point;)V

    .line 180
    invoke-virtual {p0}, Lepson/print/phlayout/PhotoPreview;->invalidate()V

    return-void
.end method

.method private static drawBorderedOutsidePrintingArea(Landroid/graphics/Canvas;Landroid/graphics/RectF;ILandroid/graphics/Paint;ZLepson/print/phlayout/ILayoutPosition;I[I)V
    .locals 16
    .param p0    # Landroid/graphics/Canvas;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1    # Landroid/graphics/RectF;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/Paint;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p5    # Lepson/print/phlayout/ILayoutPosition;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p7    # [I
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    move-object/from16 v0, p1

    move/from16 v1, p2

    move-object/from16 v8, p3

    move/from16 v9, p6

    .line 1027
    iget v2, v0, Landroid/graphics/RectF;->left:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_3

    iget v2, v0, Landroid/graphics/RectF;->top:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_3

    iget v2, v0, Landroid/graphics/RectF;->right:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_3

    iget v2, v0, Landroid/graphics/RectF;->bottom:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_3

    .line 1030
    invoke-virtual {v8, v9}, Landroid/graphics/Paint;->setColor(I)V

    const/16 v10, 0xdd

    if-nez p4, :cond_0

    .line 1032
    invoke-virtual {v8, v10}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1035
    :cond_0
    invoke-interface/range {p5 .. p5}, Lepson/print/phlayout/ILayoutPosition;->getLeftMargin()I

    move-result v2

    .line 1036
    invoke-interface/range {p5 .. p5}, Lepson/print/phlayout/ILayoutPosition;->getTopMargin()I

    move-result v11

    .line 1037
    invoke-interface/range {p5 .. p5}, Lepson/print/phlayout/ILayoutPosition;->getRightMargin()I

    move-result v12

    .line 1038
    invoke-interface/range {p5 .. p5}, Lepson/print/phlayout/ILayoutPosition;->getBottomMargin()I

    move-result v13

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 1041
    iget v5, v0, Landroid/graphics/RectF;->left:F

    int-to-float v14, v2

    sub-float/2addr v5, v14

    const/4 v15, 0x1

    aget v2, p7, v15

    int-to-float v6, v2

    move-object/from16 v2, p0

    move-object/from16 v7, p3

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1044
    iget v2, v0, Landroid/graphics/RectF;->left:F

    sub-float v3, v2, v14

    iget v2, v0, Landroid/graphics/RectF;->right:F

    int-to-float v12, v12

    add-float v5, v2, v12

    iget v2, v0, Landroid/graphics/RectF;->top:F

    int-to-float v11, v11

    sub-float v6, v2, v11

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1048
    iget v2, v0, Landroid/graphics/RectF;->right:F

    add-float v3, v2, v12

    const/4 v2, 0x0

    aget v2, p7, v2

    int-to-float v5, v2

    aget v2, p7, v15

    int-to-float v6, v2

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1051
    iget v2, v0, Landroid/graphics/RectF;->left:F

    sub-float v3, v2, v14

    iget v2, v0, Landroid/graphics/RectF;->bottom:F

    int-to-float v13, v13

    add-float v4, v2, v13

    iget v2, v0, Landroid/graphics/RectF;->right:F

    add-float v5, v2, v12

    aget v2, p7, v15

    int-to-float v6, v2

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    const/4 v1, -0x1

    .line 1057
    invoke-virtual {v8, v1}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_0

    :cond_1
    if-ne v1, v15, :cond_2

    .line 1060
    invoke-virtual {v8, v9}, Landroid/graphics/Paint;->setColor(I)V

    if-nez p4, :cond_2

    .line 1062
    invoke-virtual {v8, v10}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1068
    :cond_2
    :goto_0
    iget v1, v0, Landroid/graphics/RectF;->left:F

    sub-float v2, v1, v14

    iget v1, v0, Landroid/graphics/RectF;->top:F

    sub-float v3, v1, v11

    iget v4, v0, Landroid/graphics/RectF;->left:F

    iget v1, v0, Landroid/graphics/RectF;->bottom:F

    add-float v5, v1, v13

    move-object/from16 v1, p0

    move-object/from16 v6, p3

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1071
    iget v2, v0, Landroid/graphics/RectF;->left:F

    iget v1, v0, Landroid/graphics/RectF;->top:F

    sub-float v3, v1, v11

    iget v4, v0, Landroid/graphics/RectF;->right:F

    iget v5, v0, Landroid/graphics/RectF;->top:F

    move-object/from16 v1, p0

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1074
    iget v2, v0, Landroid/graphics/RectF;->right:F

    iget v1, v0, Landroid/graphics/RectF;->top:F

    sub-float v3, v1, v11

    iget v1, v0, Landroid/graphics/RectF;->right:F

    add-float v4, v1, v12

    iget v1, v0, Landroid/graphics/RectF;->bottom:F

    add-float v5, v1, v13

    move-object/from16 v1, p0

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1078
    iget v1, v0, Landroid/graphics/RectF;->left:F

    iget v2, v0, Landroid/graphics/RectF;->bottom:F

    iget v3, v0, Landroid/graphics/RectF;->right:F

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    add-float v4, v0, v13

    move-object/from16 v0, p0

    move-object/from16 v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :cond_3
    return-void
.end method

.method private drawCdPreviewAll(Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/Bitmap;Lepson/print/phlayout/ILayoutPosition;Landroid/graphics/Rect;ZLandroid/graphics/Path;ILandroid/graphics/RectF;)V
    .locals 3
    .param p1    # Landroid/graphics/Canvas;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/graphics/Paint;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/Bitmap;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lepson/print/phlayout/ILayoutPosition;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p5    # Landroid/graphics/Rect;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p7    # Landroid/graphics/Path;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p9    # Landroid/graphics/RectF;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    if-eqz p3, :cond_0

    .line 1094
    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {p5, v2, v2, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    const/4 v0, 0x0

    .line 1095
    invoke-virtual {p1, p3, p5, p9, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 1099
    :cond_0
    invoke-interface {p4}, Lepson/print/phlayout/ILayoutPosition;->getPreviewImageCircumscribedTargetSize()Landroid/graphics/RectF;

    move-result-object p3

    .line 1101
    invoke-virtual {p2, p8}, Landroid/graphics/Paint;->setColor(I)V

    if-nez p6, :cond_1

    const/16 p4, 0xdd

    .line 1103
    invoke-virtual {p2, p4}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1108
    :cond_1
    invoke-virtual {p3}, Landroid/graphics/RectF;->height()F

    move-result p4

    invoke-virtual {p3}, Landroid/graphics/RectF;->width()F

    move-result p5

    const/high16 p6, 0x40000000    # 2.0f

    cmpl-float p4, p4, p5

    if-lez p4, :cond_2

    .line 1109
    invoke-virtual {p3}, Landroid/graphics/RectF;->width()F

    move-result p4

    div-float/2addr p4, p6

    goto :goto_0

    .line 1111
    :cond_2
    invoke-virtual {p3}, Landroid/graphics/RectF;->height()F

    move-result p4

    div-float/2addr p4, p6

    :goto_0
    const/high16 p5, 0x422c0000    # 43.0f

    mul-float p5, p5, p4

    const/high16 p6, 0x42e80000    # 116.0f

    div-float/2addr p5, p6

    .line 1115
    invoke-virtual {p7}, Landroid/graphics/Path;->reset()V

    .line 1117
    invoke-virtual {p3}, Landroid/graphics/RectF;->centerX()F

    move-result p6

    invoke-virtual {p3}, Landroid/graphics/RectF;->centerY()F

    move-result p8

    invoke-virtual {p1, p6, p8, p5, p2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1121
    invoke-virtual {p3}, Landroid/graphics/RectF;->centerX()F

    move-result p5

    invoke-virtual {p3}, Landroid/graphics/RectF;->centerY()F

    move-result p3

    sget-object p6, Landroid/graphics/Path$Direction;->CCW:Landroid/graphics/Path$Direction;

    invoke-virtual {p7, p5, p3, p4, p6}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 1123
    sget-object p3, Landroid/graphics/Region$Op;->DIFFERENCE:Landroid/graphics/Region$Op;

    invoke-virtual {p1, p7, p3}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;Landroid/graphics/Region$Op;)Z

    .line 1125
    invoke-virtual {p1, p2}, Landroid/graphics/Canvas;->drawPaint(Landroid/graphics/Paint;)V

    return-void
.end method

.method private drawPreview(Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/Bitmap;Lepson/print/phlayout/ILayoutPosition;Landroid/graphics/Rect;ZI[ILandroid/graphics/RectF;)V
    .locals 9
    .param p1    # Landroid/graphics/Canvas;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/graphics/Paint;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/Bitmap;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lepson/print/phlayout/ILayoutPosition;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p5    # Landroid/graphics/Rect;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    move-object v0, p1

    move-object v3, p2

    move-object v1, p3

    move-object v2, p5

    .line 987
    invoke-interface {p4}, Lepson/print/phlayout/ILayoutPosition;->getLayoutId()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    .line 997
    invoke-interface {p4}, Lepson/print/phlayout/ILayoutPosition;->getPreviewImageCircumscribedTargetSize()Landroid/graphics/RectF;

    move-result-object v5

    move-object v6, v5

    goto :goto_0

    .line 1000
    :cond_0
    invoke-interface {p4}, Lepson/print/phlayout/ILayoutPosition;->getPreviewPaperRect()Landroid/graphics/Rect;

    move-result-object v5

    .line 1001
    new-instance v6, Landroid/graphics/RectF;

    invoke-direct {v6, v5}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    :goto_0
    const/4 v5, -0x1

    .line 1003
    invoke-virtual {p2, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 1004
    invoke-virtual {p1, v6, p2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    if-eqz v1, :cond_1

    .line 1008
    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    const/4 v8, 0x0

    invoke-virtual {p5, v8, v8, v5, v7}, Landroid/graphics/Rect;->set(IIII)V

    const/4 v5, 0x0

    move-object/from16 v7, p9

    .line 1009
    invoke-virtual {p1, p3, p5, v7, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    :cond_1
    move-object v0, p1

    move-object v1, v6

    move v2, v4

    move-object v3, p2

    move v4, p6

    move-object v5, p4

    move/from16 v6, p7

    move-object/from16 v7, p8

    .line 1013
    invoke-static/range {v0 .. v7}, Lepson/print/phlayout/PhotoPreview;->drawBorderedOutsidePrintingArea(Landroid/graphics/Canvas;Landroid/graphics/RectF;ILandroid/graphics/Paint;ZLepson/print/phlayout/ILayoutPosition;I[I)V

    return-void
.end method

.method private fitCircumscribed()V
    .locals 1
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    const/high16 v0, 0x3f800000    # 1.0f

    .line 221
    iput v0, p0, Lepson/print/phlayout/PhotoPreview;->scaleFactor:F

    .line 222
    iput v0, p0, Lepson/print/phlayout/PhotoPreview;->baseScaleFactor:F

    .line 225
    iget v0, p0, Lepson/print/phlayout/PhotoPreview;->mPaperSizeId:I

    invoke-virtual {p0, v0}, Lepson/print/phlayout/PhotoPreview;->setPaper(I)Z

    const/4 v0, 0x1

    .line 226
    iput v0, p0, Lepson/print/phlayout/PhotoPreview;->mFitMode:I

    return-void
.end method

.method private fitInscribed()V
    .locals 11

    const/4 v0, 0x2

    .line 235
    iput v0, p0, Lepson/print/phlayout/PhotoPreview;->mFitMode:I

    .line 237
    iget-object v1, p0, Lepson/print/phlayout/PhotoPreview;->mLayoutPosition:Lepson/print/phlayout/ILayoutPosition;

    if-nez v1, :cond_0

    return-void

    .line 241
    :cond_0
    invoke-direct {p0}, Lepson/print/phlayout/PhotoPreview;->getCurrentEpImage()Lepson/print/EPImage;

    move-result-object v1

    if-nez v1, :cond_1

    return-void

    .line 247
    :cond_1
    iget-object v2, p0, Lepson/print/phlayout/PhotoPreview;->mLayoutPosition:Lepson/print/phlayout/ILayoutPosition;

    invoke-interface {v2}, Lepson/print/phlayout/ILayoutPosition;->getPreviewImageCircumscribedTargetSize()Landroid/graphics/RectF;

    move-result-object v2

    .line 248
    iget-object v3, p0, Lepson/print/phlayout/PhotoPreview;->mFitLayout:Lepson/print/phlayout/FitLayout;

    const/4 v4, 0x4

    new-array v4, v4, [D

    iget v5, v2, Landroid/graphics/RectF;->left:F

    float-to-double v5, v5

    const/4 v7, 0x0

    aput-wide v5, v4, v7

    iget v5, v2, Landroid/graphics/RectF;->top:F

    float-to-double v5, v5

    const/4 v8, 0x1

    aput-wide v5, v4, v8

    iget v5, v2, Landroid/graphics/RectF;->right:F

    float-to-double v5, v5

    aput-wide v5, v4, v0

    const/4 v5, 0x3

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    float-to-double v9, v2

    aput-wide v9, v4, v5

    new-array v2, v0, [D

    iget v5, v1, Lepson/print/EPImage;->previewWidth:I

    int-to-double v5, v5

    aput-wide v5, v2, v7

    iget v1, v1, Lepson/print/EPImage;->previewHeight:I

    int-to-double v5, v1

    aput-wide v5, v2, v8

    invoke-virtual {v3, v4, v2}, Lepson/print/phlayout/FitLayout;->fitInscribed([D[D)[D

    move-result-object v1

    if-nez v1, :cond_2

    return-void

    .line 256
    :cond_2
    iget-object v2, p0, Lepson/print/phlayout/PhotoPreview;->trans:Landroid/graphics/Point;

    aget-wide v3, v1, v7

    double-to-int v3, v3

    iput v3, v2, Landroid/graphics/Point;->x:I

    .line 257
    aget-wide v3, v1, v8

    double-to-int v3, v3

    iput v3, v2, Landroid/graphics/Point;->y:I

    .line 258
    aget-wide v0, v1, v0

    double-to-float v0, v0

    iput v0, p0, Lepson/print/phlayout/PhotoPreview;->scaleFactor:F

    return-void
.end method

.method private static getBitmap(Lepson/print/EPImage;IIIZ)Landroid/graphics/Bitmap;
    .locals 6
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .line 305
    new-instance v0, Lepson/print/EPImageCreator;

    invoke-static {}, Lepson/print/IprintApplication;->getInstance()Lepson/print/IprintApplication;

    move-result-object v1

    invoke-direct {v0, v1}, Lepson/print/EPImageCreator;-><init>(Landroid/content/Context;)V

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    .line 307
    invoke-virtual/range {v0 .. v5}, Lepson/print/EPImageCreator;->createPreviewImage(Lepson/print/EPImage;IIIZ)Ljava/lang/String;

    .line 308
    iget-object p1, p0, Lepson/print/EPImage;->previewImageFileName:Ljava/lang/String;

    if-eqz p1, :cond_0

    .line 309
    iget-object p0, p0, Lepson/print/EPImage;->previewImageFileName:Ljava/lang/String;

    invoke-static {p0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object p0

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method private getCurrentElement()Lepson/print/phlayout/PhotoPreviewImageList$Element;
    .locals 2

    .line 272
    iget-object v0, p0, Lepson/print/phlayout/PhotoPreview;->imageList:Lepson/print/phlayout/PhotoPreviewImageList;

    iget v1, p0, Lepson/print/phlayout/PhotoPreview;->imageNo:I

    invoke-virtual {v0, v1}, Lepson/print/phlayout/PhotoPreviewImageList;->getElement(I)Lepson/print/phlayout/PhotoPreviewImageList$Element;

    move-result-object v0

    return-object v0
.end method

.method private getCurrentEpImage()Lepson/print/EPImage;
    .locals 2
    .annotation build Landroid/support/annotation/AnyThread;
    .end annotation

    .line 264
    iget v0, p0, Lepson/print/phlayout/PhotoPreview;->imageNo:I

    if-ltz v0, :cond_1

    iget v0, p0, Lepson/print/phlayout/PhotoPreview;->imageNo:I

    iget-object v1, p0, Lepson/print/phlayout/PhotoPreview;->imageList:Lepson/print/phlayout/PhotoPreviewImageList;

    invoke-virtual {v1}, Lepson/print/phlayout/PhotoPreviewImageList;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    goto :goto_0

    .line 267
    :cond_0
    iget-object v0, p0, Lepson/print/phlayout/PhotoPreview;->imageList:Lepson/print/phlayout/PhotoPreviewImageList;

    iget v1, p0, Lepson/print/phlayout/PhotoPreview;->imageNo:I

    invoke-virtual {v0, v1}, Lepson/print/phlayout/PhotoPreviewImageList;->get(I)Lepson/print/EPImage;

    move-result-object v0

    return-object v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return-object v0
.end method

.method private declared-synchronized getViewSize()[I
    .locals 2
    .annotation build Landroid/support/annotation/AnyThread;
    .end annotation

    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    monitor-enter p0

    .line 474
    :try_start_0
    iget-object v0, p0, Lepson/print/phlayout/PhotoPreview;->mViewSize:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    if-lez v0, :cond_1

    iget-object v0, p0, Lepson/print/phlayout/PhotoPreview;->mViewSize:[I

    const/4 v1, 0x1

    aget v0, v0, v1

    if-gtz v0, :cond_0

    goto :goto_0

    .line 477
    :cond_0
    iget-object v0, p0, Lepson/print/phlayout/PhotoPreview;->mViewSize:[I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    .line 475
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private initLocal(Landroid/content/Context;)V
    .locals 2

    .line 147
    new-instance v0, Landroid/support/v4/view/GestureDetectorCompat;

    new-instance v1, Lepson/print/phlayout/PhotoPreview$MyGestureListener;

    invoke-direct {v1, p0}, Lepson/print/phlayout/PhotoPreview$MyGestureListener;-><init>(Lepson/print/phlayout/PhotoPreview;)V

    invoke-direct {v0, p1, v1}, Landroid/support/v4/view/GestureDetectorCompat;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lepson/print/phlayout/PhotoPreview;->mGestureDetector:Landroid/support/v4/view/GestureDetectorCompat;

    .line 148
    new-instance v0, Landroid/view/ScaleGestureDetector;

    new-instance v1, Lepson/print/phlayout/PhotoPreview$MyScaledGestureListener;

    invoke-direct {v1, p0}, Lepson/print/phlayout/PhotoPreview$MyScaledGestureListener;-><init>(Lepson/print/phlayout/PhotoPreview;)V

    invoke-direct {v0, p1, v1}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lepson/print/phlayout/PhotoPreview;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    const/4 p1, 0x1

    .line 150
    iput p1, p0, Lepson/print/phlayout/PhotoPreview;->mFitMode:I

    return-void
.end method

.method private localGetPreviewBitmap(Lepson/print/EPImage;IIIZZ)Landroid/graphics/Bitmap;
    .locals 7
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .line 743
    iget-object v0, p0, Lepson/print/phlayout/PhotoPreview;->mPreviewBitmapMaker:Lepson/print/phlayout/PhotoPreview$PreviewBitmapMaker;

    if-eqz v0, :cond_0

    if-eqz p6, :cond_0

    .line 745
    iget-object v1, p0, Lepson/print/phlayout/PhotoPreview;->mPreviewBitmapMaker:Lepson/print/phlayout/PhotoPreview$PreviewBitmapMaker;

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-interface/range {v1 .. v6}, Lepson/print/phlayout/PhotoPreview$PreviewBitmapMaker;->getPreviewBitmap(Lepson/print/EPImage;IIIZ)Landroid/graphics/Bitmap;

    move-result-object p6

    iput-object p6, p0, Lepson/print/phlayout/PhotoPreview;->mBitmap:Landroid/graphics/Bitmap;

    .line 747
    iget-object p6, p0, Lepson/print/phlayout/PhotoPreview;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz p6, :cond_0

    return-object p6

    .line 753
    :cond_0
    invoke-static {p1, p2, p3, p4, p5}, Lepson/print/phlayout/PhotoPreview;->getBitmap(Lepson/print/EPImage;IIIZ)Landroid/graphics/Bitmap;

    move-result-object p2

    iput-object p2, p0, Lepson/print/phlayout/PhotoPreview;->mBitmap:Landroid/graphics/Bitmap;

    .line 754
    iget-object p2, p0, Lepson/print/phlayout/PhotoPreview;->mBitmap:Landroid/graphics/Bitmap;

    if-nez p2, :cond_1

    const/4 p1, 0x0

    return-object p1

    :cond_1
    const-string p2, "previewView"

    .line 757
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "bitmap <"

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p4, p0, Lepson/print/phlayout/PhotoPreview;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result p4

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p4, ","

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p4, p0, Lepson/print/phlayout/PhotoPreview;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result p4

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p4, "> preview WxH <"

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p4, p1, Lepson/print/EPImage;->previewWidth:I

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p4, ","

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p1, p1, Lepson/print/EPImage;->previewHeight:I

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ">"

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p2, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 760
    iget-object p1, p0, Lepson/print/phlayout/PhotoPreview;->mBitmap:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method private onMoveEventEnd()V
    .locals 0

    .line 834
    invoke-virtual {p0}, Lepson/print/phlayout/PhotoPreview;->invalidate()V

    return-void
.end method

.method private onMoveEventStarted(FF)V
    .locals 0

    const/4 p1, 0x0

    .line 838
    iput-boolean p1, p0, Lepson/print/phlayout/PhotoPreview;->maskExpandPreview:Z

    .line 840
    invoke-virtual {p0}, Lepson/print/phlayout/PhotoPreview;->invalidate()V

    return-void
.end method

.method private onMoving(FF)V
    .locals 2

    const/4 v0, 0x0

    .line 844
    iput v0, p0, Lepson/print/phlayout/PhotoPreview;->mFitMode:I

    .line 846
    iget-object v0, p0, Lepson/print/phlayout/PhotoPreview;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 847
    iget-object v0, p0, Lepson/print/phlayout/PhotoPreview;->trans:Landroid/graphics/Point;

    neg-float p1, p1

    iget v1, v0, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    add-float/2addr p1, v1

    float-to-int p1, p1

    iput p1, v0, Landroid/graphics/Point;->x:I

    .line 848
    iget-object p1, p0, Lepson/print/phlayout/PhotoPreview;->trans:Landroid/graphics/Point;

    neg-float p2, p2

    iget v0, p1, Landroid/graphics/Point;->y:I

    int-to-float v0, v0

    add-float/2addr p2, v0

    float-to-int p2, p2

    iput p2, p1, Landroid/graphics/Point;->y:I

    .line 850
    iget-object p1, p0, Lepson/print/phlayout/PhotoPreview;->trans:Landroid/graphics/Point;

    invoke-direct {p0, p1}, Lepson/print/phlayout/PhotoPreview;->adjustTrans(Landroid/graphics/Point;)V

    .line 852
    invoke-virtual {p0}, Lepson/print/phlayout/PhotoPreview;->invalidate()V

    :cond_0
    return-void
.end method

.method private reCalcPaperPosition()V
    .locals 6
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .line 487
    invoke-direct {p0}, Lepson/print/phlayout/PhotoPreview;->getViewSize()[I

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 492
    :cond_0
    invoke-static {}, Lepson/print/IprintApplication;->getInstance()Lepson/print/IprintApplication;

    move-result-object v1

    iget v2, p0, Lepson/print/phlayout/PhotoPreview;->mPaperSizeId:I

    invoke-static {v1, v2}, Lepson/common/Info_paper;->getInfoPaper(Landroid/content/Context;I)Lepson/common/Info_paper;

    move-result-object v1

    if-nez v1, :cond_1

    return-void

    .line 497
    :cond_1
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 498
    iget-object v3, p0, Lepson/print/phlayout/PhotoPreview;->mLayoutPosition:Lepson/print/phlayout/ILayoutPosition;

    const/4 v4, 0x0

    aget v4, v0, v4

    const/4 v5, 0x1

    aget v0, v0, v5

    invoke-interface {v3, v1, v4, v0, v2}, Lepson/print/phlayout/ILayoutPosition;->setPaperAndCalcPreviewPosition(Lepson/common/Info_paper;IILandroid/graphics/Point;)V

    return-void
.end method

.method private restoreImage(Lepson/print/phlayout/PhotoPreviewImageList$Element;)V
    .locals 3
    .param p1    # Lepson/print/phlayout/PhotoPreviewImageList$Element;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .line 811
    invoke-virtual {p1}, Lepson/print/phlayout/PhotoPreviewImageList$Element;->getEPImage()Lepson/print/EPImage;

    move-result-object v0

    .line 812
    iget-object v1, p0, Lepson/print/phlayout/PhotoPreview;->trans:Landroid/graphics/Point;

    iget v2, v0, Lepson/print/EPImage;->previewImageRectCenterX:I

    iput v2, v1, Landroid/graphics/Point;->x:I

    .line 813
    iget-object v1, p0, Lepson/print/phlayout/PhotoPreview;->trans:Landroid/graphics/Point;

    iget v2, v0, Lepson/print/EPImage;->previewImageRectCenterY:I

    iput v2, v1, Landroid/graphics/Point;->y:I

    .line 815
    iget-object v1, p0, Lepson/print/phlayout/PhotoPreview;->mLayoutPosition:Lepson/print/phlayout/ILayoutPosition;

    invoke-interface {v1, v0}, Lepson/print/phlayout/ILayoutPosition;->copyPreviewPrintAreaFromEpImage(Lepson/print/EPImage;)V

    .line 817
    iget v0, v0, Lepson/print/EPImage;->scaleFactor:F

    iput v0, p0, Lepson/print/phlayout/PhotoPreview;->scaleFactor:F

    .line 819
    invoke-virtual {p1}, Lepson/print/phlayout/PhotoPreviewImageList$Element;->getFitMode()I

    move-result p1

    iput p1, p0, Lepson/print/phlayout/PhotoPreview;->mFitMode:I

    return-void
.end method

.method private saveImage(I)Z
    .locals 5
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    const/4 v0, 0x0

    .line 773
    :try_start_0
    iget-object v1, p0, Lepson/print/phlayout/PhotoPreview;->imageList:Lepson/print/phlayout/PhotoPreviewImageList;

    invoke-virtual {v1, p1}, Lepson/print/phlayout/PhotoPreviewImageList;->getElement(I)Lepson/print/phlayout/PhotoPreviewImageList$Element;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 781
    invoke-virtual {v1}, Lepson/print/phlayout/PhotoPreviewImageList$Element;->getEPImage()Lepson/print/EPImage;

    move-result-object v2

    if-nez v2, :cond_0

    return v0

    .line 786
    :cond_0
    iget-object v0, p0, Lepson/print/phlayout/PhotoPreview;->trans:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iput v0, v2, Lepson/print/EPImage;->previewImageRectCenterX:I

    .line 787
    iget-object v0, p0, Lepson/print/phlayout/PhotoPreview;->trans:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    iput v0, v2, Lepson/print/EPImage;->previewImageRectCenterY:I

    .line 789
    iget-object v0, p0, Lepson/print/phlayout/PhotoPreview;->mLayoutPosition:Lepson/print/phlayout/ILayoutPosition;

    invoke-interface {v0, v2}, Lepson/print/phlayout/ILayoutPosition;->copyPreviewPrintAreaToEpImage(Lepson/print/EPImage;)V

    .line 791
    iget-object v0, p0, Lepson/print/phlayout/PhotoPreview;->mBitmap:Landroid/graphics/Bitmap;

    iget v3, p0, Lepson/print/phlayout/PhotoPreview;->scaleFactor:F

    iget-object v4, p0, Lepson/print/phlayout/PhotoPreview;->trans:Landroid/graphics/Point;

    invoke-virtual {p0, v0, v3, v4}, Lepson/print/phlayout/PhotoPreview;->calcPreviewImageRect(Landroid/graphics/Bitmap;FLandroid/graphics/Point;)Landroid/graphics/RectF;

    move-result-object v0

    .line 792
    iget v3, v0, Landroid/graphics/RectF;->left:F

    iput v3, v2, Lepson/print/EPImage;->previewImageRectLeft:F

    .line 793
    iget v3, v0, Landroid/graphics/RectF;->top:F

    iput v3, v2, Lepson/print/EPImage;->previewImageRectTop:F

    .line 794
    iget v3, v0, Landroid/graphics/RectF;->right:F

    iput v3, v2, Lepson/print/EPImage;->previewImageRectRight:F

    .line 795
    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    iput v0, v2, Lepson/print/EPImage;->previewImageRectBottom:F

    .line 797
    iget v0, p0, Lepson/print/phlayout/PhotoPreview;->scaleFactor:F

    iput v0, v2, Lepson/print/EPImage;->scaleFactor:F

    .line 798
    iget-object v0, p0, Lepson/print/phlayout/PhotoPreview;->imageList:Lepson/print/phlayout/PhotoPreviewImageList;

    invoke-virtual {v0, p1, v2}, Lepson/print/phlayout/PhotoPreviewImageList;->set(ILepson/print/EPImage;)V

    .line 800
    iget p1, p0, Lepson/print/phlayout/PhotoPreview;->mFitMode:I

    invoke-virtual {v1, p1}, Lepson/print/phlayout/PhotoPreviewImageList$Element;->setFitMode(I)V

    const/4 p1, 0x1

    return p1

    :catch_0
    return v0
.end method

.method private setImageCore(II)V
    .locals 2
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    if-ltz p1, :cond_4

    .line 659
    iget-object v0, p0, Lepson/print/phlayout/PhotoPreview;->imageList:Lepson/print/phlayout/PhotoPreviewImageList;

    invoke-virtual {v0}, Lepson/print/phlayout/PhotoPreviewImageList;->size()I

    move-result v0

    if-ge p1, v0, :cond_4

    .line 666
    iget v0, p0, Lepson/print/phlayout/PhotoPreview;->imageNo:I

    if-ltz v0, :cond_1

    iget v0, p0, Lepson/print/phlayout/PhotoPreview;->imageNo:I

    iget-object v1, p0, Lepson/print/phlayout/PhotoPreview;->imageList:Lepson/print/phlayout/PhotoPreviewImageList;

    invoke-virtual {v1}, Lepson/print/phlayout/PhotoPreviewImageList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 667
    iget v0, p0, Lepson/print/phlayout/PhotoPreview;->imageNo:I

    invoke-direct {p0, v0}, Lepson/print/phlayout/PhotoPreview;->saveImage(I)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 668
    :cond_0
    new-instance p1, Ljava/lang/Exception;

    invoke-direct {p1}, Ljava/lang/Exception;-><init>()V

    throw p1

    .line 672
    :cond_1
    :goto_0
    iput p1, p0, Lepson/print/phlayout/PhotoPreview;->imageNo:I

    .line 673
    invoke-direct {p0}, Lepson/print/phlayout/PhotoPreview;->getCurrentElement()Lepson/print/phlayout/PhotoPreviewImageList$Element;

    move-result-object p1

    .line 674
    invoke-direct {p0}, Lepson/print/phlayout/PhotoPreview;->getCurrentEpImage()Lepson/print/EPImage;

    move-result-object v0

    if-nez p2, :cond_2

    .line 676
    invoke-direct {p0, p1}, Lepson/print/phlayout/PhotoPreview;->setImageCore_RestorePreview(Lepson/print/phlayout/PhotoPreviewImageList$Element;)V

    goto :goto_2

    :cond_2
    const/4 p1, 0x2

    if-ne p2, p1, :cond_3

    const/4 p1, 0x1

    goto :goto_1

    :cond_3
    const/4 p1, 0x0

    .line 678
    :goto_1
    invoke-direct {p0, v0, p1}, Lepson/print/phlayout/PhotoPreview;->setImageCore2(Lepson/print/EPImage;Z)V

    :goto_2
    return-void

    .line 660
    :cond_4
    new-instance p1, Ljava/lang/Exception;

    invoke-direct {p1}, Ljava/lang/Exception;-><init>()V

    throw p1
.end method

.method private setImageCore2(Lepson/print/EPImage;Z)V
    .locals 10
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v0, 0x1

    if-eqz p2, :cond_1

    .line 687
    iget-object v1, p0, Lepson/print/phlayout/PhotoPreview;->mLayoutPosition:Lepson/print/phlayout/ILayoutPosition;

    iget v2, p1, Lepson/print/EPImage;->srcWidth:I

    iget v3, p1, Lepson/print/EPImage;->srcHeight:I

    if-le v2, v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    invoke-interface {v1, v2}, Lepson/print/phlayout/ILayoutPosition;->setPaperLandscape(Z)V

    .line 690
    invoke-direct {p0}, Lepson/print/phlayout/PhotoPreview;->fitCircumscribed()V

    goto :goto_1

    .line 693
    :cond_1
    iget v1, p0, Lepson/print/phlayout/PhotoPreview;->mPaperSizeId:I

    invoke-virtual {p0, v1}, Lepson/print/phlayout/PhotoPreview;->setPaper(I)Z

    .line 697
    :goto_1
    iget-object v1, p0, Lepson/print/phlayout/PhotoPreview;->mLayoutPosition:Lepson/print/phlayout/ILayoutPosition;

    invoke-interface {v1}, Lepson/print/phlayout/ILayoutPosition;->isPreviewImageSizeValid()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 706
    iget-object v1, p0, Lepson/print/phlayout/PhotoPreview;->mLayoutPosition:Lepson/print/phlayout/ILayoutPosition;

    invoke-interface {v1}, Lepson/print/phlayout/ILayoutPosition;->getPreviewImageCircumscribedTargetSize()Landroid/graphics/RectF;

    move-result-object v1

    .line 707
    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v2

    float-to-int v5, v2

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    float-to-int v6, v1

    iget v7, p0, Lepson/print/phlayout/PhotoPreview;->color:I

    const/4 v8, 0x1

    xor-int/lit8 v9, p2, 0x1

    move-object v3, p0

    move-object v4, p1

    invoke-direct/range {v3 .. v9}, Lepson/print/phlayout/PhotoPreview;->localGetPreviewBitmap(Lepson/print/EPImage;IIIZZ)Landroid/graphics/Bitmap;

    move-result-object p1

    iput-object p1, p0, Lepson/print/phlayout/PhotoPreview;->mBitmap:Landroid/graphics/Bitmap;

    return-void

    .line 701
    :cond_2
    new-instance p1, Ljava/lang/Exception;

    invoke-direct {p1}, Ljava/lang/Exception;-><init>()V

    throw p1
.end method

.method private setImageCore_RestorePreview(Lepson/print/phlayout/PhotoPreviewImageList$Element;)V
    .locals 8
    .param p1    # Lepson/print/phlayout/PhotoPreviewImageList$Element;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 716
    invoke-direct {p0, p1}, Lepson/print/phlayout/PhotoPreview;->restoreImage(Lepson/print/phlayout/PhotoPreviewImageList$Element;)V

    .line 719
    invoke-direct {p0}, Lepson/print/phlayout/PhotoPreview;->reCalcPaperPosition()V

    .line 722
    iget-object v0, p0, Lepson/print/phlayout/PhotoPreview;->mLayoutPosition:Lepson/print/phlayout/ILayoutPosition;

    invoke-interface {v0}, Lepson/print/phlayout/ILayoutPosition;->isPreviewImageSizeValid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 731
    iget-object v0, p0, Lepson/print/phlayout/PhotoPreview;->mLayoutPosition:Lepson/print/phlayout/ILayoutPosition;

    invoke-interface {v0}, Lepson/print/phlayout/ILayoutPosition;->getPreviewImageCircumscribedTargetSize()Landroid/graphics/RectF;

    move-result-object v0

    .line 732
    invoke-virtual {p1}, Lepson/print/phlayout/PhotoPreviewImageList$Element;->getEPImage()Lepson/print/EPImage;

    move-result-object v2

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result p1

    float-to-int v3, p1

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result p1

    float-to-int v4, p1

    iget v5, p0, Lepson/print/phlayout/PhotoPreview;->color:I

    const/4 v6, 0x0

    const/4 v7, 0x1

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lepson/print/phlayout/PhotoPreview;->localGetPreviewBitmap(Lepson/print/EPImage;IIIZZ)Landroid/graphics/Bitmap;

    move-result-object p1

    iput-object p1, p0, Lepson/print/phlayout/PhotoPreview;->mBitmap:Landroid/graphics/Bitmap;

    return-void

    .line 726
    :cond_0
    new-instance p1, Ljava/lang/Exception;

    invoke-direct {p1}, Ljava/lang/Exception;-><init>()V

    throw p1
.end method

.method private startScaleChange()V
    .locals 2

    const/4 v0, 0x0

    .line 154
    iput-boolean v0, p0, Lepson/print/phlayout/PhotoPreview;->maskExpandPreview:Z

    .line 157
    iget v1, p0, Lepson/print/phlayout/PhotoPreview;->scaleFactor:F

    iput v1, p0, Lepson/print/phlayout/PhotoPreview;->baseScaleFactor:F

    .line 158
    iput v0, p0, Lepson/print/phlayout/PhotoPreview;->mFitMode:I

    .line 160
    invoke-virtual {p0}, Lepson/print/phlayout/PhotoPreview;->invalidate()V

    return-void
.end method


# virtual methods
.method public calcPreviewImageRect(Landroid/graphics/Bitmap;FLandroid/graphics/Point;)Landroid/graphics/RectF;
    .locals 1
    .param p1    # Landroid/graphics/Bitmap;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/Point;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1141
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    .line 1142
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result p1

    .line 1144
    invoke-direct {p0, v0, p1, p2, p3}, Lepson/print/phlayout/PhotoPreview;->calcPreviewImageRect(IIFLandroid/graphics/Point;)Landroid/graphics/RectF;

    move-result-object p1

    return-object p1
.end method

.method public getImageList()Lepson/print/EPImageList;
    .locals 8

    .line 334
    iget-object v0, p0, Lepson/print/phlayout/PhotoPreview;->lockObj:Ljava/lang/Object;

    monitor-enter v0

    .line 336
    :try_start_0
    iget v1, p0, Lepson/print/phlayout/PhotoPreview;->imageNo:I

    invoke-direct {p0, v1}, Lepson/print/phlayout/PhotoPreview;->saveImage(I)Z

    .line 339
    new-instance v1, Lepson/print/EPImageList;

    invoke-direct {v1}, Lepson/print/EPImageList;-><init>()V

    const/4 v2, 0x1

    .line 340
    invoke-virtual {v1, v2}, Lepson/print/EPImageList;->setRenderingMode(I)V

    const/4 v2, 0x0

    .line 342
    :goto_0
    iget-object v3, p0, Lepson/print/phlayout/PhotoPreview;->imageList:Lepson/print/phlayout/PhotoPreviewImageList;

    invoke-virtual {v3}, Lepson/print/phlayout/PhotoPreviewImageList;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 343
    iget-object v3, p0, Lepson/print/phlayout/PhotoPreview;->imageList:Lepson/print/phlayout/PhotoPreviewImageList;

    invoke-virtual {v3, v2}, Lepson/print/phlayout/PhotoPreviewImageList;->get(I)Lepson/print/EPImage;

    move-result-object v3

    if-nez v3, :cond_0

    goto :goto_1

    .line 349
    :cond_0
    new-instance v4, Landroid/graphics/Point;

    iget v5, v3, Lepson/print/EPImage;->previewImageRectCenterX:I

    iget v6, v3, Lepson/print/EPImage;->previewImageRectCenterY:I

    invoke-direct {v4, v5, v6}, Landroid/graphics/Point;-><init>(II)V

    .line 353
    iget v5, v3, Lepson/print/EPImage;->previewWidth:I

    iget v6, v3, Lepson/print/EPImage;->previewHeight:I

    iget v7, v3, Lepson/print/EPImage;->scaleFactor:F

    invoke-direct {p0, v5, v6, v7, v4}, Lepson/print/phlayout/PhotoPreview;->calcPreviewImageRect(IIFLandroid/graphics/Point;)Landroid/graphics/RectF;

    move-result-object v4

    .line 356
    new-instance v5, Lepson/print/EPImage;

    invoke-direct {v5, v3}, Lepson/print/EPImage;-><init>(Lepson/print/EPImage;)V

    .line 357
    iget v3, v4, Landroid/graphics/RectF;->left:F

    iput v3, v5, Lepson/print/EPImage;->previewImageRectLeft:F

    .line 358
    iget v3, v4, Landroid/graphics/RectF;->top:F

    iput v3, v5, Lepson/print/EPImage;->previewImageRectTop:F

    .line 359
    iget v3, v4, Landroid/graphics/RectF;->right:F

    iput v3, v5, Lepson/print/EPImage;->previewImageRectRight:F

    .line 360
    iget v3, v4, Landroid/graphics/RectF;->bottom:F

    iput v3, v5, Lepson/print/EPImage;->previewImageRectBottom:F

    .line 362
    invoke-virtual {v1, v5}, Lepson/print/EPImageList;->add(Lepson/print/EPImage;)Z

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 365
    :cond_1
    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    .line 366
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getIsPaperLandscape()Z
    .locals 1

    .line 416
    iget-object v0, p0, Lepson/print/phlayout/PhotoPreview;->mLayoutPosition:Lepson/print/phlayout/ILayoutPosition;

    invoke-interface {v0}, Lepson/print/phlayout/ILayoutPosition;->getIsPaperLandscape()Z

    move-result v0

    return v0
.end method

.method public invalidateImageNo()V
    .locals 1
    .annotation build Landroid/support/annotation/AnyThread;
    .end annotation

    .line 507
    iget-object v0, p0, Lepson/print/phlayout/PhotoPreview;->imageList:Lepson/print/phlayout/PhotoPreviewImageList;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, -0x1

    .line 510
    iput v0, p0, Lepson/print/phlayout/PhotoPreview;->imageNo:I

    return-void
.end method

.method public invalidatePreview()V
    .locals 4

    .line 518
    iget-object v0, p0, Lepson/print/phlayout/PhotoPreview;->imageList:Lepson/print/phlayout/PhotoPreviewImageList;

    if-nez v0, :cond_0

    return-void

    .line 522
    :cond_0
    iget-object v0, p0, Lepson/print/phlayout/PhotoPreview;->lockObj:Ljava/lang/Object;

    monitor-enter v0

    const/4 v1, 0x0

    .line 523
    :goto_0
    :try_start_0
    iget-object v2, p0, Lepson/print/phlayout/PhotoPreview;->imageList:Lepson/print/phlayout/PhotoPreviewImageList;

    invoke-virtual {v2}, Lepson/print/phlayout/PhotoPreviewImageList;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 524
    iget-object v2, p0, Lepson/print/phlayout/PhotoPreview;->imageList:Lepson/print/phlayout/PhotoPreviewImageList;

    invoke-virtual {v2, v1}, Lepson/print/phlayout/PhotoPreviewImageList;->get(I)Lepson/print/EPImage;

    move-result-object v2

    const/4 v3, 0x0

    iput-object v3, v2, Lepson/print/EPImage;->previewImageFileName:Ljava/lang/String;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 526
    :cond_1
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 12

    .line 928
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 931
    iget-object v0, p0, Lepson/print/phlayout/PhotoPreview;->mDrawLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->tryLock()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 933
    :try_start_0
    iget-object v0, p0, Lepson/print/phlayout/PhotoPreview;->lockObj:Ljava/lang/Object;

    monitor-enter v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 935
    :try_start_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->isHardwareAccelerated()Z

    move-result v1

    if-nez v1, :cond_0

    .line 936
    iget v1, p0, Lepson/print/phlayout/PhotoPreview;->maxBitmapWidth:I

    if-lez v1, :cond_2

    iget v1, p0, Lepson/print/phlayout/PhotoPreview;->maxBitmapHeight:I

    if-lez v1, :cond_2

    const/4 v1, -0x1

    .line 937
    iput v1, p0, Lepson/print/phlayout/PhotoPreview;->maxBitmapWidth:I

    .line 938
    iput v1, p0, Lepson/print/phlayout/PhotoPreview;->maxBitmapHeight:I

    goto :goto_0

    .line 941
    :cond_0
    iget v1, p0, Lepson/print/phlayout/PhotoPreview;->maxBitmapWidth:I

    if-ltz v1, :cond_1

    iget v1, p0, Lepson/print/phlayout/PhotoPreview;->maxBitmapHeight:I

    if-gez v1, :cond_2

    .line 942
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getMaximumBitmapWidth()I

    move-result v1

    iput v1, p0, Lepson/print/phlayout/PhotoPreview;->maxBitmapWidth:I

    .line 943
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getMaximumBitmapHeight()I

    move-result v1

    iput v1, p0, Lepson/print/phlayout/PhotoPreview;->maxBitmapHeight:I

    :cond_2
    :goto_0
    const/4 v1, 0x0

    .line 948
    iget-object v2, p0, Lepson/print/phlayout/PhotoPreview;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_3

    .line 951
    iget-object v1, p0, Lepson/print/phlayout/PhotoPreview;->mBitmap:Landroid/graphics/Bitmap;

    iget v2, p0, Lepson/print/phlayout/PhotoPreview;->scaleFactor:F

    iget-object v3, p0, Lepson/print/phlayout/PhotoPreview;->trans:Landroid/graphics/Point;

    invoke-virtual {p0, v1, v2, v3}, Lepson/print/phlayout/PhotoPreview;->calcPreviewImageRect(Landroid/graphics/Bitmap;FLandroid/graphics/Point;)Landroid/graphics/RectF;

    move-result-object v1

    move-object v10, v1

    goto :goto_1

    :cond_3
    move-object v10, v1

    .line 953
    :goto_1
    iget-object v1, p0, Lepson/print/phlayout/PhotoPreview;->mLayoutPosition:Lepson/print/phlayout/ILayoutPosition;

    invoke-interface {v1}, Lepson/print/phlayout/ILayoutPosition;->getLayoutId()I

    move-result v1

    const v2, 0x7f050028

    const/4 v11, 0x4

    if-ne v1, v11, :cond_4

    .line 954
    iget-object v3, p0, Lepson/print/phlayout/PhotoPreview;->paint:Landroid/graphics/Paint;

    iget-object v4, p0, Lepson/print/phlayout/PhotoPreview;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v5, p0, Lepson/print/phlayout/PhotoPreview;->mLayoutPosition:Lepson/print/phlayout/ILayoutPosition;

    iget-object v6, p0, Lepson/print/phlayout/PhotoPreview;->mBitmapDrawRect:Landroid/graphics/Rect;

    iget-boolean v7, p0, Lepson/print/phlayout/PhotoPreview;->maskExpandPreview:Z

    iget-object v8, p0, Lepson/print/phlayout/PhotoPreview;->mPath:Landroid/graphics/Path;

    .line 955
    invoke-virtual {p0}, Lepson/print/phlayout/PhotoPreview;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    move-object v1, p0

    move-object v2, p1

    .line 954
    invoke-direct/range {v1 .. v10}, Lepson/print/phlayout/PhotoPreview;->drawCdPreviewAll(Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/Bitmap;Lepson/print/phlayout/ILayoutPosition;Landroid/graphics/Rect;ZLandroid/graphics/Path;ILandroid/graphics/RectF;)V

    goto :goto_2

    .line 958
    :cond_4
    iget-object v3, p0, Lepson/print/phlayout/PhotoPreview;->paint:Landroid/graphics/Paint;

    iget-object v4, p0, Lepson/print/phlayout/PhotoPreview;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v5, p0, Lepson/print/phlayout/PhotoPreview;->mLayoutPosition:Lepson/print/phlayout/ILayoutPosition;

    iget-object v6, p0, Lepson/print/phlayout/PhotoPreview;->mBitmapDrawRect:Landroid/graphics/Rect;

    iget-boolean v7, p0, Lepson/print/phlayout/PhotoPreview;->maskExpandPreview:Z

    .line 959
    invoke-virtual {p0}, Lepson/print/phlayout/PhotoPreview;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    const/4 v1, 0x2

    new-array v9, v1, [I

    const/4 v1, 0x0

    .line 960
    invoke-virtual {p0}, Lepson/print/phlayout/PhotoPreview;->getWidth()I

    move-result v2

    aput v2, v9, v1

    const/4 v1, 0x1

    invoke-virtual {p0}, Lepson/print/phlayout/PhotoPreview;->getHeight()I

    move-result v2

    aput v2, v9, v1

    move-object v1, p0

    move-object v2, p1

    .line 958
    invoke-direct/range {v1 .. v10}, Lepson/print/phlayout/PhotoPreview;->drawPreview(Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/Bitmap;Lepson/print/phlayout/ILayoutPosition;Landroid/graphics/Rect;ZI[ILandroid/graphics/RectF;)V

    .line 965
    :goto_2
    iget-object p1, p0, Lepson/print/phlayout/PhotoPreview;->drawEndHandler:Landroid/os/Handler;

    if-eqz p1, :cond_5

    .line 966
    new-instance p1, Landroid/os/Message;

    invoke-direct {p1}, Landroid/os/Message;-><init>()V

    .line 967
    iput v11, p1, Landroid/os/Message;->what:I

    .line 968
    iget v1, p0, Lepson/print/phlayout/PhotoPreview;->imageNo:I

    iput v1, p1, Landroid/os/Message;->arg1:I

    .line 969
    iget-object v1, p0, Lepson/print/phlayout/PhotoPreview;->drawEndHandler:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 971
    :cond_5
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 973
    iget-object p1, p0, Lepson/print/phlayout/PhotoPreview;->mDrawLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {p1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_3

    :catchall_0
    move-exception p1

    .line 971
    :try_start_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception p1

    .line 973
    iget-object v0, p0, Lepson/print/phlayout/PhotoPreview;->mDrawLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw p1

    :cond_6
    :goto_3
    return-void
.end method

.method public onSizeChanged(IIII)V
    .locals 0

    .line 913
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 914
    monitor-enter p0

    .line 915
    :try_start_0
    iget-object p3, p0, Lepson/print/phlayout/PhotoPreview;->mViewSize:[I

    const/4 p4, 0x0

    aput p1, p3, p4

    .line 916
    iget-object p1, p0, Lepson/print/phlayout/PhotoPreview;->mViewSize:[I

    const/4 p3, 0x1

    aput p2, p1, p3

    .line 917
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 919
    iget-object p1, p0, Lepson/print/phlayout/PhotoPreview;->mViewSizeChangeListener:Lepson/print/phlayout/PhotoPreview$ViewSizeChangeListener;

    if-eqz p1, :cond_0

    .line 920
    invoke-interface {p1}, Lepson/print/phlayout/PhotoPreview$ViewSizeChangeListener;->onViewSizeChanged()V

    :cond_0
    return-void

    :catchall_0
    move-exception p1

    .line 917
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .line 824
    iget-object v0, p0, Lepson/print/phlayout/PhotoPreview;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 825
    iget-object v1, p0, Lepson/print/phlayout/PhotoPreview;->mGestureDetector:Landroid/support/v4/view/GestureDetectorCompat;

    invoke-virtual {v1, p1}, Landroid/support/v4/view/GestureDetectorCompat;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-nez v1, :cond_1

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 826
    :goto_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    and-int/lit16 v1, v1, 0xff

    if-ne v1, v3, :cond_2

    .line 827
    invoke-direct {p0}, Lepson/print/phlayout/PhotoPreview;->onMoveEventEnd()V

    :cond_2
    if-nez v0, :cond_3

    .line 830
    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    if-eqz p1, :cond_4

    :cond_3
    const/4 v2, 0x1

    :cond_4
    return v2
.end method

.method public setColor(I)V
    .locals 0

    .line 404
    iput p1, p0, Lepson/print/phlayout/PhotoPreview;->color:I

    return-void
.end method

.method public setDrawEndHandler(Landroid/os/Handler;)V
    .locals 0

    .line 412
    iput-object p1, p0, Lepson/print/phlayout/PhotoPreview;->drawEndHandler:Landroid/os/Handler;

    return-void
.end method

.method public setImage(IILandroid/app/Activity;)Z
    .locals 4
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation

    .line 589
    :try_start_0
    iget-object v0, p0, Lepson/print/phlayout/PhotoPreview;->mDrawLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 591
    iget-object v0, p0, Lepson/print/phlayout/PhotoPreview;->lockObj:Ljava/lang/Object;

    monitor-enter v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 592
    :try_start_1
    iget-object v1, p0, Lepson/print/phlayout/PhotoPreview;->imageList:Lepson/print/phlayout/PhotoPreviewImageList;

    const/4 v2, 0x0

    if-eqz v1, :cond_9

    iget-object v1, p0, Lepson/print/phlayout/PhotoPreview;->mLayoutPosition:Lepson/print/phlayout/ILayoutPosition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-nez v1, :cond_0

    goto/16 :goto_1

    .line 600
    :cond_0
    :try_start_2
    iget-object v1, p0, Lepson/print/phlayout/PhotoPreview;->imageList:Lepson/print/phlayout/PhotoPreviewImageList;

    invoke-virtual {v1}, Lepson/print/phlayout/PhotoPreviewImageList;->size()I

    move-result v1

    const/4 v3, 0x1

    if-gtz v1, :cond_4

    const/4 p1, 0x0

    .line 601
    iput-object p1, p0, Lepson/print/phlayout/PhotoPreview;->mBitmap:Landroid/graphics/Bitmap;

    const/4 p1, 0x2

    if-ne p2, p1, :cond_1

    .line 605
    invoke-direct {p0}, Lepson/print/phlayout/PhotoPreview;->fitCircumscribed()V

    :cond_1
    if-eq p2, v3, :cond_2

    .line 609
    iget-object p1, p0, Lepson/print/phlayout/PhotoPreview;->mLayoutPosition:Lepson/print/phlayout/ILayoutPosition;

    .line 610
    invoke-interface {p1}, Lepson/print/phlayout/ILayoutPosition;->isPreviewImageSizeValid()Z

    move-result p1

    if-nez p1, :cond_3

    .line 611
    :cond_2
    iget p1, p0, Lepson/print/phlayout/PhotoPreview;->mPaperSizeId:I

    invoke-virtual {p0, p1}, Lepson/print/phlayout/PhotoPreview;->setPaper(I)Z
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 646
    :cond_3
    :try_start_3
    invoke-virtual {p0}, Lepson/print/phlayout/PhotoPreview;->postInvalidate()V

    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 652
    iget-object p1, p0, Lepson/print/phlayout/PhotoPreview;->mDrawLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {p1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return v2

    .line 617
    :cond_4
    :try_start_4
    invoke-direct {p0, p1, p2}, Lepson/print/phlayout/PhotoPreview;->setImageCore(II)V

    .line 619
    iget-object p1, p0, Lepson/print/phlayout/PhotoPreview;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz p1, :cond_8

    .line 621
    iget p1, p0, Lepson/print/phlayout/PhotoPreview;->maxBitmapWidth:I

    const/4 p2, -0x1

    if-eq p1, p2, :cond_5

    iget-object p1, p0, Lepson/print/phlayout/PhotoPreview;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result p1

    iget v1, p0, Lepson/print/phlayout/PhotoPreview;->maxBitmapWidth:I

    if-gt p1, v1, :cond_6

    :cond_5
    iget p1, p0, Lepson/print/phlayout/PhotoPreview;->maxBitmapWidth:I

    if-eq p1, p2, :cond_7

    iget-object p1, p0, Lepson/print/phlayout/PhotoPreview;->mBitmap:Landroid/graphics/Bitmap;

    .line 622
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result p1

    iget p2, p0, Lepson/print/phlayout/PhotoPreview;->maxBitmapHeight:I

    if-le p1, p2, :cond_7

    .line 623
    :cond_6
    new-instance p1, Lepson/print/phlayout/PhotoPreview$1;

    invoke-direct {p1, p0}, Lepson/print/phlayout/PhotoPreview$1;-><init>(Lepson/print/phlayout/PhotoPreview;)V

    invoke-virtual {p3, p1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 635
    :cond_7
    iput-boolean v3, p0, Lepson/print/phlayout/PhotoPreview;->maskExpandPreview:Z
    :try_end_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 646
    :try_start_5
    invoke-virtual {p0}, Lepson/print/phlayout/PhotoPreview;->postInvalidate()V

    .line 649
    monitor-exit v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 652
    iget-object p1, p0, Lepson/print/phlayout/PhotoPreview;->mDrawLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {p1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return v3

    .line 631
    :cond_8
    :try_start_6
    new-instance p1, Ljava/lang/OutOfMemoryError;

    invoke-direct {p1}, Ljava/lang/OutOfMemoryError;-><init>()V

    throw p1
    :try_end_6
    .catch Ljava/lang/OutOfMemoryError; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/lang/NullPointerException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :catchall_0
    move-exception p1

    goto :goto_0

    .line 646
    :catch_0
    :try_start_7
    invoke-virtual {p0}, Lepson/print/phlayout/PhotoPreview;->postInvalidate()V

    monitor-exit v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 652
    iget-object p1, p0, Lepson/print/phlayout/PhotoPreview;->mDrawLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {p1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return v2

    .line 646
    :catch_1
    :try_start_8
    invoke-virtual {p0}, Lepson/print/phlayout/PhotoPreview;->postInvalidate()V

    monitor-exit v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 652
    iget-object p1, p0, Lepson/print/phlayout/PhotoPreview;->mDrawLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {p1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return v2

    :catch_2
    move-exception p1

    .line 637
    :try_start_9
    throw p1
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 646
    :goto_0
    :try_start_a
    invoke-virtual {p0}, Lepson/print/phlayout/PhotoPreview;->postInvalidate()V

    throw p1

    .line 596
    :cond_9
    :goto_1
    monitor-exit v0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 652
    iget-object p1, p0, Lepson/print/phlayout/PhotoPreview;->mDrawLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {p1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return v2

    :catchall_1
    move-exception p1

    .line 650
    :try_start_b
    monitor-exit v0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    :try_start_c
    throw p1
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    :catchall_2
    move-exception p1

    .line 652
    iget-object p2, p0, Lepson/print/phlayout/PhotoPreview;->mDrawLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {p2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw p1
.end method

.method public setImageList(Lepson/print/phlayout/PhotoPreviewImageList;)V
    .locals 0
    .param p1    # Lepson/print/phlayout/PhotoPreviewImageList;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 375
    iput-object p1, p0, Lepson/print/phlayout/PhotoPreview;->imageList:Lepson/print/phlayout/PhotoPreviewImageList;

    return-void
.end method

.method public setIsPaperLandscape(ZLandroid/app/Activity;)V
    .locals 2
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .line 422
    iget-object v0, p0, Lepson/print/phlayout/PhotoPreview;->lockObj:Ljava/lang/Object;

    monitor-enter v0

    .line 423
    :try_start_0
    iget-object v1, p0, Lepson/print/phlayout/PhotoPreview;->mLayoutPosition:Lepson/print/phlayout/ILayoutPosition;

    invoke-interface {v1, p1}, Lepson/print/phlayout/ILayoutPosition;->setPaperLandscape(Z)V

    .line 425
    iget-object p1, p0, Lepson/print/phlayout/PhotoPreview;->imageList:Lepson/print/phlayout/PhotoPreviewImageList;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lepson/print/phlayout/PhotoPreview;->imageList:Lepson/print/phlayout/PhotoPreviewImageList;

    invoke-virtual {p1}, Lepson/print/phlayout/PhotoPreviewImageList;->size()I

    move-result p1

    if-lez p1, :cond_0

    const/high16 p1, 0x3f800000    # 1.0f

    .line 428
    iput p1, p0, Lepson/print/phlayout/PhotoPreview;->scaleFactor:F

    .line 429
    iput p1, p0, Lepson/print/phlayout/PhotoPreview;->baseScaleFactor:F

    const/4 p1, 0x1

    .line 430
    iput p1, p0, Lepson/print/phlayout/PhotoPreview;->mFitMode:I

    .line 431
    iget v1, p0, Lepson/print/phlayout/PhotoPreview;->imageNo:I

    invoke-virtual {p0, v1, p1, p2}, Lepson/print/phlayout/PhotoPreview;->setImage(IILandroid/app/Activity;)Z

    goto :goto_0

    .line 434
    :cond_0
    iget p1, p0, Lepson/print/phlayout/PhotoPreview;->mPaperSizeId:I

    invoke-virtual {p0, p1}, Lepson/print/phlayout/PhotoPreview;->setPaper(I)Z

    .line 436
    :goto_0
    invoke-virtual {p0}, Lepson/print/phlayout/PhotoPreview;->postInvalidate()V

    .line 437
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public setLayout(II)V
    .locals 1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    .line 397
    new-instance v0, Lepson/print/phlayout/BorderedLayoutPosition;

    invoke-direct {v0}, Lepson/print/phlayout/BorderedLayoutPosition;-><init>()V

    iput-object v0, p0, Lepson/print/phlayout/PhotoPreview;->mLayoutPosition:Lepson/print/phlayout/ILayoutPosition;

    goto :goto_0

    .line 388
    :cond_0
    new-instance v0, Lepson/print/phlayout/CdLayoutPosition;

    invoke-direct {v0}, Lepson/print/phlayout/CdLayoutPosition;-><init>()V

    iput-object v0, p0, Lepson/print/phlayout/PhotoPreview;->mLayoutPosition:Lepson/print/phlayout/ILayoutPosition;

    goto :goto_0

    .line 392
    :cond_1
    new-instance v0, Lepson/print/phlayout/BorderlessLayoutPosition;

    invoke-direct {v0}, Lepson/print/phlayout/BorderlessLayoutPosition;-><init>()V

    iput-object v0, p0, Lepson/print/phlayout/PhotoPreview;->mLayoutPosition:Lepson/print/phlayout/ILayoutPosition;

    .line 400
    :goto_0
    iget-object v0, p0, Lepson/print/phlayout/PhotoPreview;->mLayoutPosition:Lepson/print/phlayout/ILayoutPosition;

    invoke-interface {v0, p1, p2}, Lepson/print/phlayout/ILayoutPosition;->setLayoutId(II)V

    return-void
.end method

.method public setPaper(I)Z
    .locals 6
    .annotation build Landroid/support/annotation/AnyThread;
    .end annotation

    .line 447
    iget-object v0, p0, Lepson/print/phlayout/PhotoPreview;->lockObj:Ljava/lang/Object;

    monitor-enter v0

    .line 449
    :try_start_0
    iput p1, p0, Lepson/print/phlayout/PhotoPreview;->mPaperSizeId:I

    .line 450
    invoke-static {}, Lepson/print/IprintApplication;->getInstance()Lepson/print/IprintApplication;

    move-result-object p1

    iget v1, p0, Lepson/print/phlayout/PhotoPreview;->mPaperSizeId:I

    invoke-static {p1, v1}, Lepson/common/Info_paper;->getInfoPaper(Landroid/content/Context;I)Lepson/common/Info_paper;

    move-result-object p1

    const/4 v1, 0x0

    if-nez p1, :cond_0

    .line 452
    monitor-exit v0

    return v1

    .line 455
    :cond_0
    invoke-direct {p0}, Lepson/print/phlayout/PhotoPreview;->getViewSize()[I

    move-result-object v2

    if-nez v2, :cond_1

    .line 457
    monitor-exit v0

    return v1

    .line 460
    :cond_1
    iget-object v3, p0, Lepson/print/phlayout/PhotoPreview;->mLayoutPosition:Lepson/print/phlayout/ILayoutPosition;

    aget v1, v2, v1

    const/4 v4, 0x1

    aget v2, v2, v4

    iget-object v5, p0, Lepson/print/phlayout/PhotoPreview;->trans:Landroid/graphics/Point;

    invoke-interface {v3, p1, v1, v2, v5}, Lepson/print/phlayout/ILayoutPosition;->setPaperAndCalcPreviewPosition(Lepson/common/Info_paper;IILandroid/graphics/Point;)V

    .line 461
    monitor-exit v0

    return v4

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public setPreviewBitmapMaker(Lepson/print/phlayout/PhotoPreview$PreviewBitmapMaker;)V
    .locals 0

    .line 326
    iput-object p1, p0, Lepson/print/phlayout/PhotoPreview;->mPreviewBitmapMaker:Lepson/print/phlayout/PhotoPreview$PreviewBitmapMaker;

    return-void
.end method

.method public setViewSizeChangeListener(Lepson/print/phlayout/PhotoPreview$ViewSizeChangeListener;)V
    .locals 0

    .line 319
    iput-object p1, p0, Lepson/print/phlayout/PhotoPreview;->mViewSizeChangeListener:Lepson/print/phlayout/PhotoPreview$ViewSizeChangeListener;

    return-void
.end method

.method public setZoomControlHandler(Landroid/os/Handler;)V
    .locals 0

    .line 408
    iput-object p1, p0, Lepson/print/phlayout/PhotoPreview;->zoomControlHandler:Landroid/os/Handler;

    return-void
.end method

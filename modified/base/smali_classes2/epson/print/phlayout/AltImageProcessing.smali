.class public Lepson/print/phlayout/AltImageProcessing;
.super Ljava/lang/Object;
.source "AltImageProcessing.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private localResizeFile(Ljava/lang/String;Ljava/lang/String;[IZ)V
    .locals 14
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # [I
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    move-object/from16 v0, p2

    move-object v1, p0

    .line 103
    invoke-virtual {p0, v0}, Lepson/print/phlayout/AltImageProcessing;->getImageSize(Ljava/lang/String;)[I

    move-result-object v2

    .line 105
    new-instance v13, Lcom/epson/cameracopy/printlayout/ImageAndLayout;

    invoke-direct {v13}, Lcom/epson/cameracopy/printlayout/ImageAndLayout;-><init>()V

    .line 106
    invoke-virtual {v13, v0}, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->setOrgFileName(Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 107
    aget v3, v2, v0

    const/4 v4, 0x1

    aget v5, v2, v4

    invoke-virtual {v13, v3, v5}, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->setLayoutAreaSize(II)V

    .line 108
    aget v3, v2, v0

    int-to-double v5, v3

    aget v2, v2, v4

    int-to-double v7, v2

    const-wide/16 v9, 0x0

    const-wide/16 v11, 0x0

    const/4 v2, 0x0

    move-object v3, v13

    move-wide v4, v5

    move-wide v6, v7

    move-wide v8, v9

    move-wide v10, v11

    move v12, v2

    invoke-virtual/range {v3 .. v12}, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->setLayout(DDDDI)V

    .line 109
    invoke-virtual {v13, v0}, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->setOpenCvExifRotationCancel(Z)V

    move-object v2, p1

    move-object/from16 v3, p3

    move/from16 v4, p4

    .line 110
    invoke-virtual {v13, p1, v4, v3, v0}, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->createPrintData(Ljava/lang/String;Z[II)Z

    return-void
.end method


# virtual methods
.method public createPreviewImage(Lepson/print/EPImage;IIIZ)Ljava/lang/String;
    .locals 4
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    if-nez p5, :cond_0

    .line 54
    iget-object p5, p1, Lepson/print/EPImage;->previewImageFileName:Ljava/lang/String;

    if-eqz p5, :cond_0

    .line 56
    iget-object p1, p1, Lepson/print/EPImage;->previewImageFileName:Ljava/lang/String;

    return-object p1

    :cond_0
    if-lez p2, :cond_8

    if-lez p3, :cond_8

    const/4 p5, 0x0

    .line 62
    iput-object p5, p1, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    .line 63
    iput-object p5, p1, Lepson/print/EPImage;->previewImageFileName:Ljava/lang/String;

    const/4 v0, 0x0

    .line 64
    iput v0, p1, Lepson/print/EPImage;->decodeWidth:I

    .line 65
    iput v0, p1, Lepson/print/EPImage;->decodeHeight:I

    .line 67
    iget-object v1, p1, Lepson/print/EPImage;->loadImageFileName:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lepson/print/phlayout/AltImageProcessing;->getImageSize(Ljava/lang/String;)[I

    move-result-object v1

    .line 68
    aget v2, v1, v0

    if-lez v2, :cond_7

    const/4 v2, 0x1

    aget v3, v1, v2

    if-gtz v3, :cond_1

    goto :goto_3

    :cond_1
    const/4 v3, 0x2

    .line 72
    new-array v3, v3, [I

    aput p2, v3, v0

    aput p3, v3, v2

    invoke-virtual {p0, v3, v1}, Lepson/print/phlayout/AltImageProcessing;->getGaisetsuSize([I[I)[I

    move-result-object p2

    .line 73
    aget p3, p2, v0

    if-lez p3, :cond_6

    aget p3, p2, v2

    if-gtz p3, :cond_2

    goto :goto_2

    .line 78
    :cond_2
    iget p3, p1, Lepson/print/EPImage;->index:I

    invoke-virtual {p0, p3}, Lepson/print/phlayout/AltImageProcessing;->getPreviewImageFileName(I)Ljava/lang/String;

    move-result-object p3

    .line 79
    iget-object v1, p1, Lepson/print/EPImage;->loadImageFileName:Ljava/lang/String;

    if-ne p4, v2, :cond_3

    const/4 p4, 0x1

    goto :goto_0

    :cond_3
    const/4 p4, 0x0

    :goto_0
    invoke-direct {p0, p3, v1, p2, p4}, Lepson/print/phlayout/AltImageProcessing;->localResizeFile(Ljava/lang/String;Ljava/lang/String;[IZ)V

    .line 81
    invoke-virtual {p0, p3}, Lepson/print/phlayout/AltImageProcessing;->getImageSize(Ljava/lang/String;)[I

    move-result-object p2

    .line 82
    aget p4, p2, v0

    if-lez p4, :cond_5

    aget p4, p2, v2

    if-gtz p4, :cond_4

    goto :goto_1

    .line 87
    :cond_4
    aget p4, p2, v0

    iput p4, p1, Lepson/print/EPImage;->decodeWidth:I

    .line 88
    aget p4, p2, v2

    iput p4, p1, Lepson/print/EPImage;->decodeHeight:I

    .line 89
    aget p4, p2, v0

    iput p4, p1, Lepson/print/EPImage;->previewWidth:I

    .line 90
    aget p2, p2, v2

    iput p2, p1, Lepson/print/EPImage;->previewHeight:I

    .line 92
    iput-object p3, p1, Lepson/print/EPImage;->previewImageFileName:Ljava/lang/String;

    return-object p3

    :cond_5
    :goto_1
    return-object p5

    :cond_6
    :goto_2
    return-object p5

    :cond_7
    :goto_3
    return-object p5

    .line 59
    :cond_8
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance p4, Ljava/lang/StringBuilder;

    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    const-string p5, "width x height <"

    invoke-virtual {p4, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, ","

    invoke-virtual {p4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, ">"

    invoke-virtual {p4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getGaisetsuSize([I[I)[I
    .locals 8
    .param p1    # [I
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # [I
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    const/4 v0, 0x0

    .line 119
    aget v1, p1, v0

    int-to-double v1, v1

    aget v3, p2, v0

    int-to-double v3, v3

    div-double/2addr v1, v3

    const/4 v3, 0x1

    .line 120
    aget v4, p1, v3

    int-to-double v4, v4

    aget v6, p2, v3

    int-to-double v6, v6

    div-double/2addr v4, v6

    const/4 v6, 0x2

    cmpl-double v7, v1, v4

    if-lez v7, :cond_0

    .line 124
    new-array v4, v6, [I

    aget p1, p1, v0

    aput p1, v4, v0

    aget p1, p2, v3

    int-to-double p1, p1

    mul-double v1, v1, p1

    double-to-int p1, v1

    aput p1, v4, v3

    return-object v4

    .line 128
    :cond_0
    new-array v1, v6, [I

    aget p2, p2, v0

    int-to-double v6, p2

    mul-double v4, v4, v6

    double-to-int p2, v4

    aput p2, v1, v0

    aget p1, p1, v3

    aput p1, v1, v3

    return-object v1
.end method

.method public getImageSize(Ljava/lang/String;)[I
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    const/4 v0, -0x1

    .line 138
    invoke-static {p1, v0}, Lorg/opencv/imgcodecs/Imgcodecs;->imread(Ljava/lang/String;I)Lorg/opencv/core/Mat;

    move-result-object p1

    .line 139
    invoke-virtual {p1}, Lorg/opencv/core/Mat;->size()Lorg/opencv/core/Size;

    move-result-object v0

    .line 140
    invoke-virtual {p1}, Lorg/opencv/core/Mat;->release()V

    const/4 p1, 0x2

    .line 142
    new-array p1, p1, [I

    iget-wide v1, v0, Lorg/opencv/core/Size;->width:D

    double-to-int v1, v1

    const/4 v2, 0x0

    aput v1, p1, v2

    iget-wide v0, v0, Lorg/opencv/core/Size;->height:D

    double-to-int v0, v0

    const/4 v1, 0x1

    aput v0, p1, v1

    return-object p1
.end method

.method public getPreviewImageFileName(I)Ljava/lang/String;
    .locals 4
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 152
    new-instance v0, Ljava/io/File;

    invoke-static {}, Lepson/print/IprintApplication;->getInstance()Lepson/print/IprintApplication;

    move-result-object v1

    invoke-static {v1}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v1

    invoke-virtual {v1}, Lepson/common/ExternalFileUtils;->getTempViewDir()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "preview_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 p1, p1, 0x1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ".bmp"

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

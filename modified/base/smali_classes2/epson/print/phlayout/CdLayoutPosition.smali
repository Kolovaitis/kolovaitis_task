.class Lepson/print/phlayout/CdLayoutPosition;
.super Ljava/lang/Object;
.source "CdLayoutPosition.java"

# interfaces
.implements Lepson/print/phlayout/ILayoutPosition;


# instance fields
.field private layout:I

.field private layoutMulti:I

.field private mBottomMargin:I

.field private mIsPaperLandScape:Z

.field private mLeftMargin:I

.field private mPaperHeight:I

.field private mPaperWidth:I

.field private mPreviewPrintArea:Lepson/print/phlayout/AltRect;

.field private mRightMargin:I

.field private mTopMargin:I


# direct methods
.method constructor <init>()V
    .locals 1

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    .line 17
    iput v0, p0, Lepson/print/phlayout/CdLayoutPosition;->layout:I

    const/4 v0, 0x0

    .line 18
    iput v0, p0, Lepson/print/phlayout/CdLayoutPosition;->layoutMulti:I

    .line 20
    iput v0, p0, Lepson/print/phlayout/CdLayoutPosition;->mPaperWidth:I

    .line 21
    iput v0, p0, Lepson/print/phlayout/CdLayoutPosition;->mPaperHeight:I

    .line 23
    iput v0, p0, Lepson/print/phlayout/CdLayoutPosition;->mLeftMargin:I

    .line 24
    iput v0, p0, Lepson/print/phlayout/CdLayoutPosition;->mTopMargin:I

    .line 25
    iput v0, p0, Lepson/print/phlayout/CdLayoutPosition;->mRightMargin:I

    .line 26
    iput v0, p0, Lepson/print/phlayout/CdLayoutPosition;->mBottomMargin:I

    .line 29
    iput-boolean v0, p0, Lepson/print/phlayout/CdLayoutPosition;->mIsPaperLandScape:Z

    .line 32
    new-instance v0, Lepson/print/phlayout/AltRect;

    invoke-direct {v0}, Lepson/print/phlayout/AltRect;-><init>()V

    iput-object v0, p0, Lepson/print/phlayout/CdLayoutPosition;->mPreviewPrintArea:Lepson/print/phlayout/AltRect;

    return-void
.end method


# virtual methods
.method calculateLayoutSizeCdDvd(IILandroid/graphics/Point;)V
    .locals 4
    .param p3    # Landroid/graphics/Point;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    const-wide v0, 0x3fe99999a0000000L    # 0.800000011920929

    if-le p2, p1, :cond_0

    int-to-double v2, p1

    mul-double v2, v2, v0

    double-to-int v0, v2

    goto :goto_0

    :cond_0
    int-to-double v2, p2

    mul-double v2, v2, v0

    double-to-int v0, v2

    .line 171
    :goto_0
    iget-object v1, p0, Lepson/print/phlayout/CdLayoutPosition;->mPreviewPrintArea:Lepson/print/phlayout/AltRect;

    sub-int/2addr p1, v0

    div-int/lit8 p1, p1, 0x2

    iput p1, v1, Lepson/print/phlayout/AltRect;->left:I

    sub-int/2addr p2, v0

    .line 172
    div-int/lit8 p2, p2, 0x2

    iput p2, v1, Lepson/print/phlayout/AltRect;->top:I

    .line 173
    iget p1, v1, Lepson/print/phlayout/AltRect;->left:I

    add-int/2addr p1, v0

    iput p1, v1, Lepson/print/phlayout/AltRect;->right:I

    .line 174
    iget-object p1, p0, Lepson/print/phlayout/CdLayoutPosition;->mPreviewPrintArea:Lepson/print/phlayout/AltRect;

    iget p2, p1, Lepson/print/phlayout/AltRect;->top:I

    add-int/2addr p2, v0

    iput p2, p1, Lepson/print/phlayout/AltRect;->bottom:I

    .line 176
    iget-object p1, p0, Lepson/print/phlayout/CdLayoutPosition;->mPreviewPrintArea:Lepson/print/phlayout/AltRect;

    iget p1, p1, Lepson/print/phlayout/AltRect;->right:I

    iget-object p2, p0, Lepson/print/phlayout/CdLayoutPosition;->mPreviewPrintArea:Lepson/print/phlayout/AltRect;

    iget p2, p2, Lepson/print/phlayout/AltRect;->left:I

    sub-int/2addr p1, p2

    div-int/lit8 p1, p1, 0x2

    iget-object p2, p0, Lepson/print/phlayout/CdLayoutPosition;->mPreviewPrintArea:Lepson/print/phlayout/AltRect;

    iget p2, p2, Lepson/print/phlayout/AltRect;->left:I

    add-int/2addr p1, p2

    iput p1, p3, Landroid/graphics/Point;->x:I

    .line 177
    iget-object p1, p0, Lepson/print/phlayout/CdLayoutPosition;->mPreviewPrintArea:Lepson/print/phlayout/AltRect;

    iget p1, p1, Lepson/print/phlayout/AltRect;->bottom:I

    iget-object p2, p0, Lepson/print/phlayout/CdLayoutPosition;->mPreviewPrintArea:Lepson/print/phlayout/AltRect;

    iget p2, p2, Lepson/print/phlayout/AltRect;->top:I

    sub-int/2addr p1, p2

    div-int/lit8 p1, p1, 0x2

    iget-object p2, p0, Lepson/print/phlayout/CdLayoutPosition;->mPreviewPrintArea:Lepson/print/phlayout/AltRect;

    iget p2, p2, Lepson/print/phlayout/AltRect;->top:I

    add-int/2addr p1, p2

    iput p1, p3, Landroid/graphics/Point;->y:I

    .line 179
    iget-object p1, p0, Lepson/print/phlayout/CdLayoutPosition;->mPreviewPrintArea:Lepson/print/phlayout/AltRect;

    invoke-virtual {p1}, Lepson/print/phlayout/AltRect;->width()I

    move-result p1

    int-to-float p1, p1

    iget p2, p0, Lepson/print/phlayout/CdLayoutPosition;->mPaperWidth:I

    int-to-float p2, p2

    div-float/2addr p1, p2

    .line 180
    iget p2, p0, Lepson/print/phlayout/CdLayoutPosition;->mTopMargin:I

    int-to-float p2, p2

    mul-float p2, p2, p1

    float-to-int p2, p2

    iput p2, p0, Lepson/print/phlayout/CdLayoutPosition;->mTopMargin:I

    .line 181
    iget p2, p0, Lepson/print/phlayout/CdLayoutPosition;->mLeftMargin:I

    int-to-float p2, p2

    mul-float p2, p2, p1

    float-to-int p2, p2

    iput p2, p0, Lepson/print/phlayout/CdLayoutPosition;->mLeftMargin:I

    .line 182
    iget p2, p0, Lepson/print/phlayout/CdLayoutPosition;->mRightMargin:I

    int-to-float p2, p2

    mul-float p2, p2, p1

    float-to-int p2, p2

    iput p2, p0, Lepson/print/phlayout/CdLayoutPosition;->mRightMargin:I

    .line 183
    iget p2, p0, Lepson/print/phlayout/CdLayoutPosition;->mBottomMargin:I

    int-to-float p2, p2

    mul-float p2, p2, p1

    float-to-int p1, p2

    iput p1, p0, Lepson/print/phlayout/CdLayoutPosition;->mBottomMargin:I

    .line 186
    iget-boolean p1, p0, Lepson/print/phlayout/CdLayoutPosition;->mIsPaperLandScape:Z

    if-eqz p1, :cond_1

    .line 188
    iget p1, p0, Lepson/print/phlayout/CdLayoutPosition;->mTopMargin:I

    .line 189
    iget p2, p0, Lepson/print/phlayout/CdLayoutPosition;->mRightMargin:I

    iput p2, p0, Lepson/print/phlayout/CdLayoutPosition;->mTopMargin:I

    .line 190
    iget p2, p0, Lepson/print/phlayout/CdLayoutPosition;->mBottomMargin:I

    iput p2, p0, Lepson/print/phlayout/CdLayoutPosition;->mRightMargin:I

    .line 191
    iget p2, p0, Lepson/print/phlayout/CdLayoutPosition;->mLeftMargin:I

    iput p2, p0, Lepson/print/phlayout/CdLayoutPosition;->mBottomMargin:I

    .line 192
    iput p1, p0, Lepson/print/phlayout/CdLayoutPosition;->mLeftMargin:I

    :cond_1
    return-void
.end method

.method public copyPreviewPrintAreaFromEpImage(Lepson/print/EPImage;)V
    .locals 2
    .param p1    # Lepson/print/EPImage;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 122
    iget-boolean v0, p1, Lepson/print/EPImage;->isPaperLandScape:Z

    iput-boolean v0, p0, Lepson/print/phlayout/CdLayoutPosition;->mIsPaperLandScape:Z

    .line 125
    iget-object v0, p0, Lepson/print/phlayout/CdLayoutPosition;->mPreviewPrintArea:Lepson/print/phlayout/AltRect;

    iget v1, p1, Lepson/print/EPImage;->previewPaperRectLeft:I

    iput v1, v0, Lepson/print/phlayout/AltRect;->left:I

    .line 126
    iget-object v0, p0, Lepson/print/phlayout/CdLayoutPosition;->mPreviewPrintArea:Lepson/print/phlayout/AltRect;

    iget v1, p1, Lepson/print/EPImage;->previewPaperRectTop:I

    iput v1, v0, Lepson/print/phlayout/AltRect;->top:I

    .line 127
    iget-object v0, p0, Lepson/print/phlayout/CdLayoutPosition;->mPreviewPrintArea:Lepson/print/phlayout/AltRect;

    iget v1, p1, Lepson/print/EPImage;->previewPaperRectRight:I

    iput v1, v0, Lepson/print/phlayout/AltRect;->right:I

    .line 128
    iget-object v0, p0, Lepson/print/phlayout/CdLayoutPosition;->mPreviewPrintArea:Lepson/print/phlayout/AltRect;

    iget p1, p1, Lepson/print/EPImage;->previewPaperRectBottom:I

    iput p1, v0, Lepson/print/phlayout/AltRect;->bottom:I

    return-void
.end method

.method public copyPreviewPrintAreaToEpImage(Lepson/print/EPImage;)V
    .locals 1

    .line 133
    iget-boolean v0, p0, Lepson/print/phlayout/CdLayoutPosition;->mIsPaperLandScape:Z

    iput-boolean v0, p1, Lepson/print/EPImage;->isPaperLandScape:Z

    .line 137
    iget-object v0, p0, Lepson/print/phlayout/CdLayoutPosition;->mPreviewPrintArea:Lepson/print/phlayout/AltRect;

    iget v0, v0, Lepson/print/phlayout/AltRect;->left:I

    iput v0, p1, Lepson/print/EPImage;->previewPaperRectLeft:I

    .line 138
    iget-object v0, p0, Lepson/print/phlayout/CdLayoutPosition;->mPreviewPrintArea:Lepson/print/phlayout/AltRect;

    iget v0, v0, Lepson/print/phlayout/AltRect;->top:I

    iput v0, p1, Lepson/print/EPImage;->previewPaperRectTop:I

    .line 139
    iget-object v0, p0, Lepson/print/phlayout/CdLayoutPosition;->mPreviewPrintArea:Lepson/print/phlayout/AltRect;

    iget v0, v0, Lepson/print/phlayout/AltRect;->right:I

    iput v0, p1, Lepson/print/EPImage;->previewPaperRectRight:I

    .line 140
    iget-object v0, p0, Lepson/print/phlayout/CdLayoutPosition;->mPreviewPrintArea:Lepson/print/phlayout/AltRect;

    iget v0, v0, Lepson/print/phlayout/AltRect;->bottom:I

    iput v0, p1, Lepson/print/EPImage;->previewPaperRectBottom:I

    return-void
.end method

.method public getBottomMargin()I
    .locals 1

    .line 60
    iget v0, p0, Lepson/print/phlayout/CdLayoutPosition;->mBottomMargin:I

    return v0
.end method

.method public getIsPaperLandscape()Z
    .locals 1

    .line 85
    iget-boolean v0, p0, Lepson/print/phlayout/CdLayoutPosition;->mIsPaperLandScape:Z

    return v0
.end method

.method public getLayoutId()I
    .locals 1

    .line 37
    iget v0, p0, Lepson/print/phlayout/CdLayoutPosition;->layout:I

    return v0
.end method

.method public getLeftMargin()I
    .locals 1

    .line 45
    iget v0, p0, Lepson/print/phlayout/CdLayoutPosition;->mLeftMargin:I

    return v0
.end method

.method public getPreviewImageCircumscribedTargetSize()Landroid/graphics/RectF;
    .locals 1

    .line 69
    iget-object v0, p0, Lepson/print/phlayout/CdLayoutPosition;->mPreviewPrintArea:Lepson/print/phlayout/AltRect;

    invoke-virtual {v0}, Lepson/print/phlayout/AltRect;->getRectF()Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method getPreviewPaperAltRect()Lepson/print/phlayout/AltRect;
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .line 95
    iget-object v0, p0, Lepson/print/phlayout/CdLayoutPosition;->mPreviewPrintArea:Lepson/print/phlayout/AltRect;

    return-object v0
.end method

.method public getPreviewPaperRect()Landroid/graphics/Rect;
    .locals 1

    .line 90
    iget-object v0, p0, Lepson/print/phlayout/CdLayoutPosition;->mPreviewPrintArea:Lepson/print/phlayout/AltRect;

    invoke-virtual {v0}, Lepson/print/phlayout/AltRect;->getRect()Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public getRightMargin()I
    .locals 1

    .line 50
    iget v0, p0, Lepson/print/phlayout/CdLayoutPosition;->mRightMargin:I

    return v0
.end method

.method public getTopMargin()I
    .locals 1

    .line 55
    iget v0, p0, Lepson/print/phlayout/CdLayoutPosition;->mTopMargin:I

    return v0
.end method

.method public isPreviewImageSizeValid()Z
    .locals 2

    .line 74
    iget-object v0, p0, Lepson/print/phlayout/CdLayoutPosition;->mPreviewPrintArea:Lepson/print/phlayout/AltRect;

    iget v0, v0, Lepson/print/phlayout/AltRect;->right:I

    iget-object v1, p0, Lepson/print/phlayout/CdLayoutPosition;->mPreviewPrintArea:Lepson/print/phlayout/AltRect;

    iget v1, v1, Lepson/print/phlayout/AltRect;->left:I

    sub-int/2addr v0, v1

    if-lez v0, :cond_0

    iget-object v0, p0, Lepson/print/phlayout/CdLayoutPosition;->mPreviewPrintArea:Lepson/print/phlayout/AltRect;

    iget v0, v0, Lepson/print/phlayout/AltRect;->bottom:I

    iget-object v1, p0, Lepson/print/phlayout/CdLayoutPosition;->mPreviewPrintArea:Lepson/print/phlayout/AltRect;

    iget v1, v1, Lepson/print/phlayout/AltRect;->top:I

    sub-int/2addr v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public setLayoutId(II)V
    .locals 0

    .line 114
    iput p1, p0, Lepson/print/phlayout/CdLayoutPosition;->layout:I

    .line 115
    iput p2, p0, Lepson/print/phlayout/CdLayoutPosition;->layoutMulti:I

    return-void
.end method

.method public setPaperAndCalcPreviewPosition(Lepson/common/Info_paper;IILandroid/graphics/Point;)V
    .locals 0

    .line 146
    invoke-virtual {p0, p1}, Lepson/print/phlayout/CdLayoutPosition;->setPaperSizeFromPaperInfo(Lepson/common/Info_paper;)V

    .line 149
    iget p1, p0, Lepson/print/phlayout/CdLayoutPosition;->mPaperWidth:I

    if-lez p1, :cond_0

    iget p1, p0, Lepson/print/phlayout/CdLayoutPosition;->mPaperHeight:I

    if-gtz p1, :cond_1

    :cond_0
    const/16 p1, 0xb4c

    .line 150
    iput p1, p0, Lepson/print/phlayout/CdLayoutPosition;->mPaperWidth:I

    const/16 p1, 0x101d

    .line 151
    iput p1, p0, Lepson/print/phlayout/CdLayoutPosition;->mPaperHeight:I

    .line 153
    :cond_1
    invoke-virtual {p0, p2, p3, p4}, Lepson/print/phlayout/CdLayoutPosition;->calculateLayoutSizeCdDvd(IILandroid/graphics/Point;)V

    return-void
.end method

.method setPaperAndImageSize_forTest(IIII)V
    .locals 0
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .line 104
    iput p1, p0, Lepson/print/phlayout/CdLayoutPosition;->mPaperWidth:I

    .line 105
    iput p2, p0, Lepson/print/phlayout/CdLayoutPosition;->mPaperHeight:I

    return-void
.end method

.method public setPaperLandscape(Z)V
    .locals 0

    .line 80
    iput-boolean p1, p0, Lepson/print/phlayout/CdLayoutPosition;->mIsPaperLandScape:Z

    return-void
.end method

.method setPaperSizeFromPaperInfo(Lepson/common/Info_paper;)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .line 212
    invoke-virtual {p1}, Lepson/common/Info_paper;->getPaper_width()I

    move-result v0

    iput v0, p0, Lepson/print/phlayout/CdLayoutPosition;->mPaperWidth:I

    .line 213
    invoke-virtual {p1}, Lepson/common/Info_paper;->getPaper_height()I

    move-result v0

    iput v0, p0, Lepson/print/phlayout/CdLayoutPosition;->mPaperHeight:I

    .line 215
    invoke-virtual {p1}, Lepson/common/Info_paper;->getLeftMargin_border()I

    move-result v0

    iput v0, p0, Lepson/print/phlayout/CdLayoutPosition;->mLeftMargin:I

    .line 216
    invoke-virtual {p1}, Lepson/common/Info_paper;->getTopMargin_border()I

    move-result v0

    iput v0, p0, Lepson/print/phlayout/CdLayoutPosition;->mTopMargin:I

    .line 217
    invoke-virtual {p1}, Lepson/common/Info_paper;->getRightMargin_border()I

    move-result v0

    iput v0, p0, Lepson/print/phlayout/CdLayoutPosition;->mRightMargin:I

    .line 218
    invoke-virtual {p1}, Lepson/common/Info_paper;->getBottomMargin_border()I

    move-result p1

    iput p1, p0, Lepson/print/phlayout/CdLayoutPosition;->mBottomMargin:I

    return-void
.end method

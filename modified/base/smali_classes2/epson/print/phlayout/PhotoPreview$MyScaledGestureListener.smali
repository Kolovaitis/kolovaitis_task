.class Lepson/print/phlayout/PhotoPreview$MyScaledGestureListener;
.super Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;
.source "PhotoPreview.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/phlayout/PhotoPreview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "MyScaledGestureListener"
.end annotation


# instance fields
.field private final mPreviewView:Lepson/print/phlayout/PhotoPreview;


# direct methods
.method public constructor <init>(Lepson/print/phlayout/PhotoPreview;)V
    .locals 0

    .line 1227
    invoke-direct {p0}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;-><init>()V

    .line 1228
    iput-object p1, p0, Lepson/print/phlayout/PhotoPreview$MyScaledGestureListener;->mPreviewView:Lepson/print/phlayout/PhotoPreview;

    return-void
.end method


# virtual methods
.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 2

    .line 1240
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v0

    .line 1241
    iget-object v1, p0, Lepson/print/phlayout/PhotoPreview$MyScaledGestureListener;->mPreviewView:Lepson/print/phlayout/PhotoPreview;

    invoke-static {v1, v0}, Lepson/print/phlayout/PhotoPreview;->access$400(Lepson/print/phlayout/PhotoPreview;F)V

    .line 1243
    invoke-super {p0, p1}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;->onScale(Landroid/view/ScaleGestureDetector;)Z

    move-result p1

    return p1
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 1

    .line 1233
    iget-object v0, p0, Lepson/print/phlayout/PhotoPreview$MyScaledGestureListener;->mPreviewView:Lepson/print/phlayout/PhotoPreview;

    invoke-static {v0}, Lepson/print/phlayout/PhotoPreview;->access$300(Lepson/print/phlayout/PhotoPreview;)V

    .line 1235
    invoke-super {p0, p1}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;->onScaleBegin(Landroid/view/ScaleGestureDetector;)Z

    move-result p1

    return p1
.end method

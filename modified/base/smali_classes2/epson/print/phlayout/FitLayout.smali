.class public Lepson/print/phlayout/FitLayout;
.super Ljava/lang/Object;
.source "FitLayout.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public fitInscribed([D[D)[D
    .locals 12

    const/4 v0, 0x0

    .line 12
    aget-wide v1, p2, v0

    const/4 v3, 0x1

    aget-wide v4, p2, v3

    mul-double v1, v1, v4

    const-wide/16 v4, 0x0

    cmpg-double v6, v1, v4

    if-gtz v6, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    const/4 v1, 0x2

    .line 16
    aget-wide v4, p1, v1

    aget-wide v6, p1, v0

    sub-double/2addr v4, v6

    const/4 v2, 0x3

    .line 17
    aget-wide v6, p1, v2

    aget-wide v8, p1, v3

    sub-double/2addr v6, v8

    .line 19
    aget-wide v8, p2, v0

    div-double/2addr v4, v8

    .line 20
    aget-wide v8, p2, v3

    div-double/2addr v6, v8

    .line 22
    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(DD)D

    move-result-wide v4

    .line 24
    new-array p2, v2, [D

    aget-wide v6, p1, v0

    aget-wide v8, p1, v1

    add-double/2addr v6, v8

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    div-double/2addr v6, v8

    aput-wide v6, p2, v0

    aget-wide v6, p1, v3

    aget-wide v10, p1, v2

    add-double/2addr v6, v10

    div-double/2addr v6, v8

    aput-wide v6, p2, v3

    aput-wide v4, p2, v1

    return-object p2
.end method

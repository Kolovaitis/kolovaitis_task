.class public Lepson/print/phlayout/PhotoPreviewImageList$EpImageWrapper;
.super Ljava/lang/Object;
.source "PhotoPreviewImageList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/phlayout/PhotoPreviewImageList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "EpImageWrapper"
.end annotation


# instance fields
.field public epImage:Lepson/print/EPImage;

.field public orgFilename:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .line 192
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 193
    new-instance v0, Lepson/print/EPImage;

    invoke-direct {v0, p1, p3}, Lepson/print/EPImage;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lepson/print/phlayout/PhotoPreviewImageList$EpImageWrapper;->epImage:Lepson/print/EPImage;

    .line 194
    iput-object p2, p0, Lepson/print/phlayout/PhotoPreviewImageList$EpImageWrapper;->orgFilename:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getArgumentFilename()Ljava/lang/String;
    .locals 1

    .line 203
    iget-object v0, p0, Lepson/print/phlayout/PhotoPreviewImageList$EpImageWrapper;->orgFilename:Ljava/lang/String;

    if-eqz v0, :cond_0

    return-object v0

    .line 206
    :cond_0
    iget-object v0, p0, Lepson/print/phlayout/PhotoPreviewImageList$EpImageWrapper;->epImage:Lepson/print/EPImage;

    invoke-virtual {v0}, Lepson/print/EPImage;->getOriginalFileName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

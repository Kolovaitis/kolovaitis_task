.class interface abstract Lepson/print/phlayout/ILayoutPosition;
.super Ljava/lang/Object;
.source "ILayoutPosition.java"


# virtual methods
.method public abstract copyPreviewPrintAreaFromEpImage(Lepson/print/EPImage;)V
    .param p1    # Lepson/print/EPImage;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract copyPreviewPrintAreaToEpImage(Lepson/print/EPImage;)V
.end method

.method public abstract getBottomMargin()I
.end method

.method public abstract getIsPaperLandscape()Z
.end method

.method public abstract getLayoutId()I
.end method

.method public abstract getLeftMargin()I
.end method

.method public abstract getPreviewImageCircumscribedTargetSize()Landroid/graphics/RectF;
.end method

.method public abstract getPreviewPaperRect()Landroid/graphics/Rect;
.end method

.method public abstract getRightMargin()I
.end method

.method public abstract getTopMargin()I
.end method

.method public abstract isPreviewImageSizeValid()Z
.end method

.method public abstract setLayoutId(II)V
.end method

.method public abstract setPaperAndCalcPreviewPosition(Lepson/common/Info_paper;IILandroid/graphics/Point;)V
.end method

.method public abstract setPaperLandscape(Z)V
.end method

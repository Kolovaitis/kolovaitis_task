.class public Lepson/print/phlayout/AltRectF;
.super Ljava/lang/Object;
.source "AltRectF.java"


# instance fields
.field bottom:F

.field left:F

.field right:F

.field top:F


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(FFFF)V
    .locals 0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput p1, p0, Lepson/print/phlayout/AltRectF;->left:F

    .line 28
    iput p2, p0, Lepson/print/phlayout/AltRectF;->top:F

    .line 29
    iput p3, p0, Lepson/print/phlayout/AltRectF;->right:F

    .line 30
    iput p4, p0, Lepson/print/phlayout/AltRectF;->bottom:F

    return-void
.end method

.method public constructor <init>(Lepson/print/phlayout/AltRect;)V
    .locals 1
    .param p1    # Lepson/print/phlayout/AltRect;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iget v0, p1, Lepson/print/phlayout/AltRect;->left:I

    int-to-float v0, v0

    iput v0, p0, Lepson/print/phlayout/AltRectF;->left:F

    .line 21
    iget v0, p1, Lepson/print/phlayout/AltRect;->top:I

    int-to-float v0, v0

    iput v0, p0, Lepson/print/phlayout/AltRectF;->top:F

    .line 22
    iget v0, p1, Lepson/print/phlayout/AltRect;->right:I

    int-to-float v0, v0

    iput v0, p0, Lepson/print/phlayout/AltRectF;->right:F

    .line 23
    iget p1, p1, Lepson/print/phlayout/AltRect;->bottom:I

    int-to-float p1, p1

    iput p1, p0, Lepson/print/phlayout/AltRectF;->bottom:F

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_6

    .line 47
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    .line 49
    :cond_1
    check-cast p1, Lepson/print/phlayout/AltRectF;

    .line 51
    iget v2, p0, Lepson/print/phlayout/AltRectF;->left:F

    iget v3, p1, Lepson/print/phlayout/AltRectF;->left:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_2

    return v1

    .line 52
    :cond_2
    iget v2, p0, Lepson/print/phlayout/AltRectF;->top:F

    iget v3, p1, Lepson/print/phlayout/AltRectF;->top:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_3

    return v1

    .line 53
    :cond_3
    iget v2, p0, Lepson/print/phlayout/AltRectF;->right:F

    iget v3, p1, Lepson/print/phlayout/AltRectF;->right:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_4

    return v1

    .line 54
    :cond_4
    iget v2, p0, Lepson/print/phlayout/AltRectF;->bottom:F

    iget p1, p1, Lepson/print/phlayout/AltRectF;->bottom:F

    cmpl-float p1, v2, p1

    if-nez p1, :cond_5

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_6
    :goto_1
    return v1
.end method

.method getIntVal()[I
    .locals 3
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    const/4 v0, 0x4

    .line 41
    new-array v0, v0, [I

    iget v1, p0, Lepson/print/phlayout/AltRectF;->left:F

    float-to-int v1, v1

    const/4 v2, 0x0

    aput v1, v0, v2

    iget v1, p0, Lepson/print/phlayout/AltRectF;->top:F

    float-to-int v1, v1

    const/4 v2, 0x1

    aput v1, v0, v2

    iget v1, p0, Lepson/print/phlayout/AltRectF;->right:F

    float-to-int v1, v1

    const/4 v2, 0x2

    aput v1, v0, v2

    iget v1, p0, Lepson/print/phlayout/AltRectF;->bottom:F

    float-to-int v1, v1

    const/4 v2, 0x3

    aput v1, v0, v2

    return-object v0
.end method

.method getRectF()Landroid/graphics/RectF;
    .locals 5

    .line 34
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p0, Lepson/print/phlayout/AltRectF;->left:F

    iget v2, p0, Lepson/print/phlayout/AltRectF;->top:F

    iget v3, p0, Lepson/print/phlayout/AltRectF;->right:F

    iget v4, p0, Lepson/print/phlayout/AltRectF;->bottom:F

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .line 59
    iget v0, p0, Lepson/print/phlayout/AltRectF;->left:F

    float-to-int v0, v0

    mul-int/lit8 v0, v0, 0x1f

    int-to-float v0, v0

    .line 60
    iget v1, p0, Lepson/print/phlayout/AltRectF;->top:F

    add-float/2addr v0, v1

    float-to-int v0, v0

    mul-int/lit8 v0, v0, 0x1f

    int-to-float v0, v0

    .line 61
    iget v1, p0, Lepson/print/phlayout/AltRectF;->right:F

    add-float/2addr v0, v1

    float-to-int v0, v0

    mul-int/lit8 v0, v0, 0x1f

    int-to-float v0, v0

    .line 62
    iget v1, p0, Lepson/print/phlayout/AltRectF;->bottom:F

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 68
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x20

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "AltRectF("

    .line 69
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    iget v1, p0, Lepson/print/phlayout/AltRectF;->left:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ","

    .line 71
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    iget v1, p0, Lepson/print/phlayout/AltRectF;->top:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ","

    .line 73
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    iget v1, p0, Lepson/print/phlayout/AltRectF;->right:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ","

    .line 75
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    iget v1, p0, Lepson/print/phlayout/AltRectF;->bottom:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ")"

    .line 77
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

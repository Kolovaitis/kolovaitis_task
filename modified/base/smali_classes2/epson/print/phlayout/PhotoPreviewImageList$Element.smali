.class public Lepson/print/phlayout/PhotoPreviewImageList$Element;
.super Ljava/lang/Object;
.source "PhotoPreviewImageList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/phlayout/PhotoPreviewImageList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Element"
.end annotation


# instance fields
.field private mIndex:I

.field final synthetic this$0:Lepson/print/phlayout/PhotoPreviewImageList;


# direct methods
.method constructor <init>(Lepson/print/phlayout/PhotoPreviewImageList;I)V
    .locals 0
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .line 160
    iput-object p1, p0, Lepson/print/phlayout/PhotoPreviewImageList$Element;->this$0:Lepson/print/phlayout/PhotoPreviewImageList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 161
    iput p2, p0, Lepson/print/phlayout/PhotoPreviewImageList$Element;->mIndex:I

    return-void
.end method


# virtual methods
.method public getEPImage()Lepson/print/EPImage;
    .locals 2

    .line 165
    iget-object v0, p0, Lepson/print/phlayout/PhotoPreviewImageList$Element;->this$0:Lepson/print/phlayout/PhotoPreviewImageList;

    iget v1, p0, Lepson/print/phlayout/PhotoPreviewImageList$Element;->mIndex:I

    invoke-virtual {v0, v1}, Lepson/print/phlayout/PhotoPreviewImageList;->get(I)Lepson/print/EPImage;

    move-result-object v0

    return-object v0
.end method

.method public getFitMode()I
    .locals 2

    .line 174
    iget-object v0, p0, Lepson/print/phlayout/PhotoPreviewImageList$Element;->this$0:Lepson/print/phlayout/PhotoPreviewImageList;

    invoke-static {v0}, Lepson/print/phlayout/PhotoPreviewImageList;->access$000(Lepson/print/phlayout/PhotoPreviewImageList;)Ljava/util/ArrayList;

    move-result-object v0

    iget v1, p0, Lepson/print/phlayout/PhotoPreviewImageList$Element;->mIndex:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/print/phlayout/PhotoPreviewImageList$LayoutInfo;

    iget v0, v0, Lepson/print/phlayout/PhotoPreviewImageList$LayoutInfo;->fitMode:I

    return v0
.end method

.method public setFitMode(I)V
    .locals 2

    .line 169
    iget-object v0, p0, Lepson/print/phlayout/PhotoPreviewImageList$Element;->this$0:Lepson/print/phlayout/PhotoPreviewImageList;

    invoke-static {v0}, Lepson/print/phlayout/PhotoPreviewImageList;->access$000(Lepson/print/phlayout/PhotoPreviewImageList;)Ljava/util/ArrayList;

    move-result-object v0

    iget v1, p0, Lepson/print/phlayout/PhotoPreviewImageList$Element;->mIndex:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/print/phlayout/PhotoPreviewImageList$LayoutInfo;

    iput p1, v0, Lepson/print/phlayout/PhotoPreviewImageList$LayoutInfo;->fitMode:I

    return-void
.end method

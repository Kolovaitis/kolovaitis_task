.class Lepson/print/phlayout/BorderlessLayoutPosition;
.super Ljava/lang/Object;
.source "BorderlessLayoutPosition.java"

# interfaces
.implements Lepson/print/phlayout/ILayoutPosition;


# instance fields
.field private layout:I

.field private layoutMulti:I

.field private mBorderlessPrintArea:[I

.field private mBottomMargin:I

.field private mIsPaperLandScape:Z

.field private mLeftMargin:I

.field private mPaperHeight:I

.field private mPaperWidth:I

.field private mPreviewPaperRect:Lepson/print/phlayout/AltRect;

.field private mPreviewPrintAreaRect:Lepson/print/phlayout/AltRectF;

.field private mRightMargin:I

.field private mTopMargin:I


# direct methods
.method constructor <init>()V
    .locals 2

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    .line 17
    iput v0, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->layout:I

    const/4 v1, 0x0

    .line 18
    iput v1, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->layoutMulti:I

    .line 20
    iput v1, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPaperWidth:I

    .line 21
    iput v1, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPaperHeight:I

    .line 24
    new-array v0, v0, [I

    iput-object v0, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mBorderlessPrintArea:[I

    .line 27
    iput v1, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mLeftMargin:I

    .line 28
    iput v1, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mTopMargin:I

    .line 29
    iput v1, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mRightMargin:I

    .line 30
    iput v1, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mBottomMargin:I

    .line 32
    iput-boolean v1, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mIsPaperLandScape:Z

    .line 34
    new-instance v0, Lepson/print/phlayout/AltRect;

    invoke-direct {v0}, Lepson/print/phlayout/AltRect;-><init>()V

    iput-object v0, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPreviewPaperRect:Lepson/print/phlayout/AltRect;

    .line 37
    new-instance v0, Lepson/print/phlayout/AltRectF;

    invoke-direct {v0}, Lepson/print/phlayout/AltRectF;-><init>()V

    iput-object v0, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPreviewPrintAreaRect:Lepson/print/phlayout/AltRectF;

    return-void
.end method


# virtual methods
.method calculateLayoutSizeBorderless(IILandroid/graphics/Point;)V
    .locals 18
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, p2

    move-object/from16 v3, p3

    const/4 v4, 0x4

    .line 174
    new-array v4, v4, [I

    .line 180
    iget-boolean v5, v0, Lepson/print/phlayout/BorderlessLayoutPosition;->mIsPaperLandScape:Z

    const/4 v8, 0x1

    const/4 v9, 0x0

    const-wide v10, 0x3fe99999a0000000L    # 0.800000011920929

    if-eqz v5, :cond_0

    iget v5, v0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPaperWidth:I

    iget v12, v0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPaperHeight:I

    if-le v5, v12, :cond_1

    :cond_0
    iget-boolean v5, v0, Lepson/print/phlayout/BorderlessLayoutPosition;->mIsPaperLandScape:Z

    if-nez v5, :cond_3

    iget v5, v0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPaperWidth:I

    iget v12, v0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPaperHeight:I

    if-le v5, v12, :cond_3

    :cond_1
    int-to-double v12, v1

    mul-double v12, v12, v10

    double-to-int v5, v12

    .line 184
    iget v12, v0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPaperWidth:I

    mul-int v13, v5, v12

    int-to-float v13, v13

    iget v14, v0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPaperHeight:I

    int-to-float v15, v14

    div-float/2addr v13, v15

    float-to-int v13, v13

    .line 186
    iget-object v15, v0, Lepson/print/phlayout/BorderlessLayoutPosition;->mBorderlessPrintArea:[I

    aget v16, v15, v8

    mul-int v6, v5, v16

    int-to-float v6, v6

    int-to-float v7, v14

    div-float/2addr v6, v7

    float-to-int v6, v6

    .line 187
    aget v7, v15, v9

    mul-int v7, v7, v13

    int-to-float v7, v7

    int-to-float v9, v12

    div-float/2addr v7, v9

    float-to-int v7, v7

    int-to-double v8, v2

    mul-double v8, v8, v10

    double-to-int v8, v8

    if-le v13, v8, :cond_2

    mul-int v5, v8, v14

    int-to-float v5, v5

    int-to-float v6, v12

    div-float/2addr v5, v6

    float-to-int v5, v5

    const/4 v6, 0x1

    .line 194
    aget v7, v15, v6

    mul-int v7, v7, v5

    int-to-float v6, v7

    int-to-float v7, v14

    div-float/2addr v6, v7

    float-to-int v6, v6

    const/4 v7, 0x0

    .line 195
    aget v9, v15, v7

    mul-int v9, v9, v8

    int-to-float v7, v9

    int-to-float v9, v12

    div-float/2addr v7, v9

    float-to-int v7, v7

    move v13, v8

    :cond_2
    int-to-float v8, v5

    .line 199
    iget v9, v0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPaperHeight:I

    int-to-float v9, v9

    div-float/2addr v8, v9

    .line 200
    iget v9, v0, Lepson/print/phlayout/BorderlessLayoutPosition;->mTopMargin:I

    int-to-float v9, v9

    mul-float v9, v9, v8

    float-to-int v9, v9

    const/4 v10, 0x0

    aput v9, v4, v10

    .line 201
    iget v9, v0, Lepson/print/phlayout/BorderlessLayoutPosition;->mRightMargin:I

    int-to-float v9, v9

    mul-float v9, v9, v8

    float-to-int v9, v9

    const/4 v10, 0x1

    aput v9, v4, v10

    .line 202
    iget v9, v0, Lepson/print/phlayout/BorderlessLayoutPosition;->mBottomMargin:I

    int-to-float v9, v9

    mul-float v9, v9, v8

    float-to-int v9, v9

    const/4 v10, 0x2

    aput v9, v4, v10

    .line 203
    iget v9, v0, Lepson/print/phlayout/BorderlessLayoutPosition;->mLeftMargin:I

    int-to-float v9, v9

    mul-float v9, v9, v8

    float-to-int v8, v9

    const/4 v9, 0x3

    aput v8, v4, v9

    const/4 v10, 0x2

    goto/16 :goto_1

    :cond_3
    int-to-double v5, v2

    mul-double v5, v5, v10

    double-to-int v5, v5

    .line 206
    iget v6, v0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPaperWidth:I

    mul-int v7, v5, v6

    int-to-float v7, v7

    iget v8, v0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPaperHeight:I

    int-to-float v9, v8

    div-float/2addr v7, v9

    float-to-int v7, v7

    .line 207
    iget-object v9, v0, Lepson/print/phlayout/BorderlessLayoutPosition;->mBorderlessPrintArea:[I

    const/4 v12, 0x0

    aget v13, v9, v12

    mul-int v13, v13, v7

    int-to-float v12, v13

    int-to-float v13, v6

    div-float/2addr v12, v13

    float-to-int v12, v12

    const/4 v13, 0x1

    .line 208
    aget v14, v9, v13

    mul-int v14, v14, v5

    int-to-float v13, v14

    int-to-float v14, v8

    div-float/2addr v13, v14

    float-to-int v13, v13

    int-to-double v14, v1

    mul-double v14, v14, v10

    double-to-int v10, v14

    if-le v7, v10, :cond_4

    mul-int v5, v10, v8

    int-to-float v5, v5

    int-to-float v7, v6

    div-float/2addr v5, v7

    float-to-int v5, v5

    const/4 v7, 0x0

    .line 215
    aget v11, v9, v7

    mul-int v11, v11, v10

    int-to-float v7, v11

    int-to-float v6, v6

    div-float/2addr v7, v6

    float-to-int v6, v7

    const/4 v7, 0x1

    .line 216
    aget v9, v9, v7

    mul-int v9, v9, v5

    int-to-float v7, v9

    int-to-float v8, v8

    div-float/2addr v7, v8

    float-to-int v7, v7

    move v13, v5

    move v5, v10

    goto :goto_0

    :cond_4
    move v6, v12

    move/from16 v17, v13

    move v13, v5

    move v5, v7

    move/from16 v7, v17

    :goto_0
    int-to-float v8, v13

    .line 221
    iget v9, v0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPaperHeight:I

    int-to-float v9, v9

    div-float/2addr v8, v9

    .line 222
    iget v9, v0, Lepson/print/phlayout/BorderlessLayoutPosition;->mLeftMargin:I

    int-to-float v9, v9

    mul-float v9, v9, v8

    float-to-int v9, v9

    const/4 v10, 0x0

    aput v9, v4, v10

    .line 223
    iget v9, v0, Lepson/print/phlayout/BorderlessLayoutPosition;->mTopMargin:I

    int-to-float v9, v9

    mul-float v9, v9, v8

    float-to-int v9, v9

    const/4 v10, 0x1

    aput v9, v4, v10

    .line 224
    iget v9, v0, Lepson/print/phlayout/BorderlessLayoutPosition;->mRightMargin:I

    int-to-float v9, v9

    mul-float v9, v9, v8

    float-to-int v9, v9

    const/4 v10, 0x2

    aput v9, v4, v10

    .line 225
    iget v9, v0, Lepson/print/phlayout/BorderlessLayoutPosition;->mBottomMargin:I

    int-to-float v9, v9

    mul-float v9, v9, v8

    float-to-int v8, v9

    const/4 v9, 0x3

    aput v8, v4, v9

    .line 229
    :goto_1
    iget-object v8, v0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPreviewPaperRect:Lepson/print/phlayout/AltRect;

    sub-int/2addr v1, v5

    div-int/2addr v1, v10

    iput v1, v8, Lepson/print/phlayout/AltRect;->left:I

    sub-int v1, v2, v13

    .line 230
    div-int/2addr v1, v10

    iput v1, v8, Lepson/print/phlayout/AltRect;->top:I

    .line 231
    iget v1, v8, Lepson/print/phlayout/AltRect;->left:I

    add-int/2addr v1, v5

    iput v1, v8, Lepson/print/phlayout/AltRect;->right:I

    .line 232
    iget-object v1, v0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPreviewPaperRect:Lepson/print/phlayout/AltRect;

    iget v2, v1, Lepson/print/phlayout/AltRect;->top:I

    add-int/2addr v2, v13

    iput v2, v1, Lepson/print/phlayout/AltRect;->bottom:I

    .line 237
    new-instance v1, Lepson/print/phlayout/AltRectF;

    invoke-direct {v1}, Lepson/print/phlayout/AltRectF;-><init>()V

    iput-object v1, v0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPreviewPrintAreaRect:Lepson/print/phlayout/AltRectF;

    .line 238
    iget-object v1, v0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPreviewPrintAreaRect:Lepson/print/phlayout/AltRectF;

    iget-object v2, v0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPreviewPaperRect:Lepson/print/phlayout/AltRect;

    iget v2, v2, Lepson/print/phlayout/AltRect;->left:I

    const/4 v5, 0x0

    aget v5, v4, v5

    sub-int/2addr v2, v5

    int-to-float v2, v2

    iput v2, v1, Lepson/print/phlayout/AltRectF;->left:F

    .line 239
    iget-object v1, v0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPreviewPrintAreaRect:Lepson/print/phlayout/AltRectF;

    iget-object v2, v0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPreviewPaperRect:Lepson/print/phlayout/AltRect;

    iget v2, v2, Lepson/print/phlayout/AltRect;->top:I

    const/4 v5, 0x1

    aget v4, v4, v5

    sub-int/2addr v2, v4

    int-to-float v2, v2

    iput v2, v1, Lepson/print/phlayout/AltRectF;->top:F

    .line 240
    iget-object v1, v0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPreviewPrintAreaRect:Lepson/print/phlayout/AltRectF;

    iget v2, v1, Lepson/print/phlayout/AltRectF;->left:F

    int-to-float v4, v6

    add-float/2addr v2, v4

    iput v2, v1, Lepson/print/phlayout/AltRectF;->right:F

    .line 241
    iget-object v1, v0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPreviewPrintAreaRect:Lepson/print/phlayout/AltRectF;

    iget v2, v1, Lepson/print/phlayout/AltRectF;->top:F

    int-to-float v4, v7

    add-float/2addr v2, v4

    iput v2, v1, Lepson/print/phlayout/AltRectF;->bottom:F

    .line 245
    iget-object v1, v0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPreviewPrintAreaRect:Lepson/print/phlayout/AltRectF;

    iget v1, v1, Lepson/print/phlayout/AltRectF;->right:F

    iget-object v2, v0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPreviewPrintAreaRect:Lepson/print/phlayout/AltRectF;

    iget v2, v2, Lepson/print/phlayout/AltRectF;->left:F

    sub-float/2addr v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    iget-object v4, v0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPreviewPrintAreaRect:Lepson/print/phlayout/AltRectF;

    iget v4, v4, Lepson/print/phlayout/AltRectF;->left:F

    add-float/2addr v1, v4

    float-to-int v1, v1

    iput v1, v3, Landroid/graphics/Point;->x:I

    .line 246
    iget-object v1, v0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPreviewPrintAreaRect:Lepson/print/phlayout/AltRectF;

    iget v1, v1, Lepson/print/phlayout/AltRectF;->bottom:F

    iget-object v4, v0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPreviewPrintAreaRect:Lepson/print/phlayout/AltRectF;

    iget v4, v4, Lepson/print/phlayout/AltRectF;->top:F

    sub-float/2addr v1, v4

    div-float/2addr v1, v2

    iget-object v2, v0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPreviewPrintAreaRect:Lepson/print/phlayout/AltRectF;

    iget v2, v2, Lepson/print/phlayout/AltRectF;->top:F

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v3, Landroid/graphics/Point;->y:I

    return-void
.end method

.method public copyPreviewPrintAreaFromEpImage(Lepson/print/EPImage;)V
    .locals 2
    .param p1    # Lepson/print/EPImage;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 284
    iget-boolean v0, p1, Lepson/print/EPImage;->isPaperLandScape:Z

    iput-boolean v0, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mIsPaperLandScape:Z

    .line 286
    iget-object v0, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPreviewPrintAreaRect:Lepson/print/phlayout/AltRectF;

    iget v1, p1, Lepson/print/EPImage;->previewPaperRectLeft:I

    int-to-float v1, v1

    iput v1, v0, Lepson/print/phlayout/AltRectF;->left:F

    .line 287
    iget-object v0, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPreviewPrintAreaRect:Lepson/print/phlayout/AltRectF;

    iget v1, p1, Lepson/print/EPImage;->previewPaperRectTop:I

    int-to-float v1, v1

    iput v1, v0, Lepson/print/phlayout/AltRectF;->top:F

    .line 288
    iget-object v0, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPreviewPrintAreaRect:Lepson/print/phlayout/AltRectF;

    iget v1, p1, Lepson/print/EPImage;->previewPaperRectRight:I

    int-to-float v1, v1

    iput v1, v0, Lepson/print/phlayout/AltRectF;->right:F

    .line 289
    iget-object v0, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPreviewPrintAreaRect:Lepson/print/phlayout/AltRectF;

    iget p1, p1, Lepson/print/EPImage;->previewPaperRectBottom:I

    int-to-float p1, p1

    iput p1, v0, Lepson/print/phlayout/AltRectF;->bottom:F

    return-void
.end method

.method public copyPreviewPrintAreaToEpImage(Lepson/print/EPImage;)V
    .locals 1

    .line 301
    iget-boolean v0, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mIsPaperLandScape:Z

    iput-boolean v0, p1, Lepson/print/EPImage;->isPaperLandScape:Z

    .line 303
    iget-object v0, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPreviewPrintAreaRect:Lepson/print/phlayout/AltRectF;

    iget v0, v0, Lepson/print/phlayout/AltRectF;->left:F

    float-to-int v0, v0

    iput v0, p1, Lepson/print/EPImage;->previewPaperRectLeft:I

    .line 304
    iget-object v0, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPreviewPrintAreaRect:Lepson/print/phlayout/AltRectF;

    iget v0, v0, Lepson/print/phlayout/AltRectF;->top:F

    float-to-int v0, v0

    iput v0, p1, Lepson/print/EPImage;->previewPaperRectTop:I

    .line 305
    iget-object v0, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPreviewPrintAreaRect:Lepson/print/phlayout/AltRectF;

    iget v0, v0, Lepson/print/phlayout/AltRectF;->right:F

    float-to-int v0, v0

    iput v0, p1, Lepson/print/EPImage;->previewPaperRectRight:I

    .line 306
    iget-object v0, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPreviewPrintAreaRect:Lepson/print/phlayout/AltRectF;

    iget v0, v0, Lepson/print/phlayout/AltRectF;->bottom:F

    float-to-int v0, v0

    iput v0, p1, Lepson/print/EPImage;->previewPaperRectBottom:I

    return-void
.end method

.method public getBottomMargin()I
    .locals 2

    .line 60
    iget-object v0, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPreviewPrintAreaRect:Lepson/print/phlayout/AltRectF;

    iget v0, v0, Lepson/print/phlayout/AltRectF;->bottom:F

    iget-object v1, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPreviewPaperRect:Lepson/print/phlayout/AltRect;

    iget v1, v1, Lepson/print/phlayout/AltRect;->bottom:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public getIsPaperLandscape()Z
    .locals 1

    .line 85
    iget-boolean v0, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mIsPaperLandScape:Z

    return v0
.end method

.method public getLayoutId()I
    .locals 1

    .line 40
    iget v0, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->layout:I

    return v0
.end method

.method public getLeftMargin()I
    .locals 2

    .line 48
    iget-object v0, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPreviewPaperRect:Lepson/print/phlayout/AltRect;

    iget v0, v0, Lepson/print/phlayout/AltRect;->left:I

    int-to-float v0, v0

    iget-object v1, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPreviewPrintAreaRect:Lepson/print/phlayout/AltRectF;

    iget v1, v1, Lepson/print/phlayout/AltRectF;->left:F

    sub-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method getPreviewImageAltRect()Lepson/print/phlayout/AltRectF;
    .locals 1

    .line 77
    iget-object v0, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPreviewPrintAreaRect:Lepson/print/phlayout/AltRectF;

    return-object v0
.end method

.method public getPreviewImageCircumscribedTargetSize()Landroid/graphics/RectF;
    .locals 1

    .line 68
    iget-object v0, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPreviewPrintAreaRect:Lepson/print/phlayout/AltRectF;

    invoke-virtual {v0}, Lepson/print/phlayout/AltRectF;->getRectF()Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method getPreviewPaperAltRect()Lepson/print/phlayout/AltRect;
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .line 94
    iget-object v0, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPreviewPaperRect:Lepson/print/phlayout/AltRect;

    return-object v0
.end method

.method public getPreviewPaperRect()Landroid/graphics/Rect;
    .locals 1

    .line 89
    iget-object v0, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPreviewPaperRect:Lepson/print/phlayout/AltRect;

    invoke-virtual {v0}, Lepson/print/phlayout/AltRect;->getRect()Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public getRightMargin()I
    .locals 2

    .line 52
    iget-object v0, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPreviewPrintAreaRect:Lepson/print/phlayout/AltRectF;

    iget v0, v0, Lepson/print/phlayout/AltRectF;->right:F

    iget-object v1, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPreviewPaperRect:Lepson/print/phlayout/AltRect;

    iget v1, v1, Lepson/print/phlayout/AltRect;->right:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public getTopMargin()I
    .locals 2

    .line 56
    iget-object v0, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPreviewPaperRect:Lepson/print/phlayout/AltRect;

    iget v0, v0, Lepson/print/phlayout/AltRect;->top:I

    int-to-float v0, v0

    iget-object v1, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPreviewPrintAreaRect:Lepson/print/phlayout/AltRectF;

    iget v1, v1, Lepson/print/phlayout/AltRectF;->top:F

    sub-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public isPreviewImageSizeValid()Z
    .locals 2

    .line 72
    iget-object v0, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPreviewPaperRect:Lepson/print/phlayout/AltRect;

    iget v0, v0, Lepson/print/phlayout/AltRect;->right:I

    iget-object v1, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPreviewPaperRect:Lepson/print/phlayout/AltRect;

    iget v1, v1, Lepson/print/phlayout/AltRect;->left:I

    sub-int/2addr v0, v1

    if-lez v0, :cond_0

    iget-object v0, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPreviewPaperRect:Lepson/print/phlayout/AltRect;

    iget v0, v0, Lepson/print/phlayout/AltRect;->bottom:I

    iget-object v1, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPreviewPaperRect:Lepson/print/phlayout/AltRect;

    iget v1, v1, Lepson/print/phlayout/AltRect;->top:I

    sub-int/2addr v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public setLayoutId(II)V
    .locals 0

    .line 123
    iput p1, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->layout:I

    .line 124
    iput p2, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->layoutMulti:I

    return-void
.end method

.method setMargin_forTest([I)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    const/4 v0, 0x0

    .line 111
    aget v0, p1, v0

    iput v0, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mLeftMargin:I

    const/4 v0, 0x1

    .line 112
    aget v0, p1, v0

    iput v0, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mTopMargin:I

    const/4 v0, 0x2

    .line 113
    aget v0, p1, v0

    iput v0, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mRightMargin:I

    const/4 v0, 0x3

    .line 114
    aget p1, p1, v0

    iput p1, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mBottomMargin:I

    return-void
.end method

.method public setPaperAndCalcPreviewPosition(Lepson/common/Info_paper;IILandroid/graphics/Point;)V
    .locals 0

    .line 128
    invoke-virtual {p0, p1}, Lepson/print/phlayout/BorderlessLayoutPosition;->setPaperSizeFromPaperInfo(Lepson/common/Info_paper;)V

    .line 130
    iget p1, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPaperWidth:I

    if-lez p1, :cond_0

    iget p1, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPaperHeight:I

    if-gtz p1, :cond_1

    :cond_0
    const/16 p1, 0xb4c

    .line 131
    iput p1, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPaperWidth:I

    const/16 p1, 0x101d

    .line 132
    iput p1, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPaperHeight:I

    .line 135
    :cond_1
    invoke-virtual {p0, p2, p3, p4}, Lepson/print/phlayout/BorderlessLayoutPosition;->calculateLayoutSizeBorderless(IILandroid/graphics/Point;)V

    return-void
.end method

.method setPaperAndImageSize_forTest(IIII)V
    .locals 0
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .line 103
    iput p1, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPaperWidth:I

    .line 104
    iput p2, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPaperHeight:I

    .line 105
    iget-object p1, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mBorderlessPrintArea:[I

    const/4 p2, 0x0

    aput p3, p1, p2

    const/4 p2, 0x1

    .line 106
    aput p4, p1, p2

    return-void
.end method

.method public setPaperLandscape(Z)V
    .locals 0

    .line 81
    iput-boolean p1, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mIsPaperLandScape:Z

    return-void
.end method

.method setPaperSizeFromPaperInfo(Lepson/common/Info_paper;)V
    .locals 3
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .line 267
    invoke-virtual {p1}, Lepson/common/Info_paper;->getPaper_width()I

    move-result v0

    iput v0, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPaperWidth:I

    .line 268
    invoke-virtual {p1}, Lepson/common/Info_paper;->getPaper_height()I

    move-result v0

    iput v0, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mPaperHeight:I

    .line 270
    iget-object v0, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mBorderlessPrintArea:[I

    invoke-virtual {p1}, Lepson/common/Info_paper;->getPaper_width_boderless()I

    move-result v1

    const/4 v2, 0x0

    aput v1, v0, v2

    .line 271
    iget-object v0, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mBorderlessPrintArea:[I

    invoke-virtual {p1}, Lepson/common/Info_paper;->getPaper_height_boderless()I

    move-result v1

    const/4 v2, 0x1

    aput v1, v0, v2

    .line 274
    invoke-virtual {p1}, Lepson/common/Info_paper;->getLeftMargin()I

    move-result v0

    neg-int v0, v0

    iput v0, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mLeftMargin:I

    .line 275
    invoke-virtual {p1}, Lepson/common/Info_paper;->getTopMargin()I

    move-result v0

    neg-int v0, v0

    iput v0, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mTopMargin:I

    .line 276
    invoke-virtual {p1}, Lepson/common/Info_paper;->getRightMargin()I

    move-result v0

    neg-int v0, v0

    iput v0, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mRightMargin:I

    .line 277
    invoke-virtual {p1}, Lepson/common/Info_paper;->getBottomMargin()I

    move-result p1

    neg-int p1, p1

    iput p1, p0, Lepson/print/phlayout/BorderlessLayoutPosition;->mBottomMargin:I

    return-void
.end method

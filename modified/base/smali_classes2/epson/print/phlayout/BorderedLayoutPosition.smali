.class public Lepson/print/phlayout/BorderedLayoutPosition;
.super Ljava/lang/Object;
.source "BorderedLayoutPosition.java"

# interfaces
.implements Lepson/print/phlayout/ILayoutPosition;


# instance fields
.field private layout:I

.field private layoutMulti:I

.field private m3mmHeight:I

.field private m3mmWidth:I

.field private mBottomMargin:I

.field private mIsPaperLandScape:Z

.field private mLeftMargin:I

.field private mPreview3mmEffectivePrintPosition:Lepson/print/phlayout/AltRect;

.field private mPreviewMargin:[I

.field private mRightMargin:I

.field private mTopMargin:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    .line 20
    iput v0, p0, Lepson/print/phlayout/BorderedLayoutPosition;->layout:I

    const/4 v0, 0x0

    .line 21
    iput v0, p0, Lepson/print/phlayout/BorderedLayoutPosition;->layoutMulti:I

    .line 28
    iput v0, p0, Lepson/print/phlayout/BorderedLayoutPosition;->m3mmWidth:I

    .line 29
    iput v0, p0, Lepson/print/phlayout/BorderedLayoutPosition;->m3mmHeight:I

    .line 32
    iput v0, p0, Lepson/print/phlayout/BorderedLayoutPosition;->mLeftMargin:I

    .line 33
    iput v0, p0, Lepson/print/phlayout/BorderedLayoutPosition;->mTopMargin:I

    .line 34
    iput v0, p0, Lepson/print/phlayout/BorderedLayoutPosition;->mRightMargin:I

    .line 35
    iput v0, p0, Lepson/print/phlayout/BorderedLayoutPosition;->mBottomMargin:I

    const/4 v1, 0x4

    .line 37
    new-array v1, v1, [I

    iput-object v1, p0, Lepson/print/phlayout/BorderedLayoutPosition;->mPreviewMargin:[I

    .line 40
    iput-boolean v0, p0, Lepson/print/phlayout/BorderedLayoutPosition;->mIsPaperLandScape:Z

    .line 43
    new-instance v0, Lepson/print/phlayout/AltRect;

    invoke-direct {v0}, Lepson/print/phlayout/AltRect;-><init>()V

    iput-object v0, p0, Lepson/print/phlayout/BorderedLayoutPosition;->mPreview3mmEffectivePrintPosition:Lepson/print/phlayout/AltRect;

    return-void
.end method


# virtual methods
.method calculateLayoutSizeBordered(IILandroid/graphics/Point;)V
    .locals 8
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .line 199
    iget-boolean v0, p0, Lepson/print/phlayout/BorderedLayoutPosition;->mIsPaperLandScape:Z

    const-wide v1, 0x3fe99999a0000000L    # 0.800000011920929

    if-eqz v0, :cond_0

    iget v0, p0, Lepson/print/phlayout/BorderedLayoutPosition;->m3mmWidth:I

    iget v3, p0, Lepson/print/phlayout/BorderedLayoutPosition;->m3mmHeight:I

    if-le v0, v3, :cond_1

    :cond_0
    iget-boolean v0, p0, Lepson/print/phlayout/BorderedLayoutPosition;->mIsPaperLandScape:Z

    if-nez v0, :cond_2

    iget v0, p0, Lepson/print/phlayout/BorderedLayoutPosition;->m3mmWidth:I

    iget v3, p0, Lepson/print/phlayout/BorderedLayoutPosition;->m3mmHeight:I

    if-le v0, v3, :cond_2

    :cond_1
    int-to-double v3, p1

    mul-double v3, v3, v1

    double-to-int v0, v3

    .line 202
    iget v3, p0, Lepson/print/phlayout/BorderedLayoutPosition;->m3mmWidth:I

    mul-int v4, v0, v3

    int-to-float v4, v4

    iget v5, p0, Lepson/print/phlayout/BorderedLayoutPosition;->m3mmHeight:I

    int-to-float v6, v5

    div-float/2addr v4, v6

    float-to-int v4, v4

    int-to-double v6, p2

    mul-double v6, v6, v1

    double-to-int v1, v6

    if-le v4, v1, :cond_4

    mul-int v5, v5, v1

    int-to-float v0, v5

    int-to-float v2, v3

    div-float/2addr v0, v2

    float-to-int v0, v0

    move v4, v1

    goto :goto_0

    :cond_2
    int-to-double v3, p2

    mul-double v3, v3, v1

    double-to-int v4, v3

    .line 212
    iget v0, p0, Lepson/print/phlayout/BorderedLayoutPosition;->m3mmWidth:I

    mul-int v3, v4, v0

    int-to-float v3, v3

    iget v5, p0, Lepson/print/phlayout/BorderedLayoutPosition;->m3mmHeight:I

    int-to-float v6, v5

    div-float/2addr v3, v6

    float-to-int v3, v3

    int-to-double v6, p1

    mul-double v6, v6, v1

    double-to-int v1, v6

    if-le v3, v1, :cond_3

    mul-int v5, v5, v1

    int-to-float v2, v5

    int-to-float v0, v0

    div-float/2addr v2, v0

    float-to-int v4, v2

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v3

    .line 223
    :cond_4
    :goto_0
    iget-object v1, p0, Lepson/print/phlayout/BorderedLayoutPosition;->mPreview3mmEffectivePrintPosition:Lepson/print/phlayout/AltRect;

    sub-int/2addr p1, v0

    const/4 v2, 0x2

    div-int/2addr p1, v2

    iput p1, v1, Lepson/print/phlayout/AltRect;->left:I

    sub-int/2addr p2, v4

    .line 224
    div-int/2addr p2, v2

    iput p2, v1, Lepson/print/phlayout/AltRect;->top:I

    .line 225
    iget p1, v1, Lepson/print/phlayout/AltRect;->left:I

    add-int/2addr p1, v0

    iput p1, v1, Lepson/print/phlayout/AltRect;->right:I

    .line 226
    iget-object p1, p0, Lepson/print/phlayout/BorderedLayoutPosition;->mPreview3mmEffectivePrintPosition:Lepson/print/phlayout/AltRect;

    iget p2, p1, Lepson/print/phlayout/AltRect;->top:I

    add-int/2addr p2, v4

    iput p2, p1, Lepson/print/phlayout/AltRect;->bottom:I

    .line 228
    iget-object p1, p0, Lepson/print/phlayout/BorderedLayoutPosition;->mPreview3mmEffectivePrintPosition:Lepson/print/phlayout/AltRect;

    iget p1, p1, Lepson/print/phlayout/AltRect;->right:I

    iget-object p2, p0, Lepson/print/phlayout/BorderedLayoutPosition;->mPreview3mmEffectivePrintPosition:Lepson/print/phlayout/AltRect;

    iget p2, p2, Lepson/print/phlayout/AltRect;->left:I

    sub-int/2addr p1, p2

    div-int/2addr p1, v2

    iget-object p2, p0, Lepson/print/phlayout/BorderedLayoutPosition;->mPreview3mmEffectivePrintPosition:Lepson/print/phlayout/AltRect;

    iget p2, p2, Lepson/print/phlayout/AltRect;->left:I

    add-int/2addr p1, p2

    iput p1, p3, Landroid/graphics/Point;->x:I

    .line 229
    iget-object p1, p0, Lepson/print/phlayout/BorderedLayoutPosition;->mPreview3mmEffectivePrintPosition:Lepson/print/phlayout/AltRect;

    iget p1, p1, Lepson/print/phlayout/AltRect;->bottom:I

    iget-object p2, p0, Lepson/print/phlayout/BorderedLayoutPosition;->mPreview3mmEffectivePrintPosition:Lepson/print/phlayout/AltRect;

    iget p2, p2, Lepson/print/phlayout/AltRect;->top:I

    sub-int/2addr p1, p2

    div-int/2addr p1, v2

    iget-object p2, p0, Lepson/print/phlayout/BorderedLayoutPosition;->mPreview3mmEffectivePrintPosition:Lepson/print/phlayout/AltRect;

    iget p2, p2, Lepson/print/phlayout/AltRect;->top:I

    add-int/2addr p1, p2

    iput p1, p3, Landroid/graphics/Point;->y:I

    .line 231
    iget-object p1, p0, Lepson/print/phlayout/BorderedLayoutPosition;->mPreview3mmEffectivePrintPosition:Lepson/print/phlayout/AltRect;

    invoke-virtual {p1}, Lepson/print/phlayout/AltRect;->width()I

    move-result p1

    int-to-float p1, p1

    iget p2, p0, Lepson/print/phlayout/BorderedLayoutPosition;->m3mmWidth:I

    int-to-float p2, p2

    div-float/2addr p1, p2

    .line 232
    iget-object p2, p0, Lepson/print/phlayout/BorderedLayoutPosition;->mPreviewMargin:[I

    iget p3, p0, Lepson/print/phlayout/BorderedLayoutPosition;->mLeftMargin:I

    int-to-float p3, p3

    mul-float p3, p3, p1

    float-to-int p3, p3

    const/4 v0, 0x0

    aput p3, p2, v0

    .line 233
    iget p3, p0, Lepson/print/phlayout/BorderedLayoutPosition;->mTopMargin:I

    int-to-float p3, p3

    mul-float p3, p3, p1

    float-to-int p3, p3

    const/4 v1, 0x1

    aput p3, p2, v1

    .line 234
    iget p3, p0, Lepson/print/phlayout/BorderedLayoutPosition;->mRightMargin:I

    int-to-float p3, p3

    mul-float p3, p3, p1

    float-to-int p3, p3

    aput p3, p2, v2

    .line 235
    iget p3, p0, Lepson/print/phlayout/BorderedLayoutPosition;->mBottomMargin:I

    int-to-float p3, p3

    mul-float p3, p3, p1

    float-to-int p1, p3

    const/4 p3, 0x3

    aput p1, p2, p3

    .line 238
    iget-boolean p1, p0, Lepson/print/phlayout/BorderedLayoutPosition;->mIsPaperLandScape:Z

    if-eqz p1, :cond_5

    .line 240
    aget p1, p2, v1

    .line 241
    aget v3, p2, v2

    aput v3, p2, v1

    .line 242
    aget v1, p2, p3

    aput v1, p2, v2

    .line 243
    aget v1, p2, v0

    aput v1, p2, p3

    .line 244
    aput p1, p2, v0

    :cond_5
    return-void
.end method

.method public copyPreviewPrintAreaFromEpImage(Lepson/print/EPImage;)V
    .locals 2
    .param p1    # Lepson/print/EPImage;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 301
    iget-boolean v0, p1, Lepson/print/EPImage;->isPaperLandScape:Z

    iput-boolean v0, p0, Lepson/print/phlayout/BorderedLayoutPosition;->mIsPaperLandScape:Z

    .line 303
    iget-object v0, p0, Lepson/print/phlayout/BorderedLayoutPosition;->mPreview3mmEffectivePrintPosition:Lepson/print/phlayout/AltRect;

    iget v1, p1, Lepson/print/EPImage;->previewPaperRectLeft:I

    iput v1, v0, Lepson/print/phlayout/AltRect;->left:I

    .line 304
    iget-object v0, p0, Lepson/print/phlayout/BorderedLayoutPosition;->mPreview3mmEffectivePrintPosition:Lepson/print/phlayout/AltRect;

    iget v1, p1, Lepson/print/EPImage;->previewPaperRectTop:I

    iput v1, v0, Lepson/print/phlayout/AltRect;->top:I

    .line 305
    iget-object v0, p0, Lepson/print/phlayout/BorderedLayoutPosition;->mPreview3mmEffectivePrintPosition:Lepson/print/phlayout/AltRect;

    iget v1, p1, Lepson/print/EPImage;->previewPaperRectRight:I

    iput v1, v0, Lepson/print/phlayout/AltRect;->right:I

    .line 306
    iget-object v0, p0, Lepson/print/phlayout/BorderedLayoutPosition;->mPreview3mmEffectivePrintPosition:Lepson/print/phlayout/AltRect;

    iget p1, p1, Lepson/print/EPImage;->previewPaperRectBottom:I

    iput p1, v0, Lepson/print/phlayout/AltRect;->bottom:I

    return-void
.end method

.method public copyPreviewPrintAreaToEpImage(Lepson/print/EPImage;)V
    .locals 1

    .line 312
    iget-boolean v0, p0, Lepson/print/phlayout/BorderedLayoutPosition;->mIsPaperLandScape:Z

    iput-boolean v0, p1, Lepson/print/EPImage;->isPaperLandScape:Z

    .line 317
    iget-object v0, p0, Lepson/print/phlayout/BorderedLayoutPosition;->mPreview3mmEffectivePrintPosition:Lepson/print/phlayout/AltRect;

    iget v0, v0, Lepson/print/phlayout/AltRect;->left:I

    iput v0, p1, Lepson/print/EPImage;->previewPaperRectLeft:I

    .line 318
    iget-object v0, p0, Lepson/print/phlayout/BorderedLayoutPosition;->mPreview3mmEffectivePrintPosition:Lepson/print/phlayout/AltRect;

    iget v0, v0, Lepson/print/phlayout/AltRect;->top:I

    iput v0, p1, Lepson/print/EPImage;->previewPaperRectTop:I

    .line 319
    iget-object v0, p0, Lepson/print/phlayout/BorderedLayoutPosition;->mPreview3mmEffectivePrintPosition:Lepson/print/phlayout/AltRect;

    iget v0, v0, Lepson/print/phlayout/AltRect;->right:I

    iput v0, p1, Lepson/print/EPImage;->previewPaperRectRight:I

    .line 320
    iget-object v0, p0, Lepson/print/phlayout/BorderedLayoutPosition;->mPreview3mmEffectivePrintPosition:Lepson/print/phlayout/AltRect;

    iget v0, v0, Lepson/print/phlayout/AltRect;->bottom:I

    iput v0, p1, Lepson/print/EPImage;->previewPaperRectBottom:I

    return-void
.end method

.method public getBottomMargin()I
    .locals 2

    .line 70
    iget-object v0, p0, Lepson/print/phlayout/BorderedLayoutPosition;->mPreviewMargin:[I

    const/4 v1, 0x3

    aget v0, v0, v1

    return v0
.end method

.method public getIsPaperLandscape()Z
    .locals 1

    .line 108
    iget-boolean v0, p0, Lepson/print/phlayout/BorderedLayoutPosition;->mIsPaperLandScape:Z

    return v0
.end method

.method public getLayoutId()I
    .locals 1

    .line 47
    iget v0, p0, Lepson/print/phlayout/BorderedLayoutPosition;->layout:I

    return v0
.end method

.method public getLeftMargin()I
    .locals 2

    .line 55
    iget-object v0, p0, Lepson/print/phlayout/BorderedLayoutPosition;->mPreviewMargin:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    return v0
.end method

.method public getPreviewImageCircumscribedTargetSize()Landroid/graphics/RectF;
    .locals 1

    .line 92
    iget-object v0, p0, Lepson/print/phlayout/BorderedLayoutPosition;->mPreview3mmEffectivePrintPosition:Lepson/print/phlayout/AltRect;

    invoke-virtual {v0}, Lepson/print/phlayout/AltRect;->getRectF()Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method getPreviewPaperAltRect()Lepson/print/phlayout/AltRect;
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public getPreviewPaperRect()Landroid/graphics/Rect;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getPrintSize(Landroid/content/Context;II)[I
    .locals 1

    const/4 v0, 0x2

    .line 79
    invoke-virtual {p0, v0, p3}, Lepson/print/phlayout/BorderedLayoutPosition;->setLayoutId(II)V

    .line 80
    invoke-static {p1, p2}, Lepson/common/Info_paper;->getInfoPaper(Landroid/content/Context;I)Lepson/common/Info_paper;

    move-result-object p1

    .line 81
    invoke-virtual {p0, p1}, Lepson/print/phlayout/BorderedLayoutPosition;->setPaperSizeFromPaperInfo(Lepson/common/Info_paper;)V

    .line 83
    new-array p1, v0, [I

    iget p2, p0, Lepson/print/phlayout/BorderedLayoutPosition;->m3mmWidth:I

    const/4 p3, 0x0

    aput p2, p1, p3

    iget p2, p0, Lepson/print/phlayout/BorderedLayoutPosition;->m3mmHeight:I

    const/4 p3, 0x1

    aput p2, p1, p3

    return-object p1
.end method

.method public getRightMargin()I
    .locals 2

    .line 60
    iget-object v0, p0, Lepson/print/phlayout/BorderedLayoutPosition;->mPreviewMargin:[I

    const/4 v1, 0x2

    aget v0, v0, v1

    return v0
.end method

.method public getTopMargin()I
    .locals 2

    .line 65
    iget-object v0, p0, Lepson/print/phlayout/BorderedLayoutPosition;->mPreviewMargin:[I

    const/4 v1, 0x1

    aget v0, v0, v1

    return v0
.end method

.method public isPreviewImageSizeValid()Z
    .locals 2

    .line 97
    iget-object v0, p0, Lepson/print/phlayout/BorderedLayoutPosition;->mPreview3mmEffectivePrintPosition:Lepson/print/phlayout/AltRect;

    iget v0, v0, Lepson/print/phlayout/AltRect;->right:I

    iget-object v1, p0, Lepson/print/phlayout/BorderedLayoutPosition;->mPreview3mmEffectivePrintPosition:Lepson/print/phlayout/AltRect;

    iget v1, v1, Lepson/print/phlayout/AltRect;->left:I

    sub-int/2addr v0, v1

    if-lez v0, :cond_0

    iget-object v0, p0, Lepson/print/phlayout/BorderedLayoutPosition;->mPreview3mmEffectivePrintPosition:Lepson/print/phlayout/AltRect;

    iget v0, v0, Lepson/print/phlayout/AltRect;->bottom:I

    iget-object v1, p0, Lepson/print/phlayout/BorderedLayoutPosition;->mPreview3mmEffectivePrintPosition:Lepson/print/phlayout/AltRect;

    iget v1, v1, Lepson/print/phlayout/AltRect;->top:I

    sub-int/2addr v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public setLayoutId(II)V
    .locals 0

    .line 142
    iput p1, p0, Lepson/print/phlayout/BorderedLayoutPosition;->layout:I

    .line 143
    iput p2, p0, Lepson/print/phlayout/BorderedLayoutPosition;->layoutMulti:I

    return-void
.end method

.method public setPaperAndCalcPreviewPosition(Lepson/common/Info_paper;IILandroid/graphics/Point;)V
    .locals 1

    .line 148
    iget v0, p0, Lepson/print/phlayout/BorderedLayoutPosition;->layout:I

    .line 151
    invoke-virtual {p0, p1}, Lepson/print/phlayout/BorderedLayoutPosition;->setPaperSizeFromPaperInfo(Lepson/common/Info_paper;)V

    .line 154
    iget p1, p0, Lepson/print/phlayout/BorderedLayoutPosition;->m3mmWidth:I

    if-lez p1, :cond_0

    iget p1, p0, Lepson/print/phlayout/BorderedLayoutPosition;->m3mmHeight:I

    if-gtz p1, :cond_1

    :cond_0
    const/16 p1, 0xb4c

    .line 155
    iput p1, p0, Lepson/print/phlayout/BorderedLayoutPosition;->m3mmWidth:I

    const/16 p1, 0x101d

    .line 156
    iput p1, p0, Lepson/print/phlayout/BorderedLayoutPosition;->m3mmHeight:I

    .line 158
    :cond_1
    invoke-virtual {p0, p2, p3, p4}, Lepson/print/phlayout/BorderedLayoutPosition;->calculateLayoutSizeBordered(IILandroid/graphics/Point;)V

    return-void
.end method

.method setPaperAndImageSize_forTest(IIII)V
    .locals 0
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .line 131
    iput p3, p0, Lepson/print/phlayout/BorderedLayoutPosition;->m3mmWidth:I

    .line 132
    iput p4, p0, Lepson/print/phlayout/BorderedLayoutPosition;->m3mmHeight:I

    return-void
.end method

.method public setPaperLandscape(Z)V
    .locals 0

    .line 103
    iput-boolean p1, p0, Lepson/print/phlayout/BorderedLayoutPosition;->mIsPaperLandScape:Z

    return-void
.end method

.method setPaperSizeFromPaperInfo(Lepson/common/Info_paper;)V
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .line 265
    iget v0, p0, Lepson/print/phlayout/BorderedLayoutPosition;->layout:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 269
    invoke-virtual {p1}, Lepson/common/Info_paper;->getPaper_width_boder()I

    move-result v0

    iput v0, p0, Lepson/print/phlayout/BorderedLayoutPosition;->m3mmWidth:I

    .line 270
    invoke-virtual {p1}, Lepson/common/Info_paper;->getPaper_height_boder()I

    move-result v0

    iput v0, p0, Lepson/print/phlayout/BorderedLayoutPosition;->m3mmHeight:I

    .line 272
    invoke-virtual {p1}, Lepson/common/Info_paper;->getLeftMargin_border()I

    move-result v0

    iput v0, p0, Lepson/print/phlayout/BorderedLayoutPosition;->mLeftMargin:I

    .line 273
    invoke-virtual {p1}, Lepson/common/Info_paper;->getTopMargin_border()I

    move-result v0

    iput v0, p0, Lepson/print/phlayout/BorderedLayoutPosition;->mTopMargin:I

    .line 274
    invoke-virtual {p1}, Lepson/common/Info_paper;->getRightMargin_border()I

    move-result v0

    iput v0, p0, Lepson/print/phlayout/BorderedLayoutPosition;->mRightMargin:I

    .line 275
    invoke-virtual {p1}, Lepson/common/Info_paper;->getBottomMargin_border()I

    move-result v0

    iput v0, p0, Lepson/print/phlayout/BorderedLayoutPosition;->mBottomMargin:I

    .line 279
    iget v0, p0, Lepson/print/phlayout/BorderedLayoutPosition;->layoutMulti:I

    const/high16 v1, 0x10000

    if-eq v0, v1, :cond_1

    const/high16 v1, 0x20000

    if-eq v0, v1, :cond_0

    const/high16 v1, 0x40000

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 288
    :cond_0
    invoke-virtual {p1}, Lepson/common/Info_paper;->getPrintingArea_4in1()Landroid/graphics/Rect;

    move-result-object p1

    .line 289
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v0

    iput v0, p0, Lepson/print/phlayout/BorderedLayoutPosition;->m3mmWidth:I

    .line 290
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result p1

    iput p1, p0, Lepson/print/phlayout/BorderedLayoutPosition;->m3mmHeight:I

    goto :goto_0

    .line 281
    :cond_1
    invoke-virtual {p1}, Lepson/common/Info_paper;->getPrintingArea_2in1()Landroid/graphics/Rect;

    move-result-object p1

    .line 282
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v0

    iput v0, p0, Lepson/print/phlayout/BorderedLayoutPosition;->m3mmWidth:I

    .line 283
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result p1

    iput p1, p0, Lepson/print/phlayout/BorderedLayoutPosition;->m3mmHeight:I

    :goto_0
    return-void

    .line 266
    :cond_2
    new-instance p1, Ljava/lang/RuntimeException;

    invoke-direct {p1}, Ljava/lang/RuntimeException;-><init>()V

    throw p1
.end method

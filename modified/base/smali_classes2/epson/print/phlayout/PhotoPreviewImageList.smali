.class public Lepson/print/phlayout/PhotoPreviewImageList;
.super Ljava/lang/Object;
.source "PhotoPreviewImageList.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/phlayout/PhotoPreviewImageList$EpImageWrapper;,
        Lepson/print/phlayout/PhotoPreviewImageList$Element;,
        Lepson/print/phlayout/PhotoPreviewImageList$LayoutInfo;
    }
.end annotation


# instance fields
.field private mEpImageWrapperList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lepson/print/phlayout/PhotoPreviewImageList$EpImageWrapper;",
            ">;"
        }
    .end annotation
.end field

.field private mLayoutInfoList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lepson/print/phlayout/PhotoPreviewImageList$LayoutInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lepson/print/phlayout/PhotoPreviewImageList;->mEpImageWrapperList:Ljava/util/ArrayList;

    .line 19
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lepson/print/phlayout/PhotoPreviewImageList;->mLayoutInfoList:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$000(Lepson/print/phlayout/PhotoPreviewImageList;)Ljava/util/ArrayList;
    .locals 0

    .line 16
    iget-object p0, p0, Lepson/print/phlayout/PhotoPreviewImageList;->mLayoutInfoList:Ljava/util/ArrayList;

    return-object p0
.end method


# virtual methods
.method public add(Lepson/print/phlayout/PhotoPreviewImageList$EpImageWrapper;)V
    .locals 1
    .param p1    # Lepson/print/phlayout/PhotoPreviewImageList$EpImageWrapper;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 38
    iget-object v0, p0, Lepson/print/phlayout/PhotoPreviewImageList;->mEpImageWrapperList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 39
    iget-object p1, p0, Lepson/print/phlayout/PhotoPreviewImageList;->mLayoutInfoList:Ljava/util/ArrayList;

    new-instance v0, Lepson/print/phlayout/PhotoPreviewImageList$LayoutInfo;

    invoke-direct {v0}, Lepson/print/phlayout/PhotoPreviewImageList$LayoutInfo;-><init>()V

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public add(Ljava/lang/String;I)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 30
    new-instance v0, Lepson/print/phlayout/PhotoPreviewImageList$EpImageWrapper;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1, p2}, Lepson/print/phlayout/PhotoPreviewImageList$EpImageWrapper;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {p0, v0}, Lepson/print/phlayout/PhotoPreviewImageList;->add(Lepson/print/phlayout/PhotoPreviewImageList$EpImageWrapper;)V

    return-void
.end method

.method public addOrgNameEpOrgName(Lepson/print/phlayout/PhotoPreviewImageList$EpImageWrapper;)V
    .locals 3
    .param p1    # Lepson/print/phlayout/PhotoPreviewImageList$EpImageWrapper;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 47
    new-instance v0, Lepson/print/phlayout/PhotoPreviewImageList$EpImageWrapper;

    iget-object v1, p1, Lepson/print/phlayout/PhotoPreviewImageList$EpImageWrapper;->epImage:Lepson/print/EPImage;

    invoke-virtual {v1}, Lepson/print/EPImage;->getOriginalFileName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lepson/print/phlayout/PhotoPreviewImageList$EpImageWrapper;->orgFilename:Ljava/lang/String;

    iget-object p1, p1, Lepson/print/phlayout/PhotoPreviewImageList$EpImageWrapper;->epImage:Lepson/print/EPImage;

    iget p1, p1, Lepson/print/EPImage;->index:I

    invoke-direct {v0, v1, v2, p1}, Lepson/print/phlayout/PhotoPreviewImageList$EpImageWrapper;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    .line 49
    iget-object p1, p0, Lepson/print/phlayout/PhotoPreviewImageList;->mEpImageWrapperList:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 50
    iget-object p1, p0, Lepson/print/phlayout/PhotoPreviewImageList;->mLayoutInfoList:Ljava/util/ArrayList;

    new-instance v0, Lepson/print/phlayout/PhotoPreviewImageList$LayoutInfo;

    invoke-direct {v0}, Lepson/print/phlayout/PhotoPreviewImageList$LayoutInfo;-><init>()V

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public clear()V
    .locals 1

    .line 117
    iget-object v0, p0, Lepson/print/phlayout/PhotoPreviewImageList;->mEpImageWrapperList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 118
    iget-object v0, p0, Lepson/print/phlayout/PhotoPreviewImageList;->mLayoutInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method public clearExceptSrcFileName()V
    .locals 0

    return-void
.end method

.method public get(I)Lepson/print/EPImage;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .line 93
    iget-object v0, p0, Lepson/print/phlayout/PhotoPreviewImageList;->mEpImageWrapperList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lepson/print/phlayout/PhotoPreviewImageList$EpImageWrapper;

    iget-object p1, p1, Lepson/print/phlayout/PhotoPreviewImageList$EpImageWrapper;->epImage:Lepson/print/EPImage;

    return-object p1
.end method

.method public getArgumentFileList()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 101
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 102
    iget-object v1, p0, Lepson/print/phlayout/PhotoPreviewImageList;->mEpImageWrapperList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lepson/print/phlayout/PhotoPreviewImageList$EpImageWrapper;

    .line 103
    invoke-virtual {v2}, Lepson/print/phlayout/PhotoPreviewImageList$EpImageWrapper;->getArgumentFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public getElement(I)Lepson/print/phlayout/PhotoPreviewImageList$Element;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    if-ltz p1, :cond_0

    .line 133
    iget-object v0, p0, Lepson/print/phlayout/PhotoPreviewImageList;->mEpImageWrapperList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gt p1, v0, :cond_0

    .line 138
    new-instance v0, Lepson/print/phlayout/PhotoPreviewImageList$Element;

    invoke-direct {v0, p0, p1}, Lepson/print/phlayout/PhotoPreviewImageList$Element;-><init>(Lepson/print/phlayout/PhotoPreviewImageList;I)V

    return-object v0

    .line 134
    :cond_0
    new-instance p1, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {p1}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw p1
.end method

.method public getGalleryFileList()Ljava/util/ArrayList;
    .locals 3
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 75
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    .line 76
    :goto_0
    invoke-virtual {p0}, Lepson/print/phlayout/PhotoPreviewImageList;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 77
    iget-object v2, p0, Lepson/print/phlayout/PhotoPreviewImageList;->mEpImageWrapperList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lepson/print/phlayout/PhotoPreviewImageList$EpImageWrapper;

    iget-object v2, v2, Lepson/print/phlayout/PhotoPreviewImageList$EpImageWrapper;->epImage:Lepson/print/EPImage;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    .line 83
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 85
    :cond_0
    invoke-virtual {v2}, Lepson/print/EPImage;->getOriginalFileName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public remove(I)V
    .locals 1

    .line 122
    iget-object v0, p0, Lepson/print/phlayout/PhotoPreviewImageList;->mEpImageWrapperList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 123
    iget-object v0, p0, Lepson/print/phlayout/PhotoPreviewImageList;->mLayoutInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    return-void
.end method

.method public set(ILepson/print/EPImage;)V
    .locals 1
    .param p2    # Lepson/print/EPImage;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 113
    iget-object v0, p0, Lepson/print/phlayout/PhotoPreviewImageList;->mEpImageWrapperList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lepson/print/phlayout/PhotoPreviewImageList$EpImageWrapper;

    iput-object p2, p1, Lepson/print/phlayout/PhotoPreviewImageList$EpImageWrapper;->epImage:Lepson/print/EPImage;

    return-void
.end method

.method public setList(Ljava/util/ArrayList;)V
    .locals 1
    .param p1    # Ljava/util/ArrayList;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lepson/print/phlayout/PhotoPreviewImageList$EpImageWrapper;",
            ">;)V"
        }
    .end annotation

    .line 59
    invoke-virtual {p0}, Lepson/print/phlayout/PhotoPreviewImageList;->clear()V

    if-nez p1, :cond_0

    return-void

    .line 64
    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/print/phlayout/PhotoPreviewImageList$EpImageWrapper;

    .line 65
    invoke-virtual {p0, v0}, Lepson/print/phlayout/PhotoPreviewImageList;->addOrgNameEpOrgName(Lepson/print/phlayout/PhotoPreviewImageList$EpImageWrapper;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public size()I
    .locals 1

    .line 22
    iget-object v0, p0, Lepson/print/phlayout/PhotoPreviewImageList;->mEpImageWrapperList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

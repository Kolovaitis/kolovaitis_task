.class Lepson/print/phlayout/PhotoPreview$MyGestureListener;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "PhotoPreview.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/phlayout/PhotoPreview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "MyGestureListener"
.end annotation


# instance fields
.field private final mPreviewView:Lepson/print/phlayout/PhotoPreview;


# direct methods
.method public constructor <init>(Lepson/print/phlayout/PhotoPreview;)V
    .locals 0

    .line 1194
    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    .line 1195
    iput-object p1, p0, Lepson/print/phlayout/PhotoPreview$MyGestureListener;->mPreviewView:Lepson/print/phlayout/PhotoPreview;

    return-void
.end method


# virtual methods
.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 0

    .line 1216
    iget-object p1, p0, Lepson/print/phlayout/PhotoPreview$MyGestureListener;->mPreviewView:Lepson/print/phlayout/PhotoPreview;

    invoke-static {p1}, Lepson/print/phlayout/PhotoPreview;->access$200(Lepson/print/phlayout/PhotoPreview;)V

    const/4 p1, 0x1

    return p1
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 2

    .line 1209
    iget-object v0, p0, Lepson/print/phlayout/PhotoPreview$MyGestureListener;->mPreviewView:Lepson/print/phlayout/PhotoPreview;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    invoke-static {v0, v1, p1}, Lepson/print/phlayout/PhotoPreview;->access$100(Lepson/print/phlayout/PhotoPreview;FF)V

    const/4 p1, 0x1

    return p1
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 0

    .line 1202
    iget-object p1, p0, Lepson/print/phlayout/PhotoPreview$MyGestureListener;->mPreviewView:Lepson/print/phlayout/PhotoPreview;

    invoke-static {p1, p3, p4}, Lepson/print/phlayout/PhotoPreview;->access$000(Lepson/print/phlayout/PhotoPreview;FF)V

    const/4 p1, 0x1

    return p1
.end method

.class public Lepson/print/phlayout/AltRect;
.super Ljava/lang/Object;
.source "AltRect.java"


# instance fields
.field bottom:I

.field left:I

.field right:I

.field top:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(IIII)V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput p1, p0, Lepson/print/phlayout/AltRect;->left:I

    .line 20
    iput p2, p0, Lepson/print/phlayout/AltRect;->top:I

    .line 21
    iput p3, p0, Lepson/print/phlayout/AltRect;->right:I

    .line 22
    iput p4, p0, Lepson/print/phlayout/AltRect;->bottom:I

    return-void
.end method

.method constructor <init>(Lepson/print/phlayout/AltRect;)V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iget p1, p1, Lepson/print/phlayout/AltRect;->left:I

    iput p1, p0, Lepson/print/phlayout/AltRect;->left:I

    return-void
.end method


# virtual methods
.method centerX()I
    .locals 2

    .line 38
    iget v0, p0, Lepson/print/phlayout/AltRect;->left:I

    iget v1, p0, Lepson/print/phlayout/AltRect;->right:I

    add-int/2addr v0, v1

    shr-int/lit8 v0, v0, 0x1

    return v0
.end method

.method centerY()I
    .locals 2

    .line 42
    iget v0, p0, Lepson/print/phlayout/AltRect;->top:I

    iget v1, p0, Lepson/print/phlayout/AltRect;->bottom:I

    add-int/2addr v0, v1

    shr-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_6

    .line 56
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    .line 58
    :cond_1
    check-cast p1, Lepson/print/phlayout/AltRect;

    .line 60
    iget v2, p0, Lepson/print/phlayout/AltRect;->left:I

    iget v3, p1, Lepson/print/phlayout/AltRect;->left:I

    if-eq v2, v3, :cond_2

    return v1

    .line 61
    :cond_2
    iget v2, p0, Lepson/print/phlayout/AltRect;->top:I

    iget v3, p1, Lepson/print/phlayout/AltRect;->top:I

    if-eq v2, v3, :cond_3

    return v1

    .line 62
    :cond_3
    iget v2, p0, Lepson/print/phlayout/AltRect;->right:I

    iget v3, p1, Lepson/print/phlayout/AltRect;->right:I

    if-eq v2, v3, :cond_4

    return v1

    .line 63
    :cond_4
    iget v2, p0, Lepson/print/phlayout/AltRect;->bottom:I

    iget p1, p1, Lepson/print/phlayout/AltRect;->bottom:I

    if-ne v2, p1, :cond_5

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_6
    :goto_1
    return v1
.end method

.method public getRect()Landroid/graphics/Rect;
    .locals 5

    .line 46
    new-instance v0, Landroid/graphics/Rect;

    iget v1, p0, Lepson/print/phlayout/AltRect;->left:I

    iget v2, p0, Lepson/print/phlayout/AltRect;->top:I

    iget v3, p0, Lepson/print/phlayout/AltRect;->right:I

    iget v4, p0, Lepson/print/phlayout/AltRect;->bottom:I

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v0
.end method

.method public getRectF()Landroid/graphics/RectF;
    .locals 5

    .line 50
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p0, Lepson/print/phlayout/AltRect;->left:I

    int-to-float v1, v1

    iget v2, p0, Lepson/print/phlayout/AltRect;->top:I

    int-to-float v2, v2

    iget v3, p0, Lepson/print/phlayout/AltRect;->right:I

    int-to-float v3, v3

    iget v4, p0, Lepson/print/phlayout/AltRect;->bottom:I

    int-to-float v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .line 68
    iget v0, p0, Lepson/print/phlayout/AltRect;->left:I

    mul-int/lit8 v0, v0, 0x1f

    .line 69
    iget v1, p0, Lepson/print/phlayout/AltRect;->top:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 70
    iget v1, p0, Lepson/print/phlayout/AltRect;->right:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 71
    iget v1, p0, Lepson/print/phlayout/AltRect;->bottom:I

    add-int/2addr v0, v1

    return v0
.end method

.method height()I
    .locals 2

    .line 34
    iget v0, p0, Lepson/print/phlayout/AltRect;->bottom:I

    iget v1, p0, Lepson/print/phlayout/AltRect;->top:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 77
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x20

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "AltRect("

    .line 78
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    iget v1, p0, Lepson/print/phlayout/AltRect;->left:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    iget v1, p0, Lepson/print/phlayout/AltRect;->top:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    iget v1, p0, Lepson/print/phlayout/AltRect;->right:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    iget v1, p0, Lepson/print/phlayout/AltRect;->bottom:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method width()I
    .locals 2

    .line 30
    iget v0, p0, Lepson/print/phlayout/AltRect;->right:I

    iget v1, p0, Lepson/print/phlayout/AltRect;->left:I

    sub-int/2addr v0, v1

    return v0
.end method

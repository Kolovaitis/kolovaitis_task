.class public Lepson/print/ActivityRequestPermissions;
.super Landroid/app/Activity;
.source "ActivityRequestPermissions.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/ActivityRequestPermissions$DialogParameter;,
        Lepson/print/ActivityRequestPermissions$Permission;
    }
.end annotation


# static fields
.field private static final APPLICATION_DETAILS_SETTINGS:I = 0x0

.field private static final ID_ENABLED_PERMISSION:I = 0x1

.field private static final KEY_PERMISSIONS:Ljava/lang/String; = "KEY_PERMISSIONS"

.field private static final TAG:Ljava/lang/String; = "ActivityRequestPermission"


# instance fields
.field mPermissionObject:Lepson/print/ActivityRequestPermissions$Permission;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 28
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private PermisionAlertDialog(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 226
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    .line 227
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 228
    invoke-virtual {v0, p3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p3

    .line 229
    invoke-virtual {p3, p4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p3

    new-instance p4, Lepson/print/ActivityRequestPermissions$2;

    invoke-direct {p4, p0, p2, p1}, Lepson/print/ActivityRequestPermissions$2;-><init>(Lepson/print/ActivityRequestPermissions;ZLjava/lang/String;)V

    const p1, 0x7f0e04f2

    .line 230
    invoke-virtual {p3, p1, p4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    new-instance p2, Lepson/print/ActivityRequestPermissions$1;

    invoke-direct {p2, p0}, Lepson/print/ActivityRequestPermissions$1;-><init>(Lepson/print/ActivityRequestPermissions;)V

    const p3, 0x7f0e0476

    .line 241
    invoke-virtual {p1, p3, p2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 247
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void
.end method

.method static synthetic access$000(Lepson/print/ActivityRequestPermissions;Ljava/lang/String;)V
    .locals 0

    .line 28
    invoke-direct {p0, p1}, Lepson/print/ActivityRequestPermissions;->startApplicationManagementActivity(Ljava/lang/String;)V

    return-void
.end method

.method public static checkPermission(Landroid/content/Context;[Ljava/lang/String;)Z
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x17
    .end annotation

    .line 125
    array-length v0, p1

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    aget-object v3, p1, v2

    .line 126
    invoke-virtual {p0, v3}, Landroid/content/Context;->checkSelfPermission(Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_0

    return v1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x1

    return p0
.end method

.method public static isRuntimePermissionSupported()Z
    .locals 2

    .line 210
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static requestPermission(Landroid/app/Activity;Lepson/print/ActivityRequestPermissions$Permission;I)V
    .locals 2

    .line 148
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lepson/print/ActivityRequestPermissions;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "KEY_PERMISSIONS"

    .line 149
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 152
    invoke-virtual {p0, v0, p2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method public static requestPermission(Landroid/support/v4/app/Fragment;Lepson/print/ActivityRequestPermissions$Permission;I)V
    .locals 3

    .line 171
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lepson/print/ActivityRequestPermissions;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "KEY_PERMISSIONS"

    .line 172
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 175
    invoke-virtual {p0, v0, p2}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private startApplicationManagementActivity(Ljava/lang/String;)V
    .locals 3

    .line 189
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.APPLICATION_DETAILS_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 190
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "package:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const/4 p1, 0x0

    .line 193
    invoke-virtual {p0, v0, p1}, Lepson/print/ActivityRequestPermissions;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x17
    .end annotation

    if-eqz p1, :cond_0

    goto :goto_0

    .line 106
    :cond_0
    iget-object v0, p0, Lepson/print/ActivityRequestPermissions;->mPermissionObject:Lepson/print/ActivityRequestPermissions$Permission;

    invoke-virtual {v0}, Lepson/print/ActivityRequestPermissions$Permission;->getPermissionKey()[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lepson/print/ActivityRequestPermissions;->requestPermissions([Ljava/lang/String;I)V

    .line 112
    :goto_0
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x17
    .end annotation

    .line 43
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 45
    invoke-virtual {p0}, Lepson/print/ActivityRequestPermissions;->getIntent()Landroid/content/Intent;

    move-result-object p1

    if-eqz p1, :cond_0

    const-string v0, "KEY_PERMISSIONS"

    .line 47
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    check-cast p1, Lepson/print/ActivityRequestPermissions$Permission;

    iput-object p1, p0, Lepson/print/ActivityRequestPermissions;->mPermissionObject:Lepson/print/ActivityRequestPermissions$Permission;

    .line 49
    iget-object p1, p0, Lepson/print/ActivityRequestPermissions;->mPermissionObject:Lepson/print/ActivityRequestPermissions$Permission;

    invoke-virtual {p1}, Lepson/print/ActivityRequestPermissions$Permission;->getPermissionKey()[Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lepson/print/ActivityRequestPermissions;->requestPermissions([Ljava/lang/String;I)V

    goto :goto_0

    .line 51
    :cond_0
    invoke-virtual {p0}, Lepson/print/ActivityRequestPermissions;->finish()V

    :goto_0
    return-void
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 6

    .line 57
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onRequestPermissionsResult(I[Ljava/lang/String;[I)V

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    goto :goto_2

    :cond_0
    const/4 p1, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 65
    :goto_0
    array-length v3, p2

    if-ge v1, v3, :cond_4

    .line 66
    iget-object v3, p0, Lepson/print/ActivityRequestPermissions;->mPermissionObject:Lepson/print/ActivityRequestPermissions$Permission;

    invoke-virtual {v3, v1}, Lepson/print/ActivityRequestPermissions$Permission;->getPermissionKey(I)Ljava/lang/String;

    move-result-object v3

    aget-object v4, p2, v1

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 67
    aget v3, p3, v1

    if-nez v3, :cond_1

    const-string v3, "ActivityRequestPermission"

    .line 68
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "PERMISSION_GRANTED "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v5, p2, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 73
    :cond_1
    aget-object v3, p2, v1

    invoke-static {p0, v3}, Landroid/support/v4/app/ActivityCompat;->shouldShowRequestPermissionRationale(Landroid/app/Activity;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 74
    iget-object v3, p0, Lepson/print/ActivityRequestPermissions;->mPermissionObject:Lepson/print/ActivityRequestPermissions$Permission;

    invoke-virtual {v3, v1, v0}, Lepson/print/ActivityRequestPermissions$Permission;->getDialogTitle(II)Ljava/lang/String;

    move-result-object v3

    .line 75
    iget-object v4, p0, Lepson/print/ActivityRequestPermissions;->mPermissionObject:Lepson/print/ActivityRequestPermissions$Permission;

    invoke-virtual {v4, v1, v0}, Lepson/print/ActivityRequestPermissions$Permission;->getDialogMessage(II)Ljava/lang/String;

    move-result-object v4

    .line 76
    aget-object v5, p2, v1

    invoke-direct {p0, v5, v0, v3, v4}, Lepson/print/ActivityRequestPermissions;->PermisionAlertDialog(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 78
    :cond_2
    iget-object v3, p0, Lepson/print/ActivityRequestPermissions;->mPermissionObject:Lepson/print/ActivityRequestPermissions$Permission;

    invoke-virtual {v3, v1, p1}, Lepson/print/ActivityRequestPermissions$Permission;->getDialogTitle(II)Ljava/lang/String;

    move-result-object v3

    .line 79
    iget-object v4, p0, Lepson/print/ActivityRequestPermissions;->mPermissionObject:Lepson/print/ActivityRequestPermissions$Permission;

    invoke-virtual {v4, v1, p1}, Lepson/print/ActivityRequestPermissions$Permission;->getDialogMessage(II)Ljava/lang/String;

    move-result-object v4

    .line 80
    aget-object v5, p2, v1

    invoke-direct {p0, v5, p1, v3, v4}, Lepson/print/ActivityRequestPermissions;->PermisionAlertDialog(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    :cond_3
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 87
    :cond_4
    array-length p1, p2

    if-ne v2, p1, :cond_5

    const/4 p1, -0x1

    .line 88
    invoke-virtual {p0, p1}, Lepson/print/ActivityRequestPermissions;->setResult(I)V

    .line 89
    invoke-virtual {p0}, Lepson/print/ActivityRequestPermissions;->finish()V

    :cond_5
    :goto_2
    return-void
.end method

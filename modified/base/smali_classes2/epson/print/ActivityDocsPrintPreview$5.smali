.class Lepson/print/ActivityDocsPrintPreview$5;
.super Ljava/lang/Object;
.source "ActivityDocsPrintPreview.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/ActivityDocsPrintPreview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/ActivityDocsPrintPreview;


# direct methods
.method constructor <init>(Lepson/print/ActivityDocsPrintPreview;)V
    .locals 0

    .line 758
    iput-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 11

    .line 760
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-virtual {v0}, Lepson/print/ActivityDocsPrintPreview;->isFinishing()Z

    move-result v0

    const/16 v1, 0x16

    const/16 v2, 0xd

    const/4 v3, 0x4

    const/4 v4, 0x1

    if-eqz v0, :cond_1

    .line 761
    iget v0, p1, Landroid/os/Message;->what:I

    if-eq v0, v3, :cond_0

    const/4 v5, 0x6

    if-eq v0, v5, :cond_0

    if-eq v0, v2, :cond_0

    if-eq v0, v1, :cond_0

    const/16 v5, 0x27

    if-eq v0, v5, :cond_0

    const/16 v5, 0x32

    if-eq v0, v5, :cond_0

    const/16 v5, 0x64

    if-eq v0, v5, :cond_0

    packed-switch v0, :pswitch_data_0

    packed-switch v0, :pswitch_data_1

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    :cond_0
    :pswitch_0
    return v4

    .line 784
    :cond_1
    :goto_0
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v5, 0x1e

    const/4 v6, 0x7

    const/4 v7, 0x0

    const/16 v8, 0x8

    const/4 v9, 0x2

    const/4 v10, 0x0

    sparse-switch v0, :sswitch_data_0

    goto/16 :goto_7

    .line 1310
    :sswitch_0
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object p1

    const-string v1, "PAPERSOURCEINFO"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object p1

    invoke-static {v0, p1}, Lepson/print/ActivityDocsPrintPreview;->access$3302(Lepson/print/ActivityDocsPrintPreview;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 1311
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$3300(Lepson/print/ActivityDocsPrintPreview;)Ljava/util/ArrayList;

    move-result-object p1

    const v0, 0x7f05004a

    if-eqz p1, :cond_3

    .line 1312
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$3400(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/TextView;

    move-result-object p1

    iget-object v1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-virtual {v1}, Lepson/print/ActivityDocsPrintPreview;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 1313
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$3400(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/TextView;

    move-result-object p1

    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-virtual {v0}, Lepson/print/ActivityDocsPrintPreview;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f05001e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1316
    new-instance p1, Lepson/print/screen/PrintSetting;

    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    sget-object v1, Lepson/print/screen/PrintSetting$Kind;->document:Lepson/print/screen/PrintSetting$Kind;

    invoke-direct {p1, v0, v1}, Lepson/print/screen/PrintSetting;-><init>(Landroid/content/Context;Lepson/print/screen/PrintSetting$Kind;)V

    .line 1317
    invoke-virtual {p1}, Lepson/print/screen/PrintSetting;->loadSettings()V

    .line 1319
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    iget-object v0, v0, Lepson/print/ActivityDocsPrintPreview;->paperSourceInfo:Lepson/print/screen/PaperSourceInfo;

    iget-object v1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v1}, Lepson/print/ActivityDocsPrintPreview;->access$3300(Lepson/print/ActivityDocsPrintPreview;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lepson/print/screen/PaperSourceInfo;->checkPaperMissmatch(Lepson/print/screen/PrintSetting;Ljava/util/ArrayList;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 1321
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$3500(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/ImageView;

    move-result-object p1

    invoke-virtual {p1, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_7

    .line 1323
    :cond_2
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$3500(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/ImageView;

    move-result-object p1

    invoke-virtual {p1, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_7

    .line 1326
    :cond_3
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$3400(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/TextView;

    move-result-object p1

    iget-object v1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-virtual {v1}, Lepson/print/ActivityDocsPrintPreview;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070153

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1327
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$3400(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/TextView;

    move-result-object p1

    iget-object v1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-virtual {v1}, Lepson/print/ActivityDocsPrintPreview;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1328
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$3500(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/ImageView;

    move-result-object p1

    invoke-virtual {p1, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_7

    .line 1150
    :sswitch_1
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$2900(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/LinearLayout;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/LinearLayout;->invalidate()V

    .line 1151
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$3000(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/RelativeLayout;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/RelativeLayout;->invalidate()V

    goto/16 :goto_7

    .line 1209
    :sswitch_2
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    const v1, 0x7f0e0055

    invoke-virtual {v0, v1}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1210
    iget-object v2, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    const v3, 0x7f0e0051

    invoke-virtual {v2, v3}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1214
    iget-object v5, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v5, v4}, Lepson/print/ActivityDocsPrintPreview;->access$3202(Lepson/print/ActivityDocsPrintPreview;Z)Z

    .line 1217
    iget v5, p1, Landroid/os/Message;->arg1:I

    const/16 v6, -0x44c

    if-eq v5, v6, :cond_6

    const/16 v6, -0x420

    if-eq v5, v6, :cond_5

    const/4 v6, -0x1

    if-eq v5, v6, :cond_4

    packed-switch v5, :pswitch_data_3

    goto/16 :goto_1

    .line 1241
    :pswitch_1
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    const v0, 0x7f0e0059

    invoke-virtual {p1, v0}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1242
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    const v1, 0x7f0e0058

    invoke-virtual {p1, v1}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 1246
    :pswitch_2
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    const v0, 0x7f0e005c

    invoke-virtual {p1, v0}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1247
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    const v1, 0x7f0e005b

    invoke-virtual {p1, v1}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 1236
    :pswitch_3
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    const v0, 0x7f0e0057

    invoke-virtual {p1, v0}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1237
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    const v1, 0x7f0e0056

    invoke-virtual {p1, v1}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 1224
    :pswitch_4
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    const v0, 0x7f0e004f

    invoke-virtual {p1, v0}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1226
    :try_start_0
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    const v1, 0x7f0e004e

    .line 1227
    invoke-virtual {p1, v1}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object p1

    new-array v1, v4, [Ljava/lang/Object;

    iget-object v3, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    .line 1228
    invoke-static {v3}, Lepson/print/ActivityDocsPrintPreview;->access$800(Lepson/print/ActivityDocsPrintPreview;)Lepson/print/service/IEpsonService;

    move-result-object v3

    invoke-interface {v3}, Lepson/print/service/IEpsonService;->EpsonConnectGetRemotePrintMaxFileSize()I

    move-result v3

    const/high16 v4, 0x100000

    div-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v10

    .line 1226
    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    :catch_0
    move-exception p1

    .line 1230
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1

    .line 1260
    :pswitch_5
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    const v0, 0x7f0e0353

    invoke-virtual {p1, v0}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1261
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    const v1, 0x7f0e0352

    invoke-virtual {p1, v1}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 1266
    :pswitch_6
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    const v1, 0x7f0e005a

    invoke-virtual {p1, v1}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 1271
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1272
    invoke-virtual {v1, v10}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 1273
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 1274
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    const v1, 0x7f0e03fe

    .line 1275
    invoke-virtual {v0, v1}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lepson/print/ActivityDocsPrintPreview$5$3;

    invoke-direct {v1, p0}, Lepson/print/ActivityDocsPrintPreview$5$3;-><init>(Lepson/print/ActivityDocsPrintPreview$5;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 1280
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/AlertDialog;->show()V

    return v10

    .line 1287
    :pswitch_7
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    const v1, 0x7f0e0049

    invoke-virtual {v0, v1}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1288
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    const v3, 0x7f0e0047

    invoke-virtual {v2, v3}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "0X"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p1, p1, Landroid/os/Message;->arg1:I

    .line 1289
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    const v2, 0x7f0e0048

    .line 1290
    invoke-virtual {p1, v2}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 1252
    :cond_4
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    const v0, 0x7f0e0347

    invoke-virtual {p1, v0}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1253
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    const v1, 0x7f0e0346

    invoke-virtual {p1, v1}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 1296
    :cond_5
    :pswitch_8
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-virtual {v0, v1}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1297
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-virtual {v2, v3}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    const v3, 0x7f0e0052

    .line 1298
    invoke-virtual {v2, v3}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "0X"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p1, p1, Landroid/os/Message;->arg1:I

    .line 1299
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    const v2, 0x7f0e0053

    .line 1300
    invoke-virtual {p1, v2}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 1219
    :cond_6
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    const v0, 0x7f0e0060

    invoke-virtual {p1, v0}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1220
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    const v1, 0x7f0e005f

    invoke-virtual {p1, v1}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1304
    :goto_1
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-virtual {p1, v0, v2}, Lepson/print/ActivityDocsPrintPreview;->showErrorDialog(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 1122
    :sswitch_3
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-virtual {p1}, Lepson/print/ActivityDocsPrintPreview;->resetPageRangeBySettings()Z

    .line 1124
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$300(Lepson/print/ActivityDocsPrintPreview;)I

    move-result p1

    and-int/2addr p1, v3

    if-eqz p1, :cond_8

    .line 1126
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$2700(Lepson/print/ActivityDocsPrintPreview;)Z

    move-result p1

    if-eqz p1, :cond_21

    .line 1127
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$300(Lepson/print/ActivityDocsPrintPreview;)I

    move-result p1

    and-int/2addr p1, v9

    if-eqz p1, :cond_7

    .line 1129
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    iget-object p1, p1, Lepson/print/ActivityDocsPrintPreview;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {p1, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_7

    .line 1131
    :cond_7
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    iput-boolean v10, p1, Lepson/print/ActivityDocsPrintPreview;->isCreateJobDone:Z

    .line 1132
    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$1700(Lepson/print/ActivityDocsPrintPreview;)Z

    move-result v0

    invoke-virtual {p1, v0}, Lepson/print/ActivityDocsPrintPreview;->setOrentationView(Z)V

    goto/16 :goto_7

    .line 1140
    :cond_8
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$300(Lepson/print/ActivityDocsPrintPreview;)I

    move-result p1

    and-int/2addr p1, v9

    if-eqz p1, :cond_9

    .line 1141
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$2800(Lepson/print/ActivityDocsPrintPreview;)V

    .line 1142
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$1300(Lepson/print/ActivityDocsPrintPreview;)Lepson/print/pdf/pdfRender;

    move-result-object p1

    invoke-virtual {p1, v4, v4, v10}, Lepson/print/pdf/pdfRender;->startConvertPage(III)V

    goto/16 :goto_7

    .line 1144
    :cond_9
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$1700(Lepson/print/ActivityDocsPrintPreview;)Z

    move-result v0

    invoke-virtual {p1, v0}, Lepson/print/ActivityDocsPrintPreview;->setOrentationView(Z)V

    goto/16 :goto_7

    .line 1110
    :sswitch_4
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1, v10}, Lepson/print/ActivityDocsPrintPreview;->access$2102(Lepson/print/ActivityDocsPrintPreview;Z)Z

    .line 1113
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-virtual {p1}, Lepson/print/ActivityDocsPrintPreview;->resetPageRange()V

    .line 1115
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-virtual {p1}, Lepson/print/ActivityDocsPrintPreview;->updatePrevNextButtonView()V

    .line 1116
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-virtual {p1}, Lepson/print/ActivityDocsPrintPreview;->updatePageTextView()V

    goto/16 :goto_7

    :sswitch_5
    const-string v0, "ActivityDocsPrintPreview"

    const-string v1, "handleMessage : CONVERTED_OK_EC"

    .line 1068
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1069
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-virtual {v0, v10}, Lepson/print/ActivityDocsPrintPreview;->enableProgressBar(Z)V

    .line 1071
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 1074
    iget-object v1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object p1

    const-string v2, "CONVERTED_PAGE"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    invoke-static {v1, p1}, Lepson/print/ActivityDocsPrintPreview;->access$602(Lepson/print/ActivityDocsPrintPreview;I)I

    .line 1077
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-virtual {p1}, Lepson/print/ActivityDocsPrintPreview;->updatePageTextView()V

    .line 1078
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-virtual {p1}, Lepson/print/ActivityDocsPrintPreview;->updatePrevNextButtonView()V

    .line 1081
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-virtual {p1, v0}, Lepson/print/ActivityDocsPrintPreview;->setNewImageView(Ljava/lang/String;)V

    .line 1083
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$2600(Lepson/print/ActivityDocsPrintPreview;)Landroid/graphics/Bitmap;

    move-result-object p1

    if-eqz p1, :cond_b

    .line 1084
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$2600(Lepson/print/ActivityDocsPrintPreview;)Landroid/graphics/Bitmap;

    move-result-object p1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result p1

    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v0}, Lepson/print/ActivityDocsPrintPreview;->access$2600(Lepson/print/ActivityDocsPrintPreview;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    if-le p1, v0, :cond_a

    .line 1085
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1, v10}, Lepson/print/ActivityDocsPrintPreview;->access$1702(Lepson/print/ActivityDocsPrintPreview;Z)Z

    goto :goto_2

    .line 1087
    :cond_a
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1, v4}, Lepson/print/ActivityDocsPrintPreview;->access$1702(Lepson/print/ActivityDocsPrintPreview;Z)Z

    .line 1089
    :goto_2
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$1700(Lepson/print/ActivityDocsPrintPreview;)Z

    move-result v0

    invoke-virtual {p1, v0}, Lepson/print/ActivityDocsPrintPreview;->setOrentationView(Z)V

    .line 1093
    :cond_b
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    iput-boolean v4, p1, Lepson/print/ActivityDocsPrintPreview;->isEnableNFCTouch:Z

    .line 1099
    iget-boolean p1, p1, Lepson/print/ActivityDocsPrintPreview;->bAutoStartPrint:Z

    if-eqz p1, :cond_21

    .line 1101
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$2500(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {p1, v0}, Lepson/print/ActivityDocsPrintPreview;->onClick(Landroid/view/View;)V

    const-string p1, "ActivityDocsPrintPreview"

    const-string v0, "onClick(mPrint)"

    .line 1102
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1103
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    iput-boolean v10, p1, Lepson/print/ActivityDocsPrintPreview;->bAutoStartPrint:Z

    goto/16 :goto_7

    .line 1023
    :sswitch_6
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-virtual {p1, v10}, Lepson/print/ActivityDocsPrintPreview;->enableProgressBar(Z)V

    .line 1025
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    new-instance v0, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;

    invoke-direct {v0, p1, v7}, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;-><init>(Lepson/print/ActivityDocsPrintPreview;Lepson/print/ActivityDocsPrintPreview$1;)V

    iput-object v0, p1, Lepson/print/ActivityDocsPrintPreview;->loadRemotePreviewTask:Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;

    .line 1026
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    iget-object p1, p1, Lepson/print/ActivityDocsPrintPreview;->loadRemotePreviewTask:Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;

    new-array v0, v4, [Ljava/lang/String;

    iget-object v1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v1}, Lepson/print/ActivityDocsPrintPreview;->access$1000(Lepson/print/ActivityDocsPrintPreview;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v10

    invoke-virtual {p1, v0}, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_7

    .line 1163
    :sswitch_7
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    iget-object p1, p1, Lepson/print/ActivityDocsPrintPreview;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {p1, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1166
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$300(Lepson/print/ActivityDocsPrintPreview;)I

    move-result p1

    and-int/2addr p1, v3

    if-eqz p1, :cond_c

    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    iget-boolean p1, p1, Lepson/print/ActivityDocsPrintPreview;->isCreateJobDone:Z

    if-nez p1, :cond_c

    .line 1168
    new-instance p1, Lepson/print/ActivityDocsPrintPreview$PrintUploadTask;

    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-direct {p1, v0, v7}, Lepson/print/ActivityDocsPrintPreview$PrintUploadTask;-><init>(Lepson/print/ActivityDocsPrintPreview;Lepson/print/ActivityDocsPrintPreview$1;)V

    new-array v0, v4, [Ljava/lang/String;

    iget-object v1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v1}, Lepson/print/ActivityDocsPrintPreview;->access$1000(Lepson/print/ActivityDocsPrintPreview;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v10

    invoke-virtual {p1, v0}, Lepson/print/ActivityDocsPrintPreview$PrintUploadTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_7

    .line 1172
    :cond_c
    new-instance p1, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;

    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-direct {p1, v0}, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;-><init>(Lepson/print/ActivityDocsPrintPreview;)V

    new-array v0, v10, [Ljava/lang/Void;

    invoke-virtual {p1, v0}, Lepson/print/ActivityDocsPrintPreview$PreparePrintTask;->executeOnExecutor([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_7

    .line 985
    :sswitch_8
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$1300(Lepson/print/ActivityDocsPrintPreview;)Lepson/print/pdf/pdfRender;

    move-result-object p1

    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v0}, Lepson/print/ActivityDocsPrintPreview;->access$1000(Lepson/print/ActivityDocsPrintPreview;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v1}, Lepson/print/ActivityDocsPrintPreview;->access$2300(Lepson/print/ActivityDocsPrintPreview;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-virtual {p1, v0, v1, v2}, Lepson/print/pdf/pdfRender;->openPdfFile(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_f

    .line 991
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$1300(Lepson/print/ActivityDocsPrintPreview;)Lepson/print/pdf/pdfRender;

    move-result-object p1

    invoke-virtual {p1}, Lepson/print/pdf/pdfRender;->canPrintableDoc()Z

    move-result p1

    if-eq p1, v4, :cond_d

    .line 994
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    const v0, 0x7f0e04ba

    invoke-static {p1, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    .line 995
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    iget-object p1, p1, Lepson/print/ActivityDocsPrintPreview;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {p1, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return v10

    .line 1000
    :cond_d
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$1300(Lepson/print/ActivityDocsPrintPreview;)Lepson/print/pdf/pdfRender;

    move-result-object v0

    invoke-virtual {v0}, Lepson/print/pdf/pdfRender;->totalPages()I

    move-result v0

    invoke-static {p1, v0}, Lepson/print/ActivityDocsPrintPreview;->access$702(Lepson/print/ActivityDocsPrintPreview;I)I

    .line 1001
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-virtual {p1}, Lepson/print/ActivityDocsPrintPreview;->resetPageRange()V

    .line 1004
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$300(Lepson/print/ActivityDocsPrintPreview;)I

    move-result p1

    and-int/2addr p1, v9

    if-ne p1, v9, :cond_e

    .line 1005
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$1300(Lepson/print/ActivityDocsPrintPreview;)Lepson/print/pdf/pdfRender;

    move-result-object p1

    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v0}, Lepson/print/ActivityDocsPrintPreview;->access$600(Lepson/print/ActivityDocsPrintPreview;)I

    move-result v0

    invoke-virtual {p1, v0, v4, v10}, Lepson/print/pdf/pdfRender;->startConvertPage(III)V

    goto/16 :goto_7

    .line 1008
    :cond_e
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    iput-boolean v4, p1, Lepson/print/ActivityDocsPrintPreview;->isEnableNFCTouch:Z

    goto/16 :goto_7

    .line 1013
    :cond_f
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    iget-object p1, p1, Lepson/print/ActivityDocsPrintPreview;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {p1, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_7

    .line 956
    :sswitch_9
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$300(Lepson/print/ActivityDocsPrintPreview;)I

    move-result p1

    and-int/2addr p1, v3

    if-eqz p1, :cond_12

    .line 960
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1, v4}, Lepson/print/ActivityDocsPrintPreview;->access$2102(Lepson/print/ActivityDocsPrintPreview;Z)Z

    .line 962
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$300(Lepson/print/ActivityDocsPrintPreview;)I

    move-result p1

    and-int/2addr p1, v9

    if-eqz p1, :cond_11

    .line 963
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$2200(Lepson/print/ActivityDocsPrintPreview;)Z

    move-result p1

    if-eqz p1, :cond_10

    .line 965
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lepson/print/ActivityDocsPrintPreview;->showDialog(I)V

    goto/16 :goto_7

    .line 967
    :cond_10
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    iget-object p1, p1, Lepson/print/ActivityDocsPrintPreview;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {p1, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_7

    .line 971
    :cond_11
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    iput-boolean v4, p1, Lepson/print/ActivityDocsPrintPreview;->isEnableNFCTouch:Z

    goto/16 :goto_7

    .line 977
    :cond_12
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    iget-object p1, p1, Lepson/print/ActivityDocsPrintPreview;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {p1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_7

    .line 787
    :sswitch_a
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-virtual {p1, v4}, Lepson/print/ActivityDocsPrintPreview;->enableProgressBar(Z)V

    .line 789
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$800(Lepson/print/ActivityDocsPrintPreview;)Lepson/print/service/IEpsonService;

    move-result-object p1

    if-nez p1, :cond_13

    .line 790
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    iget-object p1, p1, Lepson/print/ActivityDocsPrintPreview;->mUiHandler:Landroid/os/Handler;

    const/16 v0, 0xa

    const-wide/16 v1, 0x64

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_7

    .line 795
    :cond_13
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-virtual {p1, v10}, Lepson/print/ActivityDocsPrintPreview;->enableProgressBar(Z)V

    .line 798
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1, v4}, Lepson/print/ActivityDocsPrintPreview;->access$902(Lepson/print/ActivityDocsPrintPreview;I)I

    .line 799
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1, v4}, Lepson/print/ActivityDocsPrintPreview;->access$602(Lepson/print/ActivityDocsPrintPreview;I)I

    .line 800
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1, v4}, Lepson/print/ActivityDocsPrintPreview;->access$702(Lepson/print/ActivityDocsPrintPreview;I)I

    .line 801
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-virtual {p1}, Lepson/print/ActivityDocsPrintPreview;->resetPageRange()V

    .line 803
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$1100(Lepson/print/ActivityDocsPrintPreview;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/ActivityDocsPrintPreview;->access$1002(Lepson/print/ActivityDocsPrintPreview;Ljava/lang/String;)Ljava/lang/String;

    .line 806
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    iput-boolean v10, p1, Lepson/print/ActivityDocsPrintPreview;->isEnableNFCTouch:Z

    .line 809
    invoke-static {p1}, Lepson/print/MyPrinter;->isRemotePrinter(Landroid/content/Context;)Z

    move-result p1

    const v0, 0x7f0e0373

    if-eqz p1, :cond_18

    .line 812
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$1200(Lepson/print/ActivityDocsPrintPreview;)Z

    move-result v1

    if-eqz v1, :cond_14

    const/4 v1, 0x2

    goto :goto_3

    :cond_14
    const/4 v1, 0x0

    :goto_3
    or-int/2addr v1, v4

    invoke-static {p1, v1}, Lepson/print/ActivityDocsPrintPreview;->access$302(Lepson/print/ActivityDocsPrintPreview;I)I

    .line 814
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$1000(Lepson/print/ActivityDocsPrintPreview;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lepson/common/Utils;->isECConvertFile(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_15

    .line 816
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$300(Lepson/print/ActivityDocsPrintPreview;)I

    move-result v0

    or-int/2addr v0, v3

    invoke-static {p1, v0}, Lepson/print/ActivityDocsPrintPreview;->access$302(Lepson/print/ActivityDocsPrintPreview;I)I

    goto/16 :goto_4

    .line 818
    :cond_15
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$1000(Lepson/print/ActivityDocsPrintPreview;)Ljava/lang/String;

    move-result-object p1

    const-string v1, "application/pdf"

    invoke-static {p1, v1}, Lepson/common/Utils;->checkMimeType(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_16

    .line 821
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$1300(Lepson/print/ActivityDocsPrintPreview;)Lepson/print/pdf/pdfRender;

    move-result-object p1

    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v0}, Lepson/print/ActivityDocsPrintPreview;->access$1000(Lepson/print/ActivityDocsPrintPreview;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    iget-object v2, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-virtual {p1, v0, v1, v2}, Lepson/print/pdf/pdfRender;->openPdfFile(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_1a

    .line 824
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$300(Lepson/print/ActivityDocsPrintPreview;)I

    move-result v0

    or-int/2addr v0, v3

    invoke-static {p1, v0}, Lepson/print/ActivityDocsPrintPreview;->access$302(Lepson/print/ActivityDocsPrintPreview;I)I

    goto :goto_4

    .line 829
    :cond_16
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$1000(Lepson/print/ActivityDocsPrintPreview;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lepson/common/Utils;->isGConvertFile(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_17

    .line 831
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$300(Lepson/print/ActivityDocsPrintPreview;)I

    move-result v0

    or-int/2addr v0, v8

    invoke-static {p1, v0}, Lepson/print/ActivityDocsPrintPreview;->access$302(Lepson/print/ActivityDocsPrintPreview;I)I

    .line 834
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$300(Lepson/print/ActivityDocsPrintPreview;)I

    move-result v0

    or-int/2addr v0, v3

    invoke-static {p1, v0}, Lepson/print/ActivityDocsPrintPreview;->access$302(Lepson/print/ActivityDocsPrintPreview;I)I

    goto :goto_4

    .line 838
    :cond_17
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    .line 839
    invoke-virtual {p1, v0}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 838
    invoke-static {p1, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    .line 839
    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    .line 840
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    iget-object p1, p1, Lepson/print/ActivityDocsPrintPreview;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {p1, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return v10

    .line 846
    :cond_18
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1, v9}, Lepson/print/ActivityDocsPrintPreview;->access$302(Lepson/print/ActivityDocsPrintPreview;I)I

    .line 848
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$1000(Lepson/print/ActivityDocsPrintPreview;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lepson/common/Utils;->isGConvertFile(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_19

    .line 850
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$300(Lepson/print/ActivityDocsPrintPreview;)I

    move-result v0

    or-int/2addr v0, v8

    invoke-static {p1, v0}, Lepson/print/ActivityDocsPrintPreview;->access$302(Lepson/print/ActivityDocsPrintPreview;I)I

    goto :goto_4

    .line 852
    :cond_19
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$1000(Lepson/print/ActivityDocsPrintPreview;)Ljava/lang/String;

    move-result-object p1

    const-string v1, "application/pdf"

    invoke-static {p1, v1}, Lepson/common/Utils;->checkMimeType(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1d

    .line 865
    :cond_1a
    :goto_4
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$300(Lepson/print/ActivityDocsPrintPreview;)I

    move-result p1

    and-int/2addr p1, v9

    if-eqz p1, :cond_1b

    .line 867
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$1400(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {p1, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_5

    .line 870
    :cond_1b
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$1500(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {p1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 871
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$100(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/Button;

    move-result-object p1

    invoke-virtual {p1, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 872
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$200(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/Button;

    move-result-object p1

    invoke-virtual {p1, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 874
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$1600(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/ImageView;

    move-result-object p1

    invoke-virtual {p1, v7}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 876
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$1400(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {p1, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 880
    :goto_5
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$1600(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/ImageView;

    move-result-object p1

    invoke-virtual {p1, v7}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 881
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$1700(Lepson/print/ActivityDocsPrintPreview;)Z

    move-result v0

    invoke-virtual {p1, v0}, Lepson/print/ActivityDocsPrintPreview;->setOrentationView(Z)V

    .line 884
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$300(Lepson/print/ActivityDocsPrintPreview;)I

    move-result p1

    and-int/2addr p1, v8

    if-eqz p1, :cond_1c

    .line 888
    new-instance p1, Lepson/print/ActivityDocsPrintPreview$5$1;

    invoke-direct {p1, p0}, Lepson/print/ActivityDocsPrintPreview$5$1;-><init>(Lepson/print/ActivityDocsPrintPreview$5;)V

    new-array v0, v10, [Ljava/lang/Void;

    .line 924
    invoke-virtual {p1, v0}, Lepson/print/ActivityDocsPrintPreview$5$1;->executeOnExecutor([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_7

    .line 929
    :cond_1c
    new-instance p1, Lepson/print/ActivityDocsPrintPreview$5$2;

    invoke-direct {p1, p0}, Lepson/print/ActivityDocsPrintPreview$5$2;-><init>(Lepson/print/ActivityDocsPrintPreview$5;)V

    new-array v0, v10, [Ljava/lang/Void;

    .line 951
    invoke-virtual {p1, v0}, Lepson/print/ActivityDocsPrintPreview$5$2;->executeOnExecutor([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_7

    .line 857
    :cond_1d
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    .line 858
    invoke-virtual {p1, v0}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 857
    invoke-static {p1, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    .line 858
    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    .line 859
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    iget-object p1, p1, Lepson/print/ActivityDocsPrintPreview;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {p1, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return v10

    .line 1177
    :sswitch_b
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$1300(Lepson/print/ActivityDocsPrintPreview;)Lepson/print/pdf/pdfRender;

    move-result-object p1

    if-eqz p1, :cond_1e

    .line 1178
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$1300(Lepson/print/ActivityDocsPrintPreview;)Lepson/print/pdf/pdfRender;

    move-result-object p1

    invoke-virtual {p1, v10}, Lepson/print/pdf/pdfRender;->setMIsPreviewCon(Z)V

    .line 1179
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$1300(Lepson/print/ActivityDocsPrintPreview;)Lepson/print/pdf/pdfRender;

    move-result-object p1

    invoke-virtual {p1, v10}, Lepson/print/pdf/pdfRender;->setPrintingStt(Z)V

    .line 1186
    :cond_1e
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-virtual {p1}, Lepson/print/ActivityDocsPrintPreview;->cancelLoadRemotePreviewTask()V

    .line 1189
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    iget-boolean p1, p1, Lepson/print/ActivityDocsPrintPreview;->isCreateJobDone:Z

    if-ne p1, v4, :cond_1f

    .line 1191
    :try_start_1
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$800(Lepson/print/ActivityDocsPrintPreview;)Lepson/print/service/IEpsonService;

    move-result-object p1

    invoke-interface {p1}, Lepson/print/service/IEpsonService;->EpsonConnectEndJob()I

    .line 1192
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    iput-boolean v10, p1, Lepson/print/ActivityDocsPrintPreview;->isCreateJobDone:Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_6

    :catch_1
    move-exception p1

    .line 1194
    invoke-virtual {p1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 1198
    :cond_1f
    :goto_6
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object p1

    invoke-virtual {p1}, Lepson/common/ExternalFileUtils;->initTempViewDir()V

    .line 1199
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object p1

    invoke-virtual {p1}, Lepson/common/ExternalFileUtils;->initTempCRDir()V

    .line 1200
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object p1

    invoke-virtual {p1}, Lepson/common/ExternalFileUtils;->getPrintDir()Ljava/lang/String;

    .line 1202
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-virtual {p1}, Lepson/print/ActivityDocsPrintPreview;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object p1

    sget-object v0, Lepson/common/ExternalFileUtils$TempCacheDirectory;->GOOGLE_DRIVE_CONVERT:Lepson/common/ExternalFileUtils$TempCacheDirectory;

    .line 1203
    invoke-virtual {p1, v0}, Lepson/common/ExternalFileUtils;->initTempCacheDirectory(Lepson/common/ExternalFileUtils$TempCacheDirectory;)Ljava/io/File;

    .line 1205
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-virtual {p1}, Lepson/print/ActivityDocsPrintPreview;->finish()V

    goto/16 :goto_7

    .line 1155
    :sswitch_c
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-virtual {p1, v10}, Lepson/print/ActivityDocsPrintPreview;->enableProgressBar(Z)V

    .line 1156
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    const v0, 0x7f0e04bb

    invoke-virtual {p1, v0}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    const v2, 0x7f0e04b9

    invoke-virtual {v1, v2}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lepson/print/ActivityDocsPrintPreview;->showErrorDialog(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_7

    :sswitch_d
    const-string p1, "ActivityDocsPrintPreview"

    const-string v0, "handleMessage : STAR_CONVERT"

    .line 1030
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1031
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-virtual {p1, v4}, Lepson/print/ActivityDocsPrintPreview;->enableProgressBar(Z)V

    goto :goto_7

    .line 1019
    :sswitch_e
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-virtual {p1, v9}, Lepson/print/ActivityDocsPrintPreview;->showDialog(I)V

    goto :goto_7

    :sswitch_f
    const-string v0, "ActivityDocsPrintPreview"

    const-string v1, "handleMessage : CONVERTED_OK"

    .line 1035
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1036
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-virtual {v0, v10}, Lepson/print/ActivityDocsPrintPreview;->enableProgressBar(Z)V

    .line 1039
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object p1

    const-string v1, "CONVERTED_PAGE"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    invoke-static {v0, p1}, Lepson/print/ActivityDocsPrintPreview;->access$602(Lepson/print/ActivityDocsPrintPreview;I)I

    .line 1042
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-virtual {p1}, Lepson/print/ActivityDocsPrintPreview;->updatePageTextView()V

    .line 1043
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-virtual {p1}, Lepson/print/ActivityDocsPrintPreview;->updatePrevNextButtonView()V

    .line 1046
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$1300(Lepson/print/ActivityDocsPrintPreview;)Lepson/print/pdf/pdfRender;

    move-result-object v0

    invoke-virtual {v0}, Lepson/print/pdf/pdfRender;->getConvertedPagePreview()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lepson/print/ActivityDocsPrintPreview;->setNewImageView(Ljava/lang/String;)V

    .line 1049
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$600(Lepson/print/ActivityDocsPrintPreview;)I

    move-result p1

    if-ne p1, v4, :cond_20

    .line 1050
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$1300(Lepson/print/ActivityDocsPrintPreview;)Lepson/print/pdf/pdfRender;

    move-result-object v0

    invoke-virtual {v0}, Lepson/print/pdf/pdfRender;->rotate()Z

    move-result v0

    xor-int/2addr v0, v4

    invoke-static {p1, v0}, Lepson/print/ActivityDocsPrintPreview;->access$1702(Lepson/print/ActivityDocsPrintPreview;Z)Z

    .line 1051
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$1700(Lepson/print/ActivityDocsPrintPreview;)Z

    move-result v0

    invoke-virtual {p1, v0}, Lepson/print/ActivityDocsPrintPreview;->setOrentationView(Z)V

    .line 1055
    :cond_20
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    iput-boolean v4, p1, Lepson/print/ActivityDocsPrintPreview;->isEnableNFCTouch:Z

    .line 1058
    iget-boolean p1, p1, Lepson/print/ActivityDocsPrintPreview;->bAutoStartPrint:Z

    if-eqz p1, :cond_21

    .line 1060
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$2500(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {p1, v0}, Lepson/print/ActivityDocsPrintPreview;->onClick(Landroid/view/View;)V

    const-string p1, "ActivityDocsPrintPreview"

    const-string v0, "onClick(mPrint)"

    .line 1061
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1062
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    iput-boolean v10, p1, Lepson/print/ActivityDocsPrintPreview;->bAutoStartPrint:Z

    :cond_21
    :goto_7
    return v10

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0xa
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1e
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_f
        0x1 -> :sswitch_e
        0x4 -> :sswitch_d
        0x6 -> :sswitch_c
        0x7 -> :sswitch_b
        0xa -> :sswitch_a
        0xb -> :sswitch_9
        0xd -> :sswitch_8
        0x16 -> :sswitch_7
        0x1e -> :sswitch_6
        0x1f -> :sswitch_5
        0x20 -> :sswitch_4
        0x21 -> :sswitch_3
        0x27 -> :sswitch_2
        0x32 -> :sswitch_1
        0x64 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_3
    .packed-switch -0x4b9
        :pswitch_7
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_8
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_8
    .end packed-switch
.end method

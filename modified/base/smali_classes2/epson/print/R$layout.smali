.class public final Lepson/print/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final abc_action_bar_title_item:I = 0x7f0a0000

.field public static final abc_action_bar_up_container:I = 0x7f0a0001

.field public static final abc_action_menu_item_layout:I = 0x7f0a0002

.field public static final abc_action_menu_layout:I = 0x7f0a0003

.field public static final abc_action_mode_bar:I = 0x7f0a0004

.field public static final abc_action_mode_close_item_material:I = 0x7f0a0005

.field public static final abc_activity_chooser_view:I = 0x7f0a0006

.field public static final abc_activity_chooser_view_list_item:I = 0x7f0a0007

.field public static final abc_alert_dialog_button_bar_material:I = 0x7f0a0008

.field public static final abc_alert_dialog_material:I = 0x7f0a0009

.field public static final abc_alert_dialog_title_material:I = 0x7f0a000a

.field public static final abc_cascading_menu_item_layout:I = 0x7f0a000b

.field public static final abc_dialog_title_material:I = 0x7f0a000c

.field public static final abc_expanded_menu_layout:I = 0x7f0a000d

.field public static final abc_list_menu_item_checkbox:I = 0x7f0a000e

.field public static final abc_list_menu_item_icon:I = 0x7f0a000f

.field public static final abc_list_menu_item_layout:I = 0x7f0a0010

.field public static final abc_list_menu_item_radio:I = 0x7f0a0011

.field public static final abc_popup_menu_header_item_layout:I = 0x7f0a0012

.field public static final abc_popup_menu_item_layout:I = 0x7f0a0013

.field public static final abc_screen_content_include:I = 0x7f0a0014

.field public static final abc_screen_simple:I = 0x7f0a0015

.field public static final abc_screen_simple_overlay_action_mode:I = 0x7f0a0016

.field public static final abc_screen_toolbar:I = 0x7f0a0017

.field public static final abc_search_dropdown_item_icons_2line:I = 0x7f0a0018

.field public static final abc_search_view:I = 0x7f0a0019

.field public static final abc_select_dialog_material:I = 0x7f0a001a

.field public static final abc_tooltip:I = 0x7f0a001b

.field public static final activity_camera_preview:I = 0x7f0a001c

.field public static final activity_camera_preview_option:I = 0x7f0a001d

.field public static final activity_camera_print_preview:I = 0x7f0a001e

.field public static final activity_check_eula:I = 0x7f0a001f

.field public static final activity_config_service:I = 0x7f0a0020

.field public static final activity_confirm_multi:I = 0x7f0a0021

.field public static final activity_confirm_read_memcard:I = 0x7f0a0022

.field public static final activity_confirm_write_memcard:I = 0x7f0a0023

.field public static final activity_convert_progress:I = 0x7f0a0024

.field public static final activity_crop_image:I = 0x7f0a0025

.field public static final activity_directory_selecter:I = 0x7f0a0026

.field public static final activity_document_size_edit:I = 0x7f0a0027

.field public static final activity_document_size_setting:I = 0x7f0a0028

.field public static final activity_dropbox_v2_sign_in:I = 0x7f0a0029

.field public static final activity_folder_select:I = 0x7f0a002a

.field public static final activity_i2_double_side_scan_setting:I = 0x7f0a002b

.field public static final activity_i2_scan:I = 0x7f0a002c

.field public static final activity_i2_scan_setting:I = 0x7f0a002d

.field public static final activity_i2_setting_detail:I = 0x7f0a002e

.field public static final activity_image_select:I = 0x7f0a002f

.field public static final activity_image_selector:I = 0x7f0a0030

.field public static final activity_info_display:I = 0x7f0a0031

.field public static final activity_ink_rpln_progress_dialog:I = 0x7f0a0032

.field public static final activity_jump_url_dialog:I = 0x7f0a0033

.field public static final activity_license_top:I = 0x7f0a0034

.field public static final activity_list_and_download:I = 0x7f0a0035

.field public static final activity_mcphotocopy_top:I = 0x7f0a0036

.field public static final activity_memcard_image_grid:I = 0x7f0a0037

.field public static final activity_memcard_top:I = 0x7f0a0038

.field public static final activity_notice_guide:I = 0x7f0a0039

.field public static final activity_photo_folder:I = 0x7f0a003a

.field public static final activity_picture_view:I = 0x7f0a003b

.field public static final activity_scan:I = 0x7f0a003c

.field public static final activity_temp_image_pager:I = 0x7f0a003d

.field public static final activity_user_survey_invitation:I = 0x7f0a003e

.field public static final activity_web_share:I = 0x7f0a003f

.field public static final add_bookmark:I = 0x7f0a0040

.field public static final alert_dialog_title:I = 0x7f0a0041

.field public static final ble_progress_layout:I = 0x7f0a0042

.field public static final blocked_ip_error:I = 0x7f0a0043

.field public static final boxsdk_activity_oauth:I = 0x7f0a0044

.field public static final boxsdk_alert_dialog_text_entry:I = 0x7f0a0045

.field public static final boxsdk_avatar_item:I = 0x7f0a0046

.field public static final boxsdk_choose_auth_activity:I = 0x7f0a0047

.field public static final boxsdk_list_item_account:I = 0x7f0a0048

.field public static final boxsdk_list_item_new_account:I = 0x7f0a0049

.field public static final choose_storage_server:I = 0x7f0a004a

.field public static final com_customline_row:I = 0x7f0a004b

.field public static final copy:I = 0x7f0a004c

.field public static final copy_custom_scale:I = 0x7f0a004d

.field public static final copy_scale:I = 0x7f0a004e

.field public static final copy_setting:I = 0x7f0a004f

.field public static final device_information:I = 0x7f0a0050

.field public static final dialog_custom_layout01:I = 0x7f0a0051

.field public static final dialog_epsonconnect:I = 0x7f0a0052

.field public static final dialog_inkrpln_invitation:I = 0x7f0a0053

.field public static final dialog_local_prgress:I = 0x7f0a0054

.field public static final dialog_nozzle_check_guidance:I = 0x7f0a0055

.field public static final dialog_progress:I = 0x7f0a0056

.field public static final dialog_progress_wifi_direct:I = 0x7f0a0057

.field public static final dialog_scan_continue_confirmation:I = 0x7f0a0058

.field public static final dialog_three_button:I = 0x7f0a0059

.field public static final dlg_mes_checkbox:I = 0x7f0a005a

.field public static final doc_print_preview:I = 0x7f0a005b

.field public static final edit_bookmark:I = 0x7f0a005c

.field public static final epsonconnect_print_log:I = 0x7f0a005d

.field public static final epsonconnect_printer_item:I = 0x7f0a005e

.field public static final epsonconnect_printer_setting_layout:I = 0x7f0a005f

.field public static final file_browser:I = 0x7f0a0060

.field public static final file_list_item:I = 0x7f0a0061

.field public static final file_select_row:I = 0x7f0a0062

.field public static final folder_content_layout:I = 0x7f0a0063

.field public static final folder_content_layout_for_upload:I = 0x7f0a0064

.field public static final folder_content_save_option:I = 0x7f0a0065

.field public static final folder_listview:I = 0x7f0a0066

.field public static final fragment_camera_preview:I = 0x7f0a0067

.field public static final fragment_camera_preview_option:I = 0x7f0a0068

.field public static final fragment_guide_webview:I = 0x7f0a0069

.field public static final fragment_image_grid:I = 0x7f0a006a

.field public static final fragment_image_preview_view:I = 0x7f0a006b

.field public static final fragment_image_view:I = 0x7f0a006c

.field public static final fragment_image_view_multi:I = 0x7f0a006d

.field public static final fragment_image_view_single:I = 0x7f0a006e

.field public static final fragment_item_grid:I = 0x7f0a006f

.field public static final fragment_item_list:I = 0x7f0a0070

.field public static final fragment_password_dialog:I = 0x7f0a0071

.field public static final fwupdate_progress:I = 0x7f0a0072

.field public static final grid_item_imgsel:I = 0x7f0a0073

.field public static final help_about:I = 0x7f0a0074

.field public static final home_top:I = 0x7f0a0075

.field public static final i2_scan_setting_item:I = 0x7f0a0076

.field public static final image_collect_adjustment_bar:I = 0x7f0a0077

.field public static final image_collect_layout:I = 0x7f0a0078

.field public static final image_collect_pallet:I = 0x7f0a0079

.field public static final image_collect_preview:I = 0x7f0a007a

.field public static final image_collect_toolbar_color_adjustment:I = 0x7f0a007b

.field public static final image_collect_toolbar_crop_image:I = 0x7f0a007c

.field public static final image_collect_toolbar_enhance_text:I = 0x7f0a007d

.field public static final ink_item:I = 0x7f0a007e

.field public static final ipprinter_setting_layout:I = 0x7f0a007f

.field public static final iprint_home_layout:I = 0x7f0a0080

.field public static final iprintconnect:I = 0x7f0a0081

.field public static final iprintconnect_start:I = 0x7f0a0082

.field public static final line:I = 0x7f0a0083

.field public static final listitem_device:I = 0x7f0a0084

.field public static final listitem_image_folder:I = 0x7f0a0085

.field public static final login_screen_layout:I = 0x7f0a0086

.field public static final main_support:I = 0x7f0a0087

.field public static final main_webview:I = 0x7f0a0088

.field public static final maintain:I = 0x7f0a0089

.field public static final maintain_battery_info:I = 0x7f0a008a

.field public static final maintain_executing_dialog:I = 0x7f0a008b

.field public static final memcard_card_grid_view_elem:I = 0x7f0a008c

.field public static final memcard_folder:I = 0x7f0a008d

.field public static final memcard_folder_list:I = 0x7f0a008e

.field public static final memcard_gridview:I = 0x7f0a008f

.field public static final memcard_read_progress:I = 0x7f0a0090

.field public static final memcard_write_progress:I = 0x7f0a0091

.field public static final menu_item_btn:I = 0x7f0a0092

.field public static final my_dialog_layout:I = 0x7f0a0093

.field public static final my_progress:I = 0x7f0a0094

.field public static final mylistview:I = 0x7f0a0095

.field public static final navigationbar:I = 0x7f0a0096

.field public static final notification_action:I = 0x7f0a0097

.field public static final notification_action_tombstone:I = 0x7f0a0098

.field public static final notification_media_action:I = 0x7f0a0099

.field public static final notification_media_cancel_action:I = 0x7f0a009a

.field public static final notification_template_big_media:I = 0x7f0a009b

.field public static final notification_template_big_media_custom:I = 0x7f0a009c

.field public static final notification_template_big_media_narrow:I = 0x7f0a009d

.field public static final notification_template_big_media_narrow_custom:I = 0x7f0a009e

.field public static final notification_template_custom_big:I = 0x7f0a009f

.field public static final notification_template_icon_group:I = 0x7f0a00a0

.field public static final notification_template_lines_media:I = 0x7f0a00a1

.field public static final notification_template_media:I = 0x7f0a00a2

.field public static final notification_template_media_custom:I = 0x7f0a00a3

.field public static final notification_template_part_chronometer:I = 0x7f0a00a4

.field public static final notification_template_part_time:I = 0x7f0a00a5

.field public static final page_range_layout:I = 0x7f0a00a6

.field public static final paper_source_setting:I = 0x7f0a00a7

.field public static final pdf_file_password:I = 0x7f0a00a8

.field public static final photo_preview:I = 0x7f0a00a9

.field public static final preview_open_dialog:I = 0x7f0a00aa

.field public static final print_web:I = 0x7f0a00ab

.field public static final printer_info_detail_item:I = 0x7f0a00ac

.field public static final printer_item:I = 0x7f0a00ad

.field public static final progress:I = 0x7f0a00ae

.field public static final progress_layout:I = 0x7f0a00af

.field public static final sample_i2_scan_area_view:I = 0x7f0a00b0

.field public static final save_as:I = 0x7f0a00b1

.field public static final scan:I = 0x7f0a00b2

.field public static final scan_found_item:I = 0x7f0a00b3

.field public static final scan_settings_advance_density:I = 0x7f0a00b4

.field public static final scan_settings_detail_2:I = 0x7f0a00b5

.field public static final scan_settings_item:I = 0x7f0a00b6

.field public static final scan_settings_main:I = 0x7f0a00b7

.field public static final search_printer_layout:I = 0x7f0a00b8

.field public static final select_dialog_item_material:I = 0x7f0a00b9

.field public static final select_dialog_multichoice_material:I = 0x7f0a00ba

.field public static final select_dialog_singlechoice_material:I = 0x7f0a00bb

.field public static final select_list_row:I = 0x7f0a00bc

.field public static final setting_detail:I = 0x7f0a00bd

.field public static final setting_item:I = 0x7f0a00be

.field public static final setting_layout:I = 0x7f0a00bf

.field public static final simple_list_item_2:I = 0x7f0a00c0

.field public static final ssl_certificate:I = 0x7f0a00c1

.field public static final storage_progress:I = 0x7f0a00c2

.field public static final support_simple_spinner_dropdown_item:I = 0x7f0a00c3

.field public static final tab_bookmark:I = 0x7f0a00c4

.field public static final tab_history:I = 0x7f0a00c5

.field public static final upload_dialog:I = 0x7f0a00c6

.field public static final welcome:I = 0x7f0a00c7

.field public static final wheel_dialog:I = 0x7f0a00c8

.field public static final wifidirect_error:I = 0x7f0a00c9

.field public static final wifidirect_manual:I = 0x7f0a00ca

.field public static final wifidirect_start:I = 0x7f0a00cb

.field public static final work_dialog_transparent:I = 0x7f0a00cc


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 4280
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.class Lepson/print/ActivityDocsPrintPreview$19;
.super Ljava/lang/Object;
.source "ActivityDocsPrintPreview.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/ActivityDocsPrintPreview;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/ActivityDocsPrintPreview;

.field final synthetic val$etPW:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lepson/print/ActivityDocsPrintPreview;Landroid/widget/EditText;)V
    .locals 0

    .line 1797
    iput-object p1, p0, Lepson/print/ActivityDocsPrintPreview$19;->this$0:Lepson/print/ActivityDocsPrintPreview;

    iput-object p2, p0, Lepson/print/ActivityDocsPrintPreview$19;->val$etPW:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    .line 1801
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$19;->this$0:Lepson/print/ActivityDocsPrintPreview;

    iget-object p2, p0, Lepson/print/ActivityDocsPrintPreview$19;->val$etPW:Landroid/widget/EditText;

    invoke-virtual {p2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lepson/print/ActivityDocsPrintPreview;->access$2302(Lepson/print/ActivityDocsPrintPreview;Ljava/lang/String;)Ljava/lang/String;

    .line 1804
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$19;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$1300(Lepson/print/ActivityDocsPrintPreview;)Lepson/print/pdf/pdfRender;

    move-result-object p1

    iget-object p2, p0, Lepson/print/ActivityDocsPrintPreview$19;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p2}, Lepson/print/ActivityDocsPrintPreview;->access$1000(Lepson/print/ActivityDocsPrintPreview;)Ljava/lang/String;

    move-result-object p2

    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$19;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v0}, Lepson/print/ActivityDocsPrintPreview;->access$2300(Lepson/print/ActivityDocsPrintPreview;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lepson/print/ActivityDocsPrintPreview$19;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-virtual {p1, p2, v0, v1}, Lepson/print/pdf/pdfRender;->openPdfFile(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 1808
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$19;->this$0:Lepson/print/ActivityDocsPrintPreview;

    iget-object p1, p1, Lepson/print/ActivityDocsPrintPreview;->mUiHandler:Landroid/os/Handler;

    const/16 p2, 0xd

    invoke-virtual {p1, p2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 1811
    :cond_0
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$19;->this$0:Lepson/print/ActivityDocsPrintPreview;

    iget-object p1, p1, Lepson/print/ActivityDocsPrintPreview;->mUiHandler:Landroid/os/Handler;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_0
    return-void
.end method

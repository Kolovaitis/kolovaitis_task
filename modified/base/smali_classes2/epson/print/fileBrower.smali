.class public Lepson/print/fileBrower;
.super Lepson/print/ActivityIACommon;
.source "fileBrower.java"

# interfaces
.implements Lepson/print/CommonDefine;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/fileBrower$StringComparator;
    }
.end annotation


# static fields
.field private static final Menu_Create:I = 0x1

.field private static final Menu_Delete:I = 0x1

.field private static final Menu_Rename:I = 0x2

.field private static final RESULT_RUNTIMEPERMMISSION:I = 0x1


# instance fields
.field private RootPath:Ljava/lang/String;

.field private TopPath:Ljava/lang/String;

.field private currentDirectory:Ljava/io/File;

.field private directoryEntries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private directotyEntries_new_v2:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lepson/print/IconifiedText;",
            ">;"
        }
    .end annotation
.end field

.field private list:Landroid/widget/ListView;

.field private listItemIndex:Landroid/widget/AdapterView$AdapterContextMenuInfo;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 37
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lepson/print/fileBrower;->directoryEntries:Ljava/util/List;

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lepson/print/fileBrower;->directotyEntries_new_v2:Ljava/util/List;

    const-string v0, "/"

    .line 51
    iput-object v0, p0, Lepson/print/fileBrower;->RootPath:Ljava/lang/String;

    const-string v0, "/mnt/"

    .line 55
    iput-object v0, p0, Lepson/print/fileBrower;->TopPath:Ljava/lang/String;

    .line 56
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lepson/print/fileBrower;->TopPath:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lepson/print/fileBrower;->currentDirectory:Ljava/io/File;

    return-void
.end method

.method static synthetic access$000(Lepson/print/fileBrower;)Ljava/util/List;
    .locals 0

    .line 37
    iget-object p0, p0, Lepson/print/fileBrower;->directoryEntries:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$100(Lepson/print/fileBrower;)Landroid/widget/ListView;
    .locals 0

    .line 37
    iget-object p0, p0, Lepson/print/fileBrower;->list:Landroid/widget/ListView;

    return-object p0
.end method

.method static synthetic access$200(Lepson/print/fileBrower;Ljava/io/File;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;,
            Ljava/io/IOException;
        }
    .end annotation

    .line 37
    invoke-direct {p0, p1}, Lepson/print/fileBrower;->deleteSubfile(Ljava/io/File;)V

    return-void
.end method

.method static synthetic access$300(Lepson/print/fileBrower;Ljava/io/File;)V
    .locals 0

    .line 37
    invoke-direct {p0, p1}, Lepson/print/fileBrower;->browseTo(Ljava/io/File;)V

    return-void
.end method

.method static synthetic access$400(Lepson/print/fileBrower;Ljava/lang/String;)Z
    .locals 0

    .line 37
    invoke-direct {p0, p1}, Lepson/print/fileBrower;->isNameFolder(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method static synthetic access$500(Lepson/print/fileBrower;)V
    .locals 0

    .line 37
    invoke-direct {p0}, Lepson/print/fileBrower;->mMessage()V

    return-void
.end method

.method private browseTo(Ljava/io/File;)V
    .locals 4

    .line 547
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 548
    iput-object p1, p0, Lepson/print/fileBrower;->currentDirectory:Ljava/io/File;

    .line 549
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object p1

    invoke-direct {p0, p1}, Lepson/print/fileBrower;->fill([Ljava/io/File;)V

    goto/16 :goto_1

    .line 553
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lepson/print/Util/Photo;->IsFilePhoto(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 554
    invoke-direct {p0, p1}, Lepson/print/fileBrower;->startPhotoPreview(Ljava/io/File;)V

    goto/16 :goto_1

    .line 556
    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lepson/print/fileBrower;->isFilePdf(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x3

    const v3, 0x7f0e0373

    if-nez v0, :cond_4

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lepson/common/Utils;->isGConvertFile(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0

    .line 570
    :cond_2
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lepson/print/fileBrower;->isFileText(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 571
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lepson/print/WebviewActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "from"

    .line 573
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "file:/"

    .line 576
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v1, "android.intent.extra.TEXT"

    .line 577
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 578
    invoke-virtual {p0, v0}, Lepson/print/fileBrower;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 581
    :cond_3
    invoke-virtual {p0}, Lepson/print/fileBrower;->getBaseContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p0, v3}, Lepson/print/fileBrower;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 558
    :cond_4
    :goto_0
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lepson/print/fileBrower;->getFileSize(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 559
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lepson/print/ActivityDocsPrintPreview;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "from"

    .line 561
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "send document"

    .line 562
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "print_log"

    .line 563
    invoke-direct {p0, p1}, Lepson/print/fileBrower;->getPrintLog(Ljava/io/File;)Lcom/epson/iprint/prtlogger/PrintLog;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 565
    invoke-virtual {p0, v0}, Lepson/print/fileBrower;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    :cond_5
    const-string p1, "file size"

    const-string v0, ">max"

    .line 567
    invoke-static {p1, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 568
    invoke-virtual {p0}, Lepson/print/fileBrower;->getBaseContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p0, v3}, Lepson/print/fileBrower;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    .line 584
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_1
    return-void
.end method

.method private browseToRoot()V
    .locals 2

    .line 460
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lepson/print/fileBrower;->TopPath:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lepson/print/fileBrower;->browseTo(Ljava/io/File;)V

    return-void
.end method

.method private createIcon(Ljava/lang/String;)Lepson/print/IconifiedText;
    .locals 4

    .line 777
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 778
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 779
    new-instance p1, Lepson/print/IconifiedText;

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lepson/print/fileBrower;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0700a0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {p1, v0, v1, v2}, Lepson/print/IconifiedText;-><init>(Ljava/lang/String;Landroid/graphics/drawable/Drawable;Z)V

    return-object p1

    .line 781
    :cond_0
    invoke-static {p1}, Lepson/print/Util/Photo;->IsFilePhoto(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    .line 782
    new-instance p1, Lepson/print/IconifiedText;

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lepson/print/fileBrower;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0700b5

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-direct {p1, v0, v1, v2}, Lepson/print/IconifiedText;-><init>(Ljava/lang/String;Landroid/graphics/drawable/Drawable;Z)V

    return-object p1

    .line 784
    :cond_1
    invoke-static {p1}, Lepson/common/Utils;->isGConvertFile(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-direct {p0, p1}, Lepson/print/fileBrower;->isFilePdf(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-direct {p0, p1}, Lepson/print/fileBrower;->isFileText(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    .line 787
    :cond_2
    new-instance p1, Lepson/print/IconifiedText;

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lepson/print/fileBrower;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0700b2

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-direct {p1, v0, v1, v2}, Lepson/print/IconifiedText;-><init>(Ljava/lang/String;Landroid/graphics/drawable/Drawable;Z)V

    return-object p1

    .line 785
    :cond_3
    :goto_0
    new-instance p1, Lepson/print/IconifiedText;

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lepson/print/fileBrower;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f07009f

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-direct {p1, v0, v1, v2}, Lepson/print/IconifiedText;-><init>(Ljava/lang/String;Landroid/graphics/drawable/Drawable;Z)V

    return-object p1
.end method

.method private createNewFolder()V
    .locals 5

    const-string v0, "path current"

    .line 359
    iget-object v1, p0, Lepson/print/fileBrower;->currentDirectory:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 360
    new-instance v0, Landroid/widget/EditText;

    invoke-direct {v0, p0}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x1

    .line 362
    new-array v1, v1, [Landroid/text/InputFilter;

    .line 363
    new-instance v2, Landroid/text/InputFilter$LengthFilter;

    const/16 v3, 0x40

    invoke-direct {v2, v3}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 364
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 366
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 367
    iget-object v2, p0, Lepson/print/fileBrower;->currentDirectory:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    .line 368
    new-instance v3, Lepson/print/fileBrower$8;

    invoke-direct {v3, p0, v0, v2}, Lepson/print/fileBrower$8;-><init>(Lepson/print/fileBrower;Landroid/widget/EditText;Ljava/lang/String;)V

    .line 401
    new-instance v2, Lepson/print/fileBrower$9;

    invoke-direct {v2, p0}, Lepson/print/fileBrower$9;-><init>(Lepson/print/fileBrower;)V

    const-string v4, "Question"

    .line 409
    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v4, 0x7f0700f0

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0e02a6

    invoke-virtual {p0, v1}, Lepson/print/fileBrower;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0e04f2

    .line 410
    invoke-virtual {v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0e0476

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void
.end method

.method private deleteSubfile(Ljava/io/File;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;,
            Ljava/io/IOException;
        }
    .end annotation

    .line 799
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 800
    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 801
    invoke-direct {p0, v3}, Lepson/print/fileBrower;->fileEnableDelete(Ljava/io/File;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 802
    invoke-direct {p0, v3}, Lepson/print/fileBrower;->deleteSubfile(Ljava/io/File;)V

    .line 803
    :cond_0
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 805
    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    .line 806
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_2

    return-void

    .line 807
    :cond_2
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to delete directory hierarchy \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\""

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private fileEnableDelete(Ljava/io/File;)Z
    .locals 2

    .line 812
    invoke-virtual {p1}, Ljava/io/File;->isFile()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    .line 815
    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object p1

    array-length p1, p1

    if-nez p1, :cond_1

    return v1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method private fill([Ljava/io/File;)V
    .locals 8

    const-string v0, "call"

    const-string v1, "fill function"

    .line 624
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 625
    iget-object v0, p0, Lepson/print/fileBrower;->directoryEntries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 626
    iget-object v0, p0, Lepson/print/fileBrower;->directotyEntries_new_v2:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_6

    const-string v2, "abc"

    .line 637
    array-length v3, p1

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 642
    iget-object v2, p0, Lepson/print/fileBrower;->currentDirectory:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    .line 643
    array-length v3, p1

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_5

    aget-object v5, p1, v4

    .line 644
    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    const-string v7, "."

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    goto/16 :goto_1

    .line 647
    :cond_0
    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lepson/print/fileBrower;->isFilePdf(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 651
    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lepson/print/fileBrower;->getFileShow(Ljava/lang/String;)Z

    .line 658
    :cond_1
    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "/mnt/secure"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 659
    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "/mnt/asec"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 660
    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "/mnt/.lfs"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 661
    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "/mnt/obb"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    goto :goto_1

    .line 663
    :cond_2
    iget-object v6, p0, Lepson/print/fileBrower;->currentDirectory:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "/mnt"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 664
    invoke-virtual {v5}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {v5}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v6

    if-nez v6, :cond_3

    goto :goto_1

    .line 668
    :cond_3
    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lepson/print/fileBrower;->getFileShow(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 669
    iget-object v6, p0, Lepson/print/fileBrower;->directoryEntries:Ljava/util/List;

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 670
    iget-object v6, p0, Lepson/print/fileBrower;->directotyEntries_new_v2:Ljava/util/List;

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lepson/print/fileBrower;->createIcon(Ljava/lang/String;)Lepson/print/IconifiedText;

    move-result-object v5

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    .line 682
    :cond_5
    iget-object p1, p0, Lepson/print/fileBrower;->directoryEntries:Ljava/util/List;

    new-instance v2, Lepson/print/fileBrower$StringComparator;

    invoke-direct {v2, p0}, Lepson/print/fileBrower$StringComparator;-><init>(Lepson/print/fileBrower;)V

    invoke-static {p1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 683
    iget-object p1, p0, Lepson/print/fileBrower;->directotyEntries_new_v2:Ljava/util/List;

    new-instance v2, Lepson/print/IconifiedText;

    invoke-direct {v2}, Lepson/print/IconifiedText;-><init>()V

    invoke-static {p1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 690
    :cond_6
    iget-object p1, p0, Lepson/print/fileBrower;->currentDirectory:Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lepson/print/fileBrower;->setTitle(Ljava/lang/CharSequence;)V

    const-string p1, "getparent"

    .line 701
    iget-object v2, p0, Lepson/print/fileBrower;->currentDirectory:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 704
    iget-object p1, p0, Lepson/print/fileBrower;->currentDirectory:Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object p1

    iget-object v2, p0, Lepson/print/fileBrower;->RootPath:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_7

    const-string p1, "compare"

    const-string v2, "not equals"

    .line 705
    invoke-static {p1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string p1, "getparent currentDirectory"

    .line 706
    iget-object v2, p0, Lepson/print/fileBrower;->currentDirectory:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 707
    iget-object p1, p0, Lepson/print/fileBrower;->directoryEntries:Ljava/util/List;

    const-string v2, ".."

    invoke-interface {p1, v1, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 708
    iget-object p1, p0, Lepson/print/fileBrower;->directotyEntries_new_v2:Ljava/util/List;

    new-instance v2, Lepson/print/IconifiedText;

    const-string v3, ".."

    invoke-virtual {p0}, Lepson/print/fileBrower;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f070103

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-direct {v2, v3, v4, v0}, Lepson/print/IconifiedText;-><init>(Ljava/lang/String;Landroid/graphics/drawable/Drawable;Z)V

    invoke-interface {p1, v1, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 712
    :cond_7
    new-instance p1, Lepson/print/IconifiedTextListAdapter;

    invoke-direct {p1, p0}, Lepson/print/IconifiedTextListAdapter;-><init>(Landroid/content/Context;)V

    .line 713
    iget-object v0, p0, Lepson/print/fileBrower;->directotyEntries_new_v2:Ljava/util/List;

    invoke-virtual {p1, v0}, Lepson/print/IconifiedTextListAdapter;->setListItems(Ljava/util/List;)V

    .line 714
    iget-object v0, p0, Lepson/print/fileBrower;->list:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method private getFileShow(Ljava/lang/String;)Z
    .locals 3

    .line 531
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 532
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    .line 533
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    .line 536
    :cond_0
    invoke-direct {p0, p1}, Lepson/print/fileBrower;->getFileType(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    return v1

    :cond_1
    return v2

    :cond_2
    return v2
.end method

.method private getFileSize(Ljava/lang/String;)Z
    .locals 6

    .line 513
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 514
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    .line 515
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_0

    return v2

    .line 518
    :cond_0
    invoke-static {p1}, Lepson/print/Util/Photo;->IsFilePhoto(Ljava/lang/String;)Z

    move-result p1

    const/4 v1, 0x1

    if-eqz p1, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    const-wide/32 v4, 0x300000

    cmp-long p1, v2, v4

    if-gez p1, :cond_1

    return v1

    :cond_1
    return v1

    :cond_2
    return v2
.end method

.method private getFileType(Ljava/lang/String;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 504
    :cond_0
    invoke-static {p1}, Lepson/common/Utils;->isGConvertFile(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-direct {p0, p1}, Lepson/print/fileBrower;->isFilePdf(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {p1}, Lepson/print/Util/Photo;->IsFilePhoto(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-direct {p0, p1}, Lepson/print/fileBrower;->isFileText(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    return v0

    :cond_2
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method private getPrintLog(Ljava/io/File;)Lcom/epson/iprint/prtlogger/PrintLog;
    .locals 2

    .line 616
    new-instance v0, Lcom/epson/iprint/prtlogger/PrintLog;

    invoke-direct {v0}, Lcom/epson/iprint/prtlogger/PrintLog;-><init>()V

    const/4 v1, 0x2

    .line 617
    iput v1, v0, Lcom/epson/iprint/prtlogger/PrintLog;->uiRoute:I

    .line 618
    invoke-static {p1}, Lcom/epson/iprint/prtlogger/PrintLog;->getFileExtension(Ljava/io/File;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, v0, Lcom/epson/iprint/prtlogger/PrintLog;->originalFileExtension:Ljava/lang/String;

    return-object v0
.end method

.method public static isAvailableFileName(Ljava/lang/String;)Z
    .locals 5

    const/4 v0, 0x0

    if-eqz p0, :cond_4

    .line 324
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    goto :goto_1

    :cond_0
    const/16 v1, 0xa

    .line 330
    new-array v1, v1, [C

    fill-array-data v1, :array_0

    const/4 v2, 0x0

    .line 331
    :goto_0
    array-length v3, v1

    if-ge v2, v3, :cond_2

    .line 332
    aget-char v3, v1, v2

    invoke-virtual {p0, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    return v0

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    const-string v1, "."

    .line 338
    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_3

    return v0

    :cond_3
    const/4 p0, 0x1

    return p0

    :cond_4
    :goto_1
    return v0

    :array_0
    .array-data 2
        0x3cs
        0x3es
        0x3as
        0x2as
        0x3fs
        0x22s
        0x2fs
        0x5cs
        0x7cs
        0xa5s
    .end array-data
.end method

.method private isFilePdf(Ljava/lang/String;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    :cond_0
    const-string v1, "application/pdf"

    .line 493
    invoke-static {p1, v1}, Lepson/common/Utils;->checkMimeType(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 p1, 0x1

    return p1

    :cond_1
    return v0
.end method

.method private isFileText(Ljava/lang/String;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    :cond_0
    const-string v1, "text/plain"

    .line 482
    invoke-static {p1, v1}, Lepson/common/Utils;->checkMimeType(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 p1, 0x1

    return p1

    :cond_1
    return v0
.end method

.method private isNameFolder(Ljava/lang/String;)Z
    .locals 4

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    :cond_0
    const-string v1, ""

    .line 432
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    return v0

    :cond_1
    const/4 v1, 0x1

    .line 435
    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    return v0

    :cond_2
    const-string v2, "/"

    .line 438
    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_3

    return v0

    .line 441
    :cond_3
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    const/16 v2, 0x40

    if-le p1, v2, :cond_4

    return v0

    :cond_4
    return v1
.end method

.method private mMessage()V
    .locals 4

    .line 417
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0e04b4

    .line 418
    invoke-virtual {p0, v1}, Lepson/print/fileBrower;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0e04f2

    .line 419
    invoke-virtual {p0, v2}, Lepson/print/fileBrower;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lepson/print/fileBrower$10;

    invoke-direct {v3, p0}, Lepson/print/fileBrower$10;-><init>(Lepson/print/fileBrower;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 424
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 425
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method private startPhotoPreview(Ljava/io/File;)V
    .locals 4

    .line 591
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lepson/print/ActivityViewImageSelect;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "from"

    const/4 v2, 0x3

    .line 594
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "FROM_EPSON"

    const/4 v2, 0x1

    .line 600
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    .line 602
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.intent.extra.STREAM"

    .line 603
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 604
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    const-string v3, "image/png"

    invoke-static {v1, v3}, Lepson/common/Utils;->checkMimeType(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "image/png"

    .line 606
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    const-string v1, "typeprint"

    .line 609
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "print_log"

    .line 610
    invoke-direct {p0, p1}, Lepson/print/fileBrower;->getPrintLog(Ljava/io/File;)Lcom/epson/iprint/prtlogger/PrintLog;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 612
    invoke-virtual {p0, v0}, Lepson/print/fileBrower;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private upOneLevel()Z
    .locals 2

    .line 467
    iget-object v0, p0, Lepson/print/fileBrower;->currentDirectory:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 469
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->canRead()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 472
    :cond_0
    iget-object v0, p0, Lepson/print/fileBrower;->currentDirectory:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    invoke-direct {p0, v0}, Lepson/print/fileBrower;->browseTo(Ljava/io/File;)V

    :cond_1
    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    .line 170
    invoke-super {p0, p1, p2, p3}, Lepson/print/ActivityIACommon;->onActivityResult(IILandroid/content/Intent;)V

    const/4 p3, 0x1

    if-eq p1, p3, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, -0x1

    if-eq p2, p1, :cond_1

    .line 179
    invoke-virtual {p0}, Lepson/print/fileBrower;->onBackPressed()V

    goto :goto_0

    .line 176
    :cond_1
    invoke-virtual {p0}, Lepson/print/fileBrower;->startBrowse()V

    :goto_0
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .line 348
    :try_start_0
    invoke-direct {p0}, Lepson/print/fileBrower;->upOneLevel()Z

    move-result v0

    if-nez v0, :cond_0

    .line 349
    invoke-virtual {p0}, Lepson/print/fileBrower;->finish()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 352
    :catch_0
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onBackPressed()V

    :cond_0
    :goto_0
    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 9

    .line 197
    invoke-interface {p1}, Landroid/view/MenuItem;->getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;

    move-result-object v0

    check-cast v0, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    iput-object v0, p0, Lepson/print/fileBrower;->listItemIndex:Landroid/widget/AdapterView$AdapterContextMenuInfo;

    .line 198
    iget-object v0, p0, Lepson/print/fileBrower;->listItemIndex:Landroid/widget/AdapterView$AdapterContextMenuInfo;

    const/4 v1, 0x1

    if-nez v0, :cond_0

    return v1

    .line 201
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result p1

    packed-switch p1, :pswitch_data_0

    goto/16 :goto_0

    .line 247
    :pswitch_0
    new-instance v5, Ljava/io/File;

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lepson/print/fileBrower;->currentDirectory:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lepson/print/fileBrower;->directoryEntries:Ljava/util/List;

    iget-object v2, p0, Lepson/print/fileBrower;->listItemIndex:Landroid/widget/AdapterView$AdapterContextMenuInfo;

    iget v2, v2, Landroid/widget/AdapterView$AdapterContextMenuInfo;->position:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v5, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 248
    iget-object v7, p0, Lepson/print/fileBrower;->currentDirectory:Ljava/io/File;

    .line 249
    new-instance p1, Landroid/app/AlertDialog$Builder;

    invoke-direct {p1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 250
    new-instance v4, Landroid/widget/EditText;

    invoke-direct {v4, p0}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 251
    new-array v0, v1, [Landroid/text/InputFilter;

    .line 252
    new-instance v2, Landroid/text/InputFilter$LengthFilter;

    const/16 v3, 0x40

    invoke-direct {v2, v3}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    const/4 v3, 0x0

    aput-object v2, v0, v3

    .line 253
    invoke-virtual {v4, v0}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 255
    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    .line 256
    invoke-static {v0}, Lepson/common/Utils;->getSuffix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 257
    invoke-virtual {v5}, Ljava/io/File;->isFile()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 258
    invoke-static {v0}, Lepson/common/Utils;->getPreffix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 262
    :cond_1
    invoke-virtual {v4, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 264
    invoke-virtual {p1, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    const v0, 0x7f0e04fa

    .line 265
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v0, 0x7f0700e3

    .line 266
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    .line 267
    invoke-virtual {p1, v4}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    const v0, 0x7f0e04f2

    .line 268
    new-instance v8, Lepson/print/fileBrower$6;

    move-object v2, v8

    move-object v3, p0

    invoke-direct/range {v2 .. v7}, Lepson/print/fileBrower$6;-><init>(Lepson/print/fileBrower;Landroid/widget/EditText;Ljava/io/File;Ljava/lang/String;Ljava/io/File;)V

    invoke-virtual {p1, v0, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const v0, 0x7f0e0476

    .line 305
    new-instance v2, Lepson/print/fileBrower$7;

    invoke-direct {v2, p0}, Lepson/print/fileBrower$7;-><init>(Lepson/print/fileBrower;)V

    invoke-virtual {p1, v0, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 310
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_0

    .line 203
    :pswitch_1
    new-instance p1, Ljava/io/File;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lepson/print/fileBrower;->currentDirectory:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lepson/print/fileBrower;->directoryEntries:Ljava/util/List;

    iget-object v3, p0, Lepson/print/fileBrower;->listItemIndex:Landroid/widget/AdapterView$AdapterContextMenuInfo;

    iget v3, v3, Landroid/widget/AdapterView$AdapterContextMenuInfo;->position:I

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 204
    iget-object v0, p0, Lepson/print/fileBrower;->currentDirectory:Ljava/io/File;

    .line 206
    new-instance v2, Lepson/print/fileBrower$4;

    invoke-direct {v2, p0, p1, v0}, Lepson/print/fileBrower$4;-><init>(Lepson/print/fileBrower;Ljava/io/File;Ljava/io/File;)V

    .line 223
    new-instance v0, Lepson/print/fileBrower$5;

    invoke-direct {v0, p0}, Lepson/print/fileBrower$5;-><init>(Lepson/print/fileBrower;)V

    .line 227
    invoke-virtual {p1}, Ljava/io/File;->isFile()Z

    move-result p1

    const v3, 0x7f0e052b

    const v4, 0x7f0e04e6

    const v5, 0x7f0e0316

    const v6, 0x7f070099

    if-eqz p1, :cond_2

    .line 228
    new-instance p1, Landroid/app/AlertDialog$Builder;

    invoke-direct {p1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 229
    invoke-virtual {p1, v6}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 230
    invoke-virtual {p0, v5}, Lepson/print/fileBrower;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 233
    invoke-virtual {p0, v4}, Lepson/print/fileBrower;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 234
    invoke-virtual {p0, v3}, Lepson/print/fileBrower;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0

    .line 236
    :cond_2
    new-instance p1, Landroid/app/AlertDialog$Builder;

    invoke-direct {p1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 237
    invoke-virtual {p1, v6}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 238
    invoke-virtual {p0, v5}, Lepson/print/fileBrower;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 241
    invoke-virtual {p0, v4}, Lepson/print/fileBrower;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 242
    invoke-virtual {p0, v3}, Lepson/print/fileBrower;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    :goto_0
    return v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .line 63
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0a0060

    .line 64
    invoke-virtual {p0, p1}, Lepson/print/fileBrower;->setContentView(I)V

    .line 69
    new-instance p1, Ljava/io/File;

    iget-object v0, p0, Lepson/print/fileBrower;->TopPath:Ljava/lang/String;

    invoke-direct {p1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 70
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result p1

    if-nez p1, :cond_2

    .line 72
    sget-object p1, Lepson/print/fileBrower;->DEFAULT_DIR:Ljava/lang/String;

    const-string v0, "/mnt/"

    .line 73
    invoke-virtual {p1, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 75
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x5

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "/"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const-string v0, "/"

    .line 76
    invoke-virtual {p1, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 77
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "/"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 80
    :cond_1
    :goto_0
    iput-object p1, p0, Lepson/print/fileBrower;->TopPath:Ljava/lang/String;

    .line 81
    new-instance p1, Ljava/io/File;

    iget-object v0, p0, Lepson/print/fileBrower;->TopPath:Ljava/lang/String;

    invoke-direct {p1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lepson/print/fileBrower;->currentDirectory:Ljava/io/File;

    .line 85
    :cond_2
    iget-object p1, p0, Lepson/print/fileBrower;->TopPath:Ljava/lang/String;

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lepson/print/fileBrower;->setActionBar(Ljava/lang/String;Z)V

    const p1, 0x7f080149

    .line 88
    invoke-virtual {p0, p1}, Lepson/print/fileBrower;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ListView;

    iput-object p1, p0, Lepson/print/fileBrower;->list:Landroid/widget/ListView;

    .line 93
    iget-object p1, p0, Lepson/print/fileBrower;->list:Landroid/widget/ListView;

    new-instance v1, Lepson/print/fileBrower$1;

    invoke-direct {v1, p0}, Lepson/print/fileBrower$1;-><init>(Lepson/print/fileBrower;)V

    invoke-virtual {p1, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 100
    iget-object p1, p0, Lepson/print/fileBrower;->list:Landroid/widget/ListView;

    new-instance v1, Lepson/print/fileBrower$2;

    invoke-direct {v1, p0}, Lepson/print/fileBrower$2;-><init>(Lepson/print/fileBrower;)V

    invoke-virtual {p1, v1}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 122
    iget-object p1, p0, Lepson/print/fileBrower;->list:Landroid/widget/ListView;

    invoke-virtual {p0}, Lepson/print/fileBrower;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f05001e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/widget/ListView;->setBackgroundColor(I)V

    .line 125
    invoke-static {}, Lepson/print/ActivityRequestPermissions;->isRuntimePermissionSupported()Z

    move-result p1

    if-eqz p1, :cond_3

    const-string p1, "android.permission.WRITE_EXTERNAL_STORAGE"

    .line 126
    filled-new-array {p1}, [Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x2

    .line 127
    new-array v2, v1, [Ljava/lang/String;

    const v3, 0x7f0e0422

    invoke-virtual {p0, v3}, Lepson/print/fileBrower;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    aput-object v4, v2, v5

    invoke-virtual {p0, v3}, Lepson/print/fileBrower;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 128
    new-array v1, v1, [Ljava/lang/String;

    const v3, 0x7f0e0420

    .line 129
    invoke-virtual {p0, v3}, Lepson/print/fileBrower;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4}, Lepson/print/ActivityRequestPermissions$DialogParameter;->setMessage2(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v5

    .line 130
    invoke-virtual {p0, v3}, Lepson/print/fileBrower;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0e0424

    invoke-virtual {p0, v4}, Lepson/print/fileBrower;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v3, v4}, Lepson/print/ActivityRequestPermissions$DialogParameter;->setMessage3A(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v0

    .line 136
    new-instance v3, Lepson/print/ActivityRequestPermissions$Permission;

    aget-object v4, p1, v5

    invoke-direct {v3, v4, v2, v1}, Lepson/print/ActivityRequestPermissions$Permission;-><init>(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V

    .line 138
    invoke-static {p0, p1}, Lepson/print/ActivityRequestPermissions;->checkPermission(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_3

    .line 139
    invoke-static {p0, v3, v0}, Lepson/print/ActivityRequestPermissions;->requestPermission(Landroid/app/Activity;Lepson/print/ActivityRequestPermissions$Permission;I)V

    return-void

    .line 144
    :cond_3
    invoke-virtual {p0}, Lepson/print/fileBrower;->startBrowse()V

    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 1

    .line 188
    invoke-super {p0, p1, p2, p3}, Lepson/print/ActivityIACommon;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    const p2, 0x7f0e049d

    .line 190
    invoke-interface {p1, p2}, Landroid/view/ContextMenu;->setHeaderTitle(I)Landroid/view/ContextMenu;

    const/4 p2, 0x0

    const/4 p3, 0x1

    const v0, 0x7f0e0485

    .line 191
    invoke-interface {p1, p2, p3, p2, v0}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    const/4 p3, 0x2

    const v0, 0x7f0e04fa

    .line 192
    invoke-interface {p1, p2, p3, p2, v0}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    .line 792
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    const v0, 0x7f0e03db

    .line 793
    invoke-virtual {p0, v0}, Lepson/print/fileBrower;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-interface {p1, v2, v1, v2, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object p1

    const v0, 0x7f0700f0

    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object p1

    const/16 v0, 0x30

    const/16 v2, 0x66

    invoke-interface {p1, v0, v2}, Landroid/view/MenuItem;->setShortcut(CC)Landroid/view/MenuItem;

    return v1
.end method

.method protected onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 0

    const-string p1, "onclick"

    .line 745
    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 747
    iget-object p1, p0, Lepson/print/fileBrower;->directoryEntries:Ljava/util/List;

    invoke-interface {p1, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    const-string p2, "path"

    .line 749
    invoke-static {p2, p1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string p2, "."

    .line 751
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 753
    new-instance p1, Ljava/io/File;

    iget-object p2, p0, Lepson/print/fileBrower;->TopPath:Ljava/lang/String;

    invoke-direct {p1, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lepson/print/fileBrower;->browseTo(Ljava/io/File;)V

    goto :goto_0

    :cond_0
    const-string p2, ".."

    .line 755
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_1

    .line 756
    invoke-direct {p0}, Lepson/print/fileBrower;->upOneLevel()Z

    goto :goto_0

    :cond_1
    const-string p2, "."

    .line 758
    invoke-virtual {p1, p2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    .line 766
    :cond_2
    new-instance p1, Ljava/io/File;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object p4, p0, Lepson/print/fileBrower;->currentDirectory:Ljava/io/File;

    invoke-virtual {p4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p4, "/"

    invoke-virtual {p2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p4, p0, Lepson/print/fileBrower;->directotyEntries_new_v2:Ljava/util/List;

    .line 767
    invoke-interface {p4, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lepson/print/IconifiedText;

    invoke-virtual {p3}, Lepson/print/IconifiedText;->getText()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 772
    invoke-direct {p0, p1}, Lepson/print/fileBrower;->browseTo(Ljava/io/File;)V

    :goto_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .line 449
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 451
    :cond_0
    invoke-direct {p0}, Lepson/print/fileBrower;->createNewFolder()V

    .line 455
    :goto_0
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1
.end method

.method startBrowse()V
    .locals 4

    .line 148
    invoke-static {}, Lepson/common/Utils;->isMediaMounted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 149
    invoke-direct {p0}, Lepson/print/fileBrower;->browseToRoot()V

    goto :goto_0

    .line 151
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0e04ea

    .line 152
    invoke-virtual {p0, v1}, Lepson/print/fileBrower;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0e053c

    .line 153
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x0

    .line 154
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0e04bf

    .line 155
    invoke-virtual {p0, v2}, Lepson/print/fileBrower;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lepson/print/fileBrower$3;

    invoke-direct {v3, p0}, Lepson/print/fileBrower$3;-><init>(Lepson/print/fileBrower;)V

    .line 154
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 163
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 164
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    :goto_0
    return-void
.end method

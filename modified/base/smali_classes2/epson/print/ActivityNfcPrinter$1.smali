.class Lepson/print/ActivityNfcPrinter$1;
.super Ljava/lang/Object;
.source "ActivityNfcPrinter.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/ActivityNfcPrinter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/ActivityNfcPrinter;


# direct methods
.method constructor <init>(Lepson/print/ActivityNfcPrinter;)V
    .locals 0

    .line 330
    iput-object p1, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 6

    .line 333
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x0

    if-eqz v0, :cond_13

    const/4 v2, 0x1

    const/16 v3, 0x14

    if-eq v0, v3, :cond_12

    const/4 v4, 0x2

    const/4 v5, 0x3

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_3

    .line 800
    :pswitch_0
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lepson/print/MyPrinter;

    .line 803
    iget-object v0, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    const-string v2, "epson.scanner.SelectedScanner"

    const-string v4, "SCAN_REFS_SCANNER_ID"

    invoke-static {v0, v2, v4}, Lepson/common/Utils;->getPrefString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 805
    invoke-static {v0}, Lcom/epson/mobilephone/common/wifidirect/NfcMacAddrUtils;->getMacAddressFromScannerId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 806
    invoke-virtual {p1}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/epson/mobilephone/common/wifidirect/NfcMacAddrUtils;->getMacAddressFromPrinterId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "ActivityChangeWifiPrinter"

    const-string v2, "Needless changing Scanner Info"

    .line 807
    invoke-static {v0, v2}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 809
    invoke-virtual {p1}, Lepson/print/MyPrinter;->getLocation()I

    move-result v0

    if-ne v0, v5, :cond_0

    .line 810
    new-instance v0, Lepson/print/EPPrinterManager;

    iget-object v2, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    invoke-direct {v0, v2}, Lepson/print/EPPrinterManager;-><init>(Landroid/content/Context;)V

    .line 811
    invoke-virtual {p1}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lepson/print/EPPrinterManager;->loadIpPrinterInfo(Ljava/lang/String;)Lepson/print/EPPrinterInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 813
    invoke-virtual {p1}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lepson/print/EPPrinterInfo;->printerIP:Ljava/lang/String;

    .line 814
    invoke-virtual {p1}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4, v2}, Lepson/print/EPPrinterManager;->saveIPPrinterInfo(Ljava/lang/String;Lepson/print/EPPrinterInfo;)V

    .line 815
    invoke-virtual {v0}, Lepson/print/EPPrinterManager;->commitIPPrinterInfo()V

    .line 820
    :cond_0
    iget-object v0, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    iget-object v0, v0, Lepson/print/ActivityNfcPrinter;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 821
    iput v3, v0, Landroid/os/Message;->what:I

    .line 822
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 823
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    iget-object p1, p1, Lepson/print/ActivityNfcPrinter;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_3

    .line 828
    :cond_1
    new-instance v0, Lepson/print/ActivityNfcPrinter$1$4;

    invoke-direct {v0, p0, p1}, Lepson/print/ActivityNfcPrinter$1$4;-><init>(Lepson/print/ActivityNfcPrinter$1;Lepson/print/MyPrinter;)V

    new-array p1, v1, [Ljava/lang/Void;

    .line 946
    invoke-virtual {v0, p1}, Lepson/print/ActivityNfcPrinter$1$4;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_3

    .line 632
    :pswitch_1
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lepson/print/MyPrinter;

    .line 635
    iget-object v0, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    invoke-static {v0}, Lepson/print/MyPrinter;->getCurPrinter(Landroid/content/Context;)Lepson/print/MyPrinter;

    move-result-object v0

    invoke-virtual {v0}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/wifidirect/NfcMacAddrUtils;->getMacAddressFromPrinterId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 636
    invoke-virtual {p1}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/epson/mobilephone/common/wifidirect/NfcMacAddrUtils;->getMacAddressFromPrinterId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "ActivityChangeWifiPrinter"

    const-string v4, "Needless changing Printer Info"

    .line 637
    invoke-static {v0, v4}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 639
    invoke-virtual {p1}, Lepson/print/MyPrinter;->getLocation()I

    move-result v0

    if-ne v0, v5, :cond_2

    .line 640
    new-instance v0, Lepson/print/EPPrinterManager;

    iget-object v4, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    invoke-direct {v0, v4}, Lepson/print/EPPrinterManager;-><init>(Landroid/content/Context;)V

    .line 641
    invoke-virtual {p1}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lepson/print/EPPrinterManager;->loadIpPrinterInfo(Ljava/lang/String;)Lepson/print/EPPrinterInfo;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 643
    invoke-virtual {p1}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lepson/print/EPPrinterInfo;->printerIP:Ljava/lang/String;

    .line 644
    invoke-virtual {p1}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5, v4}, Lepson/print/EPPrinterManager;->saveIPPrinterInfo(Ljava/lang/String;Lepson/print/EPPrinterInfo;)V

    .line 645
    invoke-virtual {v0}, Lepson/print/EPPrinterManager;->commitIPPrinterInfo()V

    .line 649
    :cond_2
    iget-object v0, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    invoke-static {v0}, Lepson/print/ActivityNfcPrinter;->access$700(Lepson/print/ActivityNfcPrinter;)I

    move-result v0

    if-eq v0, v2, :cond_3

    .line 651
    iget-object v0, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    iget-object v0, v0, Lepson/print/ActivityNfcPrinter;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    const/16 v2, 0xd

    .line 652
    iput v2, v0, Landroid/os/Message;->what:I

    .line 653
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 654
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    iget-object p1, p1, Lepson/print/ActivityNfcPrinter;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_3

    .line 658
    :cond_3
    iget-object v0, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    iget-object v0, v0, Lepson/print/ActivityNfcPrinter;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 659
    iput v3, v0, Landroid/os/Message;->what:I

    .line 660
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 661
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    iget-object p1, p1, Lepson/print/ActivityNfcPrinter;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_3

    .line 666
    :cond_4
    new-instance v0, Lepson/print/ActivityNfcPrinter$1$3;

    invoke-direct {v0, p0, p1}, Lepson/print/ActivityNfcPrinter$1$3;-><init>(Lepson/print/ActivityNfcPrinter$1;Lepson/print/MyPrinter;)V

    new-array p1, v1, [Ljava/lang/Void;

    .line 793
    invoke-virtual {v0, p1}, Lepson/print/ActivityNfcPrinter$1$3;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_3

    .line 523
    :pswitch_2
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    sget-object v0, Lepson/print/ActivityNfcPrinter$NfcStatus;->CONNECTING_PRINTER_VIA_SIMPLEAP:Lepson/print/ActivityNfcPrinter$NfcStatus;

    iput-object v0, p1, Lepson/print/ActivityNfcPrinter;->status:Lepson/print/ActivityNfcPrinter$NfcStatus;

    .line 526
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    iget-object p1, p1, Lepson/print/ActivityNfcPrinter;->progress:Lepson/print/screen/WorkingDialog;

    invoke-virtual {p1}, Lepson/print/screen/WorkingDialog;->isShowing()Z

    move-result p1

    if-nez p1, :cond_5

    .line 527
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    iget-object p1, p1, Lepson/print/ActivityNfcPrinter;->progress:Lepson/print/screen/WorkingDialog;

    invoke-virtual {p1}, Lepson/print/screen/WorkingDialog;->show()V

    .line 530
    :cond_5
    new-instance p1, Lepson/print/ActivityNfcPrinter$1$2;

    invoke-direct {p1, p0}, Lepson/print/ActivityNfcPrinter$1$2;-><init>(Lepson/print/ActivityNfcPrinter$1;)V

    new-array v0, v1, [Ljava/lang/Void;

    .line 590
    invoke-virtual {p1, v0}, Lepson/print/ActivityNfcPrinter$1$2;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_3

    .line 432
    :pswitch_3
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    sget-object v0, Lepson/print/ActivityNfcPrinter$NfcStatus;->CONNECTING_SIMPLEAP:Lepson/print/ActivityNfcPrinter$NfcStatus;

    iput-object v0, p1, Lepson/print/ActivityNfcPrinter;->status:Lepson/print/ActivityNfcPrinter$NfcStatus;

    .line 435
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    invoke-static {p1}, Lepson/print/ActivityNfcPrinter;->access$100(Lepson/print/ActivityNfcPrinter;)Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;->isTagWithInterface()Z

    move-result p1

    if-eqz p1, :cond_d

    .line 437
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    invoke-static {p1}, Lepson/print/ActivityNfcPrinter;->access$200(Lepson/print/ActivityNfcPrinter;)I

    move-result p1

    and-int/2addr p1, v3

    const/high16 v0, 0x10000

    if-eqz p1, :cond_a

    .line 440
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    invoke-static {p1}, Lepson/print/ActivityNfcPrinter;->access$200(Lepson/print/ActivityNfcPrinter;)I

    move-result p1

    and-int/lit8 p1, p1, 0x4

    if-nez p1, :cond_8

    .line 441
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt p1, v2, :cond_8

    .line 442
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    invoke-virtual {p1}, Lepson/print/ActivityNfcPrinter;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string v2, "wifi"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/net/wifi/WifiManager;

    if-eqz p1, :cond_8

    .line 444
    invoke-virtual {p1}, Landroid/net/wifi/WifiManager;->is5GHzBandSupported()Z

    move-result p1

    if-eqz p1, :cond_6

    goto :goto_0

    .line 449
    :cond_6
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    invoke-static {p1}, Lepson/print/ActivityNfcPrinter;->access$200(Lepson/print/ActivityNfcPrinter;)I

    move-result p1

    and-int/2addr p1, v0

    const v0, 0x7f0e03e5

    if-eqz p1, :cond_7

    .line 451
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    const v2, 0x7f0e03e9

    invoke-virtual {p1, v0, v2}, Lepson/print/ActivityNfcPrinter;->showErrorMessage(II)V

    goto/16 :goto_3

    .line 454
    :cond_7
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    const v2, 0x7f0e03e8

    invoke-virtual {p1, v0, v2}, Lepson/print/ActivityNfcPrinter;->showErrorMessage(II)V

    goto/16 :goto_3

    .line 467
    :cond_8
    :goto_0
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    invoke-static {p1}, Lepson/print/ActivityNfcPrinter;->access$800(Lepson/print/ActivityNfcPrinter;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_9

    goto :goto_1

    .line 472
    :cond_9
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    const v0, 0x7f0e03e2

    const v2, 0x7f0e03e1

    invoke-virtual {p1, v0, v2}, Lepson/print/ActivityNfcPrinter;->showErrorMessage(II)V

    goto/16 :goto_3

    .line 480
    :cond_a
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    invoke-static {p1}, Lepson/print/ActivityNfcPrinter;->access$200(Lepson/print/ActivityNfcPrinter;)I

    move-result p1

    const/high16 v2, 0x1000000

    and-int/2addr p1, v2

    const v2, 0x7f0e03e4

    if-eqz p1, :cond_b

    .line 482
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    const v0, 0x7f0e03ed

    invoke-virtual {p1, v0, v2}, Lepson/print/ActivityNfcPrinter;->showErrorMessage(II)V

    goto/16 :goto_3

    .line 485
    :cond_b
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    invoke-static {p1}, Lepson/print/ActivityNfcPrinter;->access$200(Lepson/print/ActivityNfcPrinter;)I

    move-result p1

    and-int/2addr p1, v0

    const v0, 0x7f0e03ec

    if-eqz p1, :cond_c

    .line 487
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    invoke-virtual {p1, v0, v2}, Lepson/print/ActivityNfcPrinter;->showErrorMessage(II)V

    goto/16 :goto_3

    .line 492
    :cond_c
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    const v2, 0x7f0e03ee

    invoke-virtual {p1, v0, v2}, Lepson/print/ActivityNfcPrinter;->showErrorMessage(II)V

    goto/16 :goto_3

    .line 509
    :cond_d
    :goto_1
    sget-object p1, Lepson/print/ActivityNfcPrinter$3;->$SwitchMap$epson$print$ActivityNfcPrinter$NfcSimpleApRetryStatus:[I

    iget-object v0, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    iget-object v0, v0, Lepson/print/ActivityNfcPrinter;->retrystatus:Lepson/print/ActivityNfcPrinter$NfcSimpleApRetryStatus;

    invoke-virtual {v0}, Lepson/print/ActivityNfcPrinter$NfcSimpleApRetryStatus;->ordinal()I

    move-result v0

    aget p1, p1, v0

    packed-switch p1, :pswitch_data_1

    goto/16 :goto_3

    .line 514
    :pswitch_4
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    invoke-static {p1}, Lepson/print/ActivityNfcPrinter;->access$000(Lepson/print/ActivityNfcPrinter;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    invoke-static {v2}, Lepson/print/ActivityNfcPrinter;->access$1000(Lepson/print/ActivityNfcPrinter;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v0, v2, v4}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->connectSimpleAP(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;I)Z

    goto/16 :goto_3

    .line 511
    :pswitch_5
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    invoke-static {p1}, Lepson/print/ActivityNfcPrinter;->access$000(Lepson/print/ActivityNfcPrinter;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    invoke-static {v2}, Lepson/print/ActivityNfcPrinter;->access$900(Lepson/print/ActivityNfcPrinter;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v0, v2, v4}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->connectSimpleAP(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;I)Z

    goto/16 :goto_3

    .line 353
    :pswitch_6
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    sget-object v0, Lepson/print/ActivityNfcPrinter$NfcStatus;->CONNECTING_PRINTER_VIA_INFRA:Lepson/print/ActivityNfcPrinter$NfcStatus;

    iput-object v0, p1, Lepson/print/ActivityNfcPrinter;->status:Lepson/print/ActivityNfcPrinter$NfcStatus;

    .line 356
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    invoke-static {p1}, Lepson/print/ActivityNfcPrinter;->access$100(Lepson/print/ActivityNfcPrinter;)Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils$EpsonNfcConnectInfo;->isTagWithInterface()Z

    move-result p1

    if-eqz p1, :cond_f

    .line 359
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->getCurSSID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_f

    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    .line 360
    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->isSimpleAP(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_f

    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    .line 361
    invoke-static {p1}, Lepson/print/ActivityNfcPrinter;->access$200(Lepson/print/ActivityNfcPrinter;)I

    move-result p1

    and-int/2addr p1, v4

    if-eqz p1, :cond_f

    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    .line 362
    invoke-static {p1}, Lepson/print/ActivityNfcPrinter;->access$300(Lepson/print/ActivityNfcPrinter;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_f

    .line 367
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    iget-object p1, p1, Lepson/print/ActivityNfcPrinter;->progress:Lepson/print/screen/WorkingDialog;

    invoke-virtual {p1}, Lepson/print/screen/WorkingDialog;->isShowing()Z

    move-result p1

    if-nez p1, :cond_e

    .line 368
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    iget-object p1, p1, Lepson/print/ActivityNfcPrinter;->progress:Lepson/print/screen/WorkingDialog;

    invoke-virtual {p1}, Lepson/print/screen/WorkingDialog;->show()V

    .line 371
    :cond_e
    new-instance p1, Lepson/print/ActivityNfcPrinter$1$1;

    invoke-direct {p1, p0}, Lepson/print/ActivityNfcPrinter$1$1;-><init>(Lepson/print/ActivityNfcPrinter$1;)V

    new-array v0, v1, [Ljava/lang/Void;

    .line 422
    invoke-virtual {p1, v0}, Lepson/print/ActivityNfcPrinter$1$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_3

    .line 428
    :cond_f
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    iget-object p1, p1, Lepson/print/ActivityNfcPrinter;->mHandler:Landroid/os/Handler;

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_3

    .line 336
    :pswitch_7
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v0, 0x1c

    if-le p1, v0, :cond_10

    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    .line 337
    invoke-static {p1, v2}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->canAccessWiFiInfo(Landroid/content/Context;I)Z

    move-result p1

    if-nez p1, :cond_10

    .line 339
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    invoke-static {p1, v5}, Lcom/epson/mobilephone/common/wifidirect/ActivityRequestLocationPermission;->requestLocationPermissionForce(Landroid/app/Activity;I)V

    goto/16 :goto_3

    .line 342
    :cond_10
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    invoke-static {p1}, Lepson/print/ActivityNfcPrinter;->access$000(Lepson/print/ActivityNfcPrinter;)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    invoke-static {v0}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->getCurSSID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_11

    .line 344
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    iget-object p1, p1, Lepson/print/ActivityNfcPrinter;->mHandler:Landroid/os/Handler;

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_3

    .line 347
    :cond_11
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    iget-object p1, p1, Lepson/print/ActivityNfcPrinter;->mHandler:Landroid/os/Handler;

    const/16 v0, 0x9

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_3

    .line 953
    :cond_12
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lepson/print/MyPrinter;

    .line 956
    iget-object v0, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    invoke-static {v0}, Lepson/print/ActivityNfcPrinter;->access$700(Lepson/print/ActivityNfcPrinter;)I

    move-result v0

    const/4 v3, -0x1

    packed-switch v0, :pswitch_data_2

    goto :goto_2

    .line 979
    :pswitch_8
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    invoke-virtual {p1}, Lepson/print/ActivityNfcPrinter;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "ACT_RESULT"

    const-string v4, "ACT_RESULT_SAVE"

    .line 980
    invoke-virtual {p1, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "IS_NEW_SAVE"

    .line 981
    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "NO_CLEAR_RESULT"

    .line 982
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 983
    iget-object v0, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    invoke-virtual {v0, v3, p1}, Lepson/print/ActivityNfcPrinter;->setResult(ILandroid/content/Intent;)V

    goto :goto_2

    .line 969
    :pswitch_9
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    invoke-virtual {p1}, Lepson/print/ActivityNfcPrinter;->getIntent()Landroid/content/Intent;

    move-result-object p1

    .line 970
    iget-object v0, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    invoke-virtual {v0, v3, p1}, Lepson/print/ActivityNfcPrinter;->setResult(ILandroid/content/Intent;)V

    goto :goto_2

    .line 958
    :pswitch_a
    iget-object v0, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    const v3, 0x7f0e03ef

    invoke-static {v0, v3, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 961
    iget-object v0, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    const-string v2, "printer"

    invoke-virtual {p1}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, v2, p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->disconnect(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    .line 988
    :goto_2
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    invoke-virtual {p1}, Lepson/print/ActivityNfcPrinter;->finish()V

    goto :goto_3

    .line 599
    :cond_13
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object p1

    .line 600
    new-instance v0, Lepson/print/MyPrinter;

    const-string v2, "name"

    .line 601
    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "ip"

    .line 602
    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "id"

    .line 603
    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "serial_no"

    .line 604
    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v2, v3, v4, p1}, Lepson/print/MyPrinter;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 614
    invoke-static {}, Lepson/print/ActivityNfcPrinter;->access$400()Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object p1

    invoke-virtual {p1}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doCancelFindPrinter()I

    .line 615
    iget-object p1, p0, Lepson/print/ActivityNfcPrinter$1;->this$0:Lepson/print/ActivityNfcPrinter;

    invoke-static {p1, v0}, Lepson/print/ActivityNfcPrinter;->access$502(Lepson/print/ActivityNfcPrinter;Lepson/print/MyPrinter;)Lepson/print/MyPrinter;

    :goto_3
    return v1

    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_7
        :pswitch_6
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_a
        :pswitch_9
        :pswitch_8
    .end packed-switch
.end method

.class Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;
.super Landroid/os/AsyncTask;
.source "ActivityDocsPrintPreview.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/ActivityDocsPrintPreview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadRemotePreviewTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field serverConnectDialog:Lepson/print/ActivityDocsPrintPreview$CustomProDialog;

.field final synthetic this$0:Lepson/print/ActivityDocsPrintPreview;


# direct methods
.method private constructor <init>(Lepson/print/ActivityDocsPrintPreview;)V
    .locals 0

    .line 2142
    iput-object p1, p0, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lepson/print/ActivityDocsPrintPreview;Lepson/print/ActivityDocsPrintPreview$1;)V
    .locals 0

    .line 2142
    invoke-direct {p0, p1}, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;-><init>(Lepson/print/ActivityDocsPrintPreview;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 2142
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;->doInBackground([Ljava/lang/String;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Void;
    .locals 21

    move-object/from16 v1, p0

    const/4 v2, 0x0

    .line 2181
    :try_start_0
    new-instance v0, Lepson/print/screen/PrintSetting;

    iget-object v3, v1, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    sget-object v4, Lepson/print/screen/PrintSetting$Kind;->document:Lepson/print/screen/PrintSetting$Kind;

    invoke-direct {v0, v3, v4}, Lepson/print/screen/PrintSetting;-><init>(Landroid/content/Context;Lepson/print/screen/PrintSetting$Kind;)V

    .line 2182
    invoke-virtual {v0}, Lepson/print/screen/PrintSetting;->loadSettings()V

    .line 2184
    iget v8, v0, Lepson/print/screen/PrintSetting;->paperSizeValue:I

    .line 2185
    iget v9, v0, Lepson/print/screen/PrintSetting;->paperTypeValue:I

    .line 2186
    iget v10, v0, Lepson/print/screen/PrintSetting;->layoutValue:I

    .line 2187
    iget v11, v0, Lepson/print/screen/PrintSetting;->qualityValue:I

    .line 2188
    iget v12, v0, Lepson/print/screen/PrintSetting;->paperSourceValue:I

    .line 2189
    iget v13, v0, Lepson/print/screen/PrintSetting;->colorValue:I

    .line 2191
    iget v14, v0, Lepson/print/screen/PrintSetting;->brightnessValue:I

    .line 2192
    iget v15, v0, Lepson/print/screen/PrintSetting;->contrastValue:I

    .line 2193
    iget v3, v0, Lepson/print/screen/PrintSetting;->saturationValue:I

    .line 2194
    iget v4, v0, Lepson/print/screen/PrintSetting;->duplexValue:I

    .line 2195
    iget v0, v0, Lepson/print/screen/PrintSetting;->feedDirectionValue:I

    const/4 v6, 0x1

    const/16 v19, 0x57b

    const/16 v20, 0x57b

    .line 2204
    iget-object v5, v1, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v5}, Lepson/print/ActivityDocsPrintPreview;->access$800(Lepson/print/ActivityDocsPrintPreview;)Lepson/print/service/IEpsonService;

    move-result-object v5

    invoke-interface {v5}, Lepson/print/service/IEpsonService;->EpsonConnectEndJob()I

    .line 2207
    iget-object v5, v1, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    aget-object v7, p1, v2

    invoke-virtual {v5, v7}, Lepson/print/ActivityDocsPrintPreview;->checkJobNameLength(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 2213
    iget-object v5, v1, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v5}, Lepson/print/ActivityDocsPrintPreview;->access$800(Lepson/print/ActivityDocsPrintPreview;)Lepson/print/service/IEpsonService;

    move-result-object v5

    aget-object v7, p1, v2

    move/from16 v16, v3

    move/from16 v17, v0

    move/from16 v18, v4

    invoke-interface/range {v5 .. v20}, Lepson/print/service/IEpsonService;->EpsonConnectCreateJob(ILjava/lang/String;IIIIIIIIIIIII)I

    move-result v0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_8
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_6

    if-nez v0, :cond_a

    .line 2220
    :try_start_1
    iget-object v3, v1, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    const/4 v4, 0x1

    iput-boolean v4, v3, Lepson/print/ActivityDocsPrintPreview;->isCreateJobDone:Z

    .line 2223
    invoke-virtual/range {p0 .. p0}, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;->isCancelled()Z

    move-result v3

    if-nez v3, :cond_9

    .line 2228
    iget-object v3, v1, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v3}, Lepson/print/ActivityDocsPrintPreview;->access$800(Lepson/print/ActivityDocsPrintPreview;)Lepson/print/service/IEpsonService;

    move-result-object v3

    aget-object v5, p1, v2

    invoke-interface {v3, v5, v4}, Lepson/print/service/IEpsonService;->EpsonConnectUploadFile(Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_8

    const/4 v3, 0x2

    .line 2233
    new-array v5, v3, [I
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4

    move v7, v0

    const/4 v0, 0x0

    const/4 v6, 0x0

    :goto_0
    const-wide/16 v8, 0x1388

    .line 2239
    :try_start_2
    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V

    .line 2242
    invoke-virtual/range {p0 .. p0}, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;->isCancelled()Z

    move-result v8

    if-eq v8, v4, :cond_7

    .line 2247
    invoke-static {}, Lepson/print/ActivityDocsPrintPreview;->access$4400()Ljava/lang/Object;

    move-result-object v8

    monitor-enter v8
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 2249
    :try_start_3
    iget-object v9, v1, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v9}, Lepson/print/ActivityDocsPrintPreview;->access$800(Lepson/print/ActivityDocsPrintPreview;)Lepson/print/service/IEpsonService;

    move-result-object v9

    invoke-interface {v9, v5}, Lepson/print/service/IEpsonService;->EpsonConnectGetRenderingStatus([I)I

    move-result v7

    if-nez v7, :cond_6

    .line 2252
    aget v9, v5, v2

    if-eqz v9, :cond_5

    aget v9, v5, v2

    const/4 v10, 0x4

    if-eq v9, v10, :cond_5

    .line 2261
    aget v9, v5, v4

    if-ne v0, v9, :cond_0

    .line 2263
    monitor-exit v8

    goto :goto_0

    :cond_0
    if-nez v6, :cond_3

    if-lt v9, v4, :cond_3

    .line 2268
    iget-object v0, v1, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v0, v4}, Lepson/print/ActivityDocsPrintPreview;->access$602(Lepson/print/ActivityDocsPrintPreview;I)I

    .line 2270
    new-instance v0, Ljava/io/File;

    iget-object v10, v1, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v10}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v10

    invoke-virtual {v10}, Lepson/common/ExternalFileUtils;->getTempViewDir()Ljava/lang/String;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "preview_"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v12, v1, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    .line 2271
    invoke-static {v12}, Lepson/print/ActivityDocsPrintPreview;->access$600(Lepson/print/ActivityDocsPrintPreview;)I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v12, ".jpg"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v0, v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 2272
    iget-object v10, v1, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v10}, Lepson/print/ActivityDocsPrintPreview;->access$800(Lepson/print/ActivityDocsPrintPreview;)Lepson/print/service/IEpsonService;

    move-result-object v10

    iget-object v11, v1, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v11}, Lepson/print/ActivityDocsPrintPreview;->access$600(Lepson/print/ActivityDocsPrintPreview;)I

    move-result v11

    invoke-interface {v10, v11, v0}, Lepson/print/service/IEpsonService;->EpsonConnectGetPreview(ILjava/lang/String;)I

    move-result v7

    if-nez v7, :cond_2

    .line 2278
    iget-object v10, v1, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v10}, Lepson/print/ActivityDocsPrintPreview;->access$4300(Lepson/print/ActivityDocsPrintPreview;)Lepson/print/EPImageList;

    move-result-object v10

    invoke-virtual {v10, v0}, Lepson/print/EPImageList;->add(Ljava/lang/String;)Z

    .line 2281
    new-instance v10, Landroid/os/Message;

    invoke-direct {v10}, Landroid/os/Message;-><init>()V

    const/16 v11, 0x1f

    .line 2282
    iput v11, v10, Landroid/os/Message;->what:I

    .line 2283
    iput-object v0, v10, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 2284
    invoke-virtual {v10}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v11, "CONVERTED_PAGE"

    invoke-virtual {v0, v11, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2286
    iget-object v0, v1, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    iget-object v0, v0, Lepson/print/ActivityDocsPrintPreview;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {v0, v10}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 2289
    iget-object v0, v1, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;->serverConnectDialog:Lepson/print/ActivityDocsPrintPreview$CustomProDialog;

    if-eqz v0, :cond_1

    iget-object v0, v1, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;->serverConnectDialog:Lepson/print/ActivityDocsPrintPreview$CustomProDialog;

    invoke-virtual {v0}, Lepson/print/ActivityDocsPrintPreview$CustomProDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2290
    iget-object v0, v1, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;->serverConnectDialog:Lepson/print/ActivityDocsPrintPreview$CustomProDialog;

    invoke-virtual {v0}, Lepson/print/ActivityDocsPrintPreview$CustomProDialog;->dismiss()V

    :cond_1
    const/4 v6, 0x1

    goto :goto_1

    .line 2274
    :cond_2
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    throw v0

    .line 2298
    :cond_3
    :goto_1
    iget-object v0, v1, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v0, v9}, Lepson/print/ActivityDocsPrintPreview;->access$702(Lepson/print/ActivityDocsPrintPreview;I)I

    .line 2299
    new-array v0, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v0}, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;->publishProgress([Ljava/lang/Object;)V

    .line 2300
    monitor-exit v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2303
    :try_start_4
    aget v0, v5, v2
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    if-ne v0, v3, :cond_4

    goto/16 :goto_7

    :cond_4
    move v0, v9

    goto/16 :goto_0

    :cond_5
    const/16 v2, -0x420

    .line 2257
    :try_start_5
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 2251
    :cond_6
    :try_start_6
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :catchall_0
    move-exception v0

    move v2, v7

    .line 2300
    :goto_2
    :try_start_7
    monitor-exit v8
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    throw v0
    :try_end_8
    .catch Ljava/lang/InterruptedException; {:try_start_8 .. :try_end_8} :catch_3
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_8} :catch_2
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0

    :catch_0
    nop

    goto :goto_4

    :catchall_1
    move-exception v0

    goto :goto_2

    .line 2243
    :cond_7
    :try_start_9
    new-instance v0, Ljava/lang/InterruptedException;

    invoke-direct {v0}, Ljava/lang/InterruptedException;-><init>()V

    throw v0
    :try_end_9
    .catch Ljava/lang/InterruptedException; {:try_start_9 .. :try_end_9} :catch_3
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_9} :catch_2
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1

    :catch_1
    move v2, v7

    goto :goto_4

    :catch_2
    move-exception v0

    goto :goto_5

    :catch_3
    move-exception v0

    goto :goto_6

    .line 2230
    :cond_8
    :try_start_a
    new-instance v3, Ljava/lang/Exception;

    invoke-direct {v3}, Ljava/lang/Exception;-><init>()V

    throw v3

    .line 2224
    :cond_9
    new-instance v3, Ljava/lang/InterruptedException;

    invoke-direct {v3}, Ljava/lang/InterruptedException;-><init>()V

    throw v3

    :catch_4
    move v2, v0

    goto :goto_3

    .line 2218
    :cond_a
    new-instance v3, Ljava/lang/Exception;

    invoke-direct {v3}, Ljava/lang/Exception;-><init>()V

    throw v3
    :try_end_a
    .catch Ljava/lang/InterruptedException; {:try_start_a .. :try_end_a} :catch_8
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_a} :catch_7
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_4

    :cond_b
    const/4 v0, -0x1

    .line 2209
    :try_start_b
    new-instance v3, Ljava/lang/Exception;

    invoke-direct {v3}, Ljava/lang/Exception;-><init>()V

    throw v3
    :try_end_b
    .catch Ljava/lang/InterruptedException; {:try_start_b .. :try_end_b} :catch_8
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_b} :catch_7
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_5

    :catch_5
    const/4 v2, -0x1

    :catch_6
    :goto_3
    const/4 v6, 0x0

    .line 2314
    :goto_4
    invoke-virtual/range {p0 .. p0}, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_c

    .line 2315
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    const/16 v3, 0x27

    .line 2316
    iput v3, v0, Landroid/os/Message;->what:I

    .line 2317
    iput v2, v0, Landroid/os/Message;->arg1:I

    .line 2318
    iget-object v2, v1, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    iget-object v2, v2, Lepson/print/ActivityDocsPrintPreview;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_7

    :catch_7
    move-exception v0

    const/4 v6, 0x0

    .line 2311
    :goto_5
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_7

    :catch_8
    move-exception v0

    const/4 v6, 0x0

    .line 2309
    :goto_6
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 2323
    :cond_c
    :goto_7
    invoke-virtual/range {p0 .. p0}, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_d

    if-nez v6, :cond_e

    .line 2326
    :try_start_c
    iget-object v0, v1, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v0}, Lepson/print/ActivityDocsPrintPreview;->access$800(Lepson/print/ActivityDocsPrintPreview;)Lepson/print/service/IEpsonService;

    move-result-object v0

    invoke-interface {v0}, Lepson/print/service/IEpsonService;->EpsonConnectCancel()V
    :try_end_c
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_c} :catch_9

    goto :goto_8

    :catch_9
    move-exception v0

    .line 2328
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 2330
    :goto_8
    iget-object v0, v1, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    iget-object v0, v0, Lepson/print/ActivityDocsPrintPreview;->mUiHandler:Landroid/os/Handler;

    const/4 v2, 0x7

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_9

    .line 2336
    :cond_d
    iget-object v0, v1, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    iget-object v0, v0, Lepson/print/ActivityDocsPrintPreview;->mUiHandler:Landroid/os/Handler;

    const/16 v2, 0x20

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 2339
    :cond_e
    :goto_9
    iget-object v0, v1, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;->serverConnectDialog:Lepson/print/ActivityDocsPrintPreview$CustomProDialog;

    if-eqz v0, :cond_f

    invoke-virtual {v0}, Lepson/print/ActivityDocsPrintPreview$CustomProDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 2340
    iget-object v0, v1, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;->serverConnectDialog:Lepson/print/ActivityDocsPrintPreview$CustomProDialog;

    invoke-virtual {v0}, Lepson/print/ActivityDocsPrintPreview$CustomProDialog;->dismiss()V

    :cond_f
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onPreExecute()V
    .locals 7

    .line 2147
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v0}, Lepson/print/ActivityDocsPrintPreview;->access$4300(Lepson/print/ActivityDocsPrintPreview;)Lepson/print/EPImageList;

    move-result-object v0

    invoke-virtual {v0}, Lepson/print/EPImageList;->clear()V

    .line 2148
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-virtual {v0}, Lepson/print/ActivityDocsPrintPreview;->updatePageTextView()V

    .line 2149
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v0}, Lepson/print/ActivityDocsPrintPreview;->access$2800(Lepson/print/ActivityDocsPrintPreview;)V

    .line 2151
    new-instance v0, Lepson/print/ActivityDocsPrintPreview$CustomProDialog;

    iget-object v3, p0, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    const v4, 0x7f0f000a

    const v5, 0x7f0a00cc

    const/4 v6, 0x1

    move-object v1, v0

    move-object v2, v3

    invoke-direct/range {v1 .. v6}, Lepson/print/ActivityDocsPrintPreview$CustomProDialog;-><init>(Lepson/print/ActivityDocsPrintPreview;Landroid/content/Context;IIZ)V

    iput-object v0, p0, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;->serverConnectDialog:Lepson/print/ActivityDocsPrintPreview$CustomProDialog;

    .line 2153
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;->serverConnectDialog:Lepson/print/ActivityDocsPrintPreview$CustomProDialog;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    .line 2154
    invoke-virtual {v0, v1}, Lepson/print/ActivityDocsPrintPreview$CustomProDialog;->setCancelable(Z)V

    .line 2159
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;->serverConnectDialog:Lepson/print/ActivityDocsPrintPreview$CustomProDialog;

    new-instance v1, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask$1;

    invoke-direct {v1, p0}, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask$1;-><init>(Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;)V

    invoke-virtual {v0, v1}, Lepson/print/ActivityDocsPrintPreview$CustomProDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 2171
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;->serverConnectDialog:Lepson/print/ActivityDocsPrintPreview$CustomProDialog;

    invoke-virtual {v0}, Lepson/print/ActivityDocsPrintPreview$CustomProDialog;->show()V

    :cond_0
    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .line 2142
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;->onProgressUpdate([Ljava/lang/Void;)V

    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Void;)V
    .locals 0

    .line 2348
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-virtual {p1}, Lepson/print/ActivityDocsPrintPreview;->updatePrevNextButtonView()V

    .line 2349
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$LoadRemotePreviewTask;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-virtual {p1}, Lepson/print/ActivityDocsPrintPreview;->updatePageTextView()V

    return-void
.end method

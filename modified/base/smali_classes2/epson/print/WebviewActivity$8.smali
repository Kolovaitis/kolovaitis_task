.class Lepson/print/WebviewActivity$8;
.super Landroid/os/AsyncTask;
.source "WebviewActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/WebviewActivity;->previewImage()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field imagePath:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lepson/print/WebviewActivity;


# direct methods
.method constructor <init>(Lepson/print/WebviewActivity;)V
    .locals 0

    .line 1253
    iput-object p1, p0, Lepson/print/WebviewActivity$8;->this$0:Lepson/print/WebviewActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 1255
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lepson/print/WebviewActivity$8;->imagePath:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1253
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/print/WebviewActivity$8;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 5

    .line 1282
    iget-object p1, p0, Lepson/print/WebviewActivity$8;->this$0:Lepson/print/WebviewActivity;

    invoke-static {p1}, Lepson/print/WebviewActivity;->access$1000(Lepson/print/WebviewActivity;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    const-string v0, "file"

    .line 1284
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 1287
    iget-object v0, p0, Lepson/print/WebviewActivity$8;->imagePath:Ljava/util/ArrayList;

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1292
    :cond_0
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object p1

    .line 1294
    iget-object v0, p0, Lepson/print/WebviewActivity$8;->this$0:Lepson/print/WebviewActivity;

    invoke-static {v0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v0

    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->getDownloadDir()Ljava/lang/String;

    move-result-object v0

    .line 1295
    iget-object v2, p0, Lepson/print/WebviewActivity$8;->this$0:Lepson/print/WebviewActivity;

    invoke-static {v2}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v2

    invoke-virtual {v2}, Lepson/common/ExternalFileUtils;->initDownloadDir()V

    .line 1296
    invoke-virtual {p0}, Lepson/print/WebviewActivity$8;->isCancelled()Z

    move-result v2

    if-eqz v2, :cond_1

    return-object v1

    .line 1299
    :cond_1
    invoke-static {}, Lepson/print/Util/Utils;->getInstance()Lepson/print/Util/Utils;

    .line 1300
    sget-object v2, Lepson/print/Util/Utils;->mUtils:Lepson/print/Util/Utils;

    iget-object v3, p0, Lepson/print/WebviewActivity$8;->this$0:Lepson/print/WebviewActivity;

    invoke-static {v3}, Lepson/print/WebviewActivity;->access$1000(Lepson/print/WebviewActivity;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lepson/print/WebviewActivity$8;->this$0:Lepson/print/WebviewActivity;

    invoke-virtual {v2, v3, v0, p1, v4}, Lepson/print/Util/Utils;->downloadFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Z

    .line 1301
    iget-object v2, p0, Lepson/print/WebviewActivity$8;->imagePath:Ljava/util/ArrayList;

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    return-object v1
.end method

.method protected onCancelled()V
    .locals 2

    const-string v0, "WebviewActivity"

    const-string v1, "Cancel: OK"

    .line 1333
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1335
    iget-object v0, p0, Lepson/print/WebviewActivity$8;->this$0:Lepson/print/WebviewActivity;

    invoke-static {v0}, Lepson/print/WebviewActivity;->access$2600(Lepson/print/WebviewActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1336
    iget-object v0, p0, Lepson/print/WebviewActivity$8;->this$0:Lepson/print/WebviewActivity;

    invoke-virtual {v0}, Lepson/print/WebviewActivity;->finish()V

    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 1253
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/print/WebviewActivity$8;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 2

    const-string p1, "WebviewActivity"

    .line 1315
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "previewImage::onPostExecute() Share image: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lepson/print/WebviewActivity$8;->imagePath:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1316
    iget-object p1, p0, Lepson/print/WebviewActivity$8;->this$0:Lepson/print/WebviewActivity;

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lepson/print/ActivityViewImageSelect;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {p1, v0}, Lepson/print/WebviewActivity;->access$2702(Lepson/print/WebviewActivity;Landroid/content/Intent;)Landroid/content/Intent;

    .line 1317
    iget-object p1, p0, Lepson/print/WebviewActivity$8;->this$0:Lepson/print/WebviewActivity;

    invoke-static {p1}, Lepson/print/WebviewActivity;->access$2700(Lepson/print/WebviewActivity;)Landroid/content/Intent;

    move-result-object p1

    const-string v0, "listAlbum"

    iget-object v1, p0, Lepson/print/WebviewActivity$8;->imagePath:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1318
    iget-object p1, p0, Lepson/print/WebviewActivity$8;->this$0:Lepson/print/WebviewActivity;

    invoke-static {p1}, Lepson/print/WebviewActivity;->access$2700(Lepson/print/WebviewActivity;)Landroid/content/Intent;

    move-result-object p1

    const-string v0, "typeprint"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1319
    iget-object p1, p0, Lepson/print/WebviewActivity$8;->this$0:Lepson/print/WebviewActivity;

    invoke-static {p1}, Lepson/print/WebviewActivity;->access$2700(Lepson/print/WebviewActivity;)Landroid/content/Intent;

    move-result-object p1

    const-string v0, "print_log"

    iget-object v1, p0, Lepson/print/WebviewActivity$8;->this$0:Lepson/print/WebviewActivity;

    .line 1320
    invoke-static {v1}, Lepson/print/WebviewActivity;->access$2800(Lepson/print/WebviewActivity;)Lcom/epson/iprint/prtlogger/PrintLog;

    move-result-object v1

    .line 1319
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1321
    iget-object p1, p0, Lepson/print/WebviewActivity$8;->this$0:Lepson/print/WebviewActivity;

    invoke-static {p1}, Lepson/print/WebviewActivity;->access$2700(Lepson/print/WebviewActivity;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0}, Lepson/print/WebviewActivity;->startActivity(Landroid/content/Intent;)V

    .line 1322
    iget-object p1, p0, Lepson/print/WebviewActivity$8;->this$0:Lepson/print/WebviewActivity;

    invoke-virtual {p1}, Lepson/print/WebviewActivity;->finish()V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .line 1265
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 1268
    iget-object v0, p0, Lepson/print/WebviewActivity$8;->this$0:Lepson/print/WebviewActivity;

    invoke-virtual {v0}, Lepson/print/WebviewActivity;->showLoadingDialog()V

    return-void
.end method

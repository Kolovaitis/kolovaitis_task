.class Lepson/print/ActivityViewImageSelect$3;
.super Ljava/lang/Object;
.source "ActivityViewImageSelect.java"

# interfaces
.implements Landroid/arch/lifecycle/Observer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/ActivityViewImageSelect;->observeImageList(Lepson/print/phlayout/PhotoPreviewImageList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/arch/lifecycle/Observer<",
        "Lepson/print/PhotoImageConvertViewModel$Result;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/ActivityViewImageSelect;


# direct methods
.method constructor <init>(Lepson/print/ActivityViewImageSelect;)V
    .locals 0

    .line 511
    iput-object p1, p0, Lepson/print/ActivityViewImageSelect$3;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged(Lepson/print/PhotoImageConvertViewModel$Result;)V
    .locals 1
    .param p1    # Lepson/print/PhotoImageConvertViewModel$Result;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .line 514
    iget-object v0, p1, Lepson/print/PhotoImageConvertViewModel$Result;->epImageWrappers:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 515
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$3;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {p1}, Lepson/print/ActivityViewImageSelect;->access$400(Lepson/print/ActivityViewImageSelect;)V

    return-void

    .line 519
    :cond_0
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect$3;->this$0:Lepson/print/ActivityViewImageSelect;

    iget-object p1, p1, Lepson/print/PhotoImageConvertViewModel$Result;->epImageWrappers:Ljava/util/ArrayList;

    invoke-static {v0, p1}, Lepson/print/ActivityViewImageSelect;->access$500(Lepson/print/ActivityViewImageSelect;Ljava/util/ArrayList;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 520
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$3;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {p1}, Lepson/print/ActivityViewImageSelect;->access$400(Lepson/print/ActivityViewImageSelect;)V

    return-void

    .line 523
    :cond_1
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$3;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {p1}, Lepson/print/ActivityViewImageSelect;->access$600(Lepson/print/ActivityViewImageSelect;)V

    .line 525
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$3;->this$0:Lepson/print/ActivityViewImageSelect;

    const/4 v0, 0x1

    invoke-static {p1, v0}, Lepson/print/ActivityViewImageSelect;->access$702(Lepson/print/ActivityViewImageSelect;Z)Z

    .line 526
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$3;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {p1}, Lepson/print/ActivityViewImageSelect;->access$800(Lepson/print/ActivityViewImageSelect;)V

    return-void
.end method

.method public bridge synthetic onChanged(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .line 511
    check-cast p1, Lepson/print/PhotoImageConvertViewModel$Result;

    invoke-virtual {p0, p1}, Lepson/print/ActivityViewImageSelect$3;->onChanged(Lepson/print/PhotoImageConvertViewModel$Result;)V

    return-void
.end method

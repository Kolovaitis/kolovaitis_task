.class Lepson/print/WebviewActivity$ImageFileCreator;
.super Ljava/lang/Object;
.source "WebviewActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/WebviewActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ImageFileCreator"
.end annotation


# static fields
.field static final A4_360DPI_HEIGHT:I = 0x1071

.field static final A4_360DPI_WIDTH:I = 0xba0

.field static final A4_ASPECT_RATIO:D = 1.4143145161290323


# instance fields
.field final RATE_FROME_360DPI:[I

.field mBitmap:Landroid/graphics/Bitmap;

.field mExRate:D

.field mPictureSize:Landroid/graphics/Rect;

.field final synthetic this$0:Lepson/print/WebviewActivity;


# direct methods
.method constructor <init>(Lepson/print/WebviewActivity;)V
    .locals 0

    .line 889
    iput-object p1, p0, Lepson/print/WebviewActivity$ImageFileCreator;->this$0:Lepson/print/WebviewActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x3

    .line 896
    new-array p1, p1, [I

    fill-array-data p1, :array_0

    iput-object p1, p0, Lepson/print/WebviewActivity$ImageFileCreator;->RATE_FROME_360DPI:[I

    return-void

    :array_0
    .array-data 4
        0x2
        0x3
        0x4
    .end array-data
.end method

.method private getExRateCand(I)[D
    .locals 9

    .line 914
    iget-object v0, p0, Lepson/print/WebviewActivity$ImageFileCreator;->RATE_FROME_360DPI:[I

    array-length v0, v0

    new-array v0, v0, [D

    const/4 v1, 0x0

    .line 916
    :goto_0
    iget-object v2, p0, Lepson/print/WebviewActivity$ImageFileCreator;->RATE_FROME_360DPI:[I

    array-length v3, v2

    if-ge v1, v3, :cond_0

    const-wide v3, 0x40a7400000000000L    # 2976.0

    int-to-double v5, p1

    .line 917
    aget v2, v2, v1

    int-to-double v7, v2

    mul-double v5, v5, v7

    div-double/2addr v3, v5

    aput-wide v3, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method


# virtual methods
.method public createImageFile(Landroid/webkit/WebView;Ljava/util/ArrayList;Lepson/print/WebviewActivity$InteruptChecker;)Z
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/webkit/WebView;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;",
            "Lepson/print/WebviewActivity$InteruptChecker;",
            ")Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 972
    iget-wide v0, p0, Lepson/print/WebviewActivity$ImageFileCreator;->mExRate:D

    .line 974
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    .line 976
    new-instance v3, Landroid/graphics/Canvas;

    iget-object v4, p0, Lepson/print/WebviewActivity$ImageFileCreator;->mBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v3, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    double-to-float v0, v0

    .line 978
    invoke-virtual {v3, v0, v0}, Landroid/graphics/Canvas;->scale(FF)V

    .line 983
    iget-object v0, p0, Lepson/print/WebviewActivity$ImageFileCreator;->mPictureSize:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-double v0, v0

    const-wide v4, 0x3ff6a10842108421L    # 1.4143145161290323

    mul-double v0, v0, v4

    double-to-int v0, v0

    .line 985
    iget-object v1, p0, Lepson/print/WebviewActivity$ImageFileCreator;->mPictureSize:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    const/4 v4, 0x1

    const/4 v5, 0x1

    :goto_0
    if-lez v1, :cond_1

    .line 993
    invoke-interface {p3}, Lepson/print/WebviewActivity$InteruptChecker;->check()Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 p1, 0x0

    return p1

    .line 996
    :cond_0
    new-instance v6, Ljava/io/File;

    iget-object v7, p0, Lepson/print/WebviewActivity$ImageFileCreator;->this$0:Lepson/print/WebviewActivity;

    invoke-static {v7}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v7

    invoke-virtual {v7}, Lepson/common/ExternalFileUtils;->getTempViewDir()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "multi_cut_"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v9, ".jpg"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 997
    invoke-virtual {v6}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    const/16 v7, 0xff

    .line 999
    invoke-virtual {v3, v7, v7, v7}, Landroid/graphics/Canvas;->drawRGB(III)V

    .line 1003
    monitor-enter v2

    .line 1004
    :try_start_0
    iget-object v7, p0, Lepson/print/WebviewActivity$ImageFileCreator;->this$0:Lepson/print/WebviewActivity;

    new-instance v8, Lepson/print/WebviewActivity$ImageFileCreator$1;

    invoke-direct {v8, p0, v2, p1, v3}, Lepson/print/WebviewActivity$ImageFileCreator$1;-><init>(Lepson/print/WebviewActivity$ImageFileCreator;Ljava/lang/Object;Landroid/webkit/WebView;Landroid/graphics/Canvas;)V

    invoke-virtual {v7, v8}, Lepson/print/WebviewActivity;->runOnUiThread(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    const-string v7, "WebviewActivity"

    .line 1015
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "webView.draw() wait page = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lepson/print/Util/EPLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1016
    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v7

    .line 1018
    :try_start_2
    invoke-virtual {v7}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 1020
    :goto_1
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1022
    iget-object v7, p0, Lepson/print/WebviewActivity$ImageFileCreator;->mBitmap:Landroid/graphics/Bitmap;

    invoke-static {v7, v6}, Lepson/print/WebviewActivity;->access$2300(Landroid/graphics/Bitmap;Ljava/lang/String;)V

    .line 1024
    invoke-virtual {p2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sub-int/2addr v1, v0

    const/4 v6, 0x0

    int-to-float v7, v0

    neg-float v7, v7

    .line 1026
    invoke-virtual {v3, v6, v7}, Landroid/graphics/Canvas;->translate(FF)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :catchall_0
    move-exception p1

    .line 1020
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw p1

    :cond_1
    return v4
.end method

.method public freeMemory()V
    .locals 1

    .line 1036
    iget-object v0, p0, Lepson/print/WebviewActivity$ImageFileCreator;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 1037
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    const/4 v0, 0x0

    .line 1038
    iput-object v0, p0, Lepson/print/WebviewActivity$ImageFileCreator;->mBitmap:Landroid/graphics/Bitmap;

    :cond_0
    return-void
.end method

.method public prepareBitmap()Z
    .locals 13

    .line 931
    iget-object v0, p0, Lepson/print/WebviewActivity$ImageFileCreator;->mPictureSize:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    .line 935
    invoke-direct {p0, v0}, Lepson/print/WebviewActivity$ImageFileCreator;->getExRateCand(I)[D

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const/16 v6, 0x5a

    move-wide v7, v4

    move-object v4, v3

    const/4 v3, 0x0

    .line 939
    :goto_0
    array-length v5, v1

    if-ge v3, v5, :cond_0

    if-nez v4, :cond_0

    .line 940
    aget-wide v7, v1, v3

    int-to-double v9, v0

    mul-double v9, v9, v7

    double-to-int v5, v9

    int-to-double v9, v5

    const-wide v11, 0x3ff6a10842108421L    # 1.4143145161290323

    mul-double v9, v9, v11

    double-to-int v9, v9

    .line 944
    :try_start_0
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 945
    sget-object v10, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v9, v10}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    const/16 v5, 0x168

    .line 947
    iget-object v9, p0, Lepson/print/WebviewActivity$ImageFileCreator;->RATE_FROME_360DPI:[I

    aget v9, v9, v3

    div-int/2addr v5, v9
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move v6, v5

    :catch_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    if-nez v4, :cond_1

    return v2

    .line 957
    :cond_1
    invoke-virtual {v4, v6}, Landroid/graphics/Bitmap;->setDensity(I)V

    .line 958
    iput-object v4, p0, Lepson/print/WebviewActivity$ImageFileCreator;->mBitmap:Landroid/graphics/Bitmap;

    .line 959
    iput-wide v7, p0, Lepson/print/WebviewActivity$ImageFileCreator;->mExRate:D

    const-string v0, "WebviewActivity"

    .line 960
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "expand => "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v7, v8}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    return v0
.end method

.method public setPicture(Landroid/graphics/Rect;)V
    .locals 0

    .line 907
    iput-object p1, p0, Lepson/print/WebviewActivity$ImageFileCreator;->mPictureSize:Landroid/graphics/Rect;

    return-void
.end method

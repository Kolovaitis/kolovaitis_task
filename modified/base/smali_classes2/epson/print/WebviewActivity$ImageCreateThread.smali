.class Lepson/print/WebviewActivity$ImageCreateThread;
.super Ljava/lang/Thread;
.source "WebviewActivity.java"

# interfaces
.implements Lepson/print/WebviewActivity$InteruptChecker;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/WebviewActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ImageCreateThread"
.end annotation


# instance fields
.field private mImageFileCreator:Lepson/print/WebviewActivity$ImageFileCreator;

.field private mPicture:Landroid/graphics/Rect;

.field final synthetic this$0:Lepson/print/WebviewActivity;


# direct methods
.method constructor <init>(Lepson/print/WebviewActivity;)V
    .locals 0

    .line 1138
    iput-object p1, p0, Lepson/print/WebviewActivity$ImageCreateThread;->this$0:Lepson/print/WebviewActivity;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method private freeLargeMem()V
    .locals 2

    .line 1202
    iget-object v0, p0, Lepson/print/WebviewActivity$ImageCreateThread;->mImageFileCreator:Lepson/print/WebviewActivity$ImageFileCreator;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 1203
    invoke-virtual {v0}, Lepson/print/WebviewActivity$ImageFileCreator;->freeMemory()V

    .line 1204
    iput-object v1, p0, Lepson/print/WebviewActivity$ImageCreateThread;->mImageFileCreator:Lepson/print/WebviewActivity$ImageFileCreator;

    .line 1206
    :cond_0
    iput-object v1, p0, Lepson/print/WebviewActivity$ImageCreateThread;->mPicture:Landroid/graphics/Rect;

    return-void
.end method


# virtual methods
.method public check()Z
    .locals 1

    .line 1214
    invoke-virtual {p0}, Lepson/print/WebviewActivity$ImageCreateThread;->isInterrupted()Z

    move-result v0

    return v0
.end method

.method public run()V
    .locals 3

    .line 1150
    iget-object v0, p0, Lepson/print/WebviewActivity$ImageCreateThread;->this$0:Lepson/print/WebviewActivity;

    invoke-static {v0}, Lepson/print/WebviewActivity;->access$1900(Lepson/print/WebviewActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 1155
    :cond_0
    iget-object v0, p0, Lepson/print/WebviewActivity$ImageCreateThread;->this$0:Lepson/print/WebviewActivity;

    invoke-static {v0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v0

    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->initTempViewDir()V

    .line 1157
    new-instance v0, Lepson/print/WebviewActivity$ImageFileCreator;

    iget-object v1, p0, Lepson/print/WebviewActivity$ImageCreateThread;->this$0:Lepson/print/WebviewActivity;

    invoke-direct {v0, v1}, Lepson/print/WebviewActivity$ImageFileCreator;-><init>(Lepson/print/WebviewActivity;)V

    iput-object v0, p0, Lepson/print/WebviewActivity$ImageCreateThread;->mImageFileCreator:Lepson/print/WebviewActivity$ImageFileCreator;

    .line 1158
    iget-object v0, p0, Lepson/print/WebviewActivity$ImageCreateThread;->mImageFileCreator:Lepson/print/WebviewActivity$ImageFileCreator;

    iget-object v1, p0, Lepson/print/WebviewActivity$ImageCreateThread;->mPicture:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lepson/print/WebviewActivity$ImageFileCreator;->setPicture(Landroid/graphics/Rect;)V

    .line 1160
    iget-object v0, p0, Lepson/print/WebviewActivity$ImageCreateThread;->mImageFileCreator:Lepson/print/WebviewActivity$ImageFileCreator;

    invoke-virtual {v0}, Lepson/print/WebviewActivity$ImageFileCreator;->prepareBitmap()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1161
    iget-object v0, p0, Lepson/print/WebviewActivity$ImageCreateThread;->this$0:Lepson/print/WebviewActivity;

    invoke-static {v0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v0

    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->initTempViewDir()V

    .line 1162
    iget-object v0, p0, Lepson/print/WebviewActivity$ImageCreateThread;->this$0:Lepson/print/WebviewActivity;

    invoke-virtual {v0}, Lepson/print/WebviewActivity;->cancelShowingDialog()V

    .line 1164
    iget-object v0, p0, Lepson/print/WebviewActivity$ImageCreateThread;->this$0:Lepson/print/WebviewActivity;

    iget-object v0, v0, Lepson/print/WebviewActivity;->myHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1165
    iget-object v0, p0, Lepson/print/WebviewActivity$ImageCreateThread;->this$0:Lepson/print/WebviewActivity;

    invoke-virtual {v0}, Lepson/print/WebviewActivity;->finish()V

    return-void

    .line 1168
    :cond_1
    invoke-virtual {p0}, Lepson/print/WebviewActivity$ImageCreateThread;->isInterrupted()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1169
    invoke-direct {p0}, Lepson/print/WebviewActivity$ImageCreateThread;->freeLargeMem()V

    .line 1170
    iget-object v0, p0, Lepson/print/WebviewActivity$ImageCreateThread;->this$0:Lepson/print/WebviewActivity;

    invoke-static {v0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v0

    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->initTempViewDir()V

    .line 1171
    iget-object v0, p0, Lepson/print/WebviewActivity$ImageCreateThread;->this$0:Lepson/print/WebviewActivity;

    invoke-virtual {v0}, Lepson/print/WebviewActivity;->cancelShowingDialog()V

    return-void

    .line 1178
    :cond_2
    :try_start_0
    iget-object v0, p0, Lepson/print/WebviewActivity$ImageCreateThread;->mImageFileCreator:Lepson/print/WebviewActivity$ImageFileCreator;

    iget-object v1, p0, Lepson/print/WebviewActivity$ImageCreateThread;->this$0:Lepson/print/WebviewActivity;

    invoke-static {v1}, Lepson/print/WebviewActivity;->access$300(Lepson/print/WebviewActivity;)Landroid/webkit/WebView;

    move-result-object v1

    iget-object v2, p0, Lepson/print/WebviewActivity$ImageCreateThread;->this$0:Lepson/print/WebviewActivity;

    invoke-static {v2}, Lepson/print/WebviewActivity;->access$2400(Lepson/print/WebviewActivity;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p0}, Lepson/print/WebviewActivity$ImageFileCreator;->createImageFile(Landroid/webkit/WebView;Ljava/util/ArrayList;Lepson/print/WebviewActivity$InteruptChecker;)Z

    move-result v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1188
    invoke-direct {p0}, Lepson/print/WebviewActivity$ImageCreateThread;->freeLargeMem()V

    if-eqz v0, :cond_4

    .line 1191
    invoke-virtual {p0}, Lepson/print/WebviewActivity$ImageCreateThread;->isInterrupted()Z

    move-result v0

    if-eqz v0, :cond_3

    goto :goto_0

    .line 1197
    :cond_3
    iget-object v0, p0, Lepson/print/WebviewActivity$ImageCreateThread;->this$0:Lepson/print/WebviewActivity;

    invoke-static {v0}, Lepson/print/WebviewActivity;->access$2500(Lepson/print/WebviewActivity;)V

    return-void

    .line 1193
    :cond_4
    :goto_0
    iget-object v0, p0, Lepson/print/WebviewActivity$ImageCreateThread;->this$0:Lepson/print/WebviewActivity;

    invoke-static {v0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v0

    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->initTempViewDir()V

    .line 1194
    iget-object v0, p0, Lepson/print/WebviewActivity$ImageCreateThread;->this$0:Lepson/print/WebviewActivity;

    invoke-virtual {v0}, Lepson/print/WebviewActivity;->cancelShowingDialog()V

    return-void

    :catchall_0
    move-exception v0

    goto :goto_1

    :catch_0
    move-exception v0

    .line 1180
    :try_start_1
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 1181
    iget-object v0, p0, Lepson/print/WebviewActivity$ImageCreateThread;->this$0:Lepson/print/WebviewActivity;

    invoke-static {v0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v0

    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->initTempViewDir()V

    .line 1182
    iget-object v0, p0, Lepson/print/WebviewActivity$ImageCreateThread;->this$0:Lepson/print/WebviewActivity;

    invoke-virtual {v0}, Lepson/print/WebviewActivity;->cancelShowingDialog()V

    .line 1184
    iget-object v0, p0, Lepson/print/WebviewActivity$ImageCreateThread;->this$0:Lepson/print/WebviewActivity;

    iget-object v0, v0, Lepson/print/WebviewActivity;->myHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1188
    invoke-direct {p0}, Lepson/print/WebviewActivity$ImageCreateThread;->freeLargeMem()V

    return-void

    :goto_1
    invoke-direct {p0}, Lepson/print/WebviewActivity$ImageCreateThread;->freeLargeMem()V

    throw v0
.end method

.method public setPicture(Landroid/graphics/Rect;)Lepson/print/WebviewActivity$ImageCreateThread;
    .locals 0

    .line 1144
    iput-object p1, p0, Lepson/print/WebviewActivity$ImageCreateThread;->mPicture:Landroid/graphics/Rect;

    return-object p0
.end method

.class final enum Lepson/print/ActivityNfcPrinter$NfcSimpleApRetryStatus;
.super Ljava/lang/Enum;
.source "ActivityNfcPrinter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/ActivityNfcPrinter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "NfcSimpleApRetryStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lepson/print/ActivityNfcPrinter$NfcSimpleApRetryStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lepson/print/ActivityNfcPrinter$NfcSimpleApRetryStatus;

.field public static final enum FIRST_CONNECTION:Lepson/print/ActivityNfcPrinter$NfcSimpleApRetryStatus;

.field public static final enum RETRY_CONNECTION:Lepson/print/ActivityNfcPrinter$NfcSimpleApRetryStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 100
    new-instance v0, Lepson/print/ActivityNfcPrinter$NfcSimpleApRetryStatus;

    const-string v1, "FIRST_CONNECTION"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lepson/print/ActivityNfcPrinter$NfcSimpleApRetryStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/ActivityNfcPrinter$NfcSimpleApRetryStatus;->FIRST_CONNECTION:Lepson/print/ActivityNfcPrinter$NfcSimpleApRetryStatus;

    .line 101
    new-instance v0, Lepson/print/ActivityNfcPrinter$NfcSimpleApRetryStatus;

    const-string v1, "RETRY_CONNECTION"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lepson/print/ActivityNfcPrinter$NfcSimpleApRetryStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/ActivityNfcPrinter$NfcSimpleApRetryStatus;->RETRY_CONNECTION:Lepson/print/ActivityNfcPrinter$NfcSimpleApRetryStatus;

    const/4 v0, 0x2

    .line 99
    new-array v0, v0, [Lepson/print/ActivityNfcPrinter$NfcSimpleApRetryStatus;

    sget-object v1, Lepson/print/ActivityNfcPrinter$NfcSimpleApRetryStatus;->FIRST_CONNECTION:Lepson/print/ActivityNfcPrinter$NfcSimpleApRetryStatus;

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/ActivityNfcPrinter$NfcSimpleApRetryStatus;->RETRY_CONNECTION:Lepson/print/ActivityNfcPrinter$NfcSimpleApRetryStatus;

    aput-object v1, v0, v3

    sput-object v0, Lepson/print/ActivityNfcPrinter$NfcSimpleApRetryStatus;->$VALUES:[Lepson/print/ActivityNfcPrinter$NfcSimpleApRetryStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 99
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lepson/print/ActivityNfcPrinter$NfcSimpleApRetryStatus;
    .locals 1

    .line 99
    const-class v0, Lepson/print/ActivityNfcPrinter$NfcSimpleApRetryStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lepson/print/ActivityNfcPrinter$NfcSimpleApRetryStatus;

    return-object p0
.end method

.method public static values()[Lepson/print/ActivityNfcPrinter$NfcSimpleApRetryStatus;
    .locals 1

    .line 99
    sget-object v0, Lepson/print/ActivityNfcPrinter$NfcSimpleApRetryStatus;->$VALUES:[Lepson/print/ActivityNfcPrinter$NfcSimpleApRetryStatus;

    invoke-virtual {v0}, [Lepson/print/ActivityNfcPrinter$NfcSimpleApRetryStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lepson/print/ActivityNfcPrinter$NfcSimpleApRetryStatus;

    return-object v0
.end method

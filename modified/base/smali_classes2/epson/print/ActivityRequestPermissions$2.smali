.class Lepson/print/ActivityRequestPermissions$2;
.super Ljava/lang/Object;
.source "ActivityRequestPermissions.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/ActivityRequestPermissions;->PermisionAlertDialog(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/ActivityRequestPermissions;

.field final synthetic val$isShowSetting:Z

.field final synthetic val$permission:Ljava/lang/String;


# direct methods
.method constructor <init>(Lepson/print/ActivityRequestPermissions;ZLjava/lang/String;)V
    .locals 0

    .line 230
    iput-object p1, p0, Lepson/print/ActivityRequestPermissions$2;->this$0:Lepson/print/ActivityRequestPermissions;

    iput-boolean p2, p0, Lepson/print/ActivityRequestPermissions$2;->val$isShowSetting:Z

    iput-object p3, p0, Lepson/print/ActivityRequestPermissions$2;->val$permission:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x17
    .end annotation

    .line 234
    iget-boolean p1, p0, Lepson/print/ActivityRequestPermissions$2;->val$isShowSetting:Z

    if-eqz p1, :cond_0

    .line 235
    iget-object p1, p0, Lepson/print/ActivityRequestPermissions$2;->this$0:Lepson/print/ActivityRequestPermissions;

    invoke-virtual {p1}, Lepson/print/ActivityRequestPermissions;->getPackageName()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lepson/print/ActivityRequestPermissions;->access$000(Lepson/print/ActivityRequestPermissions;Ljava/lang/String;)V

    goto :goto_0

    .line 237
    :cond_0
    iget-object p1, p0, Lepson/print/ActivityRequestPermissions$2;->this$0:Lepson/print/ActivityRequestPermissions;

    const/4 p2, 0x1

    new-array v0, p2, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lepson/print/ActivityRequestPermissions$2;->val$permission:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-virtual {p1, v0, p2}, Lepson/print/ActivityRequestPermissions;->requestPermissions([Ljava/lang/String;I)V

    :goto_0
    return-void
.end method

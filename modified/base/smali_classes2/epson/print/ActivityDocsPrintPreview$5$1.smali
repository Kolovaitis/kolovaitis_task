.class Lepson/print/ActivityDocsPrintPreview$5$1;
.super Lepson/print/Util/AsyncTaskExecutor;
.source "ActivityDocsPrintPreview.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/ActivityDocsPrintPreview$5;->handleMessage(Landroid/os/Message;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lepson/print/Util/AsyncTaskExecutor<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lepson/print/ActivityDocsPrintPreview$5;


# direct methods
.method constructor <init>(Lepson/print/ActivityDocsPrintPreview$5;)V
    .locals 0

    .line 888
    iput-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5$1;->this$1:Lepson/print/ActivityDocsPrintPreview$5;

    invoke-direct {p0}, Lepson/print/Util/AsyncTaskExecutor;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 2

    .line 896
    new-instance p1, Lepson/server/utils/MyUtility;

    invoke-direct {p1}, Lepson/server/utils/MyUtility;-><init>()V

    .line 897
    invoke-static {}, Lepson/print/Util/Utils;->getInstance()Lepson/print/Util/Utils;

    move-result-object v0

    iget-object v1, p0, Lepson/print/ActivityDocsPrintPreview$5$1;->this$1:Lepson/print/ActivityDocsPrintPreview$5;

    iget-object v1, v1, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-virtual {v0, v1}, Lepson/print/Util/Utils;->isNetworkAvailable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$5$1;->this$1:Lepson/print/ActivityDocsPrintPreview$5;

    iget-object v0, v0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-virtual {p1, v0}, Lepson/server/utils/MyUtility;->doNet(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    const-string v0, ""

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    .line 900
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    .line 898
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 888
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/print/ActivityDocsPrintPreview$5$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 1

    .line 906
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$5$1;->this$1:Lepson/print/ActivityDocsPrintPreview$5;

    iget-object v0, v0, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-virtual {v0}, Lepson/print/ActivityDocsPrintPreview;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 911
    :cond_0
    invoke-super {p0, p1}, Lepson/print/Util/AsyncTaskExecutor;->onPostExecute(Ljava/lang/Object;)V

    .line 913
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-nez p1, :cond_1

    .line 915
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5$1;->this$1:Lepson/print/ActivityDocsPrintPreview$5;

    iget-object p1, p1, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    const/4 v0, 0x6

    invoke-virtual {p1, v0}, Lepson/print/ActivityDocsPrintPreview;->showDialog(I)V

    goto :goto_0

    .line 917
    :cond_1
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5$1;->this$1:Lepson/print/ActivityDocsPrintPreview$5;

    iget-object p1, p1, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$1800(Lepson/print/ActivityDocsPrintPreview;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 918
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5$1;->this$1:Lepson/print/ActivityDocsPrintPreview$5;

    iget-object p1, p1, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lepson/print/ActivityDocsPrintPreview;->showDialog(I)V

    goto :goto_0

    .line 920
    :cond_2
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$5$1;->this$1:Lepson/print/ActivityDocsPrintPreview$5;

    iget-object p1, p1, Lepson/print/ActivityDocsPrintPreview$5;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$1900(Lepson/print/ActivityDocsPrintPreview;)V

    :goto_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 888
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lepson/print/ActivityDocsPrintPreview$5$1;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

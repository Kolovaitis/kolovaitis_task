.class public abstract Lepson/print/ecclient/HttpAccess;
.super Ljava/lang/Object;
.source "HttpAccess.java"


# instance fields
.field public final ECC_ERR_COMM_ERROR:I

.field public final ECC_ERR_NONE:I

.field public final ECC_ERR_OPR_FAIL:I

.field final LOG_TAG:Ljava/lang/String;

.field mDisplay:Lepson/print/ecclient/DebugDisplay;

.field public mHttpRetCode:I

.field public mResString:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "epson_connect"

    .line 6
    iput-object v0, p0, Lepson/print/ecclient/HttpAccess;->LOG_TAG:Ljava/lang/String;

    const/4 v0, 0x0

    .line 8
    iput v0, p0, Lepson/print/ecclient/HttpAccess;->ECC_ERR_NONE:I

    const/16 v0, -0x3e8

    .line 9
    iput v0, p0, Lepson/print/ecclient/HttpAccess;->ECC_ERR_OPR_FAIL:I

    const/16 v0, -0x44c

    .line 10
    iput v0, p0, Lepson/print/ecclient/HttpAccess;->ECC_ERR_COMM_ERROR:I

    return-void
.end method


# virtual methods
.method public abstract GetFile(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public abstract HttpGet(Ljava/lang/String;)I
.end method

.method public abstract HttpPost(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public abstract PostFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)I
.end method

.method public abstract cancel()V
.end method

.method public abstract resetCancel()V
.end method

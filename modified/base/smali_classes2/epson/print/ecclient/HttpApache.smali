.class public Lepson/print/ecclient/HttpApache;
.super Lepson/print/ecclient/HttpAccess;
.source "HttpApache.java"


# instance fields
.field private volatile mCanceled:Z

.field private mHttpClient:Lepson/common/httpclient/IAHttpClient;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 9
    invoke-direct {p0}, Lepson/print/ecclient/HttpAccess;-><init>()V

    const/4 v0, 0x0

    .line 10
    iput-boolean v0, p0, Lepson/print/ecclient/HttpApache;->mCanceled:Z

    const/4 v0, 0x0

    .line 14
    iput-object v0, p0, Lepson/print/ecclient/HttpApache;->mHttpClient:Lepson/common/httpclient/IAHttpClient;

    return-void
.end method

.method private dispHttpCmd(Ljava/net/URL;)V
    .locals 3

    .line 32
    iget-object v0, p0, Lepson/print/ecclient/HttpApache;->mDisplay:Lepson/print/ecclient/DebugDisplay;

    if-eqz v0, :cond_0

    .line 33
    iget-object v0, p0, Lepson/print/ecclient/HttpApache;->mDisplay:Lepson/print/ecclient/DebugDisplay;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "=> "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lepson/print/ecclient/DebugDisplay;->addResText(Ljava/lang/String;)V

    :cond_0
    const-string v0, "epson_connect"

    .line 35
    invoke-virtual {p1}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lepson/print/Util/EPLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private dispHttpRes(Ljava/lang/String;)V
    .locals 3

    .line 24
    iget-object v0, p0, Lepson/print/ecclient/HttpApache;->mDisplay:Lepson/print/ecclient/DebugDisplay;

    if-eqz v0, :cond_0

    .line 25
    iget-object v0, p0, Lepson/print/ecclient/HttpApache;->mDisplay:Lepson/print/ecclient/DebugDisplay;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lepson/print/ecclient/DebugDisplay;->addResText(Ljava/lang/String;)V

    :cond_0
    const-string v0, "epson_connect"

    .line 27
    invoke-static {v0, p1}, Lepson/print/Util/EPLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public GetFile(Ljava/lang/String;Ljava/lang/String;)I
    .locals 4

    .line 138
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[get file]::"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " file <"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lepson/print/ecclient/HttpApache;->dispHttpRes(Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 139
    iput-object v0, p0, Lepson/print/ecclient/HttpApache;->mResString:Ljava/lang/String;

    const/4 v0, 0x0

    .line 140
    iput v0, p0, Lepson/print/ecclient/HttpApache;->mHttpRetCode:I

    .line 141
    iget-boolean v1, p0, Lepson/print/ecclient/HttpApache;->mCanceled:Z

    if-eqz v1, :cond_0

    const/4 p1, -0x2

    return p1

    :cond_0
    const/16 v1, -0x44c

    .line 146
    :try_start_0
    new-instance v2, Lepson/common/httpclient/IAHttpClient;

    invoke-direct {v2}, Lepson/common/httpclient/IAHttpClient;-><init>()V

    .line 148
    new-instance v3, Lepson/common/httpclient/IAHttpClient$HttpGet;

    invoke-direct {v3, p1}, Lepson/common/httpclient/IAHttpClient$HttpGet;-><init>(Ljava/lang/String;)V

    .line 149
    invoke-virtual {v3, p2, v0, v0}, Lepson/common/httpclient/IAHttpClient$HttpGet;->setEntityFile(Ljava/lang/String;II)V

    .line 151
    invoke-virtual {v2, v3}, Lepson/common/httpclient/IAHttpClient;->executeFile(Lepson/common/httpclient/IAHttpClient$HttpGet;)Lepson/common/httpclient/IAHttpClient$HttpResponse;

    move-result-object p1

    const/16 p2, 0xc8

    .line 153
    invoke-virtual {p1}, Lepson/common/httpclient/IAHttpClient$HttpResponse;->getResponseCode()I

    move-result v2

    if-eq p2, v2, :cond_1

    const-string p1, "epson_connect"

    const-string p2, "error in HttpGet(): "

    .line 154
    invoke-static {p1, p2}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    .line 157
    :cond_1
    invoke-virtual {p1}, Lepson/common/httpclient/IAHttpClient$HttpResponse;->getResponseCode()I

    move-result p1

    iput p1, p0, Lepson/print/ecclient/HttpApache;->mHttpRetCode:I

    .line 158
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "getFile return :: <"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p2, p0, Lepson/print/ecclient/HttpApache;->mHttpRetCode:I

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, ">\n"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lepson/print/ecclient/HttpApache;->dispHttpRes(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    move-exception p1

    const-string p2, "epson_connect"

    .line 160
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "error in HttpGet: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p2, p1}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    return v1
.end method

.method public HttpGet(Ljava/lang/String;)I
    .locals 3

    const/4 v0, 0x0

    .line 42
    iput-object v0, p0, Lepson/print/ecclient/HttpApache;->mResString:Ljava/lang/String;

    const/4 v0, 0x0

    .line 43
    iput v0, p0, Lepson/print/ecclient/HttpApache;->mHttpRetCode:I

    .line 44
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[get]::"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lepson/print/ecclient/HttpApache;->dispHttpRes(Ljava/lang/String;)V

    .line 46
    :try_start_0
    new-instance v1, Lepson/common/httpclient/IAHttpClient;

    invoke-direct {v1}, Lepson/common/httpclient/IAHttpClient;-><init>()V

    iput-object v1, p0, Lepson/print/ecclient/HttpApache;->mHttpClient:Lepson/common/httpclient/IAHttpClient;

    .line 48
    new-instance v1, Lepson/common/httpclient/IAHttpClient$HttpGet;

    invoke-direct {v1, p1}, Lepson/common/httpclient/IAHttpClient$HttpGet;-><init>(Ljava/lang/String;)V

    .line 49
    iget-object p1, p0, Lepson/print/ecclient/HttpApache;->mHttpClient:Lepson/common/httpclient/IAHttpClient;

    invoke-virtual {p1, v1}, Lepson/common/httpclient/IAHttpClient;->execute(Lepson/common/httpclient/IAHttpClient$HttpGet;)Lepson/common/httpclient/IAHttpClient$HttpResponse;

    move-result-object p1

    .line 51
    invoke-virtual {p1}, Lepson/common/httpclient/IAHttpClient$HttpResponse;->getEntity()Ljava/io/ByteArrayOutputStream;

    move-result-object v1

    const-string v2, "UTF-8"

    invoke-virtual {v1, v2}, Ljava/io/ByteArrayOutputStream;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lepson/print/ecclient/HttpApache;->mResString:Ljava/lang/String;

    .line 52
    invoke-virtual {p1}, Lepson/common/httpclient/IAHttpClient$HttpResponse;->getResponseCode()I

    move-result p1

    iput p1, p0, Lepson/print/ecclient/HttpApache;->mHttpRetCode:I

    .line 53
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "get return :: <"

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lepson/print/ecclient/HttpApache;->mHttpRetCode:I

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "> \'"

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lepson/print/ecclient/HttpApache;->mResString:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\'\n"

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lepson/print/ecclient/HttpApache;->dispHttpRes(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    move-exception p1

    const-string v0, "epson_connect"

    .line 55
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "error in HttpGet: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    const/16 p1, -0x44c

    return p1
.end method

.method public HttpPost(Ljava/lang/String;Ljava/lang/String;)I
    .locals 3

    const/4 v0, 0x0

    .line 66
    iput-object v0, p0, Lepson/print/ecclient/HttpApache;->mResString:Ljava/lang/String;

    const/4 v0, 0x0

    .line 67
    iput v0, p0, Lepson/print/ecclient/HttpApache;->mHttpRetCode:I

    .line 68
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[post]:: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " :: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lepson/print/ecclient/HttpApache;->dispHttpRes(Ljava/lang/String;)V

    .line 71
    :try_start_0
    new-instance v1, Lepson/common/httpclient/IAHttpClient$HttpPost;

    invoke-direct {v1, p1}, Lepson/common/httpclient/IAHttpClient$HttpPost;-><init>(Ljava/lang/String;)V

    .line 73
    new-instance p1, Lepson/common/httpclient/IAHttpClient;

    invoke-direct {p1}, Lepson/common/httpclient/IAHttpClient;-><init>()V

    iput-object p1, p0, Lepson/print/ecclient/HttpApache;->mHttpClient:Lepson/common/httpclient/IAHttpClient;

    .line 76
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v1, p1}, Lepson/common/httpclient/IAHttpClient$HttpPost;->setChunked(Ljava/lang/Boolean;)V

    const-string p1, "application/json"

    .line 77
    invoke-virtual {v1, p1}, Lepson/common/httpclient/IAHttpClient$HttpPost;->setContentType(Ljava/lang/String;)V

    const-string p1, "UTF-8"

    .line 78
    invoke-virtual {v1, p1}, Lepson/common/httpclient/IAHttpClient$HttpPost;->setContentEncoding(Ljava/lang/String;)V

    const-string p1, "UTF-8"

    .line 79
    invoke-virtual {p2, p1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object p1

    invoke-virtual {v1, p1}, Lepson/common/httpclient/IAHttpClient$HttpPost;->setEntity([B)V

    .line 81
    iget-object p1, p0, Lepson/print/ecclient/HttpApache;->mHttpClient:Lepson/common/httpclient/IAHttpClient;

    invoke-virtual {p1, v1}, Lepson/common/httpclient/IAHttpClient;->execute(Lepson/common/httpclient/IAHttpClient$HttpPost;)Lepson/common/httpclient/IAHttpClient$HttpResponse;

    move-result-object p1

    .line 83
    invoke-virtual {p1}, Lepson/common/httpclient/IAHttpClient$HttpResponse;->getEntity()Ljava/io/ByteArrayOutputStream;

    move-result-object p2

    const-string v1, "UTF-8"

    invoke-virtual {p2, v1}, Ljava/io/ByteArrayOutputStream;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lepson/print/ecclient/HttpApache;->mResString:Ljava/lang/String;

    .line 84
    invoke-virtual {p1}, Lepson/common/httpclient/IAHttpClient$HttpResponse;->getResponseCode()I

    move-result p1

    iput p1, p0, Lepson/print/ecclient/HttpApache;->mHttpRetCode:I

    .line 85
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "post return :: <"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p2, p0, Lepson/print/ecclient/HttpApache;->mHttpRetCode:I

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, "> \'"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p2, p0, Lepson/print/ecclient/HttpApache;->mResString:Ljava/lang/String;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "\'\n"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lepson/print/ecclient/HttpApache;->dispHttpRes(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    move-exception p1

    const-string p2, "epson_connect"

    .line 87
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "error in HttpPost: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p2, p1}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    const/16 p1, -0x44c

    return p1
.end method

.method public PostFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)I
    .locals 2

    const/4 p2, 0x0

    .line 99
    iput-object p2, p0, Lepson/print/ecclient/HttpApache;->mResString:Ljava/lang/String;

    const/4 p2, 0x0

    .line 100
    iput p2, p0, Lepson/print/ecclient/HttpApache;->mHttpRetCode:I

    .line 101
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[post file]:: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " :: file: <"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "> offset: <"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "> dataSize: <"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lepson/print/ecclient/HttpApache;->dispHttpRes(Ljava/lang/String;)V

    .line 103
    iget-boolean v0, p0, Lepson/print/ecclient/HttpApache;->mCanceled:Z

    if-eqz v0, :cond_0

    const/4 p1, -0x2

    return p1

    :cond_0
    const/16 v0, -0x44c

    .line 108
    :try_start_0
    new-instance v1, Lepson/common/httpclient/IAHttpClient$HttpPost;

    invoke-direct {v1, p1}, Lepson/common/httpclient/IAHttpClient$HttpPost;-><init>(Ljava/lang/String;)V

    .line 111
    invoke-virtual {v1, p4, p5, p6}, Lepson/common/httpclient/IAHttpClient$HttpPost;->setEntityFile(Ljava/lang/String;II)V

    .line 112
    invoke-virtual {v1, p3}, Lepson/common/httpclient/IAHttpClient$HttpPost;->setContentType(Ljava/lang/String;)V

    .line 113
    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v1, p1}, Lepson/common/httpclient/IAHttpClient$HttpPost;->setContentLength(Ljava/lang/Integer;)V

    .line 115
    new-instance p1, Lepson/common/httpclient/IAHttpClient;

    invoke-direct {p1}, Lepson/common/httpclient/IAHttpClient;-><init>()V

    iput-object p1, p0, Lepson/print/ecclient/HttpApache;->mHttpClient:Lepson/common/httpclient/IAHttpClient;

    .line 116
    iget-object p1, p0, Lepson/print/ecclient/HttpApache;->mHttpClient:Lepson/common/httpclient/IAHttpClient;

    invoke-virtual {p1, v1}, Lepson/common/httpclient/IAHttpClient;->executeFile(Lepson/common/httpclient/IAHttpClient$HttpPost;)Lepson/common/httpclient/IAHttpClient$HttpResponse;

    move-result-object p1

    .line 117
    invoke-virtual {p1}, Lepson/common/httpclient/IAHttpClient$HttpResponse;->getResponseCode()I

    move-result p3

    const/16 p4, 0xc8

    if-eq p3, p4, :cond_1

    const-string p1, "epson_connect"

    .line 119
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "error in PostFile: status = <"

    invoke-virtual {p2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p3, ">"

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    return v0

    .line 123
    :cond_1
    invoke-virtual {p1}, Lepson/common/httpclient/IAHttpClient$HttpResponse;->getEntity()Ljava/io/ByteArrayOutputStream;

    move-result-object p3

    const-string p4, "UTF-8"

    invoke-virtual {p3, p4}, Ljava/io/ByteArrayOutputStream;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    iput-object p3, p0, Lepson/print/ecclient/HttpApache;->mResString:Ljava/lang/String;

    .line 124
    invoke-virtual {p1}, Lepson/common/httpclient/IAHttpClient$HttpResponse;->getResponseCode()I

    move-result p1

    iput p1, p0, Lepson/print/ecclient/HttpApache;->mHttpRetCode:I

    .line 125
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "post file return :: <"

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p3, p0, Lepson/print/ecclient/HttpApache;->mHttpRetCode:I

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p3, "> \'"

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p3, p0, Lepson/print/ecclient/HttpApache;->mResString:Ljava/lang/String;

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p3, "\'\n"

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lepson/print/ecclient/HttpApache;->dispHttpRes(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p2

    :catch_0
    move-exception p1

    const-string p2, "epson_connect"

    .line 127
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "exception in HttpPost: "

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p2, p1}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    return v0
.end method

.method public cancel()V
    .locals 2

    const-string v0, "epson_connect"

    const-string v1, "cancel called()"

    .line 176
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 178
    iput-boolean v0, p0, Lepson/print/ecclient/HttpApache;->mCanceled:Z

    .line 181
    invoke-virtual {p0}, Lepson/print/ecclient/HttpApache;->shutdown()V

    return-void
.end method

.method public resetCancel()V
    .locals 1

    const/4 v0, 0x0

    .line 186
    iput-boolean v0, p0, Lepson/print/ecclient/HttpApache;->mCanceled:Z

    return-void
.end method

.method public setDisplay(Lepson/print/ecclient/DebugDisplay;)V
    .locals 0

    .line 18
    iput-object p1, p0, Lepson/print/ecclient/HttpApache;->mDisplay:Lepson/print/ecclient/DebugDisplay;

    return-void
.end method

.method shutdown()V
    .locals 1

    .line 193
    iget-object v0, p0, Lepson/print/ecclient/HttpApache;->mHttpClient:Lepson/common/httpclient/IAHttpClient;

    if-eqz v0, :cond_0

    .line 194
    invoke-virtual {v0}, Lepson/common/httpclient/IAHttpClient;->disconnect()V

    :cond_0
    return-void
.end method

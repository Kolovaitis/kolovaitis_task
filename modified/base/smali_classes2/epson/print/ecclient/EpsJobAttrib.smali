.class public Lepson/print/ecclient/EpsJobAttrib;
.super Ljava/lang/Object;
.source "EpsJobAttrib.java"


# instance fields
.field public mApfAutoCorrect:I

.field public mBottomMargin:I

.field public mBrightness:I

.field public mCdDimIn:I

.field public mCdDimOut:I

.field public mCmdType:I

.field public mColorMode:I

.field public mColorPlane:I

.field public mContrast:I

.field public mCopies:I

.field public mDuplex:I

.field public mFeedDirection:I

.field public mInputResolution:I

.field public mLeftMargin:I

.field public mMediaSizeIdx:I

.field public mMediaTypeIdx:I

.field public mPageNum:I

.field public mPaletteData:[B

.field public mPaletteSize:I

.field public mPaperSource:I

.field public mPrintDirection:I

.field public mPrintLayout:I

.field public mPrintQuality:I

.field public mRedeye:I

.field public mRightMargin:I

.field public mSaturation:I

.field public mSharpness:I

.field public mTopMargin:I

.field public mUserDefHeight:I

.field public mUserDefWidth:I

.field public mVersion:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    invoke-virtual {p0}, Lepson/print/ecclient/EpsJobAttrib;->setDefault()V

    return-void
.end method


# virtual methods
.method public setDefault()V
    .locals 3

    const/4 v0, 0x4

    .line 49
    iput v0, p0, Lepson/print/ecclient/EpsJobAttrib;->mVersion:I

    const/4 v0, 0x1

    .line 51
    iput v0, p0, Lepson/print/ecclient/EpsJobAttrib;->mColorPlane:I

    const/4 v1, 0x0

    .line 52
    iput v1, p0, Lepson/print/ecclient/EpsJobAttrib;->mPaletteSize:I

    const/4 v2, 0x0

    .line 53
    iput-object v2, p0, Lepson/print/ecclient/EpsJobAttrib;->mPaletteData:[B

    .line 54
    iput v0, p0, Lepson/print/ecclient/EpsJobAttrib;->mInputResolution:I

    .line 55
    iput v1, p0, Lepson/print/ecclient/EpsJobAttrib;->mMediaSizeIdx:I

    .line 56
    iput v1, p0, Lepson/print/ecclient/EpsJobAttrib;->mMediaTypeIdx:I

    const/4 v2, 0x2

    .line 57
    iput v2, p0, Lepson/print/ecclient/EpsJobAttrib;->mPrintLayout:I

    .line 58
    iput v2, p0, Lepson/print/ecclient/EpsJobAttrib;->mPrintQuality:I

    .line 59
    iput v2, p0, Lepson/print/ecclient/EpsJobAttrib;->mPaperSource:I

    .line 60
    iput v1, p0, Lepson/print/ecclient/EpsJobAttrib;->mPrintDirection:I

    .line 61
    iput v0, p0, Lepson/print/ecclient/EpsJobAttrib;->mColorMode:I

    .line 62
    iput v1, p0, Lepson/print/ecclient/EpsJobAttrib;->mBrightness:I

    .line 63
    iput v1, p0, Lepson/print/ecclient/EpsJobAttrib;->mContrast:I

    .line 64
    iput v1, p0, Lepson/print/ecclient/EpsJobAttrib;->mSaturation:I

    .line 65
    iput v1, p0, Lepson/print/ecclient/EpsJobAttrib;->mApfAutoCorrect:I

    .line 66
    iput v1, p0, Lepson/print/ecclient/EpsJobAttrib;->mSharpness:I

    .line 67
    iput v1, p0, Lepson/print/ecclient/EpsJobAttrib;->mRedeye:I

    .line 68
    iput v1, p0, Lepson/print/ecclient/EpsJobAttrib;->mTopMargin:I

    .line 69
    iput v1, p0, Lepson/print/ecclient/EpsJobAttrib;->mLeftMargin:I

    .line 70
    iput v1, p0, Lepson/print/ecclient/EpsJobAttrib;->mBottomMargin:I

    .line 71
    iput v1, p0, Lepson/print/ecclient/EpsJobAttrib;->mRightMargin:I

    .line 72
    iput v1, p0, Lepson/print/ecclient/EpsJobAttrib;->mCdDimIn:I

    .line 73
    iput v1, p0, Lepson/print/ecclient/EpsJobAttrib;->mCdDimOut:I

    .line 75
    iput v1, p0, Lepson/print/ecclient/EpsJobAttrib;->mCmdType:I

    .line 77
    iput v1, p0, Lepson/print/ecclient/EpsJobAttrib;->mDuplex:I

    .line 78
    iput v0, p0, Lepson/print/ecclient/EpsJobAttrib;->mCopies:I

    .line 79
    iput v1, p0, Lepson/print/ecclient/EpsJobAttrib;->mFeedDirection:I

    .line 81
    iput v1, p0, Lepson/print/ecclient/EpsJobAttrib;->mUserDefWidth:I

    .line 82
    iput v1, p0, Lepson/print/ecclient/EpsJobAttrib;->mUserDefHeight:I

    .line 83
    iput v1, p0, Lepson/print/ecclient/EpsJobAttrib;->mPageNum:I

    return-void
.end method

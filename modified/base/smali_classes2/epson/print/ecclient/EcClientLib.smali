.class public Lepson/print/ecclient/EcClientLib;
.super Ljava/lang/Object;
.source "EcClientLib.java"


# static fields
.field public static final ECC_CM_COLOR:I = 0x1

.field public static final ECC_CM_MONOCHROME:I = 0x2

.field public static final ECC_RENDER_ABORTED:I = 0x4

.field public static final ECC_RENDER_CANCELED:I = 0x3

.field public static final ECC_RENDER_COMPLETED:I = 0x2

.field public static final ECC_RENDER_PROCESSING:I = 0x1

.field public static final ECC_RENDER_UNKNOWN:I = 0x0

.field public static final ECC_SOURCE_UNK:I = 0x0

.field public static final ECC_SRC_DOCFILE:I = 0x1

.field public static final ECC_SRC_IMAGES:I = 0x2

.field public static final ECC_SRC_WEB:I = 0x3

.field public static final ECT_CAPABILITY:I = 0x3

.field public static final ECT_CLIENT_INFO:I = 0x2

.field public static final ECT_JOB_INFO:I = 0x7

.field public static final ECT_PRINTER_SUPPORT:I = 0x1

.field public static final ECT_PRINT_SETTING:I = 0x4

.field public static final ECT_RENDER_STAT:I = 0x6

.field public static final ECT_URI:I = 0x5


# instance fields
.field public mClientId:Ljava/lang/String;

.field public mColorModeAtLocal:I

.field public mColorModeAtPhoto:I

.field public mColorModeAtRemote:I

.field public mCompletePage:I

.field public mEccJobInfo:Lepson/print/ecclient/EccJobInfo;

.field public mEpsonDeviceId:Ljava/lang/String;

.field mHttpAcess:Lepson/print/ecclient/HttpAccess;

.field public mJobAttrib:Lepson/print/ecclient/EpsJobAttrib;

.field public mLogUri:Ljava/lang/String;

.field public mMediaAtLocal:Lepson/print/ecclient/EpsSupportedMedia;

.field public mMediaAtPhoto:Lepson/print/ecclient/EpsSupportedMedia;

.field public mMediaAtRemote:Lepson/print/ecclient/EpsSupportedMedia;

.field mNativeInscance:J

.field public mPrinterName:Ljava/lang/String;

.field public mPrinterName2:Ljava/lang/String;

.field public mRenderStatus:I

.field public mSerialNumber:Ljava/lang/String;

.field public mType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "ecclient"

    .line 94
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    .line 36
    iput-wide v0, p0, Lepson/print/ecclient/EcClientLib;->mNativeInscance:J

    return-void
.end method

.method public static native GetPid()I
.end method


# virtual methods
.method public native ChangePrintSetting(Lepson/print/ecclient/EpsJobAttrib;Ljava/lang/String;Ljava/lang/String;Z)I
.end method

.method public native CreateJob(ILjava/lang/String;ILepson/print/ecclient/EpsJobAttrib;II)I
.end method

.method public native DebugPrintPid()V
.end method

.method public native DownloadPreview(ILjava/lang/String;)I
.end method

.method public native EndJob()I
.end method

.method public native GetCapability()I
.end method

.method public native GetDefaultSetting()I
.end method

.method public native GetPrintLogUri(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public native GetPrinterInfo()I
.end method

.method public native GetRenderingStatus()I
.end method

.method public Initialize()Z
    .locals 1

    .line 103
    new-instance v0, Lepson/print/ecclient/HttpApache;

    invoke-direct {v0}, Lepson/print/ecclient/HttpApache;-><init>()V

    .line 105
    invoke-virtual {p0, v0}, Lepson/print/ecclient/EcClientLib;->SetHttpAccess(Lepson/print/ecclient/HttpAccess;)V

    .line 106
    invoke-virtual {p0}, Lepson/print/ecclient/EcClientLib;->NativeInitialize()Z

    move-result v0

    return v0
.end method

.method public native Login(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public native Logout()I
.end method

.method public native NativeInitialize()Z
.end method

.method public native RegPrinter(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
.end method

.method SetHttpAccess(Lepson/print/ecclient/HttpAccess;)V
    .locals 0

    .line 112
    iput-object p1, p0, Lepson/print/ecclient/EcClientLib;->mHttpAcess:Lepson/print/ecclient/HttpAccess;

    return-void
.end method

.method public native StartPrint(II)I
.end method

.method public native Terminate()V
.end method

.method public native UploadFile(Ljava/lang/String;II)I
.end method

.method public cancel()V
    .locals 1

    .line 195
    iget-object v0, p0, Lepson/print/ecclient/EcClientLib;->mHttpAcess:Lepson/print/ecclient/HttpAccess;

    invoke-virtual {v0}, Lepson/print/ecclient/HttpAccess;->cancel()V

    return-void
.end method

.method public clientId()Ljava/lang/String;
    .locals 1

    .line 248
    iget-object v0, p0, Lepson/print/ecclient/EcClientLib;->mClientId:Ljava/lang/String;

    return-object v0
.end method

.method public deviceId()Ljava/lang/String;
    .locals 1

    .line 238
    iget-object v0, p0, Lepson/print/ecclient/EcClientLib;->mEpsonDeviceId:Ljava/lang/String;

    return-object v0
.end method

.method protected finalize()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 223
    invoke-virtual {p0}, Lepson/print/ecclient/EcClientLib;->Terminate()V

    return-void
.end method

.method public printerName()Ljava/lang/String;
    .locals 1

    .line 243
    iget-object v0, p0, Lepson/print/ecclient/EcClientLib;->mPrinterName:Ljava/lang/String;

    return-object v0
.end method

.method public printerName2()Ljava/lang/String;
    .locals 1

    .line 228
    iget-object v0, p0, Lepson/print/ecclient/EcClientLib;->mPrinterName2:Ljava/lang/String;

    return-object v0
.end method

.method public resetCancel()V
    .locals 1

    .line 201
    iget-object v0, p0, Lepson/print/ecclient/EcClientLib;->mHttpAcess:Lepson/print/ecclient/HttpAccess;

    invoke-virtual {v0}, Lepson/print/ecclient/HttpAccess;->resetCancel()V

    return-void
.end method

.method public serialNumber()Ljava/lang/String;
    .locals 1

    .line 233
    iget-object v0, p0, Lepson/print/ecclient/EcClientLib;->mSerialNumber:Ljava/lang/String;

    return-object v0
.end method

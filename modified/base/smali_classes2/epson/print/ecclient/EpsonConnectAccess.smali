.class public Lepson/print/ecclient/EpsonConnectAccess;
.super Ljava/lang/Object;
.source "EpsonConnectAccess.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/ecclient/EpsonConnectAccess$AltFactory;,
        Lepson/print/ecclient/EpsonConnectAccess$AltEpsonConnectAccess;,
        Lepson/print/ecclient/EpsonConnectAccess$Factory;,
        Lepson/print/ecclient/EpsonConnectAccess$RemotePrinterInfo;
    }
.end annotation


# static fields
.field private static sFactory:Lepson/print/ecclient/EpsonConnectAccess$Factory;


# instance fields
.field mEcClientLib:Lepson/print/ecclient/EcClientLib;


# direct methods
.method constructor <init>()V
    .locals 1

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    new-instance v0, Lepson/print/ecclient/EcClientLib;

    invoke-direct {v0}, Lepson/print/ecclient/EcClientLib;-><init>()V

    iput-object v0, p0, Lepson/print/ecclient/EpsonConnectAccess;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    return-void
.end method

.method public static getInstance()Lepson/print/ecclient/EpsonConnectAccess;
    .locals 1

    .line 188
    invoke-static {}, Lepson/print/ecclient/EpsonConnectAccess;->setFactory()Lepson/print/ecclient/EpsonConnectAccess$Factory;

    .line 189
    sget-object v0, Lepson/print/ecclient/EpsonConnectAccess;->sFactory:Lepson/print/ecclient/EpsonConnectAccess$Factory;

    invoke-virtual {v0}, Lepson/print/ecclient/EpsonConnectAccess$Factory;->getEpsonConnectAccessClass()Lepson/print/ecclient/EpsonConnectAccess;

    move-result-object v0

    return-object v0
.end method

.method public static getPrinterInfo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lepson/print/ecclient/EpsonConnectAccess$RemotePrinterInfo;
    .locals 3

    .line 239
    invoke-static {}, Lepson/print/ecclient/EpsonConnectAccess;->getInstance()Lepson/print/ecclient/EpsonConnectAccess;

    move-result-object v0

    .line 240
    invoke-virtual {v0}, Lepson/print/ecclient/EpsonConnectAccess;->Initialize()Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    return-object v2

    .line 243
    :cond_0
    invoke-virtual {v0, p0, p1, p2}, Lepson/print/ecclient/EpsonConnectAccess;->Login(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result p0

    if-eqz p0, :cond_1

    return-object v2

    .line 248
    :cond_1
    invoke-virtual {v0}, Lepson/print/ecclient/EpsonConnectAccess;->libGetPrinterInfo()Lepson/print/ecclient/EpsonConnectAccess$RemotePrinterInfo;

    move-result-object p0

    .line 249
    invoke-virtual {v0}, Lepson/print/ecclient/EpsonConnectAccess;->logout()I

    .line 251
    invoke-virtual {v0}, Lepson/print/ecclient/EpsonConnectAccess;->Terminate()V

    return-object p0
.end method

.method static loadClientId(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .line 272
    invoke-static {p0}, Lepson/provider/SharedPreferencesProvider;->getInstace(Landroid/content/Context;)Lepson/provider/SharedPreferencesProvider$SharedPreferencesMulti;

    move-result-object p0

    const-string v0, "PRINTER_CLIENT_ID"

    const/4 v1, 0x0

    .line 273
    invoke-virtual {p0, v0, v1}, Lepson/provider/SharedPreferencesProvider$SharedPreferencesMulti;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 274
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\u25b2clientId = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    if-eqz p0, :cond_1

    .line 275
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_0

    goto :goto_0

    :cond_0
    return-object p0

    :cond_1
    :goto_0
    return-object v1
.end method

.method public static declared-synchronized registRemotePrinter(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I
    .locals 3

    const-class v0, Lepson/print/ecclient/EpsonConnectAccess;

    monitor-enter v0

    .line 204
    :try_start_0
    invoke-static {}, Lepson/print/ecclient/EpsonConnectAccess;->getInstance()Lepson/print/ecclient/EpsonConnectAccess;

    move-result-object v1

    .line 205
    invoke-virtual {v1}, Lepson/print/ecclient/EpsonConnectAccess;->Initialize()Z

    move-result v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_0

    const/16 p0, -0x44c

    .line 207
    monitor-exit v0

    return p0

    .line 210
    :cond_0
    :try_start_1
    invoke-static {p1}, Lepson/print/ecclient/EcClientLibUtil;->quoteForJsonString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 211
    invoke-static {p2}, Lepson/print/ecclient/EcClientLibUtil;->quoteForJsonString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 213
    invoke-static {p0}, Lepson/print/ecclient/EpsonConnectAccess;->loadClientId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 214
    invoke-virtual {v1, p1, p2, v2}, Lepson/print/ecclient/EpsonConnectAccess;->RegPrinter(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    if-nez p1, :cond_2

    if-nez v2, :cond_1

    .line 218
    invoke-virtual {v1}, Lepson/print/ecclient/EpsonConnectAccess;->getClientId()Ljava/lang/String;

    move-result-object p2

    invoke-static {p0, p2}, Lepson/print/ecclient/EpsonConnectAccess;->saveClientId(Landroid/content/Context;Ljava/lang/String;)V

    .line 224
    :cond_1
    invoke-virtual {v1}, Lepson/print/ecclient/EpsonConnectAccess;->Terminate()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 226
    monitor-exit v0

    return p1

    .line 221
    :cond_2
    :try_start_2
    invoke-virtual {v1}, Lepson/print/ecclient/EpsonConnectAccess;->Terminate()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 222
    monitor-exit v0

    return p1

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method static saveClientId(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .line 288
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\u25b2 save  clientId = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;)V

    .line 289
    invoke-static {p0}, Lepson/provider/SharedPreferencesProvider;->getInstace(Landroid/content/Context;)Lepson/provider/SharedPreferencesProvider$SharedPreferencesMulti;

    move-result-object p0

    invoke-virtual {p0}, Lepson/provider/SharedPreferencesProvider$SharedPreferencesMulti;->edit()Lepson/provider/SharedPreferencesProvider$Editor;

    move-result-object p0

    const-string v0, "PRINTER_CLIENT_ID"

    invoke-virtual {p0, v0, p1}, Lepson/provider/SharedPreferencesProvider$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Lepson/provider/SharedPreferencesProvider$Editor;

    move-result-object p0

    invoke-virtual {p0}, Lepson/provider/SharedPreferencesProvider$Editor;->apply()V

    return-void
.end method

.method static setFactory()Lepson/print/ecclient/EpsonConnectAccess$Factory;
    .locals 1

    .line 296
    sget-object v0, Lepson/print/ecclient/EpsonConnectAccess;->sFactory:Lepson/print/ecclient/EpsonConnectAccess$Factory;

    if-nez v0, :cond_0

    .line 300
    new-instance v0, Lepson/print/ecclient/EpsonConnectAccess$Factory;

    invoke-direct {v0}, Lepson/print/ecclient/EpsonConnectAccess$Factory;-><init>()V

    sput-object v0, Lepson/print/ecclient/EpsonConnectAccess;->sFactory:Lepson/print/ecclient/EpsonConnectAccess$Factory;

    .line 303
    :cond_0
    sget-object v0, Lepson/print/ecclient/EpsonConnectAccess;->sFactory:Lepson/print/ecclient/EpsonConnectAccess$Factory;

    return-object v0
.end method


# virtual methods
.method public ChangePrintSetting(Lepson/print/ecclient/EpsJobAttrib;Ljava/lang/String;Ljava/lang/String;Z)I
    .locals 1

    .line 113
    iget-object v0, p0, Lepson/print/ecclient/EpsonConnectAccess;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    invoke-virtual {v0, p1, p2, p3, p4}, Lepson/print/ecclient/EcClientLib;->ChangePrintSetting(Lepson/print/ecclient/EpsJobAttrib;Ljava/lang/String;Ljava/lang/String;Z)I

    move-result p1

    return p1
.end method

.method public CreateJob(ILjava/lang/String;ILepson/print/ecclient/EpsJobAttrib;II)I
    .locals 7

    .line 86
    iget-object v0, p0, Lepson/print/ecclient/EpsonConnectAccess;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    move v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lepson/print/ecclient/EcClientLib;->CreateJob(ILjava/lang/String;ILepson/print/ecclient/EpsJobAttrib;II)I

    move-result p1

    return p1
.end method

.method public DownloadPreview(ILjava/lang/String;)I
    .locals 1

    .line 107
    iget-object v0, p0, Lepson/print/ecclient/EpsonConnectAccess;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    invoke-virtual {v0, p1, p2}, Lepson/print/ecclient/EcClientLib;->DownloadPreview(ILjava/lang/String;)I

    move-result p1

    return p1
.end method

.method public EndJob()I
    .locals 1

    .line 92
    iget-object v0, p0, Lepson/print/ecclient/EpsonConnectAccess;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    invoke-virtual {v0}, Lepson/print/ecclient/EcClientLib;->EndJob()I

    move-result v0

    return v0
.end method

.method public GetCapability()I
    .locals 1

    .line 68
    iget-object v0, p0, Lepson/print/ecclient/EpsonConnectAccess;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    invoke-virtual {v0}, Lepson/print/ecclient/EcClientLib;->GetCapability()I

    move-result v0

    return v0
.end method

.method public GetDefaultSetting()I
    .locals 1

    .line 73
    iget-object v0, p0, Lepson/print/ecclient/EpsonConnectAccess;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    invoke-virtual {v0}, Lepson/print/ecclient/EcClientLib;->GetDefaultSetting()I

    move-result v0

    return v0
.end method

.method public GetPrintLogUri(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 1

    .line 79
    iget-object v0, p0, Lepson/print/ecclient/EpsonConnectAccess;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    invoke-virtual {v0, p1, p2, p3, p4}, Lepson/print/ecclient/EcClientLib;->GetPrintLogUri(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public GetRenderingStatus()I
    .locals 1

    .line 102
    iget-object v0, p0, Lepson/print/ecclient/EpsonConnectAccess;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    invoke-virtual {v0}, Lepson/print/ecclient/EcClientLib;->GetRenderingStatus()I

    move-result v0

    return v0
.end method

.method public Initialize()Z
    .locals 1

    .line 28
    iget-object v0, p0, Lepson/print/ecclient/EpsonConnectAccess;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    invoke-virtual {v0}, Lepson/print/ecclient/EcClientLib;->Initialize()Z

    move-result v0

    return v0
.end method

.method public Login(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I
    .locals 2

    .line 43
    invoke-static {p2}, Lepson/print/ecclient/EcClientLibUtil;->quoteForJsonString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 44
    invoke-static {p3}, Lepson/print/ecclient/EcClientLibUtil;->quoteForJsonString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    .line 46
    invoke-static {p1}, Lepson/print/ecclient/EpsonConnectAccess;->loadClientId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 47
    iget-object v1, p0, Lepson/print/ecclient/EpsonConnectAccess;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    invoke-virtual {v1, p2, p3, v0}, Lepson/print/ecclient/EcClientLib;->Login(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result p2

    if-nez p2, :cond_0

    if-nez v0, :cond_0

    .line 50
    invoke-virtual {p0}, Lepson/print/ecclient/EpsonConnectAccess;->getClientId()Ljava/lang/String;

    move-result-object p3

    invoke-static {p1, p3}, Lepson/print/ecclient/EpsonConnectAccess;->saveClientId(Landroid/content/Context;Ljava/lang/String;)V

    :cond_0
    return p2
.end method

.method RegPrinter(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 1

    .line 63
    iget-object v0, p0, Lepson/print/ecclient/EpsonConnectAccess;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    invoke-virtual {v0, p1, p2, p3}, Lepson/print/ecclient/EcClientLib;->RegPrinter(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public StartPrint(II)I
    .locals 1

    .line 119
    iget-object v0, p0, Lepson/print/ecclient/EpsonConnectAccess;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    invoke-virtual {v0, p1, p2}, Lepson/print/ecclient/EcClientLib;->StartPrint(II)I

    move-result p1

    return p1
.end method

.method public Terminate()V
    .locals 1

    .line 33
    iget-object v0, p0, Lepson/print/ecclient/EpsonConnectAccess;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    invoke-virtual {v0}, Lepson/print/ecclient/EcClientLib;->Terminate()V

    return-void
.end method

.method public UploadFile(Ljava/lang/String;II)I
    .locals 1

    .line 97
    iget-object v0, p0, Lepson/print/ecclient/EpsonConnectAccess;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    invoke-virtual {v0, p1, p2, p3}, Lepson/print/ecclient/EcClientLib;->UploadFile(Ljava/lang/String;II)I

    move-result p1

    return p1
.end method

.method public cancel()V
    .locals 1

    .line 124
    iget-object v0, p0, Lepson/print/ecclient/EpsonConnectAccess;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    invoke-virtual {v0}, Lepson/print/ecclient/EcClientLib;->cancel()V

    return-void
.end method

.method public getClientId()Ljava/lang/String;
    .locals 1

    .line 182
    iget-object v0, p0, Lepson/print/ecclient/EpsonConnectAccess;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    invoke-virtual {v0}, Lepson/print/ecclient/EcClientLib;->clientId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method internalLogin(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 1

    .line 38
    iget-object v0, p0, Lepson/print/ecclient/EpsonConnectAccess;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    invoke-virtual {v0, p1, p2, p3}, Lepson/print/ecclient/EcClientLib;->Login(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public libGetPrinterInfo()Lepson/print/ecclient/EpsonConnectAccess$RemotePrinterInfo;
    .locals 1

    .line 177
    invoke-virtual {p0}, Lepson/print/ecclient/EpsonConnectAccess;->newLibGetPrinterInfo()Lepson/print/ecclient/EpsonConnectAccess$RemotePrinterInfo;

    move-result-object v0

    return-object v0
.end method

.method public logout()I
    .locals 1

    .line 58
    iget-object v0, p0, Lepson/print/ecclient/EpsonConnectAccess;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    invoke-virtual {v0}, Lepson/print/ecclient/EcClientLib;->Logout()I

    move-result v0

    return v0
.end method

.method public newLibGetPrinterInfo()Lepson/print/ecclient/EpsonConnectAccess$RemotePrinterInfo;
    .locals 2

    .line 163
    iget-object v0, p0, Lepson/print/ecclient/EpsonConnectAccess;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    invoke-virtual {v0}, Lepson/print/ecclient/EcClientLib;->GetPrinterInfo()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 167
    :cond_0
    new-instance v0, Lepson/print/ecclient/EpsonConnectAccess$RemotePrinterInfo;

    invoke-direct {v0}, Lepson/print/ecclient/EpsonConnectAccess$RemotePrinterInfo;-><init>()V

    .line 168
    iget-object v1, p0, Lepson/print/ecclient/EpsonConnectAccess;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    invoke-virtual {v1}, Lepson/print/ecclient/EcClientLib;->deviceId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lepson/print/ecclient/EpsonConnectAccess$RemotePrinterInfo;->mDeviceId:Ljava/lang/String;

    .line 169
    iget-object v1, p0, Lepson/print/ecclient/EpsonConnectAccess;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    invoke-virtual {v1}, Lepson/print/ecclient/EcClientLib;->serialNumber()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lepson/print/ecclient/EpsonConnectAccess$RemotePrinterInfo;->mSerialNumber:Ljava/lang/String;

    .line 170
    iget-object v1, p0, Lepson/print/ecclient/EpsonConnectAccess;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    invoke-virtual {v1}, Lepson/print/ecclient/EcClientLib;->printerName2()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lepson/print/ecclient/EpsonConnectAccess$RemotePrinterInfo;->mPrinterName:Ljava/lang/String;

    return-object v0
.end method

.method public oldLibGetPrinterInfo()Lepson/print/ecclient/EpsonConnectAccess$RemotePrinterInfo;
    .locals 2

    .line 142
    invoke-virtual {p0}, Lepson/print/ecclient/EpsonConnectAccess;->GetCapability()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 146
    :cond_0
    new-instance v0, Lepson/print/ecclient/EpsonConnectAccess$RemotePrinterInfo;

    invoke-direct {v0}, Lepson/print/ecclient/EpsonConnectAccess$RemotePrinterInfo;-><init>()V

    const-string v1, ""

    .line 148
    iput-object v1, v0, Lepson/print/ecclient/EpsonConnectAccess$RemotePrinterInfo;->mSerialNumber:Ljava/lang/String;

    .line 149
    iget-object v1, p0, Lepson/print/ecclient/EpsonConnectAccess;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    invoke-virtual {v1}, Lepson/print/ecclient/EcClientLib;->printerName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lepson/print/ecclient/EpsonConnectAccess$RemotePrinterInfo;->mPrinterName:Ljava/lang/String;

    .line 151
    iget-object v1, v0, Lepson/print/ecclient/EpsonConnectAccess$RemotePrinterInfo;->mPrinterName:Ljava/lang/String;

    iput-object v1, v0, Lepson/print/ecclient/EpsonConnectAccess$RemotePrinterInfo;->mDeviceId:Ljava/lang/String;

    return-object v0
.end method

.method public resetCancel()V
    .locals 1

    .line 129
    iget-object v0, p0, Lepson/print/ecclient/EpsonConnectAccess;->mEcClientLib:Lepson/print/ecclient/EcClientLib;

    invoke-virtual {v0}, Lepson/print/ecclient/EcClientLib;->resetCancel()V

    return-void
.end method

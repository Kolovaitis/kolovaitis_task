.class Lepson/print/IprintHomeMenuLogging;
.super Ljava/lang/Object;
.source "IprintHomeMenuLogging.java"


# static fields
.field private static sClassNameToButtonIdMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final CAMERADECOPY_APPID:Ljava/lang/String;

.field private final CARDPRINT:Ljava/lang/String;

.field private final CREATIVE_PRINT_APPID:Ljava/lang/String;

.field private final MULTIROLLPRINT:Ljava/lang/String;

.field private final NENGA_SEC:Ljava/lang/String;

.field private final THREE_D_PRINT_APPID:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 30
    invoke-static {}, Lepson/print/IprintHomeMenuLogging;->setMap()V

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "Creative"

    .line 20
    iput-object v0, p0, Lepson/print/IprintHomeMenuLogging;->CREATIVE_PRINT_APPID:Ljava/lang/String;

    const-string v0, "3DFramePrint"

    .line 21
    iput-object v0, p0, Lepson/print/IprintHomeMenuLogging;->THREE_D_PRINT_APPID:Ljava/lang/String;

    const-string v0, "CameraCopy"

    .line 22
    iput-object v0, p0, Lepson/print/IprintHomeMenuLogging;->CAMERADECOPY_APPID:Ljava/lang/String;

    const-string v0, "MultiRollPrint"

    .line 23
    iput-object v0, p0, Lepson/print/IprintHomeMenuLogging;->MULTIROLLPRINT:Ljava/lang/String;

    const-string v0, "CardPrint"

    .line 24
    iput-object v0, p0, Lepson/print/IprintHomeMenuLogging;->CARDPRINT:Ljava/lang/String;

    const-string v0, "Nenga"

    .line 25
    iput-object v0, p0, Lepson/print/IprintHomeMenuLogging;->NENGA_SEC:Ljava/lang/String;

    return-void
.end method

.method private static declared-synchronized setMap()V
    .locals 4

    const-class v0, Lepson/print/IprintHomeMenuLogging;

    monitor-enter v0

    .line 38
    :try_start_0
    sget-object v1, Lepson/print/IprintHomeMenuLogging;->sClassNameToButtonIdMap:Ljava/util/HashMap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    .line 39
    monitor-exit v0

    return-void

    .line 42
    :cond_0
    :try_start_1
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, Lepson/print/IprintHomeMenuLogging;->sClassNameToButtonIdMap:Ljava/util/HashMap;

    .line 43
    sget-object v1, Lepson/print/IprintHomeMenuLogging;->sClassNameToButtonIdMap:Ljava/util/HashMap;

    const-class v2, Lepson/print/activity/AFolderPhoto;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Home-Photo"

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    sget-object v1, Lepson/print/IprintHomeMenuLogging;->sClassNameToButtonIdMap:Ljava/util/HashMap;

    const-class v2, Lepson/print/fileBrower;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Home-Document"

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    sget-object v1, Lepson/print/IprintHomeMenuLogging;->sClassNameToButtonIdMap:Ljava/util/HashMap;

    const-class v2, Lepson/server/screens/StorageServer;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Home-OnlineStorage"

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    sget-object v1, Lepson/print/IprintHomeMenuLogging;->sClassNameToButtonIdMap:Ljava/util/HashMap;

    const-class v2, Lepson/print/WebviewActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Home-Web"

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    sget-object v1, Lepson/print/IprintHomeMenuLogging;->sClassNameToButtonIdMap:Ljava/util/HashMap;

    const-class v2, Lepson/scan/activity/ScanActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Home-Scan"

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    sget-object v1, Lepson/print/IprintHomeMenuLogging;->sClassNameToButtonIdMap:Ljava/util/HashMap;

    const-class v2, Lepson/print/copy/CopyActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Home-Copy"

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    sget-object v1, Lepson/print/IprintHomeMenuLogging;->sClassNameToButtonIdMap:Ljava/util/HashMap;

    const-class v2, Lcom/epson/cameracopy/ui/CameraPreviewActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Home-CameraCopy"

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    sget-object v1, Lepson/print/IprintHomeMenuLogging;->sClassNameToButtonIdMap:Ljava/util/HashMap;

    const-class v2, Lcom/epson/memcardacc/MemcardTop;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Home-MemoryCardAccess"

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    sget-object v1, Lepson/print/IprintHomeMenuLogging;->sClassNameToButtonIdMap:Ljava/util/HashMap;

    const-class v2, Lcom/epson/memcardacc/MemcardPhotocopyTop;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Home-PhotoTransfer"

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 61
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method


# virtual methods
.method public countUpInstalledOtherApps(Landroid/content/Context;Lepson/print/IconTextArrayItem;)V
    .locals 1

    .line 72
    iget-object p1, p2, Lepson/print/IconTextArrayItem;->appId:Ljava/lang/String;

    const-string v0, "Creative"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    .line 76
    :cond_0
    iget-object p1, p2, Lepson/print/IconTextArrayItem;->appId:Ljava/lang/String;

    const-string v0, "3DFramePrint"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_0

    .line 81
    :cond_1
    iget-object p1, p2, Lepson/print/IconTextArrayItem;->appId:Ljava/lang/String;

    const-string v0, "CameraCopy"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    .line 87
    :cond_2
    iget-object p1, p2, Lepson/print/IconTextArrayItem;->appId:Ljava/lang/String;

    const-string v0, "MultiRollPrint"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_3

    goto :goto_0

    .line 92
    :cond_3
    iget-object p1, p2, Lepson/print/IconTextArrayItem;->appId:Ljava/lang/String;

    const-string v0, "CardPrint"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_4

    goto :goto_0

    .line 97
    :cond_4
    iget-object p1, p2, Lepson/print/IconTextArrayItem;->appId:Ljava/lang/String;

    const-string p2, "Nenga"

    invoke-virtual {p1, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    :goto_0
    return-void
.end method

.method public countUpNotInstalledOtherApps(Landroid/content/Context;Lepson/print/IconTextArrayItem;)V
    .locals 1

    .line 108
    iget-object p1, p2, Lepson/print/IconTextArrayItem;->appId:Ljava/lang/String;

    const-string v0, "Creative"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    .line 110
    :cond_0
    iget-object p1, p2, Lepson/print/IconTextArrayItem;->appId:Ljava/lang/String;

    const-string v0, "3DFramePrint"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_0

    .line 113
    :cond_1
    iget-object p1, p2, Lepson/print/IconTextArrayItem;->appId:Ljava/lang/String;

    const-string v0, "CameraCopy"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    .line 116
    :cond_2
    iget-object p1, p2, Lepson/print/IconTextArrayItem;->appId:Ljava/lang/String;

    const-string v0, "MultiRollPrint"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_3

    goto :goto_0

    .line 119
    :cond_3
    iget-object p1, p2, Lepson/print/IconTextArrayItem;->appId:Ljava/lang/String;

    const-string v0, "CardPrint"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_4

    goto :goto_0

    .line 122
    :cond_4
    iget-object p1, p2, Lepson/print/IconTextArrayItem;->appId:Ljava/lang/String;

    const-string p2, "Nenga"

    invoke-virtual {p1, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    :goto_0
    return-void
.end method

.method public countupMoreApp(Landroid/content/Context;)V
    .locals 0

    return-void
.end method

.method public sendUiTapData(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 64
    sget-object v0, Lepson/print/IprintHomeMenuLogging;->sClassNameToButtonIdMap:Ljava/util/HashMap;

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    invoke-static {p1, p2}, Lcom/epson/iprint/prtlogger/Analytics;->sendAction(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

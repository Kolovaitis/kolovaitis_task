.class public abstract Lepson/print/ActivityIACommon;
.super Landroid/support/v7/app/AppCompatActivity;
.source "ActivityIACommon.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .line 58
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-eq v0, v1, :cond_0

    .line 66
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1

    .line 60
    :cond_0
    invoke-virtual {p0}, Lepson/print/ActivityIACommon;->onBackPressed()V

    const/4 p1, 0x1

    return p1
.end method

.method protected setActionBar(IZ)V
    .locals 1

    .line 22
    invoke-virtual {p0}, Lepson/print/ActivityIACommon;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1, p2}, Lepson/print/ActivityIACommon;->setActionBar(Ljava/lang/String;Z)V

    return-void
.end method

.method protected setActionBar(Ljava/lang/String;Z)V
    .locals 0

    .line 35
    invoke-virtual {p0, p1}, Lepson/print/ActivityIACommon;->setTitle(Ljava/lang/CharSequence;)V

    const p1, 0x7f080223

    .line 38
    invoke-virtual {p0, p1}, Lepson/print/ActivityIACommon;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/support/v7/widget/Toolbar;

    .line 39
    invoke-virtual {p0, p1}, Lepson/print/ActivityIACommon;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    if-eqz p2, :cond_0

    .line 43
    invoke-virtual {p0}, Lepson/print/ActivityIACommon;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object p1

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    :cond_0
    return-void
.end method

.class public Lepson/print/MenudataAdapter;
.super Ljava/lang/Object;
.source "MenudataAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/MenudataAdapter$MenuDataCapability;,
        Lepson/print/MenudataAdapter$MenuDataFunction;,
        Lepson/print/MenudataAdapter$MenuDataApp;
    }
.end annotation


# instance fields
.field apps:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lepson/print/MenudataAdapter$MenuDataApp;",
            ">;"
        }
    .end annotation
.end field

.field capabilities:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lepson/print/MenudataAdapter$MenuDataCapability;",
            ">;"
        }
    .end annotation
.end field

.field defaultCapability:Lepson/print/MenudataAdapter$MenuDataCapability;

.field functions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lepson/print/MenudataAdapter$MenuDataFunction;",
            ">;"
        }
    .end annotation
.end field

.field json:Lorg/json/JSONObject;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 30
    iput-object v0, p0, Lepson/print/MenudataAdapter;->defaultCapability:Lepson/print/MenudataAdapter$MenuDataCapability;

    return-void
.end method

.method private FilterApps(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 289
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 290
    iget-object v1, p0, Lepson/print/MenudataAdapter;->apps:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lepson/print/MenudataAdapter$MenuDataApp;

    .line 291
    iget-object v3, v2, Lepson/print/MenudataAdapter$MenuDataApp;->appID:Ljava/lang/String;

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 292
    iget-object v2, v2, Lepson/print/MenudataAdapter$MenuDataApp;->appID:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method private FilterFunctions(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 304
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 305
    iget-object v1, p0, Lepson/print/MenudataAdapter;->functions:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lepson/print/MenudataAdapter$MenuDataFunction;

    .line 306
    iget-object v3, v2, Lepson/print/MenudataAdapter$MenuDataFunction;->functionID:Ljava/lang/String;

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 307
    iget-object v2, v2, Lepson/print/MenudataAdapter$MenuDataFunction;->functionID:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method


# virtual methods
.method public JSONArrayToString(Lorg/json/JSONArray;)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONArray;",
            ")",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 320
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    .line 321
    :goto_0
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 323
    :try_start_0
    invoke-virtual {p1, v1}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    const-string v2, "JSON"

    const-string v3, "cannot JSONArrayToString"

    .line 326
    invoke-static {v2, v3}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public MenuListfordeviceID(Ljava/lang/String;ILandroid/content/Context;)Ljava/util/ArrayList;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList<",
            "Lepson/print/IconTextArrayItem;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p0

    move/from16 v1, p2

    .line 337
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 338
    invoke-virtual/range {p0 .. p1}, Lepson/print/MenudataAdapter;->capabilityForDeviceID(Ljava/lang/String;)Lepson/print/MenudataAdapter$MenuDataCapability;

    move-result-object v3

    .line 339
    invoke-virtual/range {p3 .. p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 341
    iget-object v5, v3, Lepson/print/MenudataAdapter$MenuDataCapability;->appIDs:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Lepson/print/MenudataAdapter;->appsForAppIDs(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v5

    const/4 v7, 0x0

    .line 342
    :goto_0
    iget-object v8, v3, Lepson/print/MenudataAdapter$MenuDataCapability;->appIDs:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v7, v8, :cond_8

    .line 343
    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lepson/print/MenudataAdapter$MenuDataApp;

    iget-object v9, v0, Lepson/print/MenudataAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v8, v9}, Lepson/print/MenudataAdapter$MenuDataApp;->isInstalled(Landroid/content/Context;)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-eqz v8, :cond_7

    .line 344
    invoke-virtual/range {p0 .. p1}, Lepson/print/MenudataAdapter;->capabilityForDeviceID(Ljava/lang/String;)Lepson/print/MenudataAdapter$MenuDataCapability;

    move-result-object v8

    const-string v9, "iPrint"

    .line 345
    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lepson/print/MenudataAdapter$MenuDataApp;

    iget-object v10, v10, Lepson/print/MenudataAdapter$MenuDataApp;->appID:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    const/4 v10, 0x2

    const/4 v11, 0x1

    if-eqz v9, :cond_2

    .line 346
    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lepson/print/MenudataAdapter$MenuDataApp;

    iget-object v9, v9, Lepson/print/MenudataAdapter$MenuDataApp;->appfunctions:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_0
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_6

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lepson/print/MenudataAdapter$MenuDataFunction;

    .line 348
    iget-object v13, v8, Lepson/print/MenudataAdapter$MenuDataCapability;->functionIDs:Ljava/util/ArrayList;

    iget-object v14, v12, Lepson/print/MenudataAdapter$MenuDataFunction;->functionID:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 350
    new-instance v13, Lepson/print/IconTextArrayItem;

    invoke-direct {v13}, Lepson/print/IconTextArrayItem;-><init>()V

    .line 351
    iget-object v14, v12, Lepson/print/MenudataAdapter$MenuDataFunction;->functionID:Ljava/lang/String;

    invoke-virtual {v0, v14}, Lepson/print/MenudataAdapter;->functionForFunctionID(Ljava/lang/String;)Lepson/print/MenudataAdapter$MenuDataFunction;

    move-result-object v14

    iget-object v14, v14, Lepson/print/MenudataAdapter$MenuDataFunction;->appID:Ljava/lang/String;

    iput-object v14, v13, Lepson/print/IconTextArrayItem;->appId:Ljava/lang/String;

    .line 352
    iget-object v14, v12, Lepson/print/MenudataAdapter$MenuDataFunction;->functionID:Ljava/lang/String;

    invoke-virtual {v0, v14}, Lepson/print/MenudataAdapter;->functionForFunctionID(Ljava/lang/String;)Lepson/print/MenudataAdapter$MenuDataFunction;

    move-result-object v14

    iget-object v14, v14, Lepson/print/MenudataAdapter$MenuDataFunction;->name:Ljava/lang/String;

    const-string v15, "string"

    .line 353
    invoke-virtual/range {p3 .. p3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v14, v15, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v6

    iput v6, v13, Lepson/print/IconTextArrayItem;->menuId:I

    .line 354
    iget-object v6, v12, Lepson/print/MenudataAdapter$MenuDataFunction;->functionID:Ljava/lang/String;

    invoke-virtual {v0, v6}, Lepson/print/MenudataAdapter;->functionForFunctionID(Ljava/lang/String;)Lepson/print/MenudataAdapter$MenuDataFunction;

    move-result-object v6

    iget-object v6, v6, Lepson/print/MenudataAdapter$MenuDataFunction;->ClassName:Ljava/lang/String;

    iput-object v6, v13, Lepson/print/IconTextArrayItem;->ClassName:Ljava/lang/String;

    .line 355
    iget-object v6, v12, Lepson/print/MenudataAdapter$MenuDataFunction;->functionID:Ljava/lang/String;

    invoke-virtual {v0, v6}, Lepson/print/MenudataAdapter;->functionForFunctionID(Ljava/lang/String;)Lepson/print/MenudataAdapter$MenuDataFunction;

    move-result-object v6

    iget-object v6, v6, Lepson/print/MenudataAdapter$MenuDataFunction;->PackageName:Ljava/lang/String;

    iput-object v6, v13, Lepson/print/IconTextArrayItem;->PackageName:Ljava/lang/String;

    .line 356
    iget-object v6, v12, Lepson/print/MenudataAdapter$MenuDataFunction;->functionID:Ljava/lang/String;

    invoke-virtual {v0, v6}, Lepson/print/MenudataAdapter;->functionForFunctionID(Ljava/lang/String;)Lepson/print/MenudataAdapter$MenuDataFunction;

    move-result-object v6

    iget-object v6, v6, Lepson/print/MenudataAdapter$MenuDataFunction;->isRemoteAvailable:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    iput-boolean v6, v13, Lepson/print/IconTextArrayItem;->isRemoteAvailable:Z

    .line 357
    iput-boolean v11, v13, Lepson/print/IconTextArrayItem;->isInstall:Z

    .line 358
    iget-object v6, v12, Lepson/print/MenudataAdapter$MenuDataFunction;->functionID:Ljava/lang/String;

    invoke-virtual {v0, v6}, Lepson/print/MenudataAdapter;->functionForFunctionID(Ljava/lang/String;)Lepson/print/MenudataAdapter$MenuDataFunction;

    move-result-object v6

    iget-object v6, v6, Lepson/print/MenudataAdapter$MenuDataFunction;->key:Ljava/lang/String;

    iput-object v6, v13, Lepson/print/IconTextArrayItem;->key:Ljava/lang/String;

    .line 360
    iget-object v6, v12, Lepson/print/MenudataAdapter$MenuDataFunction;->functionID:Ljava/lang/String;

    invoke-virtual {v0, v6}, Lepson/print/MenudataAdapter;->functionForFunctionID(Ljava/lang/String;)Lepson/print/MenudataAdapter$MenuDataFunction;

    move-result-object v6

    iget-object v6, v6, Lepson/print/MenudataAdapter$MenuDataFunction;->icon:Ljava/lang/String;

    .line 361
    iput-boolean v11, v13, Lepson/print/IconTextArrayItem;->showMenu:Z

    if-ne v1, v10, :cond_1

    .line 365
    iget-object v14, v13, Lepson/print/IconTextArrayItem;->appId:Ljava/lang/String;

    const-string v15, "iPrint"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_1

    iget-boolean v14, v13, Lepson/print/IconTextArrayItem;->isRemoteAvailable:Z

    if-nez v14, :cond_1

    .line 366
    iget-object v6, v12, Lepson/print/MenudataAdapter$MenuDataFunction;->functionID:Ljava/lang/String;

    invoke-virtual {v0, v6}, Lepson/print/MenudataAdapter;->functionForFunctionID(Ljava/lang/String;)Lepson/print/MenudataAdapter$MenuDataFunction;

    move-result-object v6

    iget-object v6, v6, Lepson/print/MenudataAdapter$MenuDataFunction;->icon:Ljava/lang/String;

    const-string v12, "_disabled"

    invoke-virtual {v6, v12}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/4 v12, 0x0

    .line 367
    iput-boolean v12, v13, Lepson/print/IconTextArrayItem;->showMenu:Z

    :cond_1
    const-string v12, "drawable"

    .line 371
    invoke-virtual/range {p3 .. p3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v4, v6, v12, v14}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v6

    iput v6, v13, Lepson/print/IconTextArrayItem;->imageID:I

    .line 372
    invoke-virtual {v2, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 377
    :cond_2
    new-instance v6, Lepson/print/IconTextArrayItem;

    invoke-direct {v6}, Lepson/print/IconTextArrayItem;-><init>()V

    .line 378
    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lepson/print/MenudataAdapter$MenuDataApp;

    iget-object v8, v8, Lepson/print/MenudataAdapter$MenuDataApp;->appID:Ljava/lang/String;

    iput-object v8, v6, Lepson/print/IconTextArrayItem;->appId:Ljava/lang/String;

    .line 379
    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lepson/print/MenudataAdapter$MenuDataApp;

    iget-object v8, v8, Lepson/print/MenudataAdapter$MenuDataApp;->icon:Ljava/lang/String;

    .line 380
    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lepson/print/MenudataAdapter$MenuDataApp;

    iget-object v9, v9, Lepson/print/MenudataAdapter$MenuDataApp;->name:Ljava/lang/String;

    const-string v12, "string"

    .line 381
    invoke-virtual/range {p3 .. p3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v4, v9, v12, v13}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v9

    iput v9, v6, Lepson/print/IconTextArrayItem;->menuId:I

    .line 382
    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lepson/print/MenudataAdapter$MenuDataApp;

    iget-object v9, v9, Lepson/print/MenudataAdapter$MenuDataApp;->ClassName:Ljava/lang/String;

    iput-object v9, v6, Lepson/print/IconTextArrayItem;->ClassName:Ljava/lang/String;

    .line 383
    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lepson/print/MenudataAdapter$MenuDataApp;

    iget-object v9, v9, Lepson/print/MenudataAdapter$MenuDataApp;->PackageName:Ljava/lang/String;

    iput-object v9, v6, Lepson/print/IconTextArrayItem;->PackageName:Ljava/lang/String;

    .line 384
    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lepson/print/MenudataAdapter$MenuDataApp;

    iget-boolean v9, v9, Lepson/print/MenudataAdapter$MenuDataApp;->isRemoteAvailable:Z

    iput-boolean v9, v6, Lepson/print/IconTextArrayItem;->isRemoteAvailable:Z

    .line 385
    iput-boolean v11, v6, Lepson/print/IconTextArrayItem;->isInstall:Z

    const-string v9, "_notinstalled"

    const-string v11, ""

    .line 386
    invoke-virtual {v8, v9, v11}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "0"

    .line 390
    iput-object v9, v6, Lepson/print/IconTextArrayItem;->key:Ljava/lang/String;

    const/4 v9, 0x3

    if-eq v1, v9, :cond_3

    if-ne v1, v10, :cond_5

    .line 394
    :cond_3
    iget-object v9, v6, Lepson/print/IconTextArrayItem;->appId:Ljava/lang/String;

    const-string v10, "Creative"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_4

    iget-object v9, v6, Lepson/print/IconTextArrayItem;->appId:Ljava/lang/String;

    const-string v10, "3DFramePrint"

    .line 395
    invoke-virtual {v9, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_4

    iget-object v9, v6, Lepson/print/IconTextArrayItem;->appId:Ljava/lang/String;

    const-string v10, "CameraCopy"

    .line 396
    invoke-virtual {v9, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_4

    iget-object v9, v6, Lepson/print/IconTextArrayItem;->appId:Ljava/lang/String;

    const-string v10, "MultiRollPrint"

    .line 397
    invoke-virtual {v9, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_4

    iget-object v9, v6, Lepson/print/IconTextArrayItem;->appId:Ljava/lang/String;

    const-string v10, "CardPrint"

    .line 398
    invoke-virtual {v9, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_4

    iget-object v9, v6, Lepson/print/IconTextArrayItem;->appId:Ljava/lang/String;

    const-string v10, "Nenga"

    .line 399
    invoke-virtual {v9, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_5

    :cond_4
    const-string v9, "_disabled"

    .line 401
    invoke-virtual {v8, v9}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    .line 402
    iput-boolean v9, v6, Lepson/print/IconTextArrayItem;->showMenu:Z

    :cond_5
    const-string v9, "drawable"

    .line 406
    invoke-virtual/range {p3 .. p3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v8, v9, v10}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v8

    iput v8, v6, Lepson/print/IconTextArrayItem;->imageID:I

    .line 407
    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_6
    const/4 v8, 0x0

    goto :goto_2

    .line 412
    :cond_7
    new-instance v6, Lepson/print/IconTextArrayItem;

    invoke-direct {v6}, Lepson/print/IconTextArrayItem;-><init>()V

    .line 413
    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lepson/print/MenudataAdapter$MenuDataApp;

    iget-object v8, v8, Lepson/print/MenudataAdapter$MenuDataApp;->appID:Ljava/lang/String;

    iput-object v8, v6, Lepson/print/IconTextArrayItem;->appId:Ljava/lang/String;

    .line 414
    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lepson/print/MenudataAdapter$MenuDataApp;

    iget-object v8, v8, Lepson/print/MenudataAdapter$MenuDataApp;->icon:Ljava/lang/String;

    const-string v9, "drawable"

    .line 415
    invoke-virtual/range {p3 .. p3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v8, v9, v10}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v8

    iput v8, v6, Lepson/print/IconTextArrayItem;->imageID:I

    .line 416
    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lepson/print/MenudataAdapter$MenuDataApp;

    iget-object v8, v8, Lepson/print/MenudataAdapter$MenuDataApp;->name:Ljava/lang/String;

    const-string v9, "string"

    .line 417
    invoke-virtual/range {p3 .. p3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v8, v9, v10}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v8

    iput v8, v6, Lepson/print/IconTextArrayItem;->menuId:I

    .line 418
    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lepson/print/MenudataAdapter$MenuDataApp;

    iget-object v8, v8, Lepson/print/MenudataAdapter$MenuDataApp;->ClassName:Ljava/lang/String;

    iput-object v8, v6, Lepson/print/IconTextArrayItem;->ClassName:Ljava/lang/String;

    .line 419
    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lepson/print/MenudataAdapter$MenuDataApp;

    iget-object v8, v8, Lepson/print/MenudataAdapter$MenuDataApp;->PackageName:Ljava/lang/String;

    iput-object v8, v6, Lepson/print/IconTextArrayItem;->PackageName:Ljava/lang/String;

    .line 420
    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lepson/print/MenudataAdapter$MenuDataApp;

    iget-boolean v8, v8, Lepson/print/MenudataAdapter$MenuDataApp;->isRemoteAvailable:Z

    iput-boolean v8, v6, Lepson/print/IconTextArrayItem;->isRemoteAvailable:Z

    .line 421
    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lepson/print/MenudataAdapter$MenuDataApp;

    iget-object v8, v8, Lepson/print/MenudataAdapter$MenuDataApp;->appDescription:Ljava/lang/String;

    const-string v9, "string"

    .line 422
    invoke-virtual/range {p3 .. p3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v8, v9, v10}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v8

    iput v8, v6, Lepson/print/IconTextArrayItem;->AppDiscId:I

    const/4 v8, 0x0

    .line 423
    iput-boolean v8, v6, Lepson/print/IconTextArrayItem;->isInstall:Z

    .line 424
    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lepson/print/MenudataAdapter$MenuDataApp;

    iget-object v9, v9, Lepson/print/MenudataAdapter$MenuDataApp;->googlePlayURL:Ljava/lang/String;

    iput-object v9, v6, Lepson/print/IconTextArrayItem;->googleStoreUrl:Ljava/lang/String;

    .line 425
    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_2
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_0

    :cond_8
    return-object v2
.end method

.method public appsForAppIDs(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/ArrayList<",
            "Lepson/print/MenudataAdapter$MenuDataApp;",
            ">;"
        }
    .end annotation

    const-string v0, "appsForAppIDs"

    const-string v1, "OK"

    .line 546
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 547
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    .line 548
    :goto_0
    iget-object v2, p0, Lepson/print/MenudataAdapter;->apps:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 549
    iget-object v2, p0, Lepson/print/MenudataAdapter;->apps:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lepson/print/MenudataAdapter$MenuDataApp;

    iget-object v2, v2, Lepson/print/MenudataAdapter$MenuDataApp;->appID:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 550
    iget-object v2, p0, Lepson/print/MenudataAdapter;->apps:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public capabilityForDeviceID(Ljava/lang/String;)Lepson/print/MenudataAdapter$MenuDataCapability;
    .locals 2

    const-string v0, "capabilityForSerialHeader"

    const-string v1, "OK"

    .line 495
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 497
    :goto_0
    iget-object v1, p0, Lepson/print/MenudataAdapter;->capabilities:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 498
    iget-object v1, p0, Lepson/print/MenudataAdapter;->capabilities:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lepson/print/MenudataAdapter$MenuDataCapability;

    iget-object v1, v1, Lepson/print/MenudataAdapter$MenuDataCapability;->deviceIDs:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 499
    iget-object p1, p0, Lepson/print/MenudataAdapter;->capabilities:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lepson/print/MenudataAdapter$MenuDataCapability;

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_1
    if-nez p1, :cond_2

    .line 505
    iget-object p1, p0, Lepson/print/MenudataAdapter;->defaultCapability:Lepson/print/MenudataAdapter$MenuDataCapability;

    :cond_2
    return-object p1
.end method

.method public functionForFunctionID(Ljava/lang/String;)Lepson/print/MenudataAdapter$MenuDataFunction;
    .locals 2

    const/4 v0, 0x0

    .line 597
    :goto_0
    iget-object v1, p0, Lepson/print/MenudataAdapter;->functions:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 598
    iget-object v1, p0, Lepson/print/MenudataAdapter;->functions:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lepson/print/MenudataAdapter$MenuDataFunction;

    iget-object v1, v1, Lepson/print/MenudataAdapter$MenuDataFunction;->functionID:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 599
    iget-object p1, p0, Lepson/print/MenudataAdapter;->functions:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lepson/print/MenudataAdapter$MenuDataFunction;

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_1
    return-object p1
.end method

.method public functionsForFunctionIDs(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/ArrayList<",
            "Lepson/print/MenudataAdapter$MenuDataFunction;",
            ">;"
        }
    .end annotation

    const-string v0, "functionsForFunctionIDs"

    const-string v1, "OK"

    .line 579
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 580
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    .line 581
    :goto_0
    iget-object v2, p0, Lepson/print/MenudataAdapter;->functions:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 582
    iget-object v2, p0, Lepson/print/MenudataAdapter;->functions:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lepson/print/MenudataAdapter$MenuDataFunction;

    iget-object v2, v2, Lepson/print/MenudataAdapter$MenuDataFunction;->functionID:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 583
    iget-object v2, p0, Lepson/print/MenudataAdapter;->functions:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method getExcludedlist(Lorg/json/JSONObject;)Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            ")",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    :try_start_0
    const-string v0, "FunctionIDsExcluded"

    .line 256
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object p1

    invoke-virtual {p0, p1}, Lepson/print/MenudataAdapter;->JSONArrayToString(Lorg/json/JSONArray;)Ljava/util/ArrayList;

    move-result-object p1

    invoke-direct {p0, p1}, Lepson/print/MenudataAdapter;->FilterFunctions(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object p1
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public getJSONFile()Lorg/json/JSONObject;
    .locals 6

    const-string v0, "getJSONFile"

    const-string v1, "OK"

    .line 65
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    iget-object v0, p0, Lepson/print/MenudataAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 67
    invoke-virtual {v0}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    const-string v1, ""

    const/4 v2, 0x0

    .line 73
    :try_start_0
    invoke-static {}, Lepson/print/IprintApplication;->isReleaseUnlimited()Z

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    const-string v3, "menudata/MenuDataUnlimited.json"

    .line 74
    invoke-virtual {v0, v3}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    goto :goto_0

    :cond_0
    const-string v3, "menudata/MenuData.json"

    .line 76
    invoke-virtual {v0, v3}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    :goto_0
    const-string v3, "jsonRead"

    .line 78
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "jsonObject"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 84
    new-instance v3, Ljava/io/InputStreamReader;

    invoke-direct {v3, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 85
    new-instance v4, Ljava/io/BufferedReader;

    invoke-direct {v4, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 86
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 89
    :goto_1
    :try_start_1
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 90
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 92
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 93
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    :catch_0
    const-string v0, "jsonRead"

    const-string v3, "error occur"

    .line 96
    invoke-static {v0, v3}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    :goto_2
    :try_start_2
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_3

    :catch_1
    const-string v0, "jsonRead"

    const-string v1, "error occur"

    .line 103
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v2

    :goto_3
    return-object v0

    :catch_2
    const-string v0, "jsonRead"

    const-string v1, "cannot read"

    .line 81
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method

.method public parseApps(Lorg/json/JSONObject;)Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            ")",
            "Ljava/util/ArrayList<",
            "Lepson/print/MenudataAdapter$MenuDataApp;",
            ">;"
        }
    .end annotation

    const-string v0, "parseApps"

    const-string v1, "Start"

    .line 112
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :try_start_0
    const-string v1, "Apps"

    .line 115
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object p1

    const/4 v1, 0x0

    .line 117
    :goto_0
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 118
    new-instance v2, Lepson/print/MenudataAdapter$MenuDataApp;

    invoke-direct {v2, p0}, Lepson/print/MenudataAdapter$MenuDataApp;-><init>(Lepson/print/MenudataAdapter;)V

    .line 119
    invoke-virtual {p1, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    const-string v4, "AppID"

    .line 121
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lepson/print/MenudataAdapter$MenuDataApp;->appID:Ljava/lang/String;

    const-string v4, "Name"

    .line 122
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lepson/print/MenudataAdapter$MenuDataApp;->name:Ljava/lang/String;

    const-string v4, "Description"

    .line 123
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lepson/print/MenudataAdapter$MenuDataApp;->appDescription:Ljava/lang/String;

    const-string v4, "GooglePlayURL"

    .line 124
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lepson/print/MenudataAdapter$MenuDataApp;->googlePlayURL:Ljava/lang/String;

    const-string v4, "AndroidPackageName"

    .line 125
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lepson/print/MenudataAdapter$MenuDataApp;->PackageName:Ljava/lang/String;

    const-string v4, "AndroidClassName"

    .line 126
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lepson/print/MenudataAdapter$MenuDataApp;->ClassName:Ljava/lang/String;

    const-string v4, "AndroidIconName"

    .line 127
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lepson/print/MenudataAdapter$MenuDataApp;->icon:Ljava/lang/String;

    const-string v4, "IsRemoteAvailable"

    .line 128
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    iput-boolean v3, v2, Lepson/print/MenudataAdapter$MenuDataApp;->isRemoteAvailable:Z

    .line 129
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, v2, Lepson/print/MenudataAdapter$MenuDataApp;->appfunctions:Ljava/util/ArrayList;

    .line 131
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :catch_0
    const-string p1, "JSON"

    const-string v1, "cannnot parseApps"

    .line 135
    invoke-static {p1, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string p1, "parseApps"

    const-string v1, "OK"

    .line 137
    invoke-static {p1, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public parseCapabilities(Lorg/json/JSONObject;)Ljava/util/ArrayList;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            ")",
            "Ljava/util/ArrayList<",
            "Lepson/print/MenudataAdapter$MenuDataCapability;",
            ">;"
        }
    .end annotation

    const-string v0, "parseCapabilities"

    const-string v1, "Start"

    .line 178
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    .line 182
    :try_start_0
    iget-object v2, p0, Lepson/print/MenudataAdapter;->defaultCapability:Lepson/print/MenudataAdapter$MenuDataCapability;

    if-nez v2, :cond_0

    const-string p1, "parseCapabilities"

    const-string v0, "not set defaultcapability"

    .line 183
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-object v1

    :cond_0
    const-string v2, "Capabilities"

    .line 186
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object p1

    const/4 v2, 0x0

    .line 188
    :goto_0
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 189
    new-instance v3, Lepson/print/MenudataAdapter$MenuDataCapability;

    invoke-direct {v3, p0}, Lepson/print/MenudataAdapter$MenuDataCapability;-><init>(Lepson/print/MenudataAdapter;)V

    .line 190
    invoke-virtual {p1, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "DeviceIDs"

    .line 192
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    invoke-virtual {p0, v5}, Lepson/print/MenudataAdapter;->JSONArrayToString(Lorg/json/JSONArray;)Ljava/util/ArrayList;

    move-result-object v5

    iput-object v5, v3, Lepson/print/MenudataAdapter$MenuDataCapability;->deviceIDs:Ljava/util/ArrayList;

    const-string v5, "IsPrinter"

    .line 193
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    iput-object v5, v3, Lepson/print/MenudataAdapter$MenuDataCapability;->isPrinter:Ljava/lang/Boolean;

    const-string v5, "IsScanner"

    .line 194
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    iput-object v5, v3, Lepson/print/MenudataAdapter$MenuDataCapability;->isScanner:Ljava/lang/Boolean;

    .line 197
    iget-object v5, v3, Lepson/print/MenudataAdapter$MenuDataCapability;->appIDs:Ljava/util/ArrayList;

    iget-object v6, p0, Lepson/print/MenudataAdapter;->defaultCapability:Lepson/print/MenudataAdapter$MenuDataCapability;

    iget-object v6, v6, Lepson/print/MenudataAdapter$MenuDataCapability;->appIDs:Ljava/util/ArrayList;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 198
    iget-object v5, v3, Lepson/print/MenudataAdapter$MenuDataCapability;->functionIDs:Ljava/util/ArrayList;

    iget-object v6, p0, Lepson/print/MenudataAdapter;->defaultCapability:Lepson/print/MenudataAdapter$MenuDataCapability;

    iget-object v6, v6, Lepson/print/MenudataAdapter$MenuDataCapability;->functionIDs:Ljava/util/ArrayList;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    const-string v5, "AppIDs"

    .line 201
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    invoke-virtual {p0, v5}, Lepson/print/MenudataAdapter;->JSONArrayToString(Lorg/json/JSONArray;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-direct {p0, v5}, Lepson/print/MenudataAdapter;->FilterApps(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v5

    .line 202
    iget-object v6, v3, Lepson/print/MenudataAdapter$MenuDataCapability;->appIDs:Ljava/util/ArrayList;

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    const-string v5, "FunctionIDs"

    .line 203
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    invoke-virtual {p0, v5}, Lepson/print/MenudataAdapter;->JSONArrayToString(Lorg/json/JSONArray;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-direct {p0, v5}, Lepson/print/MenudataAdapter;->FilterFunctions(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v5

    .line 204
    iget-object v6, v3, Lepson/print/MenudataAdapter$MenuDataCapability;->functionIDs:Ljava/util/ArrayList;

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 206
    invoke-virtual {p0, v4}, Lepson/print/MenudataAdapter;->getExcludedlist(Lorg/json/JSONObject;)Ljava/util/ArrayList;

    move-result-object v4

    .line 207
    iget-object v5, v3, Lepson/print/MenudataAdapter$MenuDataCapability;->functionIDs:Ljava/util/ArrayList;

    invoke-virtual {p0, v5, v4}, Lepson/print/MenudataAdapter;->setExcludedlist(Ljava/util/ArrayList;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v4

    iput-object v4, v3, Lepson/print/MenudataAdapter$MenuDataCapability;->functionIDs:Ljava/util/ArrayList;

    .line 209
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const-string p1, "parseCapabilities"

    const-string v1, "OK"

    .line 217
    invoke-static {p1, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    :catch_0
    const-string p1, "JSON"

    const-string v0, "cannnot parseCapabilities"

    .line 214
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method

.method public parseDefaultCapabilities(Lorg/json/JSONObject;)Lepson/print/MenudataAdapter$MenuDataCapability;
    .locals 2

    const-string v0, "parseDefaultCapabilities"

    const-string v1, "Start"

    .line 225
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    new-instance v0, Lepson/print/MenudataAdapter$MenuDataCapability;

    invoke-direct {v0, p0}, Lepson/print/MenudataAdapter$MenuDataCapability;-><init>(Lepson/print/MenudataAdapter;)V

    :try_start_0
    const-string v1, "DefaultCapability"

    .line 229
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p1

    const-string v1, "DeviceIDs"

    .line 231
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    invoke-virtual {p0, v1}, Lepson/print/MenudataAdapter;->JSONArrayToString(Lorg/json/JSONArray;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, v0, Lepson/print/MenudataAdapter$MenuDataCapability;->deviceIDs:Ljava/util/ArrayList;

    const-string v1, "IsPrinter"

    .line 232
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lepson/print/MenudataAdapter$MenuDataCapability;->isPrinter:Ljava/lang/Boolean;

    const-string v1, "IsScanner"

    .line 233
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lepson/print/MenudataAdapter$MenuDataCapability;->isScanner:Ljava/lang/Boolean;

    const-string v1, "AppIDs"

    .line 234
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    invoke-virtual {p0, v1}, Lepson/print/MenudataAdapter;->JSONArrayToString(Lorg/json/JSONArray;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-direct {p0, v1}, Lepson/print/MenudataAdapter;->FilterApps(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v1

    .line 235
    iput-object v1, v0, Lepson/print/MenudataAdapter$MenuDataCapability;->appIDs:Ljava/util/ArrayList;

    const-string v1, "FunctionIDs"

    .line 236
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object p1

    invoke-virtual {p0, p1}, Lepson/print/MenudataAdapter;->JSONArrayToString(Lorg/json/JSONArray;)Ljava/util/ArrayList;

    move-result-object p1

    invoke-direct {p0, p1}, Lepson/print/MenudataAdapter;->FilterFunctions(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object p1

    .line 237
    iput-object p1, v0, Lepson/print/MenudataAdapter$MenuDataCapability;->functionIDs:Ljava/util/ArrayList;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    const-string p1, "parseDefaultCapabilities"

    const-string v1, "OK"

    .line 244
    invoke-static {p1, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    :catch_0
    const-string p1, "JSON"

    const-string v0, "cannnot parseDefaultCapabilities"

    .line 241
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x0

    return-object p1
.end method

.method public parseFunctions(Lorg/json/JSONObject;)Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            ")",
            "Ljava/util/ArrayList<",
            "Lepson/print/MenudataAdapter$MenuDataFunction;",
            ">;"
        }
    .end annotation

    const-string v0, "parseFunctions"

    const-string v1, "Start"

    .line 145
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :try_start_0
    const-string v1, "Functions"

    .line 148
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object p1

    const/4 v1, 0x0

    .line 150
    :goto_0
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 151
    new-instance v2, Lepson/print/MenudataAdapter$MenuDataFunction;

    invoke-direct {v2, p0}, Lepson/print/MenudataAdapter$MenuDataFunction;-><init>(Lepson/print/MenudataAdapter;)V

    .line 152
    invoke-virtual {p1, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    const-string v4, "AppID"

    .line 154
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lepson/print/MenudataAdapter$MenuDataFunction;->appID:Ljava/lang/String;

    const-string v4, "FunctionID"

    .line 155
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lepson/print/MenudataAdapter$MenuDataFunction;->functionID:Ljava/lang/String;

    const-string v4, "Name"

    .line 156
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lepson/print/MenudataAdapter$MenuDataFunction;->name:Ljava/lang/String;

    const-string v4, "AndroidPackageName"

    .line 157
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lepson/print/MenudataAdapter$MenuDataFunction;->PackageName:Ljava/lang/String;

    const-string v4, "AndroidClassName"

    .line 158
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lepson/print/MenudataAdapter$MenuDataFunction;->ClassName:Ljava/lang/String;

    const-string v4, "AndroidIconName"

    .line 159
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lepson/print/MenudataAdapter$MenuDataFunction;->icon:Ljava/lang/String;

    const-string v4, "IsRemoteAvailable"

    .line 160
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, v2, Lepson/print/MenudataAdapter$MenuDataFunction;->isRemoteAvailable:Ljava/lang/Boolean;

    const-string v4, "AndroidKey"

    .line 161
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lepson/print/MenudataAdapter$MenuDataFunction;->key:Ljava/lang/String;

    .line 163
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :catch_0
    const-string p1, "JSON"

    const-string v1, "cannnot parseFunctions"

    .line 167
    invoke-static {p1, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string p1, "parseFunctions"

    const-string v1, "OK"

    .line 169
    invoke-static {p1, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public parseJSONFile()V
    .locals 5

    const-string v0, "parseJSONFile"

    const-string v1, "Start"

    .line 42
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    invoke-virtual {p0}, Lepson/print/MenudataAdapter;->getJSONFile()Lorg/json/JSONObject;

    move-result-object v0

    iput-object v0, p0, Lepson/print/MenudataAdapter;->json:Lorg/json/JSONObject;

    .line 44
    iget-object v0, p0, Lepson/print/MenudataAdapter;->json:Lorg/json/JSONObject;

    invoke-virtual {p0, v0}, Lepson/print/MenudataAdapter;->parseApps(Lorg/json/JSONObject;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lepson/print/MenudataAdapter;->apps:Ljava/util/ArrayList;

    .line 45
    iget-object v0, p0, Lepson/print/MenudataAdapter;->json:Lorg/json/JSONObject;

    invoke-virtual {p0, v0}, Lepson/print/MenudataAdapter;->parseFunctions(Lorg/json/JSONObject;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lepson/print/MenudataAdapter;->functions:Ljava/util/ArrayList;

    .line 46
    iget-object v0, p0, Lepson/print/MenudataAdapter;->json:Lorg/json/JSONObject;

    invoke-virtual {p0, v0}, Lepson/print/MenudataAdapter;->parseDefaultCapabilities(Lorg/json/JSONObject;)Lepson/print/MenudataAdapter$MenuDataCapability;

    move-result-object v0

    iput-object v0, p0, Lepson/print/MenudataAdapter;->defaultCapability:Lepson/print/MenudataAdapter$MenuDataCapability;

    .line 47
    iget-object v0, p0, Lepson/print/MenudataAdapter;->json:Lorg/json/JSONObject;

    invoke-virtual {p0, v0}, Lepson/print/MenudataAdapter;->parseCapabilities(Lorg/json/JSONObject;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lepson/print/MenudataAdapter;->capabilities:Ljava/util/ArrayList;

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 50
    :goto_0
    iget-object v2, p0, Lepson/print/MenudataAdapter;->apps:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    const/4 v2, 0x0

    .line 51
    :goto_1
    iget-object v3, p0, Lepson/print/MenudataAdapter;->functions:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 52
    iget-object v3, p0, Lepson/print/MenudataAdapter;->apps:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lepson/print/MenudataAdapter$MenuDataApp;

    iget-object v3, v3, Lepson/print/MenudataAdapter$MenuDataApp;->appID:Ljava/lang/String;

    iget-object v4, p0, Lepson/print/MenudataAdapter;->functions:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lepson/print/MenudataAdapter$MenuDataFunction;

    iget-object v4, v4, Lepson/print/MenudataAdapter$MenuDataFunction;->appID:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 53
    iget-object v3, p0, Lepson/print/MenudataAdapter;->apps:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lepson/print/MenudataAdapter$MenuDataApp;

    iget-object v3, v3, Lepson/print/MenudataAdapter$MenuDataApp;->appfunctions:Ljava/util/ArrayList;

    iget-object v4, p0, Lepson/print/MenudataAdapter;->functions:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    const-string v0, "parseJSONFile"

    const-string v1, "OK"

    .line 57
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 0

    .line 35
    iput-object p1, p0, Lepson/print/MenudataAdapter;->mContext:Landroid/content/Context;

    return-void
.end method

.method setExcludedlist(Ljava/util/ArrayList;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 271
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 273
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    if-eqz p2, :cond_0

    .line 274
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    .line 277
    :cond_0
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.class Lepson/print/ActivityPrintWeb$4;
.super Ljava/lang/Object;
.source "ActivityPrintWeb.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/ActivityPrintWeb;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/ActivityPrintWeb;


# direct methods
.method constructor <init>(Lepson/print/ActivityPrintWeb;)V
    .locals 0

    .line 246
    iput-object p1, p0, Lepson/print/ActivityPrintWeb$4;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2

    .line 248
    iget-object p1, p0, Lepson/print/ActivityPrintWeb$4;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-virtual {p1}, Lepson/print/ActivityPrintWeb;->getWindowManager()Landroid/view/WindowManager;

    move-result-object p1

    .line 249
    invoke-interface {p1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object p1

    .line 250
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 251
    invoke-virtual {p1, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 253
    iget p1, v0, Landroid/graphics/Point;->y:I

    int-to-float p1, p1

    .line 254
    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    cmpl-float v1, p1, v0

    if-lez v1, :cond_0

    move p1, v0

    .line 261
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 272
    :pswitch_0
    iget-object v0, p0, Lepson/print/ActivityPrintWeb$4;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-static {v0}, Lepson/print/ActivityPrintWeb;->access$300(Lepson/print/ActivityPrintWeb;)F

    move-result v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result p2

    sub-float/2addr v1, p2

    invoke-static {v0, v1}, Lepson/print/ActivityPrintWeb;->access$402(Lepson/print/ActivityPrintWeb;F)F

    goto :goto_0

    .line 263
    :pswitch_1
    iget-object v0, p0, Lepson/print/ActivityPrintWeb$4;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result p2

    invoke-static {v0, p2}, Lepson/print/ActivityPrintWeb;->access$302(Lepson/print/ActivityPrintWeb;F)F

    .line 264
    iget-object p2, p0, Lepson/print/ActivityPrintWeb$4;->this$0:Lepson/print/ActivityPrintWeb;

    const/4 v0, 0x0

    invoke-static {p2, v0}, Lepson/print/ActivityPrintWeb;->access$402(Lepson/print/ActivityPrintWeb;F)F

    .line 276
    :goto_0
    :pswitch_2
    iget-object p2, p0, Lepson/print/ActivityPrintWeb$4;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-static {p2}, Lepson/print/ActivityPrintWeb;->access$400(Lepson/print/ActivityPrintWeb;)F

    move-result p2

    const/high16 v0, 0x41200000    # 10.0f

    div-float/2addr p1, v0

    cmpl-float p2, p2, p1

    if-lez p2, :cond_1

    .line 277
    iget-object p1, p0, Lepson/print/ActivityPrintWeb$4;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-static {p1}, Lepson/print/ActivityPrintWeb;->access$500(Lepson/print/ActivityPrintWeb;)V

    goto :goto_1

    .line 278
    :cond_1
    iget-object p2, p0, Lepson/print/ActivityPrintWeb$4;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-static {p2}, Lepson/print/ActivityPrintWeb;->access$400(Lepson/print/ActivityPrintWeb;)F

    move-result p2

    const/high16 v0, -0x40800000    # -1.0f

    mul-float p1, p1, v0

    cmpg-float p1, p2, p1

    if-gez p1, :cond_2

    .line 279
    iget-object p1, p0, Lepson/print/ActivityPrintWeb$4;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-static {p1}, Lepson/print/ActivityPrintWeb;->access$600(Lepson/print/ActivityPrintWeb;)V

    :cond_2
    :goto_1
    const/4 p1, 0x1

    return p1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

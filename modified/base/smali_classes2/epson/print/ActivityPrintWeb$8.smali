.class Lepson/print/ActivityPrintWeb$8;
.super Landroid/os/Handler;
.source "ActivityPrintWeb.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/ActivityPrintWeb;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/ActivityPrintWeb;


# direct methods
.method constructor <init>(Lepson/print/ActivityPrintWeb;)V
    .locals 0

    .line 480
    iput-object p1, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9

    .line 482
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x64

    const/4 v2, 0x0

    if-eq v0, v1, :cond_5

    const/4 p1, 0x1

    packed-switch v0, :pswitch_data_0

    packed-switch v0, :pswitch_data_1

    goto/16 :goto_3

    .line 553
    :pswitch_0
    iget-object p1, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-static {p1}, Lepson/print/ActivityPrintWeb;->access$1000(Lepson/print/ActivityPrintWeb;)Landroid/widget/LinearLayout;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/LinearLayout;->invalidate()V

    .line 554
    iget-object p1, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-static {p1}, Lepson/print/ActivityPrintWeb;->access$1200(Lepson/print/ActivityPrintWeb;)Landroid/widget/RelativeLayout;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/RelativeLayout;->invalidate()V

    goto/16 :goto_3

    :pswitch_1
    const-string p1, "ActivityPrintWeb"

    const-string v0, "INIT"

    .line 484
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 485
    iget-object p1, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-static {p1}, Lepson/print/ActivityPrintWeb;->access$700(Lepson/print/ActivityPrintWeb;)Lepson/print/service/IEpsonService;

    move-result-object p1

    if-nez p1, :cond_0

    const/4 p1, 0x5

    const-wide/16 v0, 0x64

    .line 486
    invoke-virtual {p0, p1, v0, v1}, Lepson/print/ActivityPrintWeb$8;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_3

    .line 490
    :cond_0
    new-instance p1, Lepson/print/ActivityPrintWeb$8$1;

    invoke-direct {p1, p0}, Lepson/print/ActivityPrintWeb$8$1;-><init>(Lepson/print/ActivityPrintWeb$8;)V

    new-array v0, v2, [Ljava/lang/Void;

    .line 548
    invoke-virtual {p1, v0}, Lepson/print/ActivityPrintWeb$8$1;->executeOnExecutor([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_3

    .line 570
    :pswitch_2
    iget-object v0, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-static {v0}, Lepson/print/ActivityPrintWeb;->access$1100(Lepson/print/ActivityPrintWeb;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 572
    iget-object v0, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    const-string v1, "PrintSetting"

    invoke-virtual {v0, v1, v2}, Lepson/print/ActivityPrintWeb;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 574
    iget-object v1, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    iget-object v1, v1, Lepson/print/ActivityPrintWeb;->printWeb:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 576
    new-instance v1, Lepson/print/EPImageList;

    invoke-direct {v1}, Lepson/print/EPImageList;-><init>()V

    .line 577
    iget-object v3, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-static {v3}, Lepson/print/ActivityPrintWeb;->access$1400(Lepson/print/ActivityPrintWeb;)I

    move-result v3

    :goto_0
    iget-object v4, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-static {v4}, Lepson/print/ActivityPrintWeb;->access$1500(Lepson/print/ActivityPrintWeb;)I

    move-result v4

    if-gt v3, v4, :cond_3

    .line 578
    iget-object v4, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-static {v4}, Lepson/print/ActivityPrintWeb;->access$1600(Lepson/print/ActivityPrintWeb;)Lepson/print/EPImageList;

    move-result-object v4

    add-int/lit8 v5, v3, -0x1

    invoke-virtual {v4, v5}, Lepson/print/EPImageList;->get(I)Lepson/print/EPImage;

    move-result-object v4

    .line 581
    iget-object v6, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-static {v6}, Lepson/print/ActivityPrintWeb;->access$1000(Lepson/print/ActivityPrintWeb;)Landroid/widget/LinearLayout;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getLeft()I

    move-result v6

    iget-object v7, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-static {v7}, Lepson/print/ActivityPrintWeb;->access$1000(Lepson/print/ActivityPrintWeb;)Landroid/widget/LinearLayout;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getPaddingLeft()I

    move-result v7

    add-int/2addr v6, v7

    iput v6, v4, Lepson/print/EPImage;->previewPaperRectLeft:I

    .line 582
    iget-object v6, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-static {v6}, Lepson/print/ActivityPrintWeb;->access$1000(Lepson/print/ActivityPrintWeb;)Landroid/widget/LinearLayout;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getTop()I

    move-result v6

    iget-object v7, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-static {v7}, Lepson/print/ActivityPrintWeb;->access$1000(Lepson/print/ActivityPrintWeb;)Landroid/widget/LinearLayout;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getPaddingTop()I

    move-result v7

    add-int/2addr v6, v7

    iput v6, v4, Lepson/print/EPImage;->previewPaperRectTop:I

    .line 583
    iget-object v6, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-static {v6}, Lepson/print/ActivityPrintWeb;->access$1000(Lepson/print/ActivityPrintWeb;)Landroid/widget/LinearLayout;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getRight()I

    move-result v6

    iget-object v7, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-static {v7}, Lepson/print/ActivityPrintWeb;->access$1000(Lepson/print/ActivityPrintWeb;)Landroid/widget/LinearLayout;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getPaddingRight()I

    move-result v7

    sub-int/2addr v6, v7

    iput v6, v4, Lepson/print/EPImage;->previewPaperRectRight:I

    .line 584
    iget-object v6, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-static {v6}, Lepson/print/ActivityPrintWeb;->access$1000(Lepson/print/ActivityPrintWeb;)Landroid/widget/LinearLayout;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getBottom()I

    move-result v6

    iget-object v7, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-static {v7}, Lepson/print/ActivityPrintWeb;->access$1000(Lepson/print/ActivityPrintWeb;)Landroid/widget/LinearLayout;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getPaddingBottom()I

    move-result v7

    sub-int/2addr v6, v7

    iput v6, v4, Lepson/print/EPImage;->previewPaperRectBottom:I

    .line 587
    iget-object v6, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-static {v6}, Lepson/print/ActivityPrintWeb;->access$1000(Lepson/print/ActivityPrintWeb;)Landroid/widget/LinearLayout;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getLeft()I

    move-result v6

    iget-object v7, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-static {v7}, Lepson/print/ActivityPrintWeb;->access$1700(Lepson/print/ActivityPrintWeb;)Landroid/widget/ImageView;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLeft()I

    move-result v7

    add-int/2addr v6, v7

    int-to-float v6, v6

    iput v6, v4, Lepson/print/EPImage;->previewImageRectLeft:F

    .line 588
    iget-object v6, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-static {v6}, Lepson/print/ActivityPrintWeb;->access$1000(Lepson/print/ActivityPrintWeb;)Landroid/widget/LinearLayout;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getTop()I

    move-result v6

    iget-object v7, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-static {v7}, Lepson/print/ActivityPrintWeb;->access$1700(Lepson/print/ActivityPrintWeb;)Landroid/widget/ImageView;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/ImageView;->getTop()I

    move-result v7

    add-int/2addr v6, v7

    int-to-float v6, v6

    iput v6, v4, Lepson/print/EPImage;->previewImageRectTop:F

    .line 589
    iget-object v6, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-static {v6}, Lepson/print/ActivityPrintWeb;->access$1000(Lepson/print/ActivityPrintWeb;)Landroid/widget/LinearLayout;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getLeft()I

    move-result v6

    iget-object v7, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-static {v7}, Lepson/print/ActivityPrintWeb;->access$1700(Lepson/print/ActivityPrintWeb;)Landroid/widget/ImageView;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/ImageView;->getRight()I

    move-result v7

    add-int/2addr v6, v7

    int-to-float v6, v6

    iput v6, v4, Lepson/print/EPImage;->previewImageRectRight:F

    .line 590
    iget-object v6, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-static {v6}, Lepson/print/ActivityPrintWeb;->access$1000(Lepson/print/ActivityPrintWeb;)Landroid/widget/LinearLayout;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getTop()I

    move-result v6

    iget-object v7, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-static {v7}, Lepson/print/ActivityPrintWeb;->access$1700(Lepson/print/ActivityPrintWeb;)Landroid/widget/ImageView;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/ImageView;->getBottom()I

    move-result v7

    add-int/2addr v6, v7

    int-to-float v6, v6

    iput v6, v4, Lepson/print/EPImage;->previewImageRectBottom:F

    .line 593
    iget v6, v4, Lepson/print/EPImage;->previewImageRectRight:F

    iget v7, v4, Lepson/print/EPImage;->previewImageRectLeft:F

    sub-float/2addr v6, v7

    float-to-int v6, v6

    iput v6, v4, Lepson/print/EPImage;->previewWidth:I

    .line 594
    iget v6, v4, Lepson/print/EPImage;->previewImageRectBottom:F

    iget v7, v4, Lepson/print/EPImage;->previewImageRectTop:F

    sub-float/2addr v6, v7

    float-to-int v6, v6

    iput v6, v4, Lepson/print/EPImage;->previewHeight:I

    .line 597
    iget v6, v4, Lepson/print/EPImage;->previewPaperRectRight:I

    iget v7, v4, Lepson/print/EPImage;->previewPaperRectLeft:I

    sub-int/2addr v6, v7

    iget v7, v4, Lepson/print/EPImage;->previewPaperRectBottom:I

    iget v8, v4, Lepson/print/EPImage;->previewPaperRectTop:I

    sub-int/2addr v7, v8

    if-le v6, v7, :cond_1

    .line 599
    iput-boolean p1, v4, Lepson/print/EPImage;->isPaperLandScape:Z

    goto :goto_1

    .line 601
    :cond_1
    iput-boolean v2, v4, Lepson/print/EPImage;->isPaperLandScape:Z

    .line 604
    :goto_1
    iget-object v6, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-static {v6}, Lepson/print/ActivityPrintWeb;->access$1800(Lepson/print/ActivityPrintWeb;)Z

    move-result v6

    iput-boolean v6, v4, Lepson/print/EPImage;->isPaperLandScape:Z

    .line 608
    iget-object v4, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-static {v4}, Lepson/print/ActivityPrintWeb;->access$1600(Lepson/print/ActivityPrintWeb;)Lepson/print/EPImageList;

    move-result-object v4

    invoke-virtual {v4, v5}, Lepson/print/EPImageList;->get(I)Lepson/print/EPImage;

    move-result-object v4

    .line 610
    iget-object v5, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-static {v5}, Lepson/print/ActivityPrintWeb;->access$1900(Lepson/print/ActivityPrintWeb;)I

    move-result v5

    if-ne v5, p1, :cond_2

    const/16 v5, 0x1002

    .line 611
    invoke-virtual {v4, v5}, Lepson/print/EPImage;->setType(I)V

    goto :goto_2

    :cond_2
    const/16 v5, 0x1001

    .line 613
    invoke-virtual {v4, v5}, Lepson/print/EPImage;->setType(I)V

    .line 615
    :goto_2
    invoke-virtual {v1, v4}, Lepson/print/EPImageList;->add(Lepson/print/EPImage;)Z

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 618
    :cond_3
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    .line 620
    iget-object v0, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-static {v0}, Lepson/print/ActivityPrintWeb;->access$800(Lepson/print/ActivityPrintWeb;)Z

    move-result v0

    if-ne v0, p1, :cond_4

    .line 622
    iget-object p1, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    const-string v0, "PrintSetting"

    invoke-virtual {p1, v0, v2}, Lepson/print/ActivityPrintWeb;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    .line 623
    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    const-string v0, "SOURCE_TYPE"

    const/4 v2, 0x3

    .line 625
    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 626
    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 629
    :cond_4
    iget-object p1, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-static {p1, v1}, Lepson/print/ActivityPrintWeb;->access$2002(Lepson/print/ActivityPrintWeb;Lepson/print/EPImageList;)Lepson/print/EPImageList;

    .line 630
    iget-object p1, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-virtual {p1}, Lepson/print/ActivityPrintWeb;->check3GAndStartPrint()V

    goto/16 :goto_3

    .line 564
    :pswitch_3
    iget-object v0, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-static {v0}, Lepson/print/ActivityPrintWeb;->access$1300(Lepson/print/ActivityPrintWeb;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    const v2, 0x7f0e01b4

    .line 565
    invoke-virtual {v1, v2}, Lepson/print/ActivityPrintWeb;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 564
    invoke-static {v0, v1, p1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    .line 566
    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    goto/16 :goto_3

    .line 558
    :pswitch_4
    iget-object v0, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-static {v0}, Lepson/print/ActivityPrintWeb;->access$1300(Lepson/print/ActivityPrintWeb;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    const v2, 0x7f0e04a0

    .line 559
    invoke-virtual {v1, v2}, Lepson/print/ActivityPrintWeb;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 558
    invoke-static {v0, v1, p1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    .line 560
    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    goto/16 :goto_3

    .line 636
    :cond_5
    iget-object v0, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object p1

    const-string v1, "PAPERSOURCEINFO"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object p1

    invoke-static {v0, p1}, Lepson/print/ActivityPrintWeb;->access$2102(Lepson/print/ActivityPrintWeb;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 637
    iget-object p1, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-static {p1}, Lepson/print/ActivityPrintWeb;->access$2100(Lepson/print/ActivityPrintWeb;)Ljava/util/ArrayList;

    move-result-object p1

    const/16 v0, 0x8

    const v1, 0x7f05004a

    if-eqz p1, :cond_7

    .line 638
    iget-object p1, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-static {p1}, Lepson/print/ActivityPrintWeb;->access$2200(Lepson/print/ActivityPrintWeb;)Landroid/widget/TextView;

    move-result-object p1

    iget-object v3, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-virtual {v3}, Lepson/print/ActivityPrintWeb;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 639
    iget-object p1, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-static {p1}, Lepson/print/ActivityPrintWeb;->access$2200(Lepson/print/ActivityPrintWeb;)Landroid/widget/TextView;

    move-result-object p1

    iget-object v1, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-virtual {v1}, Lepson/print/ActivityPrintWeb;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f05001e

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 642
    new-instance p1, Lepson/print/screen/PrintSetting;

    iget-object v1, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    sget-object v3, Lepson/print/screen/PrintSetting$Kind;->document:Lepson/print/screen/PrintSetting$Kind;

    invoke-direct {p1, v1, v3}, Lepson/print/screen/PrintSetting;-><init>(Landroid/content/Context;Lepson/print/screen/PrintSetting$Kind;)V

    .line 643
    invoke-virtual {p1}, Lepson/print/screen/PrintSetting;->loadSettings()V

    .line 645
    iget-object v1, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    iget-object v1, v1, Lepson/print/ActivityPrintWeb;->paperSourceInfo:Lepson/print/screen/PaperSourceInfo;

    iget-object v3, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-static {v3}, Lepson/print/ActivityPrintWeb;->access$2100(Lepson/print/ActivityPrintWeb;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v1, p1, v3}, Lepson/print/screen/PaperSourceInfo;->checkPaperMissmatch(Lepson/print/screen/PrintSetting;Ljava/util/ArrayList;)Z

    move-result p1

    if-eqz p1, :cond_6

    .line 647
    iget-object p1, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-static {p1}, Lepson/print/ActivityPrintWeb;->access$2300(Lepson/print/ActivityPrintWeb;)Landroid/widget/ImageView;

    move-result-object p1

    invoke-virtual {p1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_3

    .line 649
    :cond_6
    iget-object p1, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-static {p1}, Lepson/print/ActivityPrintWeb;->access$2300(Lepson/print/ActivityPrintWeb;)Landroid/widget/ImageView;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_3

    .line 653
    :cond_7
    iget-object p1, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-static {p1}, Lepson/print/ActivityPrintWeb;->access$2200(Lepson/print/ActivityPrintWeb;)Landroid/widget/TextView;

    move-result-object p1

    iget-object v2, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-virtual {v2}, Lepson/print/ActivityPrintWeb;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070153

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 654
    iget-object p1, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-static {p1}, Lepson/print/ActivityPrintWeb;->access$2200(Lepson/print/ActivityPrintWeb;)Landroid/widget/TextView;

    move-result-object p1

    iget-object v2, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-virtual {v2}, Lepson/print/ActivityPrintWeb;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 655
    iget-object p1, p0, Lepson/print/ActivityPrintWeb$8;->this$0:Lepson/print/ActivityPrintWeb;

    invoke-static {p1}, Lepson/print/ActivityPrintWeb;->access$2300(Lepson/print/ActivityPrintWeb;)Landroid/widget/ImageView;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_3
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x4
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.class Lepson/print/EPImageCreator$MakeMultiPrintImage;
.super Ljava/lang/Object;
.source "EPImageCreator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/EPImageCreator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "MakeMultiPrintImage"
.end annotation


# static fields
.field public static final LAYOUT_BUTTOMLEFT:I = 0x2

.field public static final LAYOUT_BUTTOMRIGHT:I = 0x3

.field public static final LAYOUT_TOPLEFT:I = 0x0

.field public static final LAYOUT_TOPRIGHT:I = 0x1


# direct methods
.method constructor <init>()V
    .locals 0

    .line 1117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static calcLayout4in1(IZZI)I
    .locals 16

    move/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, p2

    .line 1180
    new-instance v3, Lorg/opencv/core/Mat;

    sget v4, Lorg/opencv/core/CvType;->CV_8UC1:I

    const-wide/16 v5, 0x0

    invoke-static {v5, v6}, Lorg/opencv/core/Scalar;->all(D)Lorg/opencv/core/Scalar;

    move-result-object v7

    const/4 v8, 0x2

    invoke-direct {v3, v8, v8, v4, v7}, Lorg/opencv/core/Mat;-><init>(IIILorg/opencv/core/Scalar;)V

    const/high16 v4, 0x20000

    const-wide/high16 v9, 0x4008000000000000L    # 3.0

    const-wide/high16 v11, 0x4000000000000000L    # 2.0

    const-wide/high16 v13, 0x3ff0000000000000L    # 1.0

    const/4 v7, 0x0

    const/4 v15, 0x1

    if-eq v0, v4, :cond_1

    const/high16 v4, 0x40000

    if-eq v0, v4, :cond_0

    goto :goto_0

    .line 1198
    :cond_0
    new-array v0, v15, [D

    aput-wide v5, v0, v7

    invoke-virtual {v3, v7, v7, v0}, Lorg/opencv/core/Mat;->put(II[D)I

    .line 1199
    new-array v0, v15, [D

    aput-wide v11, v0, v7

    invoke-virtual {v3, v7, v15, v0}, Lorg/opencv/core/Mat;->put(II[D)I

    .line 1200
    new-array v0, v15, [D

    aput-wide v13, v0, v7

    invoke-virtual {v3, v15, v7, v0}, Lorg/opencv/core/Mat;->put(II[D)I

    .line 1201
    new-array v0, v15, [D

    aput-wide v9, v0, v7

    invoke-virtual {v3, v15, v15, v0}, Lorg/opencv/core/Mat;->put(II[D)I

    goto :goto_0

    .line 1188
    :cond_1
    new-array v0, v15, [D

    aput-wide v5, v0, v7

    invoke-virtual {v3, v7, v7, v0}, Lorg/opencv/core/Mat;->put(II[D)I

    .line 1189
    new-array v0, v15, [D

    aput-wide v13, v0, v7

    invoke-virtual {v3, v7, v15, v0}, Lorg/opencv/core/Mat;->put(II[D)I

    .line 1190
    new-array v0, v15, [D

    aput-wide v11, v0, v7

    invoke-virtual {v3, v15, v7, v0}, Lorg/opencv/core/Mat;->put(II[D)I

    .line 1191
    new-array v0, v15, [D

    aput-wide v9, v0, v7

    invoke-virtual {v3, v15, v15, v0}, Lorg/opencv/core/Mat;->put(II[D)I

    .line 1208
    :goto_0
    new-instance v0, Lorg/opencv/core/Mat;

    invoke-direct {v0}, Lorg/opencv/core/Mat;-><init>()V

    if-ne v1, v15, :cond_2

    if-nez v2, :cond_2

    .line 1211
    invoke-virtual {v3}, Lorg/opencv/core/Mat;->t()Lorg/opencv/core/Mat;

    move-result-object v0

    .line 1212
    invoke-static {v0, v0, v7}, Lorg/opencv/core/Core;->flip(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V

    goto :goto_1

    :cond_2
    if-nez v1, :cond_3

    if-ne v2, v15, :cond_3

    .line 1215
    invoke-virtual {v3}, Lorg/opencv/core/Mat;->t()Lorg/opencv/core/Mat;

    move-result-object v0

    .line 1216
    invoke-static {v0, v0, v15}, Lorg/opencv/core/Core;->flip(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V

    goto :goto_1

    .line 1218
    :cond_3
    invoke-virtual {v3}, Lorg/opencv/core/Mat;->clone()Lorg/opencv/core/Mat;

    move-result-object v0

    .line 1220
    :goto_1
    invoke-virtual {v3}, Lorg/opencv/core/Mat;->release()V

    const/4 v1, 0x0

    .line 1226
    :goto_2
    invoke-virtual {v0}, Lorg/opencv/core/Mat;->rows()I

    move-result v2

    if-ge v1, v2, :cond_6

    const/4 v2, 0x0

    .line 1227
    :goto_3
    invoke-virtual {v0}, Lorg/opencv/core/Mat;->cols()I

    move-result v3

    if-ge v2, v3, :cond_5

    .line 1228
    invoke-virtual {v0, v1, v2}, Lorg/opencv/core/Mat;->get(II)[D

    move-result-object v3

    .line 1229
    aget-wide v4, v3, v7

    double-to-int v3, v4

    move/from16 v4, p3

    if-ne v3, v4, :cond_4

    goto :goto_4

    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_5
    move/from16 v4, p3

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_6
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 1237
    :goto_4
    invoke-virtual {v0}, Lorg/opencv/core/Mat;->release()V

    if-nez v1, :cond_8

    if-nez v2, :cond_7

    goto :goto_5

    :cond_7
    if-ne v2, v15, :cond_a

    const/4 v7, 0x1

    goto :goto_5

    :cond_8
    if-ne v1, v15, :cond_a

    if-nez v2, :cond_9

    const/4 v7, 0x2

    goto :goto_5

    :cond_9
    if-ne v2, v15, :cond_a

    const/4 v8, 0x3

    const/4 v7, 0x3

    :cond_a
    :goto_5
    return v7
.end method

.method static layoutPrintImage(Lorg/opencv/core/Mat;Ljava/lang/String;I)V
    .locals 4

    const/4 v0, -0x1

    .line 1134
    invoke-static {p1, v0}, Lorg/opencv/imgcodecs/Imgcodecs;->imread(Ljava/lang/String;I)Lorg/opencv/core/Mat;

    move-result-object v0

    const/4 v1, 0x0

    packed-switch p2, :pswitch_data_0

    const/4 p0, 0x0

    goto :goto_0

    .line 1151
    :pswitch_0
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->rows()I

    move-result p2

    invoke-virtual {v0}, Lorg/opencv/core/Mat;->rows()I

    move-result v1

    sub-int/2addr p2, v1

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->rows()I

    move-result v1

    .line 1152
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->cols()I

    move-result v2

    invoke-virtual {v0}, Lorg/opencv/core/Mat;->cols()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->cols()I

    move-result v3

    .line 1151
    invoke-virtual {p0, p2, v1, v2, v3}, Lorg/opencv/core/Mat;->submat(IIII)Lorg/opencv/core/Mat;

    move-result-object p0

    goto :goto_0

    .line 1147
    :pswitch_1
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->rows()I

    move-result p2

    invoke-virtual {v0}, Lorg/opencv/core/Mat;->rows()I

    move-result v2

    sub-int/2addr p2, v2

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->rows()I

    move-result v2

    .line 1148
    invoke-virtual {v0}, Lorg/opencv/core/Mat;->cols()I

    move-result v3

    .line 1147
    invoke-virtual {p0, p2, v2, v1, v3}, Lorg/opencv/core/Mat;->submat(IIII)Lorg/opencv/core/Mat;

    move-result-object p0

    goto :goto_0

    .line 1143
    :pswitch_2
    invoke-virtual {v0}, Lorg/opencv/core/Mat;->rows()I

    move-result p2

    .line 1144
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->cols()I

    move-result v2

    invoke-virtual {v0}, Lorg/opencv/core/Mat;->cols()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->cols()I

    move-result v3

    .line 1143
    invoke-virtual {p0, v1, p2, v2, v3}, Lorg/opencv/core/Mat;->submat(IIII)Lorg/opencv/core/Mat;

    move-result-object p0

    goto :goto_0

    .line 1140
    :pswitch_3
    invoke-virtual {v0}, Lorg/opencv/core/Mat;->rows()I

    move-result p2

    invoke-virtual {v0}, Lorg/opencv/core/Mat;->cols()I

    move-result v2

    invoke-virtual {p0, v1, p2, v1, v2}, Lorg/opencv/core/Mat;->submat(IIII)Lorg/opencv/core/Mat;

    move-result-object p0

    .line 1157
    :goto_0
    invoke-virtual {v0, p0}, Lorg/opencv/core/Mat;->copyTo(Lorg/opencv/core/Mat;)V

    .line 1160
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->release()V

    .line 1161
    invoke-virtual {v0}, Lorg/opencv/core/Mat;->release()V

    .line 1164
    new-instance p0, Ljava/io/File;

    invoke-direct {p0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1165
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

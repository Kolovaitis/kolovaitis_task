.class public Lepson/print/WebShareActivity;
.super Landroid/support/v7/app/AppCompatActivity;
.source "WebShareActivity.java"

# interfaces
.implements Lepson/print/CustomTitleDialogFragment$Callback;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatActivity;-><init>()V

    return-void
.end method

.method private checkAndStartNextActivity()V
    .locals 3

    .line 18
    invoke-virtual {p0}, Lepson/print/WebShareActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lepson/print/IprintApplication;

    .line 19
    invoke-virtual {v0}, Lepson/print/IprintApplication;->getPrinting()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0e021e

    .line 21
    invoke-virtual {p0, v0}, Lepson/print/WebShareActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0e03fe

    const/4 v2, 0x0

    .line 20
    invoke-static {v2, v0, v2, v1, v2}, Lepson/print/CustomTitleDialogFragment;->newInstance(ILjava/lang/String;III)Lepson/print/CustomTitleDialogFragment;

    move-result-object v0

    .line 22
    invoke-virtual {p0}, Lepson/print/WebShareActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "simple-dialog"

    invoke-virtual {v0, v1, v2}, Lepson/print/CustomTitleDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void

    .line 27
    :cond_0
    invoke-direct {p0}, Lepson/print/WebShareActivity;->startWebActivity()V

    return-void
.end method

.method private startWebActivity()V
    .locals 2

    .line 31
    invoke-virtual {p0}, Lepson/print/WebShareActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x14000000

    .line 32
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 33
    const-class v1, Lepson/print/WebviewActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 34
    invoke-virtual {p0, v0}, Lepson/print/WebShareActivity;->startActivity(Landroid/content/Intent;)V

    .line 35
    invoke-virtual {p0}, Lepson/print/WebShareActivity;->finish()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0

    .line 11
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0a003f

    .line 12
    invoke-virtual {p0, p1}, Lepson/print/WebShareActivity;->setContentView(I)V

    .line 14
    invoke-direct {p0}, Lepson/print/WebShareActivity;->checkAndStartNextActivity()V

    return-void
.end method

.method public onLocalNegativeCallback(I)V
    .locals 0

    .line 45
    invoke-virtual {p0}, Lepson/print/WebShareActivity;->finish()V

    return-void
.end method

.method public onLocalPositiveCallback(I)V
    .locals 0

    .line 40
    invoke-virtual {p0}, Lepson/print/WebShareActivity;->finish()V

    return-void
.end method

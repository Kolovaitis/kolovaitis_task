.class Lepson/print/ActivityDocsPrintPreview$4;
.super Ljava/lang/Object;
.source "ActivityDocsPrintPreview.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/ActivityDocsPrintPreview;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/ActivityDocsPrintPreview;


# direct methods
.method constructor <init>(Lepson/print/ActivityDocsPrintPreview;)V
    .locals 0

    .line 398
    iput-object p1, p0, Lepson/print/ActivityDocsPrintPreview$4;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3

    .line 400
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$4;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-virtual {p1}, Lepson/print/ActivityDocsPrintPreview;->getWindowManager()Landroid/view/WindowManager;

    move-result-object p1

    .line 401
    invoke-interface {p1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object p1

    .line 402
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 403
    invoke-virtual {p1, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 404
    iget p1, v0, Landroid/graphics/Point;->y:I

    int-to-float p1, p1

    .line 405
    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    .line 408
    iget-object v1, p0, Lepson/print/ActivityDocsPrintPreview$4;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v1}, Lepson/print/ActivityDocsPrintPreview;->access$300(Lepson/print/ActivityDocsPrintPreview;)I

    move-result v1

    and-int/lit8 v1, v1, 0x2

    const/4 v2, 0x1

    if-nez v1, :cond_0

    return v2

    :cond_0
    cmpl-float v1, p1, v0

    if-lez v1, :cond_1

    move p1, v0

    .line 417
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 428
    :pswitch_0
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$4;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {v0}, Lepson/print/ActivityDocsPrintPreview;->access$400(Lepson/print/ActivityDocsPrintPreview;)F

    move-result v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result p2

    sub-float/2addr v1, p2

    invoke-static {v0, v1}, Lepson/print/ActivityDocsPrintPreview;->access$502(Lepson/print/ActivityDocsPrintPreview;F)F

    goto :goto_0

    .line 419
    :pswitch_1
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$4;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result p2

    invoke-static {v0, p2}, Lepson/print/ActivityDocsPrintPreview;->access$402(Lepson/print/ActivityDocsPrintPreview;F)F

    .line 420
    iget-object p2, p0, Lepson/print/ActivityDocsPrintPreview$4;->this$0:Lepson/print/ActivityDocsPrintPreview;

    const/4 v0, 0x0

    invoke-static {p2, v0}, Lepson/print/ActivityDocsPrintPreview;->access$502(Lepson/print/ActivityDocsPrintPreview;F)F

    .line 432
    :goto_0
    :pswitch_2
    iget-object p2, p0, Lepson/print/ActivityDocsPrintPreview$4;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p2}, Lepson/print/ActivityDocsPrintPreview;->access$500(Lepson/print/ActivityDocsPrintPreview;)F

    move-result p2

    const/high16 v0, 0x41200000    # 10.0f

    div-float/2addr p1, v0

    cmpl-float p2, p2, p1

    if-lez p2, :cond_3

    .line 433
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$4;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$600(Lepson/print/ActivityDocsPrintPreview;)I

    move-result p1

    iget-object p2, p0, Lepson/print/ActivityDocsPrintPreview$4;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p2}, Lepson/print/ActivityDocsPrintPreview;->access$700(Lepson/print/ActivityDocsPrintPreview;)I

    move-result p2

    if-ne p1, p2, :cond_2

    return v2

    .line 436
    :cond_2
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$4;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$200(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/Button;

    move-result-object p2

    invoke-virtual {p1, p2}, Lepson/print/ActivityDocsPrintPreview;->onClick(Landroid/view/View;)V

    goto :goto_1

    .line 437
    :cond_3
    iget-object p2, p0, Lepson/print/ActivityDocsPrintPreview$4;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p2}, Lepson/print/ActivityDocsPrintPreview;->access$500(Lepson/print/ActivityDocsPrintPreview;)F

    move-result p2

    const/high16 v0, -0x40800000    # -1.0f

    mul-float p1, p1, v0

    cmpg-float p1, p2, p1

    if-gez p1, :cond_5

    .line 438
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$4;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$600(Lepson/print/ActivityDocsPrintPreview;)I

    move-result p1

    if-ne p1, v2, :cond_4

    return v2

    .line 441
    :cond_4
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$4;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$100(Lepson/print/ActivityDocsPrintPreview;)Landroid/widget/Button;

    move-result-object p2

    invoke-virtual {p1, p2}, Lepson/print/ActivityDocsPrintPreview;->onClick(Landroid/view/View;)V

    :cond_5
    :goto_1
    return v2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.class Lepson/print/IprintHome$3;
.super Ljava/lang/Object;
.source "IprintHome.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/IprintHome;->updateMenu()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/IprintHome;

.field final synthetic val$adapter:Landroid/widget/ListAdapter;


# direct methods
.method constructor <init>(Lepson/print/IprintHome;Landroid/widget/ListAdapter;)V
    .locals 0

    .line 746
    iput-object p1, p0, Lepson/print/IprintHome$3;->this$0:Lepson/print/IprintHome;

    iput-object p2, p0, Lepson/print/IprintHome$3;->val$adapter:Landroid/widget/ListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .line 750
    new-instance p1, Landroid/content/Intent;

    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    .line 751
    iget-object p2, p0, Lepson/print/IprintHome$3;->val$adapter:Landroid/widget/ListAdapter;

    invoke-interface {p2, p3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lepson/print/IconTextArrayItem;

    if-eqz p2, :cond_a

    .line 754
    iget-boolean p3, p2, Lepson/print/IconTextArrayItem;->showMenu:Z

    if-eqz p3, :cond_7

    .line 755
    iget-object p3, p0, Lepson/print/IprintHome$3;->this$0:Lepson/print/IprintHome;

    iget p4, p2, Lepson/print/IconTextArrayItem;->menuId:I

    invoke-virtual {p3, p4}, Lepson/print/IprintHome;->getString(I)Ljava/lang/String;

    move-result-object p3

    iget-object p4, p0, Lepson/print/IprintHome$3;->this$0:Lepson/print/IprintHome;

    const p5, 0x7f0e0532

    invoke-virtual {p4, p5}, Lepson/print/IprintHome;->getString(I)Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p3, p4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p3

    const/4 p4, 0x0

    if-eqz p3, :cond_0

    .line 757
    iget-object p1, p0, Lepson/print/IprintHome$3;->this$0:Lepson/print/IprintHome;

    invoke-virtual {p1}, Lepson/print/IprintHome;->getBuyInkTask()Lepson/print/IprintHome$BuyInkTask;

    move-result-object p1

    new-array p2, p4, [Ljava/lang/Void;

    invoke-virtual {p1, p2}, Lepson/print/IprintHome$BuyInkTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 759
    iget-object p1, p0, Lepson/print/IprintHome$3;->this$0:Lepson/print/IprintHome;

    invoke-virtual {p1}, Lepson/print/IprintHome;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string p2, "Home-BuyInk"

    invoke-static {p1, p2}, Lcom/epson/iprint/prtlogger/Analytics;->sendAction(Landroid/content/Context;Ljava/lang/String;)V

    return-void

    .line 761
    :cond_0
    iget p3, p2, Lepson/print/IconTextArrayItem;->menuId:I

    const p5, 0x7f0e0442

    if-ne p3, p5, :cond_1

    .line 762
    iget-object p1, p0, Lepson/print/IprintHome$3;->this$0:Lepson/print/IprintHome;

    invoke-virtual {p1}, Lepson/print/IprintHome;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string p2, "Home-ReadyInk"

    invoke-static {p1, p2}, Lcom/epson/iprint/prtlogger/Analytics;->sendAction(Landroid/content/Context;Ljava/lang/String;)V

    .line 763
    iget-object p1, p0, Lepson/print/IprintHome$3;->this$0:Lepson/print/IprintHome;

    invoke-static {p1}, Lepson/print/IprintHome;->access$500(Lepson/print/IprintHome;)V

    return-void

    .line 765
    :cond_1
    iget-object p3, p0, Lepson/print/IprintHome$3;->this$0:Lepson/print/IprintHome;

    iget p5, p2, Lepson/print/IconTextArrayItem;->menuId:I

    invoke-virtual {p3, p5}, Lepson/print/IprintHome;->getString(I)Ljava/lang/String;

    move-result-object p3

    iget-object p5, p0, Lepson/print/IprintHome$3;->this$0:Lepson/print/IprintHome;

    const v0, 0x7f0e022d

    invoke-virtual {p5, v0}, Lepson/print/IprintHome;->getString(I)Ljava/lang/String;

    move-result-object p5

    invoke-virtual {p3, p5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p3

    if-eqz p3, :cond_2

    const-string p2, "android.intent.action.VIEW"

    .line 767
    invoke-virtual {p1, p2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string p2, "http://iprint.epsonconnect.com/app/index.htm"

    .line 768
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 769
    iget-object p2, p0, Lepson/print/IprintHome$3;->this$0:Lepson/print/IprintHome;

    iget-object p2, p2, Lepson/print/IprintHome;->mIprintHomeLogging:Lepson/print/IprintHomeMenuLogging;

    iget-object p3, p0, Lepson/print/IprintHome$3;->this$0:Lepson/print/IprintHome;

    invoke-virtual {p2, p3}, Lepson/print/IprintHomeMenuLogging;->countupMoreApp(Landroid/content/Context;)V

    goto/16 :goto_1

    .line 770
    :cond_2
    iget-object p3, p2, Lepson/print/IconTextArrayItem;->ClassName:Ljava/lang/String;

    const-string p5, ""

    invoke-virtual {p3, p5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p3

    if-nez p3, :cond_6

    .line 772
    iget-object p3, p2, Lepson/print/IconTextArrayItem;->appId:Ljava/lang/String;

    const-string p5, "Creative"

    invoke-virtual {p3, p5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-nez p3, :cond_4

    iget-object p3, p2, Lepson/print/IconTextArrayItem;->appId:Ljava/lang/String;

    const-string p5, "3DFramePrint"

    .line 773
    invoke-virtual {p3, p5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p3

    if-nez p3, :cond_4

    iget-object p3, p2, Lepson/print/IconTextArrayItem;->appId:Ljava/lang/String;

    const-string p5, "CameraCopy"

    .line 774
    invoke-virtual {p3, p5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p3

    if-nez p3, :cond_4

    iget-object p3, p2, Lepson/print/IconTextArrayItem;->appId:Ljava/lang/String;

    const-string p5, "MultiRollPrint"

    .line 775
    invoke-virtual {p3, p5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p3

    if-nez p3, :cond_4

    iget-object p3, p2, Lepson/print/IconTextArrayItem;->appId:Ljava/lang/String;

    const-string p5, "CardPrint"

    .line 776
    invoke-virtual {p3, p5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p3

    if-nez p3, :cond_4

    iget-object p3, p2, Lepson/print/IconTextArrayItem;->appId:Ljava/lang/String;

    const-string p5, "Nenga"

    .line 777
    invoke-virtual {p3, p5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p3

    if-eqz p3, :cond_3

    goto :goto_0

    .line 804
    :cond_3
    iget-object p3, p2, Lepson/print/IconTextArrayItem;->PackageName:Ljava/lang/String;

    iget-object p5, p2, Lepson/print/IconTextArrayItem;->ClassName:Ljava/lang/String;

    invoke-virtual {p1, p3, p5}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 805
    iget-object p3, p0, Lepson/print/IprintHome$3;->this$0:Lepson/print/IprintHome;

    iget-object p3, p3, Lepson/print/IprintHome;->mIprintHomeLogging:Lepson/print/IprintHomeMenuLogging;

    iget-object p5, p0, Lepson/print/IprintHome$3;->this$0:Lepson/print/IprintHome;

    invoke-virtual {p5}, Lepson/print/IprintHome;->getApplicationContext()Landroid/content/Context;

    move-result-object p5

    iget-object p2, p2, Lepson/print/IconTextArrayItem;->ClassName:Ljava/lang/String;

    invoke-virtual {p3, p5, p2}, Lepson/print/IprintHomeMenuLogging;->sendUiTapData(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_1

    .line 778
    :cond_4
    :goto_0
    iget-boolean p3, p2, Lepson/print/IconTextArrayItem;->isInstall:Z

    if-eqz p3, :cond_5

    .line 779
    iget-object p3, p2, Lepson/print/IconTextArrayItem;->ClassName:Ljava/lang/String;

    invoke-virtual {p1, p3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 p3, 0x10200000

    .line 782
    invoke-virtual {p1, p3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string p3, "PRINTER_ID"

    .line 786
    iget-object p5, p0, Lepson/print/IprintHome$3;->this$0:Lepson/print/IprintHome;

    invoke-static {p5}, Lepson/print/IprintHome;->access$300(Lepson/print/IprintHome;)Ljava/lang/String;

    move-result-object p5

    invoke-virtual {p1, p3, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string p3, "PRINTER_IP"

    .line 787
    iget-object p5, p0, Lepson/print/IprintHome$3;->this$0:Lepson/print/IprintHome;

    invoke-static {p5}, Lepson/print/IprintHome;->access$400(Lepson/print/IprintHome;)Ljava/lang/String;

    move-result-object p5

    invoke-virtual {p1, p3, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string p3, "PRINTER_NAME"

    .line 788
    iget-object p5, p0, Lepson/print/IprintHome$3;->this$0:Lepson/print/IprintHome;

    iget-object p5, p5, Lepson/print/IprintHome;->deviceID:Ljava/lang/String;

    invoke-virtual {p1, p3, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string p3, "PRINTER_SERIAL"

    .line 789
    iget-object p5, p0, Lepson/print/IprintHome$3;->this$0:Lepson/print/IprintHome;

    iget-object p5, p5, Lepson/print/IprintHome;->printerSerial:Ljava/lang/String;

    invoke-virtual {p1, p3, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string p3, "Function"

    .line 790
    iget-object p5, p2, Lepson/print/IconTextArrayItem;->key:Ljava/lang/String;

    invoke-virtual {p1, p3, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 791
    iget-object p3, p0, Lepson/print/IprintHome$3;->this$0:Lepson/print/IprintHome;

    invoke-virtual {p3}, Lepson/print/IprintHome;->getApplicationContext()Landroid/content/Context;

    move-result-object p3

    const-string p5, "printer"

    invoke-static {p3, p5}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->getConnectInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    const-string p5, "PRINTER_SSID"

    .line 792
    invoke-virtual {p1, p5, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 793
    iget-object p3, p0, Lepson/print/IprintHome$3;->this$0:Lepson/print/IprintHome;

    iget-object p3, p3, Lepson/print/IprintHome;->mIprintHomeLogging:Lepson/print/IprintHomeMenuLogging;

    iget-object p5, p0, Lepson/print/IprintHome$3;->this$0:Lepson/print/IprintHome;

    invoke-virtual {p3, p5, p2}, Lepson/print/IprintHomeMenuLogging;->countUpInstalledOtherApps(Landroid/content/Context;Lepson/print/IconTextArrayItem;)V

    goto :goto_1

    :cond_5
    const-string p3, "android.intent.action.VIEW"

    .line 797
    invoke-virtual {p1, p3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 798
    iget-object p3, p2, Lepson/print/IconTextArrayItem;->googleStoreUrl:Ljava/lang/String;

    invoke-static {p3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p3

    invoke-virtual {p1, p3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 799
    iget-object p3, p0, Lepson/print/IprintHome$3;->this$0:Lepson/print/IprintHome;

    iget-object p3, p3, Lepson/print/IprintHome;->mIprintHomeLogging:Lepson/print/IprintHomeMenuLogging;

    iget-object p5, p0, Lepson/print/IprintHome$3;->this$0:Lepson/print/IprintHome;

    invoke-virtual {p3, p5, p2}, Lepson/print/IprintHomeMenuLogging;->countUpNotInstalledOtherApps(Landroid/content/Context;Lepson/print/IconTextArrayItem;)V

    .line 811
    :cond_6
    :goto_1
    :try_start_0
    iget-object p2, p0, Lepson/print/IprintHome$3;->this$0:Lepson/print/IprintHome;

    invoke-virtual {p2, p1}, Lepson/print/IprintHome;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 813
    :catch_0
    iget-object p1, p0, Lepson/print/IprintHome$3;->this$0:Lepson/print/IprintHome;

    const p2, 0x7f0e0542

    invoke-static {p1, p2, p4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    goto :goto_3

    .line 817
    :cond_7
    new-instance p1, Landroid/app/AlertDialog$Builder;

    iget-object p3, p0, Lepson/print/IprintHome$3;->this$0:Lepson/print/IprintHome;

    invoke-direct {p1, p3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 819
    iget p2, p2, Lepson/print/IconTextArrayItem;->menuId:I

    invoke-virtual {p1, p2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 820
    iget-object p2, p0, Lepson/print/IprintHome$3;->this$0:Lepson/print/IprintHome;

    invoke-static {p2}, Lepson/print/IprintHome;->access$200(Lepson/print/IprintHome;)I

    move-result p2

    const/4 p3, 0x3

    if-ne p2, p3, :cond_8

    const p2, 0x7f0e002b

    .line 821
    invoke-virtual {p1, p2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    goto :goto_2

    .line 822
    :cond_8
    iget-object p2, p0, Lepson/print/IprintHome$3;->this$0:Lepson/print/IprintHome;

    invoke-static {p2}, Lepson/print/IprintHome;->access$200(Lepson/print/IprintHome;)I

    move-result p2

    const/4 p3, 0x2

    if-ne p2, p3, :cond_9

    const p2, 0x7f0e002c

    .line 823
    invoke-virtual {p1, p2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    :goto_2
    const p2, 0x7f0e04f2

    .line 828
    new-instance p3, Lepson/print/IprintHome$3$1;

    invoke-direct {p3, p0}, Lepson/print/IprintHome$3$1;-><init>(Lepson/print/IprintHome$3;)V

    invoke-virtual {p1, p2, p3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 834
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_3

    :cond_9
    return-void

    :cond_a
    :goto_3
    return-void
.end method

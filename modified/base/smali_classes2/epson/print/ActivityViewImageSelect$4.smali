.class Lepson/print/ActivityViewImageSelect$4;
.super Ljava/lang/Object;
.source "ActivityViewImageSelect.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/ActivityViewImageSelect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/ActivityViewImageSelect;


# direct methods
.method constructor <init>(Lepson/print/ActivityViewImageSelect;)V
    .locals 0

    .line 1547
    iput-object p1, p0, Lepson/print/ActivityViewImageSelect$4;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 6

    .line 1549
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect$4;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-virtual {v0}, Lepson/print/ActivityViewImageSelect;->isFinishing()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    .line 1553
    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x64

    const/4 v3, 0x0

    if-eq v0, v2, :cond_3

    packed-switch v0, :pswitch_data_0

    packed-switch v0, :pswitch_data_1

    goto/16 :goto_0

    .line 1625
    :pswitch_0
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$4;->this$0:Lepson/print/ActivityViewImageSelect;

    const v0, 0x7f0e04a0

    invoke-static {p1, v0}, Lepson/print/ActivityViewImageSelect;->access$1500(Lepson/print/ActivityViewImageSelect;I)V

    return v1

    .line 1630
    :pswitch_1
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$4;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {p1}, Lepson/print/ActivityViewImageSelect;->access$1600(Lepson/print/ActivityViewImageSelect;)Lepson/print/phlayout/PhotoPreviewImageList;

    move-result-object p1

    invoke-virtual {p1}, Lepson/print/phlayout/PhotoPreviewImageList;->size()I

    move-result p1

    if-gtz p1, :cond_1

    .line 1631
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$4;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {p1}, Lepson/print/ActivityViewImageSelect;->access$1700(Lepson/print/ActivityViewImageSelect;)Landroid/widget/Button;

    move-result-object p1

    invoke-virtual {p1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1632
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$4;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {p1}, Lepson/print/ActivityViewImageSelect;->access$1800(Lepson/print/ActivityViewImageSelect;)Landroid/widget/Button;

    move-result-object p1

    invoke-virtual {p1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1635
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$4;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {p1, v3}, Lepson/print/ActivityViewImageSelect;->access$102(Lepson/print/ActivityViewImageSelect;I)I

    .line 1642
    :cond_1
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$4;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {p1}, Lepson/print/ActivityViewImageSelect;->access$1900(Lepson/print/ActivityViewImageSelect;)Landroid/widget/TextView;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lepson/print/ActivityViewImageSelect$4;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {v2}, Lepson/print/ActivityViewImageSelect;->access$100(Lepson/print/ActivityViewImageSelect;)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lepson/print/ActivityViewImageSelect$4;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {v2}, Lepson/print/ActivityViewImageSelect;->access$1600(Lepson/print/ActivityViewImageSelect;)Lepson/print/phlayout/PhotoPreviewImageList;

    move-result-object v2

    invoke-virtual {v2}, Lepson/print/phlayout/PhotoPreviewImageList;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1646
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$4;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {p1}, Lepson/print/ActivityViewImageSelect;->access$2000(Lepson/print/ActivityViewImageSelect;)V

    .line 1648
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$4;->this$0:Lepson/print/ActivityViewImageSelect;

    iget-object p1, p1, Lepson/print/ActivityViewImageSelect;->previewImageView:Lcom/epson/iprint/apf/ApfPreviewView;

    invoke-virtual {p1}, Lcom/epson/iprint/apf/ApfPreviewView;->invalidate()V

    .line 1651
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$4;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {p1}, Lepson/print/ActivityViewImageSelect;->access$1100(Lepson/print/ActivityViewImageSelect;)Landroid/content/Context;

    move-result-object p1

    iget-object v0, p0, Lepson/print/ActivityViewImageSelect$4;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {v0}, Lepson/print/ActivityViewImageSelect;->access$1100(Lepson/print/ActivityViewImageSelect;)Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0e0373

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1618
    :pswitch_2
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$4;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {p1}, Lepson/print/ActivityViewImageSelect;->access$1400(Lepson/print/ActivityViewImageSelect;)Landroid/widget/TextView;

    move-result-object p1

    iget-object v0, p0, Lepson/print/ActivityViewImageSelect$4;->this$0:Lepson/print/ActivityViewImageSelect;

    new-instance v2, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize;

    invoke-direct {v2}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize;-><init>()V

    iget-object v3, p0, Lepson/print/ActivityViewImageSelect$4;->this$0:Lepson/print/ActivityViewImageSelect;

    .line 1619
    invoke-static {v3}, Lepson/print/ActivityViewImageSelect;->access$1300(Lepson/print/ActivityViewImageSelect;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/epson/mobilephone/common/escpr/MediaInfo$PaperSize;->getStringId(I)I

    move-result v2

    .line 1618
    invoke-virtual {v0, v2}, Lepson/print/ActivityViewImageSelect;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 1589
    :pswitch_3
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$4;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {p1}, Lepson/print/ActivityViewImageSelect;->access$1200(Lepson/print/ActivityViewImageSelect;)V

    goto/16 :goto_0

    .line 1581
    :pswitch_4
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$4;->this$0:Lepson/print/ActivityViewImageSelect;

    new-instance v0, Lepson/print/ActivityViewImageSelect$AllReloadTask;

    invoke-static {p1}, Lepson/print/ActivityViewImageSelect;->access$1100(Lepson/print/ActivityViewImageSelect;)Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, p1, v2}, Lepson/print/ActivityViewImageSelect$AllReloadTask;-><init>(Lepson/print/ActivityViewImageSelect;Landroid/content/Context;)V

    invoke-static {p1, v0}, Lepson/print/ActivityViewImageSelect;->access$1002(Lepson/print/ActivityViewImageSelect;Lepson/print/ActivityViewImageSelect$AllReloadTask;)Lepson/print/ActivityViewImageSelect$AllReloadTask;

    .line 1582
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$4;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {p1}, Lepson/print/ActivityViewImageSelect;->access$1000(Lepson/print/ActivityViewImageSelect;)Lepson/print/ActivityViewImageSelect$AllReloadTask;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 1583
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$4;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {p1}, Lepson/print/ActivityViewImageSelect;->access$1000(Lepson/print/ActivityViewImageSelect;)Lepson/print/ActivityViewImageSelect$AllReloadTask;

    move-result-object p1

    new-array v0, v3, [Ljava/lang/Void;

    invoke-virtual {p1, v0}, Lepson/print/ActivityViewImageSelect$AllReloadTask;->executeOnExecutor([Ljava/lang/Void;)Landroid/os/AsyncTask;

    .line 1585
    :cond_2
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$4;->this$0:Lepson/print/ActivityViewImageSelect;

    iget-object p1, p1, Lepson/print/ActivityViewImageSelect;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x12c

    invoke-virtual {p1, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 1559
    :pswitch_5
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$4;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {p1}, Lepson/print/ActivityViewImageSelect;->access$900(Lepson/print/ActivityViewImageSelect;)Lepson/common/DialogProgressViewModel;

    move-result-object p1

    const-string v0, "dialog_load"

    invoke-virtual {p1, v0}, Lepson/common/DialogProgressViewModel;->doDismiss(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1555
    :pswitch_6
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$4;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {p1}, Lepson/print/ActivityViewImageSelect;->access$900(Lepson/print/ActivityViewImageSelect;)Lepson/common/DialogProgressViewModel;

    move-result-object p1

    const-string v0, "dialog_load"

    invoke-virtual {p1, v0}, Lepson/common/DialogProgressViewModel;->doShow(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1656
    :cond_3
    iget-object v0, p0, Lepson/print/ActivityViewImageSelect$4;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object p1

    const-string v2, "PAPERSOURCEINFO"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object p1

    invoke-static {v0, p1}, Lepson/print/ActivityViewImageSelect;->access$2102(Lepson/print/ActivityViewImageSelect;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 1657
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$4;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {p1}, Lepson/print/ActivityViewImageSelect;->access$2100(Lepson/print/ActivityViewImageSelect;)Ljava/util/ArrayList;

    move-result-object p1

    const/16 v0, 0x8

    const v2, 0x7f05004a

    const v4, 0x7f05001e

    if-eqz p1, :cond_5

    .line 1658
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$4;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {p1}, Lepson/print/ActivityViewImageSelect;->access$1400(Lepson/print/ActivityViewImageSelect;)Landroid/widget/TextView;

    move-result-object p1

    iget-object v5, p0, Lepson/print/ActivityViewImageSelect$4;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-virtual {v5}, Lepson/print/ActivityViewImageSelect;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 1659
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$4;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {p1}, Lepson/print/ActivityViewImageSelect;->access$1400(Lepson/print/ActivityViewImageSelect;)Landroid/widget/TextView;

    move-result-object p1

    iget-object v2, p0, Lepson/print/ActivityViewImageSelect$4;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-virtual {v2}, Lepson/print/ActivityViewImageSelect;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1662
    new-instance p1, Lepson/print/screen/PrintSetting;

    iget-object v2, p0, Lepson/print/ActivityViewImageSelect$4;->this$0:Lepson/print/ActivityViewImageSelect;

    sget-object v4, Lepson/print/screen/PrintSetting$Kind;->photo:Lepson/print/screen/PrintSetting$Kind;

    invoke-direct {p1, v2, v4}, Lepson/print/screen/PrintSetting;-><init>(Landroid/content/Context;Lepson/print/screen/PrintSetting$Kind;)V

    .line 1663
    invoke-virtual {p1}, Lepson/print/screen/PrintSetting;->loadSettings()V

    .line 1665
    iget-object v2, p0, Lepson/print/ActivityViewImageSelect$4;->this$0:Lepson/print/ActivityViewImageSelect;

    iget-object v2, v2, Lepson/print/ActivityViewImageSelect;->paperSourceInfo:Lepson/print/screen/PaperSourceInfo;

    iget-object v4, p0, Lepson/print/ActivityViewImageSelect$4;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {v4}, Lepson/print/ActivityViewImageSelect;->access$2100(Lepson/print/ActivityViewImageSelect;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v2, p1, v4}, Lepson/print/screen/PaperSourceInfo;->checkPaperMissmatch(Lepson/print/screen/PrintSetting;Ljava/util/ArrayList;)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 1667
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$4;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {p1}, Lepson/print/ActivityViewImageSelect;->access$2200(Lepson/print/ActivityViewImageSelect;)Landroid/widget/ImageView;

    move-result-object p1

    invoke-virtual {p1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 1669
    :cond_4
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$4;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {p1}, Lepson/print/ActivityViewImageSelect;->access$2200(Lepson/print/ActivityViewImageSelect;)Landroid/widget/ImageView;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 1673
    :cond_5
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$4;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {p1}, Lepson/print/ActivityViewImageSelect;->access$1400(Lepson/print/ActivityViewImageSelect;)Landroid/widget/TextView;

    move-result-object p1

    iget-object v3, p0, Lepson/print/ActivityViewImageSelect$4;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-virtual {v3}, Lepson/print/ActivityViewImageSelect;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {p1, v3}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 1674
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$4;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {p1}, Lepson/print/ActivityViewImageSelect;->access$1400(Lepson/print/ActivityViewImageSelect;)Landroid/widget/TextView;

    move-result-object p1

    iget-object v3, p0, Lepson/print/ActivityViewImageSelect$4;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-virtual {v3}, Lepson/print/ActivityViewImageSelect;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1675
    iget-object p1, p0, Lepson/print/ActivityViewImageSelect$4;->this$0:Lepson/print/ActivityViewImageSelect;

    invoke-static {p1}, Lepson/print/ActivityViewImageSelect;->access$2200(Lepson/print/ActivityViewImageSelect;)Landroid/widget/ImageView;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    :pswitch_7
    return v1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_7
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x5
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

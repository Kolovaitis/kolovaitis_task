.class public Lepson/print/SimpleMessageDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "SimpleMessageDialogFragment.java"


# static fields
.field private static final PARAM_MESSAGE_RESOURCE_ID:Ljava/lang/String; = "message_id"


# instance fields
.field private mMessageId:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static newInstance(I)Lepson/print/SimpleMessageDialogFragment;
    .locals 3

    .line 21
    new-instance v0, Lepson/print/SimpleMessageDialogFragment;

    invoke-direct {v0}, Lepson/print/SimpleMessageDialogFragment;-><init>()V

    .line 23
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "message_id"

    .line 24
    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 25
    invoke-virtual {v0, v1}, Lepson/print/SimpleMessageDialogFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 32
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 34
    invoke-virtual {p0}, Lepson/print/SimpleMessageDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "message_id"

    .line 35
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lepson/print/SimpleMessageDialogFragment;->mMessageId:I

    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .line 41
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    .line 43
    new-instance p1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lepson/print/SimpleMessageDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {p1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget v0, p0, Lepson/print/SimpleMessageDialogFragment;->mMessageId:I

    .line 44
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    new-instance v0, Lepson/print/SimpleMessageDialogFragment$1;

    invoke-direct {v0, p0}, Lepson/print/SimpleMessageDialogFragment$1;-><init>(Lepson/print/SimpleMessageDialogFragment;)V

    const v1, 0x7f0e03ff

    invoke-virtual {p1, v1, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 51
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    const/4 v0, 0x0

    .line 52
    invoke-virtual {p1, v0}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    return-object p1
.end method

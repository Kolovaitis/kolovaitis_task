.class Lepson/print/ActivityDocsPrintPreview$7;
.super Lepson/print/Util/AsyncTaskExecutor;
.source "ActivityDocsPrintPreview.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/ActivityDocsPrintPreview;->onActivityResult(IILandroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lepson/print/Util/AsyncTaskExecutor<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/ActivityDocsPrintPreview;


# direct methods
.method constructor <init>(Lepson/print/ActivityDocsPrintPreview;)V
    .locals 0

    .line 1456
    iput-object p1, p0, Lepson/print/ActivityDocsPrintPreview$7;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-direct {p0}, Lepson/print/Util/AsyncTaskExecutor;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1456
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/print/ActivityDocsPrintPreview$7;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 0

    .line 1467
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$7;->this$0:Lepson/print/ActivityDocsPrintPreview;

    invoke-static {p1}, Lepson/print/ActivityDocsPrintPreview;->access$2000(Lepson/print/ActivityDocsPrintPreview;)V

    const/4 p1, 0x0

    return-object p1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 1456
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/print/ActivityDocsPrintPreview$7;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 1

    .line 1473
    invoke-super {p0, p1}, Lepson/print/Util/AsyncTaskExecutor;->onPostExecute(Ljava/lang/Object;)V

    .line 1475
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$7;->this$0:Lepson/print/ActivityDocsPrintPreview;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lepson/print/ActivityDocsPrintPreview;->enableProgressBar(Z)V

    .line 1480
    iget-object p1, p0, Lepson/print/ActivityDocsPrintPreview$7;->this$0:Lepson/print/ActivityDocsPrintPreview;

    const/4 v0, 0x1

    iput-boolean v0, p1, Lepson/print/ActivityDocsPrintPreview;->bAutoStartPrint:Z

    .line 1482
    iget-object p1, p1, Lepson/print/ActivityDocsPrintPreview;->mUiHandler:Landroid/os/Handler;

    const/16 v0, 0x21

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .line 1460
    invoke-super {p0}, Lepson/print/Util/AsyncTaskExecutor;->onPreExecute()V

    .line 1462
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$7;->this$0:Lepson/print/ActivityDocsPrintPreview;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lepson/print/ActivityDocsPrintPreview;->enableProgressBar(Z)V

    return-void
.end method

.class public Lepson/print/IprintApplication;
.super Landroid/support/multidex/MultiDexApplication;
.source "IprintApplication.java"


# static fields
.field private static STAGE_BETA:Ljava/lang/String; = "Beta"

.field private static STAGE_FC:Ljava/lang/String; = "FC"

.field private static STAGE_FFE:Ljava/lang/String; = "FFE"

.field private static sInstance:Lepson/print/IprintApplication;


# instance fields
.field private mPrinting:Z

.field private mTracker:Lcom/google/android/gms/analytics/Tracker;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Landroid/support/multidex/MultiDexApplication;-><init>()V

    return-void
.end method

.method public static declared-synchronized getInstance()Lepson/print/IprintApplication;
    .locals 2

    const-class v0, Lepson/print/IprintApplication;

    monitor-enter v0

    .line 23
    :try_start_0
    sget-object v1, Lepson/print/IprintApplication;->sInstance:Lepson/print/IprintApplication;

    if-nez v1, :cond_0

    .line 25
    new-instance v1, Lepson/print/IprintApplication;

    invoke-direct {v1}, Lepson/print/IprintApplication;-><init>()V

    sput-object v1, Lepson/print/IprintApplication;->sInstance:Lepson/print/IprintApplication;

    .line 27
    :cond_0
    sget-object v1, Lepson/print/IprintApplication;->sInstance:Lepson/print/IprintApplication;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static isConnectStaging()Z
    .locals 2

    const-string v0, "FC"

    .line 60
    sget-object v1, Lepson/print/IprintApplication;->STAGE_BETA:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "FC"

    sget-object v1, Lepson/print/IprintApplication;->STAGE_FFE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    return v0

    :cond_1
    :goto_0
    const/4 v0, 0x1

    return v0
.end method

.method public static isReleaseUnlimited()Z
    .locals 2

    const-string v0, "FC"

    .line 75
    sget-object v1, Lepson/print/IprintApplication;->STAGE_FFE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public declared-synchronized getDefaultTracker()Lcom/google/android/gms/analytics/Tracker;
    .locals 2

    monitor-enter p0

    .line 40
    :try_start_0
    iget-object v0, p0, Lepson/print/IprintApplication;->mTracker:Lcom/google/android/gms/analytics/Tracker;

    if-nez v0, :cond_0

    .line 41
    invoke-static {p0}, Lcom/google/android/gms/analytics/GoogleAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/android/gms/analytics/GoogleAnalytics;

    move-result-object v0

    const v1, 0x7f110001

    .line 42
    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/GoogleAnalytics;->newTracker(I)Lcom/google/android/gms/analytics/Tracker;

    move-result-object v0

    iput-object v0, p0, Lepson/print/IprintApplication;->mTracker:Lcom/google/android/gms/analytics/Tracker;

    .line 43
    iget-object v0, p0, Lepson/print/IprintApplication;->mTracker:Lcom/google/android/gms/analytics/Tracker;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/Tracker;->setAnonymizeIp(Z)V

    .line 49
    :cond_0
    iget-object v0, p0, Lepson/print/IprintApplication;->mTracker:Lcom/google/android/gms/analytics/Tracker;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getGoEpsonServerName()Ljava/lang/String;
    .locals 1

    const v0, 0x7f0e038d

    .line 83
    invoke-virtual {p0, v0}, Lepson/print/IprintApplication;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPrinting()Z
    .locals 1

    .line 91
    iget-boolean v0, p0, Lepson/print/IprintApplication;->mPrinting:Z

    return v0
.end method

.method public onCreate()V
    .locals 1

    .line 32
    invoke-super {p0}, Landroid/support/multidex/MultiDexApplication;->onCreate()V

    .line 33
    sput-object p0, Lepson/print/IprintApplication;->sInstance:Lepson/print/IprintApplication;

    const/4 v0, 0x0

    .line 35
    invoke-static {v0}, Lepson/print/Util/EPLog;->setDebuggable(Z)V

    .line 36
    invoke-static {v0}, Lcom/epson/mobilephone/common/EpLog;->setDebuggable(Z)V

    return-void
.end method

.method public setPrinting(Z)V
    .locals 0

    .line 87
    iput-boolean p1, p0, Lepson/print/IprintApplication;->mPrinting:Z

    return-void
.end method

.class public Lepson/print/EPPrinterManager;
.super Ljava/lang/Object;
.source "EPPrinterManager.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "EPPrinterManager"

.field private static deletedRemotePrinterList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lepson/print/EPPrinterInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static ipPrinterList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lepson/print/EPPrinterInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static remotePrinterList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lepson/print/EPPrinterInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field context:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    invoke-static {p1}, Lcom/epson/iprint/storage/StorageSecureStore;->initSharedSecureStore(Landroid/content/Context;)V

    .line 38
    iput-object p1, p0, Lepson/print/EPPrinterManager;->context:Landroid/content/Context;

    return-void
.end method

.method public static checkV3RemotePrinterData(Landroid/content/Context;)Z
    .locals 2

    .line 596
    new-instance v0, Lepson/print/EPPrinterManager;

    invoke-direct {v0, p0}, Lepson/print/EPPrinterManager;-><init>(Landroid/content/Context;)V

    .line 597
    invoke-virtual {v0}, Lepson/print/EPPrinterManager;->getRemotePrinterList()Ljava/util/ArrayList;

    move-result-object p0

    .line 599
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p0

    .line 600
    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 601
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/print/EPPrinterInfo;

    .line 602
    iget-object v1, v0, Lepson/print/EPPrinterInfo;->userDefName:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v0, v0, Lepson/print/EPPrinterInfo;->userDefName:Ljava/lang/String;

    .line 603
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_0

    :cond_1
    const/4 p0, 0x1

    return p0

    :cond_2
    const/4 p0, 0x0

    return p0
.end method

.method public static convertV3RemotePrinter(Landroid/content/Context;)V
    .locals 6

    .line 561
    new-instance v0, Lepson/print/EPPrinterManager;

    invoke-direct {v0, p0}, Lepson/print/EPPrinterManager;-><init>(Landroid/content/Context;)V

    .line 562
    invoke-virtual {v0}, Lepson/print/EPPrinterManager;->getRemotePrinterList()Ljava/util/ArrayList;

    move-result-object v1

    const/4 v2, 0x0

    .line 564
    :goto_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_3

    .line 565
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lepson/print/EPPrinterInfo;

    .line 568
    iget-object v4, v3, Lepson/print/EPPrinterInfo;->userDefName:Ljava/lang/String;

    if-eqz v4, :cond_0

    iget-object v4, v3, Lepson/print/EPPrinterInfo;->userDefName:Ljava/lang/String;

    .line 569
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-gtz v4, :cond_2

    .line 570
    :cond_0
    iget-object v4, v3, Lepson/print/EPPrinterInfo;->printerEmailAddress:Ljava/lang/String;

    iget-object v5, v3, Lepson/print/EPPrinterInfo;->printerAccessKey:Ljava/lang/String;

    .line 571
    invoke-static {p0, v4, v5}, Lepson/print/ecclient/EpsonConnectAccess;->getPrinterInfo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lepson/print/ecclient/EpsonConnectAccess$RemotePrinterInfo;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 577
    iget-object v5, v3, Lepson/print/EPPrinterInfo;->printerName:Ljava/lang/String;

    iput-object v5, v3, Lepson/print/EPPrinterInfo;->userDefName:Ljava/lang/String;

    .line 578
    iget-object v5, v4, Lepson/print/ecclient/EpsonConnectAccess$RemotePrinterInfo;->mDeviceId:Ljava/lang/String;

    iput-object v5, v3, Lepson/print/EPPrinterInfo;->printerName:Ljava/lang/String;

    .line 579
    iget-object v4, v4, Lepson/print/ecclient/EpsonConnectAccess$RemotePrinterInfo;->mSerialNumber:Ljava/lang/String;

    iput-object v4, v3, Lepson/print/EPPrinterInfo;->printerSerialNo:Ljava/lang/String;

    .line 582
    :cond_1
    invoke-virtual {v1, v2, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 586
    :cond_3
    invoke-virtual {v0}, Lepson/print/EPPrinterManager;->commitRemotePrinterInfo()V

    return-void
.end method

.method private loadPrinterList(Ljava/lang/String;I)Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/ArrayList<",
            "Lepson/print/EPPrinterInfo;",
            ">;"
        }
    .end annotation

    .line 108
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 113
    :try_start_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 114
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object p1

    if-nez p1, :cond_0

    return-object v0

    .line 120
    :cond_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v1, 0x0

    .line 121
    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_8

    .line 122
    new-instance v2, Lepson/print/EPPrinterInfo;

    invoke-direct {v2}, Lepson/print/EPPrinterInfo;-><init>()V

    .line 124
    new-instance v3, Ljava/io/FileInputStream;

    aget-object v4, p1, v1

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_5

    const/4 v4, 0x0

    .line 129
    :try_start_1
    invoke-virtual {v2, v3}, Lepson/print/EPPrinterInfo;->readJSON(Ljava/io/FileInputStream;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 153
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_5

    move-object v4, v2

    goto :goto_4

    :catchall_0
    move-exception p1

    goto/16 :goto_6

    :catch_0
    move-exception v2

    .line 146
    :try_start_3
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 153
    :goto_1
    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_5

    goto :goto_4

    :catch_1
    move-exception v2

    .line 132
    :try_start_5
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    .line 133
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 137
    :try_start_6
    new-instance v3, Ljava/io/FileInputStream;

    aget-object v2, p1, v1

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 138
    :try_start_7
    new-instance v2, Ljava/io/ObjectInputStream;

    invoke-direct {v2, v3}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 139
    :try_start_8
    invoke-virtual {v2}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lepson/print/EPPrinterInfo;
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    move-object v4, v5

    goto :goto_3

    :catch_2
    move-exception v5

    goto :goto_2

    :catch_3
    move-exception v5

    move-object v2, v4

    goto :goto_2

    :catchall_1
    move-exception p1

    move-object v3, v4

    goto :goto_6

    :catch_4
    move-exception v5

    move-object v2, v4

    move-object v3, v2

    .line 142
    :goto_2
    :try_start_9
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    :goto_3
    if-eqz v2, :cond_1

    .line 150
    :try_start_a
    invoke-virtual {v2}, Ljava/io/ObjectInputStream;->close()V

    :cond_1
    if-eqz v3, :cond_2

    goto :goto_1

    :cond_2
    :goto_4
    if-eqz v4, :cond_5

    const/4 v2, 0x2

    if-ne p2, v2, :cond_4

    .line 160
    invoke-static {}, Lcom/epson/iprint/storage/StorageSecureStore;->getSharedSecureStore()Lcom/epson/iprint/storage/StorageSecureStore;

    move-result-object v2

    .line 161
    iget-object v3, v4, Lepson/print/EPPrinterInfo;->printerEmailAddress:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/epson/iprint/storage/StorageSecureStore;->fetch(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lepson/print/EPPrinterInfo;->printerAccessKey:Ljava/lang/String;

    .line 162
    iget-object v2, v4, Lepson/print/EPPrinterInfo;->printerAccessKey:Ljava/lang/String;

    if-eqz v2, :cond_3

    const-string v2, "EPPrinterManager"

    .line 163
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Success: fetch printerAccessKey. ("

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, v4, Lepson/print/EPPrinterInfo;->printerEmailAddress:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, v4, Lepson/print/EPPrinterInfo;->printerAccessKey:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, ")"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    :cond_3
    const-string v2, ""

    .line 165
    iput-object v2, v4, Lepson/print/EPPrinterInfo;->printerAccessKey:Ljava/lang/String;

    .line 168
    :cond_4
    :goto_5
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    :catchall_2
    move-exception p1

    move-object v4, v2

    :goto_6
    if-eqz v4, :cond_6

    .line 150
    invoke-virtual {v4}, Ljava/io/ObjectInputStream;->close()V

    :cond_6
    if-eqz v3, :cond_7

    .line 153
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    :cond_7
    throw p1
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_5

    :catch_5
    const-string p1, "EPPrinterManager"

    const-string p2, "loadPrinterList(): error."

    .line 172
    invoke-static {p1, p2}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    return-object v0
.end method

.method private serializePrinterInfo(Ljava/lang/String;Ljava/lang/String;Lepson/print/EPPrinterInfo;)V
    .locals 5

    const/4 v0, 0x0

    .line 439
    :try_start_0
    new-instance v1, Ljava/io/File;

    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 441
    iget v2, p3, Lepson/print/EPPrinterInfo;->printerLocation:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 444
    invoke-static {}, Lcom/epson/iprint/storage/StorageSecureStore;->getSharedSecureStore()Lcom/epson/iprint/storage/StorageSecureStore;

    move-result-object v2

    .line 446
    invoke-virtual {v2, p2}, Lcom/epson/iprint/storage/StorageSecureStore;->revoke(Ljava/lang/String;)Z

    .line 447
    iget-object v3, p3, Lepson/print/EPPrinterInfo;->printerAccessKey:Ljava/lang/String;

    invoke-virtual {v2, p2, v3}, Lcom/epson/iprint/storage/StorageSecureStore;->put(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    const-string v2, "EPPrinterManager"

    .line 449
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Success: put printerAccessKey. ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, " = "

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p2, p3, Lepson/print/EPPrinterInfo;->printerAccessKey:Ljava/lang/String;

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, ")"

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {v2, p2}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string p2, ""

    .line 451
    iput-object p2, p3, Lepson/print/EPPrinterInfo;->printerAccessKey:Ljava/lang/String;

    .line 455
    :cond_1
    new-instance p2, Ljava/io/File;

    invoke-direct {p2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 456
    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result p1

    if-nez p1, :cond_2

    .line 457
    invoke-virtual {p2}, Ljava/io/File;->mkdirs()Z

    .line 460
    :cond_2
    new-instance p1, Ljava/io/FileOutputStream;

    invoke-direct {p1, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 466
    :try_start_1
    invoke-virtual {p3, p1}, Lepson/print/EPPrinterInfo;->writeJSON(Ljava/io/FileOutputStream;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-object v0, p1

    goto :goto_0

    :catch_1
    nop

    :goto_0
    const-string p1, "EPPrinterManager"

    const-string p2, "savePrinterInfo(): writeObject error."

    .line 469
    invoke-static {p1, p2}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v0, :cond_3

    .line 473
    :try_start_2
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    :cond_3
    :goto_1
    return-void
.end method


# virtual methods
.method checkModifiedIP()V
    .locals 8

    .line 359
    iget-object v0, p0, Lepson/print/EPPrinterManager;->context:Landroid/content/Context;

    invoke-static {v0}, Lepson/print/MyPrinter;->getCurPrinter(Landroid/content/Context;)Lepson/print/MyPrinter;

    move-result-object v0

    .line 361
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getLocation()I

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x3

    if-ne v1, v4, :cond_3

    .line 365
    invoke-virtual {p0}, Lepson/print/EPPrinterManager;->getIpPrinterList()Ljava/util/ArrayList;

    move-result-object v1

    .line 366
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lepson/print/EPPrinterInfo;

    .line 367
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getPrinterId()Ljava/lang/String;

    move-result-object v6

    iget-object v7, v5, Lepson/print/EPPrinterInfo;->printerID:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 370
    invoke-virtual {v0}, Lepson/print/MyPrinter;->getIp()Ljava/lang/String;

    move-result-object v1

    iget-object v6, v5, Lepson/print/EPPrinterInfo;->printerIP:Ljava/lang/String;

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 372
    iget-object v1, v5, Lepson/print/EPPrinterInfo;->printerIP:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lepson/print/MyPrinter;->setIp(Ljava/lang/String;)V

    .line 373
    iget-object v1, p0, Lepson/print/EPPrinterManager;->context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lepson/print/MyPrinter;->setCurPrinter(Landroid/content/Context;)V

    const-string v0, "EPPrinterManager"

    const-string v1, "IP printer\'s IP changed"

    .line 374
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_3

    .line 383
    iget-object v0, p0, Lepson/print/EPPrinterManager;->context:Landroid/content/Context;

    invoke-static {v0}, Lepson/print/MyPrinter;->clearCurPrinter(Landroid/content/Context;)V

    const-string v0, "EPPrinterManager"

    const-string v1, "IP printer deleted"

    .line 384
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 390
    :cond_3
    iget-object v0, p0, Lepson/print/EPPrinterManager;->context:Landroid/content/Context;

    const-string v1, "epson.scanner.SelectedScanner"

    const-string v5, "SCAN_REFS_SCANNER_ACCESSPATH"

    invoke-static {v0, v1, v5}, Lepson/common/Utils;->getPrefInt(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-ne v0, v4, :cond_7

    .line 395
    iget-object v0, p0, Lepson/print/EPPrinterManager;->context:Landroid/content/Context;

    const-string v1, "epson.scanner.SelectedScanner"

    const-string v4, "SCAN_REFS_SCANNER_ID"

    invoke-static {v0, v1, v4}, Lepson/common/Utils;->getPrefString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 397
    iget-object v1, p0, Lepson/print/EPPrinterManager;->context:Landroid/content/Context;

    const-string v4, "epson.scanner.SelectedScanner"

    const-string v5, "SCAN_REFS_SCANNER_LOCATION"

    invoke-static {v1, v4, v5}, Lepson/common/Utils;->getPrefString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 402
    invoke-virtual {p0}, Lepson/print/EPPrinterManager;->getIpPrinterList()Ljava/util/ArrayList;

    move-result-object v4

    .line 403
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lepson/print/EPPrinterInfo;

    .line 404
    iget-object v6, v5, Lepson/print/EPPrinterInfo;->scannerID:Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 407
    iget-object v0, v5, Lepson/print/EPPrinterInfo;->printerIP:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 409
    iget-object v0, p0, Lepson/print/EPPrinterManager;->context:Landroid/content/Context;

    const-string v1, "epson.scanner.SelectedScanner"

    const-string v3, "SCAN_REFS_SCANNER_LOCATION"

    iget-object v4, v5, Lepson/print/EPPrinterInfo;->printerIP:Ljava/lang/String;

    invoke-static {v0, v1, v3, v4}, Lepson/common/Utils;->savePref(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "EPPrinterManager"

    const-string v1, "IP scanner\'s IP changed"

    .line 410
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    const/4 v2, 0x0

    :cond_6
    :goto_1
    if-nez v2, :cond_7

    .line 419
    iget-object v0, p0, Lepson/print/EPPrinterManager;->context:Landroid/content/Context;

    invoke-static {v0}, Lepson/scan/lib/ScanSettingHelper;->saveErroredBlankSettings(Landroid/content/Context;)V

    const-string v0, "EPPrinterManager"

    const-string v1, "IP scanner deleted"

    .line 420
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    return-void
.end method

.method public commitIPPrinterInfo()V
    .locals 5

    .line 328
    sget-object v0, Lepson/print/EPPrinterManager;->ipPrinterList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const-string v0, "EPPrinterManager"

    const-string v1, "commitIPPrinterInfo() called"

    .line 332
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    iget-object v0, p0, Lepson/print/EPPrinterManager;->context:Landroid/content/Context;

    invoke-static {v0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v0

    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->clearIpPrintersInfo()V

    .line 338
    invoke-virtual {p0}, Lepson/print/EPPrinterManager;->getIpPrinterList()Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    .line 339
    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 340
    iget-object v2, p0, Lepson/print/EPPrinterManager;->context:Landroid/content/Context;

    invoke-static {v2}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v2

    invoke-virtual {v2}, Lepson/common/ExternalFileUtils;->getIpPrintersInfo()Ljava/lang/String;

    move-result-object v2

    .line 341
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lepson/print/EPPrinterInfo;

    iget-object v3, v3, Lepson/print/EPPrinterInfo;->printerID:Ljava/lang/String;

    .line 342
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lepson/print/EPPrinterInfo;

    .line 340
    invoke-direct {p0, v2, v3, v4}, Lepson/print/EPPrinterManager;->serializePrinterInfo(Ljava/lang/String;Ljava/lang/String;Lepson/print/EPPrinterInfo;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 346
    :cond_1
    invoke-virtual {p0}, Lepson/print/EPPrinterManager;->checkModifiedIP()V

    .line 348
    invoke-virtual {p0}, Lepson/print/EPPrinterManager;->rollbackIPPrinterInfo()V

    return-void
.end method

.method public commitRemotePrinterInfo()V
    .locals 6

    .line 282
    sget-object v0, Lepson/print/EPPrinterManager;->remotePrinterList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const-string v0, "EPPrinterManager"

    const-string v1, "commitRemotePrinterInfo() called"

    .line 286
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    sget-object v0, Lepson/print/EPPrinterManager;->deletedRemotePrinterList:Ljava/util/ArrayList;

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    .line 290
    :goto_0
    sget-object v2, Lepson/print/EPPrinterManager;->deletedRemotePrinterList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 291
    sget-object v2, Lepson/print/EPPrinterManager;->deletedRemotePrinterList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lepson/print/EPPrinterInfo;

    iget-object v2, v2, Lepson/print/EPPrinterInfo;->printerEmailAddress:Ljava/lang/String;

    .line 294
    invoke-static {}, Lcom/epson/iprint/storage/StorageSecureStore;->getSharedSecureStore()Lcom/epson/iprint/storage/StorageSecureStore;

    move-result-object v3

    .line 295
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/epson/iprint/storage/StorageSecureStore;->revoke(Ljava/lang/String;)Z

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    const-string v3, "EPPrinterManager"

    .line 297
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Success: remove printerAccessKey. ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ")"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 303
    :cond_2
    iget-object v0, p0, Lepson/print/EPPrinterManager;->context:Landroid/content/Context;

    invoke-static {v0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v0

    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->clearRemotePrintersInfo()V

    .line 306
    invoke-virtual {p0}, Lepson/print/EPPrinterManager;->getRemotePrinterList()Ljava/util/ArrayList;

    move-result-object v0

    .line 307
    :goto_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_3

    .line 308
    iget-object v2, p0, Lepson/print/EPPrinterManager;->context:Landroid/content/Context;

    invoke-static {v2}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v2

    invoke-virtual {v2}, Lepson/common/ExternalFileUtils;->getRemotePrintersInfo()Ljava/lang/String;

    move-result-object v2

    .line 309
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lepson/print/EPPrinterInfo;

    iget-object v3, v3, Lepson/print/EPPrinterInfo;->printerEmailAddress:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    .line 310
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lepson/print/EPPrinterInfo;

    .line 308
    invoke-direct {p0, v2, v3, v4}, Lepson/print/EPPrinterManager;->serializePrinterInfo(Ljava/lang/String;Ljava/lang/String;Lepson/print/EPPrinterInfo;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 313
    :cond_3
    invoke-virtual {p0}, Lepson/print/EPPrinterManager;->rollbackRemotePrinterInfo()V

    return-void
.end method

.method public deleteIpPrinterInfo(Ljava/lang/String;)Z
    .locals 4

    .line 512
    invoke-virtual {p0}, Lepson/print/EPPrinterManager;->getIpPrinterList()Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 515
    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 516
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lepson/print/EPPrinterInfo;

    iget-object v3, v3, Lepson/print/EPPrinterInfo;->printerID:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 517
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    const/4 p1, 0x1

    return p1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const-string v0, "EPPrinterManager"

    .line 522
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "deleteIpPrinterInfo(): Couldn\'t delete - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "."

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1
.end method

.method public deleteRemotePrinterInfo(Ljava/lang/String;)Z
    .locals 5

    .line 486
    invoke-virtual {p0}, Lepson/print/EPPrinterManager;->getRemotePrinterList()Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 489
    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 490
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lepson/print/EPPrinterInfo;

    iget-object v3, v3, Lepson/print/EPPrinterInfo;->printerEmailAddress:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 492
    sget-object p1, Lepson/print/EPPrinterManager;->deletedRemotePrinterList:Ljava/util/ArrayList;

    if-nez p1, :cond_0

    .line 493
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    sput-object p1, Lepson/print/EPPrinterManager;->deletedRemotePrinterList:Ljava/util/ArrayList;

    .line 495
    :cond_0
    sget-object p1, Lepson/print/EPPrinterManager;->deletedRemotePrinterList:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 497
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    const/4 p1, 0x1

    return p1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    const-string v0, "EPPrinterManager"

    .line 503
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "deleteRemotePrinterInfo(): Couldn\'t delete - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "."

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    return v1
.end method

.method public doseContainScannerIdInIpPrinterList(Ljava/lang/String;)Z
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_3

    .line 92
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 96
    :cond_0
    invoke-virtual {p0}, Lepson/print/EPPrinterManager;->getIpPrinterList()Ljava/util/ArrayList;

    move-result-object v1

    .line 97
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lepson/print/EPPrinterInfo;

    .line 98
    iget-object v2, v2, Lepson/print/EPPrinterInfo;->scannerID:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 p1, 0x1

    return p1

    :cond_2
    return v0

    :cond_3
    :goto_0
    return v0
.end method

.method public getIpPrinterList()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lepson/print/EPPrinterInfo;",
            ">;"
        }
    .end annotation

    .line 54
    sget-object v0, Lepson/print/EPPrinterManager;->ipPrinterList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    return-object v0

    .line 57
    :cond_0
    iget-object v0, p0, Lepson/print/EPPrinterManager;->context:Landroid/content/Context;

    invoke-static {v0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v0

    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->getIpPrintersInfo()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    invoke-direct {p0, v0, v1}, Lepson/print/EPPrinterManager;->loadPrinterList(Ljava/lang/String;I)Ljava/util/ArrayList;

    move-result-object v0

    sput-object v0, Lepson/print/EPPrinterManager;->ipPrinterList:Ljava/util/ArrayList;

    .line 58
    sget-object v0, Lepson/print/EPPrinterManager;->ipPrinterList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getIpPrinterUserName(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    const/4 v0, 0x0

    if-eqz p1, :cond_3

    .line 68
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 72
    :cond_0
    invoke-virtual {p0}, Lepson/print/EPPrinterManager;->getIpPrinterList()Ljava/util/ArrayList;

    move-result-object v1

    .line 73
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lepson/print/EPPrinterInfo;

    .line 74
    iget-object v3, v2, Lepson/print/EPPrinterInfo;->scannerID:Ljava/lang/String;

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 75
    iget-object p1, v2, Lepson/print/EPPrinterInfo;->userDefName:Ljava/lang/String;

    return-object p1

    :cond_2
    return-object v0

    :cond_3
    :goto_0
    return-object v0
.end method

.method public getRemotePrinterList()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lepson/print/EPPrinterInfo;",
            ">;"
        }
    .end annotation

    .line 44
    sget-object v0, Lepson/print/EPPrinterManager;->remotePrinterList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    return-object v0

    .line 47
    :cond_0
    iget-object v0, p0, Lepson/print/EPPrinterManager;->context:Landroid/content/Context;

    invoke-static {v0}, Lepson/common/ExternalFileUtils;->getInstance(Landroid/content/Context;)Lepson/common/ExternalFileUtils;

    move-result-object v0

    invoke-virtual {v0}, Lepson/common/ExternalFileUtils;->getRemotePrintersInfo()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-direct {p0, v0, v1}, Lepson/print/EPPrinterManager;->loadPrinterList(Ljava/lang/String;I)Ljava/util/ArrayList;

    move-result-object v0

    sput-object v0, Lepson/print/EPPrinterManager;->remotePrinterList:Ljava/util/ArrayList;

    .line 48
    sget-object v0, Lepson/print/EPPrinterManager;->remotePrinterList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public loadIpPrinterInfo(Ljava/lang/String;)Lepson/print/EPPrinterInfo;
    .locals 3

    .line 195
    invoke-virtual {p0}, Lepson/print/EPPrinterManager;->getIpPrinterList()Ljava/util/ArrayList;

    move-result-object v0

    .line 196
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lepson/print/EPPrinterInfo;

    .line 197
    iget-object v2, v1, Lepson/print/EPPrinterInfo;->printerID:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    return-object v1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public loadRemotePrinterInfo(Ljava/lang/String;)Lepson/print/EPPrinterInfo;
    .locals 4

    .line 182
    invoke-virtual {p0}, Lepson/print/EPPrinterManager;->getRemotePrinterList()Ljava/util/ArrayList;

    move-result-object v0

    .line 183
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lepson/print/EPPrinterInfo;

    .line 184
    iget-object v2, v1, Lepson/print/EPPrinterInfo;->printerEmailAddress:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    return-object v1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public rollbackIPPrinterInfo()V
    .locals 2

    const-string v0, "EPPrinterManager"

    const-string v1, "rollbackIPPrinterInfo() called"

    .line 429
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 430
    sput-object v0, Lepson/print/EPPrinterManager;->ipPrinterList:Ljava/util/ArrayList;

    return-void
.end method

.method public rollbackRemotePrinterInfo()V
    .locals 2

    const-string v0, "EPPrinterManager"

    const-string v1, "rollbackRemotePrinterInfo() called"

    .line 317
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 318
    sput-object v0, Lepson/print/EPPrinterManager;->remotePrinterList:Ljava/util/ArrayList;

    .line 319
    sput-object v0, Lepson/print/EPPrinterManager;->deletedRemotePrinterList:Ljava/util/ArrayList;

    return-void
.end method

.method public saveIPPrinterInfo(Ljava/lang/String;Lepson/print/EPPrinterInfo;)V
    .locals 3

    const/4 v0, 0x3

    .line 260
    iput v0, p2, Lepson/print/EPPrinterInfo;->printerLocation:I

    .line 262
    invoke-virtual {p0}, Lepson/print/EPPrinterManager;->getIpPrinterList()Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    .line 266
    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 267
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lepson/print/EPPrinterInfo;

    iget-object v2, v2, Lepson/print/EPPrinterInfo;->printerID:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 268
    invoke-virtual {v0, v1, p2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-void

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 272
    :cond_1
    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public saveRemotePrinterInfo(Lepson/print/EPPrinterInfo;)V
    .locals 4

    const/4 v0, 0x2

    .line 241
    iput v0, p1, Lepson/print/EPPrinterInfo;->printerLocation:I

    .line 243
    invoke-virtual {p0}, Lepson/print/EPPrinterManager;->getRemotePrinterList()Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    .line 247
    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 248
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lepson/print/EPPrinterInfo;

    iget-object v2, v2, Lepson/print/EPPrinterInfo;->printerEmailAddress:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Lepson/print/EPPrinterInfo;->printerEmailAddress:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 249
    invoke-virtual {v0, v1, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-void

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 253
    :cond_1
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.class public Lepson/print/DrawCDLabel;
.super Landroid/view/View;
.source "DrawCDLabel.java"

# interfaces
.implements Lepson/print/CommonDefine;


# instance fields
.field private Center:Landroid/graphics/Point;

.field private mRadiusIn:F

.field private mRadiusOut:F

.field private viewHeight:I

.field private viewWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 22
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 14
    new-instance p1, Landroid/graphics/Point;

    invoke-direct {p1}, Landroid/graphics/Point;-><init>()V

    iput-object p1, p0, Lepson/print/DrawCDLabel;->Center:Landroid/graphics/Point;

    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .line 35
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 36
    invoke-virtual {p0}, Lepson/print/DrawCDLabel;->getWidth()I

    move-result v0

    iput v0, p0, Lepson/print/DrawCDLabel;->viewWidth:I

    .line 37
    invoke-virtual {p0}, Lepson/print/DrawCDLabel;->getHeight()I

    move-result v0

    iput v0, p0, Lepson/print/DrawCDLabel;->viewHeight:I

    .line 38
    iget-object v0, p0, Lepson/print/DrawCDLabel;->Center:Landroid/graphics/Point;

    iget v1, p0, Lepson/print/DrawCDLabel;->viewWidth:I

    div-int/lit8 v1, v1, 0x2

    iput v1, v0, Landroid/graphics/Point;->x:I

    .line 39
    iget v1, p0, Lepson/print/DrawCDLabel;->viewHeight:I

    div-int/lit8 v1, v1, 0x2

    iput v1, v0, Landroid/graphics/Point;->y:I

    .line 46
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    const v1, -0x1d1d1e

    .line 47
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 48
    iget-object v1, p0, Lepson/print/DrawCDLabel;->Center:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    iget-object v2, p0, Lepson/print/DrawCDLabel;->Center:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    iget v3, p0, Lepson/print/DrawCDLabel;->mRadiusIn:F

    invoke-virtual {p1, v1, v2, v3, v0}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 49
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    .line 50
    iget-object v2, p0, Lepson/print/DrawCDLabel;->Center:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    iget-object v3, p0, Lepson/print/DrawCDLabel;->Center:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v3, v3

    iget v4, p0, Lepson/print/DrawCDLabel;->mRadiusOut:F

    sget-object v5, Landroid/graphics/Path$Direction;->CCW:Landroid/graphics/Path$Direction;

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 51
    sget-object v2, Landroid/graphics/Path$FillType;->INVERSE_WINDING:Landroid/graphics/Path$FillType;

    invoke-virtual {v1, v2}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 52
    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    return-void
.end method

.method public setRadiusIn(F)V
    .locals 0

    .line 26
    iput p1, p0, Lepson/print/DrawCDLabel;->mRadiusIn:F

    return-void
.end method

.method public setRadiusOut(F)V
    .locals 0

    .line 30
    iput p1, p0, Lepson/print/DrawCDLabel;->mRadiusOut:F

    return-void
.end method

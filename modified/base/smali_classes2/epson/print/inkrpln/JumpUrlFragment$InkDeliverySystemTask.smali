.class Lepson/print/inkrpln/JumpUrlFragment$InkDeliverySystemTask;
.super Landroid/os/AsyncTask;
.source "JumpUrlFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/inkrpln/JumpUrlFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InkDeliverySystemTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field private mDependencyBuilder:Lepson/print/inkrpln/DependencyBuilder;

.field final synthetic this$0:Lepson/print/inkrpln/JumpUrlFragment;


# direct methods
.method public constructor <init>(Lepson/print/inkrpln/JumpUrlFragment;Lepson/print/inkrpln/DependencyBuilder;)V
    .locals 0

    .line 182
    iput-object p1, p0, Lepson/print/inkrpln/JumpUrlFragment$InkDeliverySystemTask;->this$0:Lepson/print/inkrpln/JumpUrlFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 183
    iput-object p2, p0, Lepson/print/inkrpln/JumpUrlFragment$InkDeliverySystemTask;->mDependencyBuilder:Lepson/print/inkrpln/DependencyBuilder;

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Landroid/net/Uri;
    .locals 2

    .line 188
    iget-object p1, p0, Lepson/print/inkrpln/JumpUrlFragment$InkDeliverySystemTask;->mDependencyBuilder:Lepson/print/inkrpln/DependencyBuilder;

    if-nez p1, :cond_0

    .line 190
    new-instance p1, Lepson/print/inkrpln/DependencyBuilder;

    invoke-direct {p1}, Lepson/print/inkrpln/DependencyBuilder;-><init>()V

    .line 192
    :cond_0
    new-instance v0, Lepson/print/inkrpln/InkReplenishSystem;

    .line 193
    invoke-virtual {p1}, Lepson/print/inkrpln/DependencyBuilder;->createPrinterAdapter()Lepson/print/inkrpln/PrinterAdapter;

    move-result-object v1

    .line 194
    invoke-virtual {p1}, Lepson/print/inkrpln/DependencyBuilder;->createInkRplnRepository()Lepson/print/inkrpln/InkRplnRepository;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lepson/print/inkrpln/InkReplenishSystem;-><init>(Lepson/print/inkrpln/PrinterAdapter;Lepson/print/inkrpln/InkRplnRepository;)V

    .line 195
    invoke-static {}, Lepson/print/IprintApplication;->getInstance()Lepson/print/IprintApplication;

    move-result-object p1

    .line 197
    invoke-virtual {v0, p1}, Lepson/print/inkrpln/InkReplenishSystem;->refreshReplenInfoAndGetJumpUrl(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 179
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/print/inkrpln/JumpUrlFragment$InkDeliverySystemTask;->doInBackground([Ljava/lang/Void;)Landroid/net/Uri;

    move-result-object p1

    return-object p1
.end method

.method protected onPostExecute(Landroid/net/Uri;)V
    .locals 1

    if-nez p1, :cond_0

    .line 203
    iget-object p1, p0, Lepson/print/inkrpln/JumpUrlFragment$InkDeliverySystemTask;->this$0:Lepson/print/inkrpln/JumpUrlFragment;

    invoke-static {p1}, Lepson/print/inkrpln/JumpUrlFragment;->access$000(Lepson/print/inkrpln/JumpUrlFragment;)V

    return-void

    .line 206
    :cond_0
    iget-object v0, p0, Lepson/print/inkrpln/JumpUrlFragment$InkDeliverySystemTask;->this$0:Lepson/print/inkrpln/JumpUrlFragment;

    invoke-static {v0, p1}, Lepson/print/inkrpln/JumpUrlFragment;->access$100(Lepson/print/inkrpln/JumpUrlFragment;Landroid/net/Uri;)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 179
    check-cast p1, Landroid/net/Uri;

    invoke-virtual {p0, p1}, Lepson/print/inkrpln/JumpUrlFragment$InkDeliverySystemTask;->onPostExecute(Landroid/net/Uri;)V

    return-void
.end method

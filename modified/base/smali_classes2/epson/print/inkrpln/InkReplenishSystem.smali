.class public Lepson/print/inkrpln/InkReplenishSystem;
.super Ljava/lang/Object;
.source "InkReplenishSystem.java"


# static fields
.field private static sInkReplenishSystem:Lepson/print/inkrpln/InkReplenishSystem;


# instance fields
.field private mGoEpsonClient:Lepson/print/inkrpln/GoEpsonClient;

.field private mInkRsInfoClient:Lepson/print/inkrpln/InkRplnInfoClient;

.field private mIrsRepository:Lepson/print/inkrpln/InkRplnRepository;

.field private mPrinterAdapter:Lepson/print/inkrpln/PrinterAdapter;


# direct methods
.method public constructor <init>(Lepson/print/inkrpln/PrinterAdapter;Lepson/print/inkrpln/InkRplnRepository;)V
    .locals 0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lepson/print/inkrpln/InkReplenishSystem;->mPrinterAdapter:Lepson/print/inkrpln/PrinterAdapter;

    .line 51
    iput-object p2, p0, Lepson/print/inkrpln/InkReplenishSystem;->mIrsRepository:Lepson/print/inkrpln/InkRplnRepository;

    .line 52
    new-instance p1, Lepson/print/inkrpln/GoEpsonClient;

    invoke-direct {p1}, Lepson/print/inkrpln/GoEpsonClient;-><init>()V

    iput-object p1, p0, Lepson/print/inkrpln/InkReplenishSystem;->mGoEpsonClient:Lepson/print/inkrpln/GoEpsonClient;

    .line 53
    new-instance p1, Lepson/print/inkrpln/InkRplnInfoClient;

    invoke-direct {p1}, Lepson/print/inkrpln/InkRplnInfoClient;-><init>()V

    iput-object p1, p0, Lepson/print/inkrpln/InkReplenishSystem;->mInkRsInfoClient:Lepson/print/inkrpln/InkRplnInfoClient;

    return-void
.end method

.method constructor <init>(Lepson/print/inkrpln/PrinterAdapter;Lepson/print/inkrpln/InkRplnRepository;Lepson/print/inkrpln/GoEpsonClient;Lepson/print/inkrpln/InkRplnInfoClient;)V
    .locals 0
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lepson/print/inkrpln/InkReplenishSystem;->mPrinterAdapter:Lepson/print/inkrpln/PrinterAdapter;

    .line 44
    iput-object p2, p0, Lepson/print/inkrpln/InkReplenishSystem;->mIrsRepository:Lepson/print/inkrpln/InkRplnRepository;

    .line 45
    iput-object p3, p0, Lepson/print/inkrpln/InkReplenishSystem;->mGoEpsonClient:Lepson/print/inkrpln/GoEpsonClient;

    .line 46
    iput-object p4, p0, Lepson/print/inkrpln/InkReplenishSystem;->mInkRsInfoClient:Lepson/print/inkrpln/InkRplnInfoClient;

    return-void
.end method

.method static encodePrinterSerial(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    const-string v0, "US-ASCII"

    .line 250
    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p0

    const/4 v0, 0x2

    .line 251
    invoke-static {p0, v0}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static isInkReplenishSystemEnabled(Landroid/content/Context;)Z
    .locals 1
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 294
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const v0, 0x7f040005

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result p0

    return p0
.end method

.method static useGoEpsonTls(Landroid/content/Context;)Z
    .locals 1
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 298
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const v0, 0x7f040004

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result p0

    return p0
.end method


# virtual methods
.method checkPrinterAndNetworkConnectivity(Landroid/content/Context;)Z
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .line 141
    invoke-static {p1}, Lepson/common/Utils;->isConnectedWifi(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 147
    :cond_0
    invoke-static {p1}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->isSimpleAP(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    return v1

    .line 152
    :cond_1
    invoke-static {p1}, Lepson/common/Utils;->isOnline(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    return v1

    .line 156
    :cond_2
    iget-object v0, p0, Lepson/print/inkrpln/InkReplenishSystem;->mPrinterAdapter:Lepson/print/inkrpln/PrinterAdapter;

    invoke-virtual {v0, p1}, Lepson/print/inkrpln/PrinterAdapter;->isRemotePrinter(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_3

    return v1

    :cond_3
    const/4 p1, 0x1

    return p1
.end method

.method public declared-synchronized clearServerReplyPrinterInfo(Landroid/content/Context;)V
    .locals 1

    monitor-enter p0

    .line 165
    :try_start_0
    iget-object v0, p0, Lepson/print/inkrpln/InkReplenishSystem;->mIrsRepository:Lepson/print/inkrpln/InkRplnRepository;

    invoke-virtual {v0, p1}, Lepson/print/inkrpln/InkRplnRepository;->clearServerData(Landroid/content/Context;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 166
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method getGoEpsonParams(Landroid/content/Context;)Lepson/print/inkrpln/GoEpsonClient$ReadyInkParams;
    .locals 3
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .line 64
    new-instance v0, Lepson/print/inkrpln/GoEpsonClient$ReadyInkParams;

    invoke-direct {v0}, Lepson/print/inkrpln/GoEpsonClient$ReadyInkParams;-><init>()V

    .line 65
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    .line 66
    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lepson/print/inkrpln/GoEpsonClient$ReadyInkParams;->country:Ljava/lang/String;

    .line 67
    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lepson/print/inkrpln/GoEpsonClient$ReadyInkParams;->language:Ljava/lang/String;

    .line 69
    iget-object v1, p0, Lepson/print/inkrpln/InkReplenishSystem;->mPrinterAdapter:Lepson/print/inkrpln/PrinterAdapter;

    invoke-virtual {v1, p1}, Lepson/print/inkrpln/PrinterAdapter;->getSerialNo(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lepson/print/inkrpln/GoEpsonClient$ReadyInkParams;->printerSerial:Ljava/lang/String;

    .line 70
    iget-object v1, p0, Lepson/print/inkrpln/InkReplenishSystem;->mPrinterAdapter:Lepson/print/inkrpln/PrinterAdapter;

    invoke-virtual {v1, p1}, Lepson/print/inkrpln/PrinterAdapter;->getNonRemotePrinterDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, v0, Lepson/print/inkrpln/GoEpsonClient$ReadyInkParams;->printerName:Ljava/lang/String;

    .line 72
    sget-object p1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    iput-object p1, v0, Lepson/print/inkrpln/GoEpsonClient$ReadyInkParams;->androidModel:Ljava/lang/String;

    return-object v0
.end method

.method getReadyInkUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .line 237
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object p1

    if-eqz p2, :cond_0

    const-string v0, "sn"

    .line 239
    invoke-static {p2}, Lepson/print/inkrpln/InkReplenishSystem;->encodePrinterSerial(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, v0, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 242
    :cond_0
    invoke-virtual {p1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p1

    return-object p1
.end method

.method public declared-synchronized getReadyInkUriFromSavedData(Landroid/content/Context;)Landroid/net/Uri;
    .locals 3
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    monitor-enter p0

    .line 222
    :try_start_0
    iget-object v0, p0, Lepson/print/inkrpln/InkReplenishSystem;->mIrsRepository:Lepson/print/inkrpln/InkRplnRepository;

    invoke-virtual {v0, p1}, Lepson/print/inkrpln/InkRplnRepository;->getInfo(Landroid/content/Context;)Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    .line 223
    iget-object v2, v0, Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;->landingUrl:Ljava/lang/String;

    if-nez v2, :cond_0

    goto :goto_1

    .line 226
    :cond_0
    iget-object v2, p0, Lepson/print/inkrpln/InkReplenishSystem;->mPrinterAdapter:Lepson/print/inkrpln/PrinterAdapter;

    invoke-virtual {v2, p1}, Lepson/print/inkrpln/PrinterAdapter;->getSerialNo(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    .line 228
    iget-object v2, v0, Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;->landingUrl:Ljava/lang/String;

    iget-boolean v0, v0, Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;->appendSerial:Z

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    move-object p1, v1

    :goto_0
    invoke-virtual {p0, v2, p1}, Lepson/print/inkrpln/InkReplenishSystem;->getReadyInkUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    .line 224
    :cond_2
    :goto_1
    monitor-exit p0

    return-object v1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public isInSupportedArea()Z
    .locals 1

    .line 258
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    .line 259
    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    .line 260
    invoke-virtual {p0, v0}, Lepson/print/inkrpln/InkReplenishSystem;->isSupportedCountry(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method isSupportedCountry(Ljava/lang/String;)Z
    .locals 3
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    const/16 v0, 0x13

    .line 267
    new-array v0, v0, [Ljava/lang/String;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    .line 268
    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Ljava/util/Locale;->CANADA:Ljava/util/Locale;

    .line 269
    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Ljava/util/Locale;->UK:Ljava/util/Locale;

    .line 270
    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Ljava/util/Locale;->FRANCE:Ljava/util/Locale;

    .line 271
    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Ljava/util/Locale;->ITALY:Ljava/util/Locale;

    .line 272
    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sget-object v1, Ljava/util/Locale;->GERMANY:Ljava/util/Locale;

    .line 273
    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x5

    aput-object v1, v0, v2

    const-string v1, "ES"

    const/4 v2, 0x6

    aput-object v1, v0, v2

    const-string v1, "PT"

    const/4 v2, 0x7

    aput-object v1, v0, v2

    const-string v1, "NL"

    const/16 v2, 0x8

    aput-object v1, v0, v2

    const-string v1, "CZ"

    const/16 v2, 0x9

    aput-object v1, v0, v2

    const-string v1, "DK"

    const/16 v2, 0xa

    aput-object v1, v0, v2

    const-string v1, "FI"

    const/16 v2, 0xb

    aput-object v1, v0, v2

    const-string v1, "NO"

    const/16 v2, 0xc

    aput-object v1, v0, v2

    const-string v1, "PL"

    const/16 v2, 0xd

    aput-object v1, v0, v2

    const-string v1, "SE"

    const/16 v2, 0xe

    aput-object v1, v0, v2

    const-string v1, "AT"

    const/16 v2, 0xf

    aput-object v1, v0, v2

    const-string v1, "BE"

    const/16 v2, 0x10

    aput-object v1, v0, v2

    const-string v1, "IE"

    const/16 v2, 0x11

    aput-object v1, v0, v2

    const-string v1, "CH"

    const/16 v2, 0x12

    aput-object v1, v0, v2

    .line 290
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public declared-synchronized needsDisplayInvitationDialog(Landroid/content/Context;)Z
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    monitor-enter p0

    .line 173
    :try_start_0
    iget-object v0, p0, Lepson/print/inkrpln/InkReplenishSystem;->mIrsRepository:Lepson/print/inkrpln/InkRplnRepository;

    invoke-virtual {v0, p1}, Lepson/print/inkrpln/InkRplnRepository;->getInvitationDialogDisabledValue(Landroid/content/Context;)Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 174
    monitor-exit p0

    return v1

    .line 177
    :cond_0
    :try_start_1
    iget-object v0, p0, Lepson/print/inkrpln/InkReplenishSystem;->mIrsRepository:Lepson/print/inkrpln/InkRplnRepository;

    invoke-virtual {v0, p1}, Lepson/print/inkrpln/InkRplnRepository;->getInfo(Landroid/content/Context;)Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 178
    invoke-virtual {p1}, Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;->needToDisplayInvitationDialog()Z

    move-result p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz p1, :cond_1

    const/4 v1, 0x1

    :cond_1
    monitor-exit p0

    return v1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized refreshReplenInfoAndGetJumpUrl(Landroid/content/Context;)Landroid/net/Uri;
    .locals 4
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    monitor-enter p0

    .line 193
    :try_start_0
    iget-object v0, p0, Lepson/print/inkrpln/InkReplenishSystem;->mIrsRepository:Lepson/print/inkrpln/InkRplnRepository;

    invoke-virtual {v0, p1}, Lepson/print/inkrpln/InkRplnRepository;->getInkReplenishServerUrl(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 195
    monitor-exit p0

    return-object v1

    .line 198
    :cond_0
    :try_start_1
    iget-object v2, p0, Lepson/print/inkrpln/InkReplenishSystem;->mPrinterAdapter:Lepson/print/inkrpln/PrinterAdapter;

    invoke-virtual {v2, p1}, Lepson/print/inkrpln/PrinterAdapter;->getSerialNo(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 200
    :try_start_2
    iget-object v3, p0, Lepson/print/inkrpln/InkReplenishSystem;->mInkRsInfoClient:Lepson/print/inkrpln/InkRplnInfoClient;

    invoke-virtual {v3, v2, v0}, Lepson/print/inkrpln/InkRplnInfoClient;->getInkDsInfo(Ljava/lang/String;Ljava/lang/String;)Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 201
    iget-object v3, v0, Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;->landingUrl:Ljava/lang/String;

    if-eqz v3, :cond_3

    if-nez v2, :cond_1

    goto :goto_1

    .line 205
    :cond_1
    iget-object v3, p0, Lepson/print/inkrpln/InkReplenishSystem;->mIrsRepository:Lepson/print/inkrpln/InkRplnRepository;

    invoke-virtual {v3, p1, v0}, Lepson/print/inkrpln/InkRplnRepository;->saveData(Landroid/content/Context;Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;)V

    .line 207
    iget-object p1, v0, Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;->landingUrl:Ljava/lang/String;

    iget-boolean v0, v0, Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;->appendSerial:Z

    if-eqz v0, :cond_2

    goto :goto_0

    :cond_2
    move-object v2, v1

    :goto_0
    invoke-virtual {p0, p1, v2}, Lepson/print/inkrpln/InkReplenishSystem;->getReadyInkUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 209
    monitor-exit p0

    return-object p1

    .line 203
    :cond_3
    :goto_1
    monitor-exit p0

    return-object v1

    .line 211
    :catch_0
    monitor-exit p0

    return-object v1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setServerCheckDisabled(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    monitor-enter p0

    .line 182
    :try_start_0
    iget-object v0, p0, Lepson/print/inkrpln/InkReplenishSystem;->mIrsRepository:Lepson/print/inkrpln/InkRplnRepository;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lepson/print/inkrpln/InkRplnRepository;->saveInvitationDialogDisableValue(Landroid/content/Context;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 183
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized tryRecordPrinterInfoFromServer(Landroid/content/Context;)Z
    .locals 6
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    monitor-enter p0

    .line 83
    :try_start_0
    invoke-virtual {p0, p1}, Lepson/print/inkrpln/InkReplenishSystem;->clearServerReplyPrinterInfo(Landroid/content/Context;)V

    .line 85
    invoke-virtual {p0}, Lepson/print/inkrpln/InkReplenishSystem;->isInSupportedArea()Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 87
    monitor-exit p0

    return v1

    .line 90
    :cond_0
    :try_start_1
    invoke-virtual {p0, p1}, Lepson/print/inkrpln/InkReplenishSystem;->checkPrinterAndNetworkConnectivity(Landroid/content/Context;)Z

    move-result v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_1

    .line 92
    monitor-exit p0

    return v1

    .line 95
    :cond_1
    :try_start_2
    iget-object v0, p0, Lepson/print/inkrpln/InkReplenishSystem;->mPrinterAdapter:Lepson/print/inkrpln/PrinterAdapter;

    invoke-virtual {v0, p1}, Lepson/print/inkrpln/PrinterAdapter;->getSerialNo(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-nez v0, :cond_2

    .line 97
    monitor-exit p0

    return v1

    .line 100
    :cond_2
    :try_start_3
    iget-object v2, p0, Lepson/print/inkrpln/InkReplenishSystem;->mPrinterAdapter:Lepson/print/inkrpln/PrinterAdapter;

    invoke-virtual {v2}, Lepson/print/inkrpln/PrinterAdapter;->getReadyInkInfo()Lcom/epson/mobilephone/common/escpr/EPS_REDYINKINFO;

    move-result-object v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-nez v2, :cond_3

    .line 102
    monitor-exit p0

    return v1

    .line 105
    :cond_3
    :try_start_4
    iget v3, v2, Lcom/epson/mobilephone/common/escpr/EPS_REDYINKINFO;->subscriptionStatus:I

    if-eqz v3, :cond_4

    .line 107
    iget-object v0, p0, Lepson/print/inkrpln/InkReplenishSystem;->mIrsRepository:Lepson/print/inkrpln/InkRplnRepository;

    invoke-virtual {v0, p1}, Lepson/print/inkrpln/InkRplnRepository;->saveBuyInkButtonOffReadyInkOff(Landroid/content/Context;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 108
    monitor-exit p0

    return v1

    .line 111
    :cond_4
    :try_start_5
    invoke-virtual {p0, p1}, Lepson/print/inkrpln/InkReplenishSystem;->getGoEpsonParams(Landroid/content/Context;)Lepson/print/inkrpln/GoEpsonClient$ReadyInkParams;

    move-result-object v3

    .line 112
    invoke-static {p1}, Lepson/print/inkrpln/InkReplenishSystem;->useGoEpsonTls(Landroid/content/Context;)Z

    move-result v4

    .line 113
    iget-object v5, p0, Lepson/print/inkrpln/InkReplenishSystem;->mGoEpsonClient:Lepson/print/inkrpln/GoEpsonClient;

    invoke-virtual {v5, v3, v4}, Lepson/print/inkrpln/GoEpsonClient;->getRedirectLocation(Lepson/print/inkrpln/GoEpsonClient$ReadyInkParams;Z)Ljava/lang/String;

    move-result-object v3

    .line 114
    iget-object v4, p0, Lepson/print/inkrpln/InkReplenishSystem;->mIrsRepository:Lepson/print/inkrpln/InkRplnRepository;

    invoke-virtual {v4, p1, v3}, Lepson/print/inkrpln/InkRplnRepository;->saveInkReplenishServerUrl(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    if-nez v3, :cond_5

    .line 118
    monitor-exit p0

    return v1

    .line 122
    :cond_5
    :try_start_6
    iget-object v4, p0, Lepson/print/inkrpln/InkReplenishSystem;->mPrinterAdapter:Lepson/print/inkrpln/PrinterAdapter;

    invoke-virtual {v4, v2}, Lepson/print/inkrpln/PrinterAdapter;->activateEma(Lcom/epson/mobilephone/common/escpr/EPS_REDYINKINFO;)V

    .line 124
    iget-object v2, p0, Lepson/print/inkrpln/InkReplenishSystem;->mInkRsInfoClient:Lepson/print/inkrpln/InkRplnInfoClient;

    invoke-virtual {v2, v0, v3}, Lepson/print/inkrpln/InkRplnInfoClient;->getInkDsInfo(Ljava/lang/String;Ljava/lang/String;)Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;

    move-result-object v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    if-nez v0, :cond_6

    .line 126
    monitor-exit p0

    return v1

    .line 129
    :cond_6
    :try_start_7
    iget-object v1, p0, Lepson/print/inkrpln/InkReplenishSystem;->mIrsRepository:Lepson/print/inkrpln/InkRplnRepository;

    invoke-virtual {v1, p1, v0}, Lepson/print/inkrpln/InkRplnRepository;->saveData(Landroid/content/Context;Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    const/4 p1, 0x1

    .line 131
    monitor-exit p0

    return p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

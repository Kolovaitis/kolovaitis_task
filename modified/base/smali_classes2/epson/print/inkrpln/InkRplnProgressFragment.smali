.class public Lepson/print/inkrpln/InkRplnProgressFragment;
.super Landroid/support/v4/app/Fragment;
.source "InkRplnProgressFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/inkrpln/InkRplnProgressFragment$InkDeliverySystemTask;,
        Lepson/print/inkrpln/InkRplnProgressFragment$FragmentStatus;
    }
.end annotation


# static fields
.field private static final ARG_KEY_DEPENDENCY_BUILDER:Ljava/lang/String; = "dependency_builder"

.field private static final DIALOG_TAG_THREE_BUTTON:Ljava/lang/String; = "three_button_dialog"

.field private static final FRAGMENT_TAG:Ljava/lang/String; = "InkDeliverySystem"

.field private static final KEY_FRAGMENT_STATUS:Ljava/lang/String; = "fragment_status"

.field public static final STATUS_WAITE_ACTIVITY_FINISH:I = 0x1

.field public static final STATUS_WAITE_DIALOG_INPUT:I = 0x3

.field public static final STATUS_WAITE_DIALOG_OPEN:I = 0x2

.field public static final STATUS_WAITE_THREAD_END:I


# instance fields
.field private mFragmentForeground:Z

.field private mFragmentStatus:I

.field private mInkDeliverySystemTask:Lepson/print/inkrpln/InkRplnProgressFragment$InkDeliverySystemTask;

.field private mInkReplenishSystem:Lepson/print/inkrpln/InkReplenishSystem;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 60
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lepson/print/inkrpln/InkRplnProgressFragment;)Lepson/print/inkrpln/InkReplenishSystem;
    .locals 0

    .line 29
    invoke-direct {p0}, Lepson/print/inkrpln/InkRplnProgressFragment;->getIPrintInkDsManager()Lepson/print/inkrpln/InkReplenishSystem;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$200(Lepson/print/inkrpln/InkRplnProgressFragment;)V
    .locals 0

    .line 29
    invoke-direct {p0}, Lepson/print/inkrpln/InkRplnProgressFragment;->requestToShowDialogOrFinish()V

    return-void
.end method

.method private getFragmentStatusFromSavedInstanceState(Landroid/os/Bundle;)I
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    :cond_0
    const-string v1, "fragment_status"

    .line 211
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result p1

    packed-switch p1, :pswitch_data_0

    const/4 p1, 0x0

    :pswitch_0
    return p1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private getIPrintInkDsManager()Lepson/print/inkrpln/InkReplenishSystem;
    .locals 1

    .line 87
    iget-object v0, p0, Lepson/print/inkrpln/InkRplnProgressFragment;->mInkReplenishSystem:Lepson/print/inkrpln/InkReplenishSystem;

    return-object v0
.end method

.method public static getInstance(Landroid/support/v4/app/FragmentManager;Lepson/print/inkrpln/DependencyBuilder;)Lepson/print/inkrpln/InkRplnProgressFragment;
    .locals 3
    .param p0    # Landroid/support/v4/app/FragmentManager;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1    # Lepson/print/inkrpln/DependencyBuilder;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    const-string v0, "InkDeliverySystem"

    .line 73
    invoke-virtual {p0, v0}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lepson/print/inkrpln/InkRplnProgressFragment;

    if-nez v0, :cond_0

    .line 76
    new-instance v0, Lepson/print/inkrpln/InkRplnProgressFragment;

    invoke-direct {v0}, Lepson/print/inkrpln/InkRplnProgressFragment;-><init>()V

    .line 77
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "dependency_builder"

    .line 78
    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 79
    invoke-virtual {v0, v1}, Lepson/print/inkrpln/InkRplnProgressFragment;->setArguments(Landroid/os/Bundle;)V

    .line 80
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object p0

    const-string p1, "InkDeliverySystem"

    invoke-virtual {p0, v0, p1}, Landroid/support/v4/app/FragmentTransaction;->add(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object p0

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    :cond_0
    return-object v0
.end method

.method private localFinishActivity()V
    .locals 1

    const/4 v0, 0x1

    .line 174
    iput v0, p0, Lepson/print/inkrpln/InkRplnProgressFragment;->mFragmentStatus:I

    .line 175
    invoke-virtual {p0}, Lepson/print/inkrpln/InkRplnProgressFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 177
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :cond_0
    return-void
.end method

.method private requestToShowDialog()V
    .locals 3

    .line 103
    invoke-virtual {p0}, Lepson/print/inkrpln/InkRplnProgressFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 104
    iget-boolean v1, p0, Lepson/print/inkrpln/InkRplnProgressFragment;->mFragmentForeground:Z

    if-nez v1, :cond_0

    goto :goto_0

    .line 110
    :cond_0
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "three_button_dialog"

    .line 111
    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lepson/print/ThreeButtonDialog;

    if-eqz v1, :cond_1

    .line 114
    invoke-virtual {v1}, Lepson/print/ThreeButtonDialog;->dismiss()V

    :cond_1
    const v1, 0x7f0a0053

    .line 117
    invoke-static {v1}, Lepson/print/ThreeButtonDialog;->newInstance(I)Lepson/print/ThreeButtonDialog;

    move-result-object v1

    .line 118
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v2, "three_button_dialog"

    invoke-virtual {v1, v0, v2}, Lepson/print/ThreeButtonDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    const/4 v0, 0x3

    .line 119
    iput v0, p0, Lepson/print/inkrpln/InkRplnProgressFragment;->mFragmentStatus:I

    return-void

    :cond_2
    :goto_0
    const/4 v0, 0x2

    .line 106
    iput v0, p0, Lepson/print/inkrpln/InkRplnProgressFragment;->mFragmentStatus:I

    return-void
.end method

.method private requestToShowDialogOrFinish()V
    .locals 2

    .line 94
    iget-object v0, p0, Lepson/print/inkrpln/InkRplnProgressFragment;->mInkReplenishSystem:Lepson/print/inkrpln/InkReplenishSystem;

    invoke-static {}, Lepson/print/IprintApplication;->getInstance()Lepson/print/IprintApplication;

    move-result-object v1

    invoke-virtual {v0, v1}, Lepson/print/inkrpln/InkReplenishSystem;->needsDisplayInvitationDialog(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    invoke-direct {p0}, Lepson/print/inkrpln/InkRplnProgressFragment;->requestToShowDialog()V

    goto :goto_0

    .line 98
    :cond_0
    invoke-direct {p0}, Lepson/print/inkrpln/InkRplnProgressFragment;->localFinishActivity()V

    :goto_0
    return-void
.end method


# virtual methods
.method launchWebBrowseAndFinishActivity()V
    .locals 3
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .line 128
    iget-object v0, p0, Lepson/print/inkrpln/InkRplnProgressFragment;->mInkReplenishSystem:Lepson/print/inkrpln/InkReplenishSystem;

    .line 135
    invoke-static {}, Lepson/print/IprintApplication;->getInstance()Lepson/print/IprintApplication;

    move-result-object v1

    invoke-virtual {v0, v1}, Lepson/print/inkrpln/InkReplenishSystem;->getReadyInkUriFromSavedData(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 138
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 139
    invoke-virtual {p0, v1}, Lepson/print/inkrpln/InkRplnProgressFragment;->startActivity(Landroid/content/Intent;)V

    const/4 v0, 0x1

    .line 141
    iput v0, p0, Lepson/print/inkrpln/InkRplnProgressFragment;->mFragmentStatus:I

    goto :goto_0

    .line 143
    :cond_0
    invoke-direct {p0}, Lepson/print/inkrpln/InkRplnProgressFragment;->localFinishActivity()V

    :goto_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .line 183
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x1

    .line 184
    invoke-virtual {p0, v0}, Lepson/print/inkrpln/InkRplnProgressFragment;->setRetainInstance(Z)V

    .line 186
    invoke-virtual {p0}, Lepson/print/inkrpln/InkRplnProgressFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "dependency_builder"

    .line 188
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lepson/print/inkrpln/DependencyBuilder;

    if-nez v0, :cond_0

    .line 190
    new-instance v0, Lepson/print/inkrpln/DependencyBuilder;

    invoke-direct {v0}, Lepson/print/inkrpln/DependencyBuilder;-><init>()V

    .line 192
    :cond_0
    new-instance v1, Lepson/print/inkrpln/InkReplenishSystem;

    invoke-virtual {v0}, Lepson/print/inkrpln/DependencyBuilder;->createPrinterAdapter()Lepson/print/inkrpln/PrinterAdapter;

    move-result-object v2

    .line 193
    invoke-virtual {v0}, Lepson/print/inkrpln/DependencyBuilder;->createInkRplnRepository()Lepson/print/inkrpln/InkRplnRepository;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lepson/print/inkrpln/InkReplenishSystem;-><init>(Lepson/print/inkrpln/PrinterAdapter;Lepson/print/inkrpln/InkRplnRepository;)V

    iput-object v1, p0, Lepson/print/inkrpln/InkRplnProgressFragment;->mInkReplenishSystem:Lepson/print/inkrpln/InkReplenishSystem;

    const/4 v0, 0x0

    .line 195
    iput-boolean v0, p0, Lepson/print/inkrpln/InkRplnProgressFragment;->mFragmentForeground:Z

    .line 197
    invoke-direct {p0, p1}, Lepson/print/inkrpln/InkRplnProgressFragment;->getFragmentStatusFromSavedInstanceState(Landroid/os/Bundle;)I

    move-result p1

    iput p1, p0, Lepson/print/inkrpln/InkRplnProgressFragment;->mFragmentStatus:I

    .line 199
    iget p1, p0, Lepson/print/inkrpln/InkRplnProgressFragment;->mFragmentStatus:I

    if-nez p1, :cond_1

    .line 200
    new-instance p1, Lepson/print/inkrpln/InkRplnProgressFragment$InkDeliverySystemTask;

    const/4 v1, 0x0

    invoke-direct {p1, p0, v1}, Lepson/print/inkrpln/InkRplnProgressFragment$InkDeliverySystemTask;-><init>(Lepson/print/inkrpln/InkRplnProgressFragment;Lepson/print/inkrpln/InkRplnProgressFragment$1;)V

    iput-object p1, p0, Lepson/print/inkrpln/InkRplnProgressFragment;->mInkDeliverySystemTask:Lepson/print/inkrpln/InkRplnProgressFragment$InkDeliverySystemTask;

    .line 201
    iget-object p1, p0, Lepson/print/inkrpln/InkRplnProgressFragment;->mInkDeliverySystemTask:Lepson/print/inkrpln/InkRplnProgressFragment$InkDeliverySystemTask;

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {p1, v0}, Lepson/print/inkrpln/InkRplnProgressFragment$InkDeliverySystemTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_1
    return-void
.end method

.method public onPause()V
    .locals 1

    .line 169
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    const/4 v0, 0x0

    .line 170
    iput-boolean v0, p0, Lepson/print/inkrpln/InkRplnProgressFragment;->mFragmentForeground:Z

    return-void
.end method

.method public onResume()V
    .locals 1

    .line 154
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    const/4 v0, 0x1

    .line 156
    iput-boolean v0, p0, Lepson/print/inkrpln/InkRplnProgressFragment;->mFragmentForeground:Z

    .line 157
    iget v0, p0, Lepson/print/inkrpln/InkRplnProgressFragment;->mFragmentStatus:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 162
    :pswitch_0
    invoke-direct {p0}, Lepson/print/inkrpln/InkRplnProgressFragment;->requestToShowDialog()V

    goto :goto_0

    .line 159
    :pswitch_1
    invoke-direct {p0}, Lepson/print/inkrpln/InkRplnProgressFragment;->localFinishActivity()V

    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .line 228
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "fragment_status"

    .line 230
    iget v1, p0, Lepson/print/inkrpln/InkRplnProgressFragment;->mFragmentStatus:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

.method setInkDsServerCheckDisable(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 148
    iget-object v0, p0, Lepson/print/inkrpln/InkRplnProgressFragment;->mInkReplenishSystem:Lepson/print/inkrpln/InkReplenishSystem;

    invoke-virtual {v0, p1}, Lepson/print/inkrpln/InkReplenishSystem;->setServerCheckDisabled(Landroid/content/Context;)V

    return-void
.end method

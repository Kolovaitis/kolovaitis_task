.class public Lepson/print/inkrpln/DependencyBuilder;
.super Ljava/lang/Object;
.source "DependencyBuilder.java"

# interfaces
.implements Ljava/io/Serializable;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createInkRplnRepository()Lepson/print/inkrpln/InkRplnRepository;
    .locals 2

    .line 11
    new-instance v0, Lepson/print/inkrpln/InkRplnRepository;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lepson/print/inkrpln/InkRplnRepository;-><init>(Z)V

    return-object v0
.end method

.method public createPrinterAdapter()Lepson/print/inkrpln/PrinterAdapter;
    .locals 1

    .line 7
    new-instance v0, Lepson/print/inkrpln/PrinterAdapter;

    invoke-direct {v0}, Lepson/print/inkrpln/PrinterAdapter;-><init>()V

    return-object v0
.end method

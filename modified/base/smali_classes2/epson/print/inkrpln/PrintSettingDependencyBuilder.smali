.class public Lepson/print/inkrpln/PrintSettingDependencyBuilder;
.super Lepson/print/inkrpln/DependencyBuilder;
.source "PrintSettingDependencyBuilder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/inkrpln/PrintSettingDependencyBuilder$PsPrinterAdapter;
    }
.end annotation


# instance fields
.field private final mEscprLibPrinterId:Ljava/lang/String;

.field private final mPrinterIpAddress:Ljava/lang/String;

.field private final mPrinterLocation:I

.field private final mPrinterModelName:Ljava/lang/String;

.field private final mPrinterSerialNumber:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    .line 30
    invoke-direct {p0}, Lepson/print/inkrpln/DependencyBuilder;-><init>()V

    .line 31
    iput-object p1, p0, Lepson/print/inkrpln/PrintSettingDependencyBuilder;->mEscprLibPrinterId:Ljava/lang/String;

    .line 32
    iput-object p2, p0, Lepson/print/inkrpln/PrintSettingDependencyBuilder;->mPrinterIpAddress:Ljava/lang/String;

    .line 33
    iput-object p3, p0, Lepson/print/inkrpln/PrintSettingDependencyBuilder;->mPrinterSerialNumber:Ljava/lang/String;

    .line 34
    iput p4, p0, Lepson/print/inkrpln/PrintSettingDependencyBuilder;->mPrinterLocation:I

    .line 35
    iput-object p5, p0, Lepson/print/inkrpln/PrintSettingDependencyBuilder;->mPrinterModelName:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lepson/print/inkrpln/PrintSettingDependencyBuilder;)Ljava/lang/String;
    .locals 0

    .line 15
    iget-object p0, p0, Lepson/print/inkrpln/PrintSettingDependencyBuilder;->mPrinterSerialNumber:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$100(Lepson/print/inkrpln/PrintSettingDependencyBuilder;)Ljava/lang/String;
    .locals 0

    .line 15
    iget-object p0, p0, Lepson/print/inkrpln/PrintSettingDependencyBuilder;->mPrinterModelName:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$200(Lepson/print/inkrpln/PrintSettingDependencyBuilder;)I
    .locals 0

    .line 15
    iget p0, p0, Lepson/print/inkrpln/PrintSettingDependencyBuilder;->mPrinterLocation:I

    return p0
.end method

.method static synthetic access$300(Lepson/print/inkrpln/PrintSettingDependencyBuilder;)Ljava/lang/String;
    .locals 0

    .line 15
    iget-object p0, p0, Lepson/print/inkrpln/PrintSettingDependencyBuilder;->mEscprLibPrinterId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$400(Lepson/print/inkrpln/PrintSettingDependencyBuilder;)Ljava/lang/String;
    .locals 0

    .line 15
    iget-object p0, p0, Lepson/print/inkrpln/PrintSettingDependencyBuilder;->mPrinterIpAddress:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public createInkRplnRepository()Lepson/print/inkrpln/InkRplnRepository;
    .locals 2

    .line 45
    new-instance v0, Lepson/print/inkrpln/InkRplnRepository;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lepson/print/inkrpln/InkRplnRepository;-><init>(Z)V

    return-object v0
.end method

.method public createPrinterAdapter()Lepson/print/inkrpln/PrinterAdapter;
    .locals 1

    .line 40
    new-instance v0, Lepson/print/inkrpln/PrintSettingDependencyBuilder$PsPrinterAdapter;

    invoke-direct {v0, p0}, Lepson/print/inkrpln/PrintSettingDependencyBuilder$PsPrinterAdapter;-><init>(Lepson/print/inkrpln/PrintSettingDependencyBuilder;)V

    return-object v0
.end method

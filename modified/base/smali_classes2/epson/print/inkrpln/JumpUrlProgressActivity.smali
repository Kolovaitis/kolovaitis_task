.class public Lepson/print/inkrpln/JumpUrlProgressActivity;
.super Landroid/support/v7/app/AppCompatActivity;
.source "JumpUrlProgressActivity.java"

# interfaces
.implements Lepson/print/imgsel/LocalAlertDialogFragment$DialogCallback;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatActivity;-><init>()V

    return-void
.end method

.method public static getStartIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2
    .param p0    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 36
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lepson/print/inkrpln/JumpUrlProgressActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0

    .line 21
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    const/4 p1, 0x1

    .line 22
    invoke-virtual {p0, p1}, Lepson/print/inkrpln/JumpUrlProgressActivity;->requestWindowFeature(I)Z

    const p1, 0x7f0a0033

    .line 23
    invoke-virtual {p0, p1}, Lepson/print/inkrpln/JumpUrlProgressActivity;->setContentView(I)V

    const/4 p1, 0x0

    .line 24
    invoke-virtual {p0, p1}, Lepson/print/inkrpln/JumpUrlProgressActivity;->setFinishOnTouchOutside(Z)V

    .line 26
    invoke-virtual {p0}, Lepson/print/inkrpln/JumpUrlProgressActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object p1

    invoke-static {p1}, Lepson/print/inkrpln/JumpUrlFragment;->getInstance(Landroid/support/v4/app/FragmentManager;)Lepson/print/inkrpln/JumpUrlFragment;

    return-void
.end method

.method public onDialogCallback(I)V
    .locals 0

    const/4 p1, 0x0

    .line 31
    invoke-virtual {p0, p1}, Lepson/print/inkrpln/JumpUrlProgressActivity;->setResult(I)V

    .line 32
    invoke-virtual {p0}, Lepson/print/inkrpln/JumpUrlProgressActivity;->finish()V

    return-void
.end method

.class public Lepson/print/inkrpln/InkRplnRepository;
.super Ljava/lang/Object;
.source "InkRplnRepository.java"


# static fields
.field private static final KEY_APPEND_SERIAL:Ljava/lang/String; = "append-serial"

.field private static final KEY_BUTTON_TYPE:Ljava/lang/String; = "button_type"

.field private static final KEY_INK_REPLEN_INFO_SERVER_URL:Ljava/lang/String; = "ink_replen_info_server_url"

.field private static final KEY_LANDING_PAGE_URL:Ljava/lang/String; = "server-landing_page"

.field private static final KEY_SERVER_CHECK_DISABLE:Ljava/lang/String; = "server_check_disable"

.field private static final KEY_SHOW_INVITATION:Ljava/lang/String; = "invitation"

.field private static final PREFS_NAME_CONTROL_DATA:Ljava/lang/String; = "inkDs-control"

.field private static final PREFS_NAME_PRINTER_INFO:Ljava/lang/String; = "inkDs-server"

.field private static final PREFS_NAME_TMP_PRINTER_INFO:Ljava/lang/String; = "tmp_inkDs-server"


# instance fields
.field private mTemporallySave:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 1

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 27
    iput-boolean v0, p0, Lepson/print/inkrpln/InkRplnRepository;->mTemporallySave:Z

    .line 30
    iput-boolean p1, p0, Lepson/print/inkrpln/InkRplnRepository;->mTemporallySave:Z

    return-void
.end method

.method private getControlPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    const-string v0, "inkDs-control"

    const/4 v1, 0x0

    .line 130
    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    return-object p1
.end method

.method private getPrinterInfoPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 80
    iget-boolean v0, p0, Lepson/print/inkrpln/InkRplnRepository;->mTemporallySave:Z

    invoke-direct {p0, p1, v0}, Lepson/print/inkrpln/InkRplnRepository;->getPrinterInfoPreferences(Landroid/content/Context;Z)Landroid/content/SharedPreferences;

    move-result-object p1

    return-object p1
.end method

.method private getPrinterInfoPreferences(Landroid/content/Context;Z)Landroid/content/SharedPreferences;
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    if-eqz p2, :cond_0

    const-string p2, "tmp_inkDs-server"

    goto :goto_0

    :cond_0
    const-string p2, "inkDs-server"

    :goto_0
    const/4 v0, 0x0

    .line 84
    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public clearServerData(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 38
    invoke-direct {p0, p1}, Lepson/print/inkrpln/InkRplnRepository;->getPrinterInfoPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object p1

    .line 39
    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public deleteTemporaryData(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    const/4 v0, 0x1

    .line 174
    invoke-direct {p0, p1, v0}, Lepson/print/inkrpln/InkRplnRepository;->getPrinterInfoPreferences(Landroid/content/Context;Z)Landroid/content/SharedPreferences;

    move-result-object p1

    .line 175
    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public getInfo(Landroid/content/Context;)Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;
    .locals 3
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .line 64
    invoke-static {p1}, Lepson/print/inkrpln/InkReplenishSystem;->isInkReplenishSystemEnabled(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 68
    :cond_0
    new-instance v0, Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;

    invoke-direct {v0}, Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;-><init>()V

    .line 70
    invoke-direct {p0, p1}, Lepson/print/inkrpln/InkRplnRepository;->getPrinterInfoPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object p1

    const-string v2, "server-landing_page"

    .line 71
    invoke-interface {p1, v2, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;->landingUrl:Ljava/lang/String;

    const-string v1, "invitation"

    const/4 v2, 0x0

    .line 72
    invoke-interface {p1, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, v0, Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;->showInvitation:Z

    const-string v1, "button_type"

    .line 73
    invoke-interface {p1, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, v0, Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;->buttonType:I

    const-string v1, "append-serial"

    .line 74
    invoke-interface {p1, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result p1

    iput-boolean p1, v0, Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;->appendSerial:Z

    return-object v0
.end method

.method public getInkReplenishServerUrl(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .line 116
    invoke-direct {p0, p1}, Lepson/print/inkrpln/InkRplnRepository;->getPrinterInfoPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object p1

    const-string v0, "ink_replen_info_server_url"

    const/4 v1, 0x0

    .line 117
    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getInvitationDialogDisabledValue(Landroid/content/Context;)Z
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 98
    invoke-direct {p0, p1}, Lepson/print/inkrpln/InkRplnRepository;->getControlPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object p1

    const-string v0, "server_check_disable"

    const/4 v1, 0x0

    .line 99
    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result p1

    return p1
.end method

.method public saveBuyInkButtonOffReadyInkOff(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 124
    new-instance v0, Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;

    invoke-direct {v0}, Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;-><init>()V

    const/4 v1, 0x3

    .line 125
    iput v1, v0, Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;->buttonType:I

    .line 126
    invoke-virtual {p0, p1, v0}, Lepson/print/inkrpln/InkRplnRepository;->saveData(Landroid/content/Context;Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;)V

    return-void
.end method

.method public saveData(Landroid/content/Context;Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;)V
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    if-nez p2, :cond_0

    return-void

    .line 47
    :cond_0
    invoke-direct {p0, p1}, Lepson/print/inkrpln/InkRplnRepository;->getPrinterInfoPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object p1

    .line 48
    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    .line 50
    iget-object v0, p2, Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;->landingUrl:Ljava/lang/String;

    if-eqz v0, :cond_1

    const-string v0, "server-landing_page"

    .line 51
    iget-object v1, p2, Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;->landingUrl:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    :cond_1
    const-string v0, "server-landing_page"

    .line 53
    invoke-interface {p1, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    :goto_0
    const-string v0, "invitation"

    .line 55
    iget-boolean v1, p2, Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;->showInvitation:Z

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const-string v0, "button_type"

    .line 56
    iget v1, p2, Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;->buttonType:I

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v0, "append-serial"

    .line 57
    iget-boolean p2, p2, Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;->appendSerial:Z

    invoke-interface {p1, v0, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 59
    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public saveInkReplenishServerUrl(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .line 106
    invoke-direct {p0, p1}, Lepson/print/inkrpln/InkRplnRepository;->getPrinterInfoPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object p1

    .line 107
    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    const-string v0, "ink_replen_info_server_url"

    .line 108
    invoke-interface {p1, v0, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public saveInvitationDialogDisableValue(Landroid/content/Context;Z)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 92
    invoke-direct {p0, p1}, Lepson/print/inkrpln/InkRplnRepository;->getControlPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object p1

    .line 93
    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    const-string v0, "server_check_disable"

    .line 94
    invoke-interface {p1, v0, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public savePermanently(Landroid/content/Context;)V
    .locals 5
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    const/4 v0, 0x1

    .line 134
    invoke-direct {p0, p1, v0}, Lepson/print/inkrpln/InkRplnRepository;->getPrinterInfoPreferences(Landroid/content/Context;Z)Landroid/content/SharedPreferences;

    move-result-object v0

    const/4 v1, 0x0

    .line 135
    invoke-direct {p0, p1, v1}, Lepson/print/inkrpln/InkRplnRepository;->getPrinterInfoPreferences(Landroid/content/Context;Z)Landroid/content/SharedPreferences;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    const-string v2, "server-landing_page"

    .line 137
    invoke-interface {v0, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    const-string v2, "server-landing_page"

    const-string v4, "server-landing_page"

    .line 139
    invoke-interface {v0, v4, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 138
    invoke-interface {p1, v2, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    :cond_0
    const-string v2, "server-landing_page"

    .line 141
    invoke-interface {p1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    :goto_0
    const-string v2, "button_type"

    .line 143
    invoke-interface {v0, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "button_type"

    const-string v4, "button_type"

    .line 145
    invoke-interface {v0, v4, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 144
    invoke-interface {p1, v2, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto :goto_1

    :cond_1
    const-string v2, "button_type"

    .line 147
    invoke-interface {p1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    :goto_1
    const-string v2, "invitation"

    .line 149
    invoke-interface {v0, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "invitation"

    const-string v4, "invitation"

    .line 151
    invoke-interface {v0, v4, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    .line 150
    invoke-interface {p1, v2, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    goto :goto_2

    :cond_2
    const-string v2, "invitation"

    .line 153
    invoke-interface {p1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    :goto_2
    const-string v2, "append-serial"

    .line 155
    invoke-interface {v0, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "append-serial"

    const-string v4, "append-serial"

    .line 157
    invoke-interface {v0, v4, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 156
    invoke-interface {p1, v2, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    goto :goto_3

    :cond_3
    const-string v1, "append-serial"

    .line 159
    invoke-interface {p1, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    :goto_3
    const-string v1, "ink_replen_info_server_url"

    .line 161
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "ink_replen_info_server_url"

    const-string v2, "ink_replen_info_server_url"

    .line 163
    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 162
    invoke-interface {p1, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_4

    :cond_4
    const-string v1, "ink_replen_info_server_url"

    .line 165
    invoke-interface {p1, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 168
    :goto_4
    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 170
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.class final enum Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;
.super Ljava/lang/Enum;
.source "JumpUrlFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/inkrpln/JumpUrlFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "FragmentStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;

.field public static final enum WAITE_ACTIVITY_FINISH:Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;

.field public static final enum WAITE_DIALOG_SHOW:Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;

.field public static final enum WAITE_RETURN_FROM_BROWSER:Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;

.field public static final enum WAITE_START_BROWSE_ACTIVITY:Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;

.field public static final enum WAITE_THREAD_END:Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 33
    new-instance v0, Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;

    const-string v1, "WAITE_THREAD_END"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;->WAITE_THREAD_END:Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;

    new-instance v0, Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;

    const-string v1, "WAITE_DIALOG_SHOW"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;->WAITE_DIALOG_SHOW:Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;

    new-instance v0, Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;

    const-string v1, "WAITE_START_BROWSE_ACTIVITY"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;->WAITE_START_BROWSE_ACTIVITY:Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;

    .line 34
    new-instance v0, Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;

    const-string v1, "WAITE_RETURN_FROM_BROWSER"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;->WAITE_RETURN_FROM_BROWSER:Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;

    new-instance v0, Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;

    const-string v1, "WAITE_ACTIVITY_FINISH"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;->WAITE_ACTIVITY_FINISH:Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;

    const/4 v0, 0x5

    .line 33
    new-array v0, v0, [Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;

    sget-object v1, Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;->WAITE_THREAD_END:Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;

    aput-object v1, v0, v2

    sget-object v1, Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;->WAITE_DIALOG_SHOW:Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;

    aput-object v1, v0, v3

    sget-object v1, Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;->WAITE_START_BROWSE_ACTIVITY:Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;

    aput-object v1, v0, v4

    sget-object v1, Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;->WAITE_RETURN_FROM_BROWSER:Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;

    aput-object v1, v0, v5

    sget-object v1, Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;->WAITE_ACTIVITY_FINISH:Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;

    aput-object v1, v0, v6

    sput-object v0, Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;->$VALUES:[Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 33
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;
    .locals 1

    .line 33
    const-class v0, Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;

    return-object p0
.end method

.method public static values()[Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;
    .locals 1

    .line 33
    sget-object v0, Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;->$VALUES:[Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;

    invoke-virtual {v0}, [Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;

    return-object v0
.end method

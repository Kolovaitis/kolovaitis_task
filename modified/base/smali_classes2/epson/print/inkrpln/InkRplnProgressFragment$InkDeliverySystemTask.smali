.class Lepson/print/inkrpln/InkRplnProgressFragment$InkDeliverySystemTask;
.super Landroid/os/AsyncTask;
.source "InkRplnProgressFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/inkrpln/InkRplnProgressFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InkDeliverySystemTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/inkrpln/InkRplnProgressFragment;


# direct methods
.method private constructor <init>(Lepson/print/inkrpln/InkRplnProgressFragment;)V
    .locals 0

    .line 236
    iput-object p1, p0, Lepson/print/inkrpln/InkRplnProgressFragment$InkDeliverySystemTask;->this$0:Lepson/print/inkrpln/InkRplnProgressFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lepson/print/inkrpln/InkRplnProgressFragment;Lepson/print/inkrpln/InkRplnProgressFragment$1;)V
    .locals 0

    .line 236
    invoke-direct {p0, p1}, Lepson/print/inkrpln/InkRplnProgressFragment$InkDeliverySystemTask;-><init>(Lepson/print/inkrpln/InkRplnProgressFragment;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 236
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/print/inkrpln/InkRplnProgressFragment$InkDeliverySystemTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 3

    .line 239
    invoke-static {}, Lepson/print/IprintApplication;->getInstance()Lepson/print/IprintApplication;

    move-result-object p1

    .line 241
    iget-object v0, p0, Lepson/print/inkrpln/InkRplnProgressFragment$InkDeliverySystemTask;->this$0:Lepson/print/inkrpln/InkRplnProgressFragment;

    invoke-static {v0}, Lepson/print/inkrpln/InkRplnProgressFragment;->access$100(Lepson/print/inkrpln/InkRplnProgressFragment;)Lepson/print/inkrpln/InkReplenishSystem;

    move-result-object v0

    .line 244
    :try_start_0
    invoke-virtual {v0, p1}, Lepson/print/inkrpln/InkReplenishSystem;->tryRecordPrinterInfoFromServer(Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    goto :goto_0

    :catch_1
    move-exception p1

    :goto_0
    const-string v0, "InkDeliverySystemTask"

    .line 247
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "exception <"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ">"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    const/4 p1, 0x0

    return-object p1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 236
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/print/inkrpln/InkRplnProgressFragment$InkDeliverySystemTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 0

    .line 257
    iget-object p1, p0, Lepson/print/inkrpln/InkRplnProgressFragment$InkDeliverySystemTask;->this$0:Lepson/print/inkrpln/InkRplnProgressFragment;

    invoke-static {p1}, Lepson/print/inkrpln/InkRplnProgressFragment;->access$200(Lepson/print/inkrpln/InkRplnProgressFragment;)V

    return-void
.end method

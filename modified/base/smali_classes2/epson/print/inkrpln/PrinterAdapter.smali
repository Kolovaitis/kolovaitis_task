.class public Lepson/print/inkrpln/PrinterAdapter;
.super Ljava/lang/Object;
.source "PrinterAdapter.java"


# static fields
.field private static final EMA_RETRY_COUNT:I = 0x0

.field private static final ESCPR_LIB_EMA_ACTIVATED:I = 0x1

.field private static final ESCPR_LIB_EMA_SUPPORTED:I = 0x10000


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static localActivateEma(Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;)V
    .locals 4
    .param p0    # Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    const/4 v0, 0x2

    .line 45
    new-array v0, v0, [I

    .line 46
    invoke-virtual {p0, v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->getEmaStatus([I)I

    move-result v1

    if-eqz v1, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x0

    .line 50
    aget v2, v0, v1

    const/high16 v3, 0x10000

    and-int/2addr v2, v3

    if-eqz v2, :cond_4

    aget v0, v0, v1

    const/4 v2, 0x1

    and-int/2addr v0, v2

    if-eqz v0, :cond_1

    goto :goto_2

    :cond_1
    :goto_0
    if-gtz v1, :cond_3

    .line 58
    invoke-virtual {p0, v2}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->setEmaStatus(I)I

    move-result v0

    if-nez v0, :cond_2

    goto :goto_1

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    :goto_1
    return-void

    :cond_4
    :goto_2
    return-void
.end method


# virtual methods
.method public activateEma()V
    .locals 1

    .line 40
    invoke-static {}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->getInstance()Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object v0

    .line 41
    invoke-static {v0}, Lepson/print/inkrpln/PrinterAdapter;->localActivateEma(Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;)V

    return-void
.end method

.method public activateEma(Lcom/epson/mobilephone/common/escpr/EPS_REDYINKINFO;)V
    .locals 3

    if-eqz p1, :cond_3

    .line 90
    iget v0, p1, Lcom/epson/mobilephone/common/escpr/EPS_REDYINKINFO;->emaStatus:I

    const/high16 v1, 0x10000

    and-int/2addr v0, v1

    if-eqz v0, :cond_3

    iget p1, p1, Lcom/epson/mobilephone/common/escpr/EPS_REDYINKINFO;->emaStatus:I

    const/4 v0, 0x1

    and-int/2addr p1, v0

    if-eqz p1, :cond_0

    goto :goto_2

    .line 100
    :cond_0
    invoke-static {}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->getInstance()Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object p1

    const/4 v1, 0x0

    :goto_0
    if-gtz v1, :cond_2

    .line 103
    invoke-virtual {p1, v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->setEmaStatus(I)I

    move-result v2

    if-nez v2, :cond_1

    goto :goto_1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    return-void

    :cond_3
    :goto_2
    return-void
.end method

.method public getNonRemotePrinterDeviceId(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    const/4 v0, 0x0

    .line 32
    invoke-static {p1, v0}, Lepson/print/MyPrinter;->getPrinterDeviceId(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getReadyInkInfo()Lcom/epson/mobilephone/common/escpr/EPS_REDYINKINFO;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .line 72
    invoke-static {}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->getInstance()Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object v0

    .line 73
    invoke-virtual {v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->getMEscpLib()Lcom/epson/mobilephone/common/escpr/EscprLib;

    move-result-object v0

    .line 74
    new-instance v1, Lcom/epson/mobilephone/common/escpr/EPS_REDYINKINFO;

    invoke-direct {v1}, Lcom/epson/mobilephone/common/escpr/EPS_REDYINKINFO;-><init>()V

    .line 75
    invoke-virtual {v0, v1}, Lcom/epson/mobilephone/common/escpr/EscprLib;->epsGetReadyPrintStatus(Lcom/epson/mobilephone/common/escpr/EPS_REDYINKINFO;)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    return-object v1
.end method

.method public getSerialNo(Landroid/content/Context;)Ljava/lang/String;
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 27
    invoke-static {p1}, Lepson/print/MyPrinter;->getCurPrinter(Landroid/content/Context;)Lepson/print/MyPrinter;

    move-result-object p1

    .line 28
    invoke-virtual {p1}, Lepson/print/MyPrinter;->getSerialNo()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public isRemotePrinter(Landroid/content/Context;)Z
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 36
    invoke-static {p1}, Lepson/print/MyPrinter;->isRemotePrinter(Landroid/content/Context;)Z

    move-result p1

    return p1
.end method

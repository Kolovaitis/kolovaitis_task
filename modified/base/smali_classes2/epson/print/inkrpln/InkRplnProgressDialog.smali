.class public Lepson/print/inkrpln/InkRplnProgressDialog;
.super Landroid/support/v7/app/AppCompatActivity;
.source "InkRplnProgressDialog.java"

# interfaces
.implements Lepson/print/ThreeButtonDialog$DialogCallback;


# static fields
.field private static final PARAM_KEY_PARAM_CLASS_BUILDER:Ljava/lang/String; = "param_class_builder"


# instance fields
.field private mInkDsFragment:Lepson/print/inkrpln/InkRplnProgressFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 21
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatActivity;-><init>()V

    return-void
.end method

.method public static getStartIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .line 64
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lepson/print/inkrpln/InkRplnProgressDialog;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method public static getStartIntent2(Landroid/content/Context;Lepson/print/inkrpln/DependencyBuilder;)Landroid/content/Intent;
    .locals 2

    .line 78
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lepson/print/inkrpln/InkRplnProgressDialog;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string p0, "param_class_builder"

    .line 79
    invoke-virtual {v0, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    return-object v0
.end method


# virtual methods
.method public callback(I)V
    .locals 1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    .line 55
    invoke-virtual {p0}, Lepson/print/inkrpln/InkRplnProgressDialog;->finish()V

    return-void

    .line 50
    :cond_0
    iget-object p1, p0, Lepson/print/inkrpln/InkRplnProgressDialog;->mInkDsFragment:Lepson/print/inkrpln/InkRplnProgressFragment;

    invoke-virtual {p1, p0}, Lepson/print/inkrpln/InkRplnProgressFragment;->setInkDsServerCheckDisable(Landroid/content/Context;)V

    .line 51
    invoke-virtual {p0}, Lepson/print/inkrpln/InkRplnProgressDialog;->finish()V

    return-void

    .line 47
    :cond_1
    iget-object p1, p0, Lepson/print/inkrpln/InkRplnProgressDialog;->mInkDsFragment:Lepson/print/inkrpln/InkRplnProgressFragment;

    invoke-virtual {p1}, Lepson/print/inkrpln/InkRplnProgressFragment;->launchWebBrowseAndFinishActivity()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .line 28
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    const-string p1, ""

    .line 29
    invoke-virtual {p0, p1}, Lepson/print/inkrpln/InkRplnProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    const p1, 0x7f0a0032

    .line 30
    invoke-virtual {p0, p1}, Lepson/print/inkrpln/InkRplnProgressDialog;->setContentView(I)V

    const/4 p1, 0x0

    .line 31
    invoke-virtual {p0, p1}, Lepson/print/inkrpln/InkRplnProgressDialog;->setFinishOnTouchOutside(Z)V

    .line 33
    invoke-static {p0}, Lepson/print/inkrpln/InkReplenishSystem;->isInkReplenishSystemEnabled(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 34
    invoke-virtual {p0}, Lepson/print/inkrpln/InkRplnProgressDialog;->finish()V

    return-void

    .line 38
    :cond_0
    invoke-virtual {p0}, Lepson/print/inkrpln/InkRplnProgressDialog;->getIntent()Landroid/content/Intent;

    move-result-object p1

    .line 39
    invoke-virtual {p0}, Lepson/print/inkrpln/InkRplnProgressDialog;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "param_class_builder"

    .line 40
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    check-cast p1, Lepson/print/inkrpln/DependencyBuilder;

    .line 39
    invoke-static {v0, p1}, Lepson/print/inkrpln/InkRplnProgressFragment;->getInstance(Landroid/support/v4/app/FragmentManager;Lepson/print/inkrpln/DependencyBuilder;)Lepson/print/inkrpln/InkRplnProgressFragment;

    move-result-object p1

    iput-object p1, p0, Lepson/print/inkrpln/InkRplnProgressDialog;->mInkDsFragment:Lepson/print/inkrpln/InkRplnProgressFragment;

    return-void
.end method

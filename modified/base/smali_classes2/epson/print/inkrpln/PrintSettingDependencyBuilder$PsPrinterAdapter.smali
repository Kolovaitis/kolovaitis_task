.class Lepson/print/inkrpln/PrintSettingDependencyBuilder$PsPrinterAdapter;
.super Lepson/print/inkrpln/PrinterAdapter;
.source "PrintSettingDependencyBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/inkrpln/PrintSettingDependencyBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PsPrinterAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/inkrpln/PrintSettingDependencyBuilder;


# direct methods
.method constructor <init>(Lepson/print/inkrpln/PrintSettingDependencyBuilder;)V
    .locals 0

    .line 49
    iput-object p1, p0, Lepson/print/inkrpln/PrintSettingDependencyBuilder$PsPrinterAdapter;->this$0:Lepson/print/inkrpln/PrintSettingDependencyBuilder;

    invoke-direct {p0}, Lepson/print/inkrpln/PrinterAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public activateEma()V
    .locals 5

    .line 72
    invoke-static {}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->getInstance()Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;

    move-result-object v0

    .line 73
    invoke-static {}, Lepson/print/IprintApplication;->getInstance()Lepson/print/IprintApplication;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doInitDriver(Landroid/content/Context;I)I

    .line 74
    iget-object v1, p0, Lepson/print/inkrpln/PrintSettingDependencyBuilder$PsPrinterAdapter;->this$0:Lepson/print/inkrpln/PrintSettingDependencyBuilder;

    invoke-static {v1}, Lepson/print/inkrpln/PrintSettingDependencyBuilder;->access$300(Lepson/print/inkrpln/PrintSettingDependencyBuilder;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lepson/print/inkrpln/PrintSettingDependencyBuilder$PsPrinterAdapter;->this$0:Lepson/print/inkrpln/PrintSettingDependencyBuilder;

    invoke-static {v2}, Lepson/print/inkrpln/PrintSettingDependencyBuilder;->access$400(Lepson/print/inkrpln/PrintSettingDependencyBuilder;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lepson/print/inkrpln/PrintSettingDependencyBuilder$PsPrinterAdapter;->this$0:Lepson/print/inkrpln/PrintSettingDependencyBuilder;

    .line 75
    invoke-static {v3}, Lepson/print/inkrpln/PrintSettingDependencyBuilder;->access$200(Lepson/print/inkrpln/PrintSettingDependencyBuilder;)I

    move-result v3

    const/16 v4, 0x3c

    .line 74
    invoke-virtual {v0, v4, v1, v2, v3}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doProbePrinter(ILjava/lang/String;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x0

    .line 80
    invoke-virtual {v0, v1}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->setMSearchPos(I)V

    .line 81
    invoke-virtual {v0}, Lcom/epson/mobilephone/common/maintain2/MaintainPrinter2;->doSetPrinter()I

    .line 83
    invoke-super {p0}, Lepson/print/inkrpln/PrinterAdapter;->activateEma()V

    return-void
.end method

.method public getNonRemotePrinterDeviceId(Landroid/content/Context;)Ljava/lang/String;
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 62
    iget-object p1, p0, Lepson/print/inkrpln/PrintSettingDependencyBuilder$PsPrinterAdapter;->this$0:Lepson/print/inkrpln/PrintSettingDependencyBuilder;

    invoke-static {p1}, Lepson/print/inkrpln/PrintSettingDependencyBuilder;->access$100(Lepson/print/inkrpln/PrintSettingDependencyBuilder;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getSerialNo(Landroid/content/Context;)Ljava/lang/String;
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 52
    iget-object p1, p0, Lepson/print/inkrpln/PrintSettingDependencyBuilder$PsPrinterAdapter;->this$0:Lepson/print/inkrpln/PrintSettingDependencyBuilder;

    invoke-static {p1}, Lepson/print/inkrpln/PrintSettingDependencyBuilder;->access$000(Lepson/print/inkrpln/PrintSettingDependencyBuilder;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public isRemotePrinter(Landroid/content/Context;)Z
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 67
    iget-object p1, p0, Lepson/print/inkrpln/PrintSettingDependencyBuilder$PsPrinterAdapter;->this$0:Lepson/print/inkrpln/PrintSettingDependencyBuilder;

    invoke-static {p1}, Lepson/print/inkrpln/PrintSettingDependencyBuilder;->access$200(Lepson/print/inkrpln/PrintSettingDependencyBuilder;)I

    move-result p1

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.class public Lepson/print/inkrpln/JumpUrlFragment;
.super Landroid/support/v4/app/Fragment;
.source "JumpUrlFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/inkrpln/JumpUrlFragment$InkDeliverySystemTask;,
        Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;
    }
.end annotation


# static fields
.field private static final DIALOG_TAG:Ljava/lang/String; = "error-dialog"

.field private static final FRAGMENT_TAG:Ljava/lang/String; = "tag-fragment"

.field private static final KEY_FRAGMENT_STATE:Ljava/lang/String; = "fragment-state"

.field private static final KEY_RESULT_URL:Ljava/lang/String; = "result_url"

.field private static final PARAM_KEY_DEPENDENCY_CREATOR:Ljava/lang/String; = "dependency_creator"

.field private static final REQUEST_CODE_BROWSE:I = 0x1


# instance fields
.field private mFragmentForeground:Z

.field private mFragmentStatus:Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;

.field private mResultUrl:Landroid/net/Uri;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lepson/print/inkrpln/JumpUrlFragment;)V
    .locals 0

    .line 23
    invoke-direct {p0}, Lepson/print/inkrpln/JumpUrlFragment;->requestShowErrorDialogAndFinishActivity()V

    return-void
.end method

.method static synthetic access$100(Lepson/print/inkrpln/JumpUrlFragment;Landroid/net/Uri;)V
    .locals 0

    .line 23
    invoke-direct {p0, p1}, Lepson/print/inkrpln/JumpUrlFragment;->requestStartBrowser(Landroid/net/Uri;)V

    return-void
.end method

.method private getFragmentState(Landroid/os/Bundle;)Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    const-string v0, "fragment-state"

    .line 77
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    .line 78
    invoke-static {}, Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;->values()[Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;

    move-result-object v0

    if-ltz p1, :cond_0

    .line 79
    array-length v1, v0

    if-le p1, v1, :cond_1

    :cond_0
    const/4 p1, 0x0

    .line 83
    :cond_1
    aget-object p1, v0, p1

    return-object p1
.end method

.method public static getInstance(Landroid/support/v4/app/FragmentManager;)Lepson/print/inkrpln/JumpUrlFragment;
    .locals 4
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    const-string v0, "tag-fragment"

    .line 41
    invoke-virtual {p0, v0}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lepson/print/inkrpln/JumpUrlFragment;

    if-nez v0, :cond_0

    .line 44
    new-instance v0, Lepson/print/inkrpln/JumpUrlFragment;

    invoke-direct {v0}, Lepson/print/inkrpln/JumpUrlFragment;-><init>()V

    .line 45
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "dependency_creator"

    const/4 v3, 0x0

    .line 46
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 48
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object p0

    const-string v1, "tag-fragment"

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/app/FragmentTransaction;->add(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object p0

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    :cond_0
    return-object v0
.end method

.method private requestFinishActivity()V
    .locals 1

    .line 164
    invoke-virtual {p0}, Lepson/print/inkrpln/JumpUrlFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 167
    sget-object v0, Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;->WAITE_ACTIVITY_FINISH:Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;

    iput-object v0, p0, Lepson/print/inkrpln/JumpUrlFragment;->mFragmentStatus:Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;

    return-void

    .line 173
    :cond_0
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method private requestShowErrorDialogAndFinishActivity()V
    .locals 3

    .line 134
    iget-boolean v0, p0, Lepson/print/inkrpln/JumpUrlFragment;->mFragmentForeground:Z

    if-nez v0, :cond_0

    .line 135
    sget-object v0, Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;->WAITE_DIALOG_SHOW:Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;

    iput-object v0, p0, Lepson/print/inkrpln/JumpUrlFragment;->mFragmentStatus:Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;

    return-void

    .line 138
    :cond_0
    invoke-virtual {p0}, Lepson/print/inkrpln/JumpUrlFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "error-dialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lepson/print/imgsel/LocalAlertDialogFragment;

    if-eqz v0, :cond_1

    return-void

    :cond_1
    const v0, 0x7f0e005f

    .line 143
    invoke-virtual {p0, v0}, Lepson/print/inkrpln/JumpUrlFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0e037c

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lepson/print/imgsel/LocalAlertDialogFragment;->newInstance(Ljava/lang/String;II)Lepson/print/imgsel/LocalAlertDialogFragment;

    move-result-object v0

    .line 145
    invoke-virtual {p0}, Lepson/print/inkrpln/JumpUrlFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "error-dialog"

    invoke-virtual {v0, v1, v2}, Lepson/print/imgsel/LocalAlertDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method private requestStartBrowser(Landroid/net/Uri;)V
    .locals 2

    .line 149
    invoke-virtual {p0}, Lepson/print/inkrpln/JumpUrlFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 151
    sget-object v0, Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;->WAITE_START_BROWSE_ACTIVITY:Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;

    iput-object v0, p0, Lepson/print/inkrpln/JumpUrlFragment;->mFragmentStatus:Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;

    .line 152
    iput-object p1, p0, Lepson/print/inkrpln/JumpUrlFragment;->mResultUrl:Landroid/net/Uri;

    return-void

    .line 156
    :cond_0
    sget-object v0, Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;->WAITE_RETURN_FROM_BROWSER:Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;

    iput-object v0, p0, Lepson/print/inkrpln/JumpUrlFragment;->mFragmentStatus:Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;

    const/4 v0, 0x0

    .line 157
    iput-object v0, p0, Lepson/print/inkrpln/JumpUrlFragment;->mResultUrl:Landroid/net/Uri;

    .line 159
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/4 p1, 0x1

    .line 160
    invoke-virtual {p0, v0, p1}, Lepson/print/inkrpln/JumpUrlFragment;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    .line 119
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    return-void

    .line 116
    :cond_0
    invoke-direct {p0}, Lepson/print/inkrpln/JumpUrlFragment;->requestFinishActivity()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 56
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x1

    .line 57
    invoke-virtual {p0, v0}, Lepson/print/inkrpln/JumpUrlFragment;->setRetainInstance(Z)V

    .line 58
    sget-object v0, Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;->WAITE_THREAD_END:Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;

    iput-object v0, p0, Lepson/print/inkrpln/JumpUrlFragment;->mFragmentStatus:Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;

    if-eqz p1, :cond_0

    .line 60
    invoke-direct {p0, p1}, Lepson/print/inkrpln/JumpUrlFragment;->getFragmentState(Landroid/os/Bundle;)Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;

    move-result-object v0

    iput-object v0, p0, Lepson/print/inkrpln/JumpUrlFragment;->mFragmentStatus:Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;

    const-string v0, "result_url"

    .line 61
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Landroid/net/Uri;

    iput-object p1, p0, Lepson/print/inkrpln/JumpUrlFragment;->mResultUrl:Landroid/net/Uri;

    .line 64
    :cond_0
    iget-object p1, p0, Lepson/print/inkrpln/JumpUrlFragment;->mFragmentStatus:Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;

    sget-object v0, Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;->WAITE_THREAD_END:Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;

    if-ne p1, v0, :cond_2

    const/4 p1, 0x0

    .line 67
    invoke-virtual {p0}, Lepson/print/inkrpln/JumpUrlFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string p1, "dependency_creator"

    .line 69
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    check-cast p1, Lepson/print/inkrpln/DependencyBuilder;

    .line 71
    :cond_1
    new-instance v0, Lepson/print/inkrpln/JumpUrlFragment$InkDeliverySystemTask;

    invoke-direct {v0, p0, p1}, Lepson/print/inkrpln/JumpUrlFragment$InkDeliverySystemTask;-><init>(Lepson/print/inkrpln/JumpUrlFragment;Lepson/print/inkrpln/DependencyBuilder;)V

    const/4 p1, 0x0

    .line 72
    new-array p1, p1, [Ljava/lang/Void;

    invoke-virtual {v0, p1}, Lepson/print/inkrpln/JumpUrlFragment$InkDeliverySystemTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_2
    return-void
.end method

.method public onPause()V
    .locals 1

    .line 108
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    const/4 v0, 0x0

    .line 109
    iput-boolean v0, p0, Lepson/print/inkrpln/JumpUrlFragment;->mFragmentForeground:Z

    return-void
.end method

.method public onResume()V
    .locals 2

    .line 88
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    const/4 v0, 0x1

    .line 90
    iput-boolean v0, p0, Lepson/print/inkrpln/JumpUrlFragment;->mFragmentForeground:Z

    .line 92
    sget-object v0, Lepson/print/inkrpln/JumpUrlFragment$1;->$SwitchMap$epson$print$inkrpln$JumpUrlFragment$FragmentStatus:[I

    iget-object v1, p0, Lepson/print/inkrpln/JumpUrlFragment;->mFragmentStatus:Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;

    invoke-virtual {v1}, Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 102
    :pswitch_0
    invoke-direct {p0}, Lepson/print/inkrpln/JumpUrlFragment;->requestShowErrorDialogAndFinishActivity()V

    goto :goto_0

    .line 98
    :pswitch_1
    invoke-direct {p0}, Lepson/print/inkrpln/JumpUrlFragment;->requestShowErrorDialogAndFinishActivity()V

    goto :goto_0

    .line 94
    :pswitch_2
    iget-object v0, p0, Lepson/print/inkrpln/JumpUrlFragment;->mResultUrl:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lepson/print/inkrpln/JumpUrlFragment;->requestStartBrowser(Landroid/net/Uri;)V

    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .line 124
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "fragment-state"

    .line 125
    iget-object v1, p0, Lepson/print/inkrpln/JumpUrlFragment;->mFragmentStatus:Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;

    invoke-virtual {v1}, Lepson/print/inkrpln/JumpUrlFragment$FragmentStatus;->ordinal()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "result_url"

    .line 126
    iget-object v1, p0, Lepson/print/inkrpln/JumpUrlFragment;->mResultUrl:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-void
.end method

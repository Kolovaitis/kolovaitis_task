.class public Lepson/print/inkrpln/GoEpsonClient;
.super Ljava/lang/Object;
.source "GoEpsonClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/inkrpln/GoEpsonClient$ReadyInkParams;
    }
.end annotation


# static fields
.field private static final CONNECTION_TIMEOUT:I = 0x2710

.field private static final ERROR_HOST_NAME:Ljava/lang/String; = "www.epson.com"

.field private static final READ_TIMEOUT:I = 0x2710


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getGoEpsonConnection(Ljava/net/URL;)Ljava/net/HttpURLConnection;
    .locals 2
    .param p1    # Ljava/net/URL;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 163
    invoke-virtual {p1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object p1

    check-cast p1, Ljavax/net/ssl/HttpsURLConnection;

    .line 164
    new-instance v0, Lepson/print/inkrpln/LocalSSLSocketFactory;

    invoke-virtual {p1}, Ljavax/net/ssl/HttpsURLConnection;->getSSLSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v1

    invoke-direct {v0, v1}, Lepson/print/inkrpln/LocalSSLSocketFactory;-><init>(Ljavax/net/ssl/SSLSocketFactory;)V

    invoke-virtual {p1, v0}, Ljavax/net/ssl/HttpsURLConnection;->setSSLSocketFactory(Ljavax/net/ssl/SSLSocketFactory;)V

    return-object p1
.end method

.method private static goEpsonEncodeString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    const-string v0, " "

    const-string v1, "_"

    .line 121
    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method checkGoEpsonResultUrl(Ljava/lang/String;)Z
    .locals 3
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 64
    :cond_0
    :try_start_0
    new-instance v1, Ljava/net/URL;

    invoke-direct {v1, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    const-string p1, "www.epson.com"

    .line 69
    invoke-virtual {v1}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    const-string p1, "/"

    .line 70
    invoke-virtual {v1}, Ljava/net/URL;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 71
    invoke-virtual {v1}, Ljava/net/URL;->getPath()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    if-gtz p1, :cond_2

    :cond_1
    return v0

    :cond_2
    const/4 p1, 0x1

    return p1

    :catch_0
    return v0
.end method

.method getGoEpsonRedirectLocation(Ljava/net/URL;)Ljava/lang/String;
    .locals 3
    .param p1    # Ljava/net/URL;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    const/4 v0, 0x0

    .line 133
    :try_start_0
    invoke-direct {p0, p1}, Lepson/print/inkrpln/GoEpsonClient;->getGoEpsonConnection(Ljava/net/URL;)Ljava/net/HttpURLConnection;

    move-result-object p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/16 v1, 0x2710

    .line 134
    :try_start_1
    invoke-virtual {p1, v1}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 135
    invoke-virtual {p1, v1}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    const/4 v1, 0x0

    .line 137
    invoke-virtual {p1, v1}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    const-string v1, "GET"

    .line 138
    invoke-virtual {p1, v1}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 139
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->connect()V

    .line 141
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v1
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    packed-switch v1, :pswitch_data_0

    if-eqz p1, :cond_1

    .line 156
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_0

    :pswitch_0
    :try_start_2
    const-string v1, "Location"

    .line 145
    invoke-virtual {p1, v1}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz p1, :cond_0

    .line 156
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_0
    return-object v0

    :cond_1
    :goto_0
    return-object v0

    :catchall_0
    move-exception v0

    goto :goto_1

    :catch_0
    nop

    goto :goto_2

    :catchall_1
    move-exception p1

    move-object v2, v0

    move-object v0, p1

    move-object p1, v2

    :goto_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_2
    throw v0

    :catch_1
    move-object p1, v0

    :goto_2
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_3
    return-object v0

    :pswitch_data_0
    .packed-switch 0x12d
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public getRedirectLocation(Lepson/print/inkrpln/GoEpsonClient$ReadyInkParams;Z)Ljava/lang/String;
    .locals 1
    .param p1    # Lepson/print/inkrpln/GoEpsonClient$ReadyInkParams;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 34
    invoke-virtual {p0, p1, p2}, Lepson/print/inkrpln/GoEpsonClient;->makeUrl(Lepson/print/inkrpln/GoEpsonClient$ReadyInkParams;Z)Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x0

    .line 37
    :try_start_0
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 45
    invoke-virtual {p0, v0}, Lepson/print/inkrpln/GoEpsonClient;->getGoEpsonRedirectLocation(Ljava/net/URL;)Ljava/lang/String;

    move-result-object p1

    .line 46
    invoke-virtual {p0, p1}, Lepson/print/inkrpln/GoEpsonClient;->checkGoEpsonResultUrl(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    move-object p1, p2

    :goto_0
    return-object p1

    :catch_0
    return-object p2
.end method

.method protected getServerAndPortString()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .line 115
    invoke-static {}, Lepson/print/IprintApplication;->getInstance()Lepson/print/IprintApplication;

    move-result-object v0

    invoke-virtual {v0}, Lepson/print/IprintApplication;->getGoEpsonServerName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public makeUrl(Lepson/print/inkrpln/GoEpsonClient$ReadyInkParams;Z)Ljava/lang/String;
    .locals 3
    .param p1    # Lepson/print/inkrpln/GoEpsonClient$ReadyInkParams;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 81
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    if-eqz p2, :cond_0

    const-string p2, "https"

    goto :goto_0

    :cond_0
    const-string p2, "http"

    .line 84
    :goto_0
    invoke-virtual {v0, p2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 86
    invoke-virtual {p0}, Lepson/print/inkrpln/GoEpsonClient;->getServerAndPortString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Landroid/net/Uri$Builder;->encodedAuthority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string p2, "redirect.aspx"

    .line 87
    invoke-virtual {v0, p2}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 89
    iget-object p2, p1, Lepson/print/inkrpln/GoEpsonClient$ReadyInkParams;->language:Ljava/lang/String;

    if-eqz p2, :cond_1

    const-string p2, "LG2"

    .line 90
    iget-object v1, p1, Lepson/print/inkrpln/GoEpsonClient$ReadyInkParams;->language:Ljava/lang/String;

    invoke-virtual {v0, p2, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 92
    :cond_1
    iget-object p2, p1, Lepson/print/inkrpln/GoEpsonClient$ReadyInkParams;->country:Ljava/lang/String;

    if-eqz p2, :cond_2

    const-string p2, "CN2"

    .line 93
    iget-object v1, p1, Lepson/print/inkrpln/GoEpsonClient$ReadyInkParams;->country:Ljava/lang/String;

    invoke-virtual {v0, p2, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_2
    const-string p2, "CTC"

    const-string v1, "ReadyInkInvitation"

    .line 95
    invoke-virtual {v0, p2, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 96
    iget-object p2, p1, Lepson/print/inkrpln/GoEpsonClient$ReadyInkParams;->printerName:Ljava/lang/String;

    if-eqz p2, :cond_3

    const-string p2, "PRN"

    .line 97
    iget-object v1, p1, Lepson/print/inkrpln/GoEpsonClient$ReadyInkParams;->printerName:Ljava/lang/String;

    invoke-static {v1}, Lepson/print/inkrpln/GoEpsonClient;->goEpsonEncodeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_3
    const-string p2, "OSC"

    const-string v1, "ARD"

    .line 99
    invoke-virtual {v0, p2, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string p2, "OSV"

    const-string v1, "ARDAPI_1.0.26"

    .line 100
    invoke-virtual {v0, p2, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 101
    iget-object p2, p1, Lepson/print/inkrpln/GoEpsonClient$ReadyInkParams;->androidModel:Ljava/lang/String;

    if-eqz p2, :cond_4

    const-string p2, "OATR"

    .line 102
    iget-object v1, p1, Lepson/print/inkrpln/GoEpsonClient$ReadyInkParams;->androidModel:Ljava/lang/String;

    invoke-static {v1}, Lepson/print/inkrpln/GoEpsonClient;->goEpsonEncodeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 104
    :cond_4
    iget-object p2, p1, Lepson/print/inkrpln/GoEpsonClient$ReadyInkParams;->printerSerial:Ljava/lang/String;

    if-eqz p2, :cond_5

    iget-object p2, p1, Lepson/print/inkrpln/GoEpsonClient$ReadyInkParams;->printerSerial:Ljava/lang/String;

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result p2

    const/4 v1, 0x4

    if-lt p2, v1, :cond_5

    const-string p2, "SID"

    .line 105
    iget-object p1, p1, Lepson/print/inkrpln/GoEpsonClient$ReadyInkParams;->printerSerial:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p1, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p2, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 108
    :cond_5
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p1

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

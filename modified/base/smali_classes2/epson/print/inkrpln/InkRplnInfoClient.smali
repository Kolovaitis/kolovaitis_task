.class public Lepson/print/inkrpln/InkRplnInfoClient;
.super Ljava/lang/Object;
.source "InkRplnInfoClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;
    }
.end annotation


# static fields
.field private static final INFO_PATH:Ljava/lang/String; = "sncheck"

.field private static final KEY_APPEND_SERIAL:Ljava/lang/String; = "appendSerial"

.field private static final KEY_BUTTON_TYPE:Ljava/lang/String; = "showButton"

.field private static final KEY_LANDING_URL:Ljava/lang/String; = "urlLandingPage"

.field private static final KEY_SHOW_INVITATION:Ljava/lang/String; = "showInvitation"

.field private static final VALUE_BUTTON_TYPE_BUY_INK_AND_READY_INK:Ljava/lang/String; = "BuyInk_ReadyInk"

.field private static final VALUE_BUTTON_TYPE_READY_INK_ONLY:Ljava/lang/String; = "ReadyInk"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static convertString(Ljava/lang/String;)Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;
    .locals 4
    .param p0    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    .line 75
    :cond_0
    new-instance v1, Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;

    invoke-direct {v1}, Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;-><init>()V

    .line 78
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string p0, "urlLandingPage"

    .line 80
    invoke-virtual {v2, p0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_1

    const-string p0, "urlLandingPage"

    .line 81
    invoke-virtual {v2, p0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v1, Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;->landingUrl:Ljava/lang/String;

    :cond_1
    const-string p0, "showButton"

    .line 84
    invoke-virtual {v2, p0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_3

    const/4 p0, 0x0

    .line 85
    iput p0, v1, Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;->buttonType:I

    const-string p0, "showButton"

    .line 86
    invoke-virtual {v2, p0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    const-string v3, "ReadyInk"

    .line 87
    invoke-virtual {v3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 p0, 0x1

    .line 88
    iput p0, v1, Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;->buttonType:I

    goto :goto_0

    :cond_2
    const-string v3, "BuyInk_ReadyInk"

    .line 89
    invoke-virtual {v3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    const/4 p0, 0x2

    .line 90
    iput p0, v1, Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;->buttonType:I

    :cond_3
    :goto_0
    const-string p0, "showInvitation"

    .line 94
    invoke-virtual {v2, p0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_4

    const-string p0, "showInvitation"

    .line 95
    invoke-virtual {v2, p0}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result p0

    iput-boolean p0, v1, Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;->showInvitation:Z

    :cond_4
    const-string p0, "appendSerial"

    .line 98
    invoke-virtual {v2, p0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_5

    const-string p0, "appendSerial"

    .line 99
    invoke-virtual {v2, p0}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result p0

    iput-boolean p0, v1, Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;->appendSerial:Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_5
    return-object v1

    :catch_0
    return-object v0
.end method

.method static getInkReplenishInfoUrl(Ljava/lang/String;Ljava/lang/String;)Ljava/net/URL;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/MalformedURLException;
        }
    .end annotation

    .line 54
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    .line 55
    invoke-virtual {v0, p0}, Landroid/net/Uri$Builder;->encodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string p0, "sncheck"

    .line 56
    invoke-virtual {v0, p0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 57
    invoke-static {p1}, Lepson/print/inkrpln/InkReplenishSystem;->encodePrinterSerial(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 58
    invoke-virtual {v0, p0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 60
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p0

    .line 61
    new-instance p1, Ljava/net/URL;

    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public getInkDsInfo(Ljava/lang/String;Ljava/lang/String;)Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 41
    invoke-static {p2, p1}, Lepson/print/inkrpln/InkRplnInfoClient;->getInkReplenishInfoUrl(Ljava/lang/String;Ljava/lang/String;)Ljava/net/URL;

    move-result-object p1

    .line 42
    invoke-static {p1}, Lepson/print/inkrpln/SimpleHttpClient;->getString(Ljava/net/URL;)Ljava/lang/String;

    move-result-object p1

    .line 44
    invoke-static {p1}, Lepson/print/inkrpln/InkRplnInfoClient;->convertString(Ljava/lang/String;)Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;

    move-result-object p1

    return-object p1
.end method

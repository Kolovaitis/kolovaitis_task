.class public Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;
.super Ljava/lang/Object;
.source "InkRplnInfoClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/inkrpln/InkRplnInfoClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "InkRsInfo"
.end annotation


# instance fields
.field public appendSerial:Z

.field public buttonType:I

.field public landingUrl:Ljava/lang/String;

.field public showInvitation:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 139
    iput-object v0, p0, Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;->landingUrl:Ljava/lang/String;

    const/4 v0, 0x0

    .line 140
    iput v0, p0, Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;->buttonType:I

    .line 141
    iput-boolean v0, p0, Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;->showInvitation:Z

    return-void
.end method


# virtual methods
.method public copy(Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    .line 132
    :cond_0
    iget-object v0, p1, Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;->landingUrl:Ljava/lang/String;

    iput-object v0, p0, Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;->landingUrl:Ljava/lang/String;

    .line 133
    iget v0, p1, Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;->buttonType:I

    iput v0, p0, Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;->buttonType:I

    .line 134
    iget-boolean v0, p1, Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;->showInvitation:Z

    iput-boolean v0, p0, Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;->showInvitation:Z

    .line 135
    iget-boolean p1, p1, Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;->appendSerial:Z

    iput-boolean p1, p0, Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;->appendSerial:Z

    return-void
.end method

.method public getButtonType()I
    .locals 1

    .line 150
    iget v0, p0, Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;->buttonType:I

    return v0
.end method

.method public needToDisplayInvitationDialog()Z
    .locals 1

    .line 146
    iget-boolean v0, p0, Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;->showInvitation:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lepson/print/inkrpln/InkRplnInfoClient$InkRsInfo;->landingUrl:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.class Lepson/print/ActivityDocsPrintPreview$CustomProDialog;
.super Landroid/app/Dialog;
.source "ActivityDocsPrintPreview.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/ActivityDocsPrintPreview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CustomProDialog"
.end annotation


# instance fields
.field private hasCancelButton:Z

.field private mLayoutId:I

.field final synthetic this$0:Lepson/print/ActivityDocsPrintPreview;


# direct methods
.method public constructor <init>(Lepson/print/ActivityDocsPrintPreview;Landroid/content/Context;IIZ)V
    .locals 0

    .line 3069
    iput-object p1, p0, Lepson/print/ActivityDocsPrintPreview$CustomProDialog;->this$0:Lepson/print/ActivityDocsPrintPreview;

    .line 3070
    invoke-direct {p0, p2, p3}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 3071
    iput p4, p0, Lepson/print/ActivityDocsPrintPreview$CustomProDialog;->mLayoutId:I

    .line 3072
    iput-boolean p5, p0, Lepson/print/ActivityDocsPrintPreview$CustomProDialog;->hasCancelButton:Z

    const/4 p1, 0x0

    .line 3073
    invoke-virtual {p0, p1}, Lepson/print/ActivityDocsPrintPreview$CustomProDialog;->setCancelable(Z)V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .line 3078
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    const/4 p1, 0x1

    .line 3079
    invoke-virtual {p0, p1}, Lepson/print/ActivityDocsPrintPreview$CustomProDialog;->requestWindowFeature(I)Z

    .line 3080
    invoke-virtual {p0}, Lepson/print/ActivityDocsPrintPreview$CustomProDialog;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    iget v1, p0, Lepson/print/ActivityDocsPrintPreview$CustomProDialog;->mLayoutId:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0800cf

    .line 3081
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    new-instance v3, Lepson/print/ActivityDocsPrintPreview$CustomProDialog$1;

    invoke-direct {v3, p0}, Lepson/print/ActivityDocsPrintPreview$CustomProDialog$1;-><init>(Lepson/print/ActivityDocsPrintPreview$CustomProDialog;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f0800b5

    .line 3089
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 3090
    invoke-virtual {p0, v0}, Lepson/print/ActivityDocsPrintPreview$CustomProDialog;->setContentView(Landroid/view/View;)V

    .line 3092
    iget-boolean v0, p0, Lepson/print/ActivityDocsPrintPreview$CustomProDialog;->hasCancelButton:Z

    if-ne v0, p1, :cond_0

    .line 3093
    invoke-virtual {p0, v1}, Lepson/print/ActivityDocsPrintPreview$CustomProDialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 3095
    :cond_0
    invoke-virtual {p0, v1}, Lepson/print/ActivityDocsPrintPreview$CustomProDialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    invoke-virtual {p1, v3}, Landroid/widget/Button;->setVisibility(I)V

    :goto_0
    const p1, 0x7f0801f8

    .line 3098
    invoke-virtual {p0, p1}, Lepson/print/ActivityDocsPrintPreview$CustomProDialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    .line 3099
    iget-object v0, p0, Lepson/print/ActivityDocsPrintPreview$CustomProDialog;->this$0:Lepson/print/ActivityDocsPrintPreview;

    const v1, 0x7f0e034b

    invoke-virtual {v0, v1}, Lepson/print/ActivityDocsPrintPreview;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

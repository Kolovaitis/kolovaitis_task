.class public Lepson/print/imgsel/ImageSelector;
.super Ljava/lang/Object;
.source "ImageSelector.java"


# static fields
.field public static final MAX_SELECT_NUMBER:I = 0x1e


# instance fields
.field private mImageFilenameList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lepson/print/imgsel/ImageSelector;->mImageFilenameList:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lepson/print/imgsel/ImageSelector;->mImageFilenameList:Ljava/util/ArrayList;

    .line 23
    iget-object p1, p0, Lepson/print/imgsel/ImageSelector;->mImageFilenameList:Ljava/util/ArrayList;

    if-nez p1, :cond_0

    .line 24
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lepson/print/imgsel/ImageSelector;->mImageFilenameList:Ljava/util/ArrayList;

    :cond_0
    return-void
.end method


# virtual methods
.method public add(Ljava/lang/String;)Z
    .locals 2

    .line 57
    invoke-virtual {p0}, Lepson/print/imgsel/ImageSelector;->selectedNumber()I

    move-result v0

    const/16 v1, 0x1e

    if-ge v0, v1, :cond_1

    iget-object v0, p0, Lepson/print/imgsel/ImageSelector;->mImageFilenameList:Ljava/util/ArrayList;

    .line 58
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 62
    :cond_0
    iget-object v0, p0, Lepson/print/imgsel/ImageSelector;->mImageFilenameList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 p1, 0x1

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return p1
.end method

.method public canAdd()Z
    .locals 2

    .line 112
    iget-object v0, p0, Lepson/print/imgsel/ImageSelector;->mImageFilenameList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/16 v1, 0x1e

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public clear()V
    .locals 1

    .line 80
    iget-object v0, p0, Lepson/print/imgsel/ImageSelector;->mImageFilenameList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method public getFileArrayList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 95
    iget-object v0, p0, Lepson/print/imgsel/ImageSelector;->mImageFilenameList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public isSelected(Ljava/lang/String;)Z
    .locals 1

    .line 87
    iget-object v0, p0, Lepson/print/imgsel/ImageSelector;->mImageFilenameList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public remove(Ljava/lang/String;)Z
    .locals 1

    .line 71
    iget-object v0, p0, Lepson/print/imgsel/ImageSelector;->mImageFilenameList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    const/4 p1, 0x1

    return p1
.end method

.method public replaceFiles(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 30
    iput-object p1, p0, Lepson/print/imgsel/ImageSelector;->mImageFilenameList:Ljava/util/ArrayList;

    return-void
.end method

.method public selectedNumber()I
    .locals 1

    .line 103
    iget-object v0, p0, Lepson/print/imgsel/ImageSelector;->mImageFilenameList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public toggleSelect(Ljava/lang/String;)Z
    .locals 3

    .line 39
    iget-object v0, p0, Lepson/print/imgsel/ImageSelector;->mImageFilenameList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 40
    iget-object v0, p0, Lepson/print/imgsel/ImageSelector;->mImageFilenameList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    return v1

    .line 44
    :cond_0
    invoke-virtual {p0}, Lepson/print/imgsel/ImageSelector;->selectedNumber()I

    move-result v0

    const/16 v2, 0x1e

    if-lt v0, v2, :cond_1

    return v1

    .line 47
    :cond_1
    iget-object v0, p0, Lepson/print/imgsel/ImageSelector;->mImageFilenameList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 p1, 0x1

    return p1
.end method

.class public Lepson/print/imgsel/BitmapCache;
.super Ljava/lang/Object;
.source "BitmapCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/imgsel/BitmapCache$FileAttribute;,
        Lepson/print/imgsel/BitmapCache$BitmapDataWrite;,
        Lepson/print/imgsel/BitmapCache$DataWriter;
    }
.end annotation


# static fields
.field private static final JOURNAL_FILE_NAME:Ljava/lang/String; = "journal.bin"

.field private static final JPEG_QUALITY:I = 0x5a


# instance fields
.field private mCacheDirectory:Ljava/io/File;

.field private mCacheFileInfoMap:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/io/File;",
            "Lepson/print/imgsel/BitmapCache$FileAttribute;",
            ">;"
        }
    .end annotation
.end field

.field private mCacheSizeLimit:J

.field private final mCleanupCallable:Ljava/util/concurrent/Callable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Callable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final mExecutorService:Ljava/util/concurrent/ExecutorService;

.field private mTotalCacheSize:J


# direct methods
.method public constructor <init>()V
    .locals 9

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Ljava/util/LinkedHashMap;

    const/16 v1, 0x20

    const/high16 v2, 0x3f400000    # 0.75f

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Ljava/util/LinkedHashMap;-><init>(IFZ)V

    iput-object v0, p0, Lepson/print/imgsel/BitmapCache;->mCacheFileInfoMap:Ljava/util/LinkedHashMap;

    const-wide/32 v0, 0x989680

    .line 59
    iput-wide v0, p0, Lepson/print/imgsel/BitmapCache;->mCacheSizeLimit:J

    .line 66
    new-instance v0, Ljava/util/concurrent/ThreadPoolExecutor;

    sget-object v7, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v8, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v8}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    const/4 v3, 0x0

    const/4 v4, 0x1

    const-wide/16 v5, 0x3c

    move-object v2, v0

    invoke-direct/range {v2 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;)V

    iput-object v0, p0, Lepson/print/imgsel/BitmapCache;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    .line 71
    new-instance v0, Lepson/print/imgsel/BitmapCache$1;

    invoke-direct {v0, p0}, Lepson/print/imgsel/BitmapCache$1;-><init>(Lepson/print/imgsel/BitmapCache;)V

    iput-object v0, p0, Lepson/print/imgsel/BitmapCache;->mCleanupCallable:Ljava/util/concurrent/Callable;

    return-void
.end method

.method private declared-synchronized calcTotalSizeAndDeleteNotExistEntry()V
    .locals 6

    monitor-enter p0

    const-wide/16 v0, 0x0

    .line 178
    :try_start_0
    iput-wide v0, p0, Lepson/print/imgsel/BitmapCache;->mTotalCacheSize:J

    .line 180
    iget-object v0, p0, Lepson/print/imgsel/BitmapCache;->mCacheFileInfoMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 181
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 182
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/io/File;

    .line 183
    invoke-virtual {v2}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lepson/print/imgsel/BitmapCache;->getCacheFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    .line 184
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_0

    .line 185
    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 187
    :cond_0
    iget-wide v4, p0, Lepson/print/imgsel/BitmapCache;->mTotalCacheSize:J

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v2

    add-long/2addr v4, v2

    iput-wide v4, p0, Lepson/print/imgsel/BitmapCache;->mTotalCacheSize:J

    goto :goto_0

    .line 191
    :cond_1
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/File;

    .line 192
    iget-object v2, p0, Lepson/print/imgsel/BitmapCache;->mCacheFileInfoMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, v1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 195
    :cond_2
    invoke-virtual {p0}, Lepson/print/imgsel/BitmapCache;->trimToSize()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 196
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized clearCacheFileInfoMap()V
    .locals 4

    monitor-enter p0

    .line 252
    :try_start_0
    iget-object v0, p0, Lepson/print/imgsel/BitmapCache;->mCacheFileInfoMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    const-wide/16 v0, 0x0

    .line 253
    iput-wide v0, p0, Lepson/print/imgsel/BitmapCache;->mTotalCacheSize:J

    .line 255
    iget-object v0, p0, Lepson/print/imgsel/BitmapCache;->mCacheDirectory:Ljava/io/File;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 256
    monitor-exit p0

    return-void

    .line 261
    :cond_0
    :try_start_1
    iget-object v0, p0, Lepson/print/imgsel/BitmapCache;->mCacheDirectory:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 263
    invoke-virtual {v3}, Ljava/io/File;->delete()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 265
    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized deleteNoEntryCacheFile()V
    .locals 6

    monitor-enter p0

    .line 203
    :try_start_0
    iget-object v0, p0, Lepson/print/imgsel/BitmapCache;->mCacheDirectory:Ljava/io/File;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 204
    monitor-exit p0

    return-void

    .line 207
    :cond_0
    :try_start_1
    iget-object v0, p0, Lepson/print/imgsel/BitmapCache;->mCacheFileInfoMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 208
    new-instance v1, Ljava/util/HashSet;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/HashSet;-><init>(I)V

    .line 209
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/io/File;

    .line 210
    invoke-virtual {v2}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lepson/print/imgsel/BitmapCache;->getCacheFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 213
    :cond_1
    iget-object v0, p0, Lepson/print/imgsel/BitmapCache;->mCacheDirectory:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 214
    array-length v2, v0

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v2, :cond_3

    aget-object v4, v0, v3

    .line 215
    invoke-interface {v1, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 218
    invoke-virtual {v4}, Ljava/io/File;->delete()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 221
    :cond_3
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private getCacheFile(Ljava/lang/String;)Ljava/io/File;
    .locals 3

    .line 453
    iget-object v0, p0, Lepson/print/imgsel/BitmapCache;->mCacheDirectory:Ljava/io/File;

    .line 459
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object p1

    invoke-static {p1}, Ljava/util/UUID;->nameUUIDFromBytes([B)Ljava/util/UUID;

    move-result-object p1

    .line 460
    invoke-virtual {p1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p1

    .line 461
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lepson/print/imgsel/BitmapCache;->mCacheDirectory:Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ".jpg"

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private getJournalFile()Ljava/io/File;
    .locals 3

    .line 311
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lepson/print/imgsel/BitmapCache;->mCacheDirectory:Ljava/io/File;

    const-string v2, "journal.bin"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private declared-synchronized restoreJournal()V
    .locals 6

    monitor-enter p0

    .line 134
    :try_start_0
    invoke-direct {p0}, Lepson/print/imgsel/BitmapCache;->getJournalFile()Ljava/io/File;

    move-result-object v0

    .line 135
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 136
    invoke-direct {p0}, Lepson/print/imgsel/BitmapCache;->clearCacheFileInfoMap()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 137
    monitor-exit p0

    return-void

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 144
    :try_start_1
    new-instance v3, Ljava/io/ObjectInputStream;

    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v3, v4}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 146
    :try_start_2
    invoke-virtual {v3}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/LinkedHashMap;

    iput-object v1, p0, Lepson/print/imgsel/BitmapCache;->mCacheFileInfoMap:Ljava/util/LinkedHashMap;

    .line 147
    invoke-direct {p0}, Lepson/print/imgsel/BitmapCache;->calcTotalSizeAndDeleteNotExistEntry()V

    .line 148
    invoke-direct {p0}, Lepson/print/imgsel/BitmapCache;->deleteNoEntryCacheFile()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    const/4 v2, 0x1

    .line 156
    :goto_0
    :try_start_3
    invoke-virtual {v3}, Ljava/io/ObjectInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto :goto_4

    :catch_0
    move-exception v1

    goto :goto_1

    :catch_1
    nop

    goto :goto_3

    :catchall_0
    move-exception v0

    move-object v3, v1

    goto :goto_2

    :catch_2
    move-exception v3

    move-object v5, v3

    move-object v3, v1

    move-object v1, v5

    .line 152
    :goto_1
    :try_start_4
    invoke-virtual {v1}, Ljava/lang/ClassNotFoundException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    if-eqz v3, :cond_2

    goto :goto_0

    :catchall_1
    move-exception v0

    :goto_2
    if-eqz v3, :cond_1

    .line 156
    :try_start_5
    invoke-virtual {v3}, Ljava/io/ObjectInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 159
    :catch_3
    :cond_1
    :try_start_6
    throw v0

    :catch_4
    move-object v3, v1

    :goto_3
    if-eqz v3, :cond_2

    goto :goto_0

    :catch_5
    :cond_2
    :goto_4
    if-nez v2, :cond_4

    .line 163
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 165
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 167
    :cond_3
    invoke-direct {p0}, Lepson/print/imgsel/BitmapCache;->clearCacheFileInfoMap()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 169
    :cond_4
    monitor-exit p0

    return-void

    :catchall_2
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public addBitmap(Ljava/io/File;Landroid/graphics/Bitmap;)V
    .locals 1

    .line 367
    new-instance v0, Lepson/print/imgsel/BitmapCache$BitmapDataWrite;

    invoke-direct {v0, p2}, Lepson/print/imgsel/BitmapCache$BitmapDataWrite;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {p0, p1, v0}, Lepson/print/imgsel/BitmapCache;->addFile(Ljava/io/File;Lepson/print/imgsel/BitmapCache$DataWriter;)V

    return-void
.end method

.method public addFile(Ljava/io/File;Lepson/print/imgsel/BitmapCache$DataWriter;)V
    .locals 5

    .line 416
    invoke-virtual {p1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lepson/print/imgsel/BitmapCache;->getCacheFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const-wide/16 v1, 0x0

    .line 423
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 424
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v1

    .line 427
    :cond_1
    invoke-interface {p2, v0}, Lepson/print/imgsel/BitmapCache$DataWriter;->writeData(Ljava/io/File;)Z

    move-result p2

    if-nez p2, :cond_3

    .line 429
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result p2

    if-eqz p2, :cond_2

    .line 431
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 433
    :cond_2
    iget-object p2, p0, Lepson/print/imgsel/BitmapCache;->mCacheFileInfoMap:Ljava/util/LinkedHashMap;

    invoke-virtual {p2, p1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 434
    iget-wide p1, p0, Lepson/print/imgsel/BitmapCache;->mTotalCacheSize:J

    sub-long/2addr p1, v1

    iput-wide p1, p0, Lepson/print/imgsel/BitmapCache;->mTotalCacheSize:J

    return-void

    .line 439
    :cond_3
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v3

    .line 442
    iget-object p2, p0, Lepson/print/imgsel/BitmapCache;->mCacheFileInfoMap:Ljava/util/LinkedHashMap;

    new-instance v0, Lepson/print/imgsel/BitmapCache$FileAttribute;

    invoke-direct {v0, p1}, Lepson/print/imgsel/BitmapCache$FileAttribute;-><init>(Ljava/io/File;)V

    invoke-virtual {p2, p1, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 443
    iget-wide p1, p0, Lepson/print/imgsel/BitmapCache;->mTotalCacheSize:J

    sub-long/2addr v3, v1

    add-long/2addr p1, v3

    iput-wide p1, p0, Lepson/print/imgsel/BitmapCache;->mTotalCacheSize:J

    .line 445
    invoke-virtual {p0}, Lepson/print/imgsel/BitmapCache;->trimToSize()V

    return-void
.end method

.method public declared-synchronized getBitmap(Ljava/io/File;)Landroid/graphics/Bitmap;
    .locals 0

    monitor-enter p0

    .line 317
    :try_start_0
    invoke-virtual {p0, p1}, Lepson/print/imgsel/BitmapCache;->getFile(Ljava/io/File;)Ljava/io/File;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 319
    monitor-exit p0

    return-object p1

    .line 322
    :cond_0
    :try_start_1
    invoke-virtual {p1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 324
    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized getFile(Ljava/io/File;)Ljava/io/File;
    .locals 2

    monitor-enter p0

    .line 339
    :try_start_0
    iget-object v0, p0, Lepson/print/imgsel/BitmapCache;->mCacheFileInfoMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/print/imgsel/BitmapCache$FileAttribute;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 342
    monitor-exit p0

    return-object v1

    .line 345
    :cond_0
    :try_start_1
    invoke-virtual {v0, p1}, Lepson/print/imgsel/BitmapCache$FileAttribute;->isSameAttribute(Ljava/io/File;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 348
    invoke-virtual {p1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lepson/print/imgsel/BitmapCache;->getCacheFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object p1

    .line 349
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    .line 352
    invoke-direct {p0}, Lepson/print/imgsel/BitmapCache;->calcTotalSizeAndDeleteNotExistEntry()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 353
    monitor-exit p0

    return-object v1

    .line 356
    :cond_1
    monitor-exit p0

    return-object p1

    .line 359
    :cond_2
    :try_start_2
    invoke-virtual {p0, p1}, Lepson/print/imgsel/BitmapCache;->removeCacheFile(Ljava/io/File;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 361
    monitor-exit p0

    return-object v1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public initialize(Ljava/io/File;)Z
    .locals 0

    .line 92
    invoke-virtual {p0, p1}, Lepson/print/imgsel/BitmapCache;->setCacheDirectory(Ljava/io/File;)Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return p1

    .line 97
    :cond_0
    invoke-direct {p0}, Lepson/print/imgsel/BitmapCache;->restoreJournal()V

    const/4 p1, 0x1

    return p1
.end method

.method public initialize(Ljava/io/File;I)Z
    .locals 2

    int-to-long v0, p2

    .line 85
    iput-wide v0, p0, Lepson/print/imgsel/BitmapCache;->mCacheSizeLimit:J

    .line 86
    invoke-virtual {p0, p1}, Lepson/print/imgsel/BitmapCache;->initialize(Ljava/io/File;)Z

    move-result p1

    return p1
.end method

.method public declared-synchronized removeCacheFile(Ljava/io/File;)V
    .locals 5

    monitor-enter p0

    .line 238
    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lepson/print/imgsel/BitmapCache;->getCacheFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    const-wide/16 v1, 0x0

    if-eqz v0, :cond_0

    .line 242
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v1

    .line 244
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 246
    :cond_0
    iget-wide v3, p0, Lepson/print/imgsel/BitmapCache;->mTotalCacheSize:J

    sub-long/2addr v3, v1

    iput-wide v3, p0, Lepson/print/imgsel/BitmapCache;->mTotalCacheSize:J

    .line 247
    iget-object v0, p0, Lepson/print/imgsel/BitmapCache;->mCacheFileInfoMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 248
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized saveJournal()V
    .locals 8

    monitor-enter p0

    .line 272
    :try_start_0
    invoke-direct {p0}, Lepson/print/imgsel/BitmapCache;->getJournalFile()Ljava/io/File;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 276
    :try_start_1
    new-instance v3, Ljava/io/ObjectOutputStream;

    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v3, v4}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 277
    :try_start_2
    iget-object v1, p0, Lepson/print/imgsel/BitmapCache;->mCacheFileInfoMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v3, v1}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 v2, 0x1

    .line 285
    :try_start_3
    invoke-virtual {v3}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto :goto_1

    :catchall_0
    move-exception v0

    move-object v1, v3

    goto :goto_2

    :catch_0
    move-exception v1

    move-object v7, v3

    move-object v3, v1

    move-object v1, v7

    goto :goto_0

    :catchall_1
    move-exception v0

    goto :goto_2

    :catch_1
    move-exception v3

    :goto_0
    :try_start_4
    const-string v4, "BitmapCache"

    .line 281
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "in saveJournal() exception <"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ">"

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    if-eqz v1, :cond_0

    .line 285
    :try_start_5
    invoke-virtual {v1}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :catch_2
    :cond_0
    :goto_1
    if-nez v2, :cond_1

    .line 293
    :try_start_6
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 295
    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 304
    :cond_1
    monitor-exit p0

    return-void

    :goto_2
    if-eqz v1, :cond_2

    .line 285
    :try_start_7
    invoke-virtual {v1}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 288
    :catch_3
    :cond_2
    :try_start_8
    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    :catchall_2
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setCacheDirectory(Ljava/io/File;)Z
    .locals 2

    const/4 v0, 0x0

    .line 109
    iput-object v0, p0, Lepson/print/imgsel/BitmapCache;->mCacheDirectory:Ljava/io/File;

    .line 110
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 111
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->canRead()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    return v1

    .line 115
    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->mkdirs()Z

    move-result v0

    if-nez v0, :cond_2

    return v1

    .line 120
    :cond_2
    iput-object p1, p0, Lepson/print/imgsel/BitmapCache;->mCacheDirectory:Ljava/io/File;

    const/4 p1, 0x1

    return p1
.end method

.method public declared-synchronized trimToSize()V
    .locals 5

    monitor-enter p0

    .line 228
    :goto_0
    :try_start_0
    iget-wide v0, p0, Lepson/print/imgsel/BitmapCache;->mTotalCacheSize:J

    iget-wide v2, p0, Lepson/print/imgsel/BitmapCache;->mCacheSizeLimit:J

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    .line 230
    iget-object v0, p0, Lepson/print/imgsel/BitmapCache;->mCacheFileInfoMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 231
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 232
    invoke-virtual {p0, v0}, Lepson/print/imgsel/BitmapCache;->removeCacheFile(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 234
    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

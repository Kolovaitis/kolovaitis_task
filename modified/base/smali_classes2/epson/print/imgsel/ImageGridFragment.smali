.class public Lepson/print/imgsel/ImageGridFragment;
.super Landroid/support/v4/app/Fragment;
.source "ImageGridFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/imgsel/ImageGridFragment$MyGridLayoutListener;,
        Lepson/print/imgsel/ImageGridFragment$ImageGridListener;
    }
.end annotation


# static fields
.field private static final ARG_PHOTO_FOLDER_PATH:Ljava/lang/String; = "photo_folder_path"

.field public static final DIALOG_CODE_NOTIFY:I = 0x3

.field public static final PHOTO_SELECTION_MODE_DEFAULT:I = 0x0

.field public static final PHOTO_SELECTION_MODE_MULTISELECT:I = 0x1

.field public static final PHOTO_SELECTION_MODE_ZOOM:I = 0x0

.field private static final REQUEST_CODE_IMAGE_PAGER:I = 0x2


# instance fields
.field private mAdapter:Lepson/print/imgsel/Alt2ViewImageAdapter;

.field private mDoubleClickGuard:Z

.field private mGridView:Landroid/widget/GridView;

.field private mImageFindTask:Lepson/print/imgsel/ImageFindTask;

.field private mImageSelector:Lepson/print/imgsel/ImageSelector;

.field private mImageThumbnailTask:Lepson/print/imgsel/ImageThumbnailTask;

.field private mPhotoFolderPath:Ljava/lang/String;

.field private mPhotoSelectionMode:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 87
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    const/4 v0, 0x0

    .line 66
    iput v0, p0, Lepson/print/imgsel/ImageGridFragment;->mPhotoSelectionMode:I

    return-void
.end method

.method protected static getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;
    .locals 2

    const-string v0, "PREFS_PHOTO"

    const/4 v1, 0x0

    .line 83
    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p0

    return-object p0
.end method

.method public static newInstance(Ljava/lang/String;)Lepson/print/imgsel/ImageGridFragment;
    .locals 3

    .line 75
    new-instance v0, Lepson/print/imgsel/ImageGridFragment;

    invoke-direct {v0}, Lepson/print/imgsel/ImageGridFragment;-><init>()V

    .line 76
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "photo_folder_path"

    .line 77
    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    invoke-virtual {v0, v1}, Lepson/print/imgsel/ImageGridFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v0
.end method

.method private restartImageFindTask()V
    .locals 5

    .line 369
    invoke-virtual {p0}, Lepson/print/imgsel/ImageGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lepson/print/imgsel/ImageGridFragment$ImageGridListener;

    if-nez v0, :cond_0

    return-void

    .line 375
    :cond_0
    new-instance v1, Lepson/print/imgsel/ImageFindTask;

    iget-object v2, p0, Lepson/print/imgsel/ImageGridFragment;->mPhotoFolderPath:Ljava/lang/String;

    invoke-direct {v1, p0, v2}, Lepson/print/imgsel/ImageFindTask;-><init>(Lepson/print/imgsel/ImageGridFragment;Ljava/lang/String;)V

    iput-object v1, p0, Lepson/print/imgsel/ImageGridFragment;->mImageFindTask:Lepson/print/imgsel/ImageFindTask;

    .line 376
    iget-object v1, p0, Lepson/print/imgsel/ImageGridFragment;->mImageFindTask:Lepson/print/imgsel/ImageFindTask;

    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v3, 0x1

    new-array v3, v3, [Lepson/print/imgsel/ImageFinder;

    const/4 v4, 0x0

    invoke-interface {v0}, Lepson/print/imgsel/ImageGridFragment$ImageGridListener;->getImageFinder()Lepson/print/imgsel/ImageFinder;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lepson/print/imgsel/ImageFindTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 379
    new-instance v0, Lepson/print/imgsel/ImageThumbnailTask;

    invoke-direct {v0, p0}, Lepson/print/imgsel/ImageThumbnailTask;-><init>(Lepson/print/imgsel/ImageGridFragment;)V

    iput-object v0, p0, Lepson/print/imgsel/ImageGridFragment;->mImageThumbnailTask:Lepson/print/imgsel/ImageThumbnailTask;

    return-void
.end method

.method private startViewPagerActivity(I)V
    .locals 4

    .line 243
    iget-object v0, p0, Lepson/print/imgsel/ImageGridFragment;->mAdapter:Lepson/print/imgsel/Alt2ViewImageAdapter;

    if-eqz v0, :cond_3

    iget-object v1, p0, Lepson/print/imgsel/ImageGridFragment;->mImageSelector:Lepson/print/imgsel/ImageSelector;

    if-nez v1, :cond_0

    goto :goto_1

    .line 247
    :cond_0
    invoke-virtual {v0}, Lepson/print/imgsel/Alt2ViewImageAdapter;->getFileList()Ljava/util/ArrayList;

    move-result-object v0

    .line 249
    invoke-virtual {p0}, Lepson/print/imgsel/ImageGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lepson/print/imgsel/ImageSelectActivity;

    if-nez v1, :cond_1

    return-void

    .line 256
    :cond_1
    new-instance v2, Landroid/content/Intent;

    .line 257
    invoke-virtual {v1}, Lepson/print/imgsel/ImageSelectActivity;->showNfcInvalidMessageOnPagerActivity()Z

    move-result v3

    if-eqz v3, :cond_2

    const-class v3, Lepson/print/imgsel/ImageViewPagerWithNfcInvalidPrintMessageActivity;

    goto :goto_0

    :cond_2
    const-class v3, Lepson/print/imgsel/ImageViewPagerActivity;

    :goto_0
    invoke-direct {v2, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v3, "position"

    .line 261
    invoke-virtual {v2, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string p1, "selector"

    .line 262
    iget-object v3, p0, Lepson/print/imgsel/ImageGridFragment;->mImageSelector:Lepson/print/imgsel/ImageSelector;

    .line 263
    invoke-virtual {v3}, Lepson/print/imgsel/ImageSelector;->getFileArrayList()Ljava/util/ArrayList;

    move-result-object v3

    .line 262
    invoke-virtual {v2, p1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string p1, "single_file_mode"

    .line 266
    invoke-virtual {v1}, Lepson/print/imgsel/ImageSelectActivity;->singleImageMode()Z

    move-result v3

    .line 265
    invoke-virtual {v2, p1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string p1, "message_type"

    .line 268
    invoke-virtual {v1}, Lepson/print/imgsel/ImageSelectActivity;->getMessageType()I

    move-result v1

    .line 267
    invoke-virtual {v2, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 271
    invoke-static {v0}, Lepson/print/imgsel/ImageViewPagerActivity;->setFileList(Ljava/util/ArrayList;)V

    const/4 p1, 0x2

    .line 273
    invoke-virtual {p0, v2, p1}, Lepson/print/imgsel/ImageGridFragment;->startActivityForResult(Landroid/content/Intent;I)V

    return-void

    :cond_3
    :goto_1
    return-void
.end method


# virtual methods
.method public addItem(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lepson/print/ImageItem;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_3

    .line 402
    iget-object v0, p0, Lepson/print/imgsel/ImageGridFragment;->mAdapter:Lepson/print/imgsel/Alt2ViewImageAdapter;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lepson/print/imgsel/ImageGridFragment;->mImageThumbnailTask:Lepson/print/imgsel/ImageThumbnailTask;

    if-nez v0, :cond_0

    goto :goto_1

    .line 406
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/print/ImageItem;

    .line 408
    iget-object v1, p0, Lepson/print/imgsel/ImageGridFragment;->mAdapter:Lepson/print/imgsel/Alt2ViewImageAdapter;

    invoke-virtual {v1, v0}, Lepson/print/imgsel/Alt2ViewImageAdapter;->add(Lepson/print/ImageItem;)V

    .line 410
    iget-object v1, p0, Lepson/print/imgsel/ImageGridFragment;->mImageThumbnailTask:Lepson/print/imgsel/ImageThumbnailTask;

    invoke-virtual {v1, v0}, Lepson/print/imgsel/ImageThumbnailTask;->addLast(Lepson/print/ImageItem;)V

    goto :goto_0

    .line 414
    :cond_1
    iget-object p1, p0, Lepson/print/imgsel/ImageGridFragment;->mAdapter:Lepson/print/imgsel/Alt2ViewImageAdapter;

    invoke-virtual {p1}, Lepson/print/imgsel/Alt2ViewImageAdapter;->notifyDataSetChanged()V

    .line 417
    iget-object p1, p0, Lepson/print/imgsel/ImageGridFragment;->mImageThumbnailTask:Lepson/print/imgsel/ImageThumbnailTask;

    invoke-virtual {p1}, Lepson/print/imgsel/ImageThumbnailTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object p1

    sget-object v0, Landroid/os/AsyncTask$Status;->PENDING:Landroid/os/AsyncTask$Status;

    if-ne p1, v0, :cond_2

    .line 418
    iget-object p1, p0, Lepson/print/imgsel/ImageGridFragment;->mImageThumbnailTask:Lepson/print/imgsel/ImageThumbnailTask;

    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {p1, v0, v1}, Lepson/print/imgsel/ImageThumbnailTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_2
    return-void

    :cond_3
    :goto_1
    return-void
.end method

.method public changeThumnailPriority(Lepson/print/ImageItem;)V
    .locals 1

    .line 467
    iget-object v0, p0, Lepson/print/imgsel/ImageGridFragment;->mImageThumbnailTask:Lepson/print/imgsel/ImageThumbnailTask;

    if-nez v0, :cond_0

    return-void

    .line 471
    :cond_0
    invoke-virtual {v0, p1}, Lepson/print/imgsel/ImageThumbnailTask;->moveToFirst(Lepson/print/ImageItem;)V

    return-void
.end method

.method public clearItem()V
    .locals 1

    .line 479
    iget-object v0, p0, Lepson/print/imgsel/ImageGridFragment;->mAdapter:Lepson/print/imgsel/Alt2ViewImageAdapter;

    if-nez v0, :cond_0

    return-void

    .line 483
    :cond_0
    invoke-virtual {v0}, Lepson/print/imgsel/Alt2ViewImageAdapter;->clearImageItem()V

    .line 484
    iget-object v0, p0, Lepson/print/imgsel/ImageGridFragment;->mAdapter:Lepson/print/imgsel/Alt2ViewImageAdapter;

    invoke-virtual {v0}, Lepson/print/imgsel/Alt2ViewImageAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public getFolderName()Ljava/lang/String;
    .locals 1

    .line 393
    iget-object v0, p0, Lepson/print/imgsel/ImageGridFragment;->mPhotoFolderPath:Ljava/lang/String;

    return-object v0
.end method

.method public getPhotoSelectionMode()I
    .locals 1

    .line 92
    iget v0, p0, Lepson/print/imgsel/ImageGridFragment;->mPhotoSelectionMode:I

    return v0
.end method

.method public goNext()V
    .locals 1

    .line 281
    invoke-virtual {p0}, Lepson/print/imgsel/ImageGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lepson/print/imgsel/ImageSelectActivity;

    .line 284
    invoke-virtual {v0}, Lepson/print/imgsel/ImageSelectActivity;->goNext()V

    return-void
.end method

.method public isImageSelected(Ljava/lang/String;)Z
    .locals 1

    .line 388
    iget-object v0, p0, Lepson/print/imgsel/ImageGridFragment;->mImageSelector:Lepson/print/imgsel/ImageSelector;

    invoke-virtual {v0, p1}, Lepson/print/imgsel/ImageSelector;->isSelected(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0

    .line 149
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    return-void

    :cond_0
    const-string p1, "return_selector"

    .line 326
    invoke-virtual {p3, p1}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 332
    invoke-virtual {p0, p1}, Lepson/print/imgsel/ImageGridFragment;->updateImageSelector(Ljava/util/ArrayList;)V

    :cond_1
    const/16 p1, 0xa

    if-ne p2, p1, :cond_2

    .line 336
    invoke-virtual {p0}, Lepson/print/imgsel/ImageGridFragment;->goNext()V

    :cond_2
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .line 107
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 108
    invoke-virtual {p0}, Lepson/print/imgsel/ImageGridFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 109
    invoke-virtual {p0}, Lepson/print/imgsel/ImageGridFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "photo_folder_path"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/print/imgsel/ImageGridFragment;->mPhotoFolderPath:Ljava/lang/String;

    .line 113
    :cond_0
    invoke-virtual {p0}, Lepson/print/imgsel/ImageGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p1

    invoke-static {p1}, Lepson/print/imgsel/ImageGridFragment;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object p1

    const-string v0, "PHOTO_SELECT_MODE"

    const/4 v1, 0x0

    .line 114
    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result p1

    iput p1, p0, Lepson/print/imgsel/ImageGridFragment;->mPhotoSelectionMode:I

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    const p3, 0x7f0a006a

    const/4 v0, 0x0

    .line 122
    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    const p2, 0x7f080166

    .line 123
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/GridView;

    iput-object p2, p0, Lepson/print/imgsel/ImageGridFragment;->mGridView:Landroid/widget/GridView;

    .line 125
    new-instance p2, Lepson/print/imgsel/Alt2ViewImageAdapter;

    invoke-direct {p2, p0}, Lepson/print/imgsel/Alt2ViewImageAdapter;-><init>(Lepson/print/imgsel/ImageGridFragment;)V

    iput-object p2, p0, Lepson/print/imgsel/ImageGridFragment;->mAdapter:Lepson/print/imgsel/Alt2ViewImageAdapter;

    .line 126
    iget-object p2, p0, Lepson/print/imgsel/ImageGridFragment;->mGridView:Landroid/widget/GridView;

    iget-object p3, p0, Lepson/print/imgsel/ImageGridFragment;->mAdapter:Lepson/print/imgsel/Alt2ViewImageAdapter;

    invoke-virtual {p2, p3}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 127
    iget-object p2, p0, Lepson/print/imgsel/ImageGridFragment;->mGridView:Landroid/widget/GridView;

    invoke-virtual {p2, p0}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 130
    invoke-virtual {p0}, Lepson/print/imgsel/ImageGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p2

    invoke-virtual {p2}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const p3, 0x7f060065

    invoke-virtual {p2, p3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    .line 132
    new-instance p3, Lepson/print/imgsel/ImageGridFragment$MyGridLayoutListener;

    iget-object v0, p0, Lepson/print/imgsel/ImageGridFragment;->mGridView:Landroid/widget/GridView;

    iget-object v1, p0, Lepson/print/imgsel/ImageGridFragment;->mAdapter:Lepson/print/imgsel/Alt2ViewImageAdapter;

    invoke-direct {p3, v0, v1, p2}, Lepson/print/imgsel/ImageGridFragment$MyGridLayoutListener;-><init>(Landroid/widget/GridView;Lepson/print/imgsel/Alt2ViewImageAdapter;I)V

    .line 134
    iget-object p2, p0, Lepson/print/imgsel/ImageGridFragment;->mGridView:Landroid/widget/GridView;

    invoke-virtual {p2}, Landroid/widget/GridView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object p2

    invoke-virtual {p2, p3}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 136
    invoke-virtual {p0}, Lepson/print/imgsel/ImageGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p2

    check-cast p2, Lepson/print/imgsel/ImageGridFragment$ImageGridListener;

    invoke-interface {p2}, Lepson/print/imgsel/ImageGridFragment$ImageGridListener;->getImageSelector()Lepson/print/imgsel/ImageSelector;

    move-result-object p2

    iput-object p2, p0, Lepson/print/imgsel/ImageGridFragment;->mImageSelector:Lepson/print/imgsel/ImageSelector;

    .line 139
    invoke-virtual {p0}, Lepson/print/imgsel/ImageGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p2

    invoke-virtual {p2}, Landroid/support/v4/app/FragmentActivity;->invalidateOptionsMenu()V

    .line 142
    invoke-direct {p0}, Lepson/print/imgsel/ImageGridFragment;->restartImageFindTask()V

    return-object p1
.end method

.method public onDestroy()V
    .locals 0

    .line 203
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    return-void
.end method

.method public onDetach()V
    .locals 3

    .line 209
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    const-string v0, "ImageGridFragment"

    const-string v1, "onDetach()"

    .line 210
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    iget-object v0, p0, Lepson/print/imgsel/ImageGridFragment;->mImageFindTask:Lepson/print/imgsel/ImageFindTask;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    .line 214
    invoke-virtual {v0, v1}, Lepson/print/imgsel/ImageFindTask;->cancel(Z)Z

    .line 215
    iput-object v2, p0, Lepson/print/imgsel/ImageGridFragment;->mImageFindTask:Lepson/print/imgsel/ImageFindTask;

    .line 218
    :cond_0
    iget-object v0, p0, Lepson/print/imgsel/ImageGridFragment;->mImageThumbnailTask:Lepson/print/imgsel/ImageThumbnailTask;

    if-eqz v0, :cond_1

    .line 219
    invoke-virtual {v0, v1}, Lepson/print/imgsel/ImageThumbnailTask;->cancel(Z)Z

    .line 220
    iput-object v2, p0, Lepson/print/imgsel/ImageGridFragment;->mImageThumbnailTask:Lepson/print/imgsel/ImageThumbnailTask;

    .line 223
    :cond_1
    iget-object v0, p0, Lepson/print/imgsel/ImageGridFragment;->mAdapter:Lepson/print/imgsel/Alt2ViewImageAdapter;

    if-eqz v0, :cond_2

    .line 224
    invoke-virtual {v0}, Lepson/print/imgsel/Alt2ViewImageAdapter;->recycleBitmap()V

    .line 225
    iput-object v2, p0, Lepson/print/imgsel/ImageGridFragment;->mAdapter:Lepson/print/imgsel/Alt2ViewImageAdapter;

    .line 226
    iget-object v0, p0, Lepson/print/imgsel/ImageGridFragment;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 230
    :cond_2
    invoke-virtual {p0}, Lepson/print/imgsel/ImageGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 234
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->invalidateOptionsMenu()V

    :cond_3
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .line 154
    iget-object p1, p0, Lepson/print/imgsel/ImageGridFragment;->mAdapter:Lepson/print/imgsel/Alt2ViewImageAdapter;

    if-eqz p1, :cond_6

    iget-boolean p2, p0, Lepson/print/imgsel/ImageGridFragment;->mDoubleClickGuard:Z

    if-eqz p2, :cond_0

    goto :goto_3

    :cond_0
    const/4 p2, 0x1

    .line 157
    iput-boolean p2, p0, Lepson/print/imgsel/ImageGridFragment;->mDoubleClickGuard:Z

    .line 159
    invoke-virtual {p1, p3}, Lepson/print/imgsel/Alt2ViewImageAdapter;->getImageItem(I)Lepson/print/ImageItem;

    move-result-object p1

    const/4 p4, 0x0

    if-eqz p1, :cond_5

    .line 160
    iget-object p5, p0, Lepson/print/imgsel/ImageGridFragment;->mImageSelector:Lepson/print/imgsel/ImageSelector;

    if-nez p5, :cond_1

    goto :goto_2

    .line 166
    :cond_1
    invoke-virtual {p0}, Lepson/print/imgsel/ImageGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p5

    check-cast p5, Lepson/print/imgsel/ImageSelectActivity;

    .line 167
    iget v0, p0, Lepson/print/imgsel/ImageGridFragment;->mPhotoSelectionMode:I

    if-ne v0, p2, :cond_4

    .line 168
    invoke-virtual {p5}, Lepson/print/imgsel/ImageSelectActivity;->singleImageMode()Z

    move-result p5

    if-nez p5, :cond_4

    .line 170
    invoke-virtual {p1}, Lepson/print/ImageItem;->getSelected()I

    move-result p3

    if-nez p3, :cond_2

    .line 172
    iget-object p3, p0, Lepson/print/imgsel/ImageGridFragment;->mImageSelector:Lepson/print/imgsel/ImageSelector;

    invoke-virtual {p3}, Lepson/print/imgsel/ImageSelector;->canAdd()Z

    move-result p3

    if-nez p3, :cond_2

    const p1, 0x7f0e03a7

    .line 175
    invoke-virtual {p0, p1}, Lepson/print/imgsel/ImageGridFragment;->getString(I)Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x3

    invoke-static {p1, p2}, Lepson/print/imgsel/LocalAlertDialogFragment;->newInstance(Ljava/lang/String;I)Lepson/print/imgsel/LocalAlertDialogFragment;

    move-result-object p1

    .line 178
    :try_start_0
    invoke-virtual {p0}, Lepson/print/imgsel/ImageGridFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object p2

    const-string p3, "alert dialog"

    invoke-virtual {p1, p2, p3}, Lepson/print/imgsel/LocalAlertDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 182
    :catch_0
    iput-boolean p4, p0, Lepson/print/imgsel/ImageGridFragment;->mDoubleClickGuard:Z

    return-void

    .line 188
    :cond_2
    iget-object p3, p0, Lepson/print/imgsel/ImageGridFragment;->mImageSelector:Lepson/print/imgsel/ImageSelector;

    invoke-virtual {p1}, Lepson/print/ImageItem;->getPath()Ljava/lang/String;

    move-result-object p5

    invoke-virtual {p3, p5}, Lepson/print/imgsel/ImageSelector;->toggleSelect(Ljava/lang/String;)Z

    .line 189
    invoke-virtual {p1}, Lepson/print/ImageItem;->getSelected()I

    move-result p3

    if-nez p3, :cond_3

    goto :goto_0

    :cond_3
    const/4 p2, 0x0

    :goto_0
    invoke-virtual {p1, p2}, Lepson/print/ImageItem;->setSelected(I)V

    .line 190
    iget-object p1, p0, Lepson/print/imgsel/ImageGridFragment;->mAdapter:Lepson/print/imgsel/Alt2ViewImageAdapter;

    invoke-virtual {p1}, Lepson/print/imgsel/Alt2ViewImageAdapter;->notifyDataSetChanged()V

    .line 191
    invoke-virtual {p0}, Lepson/print/imgsel/ImageGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p1

    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->invalidateOptionsMenu()V

    goto :goto_1

    .line 195
    :cond_4
    invoke-direct {p0, p3}, Lepson/print/imgsel/ImageGridFragment;->startViewPagerActivity(I)V

    .line 198
    :goto_1
    iput-boolean p4, p0, Lepson/print/imgsel/ImageGridFragment;->mDoubleClickGuard:Z

    return-void

    .line 162
    :cond_5
    :goto_2
    iput-boolean p4, p0, Lepson/print/imgsel/ImageGridFragment;->mDoubleClickGuard:Z

    return-void

    :cond_6
    :goto_3
    return-void
.end method

.method public onPause()V
    .locals 2

    .line 357
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 359
    iget-object v0, p0, Lepson/print/imgsel/ImageGridFragment;->mImageThumbnailTask:Lepson/print/imgsel/ImageThumbnailTask;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    .line 360
    invoke-virtual {v0, v1}, Lepson/print/imgsel/ImageThumbnailTask;->setSuspend(Z)V

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 2

    .line 346
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    const/4 v0, 0x0

    .line 348
    iput-boolean v0, p0, Lepson/print/imgsel/ImageGridFragment;->mDoubleClickGuard:Z

    .line 350
    iget-object v1, p0, Lepson/print/imgsel/ImageGridFragment;->mImageThumbnailTask:Lepson/print/imgsel/ImageThumbnailTask;

    if-eqz v1, :cond_0

    .line 351
    invoke-virtual {v1, v0}, Lepson/print/imgsel/ImageThumbnailTask;->setSuspend(Z)V

    :cond_0
    return-void
.end method

.method public setPhotoSelectionMode(I)V
    .locals 2

    .line 96
    iput p1, p0, Lepson/print/imgsel/ImageGridFragment;->mPhotoSelectionMode:I

    .line 99
    invoke-virtual {p0}, Lepson/print/imgsel/ImageGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p1

    invoke-static {p1}, Lepson/print/imgsel/ImageGridFragment;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object p1

    .line 100
    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    const-string v0, "PHOTO_SELECT_MODE"

    .line 101
    iget v1, p0, Lepson/print/imgsel/ImageGridFragment;->mPhotoSelectionMode:I

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 102
    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public updateData()V
    .locals 2

    .line 451
    iget-object v0, p0, Lepson/print/imgsel/ImageGridFragment;->mAdapter:Lepson/print/imgsel/Alt2ViewImageAdapter;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lepson/print/imgsel/ImageGridFragment;->mGridView:Landroid/widget/GridView;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lepson/print/imgsel/ImageGridFragment;->mImageThumbnailTask:Lepson/print/imgsel/ImageThumbnailTask;

    if-nez v1, :cond_0

    goto :goto_0

    .line 455
    :cond_0
    invoke-virtual {v0}, Lepson/print/imgsel/Alt2ViewImageAdapter;->notifyDataSetChanged()V

    .line 458
    iget-object v0, p0, Lepson/print/imgsel/ImageGridFragment;->mImageThumbnailTask:Lepson/print/imgsel/ImageThumbnailTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lepson/print/imgsel/ImageThumbnailTask;->setFinish(Z)V

    return-void

    :cond_1
    :goto_0
    return-void
.end method

.method public updateImageSelector(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    if-nez p1, :cond_0

    return-void

    .line 298
    :cond_0
    iget-object v0, p0, Lepson/print/imgsel/ImageGridFragment;->mImageSelector:Lepson/print/imgsel/ImageSelector;

    invoke-virtual {v0, p1}, Lepson/print/imgsel/ImageSelector;->replaceFiles(Ljava/util/ArrayList;)V

    .line 301
    iget-object p1, p0, Lepson/print/imgsel/ImageGridFragment;->mAdapter:Lepson/print/imgsel/Alt2ViewImageAdapter;

    if-eqz p1, :cond_2

    .line 302
    invoke-virtual {p1}, Lepson/print/imgsel/Alt2ViewImageAdapter;->getCount()I

    move-result p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_1

    .line 304
    iget-object v1, p0, Lepson/print/imgsel/ImageGridFragment;->mAdapter:Lepson/print/imgsel/Alt2ViewImageAdapter;

    invoke-virtual {v1, v0}, Lepson/print/imgsel/Alt2ViewImageAdapter;->getImageItem(I)Lepson/print/ImageItem;

    move-result-object v1

    .line 305
    iget-object v2, p0, Lepson/print/imgsel/ImageGridFragment;->mImageSelector:Lepson/print/imgsel/ImageSelector;

    invoke-virtual {v1}, Lepson/print/ImageItem;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lepson/print/imgsel/ImageSelector;->isSelected(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v1, v2}, Lepson/print/ImageItem;->setSelected(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 307
    :cond_1
    iget-object p1, p0, Lepson/print/imgsel/ImageGridFragment;->mAdapter:Lepson/print/imgsel/Alt2ViewImageAdapter;

    invoke-virtual {p1}, Lepson/print/imgsel/Alt2ViewImageAdapter;->notifyDataSetChanged()V

    .line 311
    :cond_2
    invoke-virtual {p0}, Lepson/print/imgsel/ImageGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p1

    if-nez p1, :cond_3

    return-void

    .line 315
    :cond_3
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->invalidateOptionsMenu()V

    return-void
.end method

.method public updateItem(Lepson/print/ImageItem;)V
    .locals 2

    if-eqz p1, :cond_2

    .line 428
    iget-object v0, p0, Lepson/print/imgsel/ImageGridFragment;->mAdapter:Lepson/print/imgsel/Alt2ViewImageAdapter;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lepson/print/imgsel/ImageGridFragment;->mGridView:Landroid/widget/GridView;

    if-nez v1, :cond_0

    goto :goto_0

    .line 432
    :cond_0
    invoke-virtual {v0, p1}, Lepson/print/imgsel/Alt2ViewImageAdapter;->getPosition(Lepson/print/ImageItem;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 435
    iget-object v1, p0, Lepson/print/imgsel/ImageGridFragment;->mAdapter:Lepson/print/imgsel/Alt2ViewImageAdapter;

    invoke-virtual {v1, v0}, Lepson/print/imgsel/Alt2ViewImageAdapter;->getImageItem(I)Lepson/print/ImageItem;

    move-result-object v1

    invoke-virtual {p1}, Lepson/print/ImageItem;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object p1

    invoke-virtual {v1, p1}, Lepson/print/ImageItem;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 438
    iget-object p1, p0, Lepson/print/imgsel/ImageGridFragment;->mGridView:Landroid/widget/GridView;

    invoke-virtual {p1}, Landroid/widget/GridView;->getFirstVisiblePosition()I

    move-result p1

    .line 439
    iget-object v1, p0, Lepson/print/imgsel/ImageGridFragment;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v1}, Landroid/widget/GridView;->getLastVisiblePosition()I

    move-result v1

    if-gt p1, v0, :cond_1

    if-lt v1, v0, :cond_1

    .line 441
    iget-object p1, p0, Lepson/print/imgsel/ImageGridFragment;->mAdapter:Lepson/print/imgsel/Alt2ViewImageAdapter;

    invoke-virtual {p1}, Lepson/print/imgsel/Alt2ViewImageAdapter;->notifyDataSetChanged()V

    :cond_1
    return-void

    :cond_2
    :goto_0
    return-void
.end method

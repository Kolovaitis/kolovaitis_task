.class Lepson/print/imgsel/ImageFindTask;
.super Landroid/os/AsyncTask;
.source "ImageFindTask.java"

# interfaces
.implements Lepson/print/imgsel/ImageFinder$Canceller;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Lepson/print/imgsel/ImageFinder;",
        "Ljava/util/List<",
        "Lepson/print/ImageItem;",
        ">;",
        "Ljava/lang/Void;",
        ">;",
        "Lepson/print/imgsel/ImageFinder$Canceller;"
    }
.end annotation


# static fields
.field private static IMAGE_QUERY_LIMIT:I = 0x20


# instance fields
.field mFolderPath:Ljava/lang/String;

.field mFragmentReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lepson/print/imgsel/ImageGridFragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Lepson/print/imgsel/ImageGridFragment;Ljava/lang/String;)V
    .locals 1

    .line 30
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 31
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lepson/print/imgsel/ImageFindTask;->mFragmentReference:Ljava/lang/ref/WeakReference;

    .line 32
    iput-object p2, p0, Lepson/print/imgsel/ImageFindTask;->mFolderPath:Ljava/lang/String;

    return-void
.end method

.method private getResolver()Landroid/content/ContentResolver;
    .locals 2

    .line 101
    iget-object v0, p0, Lepson/print/imgsel/ImageFindTask;->mFragmentReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/print/imgsel/ImageGridFragment;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 105
    :cond_0
    invoke-virtual {v0}, Lepson/print/imgsel/ImageGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_1

    return-object v1

    .line 109
    :cond_1
    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    return-object v0
.end method

.method private isImageSelected(Ljava/lang/String;)Z
    .locals 1

    .line 114
    iget-object v0, p0, Lepson/print/imgsel/ImageFindTask;->mFragmentReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/print/imgsel/ImageGridFragment;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 119
    :cond_0
    invoke-virtual {v0, p1}, Lepson/print/imgsel/ImageGridFragment;->isImageSelected(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method


# virtual methods
.method public checkCanceled()Z
    .locals 1

    .line 126
    invoke-virtual {p0}, Lepson/print/imgsel/ImageFindTask;->isCancelled()Z

    move-result v0

    return v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, [Lepson/print/imgsel/ImageFinder;

    invoke-virtual {p0, p1}, Lepson/print/imgsel/ImageFindTask;->doInBackground([Lepson/print/imgsel/ImageFinder;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Lepson/print/imgsel/ImageFinder;)Ljava/lang/Void;
    .locals 12

    const/4 v0, 0x0

    if-eqz p1, :cond_a

    .line 37
    array-length v1, p1

    if-gtz v1, :cond_0

    goto/16 :goto_2

    :cond_0
    const/4 v1, 0x0

    .line 40
    aget-object p1, p1, v1

    .line 41
    invoke-virtual {p0}, Lepson/print/imgsel/ImageFindTask;->isCancelled()Z

    move-result v2

    if-nez v2, :cond_9

    if-nez p1, :cond_1

    goto/16 :goto_1

    .line 45
    :cond_1
    iget-object v2, p0, Lepson/print/imgsel/ImageFindTask;->mFragmentReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lepson/print/imgsel/ImageGridFragment;

    if-nez v2, :cond_2

    return-object v0

    .line 49
    :cond_2
    invoke-virtual {v2}, Lepson/print/imgsel/ImageGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    if-nez v2, :cond_3

    return-object v0

    .line 54
    :cond_3
    invoke-virtual {p0}, Lepson/print/imgsel/ImageFindTask;->isCancelled()Z

    move-result v2

    if-eqz v2, :cond_4

    return-object v0

    .line 59
    :cond_4
    iget-object v2, p0, Lepson/print/imgsel/ImageFindTask;->mFolderPath:Ljava/lang/String;

    .line 60
    invoke-direct {p0}, Lepson/print/imgsel/ImageFindTask;->getResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 59
    invoke-interface {p1, v2, v3, p0}, Lepson/print/imgsel/ImageFinder;->findImageInDirectory(Ljava/lang/String;Landroid/content/ContentResolver;Lepson/print/imgsel/ImageFinder$Canceller;)Ljava/util/List;

    move-result-object p1

    if-eqz p1, :cond_8

    .line 63
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 65
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_5
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const/4 v4, 0x1

    if-eqz v3, :cond_7

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lepson/print/imgsel/ImageFileInfo;

    .line 66
    invoke-virtual {p0}, Lepson/print/imgsel/ImageFindTask;->isCancelled()Z

    move-result v5

    if-eqz v5, :cond_6

    return-object v0

    .line 72
    :cond_6
    iget-object v5, v3, Lepson/print/imgsel/ImageFileInfo;->mCanonicalPath:Ljava/lang/String;

    invoke-direct {p0, v5}, Lepson/print/imgsel/ImageFindTask;->isImageSelected(Ljava/lang/String;)Z

    move-result v8

    .line 77
    new-instance v5, Lepson/print/ImageItem;

    const/4 v7, 0x0

    iget-object v9, v3, Lepson/print/imgsel/ImageFileInfo;->mCanonicalPath:Ljava/lang/String;

    iget-wide v10, v3, Lepson/print/imgsel/ImageFileInfo;->mMediaInfoId:J

    move-object v6, v5

    invoke-direct/range {v6 .. v11}, Lepson/print/ImageItem;-><init>(Landroid/graphics/Bitmap;ILjava/lang/String;J)V

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    sget v5, Lepson/print/imgsel/ImageFindTask;->IMAGE_QUERY_LIMIT:I

    rem-int/2addr v3, v5

    if-nez v3, :cond_5

    .line 82
    new-array v3, v4, [Ljava/util/List;

    aput-object v2, v3, v1

    invoke-virtual {p0, v3}, Lepson/print/imgsel/ImageFindTask;->publishProgress([Ljava/lang/Object;)V

    .line 83
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0

    .line 88
    :cond_7
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result p1

    if-lez p1, :cond_8

    .line 89
    new-array p1, v4, [Ljava/util/List;

    aput-object v2, p1, v1

    invoke-virtual {p0, p1}, Lepson/print/imgsel/ImageFindTask;->publishProgress([Ljava/lang/Object;)V

    :cond_8
    return-object v0

    :cond_9
    :goto_1
    return-object v0

    :cond_a
    :goto_2
    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 21
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/print/imgsel/ImageFindTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 0

    .line 152
    iget-object p1, p0, Lepson/print/imgsel/ImageFindTask;->mFragmentReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lepson/print/imgsel/ImageGridFragment;

    if-nez p1, :cond_0

    return-void

    .line 157
    :cond_0
    invoke-virtual {p1}, Lepson/print/imgsel/ImageGridFragment;->updateData()V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .line 132
    iget-object v0, p0, Lepson/print/imgsel/ImageFindTask;->mFragmentReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/print/imgsel/ImageGridFragment;

    if-nez v0, :cond_0

    return-void

    .line 136
    :cond_0
    invoke-virtual {v0}, Lepson/print/imgsel/ImageGridFragment;->clearItem()V

    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .line 21
    check-cast p1, [Ljava/util/List;

    invoke-virtual {p0, p1}, Lepson/print/imgsel/ImageFindTask;->onProgressUpdate([Ljava/util/List;)V

    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/util/List<",
            "Lepson/print/ImageItem;",
            ">;)V"
        }
    .end annotation

    .line 141
    iget-object v0, p0, Lepson/print/imgsel/ImageFindTask;->mFragmentReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/print/imgsel/ImageGridFragment;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    .line 142
    array-length v1, p1

    if-gtz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 147
    aget-object p1, p1, v1

    invoke-virtual {v0, p1}, Lepson/print/imgsel/ImageGridFragment;->addItem(Ljava/util/List;)V

    return-void

    :cond_1
    :goto_0
    return-void
.end method

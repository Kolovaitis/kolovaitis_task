.class public Lepson/print/imgsel/ImageFolderListFragment;
.super Landroid/support/v4/app/ListFragment;
.source "ImageFolderListFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/imgsel/ImageFolderListFragment$FolderSelectedListener;
    }
.end annotation


# static fields
.field private static final FRAGMENT_TAG_NO_IMAGE_DIALOG:Ljava/lang/String; = "no_image_dialog"

.field private static final RESULT_RUNTIMEPERMMISSION:I = 0x1


# instance fields
.field private mAdapter:Lepson/print/imgsel/ImageFolderAdapter;

.field private mFolderFindTask:Lepson/print/imgsel/FolderFindTask;

.field private mIsFragmentForground:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Landroid/support/v4/app/ListFragment;-><init>()V

    return-void
.end method

.method private dismissNoImageDialog()V
    .locals 2

    .line 148
    invoke-virtual {p0}, Lepson/print/imgsel/ImageFolderListFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    :try_start_0
    const-string v1, "no_image_dialog"

    .line 150
    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lepson/print/imgsel/LocalAlertDialogFragment;

    if-eqz v0, :cond_0

    .line 153
    invoke-virtual {v0}, Lepson/print/imgsel/LocalAlertDialogFragment;->dismiss()V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method

.method private startFolderFindTask()V
    .locals 4

    .line 177
    invoke-virtual {p0}, Lepson/print/imgsel/ImageFolderListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lepson/print/imgsel/ImageFolderListFragment$FolderSelectedListener;

    if-nez v0, :cond_0

    return-void

    .line 182
    :cond_0
    iget-object v1, p0, Lepson/print/imgsel/ImageFolderListFragment;->mFolderFindTask:Lepson/print/imgsel/FolderFindTask;

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    .line 183
    invoke-virtual {v1, v2}, Lepson/print/imgsel/FolderFindTask;->cancel(Z)Z

    .line 184
    iget-object v1, p0, Lepson/print/imgsel/ImageFolderListFragment;->mAdapter:Lepson/print/imgsel/ImageFolderAdapter;

    invoke-virtual {v1}, Lepson/print/imgsel/ImageFolderAdapter;->releaseResource()V

    .line 185
    iget-object v1, p0, Lepson/print/imgsel/ImageFolderListFragment;->mAdapter:Lepson/print/imgsel/ImageFolderAdapter;

    invoke-virtual {v1}, Lepson/print/imgsel/ImageFolderAdapter;->notifyDataSetChanged()V

    .line 188
    :cond_1
    new-instance v1, Lepson/print/imgsel/FolderFindTask;

    invoke-direct {v1, p0}, Lepson/print/imgsel/FolderFindTask;-><init>(Lepson/print/imgsel/ImageFolderListFragment;)V

    iput-object v1, p0, Lepson/print/imgsel/ImageFolderListFragment;->mFolderFindTask:Lepson/print/imgsel/FolderFindTask;

    .line 189
    iget-object v1, p0, Lepson/print/imgsel/ImageFolderListFragment;->mFolderFindTask:Lepson/print/imgsel/FolderFindTask;

    new-array v2, v2, [Lepson/print/imgsel/ImageFinder;

    const/4 v3, 0x0

    invoke-interface {v0}, Lepson/print/imgsel/ImageFolderListFragment$FolderSelectedListener;->getImageFinder()Lepson/print/imgsel/ImageFinder;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lepson/print/imgsel/FolderFindTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method


# virtual methods
.method public addItem(Lepson/print/imgsel/ImageFolderInfo;Landroid/graphics/Bitmap;)V
    .locals 1

    .line 62
    iget-object v0, p0, Lepson/print/imgsel/ImageFolderListFragment;->mAdapter:Lepson/print/imgsel/ImageFolderAdapter;

    if-nez v0, :cond_0

    return-void

    .line 66
    :cond_0
    invoke-virtual {v0, p1, p2}, Lepson/print/imgsel/ImageFolderAdapter;->addItem(Lepson/print/imgsel/ImageFolderInfo;Landroid/graphics/Bitmap;)V

    .line 67
    iget-object p1, p0, Lepson/print/imgsel/ImageFolderListFragment;->mAdapter:Lepson/print/imgsel/ImageFolderAdapter;

    invoke-virtual {p1}, Lepson/print/imgsel/ImageFolderAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public getItemSize()I
    .locals 1

    .line 71
    iget-object v0, p0, Lepson/print/imgsel/ImageFolderListFragment;->mAdapter:Lepson/print/imgsel/ImageFolderAdapter;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 74
    :cond_0
    invoke-virtual {v0}, Lepson/print/imgsel/ImageFolderAdapter;->getCount()I

    move-result v0

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1

    .line 50
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 51
    new-instance p1, Lepson/print/imgsel/ImageFolderAdapter;

    invoke-virtual {p0}, Lepson/print/imgsel/ImageFolderListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {p1, v0}, Lepson/print/imgsel/ImageFolderAdapter;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lepson/print/imgsel/ImageFolderListFragment;->mAdapter:Lepson/print/imgsel/ImageFolderAdapter;

    .line 52
    iget-object p1, p0, Lepson/print/imgsel/ImageFolderListFragment;->mAdapter:Lepson/print/imgsel/ImageFolderAdapter;

    invoke-virtual {p0, p1}, Lepson/print/imgsel/ImageFolderListFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    .line 127
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/ListFragment;->onActivityResult(IILandroid/content/Intent;)V

    const/4 p3, 0x1

    if-eq p1, p3, :cond_0

    goto :goto_0

    :cond_0
    packed-switch p2, :pswitch_data_0

    goto :goto_0

    .line 137
    :pswitch_0
    invoke-virtual {p0}, Lepson/print/imgsel/ImageFolderListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p1

    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->onBackPressed()V

    :goto_0
    :pswitch_1
    return-void

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 0

    .line 162
    invoke-super {p0}, Landroid/support/v4/app/ListFragment;->onDestroy()V

    return-void
.end method

.method public onDetach()V
    .locals 1

    .line 167
    invoke-super {p0}, Landroid/support/v4/app/ListFragment;->onDetach()V

    .line 169
    iget-object v0, p0, Lepson/print/imgsel/ImageFolderListFragment;->mAdapter:Lepson/print/imgsel/ImageFolderAdapter;

    if-eqz v0, :cond_0

    .line 170
    invoke-virtual {v0}, Lepson/print/imgsel/ImageFolderAdapter;->releaseResource()V

    const/4 v0, 0x0

    .line 171
    iput-object v0, p0, Lepson/print/imgsel/ImageFolderListFragment;->mAdapter:Lepson/print/imgsel/ImageFolderAdapter;

    .line 172
    invoke-virtual {p0, v0}, Lepson/print/imgsel/ImageFolderListFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    :cond_0
    return-void
.end method

.method public onFolderFindTaskEnd()V
    .locals 3

    .line 196
    invoke-virtual {p0}, Lepson/print/imgsel/ImageFolderListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 200
    :cond_0
    iget-boolean v1, p0, Lepson/print/imgsel/ImageFolderListFragment;->mIsFragmentForground:Z

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lepson/print/imgsel/ImageFolderListFragment;->getItemSize()I

    move-result v1

    if-gtz v1, :cond_1

    .line 202
    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e04e9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0e053c

    const/4 v2, 0x1

    .line 201
    invoke-static {v0, v1, v2}, Lepson/print/imgsel/LocalAlertDialogFragment;->newInstance(Ljava/lang/String;II)Lepson/print/imgsel/LocalAlertDialogFragment;

    move-result-object v0

    .line 205
    invoke-virtual {p0}, Lepson/print/imgsel/ImageFolderListFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "no_image_dialog"

    invoke-virtual {v0, v1, v2}, Lepson/print/imgsel/LocalAlertDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 0

    .line 40
    iget-object p1, p0, Lepson/print/imgsel/ImageFolderListFragment;->mAdapter:Lepson/print/imgsel/ImageFolderAdapter;

    if-nez p1, :cond_0

    return-void

    .line 44
    :cond_0
    invoke-virtual {p0}, Lepson/print/imgsel/ImageFolderListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p1

    check-cast p1, Lepson/print/imgsel/ImageFolderListFragment$FolderSelectedListener;

    iget-object p2, p0, Lepson/print/imgsel/ImageFolderListFragment;->mAdapter:Lepson/print/imgsel/ImageFolderAdapter;

    invoke-virtual {p2, p3}, Lepson/print/imgsel/ImageFolderAdapter;->getFolderInfo(I)Lepson/print/imgsel/ImageFolderInfo;

    move-result-object p2

    invoke-interface {p1, p2}, Lepson/print/imgsel/ImageFolderListFragment$FolderSelectedListener;->onFolderSelected(Lepson/print/imgsel/ImageFolderInfo;)V

    return-void
.end method

.method public onPause()V
    .locals 2

    .line 79
    invoke-super {p0}, Landroid/support/v4/app/ListFragment;->onPause()V

    const/4 v0, 0x0

    .line 80
    iput-boolean v0, p0, Lepson/print/imgsel/ImageFolderListFragment;->mIsFragmentForground:Z

    .line 82
    iget-object v0, p0, Lepson/print/imgsel/ImageFolderListFragment;->mFolderFindTask:Lepson/print/imgsel/FolderFindTask;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    .line 84
    invoke-virtual {v0, v1}, Lepson/print/imgsel/FolderFindTask;->cancel(Z)Z

    const/4 v0, 0x0

    .line 85
    iput-object v0, p0, Lepson/print/imgsel/ImageFolderListFragment;->mFolderFindTask:Lepson/print/imgsel/FolderFindTask;

    .line 87
    iget-object v0, p0, Lepson/print/imgsel/ImageFolderListFragment;->mAdapter:Lepson/print/imgsel/ImageFolderAdapter;

    invoke-virtual {v0}, Lepson/print/imgsel/ImageFolderAdapter;->releaseResource()V

    .line 88
    iget-object v0, p0, Lepson/print/imgsel/ImageFolderListFragment;->mAdapter:Lepson/print/imgsel/ImageFolderAdapter;

    invoke-virtual {v0}, Lepson/print/imgsel/ImageFolderAdapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 7

    .line 94
    invoke-super {p0}, Landroid/support/v4/app/ListFragment;->onResume()V

    .line 98
    invoke-direct {p0}, Lepson/print/imgsel/ImageFolderListFragment;->dismissNoImageDialog()V

    const/4 v0, 0x1

    .line 99
    iput-boolean v0, p0, Lepson/print/imgsel/ImageFolderListFragment;->mIsFragmentForground:Z

    .line 102
    invoke-static {}, Lepson/print/ActivityRequestPermissions;->isRuntimePermissionSupported()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "android.permission.WRITE_EXTERNAL_STORAGE"

    .line 103
    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    .line 104
    new-array v3, v2, [Ljava/lang/String;

    const v4, 0x7f0e0422

    invoke-virtual {p0, v4}, Lepson/print/imgsel/ImageFolderListFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    aput-object v5, v3, v6

    invoke-virtual {p0, v4}, Lepson/print/imgsel/ImageFolderListFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    .line 105
    new-array v2, v2, [Ljava/lang/String;

    const v4, 0x7f0e0420

    .line 106
    invoke-virtual {p0, v4}, Lepson/print/imgsel/ImageFolderListFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v5}, Lepson/print/ActivityRequestPermissions$DialogParameter;->setMessage2(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v6

    .line 107
    invoke-virtual {p0, v4}, Lepson/print/imgsel/ImageFolderListFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f0e0424

    invoke-virtual {p0, v5}, Lepson/print/imgsel/ImageFolderListFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v4, v5}, Lepson/print/ActivityRequestPermissions$DialogParameter;->setMessage3A(Landroid/support/v4/app/Fragment;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v0

    .line 113
    new-instance v4, Lepson/print/ActivityRequestPermissions$Permission;

    aget-object v5, v1, v6

    invoke-direct {v4, v5, v3, v2}, Lepson/print/ActivityRequestPermissions$Permission;-><init>(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V

    .line 115
    invoke-virtual {p0}, Lepson/print/imgsel/ImageFolderListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2, v1}, Lepson/print/ActivityRequestPermissions;->checkPermission(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 116
    invoke-static {p0, v4, v0}, Lepson/print/ActivityRequestPermissions;->requestPermission(Landroid/support/v4/app/Fragment;Lepson/print/ActivityRequestPermissions$Permission;I)V

    return-void

    .line 122
    :cond_0
    invoke-direct {p0}, Lepson/print/imgsel/ImageFolderListFragment;->startFolderFindTask()V

    return-void
.end method

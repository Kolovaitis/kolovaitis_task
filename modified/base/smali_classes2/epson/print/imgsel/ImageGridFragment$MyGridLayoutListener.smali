.class Lepson/print/imgsel/ImageGridFragment$MyGridLayoutListener;
.super Ljava/lang/Object;
.source "ImageGridFragment.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/imgsel/ImageGridFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MyGridLayoutListener"
.end annotation


# instance fields
.field mAdapter:Lepson/print/imgsel/Alt2ViewImageAdapter;

.field private mGridView:Landroid/widget/GridView;

.field private mHorizontalSpacing:I


# direct methods
.method public constructor <init>(Landroid/widget/GridView;Lepson/print/imgsel/Alt2ViewImageAdapter;I)V
    .locals 0

    .line 504
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 505
    iput-object p1, p0, Lepson/print/imgsel/ImageGridFragment$MyGridLayoutListener;->mGridView:Landroid/widget/GridView;

    .line 506
    iput-object p2, p0, Lepson/print/imgsel/ImageGridFragment$MyGridLayoutListener;->mAdapter:Lepson/print/imgsel/Alt2ViewImageAdapter;

    .line 507
    iput p3, p0, Lepson/print/imgsel/ImageGridFragment$MyGridLayoutListener;->mHorizontalSpacing:I

    return-void
.end method

.method private setNewVerticalParams()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .line 543
    iget-object v0, p0, Lepson/print/imgsel/ImageGridFragment$MyGridLayoutListener;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->getHorizontalSpacing()I

    move-result v0

    .line 544
    iget-object v1, p0, Lepson/print/imgsel/ImageGridFragment$MyGridLayoutListener;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v1, v0}, Landroid/widget/GridView;->setVerticalSpacing(I)V

    .line 545
    iget-object v0, p0, Lepson/print/imgsel/ImageGridFragment$MyGridLayoutListener;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->getColumnWidth()I

    move-result v0

    .line 547
    iget-object v1, p0, Lepson/print/imgsel/ImageGridFragment$MyGridLayoutListener;->mAdapter:Lepson/print/imgsel/Alt2ViewImageAdapter;

    invoke-virtual {v1, v0}, Lepson/print/imgsel/Alt2ViewImageAdapter;->setHeight(I)V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .line 519
    iget-object v0, p0, Lepson/print/imgsel/ImageGridFragment$MyGridLayoutListener;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->getNumColumns()I

    move-result v0

    if-lez v0, :cond_1

    .line 523
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v1, v2, :cond_0

    .line 524
    invoke-direct {p0}, Lepson/print/imgsel/ImageGridFragment$MyGridLayoutListener;->setNewVerticalParams()V

    .line 527
    iget-object v0, p0, Lepson/print/imgsel/ImageGridFragment$MyGridLayoutListener;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0

    .line 529
    :cond_0
    iget-object v1, p0, Lepson/print/imgsel/ImageGridFragment$MyGridLayoutListener;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v1}, Landroid/widget/GridView;->getWidth()I

    move-result v1

    iget v2, p0, Lepson/print/imgsel/ImageGridFragment$MyGridLayoutListener;->mHorizontalSpacing:I

    add-int/2addr v1, v2

    .line 531
    div-int/2addr v1, v0

    .line 532
    iget-object v0, p0, Lepson/print/imgsel/ImageGridFragment$MyGridLayoutListener;->mAdapter:Lepson/print/imgsel/Alt2ViewImageAdapter;

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lepson/print/imgsel/Alt2ViewImageAdapter;->setHeight(I)V

    .line 535
    iget-object v0, p0, Lepson/print/imgsel/ImageGridFragment$MyGridLayoutListener;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    :cond_1
    :goto_0
    return-void
.end method

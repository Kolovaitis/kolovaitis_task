.class Lepson/print/imgsel/ImageViewPagerActivity$PagerAdapter;
.super Landroid/support/v4/app/FragmentStatePagerAdapter;
.source "ImageViewPagerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/imgsel/ImageViewPagerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PagerAdapter"
.end annotation


# instance fields
.field private mFileList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lepson/print/imgsel/ImageViewPagerActivity;


# direct methods
.method public constructor <init>(Lepson/print/imgsel/ImageViewPagerActivity;Landroid/support/v4/app/FragmentManager;Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/app/FragmentManager;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 338
    iput-object p1, p0, Lepson/print/imgsel/ImageViewPagerActivity$PagerAdapter;->this$0:Lepson/print/imgsel/ImageViewPagerActivity;

    .line 339
    invoke-direct {p0, p2}, Landroid/support/v4/app/FragmentStatePagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 340
    iput-object p3, p0, Lepson/print/imgsel/ImageViewPagerActivity$PagerAdapter;->mFileList:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .line 351
    iget-object v0, p0, Lepson/print/imgsel/ImageViewPagerActivity$PagerAdapter;->mFileList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Landroid/support/v4/app/Fragment;
    .locals 2

    .line 345
    iget-object v0, p0, Lepson/print/imgsel/ImageViewPagerActivity$PagerAdapter;->mFileList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iget-object v0, p0, Lepson/print/imgsel/ImageViewPagerActivity$PagerAdapter;->this$0:Lepson/print/imgsel/ImageViewPagerActivity;

    .line 346
    invoke-static {v0}, Lepson/print/imgsel/ImageViewPagerActivity;->access$000(Lepson/print/imgsel/ImageViewPagerActivity;)Z

    move-result v0

    iget-object v1, p0, Lepson/print/imgsel/ImageViewPagerActivity$PagerAdapter;->this$0:Lepson/print/imgsel/ImageViewPagerActivity;

    invoke-static {v1}, Lepson/print/imgsel/ImageViewPagerActivity;->access$100(Lepson/print/imgsel/ImageViewPagerActivity;)I

    move-result v1

    .line 345
    invoke-static {p1, v0, v1}, Lepson/print/imgsel/ImageViewBaseFragment;->newInstance(Ljava/lang/String;ZI)Lepson/print/imgsel/ImageViewBaseFragment;

    move-result-object p1

    return-object p1
.end method

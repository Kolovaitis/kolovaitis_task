.class Lepson/print/imgsel/ImageFolderInfo$DatabaseOrderComparator;
.super Ljava/lang/Object;
.source "ImageFolderInfo.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/imgsel/ImageFolderInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "DatabaseOrderComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "Lepson/print/imgsel/ImageFolderInfo;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lepson/print/imgsel/ImageFolderInfo;Lepson/print/imgsel/ImageFolderInfo;)I
    .locals 0

    .line 100
    invoke-virtual {p1}, Lepson/print/imgsel/ImageFolderInfo;->getMiniDatabaseOrder()I

    move-result p1

    .line 101
    invoke-virtual {p2}, Lepson/print/imgsel/ImageFolderInfo;->getMiniDatabaseOrder()I

    move-result p2

    if-le p1, p2, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    if-ne p1, p2, :cond_1

    const/4 p1, 0x0

    return p1

    :cond_1
    const/4 p1, -0x1

    return p1
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    .line 97
    check-cast p1, Lepson/print/imgsel/ImageFolderInfo;

    check-cast p2, Lepson/print/imgsel/ImageFolderInfo;

    invoke-virtual {p0, p1, p2}, Lepson/print/imgsel/ImageFolderInfo$DatabaseOrderComparator;->compare(Lepson/print/imgsel/ImageFolderInfo;Lepson/print/imgsel/ImageFolderInfo;)I

    move-result p1

    return p1
.end method

.class public Lepson/print/imgsel/ImageViewMultiSelectFragment;
.super Lepson/print/imgsel/ImageViewBaseFragment;
.source "ImageViewMultiSelectFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/imgsel/ImageViewMultiSelectFragment$MyToggleButton;
    }
.end annotation


# instance fields
.field protected mMyToggleButton:Lepson/print/imgsel/ImageViewMultiSelectFragment$MyToggleButton;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Lepson/print/imgsel/ImageViewBaseFragment;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lepson/print/imgsel/ImageViewMultiSelectFragment;Z)V
    .locals 0

    .line 14
    invoke-direct {p0, p1}, Lepson/print/imgsel/ImageViewMultiSelectFragment;->selectImageOnMultiFileMode(Z)V

    return-void
.end method

.method public static newInstance(Ljava/lang/String;)Lepson/print/imgsel/ImageViewMultiSelectFragment;
    .locals 3

    .line 22
    new-instance v0, Lepson/print/imgsel/ImageViewMultiSelectFragment;

    invoke-direct {v0}, Lepson/print/imgsel/ImageViewMultiSelectFragment;-><init>()V

    .line 23
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "file_name"

    .line 24
    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    invoke-virtual {v0, v1}, Lepson/print/imgsel/ImageViewMultiSelectFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v0
.end method

.method private selectImageOnMultiFileMode(Z)V
    .locals 2

    .line 58
    iget-object v0, p0, Lepson/print/imgsel/ImageViewMultiSelectFragment;->mListener:Lepson/print/imgsel/ImageViewBaseFragment$OnFragmentInteractionListener;

    invoke-interface {v0}, Lepson/print/imgsel/ImageViewBaseFragment$OnFragmentInteractionListener;->getSelector()Lepson/print/imgsel/ImageSelector;

    move-result-object v0

    if-eqz p1, :cond_2

    .line 61
    invoke-virtual {v0}, Lepson/print/imgsel/ImageSelector;->canAdd()Z

    move-result p1

    if-nez p1, :cond_1

    .line 63
    iget-object p1, p0, Lepson/print/imgsel/ImageViewMultiSelectFragment;->mMyToggleButton:Lepson/print/imgsel/ImageViewMultiSelectFragment$MyToggleButton;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    .line 65
    invoke-virtual {p1, v0}, Lepson/print/imgsel/ImageViewMultiSelectFragment$MyToggleButton;->forceSetChecked(Z)V

    :cond_0
    const p1, 0x7f0e03a7

    .line 68
    invoke-virtual {p0, p1}, Lepson/print/imgsel/ImageViewMultiSelectFragment;->getString(I)Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x2

    invoke-static {p1, v0}, Lepson/print/imgsel/LocalAlertDialogFragment;->newInstance(Ljava/lang/String;I)Lepson/print/imgsel/LocalAlertDialogFragment;

    move-result-object p1

    .line 71
    :try_start_0
    invoke-virtual {p0}, Lepson/print/imgsel/ImageViewMultiSelectFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "alert dialog"

    invoke-virtual {p1, v0, v1}, Lepson/print/imgsel/LocalAlertDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void

    .line 80
    :cond_1
    iget-object p1, p0, Lepson/print/imgsel/ImageViewMultiSelectFragment;->mFileName:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lepson/print/imgsel/ImageSelector;->add(Ljava/lang/String;)Z

    goto :goto_0

    .line 82
    :cond_2
    iget-object p1, p0, Lepson/print/imgsel/ImageViewMultiSelectFragment;->mFileName:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lepson/print/imgsel/ImageSelector;->remove(Ljava/lang/String;)Z

    .line 86
    :goto_0
    invoke-virtual {p0}, Lepson/print/imgsel/ImageViewMultiSelectFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p1

    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->invalidateOptionsMenu()V

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    const v0, 0x7f0a006d

    const/4 v1, 0x0

    .line 34
    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f080195

    .line 35
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lepson/common/ScalableImageView;

    iput-object v1, p0, Lepson/print/imgsel/ImageViewMultiSelectFragment;->mImageView:Lepson/common/ScalableImageView;

    const v1, 0x7f080193

    .line 36
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lepson/print/imgsel/ImageViewMultiSelectFragment;->mProgressBar:Landroid/widget/ProgressBar;

    .line 39
    iget-object v1, p0, Lepson/print/imgsel/ImageViewMultiSelectFragment;->mListener:Lepson/print/imgsel/ImageViewBaseFragment$OnFragmentInteractionListener;

    invoke-interface {v1}, Lepson/print/imgsel/ImageViewBaseFragment$OnFragmentInteractionListener;->getSelector()Lepson/print/imgsel/ImageSelector;

    move-result-object v1

    .line 40
    iget-object v2, p0, Lepson/print/imgsel/ImageViewMultiSelectFragment;->mFileName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lepson/print/imgsel/ImageSelector;->isSelected(Ljava/lang/String;)Z

    move-result v1

    const v2, 0x7f08021f

    .line 44
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 45
    new-instance v3, Lepson/print/imgsel/ImageViewMultiSelectFragment$MyToggleButton;

    invoke-direct {v3, p0, v2, v1}, Lepson/print/imgsel/ImageViewMultiSelectFragment$MyToggleButton;-><init>(Lepson/print/imgsel/ImageViewMultiSelectFragment;Landroid/view/View;Z)V

    iput-object v3, p0, Lepson/print/imgsel/ImageViewMultiSelectFragment;->mMyToggleButton:Lepson/print/imgsel/ImageViewMultiSelectFragment$MyToggleButton;

    .line 47
    invoke-super {p0, p1, p2, p3}, Lepson/print/imgsel/ImageViewBaseFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    return-object v0
.end method

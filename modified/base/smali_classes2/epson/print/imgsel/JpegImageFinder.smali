.class public Lepson/print/imgsel/JpegImageFinder;
.super Lepson/print/imgsel/MediaStoreImageFinder;
.source "JpegImageFinder.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Lepson/print/imgsel/MediaStoreImageFinder;-><init>()V

    return-void
.end method


# virtual methods
.method protected myCheckFile(Ljava/io/File;)Z
    .locals 0

    .line 25
    invoke-virtual {p1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/memcardacc/MemcardUtil;->isJpegFile(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    const/4 p1, 0x1

    return p1
.end method

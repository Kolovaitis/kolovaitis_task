.class public Lepson/print/imgsel/ImageViewBaseFragment;
.super Landroid/support/v4/app/Fragment;
.source "ImageViewBaseFragment.java"

# interfaces
.implements Lepson/print/imgsel/ImageResizer$BitmapDrawer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/imgsel/ImageViewBaseFragment$OnFragmentInteractionListener;
    }
.end annotation


# static fields
.field protected static final ARG_FILENAME:Ljava/lang/String; = "file_name"

.field protected static final ARG_MESSAGE_TYPE:Ljava/lang/String; = "message_type"

.field private static final paramOffsetX:Ljava/lang/String; = "paramOffsetX"

.field private static final paramOffsetY:Ljava/lang/String; = "paramOffsetY"

.field private static final paramScale:Ljava/lang/String; = "paramScale"


# instance fields
.field protected mFileName:Ljava/lang/String;

.field protected mImageView:Lepson/common/ScalableImageView;

.field protected mListener:Lepson/print/imgsel/ImageViewBaseFragment$OnFragmentInteractionListener;

.field protected mLoadTask:Lepson/print/imgsel/ImageResizer$ResizeTask;

.field protected mProgressBar:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 56
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method public static newInstance(Ljava/lang/String;ZI)Lepson/print/imgsel/ImageViewBaseFragment;
    .locals 0

    if-eqz p1, :cond_0

    .line 48
    invoke-static {p0, p2}, Lepson/print/imgsel/ImageViewSingleFragment;->newInstance(Ljava/lang/String;I)Lepson/print/imgsel/ImageViewSingleFragment;

    move-result-object p0

    goto :goto_0

    .line 50
    :cond_0
    invoke-static {p0}, Lepson/print/imgsel/ImageViewMultiSelectFragment;->newInstance(Ljava/lang/String;)Lepson/print/imgsel/ImageViewMultiSelectFragment;

    move-result-object p0

    :goto_0
    return-object p0
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1

    .line 88
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 90
    invoke-virtual {p0}, Lepson/print/imgsel/ImageViewBaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p1

    check-cast p1, Lepson/print/imgsel/ImageViewPagerActivity;

    .line 91
    invoke-virtual {p1}, Lepson/print/imgsel/ImageViewPagerActivity;->getImageResizer()Lepson/print/imgsel/ImageResizer;

    move-result-object p1

    .line 92
    iget-object v0, p0, Lepson/print/imgsel/ImageViewBaseFragment;->mFileName:Ljava/lang/String;

    invoke-virtual {p1, v0, p0}, Lepson/print/imgsel/ImageResizer;->load(Ljava/lang/String;Lepson/print/imgsel/ImageResizer$BitmapDrawer;)Lepson/print/imgsel/ImageResizer$ResizeTask;

    move-result-object p1

    iput-object p1, p0, Lepson/print/imgsel/ImageViewBaseFragment;->mLoadTask:Lepson/print/imgsel/ImageResizer$ResizeTask;

    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 2

    .line 98
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/content/Context;)V

    .line 100
    :try_start_0
    move-object v0, p1

    check-cast v0, Lepson/print/imgsel/ImageViewBaseFragment$OnFragmentInteractionListener;

    iput-object v0, p0, Lepson/print/imgsel/ImageViewBaseFragment;->mListener:Lepson/print/imgsel/ImageViewBaseFragment$OnFragmentInteractionListener;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    .line 102
    :catch_0
    new-instance v0, Ljava/lang/ClassCastException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " must implement OnFragmentInteractionListener"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 62
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 63
    invoke-virtual {p0}, Lepson/print/imgsel/ImageViewBaseFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 64
    invoke-virtual {p0}, Lepson/print/imgsel/ImageViewBaseFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "file_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/print/imgsel/ImageViewBaseFragment;->mFileName:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    if-eqz p3, :cond_0

    .line 71
    iget-object v0, p0, Lepson/print/imgsel/ImageViewBaseFragment;->mImageView:Lepson/common/ScalableImageView;

    const-string v1, "paramScale"

    invoke-virtual {v0}, Lepson/common/ScalableImageView;->getmScaleFactor()F

    move-result v2

    invoke-virtual {p3, v1, v2}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;F)F

    move-result v1

    invoke-virtual {v0, v1}, Lepson/common/ScalableImageView;->setmScaleFactor(F)V

    .line 72
    iget-object v0, p0, Lepson/print/imgsel/ImageViewBaseFragment;->mImageView:Lepson/common/ScalableImageView;

    const-string v1, "paramOffsetX"

    invoke-virtual {v0}, Lepson/common/ScalableImageView;->getmOffsetX()F

    move-result v2

    invoke-virtual {p3, v1, v2}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;F)F

    move-result v1

    invoke-virtual {v0, v1}, Lepson/common/ScalableImageView;->setmOffsetX(F)V

    .line 73
    iget-object v0, p0, Lepson/print/imgsel/ImageViewBaseFragment;->mImageView:Lepson/common/ScalableImageView;

    const-string v1, "paramOffsetY"

    invoke-virtual {v0}, Lepson/common/ScalableImageView;->getmOffsetY()F

    move-result v2

    invoke-virtual {p3, v1, v2}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;F)F

    move-result v1

    invoke-virtual {v0, v1}, Lepson/common/ScalableImageView;->setmOffsetY(F)V

    .line 75
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public onDestroy()V
    .locals 2

    .line 109
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 111
    iget-object v0, p0, Lepson/print/imgsel/ImageViewBaseFragment;->mLoadTask:Lepson/print/imgsel/ImageResizer$ResizeTask;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    .line 112
    invoke-virtual {v0, v1}, Lepson/print/imgsel/ImageResizer$ResizeTask;->cancel(Z)Z

    :cond_0
    return-void
.end method

.method public onDetach()V
    .locals 1

    .line 118
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    const/4 v0, 0x0

    .line 119
    iput-object v0, p0, Lepson/print/imgsel/ImageViewBaseFragment;->mListener:Lepson/print/imgsel/ImageViewBaseFragment$OnFragmentInteractionListener;

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .line 80
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "paramScale"

    .line 81
    iget-object v1, p0, Lepson/print/imgsel/ImageViewBaseFragment;->mImageView:Lepson/common/ScalableImageView;

    invoke-virtual {v1}, Lepson/common/ScalableImageView;->getmScaleFactor()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    const-string v0, "paramOffsetX"

    .line 82
    iget-object v1, p0, Lepson/print/imgsel/ImageViewBaseFragment;->mImageView:Lepson/common/ScalableImageView;

    invoke-virtual {v1}, Lepson/common/ScalableImageView;->getmOffsetX()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    const-string v0, "paramOffsetY"

    .line 83
    iget-object v1, p0, Lepson/print/imgsel/ImageViewBaseFragment;->mImageView:Lepson/common/ScalableImageView;

    invoke-virtual {v1}, Lepson/common/ScalableImageView;->getmOffsetY()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    return-void
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .locals 1

    .line 128
    iget-object v0, p0, Lepson/print/imgsel/ImageViewBaseFragment;->mImageView:Lepson/common/ScalableImageView;

    if-eqz v0, :cond_1

    if-nez p1, :cond_0

    goto :goto_0

    .line 132
    :cond_0
    invoke-virtual {v0, p1}, Lepson/common/ScalableImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 133
    iget-object p1, p0, Lepson/print/imgsel/ImageViewBaseFragment;->mImageView:Lepson/common/ScalableImageView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lepson/common/ScalableImageView;->setVisibility(I)V

    .line 135
    iget-object p1, p0, Lepson/print/imgsel/ImageViewBaseFragment;->mProgressBar:Landroid/widget/ProgressBar;

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    return-void

    :cond_1
    :goto_0
    return-void
.end method

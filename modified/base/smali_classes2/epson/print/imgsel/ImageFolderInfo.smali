.class Lepson/print/imgsel/ImageFolderInfo;
.super Ljava/lang/Object;
.source "ImageFolderInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/imgsel/ImageFolderInfo$DatabaseOrderComparator;
    }
.end annotation


# static fields
.field public static final IMAGE_ID_MAX:I = 0x4


# instance fields
.field public mCanonicalName:Ljava/lang/String;

.field private mImageCount:I

.field private mImageIdList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mImageNameList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mMiniDatabaseOrder:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lepson/print/imgsel/ImageFolderInfo;->mImageIdList:Ljava/util/ArrayList;

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lepson/print/imgsel/ImageFolderInfo;->mImageNameList:Ljava/util/ArrayList;

    .line 31
    iput-object p1, p0, Lepson/print/imgsel/ImageFolderInfo;->mCanonicalName:Ljava/lang/String;

    const/4 p1, -0x1

    .line 32
    iput p1, p0, Lepson/print/imgsel/ImageFolderInfo;->mMiniDatabaseOrder:I

    return-void
.end method


# virtual methods
.method public addImageLastIfPossible(JLjava/lang/String;I)Z
    .locals 2

    .line 42
    iget v0, p0, Lepson/print/imgsel/ImageFolderInfo;->mMiniDatabaseOrder:I

    if-gez v0, :cond_0

    .line 45
    iput p4, p0, Lepson/print/imgsel/ImageFolderInfo;->mMiniDatabaseOrder:I

    .line 47
    :cond_0
    iget p4, p0, Lepson/print/imgsel/ImageFolderInfo;->mImageCount:I

    const/4 v0, 0x1

    add-int/2addr p4, v0

    iput p4, p0, Lepson/print/imgsel/ImageFolderInfo;->mImageCount:I

    .line 48
    iget-object p4, p0, Lepson/print/imgsel/ImageFolderInfo;->mImageIdList:Ljava/util/ArrayList;

    invoke-virtual {p4}, Ljava/util/ArrayList;->size()I

    move-result p4

    const/4 v1, 0x4

    if-lt p4, v1, :cond_1

    const/4 p1, 0x0

    return p1

    .line 51
    :cond_1
    iget-object p4, p0, Lepson/print/imgsel/ImageFolderInfo;->mImageIdList:Ljava/util/ArrayList;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {p4, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 52
    iget-object p1, p0, Lepson/print/imgsel/ImageFolderInfo;->mImageNameList:Ljava/util/ArrayList;

    invoke-virtual {p1, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return v0
.end method

.method public getFileIdCount()I
    .locals 1

    .line 80
    iget-object v0, p0, Lepson/print/imgsel/ImageFolderInfo;->mImageIdList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getFileName(I)Ljava/lang/String;
    .locals 1

    .line 67
    iget-object v0, p0, Lepson/print/imgsel/ImageFolderInfo;->mImageNameList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_1

    if-gez p1, :cond_0

    goto :goto_0

    .line 72
    :cond_0
    iget-object v0, p0, Lepson/print/imgsel/ImageFolderInfo;->mImageNameList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    return-object p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public getId(I)J
    .locals 2

    .line 58
    iget-object v0, p0, Lepson/print/imgsel/ImageFolderInfo;->mImageIdList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_1

    if-gez p1, :cond_0

    goto :goto_0

    .line 63
    :cond_0
    iget-object v0, p0, Lepson/print/imgsel/ImageFolderInfo;->mImageIdList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0

    :cond_1
    :goto_0
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public getImageCount()I
    .locals 1

    .line 85
    iget v0, p0, Lepson/print/imgsel/ImageFolderInfo;->mImageCount:I

    return v0
.end method

.method public getMiniDatabaseOrder()I
    .locals 1

    .line 90
    iget v0, p0, Lepson/print/imgsel/ImageFolderInfo;->mMiniDatabaseOrder:I

    return v0
.end method

.class public Lepson/print/imgsel/ImageViewSingleFragment;
.super Lepson/print/imgsel/ImageViewBaseFragment;
.source "ImageViewSingleFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 35
    invoke-direct {p0}, Lepson/print/imgsel/ImageViewBaseFragment;-><init>()V

    return-void
.end method

.method public static newInstance(Ljava/lang/String;I)Lepson/print/imgsel/ImageViewSingleFragment;
    .locals 3

    .line 26
    new-instance v0, Lepson/print/imgsel/ImageViewSingleFragment;

    invoke-direct {v0}, Lepson/print/imgsel/ImageViewSingleFragment;-><init>()V

    .line 27
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "file_name"

    .line 28
    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p0, "message_type"

    .line 29
    invoke-virtual {v1, p0, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 30
    invoke-virtual {v0, v1}, Lepson/print/imgsel/ImageViewSingleFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v0
.end method

.method private onSingleModeItemSelected()V
    .locals 1

    .line 100
    iget-object v0, p0, Lepson/print/imgsel/ImageViewSingleFragment;->mListener:Lepson/print/imgsel/ImageViewBaseFragment$OnFragmentInteractionListener;

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lepson/print/imgsel/ImageViewSingleFragment;->mListener:Lepson/print/imgsel/ImageViewBaseFragment$OnFragmentInteractionListener;

    invoke-interface {v0}, Lepson/print/imgsel/ImageViewBaseFragment$OnFragmentInteractionListener;->onSingleModeItemSelected()V

    :cond_0
    return-void
.end method

.method private selectImageOnSingleFileMode()V
    .locals 2

    .line 87
    iget-object v0, p0, Lepson/print/imgsel/ImageViewSingleFragment;->mListener:Lepson/print/imgsel/ImageViewBaseFragment$OnFragmentInteractionListener;

    invoke-interface {v0}, Lepson/print/imgsel/ImageViewBaseFragment$OnFragmentInteractionListener;->getSelector()Lepson/print/imgsel/ImageSelector;

    move-result-object v0

    .line 89
    invoke-virtual {v0}, Lepson/print/imgsel/ImageSelector;->clear()V

    .line 90
    iget-object v1, p0, Lepson/print/imgsel/ImageViewSingleFragment;->mFileName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lepson/print/imgsel/ImageSelector;->add(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 91
    invoke-direct {p0}, Lepson/print/imgsel/ImageViewSingleFragment;->onSingleModeItemSelected()V

    :cond_0
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 0

    .line 78
    invoke-direct {p0}, Lepson/print/imgsel/ImageViewSingleFragment;->selectImageOnSingleFileMode()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 41
    invoke-super {p0, p1}, Lepson/print/imgsel/ImageViewBaseFragment;->onCreate(Landroid/os/Bundle;)V

    .line 42
    invoke-virtual {p0}, Lepson/print/imgsel/ImageViewSingleFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 43
    invoke-virtual {p0}, Lepson/print/imgsel/ImageViewSingleFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "file_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lepson/print/imgsel/ImageViewSingleFragment;->mFileName:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    const/4 v0, 0x0

    const v1, 0x7f0a006e

    .line 50
    invoke-virtual {p1, v1, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f080195

    .line 51
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lepson/common/ScalableImageView;

    iput-object v2, p0, Lepson/print/imgsel/ImageViewSingleFragment;->mImageView:Lepson/common/ScalableImageView;

    const v2, 0x7f080193

    .line 52
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    iput-object v2, p0, Lepson/print/imgsel/ImageViewSingleFragment;->mProgressBar:Landroid/widget/ProgressBar;

    const v2, 0x7f0802ef

    .line 54
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 55
    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 59
    invoke-virtual {p0}, Lepson/print/imgsel/ImageViewSingleFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    if-eqz v3, :cond_0

    const-string v4, "message_type"

    .line 61
    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    :cond_0
    const/4 v3, 0x1

    if-ne v0, v3, :cond_1

    const v0, 0x7f0e0397

    .line 64
    invoke-virtual {v2, v0}, Landroid/widget/Button;->setText(I)V

    .line 67
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lepson/print/imgsel/ImageViewBaseFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    return-object v1
.end method

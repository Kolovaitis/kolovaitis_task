.class public abstract Lepson/print/imgsel/ImageSelectActivity;
.super Lepson/print/ActivityIACommon;
.source "ImageSelectActivity.java"

# interfaces
.implements Lepson/print/imgsel/ImageFolderListFragment$FolderSelectedListener;
.implements Lepson/print/imgsel/ImageGridFragment$ImageGridListener;
.implements Lepson/print/imgsel/LocalAlertDialogFragment$DialogCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/imgsel/ImageSelectActivity$OsCacheDialog;
    }
.end annotation


# static fields
.field public static final DIALOG_CODE_ERROR_FINISH:I = 0x1

.field public static final PARAM_SELECTED_FILE_LIST:Ljava/lang/String; = "selected_files"

.field private static final STATE_SELECT_LIST:Ljava/lang/String; = "select_list"

.field private static final TAG_FRAGMENT_FOLDER_LIST:Ljava/lang/String; = "folder_list"

.field private static final TAG_FRAGMENT_IMAGE_GRID:Ljava/lang/String; = "image_grid"


# instance fields
.field private mImageSelector:Lepson/print/imgsel/ImageSelector;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 21
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    return-void
.end method

.method private getActiveFragmentTag()Ljava/lang/String;
    .locals 3

    .line 335
    invoke-virtual {p0}, Lepson/print/imgsel/ImageSelectActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 337
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->getBackStackEntryCount()I

    move-result v1

    const/4 v2, 0x1

    if-ge v1, v2, :cond_1

    const-string v1, "folder_list"

    .line 341
    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v1

    :cond_0
    const/4 v0, 0x0

    return-object v0

    :cond_1
    sub-int/2addr v1, v2

    .line 349
    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->getBackStackEntryAt(I)Landroid/support/v4/app/FragmentManager$BackStackEntry;

    move-result-object v0

    invoke-interface {v0}, Landroid/support/v4/app/FragmentManager$BackStackEntry;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getGridFragment()Lepson/print/imgsel/ImageGridFragment;
    .locals 2

    .line 264
    invoke-virtual {p0}, Lepson/print/imgsel/ImageSelectActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "image_grid"

    .line 265
    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lepson/print/imgsel/ImageGridFragment;

    return-object v0
.end method

.method private getSelectedListFromIntent(Landroid/content/Intent;)Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    const-string v0, "selected_files"

    .line 84
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object p1

    return-object p1
.end method

.method private reloadFragment()V
    .locals 4

    .line 317
    invoke-direct {p0}, Lepson/print/imgsel/ImageSelectActivity;->getActiveFragmentTag()Ljava/lang/String;

    move-result-object v0

    const-string v1, "image_grid"

    .line 318
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 319
    invoke-virtual {p0}, Lepson/print/imgsel/ImageSelectActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 320
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->popBackStack()V

    .line 323
    :cond_0
    new-instance v0, Lepson/print/imgsel/ImageFolderListFragment;

    invoke-direct {v0}, Lepson/print/imgsel/ImageFolderListFragment;-><init>()V

    .line 324
    invoke-virtual {p0}, Lepson/print/imgsel/ImageSelectActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f0800d8

    const-string v3, "folder_list"

    .line 325
    invoke-virtual {v1, v2, v0, v3}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 326
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    return-void
.end method

.method private startImageGridFragment(Ljava/lang/String;)V
    .locals 3

    .line 214
    invoke-static {p1}, Lepson/print/imgsel/ImageGridFragment;->newInstance(Ljava/lang/String;)Lepson/print/imgsel/ImageGridFragment;

    move-result-object p1

    .line 215
    invoke-virtual {p0}, Lepson/print/imgsel/ImageSelectActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const-string v1, "image_grid"

    const v2, 0x7f0800d8

    .line 217
    invoke-virtual {v0, v2, p1, v1}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object p1

    const-string v0, "image_grid"

    .line 218
    invoke-virtual {p1, v0}, Landroid/support/v4/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object p1

    .line 219
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    return-void
.end method


# virtual methods
.method public changeThumbnailCreateMethod(Z)V
    .locals 0

    .line 307
    invoke-static {p0, p1}, Lepson/print/imgsel/AltThumbnailCache;->setThumbnailCreateMethod(Landroid/content/Context;Z)V

    .line 308
    invoke-direct {p0}, Lepson/print/imgsel/ImageSelectActivity;->reloadFragment()V

    return-void
.end method

.method public abstract getImageFinder()Lepson/print/imgsel/ImageFinder;
.end method

.method public getImageSelector()Lepson/print/imgsel/ImageSelector;
    .locals 1

    .line 272
    iget-object v0, p0, Lepson/print/imgsel/ImageSelectActivity;->mImageSelector:Lepson/print/imgsel/ImageSelector;

    return-object v0
.end method

.method public getMessageType()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected abstract goNext()V
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .line 42
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f0a0030

    .line 43
    invoke-virtual {p0, v0}, Lepson/print/imgsel/ImageSelectActivity;->setContentView(I)V

    const-string v0, ""

    const/4 v1, 0x1

    .line 45
    invoke-virtual {p0, v0, v1}, Lepson/print/imgsel/ImageSelectActivity;->setActionBar(Ljava/lang/String;Z)V

    if-nez p1, :cond_0

    .line 48
    new-instance v0, Lepson/print/imgsel/ImageFolderListFragment;

    invoke-direct {v0}, Lepson/print/imgsel/ImageFolderListFragment;-><init>()V

    .line 49
    invoke-virtual {p0}, Lepson/print/imgsel/ImageSelectActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    const v3, 0x7f0800d8

    const-string v4, "folder_list"

    .line 50
    invoke-virtual {v2, v3, v0, v4}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 51
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 54
    :cond_0
    new-instance v0, Lepson/print/imgsel/ImageSelector;

    invoke-direct {v0}, Lepson/print/imgsel/ImageSelector;-><init>()V

    iput-object v0, p0, Lepson/print/imgsel/ImageSelectActivity;->mImageSelector:Lepson/print/imgsel/ImageSelector;

    if-eqz p1, :cond_1

    const-string v0, "select_list"

    .line 56
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 58
    iget-object v0, p0, Lepson/print/imgsel/ImageSelectActivity;->mImageSelector:Lepson/print/imgsel/ImageSelector;

    invoke-virtual {v0, p1}, Lepson/print/imgsel/ImageSelector;->replaceFiles(Ljava/util/ArrayList;)V

    goto :goto_0

    .line 61
    :cond_1
    invoke-virtual {p0}, Lepson/print/imgsel/ImageSelectActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-direct {p0, p1}, Lepson/print/imgsel/ImageSelectActivity;->getSelectedListFromIntent(Landroid/content/Intent;)Ljava/util/ArrayList;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 63
    iget-object v0, p0, Lepson/print/imgsel/ImageSelectActivity;->mImageSelector:Lepson/print/imgsel/ImageSelector;

    invoke-virtual {v0, p1}, Lepson/print/imgsel/ImageSelector;->replaceFiles(Ljava/util/ArrayList;)V

    .line 67
    :cond_2
    :goto_0
    invoke-static {}, Lepson/common/Utils;->isMediaMounted()Z

    move-result p1

    if-nez p1, :cond_3

    .line 69
    invoke-virtual {p0}, Lepson/print/imgsel/ImageSelectActivity;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v0, 0x7f0e04ea

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 68
    invoke-static {p1, v1}, Lepson/print/imgsel/LocalAlertDialogFragment;->newInstance(Ljava/lang/String;I)Lepson/print/imgsel/LocalAlertDialogFragment;

    move-result-object p1

    .line 71
    invoke-virtual {p0}, Lepson/print/imgsel/ImageSelectActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "alert_dialog"

    invoke-virtual {p1, v0, v1}, Lepson/print/imgsel/LocalAlertDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void

    .line 75
    :cond_3
    invoke-virtual {p0}, Lepson/print/imgsel/ImageSelectActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lepson/print/imgsel/AltThumbnailCache;->initThumbnailCache(Landroid/content/Context;)V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 6

    .line 91
    iget-object v0, p0, Lepson/print/imgsel/ImageSelectActivity;->mImageSelector:Lepson/print/imgsel/ImageSelector;

    invoke-virtual {v0}, Lepson/print/imgsel/ImageSelector;->selectedNumber()I

    move-result v0

    .line 93
    invoke-virtual {p0}, Lepson/print/imgsel/ImageSelectActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    const v2, 0x7f0b0004

    invoke-virtual {v1, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const v1, 0x7f080044

    .line 94
    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 96
    invoke-virtual {p0}, Lepson/print/imgsel/ImageSelectActivity;->singleImageMode()Z

    move-result v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-nez v2, :cond_1

    if-lez v0, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 101
    :goto_0
    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_1

    .line 103
    :cond_1
    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :goto_1
    if-gtz v0, :cond_4

    .line 108
    invoke-direct {p0}, Lepson/print/imgsel/ImageSelectActivity;->getGridFragment()Lepson/print/imgsel/ImageGridFragment;

    move-result-object v0

    const v1, 0x7f0e022f

    if-nez v0, :cond_2

    .line 111
    invoke-virtual {p0}, Lepson/print/imgsel/ImageSelectActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lepson/print/imgsel/ImageSelectActivity;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 114
    :cond_2
    invoke-virtual {v0}, Lepson/print/imgsel/ImageGridFragment;->getFolderName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 116
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    .line 117
    invoke-virtual {p0, v0}, Lepson/print/imgsel/ImageSelectActivity;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 120
    :cond_3
    invoke-virtual {p0}, Lepson/print/imgsel/ImageSelectActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lepson/print/imgsel/ImageSelectActivity;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 126
    :cond_4
    invoke-virtual {p0}, Lepson/print/imgsel/ImageSelectActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e042c

    new-array v5, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v4

    invoke-virtual {v1, v2, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 127
    invoke-virtual {p0, v0}, Lepson/print/imgsel/ImageSelectActivity;->setTitle(Ljava/lang/CharSequence;)V

    :goto_2
    const v0, 0x7f08004e

    .line 130
    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f08004b

    .line 131
    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object p1

    .line 134
    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 135
    invoke-interface {p1, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 138
    invoke-direct {p0}, Lepson/print/imgsel/ImageSelectActivity;->getGridFragment()Lepson/print/imgsel/ImageGridFragment;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 139
    invoke-virtual {p0}, Lepson/print/imgsel/ImageSelectActivity;->singleImageMode()Z

    move-result v1

    if-nez v1, :cond_5

    .line 140
    invoke-direct {p0}, Lepson/print/imgsel/ImageSelectActivity;->getGridFragment()Lepson/print/imgsel/ImageGridFragment;

    move-result-object v1

    invoke-virtual {v1}, Lepson/print/imgsel/ImageGridFragment;->getPhotoSelectionMode()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto :goto_3

    .line 146
    :pswitch_0
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 147
    invoke-interface {p1, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_3

    .line 142
    :pswitch_1
    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 143
    invoke-interface {p1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_5
    :goto_3
    return v3

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onDialogCallback(I)V
    .locals 1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    return-void

    .line 293
    :cond_0
    invoke-virtual {p0}, Lepson/print/imgsel/ImageSelectActivity;->finish()V

    return-void
.end method

.method public onFolderSelected(Lepson/print/imgsel/ImageFolderInfo;)V
    .locals 0

    .line 204
    iget-object p1, p1, Lepson/print/imgsel/ImageFolderInfo;->mCanonicalName:Ljava/lang/String;

    invoke-direct {p0, p1}, Lepson/print/imgsel/ImageSelectActivity;->startImageGridFragment(Ljava/lang/String;)V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4

    .line 161
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 163
    invoke-direct {p0}, Lepson/print/imgsel/ImageSelectActivity;->getGridFragment()Lepson/print/imgsel/ImageGridFragment;

    move-result-object v1

    const/4 v2, 0x1

    const v3, 0x7f08004d

    if-ne v0, v3, :cond_0

    .line 166
    invoke-virtual {p0}, Lepson/print/imgsel/ImageSelectActivity;->showOsCacheDialog()V

    return v2

    :cond_0
    const v3, 0x7f080044

    if-ne v0, v3, :cond_1

    .line 172
    invoke-virtual {p0}, Lepson/print/imgsel/ImageSelectActivity;->goNext()V

    return v2

    :cond_1
    const v3, 0x7f08004b

    if-ne v0, v3, :cond_2

    if-eqz v1, :cond_3

    .line 176
    invoke-virtual {v1, v2}, Lepson/print/imgsel/ImageGridFragment;->setPhotoSelectionMode(I)V

    .line 177
    invoke-virtual {p0}, Lepson/print/imgsel/ImageSelectActivity;->supportInvalidateOptionsMenu()V

    return v2

    :cond_2
    const v3, 0x7f08004e

    if-ne v0, v3, :cond_3

    if-eqz v1, :cond_3

    const/4 p1, 0x0

    .line 182
    invoke-virtual {v1, p1}, Lepson/print/imgsel/ImageGridFragment;->setPhotoSelectionMode(I)V

    .line 183
    invoke-virtual {p0}, Lepson/print/imgsel/ImageSelectActivity;->supportInvalidateOptionsMenu()V

    return v2

    .line 188
    :cond_3
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1
.end method

.method protected onPause()V
    .locals 1

    .line 224
    invoke-virtual {p0}, Lepson/print/imgsel/ImageSelectActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 225
    invoke-static {}, Lepson/print/imgsel/AltThumbnailCache;->deleteCacheDirectory()V

    .line 228
    :cond_0
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onPause()V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "select_list"

    .line 198
    iget-object v1, p0, Lepson/print/imgsel/ImageSelectActivity;->mImageSelector:Lepson/print/imgsel/ImageSelector;

    invoke-virtual {v1}, Lepson/print/imgsel/ImageSelector;->getFileArrayList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 199
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method protected refreshSelector(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    if-nez p1, :cond_0

    return-void

    .line 247
    :cond_0
    iget-object v0, p0, Lepson/print/imgsel/ImageSelectActivity;->mImageSelector:Lepson/print/imgsel/ImageSelector;

    invoke-virtual {v0, p1}, Lepson/print/imgsel/ImageSelector;->replaceFiles(Ljava/util/ArrayList;)V

    .line 248
    invoke-direct {p0}, Lepson/print/imgsel/ImageSelectActivity;->getGridFragment()Lepson/print/imgsel/ImageGridFragment;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 249
    invoke-direct {p0}, Lepson/print/imgsel/ImageSelectActivity;->getGridFragment()Lepson/print/imgsel/ImageGridFragment;

    move-result-object v0

    invoke-virtual {v0, p1}, Lepson/print/imgsel/ImageGridFragment;->updateImageSelector(Ljava/util/ArrayList;)V

    .line 253
    :cond_1
    invoke-virtual {p0}, Lepson/print/imgsel/ImageSelectActivity;->supportInvalidateOptionsMenu()V

    return-void
.end method

.method public showNfcInvalidMessageOnPagerActivity()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected showOsCacheDialog()V
    .locals 3

    .line 192
    invoke-static {}, Lepson/print/imgsel/ImageSelectActivity$OsCacheDialog;->newInstance()Lepson/print/imgsel/ImageSelectActivity$OsCacheDialog;

    move-result-object v0

    .line 193
    invoke-virtual {p0}, Lepson/print/imgsel/ImageSelectActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "dialog"

    invoke-virtual {v0, v1, v2}, Lepson/print/imgsel/ImageSelectActivity$OsCacheDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method public abstract singleImageMode()Z
.end method

.class public Lepson/print/imgsel/LocalAlertDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "LocalAlertDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/imgsel/LocalAlertDialogFragment$DialogCallback;
    }
.end annotation


# static fields
.field private static final KEY_DIALOG_CODE:Ljava/lang/String; = "dialog_code"

.field private static final KEY_MESSAGE:Ljava/lang/String; = "message"

.field private static final KEY_NO_TITLE:Ljava/lang/String; = "no_title"

.field private static final KEY_TITLE:Ljava/lang/String; = "title"


# instance fields
.field private mDialogCode:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lepson/print/imgsel/LocalAlertDialogFragment;)V
    .locals 0

    .line 19
    invoke-direct {p0}, Lepson/print/imgsel/LocalAlertDialogFragment;->execCallback()V

    return-void
.end method

.method private execCallback()V
    .locals 2

    .line 113
    :try_start_0
    invoke-virtual {p0}, Lepson/print/imgsel/LocalAlertDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lepson/print/imgsel/LocalAlertDialogFragment$DialogCallback;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_0

    return-void

    .line 121
    :cond_0
    iget v1, p0, Lepson/print/imgsel/LocalAlertDialogFragment;->mDialogCode:I

    invoke-interface {v0, v1}, Lepson/print/imgsel/LocalAlertDialogFragment$DialogCallback;->onDialogCallback(I)V

    return-void

    :catch_0
    return-void
.end method

.method public static newInstance(Ljava/lang/String;I)Lepson/print/imgsel/LocalAlertDialogFragment;
    .locals 3

    .line 60
    new-instance v0, Lepson/print/imgsel/LocalAlertDialogFragment;

    invoke-direct {v0}, Lepson/print/imgsel/LocalAlertDialogFragment;-><init>()V

    .line 62
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "message"

    .line 63
    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p0, "dialog_code"

    .line 64
    invoke-virtual {v1, p0, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string p0, "no_title"

    const/4 p1, 0x1

    .line 65
    invoke-virtual {v1, p0, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 67
    invoke-virtual {v0, v1}, Lepson/print/imgsel/LocalAlertDialogFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v0
.end method

.method public static newInstance(Ljava/lang/String;II)Lepson/print/imgsel/LocalAlertDialogFragment;
    .locals 3

    .line 42
    new-instance v0, Lepson/print/imgsel/LocalAlertDialogFragment;

    invoke-direct {v0}, Lepson/print/imgsel/LocalAlertDialogFragment;-><init>()V

    .line 44
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "message"

    .line 45
    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p0, "title"

    .line 46
    invoke-virtual {v1, p0, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string p0, "dialog_code"

    .line 47
    invoke-virtual {v1, p0, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 49
    invoke-virtual {v0, v1}, Lepson/print/imgsel/LocalAlertDialogFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .line 78
    invoke-virtual {p0}, Lepson/print/imgsel/LocalAlertDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "message"

    .line 80
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    :cond_0
    const-string v1, "title"

    const/4 v2, 0x0

    .line 85
    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    const-string v3, "no_title"

    .line 86
    invoke-virtual {p1, v3, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    const-string v4, "dialog_code"

    .line 88
    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lepson/print/imgsel/LocalAlertDialogFragment;->mDialogCode:I

    .line 91
    new-instance p1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lepson/print/imgsel/LocalAlertDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-direct {p1, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 92
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 93
    invoke-virtual {p0}, Lepson/print/imgsel/LocalAlertDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const v5, 0x7f0e04f2

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lepson/print/imgsel/LocalAlertDialogFragment$1;

    invoke-direct {v5, p0}, Lepson/print/imgsel/LocalAlertDialogFragment$1;-><init>(Lepson/print/imgsel/LocalAlertDialogFragment;)V

    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    if-nez v3, :cond_1

    .line 101
    invoke-virtual {p1, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 104
    :cond_1
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 105
    invoke-virtual {p1, v2}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    return-object p1
.end method

.class public Lepson/print/imgsel/ImageResizer;
.super Ljava/lang/Object;
.source "ImageResizer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/imgsel/ImageResizer$ResizeTask;,
        Lepson/print/imgsel/ImageResizer$BitmapDrawer;
    }
.end annotation


# static fields
.field private static sImageResizer:Lepson/print/imgsel/ImageResizer;


# instance fields
.field private mBitmapCache:Lepson/print/imgsel/BitmapCache;

.field private volatile mIsTargetActivityForeground:Z

.field private mReqSize:I


# direct methods
.method private constructor <init>(ILjava/io/File;)V
    .locals 0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput p1, p0, Lepson/print/imgsel/ImageResizer;->mReqSize:I

    .line 51
    new-instance p1, Lepson/print/imgsel/BitmapCache;

    invoke-direct {p1}, Lepson/print/imgsel/BitmapCache;-><init>()V

    iput-object p1, p0, Lepson/print/imgsel/ImageResizer;->mBitmapCache:Lepson/print/imgsel/BitmapCache;

    .line 52
    iget-object p1, p0, Lepson/print/imgsel/ImageResizer;->mBitmapCache:Lepson/print/imgsel/BitmapCache;

    invoke-virtual {p1, p2}, Lepson/print/imgsel/BitmapCache;->initialize(Ljava/io/File;)Z

    return-void
.end method

.method static synthetic access$000(Lepson/print/imgsel/ImageResizer;)Z
    .locals 0

    .line 27
    iget-boolean p0, p0, Lepson/print/imgsel/ImageResizer;->mIsTargetActivityForeground:Z

    return p0
.end method

.method static synthetic access$100(Lepson/print/imgsel/ImageResizer;)Lepson/print/imgsel/BitmapCache;
    .locals 0

    .line 27
    iget-object p0, p0, Lepson/print/imgsel/ImageResizer;->mBitmapCache:Lepson/print/imgsel/BitmapCache;

    return-object p0
.end method

.method public static declared-synchronized getInstance(ILjava/io/File;)Lepson/print/imgsel/ImageResizer;
    .locals 2

    const-class v0, Lepson/print/imgsel/ImageResizer;

    monitor-enter v0

    .line 43
    :try_start_0
    sget-object v1, Lepson/print/imgsel/ImageResizer;->sImageResizer:Lepson/print/imgsel/ImageResizer;

    if-nez v1, :cond_0

    .line 44
    new-instance v1, Lepson/print/imgsel/ImageResizer;

    invoke-direct {v1, p0, p1}, Lepson/print/imgsel/ImageResizer;-><init>(ILjava/io/File;)V

    sput-object v1, Lepson/print/imgsel/ImageResizer;->sImageResizer:Lepson/print/imgsel/ImageResizer;

    .line 46
    :cond_0
    sget-object p0, Lepson/print/imgsel/ImageResizer;->sImageResizer:Lepson/print/imgsel/ImageResizer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object p0

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method private updateImageViewBitmap(Landroid/graphics/Bitmap;Lepson/print/imgsel/ImageResizer$BitmapDrawer;)V
    .locals 0

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 96
    invoke-interface {p2, p1}, Lepson/print/imgsel/ImageResizer$BitmapDrawer;->setBitmap(Landroid/graphics/Bitmap;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public load(Ljava/lang/String;Lepson/print/imgsel/ImageResizer$BitmapDrawer;)Lepson/print/imgsel/ImageResizer$ResizeTask;
    .locals 2

    .line 76
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 77
    iget-object p1, p0, Lepson/print/imgsel/ImageResizer;->mBitmapCache:Lepson/print/imgsel/BitmapCache;

    invoke-virtual {p1, v0}, Lepson/print/imgsel/BitmapCache;->getBitmap(Ljava/io/File;)Landroid/graphics/Bitmap;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 79
    invoke-direct {p0, p1, p2}, Lepson/print/imgsel/ImageResizer;->updateImageViewBitmap(Landroid/graphics/Bitmap;Lepson/print/imgsel/ImageResizer$BitmapDrawer;)V

    const/4 p1, 0x0

    return-object p1

    .line 83
    :cond_0
    new-instance p1, Lepson/print/imgsel/ImageResizer$ResizeTask;

    iget v1, p0, Lepson/print/imgsel/ImageResizer;->mReqSize:I

    invoke-direct {p1, p0, v0, p2, v1}, Lepson/print/imgsel/ImageResizer$ResizeTask;-><init>(Lepson/print/imgsel/ImageResizer;Ljava/io/File;Lepson/print/imgsel/ImageResizer$BitmapDrawer;I)V

    .line 85
    sget-object p2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {p1, p2, v0}, Lepson/print/imgsel/ImageResizer$ResizeTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-object p1
.end method

.method public setActivityForeground(Z)V
    .locals 0

    .line 61
    iput-boolean p1, p0, Lepson/print/imgsel/ImageResizer;->mIsTargetActivityForeground:Z

    if-nez p1, :cond_0

    .line 63
    iget-object p1, p0, Lepson/print/imgsel/ImageResizer;->mBitmapCache:Lepson/print/imgsel/BitmapCache;

    invoke-virtual {p1}, Lepson/print/imgsel/BitmapCache;->saveJournal()V

    :cond_0
    return-void
.end method

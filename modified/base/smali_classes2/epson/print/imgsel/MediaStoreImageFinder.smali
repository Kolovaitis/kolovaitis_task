.class public abstract Lepson/print/imgsel/MediaStoreImageFinder;
.super Ljava/lang/Object;
.source "MediaStoreImageFinder.java"

# interfaces
.implements Lepson/print/imgsel/ImageFinder;


# static fields
.field private static final TAG:Ljava/lang/String; = "MediaStoreImageFinder"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private convertTempMapToSortedList(Ljava/util/Map;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lepson/print/imgsel/ImageFolderInfo;",
            ">;)",
            "Ljava/util/List<",
            "Lepson/print/imgsel/ImageFolderInfo;",
            ">;"
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 152
    :cond_0
    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p1

    .line 154
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 155
    new-instance p1, Lepson/print/imgsel/ImageFolderInfo$DatabaseOrderComparator;

    invoke-direct {p1}, Lepson/print/imgsel/ImageFolderInfo$DatabaseOrderComparator;-><init>()V

    invoke-static {v0, p1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-object v0
.end method

.method private getImageDirInfo(Landroid/database/Cursor;Lepson/print/imgsel/ImageFinder$Canceller;)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Lepson/print/imgsel/ImageFinder$Canceller;",
            ")",
            "Ljava/util/List<",
            "Lepson/print/imgsel/ImageFolderInfo;",
            ">;"
        }
    .end annotation

    const-string v0, "MediaStoreImageFinder"

    const-string v1, "Enter getImageDirInfo"

    .line 74
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-string v2, "_data"

    .line 77
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    const-string v3, "_id"

    .line 78
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    .line 80
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    const/4 v5, 0x0

    if-nez v4, :cond_0

    return-object v5

    .line 90
    :cond_0
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    const/4 v6, 0x0

    .line 97
    :cond_1
    invoke-interface {p2}, Lepson/print/imgsel/ImageFinder$Canceller;->checkCanceled()Z

    move-result v7

    if-eqz v7, :cond_2

    return-object v5

    .line 102
    :cond_2
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_3

    goto :goto_1

    .line 106
    :cond_3
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 111
    :try_start_0
    new-instance v10, Ljava/io/File;

    invoke-direct {v10, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v7

    .line 112
    invoke-virtual {v7}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_5

    .line 118
    invoke-virtual {p0, v7}, Lepson/print/imgsel/MediaStoreImageFinder;->myCheckFile(Ljava/io/File;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 119
    invoke-interface {v4, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lepson/print/imgsel/ImageFolderInfo;

    if-nez v11, :cond_4

    .line 121
    new-instance v11, Lepson/print/imgsel/ImageFolderInfo;

    invoke-direct {v11, v10}, Lepson/print/imgsel/ImageFolderInfo;-><init>(Ljava/lang/String;)V

    .line 122
    invoke-interface {v4, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    :cond_4
    invoke-virtual {v7}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v7

    .line 124
    invoke-virtual {v11, v8, v9, v7, v6}, Lepson/print/imgsel/ImageFolderInfo;->addImageLastIfPossible(JLjava/lang/String;I)Z

    goto :goto_0

    .line 114
    :cond_5
    new-instance v7, Ljava/io/IOException;

    invoke-direct {v7}, Ljava/io/IOException;-><init>()V

    throw v7
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v7

    .line 130
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    :cond_6
    :goto_0
    add-int/lit8 v6, v6, 0x1

    .line 134
    :goto_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v7

    if-nez v7, :cond_1

    .line 136
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p1

    const-string v2, "MediaStoreImageFinder"

    .line 137
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Leave getImageDirInfo time = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sub-long/2addr p1, v0

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string p1, " msec"

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v2, p1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    invoke-direct {p0, v4}, Lepson/print/imgsel/MediaStoreImageFinder;->convertTempMapToSortedList(Ljava/util/Map;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method private getImageFileInfoList(Ljava/lang/String;Landroid/database/Cursor;Lepson/print/imgsel/ImageFinder$Canceller;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/database/Cursor;",
            "Lepson/print/imgsel/ImageFinder$Canceller;",
            ")",
            "Ljava/util/List<",
            "Lepson/print/imgsel/ImageFileInfo;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p1, :cond_5

    if-eqz p2, :cond_5

    if-nez p3, :cond_0

    goto :goto_1

    .line 206
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const-string v2, "_data"

    .line 208
    invoke-interface {p2, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    const-string v3, "_id"

    .line 209
    invoke-interface {p2, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    .line 211
    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-nez v4, :cond_1

    return-object v0

    .line 217
    :cond_1
    invoke-interface {p3}, Lepson/print/imgsel/ImageFinder$Canceller;->checkCanceled()Z

    move-result v4

    if-eqz v4, :cond_2

    return-object v0

    .line 222
    :cond_2
    invoke-interface {p2, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_3

    goto :goto_0

    .line 227
    :cond_3
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 228
    invoke-virtual {v5}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v4

    .line 230
    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 231
    invoke-virtual {p0, v5}, Lepson/print/imgsel/MediaStoreImageFinder;->myCheckFile(Ljava/io/File;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 232
    invoke-interface {p2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 233
    new-instance v4, Lepson/print/imgsel/ImageFileInfo;

    invoke-virtual {v5}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, v6, v7}, Lepson/print/imgsel/ImageFileInfo;-><init>(Ljava/lang/String;J)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 237
    :cond_4
    :goto_0
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_1

    return-object v1

    :cond_5
    :goto_1
    return-object v0
.end method

.method private myQuery(Landroid/content/ContentResolver;)Landroid/database/Cursor;
    .locals 6

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    const-string v5, "date_modified DESC"

    const-string v0, "_id"

    const-string v1, "_data"

    .line 250
    filled-new-array {v0, v1}, [Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 261
    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public findImageInDirectory(Ljava/lang/String;Landroid/content/ContentResolver;Lepson/print/imgsel/ImageFinder$Canceller;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/content/ContentResolver;",
            "Lepson/print/imgsel/ImageFinder$Canceller;",
            ")",
            "Ljava/util/List<",
            "Lepson/print/imgsel/ImageFileInfo;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 173
    :try_start_0
    invoke-direct {p0, p2}, Lepson/print/imgsel/MediaStoreImageFinder;->myQuery(Landroid/content/ContentResolver;)Landroid/database/Cursor;

    move-result-object p2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-nez p2, :cond_0

    return-object v0

    .line 185
    :cond_0
    :try_start_1
    invoke-direct {p0, p1, p2, p3}, Lepson/print/imgsel/MediaStoreImageFinder;->getImageFileInfoList(Ljava/lang/String;Landroid/database/Cursor;Lepson/print/imgsel/ImageFinder$Canceller;)Ljava/util/List;

    move-result-object p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 190
    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    return-object p1

    :catchall_0
    move-exception p1

    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    throw p1

    :catch_0
    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    return-object v0

    :catch_1
    return-object v0
.end method

.method public getFolderPhotoList(Lepson/print/imgsel/ImageFinder$Canceller;Landroid/content/ContentResolver;)Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lepson/print/imgsel/ImageFinder$Canceller;",
            "Landroid/content/ContentResolver;",
            ")",
            "Ljava/util/Collection<",
            "Lepson/print/imgsel/ImageFolderInfo;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 46
    :try_start_0
    invoke-direct {p0, p2}, Lepson/print/imgsel/MediaStoreImageFinder;->myQuery(Landroid/content/ContentResolver;)Landroid/database/Cursor;

    move-result-object p2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-nez p2, :cond_0

    return-object v0

    .line 58
    :cond_0
    :try_start_1
    invoke-direct {p0, p2, p1}, Lepson/print/imgsel/MediaStoreImageFinder;->getImageDirInfo(Landroid/database/Cursor;Lepson/print/imgsel/ImageFinder$Canceller;)Ljava/util/List;

    move-result-object p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 62
    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    return-object p1

    :catchall_0
    move-exception p1

    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    throw p1

    :catch_0
    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    return-object v0

    :catch_1
    return-object v0
.end method

.method protected abstract myCheckFile(Ljava/io/File;)Z
.end method

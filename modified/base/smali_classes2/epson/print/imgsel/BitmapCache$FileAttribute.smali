.class Lepson/print/imgsel/BitmapCache$FileAttribute;
.super Ljava/lang/Object;
.source "BitmapCache.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/imgsel/BitmapCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FileAttribute"
.end annotation


# instance fields
.field public mFileSize:J

.field public mModifiedTime:J


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 2

    .line 475
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez p1, :cond_0

    return-void

    .line 480
    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v0

    iput-wide v0, p0, Lepson/print/imgsel/BitmapCache$FileAttribute;->mFileSize:J

    .line 481
    invoke-virtual {p1}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    iput-wide v0, p0, Lepson/print/imgsel/BitmapCache$FileAttribute;->mModifiedTime:J

    return-void
.end method


# virtual methods
.method public isSameAttribute(JJ)Z
    .locals 3

    .line 486
    iget-wide v0, p0, Lepson/print/imgsel/BitmapCache$FileAttribute;->mFileSize:J

    cmp-long v2, v0, p1

    if-nez v2, :cond_0

    iget-wide p1, p0, Lepson/print/imgsel/BitmapCache$FileAttribute;->mModifiedTime:J

    cmp-long v0, p1, p3

    if-nez v0, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public isSameAttribute(Ljava/io/File;)Z
    .locals 4

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return p1

    .line 500
    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v0

    invoke-virtual {p1}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    invoke-virtual {p0, v0, v1, v2, v3}, Lepson/print/imgsel/BitmapCache$FileAttribute;->isSameAttribute(JJ)Z

    move-result p1

    return p1
.end method

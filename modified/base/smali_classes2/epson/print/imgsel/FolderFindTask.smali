.class Lepson/print/imgsel/FolderFindTask;
.super Landroid/os/AsyncTask;
.source "FolderFindTask.java"

# interfaces
.implements Lepson/print/imgsel/ImageFinder$Canceller;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Lepson/print/imgsel/ImageFinder;",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        ">;",
        "Lepson/print/imgsel/ImageFinder$Canceller;"
    }
.end annotation


# instance fields
.field private mApplicationContext:Landroid/content/Context;

.field private mFragmentReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lepson/print/imgsel/ImageFolderListFragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lepson/print/imgsel/ImageFolderListFragment;)V
    .locals 1

    .line 26
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 27
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lepson/print/imgsel/FolderFindTask;->mFragmentReference:Ljava/lang/ref/WeakReference;

    .line 28
    invoke-virtual {p1}, Lepson/print/imgsel/ImageFolderListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p1

    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lepson/print/imgsel/FolderFindTask;->mApplicationContext:Landroid/content/Context;

    return-void
.end method

.method private localSleep(I)V
    .locals 2

    int-to-long v0, p1

    .line 68
    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 70
    invoke-virtual {p1}, Ljava/lang/InterruptedException;->printStackTrace()V

    :goto_0
    return-void
.end method

.method private taskAddItem(Lepson/print/imgsel/ImageFolderInfo;)V
    .locals 5

    const/4 v0, 0x0

    .line 80
    invoke-virtual {p1, v0}, Lepson/print/imgsel/ImageFolderInfo;->getFileName(I)Ljava/lang/String;

    move-result-object v1

    .line 81
    invoke-virtual {p1, v0}, Lepson/print/imgsel/ImageFolderInfo;->getId(I)J

    move-result-wide v2

    .line 85
    :try_start_0
    iget-object v4, p0, Lepson/print/imgsel/FolderFindTask;->mApplicationContext:Landroid/content/Context;

    .line 86
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 85
    invoke-static {v1, v4, v2, v3}, Lepson/print/imgsel/AltThumbnailCache;->createThumbnail(Ljava/lang/String;Landroid/content/ContentResolver;J)Landroid/graphics/Bitmap;

    move-result-object v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 88
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    const/4 v1, 0x0

    :goto_0
    const/4 v2, 0x2

    .line 90
    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v0

    const/4 p1, 0x1

    aput-object v1, v2, p1

    invoke-virtual {p0, v2}, Lepson/print/imgsel/FolderFindTask;->publishProgress([Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public checkCanceled()Z
    .locals 1

    .line 108
    invoke-virtual {p0}, Lepson/print/imgsel/FolderFindTask;->isCancelled()Z

    move-result v0

    return v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 20
    check-cast p1, [Lepson/print/imgsel/ImageFinder;

    invoke-virtual {p0, p1}, Lepson/print/imgsel/FolderFindTask;->doInBackground([Lepson/print/imgsel/ImageFinder;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Lepson/print/imgsel/ImageFinder;)Ljava/lang/Void;
    .locals 3

    .line 43
    invoke-virtual {p0}, Lepson/print/imgsel/FolderFindTask;->isCancelled()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_3

    if-eqz p1, :cond_3

    array-length v0, p1

    if-gtz v0, :cond_0

    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    .line 46
    aget-object p1, p1, v0

    .line 49
    iget-object v0, p0, Lepson/print/imgsel/FolderFindTask;->mApplicationContext:Landroid/content/Context;

    .line 51
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 50
    invoke-interface {p1, p0, v0}, Lepson/print/imgsel/ImageFinder;->getFolderPhotoList(Lepson/print/imgsel/ImageFinder$Canceller;Landroid/content/ContentResolver;)Ljava/util/Collection;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 54
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/print/imgsel/ImageFolderInfo;

    .line 55
    invoke-virtual {p0}, Lepson/print/imgsel/FolderFindTask;->isCancelled()Z

    move-result v2

    if-eqz v2, :cond_1

    return-object v1

    .line 58
    :cond_1
    invoke-direct {p0, v0}, Lepson/print/imgsel/FolderFindTask;->taskAddItem(Lepson/print/imgsel/ImageFolderInfo;)V

    goto :goto_0

    :cond_2
    return-object v1

    :cond_3
    :goto_1
    return-object v1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 20
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/print/imgsel/FolderFindTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 0

    .line 34
    iget-object p1, p0, Lepson/print/imgsel/FolderFindTask;->mFragmentReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lepson/print/imgsel/ImageFolderListFragment;

    if-nez p1, :cond_0

    return-void

    .line 38
    :cond_0
    invoke-virtual {p1}, Lepson/print/imgsel/ImageFolderListFragment;->onFolderFindTaskEnd()V

    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Object;)V
    .locals 3

    .line 95
    invoke-virtual {p0}, Lepson/print/imgsel/FolderFindTask;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 98
    :cond_0
    iget-object v0, p0, Lepson/print/imgsel/FolderFindTask;->mFragmentReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/print/imgsel/ImageFolderListFragment;

    if-nez v0, :cond_1

    return-void

    :cond_1
    const/4 v1, 0x0

    .line 102
    aget-object v1, p1, v1

    check-cast v1, Lepson/print/imgsel/ImageFolderInfo;

    const/4 v2, 0x1

    aget-object p1, p1, v2

    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1, p1}, Lepson/print/imgsel/ImageFolderListFragment;->addItem(Lepson/print/imgsel/ImageFolderInfo;Landroid/graphics/Bitmap;)V

    return-void
.end method

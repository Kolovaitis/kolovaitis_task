.class public Lepson/print/imgsel/CameraCopyImageSelectActivity;
.super Lepson/print/imgsel/ImageSelectActivity;
.source "CameraCopyImageSelectActivity.java"


# static fields
.field private static final REQUEST_CODE_IMAGE_PREVIEW:I = 0x68


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Lepson/print/imgsel/ImageSelectActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public getImageFinder()Lepson/print/imgsel/ImageFinder;
    .locals 1

    .line 18
    new-instance v0, Lepson/print/imgsel/PhotoImageFinder;

    invoke-direct {v0}, Lepson/print/imgsel/PhotoImageFinder;-><init>()V

    return-object v0
.end method

.method protected goNext()V
    .locals 4

    .line 39
    invoke-virtual {p0}, Lepson/print/imgsel/CameraCopyImageSelectActivity;->getImageSelector()Lepson/print/imgsel/ImageSelector;

    move-result-object v0

    invoke-virtual {v0}, Lepson/print/imgsel/ImageSelector;->getFileArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 41
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-gtz v1, :cond_0

    return-void

    .line 47
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/epson/cameracopy/ui/ImagePreviewActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "file_name"

    const/4 v3, 0x0

    .line 49
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 48
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/16 v0, 0x68

    .line 50
    invoke-virtual {p0, v1, v0}, Lepson/print/imgsel/CameraCopyImageSelectActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    .line 29
    invoke-super {p0, p1, p2, p3}, Lepson/print/imgsel/ImageSelectActivity;->onActivityResult(IILandroid/content/Intent;)V

    const/16 p2, 0x68

    if-eq p1, p2, :cond_0

    goto :goto_0

    .line 33
    :cond_0
    invoke-virtual {p0}, Lepson/print/imgsel/CameraCopyImageSelectActivity;->finish()V

    :goto_0
    return-void
.end method

.method public singleImageMode()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

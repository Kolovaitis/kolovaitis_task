.class public Lepson/print/imgsel/ImageViewPagerActivity;
.super Lepson/print/ActivityIACommon;
.source "ImageViewPagerActivity.java"

# interfaces
.implements Lepson/print/imgsel/ImageViewBaseFragment$OnFragmentInteractionListener;
.implements Lepson/print/imgsel/LocalAlertDialogFragment$DialogCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/imgsel/ImageViewPagerActivity$PagerAdapter;
    }
.end annotation


# static fields
.field private static final CACHE_DIRECTORY:Ljava/lang/String; = "imgsel_viewpager"

.field public static final DIALOG_CODE_ERROR_FINISH:I = 0x1

.field public static final DIALOG_CODE_NOTIFY:I = 0x2

.field public static final MESSAGE_TYPE_GENERIC:I = 0x0

.field public static final MESSAGE_TYPE_PHOTO_COPY:I = 0x1

.field public static final PARAMS_KEY_IMAGE_LIST:Ljava/lang/String; = "image_file_arraylist"

.field public static final PARAMS_KEY_POSITION:Ljava/lang/String; = "position"

.field public static final PARAMS_KEY_SELECTOR:Ljava/lang/String; = "selector"

.field public static final PARAMS_MESSAGE_TYPE:Ljava/lang/String; = "message_type"

.field public static final PARAMS_SINGLE_FILE_MODE:Ljava/lang/String; = "single_file_mode"

.field public static final RESULT_CODE_GO_NEXT:I = 0xa

.field public static final RETURN_KEY_SELECTOR:Ljava/lang/String; = "return_selector"

.field private static final STATE_SELECTED_FILE_LIST:Ljava/lang/String; = "selected_file_list"

.field private static sParamFileList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mFileList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mFirstPosition:I

.field private mImageResizer:Lepson/print/imgsel/ImageResizer;

.field private mImageSelector:Lepson/print/imgsel/ImageSelector;

.field private mMessageType:I

.field private mSingleFileMode:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 61
    invoke-direct {p0}, Lepson/print/ActivityIACommon;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lepson/print/imgsel/ImageViewPagerActivity;)Z
    .locals 0

    .line 61
    iget-boolean p0, p0, Lepson/print/imgsel/ImageViewPagerActivity;->mSingleFileMode:Z

    return p0
.end method

.method static synthetic access$100(Lepson/print/imgsel/ImageViewPagerActivity;)I
    .locals 0

    .line 61
    iget p0, p0, Lepson/print/imgsel/ImageViewPagerActivity;->mMessageType:I

    return p0
.end method

.method private getCacheDirectory()Ljava/io/File;
    .locals 3

    .line 129
    invoke-virtual {p0}, Lepson/print/imgsel/ImageViewPagerActivity;->getExternalCacheDir()Ljava/io/File;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 134
    :cond_0
    new-instance v1, Ljava/io/File;

    const-string v2, "imgsel_viewpager"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v1
.end method

.method private getFullscreenImageSize()I
    .locals 5

    .line 144
    invoke-direct {p0}, Lepson/print/imgsel/ImageViewPagerActivity;->getScreenSize()[I

    move-result-object v0

    const/4 v1, 0x0

    .line 145
    aget v2, v0, v1

    const/4 v3, 0x1

    aget v4, v0, v3

    if-le v2, v4, :cond_0

    aget v0, v0, v1

    goto :goto_0

    :cond_0
    aget v0, v0, v3

    .line 148
    :goto_0
    div-int/lit8 v0, v0, 0x2

    return v0
.end method

.method private getParamsFromIntent(Landroid/content/Intent;Landroid/os/Bundle;)V
    .locals 3

    const/4 v0, 0x0

    .line 177
    iput-object v0, p0, Lepson/print/imgsel/ImageViewPagerActivity;->mFileList:Ljava/util/ArrayList;

    if-nez p1, :cond_0

    return-void

    :cond_0
    const-string v1, "selector"

    .line 181
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    if-eqz p2, :cond_1

    const-string v2, "selected_file_list"

    .line 183
    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_1

    move-object v1, v2

    .line 188
    :cond_1
    new-instance v2, Lepson/print/imgsel/ImageSelector;

    invoke-direct {v2, v1}, Lepson/print/imgsel/ImageSelector;-><init>(Ljava/util/ArrayList;)V

    iput-object v2, p0, Lepson/print/imgsel/ImageViewPagerActivity;->mImageSelector:Lepson/print/imgsel/ImageSelector;

    if-eqz p2, :cond_2

    return-void

    .line 193
    :cond_2
    sget-object p2, Lepson/print/imgsel/ImageViewPagerActivity;->sParamFileList:Ljava/util/ArrayList;

    iput-object p2, p0, Lepson/print/imgsel/ImageViewPagerActivity;->mFileList:Ljava/util/ArrayList;

    .line 195
    sput-object v0, Lepson/print/imgsel/ImageViewPagerActivity;->sParamFileList:Ljava/util/ArrayList;

    const-string p2, "position"

    const/4 v0, 0x0

    .line 197
    invoke-virtual {p1, p2, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p2

    iput p2, p0, Lepson/print/imgsel/ImageViewPagerActivity;->mFirstPosition:I

    const-string p2, "single_file_mode"

    .line 198
    invoke-virtual {p1, p2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p2

    iput-boolean p2, p0, Lepson/print/imgsel/ImageViewPagerActivity;->mSingleFileMode:Z

    const-string p2, "message_type"

    .line 199
    invoke-virtual {p1, p2, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p1

    iput p1, p0, Lepson/print/imgsel/ImageViewPagerActivity;->mMessageType:I

    return-void
.end method

.method private getScreenSize()[I
    .locals 4

    const/4 v0, 0x2

    .line 158
    new-array v0, v0, [I

    fill-array-data v0, :array_0

    .line 160
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 161
    invoke-virtual {p0}, Lepson/print/imgsel/ImageViewPagerActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 162
    iget v2, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    const/4 v3, 0x0

    aput v2, v0, v3

    .line 163
    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    const/4 v2, 0x1

    aput v1, v0, v2

    return-object v0

    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method private returnAndGoNext()V
    .locals 3

    .line 280
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 281
    iget-object v1, p0, Lepson/print/imgsel/ImageViewPagerActivity;->mImageSelector:Lepson/print/imgsel/ImageSelector;

    invoke-virtual {v1}, Lepson/print/imgsel/ImageSelector;->getFileArrayList()Ljava/util/ArrayList;

    move-result-object v1

    const-string v2, "return_selector"

    .line 282
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    const/16 v1, 0xa

    .line 283
    invoke-virtual {p0, v1, v0}, Lepson/print/imgsel/ImageViewPagerActivity;->setResult(ILandroid/content/Intent;)V

    .line 284
    invoke-virtual {p0}, Lepson/print/imgsel/ImageViewPagerActivity;->finish()V

    return-void
.end method

.method private returnAndRefreshSelection()V
    .locals 3

    .line 292
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "return_selector"

    .line 293
    iget-object v2, p0, Lepson/print/imgsel/ImageViewPagerActivity;->mImageSelector:Lepson/print/imgsel/ImageSelector;

    invoke-virtual {v2}, Lepson/print/imgsel/ImageSelector;->getFileArrayList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    const/4 v1, -0x1

    .line 294
    invoke-virtual {p0, v1, v0}, Lepson/print/imgsel/ImageViewPagerActivity;->setResult(ILandroid/content/Intent;)V

    .line 295
    invoke-virtual {p0}, Lepson/print/imgsel/ImageViewPagerActivity;->finish()V

    return-void
.end method

.method public static setFileList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 360
    sput-object p0, Lepson/print/imgsel/ImageViewPagerActivity;->sParamFileList:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public getImageResizer()Lepson/print/imgsel/ImageResizer;
    .locals 1

    .line 316
    iget-object v0, p0, Lepson/print/imgsel/ImageViewPagerActivity;->mImageResizer:Lepson/print/imgsel/ImageResizer;

    return-object v0
.end method

.method public getSelector()Lepson/print/imgsel/ImageSelector;
    .locals 1

    .line 221
    iget-object v0, p0, Lepson/print/imgsel/ImageViewPagerActivity;->mImageSelector:Lepson/print/imgsel/ImageSelector;

    return-object v0
.end method

.method public onBackPressed()V
    .locals 0

    .line 205
    invoke-direct {p0}, Lepson/print/imgsel/ImageViewPagerActivity;->returnAndRefreshSelection()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .line 105
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f0a003d

    .line 106
    invoke-virtual {p0, v0}, Lepson/print/imgsel/ImageViewPagerActivity;->setContentView(I)V

    const-string v0, ""

    const/4 v1, 0x1

    .line 107
    invoke-virtual {p0, v0, v1}, Lepson/print/imgsel/ImageViewPagerActivity;->setActionBar(Ljava/lang/String;Z)V

    .line 109
    invoke-virtual {p0}, Lepson/print/imgsel/ImageViewPagerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lepson/print/imgsel/ImageViewPagerActivity;->getParamsFromIntent(Landroid/content/Intent;Landroid/os/Bundle;)V

    .line 110
    iget-object p1, p0, Lepson/print/imgsel/ImageViewPagerActivity;->mFileList:Ljava/util/ArrayList;

    if-nez p1, :cond_0

    .line 112
    invoke-direct {p0}, Lepson/print/imgsel/ImageViewPagerActivity;->returnAndRefreshSelection()V

    return-void

    .line 116
    :cond_0
    invoke-direct {p0}, Lepson/print/imgsel/ImageViewPagerActivity;->getFullscreenImageSize()I

    move-result p1

    .line 117
    invoke-direct {p0}, Lepson/print/imgsel/ImageViewPagerActivity;->getCacheDirectory()Ljava/io/File;

    move-result-object v0

    .line 116
    invoke-static {p1, v0}, Lepson/print/imgsel/ImageResizer;->getInstance(ILjava/io/File;)Lepson/print/imgsel/ImageResizer;

    move-result-object p1

    iput-object p1, p0, Lepson/print/imgsel/ImageViewPagerActivity;->mImageResizer:Lepson/print/imgsel/ImageResizer;

    const p1, 0x7f080249

    .line 119
    invoke-virtual {p0, p1}, Lepson/print/imgsel/ImageViewPagerActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/support/v4/view/ViewPager;

    const/4 v0, 0x2

    .line 120
    invoke-virtual {p1, v0}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 122
    new-instance v0, Lepson/print/imgsel/ImageViewPagerActivity$PagerAdapter;

    invoke-virtual {p0}, Lepson/print/imgsel/ImageViewPagerActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    iget-object v2, p0, Lepson/print/imgsel/ImageViewPagerActivity;->mFileList:Ljava/util/ArrayList;

    invoke-direct {v0, p0, v1, v2}, Lepson/print/imgsel/ImageViewPagerActivity$PagerAdapter;-><init>(Lepson/print/imgsel/ImageViewPagerActivity;Landroid/support/v4/app/FragmentManager;Ljava/util/ArrayList;)V

    .line 123
    invoke-virtual {p1, v0}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 124
    iget v0, p0, Lepson/print/imgsel/ImageViewPagerActivity;->mFirstPosition:I

    invoke-virtual {p1, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 5

    .line 237
    invoke-virtual {p0}, Lepson/print/imgsel/ImageViewPagerActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0b0005

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const v0, 0x7f080044

    .line 238
    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object p1

    .line 241
    iget-object v0, p0, Lepson/print/imgsel/ImageViewPagerActivity;->mImageSelector:Lepson/print/imgsel/ImageSelector;

    invoke-virtual {v0}, Lepson/print/imgsel/ImageSelector;->selectedNumber()I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-lez v0, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    .line 245
    :goto_0
    invoke-interface {p1, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 247
    iget-boolean v3, p0, Lepson/print/imgsel/ImageViewPagerActivity;->mSingleFileMode:Z

    if-eqz v3, :cond_1

    const-string v0, ""

    .line 248
    invoke-virtual {p0, v0}, Lepson/print/imgsel/ImageViewPagerActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 249
    invoke-interface {p1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1

    .line 251
    :cond_1
    invoke-virtual {p0}, Lepson/print/imgsel/ImageViewPagerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v3, 0x7f0e042c

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v2

    invoke-virtual {p1, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 252
    invoke-virtual {p0, p1}, Lepson/print/imgsel/ImageViewPagerActivity;->setTitle(Ljava/lang/CharSequence;)V

    :goto_1
    return v1
.end method

.method public onDialogCallback(I)V
    .locals 1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    return-void

    .line 323
    :cond_0
    invoke-virtual {p0}, Lepson/print/imgsel/ImageViewPagerActivity;->finish()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .line 263
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f080044

    if-ne v0, v1, :cond_0

    .line 267
    invoke-direct {p0}, Lepson/print/imgsel/ImageViewPagerActivity;->returnAndGoNext()V

    const/4 p1, 0x1

    return p1

    .line 272
    :cond_0
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1
.end method

.method protected onPause()V
    .locals 2

    .line 307
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onPause()V

    .line 308
    iget-object v0, p0, Lepson/print/imgsel/ImageViewPagerActivity;->mImageResizer:Lepson/print/imgsel/ImageResizer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lepson/print/imgsel/ImageResizer;->setActivityForeground(Z)V

    return-void
.end method

.method protected onResume()V
    .locals 2

    .line 301
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onResume()V

    .line 302
    iget-object v0, p0, Lepson/print/imgsel/ImageViewPagerActivity;->mImageResizer:Lepson/print/imgsel/ImageResizer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lepson/print/imgsel/ImageResizer;->setActivityForeground(Z)V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "selected_file_list"

    .line 215
    iget-object v1, p0, Lepson/print/imgsel/ImageViewPagerActivity;->mImageSelector:Lepson/print/imgsel/ImageSelector;

    invoke-virtual {v1}, Lepson/print/imgsel/ImageSelector;->getFileArrayList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 216
    invoke-super {p0, p1}, Lepson/print/ActivityIACommon;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method public onSingleModeItemSelected()V
    .locals 0

    .line 231
    invoke-direct {p0}, Lepson/print/imgsel/ImageViewPagerActivity;->returnAndGoNext()V

    return-void
.end method

.method protected onStop()V
    .locals 0

    .line 210
    invoke-super {p0}, Lepson/print/ActivityIACommon;->onStop()V

    return-void
.end method

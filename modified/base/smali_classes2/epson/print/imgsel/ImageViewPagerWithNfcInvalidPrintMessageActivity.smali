.class public Lepson/print/imgsel/ImageViewPagerWithNfcInvalidPrintMessageActivity;
.super Lepson/print/imgsel/ImageViewPagerActivity;
.source "ImageViewPagerWithNfcInvalidPrintMessageActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Lepson/print/imgsel/ImageViewPagerActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0

    .line 31
    invoke-super {p0, p1}, Lepson/print/imgsel/ImageViewPagerActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 33
    invoke-static {p0, p1}, Lepson/print/Util/CommonMessage;->showInvalidPrintMessageIfEpsonNfcPrinter(Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method

.method protected onPause()V
    .locals 0

    .line 16
    invoke-super {p0}, Lepson/print/imgsel/ImageViewPagerActivity;->onPause()V

    .line 18
    invoke-static {p0}, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils;->disableForegroundDispatch(Landroid/app/Activity;)V

    return-void
.end method

.method protected onResume()V
    .locals 2

    .line 23
    invoke-super {p0}, Lepson/print/imgsel/ImageViewPagerActivity;->onResume()V

    const/4 v0, 0x0

    .line 26
    move-object v1, v0

    check-cast v1, [[Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/epson/mobilephone/common/wifidirect/NfcTagUtils;->enableForegroundDispatch(Landroid/app/Activity;[Landroid/content/IntentFilter;[[Ljava/lang/String;)V

    return-void
.end method

.class public interface abstract Lepson/print/imgsel/ImageFinder;
.super Ljava/lang/Object;
.source "ImageFinder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/imgsel/ImageFinder$Canceller;
    }
.end annotation


# virtual methods
.method public abstract findImageInDirectory(Ljava/lang/String;Landroid/content/ContentResolver;Lepson/print/imgsel/ImageFinder$Canceller;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/content/ContentResolver;",
            "Lepson/print/imgsel/ImageFinder$Canceller;",
            ")",
            "Ljava/util/List<",
            "Lepson/print/imgsel/ImageFileInfo;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getFolderPhotoList(Lepson/print/imgsel/ImageFinder$Canceller;Landroid/content/ContentResolver;)Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lepson/print/imgsel/ImageFinder$Canceller;",
            "Landroid/content/ContentResolver;",
            ")",
            "Ljava/util/Collection<",
            "Lepson/print/imgsel/ImageFolderInfo;",
            ">;"
        }
    .end annotation
.end method

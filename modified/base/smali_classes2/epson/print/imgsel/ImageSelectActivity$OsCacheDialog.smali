.class public Lepson/print/imgsel/ImageSelectActivity$OsCacheDialog;
.super Landroid/support/v4/app/DialogFragment;
.source "ImageSelectActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/imgsel/ImageSelectActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "OsCacheDialog"
.end annotation


# instance fields
.field private mUseMediaStoreThumbnail:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 373
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$002(Lepson/print/imgsel/ImageSelectActivity$OsCacheDialog;Z)Z
    .locals 0

    .line 373
    iput-boolean p1, p0, Lepson/print/imgsel/ImageSelectActivity$OsCacheDialog;->mUseMediaStoreThumbnail:Z

    return p1
.end method

.method static synthetic access$100(Lepson/print/imgsel/ImageSelectActivity$OsCacheDialog;)V
    .locals 0

    .line 373
    invoke-direct {p0}, Lepson/print/imgsel/ImageSelectActivity$OsCacheDialog;->onOkPressed()V

    return-void
.end method

.method public static newInstance()Lepson/print/imgsel/ImageSelectActivity$OsCacheDialog;
    .locals 1

    .line 377
    new-instance v0, Lepson/print/imgsel/ImageSelectActivity$OsCacheDialog;

    invoke-direct {v0}, Lepson/print/imgsel/ImageSelectActivity$OsCacheDialog;-><init>()V

    return-object v0
.end method

.method private onOkPressed()V
    .locals 2

    .line 423
    invoke-virtual {p0}, Lepson/print/imgsel/ImageSelectActivity$OsCacheDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lepson/print/imgsel/ImageSelectActivity;

    if-nez v0, :cond_0

    return-void

    .line 427
    :cond_0
    iget-boolean v1, p0, Lepson/print/imgsel/ImageSelectActivity$OsCacheDialog;->mUseMediaStoreThumbnail:Z

    invoke-virtual {v0, v1}, Lepson/print/imgsel/ImageSelectActivity;->changeThumbnailCreateMethod(Z)V

    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    const/4 p1, 0x1

    .line 382
    new-array v0, p1, [Ljava/lang/CharSequence;

    .line 383
    invoke-virtual {p0}, Lepson/print/imgsel/ImageSelectActivity$OsCacheDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0e0518

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 385
    new-array p1, p1, [Z

    .line 387
    invoke-static {}, Lepson/print/imgsel/AltThumbnailCache;->isUseMediaStoreThumbnail()Z

    move-result v1

    iput-boolean v1, p0, Lepson/print/imgsel/ImageSelectActivity$OsCacheDialog;->mUseMediaStoreThumbnail:Z

    .line 388
    iget-boolean v1, p0, Lepson/print/imgsel/ImageSelectActivity$OsCacheDialog;->mUseMediaStoreThumbnail:Z

    aput-boolean v1, p1, v2

    .line 390
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lepson/print/imgsel/ImageSelectActivity$OsCacheDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 391
    new-instance v3, Lepson/print/imgsel/ImageSelectActivity$OsCacheDialog$3;

    invoke-direct {v3, p0}, Lepson/print/imgsel/ImageSelectActivity$OsCacheDialog$3;-><init>(Lepson/print/imgsel/ImageSelectActivity$OsCacheDialog;)V

    const v4, 0x7f0e03fe

    invoke-virtual {v1, v4, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lepson/print/imgsel/ImageSelectActivity$OsCacheDialog$2;

    invoke-direct {v4, p0}, Lepson/print/imgsel/ImageSelectActivity$OsCacheDialog$2;-><init>(Lepson/print/imgsel/ImageSelectActivity$OsCacheDialog;)V

    const v5, 0x7f0e0476

    .line 397
    invoke-virtual {v3, v5, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lepson/print/imgsel/ImageSelectActivity$OsCacheDialog$1;

    invoke-direct {v4, p0}, Lepson/print/imgsel/ImageSelectActivity$OsCacheDialog$1;-><init>(Lepson/print/imgsel/ImageSelectActivity$OsCacheDialog;)V

    .line 403
    invoke-virtual {v3, v0, p1, v4}, Landroid/app/AlertDialog$Builder;->setMultiChoiceItems([Ljava/lang/CharSequence;[ZLandroid/content/DialogInterface$OnMultiChoiceClickListener;)Landroid/app/AlertDialog$Builder;

    .line 414
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 415
    invoke-virtual {p1, v2}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    return-object p1
.end method

.class Lepson/print/imgsel/ImageViewMultiSelectFragment$MyToggleButton;
.super Ljava/lang/Object;
.source "ImageViewMultiSelectFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/imgsel/ImageViewMultiSelectFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "MyToggleButton"
.end annotation


# instance fields
.field private mImageViewMultiSelectFragment:Lepson/print/imgsel/ImageViewMultiSelectFragment;

.field private mMyToggleChecked:Z

.field private mToggleImage:Landroid/widget/ImageView;

.field private mToggleText:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lepson/print/imgsel/ImageViewMultiSelectFragment;Landroid/view/View;Z)V
    .locals 0

    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    iput-object p1, p0, Lepson/print/imgsel/ImageViewMultiSelectFragment$MyToggleButton;->mImageViewMultiSelectFragment:Lepson/print/imgsel/ImageViewMultiSelectFragment;

    .line 102
    invoke-virtual {p0, p2, p3}, Lepson/print/imgsel/ImageViewMultiSelectFragment$MyToggleButton;->init(Landroid/view/View;Z)V

    return-void
.end method

.method static synthetic access$000(Lepson/print/imgsel/ImageViewMultiSelectFragment$MyToggleButton;)V
    .locals 0

    .line 93
    invoke-direct {p0}, Lepson/print/imgsel/ImageViewMultiSelectFragment$MyToggleButton;->toggleCheckStatus()V

    return-void
.end method

.method private execCallback()V
    .locals 2

    .line 157
    iget-object v0, p0, Lepson/print/imgsel/ImageViewMultiSelectFragment$MyToggleButton;->mImageViewMultiSelectFragment:Lepson/print/imgsel/ImageViewMultiSelectFragment;

    if-eqz v0, :cond_0

    .line 158
    iget-boolean v1, p0, Lepson/print/imgsel/ImageViewMultiSelectFragment$MyToggleButton;->mMyToggleChecked:Z

    invoke-static {v0, v1}, Lepson/print/imgsel/ImageViewMultiSelectFragment;->access$100(Lepson/print/imgsel/ImageViewMultiSelectFragment;Z)V

    :cond_0
    return-void
.end method

.method private setMyChecked(Z)V
    .locals 1

    .line 144
    iget-boolean v0, p0, Lepson/print/imgsel/ImageViewMultiSelectFragment$MyToggleButton;->mMyToggleChecked:Z

    if-ne p1, v0, :cond_0

    return-void

    .line 148
    :cond_0
    invoke-virtual {p0, p1}, Lepson/print/imgsel/ImageViewMultiSelectFragment$MyToggleButton;->forceSetChecked(Z)V

    .line 150
    invoke-direct {p0}, Lepson/print/imgsel/ImageViewMultiSelectFragment$MyToggleButton;->execCallback()V

    return-void
.end method

.method private syncTextState()V
    .locals 2

    .line 164
    iget-boolean v0, p0, Lepson/print/imgsel/ImageViewMultiSelectFragment$MyToggleButton;->mMyToggleChecked:Z

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Lepson/print/imgsel/ImageViewMultiSelectFragment$MyToggleButton;->mToggleText:Landroid/widget/TextView;

    const v1, 0x7f0e0396

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 167
    :cond_0
    iget-object v0, p0, Lepson/print/imgsel/ImageViewMultiSelectFragment$MyToggleButton;->mToggleText:Landroid/widget/TextView;

    const v1, 0x7f0e0395

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    return-void
.end method

.method private toggleCheckStatus()V
    .locals 1

    .line 123
    iget-object v0, p0, Lepson/print/imgsel/ImageViewMultiSelectFragment$MyToggleButton;->mToggleImage:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->isSelected()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    .line 124
    invoke-direct {p0, v0}, Lepson/print/imgsel/ImageViewMultiSelectFragment$MyToggleButton;->setMyChecked(Z)V

    return-void
.end method


# virtual methods
.method public forceSetChecked(Z)V
    .locals 1

    .line 133
    iput-boolean p1, p0, Lepson/print/imgsel/ImageViewMultiSelectFragment$MyToggleButton;->mMyToggleChecked:Z

    .line 134
    invoke-direct {p0}, Lepson/print/imgsel/ImageViewMultiSelectFragment$MyToggleButton;->syncTextState()V

    .line 136
    iget-object p1, p0, Lepson/print/imgsel/ImageViewMultiSelectFragment$MyToggleButton;->mToggleImage:Landroid/widget/ImageView;

    iget-boolean v0, p0, Lepson/print/imgsel/ImageViewMultiSelectFragment$MyToggleButton;->mMyToggleChecked:Z

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setSelected(Z)V

    return-void
.end method

.method public init(Landroid/view/View;Z)V
    .locals 2

    const v0, 0x7f080220

    .line 106
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lepson/print/imgsel/ImageViewMultiSelectFragment$MyToggleButton;->mToggleImage:Landroid/widget/ImageView;

    .line 107
    iget-object v0, p0, Lepson/print/imgsel/ImageViewMultiSelectFragment$MyToggleButton;->mToggleImage:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setClickable(Z)V

    const v0, 0x7f080221

    .line 108
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lepson/print/imgsel/ImageViewMultiSelectFragment$MyToggleButton;->mToggleText:Landroid/widget/TextView;

    .line 109
    iget-object v0, p0, Lepson/print/imgsel/ImageViewMultiSelectFragment$MyToggleButton;->mToggleText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setClickable(Z)V

    .line 111
    new-instance v0, Lepson/print/imgsel/ImageViewMultiSelectFragment$MyToggleButton$1;

    invoke-direct {v0, p0}, Lepson/print/imgsel/ImageViewMultiSelectFragment$MyToggleButton$1;-><init>(Lepson/print/imgsel/ImageViewMultiSelectFragment$MyToggleButton;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 118
    invoke-virtual {p0, p2}, Lepson/print/imgsel/ImageViewMultiSelectFragment$MyToggleButton;->forceSetChecked(Z)V

    return-void
.end method

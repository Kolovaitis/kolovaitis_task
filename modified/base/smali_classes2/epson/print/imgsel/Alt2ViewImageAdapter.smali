.class public Lepson/print/imgsel/Alt2ViewImageAdapter;
.super Landroid/widget/BaseAdapter;
.source "Alt2ViewImageAdapter.java"

# interfaces
.implements Lepson/print/CommonDefine;


# static fields
.field private static final TAG:Ljava/lang/String; = "ViewImageAdapter"


# instance fields
.field private final mContext:Landroid/content/Context;

.field mFragmentReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lepson/print/imgsel/ImageGridFragment;",
            ">;"
        }
    .end annotation
.end field

.field private mImageItemList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lepson/print/ImageItem;",
            ">;"
        }
    .end annotation
.end field

.field private mLayoutInflater:Landroid/view/LayoutInflater;

.field private mLayoutParams:Landroid/widget/AbsListView$LayoutParams;

.field mSelectPictureBitmap:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Lepson/print/imgsel/ImageGridFragment;)V
    .locals 3

    .line 47
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lepson/print/imgsel/Alt2ViewImageAdapter;->mImageItemList:Ljava/util/List;

    const/4 v0, 0x0

    .line 41
    iput-object v0, p0, Lepson/print/imgsel/Alt2ViewImageAdapter;->mSelectPictureBitmap:Landroid/graphics/Bitmap;

    .line 48
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lepson/print/imgsel/Alt2ViewImageAdapter;->mFragmentReference:Ljava/lang/ref/WeakReference;

    .line 50
    invoke-virtual {p1}, Lepson/print/imgsel/ImageGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p1

    iput-object p1, p0, Lepson/print/imgsel/Alt2ViewImageAdapter;->mContext:Landroid/content/Context;

    .line 51
    iget-object p1, p0, Lepson/print/imgsel/Alt2ViewImageAdapter;->mContext:Landroid/content/Context;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    iput-object p1, p0, Lepson/print/imgsel/Alt2ViewImageAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 53
    new-instance p1, Landroid/widget/AbsListView$LayoutParams;

    const/4 v0, -0x1

    invoke-direct {p1, v0, v0}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    iput-object p1, p0, Lepson/print/imgsel/Alt2ViewImageAdapter;->mLayoutParams:Landroid/widget/AbsListView$LayoutParams;

    .line 57
    new-instance p1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {p1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/4 v0, 0x1

    .line 58
    iput-boolean v0, p1, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    const/4 v0, 0x0

    .line 59
    iput-boolean v0, p1, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    .line 61
    :try_start_0
    iget-object v0, p0, Lepson/print/imgsel/Alt2ViewImageAdapter;->mContext:Landroid/content/Context;

    .line 62
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070142

    .line 61
    invoke-static {v0, v1, p1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object p1

    iput-object p1, p0, Lepson/print/imgsel/Alt2ViewImageAdapter;->mSelectPictureBitmap:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const-string v0, "ViewImageAdapter"

    .line 65
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BitmapFactory.decodeResource Error "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/OutOfMemoryError;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method


# virtual methods
.method public add(Lepson/print/ImageItem;)V
    .locals 1

    .line 165
    iget-object v0, p0, Lepson/print/imgsel/Alt2ViewImageAdapter;->mImageItemList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public clearImageItem()V
    .locals 1

    .line 170
    iget-object v0, p0, Lepson/print/imgsel/Alt2ViewImageAdapter;->mImageItemList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method public getCount()I
    .locals 1

    .line 143
    iget-object v0, p0, Lepson/print/imgsel/Alt2ViewImageAdapter;->mImageItemList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getFileList()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 189
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 191
    iget-object v1, p0, Lepson/print/imgsel/Alt2ViewImageAdapter;->mImageItemList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lepson/print/ImageItem;

    .line 192
    invoke-virtual {v2}, Lepson/print/ImageItem;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public getImageItem(I)Lepson/print/ImageItem;
    .locals 1

    .line 157
    iget-object v0, p0, Lepson/print/imgsel/Alt2ViewImageAdapter;->mImageItemList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lepson/print/ImageItem;

    return-object p1
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .line 148
    iget-object v0, p0, Lepson/print/imgsel/Alt2ViewImageAdapter;->mImageItemList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getPosition(Lepson/print/ImageItem;)I
    .locals 1

    .line 161
    iget-object v0, p0, Lepson/print/imgsel/Alt2ViewImageAdapter;->mImageItemList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result p1

    return p1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    const/4 v0, 0x0

    if-nez p2, :cond_0

    .line 85
    iget-object p2, p0, Lepson/print/imgsel/Alt2ViewImageAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f0a0073

    invoke-virtual {p2, v1, p3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 86
    iget-object p3, p0, Lepson/print/imgsel/Alt2ViewImageAdapter;->mLayoutParams:Landroid/widget/AbsListView$LayoutParams;

    invoke-virtual {p2, p3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    const p3, 0x7f080194

    .line 89
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/ImageView;

    .line 95
    iget-object v1, p0, Lepson/print/imgsel/Alt2ViewImageAdapter;->mImageItemList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x0

    if-ltz v1, :cond_1

    iget-object v1, p0, Lepson/print/imgsel/Alt2ViewImageAdapter;->mImageItemList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, p1, :cond_1

    .line 97
    :try_start_0
    iget-object v1, p0, Lepson/print/imgsel/Alt2ViewImageAdapter;->mImageItemList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lepson/print/ImageItem;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 99
    invoke-virtual {p1}, Ljava/lang/NullPointerException;->printStackTrace()V

    return-object p2

    :cond_1
    move-object p1, v2

    :goto_0
    if-nez p1, :cond_2

    return-object p2

    .line 108
    :cond_2
    :try_start_1
    invoke-virtual {p1}, Lepson/print/ImageItem;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 109
    invoke-virtual {p1}, Lepson/print/ImageItem;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {p3, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1

    .line 112
    :cond_3
    invoke-virtual {p3, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    const v1, -0x333334

    .line 113
    invoke-virtual {p3, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    const-string p3, "null"

    const-string v1, "bitmap is null"

    .line 114
    invoke-static {p3, v1}, Lepson/print/Util/EPLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    iget-object p3, p0, Lepson/print/imgsel/Alt2ViewImageAdapter;->mFragmentReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {p3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lepson/print/imgsel/ImageGridFragment;

    if-eqz p3, :cond_4

    .line 119
    invoke-virtual {p3, p1}, Lepson/print/imgsel/ImageGridFragment;->changeThumnailPriority(Lepson/print/ImageItem;)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception p3

    const-string v1, "null pointer Exception"

    const-string v2, "line 61 <> View Image Adapter"

    .line 124
    invoke-static {v1, v2}, Lepson/print/Util/EPLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    invoke-virtual {p3}, Ljava/lang/NullPointerException;->printStackTrace()V

    :cond_4
    :goto_1
    const p3, 0x7f0800bb

    .line 131
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/ImageView;

    const/4 v1, 0x4

    .line 133
    invoke-virtual {p1}, Lepson/print/ImageItem;->getSelected()I

    move-result p1

    if-eqz p1, :cond_5

    goto :goto_2

    :cond_5
    const/4 v0, 0x4

    .line 136
    :goto_2
    invoke-virtual {p3, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    return-object p2
.end method

.method public recycleBitmap()V
    .locals 2

    .line 178
    iget-object v0, p0, Lepson/print/imgsel/Alt2ViewImageAdapter;->mImageItemList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lepson/print/ImageItem;

    .line 179
    invoke-virtual {v1}, Lepson/print/ImageItem;->release()V

    goto :goto_0

    .line 181
    :cond_0
    iget-object v0, p0, Lepson/print/imgsel/Alt2ViewImageAdapter;->mImageItemList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method public setHeight(I)V
    .locals 2

    .line 73
    new-instance v0, Landroid/widget/AbsListView$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, p1}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lepson/print/imgsel/Alt2ViewImageAdapter;->mLayoutParams:Landroid/widget/AbsListView$LayoutParams;

    return-void
.end method

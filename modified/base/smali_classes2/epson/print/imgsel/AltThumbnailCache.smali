.class public Lepson/print/imgsel/AltThumbnailCache;
.super Ljava/io/File;
.source "AltThumbnailCache.java"


# static fields
.field public static final CACHE_DIR_NAME:Ljava/lang/String; = "thumbnail"

.field private static final DEFAULT_USE_MEDIASTORE_THUMBNAIL:Z = true

.field private static final TAG:Ljava/lang/String; = "ThumbnailCache"

.field private static final TARGET_SIZE_MICRO_THUMBNAIL:I = 0x60

.field static bUseMediaStoreThumbnail:Z = true

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field modified:J

.field realObject:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 4

    .line 126
    invoke-direct {p0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-wide/16 v0, -0x1

    .line 62
    iput-wide v0, p0, Lepson/print/imgsel/AltThumbnailCache;->modified:J

    const/4 p1, 0x0

    .line 63
    iput-object p1, p0, Lepson/print/imgsel/AltThumbnailCache;->realObject:Ljava/io/File;

    .line 128
    invoke-virtual {p0}, Lepson/print/imgsel/AltThumbnailCache;->exists()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 129
    invoke-virtual {p0}, Lepson/print/imgsel/AltThumbnailCache;->isDirectory()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 131
    invoke-virtual {p0}, Lepson/print/imgsel/AltThumbnailCache;->listFiles()[Ljava/io/File;

    move-result-object p1

    .line 132
    array-length v0, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_2

    aget-object v2, p1, v1

    .line 133
    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_1

    .line 136
    :cond_0
    iput-object v2, p0, Lepson/print/imgsel/AltThumbnailCache;->realObject:Ljava/io/File;

    .line 138
    :try_start_0
    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lepson/print/imgsel/AltThumbnailCache;->modified:J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v2

    const-string v3, "ThumbnailCache"

    .line 140
    invoke-virtual {v2}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 143
    :cond_1
    invoke-virtual {p0}, Lepson/print/imgsel/AltThumbnailCache;->isFile()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 145
    invoke-virtual {p0}, Lepson/print/imgsel/AltThumbnailCache;->delete()Z

    :cond_2
    return-void
.end method

.method public static createThumbnail(Ljava/lang/String;I)Landroid/graphics/Bitmap;
    .locals 11

    const/4 v0, 0x0

    .line 349
    :try_start_0
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/4 v2, 0x1

    .line 350
    iput-boolean v2, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 351
    invoke-static {p0, v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 352
    iget v2, v1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    if-eqz v2, :cond_2

    iget v2, v1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    if-nez v2, :cond_0

    goto :goto_0

    .line 357
    :cond_0
    iget v2, v1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    div-int/2addr v2, p1

    iget v1, v1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    div-int/2addr v1, p1

    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 361
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 362
    sget-object v3, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v3, v2, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 363
    iput v1, v2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 364
    invoke-static {p0, v2}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    if-nez p0, :cond_1

    return-object v0

    .line 370
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v8

    int-to-float p1, p1

    int-to-float v0, v8

    div-float/2addr p1, v0

    .line 372
    new-instance v9, Landroid/graphics/Matrix;

    invoke-direct {v9}, Landroid/graphics/Matrix;-><init>()V

    .line 373
    invoke-virtual {v9, p1, p1}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 376
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result p1

    sub-int/2addr p1, v8

    div-int/lit8 v5, p1, 0x2

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result p1

    sub-int/2addr p1, v8

    div-int/lit8 v6, p1, 0x2

    const/4 v10, 0x1

    move-object v4, p0

    move v7, v8

    invoke-static/range {v4 .. v10}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object p0
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    :catch_0
    move-exception p1

    goto :goto_1

    :cond_2
    :goto_0
    return-object v0

    :catch_1
    move-exception p1

    move-object p0, v0

    .line 380
    :goto_1
    invoke-virtual {p1}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    :goto_2
    return-object p0
.end method

.method public static createThumbnail(Ljava/lang/String;Landroid/content/ContentResolver;J)Landroid/graphics/Bitmap;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 269
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 272
    new-instance v1, Ljava/io/File;

    invoke-static {}, Lepson/print/imgsel/AltThumbnailCache;->getCacheDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    .line 273
    new-instance v2, Lepson/print/imgsel/AltThumbnailCache;

    invoke-direct {v2, v1}, Lepson/print/imgsel/AltThumbnailCache;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 276
    :try_start_0
    invoke-virtual {v2}, Lepson/print/imgsel/AltThumbnailCache;->exists()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 277
    invoke-virtual {v2}, Lepson/print/imgsel/AltThumbnailCache;->lastModified()J

    move-result-wide v3

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v5

    cmp-long v7, v3, v5

    if-nez v7, :cond_0

    .line 280
    new-instance v3, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 281
    sget-object v4, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v4, v3, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 283
    invoke-virtual {v2}, Lepson/print/imgsel/AltThumbnailCache;->getCachePath()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v3}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v3

    if-eqz v3, :cond_2

    return-object v3

    .line 290
    :cond_0
    invoke-virtual {v2}, Lepson/print/imgsel/AltThumbnailCache;->delete()Z
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    :cond_1
    move-object v3, v1

    .line 294
    :cond_2
    :try_start_1
    sget-boolean v4, Lepson/print/imgsel/AltThumbnailCache;->bUseMediaStoreThumbnail:Z

    if-eqz v4, :cond_3

    const/4 v4, 0x3

    .line 298
    invoke-static {p1, p2, p3, v4, v1}, Landroid/provider/MediaStore$Images$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-eqz v1, :cond_4

    .line 302
    :try_start_2
    new-instance p1, Lepson/print/EPImageUtil;

    invoke-direct {p1}, Lepson/print/EPImageUtil;-><init>()V

    .line 303
    invoke-virtual {p1, p0, v1}, Lepson/print/EPImageUtil;->check_rotate(Ljava/lang/String;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    :cond_3
    const/16 p1, 0x60

    .line 307
    :try_start_3
    invoke-static {p0, p1}, Lepson/print/imgsel/AltThumbnailCache;->createThumbnail(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v1
    :try_end_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    if-eqz v1, :cond_4

    .line 310
    :try_start_4
    new-instance p1, Lepson/print/EPImageUtil;

    invoke-direct {p1}, Lepson/print/EPImageUtil;-><init>()V

    .line 311
    invoke-virtual {p1, p0, v1}, Lepson/print/EPImageUtil;->check_rotate(Ljava/lang/String;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1
    :try_end_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    :catch_0
    move-object v1, v3

    goto :goto_1

    :catch_1
    move-exception p0

    move-object v1, v3

    goto :goto_0

    :catch_2
    nop

    goto :goto_1

    :catch_3
    move-exception p0

    .line 315
    :goto_0
    invoke-virtual {p0}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    :cond_4
    :goto_1
    if-eqz v1, :cond_5

    .line 324
    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide p0

    invoke-virtual {v2, p0, p1}, Lepson/print/imgsel/AltThumbnailCache;->setLastModified(J)Z

    .line 326
    new-instance p0, Ljava/io/FileOutputStream;

    invoke-virtual {v2}, Lepson/print/imgsel/AltThumbnailCache;->getCachePath()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 327
    sget-object p1, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 p2, 0x50

    invoke-virtual {v1, p1, p2, p0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 328
    invoke-virtual {p0}, Ljava/io/FileOutputStream;->flush()V

    .line 329
    invoke-virtual {p0}, Ljava/io/FileOutputStream;->close()V

    :cond_5
    return-object v1
.end method

.method public static deleteCacheDirectory()V
    .locals 2

    .line 223
    invoke-static {}, Lepson/print/imgsel/AltThumbnailCache;->getCacheDirectory()Ljava/io/File;

    move-result-object v0

    .line 224
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 225
    invoke-static {v0}, Lepson/print/imgsel/AltThumbnailCache;->deleteDirectory(Ljava/io/File;)V

    :cond_0
    return-void
.end method

.method public static deleteDirectory(Ljava/io/File;)V
    .locals 4

    .line 235
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 238
    :cond_0
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-nez v0, :cond_1

    .line 239
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    goto :goto_1

    .line 241
    :cond_1
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object p0

    .line 242
    array-length v0, p0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_4

    aget-object v2, p0, v1

    .line 243
    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 244
    invoke-static {v2}, Lepson/print/imgsel/AltThumbnailCache;->deleteDirectory(Ljava/io/File;)V

    .line 246
    :cond_2
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 247
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_4
    :goto_1
    return-void
.end method

.method static getCacheDirectory()Ljava/io/File;
    .locals 3

    .line 203
    invoke-static {}, Lepson/print/IprintApplication;->getInstance()Lepson/print/IprintApplication;

    move-result-object v0

    .line 205
    new-instance v1, Ljava/io/File;

    invoke-virtual {v0}, Lepson/print/IprintApplication;->getExternalCacheDir()Ljava/io/File;

    move-result-object v0

    const-string v2, "thumbnail"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v1
.end method

.method protected static getThumbnailCacheSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;
    .locals 2

    const-string v0, "PREFS_PHOTO"

    const/4 v1, 0x0

    .line 115
    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p0

    return-object p0
.end method

.method public static initCacheDirectory()V
    .locals 2

    .line 215
    invoke-static {}, Lepson/print/imgsel/AltThumbnailCache;->getCacheDirectory()Ljava/io/File;

    move-result-object v0

    .line 216
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 217
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    :cond_0
    return-void
.end method

.method public static initThumbnailCache(Landroid/content/Context;)V
    .locals 0

    .line 79
    invoke-static {}, Lepson/print/imgsel/AltThumbnailCache;->initCacheDirectory()V

    .line 80
    invoke-static {p0}, Lepson/print/imgsel/AltThumbnailCache;->loadUseMediaStoreValue(Landroid/content/Context;)Z

    move-result p0

    sput-boolean p0, Lepson/print/imgsel/AltThumbnailCache;->bUseMediaStoreThumbnail:Z

    return-void
.end method

.method public static isUseMediaStoreThumbnail()Z
    .locals 1

    .line 121
    sget-boolean v0, Lepson/print/imgsel/AltThumbnailCache;->bUseMediaStoreThumbnail:Z

    return v0
.end method

.method protected static loadUseMediaStoreValue(Landroid/content/Context;)Z
    .locals 2

    .line 102
    invoke-static {p0}, Lepson/print/imgsel/AltThumbnailCache;->getThumbnailCacheSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object p0

    const-string v0, "USE_MEDIASTORE_THUMBNAIL"

    const/4 v1, 0x1

    .line 103
    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result p0

    return p0
.end method

.method protected static saveUseMediaStoreValue(Landroid/content/Context;Z)V
    .locals 1

    .line 108
    invoke-static {p0}, Lepson/print/imgsel/AltThumbnailCache;->getThumbnailCacheSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object p0

    .line 109
    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p0

    const-string v0, "USE_MEDIASTORE_THUMBNAIL"

    .line 110
    invoke-interface {p0, v0, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 111
    invoke-interface {p0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public static setThumbnailCreateMethod(Landroid/content/Context;Z)V
    .locals 0

    .line 91
    sput-boolean p1, Lepson/print/imgsel/AltThumbnailCache;->bUseMediaStoreThumbnail:Z

    .line 94
    sget-boolean p1, Lepson/print/imgsel/AltThumbnailCache;->bUseMediaStoreThumbnail:Z

    invoke-static {p0, p1}, Lepson/print/imgsel/AltThumbnailCache;->saveUseMediaStoreValue(Landroid/content/Context;Z)V

    .line 97
    invoke-static {}, Lepson/print/imgsel/AltThumbnailCache;->deleteCacheDirectory()V

    .line 98
    invoke-static {}, Lepson/print/imgsel/AltThumbnailCache;->initCacheDirectory()V

    return-void
.end method


# virtual methods
.method public getCachePath()Ljava/lang/String;
    .locals 1

    .line 189
    invoke-virtual {p0}, Lepson/print/imgsel/AltThumbnailCache;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 190
    invoke-virtual {p0}, Lepson/print/imgsel/AltThumbnailCache;->mkdir()Z

    .line 193
    :cond_0
    iget-object v0, p0, Lepson/print/imgsel/AltThumbnailCache;->realObject:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public lastModified()J
    .locals 2

    .line 179
    iget-wide v0, p0, Lepson/print/imgsel/AltThumbnailCache;->modified:J

    return-wide v0
.end method

.method public setLastModified(J)Z
    .locals 2

    .line 163
    invoke-virtual {p0}, Lepson/print/imgsel/AltThumbnailCache;->delete()Z

    .line 165
    iput-wide p1, p0, Lepson/print/imgsel/AltThumbnailCache;->modified:J

    .line 166
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lepson/print/imgsel/AltThumbnailCache;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lepson/print/imgsel/AltThumbnailCache;->realObject:Ljava/io/File;

    const/4 p1, 0x1

    return p1
.end method

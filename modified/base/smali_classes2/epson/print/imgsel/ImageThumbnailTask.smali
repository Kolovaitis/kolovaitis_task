.class public Lepson/print/imgsel/ImageThumbnailTask;
.super Landroid/os/AsyncTask;
.source "ImageThumbnailTask.java"

# interfaces
.implements Lepson/print/imgsel/ImageFinder$Canceller;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Lepson/print/ImageItem;",
        "Ljava/lang/Void;",
        ">;",
        "Lepson/print/imgsel/ImageFinder$Canceller;"
    }
.end annotation


# static fields
.field private static final RETRY_CHECKLIST:I = 0x64

.field private static final RETRY_SUSPEND:I = 0x3e8

.field private static final TAG:Ljava/lang/String; = "ImageThumbnailTask"


# instance fields
.field private volatile finish:Z

.field private volatile imageItems:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList<",
            "Lepson/print/ImageItem;",
            ">;"
        }
    .end annotation
.end field

.field mFragmentReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lepson/print/imgsel/ImageGridFragment;",
            ">;"
        }
    .end annotation
.end field

.field private volatile suspend:Z


# direct methods
.method public constructor <init>(Lepson/print/imgsel/ImageGridFragment;)V
    .locals 1

    .line 41
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    const/4 v0, 0x0

    .line 36
    iput-boolean v0, p0, Lepson/print/imgsel/ImageThumbnailTask;->finish:Z

    .line 39
    iput-boolean v0, p0, Lepson/print/imgsel/ImageThumbnailTask;->suspend:Z

    .line 42
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lepson/print/imgsel/ImageThumbnailTask;->mFragmentReference:Ljava/lang/ref/WeakReference;

    .line 43
    new-instance p1, Ljava/util/LinkedList;

    invoke-direct {p1}, Ljava/util/LinkedList;-><init>()V

    iput-object p1, p0, Lepson/print/imgsel/ImageThumbnailTask;->imageItems:Ljava/util/LinkedList;

    return-void
.end method

.method private getResolver()Landroid/content/ContentResolver;
    .locals 2

    .line 171
    iget-object v0, p0, Lepson/print/imgsel/ImageThumbnailTask;->mFragmentReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/print/imgsel/ImageGridFragment;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 175
    :cond_0
    invoke-virtual {v0}, Lepson/print/imgsel/ImageGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_1

    return-object v1

    .line 179
    :cond_1
    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method declared-synchronized addLast(Lepson/print/ImageItem;)V
    .locals 1

    monitor-enter p0

    .line 152
    :try_start_0
    iget-object v0, p0, Lepson/print/imgsel/ImageThumbnailTask;->imageItems:Ljava/util/LinkedList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 153
    monitor-exit p0

    return-void

    .line 156
    :cond_0
    :try_start_1
    iget-object v0, p0, Lepson/print/imgsel/ImageThumbnailTask;->imageItems:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 157
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public checkCanceled()Z
    .locals 1

    .line 185
    invoke-virtual {p0}, Lepson/print/imgsel/ImageThumbnailTask;->isCancelled()Z

    move-result v0

    return v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 19
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/print/imgsel/ImageThumbnailTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 9

    const-string p1, "ImageThumbnailTask"

    const-string v0, "Enter doInBackground"

    .line 48
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    invoke-direct {p0}, Lepson/print/imgsel/ImageThumbnailTask;->getResolver()Landroid/content/ContentResolver;

    move-result-object p1

    .line 54
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lepson/print/imgsel/ImageThumbnailTask;->getFirst()Lepson/print/ImageItem;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_1

    iget-boolean v2, p0, Lepson/print/imgsel/ImageThumbnailTask;->finish:Z

    if-nez v2, :cond_2

    .line 55
    :cond_1
    invoke-virtual {p0}, Lepson/print/imgsel/ImageThumbnailTask;->isCancelled()Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    const-string p1, "ImageThumbnailTask"

    const-string v0, "Leave doInBackground"

    .line 104
    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-object v1

    :cond_3
    if-nez v0, :cond_4

    :try_start_0
    const-string v0, "ImageThumbnailTask"

    const-string v1, "Wait doInBackground"

    .line 62
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-wide/16 v0, 0x64

    .line 63
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 65
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    :cond_4
    :try_start_1
    const-string v2, "ImageThumbnailTask"

    .line 74
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "createThumbnail "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lepson/print/ImageItem;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    invoke-virtual {v0}, Lepson/print/ImageItem;->getPath()Ljava/lang/String;

    move-result-object v2

    .line 77
    invoke-virtual {v0}, Lepson/print/ImageItem;->getDatabaseId()J

    move-result-wide v3

    .line 75
    invoke-static {v2, p1, v3, v4}, Lepson/print/imgsel/AltThumbnailCache;->createThumbnail(Ljava/lang/String;Landroid/content/ContentResolver;J)Landroid/graphics/Bitmap;

    move-result-object v1
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v4, v1

    goto :goto_1

    :catch_1
    move-exception v2

    .line 79
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move-object v4, v1

    .line 83
    :goto_1
    new-instance v1, Lepson/print/ImageItem;

    const/4 v5, 0x0

    .line 84
    invoke-virtual {v0}, Lepson/print/ImageItem;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lepson/print/ImageItem;->getDatabaseId()J

    move-result-wide v7

    move-object v3, v1

    invoke-direct/range {v3 .. v8}, Lepson/print/ImageItem;-><init>(Landroid/graphics/Bitmap;ILjava/lang/String;J)V

    const/4 v0, 0x1

    .line 85
    new-array v0, v0, [Lepson/print/ImageItem;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Lepson/print/imgsel/ImageThumbnailTask;->publishProgress([Ljava/lang/Object;)V

    .line 88
    iget-boolean v0, p0, Lepson/print/imgsel/ImageThumbnailTask;->suspend:Z

    if-eqz v0, :cond_0

    const-string v0, "ImageThumbnailTask"

    const-string v1, "Enter suspend"

    .line 89
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    :goto_2
    iget-boolean v0, p0, Lepson/print/imgsel/ImageThumbnailTask;->suspend:Z

    if-eqz v0, :cond_6

    .line 91
    invoke-virtual {p0}, Lepson/print/imgsel/ImageThumbnailTask;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_5

    goto :goto_3

    :cond_5
    const-wide/16 v0, 0x3e8

    .line 95
    :try_start_2
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_2

    :catch_2
    :cond_6
    :goto_3
    const-string v0, "ImageThumbnailTask"

    const-string v1, "Leave suspend"

    .line 100
    invoke-static {v0, v1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method declared-synchronized getFirst()Lepson/print/ImageItem;
    .locals 2

    monitor-enter p0

    .line 123
    :try_start_0
    iget-object v0, p0, Lepson/print/imgsel/ImageThumbnailTask;->imageItems:Ljava/util/LinkedList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lepson/print/imgsel/ImageThumbnailTask;->imageItems:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-gtz v0, :cond_0

    goto :goto_0

    .line 127
    :cond_0
    iget-object v0, p0, Lepson/print/imgsel/ImageThumbnailTask;->imageItems:Ljava/util/LinkedList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/print/ImageItem;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    .line 124
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized moveToFirst(Lepson/print/ImageItem;)V
    .locals 4

    monitor-enter p0

    .line 134
    :try_start_0
    iget-object v0, p0, Lepson/print/imgsel/ImageThumbnailTask;->imageItems:Ljava/util/LinkedList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 135
    monitor-exit p0

    return-void

    .line 138
    :cond_0
    :try_start_1
    iget-object v0, p0, Lepson/print/imgsel/ImageThumbnailTask;->imageItems:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    goto :goto_0

    :cond_1
    const-string v1, "ImageThumbnailTask"

    .line 141
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "moveToFirst "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lepson/print/ImageItem;->getPath()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    iget-object p1, p0, Lepson/print/imgsel/ImageThumbnailTask;->imageItems:Ljava/util/LinkedList;

    iget-object v1, p0, Lepson/print/imgsel/ImageThumbnailTask;->imageItems:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 145
    :goto_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method protected varargs onProgressUpdate([Lepson/print/ImageItem;)V
    .locals 2

    .line 110
    iget-object v0, p0, Lepson/print/imgsel/ImageThumbnailTask;->mFragmentReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/print/imgsel/ImageGridFragment;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    .line 111
    array-length v1, p1

    if-gtz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 116
    aget-object p1, p1, v1

    invoke-virtual {v0, p1}, Lepson/print/imgsel/ImageGridFragment;->updateItem(Lepson/print/ImageItem;)V

    return-void

    :cond_1
    :goto_0
    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .line 19
    check-cast p1, [Lepson/print/ImageItem;

    invoke-virtual {p0, p1}, Lepson/print/imgsel/ImageThumbnailTask;->onProgressUpdate([Lepson/print/ImageItem;)V

    return-void
.end method

.method public setFinish(Z)V
    .locals 0

    .line 160
    iput-boolean p1, p0, Lepson/print/imgsel/ImageThumbnailTask;->finish:Z

    return-void
.end method

.method public setSuspend(Z)V
    .locals 0

    .line 164
    iput-boolean p1, p0, Lepson/print/imgsel/ImageThumbnailTask;->suspend:Z

    return-void
.end method

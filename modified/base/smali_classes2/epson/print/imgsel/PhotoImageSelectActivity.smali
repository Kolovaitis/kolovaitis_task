.class public Lepson/print/imgsel/PhotoImageSelectActivity;
.super Lepson/print/imgsel/ImageSelectActivity;
.source "PhotoImageSelectActivity.java"


# static fields
.field private static final REQUEST_CODE_PREVIEW:I = 0x67


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Lepson/print/imgsel/ImageSelectActivity;-><init>()V

    return-void
.end method

.method private getPrintLog()Lcom/epson/iprint/prtlogger/PrintLog;
    .locals 2

    .line 82
    new-instance v0, Lcom/epson/iprint/prtlogger/PrintLog;

    invoke-direct {v0}, Lcom/epson/iprint/prtlogger/PrintLog;-><init>()V

    const/4 v1, 0x1

    .line 83
    iput v1, v0, Lcom/epson/iprint/prtlogger/PrintLog;->uiRoute:I

    return-object v0
.end method

.method private popFragmentStackToFolderView()V
    .locals 1

    .line 65
    invoke-virtual {p0}, Lepson/print/imgsel/PhotoImageSelectActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 66
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->popBackStackImmediate()Z

    return-void
.end method

.method public static startAddImageSelect(Landroid/app/Activity;Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 108
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lepson/print/imgsel/PhotoImageSelectActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x14000000

    .line 110
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v1, "selected_files"

    .line 111
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 113
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public getImageFinder()Lepson/print/imgsel/ImageFinder;
    .locals 1

    .line 23
    new-instance v0, Lepson/print/imgsel/PhotoImageFinder;

    invoke-direct {v0}, Lepson/print/imgsel/PhotoImageFinder;-><init>()V

    return-object v0
.end method

.method protected goNext()V
    .locals 3

    .line 72
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lepson/print/ActivityViewImageSelect;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 74
    invoke-virtual {p0}, Lepson/print/imgsel/PhotoImageSelectActivity;->getImageSelector()Lepson/print/imgsel/ImageSelector;

    move-result-object v1

    invoke-virtual {v1}, Lepson/print/imgsel/ImageSelector;->getFileArrayList()Ljava/util/ArrayList;

    move-result-object v1

    const-string v2, "imageList"

    .line 75
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v1, "epson_color_mode"

    const/4 v2, 0x1

    .line 76
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "print_log"

    .line 77
    invoke-direct {p0}, Lepson/print/imgsel/PhotoImageSelectActivity;->getPrintLog()Lcom/epson/iprint/prtlogger/PrintLog;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const/16 v1, 0x67

    .line 78
    invoke-virtual {p0, v0, v1}, Lepson/print/imgsel/PhotoImageSelectActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .line 34
    invoke-super {p0, p1, p2, p3}, Lepson/print/imgsel/ImageSelectActivity;->onActivityResult(IILandroid/content/Intent;)V

    const/16 v0, 0x67

    if-eq p1, v0, :cond_0

    return-void

    :cond_0
    if-eqz p3, :cond_3

    const-string p1, "imageList"

    .line 38
    invoke-virtual {p3, p1}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object p1

    const/16 p3, 0x16

    if-ne p2, p3, :cond_1

    .line 43
    invoke-direct {p0}, Lepson/print/imgsel/PhotoImageSelectActivity;->popFragmentStackToFolderView()V

    :cond_1
    if-nez p1, :cond_2

    return-void

    .line 52
    :cond_2
    invoke-virtual {p0, p1}, Lepson/print/imgsel/PhotoImageSelectActivity;->refreshSelector(Ljava/util/ArrayList;)V

    :cond_3
    return-void
.end method

.method protected onPause()V
    .locals 1

    .line 89
    invoke-virtual {p0}, Lepson/print/imgsel/PhotoImageSelectActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    invoke-static {p0}, Lcom/epson/iprint/apf/ApfPreviewView;->deleteWorkingDirectory(Landroid/content/Context;)V

    .line 95
    :cond_0
    invoke-super {p0}, Lepson/print/imgsel/ImageSelectActivity;->onPause()V

    return-void
.end method

.method public showNfcInvalidMessageOnPagerActivity()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public singleImageMode()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

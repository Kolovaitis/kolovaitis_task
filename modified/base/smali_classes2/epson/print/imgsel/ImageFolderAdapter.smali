.class public Lepson/print/imgsel/ImageFolderAdapter;
.super Landroid/widget/BaseAdapter;
.source "ImageFolderAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/imgsel/ImageFolderAdapter$FolderInfoAndThumbnail;
    }
.end annotation


# instance fields
.field private mFolderList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lepson/print/imgsel/ImageFolderAdapter$FolderInfoAndThumbnail;",
            ">;"
        }
    .end annotation
.end field

.field private mLayoutInflater:Landroid/view/LayoutInflater;

.field private mResources:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 72
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 73
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lepson/print/imgsel/ImageFolderAdapter;->mFolderList:Ljava/util/List;

    .line 74
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lepson/print/imgsel/ImageFolderAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 75
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    iput-object p1, p0, Lepson/print/imgsel/ImageFolderAdapter;->mResources:Landroid/content/res/Resources;

    return-void
.end method


# virtual methods
.method public addItem(Lepson/print/imgsel/ImageFolderInfo;Landroid/graphics/Bitmap;)V
    .locals 2

    .line 83
    iget-object v0, p0, Lepson/print/imgsel/ImageFolderAdapter;->mFolderList:Ljava/util/List;

    new-instance v1, Lepson/print/imgsel/ImageFolderAdapter$FolderInfoAndThumbnail;

    invoke-direct {v1, p1, p2}, Lepson/print/imgsel/ImageFolderAdapter$FolderInfoAndThumbnail;-><init>(Lepson/print/imgsel/ImageFolderInfo;Landroid/graphics/Bitmap;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 84
    invoke-virtual {p0}, Lepson/print/imgsel/ImageFolderAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public getCount()I
    .locals 1

    .line 28
    iget-object v0, p0, Lepson/print/imgsel/ImageFolderAdapter;->mFolderList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getFolderInfo(I)Lepson/print/imgsel/ImageFolderInfo;
    .locals 1

    .line 89
    iget-object v0, p0, Lepson/print/imgsel/ImageFolderAdapter;->mFolderList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lepson/print/imgsel/ImageFolderAdapter$FolderInfoAndThumbnail;

    iget-object p1, p1, Lepson/print/imgsel/ImageFolderAdapter$FolderInfoAndThumbnail;->mImageFolderInfo:Lepson/print/imgsel/ImageFolderInfo;

    return-object p1
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .line 33
    iget-object v0, p0, Lepson/print/imgsel/ImageFolderAdapter;->mFolderList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    const/4 v0, 0x0

    if-nez p2, :cond_0

    .line 45
    iget-object p2, p0, Lepson/print/imgsel/ImageFolderAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f0a0085

    invoke-virtual {p2, v1, p3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    :cond_0
    const p3, 0x7f080150

    .line 47
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/TextView;

    .line 48
    iget-object v1, p0, Lepson/print/imgsel/ImageFolderAdapter;->mFolderList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lepson/print/imgsel/ImageFolderAdapter$FolderInfoAndThumbnail;

    if-nez p1, :cond_1

    return-object p2

    .line 53
    :cond_1
    iget-object v1, p1, Lepson/print/imgsel/ImageFolderAdapter$FolderInfoAndThumbnail;->mImageFolderInfo:Lepson/print/imgsel/ImageFolderInfo;

    iget-object v1, v1, Lepson/print/imgsel/ImageFolderInfo;->mCanonicalName:Ljava/lang/String;

    .line 54
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    .line 55
    invoke-virtual {p3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const p3, 0x7f080199

    .line 57
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/TextView;

    .line 58
    iget-object v1, p0, Lepson/print/imgsel/ImageFolderAdapter;->mResources:Landroid/content/res/Resources;

    const v2, 0x7f0e03bc

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p1, Lepson/print/imgsel/ImageFolderAdapter$FolderInfoAndThumbnail;->mImageFolderInfo:Lepson/print/imgsel/ImageFolderInfo;

    .line 59
    invoke-virtual {v4}, Lepson/print/imgsel/ImageFolderInfo;->getImageCount()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    .line 58
    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 60
    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const p3, 0x7f080195

    .line 62
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/ImageView;

    .line 63
    iget-object v0, p1, Lepson/print/imgsel/ImageFolderAdapter$FolderInfoAndThumbnail;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    .line 64
    iget-object p1, p1, Lepson/print/imgsel/ImageFolderAdapter$FolderInfoAndThumbnail;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p3, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    :cond_2
    const p1, 0x7f0700ad

    .line 66
    invoke-virtual {p3, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_0
    return-object p2
.end method

.method public releaseResource()V
    .locals 3

    .line 97
    iget-object v0, p0, Lepson/print/imgsel/ImageFolderAdapter;->mFolderList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lepson/print/imgsel/ImageFolderAdapter$FolderInfoAndThumbnail;

    .line 98
    iget-object v2, v1, Lepson/print/imgsel/ImageFolderAdapter$FolderInfoAndThumbnail;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_0

    .line 99
    iget-object v2, v1, Lepson/print/imgsel/ImageFolderAdapter$FolderInfoAndThumbnail;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    const/4 v2, 0x0

    .line 100
    iput-object v2, v1, Lepson/print/imgsel/ImageFolderAdapter$FolderInfoAndThumbnail;->mBitmap:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 104
    :cond_1
    iget-object v0, p0, Lepson/print/imgsel/ImageFolderAdapter;->mFolderList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

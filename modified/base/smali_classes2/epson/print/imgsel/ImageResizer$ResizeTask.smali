.class public Lepson/print/imgsel/ImageResizer$ResizeTask;
.super Landroid/os/AsyncTask;
.source "ImageResizer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/imgsel/ImageResizer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ResizeTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private mImageImageDrawerReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lepson/print/imgsel/ImageResizer$BitmapDrawer;",
            ">;"
        }
    .end annotation
.end field

.field private mOrgFile:Ljava/io/File;

.field private mReqSize:I

.field final synthetic this$0:Lepson/print/imgsel/ImageResizer;


# direct methods
.method public constructor <init>(Lepson/print/imgsel/ImageResizer;Ljava/io/File;Lepson/print/imgsel/ImageResizer$BitmapDrawer;I)V
    .locals 0

    .line 109
    iput-object p1, p0, Lepson/print/imgsel/ImageResizer$ResizeTask;->this$0:Lepson/print/imgsel/ImageResizer;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 110
    iput-object p2, p0, Lepson/print/imgsel/ImageResizer$ResizeTask;->mOrgFile:Ljava/io/File;

    .line 111
    new-instance p1, Ljava/lang/ref/WeakReference;

    invoke-direct {p1, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lepson/print/imgsel/ImageResizer$ResizeTask;->mImageImageDrawerReference:Ljava/lang/ref/WeakReference;

    .line 112
    iput p4, p0, Lepson/print/imgsel/ImageResizer$ResizeTask;->mReqSize:I

    return-void
.end method

.method private getExifRotation(Ljava/io/File;)I
    .locals 3

    const/4 v0, 0x0

    .line 186
    :try_start_0
    new-instance v1, Landroid/media/ExifInterface;

    invoke-virtual {p1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    const-string p1, "Orientation"

    const/4 v2, 0x1

    .line 187
    invoke-virtual {v1, p1, v2}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x3

    if-eq p1, v1, :cond_2

    const/4 v1, 0x6

    if-eq p1, v1, :cond_1

    const/16 v1, 0x8

    if-eq p1, v1, :cond_0

    return v0

    :cond_0
    const/16 p1, 0x10e

    return p1

    :cond_1
    const/16 p1, 0x5a

    return p1

    :cond_2
    const/16 p1, 0xb4

    return p1

    :catch_0
    return v0
.end method

.method private rotateBitmap(Ljava/io/File;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 7

    .line 160
    invoke-direct {p0, p1}, Lepson/print/imgsel/ImageResizer$ResizeTask;->getExifRotation(Ljava/io/File;)I

    move-result p1

    if-nez p1, :cond_0

    return-object p2

    .line 168
    :cond_0
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    int-to-float p1, p1

    .line 169
    invoke-virtual {v5, p1}, Landroid/graphics/Matrix;->setRotate(F)V

    .line 171
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 172
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v6, 0x1

    move-object v0, p2

    .line 174
    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object p1

    return-object p1
.end method

.method private testSleep(I)V
    .locals 2

    int-to-long v0, p1

    .line 150
    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 152
    invoke-virtual {p1}, Ljava/lang/InterruptedException;->printStackTrace()V

    :goto_0
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Landroid/graphics/Bitmap;
    .locals 4

    .line 117
    invoke-virtual {p0}, Lepson/print/imgsel/ImageResizer$ResizeTask;->isCancelled()Z

    move-result p1

    const/4 v0, 0x0

    if-nez p1, :cond_4

    iget-object p1, p0, Lepson/print/imgsel/ImageResizer$ResizeTask;->this$0:Lepson/print/imgsel/ImageResizer;

    invoke-static {p1}, Lepson/print/imgsel/ImageResizer;->access$000(Lepson/print/imgsel/ImageResizer;)Z

    move-result p1

    if-nez p1, :cond_0

    goto :goto_1

    .line 120
    :cond_0
    iget-object p1, p0, Lepson/print/imgsel/ImageResizer$ResizeTask;->mOrgFile:Ljava/io/File;

    if-nez p1, :cond_1

    return-object v0

    .line 127
    :cond_1
    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object p1

    iget v1, p0, Lepson/print/imgsel/ImageResizer$ResizeTask;->mReqSize:I

    iget v2, p0, Lepson/print/imgsel/ImageResizer$ResizeTask;->mReqSize:I

    const/4 v3, 0x1

    .line 126
    invoke-static {p1, v1, v2, v3, v3}, Lepson/common/ImageUtil;->loadBitmap(Ljava/lang/String;IIZZ)Landroid/graphics/Bitmap;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const-string v1, "ImageResizer"

    .line 129
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "doInBackground:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/OutOfMemoryError;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object p1, v0

    .line 132
    :goto_0
    invoke-virtual {p0}, Lepson/print/imgsel/ImageResizer$ResizeTask;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_2

    return-object v0

    :cond_2
    if-eqz p1, :cond_3

    .line 136
    iget-object v0, p0, Lepson/print/imgsel/ImageResizer$ResizeTask;->mOrgFile:Ljava/io/File;

    invoke-direct {p0, v0, p1}, Lepson/print/imgsel/ImageResizer$ResizeTask;->rotateBitmap(Ljava/io/File;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object p1

    .line 137
    iget-object v0, p0, Lepson/print/imgsel/ImageResizer$ResizeTask;->this$0:Lepson/print/imgsel/ImageResizer;

    invoke-static {v0}, Lepson/print/imgsel/ImageResizer;->access$100(Lepson/print/imgsel/ImageResizer;)Lepson/print/imgsel/BitmapCache;

    move-result-object v0

    iget-object v1, p0, Lepson/print/imgsel/ImageResizer$ResizeTask;->mOrgFile:Ljava/io/File;

    invoke-virtual {v0, v1, p1}, Lepson/print/imgsel/BitmapCache;->addBitmap(Ljava/io/File;Landroid/graphics/Bitmap;)V

    :cond_3
    return-object p1

    :cond_4
    :goto_1
    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 104
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lepson/print/imgsel/ImageResizer$ResizeTask;->doInBackground([Ljava/lang/Void;)Landroid/graphics/Bitmap;

    move-result-object p1

    return-object p1
.end method

.method protected onPostExecute(Landroid/graphics/Bitmap;)V
    .locals 1

    .line 208
    iget-object v0, p0, Lepson/print/imgsel/ImageResizer$ResizeTask;->mImageImageDrawerReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepson/print/imgsel/ImageResizer$BitmapDrawer;

    if-eqz p1, :cond_0

    if-eqz v0, :cond_0

    .line 210
    invoke-interface {v0, p1}, Lepson/print/imgsel/ImageResizer$BitmapDrawer;->setBitmap(Landroid/graphics/Bitmap;)V

    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 104
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lepson/print/imgsel/ImageResizer$ResizeTask;->onPostExecute(Landroid/graphics/Bitmap;)V

    return-void
.end method

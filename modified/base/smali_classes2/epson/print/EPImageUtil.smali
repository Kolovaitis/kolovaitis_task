.class public Lepson/print/EPImageUtil;
.super Ljava/lang/Object;
.source "EPImageUtil.java"


# static fields
.field public static final CANCEL_FILE_BASE_NAME:Ljava/lang/String; = "cancel.dat"

.field public static final RETURN_CODE_CANCLE:I = -0x101


# direct methods
.method static constructor <clinit>()V
    .locals 1

    :try_start_0
    const-string v0, "epsimage"

    .line 12
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 15
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static native jpegTo1BitBmpPixelRoundDown(Ljava/lang/String;Ljava/lang/String;I)I
.end method


# virtual methods
.method public native bmp2jpg(Ljava/lang/String;Ljava/lang/String;I)I
.end method

.method public check_rotate(Ljava/lang/String;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 10

    .line 90
    :try_start_0
    new-instance v0, Landroid/media/ExifInterface;

    invoke-direct {v0, p1}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    const-string p1, "Orientation"

    const/4 v1, 0x0

    .line 91
    invoke-virtual {v0, p1, v1}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result p1

    const/16 v0, 0x8

    const/4 v1, 0x3

    const/4 v2, 0x6

    if-eq p1, v2, :cond_0

    if-eq p1, v1, :cond_0

    if-eq p1, v0, :cond_0

    return-object p2

    .line 102
    :cond_0
    new-instance v8, Landroid/graphics/Matrix;

    invoke-direct {v8}, Landroid/graphics/Matrix;-><init>()V

    if-eq p1, v1, :cond_3

    if-eq p1, v2, :cond_2

    if-eq p1, v0, :cond_1

    const/4 p1, 0x0

    .line 117
    invoke-virtual {v8, p1}, Landroid/graphics/Matrix;->setRotate(F)V

    goto :goto_0

    :cond_1
    const/high16 p1, 0x43870000    # 270.0f

    .line 113
    invoke-virtual {v8, p1}, Landroid/graphics/Matrix;->setRotate(F)V

    goto :goto_0

    :cond_2
    const/high16 p1, 0x42b40000    # 90.0f

    .line 105
    invoke-virtual {v8, p1}, Landroid/graphics/Matrix;->setRotate(F)V

    goto :goto_0

    :cond_3
    const/high16 p1, 0x43340000    # 180.0f

    .line 109
    invoke-virtual {v8, p1}, Landroid/graphics/Matrix;->setRotate(F)V

    :goto_0
    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 120
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    const/4 v9, 0x0

    move-object v3, p2

    invoke-static/range {v3 .. v9}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object p2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    .line 122
    invoke-virtual {p1}, Ljava/io/IOException;->printStackTrace()V

    :goto_1
    return-object p2
.end method

.method public native color2grayscale(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public native getBitCount(Ljava/lang/String;)I
.end method

.method public native jpg2bmp(Ljava/lang/String;Ljava/lang/String;I)I
.end method

.method public native resizeImage(Ljava/lang/String;Ljava/lang/String;IIIIIIIIII)I
.end method

.method public native rotate180Image(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public rotateImage(Ljava/lang/String;Ljava/lang/String;I)I
    .locals 1

    const/16 v0, 0x5a

    if-eq p3, v0, :cond_2

    const/16 v0, 0xb4

    if-eq p3, v0, :cond_1

    const/16 v0, 0x10e

    if-eq p3, v0, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 71
    :cond_0
    invoke-virtual {p0, p1, p2}, Lepson/print/EPImageUtil;->rotateR270Image(Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    goto :goto_0

    .line 67
    :cond_1
    invoke-virtual {p0, p1, p2}, Lepson/print/EPImageUtil;->rotate180Image(Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    goto :goto_0

    .line 63
    :cond_2
    invoke-virtual {p0, p1, p2}, Lepson/print/EPImageUtil;->rotateR90Image(Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    :goto_0
    return p1
.end method

.method public native rotateR270Image(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public native rotateR90Image(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public native tiff2bmp(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public native writeDate(Ljava/lang/String;Ljava/lang/String;II)I
.end method

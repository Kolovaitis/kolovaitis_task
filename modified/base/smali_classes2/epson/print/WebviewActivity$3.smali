.class Lepson/print/WebviewActivity$3;
.super Landroid/webkit/WebViewClient;
.source "WebviewActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/WebviewActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/WebviewActivity;


# direct methods
.method constructor <init>(Lepson/print/WebviewActivity;)V
    .locals 0

    .line 363
    iput-object p1, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 6

    const-string p1, "WebviewActivity"

    .line 386
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onPageFinished: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 388
    iget-object p1, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    const/4 v0, 0x1

    invoke-static {p1, v0}, Lepson/print/WebviewActivity;->access$702(Lepson/print/WebviewActivity;Z)Z

    .line 389
    iget-object p1, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    invoke-static {p1, v0}, Lepson/print/WebviewActivity;->access$002(Lepson/print/WebviewActivity;Z)Z

    .line 390
    iget-object p1, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    const/4 v1, 0x0

    invoke-static {p1, v1}, Lepson/print/WebviewActivity;->access$802(Lepson/print/WebviewActivity;Z)Z

    .line 391
    iget-object p1, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    invoke-virtual {p1, v0}, Lepson/print/WebviewActivity;->setClearButtonAppearance(I)V

    .line 392
    iget-object p1, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    invoke-virtual {p1}, Lepson/print/WebviewActivity;->getTitle()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v2, "Web page not available"

    .line 393
    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 394
    iget-object p1, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    invoke-static {p1, p2}, Lepson/print/WebviewActivity;->access$902(Lepson/print/WebviewActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 395
    iget-object p1, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    const/4 v2, 0x0

    invoke-static {p1, v2}, Lepson/print/WebviewActivity;->access$1002(Lepson/print/WebviewActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 396
    iget-object p1, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    iput-boolean v1, p1, Lepson/print/WebviewActivity;->mIsLoadError:Z

    goto :goto_0

    .line 398
    :cond_0
    iget-object p1, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    iput-boolean v0, p1, Lepson/print/WebviewActivity;->mIsLoadError:Z

    .line 402
    :goto_0
    iget-object p1, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    invoke-virtual {p1}, Lepson/print/WebviewActivity;->invalidateOptionsMenu()V

    .line 409
    iget-object p1, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    iget-boolean p1, p1, Lepson/print/WebviewActivity;->isBuy:Z

    if-nez p1, :cond_9

    iget-object p1, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    iget-boolean p1, p1, Lepson/print/WebviewActivity;->isFaq:Z

    if-nez p1, :cond_9

    iget-object p1, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    iget-boolean p1, p1, Lepson/print/WebviewActivity;->isInfo:Z

    if-nez p1, :cond_9

    .line 412
    iget-object p1, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    iget-object p1, p1, Lepson/print/WebviewActivity;->mStrictMode:Lepson/print/IprintStrictMode$StrictModeInterface;

    invoke-interface {p1}, Lepson/print/IprintStrictMode$StrictModeInterface;->permitAll()V

    .line 413
    iget-object p1, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    const-string v2, "history"

    invoke-virtual {p1, v2, v1}, Lepson/print/WebviewActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-static {p1, v2}, Lepson/print/WebviewActivity;->access$1102(Lepson/print/WebviewActivity;Landroid/content/SharedPreferences;)Landroid/content/SharedPreferences;

    .line 415
    iget-object p1, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    iget-boolean p1, p1, Lepson/print/WebviewActivity;->mIsFistLoad:Z

    if-nez p1, :cond_3

    .line 416
    iget-object p1, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    invoke-static {p1}, Lepson/print/WebviewActivity;->access$1100(Lepson/print/WebviewActivity;)Landroid/content/SharedPreferences;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object p1

    if-eqz p1, :cond_3

    .line 418
    iget-object v2, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    invoke-static {v2}, Lepson/print/WebviewActivity;->access$1200(Lepson/print/WebviewActivity;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 419
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 420
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    const-string v5, "key"

    .line 421
    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v5, "value"

    .line 422
    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 423
    iget-object v3, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    invoke-static {v3}, Lepson/print/WebviewActivity;->access$1200(Lepson/print/WebviewActivity;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 425
    :cond_1
    iget-object p1, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    invoke-static {p1}, Lepson/print/WebviewActivity;->access$1200(Lepson/print/WebviewActivity;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-static {p1, v2}, Lepson/print/WebviewActivity;->access$1302(Lepson/print/WebviewActivity;I)I

    const/4 p1, 0x0

    const/4 v2, 0x0

    .line 426
    :goto_2
    iget-object v3, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    invoke-static {v3}, Lepson/print/WebviewActivity;->access$1200(Lepson/print/WebviewActivity;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge p1, v3, :cond_4

    .line 427
    iget-object v3, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    invoke-static {v3}, Lepson/print/WebviewActivity;->access$1200(Lepson/print/WebviewActivity;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/HashMap;

    const-string v4, "value"

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {p2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v2, 0x1

    :cond_2
    add-int/lit8 p1, p1, 0x1

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    :cond_4
    if-eqz v2, :cond_6

    .line 434
    iget-object p1, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    iget-boolean p1, p1, Lepson/print/WebviewActivity;->mIsFistLoad:Z

    if-eqz p1, :cond_5

    goto :goto_3

    .line 450
    :cond_5
    iget-object p1, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    iput-boolean v1, p1, Lepson/print/WebviewActivity;->mIsFistLoad:Z

    .line 451
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Link: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, " already exist"

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 436
    :cond_6
    :goto_3
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    .line 437
    iget-object v0, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    invoke-static {v0}, Lepson/print/WebviewActivity;->access$1308(Lepson/print/WebviewActivity;)I

    .line 438
    iget-object v0, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    invoke-static {v0}, Lepson/print/WebviewActivity;->access$1100(Lepson/print/WebviewActivity;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-static {v0, v2}, Lepson/print/WebviewActivity;->access$1402(Lepson/print/WebviewActivity;Landroid/content/SharedPreferences$Editor;)Landroid/content/SharedPreferences$Editor;

    .line 439
    iget-object v0, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    invoke-static {v0}, Lepson/print/WebviewActivity;->access$1400(Lepson/print/WebviewActivity;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v2, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    invoke-static {v2}, Lepson/print/WebviewActivity;->access$1300(Lepson/print/WebviewActivity;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 440
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Save countUrl "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    invoke-static {v2}, Lepson/print/WebviewActivity;->access$1300(Lepson/print/WebviewActivity;)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, " : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    iget-object v0, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    invoke-static {v0}, Lepson/print/WebviewActivity;->access$1400(Lepson/print/WebviewActivity;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    const-string v0, "key"

    .line 442
    iget-object v2, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    invoke-static {v2}, Lepson/print/WebviewActivity;->access$1300(Lepson/print/WebviewActivity;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "value"

    .line 443
    invoke-virtual {p1, v0, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 444
    iget-object v0, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    invoke-static {v0}, Lepson/print/WebviewActivity;->access$1200(Lepson/print/WebviewActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 445
    iget-object p1, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    iput-boolean v1, p1, Lepson/print/WebviewActivity;->mIsFistLoad:Z

    const/4 p1, 0x0

    .line 446
    :goto_4
    iget-object v0, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    invoke-static {v0}, Lepson/print/WebviewActivity;->access$1200(Lepson/print/WebviewActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_7

    .line 447
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    invoke-static {v2}, Lepson/print/WebviewActivity;->access$1200(Lepson/print/WebviewActivity;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/HashMap;

    const-string v3, "key"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    invoke-static {v2}, Lepson/print/WebviewActivity;->access$1200(Lepson/print/WebviewActivity;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/HashMap;

    const-string v3, "value"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v0, v2}, Lepson/print/Util/EPLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 p1, p1, 0x1

    goto :goto_4

    :cond_7
    :goto_5
    const-string p1, "file://"

    .line 455
    invoke-virtual {p2, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_8

    .line 456
    iget-object p1, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    const-string v0, "SaveUrl"

    invoke-virtual {p1, v0, v1}, Lepson/print/WebviewActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/WebviewActivity;->access$1102(Lepson/print/WebviewActivity;Landroid/content/SharedPreferences;)Landroid/content/SharedPreferences;

    .line 457
    iget-object p1, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    invoke-static {p1}, Lepson/print/WebviewActivity;->access$1100(Lepson/print/WebviewActivity;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/WebviewActivity;->access$1402(Lepson/print/WebviewActivity;Landroid/content/SharedPreferences$Editor;)Landroid/content/SharedPreferences$Editor;

    .line 458
    iget-object p1, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    invoke-static {p1}, Lepson/print/WebviewActivity;->access$1400(Lepson/print/WebviewActivity;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    const-string v0, "SaveUrl"

    invoke-interface {p1, v0, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 459
    iget-object p1, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    invoke-static {p1}, Lepson/print/WebviewActivity;->access$1400(Lepson/print/WebviewActivity;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 461
    :cond_8
    iget-object p1, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    iget-object p1, p1, Lepson/print/WebviewActivity;->mStrictMode:Lepson/print/IprintStrictMode$StrictModeInterface;

    invoke-interface {p1}, Lepson/print/IprintStrictMode$StrictModeInterface;->restoreStatus()V

    .line 465
    :cond_9
    iget-object p1, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    invoke-static {p1}, Lepson/print/WebviewActivity;->access$400(Lepson/print/WebviewActivity;)Z

    move-result p1

    if-eqz p1, :cond_a

    iget-object p1, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    invoke-static {p1}, Lepson/print/WebviewActivity;->access$1500(Lepson/print/WebviewActivity;)Z

    move-result p1

    if-nez p1, :cond_a

    .line 466
    iget-object p1, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    invoke-static {p1}, Lepson/print/WebviewActivity;->access$1600(Lepson/print/WebviewActivity;)V

    :cond_a
    return-void
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 0

    .line 370
    iget-object p1, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    invoke-static {p1}, Lepson/print/WebviewActivity;->access$400(Lepson/print/WebviewActivity;)Z

    move-result p1

    const/4 p3, 0x0

    if-nez p1, :cond_0

    .line 371
    iget-object p1, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    invoke-static {p1}, Lepson/print/WebviewActivity;->access$500(Lepson/print/WebviewActivity;)Landroid/widget/LinearLayout;

    move-result-object p1

    invoke-virtual {p1, p3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 373
    :cond_0
    iget-object p1, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    invoke-virtual {p1}, Lepson/print/WebviewActivity;->showLoadingDialog()V

    .line 375
    :goto_0
    iget-object p1, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    invoke-static {p1, p2}, Lepson/print/WebviewActivity;->access$600(Lepson/print/WebviewActivity;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lepson/print/WebviewActivity;->setTextOnUrlEditText(Ljava/lang/String;)V

    .line 376
    iget-object p1, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    invoke-static {p1, p3}, Lepson/print/WebviewActivity;->access$702(Lepson/print/WebviewActivity;Z)Z

    .line 377
    iget-object p1, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    const/4 p2, 0x1

    invoke-static {p1, p2}, Lepson/print/WebviewActivity;->access$802(Lepson/print/WebviewActivity;Z)Z

    .line 378
    iget-object p1, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    invoke-virtual {p1, p3}, Lepson/print/WebviewActivity;->setClearButtonAppearance(I)V

    .line 379
    iget-object p1, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    invoke-static {p1, p3}, Lepson/print/WebviewActivity;->access$002(Lepson/print/WebviewActivity;Z)Z

    .line 382
    iget-object p1, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    invoke-virtual {p1}, Lepson/print/WebviewActivity;->invalidateOptionsMenu()V

    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 471
    iget-object p1, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    invoke-static {p1, p3}, Lepson/print/WebviewActivity;->access$1700(Lepson/print/WebviewActivity;Ljava/lang/String;)Landroid/app/AlertDialog;

    move-result-object p2

    iput-object p2, p1, Lepson/print/WebviewActivity;->mErrorDialog:Landroid/app/AlertDialog;

    .line 473
    :try_start_0
    iget-object p1, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    iget-object p1, p1, Lepson/print/WebviewActivity;->mErrorDialog:Landroid/app/AlertDialog;

    invoke-virtual {p1}, Landroid/app/AlertDialog;->show()V
    :try_end_0
    .catch Landroid/view/WindowManager$BadTokenException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 475
    invoke-virtual {p1}, Landroid/view/WindowManager$BadTokenException;->printStackTrace()V

    :goto_0
    return-void
.end method

.method public onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V
    .locals 2

    const-string p1, "WebviewActivity"

    .line 484
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SSL error: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lepson/print/Util/EPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 486
    iget-object p1, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    invoke-virtual {p1}, Lepson/print/WebviewActivity;->isFinishing()Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    .line 494
    :cond_0
    new-instance p1, Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    invoke-direct {p1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v0, 0x7f0700ad

    .line 495
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    const-string v0, "SSL error"

    .line 496
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 497
    invoke-virtual {p3}, Landroid/net/http/SslError;->getPrimaryError()I

    move-result p3

    invoke-virtual {p0, p3}, Lepson/print/WebviewActivity$3;->trasrateSslError(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const/4 p3, 0x0

    .line 498
    invoke-virtual {p1, p3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    const p3, 0x7f0e0482

    new-instance v0, Lepson/print/WebviewActivity$3$2;

    invoke-direct {v0, p0, p2}, Lepson/print/WebviewActivity$3$2;-><init>(Lepson/print/WebviewActivity$3;Landroid/webkit/SslErrorHandler;)V

    .line 499
    invoke-virtual {p1, p3, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    iget-object p3, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    .line 504
    invoke-virtual {p3}, Lepson/print/WebviewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    const v0, 0x7f0e0476

    invoke-virtual {p3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p3

    new-instance v0, Lepson/print/WebviewActivity$3$1;

    invoke-direct {v0, p0, p2}, Lepson/print/WebviewActivity$3$1;-><init>(Lepson/print/WebviewActivity$3;Landroid/webkit/SslErrorHandler;)V

    invoke-virtual {p1, p3, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 515
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void
.end method

.method public onScaleChanged(Landroid/webkit/WebView;FF)V
    .locals 0

    .line 556
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onScaleChanged(Landroid/webkit/WebView;FF)V

    .line 557
    iget-object p1, p0, Lepson/print/WebviewActivity$3;->this$0:Lepson/print/WebviewActivity;

    invoke-static {p1, p3}, Lepson/print/WebviewActivity;->access$1802(Lepson/print/WebviewActivity;F)F

    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method trasrateSslError(I)Ljava/lang/String;
    .locals 2

    .line 525
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v1, "primaryError = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    packed-switch p1, :pswitch_data_0

    .line 547
    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    goto :goto_0

    :pswitch_0
    const-string p1, "SSL_INVALID"

    .line 544
    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    :pswitch_1
    const-string p1, "SSL_DATE_INVALID "

    .line 541
    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    :pswitch_2
    const-string p1, "SSL_UNTRUSTED"

    .line 538
    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    :pswitch_3
    const-string p1, "SSL_IDMISMATCH"

    .line 535
    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    :pswitch_4
    const-string p1, "SSL_EXPIRED"

    .line 532
    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    :pswitch_5
    const-string p1, "SSL_NOTYETVALID"

    .line 529
    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 550
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.class Lepson/print/fileBrower$8;
.super Ljava/lang/Object;
.source "fileBrower.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/fileBrower;->createNewFolder()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/fileBrower;

.field final synthetic val$input:Landroid/widget/EditText;

.field final synthetic val$pathCurrent:Ljava/lang/String;


# direct methods
.method constructor <init>(Lepson/print/fileBrower;Landroid/widget/EditText;Ljava/lang/String;)V
    .locals 0

    .line 368
    iput-object p1, p0, Lepson/print/fileBrower$8;->this$0:Lepson/print/fileBrower;

    iput-object p2, p0, Lepson/print/fileBrower$8;->val$input:Landroid/widget/EditText;

    iput-object p3, p0, Lepson/print/fileBrower$8;->val$pathCurrent:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .line 371
    iget-object p1, p0, Lepson/print/fileBrower$8;->val$input:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 372
    iget-object p2, p0, Lepson/print/fileBrower$8;->this$0:Lepson/print/fileBrower;

    invoke-static {p2, p1}, Lepson/print/fileBrower;->access$400(Lepson/print/fileBrower;Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_2

    .line 373
    new-instance p2, Ljava/io/File;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lepson/print/fileBrower$8;->val$pathCurrent:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-string v0, "create file"

    .line 374
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lepson/print/fileBrower$8;->val$pathCurrent:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result p1

    if-nez p1, :cond_1

    .line 378
    :try_start_0
    invoke-virtual {p2}, Ljava/io/File;->mkdirs()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 379
    iget-object p1, p0, Lepson/print/fileBrower$8;->this$0:Lepson/print/fileBrower;

    new-instance p2, Ljava/io/File;

    iget-object v0, p0, Lepson/print/fileBrower$8;->val$pathCurrent:Ljava/lang/String;

    invoke-direct {p2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {p1, p2}, Lepson/print/fileBrower;->access$300(Lepson/print/fileBrower;Ljava/io/File;)V

    goto :goto_0

    .line 381
    :cond_0
    iget-object p1, p0, Lepson/print/fileBrower$8;->this$0:Lepson/print/fileBrower;

    invoke-static {p1}, Lepson/print/fileBrower;->access$500(Lepson/print/fileBrower;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 389
    :catch_0
    iget-object p1, p0, Lepson/print/fileBrower$8;->this$0:Lepson/print/fileBrower;

    invoke-static {p1}, Lepson/print/fileBrower;->access$500(Lepson/print/fileBrower;)V

    const-string p1, "exception"

    const-string p2, "2"

    .line 390
    invoke-static {p1, p2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 385
    :catch_1
    iget-object p1, p0, Lepson/print/fileBrower$8;->this$0:Lepson/print/fileBrower;

    invoke-static {p1}, Lepson/print/fileBrower;->access$500(Lepson/print/fileBrower;)V

    const-string p1, "exception"

    const-string p2, "1"

    .line 386
    invoke-static {p1, p2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 393
    :cond_1
    iget-object p1, p0, Lepson/print/fileBrower$8;->this$0:Lepson/print/fileBrower;

    invoke-static {p1}, Lepson/print/fileBrower;->access$500(Lepson/print/fileBrower;)V

    goto :goto_0

    .line 396
    :cond_2
    iget-object p1, p0, Lepson/print/fileBrower$8;->this$0:Lepson/print/fileBrower;

    invoke-static {p1}, Lepson/print/fileBrower;->access$500(Lepson/print/fileBrower;)V

    :goto_0
    return-void
.end method

.class Lepson/print/service/EpsonService$3;
.super Ljava/lang/Thread;
.source "EpsonService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lepson/print/service/EpsonService;->createECPrintingThread(Lepson/print/screen/PrintSetting$Kind;Lepson/print/EPImageList;)I
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lepson/print/service/EpsonService;

.field final synthetic val$epImageList:Lepson/print/EPImageList;

.field final synthetic val$kind:Lepson/print/screen/PrintSetting$Kind;


# direct methods
.method constructor <init>(Lepson/print/service/EpsonService;Lepson/print/screen/PrintSetting$Kind;Lepson/print/EPImageList;)V
    .locals 0

    .line 1698
    iput-object p1, p0, Lepson/print/service/EpsonService$3;->this$0:Lepson/print/service/EpsonService;

    iput-object p2, p0, Lepson/print/service/EpsonService$3;->val$kind:Lepson/print/screen/PrintSetting$Kind;

    iput-object p3, p0, Lepson/print/service/EpsonService$3;->val$epImageList:Lepson/print/EPImageList;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 29

    move-object/from16 v7, p0

    .line 1700
    invoke-static {}, Lepson/print/service/EpsonService;->access$1500()Ljava/lang/Object;

    move-result-object v8

    monitor-enter v8

    :try_start_0
    const-string v0, "EpsonService"

    const-string v1, "createECPrintingThread() called."

    .line 1703
    invoke-static {v0, v1}, Lcom/epson/mobilephone/common/EpLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1704
    new-instance v0, Lepson/print/screen/PrintSetting;

    iget-object v1, v7, Lepson/print/service/EpsonService$3;->this$0:Lepson/print/service/EpsonService;

    iget-object v2, v7, Lepson/print/service/EpsonService$3;->val$kind:Lepson/print/screen/PrintSetting$Kind;

    invoke-direct {v0, v1, v2}, Lepson/print/screen/PrintSetting;-><init>(Landroid/content/Context;Lepson/print/screen/PrintSetting$Kind;)V

    .line 1705
    invoke-virtual {v0}, Lepson/print/screen/PrintSetting;->loadSettings()V

    .line 1707
    iget v6, v0, Lepson/print/screen/PrintSetting;->paperSizeValue:I

    .line 1708
    iget v13, v0, Lepson/print/screen/PrintSetting;->paperTypeValue:I

    .line 1709
    iget v5, v0, Lepson/print/screen/PrintSetting;->layoutValue:I

    .line 1710
    iget v4, v0, Lepson/print/screen/PrintSetting;->qualityValue:I

    .line 1711
    iget v1, v0, Lepson/print/screen/PrintSetting;->paperSourceValue:I

    .line 1712
    iget v3, v0, Lepson/print/screen/PrintSetting;->colorValue:I

    .line 1713
    iget v2, v0, Lepson/print/screen/PrintSetting;->copiesValue:I

    .line 1715
    iget v15, v0, Lepson/print/screen/PrintSetting;->brightnessValue:I

    .line 1716
    iget v14, v0, Lepson/print/screen/PrintSetting;->contrastValue:I

    .line 1717
    iget v12, v0, Lepson/print/screen/PrintSetting;->saturationValue:I

    .line 1718
    iget v11, v0, Lepson/print/screen/PrintSetting;->duplexValue:I

    .line 1719
    iget v10, v0, Lepson/print/screen/PrintSetting;->feedDirectionValue:I

    const/16 v25, 0x0

    .line 1727
    iget-object v9, v7, Lepson/print/service/EpsonService$3;->this$0:Lepson/print/service/EpsonService;

    move/from16 v26, v3

    const/4 v3, 0x1

    invoke-static {v9, v3}, Lepson/print/service/EpsonService;->access$1702(Lepson/print/service/EpsonService;Z)Z

    const/4 v9, 0x0

    .line 1728
    invoke-static {v9}, Lepson/print/service/EpsonService;->access$1902(Z)Z

    .line 1732
    iget-object v3, v7, Lepson/print/service/EpsonService$3;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v3}, Lepson/provider/SharedPreferencesProvider;->getInstace(Landroid/content/Context;)Lepson/provider/SharedPreferencesProvider$SharedPreferencesMulti;

    move-result-object v3

    const-string v9, "SOURCE_TYPE"

    move/from16 v17, v10

    const/4 v10, 0x2

    .line 1733
    invoke-virtual {v3, v9, v10}, Lepson/provider/SharedPreferencesProvider$SharedPreferencesMulti;->getInt(Ljava/lang/String;I)I

    move-result v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_14

    .line 1738
    :try_start_1
    iget-object v9, v7, Lepson/print/service/EpsonService$3;->this$0:Lepson/print/service/EpsonService;

    const/4 v10, 0x0

    invoke-static {v9, v10}, Lcom/epson/mobilephone/common/wifidirect/WiFiDirectManager;->setPriorityToSimpleAP(Landroid/content/Context;Z)V

    .line 1740
    iget-object v9, v7, Lepson/print/service/EpsonService$3;->val$epImageList:Lepson/print/EPImageList;

    invoke-virtual {v9, v10}, Lepson/print/EPImageList;->get(I)Lepson/print/EPImage;

    move-result-object v9
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_12
    .catchall {:try_start_1 .. :try_end_1} :catchall_11

    const/4 v10, 0x3

    move/from16 v20, v1

    const/4 v1, 0x2

    if-eq v3, v1, :cond_9

    if-ne v3, v10, :cond_0

    goto/16 :goto_0

    :cond_0
    const/4 v1, 0x1

    if-ne v3, v1, :cond_8

    .line 1871
    :try_start_2
    iget-object v4, v7, Lepson/print/service/EpsonService$3;->val$epImageList:Lepson/print/EPImageList;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    const/4 v5, 0x0

    :try_start_3
    invoke-virtual {v4, v5}, Lepson/print/EPImageList;->get(I)Lepson/print/EPImage;

    .line 1875
    iget-object v4, v7, Lepson/print/service/EpsonService$3;->this$0:Lepson/print/service/EpsonService;

    invoke-virtual {v4, v5}, Lepson/print/service/EpsonService;->epsNotifyPage(I)V

    .line 1876
    invoke-static {}, Lepson/print/service/EpsonService;->access$1900()Z

    move-result v4

    if-eq v4, v1, :cond_7

    .line 1883
    iget-object v1, v7, Lepson/print/service/EpsonService$3;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v1}, Lepson/print/service/EpsonService;->access$2200(Lepson/print/service/EpsonService;)Lepson/print/ecclient/EpsJobAttrib;

    move-result-object v1

    iput v2, v1, Lepson/print/ecclient/EpsJobAttrib;->mCopies:I

    .line 1884
    iget-object v1, v7, Lepson/print/service/EpsonService$3;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v1}, Lepson/print/service/EpsonService;->access$2900(Lepson/print/service/EpsonService;)Lepson/print/service/IEpsonService$Stub;

    move-result-object v1

    invoke-virtual {v1}, Lepson/print/service/IEpsonService$Stub;->EpsonConnectChangePrintSetting()I

    move-result v9
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    if-nez v9, :cond_6

    .line 1888
    :try_start_4
    iget-object v1, v7, Lepson/print/service/EpsonService$3;->this$0:Lepson/print/service/EpsonService;

    const/16 v2, 0x32

    invoke-virtual {v1, v2}, Lepson/print/service/EpsonService;->epsNotifyPage(I)V

    .line 1889
    invoke-static {}, Lepson/print/service/EpsonService;->access$1900()Z

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_5

    .line 1896
    iget v1, v0, Lepson/print/screen/PrintSetting;->startValue:I

    .line 1897
    iget v2, v0, Lepson/print/screen/PrintSetting;->endValue:I

    .line 1900
    iget-object v4, v7, Lepson/print/service/EpsonService$3;->this$0:Lepson/print/service/EpsonService;

    const-string v6, "PREFS_EPSON_CONNECT"

    invoke-static {v4, v6}, Lepson/provider/SharedPreferencesProvider;->getInstace(Landroid/content/Context;Ljava/lang/String;)Lepson/provider/SharedPreferencesProvider$SharedPreferencesMulti;

    move-result-object v4

    const-string v6, "ENABLE_SHOW_PREVIEW"

    const/4 v10, 0x1

    .line 1901
    invoke-virtual {v4, v6, v10}, Lepson/provider/SharedPreferencesProvider$SharedPreferencesMulti;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    .line 1902
    iget-boolean v0, v0, Lepson/print/screen/PrintSetting;->printAll:Z

    if-eqz v4, :cond_1

    if-ne v0, v10, :cond_2

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 1909
    :cond_2
    invoke-static {}, Lepson/print/service/EpsonService;->access$1900()Z

    move-result v0

    if-eq v0, v10, :cond_4

    .line 1917
    iget-object v0, v7, Lepson/print/service/EpsonService$3;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v0}, Lepson/print/service/EpsonService;->access$2900(Lepson/print/service/EpsonService;)Lepson/print/service/IEpsonService$Stub;

    move-result-object v0

    invoke-virtual {v0, v1, v2}, Lepson/print/service/IEpsonService$Stub;->EpsonConnectStartPrint(II)I

    move-result v9

    if-nez v9, :cond_3

    .line 1922
    iget-object v0, v7, Lepson/print/service/EpsonService$3;->this$0:Lepson/print/service/EpsonService;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Lepson/print/service/EpsonService;->epsNotifyPage(I)V

    move/from16 v28, v3

    move v1, v9

    const/4 v9, 0x1

    const/4 v15, 0x1

    goto/16 :goto_b

    .line 1919
    :cond_3
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    throw v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1912
    :cond_4
    :try_start_5
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    throw v0

    .line 1892
    :cond_5
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    throw v0
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :catchall_0
    move-exception v0

    move-object v1, v0

    move v2, v3

    const/4 v3, 0x1

    goto :goto_2

    :catch_0
    move-exception v0

    move v2, v3

    const/4 v3, 0x1

    goto :goto_4

    .line 1886
    :cond_6
    :try_start_6
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    throw v0
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 1879
    :cond_7
    :try_start_7
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    throw v0
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    :catchall_1
    move-exception v0

    const/4 v5, 0x0

    goto :goto_1

    :catch_1
    move-exception v0

    const/4 v5, 0x0

    goto :goto_3

    :cond_8
    const/4 v5, 0x0

    move/from16 v28, v3

    const/4 v1, 0x0

    const/4 v9, 0x1

    const/4 v15, 0x1

    goto/16 :goto_b

    :cond_9
    :goto_0
    const/16 v1, 0x64

    const/16 v16, 0x0

    .line 1745
    :try_start_8
    iget-object v0, v9, Lepson/print/EPImage;->loadImageFileName:Ljava/lang/String;
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_12
    .catchall {:try_start_8 .. :try_end_8} :catchall_11

    if-ne v3, v10, :cond_a

    .line 1748
    :try_start_9
    iget-object v0, v9, Lepson/print/EPImage;->webUrl:Ljava/lang/String;
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    goto :goto_5

    :catchall_2
    move-exception v0

    :goto_1
    move-object v1, v0

    move v2, v3

    const/4 v3, 0x1

    const/4 v9, 0x0

    :goto_2
    const/4 v15, 0x1

    goto/16 :goto_28

    :catch_2
    move-exception v0

    :goto_3
    move v2, v3

    const/4 v3, 0x1

    const/4 v9, 0x0

    :goto_4
    const/4 v15, 0x1

    goto/16 :goto_1f

    .line 1752
    :cond_a
    :goto_5
    :try_start_a
    iget-object v9, v7, Lepson/print/service/EpsonService$3;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v9}, Lepson/print/service/EpsonService;->access$2900(Lepson/print/service/EpsonService;)Lepson/print/service/IEpsonService$Stub;

    move-result-object v9

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/4 v10, 0x0

    move/from16 v27, v2

    move/from16 v21, v17

    const/4 v2, 0x0

    move v10, v3

    move/from16 v22, v11

    move-object v11, v0

    move v0, v12

    move v12, v6

    move/from16 v19, v14

    move v14, v5

    move/from16 v18, v15

    move v15, v4

    move/from16 v16, v20

    move/from16 v17, v26

    move/from16 v20, v0

    invoke-virtual/range {v9 .. v24}, Lepson/print/service/IEpsonService$Stub;->EpsonConnectCreateJob(ILjava/lang/String;IIIIIIIIIIIII)I

    move-result v19
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_12
    .catchall {:try_start_a .. :try_end_a} :catchall_11

    if-nez v19, :cond_1a

    .line 1761
    :try_start_b
    invoke-static {}, Lepson/print/service/EpsonService;->access$1900()Z

    move-result v0

    const/4 v9, 0x1

    if-eq v0, v9, :cond_19

    .line 1770
    iget-object v9, v7, Lepson/print/service/EpsonService$3;->this$0:Lepson/print/service/EpsonService;

    iget-object v10, v7, Lepson/print/service/EpsonService$3;->val$epImageList:Lepson/print/EPImageList;

    const/4 v11, 0x0

    const/4 v14, 0x0

    const/16 v16, 0x0

    const/16 v18, 0x0

    move v12, v6

    move/from16 v13, v26

    move v15, v5

    move/from16 v17, v4

    invoke-virtual/range {v9 .. v18}, Lepson/print/service/EpsonService;->createPrintImage(Lepson/print/EPImageList;IIIIIIII)Ljava/lang/String;
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_10
    .catchall {:try_start_b .. :try_end_b} :catchall_f

    const/4 v9, 0x0

    .line 1772
    :goto_6
    :try_start_c
    iget-object v0, v7, Lepson/print/service/EpsonService$3;->val$epImageList:Lepson/print/EPImageList;

    invoke-virtual {v0}, Lepson/print/EPImageList;->size()I

    move-result v0
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_e
    .catchall {:try_start_c .. :try_end_c} :catchall_d

    if-ge v9, v0, :cond_12

    .line 1773
    :try_start_d
    iget-object v0, v7, Lepson/print/service/EpsonService$3;->val$epImageList:Lepson/print/EPImageList;

    invoke-virtual {v0, v9}, Lepson/print/EPImageList;->get(I)Lepson/print/EPImage;

    move-result-object v0
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_8
    .catchall {:try_start_d .. :try_end_d} :catchall_8

    .line 1776
    :try_start_e
    iget-object v10, v7, Lepson/print/service/EpsonService$3;->this$0:Lepson/print/service/EpsonService;

    invoke-virtual {v10, v2}, Lepson/print/service/EpsonService;->epsNotifyPage(I)V

    .line 1778
    invoke-static {}, Lepson/print/service/EpsonService;->access$1900()Z

    move-result v10

    const/4 v11, 0x1

    if-eq v10, v11, :cond_11

    .line 1785
    iget-object v10, v7, Lepson/print/service/EpsonService$3;->this$0:Lepson/print/service/EpsonService;

    add-int/lit8 v12, v9, 0x1

    invoke-static {v10, v12}, Lepson/print/service/EpsonService;->access$3002(Lepson/print/service/EpsonService;I)I

    .line 1786
    new-instance v10, Lepson/print/service/EpsonService$3$1;
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_7
    .catchall {:try_start_e .. :try_end_e} :catchall_7

    const/16 v13, 0x64

    move-object v1, v10

    move/from16 v14, v27

    const/4 v15, 0x0

    move-object/from16 v2, p0

    move/from16 v28, v3

    move/from16 v11, v26

    const/4 v15, 0x1

    move v3, v6

    move/from16 v17, v4

    move v4, v11

    move/from16 v18, v5

    move/from16 v19, v6

    move/from16 v6, v17

    :try_start_f
    invoke-direct/range {v1 .. v6}, Lepson/print/service/EpsonService$3$1;-><init>(Lepson/print/service/EpsonService$3;IIII)V
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_6
    .catchall {:try_start_f .. :try_end_f} :catchall_6

    .line 1802
    :try_start_10
    iget-object v1, v7, Lepson/print/service/EpsonService$3;->val$epImageList:Lepson/print/EPImageList;

    invoke-virtual {v1}, Lepson/print/EPImageList;->size()I

    move-result v1

    if-ge v12, v1, :cond_b

    .line 1803
    invoke-virtual {v10}, Ljava/lang/Thread;->start()V

    .line 1805
    :cond_b
    iget-object v1, v7, Lepson/print/service/EpsonService$3;->this$0:Lepson/print/service/EpsonService;

    const/16 v2, 0x19

    invoke-virtual {v1, v2}, Lepson/print/service/EpsonService;->epsNotifyPage(I)V

    .line 1812
    new-instance v1, Lepson/print/EPImageCreator;

    iget-object v2, v7, Lepson/print/service/EpsonService$3;->this$0:Lepson/print/service/EpsonService;

    invoke-virtual {v2}, Lepson/print/service/EpsonService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lepson/print/EPImageCreator;-><init>(Landroid/content/Context;)V

    .line 1813
    invoke-virtual {v1, v0, v13}, Lepson/print/EPImageCreator;->createJpegImage(Lepson/print/EPImage;I)I

    move-result v1
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_5
    .catchall {:try_start_10 .. :try_end_10} :catchall_5

    if-nez v1, :cond_10

    .line 1819
    :try_start_11
    iget-object v2, v7, Lepson/print/service/EpsonService$3;->this$0:Lepson/print/service/EpsonService;

    const/16 v3, 0x32

    invoke-virtual {v2, v3}, Lepson/print/service/EpsonService;->epsNotifyPage(I)V

    .line 1820
    invoke-static {}, Lepson/print/service/EpsonService;->access$1900()Z

    move-result v2

    if-eq v2, v15, :cond_f

    .line 1827
    iget-object v2, v7, Lepson/print/service/EpsonService$3;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v2}, Lepson/print/service/EpsonService;->access$2900(Lepson/print/service/EpsonService;)Lepson/print/service/IEpsonService$Stub;

    move-result-object v2

    iget-object v0, v0, Lepson/print/EPImage;->decodeImageFileName:Ljava/lang/String;

    invoke-virtual {v2, v0, v12}, Lepson/print/service/IEpsonService$Stub;->EpsonConnectUploadFile(Ljava/lang/String;I)I

    move-result v1

    if-nez v1, :cond_e

    .line 1831
    iget-object v0, v7, Lepson/print/service/EpsonService$3;->this$0:Lepson/print/service/EpsonService;

    const/16 v2, 0x4b

    invoke-virtual {v0, v2}, Lepson/print/service/EpsonService;->epsNotifyPage(I)V

    .line 1832
    invoke-static {}, Lepson/print/service/EpsonService;->access$1900()Z

    move-result v0

    if-eq v0, v15, :cond_d

    .line 1840
    invoke-virtual {v10}, Ljava/lang/Thread;->join()V

    .line 1842
    iget-object v0, v7, Lepson/print/service/EpsonService$3;->this$0:Lepson/print/service/EpsonService;

    invoke-virtual {v0, v13}, Lepson/print/service/EpsonService;->epsNotifyPage(I)V

    .line 1843
    invoke-static {}, Lepson/print/service/EpsonService;->access$1900()Z

    move-result v0
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_3
    .catchall {:try_start_11 .. :try_end_11} :catchall_3

    if-eq v0, v15, :cond_c

    move-object/from16 v25, v10

    move/from16 v26, v11

    move v9, v12

    move/from16 v27, v14

    move/from16 v4, v17

    move/from16 v5, v18

    move/from16 v6, v19

    move/from16 v3, v28

    const/4 v2, 0x0

    move/from16 v19, v1

    const/16 v1, 0x64

    goto/16 :goto_6

    .line 1846
    :cond_c
    :try_start_12
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    throw v0

    .line 1835
    :cond_d
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    throw v0
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_5
    .catchall {:try_start_12 .. :try_end_12} :catchall_5

    .line 1829
    :cond_e
    :try_start_13
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    throw v0
    :try_end_13
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_3
    .catchall {:try_start_13 .. :try_end_13} :catchall_3

    .line 1823
    :cond_f
    :try_start_14
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    throw v0
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_5
    .catchall {:try_start_14 .. :try_end_14} :catchall_5

    :catchall_3
    move-exception v0

    move v3, v9

    move-object/from16 v25, v10

    move/from16 v2, v28

    goto/16 :goto_13

    :catch_3
    move-exception v0

    move v3, v9

    move-object/from16 v25, v10

    move/from16 v2, v28

    goto/16 :goto_15

    :cond_10
    const/16 v1, -0x4b0

    .line 1817
    :try_start_15
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    throw v0
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_4
    .catchall {:try_start_15 .. :try_end_15} :catchall_4

    :catchall_4
    move-exception v0

    move-object v1, v0

    move v3, v9

    move-object/from16 v25, v10

    move/from16 v2, v28

    const/16 v9, -0x4b0

    goto/16 :goto_28

    :catch_4
    move-exception v0

    move v3, v9

    move-object/from16 v25, v10

    move/from16 v2, v28

    const/16 v9, -0x4b0

    goto/16 :goto_1f

    :catchall_5
    move-exception v0

    move-object v1, v0

    move v3, v9

    move-object/from16 v25, v10

    goto :goto_7

    :catch_5
    move-exception v0

    move v3, v9

    move-object/from16 v25, v10

    goto :goto_8

    :cond_11
    move/from16 v28, v3

    const/4 v15, 0x1

    .line 1781
    :try_start_16
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    throw v0
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_16} :catch_6
    .catchall {:try_start_16 .. :try_end_16} :catchall_6

    :catchall_6
    move-exception v0

    move-object v1, v0

    move v3, v9

    :goto_7
    move/from16 v2, v28

    goto/16 :goto_1c

    :catch_6
    move-exception v0

    move v3, v9

    :goto_8
    move/from16 v2, v28

    goto/16 :goto_1e

    :catchall_7
    move-exception v0

    const/4 v15, 0x1

    move-object v1, v0

    move v2, v3

    :goto_9
    move v3, v9

    goto/16 :goto_1c

    :catch_7
    move-exception v0

    const/4 v15, 0x1

    move v2, v3

    :goto_a
    move v3, v9

    goto/16 :goto_1e

    :catchall_8
    move-exception v0

    const/4 v15, 0x1

    move-object v1, v0

    move v2, v3

    goto/16 :goto_17

    :catch_8
    move-exception v0

    const/4 v15, 0x1

    move v2, v3

    goto/16 :goto_18

    :cond_12
    move/from16 v28, v3

    move/from16 v14, v27

    const/4 v15, 0x1

    .line 1851
    :try_start_17
    iget-object v0, v7, Lepson/print/service/EpsonService$3;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v0}, Lepson/print/service/EpsonService;->access$2200(Lepson/print/service/EpsonService;)Lepson/print/ecclient/EpsJobAttrib;

    move-result-object v0

    iput v14, v0, Lepson/print/ecclient/EpsJobAttrib;->mCopies:I

    .line 1852
    iget-object v0, v7, Lepson/print/service/EpsonService$3;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v0}, Lepson/print/service/EpsonService;->access$2900(Lepson/print/service/EpsonService;)Lepson/print/service/IEpsonService$Stub;

    move-result-object v0

    invoke-virtual {v0}, Lepson/print/service/IEpsonService$Stub;->EpsonConnectChangePrintSetting()I

    move-result v1
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_17} :catch_d
    .catchall {:try_start_17 .. :try_end_17} :catchall_c

    if-nez v1, :cond_18

    .line 1857
    :try_start_18
    invoke-static {}, Lepson/print/service/EpsonService;->access$1900()Z

    move-result v0

    if-eq v0, v15, :cond_17

    .line 1864
    iget-object v0, v7, Lepson/print/service/EpsonService$3;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v0}, Lepson/print/service/EpsonService;->access$2900(Lepson/print/service/EpsonService;)Lepson/print/service/IEpsonService$Stub;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v2}, Lepson/print/service/IEpsonService$Stub;->EpsonConnectStartPrint(II)I

    move-result v1
    :try_end_18
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_18} :catch_b
    .catchall {:try_start_18 .. :try_end_18} :catchall_a

    if-nez v1, :cond_16

    :goto_b
    if-eqz v25, :cond_13

    .line 1945
    :try_start_19
    invoke-virtual/range {v25 .. v25}, Ljava/lang/Thread;->join()V

    move/from16 v2, v28

    goto :goto_c

    :cond_13
    move/from16 v2, v28

    :goto_c
    if-eq v2, v15, :cond_14

    .line 1952
    iget-object v0, v7, Lepson/print/service/EpsonService$3;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v0}, Lepson/print/service/EpsonService;->access$2900(Lepson/print/service/EpsonService;)Lepson/print/service/IEpsonService$Stub;

    move-result-object v0

    invoke-virtual {v0}, Lepson/print/service/IEpsonService$Stub;->EpsonConnectEndJob()I

    goto :goto_d

    :catch_9
    move-exception v0

    goto :goto_f

    :cond_14
    :goto_d
    if-ne v2, v15, :cond_15

    .line 1957
    iget-object v0, v7, Lepson/print/service/EpsonService$3;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v0, v15}, Lcom/epson/iprint/prtlogger/Analytics;->savePrintInfo(Landroid/content/Context;I)V

    goto :goto_e

    .line 1959
    :cond_15
    iget-object v0, v7, Lepson/print/service/EpsonService$3;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v0, v9}, Lcom/epson/iprint/prtlogger/Analytics;->savePrintInfo(Landroid/content/Context;I)V

    .line 1963
    :goto_e
    invoke-static {v1}, Lepson/print/service/EpsonService;->onNotifyEndJob(I)V
    :try_end_19
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_19} :catch_9
    .catchall {:try_start_19 .. :try_end_19} :catchall_14

    goto :goto_10

    .line 1966
    :goto_f
    :try_start_1a
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1969
    :goto_10
    iget-object v0, v7, Lepson/print/service/EpsonService$3;->this$0:Lepson/print/service/EpsonService;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lepson/print/service/EpsonService;->access$1702(Lepson/print/service/EpsonService;Z)Z

    .line 1970
    :goto_11
    invoke-static {v1}, Lepson/print/service/EpsonService;->access$1902(Z)Z
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_14

    goto/16 :goto_26

    :cond_16
    move/from16 v2, v28

    .line 1866
    :try_start_1b
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    throw v0
    :try_end_1b
    .catch Ljava/lang/Exception; {:try_start_1b .. :try_end_1b} :catch_c
    .catchall {:try_start_1b .. :try_end_1b} :catchall_b

    :cond_17
    move/from16 v2, v28

    .line 1860
    :try_start_1c
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    throw v0
    :try_end_1c
    .catch Ljava/lang/Exception; {:try_start_1c .. :try_end_1c} :catch_a
    .catchall {:try_start_1c .. :try_end_1c} :catchall_9

    :catchall_9
    move-exception v0

    move-object v1, v0

    goto/16 :goto_9

    :catch_a
    move-exception v0

    goto/16 :goto_a

    :catchall_a
    move-exception v0

    move/from16 v2, v28

    :goto_12
    move v3, v9

    :goto_13
    move v9, v1

    goto/16 :goto_27

    :catch_b
    move-exception v0

    move/from16 v2, v28

    :goto_14
    move v3, v9

    :goto_15
    move v9, v1

    goto/16 :goto_1f

    :cond_18
    move/from16 v2, v28

    .line 1854
    :try_start_1d
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    throw v0
    :try_end_1d
    .catch Ljava/lang/Exception; {:try_start_1d .. :try_end_1d} :catch_c
    .catchall {:try_start_1d .. :try_end_1d} :catchall_b

    :catchall_b
    move-exception v0

    goto :goto_12

    :catch_c
    move-exception v0

    goto :goto_14

    :catchall_c
    move-exception v0

    move/from16 v2, v28

    goto :goto_16

    :catch_d
    move-exception v0

    move/from16 v2, v28

    goto :goto_18

    :catchall_d
    move-exception v0

    move v2, v3

    const/4 v15, 0x1

    :goto_16
    move-object v1, v0

    :goto_17
    move v3, v9

    move/from16 v9, v19

    goto/16 :goto_28

    :catch_e
    move-exception v0

    move v2, v3

    const/4 v15, 0x1

    :goto_18
    move v3, v9

    move/from16 v9, v19

    goto :goto_1f

    :cond_19
    move v2, v3

    const/4 v15, 0x1

    .line 1764
    :try_start_1e
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    throw v0
    :try_end_1e
    .catch Ljava/lang/Exception; {:try_start_1e .. :try_end_1e} :catch_f
    .catchall {:try_start_1e .. :try_end_1e} :catchall_e

    :catchall_e
    move-exception v0

    goto :goto_1b

    :catch_f
    move-exception v0

    goto :goto_1d

    :catchall_f
    move-exception v0

    move v2, v3

    const/4 v15, 0x1

    :goto_19
    move-object v1, v0

    move/from16 v9, v19

    const/4 v3, 0x1

    goto/16 :goto_28

    :catch_10
    move-exception v0

    move v2, v3

    const/4 v15, 0x1

    :goto_1a
    move/from16 v9, v19

    const/4 v3, 0x1

    goto :goto_1f

    :cond_1a
    move v2, v3

    const/4 v15, 0x1

    .line 1758
    :try_start_1f
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    throw v0
    :try_end_1f
    .catch Ljava/lang/Exception; {:try_start_1f .. :try_end_1f} :catch_11
    .catchall {:try_start_1f .. :try_end_1f} :catchall_10

    :catchall_10
    move-exception v0

    goto :goto_19

    :catch_11
    move-exception v0

    goto :goto_1a

    :catchall_11
    move-exception v0

    move v2, v3

    const/4 v15, 0x1

    :goto_1b
    move-object v1, v0

    const/4 v3, 0x1

    :goto_1c
    const/4 v9, 0x0

    goto/16 :goto_28

    :catch_12
    move-exception v0

    move v2, v3

    const/4 v15, 0x1

    :goto_1d
    const/4 v3, 0x1

    :goto_1e
    const/4 v9, 0x0

    .line 1925
    :goto_1f
    :try_start_20
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1928
    invoke-static {}, Lepson/print/service/EpsonService;->access$1000()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1
    :try_end_20
    .catchall {:try_start_20 .. :try_end_20} :catchall_13

    .line 1929
    :try_start_21
    sget-object v0, Lepson/print/service/EpsonService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v4
    :try_end_21
    .catchall {:try_start_21 .. :try_end_21} :catchall_12

    const/4 v5, 0x0

    :goto_20
    if-ge v5, v4, :cond_1b

    .line 1932
    :try_start_22
    sget-object v0, Lepson/print/service/EpsonService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, v5}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lepson/print/service/IEpsonServiceCallback;

    const/4 v6, 0x0

    invoke-interface {v0, v6, v9, v6}, Lepson/print/service/IEpsonServiceCallback;->onNotifyError(IIZ)V
    :try_end_22
    .catch Landroid/os/RemoteException; {:try_start_22 .. :try_end_22} :catch_13
    .catchall {:try_start_22 .. :try_end_22} :catchall_12

    goto :goto_21

    :catch_13
    move-exception v0

    :try_start_23
    const-string v6, "EpsonService"

    .line 1936
    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Lepson/print/Util/EPLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_21
    add-int/lit8 v5, v5, 0x1

    goto :goto_20

    .line 1939
    :cond_1b
    sget-object v0, Lepson/print/service/EpsonService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 1940
    monitor-exit v1
    :try_end_23
    .catchall {:try_start_23 .. :try_end_23} :catchall_12

    if-eqz v25, :cond_1c

    .line 1945
    :try_start_24
    invoke-virtual/range {v25 .. v25}, Ljava/lang/Thread;->join()V

    :cond_1c
    if-eq v2, v15, :cond_1d

    .line 1952
    iget-object v0, v7, Lepson/print/service/EpsonService$3;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v0}, Lepson/print/service/EpsonService;->access$2900(Lepson/print/service/EpsonService;)Lepson/print/service/IEpsonService$Stub;

    move-result-object v0

    invoke-virtual {v0}, Lepson/print/service/IEpsonService$Stub;->EpsonConnectEndJob()I

    goto :goto_22

    :catch_14
    move-exception v0

    goto :goto_24

    :cond_1d
    :goto_22
    if-ne v2, v15, :cond_1e

    .line 1957
    iget-object v0, v7, Lepson/print/service/EpsonService$3;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v0, v15}, Lcom/epson/iprint/prtlogger/Analytics;->savePrintInfo(Landroid/content/Context;I)V

    goto :goto_23

    .line 1959
    :cond_1e
    iget-object v0, v7, Lepson/print/service/EpsonService$3;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v0, v3}, Lcom/epson/iprint/prtlogger/Analytics;->savePrintInfo(Landroid/content/Context;I)V

    .line 1963
    :goto_23
    invoke-static {v9}, Lepson/print/service/EpsonService;->onNotifyEndJob(I)V
    :try_end_24
    .catch Ljava/lang/Exception; {:try_start_24 .. :try_end_24} :catch_14
    .catchall {:try_start_24 .. :try_end_24} :catchall_14

    goto :goto_25

    .line 1966
    :goto_24
    :try_start_25
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1969
    :goto_25
    iget-object v0, v7, Lepson/print/service/EpsonService$3;->this$0:Lepson/print/service/EpsonService;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lepson/print/service/EpsonService;->access$1702(Lepson/print/service/EpsonService;Z)Z

    goto/16 :goto_11

    .line 1972
    :goto_26
    monitor-exit v8
    :try_end_25
    .catchall {:try_start_25 .. :try_end_25} :catchall_14

    return-void

    :catchall_12
    move-exception v0

    .line 1940
    :try_start_26
    monitor-exit v1
    :try_end_26
    .catchall {:try_start_26 .. :try_end_26} :catchall_12

    :try_start_27
    throw v0
    :try_end_27
    .catchall {:try_start_27 .. :try_end_27} :catchall_13

    :catchall_13
    move-exception v0

    :goto_27
    move-object v1, v0

    :goto_28
    if-eqz v25, :cond_1f

    .line 1945
    :try_start_28
    invoke-virtual/range {v25 .. v25}, Ljava/lang/Thread;->join()V

    :cond_1f
    if-eq v2, v15, :cond_20

    .line 1952
    iget-object v0, v7, Lepson/print/service/EpsonService$3;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v0}, Lepson/print/service/EpsonService;->access$2900(Lepson/print/service/EpsonService;)Lepson/print/service/IEpsonService$Stub;

    move-result-object v0

    invoke-virtual {v0}, Lepson/print/service/IEpsonService$Stub;->EpsonConnectEndJob()I

    goto :goto_29

    :catch_15
    move-exception v0

    goto :goto_2b

    :cond_20
    :goto_29
    if-ne v2, v15, :cond_21

    .line 1957
    iget-object v0, v7, Lepson/print/service/EpsonService$3;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v0, v15}, Lcom/epson/iprint/prtlogger/Analytics;->savePrintInfo(Landroid/content/Context;I)V

    goto :goto_2a

    .line 1959
    :cond_21
    iget-object v0, v7, Lepson/print/service/EpsonService$3;->this$0:Lepson/print/service/EpsonService;

    invoke-static {v0, v3}, Lcom/epson/iprint/prtlogger/Analytics;->savePrintInfo(Landroid/content/Context;I)V

    .line 1963
    :goto_2a
    invoke-static {v9}, Lepson/print/service/EpsonService;->onNotifyEndJob(I)V
    :try_end_28
    .catch Ljava/lang/Exception; {:try_start_28 .. :try_end_28} :catch_15
    .catchall {:try_start_28 .. :try_end_28} :catchall_14

    goto :goto_2c

    .line 1966
    :goto_2b
    :try_start_29
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1969
    :goto_2c
    iget-object v0, v7, Lepson/print/service/EpsonService$3;->this$0:Lepson/print/service/EpsonService;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lepson/print/service/EpsonService;->access$1702(Lepson/print/service/EpsonService;Z)Z

    .line 1970
    invoke-static {v2}, Lepson/print/service/EpsonService;->access$1902(Z)Z

    throw v1

    :catchall_14
    move-exception v0

    .line 1972
    monitor-exit v8
    :try_end_29
    .catchall {:try_start_29 .. :try_end_29} :catchall_14

    throw v0
.end method

.class Lepson/print/service/CreatePrintImageThread$LocalImageCreator;
.super Ljava/lang/Object;
.source "CreatePrintImageThread.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/service/CreatePrintImageThread;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "LocalImageCreator"
.end annotation


# instance fields
.field mColor:I

.field mDate:I

.field mDuplex:I

.field mLayout:I

.field mLayoutMulti:I

.field mPaperSize:I

.field mQuality:I


# direct methods
.method public constructor <init>(IIIIIII)V
    .locals 0

    .line 143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144
    iput p1, p0, Lepson/print/service/CreatePrintImageThread$LocalImageCreator;->mPaperSize:I

    .line 145
    iput p2, p0, Lepson/print/service/CreatePrintImageThread$LocalImageCreator;->mColor:I

    .line 146
    iput p3, p0, Lepson/print/service/CreatePrintImageThread$LocalImageCreator;->mDuplex:I

    .line 147
    iput p4, p0, Lepson/print/service/CreatePrintImageThread$LocalImageCreator;->mLayout:I

    .line 148
    iput p5, p0, Lepson/print/service/CreatePrintImageThread$LocalImageCreator;->mLayoutMulti:I

    .line 149
    iput p6, p0, Lepson/print/service/CreatePrintImageThread$LocalImageCreator;->mQuality:I

    .line 150
    iput p7, p0, Lepson/print/service/CreatePrintImageThread$LocalImageCreator;->mDate:I

    return-void
.end method


# virtual methods
.method public createImage(Lepson/print/service/PrintService;Lepson/print/EPImageList;I)Ljava/lang/String;
    .locals 10

    .line 154
    iget v3, p0, Lepson/print/service/CreatePrintImageThread$LocalImageCreator;->mPaperSize:I

    iget v4, p0, Lepson/print/service/CreatePrintImageThread$LocalImageCreator;->mColor:I

    iget v5, p0, Lepson/print/service/CreatePrintImageThread$LocalImageCreator;->mDuplex:I

    iget v6, p0, Lepson/print/service/CreatePrintImageThread$LocalImageCreator;->mLayout:I

    iget v7, p0, Lepson/print/service/CreatePrintImageThread$LocalImageCreator;->mLayoutMulti:I

    iget v8, p0, Lepson/print/service/CreatePrintImageThread$LocalImageCreator;->mQuality:I

    iget v9, p0, Lepson/print/service/CreatePrintImageThread$LocalImageCreator;->mDate:I

    move-object v0, p1

    move-object v1, p2

    move v2, p3

    invoke-interface/range {v0 .. v9}, Lepson/print/service/PrintService;->createPrintImage(Lepson/print/EPImageList;IIIIIIII)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.class Lepson/print/service/PdfRenderThread;
.super Ljava/lang/Thread;
.source "PdfRenderThread.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "PdfRenderThread"


# instance fields
.field private mDeque:Ljava/util/concurrent/LinkedBlockingDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/LinkedBlockingDeque<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mEpImageList:Lepson/print/EPImageList;

.field private mIsLandscape:Z

.field private final mLayoutMulti:I

.field private mOriginalFilename:Ljava/lang/String;

.field private mPageList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mPassword:Ljava/lang/String;

.field private mPdfFilename:Ljava/lang/String;

.field private mPdfRenderer:Lepson/print/pdf/AreaPdfRenderer;

.field private mPrintRange:[I

.field private mPrintService:Lepson/print/service/PrintService;

.field private mPrintableArea:[I


# direct methods
.method public constructor <init>(Lepson/print/service/PrintService;Lepson/print/pdf/AreaPdfRenderer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/util/concurrent/LinkedBlockingDeque;[I[II)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lepson/print/service/PrintService;",
            "Lepson/print/pdf/AreaPdfRenderer;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/concurrent/LinkedBlockingDeque<",
            "Ljava/lang/Integer;",
            ">;[I[II)V"
        }
    .end annotation

    const-string v0, "pdf-rendering"

    .line 66
    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    if-eqz p7, :cond_1

    if-eqz p8, :cond_1

    .line 67
    array-length v0, p8

    const/4 v1, 0x2

    if-lt v0, v1, :cond_1

    .line 73
    iput-object p1, p0, Lepson/print/service/PdfRenderThread;->mPrintService:Lepson/print/service/PrintService;

    .line 74
    iput-object p7, p0, Lepson/print/service/PdfRenderThread;->mDeque:Ljava/util/concurrent/LinkedBlockingDeque;

    .line 76
    iput-object p8, p0, Lepson/print/service/PdfRenderThread;->mPrintableArea:[I

    .line 77
    iput-object p9, p0, Lepson/print/service/PdfRenderThread;->mPrintRange:[I

    .line 78
    iput-object p5, p0, Lepson/print/service/PdfRenderThread;->mPassword:Ljava/lang/String;

    .line 79
    iput-boolean p6, p0, Lepson/print/service/PdfRenderThread;->mIsLandscape:Z

    .line 81
    iput-object p2, p0, Lepson/print/service/PdfRenderThread;->mPdfRenderer:Lepson/print/pdf/AreaPdfRenderer;

    .line 82
    iput-object p3, p0, Lepson/print/service/PdfRenderThread;->mPdfFilename:Ljava/lang/String;

    if-eqz p4, :cond_0

    move-object p3, p4

    .line 83
    :cond_0
    iput-object p3, p0, Lepson/print/service/PdfRenderThread;->mOriginalFilename:Ljava/lang/String;

    .line 84
    iput p10, p0, Lepson/print/service/PdfRenderThread;->mLayoutMulti:I

    return-void

    .line 70
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-direct {p1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p1
.end method

.method private copyPreviewPaperRectToPreviewImageRect(Lepson/print/EPImage;)V
    .locals 1
    .param p1    # Lepson/print/EPImage;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 308
    iget v0, p1, Lepson/print/EPImage;->previewPaperRectLeft:I

    int-to-float v0, v0

    iput v0, p1, Lepson/print/EPImage;->previewImageRectLeft:F

    .line 309
    iget v0, p1, Lepson/print/EPImage;->previewPaperRectTop:I

    int-to-float v0, v0

    iput v0, p1, Lepson/print/EPImage;->previewImageRectTop:F

    .line 311
    iget v0, p1, Lepson/print/EPImage;->previewPaperRectRight:I

    int-to-float v0, v0

    iput v0, p1, Lepson/print/EPImage;->previewImageRectRight:F

    .line 312
    iget v0, p1, Lepson/print/EPImage;->previewPaperRectBottom:I

    int-to-float v0, v0

    iput v0, p1, Lepson/print/EPImage;->previewImageRectBottom:F

    return-void
.end method

.method static getRotateFilename(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    const-string v0, "."

    .line 423
    invoke-virtual {p0, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    if-gez v0, :cond_0

    .line 425
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    .line 427
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x0

    invoke-virtual {p0, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "-90"

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, ".bmp"

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method checkFileExists(Ljava/lang/String;)V
    .locals 3
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 398
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 399
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 400
    :cond_0
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "rotate out file \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "] exists"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public createEpImageList()V
    .locals 7

    .line 96
    iget-object v0, p0, Lepson/print/service/PdfRenderThread;->mPdfFilename:Ljava/lang/String;

    .line 97
    iget-object v1, p0, Lepson/print/service/PdfRenderThread;->mPrintRange:[I

    .line 99
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lepson/print/service/PdfRenderThread;->mPageList:Ljava/util/ArrayList;

    .line 100
    new-instance v2, Lepson/print/EPImageList;

    invoke-direct {v2}, Lepson/print/EPImageList;-><init>()V

    iput-object v2, p0, Lepson/print/service/PdfRenderThread;->mEpImageList:Lepson/print/EPImageList;

    .line 103
    iget-object v2, p0, Lepson/print/service/PdfRenderThread;->mPdfRenderer:Lepson/print/pdf/AreaPdfRenderer;

    iget-object v3, p0, Lepson/print/service/PdfRenderThread;->mPassword:Ljava/lang/String;

    iget-object v4, p0, Lepson/print/service/PdfRenderThread;->mPrintService:Lepson/print/service/PrintService;

    invoke-interface {v4}, Lepson/print/service/PrintService;->getLocalApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v2, v0, v3, v4}, Lepson/print/pdf/AreaPdfRenderer;->openPdfFile(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    .line 108
    array-length v3, v1

    if-lt v3, v2, :cond_0

    .line 109
    aget v3, v1, v0

    goto :goto_0

    :cond_0
    const/4 v3, 0x1

    .line 111
    :goto_0
    iget-object v4, p0, Lepson/print/service/PdfRenderThread;->mPdfRenderer:Lepson/print/pdf/AreaPdfRenderer;

    invoke-virtual {v4}, Lepson/print/pdf/AreaPdfRenderer;->totalPages()I

    move-result v4

    if-eqz v1, :cond_1

    .line 112
    array-length v5, v1

    const/4 v6, 0x2

    if-lt v5, v6, :cond_1

    .line 114
    aget v4, v1, v2

    :cond_1
    :goto_1
    if-gt v3, v4, :cond_2

    .line 119
    iget-object v1, p0, Lepson/print/service/PdfRenderThread;->mPageList:Ljava/util/ArrayList;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 120
    new-instance v1, Lepson/print/EPImage;

    invoke-direct {v1}, Lepson/print/EPImage;-><init>()V

    .line 121
    iput v0, v1, Lepson/print/EPImage;->index:I

    .line 122
    invoke-virtual {p0, v3}, Lepson/print/service/PdfRenderThread;->getPdfDecodeFilename(I)Ljava/io/File;

    move-result-object v5

    .line 123
    invoke-virtual {v5}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, Lepson/print/EPImage;->loadImageFileName:Ljava/lang/String;

    .line 124
    iget-object v5, p0, Lepson/print/service/PdfRenderThread;->mOriginalFilename:Ljava/lang/String;

    invoke-virtual {v1, v5}, Lepson/print/EPImage;->setOrgName(Ljava/lang/String;)V

    .line 126
    iget-object v5, p0, Lepson/print/service/PdfRenderThread;->mEpImageList:Lepson/print/EPImageList;

    invoke-virtual {v5, v1}, Lepson/print/EPImageList;->add(Lepson/print/EPImage;)Z

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v0, v2

    goto :goto_1

    :cond_2
    return-void

    .line 104
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method deleteFile(Ljava/lang/String;)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 410
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 411
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    return-void
.end method

.method epImageRotate(Lepson/print/EPImage;I)V
    .locals 2
    .param p1    # Lepson/print/EPImage;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 359
    iget-object v0, p1, Lepson/print/EPImage;->loadImageFileName:Ljava/lang/String;

    .line 361
    :try_start_0
    invoke-static {v0}, Lepson/print/service/PdfRenderThread;->getRotateFilename(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 362
    invoke-virtual {p0, v1}, Lepson/print/service/PdfRenderThread;->checkFileExists(Ljava/lang/String;)V

    .line 363
    invoke-virtual {p0, v0, v1, p2}, Lepson/print/service/PdfRenderThread;->rotateImage(Ljava/lang/String;Ljava/lang/String;I)V

    .line 365
    iput-object v1, p1, Lepson/print/EPImage;->loadImageFileName:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 368
    invoke-virtual {p0, v0}, Lepson/print/service/PdfRenderThread;->deleteFile(Ljava/lang/String;)V

    return-void

    :catchall_0
    move-exception p1

    invoke-virtual {p0, v0}, Lepson/print/service/PdfRenderThread;->deleteFile(Ljava/lang/String;)V

    throw p1
.end method

.method public getImageList()Lepson/print/EPImageList;
    .locals 1

    .line 160
    iget-object v0, p0, Lepson/print/service/PdfRenderThread;->mEpImageList:Lepson/print/EPImageList;

    return-object v0
.end method

.method getLimitAreaSize([I)[I
    .locals 6
    .param p1    # [I
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    const/4 v0, 0x1

    .line 260
    invoke-static {p1, v0}, Lepson/print/pdf/AreaPdfRenderer;->getPortraitOrLandscapeSize([IZ)[I

    move-result-object p1

    .line 261
    invoke-virtual {p0}, Lepson/print/service/PdfRenderThread;->getMaxAreaSize()[I

    move-result-object v1

    invoke-static {v1, v0}, Lepson/print/pdf/AreaPdfRenderer;->getPortraitOrLandscapeSize([IZ)[I

    move-result-object v1

    const/4 v2, 0x2

    .line 263
    new-array v2, v2, [I

    const/4 v3, 0x0

    aget v4, p1, v3

    aget v5, v1, v3

    .line 264
    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    aput v4, v2, v3

    aget p1, p1, v0

    aget v1, v1, v0

    .line 265
    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result p1

    aput p1, v2, v0

    return-object v2
.end method

.method getMaxAreaSize()[I
    .locals 4
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .line 274
    iget-object v0, p0, Lepson/print/service/PdfRenderThread;->mPrintService:Lepson/print/service/PrintService;

    invoke-interface {v0}, Lepson/print/service/PrintService;->getLocalApplicationContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->EPS_MSID_A3:Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;

    .line 275
    invoke-virtual {v1}, Lcom/epson/mobilephone/common/PrintingLib/define/Constants$PaperName;->getCode()I

    move-result v1

    .line 274
    invoke-static {v0, v1}, Lepson/common/Info_paper;->getInfoPaper(Landroid/content/Context;I)Lepson/common/Info_paper;

    move-result-object v0

    const/4 v1, 0x2

    .line 276
    new-array v1, v1, [I

    invoke-virtual {v0}, Lepson/common/Info_paper;->getPaper_width_boder()I

    move-result v2

    const/4 v3, 0x0

    aput v2, v1, v3

    invoke-virtual {v0}, Lepson/common/Info_paper;->getPaper_height_boder()I

    move-result v0

    const/4 v2, 0x1

    aput v0, v1, v2

    return-object v1
.end method

.method getPdfDecodeFilename(I)Ljava/io/File;
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .line 132
    iget-object v0, p0, Lepson/print/service/PdfRenderThread;->mPrintService:Lepson/print/service/PrintService;

    invoke-interface {v0}, Lepson/print/service/PrintService;->getLocalApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lepson/print/pdf/AreaPdfRenderer;->getPdfDecodeFilename(Landroid/content/Context;I)Ljava/io/File;

    move-result-object p1

    return-object p1
.end method

.method getPreviewAreaSize([IZ[I)[I
    .locals 4
    .param p1    # [I
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # [I
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    const/4 v0, 0x0

    .line 245
    aget v1, p3, v0

    const/4 v2, 0x1

    aget v3, p3, v2

    if-gt v1, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-ne v1, p2, :cond_1

    const/4 v0, 0x1

    :cond_1
    invoke-static {p1, v0}, Lepson/print/pdf/AreaPdfRenderer;->getPortraitOrLandscapeSize([IZ)[I

    move-result-object p1

    .line 247
    invoke-static {p3, p1}, Lepson/print/pdf/AreaPdfRenderer;->getCircumscribedSize([I[I)[I

    move-result-object p1

    return-object p1
.end method

.method getRotate(Z)I
    .locals 4
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .line 332
    iget v0, p0, Lepson/print/service/PdfRenderThread;->mLayoutMulti:I

    const/16 v1, 0x10e

    const/16 v2, 0x5a

    const/high16 v3, 0x10000

    if-eq v0, v3, :cond_1

    if-eqz p1, :cond_0

    return v1

    :cond_0
    return v2

    :cond_1
    if-nez p1, :cond_2

    return v1

    :cond_2
    return v2
.end method

.method localGetImageSize(Ljava/lang/String;)[I
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .line 322
    invoke-static {p1}, Lepson/common/ImageUtil;->getImageSize2(Ljava/lang/String;)Landroid/graphics/BitmapFactory$Options;

    move-result-object p1

    const/4 v0, 0x2

    .line 323
    new-array v0, v0, [I

    iget v1, p1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    const/4 v2, 0x0

    aput v1, v0, v2

    iget p1, p1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    const/4 v1, 0x1

    aput p1, v0, v1

    return-object v0
.end method

.method renderPdf()Z
    .locals 12
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .line 186
    iget-object v0, p0, Lepson/print/service/PdfRenderThread;->mPdfRenderer:Lepson/print/pdf/AreaPdfRenderer;

    if-eqz v0, :cond_6

    iget-object v1, p0, Lepson/print/service/PdfRenderThread;->mEpImageList:Lepson/print/EPImageList;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lepson/print/service/PdfRenderThread;->mDeque:Ljava/util/concurrent/LinkedBlockingDeque;

    if-eqz v1, :cond_6

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 192
    :try_start_0
    invoke-virtual {v0, v1}, Lepson/print/pdf/AreaPdfRenderer;->isPageLandscape(I)Z

    move-result v0

    .line 194
    iget-boolean v3, p0, Lepson/print/service/PdfRenderThread;->mIsLandscape:Z

    if-ne v0, v3, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    .line 195
    :goto_0
    iget-object v4, p0, Lepson/print/service/PdfRenderThread;->mPrintableArea:[I

    invoke-virtual {p0, v4}, Lepson/print/service/PdfRenderThread;->getLimitAreaSize([I)[I

    move-result-object v4

    const/4 v5, 0x0

    .line 197
    :goto_1
    iget-object v6, p0, Lepson/print/service/PdfRenderThread;->mPageList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v5, v6, :cond_5

    .line 198
    iget-object v6, p0, Lepson/print/service/PdfRenderThread;->mPageList:Ljava/util/ArrayList;

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 199
    iget-object v7, p0, Lepson/print/service/PdfRenderThread;->mPrintService:Lepson/print/service/PrintService;

    invoke-interface {v7}, Lepson/print/service/PrintService;->getCancelPrinting()Z

    move-result v7

    if-eqz v7, :cond_1

    return v2

    .line 203
    :cond_1
    iget-object v7, p0, Lepson/print/service/PdfRenderThread;->mPdfRenderer:Lepson/print/pdf/AreaPdfRenderer;

    invoke-virtual {v7, v6}, Lepson/print/pdf/AreaPdfRenderer;->isPageLandscape(I)Z

    move-result v7

    if-eq v7, v3, :cond_2

    const/4 v8, 0x1

    goto :goto_2

    :cond_2
    const/4 v8, 0x0

    .line 205
    :goto_2
    invoke-static {v4, v8}, Lepson/print/pdf/AreaPdfRenderer;->getPortraitOrLandscapeSize([IZ)[I

    move-result-object v8

    .line 208
    iget-object v9, p0, Lepson/print/service/PdfRenderThread;->mEpImageList:Lepson/print/EPImageList;

    invoke-virtual {v9, v5}, Lepson/print/EPImageList;->get(I)Lepson/print/EPImage;

    move-result-object v9

    .line 209
    iget-object v10, p0, Lepson/print/service/PdfRenderThread;->mPdfRenderer:Lepson/print/pdf/AreaPdfRenderer;

    iget-object v11, v9, Lepson/print/EPImage;->loadImageFileName:Ljava/lang/String;

    invoke-virtual {v10, v11, v6, v8}, Lepson/print/pdf/AreaPdfRenderer;->convertPageForPrint(Ljava/lang/String;I[I)Z

    move-result v6

    if-nez v6, :cond_3

    return v2

    :cond_3
    if-eq v0, v7, :cond_4

    .line 216
    invoke-virtual {p0, v0}, Lepson/print/service/PdfRenderThread;->getRotate(Z)I

    move-result v6

    .line 217
    invoke-virtual {p0, v9, v6}, Lepson/print/service/PdfRenderThread;->epImageRotate(Lepson/print/EPImage;I)V

    .line 221
    :cond_4
    iget-object v6, v9, Lepson/print/EPImage;->loadImageFileName:Ljava/lang/String;

    invoke-virtual {p0, v6}, Lepson/print/service/PdfRenderThread;->localGetImageSize(Ljava/lang/String;)[I

    move-result-object v6

    .line 222
    iget-object v7, p0, Lepson/print/service/PdfRenderThread;->mPrintableArea:[I

    invoke-virtual {p0, v7, v3, v6}, Lepson/print/service/PdfRenderThread;->getPreviewAreaSize([IZ[I)[I

    move-result-object v7

    .line 225
    invoke-virtual {p0, v9, v6, v7}, Lepson/print/service/PdfRenderThread;->setEPImage(Lepson/print/EPImage;[I[I)V

    .line 227
    iget-object v6, p0, Lepson/print/service/PdfRenderThread;->mDeque:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/concurrent/LinkedBlockingDeque;->offerLast(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_5
    return v1

    :catch_0
    return v2

    .line 187
    :cond_6
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method rotateImage(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 380
    new-instance v0, Lepson/print/EPImageUtil;

    invoke-direct {v0}, Lepson/print/EPImageUtil;-><init>()V

    .line 381
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ".bmp"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    .line 383
    invoke-virtual {v0, p1, v1, v2}, Lepson/print/EPImageUtil;->jpg2bmp(Ljava/lang/String;Ljava/lang/String;I)I

    move-result p1

    if-nez p1, :cond_1

    .line 387
    invoke-virtual {v0, v1, p2, p3}, Lepson/print/EPImageUtil;->rotateImage(Ljava/lang/String;Ljava/lang/String;I)I

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 388
    :cond_0
    new-instance p1, Ljava/io/IOException;

    invoke-direct {p1}, Ljava/io/IOException;-><init>()V

    throw p1

    .line 384
    :cond_1
    new-instance p1, Ljava/io/IOException;

    invoke-direct {p1}, Ljava/io/IOException;-><init>()V

    throw p1
.end method

.method public run()V
    .locals 0

    .line 170
    invoke-virtual {p0}, Lepson/print/service/PdfRenderThread;->renderPdf()Z

    return-void
.end method

.method setEPImage(Lepson/print/EPImage;[I[I)V
    .locals 2
    .param p1    # Lepson/print/EPImage;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # [I
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # [I
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    const/4 v0, 0x0

    .line 289
    aget v1, p2, v0

    iput v1, p1, Lepson/print/EPImage;->srcWidth:I

    const/4 v1, 0x1

    .line 290
    aget p2, p2, v1

    iput p2, p1, Lepson/print/EPImage;->srcHeight:I

    .line 291
    iget-boolean p2, p0, Lepson/print/service/PdfRenderThread;->mIsLandscape:Z

    iput-boolean p2, p1, Lepson/print/EPImage;->isPaperLandScape:Z

    .line 293
    iput v0, p1, Lepson/print/EPImage;->previewPaperRectTop:I

    .line 294
    iput v0, p1, Lepson/print/EPImage;->previewPaperRectLeft:I

    .line 296
    aget p2, p3, v0

    iput p2, p1, Lepson/print/EPImage;->previewPaperRectRight:I

    .line 297
    aget p2, p3, v1

    iput p2, p1, Lepson/print/EPImage;->previewPaperRectBottom:I

    .line 300
    invoke-direct {p0, p1}, Lepson/print/service/PdfRenderThread;->copyPreviewPaperRectToPreviewImageRect(Lepson/print/EPImage;)V

    .line 303
    iget p2, p1, Lepson/print/EPImage;->previewImageRectRight:F

    iget p3, p1, Lepson/print/EPImage;->previewImageRectLeft:F

    sub-float/2addr p2, p3

    float-to-int p2, p2

    iput p2, p1, Lepson/print/EPImage;->previewWidth:I

    .line 304
    iget p2, p1, Lepson/print/EPImage;->previewImageRectBottom:F

    iget p3, p1, Lepson/print/EPImage;->previewImageRectTop:F

    sub-float/2addr p2, p3

    float-to-int p2, p2

    iput p2, p1, Lepson/print/EPImage;->previewHeight:I

    return-void
.end method

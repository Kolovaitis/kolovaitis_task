.class public abstract Lepson/print/service/IEpsonService$Stub;
.super Landroid/os/Binder;
.source "IEpsonService.java"

# interfaces
.implements Lepson/print/service/IEpsonService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lepson/print/service/IEpsonService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lepson/print/service/IEpsonService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "epson.print.service.IEpsonService"

.field static final TRANSACTION_EpsonConnectCancel:I = 0x25

.field static final TRANSACTION_EpsonConnectChangePrintSetting:I = 0x1b

.field static final TRANSACTION_EpsonConnectCheckSupportedFileType:I = 0x1f

.field static final TRANSACTION_EpsonConnectCreateJob:I = 0x1a

.field static final TRANSACTION_EpsonConnectEndJob:I = 0x1e

.field static final TRANSACTION_EpsonConnectGetPreview:I = 0x21

.field static final TRANSACTION_EpsonConnectGetRemotePrintMaxFileSize:I = 0x24

.field static final TRANSACTION_EpsonConnectGetRenderingStatus:I = 0x20

.field static final TRANSACTION_EpsonConnectGetSupportedMedia:I = 0x22

.field static final TRANSACTION_EpsonConnectStartPrint:I = 0x1d

.field static final TRANSACTION_EpsonConnectUpdatePrinterSettings:I = 0x19

.field static final TRANSACTION_EpsonConnectUploadFile:I = 0x1c

.field static final TRANSACTION_cancelPrint:I = 0x13

.field static final TRANSACTION_cancelSearchPrinter:I = 0x4

.field static final TRANSACTION_confirmCancel:I = 0x14

.field static final TRANSACTION_confirmContinueable:I = 0x15

.field static final TRANSACTION_ensureLogin:I = 0x23

.field static final TRANSACTION_getColor:I = 0xe

.field static final TRANSACTION_getDuplex:I = 0xf

.field static final TRANSACTION_getLang:I = 0x7

.field static final TRANSACTION_getLayout:I = 0xb

.field static final TRANSACTION_getPaperSize:I = 0x9

.field static final TRANSACTION_getPaperSource:I = 0xd

.field static final TRANSACTION_getPaperType:I = 0xa

.field static final TRANSACTION_getQuality:I = 0xc

.field static final TRANSACTION_getSupportedMedia:I = 0x8

.field static final TRANSACTION_isPrinting:I = 0x17

.field static final TRANSACTION_isSearchingPrinter:I = 0x16

.field static final TRANSACTION_print:I = 0x10

.field static final TRANSACTION_printLocalPdf:I = 0x11

.field static final TRANSACTION_printWithImagesAndLayouts:I = 0x12

.field static final TRANSACTION_refreshRemotePrinterLogin:I = 0x26

.field static final TRANSACTION_registerCallback:I = 0x1

.field static final TRANSACTION_searchPrinters:I = 0x3

.field static final TRANSACTION_setCurPrinter:I = 0x6

.field static final TRANSACTION_setTimeOut:I = 0x5

.field static final TRANSACTION_unregisterCallback:I = 0x2

.field static final TRANSACTION_updatePrinterSettings:I = 0x18


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    const-string v0, "epson.print.service.IEpsonService"

    .line 15
    invoke-virtual {p0, p0, v0}, Lepson/print/service/IEpsonService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lepson/print/service/IEpsonService;
    .locals 2

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    const-string v0, "epson.print.service.IEpsonService"

    .line 26
    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 27
    instance-of v1, v0, Lepson/print/service/IEpsonService;

    if-eqz v1, :cond_1

    .line 28
    check-cast v0, Lepson/print/service/IEpsonService;

    return-object v0

    .line 30
    :cond_1
    new-instance v0, Lepson/print/service/IEpsonService$Stub$Proxy;

    invoke-direct {v0, p0}, Lepson/print/service/IEpsonService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    return-object v0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 20
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    move-object/from16 v15, p0

    move/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v14, p3

    const-string v2, "epson.print.service.IEpsonService"

    const v3, 0x5f4e5446

    const/16 v16, 0x1

    if-eq v0, v3, :cond_b

    const/4 v3, 0x0

    const/4 v4, 0x0

    packed-switch v0, :pswitch_data_0

    .line 495
    invoke-super/range {p0 .. p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    return v0

    .line 488
    :pswitch_0
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 489
    invoke-virtual/range {p0 .. p0}, Lepson/print/service/IEpsonService$Stub;->refreshRemotePrinterLogin()V

    .line 490
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    return v16

    .line 481
    :pswitch_1
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 482
    invoke-virtual/range {p0 .. p0}, Lepson/print/service/IEpsonService$Stub;->EpsonConnectCancel()V

    .line 483
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    return v16

    .line 473
    :pswitch_2
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 474
    invoke-virtual/range {p0 .. p0}, Lepson/print/service/IEpsonService$Stub;->EpsonConnectGetRemotePrintMaxFileSize()I

    move-result v0

    .line 475
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 476
    invoke-virtual {v14, v0}, Landroid/os/Parcel;->writeInt(I)V

    return v16

    .line 463
    :pswitch_3
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 465
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v4, 0x1

    .line 466
    :cond_0
    invoke-virtual {v15, v4}, Lepson/print/service/IEpsonService$Stub;->ensureLogin(Z)I

    move-result v0

    .line 467
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 468
    invoke-virtual {v14, v0}, Landroid/os/Parcel;->writeInt(I)V

    return v16

    .line 449
    :pswitch_4
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 451
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 453
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 455
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 456
    invoke-virtual {v15, v0, v2, v1}, Lepson/print/service/IEpsonService$Stub;->EpsonConnectGetSupportedMedia(ILjava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 457
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 458
    invoke-virtual {v14, v0}, Landroid/os/Parcel;->writeInt(I)V

    return v16

    .line 437
    :pswitch_5
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 439
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 441
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 442
    invoke-virtual {v15, v0, v1}, Lepson/print/service/IEpsonService$Stub;->EpsonConnectGetPreview(ILjava/lang/String;)I

    move-result v0

    .line 443
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 444
    invoke-virtual {v14, v0}, Landroid/os/Parcel;->writeInt(I)V

    return v16

    .line 420
    :pswitch_6
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 422
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-gez v0, :cond_1

    goto :goto_0

    .line 427
    :cond_1
    new-array v3, v0, [I

    .line 429
    :goto_0
    invoke-virtual {v15, v3}, Lepson/print/service/IEpsonService$Stub;->EpsonConnectGetRenderingStatus([I)I

    move-result v0

    .line 430
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 431
    invoke-virtual {v14, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 432
    invoke-virtual {v14, v3}, Landroid/os/Parcel;->writeIntArray([I)V

    return v16

    .line 410
    :pswitch_7
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 412
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 413
    invoke-virtual {v15, v0}, Lepson/print/service/IEpsonService$Stub;->EpsonConnectCheckSupportedFileType(Ljava/lang/String;)I

    move-result v0

    .line 414
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 415
    invoke-virtual {v14, v0}, Landroid/os/Parcel;->writeInt(I)V

    return v16

    .line 402
    :pswitch_8
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 403
    invoke-virtual/range {p0 .. p0}, Lepson/print/service/IEpsonService$Stub;->EpsonConnectEndJob()I

    move-result v0

    .line 404
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 405
    invoke-virtual {v14, v0}, Landroid/os/Parcel;->writeInt(I)V

    return v16

    .line 390
    :pswitch_9
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 392
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 394
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 395
    invoke-virtual {v15, v0, v1}, Lepson/print/service/IEpsonService$Stub;->EpsonConnectStartPrint(II)I

    move-result v0

    .line 396
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 397
    invoke-virtual {v14, v0}, Landroid/os/Parcel;->writeInt(I)V

    return v16

    .line 378
    :pswitch_a
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 380
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 382
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 383
    invoke-virtual {v15, v0, v1}, Lepson/print/service/IEpsonService$Stub;->EpsonConnectUploadFile(Ljava/lang/String;I)I

    move-result v0

    .line 384
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 385
    invoke-virtual {v14, v0}, Landroid/os/Parcel;->writeInt(I)V

    return v16

    .line 370
    :pswitch_b
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 371
    invoke-virtual/range {p0 .. p0}, Lepson/print/service/IEpsonService$Stub;->EpsonConnectChangePrintSetting()I

    move-result v0

    .line 372
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 373
    invoke-virtual {v14, v0}, Landroid/os/Parcel;->writeInt(I)V

    return v16

    .line 332
    :pswitch_c
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 334
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 336
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 338
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 340
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    .line 342
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .line 344
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    .line 346
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    .line 348
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    .line 350
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    .line 352
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v11

    .line 354
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v12

    .line 356
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v13

    .line 358
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v17

    .line 360
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v18

    .line 362
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v19

    move-object/from16 v0, p0

    move v1, v2

    move-object v2, v3

    move v3, v4

    move v4, v5

    move v5, v6

    move v6, v7

    move v7, v8

    move v8, v9

    move v9, v10

    move v10, v11

    move v11, v12

    move v12, v13

    move/from16 v13, v17

    move/from16 v14, v18

    move/from16 v15, v19

    .line 363
    invoke-virtual/range {v0 .. v15}, Lepson/print/service/IEpsonService$Stub;->EpsonConnectCreateJob(ILjava/lang/String;IIIIIIIIIIIII)I

    move-result v0

    .line 364
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    move-object/from16 v8, p3

    .line 365
    invoke-virtual {v8, v0}, Landroid/os/Parcel;->writeInt(I)V

    return v16

    :pswitch_d
    move-object v8, v14

    .line 322
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 324
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    move-object/from16 v9, p0

    .line 325
    invoke-virtual {v9, v0}, Lepson/print/service/IEpsonService$Stub;->EpsonConnectUpdatePrinterSettings(Ljava/lang/String;)I

    move-result v0

    .line 326
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 327
    invoke-virtual {v8, v0}, Landroid/os/Parcel;->writeInt(I)V

    return v16

    :pswitch_e
    move-object v8, v14

    move-object v9, v15

    .line 312
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 314
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 315
    invoke-virtual {v9, v0}, Lepson/print/service/IEpsonService$Stub;->updatePrinterSettings(Ljava/lang/String;)I

    move-result v0

    .line 316
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 317
    invoke-virtual {v8, v0}, Landroid/os/Parcel;->writeInt(I)V

    return v16

    :pswitch_f
    move-object v8, v14

    move-object v9, v15

    .line 304
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 305
    invoke-virtual/range {p0 .. p0}, Lepson/print/service/IEpsonService$Stub;->isPrinting()Z

    move-result v0

    .line 306
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 307
    invoke-virtual {v8, v0}, Landroid/os/Parcel;->writeInt(I)V

    return v16

    :pswitch_10
    move-object v8, v14

    move-object v9, v15

    .line 296
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 297
    invoke-virtual/range {p0 .. p0}, Lepson/print/service/IEpsonService$Stub;->isSearchingPrinter()Z

    move-result v0

    .line 298
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 299
    invoke-virtual {v8, v0}, Landroid/os/Parcel;->writeInt(I)V

    return v16

    :pswitch_11
    move-object v8, v14

    move-object v9, v15

    .line 286
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 288
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v4, 0x1

    .line 289
    :cond_2
    invoke-virtual {v9, v4}, Lepson/print/service/IEpsonService$Stub;->confirmContinueable(Z)I

    move-result v0

    .line 290
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 291
    invoke-virtual {v8, v0}, Landroid/os/Parcel;->writeInt(I)V

    return v16

    :pswitch_12
    move-object v8, v14

    move-object v9, v15

    .line 276
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 278
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_3

    const/4 v4, 0x1

    .line 279
    :cond_3
    invoke-virtual {v9, v4}, Lepson/print/service/IEpsonService$Stub;->confirmCancel(Z)I

    move-result v0

    .line 280
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 281
    invoke-virtual {v8, v0}, Landroid/os/Parcel;->writeInt(I)V

    return v16

    :pswitch_13
    move-object v8, v14

    move-object v9, v15

    .line 268
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 269
    invoke-virtual/range {p0 .. p0}, Lepson/print/service/IEpsonService$Stub;->cancelPrint()I

    move-result v0

    .line 270
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 271
    invoke-virtual {v8, v0}, Landroid/os/Parcel;->writeInt(I)V

    return v16

    :pswitch_14
    move-object v8, v14

    move-object v9, v15

    .line 256
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 258
    sget-object v0, Lcom/epson/cameracopy/printlayout/ImageAndLayout;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v0

    .line 260
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_4

    const/4 v4, 0x1

    .line 261
    :cond_4
    invoke-virtual {v9, v0, v4}, Lepson/print/service/IEpsonService$Stub;->printWithImagesAndLayouts(Ljava/util/List;Z)I

    move-result v0

    .line 262
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 263
    invoke-virtual {v8, v0}, Landroid/os/Parcel;->writeInt(I)V

    return v16

    :pswitch_15
    move-object v8, v14

    move-object v9, v15

    .line 234
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 236
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 238
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 240
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .line 242
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_5

    const/4 v6, 0x1

    goto :goto_1

    :cond_5
    const/4 v6, 0x0

    .line 244
    :goto_1
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    .line 246
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    .line 248
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_6

    const/4 v11, 0x1

    goto :goto_2

    :cond_6
    const/4 v11, 0x0

    :goto_2
    move-object/from16 v0, p0

    move-object v1, v2

    move-object v2, v3

    move-object v3, v5

    move v4, v6

    move v5, v7

    move v6, v10

    move v7, v11

    .line 249
    invoke-virtual/range {v0 .. v7}, Lepson/print/service/IEpsonService$Stub;->printLocalPdf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIIZ)I

    move-result v0

    .line 250
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 251
    invoke-virtual {v8, v0}, Landroid/os/Parcel;->writeInt(I)V

    return v16

    :pswitch_16
    move-object v8, v14

    move-object v9, v15

    .line 215
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 217
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_7

    .line 218
    sget-object v0, Lepson/print/EPImageList;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lepson/print/EPImageList;

    .line 224
    :cond_7
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 226
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_8

    const/4 v4, 0x1

    .line 227
    :cond_8
    invoke-virtual {v9, v3, v0, v4}, Lepson/print/service/IEpsonService$Stub;->print(Lepson/print/EPImageList;Ljava/lang/String;Z)I

    move-result v0

    .line 228
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 229
    invoke-virtual {v8, v0}, Landroid/os/Parcel;->writeInt(I)V

    return v16

    :pswitch_17
    move-object v8, v14

    move-object v9, v15

    .line 203
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 205
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 207
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 208
    invoke-virtual {v9, v0, v1}, Lepson/print/service/IEpsonService$Stub;->getDuplex(II)I

    move-result v0

    .line 209
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 210
    invoke-virtual {v8, v0}, Landroid/os/Parcel;->writeInt(I)V

    return v16

    :pswitch_18
    move-object v8, v14

    move-object v9, v15

    .line 187
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 189
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_9

    const/4 v4, 0x1

    .line 191
    :cond_9
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 193
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 195
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 196
    invoke-virtual {v9, v4, v0, v2, v1}, Lepson/print/service/IEpsonService$Stub;->getColor(ZIII)[I

    move-result-object v0

    .line 197
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 198
    invoke-virtual {v8, v0}, Landroid/os/Parcel;->writeIntArray([I)V

    return v16

    :pswitch_19
    move-object v8, v14

    move-object v9, v15

    .line 173
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 175
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 177
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 179
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 180
    invoke-virtual {v9, v0, v2, v1}, Lepson/print/service/IEpsonService$Stub;->getPaperSource(III)[I

    move-result-object v0

    .line 181
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 182
    invoke-virtual {v8, v0}, Landroid/os/Parcel;->writeIntArray([I)V

    return v16

    :pswitch_1a
    move-object v8, v14

    move-object v9, v15

    .line 161
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 163
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 165
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 166
    invoke-virtual {v9, v0, v1}, Lepson/print/service/IEpsonService$Stub;->getQuality(II)[I

    move-result-object v0

    .line 167
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 168
    invoke-virtual {v8, v0}, Landroid/os/Parcel;->writeIntArray([I)V

    return v16

    :pswitch_1b
    move-object v8, v14

    move-object v9, v15

    .line 149
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 151
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 153
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 154
    invoke-virtual {v9, v0, v1}, Lepson/print/service/IEpsonService$Stub;->getLayout(II)[I

    move-result-object v0

    .line 155
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 156
    invoke-virtual {v8, v0}, Landroid/os/Parcel;->writeIntArray([I)V

    return v16

    :pswitch_1c
    move-object v8, v14

    move-object v9, v15

    .line 139
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 141
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 142
    invoke-virtual {v9, v0}, Lepson/print/service/IEpsonService$Stub;->getPaperType(I)[I

    move-result-object v0

    .line 143
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 144
    invoke-virtual {v8, v0}, Landroid/os/Parcel;->writeIntArray([I)V

    return v16

    :pswitch_1d
    move-object v8, v14

    move-object v9, v15

    .line 131
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 132
    invoke-virtual/range {p0 .. p0}, Lepson/print/service/IEpsonService$Stub;->getPaperSize()[I

    move-result-object v0

    .line 133
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 134
    invoke-virtual {v8, v0}, Landroid/os/Parcel;->writeIntArray([I)V

    return v16

    :pswitch_1e
    move-object v8, v14

    move-object v9, v15

    .line 115
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 117
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_a

    const/4 v4, 0x1

    .line 119
    :cond_a
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 121
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 123
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 124
    invoke-virtual {v9, v4, v0, v2, v1}, Lepson/print/service/IEpsonService$Stub;->getSupportedMedia(ZILjava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 125
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 126
    invoke-virtual {v8, v0}, Landroid/os/Parcel;->writeInt(I)V

    return v16

    :pswitch_1f
    move-object v8, v14

    move-object v9, v15

    .line 107
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 108
    invoke-virtual/range {p0 .. p0}, Lepson/print/service/IEpsonService$Stub;->getLang()I

    move-result v0

    .line 109
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 110
    invoke-virtual {v8, v0}, Landroid/os/Parcel;->writeInt(I)V

    return v16

    :pswitch_20
    move-object v8, v14

    move-object v9, v15

    .line 97
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 99
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 100
    invoke-virtual {v9, v0}, Lepson/print/service/IEpsonService$Stub;->setCurPrinter(I)I

    move-result v0

    .line 101
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 102
    invoke-virtual {v8, v0}, Landroid/os/Parcel;->writeInt(I)V

    return v16

    :pswitch_21
    move-object v8, v14

    move-object v9, v15

    .line 88
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 90
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 91
    invoke-virtual {v9, v0}, Lepson/print/service/IEpsonService$Stub;->setTimeOut(I)V

    .line 92
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    return v16

    :pswitch_22
    move-object v8, v14

    move-object v9, v15

    .line 80
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 81
    invoke-virtual/range {p0 .. p0}, Lepson/print/service/IEpsonService$Stub;->cancelSearchPrinter()I

    move-result v0

    .line 82
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 83
    invoke-virtual {v8, v0}, Landroid/os/Parcel;->writeInt(I)V

    return v16

    :pswitch_23
    move-object v8, v14

    move-object v9, v15

    .line 66
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 68
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 70
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 72
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 73
    invoke-virtual {v9, v0, v2, v1}, Lepson/print/service/IEpsonService$Stub;->searchPrinters(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    .line 74
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 75
    invoke-virtual {v8, v0}, Landroid/os/Parcel;->writeInt(I)V

    return v16

    :pswitch_24
    move-object v8, v14

    move-object v9, v15

    .line 57
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 59
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lepson/print/service/IEpsonServiceCallback$Stub;->asInterface(Landroid/os/IBinder;)Lepson/print/service/IEpsonServiceCallback;

    move-result-object v0

    .line 60
    invoke-virtual {v9, v0}, Lepson/print/service/IEpsonService$Stub;->unregisterCallback(Lepson/print/service/IEpsonServiceCallback;)V

    .line 61
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    return v16

    :pswitch_25
    move-object v8, v14

    move-object v9, v15

    .line 48
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 50
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lepson/print/service/IEpsonServiceCallback$Stub;->asInterface(Landroid/os/IBinder;)Lepson/print/service/IEpsonServiceCallback;

    move-result-object v0

    .line 51
    invoke-virtual {v9, v0}, Lepson/print/service/IEpsonService$Stub;->registerCallback(Lepson/print/service/IEpsonServiceCallback;)V

    .line 52
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    return v16

    :cond_b
    move-object v8, v14

    move-object v9, v15

    .line 43
    invoke-virtual {v8, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return v16

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_25
        :pswitch_24
        :pswitch_23
        :pswitch_22
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

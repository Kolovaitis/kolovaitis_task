.class public interface abstract Lepson/print/service/RenderingController;
.super Ljava/lang/Object;
.source "RenderingController.java"


# virtual methods
.method public abstract drawBeforeStartJob()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lepson/print/service/LocalInterrupt;
        }
    .end annotation
.end method

.method public abstract getImageList()Lepson/print/EPImageList;
.end method

.method public abstract getKind()Lepson/print/screen/PrintSetting$Kind;
.end method

.method public abstract interruptSubThreads()V
.end method

.method public abstract joinSubThread(J)V
.end method

.method public abstract startDrawAfterStartJob(I)V
.end method

.method public abstract waitPage(II)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method
